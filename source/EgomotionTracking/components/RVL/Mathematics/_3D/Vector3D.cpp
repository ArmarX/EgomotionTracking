/*
 * Vector3D.cpp
 */

#include "Vector3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            const CVector3D CVector3D::s_PlusInfinity(g_RealPlusInfinity, g_RealPlusInfinity, g_RealPlusInfinity);
            const CVector3D CVector3D::s_Zero(Real(0), Real(0), Real(0));
            const CVector3D CVector3D::s_MinusInfinity(g_RealMinusInfinity, g_RealMinusInfinity, g_RealMinusInfinity);
            const CVector3D CVector3D::s_Unitary_X(Real(1), Real(0), Real(0));
            const CVector3D CVector3D::s_Unitary_Y(Real(0), Real(1), Real(0));
            const CVector3D CVector3D::s_Unitary_Z(Real(0), Real(0), Real(1));

            CVector3D CVector3D::CreateLinealCombination(const CVector3D& A, const Real SA, const CVector3D& B, const Real SB)
            {
                return CVector3D(A.m_X * SA + B.m_X * SB, A.m_Y * SA + B.m_Y * SB, A.m_Z * SA + B.m_Z * SB);
            }

            CVector3D CVector3D::CreateComplementaryLinealCombination(const CVector3D& A, const CVector3D& B, const Real SA)
            {
                const Real SB = Real(1) - SA;
                return CVector3D(A.m_X * SA + B.m_X * SB, A.m_Y * SA + B.m_Y * SB, A.m_Z * SA + B.m_Z * SB);
            }

            CVector3D CVector3D::CreateMidPoint(const CVector3D& A, const CVector3D& B)
            {
                return CVector3D((A.m_X + B.m_X) * Real(0.5), (A.m_Y + B.m_Y) * Real(0.5), (A.m_Z + B.m_Z) * Real(0.5));
            }

            CVector3D CVector3D::CreateNormalized(const CVector3D& Vector)
            {
                CVector3D NormalizedVector(Vector);
                NormalizedVector.Normalize();
                return NormalizedVector;
            }

            CVector3D CVector3D::CreateCannonicalForm(const Real X, const Real Y, const Real Z)
            {
                const Real Components[3] = { X, Y, Z };
                Real Maximal = g_RealPlusEpsilon;
                int Index = -1;
                for (int i = 0 ; i < 3 ; ++i)
                {
                    const Real Magnitud = std::abs(Components[i]);
                    if (Magnitud > Maximal)
                    {
                        Index = i;
                        Maximal = Magnitud;
                    }
                }
                if (Index >= 0)
                {
                    if (IsNegative(Components[Index]))
                    {
                        return CVector3D(-X, -Y, -Z);
                    }
                    return CVector3D(X, Y, Z);
                }
                return CVector3D();
            }

            CVector3D::CVector3D() :
                m_X(Real(0)),
                m_Y(Real(0)),
                m_Z(Real(0))
            {
            }

            CVector3D::CVector3D(const Real X, const Real Y, const Real Z) :
                m_X(X),
                m_Y(Y),
                m_Z(Z)
            {
            }

            CVector3D::CVector3D(const CVector3D& Vector)

                = default;

            CVector3D::~CVector3D()
                = default;

            bool CVector3D::SetCannonicalForm()
            {
                const Real Components[3] = { m_X, m_Y, m_Z };
                Real Maximal = g_RealPlusEpsilon;
                int Index = -1;
                for (int i = 0 ; i < 3 ; ++i)
                {
                    const Real Magnitud = std::abs(Components[i]);
                    if (Magnitud > Maximal)
                    {
                        Index = i;
                        Maximal = Magnitud;
                    }
                }
                if (Index >= 0)
                {
                    if (IsNegative(Components[Index]))
                    {
                        Negate();
                    }
                    return true;
                }
                return false;
            }

            void CVector3D::SetX(const Real X)
            {
                m_X = X;
            }

            void CVector3D::SetY(const Real Y)
            {
                m_Y = Y;
            }

            void CVector3D::SetZ(const Real Z)
            {
                m_Z = Z;
            }

            void CVector3D::Set(const Real X, const Real Y, const Real Z)
            {
                m_X = X;
                m_Y = Y;
                m_Z = Z;
            }

            void CVector3D::SetMaximalComponentValue(const Real X, const Real Y, const Real Z)
            {
                if (X > m_X)
                {
                    m_X = X;
                }
                if (Y > m_Y)
                {
                    m_Y = Y;
                }
                if (Z > m_Z)
                {
                    m_Z = Z;
                }
            }

            void CVector3D::SetMinimalComponentValue(const Real X, const Real Y, const Real Z)
            {
                if (X < m_X)
                {
                    m_X = X;
                }
                if (Y < m_Y)
                {
                    m_Y = Y;
                }
                if (Z < m_Z)
                {
                    m_Z = Z;
                }
            }

            void CVector3D::SetMaximalComponentValue(const CVector3D& Vector)
            {
                if (Vector.m_X > m_X)
                {
                    m_X = Vector.m_X;
                }
                if (Vector.m_Y > m_Y)
                {
                    m_Y = Vector.m_Y;
                }
                if (Vector.m_Z > m_Z)
                {
                    m_Z = Vector.m_Z;
                }
            }

            void CVector3D::SetMinimalComponentValue(const CVector3D& Vector)
            {
                if (Vector.m_X < m_X)
                {
                    m_X = Vector.m_X;
                }
                if (Vector.m_Y < m_Y)
                {
                    m_Y = Vector.m_Y;
                }
                if (Vector.m_Z < m_Z)
                {
                    m_Z = Vector.m_Z;
                }
            }

            void CVector3D::SetMidPoint(const CVector3D& A, const CVector3D& B)
            {
                m_X = (A.m_X + B.m_X) * Real(0.5);
                m_Y = (A.m_Y + B.m_Y) * Real(0.5);
                m_Z = (A.m_Z + B.m_Z) * Real(0.5);
            }

            void CVector3D::SetWeighted(const CVector3D& Vector, const Real Weight)
            {
                m_X = Vector.m_X * Weight;
                m_Y = Vector.m_Y * Weight;
                m_Z = Vector.m_Z * Weight;
            }

            void CVector3D::SetWeightedInverse(const CVector3D& Vector, const Real Weight)
            {
                m_X = Vector.m_X / Weight;
                m_Y = Vector.m_Y / Weight;
                m_Z = Vector.m_Z / Weight;
            }

            void CVector3D::SetLinealCombination(const CVector3D& A, const Real SA, const CVector3D& B, const Real SB)
            {
                m_X = A.m_X * SA + B.m_X * SB;
                m_Y = A.m_Y * SA + B.m_Y * SB;
                m_Z = A.m_Z * SA + B.m_Z * SB;
            }

            void CVector3D::SetZero()
            {
                m_Z = m_Y = m_X = Real(0);
            }

            void CVector3D::SetAtPlusInfinity()
            {
                m_Z = m_Y = m_X = g_RealPlusInfinity;
            }

            void CVector3D::SetAtMinusInfinity()
            {
                m_Z = m_Y = m_X = g_RealMinusInfinity;
            }

            void CVector3D::Negate()
            {
                m_X = -m_X;
                m_Y = -m_Y;
                m_Z = -m_Z;
            }

            void CVector3D::AddWeightedOffset(const CVector3D& Vector, const Real Weight)
            {
                m_X += Vector.m_X * Weight;
                m_Y += Vector.m_Y * Weight;
                m_Z += Vector.m_Z * Weight;
            }

            void CVector3D::AddOffset(const Real DX, const Real DY, const Real DZ)
            {
                m_X += DX;
                m_Y += DY;
                m_Z += DZ;
            }

            void CVector3D::AddOffsetX(const Real DX)
            {
                m_X += DX;
            }

            void CVector3D::AddOffsetY(const Real DY)
            {
                m_Y += DY;
            }

            void CVector3D::AddOffsetZ(const Real DZ)
            {
                m_Z += DZ;
            }

            void CVector3D::SubtractOffsetX(const Real DX)
            {
                m_X -= DX;
            }

            void CVector3D::SubtractOffsetY(const Real DY)
            {
                m_Y -= DY;
            }

            void CVector3D::SubtractOffsetZ(const Real DZ)
            {
                m_Z -= DZ;
            }

            void CVector3D::AddCovariance(const CVector3D& Vector, Real CXX, Real CXY, Real CXZ, Real CYY, Real CYZ, Real CZZ) const
            {
                const Real DX = Vector.m_X - m_X;
                const Real DY = Vector.m_Y - m_Y;
                const Real DZ = Vector.m_Z - m_Z;
                CXX += DX * DX;
                CXY += DX * DY;
                CXZ += DX * DZ;
                CYY += DY * DY;
                CYZ += DY * DZ;
                CZZ += DZ * DZ;
            }

            void CVector3D::AddWeightedCovariance(const CVector3D& Vector, const Real Weight, Real CXX, Real CXY, Real CXZ, Real CYY, Real CYZ, Real CZZ) const
            {
                const Real DX = Vector.m_X - m_X;
                const Real DY = Vector.m_Y - m_Y;
                const Real DZ = Vector.m_Z - m_Z;
                CXX += DX * DX * Weight;
                CXY += DX * DY * Weight;
                CXZ += DX * DZ * Weight;
                CYY += DY * DY * Weight;
                CYZ += DY * DZ * Weight;
                CZZ += DZ * DZ * Weight;
            }

            bool CVector3D::IsNull() const
            {
                return IsNonPositive(std::abs(m_X) + std::abs(m_Y) + std::abs(m_Z));
            }

            bool CVector3D::IsNonNull() const
            {
                return IsPositive(std::abs(m_X) + std::abs(m_Y) + std::abs(m_Z));
            }

            bool CVector3D::IsAtInfinity() const
            {
                return RVL::IsAtInfinity(m_X) || RVL::IsAtInfinity(m_Y) || RVL::IsAtInfinity(m_Z);
            }

            bool CVector3D::IsNotAtInfinity() const
            {
                return RVL::IsNotAtInfinity(m_X) && RVL::IsNotAtInfinity(m_Y) && RVL::IsNotAtInfinity(m_Z);
            }

            bool CVector3D::IsUnitary() const
            {
                return IsOne(std::sqrt(m_X * m_X + m_Y * m_Y + m_Z * m_Z));
            }

            bool CVector3D::IsNonUnitary() const
            {
                return IsNonZero(std::sqrt(m_X * m_X + m_Y * m_Y + m_Z * m_Z) - Real(1));
            }

            Real CVector3D::GetX() const
            {
                return m_X;
            }

            Real CVector3D::GetY() const
            {
                return m_Y;
            }

            Real CVector3D::GetZ() const
            {
                return m_Z;
            }

            Real CVector3D::GetAngle(const CVector3D& Vector) const
            {
                return std::acos((m_X * Vector.m_X + m_Y * Vector.m_Y + m_Z * Vector.m_Z) / (std::sqrt(m_X * m_X + m_Y * m_Y + m_Z * m_Z) * std::sqrt(Vector.m_X * Vector.m_X + Vector.m_Y * Vector.m_Y + Vector.m_Z * Vector.m_Z)));
            }

            Real CVector3D::GetAperture(const CVector3D& Vector) const
            {
                return (m_X * Vector.m_X + m_Y * Vector.m_Y + m_Z * Vector.m_Z) / (std::sqrt(m_X * m_X + m_Y * m_Y + m_Z * m_Z) * std::sqrt(Vector.m_X * Vector.m_X + Vector.m_Y * Vector.m_Y + Vector.m_Z * Vector.m_Z));
            }

            Real CVector3D::GetAbsoluteAperture(const CVector3D& Vector) const
            {
                return std::abs((m_X * Vector.m_X + m_Y * Vector.m_Y + m_Z * Vector.m_Z) / (std::sqrt(m_X * m_X + m_Y * m_Y + m_Z * m_Z) * std::sqrt(Vector.m_X * Vector.m_X + Vector.m_Y * Vector.m_Y + Vector.m_Z * Vector.m_Z)));
            }

            Real CVector3D::GetDistance(const CVector3D& Vector) const
            {
                const Real DX = m_X - Vector.m_X;
                const Real DY = m_Y - Vector.m_Y;
                const Real DZ = m_Z - Vector.m_Z;
                return std::sqrt(DX * DX + DY * DY + DZ * DZ);
            }

            Real CVector3D::GetLength() const
            {
                return std::sqrt(m_X * m_X + m_Y * m_Y + m_Z * m_Z);
            }

            Real CVector3D::GetSquareLength() const
            {
                return m_X * m_X + m_Y * m_Y + m_Z * m_Z;
            }

            Real CVector3D::Normalize()
            {
                const Real Length = std::sqrt(m_X * m_X + m_Y * m_Y + m_Z * m_Z);
                m_X /= Length;
                m_Y /= Length;
                m_Z /= Length;
                return Length;
            }

            void CVector3D::ScaleAnisotropic(const Real SX, const Real SY, const Real SZ)
            {
                m_X *= SX;
                m_Y *= SY;
                m_Z *= SZ;
            }

            const std::string CVector3D::ToString() const
            {
                std::ostringstream OutputString;
                OutputString.precision(g_RealDisplayDigits);
                OutputString << "<Vector3D X=\"" << m_X << "\" Y=\"" << m_Y << "\" Z=\"" << m_Z << "\"/>";
                return OutputString.str();
            }

            CVector3D CVector3D::VectorProduct(const CVector3D& Vector) const
            {
                return CVector3D(m_Y * Vector.m_Z - m_Z * Vector.m_Y, m_Z * Vector.m_X - m_X * Vector.m_Z, m_X * Vector.m_Y - m_Y * Vector.m_X);
            }

            Real CVector3D::ScalarProduct(const CVector3D& Vector) const
            {
                return m_X * Vector.m_X + m_Y * Vector.m_Y + m_Z * Vector.m_Z;
            }

            Real CVector3D::ScalarProduct(const Real X, const Real Y, const Real Z) const
            {
                return m_X * X + m_Y * Y + m_Z * Z;
            }

            Real CVector3D::GetSquareDistance(const CVector3D& Vector) const
            {
                const Real DX = m_X - Vector.m_X;
                const Real DY = m_Y - Vector.m_Y;
                const Real DZ = m_Z - Vector.m_Z;
                return DX * DX + DY * DY + DZ * DZ;
            }

            void CVector3D::operator=(const CVector3D& Vector)
            {
                m_X = Vector.m_X;
                m_Y = Vector.m_Y;
                m_Z = Vector.m_Z;
            }

            void CVector3D::operator+=(const CVector3D& Vector)
            {
                m_X += Vector.m_X;
                m_Y += Vector.m_Y;
                m_Z += Vector.m_Z;
            }

            void CVector3D::operator-=(const CVector3D& Vector)
            {
                m_X -= Vector.m_X;
                m_Y -= Vector.m_Y;
                m_Z -= Vector.m_Z;
            }

            void CVector3D::operator*=(const Real Scalar)
            {
                m_X *= Scalar;
                m_Y *= Scalar;
                m_Z *= Scalar;
            }

            void CVector3D::operator/=(const Real Scalar)
            {
                m_X /= Scalar;
                m_Y /= Scalar;
                m_Z /= Scalar;
            }

            bool CVector3D::operator==(const CVector3D& Vector) const
            {
                return Equals(m_X, Vector.m_X) && Equals(m_Y, Vector.m_Y) && Equals(m_Z, Vector.m_Z);
            }

            bool CVector3D::operator!=(const CVector3D& Vector) const
            {
                return NonEquals(m_X, Vector.m_X) || NonEquals(m_Y, Vector.m_Y) || NonEquals(m_Z, Vector.m_Z);
            }

            CVector3D operator/(const CVector3D& Vector, const Real Scalar)
            {
                return CVector3D(Vector.GetX() / Scalar, Vector.GetY() / Scalar, Vector.GetZ() / Scalar);
            }

            CVector3D operator*(const CVector3D& Vector, const Real Scalar)
            {
                return CVector3D(Vector.GetX() * Scalar, Vector.GetY() * Scalar, Vector.GetZ() * Scalar);
            }

            CVector3D operator*(const Real Scalar, const CVector3D& Vector)
            {
                return CVector3D(Vector.GetX() * Scalar, Vector.GetY() * Scalar, Vector.GetZ() * Scalar);
            }

            CVector3D operator+(const CVector3D& A, const CVector3D& B)
            {
                return CVector3D(A.GetX() + B.GetX(), A.GetY() + B.GetY(), A.GetZ() + B.GetZ());
            }

            CVector3D operator-(const CVector3D& A, const CVector3D& B)
            {
                return CVector3D(A.GetX() - B.GetX(), A.GetY() - B.GetY(), A.GetZ() - B.GetZ());
            }
        }
    }
}
