/*
 * Matrix3D.cpp
 */

#include "Matrix3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            const CMatrix3D CMatrix3D::s_Identity(Real(1), Real(0), Real(0), Real(0), Real(1), Real(0), Real(0), Real(0), Real(1));

            CMatrix3D CMatrix3D::CreateHatMatrix(const CVector3D& T)
            {
                return CMatrix3D(Real(0), -T.GetZ(), T.GetY(), T.GetZ(), Real(0), -T.GetX(), -T.GetY(), T.GetX(), Real(0));
            }

            CMatrix3D::CMatrix3D()
            {
                memset(&m_Matrix3D, 0, sizeof(MatrixStructure3D));
            }

            CMatrix3D::CMatrix3D(const Real E00, const Real E01, const Real E02, const Real E10, const Real E11, const Real E12, const Real E20, const Real E21, const Real E22)
            {
                m_Matrix3D.m_Elements[0][0] = E00;
                m_Matrix3D.m_Elements[0][1] = E01;
                m_Matrix3D.m_Elements[0][2] = E02;
                m_Matrix3D.m_Elements[1][0] = E10;
                m_Matrix3D.m_Elements[1][1] = E11;
                m_Matrix3D.m_Elements[1][2] = E12;
                m_Matrix3D.m_Elements[2][0] = E20;
                m_Matrix3D.m_Elements[2][1] = E21;
                m_Matrix3D.m_Elements[2][2] = E22;
            }

            CMatrix3D::CMatrix3D(const CMatrix3D& Matrix)
            {
                memcpy(&m_Matrix3D, &Matrix.m_Matrix3D, sizeof(MatrixStructure3D));
            }

            CMatrix3D::CMatrix3D(const CMatrix3D* pMatrix)
            {
                if (pMatrix)
                {
                    memcpy(&m_Matrix3D, &(pMatrix->m_Matrix3D), sizeof(MatrixStructure3D));
                }
                else
                {
                    memset(&m_Matrix3D, 0, sizeof(MatrixStructure3D));
                }
            }

            CMatrix3D::CMatrix3D(const CVector3D& C0, const CVector3D& C1, const CVector3D& C2)
            {
                m_Matrix3D.m_Elements[0][0] = C0.GetX();
                m_Matrix3D.m_Elements[0][1] = C1.GetX();
                m_Matrix3D.m_Elements[0][2] = C2.GetX();
                m_Matrix3D.m_Elements[1][0] = C0.GetY();
                m_Matrix3D.m_Elements[1][1] = C1.GetY();
                m_Matrix3D.m_Elements[1][2] = C2.GetY();
                m_Matrix3D.m_Elements[2][0] = C0.GetZ();
                m_Matrix3D.m_Elements[2][1] = C1.GetZ();
                m_Matrix3D.m_Elements[2][2] = C2.GetZ();
            }

            CMatrix3D::CMatrix3D(const Real Alpha, const CVector3D& Axis)
            {
                const CVector3D NormalizedAxis = CVector3D::CreateNormalized(Axis);
                const Real HalfAngleSinus = std::sin(Alpha * Real(0.5));
                const Real Qw = std::cos(Alpha * Real(0.5));
                const Real Qx = NormalizedAxis.GetX() * HalfAngleSinus;
                const Real Qy = NormalizedAxis.GetY() * HalfAngleSinus;
                const Real Qz = NormalizedAxis.GetZ() * HalfAngleSinus;
                const Real Magnitude = std::sqrt(Qw * Qw + Qx * Qx + Qy * Qy + Qz * Qz);
                if (IsPositive(Magnitude))
                {
                    const Real NW = Qw / Magnitude;
                    const Real NX = Qx / Magnitude;
                    const Real NY = Qy / Magnitude;
                    const Real NZ = Qz / Magnitude;
                    m_Matrix3D.m_Elements[0][0] = Real(1.0) - Real(2.0) * (NY * NY + NZ * NZ);
                    m_Matrix3D.m_Elements[0][1] = Real(2.0) * NX * NY - Real(2.0) * NW * NZ;
                    m_Matrix3D.m_Elements[0][2] = Real(2.0) * NW * NY + Real(2.0) * NX * NZ;
                    m_Matrix3D.m_Elements[1][0] = Real(2.0) * NW * NZ + Real(2.0) * NX * NY;
                    m_Matrix3D.m_Elements[1][1] = Real(1.0) - Real(2.0) * (NX * NX + NZ * NZ);
                    m_Matrix3D.m_Elements[1][2] = Real(2.0) * NY * NZ - Real(2.0) * NW * NX;
                    m_Matrix3D.m_Elements[2][0] = Real(2.0) * NX * NZ - Real(2.0) * NW * NY;
                    m_Matrix3D.m_Elements[2][1] = Real(2.0) * NW * NX + Real(2.0) * NY * NZ;
                    m_Matrix3D.m_Elements[2][2] = Real(1.0) - Real(2.0) * (NX * NX + NY * NY);
                }
                else
                {
                    memset(&m_Matrix3D, 0, sizeof(MatrixStructure3D));
                }
            }

            CMatrix3D::CMatrix3D(const Real Qw, const Real Qx, const Real Qy, const Real Qz)
            {
                const Real Magnitude = std::sqrt(Qw * Qw + Qx * Qx + Qy * Qy + Qz * Qz);
                if (IsPositive(Magnitude))
                {
                    const Real NW = Qw / Magnitude;
                    const Real NX = Qx / Magnitude;
                    const Real NY = Qy / Magnitude;
                    const Real NZ = Qz / Magnitude;
                    m_Matrix3D.m_Elements[0][0] = Real(1.0) - Real(2.0) * (NY * NY + NZ * NZ);
                    m_Matrix3D.m_Elements[0][1] = Real(2.0) * NX * NY - Real(2.0) * NW * NZ;
                    m_Matrix3D.m_Elements[0][2] = Real(2.0) * NW * NY + Real(2.0) * NX * NZ;
                    m_Matrix3D.m_Elements[1][0] = Real(2.0) * NW * NZ + Real(2.0) * NX * NY;
                    m_Matrix3D.m_Elements[1][1] = Real(1.0) - Real(2.0) * (NX * NX + NZ * NZ);
                    m_Matrix3D.m_Elements[1][2] = Real(2.0) * NY * NZ - Real(2.0) * NW * NX;
                    m_Matrix3D.m_Elements[2][0] = Real(2.0) * NX * NZ - Real(2.0) * NW * NY;
                    m_Matrix3D.m_Elements[2][1] = Real(2.0) * NW * NX + Real(2.0) * NY * NZ;
                    m_Matrix3D.m_Elements[2][2] = Real(1.0) - Real(2.0) * (NX * NX + NY * NY);
                }
                else
                {
                    memset(&m_Matrix3D, 0, sizeof(MatrixStructure3D));
                }
            }

            CMatrix3D::~CMatrix3D()
                = default;

            void CMatrix3D::MakeIdentity()
            {
                m_Matrix3D = s_Identity.m_Matrix3D;
            }

            Real CMatrix3D::GetDeterminat() const
            {
                return -m_Matrix3D.m_Elements[0][2] * m_Matrix3D.m_Elements[1][1] * m_Matrix3D.m_Elements[2][0] + m_Matrix3D.m_Elements[0][1] * m_Matrix3D.m_Elements[1][2] * m_Matrix3D.m_Elements[2][0] + m_Matrix3D.m_Elements[0][2] * m_Matrix3D.m_Elements[1][0] * m_Matrix3D.m_Elements[2][1] - m_Matrix3D.m_Elements[0][0] * m_Matrix3D.m_Elements[1][2] * m_Matrix3D.m_Elements[2][1] - m_Matrix3D.m_Elements[0][1] * m_Matrix3D.m_Elements[1][0] * m_Matrix3D.m_Elements[2][2] + m_Matrix3D.m_Elements[0][0] * m_Matrix3D.m_Elements[1][1] * m_Matrix3D.m_Elements[2][2];
            }

            bool CMatrix3D::Invert()
            {
                const Real Determinant = -m_Matrix3D.m_Elements[0][2] * m_Matrix3D.m_Elements[1][1] * m_Matrix3D.m_Elements[2][0] + m_Matrix3D.m_Elements[0][1] * m_Matrix3D.m_Elements[1][2] * m_Matrix3D.m_Elements[2][0] + m_Matrix3D.m_Elements[0][2] * m_Matrix3D.m_Elements[1][0] * m_Matrix3D.m_Elements[2][1] - m_Matrix3D.m_Elements[0][0] * m_Matrix3D.m_Elements[1][2] * m_Matrix3D.m_Elements[2][1] - m_Matrix3D.m_Elements[0][1] * m_Matrix3D.m_Elements[1][0] * m_Matrix3D.m_Elements[2][2] + m_Matrix3D.m_Elements[0][0] * m_Matrix3D.m_Elements[1][1] * m_Matrix3D.m_Elements[2][2];
                if (IsNonZero(Determinant))
                {
                    MatrixStructure3D InverseMatrix3D;
                    const Real Normalization = Real(1) / Determinant;
                    InverseMatrix3D.m_Elements[0][0] = (m_Matrix3D.m_Elements[1][1] * m_Matrix3D.m_Elements[2][2] - m_Matrix3D.m_Elements[1][2] * m_Matrix3D.m_Elements[2][1]) * Normalization;
                    InverseMatrix3D.m_Elements[0][1] = (m_Matrix3D.m_Elements[0][2] * m_Matrix3D.m_Elements[2][1] - m_Matrix3D.m_Elements[0][1] * m_Matrix3D.m_Elements[2][2]) * Normalization;
                    InverseMatrix3D.m_Elements[0][2] = (m_Matrix3D.m_Elements[0][1] * m_Matrix3D.m_Elements[1][2] - m_Matrix3D.m_Elements[0][2] * m_Matrix3D.m_Elements[1][1]) * Normalization;
                    InverseMatrix3D.m_Elements[1][0] = (m_Matrix3D.m_Elements[1][2] * m_Matrix3D.m_Elements[2][0] - m_Matrix3D.m_Elements[1][0] * m_Matrix3D.m_Elements[2][2]) * Normalization;
                    InverseMatrix3D.m_Elements[1][1] = (m_Matrix3D.m_Elements[0][0] * m_Matrix3D.m_Elements[2][2] - m_Matrix3D.m_Elements[0][2] * m_Matrix3D.m_Elements[2][0]) * Normalization;
                    InverseMatrix3D.m_Elements[1][2] = (m_Matrix3D.m_Elements[0][2] * m_Matrix3D.m_Elements[1][0] - m_Matrix3D.m_Elements[0][0] * m_Matrix3D.m_Elements[1][2]) * Normalization;
                    InverseMatrix3D.m_Elements[2][0] = (m_Matrix3D.m_Elements[1][0] * m_Matrix3D.m_Elements[2][1] - m_Matrix3D.m_Elements[1][1] * m_Matrix3D.m_Elements[2][0]) * Normalization;
                    InverseMatrix3D.m_Elements[2][1] = (m_Matrix3D.m_Elements[0][1] * m_Matrix3D.m_Elements[2][0] - m_Matrix3D.m_Elements[0][0] * m_Matrix3D.m_Elements[2][1]) * Normalization;
                    InverseMatrix3D.m_Elements[2][2] = (m_Matrix3D.m_Elements[0][0] * m_Matrix3D.m_Elements[1][1] - m_Matrix3D.m_Elements[0][1] * m_Matrix3D.m_Elements[1][0]) * Normalization;
                    m_Matrix3D = InverseMatrix3D;
                    return true;
                }
                return false;
            }

            CMatrix3D CMatrix3D::GetInverse(bool* pSingularity) const
            {
                CMatrix3D InverseMatrix;
                const Real Determinant = -m_Matrix3D.m_Elements[0][2] * m_Matrix3D.m_Elements[1][1] * m_Matrix3D.m_Elements[2][0] + m_Matrix3D.m_Elements[0][1] * m_Matrix3D.m_Elements[1][2] * m_Matrix3D.m_Elements[2][0] + m_Matrix3D.m_Elements[0][2] * m_Matrix3D.m_Elements[1][0] * m_Matrix3D.m_Elements[2][1] - m_Matrix3D.m_Elements[0][0] * m_Matrix3D.m_Elements[1][2] * m_Matrix3D.m_Elements[2][1] - m_Matrix3D.m_Elements[0][1] * m_Matrix3D.m_Elements[1][0] * m_Matrix3D.m_Elements[2][2] + m_Matrix3D.m_Elements[0][0] * m_Matrix3D.m_Elements[1][1] * m_Matrix3D.m_Elements[2][2];
                if (IsNonZero(Determinant))
                {
                    if (pSingularity)
                    {
                        *pSingularity = false;
                    }
                    const Real Normalization = Real(1) / Determinant;
                    InverseMatrix.m_Matrix3D.m_Elements[0][0] = (m_Matrix3D.m_Elements[1][1] * m_Matrix3D.m_Elements[2][2] - m_Matrix3D.m_Elements[1][2] * m_Matrix3D.m_Elements[2][1]) * Normalization;
                    InverseMatrix.m_Matrix3D.m_Elements[0][1] = (m_Matrix3D.m_Elements[0][2] * m_Matrix3D.m_Elements[2][1] - m_Matrix3D.m_Elements[0][1] * m_Matrix3D.m_Elements[2][2]) * Normalization;
                    InverseMatrix.m_Matrix3D.m_Elements[0][2] = (m_Matrix3D.m_Elements[0][1] * m_Matrix3D.m_Elements[1][2] - m_Matrix3D.m_Elements[0][2] * m_Matrix3D.m_Elements[1][1]) * Normalization;
                    InverseMatrix.m_Matrix3D.m_Elements[1][0] = (m_Matrix3D.m_Elements[1][2] * m_Matrix3D.m_Elements[2][0] - m_Matrix3D.m_Elements[1][0] * m_Matrix3D.m_Elements[2][2]) * Normalization;
                    InverseMatrix.m_Matrix3D.m_Elements[1][1] = (m_Matrix3D.m_Elements[0][0] * m_Matrix3D.m_Elements[2][2] - m_Matrix3D.m_Elements[0][2] * m_Matrix3D.m_Elements[2][0]) * Normalization;
                    InverseMatrix.m_Matrix3D.m_Elements[1][2] = (m_Matrix3D.m_Elements[0][2] * m_Matrix3D.m_Elements[1][0] - m_Matrix3D.m_Elements[0][0] * m_Matrix3D.m_Elements[1][2]) * Normalization;
                    InverseMatrix.m_Matrix3D.m_Elements[2][0] = (m_Matrix3D.m_Elements[1][0] * m_Matrix3D.m_Elements[2][1] - m_Matrix3D.m_Elements[1][1] * m_Matrix3D.m_Elements[2][0]) * Normalization;
                    InverseMatrix.m_Matrix3D.m_Elements[2][1] = (m_Matrix3D.m_Elements[0][1] * m_Matrix3D.m_Elements[2][0] - m_Matrix3D.m_Elements[0][0] * m_Matrix3D.m_Elements[2][1]) * Normalization;
                    InverseMatrix.m_Matrix3D.m_Elements[2][2] = (m_Matrix3D.m_Elements[0][0] * m_Matrix3D.m_Elements[1][1] - m_Matrix3D.m_Elements[0][1] * m_Matrix3D.m_Elements[1][0]) * Normalization;
                }
                else if (pSingularity)
                {
                    *pSingularity = true;
                }
                return InverseMatrix;
            }

            void CMatrix3D::Transpose()
            {
                MatrixStructure3D TransposedMatrix3D;
                for (int r = 0 ; r < 3 ; ++r)
                    for (int c = 0 ; c < 3 ; ++c)
                    {
                        TransposedMatrix3D.m_Elements[c][r] = m_Matrix3D.m_Elements[r][c];
                    }
                m_Matrix3D = TransposedMatrix3D;
            }

            CMatrix3D CMatrix3D::GetTranspose() const
            {
                CMatrix3D TransposedMatrix;
                for (int r = 0 ; r < 3 ; ++r)
                    for (int c = 0 ; c < 3 ; ++c)
                    {
                        TransposedMatrix.m_Matrix3D.m_Elements[c][r] = m_Matrix3D.m_Elements[r][c];
                    }
                return TransposedMatrix;
            }

            void CMatrix3D::Homography(const _2D::CVector2D& P, _2D::CVector2D& Q) const
            {
                const Real X = m_Matrix3D.m_Elements[0][0] * P.GetX() + m_Matrix3D.m_Elements[0][1] * P.GetY() + m_Matrix3D.m_Elements[0][2];
                const Real Y = m_Matrix3D.m_Elements[1][0] * P.GetX() + m_Matrix3D.m_Elements[1][1] * P.GetY() + m_Matrix3D.m_Elements[1][2];
                const Real Z = m_Matrix3D.m_Elements[2][0] * P.GetX() + m_Matrix3D.m_Elements[2][1] * P.GetY() + m_Matrix3D.m_Elements[2][2];
                Q.Set(X / Z, Y / Z);
            }

            CVector3D CMatrix3D::HomogeneousProduct(const _2D::CVector2D& P) const
            {
                return CVector3D(m_Matrix3D.m_Elements[0][0] * P.GetX() + m_Matrix3D.m_Elements[0][1] * P.GetY() + m_Matrix3D.m_Elements[0][2], m_Matrix3D.m_Elements[1][0] * P.GetX() + m_Matrix3D.m_Elements[1][1] * P.GetY() + m_Matrix3D.m_Elements[1][2], m_Matrix3D.m_Elements[2][0] * P.GetX() + m_Matrix3D.m_Elements[2][1] * P.GetY() + m_Matrix3D.m_Elements[2][2]);
            }

            void CMatrix3D::SetByElements(const Real E00, const Real E01, const Real E02, const Real E10, const Real E11, const Real E12, const Real E20, const Real E21, const Real E22)
            {
                m_Matrix3D.m_Elements[0][0] = E00;
                m_Matrix3D.m_Elements[0][1] = E01;
                m_Matrix3D.m_Elements[0][2] = E02;
                m_Matrix3D.m_Elements[1][0] = E10;
                m_Matrix3D.m_Elements[1][1] = E11;
                m_Matrix3D.m_Elements[1][2] = E12;
                m_Matrix3D.m_Elements[2][0] = E20;
                m_Matrix3D.m_Elements[2][1] = E21;
                m_Matrix3D.m_Elements[2][2] = E22;
            }

            bool CMatrix3D::SetByRow(const Real* pMatrix)
            {
                if (pMatrix)
                {
                    for (auto& m_Element : m_Matrix3D.m_Elements)
                        for (int c = 0 ; c < 3 ; ++c)
                        {
                            m_Element[c] = *pMatrix++;
                        }
                    return true;
                }
                return false;
            }

            bool CMatrix3D::SetByColumn(const Real* pMatrix)
            {
                if (pMatrix)
                {
                    for (int c = 0 ; c < 3 ; ++c)
                        for (auto& m_Element : m_Matrix3D.m_Elements)
                        {
                            m_Element[c] = *pMatrix++;
                        }
                    return true;
                }
                return false;
            }

            void CMatrix3D::SetByAngleAxis(const Real Alpha, const CVector3D& Axis)
            {
                const CVector3D NormalizedAxis = CVector3D::CreateNormalized(Axis);
                const Real HalfAngleSinus = std::sin(Alpha * Real(0.5));
                SetByQuaternion(std::cos(Alpha * Real(0.5)), NormalizedAxis.GetX() * HalfAngleSinus, NormalizedAxis.GetY() * HalfAngleSinus, NormalizedAxis.GetZ() * HalfAngleSinus);
            }

            bool CMatrix3D::SetByQuaternion(const Real Qw, const Real Qx, const Real Qy, const Real Qz)
            {
                const Real Magnitude = std::sqrt(Qw * Qw + Qx * Qx + Qy * Qy + Qz * Qz);
                if (IsNonZero(Magnitude))
                {
                    const Real NW = Qw / Magnitude;
                    const Real NX = Qx / Magnitude;
                    const Real NY = Qy / Magnitude;
                    const Real NZ = Qz / Magnitude;
                    m_Matrix3D.m_Elements[0][0] = Real(1.0) - Real(2.0) * (NY * NY + NZ * NZ);
                    m_Matrix3D.m_Elements[0][1] = Real(2.0) * NX * NY - Real(2.0) * NW * NZ;
                    m_Matrix3D.m_Elements[0][2] = Real(2.0) * NW * NY + Real(2.0) * NX * NZ;
                    m_Matrix3D.m_Elements[1][0] = Real(2.0) * NW * NZ + Real(2.0) * NX * NY;
                    m_Matrix3D.m_Elements[1][1] = Real(1.0) - Real(2.0) * (NX * NX + NZ * NZ);
                    m_Matrix3D.m_Elements[1][2] = Real(2.0) * NY * NZ - Real(2.0) * NW * NX;
                    m_Matrix3D.m_Elements[2][0] = Real(2.0) * NX * NZ - Real(2.0) * NW * NY;
                    m_Matrix3D.m_Elements[2][1] = Real(2.0) * NW * NX + Real(2.0) * NY * NZ;
                    m_Matrix3D.m_Elements[2][2] = Real(1.0) - Real(2.0) * (NX * NX + NY * NY);
                    return true;
                }
                return false;
            }

            bool CMatrix3D::GetAsQuaternion(Real& Qw, Real& Qx, Real& Qy, Real& Qz)
            {
                if (IsOne(GetDeterminat()) && IsOrthonormal())
                {
                    Qw = std::sqrt(Real(1.0) + m_Matrix3D.m_Elements[0][0] + m_Matrix3D.m_Elements[1][1] + m_Matrix3D.m_Elements[2][2]) / Real(2.0);
                    const Real Qw4 = Real(4.0) * Qw;
                    Qx = (m_Matrix3D.m_Elements[2][1] - m_Matrix3D.m_Elements[1][2]) / Qw4;
                    Qy = (m_Matrix3D.m_Elements[0][2] - m_Matrix3D.m_Elements[2][0]) / Qw4;
                    Qz = (m_Matrix3D.m_Elements[1][0] - m_Matrix3D.m_Elements[0][1]) / Qw4;
                    return true;
                }
                return false;
            }

            Real* CMatrix3D::operator[](const int Row)
            {
                return m_Matrix3D.m_Elements[Row];
            }

            const Real* CMatrix3D::operator[](const int Row) const
            {
                return m_Matrix3D.m_Elements[Row];
            }

            void CMatrix3D::operator=(const CMatrix3D& Matrix)
            {
                memcpy(&m_Matrix3D, &Matrix.m_Matrix3D, sizeof(MatrixStructure3D));
            }

            void CMatrix3D::operator*=(const CMatrix3D& Matrix)
            {
                MatrixStructure3D TemporalMatrixStructure;
                for (int r = 0 ; r < 3 ; ++r)
                    for (int c = 0 ; c < 3 ; ++c)
                    {
                        TemporalMatrixStructure.m_Elements[r][c] = m_Matrix3D.m_Elements[r][0] * Matrix.m_Matrix3D.m_Elements[0][c] + m_Matrix3D.m_Elements[r][1] * Matrix.m_Matrix3D.m_Elements[1][c] + m_Matrix3D.m_Elements[r][2] * Matrix.m_Matrix3D.m_Elements[2][c];
                    }
                memcpy(&m_Matrix3D, &TemporalMatrixStructure, sizeof(MatrixStructure3D));
            }

            CMatrix3D CMatrix3D::operator*(const CMatrix3D& Matrix) const
            {
                CMatrix3D ResultMatrix;
                for (int r = 0 ; r < 3 ; ++r)
                    for (int c = 0 ; c < 3 ; ++c)
                    {
                        ResultMatrix.m_Matrix3D.m_Elements[r][c] = m_Matrix3D.m_Elements[r][0] * Matrix.m_Matrix3D.m_Elements[0][c] + m_Matrix3D.m_Elements[r][1] * Matrix.m_Matrix3D.m_Elements[1][c] + m_Matrix3D.m_Elements[r][2] * Matrix.m_Matrix3D.m_Elements[2][c];
                    }
                return ResultMatrix;
            }

            CVector3D CMatrix3D::operator*(const CVector3D& Vector) const
            {
                const Real X = Vector.GetX();
                const Real Y = Vector.GetY();
                const Real Z = Vector.GetZ();
                return CVector3D(m_Matrix3D.m_Elements[0][0] * X + m_Matrix3D.m_Elements[0][1] * Y + m_Matrix3D.m_Elements[0][2] * Z, m_Matrix3D.m_Elements[1][0] * X + m_Matrix3D.m_Elements[1][1] * Y + m_Matrix3D.m_Elements[1][2] * Z, m_Matrix3D.m_Elements[2][0] * X + m_Matrix3D.m_Elements[2][1] * Y + m_Matrix3D.m_Elements[2][2] * Z);
            }

            CMatrix3D CMatrix3D::operator*(const Real Scalar) const
            {
                CMatrix3D ResultMatrix;
                for (int i = 0 ; i < 9 ; ++i)
                {
                    ResultMatrix.m_Matrix3D.m_Array[i] = m_Matrix3D.m_Array[i] * Scalar;
                }
                return ResultMatrix;
            }

            bool CMatrix3D::operator==(const CMatrix3D& Matrix) const
            {
                for (int i = 0 ; i < 9 ; ++i)
                    if (IsNonZero(m_Matrix3D.m_Array[i] - Matrix.m_Matrix3D.m_Array[i]))
                    {
                        return false;
                    }
                return true;
            }

            bool CMatrix3D::operator!=(const CMatrix3D& Matrix) const
            {
                for (int i = 0 ; i < 9 ; ++i)
                    if (IsNonZero(m_Matrix3D.m_Array[i] - Matrix.m_Matrix3D.m_Array[i]))
                    {
                        return true;
                    }
                return false;
            }

            const std::string CMatrix3D::ToString() const
            {
                std::ostringstream OutputString;
                OutputString.precision(g_RealDisplayDigits);
                for (const auto& m_Element : m_Matrix3D.m_Elements)
                {
                    OutputString << MapToTextDisplay(m_Element[0]) << "\t" << MapToTextDisplay(m_Element[1]) << "\t" << MapToTextDisplay(m_Element[2]) << std::endl;
                }
                return OutputString.str();
            }

            bool CMatrix3D::IsIsometry() const
            {
                return IsOne(GetDeterminat());
            }

            bool CMatrix3D::IsOrthonormal() const
            {
                for (int r = 0 ; r < 3 ; ++r)
                    for (int c = 0 ; c < 3 ; ++c)
                    {
                        const Real Elements = m_Matrix3D.m_Elements[r][0] * m_Matrix3D.m_Elements[c][0] + m_Matrix3D.m_Elements[r][1] * m_Matrix3D.m_Elements[c][1] + m_Matrix3D.m_Elements[r][2] * m_Matrix3D.m_Elements[c][2];
                        if (r == c)
                        {
                            if (!IsOne(Elements))
                            {
                                return false;
                            }
                        }
                        else if (IsNonZero(Elements))
                        {
                            return false;
                        }
                    }
                return true;
            }

            bool CMatrix3D::IsUnitary() const
            {
                for (int r = 0 ; r < 3 ; ++r)
                    for (int c = 0 ; c < 3 ; ++c)
                        if (r == c)
                        {
                            if (!IsOne(m_Matrix3D.m_Elements[r][c]))
                            {
                                return false;
                            }
                        }
                        else if (IsNonZero(m_Matrix3D.m_Elements[r][c]))
                        {
                            return false;
                        }
                return true;
            }
        }
    }
}
