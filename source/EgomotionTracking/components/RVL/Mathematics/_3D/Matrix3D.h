/*
 * Matrix3D.h
 */

#pragma once

#include "../../Common/DataTypes.h"
#include "../_2D/Vector2D.h"
#include "Vector3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            class CMatrix3D
            {
            public:

                static const CMatrix3D s_Identity;

                static CMatrix3D CreateHatMatrix(const CVector3D& T);

                CMatrix3D();
                CMatrix3D(const Real E00, const Real E01, const Real E02, const Real E10, const Real E11, const Real E12, const Real E20, const Real E21, const Real E22);
                CMatrix3D(const CMatrix3D& Matrix);
                CMatrix3D(const CMatrix3D* pMatrix);
                CMatrix3D(const CVector3D& C0, const CVector3D& C1, const CVector3D& C2);
                CMatrix3D(const Real Alpha, const CVector3D& Axis);
                CMatrix3D(const Real Qw, const Real Qx, const Real Qy, const Real Qz);
                ~CMatrix3D();

                void MakeIdentity();
                Real GetDeterminat() const;
                bool Invert();
                CMatrix3D GetInverse(bool* pSingularity = NULL) const;
                void Transpose();
                CMatrix3D GetTranspose() const;
                void Homography(const _2D::CVector2D& P, _2D::CVector2D& Q) const;
                CVector3D HomogeneousProduct(const _2D::CVector2D& P) const;
                void SetByElements(const Real E00, const Real E01, const Real E02, const Real E10, const Real E11, const Real E12, const Real E20, const Real E21, const Real E22);
                bool SetByRow(const Real* pMatrix);
                bool SetByColumn(const Real* pMatrix);
                void SetByAngleAxis(const Real Alpha, const CVector3D& Axis);
                bool SetByQuaternion(const Real Qw, const Real Qx, const Real Qy, const Real Qz);
                bool GetAsQuaternion(Real& Qw, Real& Qx, Real& Qy, Real& Qz);
                Real* operator[](const int Row);
                const Real* operator[](const int Row) const;
                void operator=(const CMatrix3D& Matrix);
                void operator*=(const CMatrix3D& Matrix);
                CMatrix3D operator*(const CMatrix3D& Matrix) const;
                CVector3D operator*(const CVector3D& Vector) const;
                CMatrix3D operator*(const Real Scalar) const;
                bool operator==(const CMatrix3D& Matrix) const;
                bool operator!=(const CMatrix3D& Matrix) const;
                const std::string ToString() const;
                bool IsIsometry() const;
                bool IsOrthonormal() const;
                bool IsUnitary() const;

            protected:

                union MatrixStructure3D
                {
                    Real m_Array[9];
                    Real m_Elements[3][3];
                };
                MatrixStructure3D m_Matrix3D;
            };
        }
    }
}

