/*
 * Line3DSourcePoints.h
 */

#pragma once

#include "Geometry3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                class CLine3DSourcePoints
                {
                public:

                    CLine3DSourcePoints(const CVector3D& A, const CVector3D& B);
                    virtual ~CLine3DSourcePoints();

                    bool SetPoints(const CVector3D& A, const CVector3D& B);

                    void GetPoints(CVector3D& A, CVector3D& B) const;
                    bool ArePointsValid() const;
                    CVector3D GetMidPoint() const;
                    CVector3D GetDirection() const;
                    const CVector3D& GetPointA() const;
                    const CVector3D& GetPointB() const;

                protected:

                    CVector3D m_A;
                    CVector3D m_B;
                };
            }
        }
    }
}

