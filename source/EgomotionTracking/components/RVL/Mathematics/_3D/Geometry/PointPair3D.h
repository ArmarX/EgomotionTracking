/*
 * PointPair3D.h
 */

#pragma once

#include "GeometricPrimitive3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                class CPoint3D;

                class CPointPair3D : public CGeometricPrimitive3D
                {
                public:

                    CPointPair3D();
                    CPointPair3D(const Real Xa, const Real Ya, const Real Za, const Real Xb, const Real Yb, const Real Zb, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB);
                    CPointPair3D(const CVector3D& PointA, const CVector3D& PointB, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB);
                    ~CPointPair3D() override;

                    void SetPointA(const CVector3D& PointA);
                    void SetPointB(const CVector3D& PointB);

                    const CVector3D& GetPointA() const;
                    const CVector3D& GetPointB() const;
                    const CVector3D GetMidPoint() const;
                    bool IsSinglePoint() const;
                    Real GetABDistance() const;
                    const std::string ToString() const override;
                    bool operator==(const CGeometricPrimitive3D& GeometricPrimitive) const override;
                    IntersectionType AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const override;
                    CGeometricPrimitive3D* GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const override;

                protected:

                    IntersectionType AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;

                private:

                    CVector3D m_PointA;
                    CVector3D m_PointB;
                };
            }
        }
    }
}

