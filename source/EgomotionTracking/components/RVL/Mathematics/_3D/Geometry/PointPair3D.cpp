/*
 * PointPair3D.cpp
 */

#include "Point3D.h"
#include "PointPair3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CPointPair3D::CPointPair3D() :
                    CGeometricPrimitive3D(ePointPair, nullptr, nullptr),
                    m_PointA(CVector3D::s_PlusInfinity),
                    m_PointB(CVector3D::s_PlusInfinity)
                {
                }

                CPointPair3D::CPointPair3D(const Real Xa, const Real Ya, const Real Za, const Real Xb, const Real Yb, const Real Zb, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(ePointPair, pParentA, pParentB),
                    m_PointA(Xa, Ya, Za),
                    m_PointB(Xb, Yb, Zb)
                {
                    if (m_PointA.IsNotAtInfinity() && m_PointB.IsNotAtInfinity())
                    {
                        SetStatus(eDefined);
                    }
                }

                CPointPair3D::CPointPair3D(const CVector3D& PointA, const CVector3D& PointB, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(ePointPair, pParentA, pParentB),
                    m_PointA(PointA),
                    m_PointB(PointB)
                {
                    if (m_PointA.IsNotAtInfinity() && m_PointB.IsNotAtInfinity())
                    {
                        SetStatus(eDefined);
                    }
                }

                CPointPair3D::~CPointPair3D()
                    = default;

                void CPointPair3D::SetPointA(const CVector3D& PointA)
                {
                    if (PointA != m_PointA)
                    {
                        m_PointA = PointA;
                        SetStatus((m_PointA.IsAtInfinity() || m_PointB.IsAtInfinity()) ? eUndefined : eDefined);
                        OnChange();
                    }
                }

                void CPointPair3D::SetPointB(const CVector3D& PointB)
                {
                    if (PointB != m_PointB)
                    {
                        m_PointB = PointB;
                        SetStatus((m_PointB.IsAtInfinity() || m_PointA.IsAtInfinity()) ? eUndefined : eDefined);
                        OnChange();
                    }
                }

                const CVector3D& CPointPair3D::GetPointA() const
                {
                    return m_PointA;
                }

                const CVector3D& CPointPair3D::GetPointB() const
                {
                    return m_PointB;
                }

                const CVector3D CPointPair3D::GetMidPoint() const
                {
                    return CVector3D::CreateMidPoint(m_PointA, m_PointB);
                }

                bool CPointPair3D::IsSinglePoint() const
                {
                    return (m_PointA == m_PointB);
                }

                Real CPointPair3D::GetABDistance() const
                {
                    return m_PointA.GetDistance(m_PointB);
                }

                const std::string CPointPair3D::ToString() const
                {
                    std::ostringstream Content;
                    Content.precision(g_RealDisplayDigits);
                    Content << "<PointPair3D Id=\"" << GetInstanceId() << "\" Status=\"" << GetStatusStringMode() << "\" Xa=\"" << m_PointA.GetX() << "\" Ya=\"" << m_PointA.GetY() << "\" Za=\"" << m_PointA.GetZ() << "\" Xb=\"" << m_PointB.GetX() << "\" Yb=\"" << m_PointB.GetY() << "\" Zb=\"" << m_PointB.GetZ() << "\"";
                    AppendParentalInformation(Content);
                    Content << "/>";
                    return Content.str();
                }

                bool CPointPair3D::operator==(const CGeometricPrimitive3D& GeometricPrimitive) const
                {
                    if (IsUndefined() || GeometricPrimitive.IsUndefined())
                    {
                        return false;
                    }
                    else if (&GeometricPrimitive == this)
                    {
                        return true;
                    }
                    else if (GeometricPrimitive.GetTypeId() == GetTypeId())
                    {
                        const CPointPair3D* pPointPair = dynamic_cast<const CPointPair3D*>(&GeometricPrimitive);
                        return ((m_PointA == pPointPair->m_PointA) && (m_PointB == pPointPair->m_PointB)) || ((m_PointA == pPointPair->m_PointB) && (m_PointB == pPointPair->m_PointA));
                    }
                    return false;
                }

                IntersectionType CPointPair3D::AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (pGeometricPrimitive && pGeometricPrimitive->IsDefined() && IsDefined())
                    {
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case eCircle:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case ePointPair:
                                if (pGeometricPrimitive == this)
                                {
                                    _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePointPair)
                                    return eSelfInstanceIntersection;
                                }
                                else
                                {
                                    return AnalyzeIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                }
                            case ePoint:
                                return AnalyzeIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case ePlane:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case eLine:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case eNone:
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                            default:
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                CGeometricPrimitive3D* CPointPair3D::GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pGeometricPrimitive && pGeometricPrimitive->IsDefined() && IsDefined())
                    {
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case eCircle:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case ePointPair:
                                if (pGeometricPrimitive == this)
                                {
                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSelfInstanceIntersection)
                                    return nullptr;
                                }
                                else
                                {
                                    return GenerateIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                }
                            case ePoint:
                                return GenerateIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case ePlane:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case eLine:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case eNone:
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                            default:
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                        }
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                IntersectionType CPointPair3D::AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const CVector3D& Point = pPoint->GetPoint();
                    if ((m_PointA == Point) || (m_PointB == Point))
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                        return eSelfNumericalIntersection;
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                IntersectionType CPointPair3D::AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const bool IntersectionAA = (m_PointA == pPointPair->m_PointA);
                    const bool IntersectionAB = (m_PointA == pPointPair->m_PointB);
                    const bool IntersectionBA = (m_PointB == pPointPair->m_PointA);
                    const bool IntersectionBB = (m_PointB == pPointPair->m_PointB);
                    if (IntersectionAA || IntersectionAB || IntersectionBA || IntersectionBB)
                    {
                        const bool DirectIncidenceIntersection = IntersectionAA && IntersectionBB;
                        const bool CrossedIncidenceIntersection = IntersectionAB && IntersectionBA;
                        if (DirectIncidenceIntersection || CrossedIncidenceIntersection)
                        {
                            if (DirectIncidenceIntersection && CrossedIncidenceIntersection)
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                                return eSelfNumericalIntersection;
                            }
                            else
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePointPair)
                                return eSelfNumericalIntersection;
                            }
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                            return eSubspace;
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                CGeometricPrimitive3D* CPointPair3D::GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if ((m_PointA == pPoint->GetPoint()) || (m_PointB == pPoint->GetPoint()))
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSelfNumericalIntersection)
                        return nullptr;
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                CGeometricPrimitive3D* CPointPair3D::GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const bool IntersectionAA = (m_PointA == pPointPair->m_PointA);
                    const bool IntersectionAB = (m_PointA == pPointPair->m_PointB);
                    const bool IntersectionBA = (m_PointB == pPointPair->m_PointA);
                    const bool IntersectionBB = (m_PointB == pPointPair->m_PointB);
                    if (IntersectionAA || IntersectionAB || IntersectionBA || IntersectionBB)
                    {
                        if ((IntersectionAA && IntersectionBB) || (IntersectionAB && IntersectionBA))
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePointPair, eSelfNumericalIntersection)
                            return nullptr;
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                            return new CPoint3D((IntersectionAA || IntersectionAB) ? m_PointA : m_PointB, this, pPointPair);
                        }
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }
            }
        }
    }
}
