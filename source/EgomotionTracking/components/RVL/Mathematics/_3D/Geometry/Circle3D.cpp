/*
 * Circle3D.cpp
 */

#include "Point3D.h"
#include "PointPair3D.h"
#include "Line3D.h"
#include "Circle3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CCircle3D::CCircle3D() :
                    CGeometricPrimitive3D(eCircle, nullptr, nullptr),
                    m_Center(CVector3D::s_PlusInfinity),
                    m_Normal(CVector3D::s_Zero),
                    m_Radius(Real(0))
                {
                }

                CCircle3D::CCircle3D(const Real CX, const Real CY, const Real CZ, const Real NX, const Real NY, const Real NZ, const Real Radius, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(eCircle, pParentA, pParentB),
                    m_Center(CX, CY, CZ),
                    m_Normal(NX, NY, NZ),
                    m_Radius(Radius)
                {
                    if (IsNonZero(m_Radius) && IsNotAtInfinity(m_Radius) && m_Normal.IsNonNull() && m_Center.IsNotAtInfinity())
                    {
                        m_Normal.Normalize();
                        SetStatus(eDefined);
                    }
                }

                CCircle3D::CCircle3D(const CVector3D& Center, const CVector3D& Normal, const Real Radius, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(eCircle, pParentA, pParentB),
                    m_Center(Center),
                    m_Normal(Normal),
                    m_Radius(Radius)
                {
                    if (IsNonZero(m_Radius) && IsNotAtInfinity(m_Radius) && m_Normal.IsNonNull() && m_Center.IsNotAtInfinity())
                    {
                        m_Normal.Normalize();
                        SetStatus(eDefined);
                    }
                }

                CCircle3D::~CCircle3D()
                    = default;

                void CCircle3D::SetCenter(const CVector3D& Center)
                {
                    if (Center != m_Center)
                    {
                        m_Center = Center;
                        SetStatus((m_Center.IsNotAtInfinity() && IsNonZero(m_Radius) && IsNotAtInfinity(m_Radius) && m_Normal.IsNonNull()) ? eDefined : eUndefined);
                        OnChange();
                    }
                }

                const CVector3D& CCircle3D::GetCenter() const
                {
                    return m_Center;
                }

                void CCircle3D::SetNormal(const CVector3D& Normal)
                {
                    if (Normal != m_Normal)
                    {
                        m_Normal = Normal;
                        if (m_Normal.IsNonNull() && IsNonZero(m_Radius) && IsNotAtInfinity(m_Radius) && m_Center.IsNotAtInfinity())
                        {
                            m_Normal.Normalize();
                            SetStatus(eDefined);
                        }
                        else
                        {
                            SetStatus(eUndefined);
                        }
                        OnChange();
                    }
                }

                const CVector3D& CCircle3D::GetNormal() const
                {
                    return m_Normal;
                }

                void CCircle3D::SetRadius(const Real Radius)
                {
                    if (NonEquals(m_Radius, Radius))
                    {
                        m_Radius = Radius;
                        SetStatus((IsNonZero(m_Radius) && IsNotAtInfinity(m_Radius) && m_Normal.IsNonNull() && m_Center.IsNotAtInfinity()) ? eDefined : eUndefined);
                        OnChange();
                    }
                }

                Real CCircle3D::GetRadius() const
                {
                    return m_Radius;
                }

                Real CCircle3D::GetHesseDistance() const
                {
                    return m_Normal.ScalarProduct(m_Center);
                }

                const std::string CCircle3D::ToString() const
                {
                    std::ostringstream Content;
                    Content.precision(g_RealDisplayDigits);
                    Content << "<Circle3D Id=\"" << GetInstanceId() << "\" Status=\"" << GetStatusStringMode() << "\" Cx=\"" << m_Center.GetX() << "\" Cy=\"" << m_Center.GetY() << "\" Cz=\"" << m_Center.GetZ() << "\" Nx=\"" << m_Normal.GetX() << "\" Ny=\"" << m_Normal.GetY() << "\" Nz=\"" << m_Normal.GetZ() << "\" Radius=\"" << m_Radius << "\"";
                    AppendParentalInformation(Content);
                    Content << "/>";
                    return Content.str();
                }

                bool CCircle3D::operator==(const CGeometricPrimitive3D& GeometricPrimitive) const
                {
                    if (IsUndefined() || GeometricPrimitive.IsUndefined())
                    {
                        return false;
                    }
                    else if (&GeometricPrimitive == this)
                    {
                        return true;
                    }
                    else if (GeometricPrimitive.GetTypeId() == GetTypeId())
                    {
                        const CCircle3D* pCircle = dynamic_cast<const CCircle3D*>(&GeometricPrimitive);
                        return Equals(m_Radius, pCircle->m_Radius) && (m_Center == pCircle->m_Center) && (m_Normal == pCircle->m_Normal);
                    }
                    else
                    {
                        return false;
                    }
                }

                IntersectionType CCircle3D::AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (pGeometricPrimitive && pGeometricPrimitive->IsDefined() && IsDefined())
                    {
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case eCircle:
                                if (pGeometricPrimitive == this)
                                {
                                    _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                                    return eSelfInstanceIntersection;
                                }
                                else
                                {
                                    return AnalyzeIntersection(dynamic_cast<const CCircle3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                }
                            case ePointPair:
                                return AnalyzeIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case ePoint:
                                return AnalyzeIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case ePlane:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case eLine:
                                return AnalyzeIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case eNone:
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                            default:
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                CGeometricPrimitive3D* CCircle3D::GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pGeometricPrimitive && pGeometricPrimitive->IsDefined() && IsDefined())
                    {
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case eCircle:
                                if (pGeometricPrimitive == this)
                                {
                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eCircle, eSelfInstanceIntersection)
                                    return nullptr;
                                }
                                else
                                {
                                    return GenerateIntersection(dynamic_cast<const CCircle3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                }
                            case ePointPair:
                                return GenerateIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case ePoint:
                                return GenerateIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case ePlane:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case eLine:
                                return GenerateIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case eNone:
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                            default:
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                        }
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                IntersectionType CCircle3D::AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (CGeometry3D::IsOnSphere(m_Center, m_Radius, pPoint->GetPoint()) && CGeometry3D::IsOnPlane(m_Normal, m_Center, pPoint->GetPoint()))
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                        return eSelfNumericalIntersection;
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                IntersectionType CCircle3D::AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const bool IntersectionA = CGeometry3D::IsOnSphere(m_Center, m_Radius, pPointPair->GetPointA()) && CGeometry3D::IsOnPlane(m_Normal, m_Center, pPointPair->GetPointA());
                    const bool IntersectionB = CGeometry3D::IsOnSphere(m_Center, m_Radius, pPointPair->GetPointB()) && CGeometry3D::IsOnPlane(m_Normal, m_Center, pPointPair->GetPointB());
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePointPair)
                            return eSelfNumericalIntersection;
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                            return eSubspace;
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                IntersectionType CCircle3D::AnalyzeIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const CVector3D& LineSupportPoint = pLine->GetSupportPoint();
                    const CVector3D& LineDirection = pLine->GetDirection();
                    if (CGeometry3D::AreUnitaryVectorsOrthogonal(m_Normal, LineDirection))
                    {
                        if (CGeometry3D::IsOnPlane(m_Normal, m_Center, LineSupportPoint))
                        {
                            const Real Distance = m_Center.GetDistance(CGeometry3D::ComputeProjectionPointToLine(LineSupportPoint, LineDirection, m_Center));
                            if (Equals(Distance, m_Radius))
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                                return eSubspace;
                            }
                            else if (Distance < m_Radius)
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePointPair)
                                return eSubspace;
                            }
                            else
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                            }
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                            return eEmpty;
                        }
                    }
                    else
                    {
                        CVector3D X;
                        if (CGeometry3D::ComputeIntersectionPlaneAndLine(LineSupportPoint, LineDirection, m_Normal, m_Center, X) && CGeometry3D::IsOnSphere(m_Center, m_Radius, X))
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                            return eSubspace;
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                            return eEmpty;
                        }
                    }
                }

                IntersectionType CCircle3D::AnalyzeIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (CGeometry3D::AreUnitaryVectorsParallel(m_Normal, pCircle->m_Normal))
                    {
                        if (CGeometry3D::IsOnPlane(m_Normal, m_Center, pCircle->m_Center))
                        {
                            const Real CentersDistance = m_Center.GetDistance(pCircle->m_Center);
                            bool SinglePointFlag = false;
                            if (CGeometry3D::IsIntersectingConfiguration(std::max(m_Radius, pCircle->m_Radius), std::min(m_Radius, pCircle->m_Radius), CentersDistance, SinglePointFlag))
                            {
                                if (SinglePointFlag)
                                {
                                    _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                                    return eSubspace;
                                }
                                else if (IsZero(CentersDistance))
                                {
                                    if (Equals(m_Radius, pCircle->m_Radius))
                                    {
                                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eCircle)
                                        return eSelfNumericalIntersection;
                                    }
                                    else
                                    {
                                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                        return eEmpty;
                                    }
                                }
                                else
                                {
                                    _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePointPair)
                                    return eSubspace;
                                }
                            }
                            else
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                            }
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                            return eEmpty;
                        }
                    }
                    else
                    {
                        CVector3D LineDirection, LineSupportPoint;
                        if (CGeometry3D::ComputeIntersectionPlaneAndPlane(m_Normal, GetHesseDistance(), pCircle->m_Normal, pCircle->GetHesseDistance(), LineDirection, LineSupportPoint))
                        {
                            CVector3D A, B;
                            bool SinglePointFlag = false;
                            if (CGeometry3D::ComputeIntersectionCircleAndLine(m_Normal, m_Center, m_Radius, LineSupportPoint, LineDirection, A, B, SinglePointFlag))
                            {
                                if (SinglePointFlag)
                                {
                                    if (CGeometry3D::IsOnSphere(pCircle->m_Center, pCircle->m_Radius, A))
                                    {
                                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                                        return eSubspace;
                                    }
                                    else
                                    {
                                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                        return eEmpty;
                                    }
                                }
                                else
                                {
                                    const bool IntersectionA = CGeometry3D::IsOnSphere(pCircle->m_Center, pCircle->m_Radius, A);
                                    const bool IntersectionB = CGeometry3D::IsOnSphere(pCircle->m_Center, pCircle->m_Radius, B);
                                    if (IntersectionA || IntersectionB)
                                    {
                                        if (IntersectionA && IntersectionB)
                                        {
                                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePointPair)
                                            return eSubspace;
                                        }
                                        else
                                        {
                                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                                            return eSubspace;
                                        }
                                    }
                                    else
                                    {
                                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                                        return eSubspace;
                                    }
                                }
                            }
                            else
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                            }
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                            return eEmpty;
                        }
                    }
                    return eEmpty;
                }

                CGeometricPrimitive3D* CCircle3D::GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (CGeometry3D::IsOnSphere(m_Center, m_Radius, pPoint->GetPoint()) && CGeometry3D::IsOnPlane(m_Normal, m_Center, pPoint->GetPoint()))
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSelfNumericalIntersection)
                        return nullptr;
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                CGeometricPrimitive3D* CCircle3D::GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const bool IntersectionA = CGeometry3D::IsOnSphere(m_Center, m_Radius, pPointPair->GetPointA()) && CGeometry3D::IsOnPlane(m_Normal, m_Center, pPointPair->GetPointA());
                    const bool IntersectionB = CGeometry3D::IsOnSphere(m_Center, m_Radius, pPointPair->GetPointB()) && CGeometry3D::IsOnPlane(m_Normal, m_Center, pPointPair->GetPointB());
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePointPair, eSelfNumericalIntersection)
                            return nullptr;
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                            return new CPoint3D(IntersectionA ? pPointPair->GetPointA() : pPointPair->GetPointB(), this, pPointPair);
                        }
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                CGeometricPrimitive3D* CCircle3D::GenerateIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const CVector3D& LineSupportPoint = pLine->GetSupportPoint();
                    const CVector3D& LineDirection = pLine->GetDirection();
                    if (CGeometry3D::AreUnitaryVectorsOrthogonal(m_Normal, LineDirection))
                    {
                        if (CGeometry3D::IsOnPlane(m_Normal, m_Center, LineSupportPoint))
                        {
                            CVector3D A, B;
                            bool SinglePointFlag = false;
                            if (CGeometry3D::ComputeIntersectionCircleAndLine(m_Normal, m_Center, m_Radius, LineSupportPoint, LineDirection, A, B, SinglePointFlag))
                            {
                                if (SinglePointFlag)
                                {
                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                                    return new CPoint3D(A, this, pLine);
                                }
                                else
                                {
                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace)
                                    return new CPointPair3D(A, B, this, pLine);
                                }
                            }
                            else
                            {
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                            }
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                            return nullptr;
                        }
                    }
                    else
                    {
                        CVector3D X;
                        if (CGeometry3D::ComputeIntersectionPlaneAndLine(LineSupportPoint, LineDirection, m_Normal, m_Center, X) && CGeometry3D::IsOnSphere(m_Center, m_Radius, X))
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                            return new CPoint3D(X, this, pLine);
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                            return nullptr;
                        }
                    }
                }

                CGeometricPrimitive3D* CCircle3D::GenerateIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (CGeometry3D::AreUnitaryVectorsParallel(m_Normal, pCircle->m_Normal))
                    {
                        if (CGeometry3D::IsOnPlane(m_Normal, m_Center, pCircle->m_Center))
                        {
                            const Real CentersDistance = m_Center.GetDistance(pCircle->m_Center);
                            bool SinglePointFlag = false;
                            if (CGeometry3D::IsIntersectingConfiguration(std::max(m_Radius, pCircle->m_Radius), std::min(m_Radius, pCircle->m_Radius), CentersDistance, SinglePointFlag))
                            {
                                if (SinglePointFlag)
                                {
                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                                    return new CPoint3D(m_Center + CGeometry3D::ComputeUnitaryDirectionVector(m_Center, pCircle->m_Center) * m_Radius, this, pCircle);
                                }
                                else if (IsZero(CentersDistance))
                                {
                                    if (Equals(m_Radius, pCircle->m_Radius))
                                    {
                                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eCircle, eSelfNumericalIntersection)
                                        return nullptr;
                                    }
                                    else
                                    {
                                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                        return nullptr;
                                    }
                                }
                                else
                                {
                                    Real Dx = Real(0), Dy = Real(0);
                                    if (CGeometry3D::CirclesIntersection(std::max(m_Radius, pCircle->m_Radius), std::min(m_Radius, pCircle->m_Radius), CentersDistance, Dx, Dy, SinglePointFlag))
                                    {
                                        const CVector3D CenterBase = (m_Radius > pCircle->m_Radius) ? m_Center : pCircle->m_Center;
                                        const CVector3D CenterDirectionPoint = (m_Radius > pCircle->m_Radius) ? pCircle->m_Center : m_Center;
                                        const CVector3D Delta = CGeometry3D::ComputeUnitaryDirectionVector(CenterBase, CenterDirectionPoint) * Dx;
                                        CVector3D Beta;
                                        if (CGeometry3D::ComputeUnitaryOrthogonalVector(Delta, m_Normal, Beta))
                                        {
                                            const CVector3D AuxiliarDisplacement = Beta * Dy;
                                            const CVector3D AuxiliarPoint = CenterBase + Delta;
                                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace)
                                            return new CPointPair3D(AuxiliarPoint + AuxiliarDisplacement, AuxiliarPoint - AuxiliarDisplacement, this, pCircle);
                                        }
                                        else
                                        {
                                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                            return nullptr;
                                        }
                                    }
                                    else
                                    {
                                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                        return nullptr;
                                    }
                                }
                            }
                            else
                            {
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                            }
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                            return nullptr;
                        }
                    }
                    else
                    {
                        CVector3D LineDirection, LineSupportPoint;
                        if (CGeometry3D::ComputeIntersectionPlaneAndPlane(m_Normal, GetHesseDistance(), pCircle->m_Normal, pCircle->GetHesseDistance(), LineDirection, LineSupportPoint))
                        {
                            CVector3D A, B;
                            bool SinglePointFlag = false;
                            if (CGeometry3D::ComputeIntersectionCircleAndLine(m_Normal, m_Center, m_Radius, LineSupportPoint, LineDirection, A, B, SinglePointFlag))
                            {
                                if (SinglePointFlag)
                                {
                                    if (CGeometry3D::IsOnSphere(pCircle->m_Center, pCircle->m_Radius, A))
                                    {
                                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                                        return new CPoint3D(A, this, pCircle);
                                    }
                                    else
                                    {
                                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                        return nullptr;
                                    }
                                }
                                else
                                {
                                    const bool IntersectionA = CGeometry3D::IsOnSphere(pCircle->m_Center, pCircle->m_Radius, A);
                                    const bool IntersectionB = CGeometry3D::IsOnSphere(pCircle->m_Center, pCircle->m_Radius, B);
                                    if (IntersectionA || IntersectionB)
                                    {
                                        if (IntersectionA && IntersectionB)
                                        {
                                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace)
                                            return new CPointPair3D(A, B, this, pCircle);
                                        }
                                        else
                                        {
                                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                                            return new CPoint3D(IntersectionA ? A : B, this, pCircle);
                                        }
                                    }
                                    else
                                    {
                                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                        return nullptr;
                                    }
                                }
                            }
                            else
                            {
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                            }
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                            return nullptr;
                        }
                    }
                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                    return nullptr;
                }
            }
        }
    }
}
