/*
 * Geometry3D.h
 */

#pragma once

#include "../../../Common/Includes.h"
#include "../../../Common/DataTypes.h"
#include "../Vector3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                class CGeometry3D
                {
                public:

                    static bool AreUnitaryVectorsParallel(const CVector3D& A, const CVector3D& B);
                    static bool AreUnitaryVectorsOrthogonal(const CVector3D& A, const CVector3D& B);
                    static bool IsOnPlane(const CVector3D& Normal, const Real HesseDistance, const CVector3D& X);
                    static bool IsOnPlane(const CVector3D& Normal, const CVector3D& SupportPoint, const CVector3D& X);
                    static bool IsOnPositiveSideOfPlane(const CVector3D& Normal, const Real HesseDistance, const CVector3D& X);
                    static bool IsOnSphere(const CVector3D& Center, const Real Radius, const CVector3D& X);
                    static bool IsInsideSphere(const CVector3D& Center, const Real Radius, const CVector3D& X);
                    static CVector3D ComputeUnitaryDirectionVector(const CVector3D& A, const CVector3D& B);
                    static bool ComputeUnitaryOrthogonalVector(const CVector3D& A, const CVector3D& B, CVector3D& C);
                    static CVector3D ComputeProjectionPointToLine(const CVector3D& SupportPoint, const CVector3D& Direction, const CVector3D& X);
                    static CVector3D ComputeProjectionPointToPlane(const CVector3D& Normal, const CVector3D& SupportPoint, const CVector3D& X);
                    static CVector3D ComputeProjectionPointToPlane(const CVector3D& Normal, const Real HesseDistance, const CVector3D& X);
                    static bool ComputeIntersectionPlaneAndLine(const CVector3D& LineSupportPoint, const CVector3D& LineDirection, const CVector3D& PlaneNormal, const CVector3D& PlaneSupportPoint, CVector3D& X);
                    static bool ComputeIntersectionPlaneAndPlane(const CVector3D& PlaneNormal1, const Real HesseDistance1, const CVector3D& PlaneNormal2, const Real HesseDistance2, CVector3D& LineDirection, CVector3D& LineSupportPoint);
                    static bool IsIntersectingConfiguration(const Real R1, const Real R2, const Real D12, bool& SinglePointFlag);
                    static bool IsIntersectingConfiguration(const Real R1, const Real R2, const Real D12);
                    static bool CirclesIntersection(const Real LargeRadius, const Real ShortRadius, const Real CentersDistance, Real& DirectedCenterTranslation, Real& PerpendicularOffsetTranslation, bool& SinglePointFlag);
                    static bool ComputeLineToLineClosestPoints(const CVector3D& LineSupportPoint1, const CVector3D& LineDirection1, const CVector3D& LineSupportPoint2, const CVector3D& LineDirection2, CVector3D& Point1, CVector3D& Point2);
                    static bool ComputeIntersectionCircleAndLine(const CVector3D& Normal, const CVector3D& Center, const Real Radius, const CVector3D& LineSupportPoint, const CVector3D& LineDirection, CVector3D& A, CVector3D& B, bool& SinglePointFlag);

                private:

                    CGeometry3D();
                    virtual ~CGeometry3D();
                };
            }
        }
    }
}

