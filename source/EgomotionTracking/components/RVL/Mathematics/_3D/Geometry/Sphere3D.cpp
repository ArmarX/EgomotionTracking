/*
 * Sphere3D.cpp
 */

#include "Point3D.h"
#include "PointPair3D.h"
#include "Line3D.h"
#include "Circle3D.h"
#include "Plane3D.h"
#include "Sphere3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CSphere3D::CSphere3D() :
                    CGeometricPrimitive3D(eSphere, nullptr, nullptr),
                    m_Center(CVector3D::s_PlusInfinity),
                    m_Radius(Real(0))
                {
                }

                CSphere3D::CSphere3D(const Real CX, const Real CY, const Real CZ, const Real Radius, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(eSphere, pParentA, pParentB),
                    m_Center(CX, CY, CZ),
                    m_Radius(Radius)
                {
                    if (m_Center.IsNotAtInfinity() && IsNonZero(m_Radius) && IsNotAtInfinity(m_Radius))
                    {
                        SetStatus(eDefined);
                    }
                }

                CSphere3D::CSphere3D(const CVector3D& Center, const Real Radius, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(eSphere, pParentA, pParentB),
                    m_Center(Center),
                    m_Radius(Radius)
                {
                    if (m_Center.IsNotAtInfinity() && IsNonZero(m_Radius) && IsNotAtInfinity(m_Radius))
                    {
                        SetStatus(eDefined);
                    }
                }

                CSphere3D::~CSphere3D()
                    = default;

                void CSphere3D::SetCenter(const CVector3D& Center)
                {
                    if (Center != m_Center)
                    {
                        m_Center = Center;
                        if (m_Center.IsAtInfinity())
                        {
                            SetStatus(eUndefined);
                        }
                        else
                        {
                            SetStatus((IsNonZero(m_Radius) && IsNotAtInfinity(m_Radius)) ? eDefined : eUndefined);
                        }
                        OnChange();
                    }
                }

                const CVector3D& CSphere3D::GetCenter() const
                {
                    return m_Center;
                }

                void CSphere3D::SetRadius(const Real Radius)
                {
                    if (NonEquals(Radius, m_Radius))
                    {
                        m_Radius = Radius;
                        if (IsZero(m_Radius) || IsAtInfinity(m_Radius))
                        {
                            SetStatus(eUndefined);
                        }
                        else
                        {
                            SetStatus(m_Center.IsNotAtInfinity() ? eDefined : eUndefined);
                        }
                        OnChange();
                    }
                }

                Real CSphere3D::GetRadius() const
                {
                    return m_Radius;
                }

                bool CSphere3D::IsOnSurface(const CVector3D& X) const
                {
                    return CGeometry3D::IsOnSphere(m_Center, m_Radius, X);
                }

                bool CSphere3D::IsInside(const CVector3D& X) const
                {
                    return CGeometry3D::IsInsideSphere(m_Center, m_Radius, X);
                }

                Real CSphere3D::GetDistanceToSurface(const CVector3D& X) const
                {
                    return std::abs(m_Center.GetDistance(X) - m_Radius);
                }

                Real CSphere3D::GetOrientedDistanceToSurface(const CVector3D& X) const
                {
                    return m_Center.GetDistance(X) - m_Radius;
                }

                Real CSphere3D::GetDistanceToCenter(const CVector3D& X) const
                {
                    return m_Center.GetDistance(X);
                }

                CVector3D CSphere3D::GetClosestPointOnSurface(const CVector3D& X) const
                {
                    return m_Center + CGeometry3D::ComputeUnitaryDirectionVector(m_Center, X) * m_Radius;
                }

                const std::string CSphere3D::ToString() const
                {
                    std::ostringstream Content;
                    Content.precision(g_RealDisplayDigits);
                    Content << "<Sphere3D Id=\"" << GetInstanceId() << "\" Status=\"" << GetStatusStringMode() << "\" Cx=\"" << m_Center.GetX() << "\" Cy=\"" << m_Center.GetY() << "\" Cz=\"" << m_Center.GetZ() << "\" Radius=\"" << m_Radius << "\"";
                    AppendParentalInformation(Content);
                    Content << "/>";
                    return Content.str();
                }

                bool CSphere3D::operator==(const CGeometricPrimitive3D& GeometricPrimitive) const
                {
                    if (IsUndefined() || GeometricPrimitive.IsUndefined())
                    {
                        return false;
                    }
                    else if (&GeometricPrimitive == this)
                    {
                        return true;
                    }
                    else if (GeometricPrimitive.GetTypeId() == GetTypeId())
                    {
                        const CSphere3D* pSphere = dynamic_cast<const CSphere3D*>(&GeometricPrimitive);
                        return Equals(m_Radius, pSphere->m_Radius) && (m_Center == pSphere->m_Center);
                    }
                    else
                    {
                        return false;
                    }
                }

                IntersectionType CSphere3D::AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (pGeometricPrimitive && pGeometricPrimitive->IsDefined() && IsDefined())
                    {
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                if (pGeometricPrimitive == this)
                                {
                                    _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eSphere)
                                    return eSelfInstanceIntersection;
                                }
                                return AnalyzeIntersection(dynamic_cast<const CSphere3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case eCircle:
                                return AnalyzeIntersection(dynamic_cast<const CCircle3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case ePointPair:
                                return AnalyzeIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case ePoint:
                                return AnalyzeIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case ePlane:
                                return AnalyzeIntersection(dynamic_cast<const CPlane3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case eLine:
                                return AnalyzeIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case eNone:
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                            default:
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                CGeometricPrimitive3D* CSphere3D::GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pGeometricPrimitive && pGeometricPrimitive->IsDefined() && IsDefined())
                    {
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                if (pGeometricPrimitive == this)
                                {
                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eSphere, eSelfInstanceIntersection)
                                    return nullptr;
                                }
                                else
                                {
                                    return GenerateIntersection(dynamic_cast<const CSphere3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                }
                            case eCircle:
                                return GenerateIntersection(dynamic_cast<const CCircle3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case ePointPair:
                                return GenerateIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case ePoint:
                                return GenerateIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case ePlane:
                                return GenerateIntersection(dynamic_cast<const CPlane3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case eLine:
                                return GenerateIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case eNone:
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                            default:
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                        }
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                IntersectionType CSphere3D::AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (CGeometry3D::IsOnSphere(m_Center, m_Radius, pPoint->GetPoint()))
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                        return eSelfNumericalIntersection;
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                IntersectionType CSphere3D::AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const bool IntersectionA = CGeometry3D::IsOnSphere(m_Center, m_Radius, pPointPair->GetPointA());
                    const bool IntersectionB = CGeometry3D::IsOnSphere(m_Center, m_Radius, pPointPair->GetPointB());
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePointPair)
                            return eSelfNumericalIntersection;
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                            return eSubspace;
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                IntersectionType CSphere3D::AnalyzeIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const CVector3D ClosestPoint = CGeometry3D::ComputeProjectionPointToLine(pLine->GetSupportPoint(), pLine->GetDirection(), m_Center);
                    const Real Deviation = m_Center.GetDistance(ClosestPoint) - m_Radius;
                    if (IsNonPositive(Deviation))
                    {
                        if (IsZero(Deviation))
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                            return eSubspace;
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePointPair)
                            return eSubspace;
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                IntersectionType CSphere3D::AnalyzeIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const CVector3D& CircleCenter = pCircle->GetCenter();
                    const CVector3D& CircleNormal = pCircle->GetNormal();
                    const Real CircleRadius = pCircle->GetRadius();
                    const CVector3D Displacement = CircleCenter - m_Center;
                    const Real DisplacementLength = Displacement.GetLength();
                    bool SinglePointFlag = false;
                    if (CGeometry3D::IsIntersectingConfiguration(m_Radius, CircleRadius, DisplacementLength, SinglePointFlag))
                    {
                        if (SinglePointFlag)
                        {
                            const CVector3D X = m_Center + Displacement * (m_Radius / DisplacementLength);
                            if (CGeometry3D::IsOnPlane(CircleNormal, CircleCenter, X))
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                                return eSubspace;
                            }
                            else
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                            }
                        }
                        else if (m_Center == CircleCenter)
                        {
                            if (Equals(m_Radius, CircleRadius))
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eCircle)
                                return eSelfNumericalIntersection;
                            }
                            else
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                            }
                        }
                        else
                        {
                            const CVector3D ImplicitCircleCenter = CGeometry3D::ComputeProjectionPointToPlane(CircleNormal, CircleCenter, m_Center);
                            const Real DisplacementLength = m_Center.GetDistance(ImplicitCircleCenter);
                            const Real DeviationDisplacementLength = DisplacementLength - m_Radius;
                            if (IsNonPositive(DeviationDisplacementLength))
                            {
                                if (IsZero(DeviationDisplacementLength))
                                {
                                    if (Equals(CircleRadius, CircleCenter.GetDistance(ImplicitCircleCenter)))
                                    {
                                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                                        return eSubspace;
                                    }
                                    else
                                    {
                                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                        return eEmpty;
                                    }
                                }
                                else if (CGeometry3D::IsIntersectingConfiguration(std::sqrt(m_Radius * m_Radius - DisplacementLength * DisplacementLength), CircleRadius, CircleCenter.GetDistance(ImplicitCircleCenter), SinglePointFlag))
                                {
                                    if (SinglePointFlag)
                                    {
                                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                                        return eSubspace;
                                    }
                                    else
                                    {
                                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePointPair)
                                        return eSubspace;
                                    }
                                }
                                else
                                {
                                    _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                    return eEmpty;
                                }
                            }
                            else
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                            }
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                IntersectionType CSphere3D::AnalyzeIntersection(const CPlane3D* pPlane, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const Real Deviation = m_Center.GetDistance(CGeometry3D::ComputeProjectionPointToPlane(pPlane->GetNormal(), pPlane->GetHesseDistance(), m_Center)) - m_Radius;
                    if (IsNonPositive(Deviation))
                    {
                        if (IsZero(Deviation))
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                            return eSubspace;
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eCircle)
                            return eSubspace;
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                IntersectionType CSphere3D::AnalyzeIntersection(const CSphere3D* pSphere, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (m_Center == pSphere->m_Center)
                    {
                        if (Equals(m_Radius, pSphere->m_Radius))
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eSphere)
                            return eSelfNumericalIntersection;
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                            return eEmpty;
                        }
                    }
                    else
                    {
                        bool SinglePointFlag = false;
                        if (CGeometry3D::IsIntersectingConfiguration(m_Radius, pSphere->m_Radius, m_Center.GetDistance(pSphere->m_Center), SinglePointFlag))
                        {
                            if (SinglePointFlag)
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                                return eSubspace;
                            }
                            else
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eCircle)
                                return eSubspace;
                            }
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                            return eEmpty;
                        }
                    }
                }

                CGeometricPrimitive3D* CSphere3D::GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (CGeometry3D::IsOnSphere(m_Center, m_Radius, pPoint->GetPoint()))
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSelfNumericalIntersection)
                        return nullptr;
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                CGeometricPrimitive3D* CSphere3D::GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const bool IntersectionA = CGeometry3D::IsOnSphere(m_Center, m_Radius, pPointPair->GetPointA());
                    const bool IntersectionB = CGeometry3D::IsOnSphere(m_Center, m_Radius, pPointPair->GetPointB());
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePointPair, eSelfNumericalIntersection)
                            return nullptr;
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                            return new CPoint3D(IntersectionA ? pPointPair->GetPointA() : pPointPair->GetPointB(), this, pPointPair);
                        }
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                CGeometricPrimitive3D* CSphere3D::GenerateIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const CVector3D ClosestPoint = CGeometry3D::ComputeProjectionPointToLine(pLine->GetSupportPoint(), pLine->GetDirection(), m_Center);
                    const Real DisplacementLength = m_Center.GetDistance(ClosestPoint);
                    const Real Deviation = DisplacementLength - m_Radius;
                    if (IsNonPositive(Deviation))
                    {
                        if (IsZero(Deviation))
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                            return new CPoint3D(ClosestPoint, this, pLine);
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace)
                            const CVector3D AuxiliarDisplacement = pLine->GetDirection() * std::sqrt(m_Radius * m_Radius - DisplacementLength * DisplacementLength);
                            return new CPointPair3D(ClosestPoint + AuxiliarDisplacement, ClosestPoint - AuxiliarDisplacement, this, pLine);
                        }
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                CGeometricPrimitive3D* CSphere3D::GenerateIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const CVector3D& CircleCenter = pCircle->GetCenter();
                    const CVector3D& CircleNormal = pCircle->GetNormal();
                    const Real CircleRadius = pCircle->GetRadius();
                    const CVector3D Displacement = CircleCenter - m_Center;
                    const Real DisplacementLength = Displacement.GetLength();
                    bool SinglePointFlag = false;
                    if (CGeometry3D::IsIntersectingConfiguration(m_Radius, CircleRadius, DisplacementLength, SinglePointFlag))
                    {
                        if (SinglePointFlag)
                        {
                            const CVector3D X = m_Center + Displacement * (m_Radius / DisplacementLength);
                            if (CGeometry3D::IsOnPlane(CircleNormal, CircleCenter, X))
                            {
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                                return new CPoint3D(X, this, pCircle);
                            }
                            else
                            {
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                            }
                        }
                        else if (m_Center == CircleCenter)
                        {
                            if (Equals(m_Radius, CircleRadius))
                            {
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eCircle, eSelfNumericalIntersection)
                                return nullptr;
                            }
                            else
                            {
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                            }
                        }
                        else
                        {
                            const CVector3D ImplicitCircleCenter = CGeometry3D::ComputeProjectionPointToPlane(CircleNormal, CircleCenter, m_Center);
                            const CVector3D ImplicitCircleDisplacement = ImplicitCircleCenter - m_Center;
                            const Real ImplicitDisplacementLength = ImplicitCircleDisplacement.GetLength();
                            const Real DeviationDisplacementLength = ImplicitDisplacementLength - m_Radius;
                            if (IsNonPositive(DeviationDisplacementLength))
                            {
                                if (IsZero(DeviationDisplacementLength))
                                {
                                    const CVector3D X = m_Center + ImplicitCircleDisplacement * m_Radius / ImplicitDisplacementLength;
                                    if (CGeometry3D::IsOnSphere(CircleCenter, CircleRadius, X))
                                    {
                                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                                        return new CPoint3D(X, this, pCircle);
                                    }
                                    else
                                    {
                                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                        return nullptr;
                                    }
                                }
                                else
                                {
                                    const Real ImplicitCircleRadius = std::sqrt(m_Radius * m_Radius - ImplicitDisplacementLength * ImplicitDisplacementLength);
                                    const Real CircleToCircleDisplacementLenght = CircleCenter.GetDistance(ImplicitCircleCenter);
                                    if (CGeometry3D::IsIntersectingConfiguration(ImplicitCircleRadius, CircleRadius, CircleToCircleDisplacementLenght, SinglePointFlag))
                                    {
                                        Real Dx = Real(0), Dy = Real(0);
                                        if (CGeometry3D::CirclesIntersection(std::max(ImplicitCircleRadius, CircleRadius), std::min(ImplicitCircleRadius, CircleRadius), CircleToCircleDisplacementLenght, Dx, Dy, SinglePointFlag))
                                        {
                                            const CVector3D CenterBase = (ImplicitCircleRadius > CircleRadius) ? ImplicitCircleCenter : CircleCenter;
                                            const CVector3D CenterDirection = (ImplicitCircleRadius > CircleRadius) ? CircleCenter : ImplicitCircleCenter;
                                            const CVector3D Delta = CGeometry3D::ComputeUnitaryDirectionVector(CenterBase, CenterDirection) * Dx;
                                            const CVector3D MidPoint = CenterBase + Delta;
                                            if (SinglePointFlag)
                                            {
                                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                                                return new CPoint3D(MidPoint, this, pCircle);
                                            }
                                            else
                                            {
                                                CVector3D Beta;
                                                if (CGeometry3D::ComputeUnitaryOrthogonalVector(Delta, CircleNormal, Beta))
                                                {
                                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace)
                                                    Beta *= Dy;
                                                    return new CPointPair3D(MidPoint + Beta, MidPoint - Beta, this, pCircle);
                                                }
                                                else
                                                {
                                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                                    return nullptr;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                            return nullptr;
                                        }
                                    }
                                    else
                                    {
                                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                        return nullptr;
                                    }
                                }
                            }
                            else
                            {
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                            }
                        }
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                CGeometricPrimitive3D* CSphere3D::GenerateIntersection(const CPlane3D* pPlane, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const CVector3D ImplicitCircleCenter = CGeometry3D::ComputeProjectionPointToPlane(pPlane->GetNormal(), pPlane->GetHesseDistance(), m_Center);
                    const CVector3D Displacement = ImplicitCircleCenter - m_Center;
                    const Real DisplacementLength = Displacement.GetLength();
                    const Real DeviationDisplacementLength = DisplacementLength - m_Radius;
                    if (IsNonPositive(DeviationDisplacementLength))
                    {
                        if (IsZero(DeviationDisplacementLength))
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                            return new CPoint3D(m_Center + Displacement * (m_Radius / DisplacementLength), this, pPlane);
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eCircle, eSubspace)
                            return new CCircle3D(ImplicitCircleCenter, pPlane->GetNormal(), std::sqrt(m_Radius * m_Radius - DisplacementLength * DisplacementLength), this, pPlane);
                        }
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                CGeometricPrimitive3D* CSphere3D::GenerateIntersection(const CSphere3D* pSphere, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const CVector3D Displacement = pSphere->m_Center - m_Center;
                    const Real DisplacementLength = Displacement.GetLength();
                    if (CGeometry3D::IsIntersectingConfiguration(m_Radius, pSphere->m_Radius, DisplacementLength))
                    {
                        if (IsZero(DisplacementLength))
                        {
                            if (Equals(m_Radius, pSphere->m_Radius))
                            {
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eSphere, eSelfNumericalIntersection)
                                return nullptr;
                            }
                            else
                            {
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                            }
                        }
                        else
                        {
                            bool SinglePointFlag = false;
                            Real Dx = Real(0), Dy = Real(0);
                            if (CGeometry3D::CirclesIntersection(std::max(m_Radius, pSphere->m_Radius), std::min(m_Radius, pSphere->m_Radius), DisplacementLength, Dx, Dy, SinglePointFlag))
                            {
                                if (SinglePointFlag)
                                {
                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                                    return new CPoint3D(m_Center + Displacement * (m_Radius / DisplacementLength), this, pSphere);
                                }
                                else
                                {
                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eCircle, eSubspace)
                                    const CVector3D CenterBase = (m_Radius > pSphere->m_Radius) ? m_Center : pSphere->m_Center;
                                    const CVector3D Normal = CGeometry3D::ComputeUnitaryDirectionVector(CenterBase, (m_Radius > pSphere->m_Radius) ? pSphere->m_Center : m_Center);
                                    return new CCircle3D(CenterBase + (Normal * Dx), Normal, Dy, this, pSphere);
                                }
                            }
                            else
                            {
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                            }
                        }
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }
            }
        }
    }
}
