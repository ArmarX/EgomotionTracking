/*
 * Plane3DSourcePoints.cpp
 */

#include "Plane3DSourcePoints.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CPlane3DSourcePoints::CPlane3DSourcePoints(const CVector3D& A, const CVector3D& B, const CVector3D& C) :
                    m_A(A),
                    m_B(B),
                    m_C(C)
                {
                }

                CPlane3DSourcePoints::~CPlane3DSourcePoints()
                    = default;

                bool CPlane3DSourcePoints::SetPoints(const CVector3D& A, const CVector3D& B, const CVector3D& C)
                {
                    if ((A != m_A) || (B != m_B) || (C != m_C))
                    {
                        m_A = A;
                        m_B = B;
                        m_C = C;
                        return true;
                    }
                    return false;
                }

                void CPlane3DSourcePoints::GetPoints(CVector3D& A, CVector3D& B, CVector3D& C) const
                {
                    A = m_A;
                    B = m_B;
                    C = m_C;
                }

                const CVector3D& CPlane3DSourcePoints::GetPointA() const
                {
                    return m_A;
                }

                const CVector3D& CPlane3DSourcePoints::GetPointB() const
                {
                    return m_B;
                }

                const CVector3D& CPlane3DSourcePoints::GetPointC() const
                {
                    return m_C;
                }
            }
        }
    }
}
