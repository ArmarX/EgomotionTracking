/*
 * GeometricPrimitive3D.cpp
 */

#include "GeometricPrimitive3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                int CGeometricPrimitive3D::s_InstanceIdCounter = 0;

                void CGeometricPrimitive3D::ResetInstanceIdCounter()
                {
                    s_InstanceIdCounter = 0;
                }

                CGeometricPrimitive3D::CGeometricPrimitive3D(const GeometricPrimitive3DTypeId TypeId, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    m_InstanceId(++s_InstanceIdCounter),
                    m_TypeId(TypeId),
                    m_Status(eUndefined),
                    m_pParentA(nullptr),
                    m_pParentB(nullptr),
                    m_pDataPointer(nullptr)
                {
                    if (pParentA && pParentB)
                    {
                        if (pParentA->GetTypeId() < pParentB->GetTypeId())
                        {
                            m_pParentA = pParentA;
                            m_pParentB = pParentB;
                        }
                        else if (pParentA->GetTypeId() == pParentB->GetTypeId())
                        {
                            if (pParentA->GetInstanceId() < pParentB->GetInstanceId())
                            {
                                m_pParentA = pParentA;
                                m_pParentB = pParentB;
                            }
                            else
                            {
                                m_pParentA = pParentB;
                                m_pParentB = pParentA;
                            }
                        }
                        else
                        {
                            m_pParentA = pParentB;
                            m_pParentB = pParentA;
                        }
                    }
                }

                CGeometricPrimitive3D::~CGeometricPrimitive3D()
                    = default;

                int CGeometricPrimitive3D::GetInstanceId() const
                {
                    return m_InstanceId;
                }
                CGeometricPrimitive3D::GeometricPrimitive3DTypeId CGeometricPrimitive3D::GetTypeId() const
                {
                    return m_TypeId;
                }

                void CGeometricPrimitive3D::SetStatus(const Status CurrentStatus)
                {
                    m_Status = CurrentStatus;
                }

                CGeometricPrimitive3D::Status CGeometricPrimitive3D::GetStatus() const
                {
                    return m_Status;
                }

                bool CGeometricPrimitive3D::IsDefined() const
                {
                    return (m_Status == eDefined);
                }

                bool CGeometricPrimitive3D::IsUndefined() const
                {
                    return (m_Status == eUndefined);
                }

                const std::string CGeometricPrimitive3D::GetStatusStringMode() const
                {
                    switch (m_Status)
                    {
                        case eUndefined:
                            return std::string("Undefined");
                        case eDefined:
                            return std::string("Defined");
                    }
                    return std::string("Error");
                }

                void CGeometricPrimitive3D::SetDataPointer(const void* pDataPointer)
                {
                    m_pDataPointer = pDataPointer;
                }

                const void* CGeometricPrimitive3D::GetDataPointer() const
                {
                    return m_pDataPointer;
                }

                bool CGeometricPrimitive3D::HasDataPointer() const
                {
                    return m_pDataPointer;
                }

                bool CGeometricPrimitive3D::HasParents() const
                {
                    return (m_pParentA && m_pParentB);
                }

                const CGeometricPrimitive3D* CGeometricPrimitive3D::GetParentA() const
                {
                    return m_pParentA;
                }

                const CGeometricPrimitive3D* CGeometricPrimitive3D::GetParentB() const
                {
                    return m_pParentB;
                }

                bool CGeometricPrimitive3D::IsAncestor(const CGeometricPrimitive3D* pGeometricPrimitive, const bool FullSearch) const
                {
                    if (pGeometricPrimitive && m_pParentA && m_pParentB)
                    {
                        if ((m_pParentA == pGeometricPrimitive) || (m_pParentB == pGeometricPrimitive))
                        {
                            return true;
                        }
                        else if (FullSearch)
                        {
                            const CGeometricPrimitive3D* pAncestor = nullptr;
                            std::list<const CGeometricPrimitive3D*> AncestorsSearchList;
                            AncestorsSearchList.push_back(m_pParentA);
                            AncestorsSearchList.push_back(m_pParentB);
                            while (AncestorsSearchList.size())
                            {
                                pAncestor = AncestorsSearchList.front();
                                AncestorsSearchList.pop_front();
                                if (pAncestor == pGeometricPrimitive)
                                {
                                    return true;
                                }
                                if (pAncestor->m_pParentA && pAncestor->m_pParentB)
                                {
                                    AncestorsSearchList.push_back(pAncestor->m_pParentA);
                                    AncestorsSearchList.push_back(pAncestor->m_pParentB);
                                }
                            }
                        }
                    }
                    return false;
                }

                void CGeometricPrimitive3D::AppendParentalInformation(std::ostringstream& Content) const
                {
                    if (m_pParentA && m_pParentB)
                    {
                        Content << " ParentA=\"" << m_pParentA->GetInstanceId() << "\" ParentB=\"" << m_pParentB->GetInstanceId() << "\"";
                    }
                }
            }
        }
    }
}
