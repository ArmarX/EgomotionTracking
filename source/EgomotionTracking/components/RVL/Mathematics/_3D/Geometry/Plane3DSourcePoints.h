/*
 * Plane3DSourcePoints.h
 */

#pragma once

#include "Geometry3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                class CPlane3DSourcePoints
                {
                public:

                    CPlane3DSourcePoints(const CVector3D& A, const CVector3D& B, const CVector3D& C);
                    virtual ~CPlane3DSourcePoints();

                    bool SetPoints(const CVector3D& A, const CVector3D& B, const CVector3D& C);
                    void GetPoints(CVector3D& A, CVector3D& B, CVector3D& C) const;
                    const CVector3D& GetPointA() const;
                    const CVector3D& GetPointB() const;
                    const CVector3D& GetPointC() const;

                private:

                    CVector3D m_A;
                    CVector3D m_B;
                    CVector3D m_C;
                };
            }
        }
    }
}

