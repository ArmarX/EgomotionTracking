/*
 * Point3D.h
 */

#pragma once

#include "GeometricPrimitive3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                class CPoint3D : public CGeometricPrimitive3D
                {
                public:

                    CPoint3D();
                    CPoint3D(const Real X, const Real Y, const Real Z, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB);
                    CPoint3D(const CVector3D& Point, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB);
                    ~CPoint3D() override;

                    void SetPoint(const CVector3D& Point);
                    const CVector3D& GetPoint() const;
                    const std::string ToString() const override;
                    bool operator==(const CGeometricPrimitive3D& GeometricPrimitive) const override;
                    IntersectionType AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const override;
                    CGeometricPrimitive3D* GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const override;

                protected:

                    IntersectionType AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;

                private:

                    CVector3D m_Point;
                };
            }
        }
    }
}

