/*
 * Plane3D.cpp
 */

#include "Point3D.h"
#include "PointPair3D.h"
#include "Line3D.h"
#include "Circle3D.h"
#include "Plane3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CPlane3D::CPlane3D() :
                    CGeometricPrimitive3D(ePlane, nullptr, nullptr),
                    m_Normal(CVector3D::s_Zero),
                    m_HesseDistance(g_RealPlusInfinity),
                    m_pSourcePoints(nullptr)
                {
                }

                CPlane3D::CPlane3D(const Real NX, const Real NY, const Real NZ, const Real HesseDistance, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(ePlane, pParentA, pParentB),
                    m_Normal(NX, NY, NZ),
                    m_HesseDistance(HesseDistance),
                    m_pSourcePoints(nullptr)
                {
                    if (m_Normal.IsNonNull() && m_Normal.IsNotAtInfinity() && IsNotAtInfinity(m_HesseDistance))
                    {
                        m_Normal.Normalize();
                        SetStatus(eDefined);
                    }
                }

                CPlane3D::CPlane3D(const CVector3D& Normal, const CVector3D& SupportPoint, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(ePlane, pParentA, pParentB),
                    m_Normal(Normal),
                    m_HesseDistance(g_RealPlusInfinity),
                    m_pSourcePoints(nullptr)
                {
                    if (m_Normal.IsNonNull() && m_Normal.IsNotAtInfinity() && SupportPoint.IsNotAtInfinity())
                    {
                        m_Normal.Normalize();
                        m_HesseDistance = m_Normal.ScalarProduct(SupportPoint);
                        SetStatus(eDefined);
                    }
                }

                CPlane3D::CPlane3D(const CVector3D& Normal, const Real HesseDistance, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(ePlane, pParentA, pParentB),
                    m_Normal(Normal),
                    m_HesseDistance(HesseDistance),
                    m_pSourcePoints(nullptr)
                {
                    if (m_Normal.IsNonNull() && m_Normal.IsNotAtInfinity() && IsNotAtInfinity(m_HesseDistance))
                    {
                        m_Normal.Normalize();
                        SetStatus(eDefined);
                    }
                }

                CPlane3D::CPlane3D(const CVector3D& A, const CVector3D& B, const CVector3D& C, const CVector3D* pPointOnPositiveSide) :
                    CGeometricPrimitive3D(ePlane, nullptr, nullptr),
                    m_Normal(CVector3D::s_Zero),
                    m_HesseDistance(g_RealPlusInfinity),
                    m_pSourcePoints(nullptr)
                {
                    SetFromPoints(A, B, C, pPointOnPositiveSide);
                }

                CPlane3D::~CPlane3D()
                {
                    if (m_pSourcePoints)
                    {
                        delete m_pSourcePoints;
                    }
                }

                bool CPlane3D::SetFromPoints(const CVector3D& A, const CVector3D& B, const CVector3D& C, const CVector3D* pPointOnSide)
                {
                    if (A.IsNotAtInfinity() && B.IsNotAtInfinity() && C.IsNotAtInfinity())
                    {
                        const CVector3D AB = B - A;
                        if (AB.IsNonNull())
                        {
                            const CVector3D AC = C - A;
                            if (AC.IsNonNull())
                            {
                                const CVector3D PerpendicularVector = AB.VectorProduct(AC);
                                if (PerpendicularVector.IsNonNull())
                                {
                                    bool HasChanged = false;
                                    if (m_pSourcePoints)
                                    {
                                        HasChanged = m_pSourcePoints->SetPoints(A, B, C);
                                    }
                                    else
                                    {
                                        m_pSourcePoints = new CPlane3DSourcePoints(A, B, C);
                                        HasChanged = true;
                                    }
                                    m_Normal = PerpendicularVector;
                                    m_Normal.Normalize();
                                    m_HesseDistance = m_Normal.ScalarProduct(A);
                                    if (pPointOnSide && IsNegative(m_Normal.ScalarProduct(*pPointOnSide) - m_HesseDistance))
                                    {
                                        m_Normal.Negate();
                                        m_HesseDistance = m_Normal.ScalarProduct(A);
                                        m_pSourcePoints->SetPoints(A, C, B);
                                    }
                                    SetStatus(eDefined);
                                    if (HasChanged)
                                    {
                                        OnChange();
                                    }
                                    return true;
                                }
                                else
                                {
                                    if (m_pSourcePoints)
                                    {
                                        delete m_pSourcePoints;
                                        m_pSourcePoints = nullptr;
                                    }
                                    if (IsDefined())
                                    {
                                        SetStatus(eUndefined);
                                        OnChange();
                                    }
                                    return false;
                                }
                            }
                            else
                            {
                                if (m_pSourcePoints)
                                {
                                    delete m_pSourcePoints;
                                    m_pSourcePoints = nullptr;
                                }
                                if (IsDefined())
                                {
                                    SetStatus(eUndefined);
                                    OnChange();
                                }
                                return false;
                            }
                        }
                        else
                        {
                            if (m_pSourcePoints)
                            {
                                delete m_pSourcePoints;
                                m_pSourcePoints = nullptr;
                            }
                            if (IsDefined())
                            {
                                SetStatus(eUndefined);
                                OnChange();
                            }
                            return false;
                        }
                    }
                    else
                    {
                        if (m_pSourcePoints)
                        {
                            delete m_pSourcePoints;
                            m_pSourcePoints = nullptr;
                        }
                        if (IsDefined())
                        {
                            SetStatus(eUndefined);
                            OnChange();
                        }
                        return false;
                    }
                }

                void CPlane3D::SetNormal(const CVector3D& Normal)
                {
                    if (Normal != m_Normal)
                    {
                        m_Normal = Normal;
                        if (m_Normal.IsNull())
                        {
                            SetStatus(eUndefined);
                        }
                        else
                        {
                            m_Normal.Normalize();
                            SetStatus(IsNotAtInfinity(m_HesseDistance) ? eDefined : eUndefined);
                        }
                        OnChange();
                    }
                }

                const CVector3D& CPlane3D::GetNormal() const
                {
                    return m_Normal;
                }

                void CPlane3D::SetHesseDistance(const Real HesseDistance)
                {
                    if (NonEquals(HesseDistance, m_HesseDistance))
                    {
                        m_HesseDistance = HesseDistance;
                        if (IsAtInfinity(m_HesseDistance))
                        {
                            SetStatus(eUndefined);
                        }
                        else
                        {
                            SetStatus(m_Normal.IsNonNull() ? eDefined : eUndefined);
                        }
                        OnChange();
                    }
                }

                Real CPlane3D::GetHesseDistance() const
                {
                    return m_HesseDistance;
                }

                Real CPlane3D::GetDistanceToPoint(const CVector3D& X) const
                {
                    return std::abs(m_Normal.ScalarProduct(X) - m_HesseDistance);
                }

                Real CPlane3D::GetOrientedDistanceToPoint(const CVector3D& X) const
                {
                    return m_Normal.ScalarProduct(X) - m_HesseDistance;
                }

                bool CPlane3D::InOnPositiveSide(const CVector3D& X) const
                {
                    return CGeometry3D::IsOnPositiveSideOfPlane(m_Normal, m_HesseDistance, X);
                }

                CVector3D CPlane3D::GetClosestPointOnPlane(const CVector3D& X) const
                {
                    return X - (m_Normal * (m_Normal.ScalarProduct(X) - m_HesseDistance));
                }

                CVector3D CPlane3D::GetSupportPoint() const
                {
                    return m_Normal * m_HesseDistance;
                }

                bool CPlane3D::HasSourcePoints() const
                {
                    return m_pSourcePoints;
                }

                bool CPlane3D::GetSourcePoints(CVector3D& A, CVector3D& B, CVector3D& C) const
                {
                    if (IsDefined() && m_pSourcePoints)
                    {
                        m_pSourcePoints->GetPoints(A, B, C);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                const std::string CPlane3D::ToString() const
                {
                    std::ostringstream Content;
                    Content.precision(g_RealDisplayDigits);
                    Content << "<Plane3D Id=\"" << GetInstanceId() << "\" Status=\"" << GetStatusStringMode() << "\" Nx=\"" << m_Normal.GetX() << "\" Ny=\"" << m_Normal.GetY() << "\" Nz=\"" << m_Normal.GetZ() << "\" HesseDistance=\"" << m_HesseDistance << "\"";
                    AppendParentalInformation(Content);
                    Content << "/>";
                    return Content.str();
                }

                bool CPlane3D::operator==(const CGeometricPrimitive3D& GeometricPrimitive) const
                {
                    if (IsUndefined() || GeometricPrimitive.IsUndefined())
                    {
                        return false;
                    }
                    else if (&GeometricPrimitive == this)
                    {
                        return true;
                    }
                    else if (GeometricPrimitive.GetTypeId() == GetTypeId())
                    {
                        const CPlane3D* pPlane = dynamic_cast<const CPlane3D*>(&GeometricPrimitive);
                        return (m_Normal == pPlane->m_Normal) && CGeometry3D::IsOnPlane(m_Normal, m_HesseDistance, pPlane->m_Normal * pPlane->m_HesseDistance);
                    }
                    else
                    {
                        return false;
                    }
                }

                IntersectionType CPlane3D::AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (pGeometricPrimitive && pGeometricPrimitive->IsDefined() && IsDefined())
                    {
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case eCircle:
                                return AnalyzeIntersection(dynamic_cast<const CCircle3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case ePointPair:
                                return AnalyzeIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case ePoint:
                                return AnalyzeIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case ePlane:
                                if (pGeometricPrimitive == this)
                                {
                                    _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePlane)
                                    return eSelfInstanceIntersection;
                                }
                                return AnalyzeIntersection(dynamic_cast<const CPlane3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case eLine:
                                return AnalyzeIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case eNone:
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                            default:
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                CGeometricPrimitive3D* CPlane3D::GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pGeometricPrimitive && pGeometricPrimitive->IsDefined() && IsDefined())
                    {
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case eCircle:
                                return GenerateIntersection(dynamic_cast<const CCircle3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case ePointPair:
                                return GenerateIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case ePoint:
                                return GenerateIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case ePlane:
                                if (pGeometricPrimitive == this)
                                {
                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePlane, eSelfInstanceIntersection)
                                    return nullptr;
                                }
                                return GenerateIntersection(dynamic_cast<const CPlane3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case eLine:
                                return GenerateIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case eNone:
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                            default:
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                        }
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                IntersectionType CPlane3D::AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (CGeometry3D::IsOnPlane(m_Normal, m_HesseDistance, pPoint->GetPoint()))
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                        return eSelfNumericalIntersection;
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                IntersectionType CPlane3D::AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const bool IntersectionA = CGeometry3D::IsOnPlane(m_Normal, m_HesseDistance, pPointPair->GetPointA());
                    const bool IntersectionB = CGeometry3D::IsOnPlane(m_Normal, m_HesseDistance, pPointPair->GetPointB());
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePointPair)
                            return eSelfNumericalIntersection;
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                            return eSubspace;
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                IntersectionType CPlane3D::AnalyzeIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (CGeometry3D::AreUnitaryVectorsOrthogonal(m_Normal, pLine->GetDirection()))
                    {
                        if (CGeometry3D::IsOnPlane(m_Normal, m_HesseDistance, pLine->GetSupportPoint()))
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eLine)
                            return eSelfNumericalIntersection;
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                            return eEmpty;
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                        return eSubspace;
                    }
                }

                IntersectionType CPlane3D::AnalyzeIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const CVector3D& CircleCenter = pCircle->GetCenter();
                    const CVector3D& CircleNormal = pCircle->GetNormal();
                    if (CGeometry3D::AreUnitaryVectorsParallel(m_Normal, CircleNormal))
                    {
                        if (CGeometry3D::IsOnPlane(m_Normal, m_HesseDistance, CircleCenter))
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eCircle)
                            return eSelfNumericalIntersection;
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                            return eEmpty;
                        }
                    }
                    else
                    {
                        CVector3D LineDirection, LineSupportPoint;
                        if (CGeometry3D::ComputeIntersectionPlaneAndPlane(m_Normal, m_HesseDistance, CircleNormal, pCircle->GetHesseDistance(), LineDirection, LineSupportPoint))
                        {
                            CVector3D A, B;
                            bool SinglePointFlag = false;
                            if (CGeometry3D::ComputeIntersectionCircleAndLine(CircleNormal, CircleCenter, pCircle->GetRadius(), LineSupportPoint, LineDirection, A, B, SinglePointFlag))
                            {
                                if (SinglePointFlag)
                                {
                                    _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                                    return eSubspace;
                                }
                                else
                                {
                                    _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePointPair)
                                    return eSubspace;
                                }
                            }
                            else
                            {
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                            }
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                            return eEmpty;
                        }
                    }
                }

                IntersectionType CPlane3D::AnalyzeIntersection(const CPlane3D* pPlane, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (CGeometry3D::AreUnitaryVectorsParallel(m_Normal, pPlane->m_Normal))
                    {
                        if (CGeometry3D::IsOnPlane(m_Normal, m_HesseDistance, pPlane->m_Normal * pPlane->m_HesseDistance))
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePlane)
                            return eSelfNumericalIntersection;
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                            return eEmpty;
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eLine)
                        return eSubspace;
                    }
                }

                CGeometricPrimitive3D* CPlane3D::GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (CGeometry3D::IsOnPlane(m_Normal, m_HesseDistance, pPoint->GetPoint()))
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSelfNumericalIntersection)
                        return nullptr;
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                CGeometricPrimitive3D* CPlane3D::GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const bool IntersectionA = CGeometry3D::IsOnPlane(m_Normal, m_HesseDistance, pPointPair->GetPointA());
                    const bool IntersectionB = CGeometry3D::IsOnPlane(m_Normal, m_HesseDistance, pPointPair->GetPointB());
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePointPair, eSelfNumericalIntersection)
                            return nullptr;
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                            return new CPoint3D(IntersectionA ? pPointPair->GetPointA() : pPointPair->GetPointB(), this, pPointPair);
                        }
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                CGeometricPrimitive3D* CPlane3D::GenerateIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (CGeometry3D::AreUnitaryVectorsOrthogonal(m_Normal, pLine->GetDirection()))
                    {
                        if (CGeometry3D::IsOnPlane(m_Normal, m_HesseDistance, pLine->GetSupportPoint()))
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eLine, eSelfNumericalIntersection)
                            return nullptr;
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                            return nullptr;
                        }
                    }
                    else
                    {
                        CVector3D X;
                        if (CGeometry3D::ComputeIntersectionPlaneAndLine(pLine->GetSupportPoint(), pLine->GetDirection(), m_Normal, m_Normal * m_HesseDistance, X))
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                            return new CPoint3D(X, this, pLine);
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                            return nullptr;
                        }
                    }
                }

                CGeometricPrimitive3D* CPlane3D::GenerateIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const CVector3D& CircleCenter = pCircle->GetCenter();
                    const CVector3D& CircleNormal = pCircle->GetNormal();
                    if (CGeometry3D::AreUnitaryVectorsParallel(m_Normal, CircleNormal))
                    {
                        if (CGeometry3D::IsOnPlane(m_Normal, m_HesseDistance, CircleCenter))
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eCircle, eSelfNumericalIntersection)
                            return nullptr;
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                            return nullptr;
                        }
                    }
                    else
                    {
                        CVector3D LineDirection, LineSupportPoint;
                        if (CGeometry3D::ComputeIntersectionPlaneAndPlane(m_Normal, m_HesseDistance, CircleNormal, pCircle->GetHesseDistance(), LineDirection, LineSupportPoint))
                        {
                            CVector3D A, B;
                            bool SinglePointFlag = false;
                            if (CGeometry3D::ComputeIntersectionCircleAndLine(CircleNormal, CircleCenter, pCircle->GetRadius(), LineSupportPoint, LineDirection, A, B, SinglePointFlag))
                            {
                                if (SinglePointFlag)
                                {
                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                                    return new CPoint3D(A, this, pCircle);
                                }
                                else
                                {
                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace)
                                    return new CPointPair3D(A, B, this, pCircle);
                                }
                            }
                            else
                            {
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                            }
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                            return nullptr;
                        }
                    }
                }

                CGeometricPrimitive3D* CPlane3D::GenerateIntersection(const CPlane3D* pPlane, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (CGeometry3D::AreUnitaryVectorsParallel(m_Normal, pPlane->m_Normal))
                    {
                        if (CGeometry3D::IsOnPlane(m_Normal, m_HesseDistance, pPlane->m_Normal * pPlane->m_HesseDistance))
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePlane, eSelfNumericalIntersection)
                            return nullptr;
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                            return nullptr;
                        }
                    }
                    else
                    {
                        CVector3D LineDirection, LineSupportPoint;
                        if (CGeometry3D::ComputeIntersectionPlaneAndPlane(m_Normal, m_HesseDistance, pPlane->m_Normal, pPlane->m_HesseDistance, LineDirection, LineSupportPoint))
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eLine, eSubspace)
                            return new CLine3D(LineDirection, LineSupportPoint, CLine3D::eDirectionVectorAndSupportPoint, this, pPlane);
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                            return nullptr;
                        }
                    }
                }
            }
        }
    }
}
