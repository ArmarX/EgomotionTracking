/*
 * Geometry3D.cpp
 */

#include "Geometry3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CGeometry3D::CGeometry3D()
                    = default;

                CGeometry3D::~CGeometry3D()
                    = default;

                bool CGeometry3D::AreUnitaryVectorsParallel(const CVector3D& A, const CVector3D& B)
                {
                    return IsNonPositive(Real(1.0) - std::abs(A.ScalarProduct(B)));
                }

                bool CGeometry3D::AreUnitaryVectorsOrthogonal(const CVector3D& A, const CVector3D& B)
                {
                    return IsZero(A.ScalarProduct(B));
                }

                bool CGeometry3D::IsOnPlane(const CVector3D& Normal, const Real HesseDistance, const CVector3D& X)
                {
                    return IsZero(Normal.ScalarProduct(X) - HesseDistance);
                }

                bool CGeometry3D::IsOnPlane(const CVector3D& Normal, const CVector3D& SupportPoint, const CVector3D& X)
                {
                    return IsZero(Normal.ScalarProduct(X - SupportPoint));
                }

                bool CGeometry3D::IsOnPositiveSideOfPlane(const CVector3D& Normal, const Real HesseDistance, const CVector3D& X)
                {
                    return IsPositive(Normal.ScalarProduct(X) - HesseDistance);
                }

                bool CGeometry3D::IsOnSphere(const CVector3D& Center, const Real Radius, const CVector3D& X)
                {
                    return IsZero(Center.GetDistance(X) - Radius);
                }

                bool CGeometry3D::IsInsideSphere(const CVector3D& Center, const Real Radius, const CVector3D& X)
                {
                    return IsPositive(Radius - Center.GetDistance(X));
                }

                CVector3D CGeometry3D::ComputeUnitaryDirectionVector(const CVector3D& A, const CVector3D& B)
                {
                    CVector3D UnitaryDirectionVector = B - A;
                    UnitaryDirectionVector.Normalize();
                    return UnitaryDirectionVector;
                }

                bool CGeometry3D::ComputeUnitaryOrthogonalVector(const CVector3D& A, const CVector3D& B, CVector3D& C)
                {
                    C = A.VectorProduct(B);
                    return (C.Normalize() > Real(0));
                }

                CVector3D CGeometry3D::ComputeProjectionPointToLine(const CVector3D& SupportPoint, const CVector3D& Direction, const CVector3D& X)
                {
                    return SupportPoint + (Direction * Direction.ScalarProduct(X - SupportPoint));
                }

                CVector3D CGeometry3D::ComputeProjectionPointToPlane(const CVector3D& Normal, const CVector3D& SupportPoint, const CVector3D& X)
                {
                    return X - (Normal * Normal.ScalarProduct(X - SupportPoint));
                }

                CVector3D CGeometry3D::ComputeProjectionPointToPlane(const CVector3D& Normal, const Real HesseDistance, const CVector3D& X)
                {
                    return X - Normal * (Normal.ScalarProduct(X) - HesseDistance);
                }

                bool CGeometry3D::ComputeIntersectionPlaneAndLine(const CVector3D& LineSupportPoint, const CVector3D& LineDirection, const CVector3D& PlaneNormal, const CVector3D& PlaneSupportPoint, CVector3D& X)
                {
                    const Real Discriminat = PlaneNormal.ScalarProduct(LineDirection);
                    if (IsZero(Discriminat))
                    {
                        return false;
                    }
                    else
                    {
                        X = LineSupportPoint + LineDirection * (PlaneNormal.ScalarProduct(PlaneSupportPoint - LineSupportPoint) / Discriminat);
                        return true;
                    }
                }

                bool CGeometry3D::ComputeIntersectionPlaneAndPlane(const CVector3D& PlaneNormal1, const Real HesseDistance1, const CVector3D& PlaneNormal2, const Real HesseDistance2, CVector3D& LineDirection, CVector3D& LineSupportPoint)
                {
                    LineDirection = PlaneNormal1.VectorProduct(PlaneNormal2);
                    if (IsPositive(LineDirection.GetLength()))
                    {
                        LineDirection.Normalize();
                        Real A[3][3];
                        A[0][0] = PlaneNormal1.GetX();
                        A[0][1] = PlaneNormal1.GetY();
                        A[0][2] = PlaneNormal1.GetZ();
                        A[1][0] = PlaneNormal2.GetX();
                        A[1][1] = PlaneNormal2.GetY();
                        A[1][2] = PlaneNormal2.GetZ();
                        A[2][0] = LineDirection.GetX();
                        A[2][1] = LineDirection.GetY();
                        A[2][2] = LineDirection.GetZ();
                        const Real Determinant = A[0][0] * A[1][1] * A[2][2] - A[0][2] * A[1][1] * A[2][0] + A[0][1] * A[1][2] * A[2][0] + A[0][2] * A[1][0] * A[2][1] - A[0][0] * A[1][2] * A[2][1] - A[0][1] * A[1][0] * A[2][2];
                        if (IsNonZero(Determinant))
                        {
                            const Real Scale = Real(1.0) / Determinant;
                            Real PartialAI[3][2];
                            PartialAI[0][0] = (A[1][1] * A[2][2] - A[1][2] * A[2][1]) * Scale;
                            PartialAI[0][1] = (A[0][2] * A[2][1] - A[0][1] * A[2][2]) * Scale;
                            PartialAI[1][0] = (A[1][2] * A[2][0] - A[1][0] * A[2][2]) * Scale;
                            PartialAI[1][1] = (A[0][0] * A[2][2] - A[0][2] * A[2][0]) * Scale;
                            PartialAI[2][0] = (A[1][0] * A[2][1] - A[1][1] * A[2][0]) * Scale;
                            PartialAI[2][1] = (A[0][1] * A[2][0] - A[0][0] * A[2][1]) * Scale;
                            LineSupportPoint.Set(PartialAI[0][0] * HesseDistance1 + PartialAI[0][1] * HesseDistance2, PartialAI[1][0] * HesseDistance1 + PartialAI[1][1] * HesseDistance2, PartialAI[2][0] * HesseDistance1 + PartialAI[2][1] * HesseDistance2);
                            return true;
                        }
                    }
                    return false;
                }

                bool CGeometry3D::IsIntersectingConfiguration(const Real R1, const Real R2, const Real D12, bool& SinglePointFlag)
                {
                    if (IsNegative(R1 + R2 - D12) || IsNegative(R2 - R1 + D12) || IsNegative(R1 - R2 + D12))
                    {
                        return false;
                    }
                    SinglePointFlag = IsZero(R1 + R2 - D12);
                    return true;
                }

                bool CGeometry3D::IsIntersectingConfiguration(const Real R1, const Real R2, const Real D12)
                {
                    return !(IsNegative(R1 + R2 - D12) || IsNegative(R2 - R1 + D12) || IsNegative(R1 - R2 + D12));
                }

                bool CGeometry3D::CirclesIntersection(const Real LargeRadius, const Real ShortRadius, const Real CentersDistance, Real& DirectedCenterTranslation, Real& PerpendicularOffsetTranslation, bool& SinglePointFlag)
                {
                    if (IsZero(CentersDistance))
                    {
                        return false;
                    }
                    SinglePointFlag = IsZero(LargeRadius + ShortRadius - CentersDistance);
                    if (SinglePointFlag)
                    {
                        DirectedCenterTranslation = LargeRadius;
                        PerpendicularOffsetTranslation = Real(0);
                        return true;
                    }
                    else
                    {
                        DirectedCenterTranslation = (CentersDistance * CentersDistance - ShortRadius * ShortRadius + LargeRadius * LargeRadius) / (Real(2.0) * CentersDistance);
                        const Real Discriminat = LargeRadius * LargeRadius - DirectedCenterTranslation * DirectedCenterTranslation;
                        if (Discriminat < Real(0))
                        {
                            return false;
                        }
                        PerpendicularOffsetTranslation = std::sqrt(Discriminat);
                        return true;
                    }
                }

                bool CGeometry3D::ComputeLineToLineClosestPoints(const CVector3D& LineSupportPoint1, const CVector3D& LineDirection1, const CVector3D& LineSupportPoint2, const CVector3D& LineDirection2, CVector3D& Point1, CVector3D& Point2)
                {
                    if (IsPositive(LineDirection1.VectorProduct(LineDirection2).GetLength()))
                    {
                        Real A[3][2];//TODO OPTIMIZE THIS STEP
                        Real ATA[2][2];
                        A[0][0] = LineDirection1.GetX();
                        A[1][0] = LineDirection1.GetY();
                        A[2][0] = LineDirection1.GetZ();
                        A[0][1] = -LineDirection2.GetX();
                        A[1][1] = -LineDirection2.GetY();
                        A[2][1] = -LineDirection2.GetZ();
                        for (int r = 0 ; r < 2 ; ++r)
                            for (int c = 0 ; c < 2 ; ++c)
                            {
                                Real Accumulator = Real(0);
                                for (auto& i : A)
                                {
                                    Accumulator += i[r] * i[c];
                                }
                                ATA[r][c] = Accumulator;
                            }
                        const Real Determinant = ATA[0][0] * ATA[1][1] - ATA[1][0] * ATA[0][1];
                        if (IsNonZero(Determinant))
                        {
                            Real ATA_I[2][2];
                            Real ATA_IAT[2][3];
                            ATA_I[0][0] = ATA[1][1] / Determinant;
                            ATA_I[0][1] = -ATA[0][1] / Determinant;
                            ATA_I[1][0] = -ATA[1][0] / Determinant;
                            ATA_I[1][1] = ATA[0][0] / Determinant;
                            for (int r = 0 ; r < 2 ; ++r)
                                for (int c = 0 ; c < 3 ; ++c)
                                {
                                    Real Accumulator = Real(0);
                                    for (int i = 0 ; i < 2 ; ++i)
                                    {
                                        Accumulator += ATA_I[r][i] * A[c][i];
                                    }
                                    ATA_IAT[r][c] = Accumulator;
                                }
                            Real X[2] = { Real(0) };
                            const Real Deltas[] = { LineSupportPoint2.GetX() - LineSupportPoint1.GetX(), LineSupportPoint2.GetY() - LineSupportPoint1.GetY(), LineSupportPoint2.GetZ() - LineSupportPoint1.GetZ() };
                            for (int r = 0 ; r < 2 ; ++r)
                                for (int c = 0 ; c < 1 ; ++c)
                                {
                                    Real Accumulator = Real(0);
                                    for (int i = 0 ; i < 3 ; ++i)
                                    {
                                        Accumulator += ATA_IAT[r][i] * Deltas[i];
                                    }
                                    X[r] = Accumulator;
                                }
                            Point1 = LineSupportPoint1 + LineDirection1 * X[0];
                            Point2 = LineSupportPoint2 + LineDirection2 * X[1];
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                bool CGeometry3D::ComputeIntersectionCircleAndLine(const CVector3D& Normal, const CVector3D& Center, const Real Radius, const CVector3D& LineSupportPoint, const CVector3D& LineDirection, CVector3D& A, CVector3D& B, bool& SinglePointFlag)
                {
                    const CVector3D CircleCenterProjectedOntoLine = ComputeProjectionPointToLine(LineSupportPoint, LineDirection, Center);
                    const CVector3D Displacement = CircleCenterProjectedOntoLine - Center;
                    const Real DisplacementLenght = Displacement.GetLength();
                    const Real LenghtDeviation = DisplacementLenght - Radius;
                    SinglePointFlag = IsZero(LenghtDeviation);
                    if (SinglePointFlag)
                    {
                        A = CircleCenterProjectedOntoLine;
                        B = CircleCenterProjectedOntoLine;
                        return true;
                    }
                    else if (IsNegative(LenghtDeviation))
                    {
                        CVector3D ComplementaryDisplacement = Displacement.VectorProduct(Normal);
                        ComplementaryDisplacement.Normalize();
                        ComplementaryDisplacement *= std::sqrt(Radius * Radius - DisplacementLenght * DisplacementLenght);
                        A = CircleCenterProjectedOntoLine + ComplementaryDisplacement;
                        B = CircleCenterProjectedOntoLine - ComplementaryDisplacement;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }
}
