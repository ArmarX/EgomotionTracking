/*
 * Point3D.cpp
 */

#include "Point3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CPoint3D::CPoint3D() :
                    CGeometricPrimitive3D(ePoint, nullptr, nullptr),
                    m_Point(CVector3D::s_PlusInfinity)
                {
                }

                CPoint3D::CPoint3D(const Real X, const Real Y, const Real Z, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(ePoint, pParentA, pParentB),
                    m_Point(X, Y, Z)
                {
                    if (m_Point.IsNotAtInfinity())
                    {
                        SetStatus(eDefined);
                    }
                }

                CPoint3D::CPoint3D(const CVector3D& Point, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(ePoint, pParentA, pParentB),
                    m_Point(Point)
                {
                    if (m_Point.IsNotAtInfinity())
                    {
                        SetStatus(eDefined);
                    }
                }

                CPoint3D::~CPoint3D()
                    = default;

                void CPoint3D::SetPoint(const CVector3D& Point)
                {
                    if (Point != m_Point)
                    {
                        m_Point = Point;
                        SetStatus(m_Point.IsAtInfinity() ? eUndefined : eDefined);
                        OnChange();
                    }
                }

                const CVector3D& CPoint3D::GetPoint() const
                {
                    return m_Point;
                }

                const std::string CPoint3D::ToString() const
                {
                    std::ostringstream Content;
                    Content.precision(g_RealDisplayDigits);
                    Content << "<Point3D Id=\"" << GetInstanceId() << "\" Status=\"" << GetStatusStringMode() << "\" X=\"" << m_Point.GetX() << "\" Y=\"" << m_Point.GetY() << "\" Z=\"" << m_Point.GetZ() << "\"";
                    AppendParentalInformation(Content);
                    Content << "/>";
                    return Content.str();
                }

                bool CPoint3D::operator==(const CGeometricPrimitive3D& GeometricPrimitive) const
                {
                    if (IsUndefined() || GeometricPrimitive.IsUndefined())
                    {
                        return false;
                    }
                    else if (&GeometricPrimitive == this)
                    {
                        return true;
                    }
                    else if (GeometricPrimitive.GetTypeId() == GetTypeId())
                    {
                        const CPoint3D* pPoints = dynamic_cast<const CPoint3D*>(&GeometricPrimitive);
                        return (m_Point == pPoints->m_Point);
                    }
                    return false;
                }

                IntersectionType CPoint3D::AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case eCircle:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case ePointPair:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case ePoint:
                                if (pGeometricPrimitive == this)
                                {
                                    _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint);
                                    return eSelfInstanceIntersection;
                                }
                                return AnalyzeIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case ePlane:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case eLine:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case eNone:
                                break;
                        }
                    _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive3D* CPoint3D::GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case eCircle:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case ePointPair:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case ePoint:
                                if (pGeometricPrimitive == this)
                                {
                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSelfInstanceIntersection);
                                    return nullptr;
                                }
                                return GenerateIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case ePlane:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case eLine:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case eNone:
                                break;
                        }
                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                IntersectionType CPoint3D::AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (m_Point == pPoint->m_Point)
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint);
                        return eSelfNumericalIntersection;
                    }
                    _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive3D* CPoint3D::GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (m_Point == pPoint->m_Point)
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSelfNumericalIntersection);
                        return nullptr;
                    }
                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }
            }
        }
    }
}
