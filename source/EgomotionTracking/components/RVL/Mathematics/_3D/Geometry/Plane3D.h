/*
 * Plane3D.h
 */

#pragma once

#include "GeometricPrimitive3D.h"
#include "Plane3DSourcePoints.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                class CPoint3D;
                class CPointPair3D;
                class CLine3D;
                class CCircle3D;

                class CPlane3D : public CGeometricPrimitive3D
                {
                public:

                    CPlane3D();
                    CPlane3D(const Real NX, const Real NY, const Real NZ, const Real HesseDistance, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB);
                    CPlane3D(const CVector3D& Normal, const CVector3D& SupportPoint, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB);
                    CPlane3D(const CVector3D& Normal, const Real HesseDistance, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB);
                    CPlane3D(const CVector3D& A, const CVector3D& B, const CVector3D& C, const CVector3D* pPointOnPositiveSide);
                    ~CPlane3D() override;

                    bool SetFromPoints(const CVector3D& A, const CVector3D& B, const CVector3D& C, const CVector3D* pPointOnSide);
                    void SetNormal(const CVector3D& Normal);
                    const CVector3D& GetNormal() const;
                    void SetHesseDistance(const Real HesseDistance);
                    Real GetHesseDistance() const;
                    Real GetDistanceToPoint(const CVector3D& X) const;
                    Real GetOrientedDistanceToPoint(const CVector3D& X) const;
                    bool InOnPositiveSide(const CVector3D& X) const;
                    CVector3D GetClosestPointOnPlane(const CVector3D& X) const;
                    CVector3D GetSupportPoint() const;
                    bool HasSourcePoints() const;
                    bool GetSourcePoints(CVector3D& A, CVector3D& B, CVector3D& C) const;
                    const std::string ToString() const override;
                    bool operator==(const CGeometricPrimitive3D& GeometricPrimitive) const override;
                    IntersectionType AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const override;
                    CGeometricPrimitive3D* GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const override;

                protected:

                    IntersectionType AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CPlane3D* pPlane, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPlane3D* pPlane, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;

                private:

                    CVector3D m_Normal;
                    Real m_HesseDistance;
                    CPlane3DSourcePoints* m_pSourcePoints;
                };
            }
        }
    }
}

