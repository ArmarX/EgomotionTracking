/*
 * GeometricPrimitive3D.h
 */

#pragma once

#include "../../../Dependency/DependencyManager.h"
#include "Geometry3D.h"

//MACROS
#define _SET_GEOMETRIC_PRIMITIVE_TYPE_(pGeometricPrimitiveTypeId,TypeId) if(pGeometricPrimitiveTypeId) *pGeometricPrimitiveTypeId = TypeId;
#define _SET_INTERSECTION_INFORMATION_(pGeometricPrimitiveTypeId,pIntersectionType,TypeId,IntersectionTypeId) if(pGeometricPrimitiveTypeId) *pGeometricPrimitiveTypeId = TypeId; if(pIntersectionType) *pIntersectionType = IntersectionTypeId;

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                enum IntersectionType
                {
                    eEmpty = 0, eSubspace, eSelfNumericalIntersection, eSelfInstanceIntersection
                };

                class CGeometricPrimitive3D : public Dependency::IDependencyManager
                {
                public:

                    enum Status
                    {
                        eUndefined = 0, eDefined
                    };

                    enum GeometricPrimitive3DTypeId
                    {
                        eNone = 0, ePoint, ePointPair, eLine, eCircle, ePlane, eSphere
                    };

                    static void ResetInstanceIdCounter();

                    CGeometricPrimitive3D(const GeometricPrimitive3DTypeId TypeId, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB);
                    ~CGeometricPrimitive3D() override;

                    int GetInstanceId() const;

                    GeometricPrimitive3DTypeId GetTypeId() const;

                    void SetStatus(const Status CurrentStatus);
                    Status GetStatus() const;
                    bool IsDefined() const;
                    bool IsUndefined() const;
                    const std::string GetStatusStringMode() const;

                    void SetDataPointer(const void* pDataPointer);
                    const void* GetDataPointer() const;
                    bool HasDataPointer() const;

                    bool HasParents() const;
                    const CGeometricPrimitive3D* GetParentA() const;
                    const CGeometricPrimitive3D* GetParentB() const;
                    bool IsAncestor(const CGeometricPrimitive3D* pGeometricPrimitive, const bool FullSearch) const;

                    virtual const std::string ToString() const = 0;
                    virtual bool operator==(const CGeometricPrimitive3D& GeometricPrimitive) const = 0;
                    virtual IntersectionType AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const = 0;
                    virtual CGeometricPrimitive3D* GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const = 0;

                protected:

                    void AppendParentalInformation(std::ostringstream& Content) const;

                private:

                    static int s_InstanceIdCounter;
                    int m_InstanceId;
                    const GeometricPrimitive3DTypeId m_TypeId;
                    Status m_Status;
                    const CGeometricPrimitive3D* m_pParentA;
                    const CGeometricPrimitive3D* m_pParentB;
                    const void* m_pDataPointer;
                };

                inline Geometry::CGeometricPrimitive3D* operator^(const Geometry::CGeometricPrimitive3D& A, const Geometry::CGeometricPrimitive3D& B)
                {
                    return (A.GetTypeId() >= B.GetTypeId()) ? A.GenerateIntersection(&B, NULL, NULL) : B.GenerateIntersection(&A, NULL, NULL);
                }

                inline Geometry::IntersectionType operator&(const Geometry::CGeometricPrimitive3D& A, const Geometry::CGeometricPrimitive3D& B)
                {
                    return (A.GetTypeId() >= B.GetTypeId()) ? A.AnalyzeIntersection(&B, NULL) : B.AnalyzeIntersection(&A, NULL);
                }
            }
        }
    }
}

