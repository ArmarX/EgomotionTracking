/*
 * Line3D.h
 */

#pragma once

#include "GeometricPrimitive3D.h"
#include "Line3DSourcePoints.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                class CPoint3D;
                class CPointPair3D;

                class CLine3D : public CGeometricPrimitive3D
                {
                public:

                    enum ArgumentMode
                    {
                        eDirectionVectorAndSupportPoint, ePointAPointB
                    };

                    CLine3D();
                    CLine3D(const Real AX, const Real AY, const Real AZ, const Real BX, const Real BY, const Real BZ, const ArgumentMode Mode, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB);
                    CLine3D(const CVector3D& A, const CVector3D& B, const ArgumentMode Mode, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB);
                    ~CLine3D() override;

                    void SetLineByPoints(const CVector3D& A, const CVector3D& B);
                    bool HasSourcePoints() const;
                    bool GetSourcePoints(CVector3D& A, CVector3D& B) const;
                    void SetDirection(const CVector3D& Direction);
                    void SetSupportPoint(const CVector3D& SupportPoint);
                    const CVector3D& GetDirection() const;
                    const CVector3D& GetSupportPoint() const;
                    CVector3D GetPointAtUnitaryDisplacement() const;
                    CVector3D GetPointAtDisplacement(const Real Displacement) const;
                    CVector3D GetClosestPoint(const CVector3D& X) const;
                    bool GetClosestPoints(const CLine3D* pLine, CVector3D& Local, CVector3D& Remote) const;
                    const std::string ToString() const override;
                    bool operator==(const CGeometricPrimitive3D& GeometricPrimitive) const override;
                    IntersectionType AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const override;
                    CGeometricPrimitive3D* GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const override;

                protected:

                    IntersectionType AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;

                private:

                    CVector3D m_Direction;
                    CVector3D m_SupportPoint;
                    CLine3DSourcePoints* m_pLine3DSourcePoints;
                };
            }
        }
    }
}

