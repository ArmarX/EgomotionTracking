/*
 * Line3D.cpp
 */

#include "Point3D.h"
#include "PointPair3D.h"
#include "Line3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CLine3D::CLine3D() :
                    CGeometricPrimitive3D(eLine, nullptr, nullptr),
                    m_Direction(CVector3D::s_Zero),
                    m_SupportPoint(CVector3D::s_PlusInfinity),
                    m_pLine3DSourcePoints(nullptr)
                {
                }

                CLine3D::CLine3D(const Real AX, const Real AY, const Real AZ, const Real BX, const Real BY, const Real BZ, const ArgumentMode Mode, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(eLine, pParentA, pParentB),
                    m_Direction(CVector3D::s_Zero),
                    m_SupportPoint(CVector3D::s_PlusInfinity),
                    m_pLine3DSourcePoints(nullptr)
                {
                    switch (Mode)
                    {
                        case eDirectionVectorAndSupportPoint:
                            m_Direction.Set(AX, AY, AZ);
                            m_SupportPoint.Set(BX, BY, BZ);
                            if (m_Direction.IsNonNull() && m_SupportPoint.IsNotAtInfinity())
                            {
                                m_Direction.Normalize();
                                SetStatus(eDefined);
                            }
                            break;
                        case ePointAPointB:
                            SetLineByPoints(CVector3D(AX, AY, AZ), CVector3D(BX, BY, BZ));
                            break;
                    }
                }

                CLine3D::CLine3D(const CVector3D& A, const CVector3D& B, const ArgumentMode Mode, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(eLine, pParentA, pParentB),
                    m_Direction(CVector3D::s_Zero),
                    m_SupportPoint(CVector3D::s_PlusInfinity),
                    m_pLine3DSourcePoints(nullptr)
                {
                    switch (Mode)
                    {
                        case eDirectionVectorAndSupportPoint:
                            m_Direction = A;
                            m_SupportPoint = B;
                            if (m_Direction.IsNotAtInfinity() && m_SupportPoint.IsNotAtInfinity())
                            {
                                m_Direction.Normalize();
                                SetStatus(eDefined);
                            }
                            break;
                        case ePointAPointB:
                            SetLineByPoints(A, B);
                            break;
                    }
                }

                CLine3D::~CLine3D()
                {
                    if (m_pLine3DSourcePoints)
                    {
                        delete m_pLine3DSourcePoints;
                    }
                }

                void CLine3D::SetLineByPoints(const CVector3D& A, const CVector3D& B)
                {
                    if (A != B)
                    {
                        bool HasChanged = false;
                        if (m_pLine3DSourcePoints)
                        {
                            HasChanged = m_pLine3DSourcePoints->SetPoints(A, B);
                        }
                        else
                        {
                            m_pLine3DSourcePoints = new CLine3DSourcePoints(A, B);
                            HasChanged = true;
                        }
                        if (m_pLine3DSourcePoints->ArePointsValid())
                        {
                            m_SupportPoint = m_pLine3DSourcePoints->GetMidPoint();
                            m_Direction = m_pLine3DSourcePoints->GetDirection();
                            SetStatus(eDefined);
                        }
                        else
                        {
                            m_Direction = CVector3D::s_Zero;
                            m_SupportPoint = CVector3D::s_PlusInfinity;
                            SetStatus(eUndefined);
                        }
                        if (HasChanged)
                        {
                            OnChange();
                        }
                    }
                    else
                    {
                        m_Direction.SetZero();
                        m_SupportPoint = A;
                        SetStatus(eUndefined);
                        OnChange();
                    }
                }

                bool CLine3D::HasSourcePoints() const
                {
                    return m_pLine3DSourcePoints;
                }

                bool CLine3D::GetSourcePoints(CVector3D& A, CVector3D& B) const
                {
                    if (m_pLine3DSourcePoints)
                    {
                        m_pLine3DSourcePoints->GetPoints(A, B);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                void CLine3D::SetDirection(const CVector3D& Direction)
                {
                    if (Direction != m_Direction)
                    {
                        m_Direction = Direction;
                        if (m_Direction.IsNull())
                        {
                            SetStatus(eUndefined);
                        }
                        else
                        {
                            m_Direction.Normalize();
                            SetStatus(m_SupportPoint.IsAtInfinity() ? eUndefined : eDefined);
                        }
                        OnChange();
                    }
                }

                void CLine3D::SetSupportPoint(const CVector3D& SupportPoint)
                {
                    if (SupportPoint != m_SupportPoint)
                    {
                        m_SupportPoint = SupportPoint;
                        if (m_SupportPoint.IsAtInfinity())
                        {
                            SetStatus(eUndefined);
                        }
                        else
                        {
                            SetStatus(m_Direction.IsUnitary() ? eDefined : eUndefined);
                        }
                        OnChange();
                    }
                }

                const CVector3D& CLine3D::GetDirection() const
                {
                    return m_Direction;
                }

                const CVector3D& CLine3D::GetSupportPoint() const
                {
                    return m_SupportPoint;
                }

                CVector3D CLine3D::GetPointAtUnitaryDisplacement() const
                {
                    return m_SupportPoint + m_Direction;
                }

                CVector3D CLine3D::GetPointAtDisplacement(const Real Displacement) const
                {
                    return m_SupportPoint + (m_Direction * Displacement);
                }

                CVector3D CLine3D::GetClosestPoint(const CVector3D& X) const
                {
                    return CGeometry3D::ComputeProjectionPointToLine(m_SupportPoint, m_Direction, X);
                }

                bool CLine3D::GetClosestPoints(const CLine3D* pLine, CVector3D& Local, CVector3D& Remote) const
                {
                    return CGeometry3D::ComputeLineToLineClosestPoints(m_SupportPoint, m_Direction, pLine->m_SupportPoint, pLine->m_Direction, Local, Remote);
                }

                const std::string CLine3D::ToString() const
                {
                    std::ostringstream Content;
                    Content.precision(g_RealDisplayDigits);
                    Content << "<Line3D Id=\"" << GetInstanceId() << "\" Status=\"" << GetStatusStringMode() << "\" Dx=\"" << m_Direction.GetX() << "\" Dy=\"" << m_Direction.GetY() << "\" Dz=\"" << m_Direction.GetZ() << "\" Sx=\"" << m_SupportPoint.GetX() << "\" Sy=\"" << m_SupportPoint.GetY() << "\" Sz=\"" << m_SupportPoint.GetZ() << "\"";
                    AppendParentalInformation(Content);
                    Content << "/>";
                    return Content.str();
                }

                bool CLine3D::operator==(const CGeometricPrimitive3D& GeometricPrimitive) const
                {
                    if (IsUndefined() || GeometricPrimitive.IsUndefined())
                    {
                        return false;
                    }
                    else if (&GeometricPrimitive == this)
                    {
                        return true;
                    }
                    else if (GeometricPrimitive.GetTypeId() == GetTypeId())
                    {
                        const CLine3D* pLine = dynamic_cast<const CLine3D*>(&GeometricPrimitive);
                        if (CGeometry3D::AreUnitaryVectorsParallel(m_Direction, pLine->m_Direction))
                        {
                            return (pLine->m_SupportPoint == CGeometry3D::ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pLine->m_SupportPoint));
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                IntersectionType CLine3D::AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (pGeometricPrimitive && pGeometricPrimitive->IsDefined() && IsDefined())
                    {
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case eCircle:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case ePointPair:
                                return AnalyzeIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case ePoint:
                                return AnalyzeIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId);
                            case ePlane:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                            case eLine:
                                if (pGeometricPrimitive == this)
                                {
                                    _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eLine)
                                    return eSelfInstanceIntersection;
                                }
                                else
                                {
                                    return AnalyzeIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                }
                            case eNone:
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                            default:
                                _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                                return eEmpty;
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                CGeometricPrimitive3D* CLine3D::GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pGeometricPrimitive && pGeometricPrimitive->IsDefined() && IsDefined())
                    {
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case eCircle:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case ePointPair:
                                return GenerateIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case ePoint:
                                return GenerateIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                            case ePlane:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                            case eLine:
                                if (pGeometricPrimitive == this)
                                {
                                    _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eLine, eSelfInstanceIntersection)
                                    return nullptr;
                                }
                                else
                                {
                                    return GenerateIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                }
                            case eNone:
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                            default:
                                _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                                return nullptr;
                        }
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                IntersectionType CLine3D::AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (pPoint->GetPoint() == CGeometry3D::ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pPoint->GetPoint()))
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                        return eSelfNumericalIntersection;
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                IntersectionType CLine3D::AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const bool IntersectionA = (pPointPair->GetPointA() == CGeometry3D::ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pPointPair->GetPointA()));
                    const bool IntersectionB = (pPointPair->GetPointB() == CGeometry3D::ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pPointPair->GetPointB()));
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePointPair)
                            return eSelfNumericalIntersection;
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                            return eSubspace;
                        }
                    }
                    else
                    {
                        _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                        return eEmpty;
                    }
                }

                IntersectionType CLine3D::AnalyzeIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (CGeometry3D::AreUnitaryVectorsParallel(m_Direction, pLine->m_Direction))
                    {
                        if (pLine->m_SupportPoint == CGeometry3D::ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pLine->m_SupportPoint))
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eLine)
                            return eSelfNumericalIntersection;
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                            return eEmpty;
                        }
                    }
                    else
                    {
                        CVector3D Local, Remote;
                        if (CGeometry3D::ComputeLineToLineClosestPoints(m_SupportPoint, m_Direction, pLine->m_SupportPoint, pLine->m_Direction, Local, Remote) && (Local == Remote))
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, ePoint)
                            return eSubspace;
                        }
                        else
                        {
                            _SET_GEOMETRIC_PRIMITIVE_TYPE_(pIntersectionTypeId, eNone)
                            return eEmpty;
                        }
                    }
                }

                CGeometricPrimitive3D* CLine3D::GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pPoint->GetPoint() == CGeometry3D::ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pPoint->GetPoint()))
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSelfNumericalIntersection)
                        return nullptr;
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                CGeometricPrimitive3D* CLine3D::GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const bool IntersectionA = (pPointPair->GetPointA() == CGeometry3D::ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pPointPair->GetPointA()));
                    const bool IntersectionB = (pPointPair->GetPointB() == CGeometry3D::ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pPointPair->GetPointB()));
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePointPair, eSelfNumericalIntersection)
                            return nullptr;
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                            return new CPoint3D(IntersectionA ? pPointPair->GetPointA() : pPointPair->GetPointB(), this, pPointPair);
                        }
                    }
                    else
                    {
                        _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                        return nullptr;
                    }
                }

                CGeometricPrimitive3D* CLine3D::GenerateIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (CGeometry3D::AreUnitaryVectorsParallel(m_Direction, pLine->m_Direction))
                    {
                        if (pLine->m_SupportPoint == CGeometry3D::ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pLine->m_SupportPoint))
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eLine, eSelfNumericalIntersection)
                            return nullptr;
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                            return nullptr;
                        }
                    }
                    else
                    {
                        CVector3D Local, Remote;
                        if (CGeometry3D::ComputeLineToLineClosestPoints(m_SupportPoint, m_Direction, pLine->m_SupportPoint, pLine->m_Direction, Local, Remote) && (Local == Remote))
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace)
                            return new CPoint3D(CVector3D::CreateMidPoint(Local, Remote), this, pLine);
                        }
                        else
                        {
                            _SET_INTERSECTION_INFORMATION_(pIntersectionTypeId, pIntersectionType, eNone, eEmpty)
                            return nullptr;
                        }
                    }
                }
            }
        }
    }
}
