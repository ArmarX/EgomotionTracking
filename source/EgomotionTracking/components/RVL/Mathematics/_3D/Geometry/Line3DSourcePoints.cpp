/*
 * Line3DSourcePoints.cpp
 */

#include "Line3DSourcePoints.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CLine3DSourcePoints::CLine3DSourcePoints(const CVector3D& A, const CVector3D& B) :
                    m_A(A),
                    m_B(B)
                {
                }

                CLine3DSourcePoints::~CLine3DSourcePoints()
                    = default;

                bool CLine3DSourcePoints::SetPoints(const CVector3D& A, const CVector3D& B)
                {
                    if ((A != m_A) || (B != m_B))
                    {
                        m_A = A;
                        m_B = B;
                        return true;
                    }
                    return false;
                }

                void CLine3DSourcePoints::GetPoints(CVector3D& A, CVector3D& B) const
                {
                    A = m_A;
                    B = m_B;
                }

                bool CLine3DSourcePoints::ArePointsValid() const
                {
                    return ((m_A != m_B) && m_A.IsNotAtInfinity() && m_B.IsNotAtInfinity());
                }

                CVector3D CLine3DSourcePoints::GetMidPoint() const
                {
                    return CVector3D::CreateMidPoint(m_A, m_B);
                }

                CVector3D CLine3DSourcePoints::GetDirection() const
                {
                    return CGeometry3D::ComputeUnitaryDirectionVector(m_A, m_B);
                }

                const CVector3D& CLine3DSourcePoints::GetPointA() const
                {
                    return m_A;
                }

                const CVector3D& CLine3DSourcePoints::GetPointB() const
                {
                    return m_B;
                }
            }
        }
    }
}
