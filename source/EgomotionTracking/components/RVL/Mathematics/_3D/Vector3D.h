/*
 * Vector3D.h
 */

#pragma once

#include "../../Common/DataTypes.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            class CVector3D
            {
            public:

                static const CVector3D s_PlusInfinity;
                static const CVector3D s_Zero;
                static const CVector3D s_MinusInfinity;
                static const CVector3D s_Unitary_X;
                static const CVector3D s_Unitary_Y;
                static const CVector3D s_Unitary_Z;

                static CVector3D CreateLinealCombination(const CVector3D& A, const Real SA, const CVector3D& B, const Real SB);
                static CVector3D CreateComplementaryLinealCombination(const CVector3D& A, const CVector3D& B, const Real SA);
                static CVector3D CreateMidPoint(const CVector3D& A, const CVector3D& B);
                static CVector3D CreateNormalized(const CVector3D& Vector);
                static CVector3D CreateCannonicalForm(const Real X, const Real Y, const Real Z);

                CVector3D();
                CVector3D(const Real X, const Real Y, const Real Z);
                CVector3D(const CVector3D& Vector);
                ~CVector3D();

                bool SetCannonicalForm();
                void SetX(const Real X);
                void SetY(const Real Y);
                void SetZ(const Real Z);
                void Set(const Real X, const Real Y, const Real Z);
                void SetMaximalComponentValue(const Real X, const Real Y, const Real Z);
                void SetMinimalComponentValue(const Real X, const Real Y, const Real Z);
                void SetMaximalComponentValue(const CVector3D& Vector);
                void SetMinimalComponentValue(const CVector3D& Vector);
                void SetMidPoint(const CVector3D& A, const CVector3D& B);
                void SetWeighted(const CVector3D& Vector, const Real Weight);
                void SetWeightedInverse(const CVector3D& Vector, const Real Weight);
                void SetLinealCombination(const CVector3D& A, const Real SA, const CVector3D& B, const Real SB);
                void SetZero();
                void SetAtPlusInfinity();
                void SetAtMinusInfinity();
                void Negate();
                void AddWeightedOffset(const CVector3D& Vector, const Real Weight);
                void AddOffset(const Real DX, const Real DY, const Real DZ);
                void AddOffsetX(const Real DX);
                void AddOffsetY(const Real DY);
                void AddOffsetZ(const Real DZ);
                void SubtractOffsetX(const Real DX);
                void SubtractOffsetY(const Real DY);
                void SubtractOffsetZ(const Real DZ);
                void AddCovariance(const CVector3D& Vector, Real CXX, Real CXY, Real CXZ, Real CYY, Real CYZ, Real CZZ) const;
                void AddWeightedCovariance(const CVector3D& Vector, const Real Weight, Real CXX, Real CXY, Real CXZ, Real CYY, Real CYZ, Real CZZ) const;
                bool IsNull() const;
                bool IsNonNull() const;
                bool IsAtInfinity() const;
                bool IsNotAtInfinity() const;
                bool IsUnitary() const;
                bool IsNonUnitary() const;
                Real GetX() const;
                Real GetY() const;
                Real GetZ() const;
                Real GetAngle(const CVector3D& Vector) const;
                Real GetAperture(const CVector3D& Vector) const;
                Real GetAbsoluteAperture(const CVector3D& Vector) const;
                Real GetDistance(const CVector3D& Vector) const;
                Real GetLength() const;
                Real GetSquareLength() const;
                Real Normalize();
                void ScaleAnisotropic(const Real SX, const Real SY, const Real SZ);
                const std::string ToString() const;
                CVector3D VectorProduct(const CVector3D& Vector) const;
                Real ScalarProduct(const CVector3D& Vector) const;
                Real ScalarProduct(const Real X, const Real Y, const Real Z) const;
                Real GetSquareDistance(const CVector3D& Vector) const;
                void operator=(const CVector3D& Vector);
                void operator+=(const CVector3D& Vector);
                void operator-=(const CVector3D& Vector);
                void operator*=(const Real Scalar);
                void operator/=(const Real Scalar);
                bool operator==(const CVector3D& Vector) const;
                bool operator!=(const CVector3D& Vector) const;

            protected:

                Real m_X;
                Real m_Y;
                Real m_Z;
            };

            CVector3D operator/(const CVector3D& Vector, const Real Scalar);
            CVector3D operator*(const CVector3D& Vector, const Real Scalar);
            CVector3D operator*(const Real Scalar, const CVector3D& Vector);
            CVector3D operator+(const CVector3D& A, const CVector3D& B);
            CVector3D operator-(const CVector3D& A, const CVector3D& B);
        }
    }
}

