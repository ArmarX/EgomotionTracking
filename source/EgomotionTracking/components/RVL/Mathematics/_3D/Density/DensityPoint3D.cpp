/*
 * DensityPoint3D.cpp
 */

#include "DensityPoint3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                CDensityPoint3D::CDensityPoint3D() :
                    m_Point(CVector3D::s_PlusInfinity),
                    m_Density(Real(0))
                {
                }

                CDensityPoint3D::CDensityPoint3D(const CDensityPoint3D& DensityPoint3D)

                    = default;

                CDensityPoint3D::CDensityPoint3D(const CVector3D& Point, const Real Density) :
                    m_Point(Point),
                    m_Density(Density)
                {
                }

                CDensityPoint3D::~CDensityPoint3D()
                    = default;

                void CDensityPoint3D::SetValues(const CVector3D& Location, const Real Density)
                {
                    m_Point = Location;
                    m_Density = Density;
                }

                void CDensityPoint3D::SetDensity(const Real Density)
                {
                    m_Density = Density;
                }

                void CDensityPoint3D::Clear()
                {
                    m_Point = CVector3D::s_PlusInfinity;
                    m_Density = Real(0);
                }

                const CVector3D& CDensityPoint3D::GetPoint() const
                {
                    return m_Point;
                }

                Real CDensityPoint3D::GetDensity() const
                {
                    return m_Density;
                }

                bool CDensityPoint3D::IsValid() const
                {
                    return m_Point.IsNotAtInfinity();
                }
            }
        }
    }
}
