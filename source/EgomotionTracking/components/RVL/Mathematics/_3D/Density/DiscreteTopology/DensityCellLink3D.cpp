/*
 * DensityCellLink3D.cpp
 */

#include "DensityCellLink3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace DiscreteTopology
                {
                    CDensityCellLink3D::CDensityCellLink3D(const CVector3D& Point, const Real Density, const CVector3D& Normal, const CDensityPoint3D* pInnerCell, const CDensityPoint3D* pOuterCell) :
                        CDensityPoint3D(Point, Density),
                        m_Normal(Normal),
                        m_pInnerCell(pInnerCell),
                        m_pOuterCell(pOuterCell),
                        m_pUserData(nullptr)
                    {
                    }

                    CDensityCellLink3D::~CDensityCellLink3D()
                        = default;

                    void CDensityCellLink3D::SetUserData(const void* pUserData)
                    {
                        m_pUserData = pUserData;
                    }

                    const CVector3D& CDensityCellLink3D::GetNormal() const
                    {
                        return m_Normal;
                    }

                    const CDensityPoint3D* CDensityCellLink3D::GetInnerCell() const
                    {
                        return m_pInnerCell;
                    }

                    const CDensityPoint3D* CDensityCellLink3D::GetOuterCell() const
                    {
                        return m_pOuterCell;
                    }

                    const void* CDensityCellLink3D::GetUserData() const
                    {
                        return m_pUserData;
                    }
                }
            }
        }
    }
}
