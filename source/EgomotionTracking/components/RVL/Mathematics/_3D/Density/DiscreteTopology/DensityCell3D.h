/*
 * DensityCell3D.h
 */

#pragma once

#include "../DensityPoint3D.h"
#include "DensityCellLink3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace DiscreteTopology
                {
                    class CDensityCell3D : public CDensityPoint3D
                    {
                    public:

                        enum Status
                        {
                            eUndetermined, eUnsegmented, eOutter, eInner
                        };

                        CDensityCell3D();
                        ~CDensityCell3D();

                        void AddLink(CDensityCell3D* pCell, CDensityCellLink3D* pDensityCellLink);
                        const CDensityCellLink3D* GetLink(CDensityCell3D* pCell) const;
                        void Set(const CVector3D& Point, const Real Density);
                        void SetDensity(const Real Density);
                        bool Segment(const Real ThresholdDensity);
                        Status GetStatus() const;
                        bool IsUndetermined() const;
                        bool IsUnsegmented() const;
                        bool IsOutter() const;
                        bool IsInner() const;
                        bool IsBoundary() const;
                        void Clear();

                    protected:

                        Status m_Status;
                        std::map<CDensityCell3D*, CDensityCellLink3D*> m_Links;
                    };
                }
            }
        }
    }
}

