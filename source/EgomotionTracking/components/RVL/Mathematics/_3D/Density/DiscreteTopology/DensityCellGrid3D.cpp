/*
 * DensityCellGrid3D.cpp
 */

#include "DensityCellGrid3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace DiscreteTopology
                {
                    CDensityCellGrid3D::CDensityCellGrid3D() :
                        m_pCells(nullptr),
                        m_ConditioningMode(eConjunctive),
                        m_CellsWidth(0),
                        m_CellsHeight(0),
                        m_CellsDepth(0),
                        m_CellsSlide(0),
                        m_CellsVolume(0),
                        m_DiscretizationResolution(Real(0)),
                        m_CellDelta(),
                        m_BoundingBox(),
                        m_PrimitiveSet(),
                        m_CellLinks()
                    {
                    }

                    CDensityCellGrid3D::~CDensityCellGrid3D()
                    {
                        Clear();
                    }

                    bool CDensityCellGrid3D::Generate(const std::set<const CDensityPrimitive3D*>& PrimitiveSet, const ConditioningMode Mode, const Real ThresholdDensity, const Containers::_3D::CContinuousBoundingBox3D& BoundingBox, const Real DiscretizationResolution)
                    {
                        if (PrimitiveSet.size() && ((Mode == eConjunctive) || (Mode == eDisjunctive)) && BoundingBox.HasVolume() && IsNonPositive(DiscretizationResolution))
                        {
                            if (SetConfiguration(BoundingBox, DiscretizationResolution))
                            {
                                if (LoadDensity(PrimitiveSet, Mode))
                                {
                                    if (ExtractIsoDensityStructure(ThresholdDensity))
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                        return false;
                    }

                    CDensityCell3D* CDensityCellGrid3D::GetWritableCells()
                    {
                        return m_pCells;
                    }

                    const CDensityCell3D* CDensityCellGrid3D::GetReadOnlyCells() const
                    {
                        return m_pCells;
                    }

                    ConditioningMode CDensityCellGrid3D::GetConditioningMode() const
                    {
                        return m_ConditioningMode;
                    }

                    Real CDensityCellGrid3D::GetDiscretizationResolution() const
                    {
                        return m_DiscretizationResolution;
                    }

                    const CVector3D& CDensityCellGrid3D::GetCellDelta() const
                    {
                        return m_CellDelta;
                    }

                    const Containers::_3D::CContinuousBoundingBox3D& CDensityCellGrid3D::GetBoundingBox() const
                    {
                        return m_BoundingBox;
                    }

                    const std::set<const CDensityPrimitive3D*>& CDensityCellGrid3D::GetPrimitiveSet() const
                    {
                        return m_PrimitiveSet;
                    }

                    const std::list<CDensityCellLink3D*>& CDensityCellGrid3D::GetDensityCellLinks() const
                    {
                        return m_CellLinks;
                    }

                    int CDensityCellGrid3D::GetCellsWidth() const
                    {
                        return m_CellsWidth;
                    }

                    int CDensityCellGrid3D::GetCellsHeight() const
                    {
                        return m_CellsHeight;
                    }

                    int CDensityCellGrid3D::GetCellsDepth() const
                    {
                        return m_CellsDepth;
                    }

                    int CDensityCellGrid3D::GetCellsSlide() const
                    {
                        return m_CellsSlide;
                    }

                    int CDensityCellGrid3D::GetCellsVolume() const
                    {
                        return m_CellsVolume;
                    }

                    bool CDensityCellGrid3D::IsContourable() const
                    {
                        return m_CellLinks.size();
                    }

                    void CDensityCellGrid3D::Clear()
                    {
                        if (m_pCells)
                        {
                            delete[] m_pCells;
                            m_pCells = nullptr;
                        }
                        m_ConditioningMode = eConjunctive;
                        m_CellsWidth = 0;
                        m_CellsHeight = 0;
                        m_CellsDepth = 0;
                        m_CellsSlide = 0;
                        m_CellsVolume = 0;
                        m_DiscretizationResolution = Real(0);
                        m_CellDelta.SetZero();
                        m_BoundingBox.SetEmpty();
                        m_PrimitiveSet.clear();
                        if (m_CellLinks.size())
                        {
                            for (auto& m_CellLink : m_CellLinks)
                            {
                                delete m_CellLink;
                            }
                            m_CellLinks.clear();
                        }
                    }

                    bool CDensityCellGrid3D::SetConfiguration(const Containers::_3D::CContinuousBoundingBox3D& BoundingBox, const Real DiscretizationResolution)
                    {
                        if (BoundingBox.HasVolume() && (IsNonPositive(DiscretizationResolution)))
                        {
                            const Real Width = BoundingBox.GetWidth();
                            const Real Height = BoundingBox.GetHeight();
                            const Real Depth = BoundingBox.GetDepth();
                            if ((std::min(std::min(Width, Height), Depth) * Real(0.5)) >= DiscretizationResolution)
                            {
                                Clear();
                                m_BoundingBox = BoundingBox;
                                m_DiscretizationResolution = DiscretizationResolution;
                                m_CellsWidth = int(std::ceil(Width / m_DiscretizationResolution));
                                m_CellsHeight = int(std::ceil(Height / m_DiscretizationResolution));
                                m_CellsDepth = int(std::ceil(Depth / m_DiscretizationResolution));
                                m_CellsSlide = m_CellsWidth * m_CellsHeight;
                                m_CellsVolume = m_CellsSlide * m_CellsDepth;
                                m_CellDelta.Set(Width / Real(m_CellsWidth), Height / Real(m_CellsHeight), Depth / Real(m_CellsDepth));
                                try
                                {
                                    m_pCells = new CDensityCell3D[m_CellsVolume];
                                    return true;
                                }
                                catch (std::bad_alloc& Exception)
                                {
                                    std::ostringstream ExceptionInformation;
                                    ExceptionInformation << _RVL_HEAD_FULL_INFORMATION_ << _RVL_CONSOLE_CURRENT_PREFIX_LEVEL_ << Exception.what();
                                    _RVL_CONSOLE_OUTPUT_EXECEPTIONS_(ExceptionInformation, 0);
                                }
                            }
                        }
                        return false;
                    }

                    bool CDensityCellGrid3D::LoadDensity(const std::set<const CDensityPrimitive3D*>& PrimitiveSet, const ConditioningMode Mode)
                    {
                        if (PrimitiveSet.size() && m_pCells)
                        {
                            m_PrimitiveSet = PrimitiveSet;
                            m_ConditioningMode = Mode;
                            std::set<const CDensityPrimitive3D*>::const_iterator BeginDensityPrimitives = m_PrimitiveSet.begin();
                            std::set<const CDensityPrimitive3D*>::const_iterator EndDensityPrimitives = m_PrimitiveSet.end();
                            CVector3D Location = m_BoundingBox.GetMinPoint();
                            const Real Lx = Location.GetX();
                            const Real Ly = Location.GetY();
                            const Real Dx = m_CellDelta.GetX();
                            const Real Dy = m_CellDelta.GetY();
                            const Real Dz = m_CellDelta.GetZ();
                            CDensityCell3D* pDensityCells = m_pCells;
                            switch (m_ConditioningMode)
                            {
                                case eConjunctive:
                                    for (int Z = 0 ; Z < m_CellsDepth ; ++Z, Location.AddOffsetZ(Dz), Location.SetY(Ly))
                                        for (int Y = 0 ; Y < m_CellsHeight ; ++Y, Location.AddOffsetY(Dy), Location.SetX(Lx))
                                            for (int X = 0 ; X < m_CellsWidth ; ++X, ++pDensityCells, Location.AddOffsetX(Dx))
                                            {
                                                Real Density = Real(1);
                                                for (std::set<const CDensityPrimitive3D*>::const_iterator ppDensityPrimitive = BeginDensityPrimitives ; ppDensityPrimitive != EndDensityPrimitives ; ++ppDensityPrimitive)
                                                {
                                                    Density *= (*ppDensityPrimitive)->GetDensity(Location);
                                                }
                                                pDensityCells->Set(Location, Density);
                                            }
                                    break;
                                case eDisjunctive:
                                    for (int Z = 0 ; Z < m_CellsDepth ; ++Z, Location.AddOffsetZ(Dz), Location.SetY(Ly))
                                        for (int Y = 0 ; Y < m_CellsHeight ; ++Y, Location.AddOffsetY(Dy), Location.SetX(Lx))
                                            for (int X = 0 ; X < m_CellsWidth ; ++X, ++pDensityCells, Location.AddOffsetX(Dx))
                                            {
                                                Real Density = Real(0);
                                                for (std::set<const CDensityPrimitive3D*>::const_iterator ppDensityPrimitive = BeginDensityPrimitives ; ppDensityPrimitive != EndDensityPrimitives ; ++ppDensityPrimitive)
                                                {
                                                    Density += (*ppDensityPrimitive)->GetDensity(Location);
                                                }
                                                pDensityCells->Set(Location, Density);
                                            }
                                    break;
                            }
                            return true;
                        }
                        return false;
                    }

                    bool CDensityCellGrid3D::ExtractIsoDensityStructure(const Real ThresholdDensity)
                    {
                        const std::list<CDensityCell3D*> InnerDensityCells = SegmentDensityCells(ThresholdDensity);
                        if (InnerDensityCells.size())
                        {
                            int TotaBoundaryCells = 0;
                            switch (m_ConditioningMode)
                            {
                                case eConjunctive:
                                    TotaBoundaryCells = GenerateConjunctiveLinkage(InnerDensityCells, ThresholdDensity);
                                    break;
                                case eDisjunctive:
                                    TotaBoundaryCells = GenerateDisjunctiveLinkage(InnerDensityCells, ThresholdDensity);
                                    break;
                            }
                            return TotaBoundaryCells;
                        }
                        return false;
                    }

                    std::list<CDensityCell3D*> CDensityCellGrid3D::SegmentDensityCells(const Real ThresholdDensity)
                    {
                        const CDensityCell3D* const pEndDensityCells = m_pCells + m_CellsVolume;
                        CDensityCell3D* pDensityCell = m_pCells;
                        std::list<CDensityCell3D*> InnerDensityCells;
                        while (pDensityCell < pEndDensityCells)
                        {
                            if (pDensityCell->Segment(ThresholdDensity))
                            {
                                InnerDensityCells.push_back(pDensityCell);
                            }
                            pDensityCell++;
                        }
                        return InnerDensityCells;
                    }

                    void CDensityCellGrid3D::GetCellLocation(const CDensityCell3D* pDensityCell, int& X, int& Y, int& Z) const
                    {
                        const int Offset = pDensityCell - m_pCells;
                        Z = Offset / m_CellsSlide;
                        const int SubOffset = Offset - (Z * m_CellsSlide);
                        Y = SubOffset / m_CellsWidth;
                        X = SubOffset - (Y * m_CellsWidth);
                    }

                    int CDensityCellGrid3D::GenerateConjunctiveLinkage(const std::list<CDensityCell3D*>& InnerDensityCells, const Real ThresholdDensity)
                    {
                        const int SubCellsWidth = m_CellsWidth - 1;
                        const int SubCellsHeight = m_CellsHeight - 1;
                        const int SubCellsDepth = m_CellsDepth - 1;
                        int X = 0, Y = 0, Z = 0, TotaBoundaryCells = 0;
                        for (auto InnerDensityCell : InnerDensityCells)
                        {
                            CDensityCell3D* pInnerDensityCell = InnerDensityCell;
                            GetCellLocation(pInnerDensityCell, X, Y, Z);
                            if (X && (pInnerDensityCell - 1)->IsOutter())
                            {
                                m_CellLinks.push_back(CreateConjunctiveLink(pInnerDensityCell, pInnerDensityCell - 1, ThresholdDensity));
                            }
                            if ((X < SubCellsWidth) && (pInnerDensityCell + 1)->IsOutter())
                            {
                                m_CellLinks.push_back(CreateConjunctiveLink(pInnerDensityCell, pInnerDensityCell + 1, ThresholdDensity));
                            }
                            if (Y && (pInnerDensityCell - m_CellsWidth)->IsOutter())
                            {
                                m_CellLinks.push_back(CreateConjunctiveLink(pInnerDensityCell, pInnerDensityCell - m_CellsWidth, ThresholdDensity));
                            }
                            if ((Y < SubCellsHeight) && (pInnerDensityCell + m_CellsWidth)->IsOutter())
                            {
                                m_CellLinks.push_back(CreateConjunctiveLink(pInnerDensityCell, pInnerDensityCell + m_CellsWidth, ThresholdDensity));
                            }
                            if (Z && (pInnerDensityCell - m_CellsSlide)->IsOutter())
                            {
                                m_CellLinks.push_back(CreateConjunctiveLink(pInnerDensityCell, pInnerDensityCell - m_CellsSlide, ThresholdDensity));
                            }
                            if ((Z < SubCellsDepth) && (pInnerDensityCell + m_CellsSlide)->IsOutter())
                            {
                                m_CellLinks.push_back(CreateConjunctiveLink(pInnerDensityCell, pInnerDensityCell + m_CellsSlide, ThresholdDensity));
                            }
                            if (InnerDensityCell->IsBoundary())
                            {
                                ++TotaBoundaryCells;
                            }
                        }
                        return TotaBoundaryCells;
                    }

                    CDensityCellLink3D* CDensityCellGrid3D::CreateConjunctiveLink(CDensityCell3D* pInnerDensityCell, CDensityCell3D* pOutterDensityCell, const Real ThresholdDensity)
                    {
                        const Real DensityA = pInnerDensityCell->GetDensity();
                        const Real DensityB = pOutterDensityCell->GetDensity();
                        const Real DeltaA = DensityA - ThresholdDensity;
                        const Real DeltaB = ThresholdDensity - DensityB;
                        const Real TotalDeltas = DeltaA + DeltaB;
                        const Real ContributionA = DeltaB / TotalDeltas;
                        const Real ContributionB = DeltaA / TotalDeltas;
                        const CVector3D X = CVector3D::CreateLinealCombination(pInnerDensityCell->GetPoint(), ContributionA, pOutterDensityCell->GetPoint(), ContributionB);
                        CDensityCellLink3D* pDensityCellLink = new CDensityCellLink3D(X, DensityA * ContributionA + DensityB * ContributionB, EstimateConjunctiveNormal(X), pInnerDensityCell, pOutterDensityCell);
                        pInnerDensityCell->AddLink(pOutterDensityCell, pDensityCellLink);
                        pOutterDensityCell->AddLink(pInnerDensityCell, pDensityCellLink);
                        return pDensityCellLink;
                    }

                    CVector3D CDensityCellGrid3D::EstimateConjunctiveNormal(const CVector3D& X) const
                    {
                        Real Density = Real(0);
                        CVector3D Normal;
                        std::set<const CDensityPrimitive3D*>::const_iterator EndDensityPrimitives = m_PrimitiveSet.end();
                        for (std::set<const CDensityPrimitive3D*>::const_iterator ppDensityPrimitive = m_PrimitiveSet.begin() ; ppDensityPrimitive != EndDensityPrimitives ; ++ppDensityPrimitive)
                        {
                            const CVector3D Gradient = (*ppDensityPrimitive)->GetDensityGradientFullSampling(X, m_DiscretizationResolution, &Density);
                            Normal -= Gradient / Density;
                        }
                        Normal.Normalize();
                        return Normal;
                    }

                    int CDensityCellGrid3D::GenerateDisjunctiveLinkage(const std::list<CDensityCell3D*>& InnerDensityCells, const Real ThresholdDensity)
                    {
                        const int SubCellsWidth = m_CellsWidth - 1;
                        const int SubCellsHeight = m_CellsHeight - 1;
                        const int SubCellsDepth = m_CellsDepth - 1;
                        int X = 0, Y = 0, Z = 0, TotaBoundaryCells = 0;
                        for (auto InnerDensityCell : InnerDensityCells)
                        {
                            CDensityCell3D* pInnerDensityCell = InnerDensityCell;
                            GetCellLocation(pInnerDensityCell, X, Y, Z);
                            if (X && (pInnerDensityCell - 1)->IsOutter())
                            {
                                m_CellLinks.push_back(CreateDisConjunctiveLink(pInnerDensityCell, pInnerDensityCell - 1, ThresholdDensity));
                            }
                            if ((X < SubCellsWidth) && (pInnerDensityCell + 1)->IsOutter())
                            {
                                m_CellLinks.push_back(CreateDisConjunctiveLink(pInnerDensityCell, pInnerDensityCell + 1, ThresholdDensity));
                            }
                            if (Y && (pInnerDensityCell - m_CellsWidth)->IsOutter())
                            {
                                m_CellLinks.push_back(CreateDisConjunctiveLink(pInnerDensityCell, pInnerDensityCell - m_CellsWidth, ThresholdDensity));
                            }
                            if ((Y < SubCellsHeight) && (pInnerDensityCell + m_CellsWidth)->IsOutter())
                            {
                                m_CellLinks.push_back(CreateDisConjunctiveLink(pInnerDensityCell, pInnerDensityCell + m_CellsWidth, ThresholdDensity));
                            }
                            if (Z && (pInnerDensityCell - m_CellsSlide)->IsOutter())
                            {
                                m_CellLinks.push_back(CreateDisConjunctiveLink(pInnerDensityCell, pInnerDensityCell - m_CellsSlide, ThresholdDensity));
                            }
                            if ((Z < SubCellsDepth) && (pInnerDensityCell + m_CellsSlide)->IsOutter())
                            {
                                m_CellLinks.push_back(CreateDisConjunctiveLink(pInnerDensityCell, pInnerDensityCell + m_CellsSlide, ThresholdDensity));
                            }
                            if (InnerDensityCell->IsBoundary())
                            {
                                ++TotaBoundaryCells;
                            }
                        }
                        return TotaBoundaryCells;
                    }

                    CDensityCellLink3D* CDensityCellGrid3D::CreateDisConjunctiveLink(CDensityCell3D* pInnerDensityCell, CDensityCell3D* pOutterDensityCell, const Real ThresholdDensity)
                    {
                        const Real DensityA = pInnerDensityCell->GetDensity();
                        const Real DensityB = pOutterDensityCell->GetDensity();
                        const Real DeltaA = DensityA - ThresholdDensity;
                        const Real DeltaB = ThresholdDensity - DensityB;
                        const Real TotalDeltas = DeltaA + DeltaB;
                        const Real ContributionA = DeltaB / TotalDeltas;
                        const Real ContributionB = DeltaA / TotalDeltas;
                        const CVector3D X = CVector3D::CreateLinealCombination(pInnerDensityCell->GetPoint(), ContributionA, pOutterDensityCell->GetPoint(), ContributionB);
                        CDensityCellLink3D* pDensityCellLink = new CDensityCellLink3D(X, DensityA * ContributionA + DensityB * ContributionB, EstimateDisjunctiveNormal(X), pInnerDensityCell, pOutterDensityCell);
                        pInnerDensityCell->AddLink(pOutterDensityCell, pDensityCellLink);
                        pOutterDensityCell->AddLink(pInnerDensityCell, pDensityCellLink);
                        return pDensityCellLink;
                    }

                    CVector3D CDensityCellGrid3D::EstimateDisjunctiveNormal(const CVector3D& X) const
                    {
                        CVector3D Normal;
                        std::set<const CDensityPrimitive3D*>::const_iterator EndDensityPrimitives = m_PrimitiveSet.end();
                        for (std::set<const CDensityPrimitive3D*>::const_iterator ppDensityPrimitive = m_PrimitiveSet.begin() ; ppDensityPrimitive != EndDensityPrimitives ; ++ppDensityPrimitive)
                        {
                            Normal -= (*ppDensityPrimitive)->GetDensityGradientFullSampling(X, m_DiscretizationResolution, nullptr);
                        }
                        Normal.Normalize();
                        return Normal;
                    }
                }
            }
        }
    }
}
