/*
 * DensityCell3D.cpp
 */

#include "DensityCell3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace DiscreteTopology
                {
                    CDensityCell3D::CDensityCell3D() :
                        CDensityPoint3D(),
                        m_Status(eUndetermined)
                    {
                    }

                    CDensityCell3D::~CDensityCell3D()
                        = default;

                    void CDensityCell3D::AddLink(CDensityCell3D* pCell, CDensityCellLink3D* pDensityCellLink)
                    {
                        m_Links[pCell] = pDensityCellLink;
                    }

                    const CDensityCellLink3D* CDensityCell3D::GetLink(CDensityCell3D* pCell) const
                    {
                        std::map<CDensityCell3D*, CDensityCellLink3D*>::const_iterator pLocation = m_Links.find(pCell);
                        if (pLocation != m_Links.end())
                        {
                            return pLocation->second;
                        }
                        return nullptr;
                    }

                    void CDensityCell3D::Set(const CVector3D& Point, const Real Density)
                    {
                        CDensityPoint3D::m_Point = Point;
                        CDensityPoint3D::m_Density = Density;
                        m_Status = eUndetermined;
                    }

                    void CDensityCell3D::SetDensity(const Real Density)
                    {
                        CDensityPoint3D::m_Density = Density;
                        m_Status = eUndetermined;
                    }

                    bool CDensityCell3D::Segment(const Real ThresholdDensity)
                    {
                        if (CDensityPoint3D::m_Density >= ThresholdDensity)
                        {
                            m_Status = eInner;
                            return true;
                        }
                        else
                        {
                            m_Status = eOutter;
                            return false;
                        }
                    }

                    CDensityCell3D::Status CDensityCell3D::GetStatus() const
                    {
                        return m_Status;
                    }

                    bool CDensityCell3D::IsUndetermined() const
                    {
                        return (m_Status == eUndetermined);
                    }

                    bool CDensityCell3D::IsUnsegmented() const
                    {
                        return (m_Status == eUnsegmented);
                    }

                    bool CDensityCell3D::IsOutter() const
                    {
                        return (m_Status == eOutter);
                    }

                    bool CDensityCell3D::IsInner() const
                    {
                        return (m_Status == eInner);
                    }

                    bool CDensityCell3D::IsBoundary() const
                    {
                        return m_Links.size();
                    }

                    void CDensityCell3D::Clear()
                    {
                        CDensityPoint3D::m_Point = CVector3D::s_PlusInfinity;
                        CDensityPoint3D::m_Density = Real(0);
                        m_Links.clear();
                        m_Status = eUndetermined;
                    }
                }
            }
        }
    }
}
