/*
 * DensityCellLink3D.h
 */

#pragma once

#include "../DensityPoint3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace DiscreteTopology
                {
                    class CDensityCellLink3D : public CDensityPoint3D
                    {
                    public:

                        CDensityCellLink3D(const CVector3D& Point, const Real Density, const CVector3D& Normal, const CDensityPoint3D* pInnerCell, const CDensityPoint3D* pOuterCell);
                        ~CDensityCellLink3D();

                        void SetUserData(const void* pUserData);
                        const CVector3D& GetNormal() const;
                        const CDensityPoint3D* GetInnerCell() const;
                        const CDensityPoint3D* GetOuterCell() const;
                        const void* GetUserData() const;
                    protected:

                        const CVector3D m_Normal;
                        const CDensityPoint3D* m_pInnerCell;
                        const CDensityPoint3D* m_pOuterCell;
                        const void* m_pUserData;

                    };
                }
            }
        }
    }
}

