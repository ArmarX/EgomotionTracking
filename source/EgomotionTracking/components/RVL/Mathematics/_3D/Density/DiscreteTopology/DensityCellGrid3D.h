/*
 * DensityCellGrid3D.h
 */

#pragma once

#include "../../../../Console/ConsoleOutputManager.h"
#include "../DensityPrimitive3D.h"
#include "DensityCellLink3D.h"
#include "DensityCell3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace DiscreteTopology
                {
                    class CDensityCellGrid3D
                    {
                    public:

                        CDensityCellGrid3D();
                        virtual ~CDensityCellGrid3D();

                        bool Generate(const std::set<const CDensityPrimitive3D*>& PrimitiveSet, const ConditioningMode Mode, const Real ThresholdDensity, const Containers::_3D::CContinuousBoundingBox3D& BoundingBox, const Real DiscretizationResolution);
                        CDensityCell3D* GetWritableCells();
                        const CDensityCell3D* GetReadOnlyCells() const;
                        ConditioningMode GetConditioningMode() const;
                        Real GetDiscretizationResolution() const;
                        const CVector3D& GetCellDelta() const;
                        const Containers::_3D::CContinuousBoundingBox3D& GetBoundingBox() const;
                        const std::set<const CDensityPrimitive3D*>& GetPrimitiveSet() const;
                        const std::list<CDensityCellLink3D*>& GetDensityCellLinks() const;
                        int GetCellsWidth() const;
                        int GetCellsHeight() const;
                        int GetCellsDepth() const;
                        int GetCellsSlide() const;
                        int GetCellsVolume() const;
                        bool IsContourable() const;
                        void Clear();

                    protected:

                        bool SetConfiguration(const Containers::_3D::CContinuousBoundingBox3D& BoundingBox, const Real DiscretizationResolution);
                        bool LoadDensity(const std::set<const CDensityPrimitive3D*>& PrimitiveSet, const ConditioningMode Mode);
                        bool ExtractIsoDensityStructure(const Real ThresholdDensity);
                        std::list<CDensityCell3D*> SegmentDensityCells(const Real ThresholdDensity);
                        void GetCellLocation(const CDensityCell3D* pDensityCell, int& X, int& Y, int& Z) const;
                        int GenerateConjunctiveLinkage(const std::list<CDensityCell3D*>& InnerDensityCells, const Real ThresholdDensity);
                        CDensityCellLink3D* CreateConjunctiveLink(CDensityCell3D* pInnerDensityCell, CDensityCell3D* pOutterDensityCell, const Real ThresholdDensity);
                        CVector3D EstimateConjunctiveNormal(const CVector3D& X) const;
                        int GenerateDisjunctiveLinkage(const std::list<CDensityCell3D*>& InnerDensityCells, const Real ThresholdDensity);
                        CDensityCellLink3D* CreateDisConjunctiveLink(CDensityCell3D* pInnerDensityCell, CDensityCell3D* pOutterDensityCell, const Real ThresholdDensity);
                        CVector3D EstimateDisjunctiveNormal(const CVector3D& X) const;

                    private:

                        CDensityCell3D* m_pCells;
                        ConditioningMode m_ConditioningMode;
                        int m_CellsWidth;
                        int m_CellsHeight;
                        int m_CellsDepth;
                        int m_CellsSlide;
                        int m_CellsVolume;
                        Real m_DiscretizationResolution;
                        CVector3D m_CellDelta;
                        Containers::_3D::CContinuousBoundingBox3D m_BoundingBox;
                        std::set<const CDensityPrimitive3D*> m_PrimitiveSet;
                        std::list<CDensityCellLink3D*> m_CellLinks;
                    };
                }
            }
        }
    }
}

