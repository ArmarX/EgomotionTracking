/*
 * DensityPrimitive3D.cpp
 */

#include "DensityPrimitive3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                CDensityPrimitive3D::CDensityPrimitive3D(const ShapeTypeId ShapeId, const DensityProfileTypeId ProfileId) :
                    m_ShapeTypeId(ShapeId),
                    m_DensityProfileTypeId(ProfileId)
                {
                }

                CDensityPrimitive3D::~CDensityPrimitive3D()
                    = default;

                ShapeTypeId CDensityPrimitive3D::GetShapeTypeId() const
                {
                    return m_ShapeTypeId;
                }

                DensityProfileTypeId CDensityPrimitive3D::GetDensityProfileTypeId() const
                {
                    return m_DensityProfileTypeId;
                }
            }
        }
    }
}
