/*
 * IsodensityTriangularFace3D.cpp
 */

#include "IsodensityTriangularFace3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Isodensity
                {

                    CIsodensityTriangularFace3D::CIsodensityTriangularFace3D(const CIsodensityVertex3D* pIsodensityVertexA, const CIsodensityVertex3D* pIsodensityVertexB, const CIsodensityVertex3D* pIsodensityVertexC) :
                        m_pIsodensityVertexA(pIsodensityVertexA),
                        m_pIsodensityVertexB(pIsodensityVertexB),
                        m_pIsodensityVertexC(pIsodensityVertexC),
                        m_Normal(),
                        m_Area(Real(0))
                    {
                        const CVector3D AB = m_pIsodensityVertexB->GetPoint() - m_pIsodensityVertexA->GetPoint();
                        const CVector3D AC = m_pIsodensityVertexC->GetPoint() - m_pIsodensityVertexA->GetPoint();
                        m_Normal = AB.VectorProduct(AC);
                        m_Area = m_Normal.Normalize() * Real(0.5);
                    }

                    CIsodensityTriangularFace3D::~CIsodensityTriangularFace3D()
                        = default;

                    int CIsodensityTriangularFace3D::GetIndexIsodensityVertexA() const
                    {
                        return m_pIsodensityVertexA->GetIndex();
                    }

                    int CIsodensityTriangularFace3D::GetIndexIsodensityVertexB() const
                    {
                        return m_pIsodensityVertexB->GetIndex();
                    }

                    int CIsodensityTriangularFace3D::GetIndexIsodensityVertexC() const
                    {
                        return m_pIsodensityVertexC->GetIndex();
                    }

                    const CIsodensityVertex3D* CIsodensityTriangularFace3D::GetIsodensityVertexA() const
                    {
                        return m_pIsodensityVertexA;
                    }

                    const CIsodensityVertex3D* CIsodensityTriangularFace3D::GetIsodensityVertexB() const
                    {
                        return m_pIsodensityVertexB;
                    }

                    const CIsodensityVertex3D* CIsodensityTriangularFace3D::GetIsodensityVertexC() const
                    {
                        return m_pIsodensityVertexC;
                    }

                    const CVector3D& CIsodensityTriangularFace3D::GetNorma() const
                    {
                        return m_Normal;
                    }

                    Real CIsodensityTriangularFace3D::GetArea() const
                    {
                        return m_Area;
                    }

                    CVector3D CIsodensityTriangularFace3D::GetAreaWeightedNorma() const
                    {
                        return m_Normal * m_Area;
                    }
                }
            }
        }
    }
}
