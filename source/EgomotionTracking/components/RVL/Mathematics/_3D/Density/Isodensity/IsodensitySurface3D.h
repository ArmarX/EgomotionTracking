/*
 * IsodensitySurface3D.h
 */

#pragma once

#include "../DiscreteTopology/DensityCell3D.h"
#include "../DiscreteTopology/DensityCellGrid3D.h"
#include "../DiscreteTopology/DensityCellLink3D.h"

#include "IsodensityVertex3D.h"
#include "IsodensityTriangularFace3D.h"
#include "MarchingCubes.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Isodensity
                {
                    class CIsodensitySurface3D
                    {
                    public:

                        CIsodensitySurface3D();
                        virtual ~CIsodensitySurface3D();
                        bool Generate(DiscreteTopology::CDensityCellGrid3D* pDensityCellGrid);
                        const DiscreteTopology::CDensityCellGrid3D* GetDensityCellGrid() const;
                        const std::list<CIsodensityVertex3D*>& GetIsodensityVertices() const;
                        const std::list<CIsodensityTriangularFace3D*>& GetIsodensityTriangularFaces() const;
                        void Clear();

                    protected:

                        void CreateIsodensityVertices();
                        void CreateIsodensityFaces();

                        const CMarchingCubes::Configuration* m_pMarchingCubesConfigurationLUT;
                        DiscreteTopology::CDensityCellGrid3D* m_pDensityCellGrid;
                        std::list<CIsodensityVertex3D*> m_IsodensityVertices;
                        std::list<CIsodensityTriangularFace3D*> m_IsodensityTriangularFaces;
                    };
                }
            }
        }
    }
}

