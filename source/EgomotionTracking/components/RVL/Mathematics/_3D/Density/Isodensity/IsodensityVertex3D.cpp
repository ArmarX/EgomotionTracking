/*
 * IsodensityVertex3D.cpp
 */

#include "IsodensityVertex3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Isodensity
                {
                    CIsodensityVertex3D::CIsodensityVertex3D(const int Index, const CVector3D& Point, const CVector3D& Normal, const Real Density) :
                        CDensityPoint3D(Point, Density),
                        m_Index(Index),
                        m_Normal(Normal)
                    {
                    }

                    CIsodensityVertex3D::~CIsodensityVertex3D()
                        = default;

                    int CIsodensityVertex3D::GetIndex() const
                    {
                        return m_Index;
                    }

                    const CVector3D& CIsodensityVertex3D::GetNormal() const
                    {
                        return m_Normal;
                    }
                }
            }
        }
    }
}
