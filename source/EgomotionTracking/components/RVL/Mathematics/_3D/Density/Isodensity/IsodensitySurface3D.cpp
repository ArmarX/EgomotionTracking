/*
 * IsodensitySurface3D.cpp
 */

#include "IsodensitySurface3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Isodensity
                {
                    CIsodensitySurface3D::CIsodensitySurface3D() :
                        m_pMarchingCubesConfigurationLUT(CMarchingCubes::GetConfigurationLUT()),
                        m_pDensityCellGrid(nullptr),
                        m_IsodensityVertices(),
                        m_IsodensityTriangularFaces()
                    {
                    }

                    CIsodensitySurface3D::~CIsodensitySurface3D()
                    {
                        Clear();
                    }

                    bool CIsodensitySurface3D::Generate(DiscreteTopology::CDensityCellGrid3D* pDensityCellGrid)
                    {
                        if (pDensityCellGrid && pDensityCellGrid->IsContourable())
                        {
                            m_pDensityCellGrid = pDensityCellGrid;
                            Clear();
                            CreateIsodensityVertices();
                            CreateIsodensityFaces();
                            return true;
                        }
                        return false;
                    }

                    const DiscreteTopology::CDensityCellGrid3D* CIsodensitySurface3D::GetDensityCellGrid() const
                    {
                        return m_pDensityCellGrid;
                    }

                    const std::list<CIsodensityVertex3D*>& CIsodensitySurface3D::GetIsodensityVertices() const
                    {
                        return m_IsodensityVertices;
                    }

                    const std::list<CIsodensityTriangularFace3D*>& CIsodensitySurface3D::GetIsodensityTriangularFaces() const
                    {
                        return m_IsodensityTriangularFaces;
                    }

                    void CIsodensitySurface3D::Clear()
                    {
                        if (m_IsodensityVertices.size())
                        {
                            for (auto& m_IsodensityVertice : m_IsodensityVertices)
                            {
                                delete m_IsodensityVertice;
                            }
                            m_IsodensityVertices.clear();
                        }
                        if (m_IsodensityTriangularFaces.size())
                        {
                            for (auto& m_IsodensityTriangularFace : m_IsodensityTriangularFaces)
                            {
                                delete m_IsodensityTriangularFace;
                            }
                            m_IsodensityTriangularFaces.clear();
                        }
                    }

                    void CIsodensitySurface3D::CreateIsodensityVertices()
                    {
                        const std::list<DiscreteTopology::CDensityCellLink3D*>& DensityCellLinks = m_pDensityCellGrid->GetDensityCellLinks();
                        for (auto pDensityCellLink : DensityCellLinks)
                        {
                            CIsodensityVertex3D* pIsodensityVertex = new CIsodensityVertex3D(m_IsodensityVertices.size(), pDensityCellLink->GetPoint(), pDensityCellLink->GetNormal(), pDensityCellLink->GetDensity());
                            pDensityCellLink->SetUserData(pIsodensityVertex);
                            m_IsodensityVertices.push_back(pIsodensityVertex);
                        }
                    }

                    void CIsodensitySurface3D::CreateIsodensityFaces()
                    {
                        const int CellsWidth = m_pDensityCellGrid->GetCellsWidth();
                        const int CellsHeight = m_pDensityCellGrid->GetCellsHeight();
                        const int CellsDepth = m_pDensityCellGrid->GetCellsDepth();
                        const int CellsSlide = m_pDensityCellGrid->GetCellsSlide();
                        const int SubCellsWidth = CellsWidth - 1;
                        const int SubCellsHeight = CellsHeight - 1;
                        const int SubCellsDepth = CellsDepth - 1;
                        const int Deltas[8] = { 0, 1, 1 + CellsSlide, CellsSlide, CellsWidth, 1 + CellsWidth, 1 + CellsWidth + CellsSlide, CellsWidth + CellsSlide };
                        DiscreteTopology::CDensityCell3D* pDensityCell = m_pDensityCellGrid->GetWritableCells();
                        for (int Z = 0 ; Z < SubCellsDepth ; ++Z, pDensityCell += CellsWidth)
                            for (int Y = 0 ; Y < SubCellsHeight ; ++Y, ++pDensityCell)
                                for (int X = 0 ; X < SubCellsWidth ; ++X, ++pDensityCell)
                                {
                                    int LUT_Index = pDensityCell->IsInner();
                                    for (int i = 1 ; i < 8 ; ++i)
                                        if ((pDensityCell + Deltas[i])->IsInner())
                                        {
                                            LUT_Index |= (0X1 << i);
                                        }
                                    if (LUT_Index && (LUT_Index != 0XFF))
                                    {
                                        CMarchingCubes::Configuration::const_iterator EndTriangleFaces = m_pMarchingCubesConfigurationLUT[LUT_Index].end();
                                        for (CMarchingCubes::Configuration::const_iterator pTriangleVertexDeltas = m_pMarchingCubesConfigurationLUT[LUT_Index].begin() ; pTriangleVertexDeltas != EndTriangleFaces ; ++pTriangleVertexDeltas)
                                        {
                                            const CIsodensityVertex3D* pIsodensityVertexA = (const CIsodensityVertex3D*)((pDensityCell + Deltas[pTriangleVertexDeltas->m_A0])->GetLink(pDensityCell + Deltas[pTriangleVertexDeltas->m_A1])->GetUserData());
                                            const CIsodensityVertex3D* pIsodensityVertexB = (const CIsodensityVertex3D*)((pDensityCell + Deltas[pTriangleVertexDeltas->m_B0])->GetLink(pDensityCell + Deltas[pTriangleVertexDeltas->m_B1])->GetUserData());
                                            const CIsodensityVertex3D* pIsodensityVertexC = (const CIsodensityVertex3D*)((pDensityCell + Deltas[pTriangleVertexDeltas->m_C0])->GetLink(pDensityCell + Deltas[pTriangleVertexDeltas->m_C1])->GetUserData());
                                            m_IsodensityTriangularFaces.push_back(new CIsodensityTriangularFace3D(pIsodensityVertexA, pIsodensityVertexB, pIsodensityVertexC));
                                        }
                                    }
                                }
                    }
                }
            }
        }
    }
}
