/*
 * IsodensityVertex3D.h
 */

#pragma once

#include "../DensityPoint3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Isodensity
                {
                    class CIsodensityVertex3D : public CDensityPoint3D
                    {
                    public:

                        CIsodensityVertex3D(const int Index, const CVector3D& Point, const CVector3D& Normal, const Real Density);
                        ~CIsodensityVertex3D();
                        int GetIndex() const;
                        const CVector3D& GetNormal() const;

                    protected:

                        const int m_Index;
                        CVector3D m_Normal;
                    };
                }
            }
        }
    }
}

