/*
 * MarchingCubes.h
 */

#pragma once

#include "../../../../Common/Includes.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Isodensity
                {
                    class CMarchingCubes
                    {
                    public:

                        struct TriangleVertexDeltas
                        {
                            int m_A0;
                            int m_A1;
                            int m_B0;
                            int m_B1;
                            int m_C0;
                            int m_C1;
                        };

                        typedef std::list<TriangleVertexDeltas> Configuration;

                        static bool IsConfigurationLUTLoaded();
                        static void LoadConfigurationLUT();
                        static const Configuration* GetConfigurationLUT();

                    protected:

                        static bool s_IsConfigurationLUTLoaded;
                        static Configuration s_ConfigurationLUT[256];

                        CMarchingCubes();
                        virtual ~CMarchingCubes();
                    };
                }
            }
        }
    }
}

