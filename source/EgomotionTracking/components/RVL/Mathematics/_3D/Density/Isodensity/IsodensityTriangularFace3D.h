/*
 * IsodensityTriangularFace3D.h
 */

#pragma once

#include "IsodensityVertex3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Isodensity
                {
                    class CIsodensityTriangularFace3D
                    {
                    public:

                        CIsodensityTriangularFace3D(const CIsodensityVertex3D* pIsodensityVertexA, const CIsodensityVertex3D* pIsodensityVertexB, const CIsodensityVertex3D* pIsodensityVertexC);
                        virtual ~CIsodensityTriangularFace3D();

                        int GetIndexIsodensityVertexA() const;
                        int GetIndexIsodensityVertexB() const;
                        int GetIndexIsodensityVertexC() const;
                        const CIsodensityVertex3D* GetIsodensityVertexA() const;
                        const CIsodensityVertex3D* GetIsodensityVertexB() const;
                        const CIsodensityVertex3D* GetIsodensityVertexC() const;
                        const CVector3D& GetNorma() const;
                        Real GetArea() const;
                        CVector3D GetAreaWeightedNorma() const;

                    protected:

                        const CIsodensityVertex3D* m_pIsodensityVertexA;
                        const CIsodensityVertex3D* m_pIsodensityVertexB;
                        const CIsodensityVertex3D* m_pIsodensityVertexC;
                        CVector3D m_Normal;
                        Real m_Area;
                    };
                }
            }
        }
    }
}

