/*
 * DensityComposition3D.cpp
 */

#include "DensityComposition3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                CDensityComposition3D::CDensityComposition3D(const bool Ownership) :
                    m_Ownership(Ownership)
                {
                }

                CDensityComposition3D::~CDensityComposition3D()
                {
                    ClearPrimitiveSet();
                }

                Real CDensityComposition3D::GetDensity(const CVector3D& X, const ConditioningMode Mode) const
                {
                    if (m_PrimitiveSet.size())
                        switch (Mode)
                        {
                            case eConjunctive:
                            {
                                Real Density = Real(1);
                                for (auto ppDensityPrimitive3D : m_PrimitiveSet)
                                {
                                    Density *= ppDensityPrimitive3D->GetDensity(X);
                                }
                                return Density;
                            }
                            case eDisjunctive:
                            {
                                Real Density = Real(0);
                                for (auto ppDensityPrimitive3D : m_PrimitiveSet)
                                {
                                    Density += ppDensityPrimitive3D->GetDensity(X);
                                }
                                return Density;
                            }
                        }
                    return Real(0);
                }

                Real CDensityComposition3D::GetLogDensity(const CVector3D& X, const ConditioningMode Mode) const
                {
                    if (m_PrimitiveSet.size())
                        switch (Mode)
                        {
                            case eConjunctive:
                            {
                                Real LogDensity = Real(0);
                                for (auto ppDensityPrimitive3D : m_PrimitiveSet)
                                {
                                    LogDensity += ppDensityPrimitive3D->GetLogDensity(X);
                                }
                                return LogDensity;
                            }
                            case eDisjunctive:
                            {
                                Real Density = Real(0);
                                for (auto ppDensityPrimitive3D : m_PrimitiveSet)
                                {
                                    Density += ppDensityPrimitive3D->GetDensity(X);
                                }
                                return std::log(Density);
                            }
                        }
                    return g_RealMinusInfinity;
                }

                CVector3D CDensityComposition3D::GetDensityGradientFullSampling(const CVector3D& X, const Real SamplingDelta, const ConditioningMode Mode, Real* pDensity) const
                {
                    if (m_PrimitiveSet.size())
                        switch (Mode)
                        {
                            case eConjunctive:
                            {
                                CVector3D DensityGradient;
                                Real ConjunctiveDensity = Real(1);
                                Real Density = Real(1);
                                for (auto ppDensityPrimitive3D : m_PrimitiveSet)
                                {
                                    DensityGradient += ppDensityPrimitive3D->GetDensityGradientFullSampling(X, SamplingDelta, &Density) * Density;
                                    ConjunctiveDensity *= Density;
                                }
                                if (pDensity)
                                {
                                    *pDensity = ConjunctiveDensity;
                                }
                                DensityGradient.Normalize();
                                return DensityGradient;
                            }
                            case eDisjunctive:
                            {
                                CVector3D DensityGradient;
                                Real DisjunctiveDensity = Real(1);
                                Real Density = Real(1);
                                for (auto ppDensityPrimitive3D : m_PrimitiveSet)
                                {
                                    DensityGradient += ppDensityPrimitive3D->GetDensityGradientFullSampling(X, SamplingDelta, &Density) * Density;
                                    DisjunctiveDensity += Density;
                                }
                                if (pDensity)
                                {
                                    *pDensity = DisjunctiveDensity;
                                }
                                DensityGradient.Normalize();
                                return DensityGradient;
                            }
                        }
                    return CVector3D::s_Zero;
                }

                CVector3D CDensityComposition3D::GetDensityGradientPartialSampling(const CVector3D& X, const Real SamplingDelta, const ConditioningMode Mode, Real* pDensity) const
                {
                    if (m_PrimitiveSet.size())
                        switch (Mode)
                        {
                            case eConjunctive:
                            {
                                CVector3D DensityGradient;
                                Real ConjunctiveDensity = Real(1);
                                Real Density = Real(1);
                                for (auto ppDensityPrimitive3D : m_PrimitiveSet)
                                {
                                    DensityGradient += ppDensityPrimitive3D->GetDensityGradientPartialSampling(X, SamplingDelta, &Density) * Density;
                                    ConjunctiveDensity *= Density;
                                }
                                if (pDensity)
                                {
                                    *pDensity = ConjunctiveDensity;
                                }
                                DensityGradient.Normalize();
                                return DensityGradient;
                            }
                            case eDisjunctive:
                            {
                                CVector3D DensityGradient;
                                Real DisjunctiveDensity = Real(1);
                                Real Density = Real(1);
                                for (auto ppDensityPrimitive3D : m_PrimitiveSet)
                                {
                                    DensityGradient += ppDensityPrimitive3D->GetDensityGradientPartialSampling(X, SamplingDelta, &Density) * Density;
                                    DisjunctiveDensity += Density;
                                }
                                if (pDensity)
                                {
                                    *pDensity = DisjunctiveDensity;
                                }
                                DensityGradient.Normalize();
                                return DensityGradient;
                            }
                        }
                    return CVector3D::s_Zero;
                }

                Containers::_3D::CContinuousBoundingBox3D CDensityComposition3D::GetDensityBoundingBox(const Real MinimalDensity, const ConditioningMode Mode) const
                {
                    if (m_PrimitiveSet.size())
                        switch (Mode)
                        {
                            case eConjunctive:
                            {
                                std::set<const CDensityPrimitive3D*>::const_iterator ppDensityPrimitive3D = m_PrimitiveSet.begin();
                                Containers::_3D::CContinuousBoundingBox3D AggregationBoundingBox = (*ppDensityPrimitive3D)->GetDensityBoundingBox(MinimalDensity);
                                while (++ppDensityPrimitive3D != m_PrimitiveSet.end())
                                    if (!AggregationBoundingBox.GetIntersection((*ppDensityPrimitive3D)->GetDensityBoundingBox(MinimalDensity), AggregationBoundingBox))
                                    {
                                        break;
                                    }
                                return AggregationBoundingBox;
                            }
                            case eDisjunctive:
                            {
                                Containers::_3D::CContinuousBoundingBox3D AggregationBoundingBox;
                                for (auto ppDensityPrimitive3D : m_PrimitiveSet)
                                {
                                    AggregationBoundingBox.Extend(ppDensityPrimitive3D->GetDensityBoundingBox(MinimalDensity));
                                }
                                return AggregationBoundingBox;
                            }
                        }
                    return Containers::_3D::CContinuousBoundingBox3D();
                }

                bool CDensityComposition3D::AddDensityPrimitive(const CDensityPrimitive3D* pDensityPrimitive3D)
                {
                    if (pDensityPrimitive3D && (m_PrimitiveSet.find(pDensityPrimitive3D) == m_PrimitiveSet.end()))
                    {
                        m_PrimitiveSet.insert(pDensityPrimitive3D);
                        return true;
                    }
                    return false;
                }

                void CDensityComposition3D::ClearPrimitiveSet()
                {
                    if (m_Ownership)
                        for (auto ppDensityPrimitive3D : m_PrimitiveSet)
                        {
                            delete ppDensityPrimitive3D;
                        }
                    m_PrimitiveSet.clear();
                }

                int CDensityComposition3D::GetTotalPrimitives() const
                {
                    return m_PrimitiveSet.size();
                }
            }
        }
    }
}
