/*
 * DensityPrimitive3D.h
 */

#pragma once

#include "../../../Common/DataTypes.h"
#include "../../../Containers/_3D/ContinuousBoundingBox3D.h"
#include "../Vector3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                enum DensityProfileTypeId
                {
                    eFreeForm, eGaussian
                };

                enum ConditioningMode
                {
                    eConjunctive, eDisjunctive
                };

                enum ShapeTypeId
                {
                    eGeometricPrimitive3D
                };

                class CDensityPrimitive3D
                {
                public:

                    CDensityPrimitive3D(const ShapeTypeId ShapeId, const DensityProfileTypeId ProfileId);
                    virtual ~CDensityPrimitive3D();

                    ShapeTypeId GetShapeTypeId() const;
                    DensityProfileTypeId GetDensityProfileTypeId() const;

                    virtual Real GetDensity(const CVector3D& X) const = 0;
                    virtual Real GetLogDensity(const CVector3D& X) const = 0;
                    virtual CVector3D GetDensityGradientFullSampling(const CVector3D& X, const Real SamplingDelta, Real* pDensity) const = 0;
                    virtual CVector3D GetDensityGradientPartialSampling(const CVector3D& X, const Real SamplingDelta, Real* pDensity) const = 0;
                    virtual Containers::_3D::CContinuousBoundingBox3D GetDensityBoundingBox(const Real MinimalDensity) const = 0;

                protected:

                    const ShapeTypeId m_ShapeTypeId;
                    const DensityProfileTypeId m_DensityProfileTypeId;
                };
            }
        }
    }
}

