/*
 * DensityPoint3D.h
 */

#pragma once

#include "../../../Common/DataTypes.h"
#include "../Vector3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                class CDensityPoint3D
                {
                public:

                    CDensityPoint3D();
                    CDensityPoint3D(const CDensityPoint3D& DensityPoint3D);
                    CDensityPoint3D(const CVector3D& Point, const Real Density);
                    ~CDensityPoint3D();

                    void SetValues(const CVector3D& Location, const Real Density);
                    void SetDensity(const Real Density);
                    void Clear();

                    const CVector3D& GetPoint() const;
                    Real GetDensity() const;
                    bool IsValid() const;

                protected:

                    CVector3D m_Point;
                    Real m_Density;
                };
            }
        }
    }
}

