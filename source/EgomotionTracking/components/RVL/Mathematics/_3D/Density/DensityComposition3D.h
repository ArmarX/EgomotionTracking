/*
 * DensityComposition3D.h
 */

#pragma once

#include "../../../Common/DataTypes.h"
#include "../Vector3D.h"
#include "DensityPrimitive3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                class CDensityComposition3D
                {
                public:

                    CDensityComposition3D(const bool Ownership);
                    virtual ~CDensityComposition3D();
                    virtual Real GetDensity(const CVector3D& X, const ConditioningMode Mode) const;
                    virtual Real GetLogDensity(const CVector3D& X, const ConditioningMode Mode) const;
                    virtual CVector3D GetDensityGradientFullSampling(const CVector3D& X, const Real SamplingDelta, const ConditioningMode Mode, Real* pDensity) const;
                    virtual CVector3D GetDensityGradientPartialSampling(const CVector3D& X, const Real SamplingDelta, const ConditioningMode Mode, Real* pDensity) const;
                    virtual Containers::_3D::CContinuousBoundingBox3D GetDensityBoundingBox(const Real MinimalDensity, const ConditioningMode Mode) const;
                    bool AddDensityPrimitive(const CDensityPrimitive3D* pDensityPrimitive3D);
                    void ClearPrimitiveSet();
                    int GetTotalPrimitives() const;

                protected:

                    bool m_Ownership;
                    std::set<const CDensityPrimitive3D*> m_PrimitiveSet;

                };
            }
        }
    }
}

