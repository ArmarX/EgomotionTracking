/*
 * GaussianDensitySphere3D.cpp
 */

#include "GaussianDensitySphere3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Geometric
                {
                    namespace GaussianProfile
                    {

                        CGaussianDensitySphere3D::CGaussianDensitySphere3D(const CVector3D& Center, const Real Radius, const Real StandarDeviation) :
                            CDensitySphere3D(eGaussian, Center, Radius),
                            m_StandarDeviation(StandarDeviation),
                            m_ExponentFactor(_1D::CNormalDistribution::DetermineExponentFactor(StandarDeviation))
                        {
                        }

                        CGaussianDensitySphere3D::~CGaussianDensitySphere3D()
                            = default;

                        bool CGaussianDensitySphere3D::SetStandarDeviation(const Real StandarDeviation)
                        {
                            if (IsPositive(StandarDeviation))
                            {
                                m_StandarDeviation = StandarDeviation;
                                m_ExponentFactor = _1D::CNormalDistribution::DetermineExponentFactor(StandarDeviation);
                                return true;
                            }
                            return false;
                        }

                        Real CGaussianDensitySphere3D::GetStandarDeviation() const
                        {
                            return m_StandarDeviation;
                        }

                        Real CGaussianDensitySphere3D::GetDensity(const CVector3D& X) const
                        {
                            const Real OrientedDistanceToSurface = Geometry::CSphere3D::GetOrientedDistanceToSurface(X);
                            return std::exp(m_ExponentFactor * OrientedDistanceToSurface * OrientedDistanceToSurface);
                        }

                        Real CGaussianDensitySphere3D::GetLogDensity(const CVector3D& X) const
                        {
                            const Real OrientedDistanceToSurface = Geometry::CSphere3D::GetOrientedDistanceToSurface(X);
                            return m_ExponentFactor * OrientedDistanceToSurface * OrientedDistanceToSurface;
                        }

                        CVector3D CGaussianDensitySphere3D::GetDensityGradientFullSampling(const CVector3D& X, const Real /*SamplingDelta*/, Real* pDensity) const
                        {
                            const CVector3D Delta = X - Geometry::CSphere3D::GetCenter();
                            const Real Lenght = Delta.GetLength();
                            if (pDensity)
                            {
                                const Real Deviation = Lenght - Geometry::CSphere3D::GetRadius();
                                *pDensity = std::exp(m_ExponentFactor * Deviation * Deviation);
                            }
                            return Delta * ((Geometry::CSphere3D::GetRadius() - Lenght) / Lenght);
                        }

                        CVector3D CGaussianDensitySphere3D::GetDensityGradientPartialSampling(const CVector3D& X, const Real /*SamplingDelta*/, Real* pDensity) const
                        {
                            const CVector3D Delta = X - Geometry::CSphere3D::GetCenter();
                            const Real Lenght = Delta.GetLength();
                            if (pDensity)
                            {
                                const Real Deviation = Lenght - Geometry::CSphere3D::GetRadius();
                                *pDensity = std::exp(m_ExponentFactor * Deviation * Deviation);
                            }
                            return Delta * ((Geometry::CSphere3D::GetRadius() - Lenght) / Lenght);
                        }

                        Containers::_3D::CContinuousBoundingBox3D CGaussianDensitySphere3D::GetDensityBoundingBox(const Real MinimalDensity) const
                        {
                            const Real Deviation = _1D::CNormalDistribution::DetermineDeviationAtDensity(MinimalDensity, m_StandarDeviation);
                            const CVector3D Delta(Deviation, Deviation, Deviation);
                            return Containers::_3D::CContinuousBoundingBox3D(Geometry::CSphere3D::GetCenter() - Delta, Geometry::CSphere3D::GetCenter() + Delta);
                        }
                    }
                }
            }
        }
    }
}
