/*
 * DensitySphere3D.cpp
 */

#include "DensitySphere3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Geometric
                {
                    CDensitySphere3D::CDensitySphere3D(const DensityProfileTypeId ProfileTypeId, const CVector3D& Center, const Real Radius) :
                        CDensityPrimitive3D(eGeometricPrimitive3D, ProfileTypeId),
                        Geometry::CSphere3D(Center, Radius, nullptr, nullptr)
                    {
                    }

                    CDensitySphere3D::~CDensitySphere3D()
                        = default;
                }
            }
        }
    }
}
