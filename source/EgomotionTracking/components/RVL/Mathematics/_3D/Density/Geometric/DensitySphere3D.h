/*
 * DensitySphere3D.h
 */

#pragma once

#include "../../Geometry/Sphere3D.h"
#include "../DensityPrimitive3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Geometric
                {
                    class CDensitySphere3D : public CDensityPrimitive3D, public Geometry::CSphere3D
                    {
                    public:

                        CDensitySphere3D(const DensityProfileTypeId ProfileTypeId, const CVector3D& Center, const Real Radius);
                        ~CDensitySphere3D() override;
                    };
                }
            }
        }
    }
}

