/*
 * GaussianDensitySphere3D.h
 */

#pragma once

#include "../../../../_1D/NormalDistribution.h"
#include "../DensitySphere3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Geometric
                {
                    namespace GaussianProfile
                    {
                        class CGaussianDensitySphere3D : public CDensitySphere3D
                        {
                        public:

                            CGaussianDensitySphere3D(const CVector3D& Center, const Real Radius, const Real StandarDeviation);
                            ~CGaussianDensitySphere3D() override;

                            bool SetStandarDeviation(const Real StandarDeviation);
                            Real GetStandarDeviation() const;

                            Real GetDensity(const CVector3D& X) const override;
                            Real GetLogDensity(const CVector3D& X) const override;
                            CVector3D GetDensityGradientFullSampling(const CVector3D& X, const Real /*SamplingDelta*/, Real* pDensity) const override;
                            CVector3D GetDensityGradientPartialSampling(const CVector3D& X, const Real /*SamplingDelta*/, Real* pDensity) const override;
                            Containers::_3D::CContinuousBoundingBox3D GetDensityBoundingBox(const Real MinimalDensity) const override;

                        protected:

                            Real m_StandarDeviation;
                            Real m_ExponentFactor;
                        };
                    }
                }
            }
        }
    }
}

