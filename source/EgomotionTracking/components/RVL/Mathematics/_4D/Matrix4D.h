/*
 * Matrix4D.h
 */

#pragma once

#include "../../Common/DataTypes.h"
#include "../_3D/Vector3D.h"
#include "../_3D/Matrix3D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _4D
        {
            class CMatrix4D
            {
            public:

                static const CMatrix4D s_Identity;

                CMatrix4D();
                CMatrix4D(const Real E00, const Real E01, const Real E02, const Real E03, const Real E10, const Real E11, const Real E12, const Real E13, const Real E20, const Real E21, const Real E22, const Real E23, const Real E30, const Real E31, const Real E32, const Real E33);
                CMatrix4D(const CMatrix4D& Matrix);
                CMatrix4D(const CMatrix4D* pMatrix);
                CMatrix4D(const Mathematics::_3D::CMatrix3D& Rotation, const Mathematics::_3D::CVector3D& Translation, const Real Scale);
                ~CMatrix4D();

                void MakeIdentity();
                Real GetDeterminat() const;
                CMatrix4D GetInverse(bool* pSingularity) const;
                CMatrix4D GetTranspose() const;
                void SetByElements(const Real E00, const Real E01, const Real E02, const Real E03, const Real E10, const Real E11, const Real E12, const Real E13, const Real E20, const Real E21, const Real E22, const Real E23, const Real E30, const Real E31, const Real E32, const Real E33);
                bool SetTransformation(const Mathematics::_3D::CMatrix3D& Rotation, const Mathematics::_3D::CVector3D& Translation, const Real Scale);
                void SetTranslation(const Mathematics::_3D::CVector3D& Translation);
                void AddTranslation(const Mathematics::_3D::CVector3D& Translation);
                Real* operator[](const int Row);
                const Real* operator[](const int Row) const;
                void operator=(const CMatrix4D& Matrix);
                void operator*=(const CMatrix4D& Matrix);
                CMatrix4D operator*(const CMatrix4D& Matrix) const;

                void Transform(const Mathematics::_3D::CVector3D& Point, Mathematics::_3D::CVector3D& MappedPoint) const;
                void RigidTransform(const Mathematics::_3D::CVector3D& Point, Mathematics::_3D::CVector3D& MappedPoint) const;

                _3D::CVector3D Transform(const Mathematics::_3D::CVector3D& Point) const;
                _3D::CVector3D RigidTransform(const Mathematics::_3D::CVector3D& Point) const;

                void Rotation(const Mathematics::_3D::CVector3D& Point, Mathematics::_3D::CVector3D& MappedPoint) const;
                _3D::CVector3D Rotation(const Mathematics::_3D::CVector3D& Point) const;

                CMatrix4D operator*(const Real Scalar);
                bool operator==(const CMatrix4D& Matrix) const;
                bool operator!=(const CMatrix4D& Matrix) const;
                const std::string ToString() const;
                bool IsIsometry() const;
                bool IsOrthonormal() const;
                bool IsUnitary() const;

                Mathematics::_3D::CMatrix3D GetSubRotationMatrix() const;

            protected:

                union MatrixStructure4D
                {
                    Real m_Array[16];
                    Real m_Elements[4][4];
                };
                MatrixStructure4D m_Matrix4D;
            };
        }
    }
}

