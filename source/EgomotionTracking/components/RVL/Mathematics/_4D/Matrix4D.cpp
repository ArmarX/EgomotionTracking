/*
 * Matric4D.cpp
 */

#include "Matrix4D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _4D
        {
            const CMatrix4D CMatrix4D::s_Identity(Real(1), Real(0), Real(0), Real(0), Real(0), Real(1), Real(0), Real(0), Real(0), Real(0), Real(1), Real(0), Real(0), Real(0), Real(0), Real(1));

            CMatrix4D::CMatrix4D()
            {
                memset(&m_Matrix4D, 0, sizeof(MatrixStructure4D));
            }

            CMatrix4D::CMatrix4D(const Real E00, const Real E01, const Real E02, const Real E03, const Real E10, const Real E11, const Real E12, const Real E13, const Real E20, const Real E21, const Real E22, const Real E23, const Real E30, const Real E31, const Real E32, const Real E33)
            {
                m_Matrix4D.m_Elements[0][0] = E00;
                m_Matrix4D.m_Elements[0][1] = E01;
                m_Matrix4D.m_Elements[0][2] = E02;
                m_Matrix4D.m_Elements[0][3] = E03;
                m_Matrix4D.m_Elements[1][0] = E10;
                m_Matrix4D.m_Elements[1][1] = E11;
                m_Matrix4D.m_Elements[1][2] = E12;
                m_Matrix4D.m_Elements[1][3] = E13;
                m_Matrix4D.m_Elements[2][0] = E20;
                m_Matrix4D.m_Elements[2][1] = E21;
                m_Matrix4D.m_Elements[2][2] = E22;
                m_Matrix4D.m_Elements[2][3] = E23;
                m_Matrix4D.m_Elements[3][0] = E30;
                m_Matrix4D.m_Elements[3][1] = E31;
                m_Matrix4D.m_Elements[3][2] = E32;
                m_Matrix4D.m_Elements[3][3] = E33;
            }

            CMatrix4D::CMatrix4D(const CMatrix4D& Matrix)
            {
                memcpy(&m_Matrix4D, &Matrix.m_Matrix4D, sizeof(MatrixStructure4D));
            }

            CMatrix4D::CMatrix4D(const CMatrix4D* pMatrix)
            {
                if (pMatrix)
                {
                    memcpy(&m_Matrix4D, &(pMatrix->m_Matrix4D), sizeof(MatrixStructure4D));
                }
                else
                {
                    memset(&m_Matrix4D, 0, sizeof(MatrixStructure4D));
                }
            }

            CMatrix4D::CMatrix4D(const Mathematics::_3D::CMatrix3D& Rotation, const Mathematics::_3D::CVector3D& Translation, const Real Scale)
            {
                SetTransformation(Rotation, Translation, Scale);
            }

            CMatrix4D::~CMatrix4D()
                = default;

            void CMatrix4D::MakeIdentity()
            {
                m_Matrix4D = s_Identity.m_Matrix4D;
            }

            Real CMatrix4D::GetDeterminat() const
            {
                return (m_Matrix4D.m_Elements[0][3] * m_Matrix4D.m_Elements[1][2] * m_Matrix4D.m_Elements[2][1] * m_Matrix4D.m_Elements[3][0] - m_Matrix4D.m_Elements[0][2] * m_Matrix4D.m_Elements[1][3] * m_Matrix4D.m_Elements[2][1] * m_Matrix4D.m_Elements[3][0] - m_Matrix4D.m_Elements[0][3] * m_Matrix4D.m_Elements[1][0] * m_Matrix4D.m_Elements[2][2] * m_Matrix4D.m_Elements[3][0] + m_Matrix4D.m_Elements[0][1] * m_Matrix4D.m_Elements[1][3] * m_Matrix4D.m_Elements[2][2] * m_Matrix4D.m_Elements[3][0] + m_Matrix4D.m_Elements[0][2] * m_Matrix4D.m_Elements[1][0] * m_Matrix4D.m_Elements[2][3] * m_Matrix4D.m_Elements[3][0] - m_Matrix4D.m_Elements[0][1] * m_Matrix4D.m_Elements[1][2] * m_Matrix4D.m_Elements[2][3] * m_Matrix4D.m_Elements[3][0] - m_Matrix4D.m_Elements[0][3] * m_Matrix4D.m_Elements[1][2] * m_Matrix4D.m_Elements[2][0] * m_Matrix4D.m_Elements[3][1] + m_Matrix4D.m_Elements[0][2] * m_Matrix4D.m_Elements[1][3] * m_Matrix4D.m_Elements[2][0] * m_Matrix4D.m_Elements[3][1] + m_Matrix4D.m_Elements[0][3] * m_Matrix4D.m_Elements[1][0] * m_Matrix4D.m_Elements[2][2] * m_Matrix4D.m_Elements[3][1] - m_Matrix4D.m_Elements[0][0] * m_Matrix4D.m_Elements[1][3] * m_Matrix4D.m_Elements[2][2] * m_Matrix4D.m_Elements[3][1] - m_Matrix4D.m_Elements[0][2] * m_Matrix4D.m_Elements[1][0] * m_Matrix4D.m_Elements[2][3] * m_Matrix4D.m_Elements[3][1] + m_Matrix4D.m_Elements[0][0] * m_Matrix4D.m_Elements[1][2] * m_Matrix4D.m_Elements[2][3] * m_Matrix4D.m_Elements[3][1] + m_Matrix4D.m_Elements[0][3] * m_Matrix4D.m_Elements[1][0] * m_Matrix4D.m_Elements[2][0] * m_Matrix4D.m_Elements[3][2] - m_Matrix4D.m_Elements[0][1] * m_Matrix4D.m_Elements[1][3] * m_Matrix4D.m_Elements[2][0] * m_Matrix4D.m_Elements[3][2] - m_Matrix4D.m_Elements[0][3] * m_Matrix4D.m_Elements[1][0] * m_Matrix4D.m_Elements[2][1] * m_Matrix4D.m_Elements[3][2] + m_Matrix4D.m_Elements[0][0] * m_Matrix4D.m_Elements[1][3] * m_Matrix4D.m_Elements[2][1] * m_Matrix4D.m_Elements[3][2] + m_Matrix4D.m_Elements[0][1] * m_Matrix4D.m_Elements[1][0] * m_Matrix4D.m_Elements[2][3] * m_Matrix4D.m_Elements[3][2] - m_Matrix4D.m_Elements[0][0] * m_Matrix4D.m_Elements[1][0] * m_Matrix4D.m_Elements[2][3] * m_Matrix4D.m_Elements[3][2] - m_Matrix4D.m_Elements[0][2] * m_Matrix4D.m_Elements[1][0] * m_Matrix4D.m_Elements[2][0] * m_Matrix4D.m_Elements[3][3] + m_Matrix4D.m_Elements[0][1] * m_Matrix4D.m_Elements[1][2] * m_Matrix4D.m_Elements[2][0] * m_Matrix4D.m_Elements[3][3] + m_Matrix4D.m_Elements[0][2] * m_Matrix4D.m_Elements[1][0] * m_Matrix4D.m_Elements[2][1] * m_Matrix4D.m_Elements[3][3] - m_Matrix4D.m_Elements[0][0] * m_Matrix4D.m_Elements[1][2] * m_Matrix4D.m_Elements[2][1] * m_Matrix4D.m_Elements[3][3] - m_Matrix4D.m_Elements[0][1] * m_Matrix4D.m_Elements[1][0] * m_Matrix4D.m_Elements[2][2] * m_Matrix4D.m_Elements[3][3] + m_Matrix4D.m_Elements[0][0] * m_Matrix4D.m_Elements[1][0] * m_Matrix4D.m_Elements[2][2] * m_Matrix4D.m_Elements[3][3]);
            }

            CMatrix4D CMatrix4D::GetInverse(bool* pSingularity) const
            {
                CMatrix4D InverseMatrix;
                InverseMatrix.m_Matrix4D.m_Array[0] = m_Matrix4D.m_Array[5] * m_Matrix4D.m_Array[10] * m_Matrix4D.m_Array[15] - m_Matrix4D.m_Array[5] * m_Matrix4D.m_Array[11] * m_Matrix4D.m_Array[14] - m_Matrix4D.m_Array[9] * m_Matrix4D.m_Array[6] * m_Matrix4D.m_Array[15] + m_Matrix4D.m_Array[9] * m_Matrix4D.m_Array[7] * m_Matrix4D.m_Array[14] + m_Matrix4D.m_Array[13] * m_Matrix4D.m_Array[6] * m_Matrix4D.m_Array[11] - m_Matrix4D.m_Array[13] * m_Matrix4D.m_Array[7] * m_Matrix4D.m_Array[10];
                InverseMatrix.m_Matrix4D.m_Array[1] = m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[11] * m_Matrix4D.m_Array[14] + m_Matrix4D.m_Array[9] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[15] - m_Matrix4D.m_Array[9] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[14] - m_Matrix4D.m_Array[13] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[11] + m_Matrix4D.m_Array[13] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[10] - m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[10] * m_Matrix4D.m_Array[15];
                InverseMatrix.m_Matrix4D.m_Array[2] = m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[6] * m_Matrix4D.m_Array[15] - m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[7] * m_Matrix4D.m_Array[14] - m_Matrix4D.m_Array[5] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[15] + m_Matrix4D.m_Array[5] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[14] + m_Matrix4D.m_Array[13] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[7] - m_Matrix4D.m_Array[13] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[6];
                InverseMatrix.m_Matrix4D.m_Array[3] = m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[7] * m_Matrix4D.m_Array[10] + m_Matrix4D.m_Array[5] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[11] - m_Matrix4D.m_Array[5] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[10] - m_Matrix4D.m_Array[9] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[7] + m_Matrix4D.m_Array[9] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[6] - m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[6] * m_Matrix4D.m_Array[11];
                InverseMatrix.m_Matrix4D.m_Array[4] = m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[11] * m_Matrix4D.m_Array[14] + m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[6] * m_Matrix4D.m_Array[15] - m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[7] * m_Matrix4D.m_Array[14] - m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[6] * m_Matrix4D.m_Array[11] + m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[7] * m_Matrix4D.m_Array[10] - m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[10] * m_Matrix4D.m_Array[15];
                InverseMatrix.m_Matrix4D.m_Array[5] = m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[10] * m_Matrix4D.m_Array[15] - m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[11] * m_Matrix4D.m_Array[14] - m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[15] + m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[14] + m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[11] - m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[10];
                InverseMatrix.m_Matrix4D.m_Array[6] = m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[7] * m_Matrix4D.m_Array[14] + m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[15] - m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[14] - m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[7] + m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[6] - m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[6] * m_Matrix4D.m_Array[15];
                InverseMatrix.m_Matrix4D.m_Array[7] = m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[6] * m_Matrix4D.m_Array[11] - m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[7] * m_Matrix4D.m_Array[10] - m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[11] + m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[10] + m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[7] - m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[6];
                InverseMatrix.m_Matrix4D.m_Array[8] = m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[9] * m_Matrix4D.m_Array[15] - m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[11] * m_Matrix4D.m_Array[13] - m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[5] * m_Matrix4D.m_Array[15] + m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[7] * m_Matrix4D.m_Array[13] + m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[5] * m_Matrix4D.m_Array[11] - m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[7] * m_Matrix4D.m_Array[9];
                InverseMatrix.m_Matrix4D.m_Array[9] = m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[11] * m_Matrix4D.m_Array[13] + m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[15] - m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[13] - m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[11] + m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[9] - m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[9] * m_Matrix4D.m_Array[15];
                InverseMatrix.m_Matrix4D.m_Array[10] = m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[5] * m_Matrix4D.m_Array[15] - m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[7] * m_Matrix4D.m_Array[13] - m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[15] + m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[13] + m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[7] - m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[5];
                InverseMatrix.m_Matrix4D.m_Array[11] = m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[7] * m_Matrix4D.m_Array[9] + m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[11] - m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[9] - m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[7] + m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[3] * m_Matrix4D.m_Array[5] - m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[5] * m_Matrix4D.m_Array[11];
                InverseMatrix.m_Matrix4D.m_Array[12] = m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[10] * m_Matrix4D.m_Array[13] + m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[5] * m_Matrix4D.m_Array[14] - m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[6] * m_Matrix4D.m_Array[13] - m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[5] * m_Matrix4D.m_Array[10] + m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[6] * m_Matrix4D.m_Array[9] - m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[9] * m_Matrix4D.m_Array[14];
                InverseMatrix.m_Matrix4D.m_Array[13] = m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[9] * m_Matrix4D.m_Array[14] - m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[10] * m_Matrix4D.m_Array[13] - m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[14] + m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[13] + m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[10] - m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[9];
                InverseMatrix.m_Matrix4D.m_Array[14] = m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[6] * m_Matrix4D.m_Array[13] + m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[14] - m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[13] - m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[6] + m_Matrix4D.m_Array[12] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[5] - m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[5] * m_Matrix4D.m_Array[14];
                InverseMatrix.m_Matrix4D.m_Array[15] = m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[5] * m_Matrix4D.m_Array[10] - m_Matrix4D.m_Array[0] * m_Matrix4D.m_Array[6] * m_Matrix4D.m_Array[9] - m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[10] + m_Matrix4D.m_Array[4] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[9] + m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[1] * m_Matrix4D.m_Array[6] - m_Matrix4D.m_Array[8] * m_Matrix4D.m_Array[2] * m_Matrix4D.m_Array[5];
                const Real Determinant = m_Matrix4D.m_Array[0] * InverseMatrix.m_Matrix4D.m_Array[0] + m_Matrix4D.m_Array[1] * InverseMatrix.m_Matrix4D.m_Array[4] + m_Matrix4D.m_Array[2] * InverseMatrix.m_Matrix4D.m_Array[8] + m_Matrix4D.m_Array[3] * InverseMatrix.m_Matrix4D.m_Array[12];
                if (IsNonZero(Determinant))
                {
                    if (pSingularity)
                    {
                        *pSingularity = false;
                    }
                    const Real Normalization = Real(1) / Determinant;
                    for (double& i : InverseMatrix.m_Matrix4D.m_Array)
                    {
                        i *= Normalization;
                    }
                }
                else if (pSingularity)
                {
                    *pSingularity = true;
                }
                return InverseMatrix;
            }

            CMatrix4D CMatrix4D::GetTranspose() const
            {
                CMatrix4D TrasposedMatrix;
                for (int r = 0 ; r < 4 ; ++r)
                    for (int c = 0 ; c < 4 ; ++c)
                    {
                        TrasposedMatrix.m_Matrix4D.m_Elements[c][r] = m_Matrix4D.m_Elements[r][c];
                    }
                return TrasposedMatrix;
            }

            void CMatrix4D::SetByElements(const Real E00, const Real E01, const Real E02, const Real E03, const Real E10, const Real E11, const Real E12, const Real E13, const Real E20, const Real E21, const Real E22, const Real E23, const Real E30, const Real E31, const Real E32, const Real E33)
            {
                m_Matrix4D.m_Elements[0][0] = E00;
                m_Matrix4D.m_Elements[0][1] = E01;
                m_Matrix4D.m_Elements[0][2] = E02;
                m_Matrix4D.m_Elements[0][3] = E03;
                m_Matrix4D.m_Elements[1][0] = E10;
                m_Matrix4D.m_Elements[1][1] = E11;
                m_Matrix4D.m_Elements[1][2] = E12;
                m_Matrix4D.m_Elements[1][3] = E13;
                m_Matrix4D.m_Elements[2][0] = E20;
                m_Matrix4D.m_Elements[2][1] = E21;
                m_Matrix4D.m_Elements[2][2] = E22;
                m_Matrix4D.m_Elements[2][3] = E23;
                m_Matrix4D.m_Elements[3][0] = E30;
                m_Matrix4D.m_Elements[3][1] = E31;
                m_Matrix4D.m_Elements[3][2] = E32;
                m_Matrix4D.m_Elements[3][3] = E33;
            }

            bool CMatrix4D::SetTransformation(const Mathematics::_3D::CMatrix3D& Rotation, const Mathematics::_3D::CVector3D& Translation, const Real Scale)
            {
                if (Translation.IsNotAtInfinity() && IsZero(Rotation.GetDeterminat() - Real(1)))
                {
                    memset(&m_Matrix4D, 0, sizeof(MatrixStructure4D));
                    for (int r = 0 ; r < 3 ; ++r)
                        for (int c = 0 ; c < 3 ; ++c)
                        {
                            m_Matrix4D.m_Elements[r][c] = Rotation[r][c];
                        }
                    m_Matrix4D.m_Elements[0][3] = Translation.GetX();
                    m_Matrix4D.m_Elements[1][3] = Translation.GetY();
                    m_Matrix4D.m_Elements[2][3] = Translation.GetZ();
                    m_Matrix4D.m_Elements[3][3] = Scale; //Verify this, this should be a diagonal matrix affecting the whole matrix;
                    return true;
                }
                return false;
            }

            void CMatrix4D::SetTranslation(const Mathematics::_3D::CVector3D& Translation)
            {
                m_Matrix4D.m_Elements[0][3] = Translation.GetX();
                m_Matrix4D.m_Elements[1][3] = Translation.GetY();
                m_Matrix4D.m_Elements[2][3] = Translation.GetZ();
            }

            void CMatrix4D::AddTranslation(const Mathematics::_3D::CVector3D& Translation)
            {
                m_Matrix4D.m_Elements[0][3] += Translation.GetX();
                m_Matrix4D.m_Elements[1][3] += Translation.GetY();
                m_Matrix4D.m_Elements[2][3] += Translation.GetZ();
            }

            Real* CMatrix4D::operator[](const int Row)
            {
                return m_Matrix4D.m_Elements[Row];
            }

            const Real* CMatrix4D::operator[](const int Row) const
            {
                return m_Matrix4D.m_Elements[Row];
            }

            void CMatrix4D::operator=(const CMatrix4D& Matrix)
            {
                memcpy(&m_Matrix4D, &Matrix.m_Matrix4D, sizeof(MatrixStructure4D));
            }

            void CMatrix4D::operator*=(const CMatrix4D& Matrix)
            {
                MatrixStructure4D TemporalMatrixStructure;
                for (int r = 0 ; r < 4 ; ++r)
                    for (int c = 0 ; c < 4 ; ++c)
                    {
                        TemporalMatrixStructure.m_Elements[r][c] = m_Matrix4D.m_Elements[r][0] * Matrix.m_Matrix4D.m_Elements[0][c] + m_Matrix4D.m_Elements[r][1] * Matrix.m_Matrix4D.m_Elements[1][c] + m_Matrix4D.m_Elements[r][2] * Matrix.m_Matrix4D.m_Elements[2][c] + m_Matrix4D.m_Elements[r][3] * Matrix.m_Matrix4D.m_Elements[3][c];
                    }
                memcpy(&m_Matrix4D, &TemporalMatrixStructure, sizeof(MatrixStructure4D));
            }

            CMatrix4D CMatrix4D::operator*(const CMatrix4D& Matrix) const
            {
                CMatrix4D ResultMatrix;
                for (int r = 0 ; r < 4 ; ++r)
                    for (int c = 0 ; c < 4 ; ++c)
                    {
                        ResultMatrix.m_Matrix4D.m_Elements[r][c] = m_Matrix4D.m_Elements[r][0] * Matrix.m_Matrix4D.m_Elements[0][c] + m_Matrix4D.m_Elements[r][1] * Matrix.m_Matrix4D.m_Elements[1][c] + m_Matrix4D.m_Elements[r][2] * Matrix.m_Matrix4D.m_Elements[2][c] + m_Matrix4D.m_Elements[r][3] * Matrix.m_Matrix4D.m_Elements[3][c];
                    }
                return ResultMatrix;
            }

            void CMatrix4D::Transform(const Mathematics::_3D::CVector3D& Point, Mathematics::_3D::CVector3D& MappedPoint) const
            {
                const Real Elements[3] = { Point.GetX(), Point.GetY(), Point.GetZ() };
                Real MappedElements[4];
                for (int r = 0 ; r < 4 ; ++r)
                {
                    Real Accumulator = m_Matrix4D.m_Elements[r][3];
                    for (int c = 0 ; c < 3 ; ++c)
                    {
                        Accumulator += m_Matrix4D.m_Elements[r][c] * Elements[c];
                    }
                    MappedElements[r] = Accumulator;
                }
                MappedPoint.Set(MappedElements[0] / MappedElements[3], MappedElements[1] / MappedElements[3], MappedElements[2] / MappedElements[3]);
            }

            void CMatrix4D::RigidTransform(const Mathematics::_3D::CVector3D& Point, Mathematics::_3D::CVector3D& MappedPoint) const
            {
                const Real Elements[3] = { Point.GetX(), Point.GetY(), Point.GetZ() };
                Real MappedElements[3];
                for (int r = 0 ; r < 3 ; ++r)
                {
                    Real Accumulator = m_Matrix4D.m_Elements[r][3];
                    for (int c = 0 ; c < 3 ; ++c)
                    {
                        Accumulator += m_Matrix4D.m_Elements[r][c] * Elements[c];
                    }
                    MappedElements[r] = Accumulator;
                }
                MappedPoint.Set(MappedElements[0], MappedElements[1], MappedElements[2]);
            }

            _3D::CVector3D CMatrix4D::Transform(const Mathematics::_3D::CVector3D& Point) const
            {
                const Real Elements[3] = { Point.GetX(), Point.GetY(), Point.GetZ() };
                Real MappedElements[4];
                for (int r = 0 ; r < 4 ; ++r)
                {
                    Real Accumulator = m_Matrix4D.m_Elements[r][3];
                    for (int c = 0 ; c < 3 ; ++c)
                    {
                        Accumulator += m_Matrix4D.m_Elements[r][c] * Elements[c];
                    }
                    MappedElements[r] = Accumulator;
                }
                return Mathematics::_3D::CVector3D(MappedElements[0] / MappedElements[3], MappedElements[1] / MappedElements[3], MappedElements[2] / MappedElements[3]);
            }

            _3D::CVector3D CMatrix4D::RigidTransform(const Mathematics::_3D::CVector3D& Point) const
            {
                return Mathematics::_3D::CVector3D(m_Matrix4D.m_Elements[0][0] * Point.GetX() + m_Matrix4D.m_Elements[0][1] * Point.GetY() + m_Matrix4D.m_Elements[0][2] * Point.GetZ() + m_Matrix4D.m_Elements[0][3], m_Matrix4D.m_Elements[1][0] * Point.GetX() + m_Matrix4D.m_Elements[1][1] * Point.GetY() + m_Matrix4D.m_Elements[1][2] * Point.GetZ() + m_Matrix4D.m_Elements[1][3], m_Matrix4D.m_Elements[2][0] * Point.GetX() + m_Matrix4D.m_Elements[2][1] * Point.GetY() + m_Matrix4D.m_Elements[2][2] * Point.GetZ() + m_Matrix4D.m_Elements[2][3]);
                /*
                const Real Elements[3] = { Point.GetX() , Point.GetY() , Point.GetZ() };
                Real MappedElements[3];
                for(int r = 0 ; r < 3 ; ++r)
                {
                    Real Accumulator = m_Matrix4D.m_Elements[r][3];
                    for(int c = 0 ; c < 3 ; ++c)
                        Accumulator += m_Matrix4D.m_Elements[r][c] * Elements[c];
                    MappedElements[r] = Accumulator;
                }
                return Mathematics::_3D::CVector3D(MappedElements[0],MappedElements[1],MappedElements[2]);
                */
            }

            void CMatrix4D::Rotation(const Mathematics::_3D::CVector3D& Point, Mathematics::_3D::CVector3D& MappedPoint) const
            {
                const Real Elements[3] = { Point.GetX(), Point.GetY(), Point.GetZ() };
                Real MappedElements[3];
                for (int r = 0 ; r < 3 ; ++r)
                {
                    MappedElements[r] = m_Matrix4D.m_Elements[r][0] * Elements[0] + m_Matrix4D.m_Elements[r][1] * Elements[1] + m_Matrix4D.m_Elements[r][2] * Elements[2];
                }
                MappedPoint.Set(MappedElements[0], MappedElements[1], MappedElements[2]);
            }

            _3D::CVector3D CMatrix4D::Rotation(const Mathematics::_3D::CVector3D& Point) const
            {
                const Real Elements[3] = { Point.GetX(), Point.GetY(), Point.GetZ() };
                Real MappedElements[3];
                for (int r = 0 ; r < 3 ; ++r)
                {
                    MappedElements[r] = m_Matrix4D.m_Elements[r][0] * Elements[0] + m_Matrix4D.m_Elements[r][1] * Elements[1] + m_Matrix4D.m_Elements[r][2] * Elements[2];
                }
                return Mathematics::_3D::CVector3D(MappedElements[0], MappedElements[1], MappedElements[2]);
            }

            CMatrix4D CMatrix4D::operator*(const Real Scalar)
            {
                for (double& i : m_Matrix4D.m_Array)
                {
                    i *= Scalar;
                }
                return *this;
            }

            bool CMatrix4D::operator==(const CMatrix4D& Matrix) const
            {
                for (int i = 0 ; i < 16 ; ++i)
                    if (IsNonZero(m_Matrix4D.m_Array[i] - Matrix.m_Matrix4D.m_Array[i]))
                    {
                        return false;
                    }
                return true;
            }

            bool CMatrix4D::operator!=(const CMatrix4D& Matrix) const
            {
                for (int i = 0 ; i < 16 ; ++i)
                    if (IsNonZero(m_Matrix4D.m_Array[i] - Matrix.m_Matrix4D.m_Array[i]))
                    {
                        return true;
                    }
                return false;
            }

            const std::string CMatrix4D::ToString() const
            {
                std::ostringstream OutputString;
                OutputString.precision(g_RealDisplayDigits);
                for (const auto& m_Element : m_Matrix4D.m_Elements)
                {
                    OutputString << MapToTextDisplay(m_Element[0]) << "\t" << MapToTextDisplay(m_Element[1]) << "\t" << MapToTextDisplay(m_Element[2]) << "\t" << MapToTextDisplay(m_Element[3]) << std::endl;
                }
                return OutputString.str();
            }

            bool CMatrix4D::IsIsometry() const
            {
                return IsOne(GetDeterminat());
            }

            bool CMatrix4D::IsOrthonormal() const
            {
                for (int r = 0 ; r < 4 ; ++r)
                    for (int c = 0 ; c < 4 ; ++c)
                    {
                        const Real Elements = m_Matrix4D.m_Elements[r][0] * m_Matrix4D.m_Elements[c][0] + m_Matrix4D.m_Elements[r][1] * m_Matrix4D.m_Elements[c][2] + m_Matrix4D.m_Elements[r][2] * m_Matrix4D.m_Elements[c][2] + m_Matrix4D.m_Elements[r][3] * m_Matrix4D.m_Elements[c][3];
                        if (r == c)
                        {
                            if (!IsOne(Elements))
                            {
                                return false;
                            }
                        }
                        else if (IsNonZero(Elements))
                        {
                            return false;
                        }
                    }
                return true;
            }

            bool CMatrix4D::IsUnitary() const
            {
                for (int r = 0 ; r < 4 ; ++r)
                    for (int c = 0 ; c < 4 ; ++c)
                        if (r == c)
                        {
                            if (!IsOne(m_Matrix4D.m_Elements[r][c]))
                            {
                                return false;
                            }
                        }
                        else if (IsNonZero(m_Matrix4D.m_Elements[r][c]))
                        {
                            return false;
                        }
                return true;
            }

            Mathematics::_3D::CMatrix3D CMatrix4D::GetSubRotationMatrix() const
            {
                return Mathematics::_3D::CMatrix3D(m_Matrix4D.m_Elements[0][0], m_Matrix4D.m_Elements[0][1], m_Matrix4D.m_Elements[0][2], m_Matrix4D.m_Elements[1][0], m_Matrix4D.m_Elements[1][1], m_Matrix4D.m_Elements[1][2], m_Matrix4D.m_Elements[2][0], m_Matrix4D.m_Elements[2][1], m_Matrix4D.m_Elements[2][2]);
            }
        }
    }
}
