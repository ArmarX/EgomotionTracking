/*
 * Matrix2D.cpp
 */

#include "Matrix2D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _2D
        {
            const CMatrix2D CMatrix2D::s_Identity(Real(1), Real(0), Real(0), Real(1));

            CMatrix2D::CMatrix2D()
            {
                memset(&m_Matrix2D, 0, sizeof(MatrixStructure2D));
            }

            CMatrix2D::CMatrix2D(const Real E00, const Real E01, const Real E10, const Real E11)
            {
                m_Matrix2D.m_Elements[0][0] = E00;
                m_Matrix2D.m_Elements[0][1] = E01;
                m_Matrix2D.m_Elements[1][0] = E10;
                m_Matrix2D.m_Elements[1][1] = E11;
            }

            CMatrix2D::CMatrix2D(const CMatrix2D& Matrix)
            {
                memcpy(&m_Matrix2D, &Matrix.m_Matrix2D, sizeof(MatrixStructure2D));
            }

            CMatrix2D::CMatrix2D(const CMatrix2D* pMatrix)
            {
                if (pMatrix)
                {
                    memcpy(&m_Matrix2D, &pMatrix->m_Matrix2D, sizeof(MatrixStructure2D));
                }
                else
                {
                    memset(&m_Matrix2D, 0, sizeof(MatrixStructure2D));
                }
            }

            CMatrix2D::CMatrix2D(const CVector2D& C0, const CVector2D& C1)
            {
                m_Matrix2D.m_Elements[0][0] = C0.GetX();
                m_Matrix2D.m_Elements[0][1] = C1.GetX();
                m_Matrix2D.m_Elements[1][0] = C0.GetY();
                m_Matrix2D.m_Elements[1][1] = C1.GetY();
            }

            CMatrix2D::CMatrix2D(const Real Alpha)
            {
                const Real CosinusAlpha = std::cos(Alpha);
                const Real SinusAlpha = std::sin(Alpha);
                m_Matrix2D.m_Elements[0][0] = CosinusAlpha;
                m_Matrix2D.m_Elements[0][1] = -SinusAlpha;
                m_Matrix2D.m_Elements[1][0] = SinusAlpha;
                m_Matrix2D.m_Elements[1][1] = CosinusAlpha;
            }

            CMatrix2D::~CMatrix2D()
                = default;

            void CMatrix2D::MakeIdentity()
            {
                m_Matrix2D = s_Identity.m_Matrix2D;
            }

            Real CMatrix2D::GetDeterminat() const
            {
                return m_Matrix2D.m_Elements[0][0] * m_Matrix2D.m_Elements[1][1] - m_Matrix2D.m_Elements[0][1] * m_Matrix2D.m_Elements[1][0];
            }

            bool CMatrix2D::Invert()
            {
                const Real Determinant = m_Matrix2D.m_Elements[0][0] * m_Matrix2D.m_Elements[1][1] - m_Matrix2D.m_Elements[0][1] * m_Matrix2D.m_Elements[1][0];
                if (IsNonZero(Determinant))
                {
                    MatrixStructure2D InverseMatrix;
                    const Real Normalization = Real(1) / Determinant;
                    InverseMatrix.m_Elements[0][0] = m_Matrix2D.m_Elements[1][1] * Normalization;
                    InverseMatrix.m_Elements[0][1] = -m_Matrix2D.m_Elements[0][1] * Normalization;
                    InverseMatrix.m_Elements[1][0] = -m_Matrix2D.m_Elements[1][0] * Normalization;
                    InverseMatrix.m_Elements[1][1] = m_Matrix2D.m_Elements[0][0] * Normalization;
                    m_Matrix2D = InverseMatrix;
                    return true;
                }
                return false;
            }

            CMatrix2D CMatrix2D::GetInverse(bool* pSingularity) const
            {
                CMatrix2D InverseMatrix;
                const Real Determinant = m_Matrix2D.m_Elements[0][0] * m_Matrix2D.m_Elements[1][1] - m_Matrix2D.m_Elements[0][1] * m_Matrix2D.m_Elements[1][0];
                if (IsNonZero(Determinant))
                {
                    if (pSingularity)
                    {
                        *pSingularity = false;
                    }
                    const Real Normalization = Real(1) / Determinant;
                    InverseMatrix.m_Matrix2D.m_Elements[0][0] = m_Matrix2D.m_Elements[1][1] * Normalization;
                    InverseMatrix.m_Matrix2D.m_Elements[0][1] = -m_Matrix2D.m_Elements[0][1] * Normalization;
                    InverseMatrix.m_Matrix2D.m_Elements[1][0] = -m_Matrix2D.m_Elements[1][0] * Normalization;
                    InverseMatrix.m_Matrix2D.m_Elements[1][1] = m_Matrix2D.m_Elements[0][0] * Normalization;
                }
                else if (pSingularity)
                {
                    *pSingularity = true;
                }
                return InverseMatrix;
            }

            void CMatrix2D::Transpose()
            {
                std::swap(m_Matrix2D.m_Elements[0][1], m_Matrix2D.m_Elements[1][0]);
            }

            CMatrix2D CMatrix2D::GetTranspose() const
            {
                return CMatrix2D(m_Matrix2D.m_Elements[0][0], m_Matrix2D.m_Elements[1][0], m_Matrix2D.m_Elements[0][1], m_Matrix2D.m_Elements[1][1]);
            }

            void CMatrix2D::SetByElements(const Real E00, const Real E01, const Real E10, const Real E11)
            {
                m_Matrix2D.m_Elements[0][0] = E00;
                m_Matrix2D.m_Elements[0][1] = E01;
                m_Matrix2D.m_Elements[1][0] = E10;
                m_Matrix2D.m_Elements[1][1] = E11;
            }

            bool CMatrix2D::SetByRow(const Real* pMatrix)
            {
                if (pMatrix)
                {
                    for (auto& m_Element : m_Matrix2D.m_Elements)
                        for (int c = 0 ; c < 2 ; ++c)
                        {
                            m_Element[c] = *pMatrix++;
                        }
                    return true;
                }
                return false;
            }

            bool CMatrix2D::SetByColumn(const Real* pMatrix)
            {
                if (pMatrix)
                {
                    for (int c = 0 ; c < 2 ; ++c)
                        for (auto& m_Element : m_Matrix2D.m_Elements)
                        {
                            m_Element[c] = *pMatrix++;
                        }
                    return true;
                }
                return false;
            }

            void CMatrix2D::SetByAngle(const Real Alpha)
            {
                const Real CosinusAlpha = std::cos(Alpha);
                const Real SinusAlpha = std::sin(Alpha);
                m_Matrix2D.m_Elements[0][0] = CosinusAlpha;
                m_Matrix2D.m_Elements[0][1] = -SinusAlpha;
                m_Matrix2D.m_Elements[1][0] = SinusAlpha;
                m_Matrix2D.m_Elements[1][1] = CosinusAlpha;
            }

            Real* CMatrix2D::operator[](const int Row)
            {
                return m_Matrix2D.m_Elements[Row];
            }

            const Real* CMatrix2D::operator[](const int Row) const
            {
                return m_Matrix2D.m_Elements[Row];
            }

            void CMatrix2D::operator=(const CMatrix2D& Matrix)
            {
                memcpy(&m_Matrix2D, &Matrix.m_Matrix2D, sizeof(MatrixStructure2D));
            }

            void CMatrix2D::operator*=(const CMatrix2D& Matrix)
            {
                MatrixStructure2D TemporalMatrixStructure;
                for (int r = 0 ; r < 2 ; ++r)
                    for (int c = 0 ; c < 2 ; ++c)
                    {
                        TemporalMatrixStructure.m_Elements[r][c] =
                            m_Matrix2D.m_Elements[r][0] * Matrix.m_Matrix2D.m_Elements[0][c] +
                            m_Matrix2D.m_Elements[r][1] * Matrix.m_Matrix2D.m_Elements[1][c];
                    }
                memcpy(&m_Matrix2D, &TemporalMatrixStructure, sizeof(MatrixStructure2D));
            }

            CMatrix2D CMatrix2D::operator*(const CMatrix2D& Matrix) const
            {
                CMatrix2D ResultMatrix;
                for (int r = 0 ; r < 2 ; ++r)
                    for (int c = 0 ; c < 2 ; ++c)
                    {
                        ResultMatrix.m_Matrix2D.m_Elements[r][c] =
                            m_Matrix2D.m_Elements[r][0] * Matrix.m_Matrix2D.m_Elements[0][c] +
                            m_Matrix2D.m_Elements[r][1] * Matrix.m_Matrix2D.m_Elements[1][c];
                    }
                return ResultMatrix;
            }

            CVector2D CMatrix2D::operator*(const CVector2D& Vector) const
            {
                return CVector2D(m_Matrix2D.m_Elements[0][0] * Vector.GetX() + m_Matrix2D.m_Elements[0][1] * Vector.GetY(), m_Matrix2D.m_Elements[1][0] * Vector.GetX() + m_Matrix2D.m_Elements[1][1] * Vector.GetY());
            }

            CMatrix2D CMatrix2D::operator*(const Real Scalar)
            {
                return CMatrix2D(m_Matrix2D.m_Elements[0][0] * Scalar, m_Matrix2D.m_Elements[0][1] * Scalar, m_Matrix2D.m_Elements[1][0] * Scalar, m_Matrix2D.m_Elements[1][1] * Scalar);
            }

            bool CMatrix2D::operator==(const CMatrix2D& Matrix) const
            {
                for (int i = 0 ; i < 4 ; ++i)
                    if (IsNonZero(m_Matrix2D.m_Array[i] - Matrix.m_Matrix2D.m_Array[i]))
                    {
                        return false;
                    }
                return true;
            }

            bool CMatrix2D::operator!=(const CMatrix2D& Matrix) const
            {
                for (int i = 0 ; i < 4 ; ++i)
                    if (IsNonZero(m_Matrix2D.m_Array[i] - Matrix.m_Matrix2D.m_Array[i]))
                    {
                        return true;
                    }
                return false;
            }

            const std::string CMatrix2D::ToString() const
            {
                std::ostringstream OutputString;
                OutputString.precision(g_RealDisplayDigits);
                for (auto m_Element : m_Matrix2D.m_Elements)
                {
                    OutputString << MapToTextDisplay(m_Element[0]) << "\t" << MapToTextDisplay(m_Element[1]) << std::endl;
                }
                return OutputString.str();
            }
        }
    }
}
