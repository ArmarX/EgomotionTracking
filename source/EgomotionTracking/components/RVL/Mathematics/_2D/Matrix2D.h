/*
 * Matrix2D.h
 */

#pragma once

#include "../../Common/DataTypes.h"
#include "Vector2D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _2D
        {
            class CMatrix2D
            {
            public:

                static const CMatrix2D s_Identity;

                CMatrix2D();
                CMatrix2D(const Real E00, const Real E01, const Real E10, const Real E11);
                CMatrix2D(const CMatrix2D& Matrix);
                CMatrix2D(const CMatrix2D* pMatrix);
                CMatrix2D(const CVector2D& C0, const CVector2D& C1);
                CMatrix2D(const Real Alpha);
                ~CMatrix2D();

                void MakeIdentity();
                Real GetDeterminat() const;
                bool Invert();
                CMatrix2D GetInverse(bool* pSingularity = NULL) const;
                void Transpose();
                CMatrix2D GetTranspose() const;
                void SetByElements(const Real E00, const Real E01, const Real E10, const Real E11);
                bool SetByRow(const Real* pMatrix);
                bool SetByColumn(const Real* pMatrix);
                void SetByAngle(const Real Alpha);

                Real* operator[](const int Row);
                const Real* operator[](const int Row) const;
                void operator=(const CMatrix2D& Matrix);
                void operator*=(const CMatrix2D& Matrix);
                CMatrix2D operator*(const CMatrix2D& Matrix) const;
                CVector2D operator*(const CVector2D& Vector) const;
                CMatrix2D operator*(const Real Scalar);
                bool operator==(const CMatrix2D& Matrix) const;
                bool operator!=(const CMatrix2D& Matrix) const;
                const std::string ToString() const;

            protected:

                union MatrixStructure2D
                {
                    Real m_Array[4];
                    Real m_Elements[2][2];
                };
                MatrixStructure2D m_Matrix2D;
            };
        }
    }
}

