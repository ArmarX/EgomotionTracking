/*
 * ConvolutionGaborKernel2D.h
 */

#pragma once

#include "../Vector2D.h"
#include "ConvolutionKernel2D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Kernels
            {
                class CConvolutionGaborKernel2D : public CConvolutionKernel2D
                {
                public:

                    CConvolutionGaborKernel2D();
                    CConvolutionGaborKernel2D(const CVector2D& Orientation, const Real StandardDeviation = Real(1) / Real(3), const Real Lambda = Real(0.5), const Real Gamma1 = Real(1), const Real Gamma2 = Real(2));
                    ~CConvolutionGaborKernel2D() override;

                    bool SetOrientation(const CVector2D& Orientation);
                    bool SetStandardDeviation(const Real StandardDeviation);
                    bool SetLambda(const Real Lambda);
                    bool SetGamma1(const Real Gamma1);
                    bool SetGamma2(const Real Gamma2);

                    const CVector2D& GetOrientation() const;
                    Real GetStandardDeviation() const;
                    Real GetLambda() const;
                    Real GetGamma1() const;
                    Real GetGamma2() const;

                protected:

                    void Generate() override;

                    CVector2D m_Orientation;
                    Real m_StandardDeviation;
                    Real m_Lambda;
                    Real m_Gamma1;
                    Real m_Gamma2;
                };
            }
        }
    }
}

