/*
 * ConvolutionGaborKernel2D.cpp
 */

#include "ConvolutionGaborKernel2D.h"
#include "../../_1D/NormalDistribution.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Kernels
            {
                CConvolutionGaborKernel2D::CConvolutionGaborKernel2D() :
                    CConvolutionKernel2D(),
                    m_Orientation(),
                    m_StandardDeviation(Real(0)),
                    m_Lambda(Real(0)),
                    m_Gamma1(Real(0)),
                    m_Gamma2(Real(0))
                {
                }

                CConvolutionGaborKernel2D::CConvolutionGaborKernel2D(const CVector2D& Orientation, const Real StandardDeviation, const Real Lambda, const Real Gamma1, const Real Gamma2) :
                    CConvolutionKernel2D(),
                    m_Orientation(Orientation),
                    m_StandardDeviation(StandardDeviation),
                    m_Lambda(Lambda),
                    m_Gamma1(Gamma1),
                    m_Gamma2(Gamma2)
                {
                    if (Create(std::max(int(std::ceil(m_StandardDeviation * Real(3))), 1)))
                    {
                        Generate();
                    }
                }

                CConvolutionGaborKernel2D::~CConvolutionGaborKernel2D()
                    = default;

                bool CConvolutionGaborKernel2D::SetOrientation(const CVector2D& Orientation)
                {
                    if (Orientation.IsUnitary())
                    {
                        m_Orientation = Orientation;
                        Generate();
                        return true;
                    }
                    return false;
                }

                bool CConvolutionGaborKernel2D::SetStandardDeviation(const Real StandardDeviation)
                {
                    if (IsPositive(StandardDeviation) && NonEquals(StandardDeviation, m_StandardDeviation))
                    {
                        if (Create(std::max(int(std::ceil(StandardDeviation * Real(3))), 1)))
                        {
                            m_StandardDeviation = StandardDeviation;
                        }
                        Generate();
                        return true;
                    }
                    return false;
                }

                bool CConvolutionGaborKernel2D::SetLambda(const Real Lambda)
                {
                    if (IsPositive(Lambda) && (Lambda < g_RealPI))
                    {
                        m_Lambda = Lambda;
                        Generate();
                        return true;
                    }
                    return false;
                }

                bool CConvolutionGaborKernel2D::SetGamma1(const Real Gamma1)
                {
                    if (IsPositive(Gamma1))
                    {
                        m_Gamma1 = Gamma1;
                        Generate();
                        return true;
                    }
                    return false;
                }

                bool CConvolutionGaborKernel2D::SetGamma2(const Real Gamma2)
                {
                    if (IsPositive(Gamma2))
                    {
                        m_Gamma2 = Gamma2;
                        Generate();
                        return true;
                    }
                    return false;
                }

                const CVector2D& CConvolutionGaborKernel2D::GetOrientation() const
                {
                    return m_Orientation;
                }

                Real CConvolutionGaborKernel2D::GetStandardDeviation() const
                {
                    return m_StandardDeviation;
                }

                Real CConvolutionGaborKernel2D::GetLambda() const
                {
                    return m_Lambda;
                }

                Real CConvolutionGaborKernel2D::GetGamma1() const
                {
                    return m_Gamma1;
                }

                Real CConvolutionGaborKernel2D::GetGamma2() const
                {
                    return m_Gamma2;
                }

                void CConvolutionGaborKernel2D::Generate()
                {
                    if (m_pKernel && IsPositive(m_Radius) && IsPositive(m_Lambda) && IsPositive(m_Gamma1) && IsPositive(m_Gamma2))
                    {
                        Real* pKernel = m_pKernel;
                        const Real ExponentFactor = _1D::CNormalDistribution::DetermineExponentFactor(m_StandardDeviation);
                        const Real AngularFactor = m_Lambda * (g_RealPI / Real(m_Radius));
                        Real AbsoluteIntegral = Real(0);
                        for (int Y = -m_Radius ; Y <= m_Radius ; ++Y)
                            for (int X = -m_Radius ; X <= m_Radius ; ++X, ++pKernel)
                            {
                                const Real Xn = m_Gamma1 * m_Orientation.ScalarProduct(X, Y);
                                const Real Yn = m_Gamma2 * m_Orientation.ScalarProduct(Y, -X);
                                *pKernel = std::exp(ExponentFactor * (Xn * Xn + Yn * Yn)) * std::sin(Xn * AngularFactor);
                                AbsoluteIntegral += std::abs(*pKernel);
                            }
                        pKernel = m_pKernel;
                        for (int Y = -m_Radius ; Y <= m_Radius ; ++Y)
                            for (int X = -m_Radius ; X <= m_Radius ; ++X, ++pKernel)
                            {
                                *pKernel /= AbsoluteIntegral;
                            }
                    }
                }
            }
        }
    }
}
