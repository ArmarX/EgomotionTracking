/*
 * ConvolutionKernel2D.cpp
 */

#include "ConvolutionKernel2D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Kernels
            {
                CConvolutionKernel2D::CConvolutionKernel2D() :
                    m_Radius(0),
                    m_pKernel(nullptr)
                {
                }

                CConvolutionKernel2D::CConvolutionKernel2D(const int Radius) :
                    m_Radius(Radius),
                    m_pKernel(nullptr)
                {
                    Create(Radius);
                }

                CConvolutionKernel2D::~CConvolutionKernel2D()
                {
                    if (m_pKernel)
                    {
                        delete[] m_pKernel;
                    }
                }

                bool CConvolutionKernel2D::Create(const int Radius)
                {
                    if (Radius > 0)
                    {
                        m_Radius = Radius;
                        const int Diameter = (m_Radius << 1) + 1;
                        const int Area = Diameter * Diameter;
                        if (m_pKernel)
                        {
                            delete[] m_pKernel;
                        }
                        m_pKernel = new Real[Area];
                        memset(m_pKernel, 0, sizeof(Real) * Area);
                        return true;
                    }
                    return false;
                }

                bool CConvolutionKernel2D::IsValid() const
                {
                    return m_pKernel;
                }

                int CConvolutionKernel2D::GetRadius() const
                {
                    return m_Radius;
                }

                int CConvolutionKernel2D::GetDiameter() const
                {
                    return (m_Radius << 1) + 1;
                }

                int CConvolutionKernel2D::GetArea() const
                {
                    return ((m_Radius << 1) + 1) * ((m_Radius << 1) + 1);
                }

                const Real* CConvolutionKernel2D::GetReadOnlyKernel() const
                {
                    return m_pKernel;
                }

                const Real* CConvolutionKernel2D::GetReadOnlyKernelAt(const int DX, const int DY) const
                {
                    if ((std::abs(DX) > m_Radius) || (std::abs(DY)) > m_Radius)
                    {
                        return nullptr;
                    }
                    const int Diameter = (m_Radius << 1) + 1;
                    const int X = DX + m_Radius;
                    const int Y = DY + m_Radius;
                    return m_pKernel + Y * Diameter + X;
                }

                Real CConvolutionKernel2D::GetKernelAt(const int DX, const int DY) const
                {
                    if ((std::abs(DX) > m_Radius) || (std::abs(DY)) > m_Radius)
                    {
                        return Real(0);
                    }
                    const int Diameter = (m_Radius << 1) + 1;
                    const int X = DX + m_Radius;
                    const int Y = DY + m_Radius;
                    return m_pKernel[Y * Diameter + X];
                }

                Real* CConvolutionKernel2D::GetWritableKernel()
                {
                    return m_pKernel;
                }

                bool CConvolutionKernel2D::SetKernelAt(const int DX, const int DY, const Real KXY)
                {
                    if ((std::abs(DX) > m_Radius) || (std::abs(DY)) > m_Radius)
                    {
                        return false;
                    }
                    const int Diameter = (m_Radius << 1) + 1;
                    const int X = DX + m_Radius;
                    const int Y = DY + m_Radius;
                    m_pKernel[Y * Diameter + X] = KXY;
                    return true;
                }

                const std::string CConvolutionKernel2D::ToString() const
                {
                    std::ostringstream OutputString;
                    OutputString.precision(g_RealDisplayDigits);
                    const Real* pKernel = m_pKernel;
                    for (int Y = -m_Radius ; Y <= m_Radius ; ++Y, OutputString << std::endl)
                        for (int X = -m_Radius ; X <= m_Radius ; ++X, ++pKernel)
                        {
                            OutputString << MapToTextDisplay(*pKernel) << "\t";
                        }
                    return OutputString.str();
                }
            }
        }
    }
}
