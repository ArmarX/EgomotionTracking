/*
 * ConvolutionKernel2D.h
 */

#pragma once

#include "../../../Common/DataTypes.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Kernels
            {
                class CConvolutionKernel2D
                {
                public:

                    CConvolutionKernel2D();
                    CConvolutionKernel2D(const int Radius);
                    virtual ~CConvolutionKernel2D();

                    bool Create(const int Radius);
                    bool IsValid() const;

                    int GetRadius() const;
                    int GetDiameter() const;
                    int GetArea() const;
                    const Real* GetReadOnlyKernel() const;
                    const Real* GetReadOnlyKernelAt(const int DX, const int DY) const;
                    Real GetKernelAt(const int DX, const int DY) const;

                    Real* GetWritableKernel();
                    bool SetKernelAt(const int DX, const int DY, const Real KXY);
                    const std::string ToString() const;

                protected:

                    virtual void Generate() = 0;

                    int m_Radius;
                    Real* m_pKernel;
                };
            }
        }
    }
}

