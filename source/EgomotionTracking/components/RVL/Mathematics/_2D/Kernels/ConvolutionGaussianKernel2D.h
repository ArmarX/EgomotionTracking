/*
 * ConvolutionGaussianKernel2D.h
 */

#pragma once

#include "ConvolutionKernel2D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Kernels
            {
                class CConvolutionGaussianKernel2D : public CConvolutionKernel2D
                {
                public:

                    CConvolutionGaussianKernel2D();
                    CConvolutionGaussianKernel2D(const Real StandardDeviation);
                    ~CConvolutionGaussianKernel2D() override;

                    bool SetStandardDeviation(const Real StandardDeviation);
                    bool Normalize();
                    bool ScaleIntegral(const Real ScaleFactor);
                    bool ScaleMaximal(const Real ScaleFactor);
                    bool ScaleMinimal(const Real ScaleFactor);
                    bool ScaleMean(const Real ScaleFactor);

                    Real GetStandardDeviation() const;

                protected:

                    void Generate() override;

                    Real m_StandardDeviation;
                };
            }
        }
    }
}

