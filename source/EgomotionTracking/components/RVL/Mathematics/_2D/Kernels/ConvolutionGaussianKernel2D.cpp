/*
 * ConvolutionGaussianKernel2D.cpp
 */

#include "ConvolutionGaussianKernel2D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Kernels
            {
                CConvolutionGaussianKernel2D::CConvolutionGaussianKernel2D() :
                    CConvolutionKernel2D(),
                    m_StandardDeviation(Real(0))
                {
                }

                CConvolutionGaussianKernel2D::CConvolutionGaussianKernel2D(const Real StandardDeviation) :
                    CConvolutionKernel2D(),
                    m_StandardDeviation(0)
                {
                    SetStandardDeviation(StandardDeviation);
                }

                CConvolutionGaussianKernel2D::~CConvolutionGaussianKernel2D()
                    = default;

                void CConvolutionGaussianKernel2D::Generate()
                {
                    if (m_pKernel)
                    {
                        Real* pKernel = m_pKernel;
                        const Real ExponentFactor = Real(-1) / (Real(2) * m_StandardDeviation * m_StandardDeviation);
                        const Real ScaleFactor = Real(1) / std::sqrt(g_RealPI * Real(2) * m_StandardDeviation * m_StandardDeviation);
                        for (int Y = -m_Radius ; Y <= m_Radius ; ++Y)
                            for (int X = -m_Radius ; X <= m_Radius ; ++X, ++pKernel)
                            {
                                const Real SquareRadius = X * X + Y * Y;
                                *pKernel = ScaleFactor * std::exp(ExponentFactor * SquareRadius);
                            }
                    }
                }

                bool CConvolutionGaussianKernel2D::SetStandardDeviation(const Real StandardDeviation)
                {
                    if (IsPositive(StandardDeviation))
                    {
                        if (NonEquals(StandardDeviation, m_StandardDeviation))
                        {
                            m_StandardDeviation = StandardDeviation;
                            if (Create(std::max(int(std::ceil(StandardDeviation * Real(3))), 1)))
                            {
                                Generate();
                            }
                        }
                        return true;
                    }
                    return false;
                }

                bool CConvolutionGaussianKernel2D::Normalize()
                {
                    if (m_pKernel)
                    {
                        Real Accumulator = Real(0);
                        const int Area = GetArea();
                        for (int i = 0 ; i < Area ; ++i)
                        {
                            Accumulator += m_pKernel[i];
                        }
                        const Real NormalizationFactor = Real(1) / Accumulator;
                        for (int i = 0 ; i < Area ; ++i)
                        {
                            m_pKernel[i] *= NormalizationFactor;
                        }
                        return true;
                    }
                    return false;
                }

                bool CConvolutionGaussianKernel2D::ScaleIntegral(const Real ScaleFactor)
                {
                    if (m_pKernel)
                    {
                        Real Accumulator = Real(0);
                        const int Area = GetArea();
                        for (int i = 0 ; i < Area ; ++i)
                        {
                            Accumulator += m_pKernel[i];
                        }
                        const Real NormalizationFactor = ScaleFactor / Accumulator;
                        for (int i = 0 ; i < Area ; ++i)
                        {
                            m_pKernel[i] *= NormalizationFactor;
                        }
                        return true;
                    }
                    return false;
                }

                bool CConvolutionGaussianKernel2D::ScaleMaximal(const Real ScaleFactor)
                {
                    if (m_pKernel)
                    {
                        const int Area = GetArea();
                        Real Maximal = m_pKernel[0];
                        for (int i = 0 ; i < Area ; ++i)
                            if (m_pKernel[i] > Maximal)
                            {
                                Maximal = m_pKernel[i];
                            }
                        const Real NormalizationFactor = ScaleFactor / Maximal;
                        for (int i = 0 ; i < Area ; ++i)
                        {
                            m_pKernel[i] *= NormalizationFactor;
                        }
                        return true;
                    }
                    return false;
                }

                bool CConvolutionGaussianKernel2D::ScaleMinimal(const Real ScaleFactor)
                {
                    if (m_pKernel)
                    {
                        Real Minimal = m_pKernel[0];
                        const int Area = GetArea();
                        for (int i = 0 ; i < Area ; ++i)
                            if (m_pKernel[i] < Minimal)
                            {
                                Minimal = m_pKernel[i];
                            }
                        const Real NormalizationFactor = ScaleFactor / Minimal;
                        for (int i = 0 ; i < Area ; ++i)
                        {
                            m_pKernel[i] *= NormalizationFactor;
                        }
                        return true;
                    }
                    return false;
                }

                bool CConvolutionGaussianKernel2D::ScaleMean(const Real ScaleFactor)
                {
                    if (m_pKernel)
                    {
                        Real Accumulator = Real(0);
                        const int Area = GetArea();
                        for (int i = 0 ; i < Area ; ++i)
                        {
                            Accumulator += m_pKernel[i];
                        }
                        const Real Mean = Accumulator / Real(Area);
                        const Real NormalizationFactor = ScaleFactor / Mean;
                        for (int i = 0 ; i < Area ; ++i)
                        {
                            m_pKernel[i] *= NormalizationFactor;
                        }
                        return true;
                    }
                    return false;
                }

                Real CConvolutionGaussianKernel2D::GetStandardDeviation() const
                {
                    return m_StandardDeviation;
                }
            }
        }
    }
}
