/*
 * BicubicInterpolation.cpp
 *
 *  Created on: Oct 3, 2013
 *      Author: gonzalez
 */

#include "BicubicInterpolation.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Interpolation
            {
                void CBicubicInterpolation::EstimateBicubicCoefficients(Real I[4][4], Real A[16])
                {
                    A[0] = I[1][1];
                    A[1] = Real(0.5) * (I[1][2] - I[1][0]);
                    A[2] = Real(-0.5) * I[1][3] + I[1][0] + Real(2) * I[1][2] + Real(-2.5) * I[1][1];
                    A[3] = Real(1.5) * (I[1][1] - I[1][2]) + Real(0.5) * (I[1][3] - I[1][0]);
                    A[4] = Real(0.5) * (I[2][1] - I[0][1]);
                    A[5] = Real(0.25) * (I[0][0] + I[2][2] - I[0][2] - I[2][0]);
                    A[6] = Real(0.25) * (I[0][3] - I[2][3]) + Real(0.5) * (I[2][0] - I[0][0]) + (I[2][2] - I[0][2]) + Real(1.25) * (I[0][1] - I[2][1]);
                    A[7] = Real(0.25) * (I[0][0] + I[2][3] - I[0][3] - I[2][0]) + Real(0.75) * (I[0][2] + I[2][1] - I[0][1] - I[2][2]);
                    A[8] = Real(-0.5) * I[3][1] + I[0][1] + Real(2) * I[2][1] + Real(-2.5) * I[1][1];
                    A[9] = Real(0.25) * (I[3][0] - I[3][2]) + Real(0.5) * (I[0][2] - I[0][0]) + Real(1.25) * (I[1][0] - I[1][2]) + I[2][2] - I[2][0];
                    A[10] = Real(0.25) * I[3][3] + Real(-0.5) * (I[0][3] + I[3][0]) + (I[0][0] - I[2][3] - I[3][2]) + Real(1.25) * (I[1][3] + I[3][1]) + Real(2) * (I[0][2] + I[2][0]) + Real(-2.5) * (I[0][1] + I[1][0]) + Real(4) * I[2][2] + Real(-5) * (I[1][2] + I[2][1]) + Real(6.25) * I[1][1];
                    A[11] = Real(0.25) * (I[3][0] - I[3][3]) + Real(0.5) * (I[0][3] - I[0][0]) + Real(0.75) * (I[3][2] - I[3][1]) + (I[2][3] - I[2][0]) + Real(1.25) * (I[1][0] - I[1][3]) + Real(1.5) * (I[0][1] - I[0][2]) + Real(3) * (I[2][1] - I[2][2]) + Real(3.75) * (I[1][2] - I[1][1]);
                    A[12] = Real(0.5) * (I[3][1] - I[0][1]) + Real(1.5) * (I[1][1] - I[2][1]);
                    A[13] = Real(0.25) * (I[0][0] + I[3][2] - I[0][2] - I[3][0]) + Real(0.75) * (I[1][2] + I[2][0] - I[1][0] - I[2][2]);
                    A[14] = Real(0.25) * (I[0][3] - I[3][3]) + Real(0.5) * (I[3][0] - I[0][0]) + Real(0.75) * (I[2][3] - I[1][3]) + (I[3][2] - I[0][2]) + Real(1.25) * (I[0][1] - I[3][1]) + Real(1.5) * (I[1][0] - I[2][0]) + Real(3) * (I[1][2] - I[2][2]) + Real(3.75) * (I[2][1] - I[1][1]);
                    A[15] = Real(0.25) * (I[0][0] + I[3][3] - I[0][3] - I[3][0]) + Real(0.75) * (I[0][2] + I[1][3] + I[2][0] + I[3][1] - I[0][1] - I[1][0] - I[2][3] - I[3][2]) + Real(2.25) * (I[1][1] + I[2][2] - I[1][2] - I[2][1]);
                }

                Real CBicubicInterpolation::EvaluateBicubic(const Real X1, const Real Y1, const Real A[16])
                {
                    const Real X2 = X1 * X1;
                    const Real X3 = X2 * X1;
                    const Real Y2 = Y1 * Y1;
                    const Real Y3 = Y2 * Y1;
                    return (A[0] + A[1] * X1 + A[2] * X2 + A[3] * X3) + (A[4] + A[5] * X1 + A[6] * X2 + A[7] * X3) * Y1 + (A[8] + A[9] * X1 + A[10] * X2 + A[11] * X3) * Y2 + (A[12] + A[13] * X1 + A[14] * X2 + A[15] * X3) * Y3;
                }

                CBicubicInterpolation::CBicubicInterpolation()
                    = default;

                CBicubicInterpolation::~CBicubicInterpolation()
                    = default;
            }
        }
    }
}
