/*
 * BicubicInterpolation.h
 *
 *  Created on: Oct 3, 2013
 *      Author: gonzalez
 */

#pragma once

#include "../../../Common/DataTypes.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Interpolation
            {
                class CBicubicInterpolation
                {
                public:

                    static void EstimateBicubicCoefficients(Real I[4][4], Real A[16]);
                    static Real EvaluateBicubic(const Real X1, const Real Y1, const Real A[16]);

                protected:

                    CBicubicInterpolation();
                    virtual ~CBicubicInterpolation();
                };
            }
        }
    }
}

