/*
 * Vector2D.h
 */

#pragma once

#include "../../Common/DataTypes.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _2D
        {
            class CVector2D
            {
            public:

                static const CVector2D s_PlusInfinity;
                static const CVector2D s_Zero;
                static const CVector2D s_MinusInfinity;
                static const CVector2D s_Unitary_X;
                static const CVector2D s_Unitary_Y;

                static CVector2D CreateLinealCombination(const CVector2D& A, const Real SA, const CVector2D& B, const Real SB);
                static CVector2D CreateComplementaryLinealCombination(const CVector2D& A, const CVector2D& B, const Real SA);
                static CVector2D CreateMidPoint(const CVector2D& A, const CVector2D& B);
                static CVector2D CreateNormalized(const CVector2D& Vector);
                static CVector2D CreateCannonicalForm(const Real X, const Real Y);

                CVector2D();
                CVector2D(const Real X, const Real Y);
                CVector2D(const int X, const int Y);
                CVector2D(const CVector2D& Vector);
                ~CVector2D();

                bool SetCannonicalForm();
                void SetX(const Real X);
                void SetY(const Real Y);
                void Set(const Real X, const Real Y);
                void SetMaximalComponentValue(const Real X, const Real Y);
                void SetMinimalComponentValue(const Real X, const Real Y);
                void SetMaximalComponentValue(const CVector2D& Vector);
                void SetMinimalComponentValue(const CVector2D& Vector);
                void SetMidPoint(const CVector2D& A, const CVector2D& B);
                void SetWeighted(const CVector2D& Vector, const Real Weight);
                void SetWeightedInverse(const CVector2D& Vector, const Real Weight);
                void SetLinealCombination(const CVector2D& A, const Real SA, const CVector2D& B, const Real SB);
                void SetZero();
                void Negate();
                void AddWeightedOffset(const CVector2D& Vector, const Real Weight);
                void AddOffset(const Real DX, const Real DY);
                void AddOffsetX(const Real DX);
                void AddOffsetY(const Real DY);
                void SubtractOffsetX(const Real DX);
                void SubtractOffsetY(const Real DY);
                void AddCovariance(const CVector2D& Vector, Real CXX, Real CXY, Real CYY) const;
                void AddWeightedCovariance(const CVector2D& Vector, const Real Weight, Real CXX, Real CXY, Real CYY) const;
                bool IsNull() const;
                bool IsNonNull() const;
                bool IsAtInfinity() const;
                bool IsNotAtInfinity() const;
                bool IsUnitary() const;
                bool IsNonUnitary() const;
                Real GetX() const;
                Real GetY() const;
                Real GetAngle() const;
                Real GetAngle(const CVector2D& Vector) const;
                Real GetAperture(const CVector2D& Vector) const;
                Real GetAbsoluteAperture(const CVector2D& Vector) const;
                Real GetDistance(const CVector2D& Vector) const;
                Real GetLength() const;
                Real GetSquareLength() const;
                Real Normalize();
                void ScaleAnisotropic(const Real SX, const Real SY);
                const std::string ToString() const;
                Real ScalarProduct(const CVector2D& Vector) const;
                Real ScalarProduct(const Real X, const Real Y) const;
                Real GetSquareDistance(const CVector2D& Vector) const;
                void operator=(const CVector2D& Vector);
                void operator+=(const CVector2D& Vector);
                void operator-=(const CVector2D& Vector);
                void operator*=(const Real Scalar);
                void operator/=(const Real Scalar);
                bool operator==(const CVector2D& Vector) const;
                bool operator!=(const CVector2D& Vector) const;

            protected:

                Real m_X;
                Real m_Y;
            };

            CVector2D operator/(const CVector2D& Vector, const Real Scalar);
            CVector2D operator*(const CVector2D& Vector, const Real Scalar);
            CVector2D operator*(const Real Scalar, const CVector2D& Vector);
            CVector2D operator+(const CVector2D& A, const CVector2D& B);
            CVector2D operator-(const CVector2D& A, const CVector2D& B);
        }
    }
}

