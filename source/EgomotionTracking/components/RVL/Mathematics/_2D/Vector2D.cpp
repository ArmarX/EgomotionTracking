/*
 * Vector2D.cpp
 */

#include "Vector2D.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _2D
        {

            const CVector2D CVector2D::s_PlusInfinity(g_RealPlusInfinity, g_RealPlusInfinity);
            const CVector2D CVector2D::s_Zero(Real(0), Real(0));
            const CVector2D CVector2D::s_MinusInfinity(g_RealMinusInfinity, g_RealMinusInfinity);
            const CVector2D CVector2D::s_Unitary_X(Real(1), Real(0));
            const CVector2D CVector2D::s_Unitary_Y(Real(0), Real(1));

            CVector2D CVector2D::CreateLinealCombination(const CVector2D& A, const Real SA, const CVector2D& B, const Real SB)
            {
                return CVector2D(A.m_X * SA + B.m_X * SB, A.m_Y * SA + B.m_Y * SB);
            }

            CVector2D CVector2D::CreateComplementaryLinealCombination(const CVector2D& A, const CVector2D& B, const Real SA)
            {
                const Real SB = Real(1) - SA;
                return CVector2D(A.m_X * SA + B.m_X * SB, A.m_Y * SA + B.m_Y * SB);
            }

            CVector2D CVector2D::CreateMidPoint(const CVector2D& A, const CVector2D& B)
            {
                return CVector2D((A.m_X + B.m_X) * Real(0.5), (A.m_Y + B.m_Y) * Real(0.5));
            }

            CVector2D CVector2D::CreateNormalized(const CVector2D& Vector)
            {
                CVector2D NormalizedVector(Vector);
                NormalizedVector.Normalize();
                return NormalizedVector;
            }

            CVector2D CVector2D::CreateCannonicalForm(const Real X, const Real Y)
            {
                const Real Components[2] = { X, Y };
                Real Maximal = g_RealPlusEpsilon;
                int Index = -1;
                for (int i = 0 ; i < 2 ; ++i)
                {
                    const Real Magnitud = std::abs(Components[i]);
                    if (Magnitud > Maximal)
                    {
                        Index = i;
                        Maximal = Magnitud;
                    }
                }
                if (Index >= 0)
                {
                    if (IsNegative(Components[Index]))
                    {
                        return CVector2D(-X, -Y);
                    }
                    return CVector2D(X, Y);
                }
                return CVector2D();
            }

            CVector2D::CVector2D() :
                m_X(Real(0)),
                m_Y(Real(0))
            {
            }

            CVector2D::CVector2D(const Real X, const Real Y) :
                m_X(X),
                m_Y(Y)
            {
            }

            CVector2D::CVector2D(const int X, const int Y) :
                m_X(X),
                m_Y(Y)
            {
            }

            CVector2D::CVector2D(const CVector2D& Vector)

                = default;

            CVector2D::~CVector2D()
                = default;

            bool CVector2D::SetCannonicalForm()
            {
                const Real Components[2] = { m_X, m_Y };
                Real Maximal = g_RealPlusEpsilon;
                int Index = -1;
                for (int i = 0 ; i < 2 ; ++i)
                {
                    const Real Magnitud = std::abs(Components[i]);
                    if (Magnitud > Maximal)
                    {
                        Index = i;
                        Maximal = Magnitud;
                    }
                }
                if (Index >= 0)
                {
                    if (IsNegative(Components[Index]))
                    {
                        Negate();
                    }
                    return true;
                }
                return false;
            }

            void CVector2D::SetX(const Real X)
            {
                m_X = X;
            }

            void CVector2D::SetY(const Real Y)
            {
                m_Y = Y;
            }

            void CVector2D::Set(const Real X, const Real Y)
            {
                m_X = X;
                m_Y = Y;
            }

            void CVector2D::SetMaximalComponentValue(const Real X, const Real Y)
            {
                if (X > m_X)
                {
                    m_X = X;
                }
                if (Y > m_Y)
                {
                    m_Y = Y;
                }
            }

            void CVector2D::SetMinimalComponentValue(const Real X, const Real Y)
            {
                if (X < m_X)
                {
                    m_X = X;
                }
                if (Y < m_Y)
                {
                    m_Y = Y;
                }
            }

            void CVector2D::SetMaximalComponentValue(const CVector2D& Vector)
            {
                if (Vector.m_X > m_X)
                {
                    m_X = Vector.m_X;
                }
                if (Vector.m_Y > m_Y)
                {
                    m_Y = Vector.m_Y;
                }
            }

            void CVector2D::SetMinimalComponentValue(const CVector2D& Vector)
            {
                if (Vector.m_X < m_X)
                {
                    m_X = Vector.m_X;
                }
                if (Vector.m_Y < m_Y)
                {
                    m_Y = Vector.m_Y;
                }
            }

            void CVector2D::SetMidPoint(const CVector2D& A, const CVector2D& B)
            {
                m_X = (A.m_X + B.m_X) * Real(0.5);
                m_Y = (A.m_Y + B.m_Y) * Real(0.5);
            }

            void CVector2D::SetWeighted(const CVector2D& Vector, const Real Weight)
            {
                m_X = Vector.m_X * Weight;
                m_Y = Vector.m_Y * Weight;
            }

            void CVector2D::SetWeightedInverse(const CVector2D& Vector, const Real Weight)
            {
                m_X = Vector.m_X / Weight;
                m_Y = Vector.m_Y / Weight;
            }

            void CVector2D::SetLinealCombination(const CVector2D& A, const Real SA, const CVector2D& B, const Real SB)
            {
                m_X = A.m_X * SA + B.m_X * SB;
                m_Y = A.m_Y * SA + B.m_Y * SB;
            }

            void CVector2D::SetZero()
            {
                m_X = Real(0);
                m_Y = Real(0);
            }

            void CVector2D::Negate()
            {
                m_X = -m_X;
                m_Y = -m_Y;
            }

            void CVector2D::AddWeightedOffset(const CVector2D& Vector, const Real Weight)
            {
                m_X += Vector.m_X * Weight;
                m_Y += Vector.m_Y * Weight;
            }

            void CVector2D::AddOffset(const Real DX, const Real DY)
            {
                m_X += DX;
                m_Y += DY;
            }

            void CVector2D::AddOffsetX(const Real DX)
            {
                m_X += DX;
            }

            void CVector2D::AddOffsetY(const Real DY)
            {
                m_Y += DY;
            }

            void CVector2D::SubtractOffsetX(const Real DX)
            {
                m_X -= DX;
            }

            void CVector2D::SubtractOffsetY(const Real DY)
            {
                m_Y -= DY;
            }

            void CVector2D::AddCovariance(const CVector2D& Vector, Real CXX, Real CXY, Real CYY) const
            {
                const Real DX = Vector.m_X - m_X;
                const Real DY = Vector.m_Y - m_Y;
                CXX += DX * DX;
                CXY += DX * DY;
                CYY += DY * DY;
            }

            void CVector2D::AddWeightedCovariance(const CVector2D& Vector, const Real Weight, Real CXX, Real CXY, Real CYY) const
            {
                const Real DX = Vector.m_X - m_X;
                const Real DY = Vector.m_Y - m_Y;
                CXX += DX * DX * Weight;
                CXY += DX * DY * Weight;
                CYY += DY * DY * Weight;
            }

            bool CVector2D::IsNull() const
            {
                return IsNonPositive(std::abs(m_X) + std::abs(m_Y));
            }

            bool CVector2D::IsNonNull() const
            {
                return IsPositive(std::abs(m_X) + std::abs(m_Y));
            }

            bool CVector2D::IsAtInfinity() const
            {
                return RVL::IsAtInfinity(m_X) || RVL::IsAtInfinity(m_Y);
            }

            bool CVector2D::IsNotAtInfinity() const
            {
                return RVL::IsNotAtInfinity(m_X) && RVL::IsNotAtInfinity(m_Y);
            }

            bool CVector2D::IsUnitary() const
            {
                return IsOne(std::sqrt(m_X * m_X + m_Y * m_Y));
            }

            bool CVector2D::IsNonUnitary() const
            {
                return IsNonZero(std::sqrt(m_X * m_X + m_Y * m_Y) - Real(1));
            }

            Real CVector2D::GetX() const
            {
                return m_X;
            }

            Real CVector2D::GetY() const
            {
                return m_Y;
            }

            Real CVector2D::GetAngle() const
            {
                const Real Alpha = std::atan2(m_Y, m_X);
                return IsPositive(Alpha) ? Alpha : Alpha + g_Real2PI;
            }

            Real CVector2D::GetAngle(const CVector2D& Vector) const
            {
                return std::acos((m_X * Vector.m_X + m_Y * Vector.m_Y) / (std::sqrt(m_X * m_X + m_Y * m_Y) * std::sqrt(Vector.m_X * Vector.m_X + Vector.m_Y * Vector.m_Y)));
            }

            Real CVector2D::GetAperture(const CVector2D& Vector) const
            {
                return (m_X * Vector.m_X + m_Y * Vector.m_Y) / (std::sqrt(m_X * m_X + m_Y * m_Y) * std::sqrt(Vector.m_X * Vector.m_X + Vector.m_Y * Vector.m_Y));
            }

            Real CVector2D::GetAbsoluteAperture(const CVector2D& Vector) const
            {
                return std::abs((m_X * Vector.m_X + m_Y * Vector.m_Y) / (std::sqrt(m_X * m_X + m_Y * m_Y) * std::sqrt(Vector.m_X * Vector.m_X + Vector.m_Y * Vector.m_Y)));
            }

            Real CVector2D::GetDistance(const CVector2D& Vector) const
            {
                const Real DX = m_X - Vector.m_X;
                const Real DY = m_Y - Vector.m_Y;
                return std::sqrt(DX * DX + DY * DY);
            }

            Real CVector2D::GetLength() const
            {
                return std::sqrt(m_X * m_X + m_Y * m_Y);
            }

            Real CVector2D::GetSquareLength() const
            {
                return m_X * m_X + m_Y * m_Y;
            }

            Real CVector2D::Normalize()
            {
                const Real Length = hypot(m_X, m_Y);
                m_X /= Length;
                m_Y /= Length;
                return Length;
            }

            void CVector2D::ScaleAnisotropic(const Real SX, const Real SY)
            {
                m_X *= SX;
                m_Y *= SY;
            }

            const std::string CVector2D::ToString() const
            {
                std::ostringstream OutputString;
                OutputString.precision(g_RealDisplayDigits);
                OutputString << "<Vector2D X=\"" << m_X << "\" Y=\"" << m_Y << "\"/>";
                return OutputString.str();
            }

            Real CVector2D::ScalarProduct(const CVector2D& Vector) const
            {
                return m_X * Vector.m_X + m_Y * Vector.m_Y;
            }

            Real CVector2D::ScalarProduct(const Real X, const Real Y) const
            {
                return m_X * X + m_Y * Y;
            }

            Real CVector2D::GetSquareDistance(const CVector2D& Vector) const
            {
                const Real DX = m_X - Vector.m_X;
                const Real DY = m_Y - Vector.m_Y;
                return DX * DX + DY * DY;
            }

            void CVector2D::operator=(const CVector2D& Vector)
            {
                m_X = Vector.m_X;
                m_Y = Vector.m_Y;
            }

            void CVector2D::operator+=(const CVector2D& Vector)
            {
                m_X += Vector.m_X;
                m_Y += Vector.m_Y;
            }

            void CVector2D::operator-=(const CVector2D& Vector)
            {
                m_X -= Vector.m_X;
                m_Y -= Vector.m_Y;
            }

            void CVector2D::operator*=(const Real Scalar)
            {
                m_X *= Scalar;
                m_Y *= Scalar;
            }

            void CVector2D::operator/=(const Real Scalar)
            {
                m_X /= Scalar;
                m_Y /= Scalar;
            }

            bool CVector2D::operator==(const CVector2D& Vector) const
            {
                return Equals(m_X, Vector.m_X) && Equals(m_Y, Vector.m_Y);
            }

            bool CVector2D::operator!=(const CVector2D& Vector) const
            {
                return NonEquals(m_X, Vector.m_X) || NonEquals(m_Y, Vector.m_Y);
            }

            CVector2D operator/(const CVector2D& Vector, const Real Scalar)
            {
                return CVector2D(Vector.GetX() / Scalar, Vector.GetY() / Scalar);
            }

            CVector2D operator*(const CVector2D& Vector, const Real Scalar)
            {
                return CVector2D(Vector.GetX() * Scalar, Vector.GetY() * Scalar);
            }

            CVector2D operator*(const Real Scalar, const CVector2D& Vector)
            {
                return CVector2D(Vector.GetX() * Scalar, Vector.GetY() * Scalar);
            }

            CVector2D operator+(const CVector2D& A, const CVector2D& B)
            {
                return CVector2D(A.GetX() + B.GetX(), A.GetY() + B.GetY());
            }

            CVector2D operator-(const CVector2D& A, const CVector2D& B)
            {
                return CVector2D(A.GetX() - B.GetX(), A.GetY() - B.GetY());
            }
        }
    }
}
