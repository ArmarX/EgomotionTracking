/*
 * NormalDistribution.cpp
 */

#include "NormalDistribution.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _1D
        {
            CNormalDistribution::CNormalDistribution()
                = default;

            CNormalDistribution::~CNormalDistribution()
                = default;

            Real CNormalDistribution::DetermineNonNormalizedStandardDeviation(const Real Density, const Real X)
            {
                return std::sqrt((X * X) / (Real(-2) * std::log(Density)));
            }

            Real CNormalDistribution::DetermineDeviationAtDensity(const Real Density, const Real StandardDeviation)
            {
                return std::sqrt(std::log(Density) * (Real(-2)) * StandardDeviation * StandardDeviation);
            }

            Real CNormalDistribution::DetermineExponentFactor(const Real StandardDeviation)
            {
                return Real(-1) / (Real(2) * StandardDeviation * StandardDeviation);
            }

            Real CNormalDistribution::DetermineCoefficient(const Real StandardDeviation)
            {
                return Real(1) / std::sqrt(g_Real2PI * StandardDeviation * StandardDeviation);
            }

            Real CNormalDistribution::DetermineNormalizedDensity(const Real X, const Real Mean, const Real StandardDeviation)
            {
                const Real Deviation = X - Mean;
                const Real Variance = StandardDeviation * StandardDeviation;
                return std::exp((Deviation * Deviation) / (Real(-2) * Variance)) / std::sqrt(g_Real2PI * Variance);
            }

            Real CNormalDistribution::DetermineNonNormalizedDensity(const Real X, const Real Mean, const Real StandardDeviation)
            {
                const Real Deviation = X - Mean;
                return std::exp((Deviation * Deviation) / (Real(-2) * StandardDeviation * StandardDeviation));
            }

            Real CNormalDistribution::DetermineBandWidthByStandardDeviation(const Real StandardDeviation, const int TotalSamples)
            {
                return std::pow((Real(4) / Real(3 * TotalSamples)) * std::pow(StandardDeviation, Real(5)), Real(1) / Real(5));
            }
        }
    }
}
