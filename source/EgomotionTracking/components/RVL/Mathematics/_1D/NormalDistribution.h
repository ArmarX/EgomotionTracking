/*
 * NormalDistribution.h
 */

#pragma once

#include "../../Common/DataTypes.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _1D
        {
            class CNormalDistribution
            {
            public:

                static Real DetermineNonNormalizedStandardDeviation(const Real Density, const Real X);
                static Real DetermineDeviationAtDensity(const Real Density, const Real StandardDeviation);
                static Real DetermineExponentFactor(const Real StandardDeviation);
                static Real DetermineCoefficient(const Real StandardDeviation);
                static Real DetermineNormalizedDensity(const Real X, const Real Mean, const Real StandardDeviation);
                static Real DetermineNonNormalizedDensity(const Real X, const Real Mean, const Real StandardDeviation);
                static Real DetermineBandWidthByStandardDeviation(const Real StandardDeviation, const int TotalSamples);

            protected:

                CNormalDistribution();
                ~CNormalDistribution();
            };
        }
    }
}

