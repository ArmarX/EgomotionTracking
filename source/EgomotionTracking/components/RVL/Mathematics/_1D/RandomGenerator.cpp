/*
 * RandomGenerator.cpp
 */

#include "RandomGenerator.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _1D
        {
            const Real CRandomGenerator::s_Normalization = Real(1) / Real(RAND_MAX);

            CRandomGenerator::CRandomGenerator()
                = default;

            CRandomGenerator::~CRandomGenerator()
                = default;

            void CRandomGenerator::InitializeSeed(const int RandomSeed)
            {
                srand(RandomSeed);
            }

            Real CRandomGenerator::GenerateRandomValue(const int RandomSeed)
            {
                if (RandomSeed)
                {
                    InitializeSeed(RandomSeed);
                }
                return Real(rand()) * CRandomGenerator::s_Normalization;
            }

            Byte CRandomGenerator::GenerateRandomValue(const Byte A, const Byte B, const int RandomSeed)
            {
                const Byte Upper = std::max(A, B);
                const Byte Lower = std::min(A, B);
                return Byte(std::round((Upper - Lower) * GenerateRandomValue(RandomSeed))) + Lower;
            }

            int CRandomGenerator::GenerateRandomValue(const int A, const int B, const int RandomSeed)
            {
                const int Upper = std::max(A, B);
                const int Lower = std::min(A, B);
                return int(std::round((Upper - Lower) * GenerateRandomValue(RandomSeed))) + Lower;
            }

            Real CRandomGenerator::GenerateRandomValue(const Real A, const Real B, const int RandomSeed)
            {
                const Real Upper = std::max(A, B);
                const Real Lower = std::min(A, B);
                return (Upper - Lower) * GenerateRandomValue(RandomSeed) + Lower;
            }
        }
    }
}
