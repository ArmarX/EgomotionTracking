/*
 * ExponentialLookupTable.h
 */

#pragma once

#include "../../../Common/DataTypes.h"
#include "../../../Console/ConsoleOutputManager.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _1D
        {
            namespace LookupTables
            {
                class CExponentialLookupTable
                {
                public:

                    static const Real* GetTable();
                    static void Initialize(const Real MinimalDensity, const int SamplesPerUnit);

                    static Real DirectEvaluate(const Real X);
                    static Real LinearEvaluate(const Real X);

                    static bool IsReady();
                    static Real GetMaximalArgument();
                    static Real GetMinimalArgument();
                    static Real GetSamplesPerUnit();

                    static void Clear();

                protected:

                    CExponentialLookupTable();
                    ~CExponentialLookupTable();

                    void Set(const int ZeroOffset, const Real MaximalArgument, const Real MinimalArgument, const Real SamplesPerUnit, const Real* pLUT);

                    static bool s_Ready;
                    static int s_ZeroOffset;
                    static Real s_MaximalArgument;
                    static Real s_MinimalArgument;
                    static Real s_SamplesPerUnit;
                    static const Real* s_pLUT;
                };
            }
        }
    }
}

