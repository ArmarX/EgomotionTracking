/*
 * ExponentialLookupTable.cpp
 */

#include "ExponentialLookupTable.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _1D
        {
            namespace LookupTables
            {
                bool CExponentialLookupTable::s_Ready = false;
                int CExponentialLookupTable::s_ZeroOffset = 0;
                Real CExponentialLookupTable::s_MaximalArgument = Real(0);
                Real CExponentialLookupTable::s_MinimalArgument = Real(0);
                Real CExponentialLookupTable::s_SamplesPerUnit = Real(0);
                const Real* CExponentialLookupTable::s_pLUT = nullptr;

                CExponentialLookupTable::CExponentialLookupTable()
                    = default;

                CExponentialLookupTable::~CExponentialLookupTable()
                    = default;

                const Real* CExponentialLookupTable::GetTable()
                {
                    if (!s_Ready)
                    {
                        Initialize(Real(0.0001), 1000);
                    }
                    return s_pLUT;
                }

                void CExponentialLookupTable::Initialize(const Real MinimalDensity, const int SamplesPerUnit)
                {
                    Clear();
                    const Real MaximalArgument = -std::log(MinimalDensity);
                    const int SamplesPerDirection = int(std::ceil(MaximalArgument * SamplesPerUnit));
                    const int TotalSamples = (SamplesPerDirection + 1) * 2 + 1;
                    try
                    {
                        Real* pLUT = new Real[TotalSamples];
                        Real* pPlusLUT = pLUT + SamplesPerDirection;
                        Real* pMinusLUT = pPlusLUT;
                        const Real ArgumentResolution = Real(1) / SamplesPerUnit;
                        for (int i = 0 ; i < int(SamplesPerDirection) ; ++i)
                        {
                            pPlusLUT[i] = std::exp(ArgumentResolution * i);
                            pMinusLUT[-i] = std::exp(ArgumentResolution * (-i));
                        }
                        s_Ready = true;
                        s_ZeroOffset = SamplesPerDirection;
                        s_MaximalArgument = MaximalArgument;
                        s_MinimalArgument = -MaximalArgument;
                        s_SamplesPerUnit = SamplesPerUnit;
                        s_pLUT = pLUT;

                    }
                    catch (std::bad_alloc& Exception)
                    {
                        std::ostringstream ExceptionInformation;
                        ExceptionInformation << _RVL_HEAD_FULL_INFORMATION_ << _RVL_CONSOLE_CURRENT_PREFIX_LEVEL_ << Exception.what();
                        _RVL_CONSOLE_OUTPUT_EXECEPTIONS_(ExceptionInformation, 0);
                    }
                }

                Real CExponentialLookupTable::DirectEvaluate(const Real X)
                {
                    if ((X < s_MinimalArgument) || (X > s_MaximalArgument))
                    {
                        return std::exp(X);
                    }
                    return s_pLUT[int(std::round(X * s_SamplesPerUnit)) + s_ZeroOffset];
                }

                Real CExponentialLookupTable::LinearEvaluate(const Real X)
                {
                    if ((X < s_MinimalArgument) || (X > s_MaximalArgument))
                    {
                        return std::exp(X);
                    }
                    const Real ContinousIndex = X * s_SamplesPerUnit;
                    const int A = std::floor(ContinousIndex);
                    const Real FA = s_pLUT[A + s_ZeroOffset];
                    return (s_pLUT[int(std::ceil(ContinousIndex)) + s_ZeroOffset] - FA) * (ContinousIndex - A) + FA;
                }

                bool CExponentialLookupTable::IsReady()
                {
                    return s_Ready;
                }

                Real CExponentialLookupTable::GetMaximalArgument()
                {
                    return s_MaximalArgument;
                }

                Real CExponentialLookupTable::GetMinimalArgument()
                {
                    return s_MinimalArgument;
                }

                Real CExponentialLookupTable::GetSamplesPerUnit()
                {
                    return s_SamplesPerUnit;
                }

                void CExponentialLookupTable::Clear()
                {
                    if (s_Ready)
                    {
                        delete[] s_pLUT;
                        s_pLUT = nullptr;
                        s_Ready = false;
                        s_MaximalArgument = Real(0);
                        s_MinimalArgument = Real(0);
                        s_SamplesPerUnit = Real(0);
                    }
                }
            }
        }
    }
}
