/*
 * RandomGenerator.h
 */

#pragma once

#include "../../Common/DataTypes.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _1D
        {
            class CRandomGenerator
            {
            public:

                static void InitializeSeed(const int RandomSeed);
                static Real GenerateRandomValue(const int RandomSeed);
                static Byte GenerateRandomValue(const Byte A, const Byte B, const int RandomSeed = 0);
                static int GenerateRandomValue(const int A, const int B, const int RandomSeed = 0);
                static Real GenerateRandomValue(const Real A, const Real B, const int RandomSeed = 0);

            protected:

                CRandomGenerator();
                ~CRandomGenerator();

                static const Real s_Normalization;
            };
        }
    }
}

