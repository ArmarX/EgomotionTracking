/*
 * MatrixND.cpp
 */

#include "MatrixND.h"

namespace RVL
{
    namespace Mathematics
    {
        namespace _ND
        {

            CMatrixND CreateZeroMatrix(const int Rows, const int Cols)
            {
                CMatrixND ZeroMatrix(Rows, Cols);
                ZeroMatrix.setZero();
                return ZeroMatrix;
            }

            /*
             CDiagonalMatrixND S;
             CMatrixND U , V;
             SVD(A,S,U,V);
             const int TotalRows = S.Nrows();
             CMatrixND DPI = CreateZeroMatrix(TotalRows,TotalRows);
             for(int i = 1 ; i <= TotalRows ; ++i)
             if (std::abs(S(i,i)) > std::numeric_limits<MatrixReal>::epsilon())
             DPI(i,i) = MatrixReal(1) / S(i,i);
             return V * DPI * U.t();
            */

            const CMatrixND PseudoInverseSVD(const CMatrixND& A)
            {
                const int TotalRows = A.rows();

                Eigen::JacobiSVD<Eigen::Matrix<MatrixReal, Eigen::Dynamic, Eigen::Dynamic> > SVD(A, Eigen::ComputeFullU | Eigen::ComputeFullV);

                Eigen::Matrix<MatrixReal, Eigen::Dynamic, Eigen::Dynamic> S = SVD.singularValues();

                CMatrixND DPI(TotalRows, TotalRows);

                DPI.setZero();

                for (int i = 0 ; i < TotalRows ; ++i)
                {
                    const double Value = (double) std::abs((long double)S(i, i));
                    if (Value > std::numeric_limits<MatrixReal>::epsilon())
                    {
                        DPI(i, i) = MatrixReal(1.0) / Value;
                    }
                }

                return SVD.matrixV() * DPI * SVD.matrixU().transpose();
            }

            std::string DisplayMatrix(const std::string& Name, const CMatrixND& A)
            {
                const int Rows = A.rows();
                const int Cols = A.cols();
                std::ostringstream OutputText;
                OutputText.precision(std::numeric_limits<MatrixReal>::digits10);
                OutputText << "\t\n" << Name << "\n";
                for (int r = 0 ; r < Rows ; ++r)
                {
                    OutputText << "|\t";
                    for (int c = 0 ; c < Cols ; ++c)
                    {
                        OutputText << A(r, c) << "\t";
                    }
                    OutputText << "|\n";
                }
                OutputText << "\n";
                return OutputText.str();
            }

            /*
             //---------------------------------------------------------------
             CMatrixND CreateZeroMatrix(const int Rows, const int Cols)
             {
             CMatrixND ZeroMatrix(Rows,Cols);
             for(int r = 1 ; r <= Rows ; ++r)
             for(int c = 1 ; c <= Cols ; ++c)
             ZeroMatrix(r,c) = MatrixReal(0);
             return ZeroMatrix;
             }

             const CMatrixND PseudoInverseSVD(const CMatrixND& A)
             {
             CDiagonalMatrixND S;
             CMatrixND U , V;
             SVD(A,S,U,V);
             const int TotalRows = S.Nrows();
             CMatrixND DPI = CreateZeroMatrix(TotalRows,TotalRows);
             for(int i = 1 ; i <= TotalRows ; ++i)
             if (std::abs(S(i,i)) > std::numeric_limits<MatrixReal>::epsilon())
             DPI(i,i) = MatrixReal(1) / S(i,i);
             return V * DPI * U.t();
             }

             std::string DisplayMatrix(const std::string& Name, const CMatrixND& A)
             {
             const int Rows = A.Nrows();
             const int Cols = A.Ncols();
             std::ostringstream OutputText;
             OutputText.precision(std::numeric_limits<MatrixReal>::digits10);
             OutputText << "\t\n" << Name << "\n";
             for(int r = 1 ; r <= Rows ; ++r)
             {
             OutputText << "|\t";
             for(int c = 1 ; c <= Cols ; ++c)
             OutputText << A(r,c) << "\t";
             OutputText << "|\n";
             }
             OutputText << "\n";
             return OutputText.str();
             }
             //---------------------------------------------------------------
             */
        }
    }
}
