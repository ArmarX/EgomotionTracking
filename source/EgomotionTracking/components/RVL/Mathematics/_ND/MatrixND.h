/*
 * MatrixND.h
 */

#pragma once

#include "../../Common/DataTypes.h"

// Eigen
#include <eigen3/Eigen/Eigen>

namespace RVL
{
    namespace Mathematics
    {
        namespace _ND
        {
            typedef double MatrixReal;
            typedef Eigen::Matrix<MatrixReal, Eigen::Dynamic, Eigen::Dynamic> CMatrixND;
            //typedef NEWMAT::DiagonalMatrix CDiagonalMatrixND;
            //typedef NEWMAT::BaseException CMatrixBaseException;

            CMatrixND CreateZeroMatrix(const int Rows, const int Cols);
            const CMatrixND PseudoInverseSVD(const CMatrixND& A);
            std::string DisplayMatrix(const std::string& Name, const CMatrixND& A);

        }
    }
}

