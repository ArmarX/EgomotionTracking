/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE VisionX::RVL::IVLoaderTest
#define ARMARX_BOOST_TEST
#include <EgomotionTracking/Test.h>
#include "../Representation/ModelBased/GeometricGraph/Loaders/OpenInventor/OpenInventorModelLoader.h"


using namespace armarx;
using namespace RVL::Representation::ModelBased::GeometricGraph::Loaders::OpenInventor;
using namespace RVL::Representation::ModelBased::GeometricGraph::Loaders;



BOOST_AUTO_TEST_CASE(IVLoaderTest2_1)
{
    COpenInventorModelLoader loader;
    loader.LoadFromFile("Schubladenscrank-IIT.iv", CModelLoader::eMeters);


}




