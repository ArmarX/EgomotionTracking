/*
 * Color.h
 */

#pragma once

namespace RVL
{
    namespace Visualization
    {
        struct RGBAColor
        {
            RGBAColor()
            {
                m_Red = 0;
                m_Green = 0;
                m_Blue = 0;
                m_Alpha = 0;
            }

            RGBAColor(const Byte Red, const Byte Green, const Byte Blue, const Byte Alpha)
            {
                m_Red = Red;
                m_Green = Green;
                m_Blue = Blue;
                m_Alpha = Alpha;
            }

            void Set(const Byte Red, const Byte Green, const Byte Blue, const Byte Alpha)
            {
                m_Red = Red;
                m_Green = Green;
                m_Blue = Blue;
                m_Alpha = Alpha;
            }

            void SetRed(const Byte Red)
            {
                m_Red = Red;
            }

            void SetGreen(const Byte Green)
            {
                m_Green = Green;
            }

            void SetBlue(const Byte Blue)
            {
                m_Blue = Blue;
            }

            void SetAlpha(const Byte Alpha)
            {
                m_Blue = Alpha;
            }

            bool IsTransparent() const
            {
                return !m_Alpha;
            }

            bool IsTranslucent() const
            {
                return m_Alpha < 255;
            }

            bool IsOpaque() const
            {
                return (m_Alpha == 255);
            }

            Real GetNormalizedRed() const
            {
                return Real(m_Red) * Real(0.003921568627450980392157);
            }

            Real GetNormalizedGreen() const
            {
                return Real(m_Green) * Real(0.003921568627450980392157);
            }

            Real GetNormalizedBlue() const
            {
                return Real(m_Blue) * Real(0.003921568627450980392157);
            }

            Real GetNormalizedAlpha() const
            {
                return Real(m_Alpha) * Real(0.003921568627450980392157);
            }

            Byte m_Red;
            Byte m_Green;
            Byte m_Blue;
            Byte m_Alpha;

        };

        const RGBAColor g_RGBA_Black(0, 0, 0, 255);
        const RGBAColor g_RGBA_Red(255, 0, 0, 255);
        const RGBAColor g_RGBA_Green(0, 255, 0, 255);
        const RGBAColor g_RGBA_Blue(0, 0, 255, 255);
        const RGBAColor g_RGBA_Yellow(255, 255, 0, 255);
        const RGBAColor g_RGBA_Cyan(0, 255, 255, 255);
        const RGBAColor g_RGBA_Magenta(255, 0, 255, 255);
        const RGBAColor g_RGBA_Silver(192, 192, 192, 255);
        const RGBAColor g_RGBA_Gray(128, 128, 128, 255);
        const RGBAColor g_RGBA_White(255, 255, 255, 255);
    }
}

