/*
 * SVGPolyLine.cpp
 */

#include "SVGPolyLine.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    CSVGPolyLine::CSVGPolyLine(const std::list<Mathematics::_2D::CVector2D>& Points, const RGBAColor& StrokeColor, const Real StrockWidth, const int CurrentStyleFlags) :
                        CSVGPrimitive(CurrentStyleFlags),
                        m_Points(Points),
                        m_StrokeColor(StrokeColor),
                        m_StrokeWidth(StrockWidth)
                    {
                    }

                    CSVGPolyLine::CSVGPolyLine(const RGBAColor& StrokeColor, const Real StrockWidth, const int CurrentStyleFlags) :
                        CSVGPrimitive(CurrentStyleFlags),
                        m_Points(),
                        m_StrokeColor(StrokeColor),
                        m_StrokeWidth(StrockWidth)
                    {
                    }

                    CSVGPolyLine::~CSVGPolyLine()
                        = default;

                    void CSVGPolyLine::AddPoint(const Mathematics::_2D::CVector2D& X)
                    {
                        m_Points.push_back(X);
                    }

                    void CSVGPolyLine::SetDashing(const std::vector<Real>& Dashing)
                    {
                        m_Dashing = Dashing;
                    }

                    const std::string CSVGPolyLine::ToString() const
                    {
                        std::ostringstream OutputStream;
                        OutputStream.precision(s_DisplayDigits);
                        OutputStream << "<polyline points=\"";
                        std::list<Mathematics::_2D::CVector2D>::const_iterator pPoint = m_Points.begin();
                        while (pPoint != m_Points.end())
                        {
                            OutputStream << pPoint->GetX() << "," << pPoint->GetY();
                            if (++pPoint != m_Points.end())
                            {
                                OutputStream << " ";
                            }
                        }
                        OutputStream << "\" style=\"";
                        if (m_Dashing.size())
                        {
                            OutputStream << DashingToString(m_Dashing);
                        }
                        OutputStream << "stroke:rgb(" << int(m_StrokeColor.m_Red) << "," << int(m_StrokeColor.m_Green) << "," << int(m_StrokeColor.m_Blue) << ");";
                        if (m_StrokeColor.IsTranslucent())
                        {
                            OutputStream << "stroke-opacity:" << m_StrokeColor.GetNormalizedAlpha() << ";";
                        }
                        OutputStream << "stroke-width:" << m_StrokeWidth << ";stroke-linecap:";
                        switch ((m_StyleFlags & 0X70))
                        {
                            case eLinecapButt:
                                OutputStream << "butt";
                                break;
                            case eLineSquare:
                                OutputStream << "square";
                                break;
                            case eLinecapRound:
                            default:
                                OutputStream << "round";
                                break;
                        }
                        OutputStream << ";\"/>";
                        return OutputStream.str();
                    }

                    void CSVGPolyLine::Serialize(std::ostringstream& OutputStream) const
                    {
                        if (m_IsActive && (m_Points.size() > 1))
                        {
                            OutputStream << ToString();
                        }
                    }
                }
            }
        }
    }
}
