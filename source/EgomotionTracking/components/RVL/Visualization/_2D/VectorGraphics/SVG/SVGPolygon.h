/*
 * SVGPolygon.h
 */

#pragma once

#include "SVGPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    class CSVGPolygon : public CSVGPrimitive
                    {
                    public:

                        CSVGPolygon(const std::list<Mathematics::_2D::CVector2D>& Points, const RGBAColor& StrokeColor, const Real StrockWidth, const RGBAColor& FillColor, const int CurrentStyleFlags);
                        ~CSVGPolygon() override;

                        void SetDashing(const std::vector<Real>& Dashing);
                        const std::string ToString() const override;
                        void Serialize(std::ostringstream& OutputStream) const override;

                    protected:

                        std::list<Mathematics::_2D::CVector2D> m_Points;
                        RGBAColor m_StrokeColor;
                        Real m_StrokeWidth;
                        RGBAColor m_FillColor;
                        std::vector<Real> m_Dashing;
                    };
                }
            }
        }
    }
}

