/*
 * SVGCircle.h
 */

#pragma once

#include "SVGPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    class CSVGCircle : public CSVGPrimitive
                    {
                    public:

                        CSVGCircle(const Mathematics::_2D::CVector2D& Center, const Real Radius, const RGBAColor& StrokeColor, const Real StrockWidth, const RGBAColor& FillColor, const int CurrentStyleFlags);
                        CSVGCircle(const int CX, const int CY, const Real Radius, const RGBAColor& StrokeColor, const Real StrockWidth, const RGBAColor& FillColor, const int CurrentStyleFlags);
                        ~CSVGCircle() override;

                        void SetDashing(const std::vector<Real>& Dashing);
                        const std::string ToString() const override;
                        void Serialize(std::ostringstream& OutputStream) const override;

                    protected:

                        Mathematics::_2D::CVector2D m_Center;
                        Real m_Radius;
                        RGBAColor m_StrokeColor;
                        Real m_StrokeWidth;
                        RGBAColor m_FillColor;
                        std::vector<Real> m_Dashing;
                    };
                }
            }
        }
    }
}

