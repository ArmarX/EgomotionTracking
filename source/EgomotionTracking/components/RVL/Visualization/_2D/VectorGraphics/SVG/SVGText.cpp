/*
 * SVGText.cpp
 */

#include "SVGText.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    CSVGText::CSVGText(const std::string& Text, const Mathematics::_2D::CVector2D& LeftUpperPoint, const RGBAColor& TextColor, const Real FontSize) :
                        CSVGPrimitive(eNone),
                        m_Text(Text),
                        m_LeftUpperPoint(LeftUpperPoint),
                        m_TextColor(TextColor),
                        m_FontSize(FontSize)
                    {
                    }

                    CSVGText::~CSVGText()
                        = default;

                    const std::string CSVGText::ToString() const
                    {
                        std::ostringstream OutputStream;
                        OutputStream.precision(s_DisplayDigits);
                        OutputStream << "<text x=\"" << m_LeftUpperPoint.GetX() << "\" y=\"" << m_LeftUpperPoint.GetY() << "\" style=\"font-family:Arial;font-size:" << m_FontSize << ";fill:rgb(" << int(m_TextColor.m_Red) << "," << int(m_TextColor.m_Green) << "," << int(m_TextColor.m_Blue) << ");";
                        if (m_TextColor.IsTranslucent())
                        {
                            OutputStream << "fill-opacity:" << m_TextColor.GetNormalizedAlpha() << ";";
                        }
                        OutputStream << "\">" << m_Text << "</text>";
                        return OutputStream.str();
                    }

                    void CSVGText::Serialize(std::ostringstream& OutputStream) const
                    {
                        if (m_IsActive && m_Text.length() && m_LeftUpperPoint.IsNotAtInfinity() && IsPositive(m_FontSize))
                        {
                            OutputStream << ToString();
                        }
                    }
                }
            }
        }
    }
}
