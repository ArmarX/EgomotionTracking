/*
 * SVGGroup.h
 */

#pragma once

#include "SVGPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    class CSVGGroup : public CSVGPrimitive
                    {
                    public:

                        CSVGGroup();
                        ~CSVGGroup() override;

                        CSVGGroup* CreateSubGroup();
                        bool AddElement(const CSVGPrimitive* pSVGPrimitive);
                        void ClearElements();
                        int GetTotalElements() const;
                        void SetRotatio(const Real AlphaRadians);
                        void SetTranslation(Mathematics::_2D::CVector2D& Translation);
                        const std::string ToString() const override;
                        void Serialize(std::ostringstream& OutputStream) const override;

                    protected:

                        const std::string ToString(const bool CheckInternal) const;

                        Real m_AlphaRadians;
                        Mathematics::_2D::CVector2D m_Translation;
                        std::set<const CSVGPrimitive*> m_Elements;
                    };
                }
            }
        }
    }
}

