/*
 * SVGLine.cpp
 */

#include "SVGLine.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    CSVGLine::CSVGLine(const Mathematics::_2D::CVector2D& A, const Mathematics::_2D::CVector2D& B, const RGBAColor& StrokeColor, const Real StrockWidth, const int CurrentStyleFlags) :
                        CSVGPrimitive(CurrentStyleFlags),
                        m_A(A),
                        m_B(B),
                        m_StrokeColor(StrokeColor),
                        m_StrokeWidth(StrockWidth)
                    {
                    }

                    CSVGLine::CSVGLine(const int AX, const int AY, const int BX, const int BY, const RGBAColor& StrokeColor, const Real StrockWidth, const int CurrentStyleFlags) :
                        CSVGPrimitive(CurrentStyleFlags),
                        m_A(AX, AY),
                        m_B(BX, BY),
                        m_StrokeColor(StrokeColor),
                        m_StrokeWidth(StrockWidth)
                    {
                    }

                    CSVGLine::~CSVGLine()
                        = default;

                    void CSVGLine::SetDashing(const std::vector<Real>& Dashing)
                    {
                        m_Dashing = Dashing;
                    }

                    const std::string CSVGLine::ToString() const
                    {
                        std::ostringstream OutputStream;
                        OutputStream.precision(s_DisplayDigits);
                        OutputStream << "<line x1=\"" << m_A.GetX() << "\" y1=\"" << m_A.GetY() << "\" x2=\"" << m_B.GetX() << "\" y2=\"" << m_B.GetY() << "\" style=\"";
                        if (m_Dashing.size())
                        {
                            OutputStream << DashingToString(m_Dashing);
                        }
                        OutputStream << "stroke:rgb(" << int(m_StrokeColor.m_Red) << "," << int(m_StrokeColor.m_Green) << "," << int(m_StrokeColor.m_Blue) << ");";
                        if (m_StrokeColor.IsTranslucent())
                        {
                            OutputStream << "stroke-opacity:" << m_StrokeColor.GetNormalizedAlpha() << ";";
                        }
                        OutputStream << "stroke-width:" << m_StrokeWidth << ";stroke-linecap:";
                        switch ((m_StyleFlags & 0X70))
                        {
                            case eLinecapButt:
                                OutputStream << "butt";
                                break;
                            case eLineSquare:
                                OutputStream << "square";
                                break;
                            case eLinecapRound:
                            default:
                                OutputStream << "round";
                                break;
                        }
                        OutputStream << ";\"/>";
                        return OutputStream.str();
                    }

                    void CSVGLine::Serialize(std::ostringstream& OutputStream) const
                    {
                        if (m_IsActive && (m_A != m_B))
                        {
                            OutputStream << ToString();
                        }
                    }
                }
            }
        }
    }
}
