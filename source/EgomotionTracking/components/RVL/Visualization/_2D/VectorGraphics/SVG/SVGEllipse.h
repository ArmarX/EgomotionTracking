/*
 * SVGEllipse.h
 */

#pragma once

#include "SVGPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    class CSVGEllipse : public CSVGPrimitive
                    {
                    public:

                        CSVGEllipse(const Mathematics::_2D::CVector2D& Center, const Real RadiusX, const Real RadiusY, const Real AlphaRadians, const RGBAColor& StrokeColor, const Real StrockWidth, const RGBAColor& FillColor, const int CurrentStyleFlags);
                        ~CSVGEllipse() override;

                        void SetDashing(const std::vector<Real>& Dashing);
                        const std::string ToString() const override;
                        void Serialize(std::ostringstream& OutputStream) const override;

                    protected:

                        Mathematics::_2D::CVector2D m_Center;
                        Real m_RadiusX;
                        Real m_RadiusY;
                        Real m_AlphaRadians;
                        RGBAColor m_StrokeColor;
                        Real m_StrokeWidth;
                        RGBAColor m_FillColor;
                        std::vector<Real> m_Dashing;
                    };
                }
            }
        }
    }
}

