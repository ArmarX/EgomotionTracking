/*
 * SVGGroup.cpp
 */

#include "SVGGroup.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    CSVGGroup::CSVGGroup() :
                        CSVGPrimitive(eNone),
                        m_AlphaRadians(Real(0)),
                        m_Translation()
                    {
                    }

                    CSVGGroup::~CSVGGroup()
                    {
                        ClearElements();
                    }

                    CSVGGroup* CSVGGroup::CreateSubGroup()
                    {
                        CSVGGroup* pSubGroup = new CSVGGroup();
                        m_Elements.insert(pSubGroup);
                        return pSubGroup;
                    }

                    bool CSVGGroup::AddElement(const CSVGPrimitive* pSVGPrimitive)
                    {
                        if (pSVGPrimitive && (pSVGPrimitive != this) && (m_Elements.find(pSVGPrimitive) == m_Elements.end()))
                        {
                            m_Elements.insert(pSVGPrimitive);
                            return true;
                        }
                        return false;
                    }

                    void CSVGGroup::ClearElements()
                    {
                        for (auto m_Element : m_Elements)
                        {
                            delete m_Element;
                        }
                        m_Elements.clear();
                    }

                    int CSVGGroup::GetTotalElements() const
                    {
                        return m_Elements.size();
                    }

                    void CSVGGroup::SetRotatio(const Real AlphaRadians)
                    {
                        m_AlphaRadians = AlphaRadians;
                    }

                    void CSVGGroup::SetTranslation(Mathematics::_2D::CVector2D& Translation)
                    {
                        m_Translation = Translation;
                    }

                    const std::string CSVGGroup::ToString() const
                    {
                        if (m_Elements.size())
                        {
                            return ToString(false);
                        }
                        return std::string();
                    }

                    void CSVGGroup::Serialize(std::ostringstream& OutputStream) const
                    {
                        if (m_IsActive && m_Elements.size())
                        {
                            OutputStream << ToString(true);
                        }
                    }

                    const std::string CSVGGroup::ToString(const bool CheckInternal) const
                    {
                        std::ostringstream OutputStream;
                        OutputStream.precision(s_DisplayDigits);
                        const bool TranslationFlag = m_Translation.IsNonNull();
                        const bool RotationFlag = IsNonZero(m_AlphaRadians);
                        if (TranslationFlag && RotationFlag)
                        {
                            OutputStream << "<g transform=\"translate(" << m_Translation.GetX() << "," << m_Translation.GetY() << "):rotate(" << AngleInDegrees(m_AlphaRadians) << ")\">";
                        }
                        else if (TranslationFlag)
                        {
                            OutputStream << "<g transform=\"translate(" << m_Translation.GetX() << "," << m_Translation.GetY() << ")\">";
                        }
                        else if (RotationFlag)
                        {
                            OutputStream << "<g transform=\"rotate(" << AngleInDegrees(m_AlphaRadians) << ")\">";
                        }
                        else
                        {
                            OutputStream << "<g>";
                        }
                        if (CheckInternal)
                            for (auto m_Element : m_Elements)
                            {
                                m_Element->Serialize(OutputStream);
                            }
                        else
                            for (auto m_Element : m_Elements)
                            {
                                OutputStream << m_Element->ToString();
                            }
                        OutputStream << "</g>";
                        return OutputStream.str();
                    }
                }
            }
        }
    }
}
