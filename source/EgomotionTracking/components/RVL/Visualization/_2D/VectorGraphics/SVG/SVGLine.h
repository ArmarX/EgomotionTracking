/*
 * SVGLine.h
 */

#pragma once

#include "SVGPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    class CSVGLine : public CSVGPrimitive
                    {
                    public:

                        CSVGLine(const Mathematics::_2D::CVector2D& A, const Mathematics::_2D::CVector2D& B, const RGBAColor& StrokeColor, const Real StrockWidth, const int CurrentStyleFlags);
                        CSVGLine(const int AX, const int AY, const int BX, const int BY, const RGBAColor& StrokeColor, const Real StrockWidth, const int CurrentStyleFlags);
                        ~CSVGLine() override;

                        void SetDashing(const std::vector<Real>& Dashing);
                        const std::string ToString() const override;
                        void Serialize(std::ostringstream& OutputStream) const override;

                    protected:

                        Mathematics::_2D::CVector2D m_A;
                        Mathematics::_2D::CVector2D m_B;
                        RGBAColor m_StrokeColor;
                        Real m_StrokeWidth;
                        std::vector<Real> m_Dashing;
                    };
                }
            }
        }
    }
}

