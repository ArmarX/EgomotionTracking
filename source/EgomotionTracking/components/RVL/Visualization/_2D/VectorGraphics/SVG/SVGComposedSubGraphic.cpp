/*
 * SVGComposedSubGraphic.cpp
 */

#include "SVGComposedSubGraphic.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    CSVGComposedSubGraphic::CSVGComposedSubGraphic() :
                        CSVGPrimitive(eNone)
                    {
                    }

                    CSVGComposedSubGraphic::~CSVGComposedSubGraphic()
                    {
                        ClearElements();
                    }

                    bool CSVGComposedSubGraphic::AddElement(const CSVGPrimitive* pSVGPrimitive)
                    {
                        if (pSVGPrimitive && (pSVGPrimitive != this) && (m_Elements.find(pSVGPrimitive) == m_Elements.end()))
                        {
                            m_Elements.insert(pSVGPrimitive);
                            return true;
                        }
                        return false;
                    }

                    void CSVGComposedSubGraphic::ClearElements()
                    {
                        for (auto m_Element : m_Elements)
                        {
                            delete m_Element;
                        }
                        m_Elements.clear();
                    }

                    int CSVGComposedSubGraphic::GetTotalElements() const
                    {
                        return m_Elements.size();
                    }

                    const std::string CSVGComposedSubGraphic::ToString() const
                    {
                        if (m_Elements.size())
                        {
                            return ToString(false);
                        }
                        return std::string();
                    }

                    void CSVGComposedSubGraphic::Serialize(std::ostringstream& OutputStream) const
                    {
                        if (m_IsActive && m_Elements.size())
                        {
                            OutputStream << ToString(true);
                        }
                    }

                    CSVGPrimitive* CSVGComposedSubGraphic::CreateInstance(const Mathematics::_2D::CVector2D& Point)
                    {
                        return new CSVGComposedSubGraphicIntance(this, Point);
                    }

                    const std::string CSVGComposedSubGraphic::ToString(const bool CheckInternal) const
                    {
                        std::ostringstream OutputStream;
                        OutputStream.precision(s_DisplayDigits);
                        OutputStream << "<symbol id =\"S" << m_InstanceId << "\" >";
                        if (CheckInternal)
                            for (auto m_Element : m_Elements)
                            {
                                m_Element->Serialize(OutputStream);
                            }
                        else
                            for (auto m_Element : m_Elements)
                            {
                                OutputStream << m_Element->ToString();
                            }
                        OutputStream << "</symbol>";
                        return OutputStream.str();
                    }

                    CSVGComposedSubGraphic::CSVGComposedSubGraphicIntance::CSVGComposedSubGraphicIntance(const CSVGComposedSubGraphic* pComposedSubGraphic, const Mathematics::_2D::CVector2D& Point) :
                        CSVGPrimitive(eNone),
                        m_pComposedSubGraphic(pComposedSubGraphic),
                        m_Point(Point)
                    {
                    }

                    CSVGComposedSubGraphic::CSVGComposedSubGraphicIntance::~CSVGComposedSubGraphicIntance()
                        = default;

                    const std::string CSVGComposedSubGraphic::CSVGComposedSubGraphicIntance::ToString() const
                    {
                        std::ostringstream OutputStream;
                        OutputStream.precision(s_DisplayDigits);
                        OutputStream << "<use x=\"" << m_Point.GetX() << "\" y=\"" << m_Point.GetY() << "\" xlink:href=\"#S" << m_pComposedSubGraphic->GetInstanceId() << "\"/>";
                        return OutputStream.str();
                    }

                    void CSVGComposedSubGraphic::CSVGComposedSubGraphicIntance::Serialize(std::ostringstream& OutputStream) const
                    {
                        if (m_pComposedSubGraphic && m_pComposedSubGraphic->IsActive() && m_pComposedSubGraphic->GetTotalElements())
                        {
                            OutputStream << ToString();
                        }
                    }
                }
            }
        }
    }
}
