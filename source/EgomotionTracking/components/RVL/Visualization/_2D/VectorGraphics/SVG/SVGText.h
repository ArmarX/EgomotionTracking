/*
 * SVGText.h
 */

#pragma once

#include "SVGPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    class CSVGText : public CSVGPrimitive
                    {
                    public:

                        CSVGText(const std::string& Text, const Mathematics::_2D::CVector2D& LeftUpperPoint, const RGBAColor& TextColor, const Real FontSize);
                        ~CSVGText() override;

                        const std::string ToString() const override;
                        void Serialize(std::ostringstream& OutputStream) const override;

                    protected:

                        std::string m_Text;
                        Mathematics::_2D::CVector2D m_LeftUpperPoint;
                        RGBAColor m_TextColor;
                        Real m_FontSize;
                    };
                }
            }
        }
    }
}

