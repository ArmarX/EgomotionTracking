/*
 * SVGEllipse.cpp
 */

#include "SVGEllipse.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    CSVGEllipse::CSVGEllipse(const Mathematics::_2D::CVector2D& Center, const Real RadiusX, const Real RadiusY, const Real AlphaRadians, const RGBAColor& StrokeColor, const Real StrockWidth, const RGBAColor& FillColor, const int CurrentStyleFlags) :
                        CSVGPrimitive(CurrentStyleFlags),
                        m_Center(Center),
                        m_RadiusX(RadiusX),
                        m_RadiusY(RadiusY),
                        m_AlphaRadians(AlphaRadians),
                        m_StrokeColor(StrokeColor),
                        m_StrokeWidth(StrockWidth),
                        m_FillColor(FillColor)
                    {
                    }

                    CSVGEllipse::~CSVGEllipse()
                        = default;

                    void CSVGEllipse::SetDashing(const std::vector<Real>& Dashing)
                    {
                        m_Dashing = Dashing;
                    }

                    const std::string CSVGEllipse::ToString() const
                    {
                        std::ostringstream OutputStream;
                        OutputStream.precision(s_DisplayDigits);
                        OutputStream << "<ellipse cx=\"" << m_Center.GetX() << "\" cy=\"" << m_Center.GetY() << "\" rx=\"" << m_RadiusX << "\" ry=\"" << m_RadiusY << "\" transform=\"rotate(" << AngleInDegrees(m_AlphaRadians) << " " << m_Center.GetX() << " " << m_Center.GetY() << ")\" ";
                        if (m_StyleFlags & (eStroke | eFill))
                        {
                            OutputStream << "style=\"";
                            if (m_StyleFlags & eStroke)
                            {
                                if (m_Dashing.size())
                                {
                                    OutputStream << DashingToString(m_Dashing);
                                }
                                OutputStream << "stroke:rgb(" << int(m_StrokeColor.m_Red) << "," << int(m_StrokeColor.m_Green) << "," << int(m_StrokeColor.m_Blue) << ");";
                                if (m_StrokeColor.IsTranslucent())
                                {
                                    OutputStream << "stroke-opacity:" << m_StrokeColor.GetNormalizedAlpha() << ";";
                                }
                                OutputStream << "stroke-width:" << m_StrokeWidth << ";";
                            }
                            if (m_StyleFlags & eFill)
                            {
                                OutputStream << "fill:rgb(" << int(m_FillColor.m_Red) << "," << int(m_FillColor.m_Green) << "," << int(m_FillColor.m_Blue) << ");";
                                if (m_FillColor.IsTranslucent())
                                {
                                    OutputStream << "fill-opacity:" << m_FillColor.GetNormalizedAlpha() << ";";
                                }
                            }
                            OutputStream << "\"";
                        }
                        OutputStream << "/>";
                        return OutputStream.str();
                    }

                    void CSVGEllipse::Serialize(std::ostringstream& OutputStream) const
                    {
                        if (m_IsActive && IsPositive(m_RadiusX) && IsPositive(m_RadiusY) && m_Center.IsNotAtInfinity())
                        {
                            OutputStream << ToString();
                        }
                    }
                }
            }
        }
    }
}
