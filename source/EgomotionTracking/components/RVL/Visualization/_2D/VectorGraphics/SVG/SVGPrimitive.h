/*
 * SVGPrimitive.h
 */

#pragma once

#include "../../../../Common/Includes.h"
#include "../../../../Common/DataTypes.h"
#include "../../../../Mathematics/_2D/Vector2D.h"
#include "../../../Color/Color.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    class CSVGPrimitive
                    {
                    public:

                        static bool SetDisplayDigits(const int DisplayDigits);
                        static void ResetInstanceIdCounter();

                        enum StyleFlags
                        {
                            eNone = 0x0, eStroke = 0x1, eFill = 0x2, eLinecapButt = 0x10, eLinecapRound = 0x20, eLineSquare = 0x40
                        };

                        CSVGPrimitive(const int CurrentStyleFlags);
                        virtual ~CSVGPrimitive();

                        int GetInstanceId() const;
                        bool IsActive() const;
                        void SetActive(const bool Active);
                        const std::string DashingToString(const std::vector<Real>& Dashing) const;

                        virtual const std::string ToString() const = 0;
                        virtual void Serialize(std::ostringstream& OutputStream) const = 0;

                    protected:

                        static int s_DisplayDigits;
                        static int s_InstanceIdCounter;
                        int m_InstanceId;
                        int m_StyleFlags;
                        bool m_IsActive;
                    };
                }
            }
        }
    }
}

