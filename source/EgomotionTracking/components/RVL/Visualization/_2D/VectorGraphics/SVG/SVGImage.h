/*
 * SVGImage.h
 */

#pragma once

#include "SVGPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    class CSVGImage : public CSVGPrimitive
                    {
                    public:

                        CSVGImage(const std::string& FileName, const Mathematics::_2D::CVector2D& LeftUpperPoint, const Real Width, const Real Height);
                        ~CSVGImage() override;

                        const std::string ToString() const override;
                        void Serialize(std::ostringstream& OutputStream) const override;

                    protected:

                        std::string m_FileName;
                        Mathematics::_2D::CVector2D m_LeftUpperPoint;
                        Real m_Width;
                        Real m_Height;
                    };
                }
            }
        }
    }
}

