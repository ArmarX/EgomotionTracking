/*
 * SVGComposedSubGraphic.h
 */

#pragma once

#include "SVGPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    class CSVGComposedSubGraphic : public CSVGPrimitive
                    {
                    public:

                        CSVGComposedSubGraphic();
                        ~CSVGComposedSubGraphic() override;

                        bool AddElement(const CSVGPrimitive* pSVGPrimitive);
                        void ClearElements();
                        int GetTotalElements() const;
                        const std::string ToString() const override;
                        void Serialize(std::ostringstream& OutputStream) const override;
                        CSVGPrimitive* CreateInstance(const Mathematics::_2D::CVector2D& Point);

                    protected:

                        const std::string ToString(const bool CheckInternal) const;

                        class CSVGComposedSubGraphicIntance : public CSVGPrimitive
                        {
                        public:

                            CSVGComposedSubGraphicIntance(const CSVGComposedSubGraphic* pComposedSubGraphic, const Mathematics::_2D::CVector2D& Point);
                            ~CSVGComposedSubGraphicIntance() override;

                            const std::string ToString() const override;
                            void Serialize(std::ostringstream& OutputStream) const override;

                        protected:

                            const CSVGComposedSubGraphic* m_pComposedSubGraphic;
                            Mathematics::_2D::CVector2D m_Point;
                        };

                        std::set<const CSVGPrimitive*> m_Elements;
                    };
                }
            }
        }
    }
}

