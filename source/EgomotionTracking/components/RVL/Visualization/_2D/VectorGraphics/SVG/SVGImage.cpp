/*
 * SVGImage.cpp
 */

#include "SVGImage.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    CSVGImage::CSVGImage(const std::string& FileName, const Mathematics::_2D::CVector2D& LeftUpperPoint, const Real Width, const Real Height) :
                        CSVGPrimitive(eNone),
                        m_FileName(FileName),
                        m_LeftUpperPoint(LeftUpperPoint),
                        m_Width(Width),
                        m_Height(Height)
                    {
                    }

                    CSVGImage::~CSVGImage()
                        = default;

                    const std::string CSVGImage::ToString() const
                    {
                        std::ostringstream OutputStream;
                        OutputStream.precision(s_DisplayDigits);
                        OutputStream << "<image x=\"" << m_LeftUpperPoint.GetX() << "\" y=\"" << m_LeftUpperPoint.GetY() << "\" width=\"" << m_Width << "\" height=\"" << m_Height << "\" xlink:href=\"" << m_FileName << "\"/>";
                        return OutputStream.str();
                    }

                    void CSVGImage::Serialize(std::ostringstream& OutputStream) const
                    {
                        if (m_IsActive && m_FileName.length() && m_LeftUpperPoint.IsNotAtInfinity() && IsPositive(m_Width) && IsPositive(m_Height))
                        {
                            OutputStream << ToString();
                        }
                    }
                }
            }
        }
    }
}
