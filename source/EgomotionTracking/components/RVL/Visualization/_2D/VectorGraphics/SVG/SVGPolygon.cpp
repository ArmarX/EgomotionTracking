/*
 * SVGPolygon.cpp
 */

#include "SVGPolygon.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    CSVGPolygon::CSVGPolygon(const std::list<Mathematics::_2D::CVector2D>& Points, const RGBAColor& StrokeColor, const Real StrockWidth, const RGBAColor& FillColor, const int CurrentStyleFlags) :
                        CSVGPrimitive(CurrentStyleFlags),
                        m_Points(Points),
                        m_StrokeColor(StrokeColor),
                        m_StrokeWidth(StrockWidth),
                        m_FillColor(FillColor)
                    {
                    }

                    CSVGPolygon::~CSVGPolygon()
                        = default;

                    void CSVGPolygon::SetDashing(const std::vector<Real>& Dashing)
                    {
                        m_Dashing = Dashing;
                    }

                    const std::string CSVGPolygon::ToString() const
                    {
                        std::ostringstream OutputStream;
                        OutputStream.precision(s_DisplayDigits);
                        OutputStream << "<polygon points=\"";
                        std::list<Mathematics::_2D::CVector2D>::const_iterator pPoint = m_Points.begin();
                        while (pPoint != m_Points.end())
                        {
                            OutputStream << pPoint->GetX() << "," << pPoint->GetY();
                            if (++pPoint != m_Points.end())
                            {
                                OutputStream << " ";
                            }
                        }
                        OutputStream << "\" ";
                        if (m_StyleFlags & (eStroke | eFill))
                        {
                            OutputStream << "style=\"";
                            if (m_StyleFlags & eStroke)
                            {
                                if (m_Dashing.size())
                                {
                                    OutputStream << DashingToString(m_Dashing);
                                }
                                OutputStream << "stroke:rgb(" << int(m_StrokeColor.m_Red) << "," << int(m_StrokeColor.m_Green) << "," << int(m_StrokeColor.m_Blue) << ");";
                                if (m_StrokeColor.IsTranslucent())
                                {
                                    OutputStream << "stroke-opacity:" << m_StrokeColor.GetNormalizedAlpha() << ";";
                                }
                                OutputStream << "stroke-width:" << m_StrokeWidth << ";";
                            }
                            if (m_StyleFlags & eFill)
                            {
                                OutputStream << "fill:rgb(" << int(m_FillColor.m_Red) << "," << int(m_FillColor.m_Green) << "," << int(m_FillColor.m_Blue) << ");";
                                if (m_FillColor.IsTranslucent())
                                {
                                    OutputStream << "fill-opacity:" << m_FillColor.GetNormalizedAlpha() << ";";
                                }
                            }
                            OutputStream << "\"";
                        }
                        OutputStream << "/>";
                        return OutputStream.str();
                    }

                    void CSVGPolygon::Serialize(std::ostringstream& OutputStream) const
                    {
                        if (m_IsActive && (m_Points.size() > 2))
                        {
                            OutputStream << ToString();
                        }
                    }
                }
            }
        }
    }
}
