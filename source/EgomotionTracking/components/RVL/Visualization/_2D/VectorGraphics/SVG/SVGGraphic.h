/*
 * SVGGraphic.h
 */

#pragma once

#include "../../../../Common/About.h"
#include "../../../../Common/Tags.h"
#include "../../../../Files/OutFile.h"
#include "../../../../Compression/Compression.h"

#include "SVGCircle.h"
#include "SVGComposedSubGraphic.h"
#include "SVGEllipse.h"
#include "SVGGroup.h"
#include "SVGImage.h"
#include "SVGLine.h"
#include "SVGPolygon.h"
#include "SVGPolyLine.h"
#include "SVGRectangle.h"
#include "SVGText.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    class CSVGGraphic : public CSVGGroup
                    {
                    public:

                        CSVGGraphic(const Real Width, const Real Height);
                        ~CSVGGraphic() override;

                        const std::string ToString() const override;
                        void Serialize(std::ostringstream& OutputStream) const override;
                        bool SaveToFile(const std::string& FileName) const;
                        bool SaveToBinaryCompressedFile(const std::string& FileName) const;

                    protected:

                        const std::string ToString(const bool CheckInternal) const;

                        Real m_Width;
                        Real m_Height;
                    };
                }
            }
        }
    }
}

