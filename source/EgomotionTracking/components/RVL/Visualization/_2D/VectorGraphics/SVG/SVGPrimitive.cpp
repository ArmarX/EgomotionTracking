/*
 * SVGPrimitive.cpp
 */

#include "SVGPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    int CSVGPrimitive::s_DisplayDigits = g_RealDisplayDigits;
                    int CSVGPrimitive::s_InstanceIdCounter = 0;

                    bool CSVGPrimitive::SetDisplayDigits(const int DisplayDigits)
                    {
                        if (DisplayDigits >= 3)
                        {
                            s_DisplayDigits = DisplayDigits;
                            return true;
                        }
                        return false;
                    }
                    void CSVGPrimitive::ResetInstanceIdCounter()
                    {
                        s_InstanceIdCounter = 0;
                    }

                    CSVGPrimitive::CSVGPrimitive(const int CurrentStyleFlags) :
                        m_InstanceId(++s_InstanceIdCounter),
                        m_StyleFlags(CurrentStyleFlags),
                        m_IsActive(true)
                    {
                    }

                    CSVGPrimitive::~CSVGPrimitive()
                        = default;

                    int CSVGPrimitive::GetInstanceId() const
                    {
                        return m_InstanceId;
                    }

                    bool CSVGPrimitive::IsActive() const
                    {
                        return m_IsActive;
                    }

                    void CSVGPrimitive::SetActive(const bool Active)
                    {
                        m_IsActive = Active;
                    }

                    const std::string CSVGPrimitive::DashingToString(const std::vector<Real>& Dashing) const
                    {
                        std::ostringstream OutputStream;
                        OutputStream.precision(g_RealDisplayDigits);
                        OutputStream << "stroke-dasharray:";
                        const int Size = Dashing.size();
                        for (int i = 0 ; i < Size ; ++i)
                        {
                            OutputStream << Dashing[i];
                            if ((i + 1) != Size)
                            {
                                OutputStream << ",";
                            }
                        }
                        OutputStream << ";";
                        return OutputStream.str();
                    }
                }
            }
        }
    }
}
