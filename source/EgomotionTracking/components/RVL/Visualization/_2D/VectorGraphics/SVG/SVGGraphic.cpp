/*
 * SVGGraphic.cpp
 */

#include "SVGGraphic.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    CSVGGraphic::CSVGGraphic(const Real Width, const Real Height) :
                        CSVGGroup(),
                        m_Width(Width),
                        m_Height(Height)
                    {
                    }

                    CSVGGraphic::~CSVGGraphic()
                        = default;

                    const std::string CSVGGraphic::ToString() const
                    {
                        return ToString(false);
                    }

                    void CSVGGraphic::Serialize(std::ostringstream& OutputStream) const
                    {
                        if (CSVGGroup::m_IsActive && CSVGGroup::m_Elements.size())
                        {
                            OutputStream << ToString(true);
                        }
                    }

                    bool CSVGGraphic::SaveToFile(const std::string& FileName) const
                    {
                        if (CSVGGroup::m_IsActive)
                        {
                            return Files::COutFile::WriteStringToFile(FileName, ToString(true));
                        }
                        return false;
                    }

                    bool CSVGGraphic::SaveToBinaryCompressedFile(const std::string& FileName) const
                    {
                        if (CSVGGroup::m_IsActive)
                        {
                            Files::COutFile OutFile(FileName, Files::CFile::eBinary);
                            if (OutFile.IsReady())
                            {
                                const std::string Content = ToString(true);
                                unsigned long NonCompressedLength = Content.size();
                                unsigned long CompressedLength = 0;
                                const Byte* pCompressedBuffer = Compression::CCompression::Compress(reinterpret_cast<const Byte*>(Content.c_str()), NonCompressedLength, CompressedLength);
                                if (pCompressedBuffer)
                                {
                                    OutFile.Write(&NonCompressedLength, sizeof(unsigned long));
                                    OutFile.Write(&CompressedLength, sizeof(unsigned long));
                                    OutFile.Write(pCompressedBuffer, CompressedLength);
                                    delete[] pCompressedBuffer;
                                    return OutFile.Close();
                                }
                            }
                        }
                        return false;
                    }

                    const std::string CSVGGraphic::ToString(const bool CheckInternal) const
                    {
                        std::ostringstream OutputStream;
                        OutputStream.precision(s_DisplayDigits);
                        OutputStream << "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"" << m_Width << "\" height=\"" << m_Height << "\">";
                        OutputStream << "<!-- Generated with " << _LIBRARY_NAME_ << " [" << _LIBRARY_ACRONYM_ << ", Version " << _MAJOR_VERSION_ << "." << _MINOR_VERSION_ << "." << _REVISION_NUMBER_ << "] by " << _AUTHOR_ << ", " << _AUTHOR_MAIL_ << " -->";
                        OutputStream << CSVGGroup::ToString(CheckInternal);
                        OutputStream << "</svg>";
                        return OutputStream.str();
                    }
                }
            }
        }
    }
}
