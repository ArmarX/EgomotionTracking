/*
 * SVGRectangle.h
 */

#pragma once

#include "SVGPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    class CSVGRectangle : public CSVGPrimitive
                    {
                    public:

                        CSVGRectangle(const Mathematics::_2D::CVector2D& A, const Mathematics::_2D::CVector2D& B, const RGBAColor& StrokeColor, const Real StrockWidth, const RGBAColor& FillColor, const int CurrentStyleFlags);
                        ~CSVGRectangle() override;

                        void SetDashing(const std::vector<Real>& Dashing);
                        const std::string ToString() const override;
                        void Serialize(std::ostringstream& OutputStream) const override;

                    protected:

                        Mathematics::_2D::CVector2D m_LeftUpperPoint;
                        Real m_Width;
                        Real m_Heigth;
                        RGBAColor m_StrokeColor;
                        Real m_StrokeWidth;
                        RGBAColor m_FillColor;
                        std::vector<Real> m_Dashing;
                    };
                }
            }
        }
    }
}

