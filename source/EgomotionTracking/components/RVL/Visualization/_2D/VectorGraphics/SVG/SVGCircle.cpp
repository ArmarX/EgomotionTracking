/*
 * SVGCircle.cpp
 */

#include "SVGCircle.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    CSVGCircle::CSVGCircle(const Mathematics::_2D::CVector2D& Center, const Real Radius, const RGBAColor& StrokeColor, const Real StrockWidth, const RGBAColor& FillColor, const int CurrentStyleFlags) :
                        CSVGPrimitive(CurrentStyleFlags),
                        m_Center(Center),
                        m_Radius(Radius),
                        m_StrokeColor(StrokeColor),
                        m_StrokeWidth(StrockWidth),
                        m_FillColor(FillColor)
                    {
                    }

                    CSVGCircle::CSVGCircle(const int CX, const int CY, const Real Radius, const RGBAColor& StrokeColor, const Real StrockWidth, const RGBAColor& FillColor, const int CurrentStyleFlags) :
                        CSVGPrimitive(CurrentStyleFlags),
                        m_Center(CX, CY),
                        m_Radius(Radius),
                        m_StrokeColor(StrokeColor),
                        m_StrokeWidth(StrockWidth),
                        m_FillColor(FillColor)
                    {
                    }

                    CSVGCircle::~CSVGCircle()
                        = default;

                    void CSVGCircle::SetDashing(const std::vector<Real>& Dashing)
                    {
                        m_Dashing = Dashing;
                    }

                    const std::string CSVGCircle::ToString() const
                    {
                        std::ostringstream OutputStream;
                        OutputStream.precision(s_DisplayDigits);
                        OutputStream << "<circle cx=\"" << m_Center.GetX() << "\" cy=\"" << m_Center.GetY() << "\" r=\"" << m_Radius << "\" ";
                        if (m_StyleFlags & (eStroke | eFill))
                        {
                            OutputStream << "style=\"";
                            if (m_StyleFlags & eStroke)
                            {
                                if (m_Dashing.size())
                                {
                                    OutputStream << DashingToString(m_Dashing);
                                }
                                OutputStream << "stroke:rgb(" << int(m_StrokeColor.m_Red) << "," << int(m_StrokeColor.m_Green) << "," << int(m_StrokeColor.m_Blue) << ");";
                                if (m_StrokeColor.IsTranslucent())
                                {
                                    OutputStream << "stroke-opacity:" << m_StrokeColor.GetNormalizedAlpha() << ";";
                                }
                                OutputStream << "stroke-width:" << m_StrokeWidth << ";";
                            }
                            if (m_StyleFlags & eFill)
                            {
                                OutputStream << "fill:rgb(" << int(m_FillColor.m_Red) << "," << int(m_FillColor.m_Green) << "," << int(m_FillColor.m_Blue) << ");";
                                if (m_FillColor.IsTranslucent())
                                {
                                    OutputStream << "fill-opacity:" << m_FillColor.GetNormalizedAlpha() << ";";
                                }
                            }
                            OutputStream << "\"";
                        }
                        OutputStream << "/>";
                        return OutputStream.str();
                    }

                    void CSVGCircle::Serialize(std::ostringstream& OutputStream) const
                    {
                        if (m_IsActive && IsPositive(m_Radius) && m_Center.IsNotAtInfinity())
                        {
                            OutputStream << ToString();
                        }
                    }
                }
            }
        }
    }
}
