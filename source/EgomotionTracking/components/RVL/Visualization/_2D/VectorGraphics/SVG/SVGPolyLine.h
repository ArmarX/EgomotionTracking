/*
 * SVGPolyLine.h
 */

#pragma once

#include "SVGPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    class CSVGPolyLine : public CSVGPrimitive
                    {
                    public:

                        CSVGPolyLine(const std::list<Mathematics::_2D::CVector2D>& Points, const RGBAColor& StrokeColor, const Real StrockWidth, const int CurrentStyleFlags);
                        CSVGPolyLine(const RGBAColor& StrokeColor, const Real StrockWidth, const int CurrentStyleFlags);
                        ~CSVGPolyLine() override;

                        void AddPoint(const Mathematics::_2D::CVector2D& X);
                        void SetDashing(const std::vector<Real>& Dashing);
                        const std::string ToString() const override;
                        void Serialize(std::ostringstream& OutputStream) const override;

                    protected:

                        std::list<Mathematics::_2D::CVector2D> m_Points;
                        RGBAColor m_StrokeColor;
                        Real m_StrokeWidth;
                        std::vector<Real> m_Dashing;
                    };
                }
            }
        }
    }
}

