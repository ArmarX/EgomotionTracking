/*
 * SVGRectangle.cpp
 */

#include "SVGRectangle.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _2D
        {
            namespace VectorGraphics
            {
                namespace SVG
                {
                    CSVGRectangle::CSVGRectangle(const Mathematics::_2D::CVector2D& A, const Mathematics::_2D::CVector2D& B, const RGBAColor& StrokeColor, const Real StrockWidth, const RGBAColor& FillColor, const int CurrentStyleFlags) :
                        CSVGPrimitive(CurrentStyleFlags),
                        m_LeftUpperPoint(std::min(A.GetX(), B.GetX()), std::min(A.GetY(), B.GetY())),
                        m_Width(std::max(A.GetX(), B.GetX()) - std::min(A.GetX(), B.GetX())),
                        m_Heigth(std::max(A.GetY(), B.GetY()) - std::min(A.GetY(), B.GetY())),
                        m_StrokeColor(StrokeColor),
                        m_StrokeWidth(StrockWidth),
                        m_FillColor(FillColor)
                    {
                    }

                    CSVGRectangle::~CSVGRectangle()
                        = default;

                    void CSVGRectangle::SetDashing(const std::vector<Real>& Dashing)
                    {
                        m_Dashing = Dashing;
                    }

                    const std::string CSVGRectangle::ToString() const
                    {
                        std::ostringstream OutputStream;
                        OutputStream.precision(s_DisplayDigits);
                        OutputStream << "<rect x=\"" << m_LeftUpperPoint.GetX() << "\" y=\"" << m_LeftUpperPoint.GetY() << "\" width=\"" << m_Width << "\" height=\"" << m_Heigth << "\" ";
                        if (m_StyleFlags & (eStroke | eFill))
                        {
                            OutputStream << "style=\"";
                            if (m_StyleFlags & eStroke)
                            {
                                if (m_Dashing.size())
                                {
                                    OutputStream << DashingToString(m_Dashing);
                                }
                                OutputStream << "stroke:rgb(" << int(m_StrokeColor.m_Red) << "," << int(m_StrokeColor.m_Green) << "," << int(m_StrokeColor.m_Blue) << ");";
                                if (m_StrokeColor.IsTranslucent())
                                {
                                    OutputStream << "stroke-opacity:" << m_StrokeColor.GetNormalizedAlpha() << ";";
                                }
                                OutputStream << "stroke-width:" << m_StrokeWidth << ";";
                            }
                            if (m_StyleFlags & eFill)
                            {
                                OutputStream << "fill:rgb(" << int(m_FillColor.m_Red) << "," << int(m_FillColor.m_Green) << "," << int(m_FillColor.m_Blue) << ");";
                                if (m_FillColor.IsTranslucent())
                                {
                                    OutputStream << "fill-opacity:" << m_FillColor.GetNormalizedAlpha() << ";";
                                }
                            }
                            OutputStream << "\"";
                        }
                        OutputStream << "/>";
                        return OutputStream.str();
                    }

                    void CSVGRectangle::Serialize(std::ostringstream& OutputStream) const
                    {
                        if (m_IsActive && IsPositive(m_Width) && IsPositive(m_Heigth) && m_LeftUpperPoint.IsNotAtInfinity())
                        {
                            OutputStream << ToString();
                        }
                    }
                }
            }
        }
    }
}
