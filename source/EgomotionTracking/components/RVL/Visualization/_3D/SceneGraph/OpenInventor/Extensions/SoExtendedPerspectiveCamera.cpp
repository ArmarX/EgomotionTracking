/*
 * SoExtendedPerspectiveCamera.cpp
 */

#include "SoExtendedPerspectiveCamera.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Extensions
                    {
                        SoExtendedPerspectiveCamera::SoExtendedPerspectiveCamera() :
                            SoPerspectiveCamera(),
                            m_RenderingMode(eVirtualCameraParameteres),
                            m_pCameraCalibration(nullptr),
                            m_Near(0.0f),
                            m_Far(0.0f),
                            m_AspectRatio(0.0f)
                        {
                        }

                        SoExtendedPerspectiveCamera::~SoExtendedPerspectiveCamera()
                            = default;

                        bool SoExtendedPerspectiveCamera::LoadCalibration(const Sensors::Cameras::Pasive::Calibration::Geometric::CMonocularCameraGeometricCalibration* pCameraCalibration, const float Near, const float Far)
                        {
                            if (pCameraCalibration && (Far > 0.0f) && (Far > Near) && (Far - Near > FLT_EPSILON))
                            {
                                m_pCameraCalibration = pCameraCalibration;
                                m_Near = Near;
                                m_Far = Far;
                                Mathematics::_3D::CMatrix3D K = m_pCameraCalibration->GetCalibrationMatrix();
                                m_ImageSize.setValue(float(m_pCameraCalibration->GetImageWidth()), float(m_pCameraCalibration->GetImageHeight()));
                                m_FocalLenght.setValue(float(m_pCameraCalibration->GetFocalLengthX()), float(m_pCameraCalibration->GetFocalLengthY()));
                                m_PrincipalPoint.setValue(float(m_pCameraCalibration->GetImagePrincipalPointX()), float(m_pCameraCalibration->GetImagePrincipalPointY()));
                                m_FieldOfView.setValue(atanf((m_ImageSize[0] - m_PrincipalPoint[0]) / m_FocalLenght[0]) + atanf(m_PrincipalPoint[0] / m_FocalLenght[0]), atanf((m_ImageSize[1] - m_PrincipalPoint[1]) / m_FocalLenght[1]) + atanf(m_PrincipalPoint[1] / m_FocalLenght[1]));
                                m_AspectRatio = m_FocalLenght[1] / m_FocalLenght[0];
                                m_ProjectionMatrix[0][0] = (2.0f * float(K[0][0]) / m_ImageSize[0]);
                                m_ProjectionMatrix[0][1] = 0.0f;
                                m_ProjectionMatrix[0][2] = 0.0f;
                                m_ProjectionMatrix[0][3] = 0.0f;
                                m_ProjectionMatrix[1][0] = (2.0f * float(K[0][1]) / m_ImageSize[0]);
                                m_ProjectionMatrix[1][1] = (2.0f * float(K[1][1]) / m_ImageSize[1]);
                                m_ProjectionMatrix[1][2] = 0.0f;
                                m_ProjectionMatrix[1][3] = 0.0f;
                                m_ProjectionMatrix[2][0] = -((2.0f * float(K[0][2]) / m_ImageSize[0]) - 1.0f);
                                m_ProjectionMatrix[2][1] = ((2.0f * float(K[1][2]) / m_ImageSize[1]) - 1.0f);
                                m_ProjectionMatrix[2][2] = -(m_Far + m_Near) / (m_Far - m_Near);
                                m_ProjectionMatrix[2][3] = -1.0f;
                                m_ProjectionMatrix[3][0] = 0.0f;
                                m_ProjectionMatrix[3][1] = 0.0f;
                                m_ProjectionMatrix[3][2] = (-2.0f * m_Far * m_Near) / (m_Far - m_Near);
                                m_ProjectionMatrix[3][3] = 0.0f;
                                return true;
                            }
                            return false;
                        }

                        bool SoExtendedPerspectiveCamera::SetRenderingMode(const CameraRenderingMode Mode)
                        {
                            switch (Mode)
                            {
                                case eVirtualCameraParameteres:
                                    m_RenderingMode = eVirtualCameraParameteres;
                                    return true;
                                case eCalibrationCameraParameters:
                                    if (m_pCameraCalibration)
                                    {
                                        m_RenderingMode = eCalibrationCameraParameters;
                                        return true;
                                    }
                                    return false;
                            }
                            return false;
                        }

                        SbViewVolume SoExtendedPerspectiveCamera::getViewVolume(const float UseAspectRatio) const
                        {
                            if (m_RenderingMode == eVirtualCameraParameteres)
                            {
                                return SoPerspectiveCamera::getViewVolume(UseAspectRatio);
                            }
                            SbViewVolume ViewVolume;
                            ViewVolume.perspective(m_FieldOfView[1], m_AspectRatio, m_Near, m_Far);
                            ViewVolume.rotateCamera(orientation.getValue());
                            ViewVolume.translateCamera(position.getValue());
                            return ViewVolume;
                        }

                        void SoExtendedPerspectiveCamera::GLRender(SoGLRenderAction* pAction)
                        {
                            if (m_RenderingMode == eVirtualCameraParameteres)
                            {
                                SoPerspectiveCamera::GLRender(pAction);
                            }
                            else
                            {
                                SoState* pState = pAction->getState();
                                SoProjectionMatrixElement::set(pState, this, m_ProjectionMatrix);
                                SbMatrix TransformationMatrix;
                                TransformationMatrix.setTransform(position.getValue(), orientation.getValue(), SbVec3f(1.0f, 1.0f, 1.0f));
                                SoViewingMatrixElement::set(pState, this, TransformationMatrix.inverse());
                                SoFocalDistanceElement::set(pState, this, focalDistance.getValue());
                                SoPerspectiveCamera::GLRender(pAction);
                            }
                        }
                    }
                }
            }
        }
    }
}
