/*
 * ContainerOpenInventorSet.cpp
 */

#include "ContainerOpenInventorSet.h"
#include "ContainedOpenInventorElement.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Base
                    {
                        CContainerOpenInventorSet::CContainerOpenInventorSet(const SbBox3f& BoundingBox) :
                            COpenInventorElement(COpenInventorElement::eVisualizationVolume),
                            m_ToleranceDelta(_VISUALIZATION_EPSILON_, _VISUALIZATION_EPSILON_, _VISUALIZATION_EPSILON_)
                        {
                            m_BoundingBox = BoundingBox;
                            m_pSoCoordinate3 = new SoCoordinate3;
                            AddElement(m_pSoCoordinate3);

                            m_pSoIndexedLineSet = new SoIndexedLineSet;
                            AddElement(m_pSoIndexedLineSet);
                            m_pSoIndexedLineSet->setUserData(this);

                            SetLineWidth(_VISUALIZATION_VOLUME_DEFAULT_LINE_WIDTH_);
                            SetLinePattern(_VISUALIZATION_VOLUME_DEFAULT_LINE_PATTERN_);
                            SetTransparency(_VISUALIZATION_VOLUME_DEFAULT_TRANSPARENCY_);
                            SetColor(_VISUALIZATION_VOLUME_DEFAULT_LINE_COLOR_);
                            Update(true);
                        }

                        CContainerOpenInventorSet::~CContainerOpenInventorSet()
                            = default;

                        void CContainerOpenInventorSet::SetVisible(const bool Visible)
                        {
                            COpenInventorElement::SetVisible(Visible);
                            Update(false);
                        }

                        void CContainerOpenInventorSet::SetBoundingBox(const SbBox3f& BoundingBox)
                        {
                            if (m_BoundingBox == BoundingBox)
                            {
                                return;
                            }
                            m_BoundingBox = BoundingBox;
                            Update(true);
                            UpdateRegisteredGeometricPrimitives(false);
                        }

                        const SbBox3f& CContainerOpenInventorSet::GetBoundingBox() const
                        {
                            return m_BoundingBox;
                        }

                        bool CContainerOpenInventorSet::IsEmpty() const
                        {
                            return m_BoundingBox.isEmpty();
                        }

                        COpenInventorElement::InclusionType CContainerOpenInventorSet::MapPoint(const SbVec3f& Point) const
                        {
                            if (m_BoundingBox.isEmpty())
                            {
                                return eEmptyInclusion;
                            }
                            return m_BoundingBox.intersect(SbBox3f(Point - m_ToleranceDelta, Point + m_ToleranceDelta)) ? eFullInclusion : eEmptyInclusion;
                        }

                        COpenInventorElement::InclusionType CContainerOpenInventorSet::MapPointPair(const SbVec3f& PointA, const SbVec3f& PointB, bool& FlagA, bool& FlagB) const
                        {
                            if (m_BoundingBox.isEmpty())
                            {
                                FlagA = false;
                                FlagB = false;
                                return eEmptyInclusion;
                            }
                            FlagA = m_BoundingBox.intersect(SbBox3f(PointA - m_ToleranceDelta, PointA + m_ToleranceDelta));
                            FlagB = m_BoundingBox.intersect(SbBox3f(PointB - m_ToleranceDelta, PointB + m_ToleranceDelta));
                            if (FlagA && FlagB)
                            {
                                return eFullInclusion;
                            }
                            if (FlagA || FlagB)
                            {
                                return ePartialInclusion;
                            }
                            return eEmptyInclusion;
                        }

                        COpenInventorElement::InclusionType CContainerOpenInventorSet::MapLine(const SbLine& Line, SbVec3f& PointA, SbVec3f& PointB) const
                        {
                            if (m_BoundingBox.isEmpty())
                            {
                                return eEmptyInclusion;
                            }

                            const SbBox3f ToleranceExtendedBoundingBox(m_BoundingBox.getMin() - m_ToleranceDelta, m_BoundingBox.getMax() + m_ToleranceDelta);
                            SbVec3f IntersectionPoint;
                            std::vector<SbVec3f> IntersectionPoints;
                            for (auto m_Plane : m_Planes)
                                if (m_Plane.intersect(Line, IntersectionPoint) && ToleranceExtendedBoundingBox.intersect(IntersectionPoint))
                                {
                                    IntersectionPoints.push_back(IntersectionPoint);
                                }

                            const int TotalPoints = IntersectionPoints.size();
                            if (TotalPoints >= 2)
                            {
                                int IndexA = -1, IndexB = -1;
                                float MaximalDistance = _VISUALIZATION_EPSILON_;
                                for (int i = 0 ; i < TotalPoints ; ++i)
                                    for (int j = i + 1 ; j < TotalPoints ; j++)
                                    {
                                        const float CurrentDistance = (IntersectionPoints[j] - IntersectionPoints[i]).length();
                                        if (CurrentDistance > MaximalDistance)
                                        {
                                            MaximalDistance = CurrentDistance;
                                            IndexA = i;
                                            IndexB = j;
                                        }
                                    }
                                if (IndexA >= 0)
                                {
                                    PointA = IntersectionPoints[IndexA];
                                    PointB = IntersectionPoints[IndexB];
                                    return ePartialInclusion;
                                }
                            }
                            return eEmptyInclusion;
                        }

                        COpenInventorElement::InclusionType CContainerOpenInventorSet::MapCircle(const SbVec3f& Normal, const SbVec3f& Center, const float Radius) const
                        {
                            switch (MapSphere(Center, Radius))
                            {
                                case eEmptyInclusion:
                                    return eEmptyInclusion;
                                    break;
                                case ePartialInclusion:
                                    break;
                                case eFullInclusion:
                                    return eFullInclusion;
                                    break;
                                case eImproperInclusion:
                                    return eEmptyInclusion;
                                    break;
                            }
                            SbPlane ImplicitPlane(Normal, Center);
                            SbLine IntersectionLine;
                            for (auto m_Plane : m_Planes)
                                if (ImplicitPlane.intersect(m_Plane, IntersectionLine))
                                {
                                    const SbVec3f ProjectedCenter = IntersectionLine.getClosestPoint(Center);
                                    const float DisplacementLength = (ProjectedCenter - Center).length();
                                    if (DisplacementLength <= Radius)
                                    {
                                        SbVec3f A, B;
                                        if (MapLine(IntersectionLine, A, B) == ePartialInclusion)
                                        {
                                            SbVec3f C = B - A;
                                            const float SegmentLenght = C.normalize();
                                            const float AuxiliarDisplacement = sqrtf(Radius * Radius - DisplacementLength * DisplacementLength);
                                            const float ProjectedCenterLocation = C.dot(ProjectedCenter - A);
                                            if (((ProjectedCenterLocation - AuxiliarDisplacement) <= SegmentLenght) && ((ProjectedCenterLocation + AuxiliarDisplacement) >= 0.0f))
                                            {
                                                return ePartialInclusion;
                                            }
                                        }
                                    }
                                }
                            return eEmptyInclusion;
                        }

                        COpenInventorElement::InclusionType CContainerOpenInventorSet::MapPlane(const SbPlane& Plane, std::vector<SbVec3f>& Points) const
                        {
                            if (m_BoundingBox.isEmpty())
                            {
                                return eEmptyInclusion;
                            }

                            const SbBox3f ToleranceExtendedBoundingBox(m_BoundingBox.getMin() - m_ToleranceDelta, m_BoundingBox.getMax() + m_ToleranceDelta);
                            SbVec3f IntersectionPointsAccumulator(0.0f, 0.0f, 0.0f);
                            AxisIntersectionPoint CurrentAxisIntersectionPoint;
                            std::vector<AxisIntersectionPoint> AxisIntersectionPoints;
                            for (const auto& m_Line : m_Lines)
                                if (Plane.intersect(m_Line, CurrentAxisIntersectionPoint.m_Location) && ToleranceExtendedBoundingBox.intersect(CurrentAxisIntersectionPoint.m_Location))
                                {
                                    IntersectionPointsAccumulator += CurrentAxisIntersectionPoint.m_Location;
                                    AxisIntersectionPoints.push_back(CurrentAxisIntersectionPoint);
                                }
                            const int TotalPoints = AxisIntersectionPoints.size();
                            if (TotalPoints > 2)
                            {
                                const SbVec3f Centroid = IntersectionPointsAccumulator * (1.0f / float(TotalPoints));
                                const SbRotation Rotation(Plane.getNormal(), SbVec3f(0.0f, 0.0f, 1.0f));
                                for (auto& pAxisIntersectionPoint : AxisIntersectionPoints)
                                {
                                    const SbVec3f Displacement = pAxisIntersectionPoint.m_Location - Centroid;
                                    SbVec3f DimensionReduced;
                                    Rotation.multVec(Displacement, DimensionReduced);
                                    const float Alpha = std::atan2(DimensionReduced[1], DimensionReduced[0]);
                                    if (Alpha < 0.0f)
                                    {
                                        pAxisIntersectionPoint.m_Alpha = Alpha + g_Real2PI;
                                    }
                                    else
                                    {
                                        pAxisIntersectionPoint.m_Alpha = Alpha;
                                    }
                                }
                                std::sort(AxisIntersectionPoints.begin(), AxisIntersectionPoints.end(), SortAxisIntersectionPointByAlpha);
                                if ((AxisIntersectionPoints[0].m_Location - Centroid).cross((AxisIntersectionPoints[1].m_Location - Centroid)).dot(Plane.getNormal()) > 0.0f)
                                    for (int i = 0 ; i < TotalPoints ; ++i)
                                    {
                                        Points.push_back(AxisIntersectionPoints[i].m_Location);
                                    }
                                else
                                    for (int i = int(TotalPoints) - 1 ; i >= 0 ; --i)
                                    {
                                        Points.push_back(AxisIntersectionPoints[i].m_Location);
                                    }
                                return ePartialInclusion;
                            }
                            return eEmptyInclusion;
                        }

                        COpenInventorElement::InclusionType CContainerOpenInventorSet::MapSphere(const SbVec3f& Center, const float Radius) const
                        {
                            if (m_BoundingBox.isEmpty())
                            {
                                return eEmptyInclusion;
                            }

                            const float ToleranceRadiusPlus = Radius + _VISUALIZATION_EPSILON_;
                            const SbVec3f ToleranceRadiusDelta(ToleranceRadiusPlus, ToleranceRadiusPlus, ToleranceRadiusPlus);
                            if (m_BoundingBox.intersect(SbBox3f(Center - ToleranceRadiusDelta, Center + ToleranceRadiusDelta)))
                            {
                                const float xc = Center[0];
                                const float yc = Center[1];
                                const float zc = Center[2];
                                const float ToleranceRadiusMinus = Radius - _VISUALIZATION_EPSILON_;
                                const float Ix0 = xc - ToleranceRadiusMinus;
                                const float Ix1 = xc + ToleranceRadiusMinus;
                                const float Iy0 = yc - ToleranceRadiusMinus;
                                const float Iy1 = yc + ToleranceRadiusMinus;
                                const float Iz0 = zc - ToleranceRadiusMinus;
                                const float Iz1 = zc + ToleranceRadiusMinus;
                                float x0 = 0.0f, y0 = 0.0f, z0 = 0.0f, x1 = 0.0f, y1 = 0.0f, z1 = 0.0f;
                                m_BoundingBox.getBounds(x0, y0, z0, x1, y1, z1);
                                if ((x0 <= Ix0) && (x1 >= Ix1) && (y0 <= Iy0) && (y1 >= Iy1) && (z0 <= Iz0) && (z1 >= Iz1))
                                {
                                    return eFullInclusion;
                                }

                                if (m_BoundingBox.intersect(SbBox3f(Center - m_ToleranceDelta, Center + m_ToleranceDelta)))
                                {
                                    const float SquareRadius = Radius * Radius - _VISUALIZATION_EPSILON_;
                                    int VertexInsideSphereCounter = 0;
                                    for (auto m_Vertice : m_Vertices)
                                        if ((m_Vertice - Center).sqrLength() < SquareRadius)
                                        {
                                            ++VertexInsideSphereCounter;
                                        }
                                    return (VertexInsideSphereCounter == _VISUALIZATION_VOLUME_TOTAL_BOUNDING_VERTICES_) ? eImproperInclusion : ePartialInclusion;
                                }

                                if ((xc >= (x0 - ToleranceRadiusPlus)) && (xc <= (x1 + ToleranceRadiusPlus)) && (yc >= (y0 - ToleranceRadiusPlus)) && (yc <= (y1 + ToleranceRadiusPlus)) && (zc >= (z0 - ToleranceRadiusPlus)) && (zc <= (z1 + ToleranceRadiusPlus)))
                                {
                                    const int CX = ((xc >= x0) && (xc <= x1)) ? 1 : 0;
                                    const int CY = ((yc >= y0) && (yc <= y1)) ? 1 : 0;
                                    const int CZ = ((zc >= z0) && (zc <= z1)) ? 1 : 0;
                                    if ((CX + CY + CZ) >= 2)
                                    {
                                        return ePartialInclusion;
                                    }

                                    const float SquareRadius = Radius * Radius;
                                    for (int i = 0 ; i < _VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_ ; ++i)
                                    {
                                        const SbVec3f ClosestPoint = m_Lines[i].getClosestPoint(Center);
                                        const float Projection = m_LineDirectionAB[i].dot(ClosestPoint - m_LineBasePoint[i]);
                                        if ((Projection >= 0.0f) && (Projection <= m_LineLengths[i]) && ((ClosestPoint - Center).sqrLength() <= SquareRadius))
                                        {
                                            return ePartialInclusion;
                                        }
                                    }
                                    for (auto m_Vertice : m_Vertices)
                                        if ((m_Vertice - Center).sqrLength() <= SquareRadius)
                                        {
                                            return ePartialInclusion;
                                        }
                                }
                            }
                            return eEmptyInclusion;
                        }

                        COpenInventorElement::InclusionType CContainerOpenInventorSet::MapBoundingBox(const SbBox3f& BoundingBox) const
                        {
                            if (m_BoundingBox.isEmpty())
                            {
                                return eEmptyInclusion;
                            }

                            if (m_BoundingBox.intersect(BoundingBox))
                            {
                                if (m_BoundingBox.getVolume() >= BoundingBox.getVolume())
                                {
                                    float X0, Y0, Z0, X1, Y1, Z1;
                                    BoundingBox.getBounds(X0, Y0, Z0, X1, Y1, Z1);
                                    if (m_BoundingBox.intersect(SbVec3f(X0, Y0, Z0)) && m_BoundingBox.intersect(SbVec3f(X0, Y1, Z0)) && m_BoundingBox.intersect(SbVec3f(X1, Y0, Z0)) && m_BoundingBox.intersect(SbVec3f(X1, Y1, Z0)) && m_BoundingBox.intersect(SbVec3f(X0, Y0, Z1)) && m_BoundingBox.intersect(SbVec3f(X0, Y1, Z1)) && m_BoundingBox.intersect(SbVec3f(X1, Y0, Z1)) && m_BoundingBox.intersect(SbVec3f(X1, Y1, Z1)))
                                    {
                                        return eFullInclusion;
                                    }
                                }
                                return ePartialInclusion;
                            }
                            return eEmptyInclusion;
                        }

                        void CContainerOpenInventorSet::UpdateRegisteredGeometricPrimitives(const bool FullUpdate)
                        {
                            std::set<CContainedOpenInventorElement*>::iterator End = m_Set.end();
                            for (std::set<CContainedOpenInventorElement*>::iterator ppGeometricPrimitive = m_Set.begin() ; ppGeometricPrimitive != End ; ++ppGeometricPrimitive)
                            {
                                (*ppGeometricPrimitive)->Update(FullUpdate);
                            }
                        }

                        void CContainerOpenInventorSet::Update(const bool FullUpdate)
                        {
                            if (FullUpdate && m_BoundingBox.hasVolume())
                            {
                                float X0 = 0.0f, Y0 = 0.0f, Z0 = 0.0f, X1 = 0.0f, Y1 = 0.0f, Z1 = 0.0f;
                                m_BoundingBox.getBounds(X0, Y0, Z0, X1, Y1, Z1);
                                m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y0Z0_].setValue(X0, Y0, Z0);
                                m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y0Z0_].setValue(X1, Y0, Z0);
                                m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y0Z1_].setValue(X1, Y0, Z1);
                                m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y0Z1_].setValue(X0, Y0, Z1);
                                m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y1Z0_].setValue(X0, Y1, Z0);
                                m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y1Z0_].setValue(X1, Y1, Z0);
                                m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y1Z1_].setValue(X1, Y1, Z1);
                                m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y1Z1_].setValue(X0, Y1, Z1);
                                m_Planes[_VISUALIZATION_VOLUME_BOUNDING_PLANE_XY0_] = SbPlane(SbVec3f(0.0f, 0.0f, 1.0f), Z0);
                                m_Planes[_VISUALIZATION_VOLUME_BOUNDING_PLANE_XY1_] = SbPlane(SbVec3f(0.0f, 0.0f, -1.0f), -Z1);
                                m_Planes[_VISUALIZATION_VOLUME_BOUNDING_PLANE_XZ0_] = SbPlane(SbVec3f(0.0f, 1.0f, 0.0f), Y0);
                                m_Planes[_VISUALIZATION_VOLUME_BOUNDING_PLANE_XZ1_] = SbPlane(SbVec3f(0.0f, -1.0f, 0.0f), -Y1);
                                m_Planes[_VISUALIZATION_VOLUME_BOUNDING_PLANE_YZ0_] = SbPlane(SbVec3f(1.0f, 0.0f, 0.0f), X0);
                                m_Planes[_VISUALIZATION_VOLUME_BOUNDING_PLANE_YZ1_] = SbPlane(SbVec3f(-1.0f, 0.0f, 0.0f), -X1);
                                const int IncidenceA[_VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_] = { 0, 1, 5, 4, 3, 2, 6, 7, 0, 1, 5, 4 };
                                const int IncidenceB[_VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_] = { 1, 5, 4, 0, 2, 6, 7, 3, 3, 2, 6, 7 };
                                for (int i = 0 ; i < _VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_ ; ++i)
                                {
                                    m_Lines[i] = SbLine(m_Vertices[IncidenceA[i]], m_Vertices[IncidenceB[i]]);
                                    m_LineBasePoint[i] = m_Vertices[IncidenceA[i]];
                                    m_LineDirectionAB[i] = m_Vertices[IncidenceB[i]] - m_Vertices[IncidenceA[i]];
                                    m_LineLengths[i] = m_LineDirectionAB[i].normalize();
                                }
                                for (int i = 0 ; i < _VISUALIZATION_VOLUME_TOTAL_BOUNDING_VERTICES_ ; ++i)
                                {
                                    m_pSoCoordinate3->point.set1Value(i, m_Vertices[i]);
                                }
                                const int LineSet[24] = { 0, 1, 5, 4, 0, -1, 3, 2, 6, 7, 3, -1, 0, 3, -1, 1, 2, -1, 5, 6, -1, 4, 7, -1 };
                                for (int i = 0 ; i < 24 ; ++i)
                                {
                                    m_pSoIndexedLineSet->coordIndex.set1Value(i, LineSet[i]);
                                }
                            }
                            SwitchVisibility(GetVisible() && m_BoundingBox.hasVolume());
                        }

                        bool CContainerOpenInventorSet::IsInside(const SbVec3f& Point) const
                        {
                            return m_BoundingBox.intersect(Point);
                        }

                        bool CContainerOpenInventorSet::SortAxisIntersectionPointByAlpha(const AxisIntersectionPoint& lhs, const AxisIntersectionPoint& rhs)
                        {
                            return lhs.m_Alpha < rhs.m_Alpha;
                        }

                        bool CContainerOpenInventorSet::RegisterGeometricPrimitive(CContainedOpenInventorElement* pContainedOpenInventorElement)
                        {
                            if (pContainedOpenInventorElement && (pContainedOpenInventorElement->GetContainerSet() != this))
                            {
                                pContainedOpenInventorElement->SetContainerSet(this);
                                m_Set.insert(pContainedOpenInventorElement);
                                return true;
                            }
                            return false;
                        }

                        bool CContainerOpenInventorSet::UnRegisterGeometricPrimitive(CContainedOpenInventorElement* pContainedOpenInventorElement)
                        {
                            if (pContainedOpenInventorElement && (pContainedOpenInventorElement->GetContainerSet() == this) && (m_Set.find(pContainedOpenInventorElement) != m_Set.end()))
                            {
                                m_Set.erase(pContainedOpenInventorElement);
                                return true;
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}

