/*
 * OpenInventorPlane.h
 */

#pragma once

#include "../../../../../Mathematics/_3D/Geometry/Plane3D.h"
#include "OpenInventorGeometricPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        class COpenInventorPlane : public COpenInventorGeometricPrimitive
                        {
                        public:

                            COpenInventorPlane(Mathematics::_3D::Geometry::CPlane3D* pPlane);
                            ~COpenInventorPlane() override;

                            bool HasSourcePoints() const;
                            void SetDisplayBySourcePoints(const bool Active);

                        protected:

                            void Create() override;
                            void FullUpdate() override;
                            bool IsComposedVisible() override;

                        private:

                            bool m_DisplayBySourcePoints;
                            SoCoordinate3* m_pSoCoordinate3;
                            SoFaceSet* m_SoFaceSet;
                        };
                    }
                }
            }
        }
    }
}

