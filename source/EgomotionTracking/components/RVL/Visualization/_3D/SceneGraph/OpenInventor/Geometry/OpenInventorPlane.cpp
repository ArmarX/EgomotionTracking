/*
 * OpenInventorPlane.cpp
 */

#include "OpenInventorPlane.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        COpenInventorPlane::COpenInventorPlane(Mathematics::_3D::Geometry::CPlane3D* pPlane) :
                            COpenInventorGeometricPrimitive(pPlane, Mathematics::_3D::Geometry::CGeometricPrimitive3D::ePlane, true, true, true),
                            m_DisplayBySourcePoints(pPlane ? pPlane->HasSourcePoints() : false),
                            m_pSoCoordinate3(nullptr),
                            m_SoFaceSet(nullptr)
                        {
                            Create();
                            Update(false);
                        }

                        COpenInventorPlane::~COpenInventorPlane()
                            = default;

                        bool COpenInventorPlane::HasSourcePoints() const
                        {
                            return m_pGeometricPrimitive3D ? dynamic_cast<Mathematics::_3D::Geometry::CPlane3D*>(m_pGeometricPrimitive3D)->HasSourcePoints() : false;
                        }

                        void COpenInventorPlane::SetDisplayBySourcePoints(const bool Active)
                        {
                            if (HasSourcePoints())
                            {
                                m_DisplayBySourcePoints = Active;
                                Update(false);
                            }
                        }

                        void COpenInventorPlane::Create()
                        {
                            m_pSoCoordinate3 = new SoCoordinate3;
                            AddElement(m_pSoCoordinate3);

                            m_SoFaceSet = new SoFaceSet;
                            AddElement(m_SoFaceSet);
                            m_SoFaceSet->setUserData(this);

                            SetTransparency(_VISUALIZATION_PLANE_DEFAULT_TRANSPARENCY_);
                        }

                        void COpenInventorPlane::FullUpdate()
                        {
                        }

                        bool COpenInventorPlane::IsComposedVisible()
                        {
                            if (GetVisible() && m_pGeometricPrimitive3D)
                            {
                                Mathematics::_3D::Geometry::CPlane3D* pPlane = dynamic_cast<Mathematics::_3D::Geometry::CPlane3D*>(m_pGeometricPrimitive3D);
                                if (m_pContainerSet)
                                {
                                    if (m_DisplayBySourcePoints)
                                    {
                                        Mathematics::_3D::CVector3D Aq, Bq, Cq;
                                        if (pPlane->GetSourcePoints(Aq, Bq, Cq))
                                        {
                                            m_pSoCoordinate3->point.set1Value(0, T(Aq));
                                            m_pSoCoordinate3->point.set1Value(1, T(Bq));
                                            m_pSoCoordinate3->point.set1Value(2, T(Cq));
                                            m_SoFaceSet->numVertices.setValue(3);
                                            return true;
                                        }
                                        return false;
                                    }
                                    std::vector<SbVec3f> Points;
                                    switch (m_pContainerSet->MapPlane(SbPlane(T(pPlane->GetNormal()), T(pPlane->GetHesseDistance())), Points))
                                    {
                                        case eEmptyInclusion:
                                        case eImproperInclusion:
                                            return false;
                                        case ePartialInclusion:
                                            for (int i = 0 ; i < int(Points.size()) ; ++i)
                                            {
                                                m_pSoCoordinate3->point.set1Value(i, Points[i]);
                                            }
                                            m_SoFaceSet->numVertices.setValue(Points.size());
                                            return true;
                                        case eFullInclusion:
                                            return true;
                                    }
                                }
                                else
                                {
                                    Mathematics::_3D::CVector3D Aq, Bq, Cq;
                                    if (pPlane->GetSourcePoints(Aq, Bq, Cq))
                                    {
                                        m_pSoCoordinate3->point.set1Value(0, T(Aq));
                                        m_pSoCoordinate3->point.set1Value(1, T(Bq));
                                        m_pSoCoordinate3->point.set1Value(2, T(Cq));
                                        m_SoFaceSet->numVertices.setValue(3);
                                        return true;
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}
