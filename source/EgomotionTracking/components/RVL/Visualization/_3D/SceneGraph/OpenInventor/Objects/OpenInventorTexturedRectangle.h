/*
 * OpenInventorTexturedRectangle.h
 */

#pragma once

#include "../Base/OpenInventorElement.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Objects
                    {
                        class COpenInventorTexturedRectangle : public Base::COpenInventorElement
                        {
                        public:

                            COpenInventorTexturedRectangle(const std::string& FileName, const SbVec3f& Origin, const SbVec3f& A, const SbVec3f& B, const SbVec2f& Repetitions);
                            COpenInventorTexturedRectangle(const std::string& FileName, const float LenghtX, const float LenghtY, const float OffsetZ, const float PatternRepetitionsX, const float PatternRepetitionsY);
                            ~COpenInventorTexturedRectangle() override;


                            bool Set(const SbVec3f& Origin, const SbVec3f& A, const SbVec3f& B, const SbVec2f& Repetitions);

                            void Update(const bool FullUpdate) override;

                        protected:

                            void Create();

                            std::string m_TextureFileName;
                            SbVec3f m_RectangleVertices[4];
                            SbVec2f m_TextureVertices[4];
                            SoCoordinate3* m_pSoCoordinate3;
                            SoNormal* m_pSoNormal;
                            SoNormalBinding* m_pSoNormalBinding;
                            SoFaceSet* m_pSoFaceSet;
                            SoTextureCoordinate2* m_pSoTextureCoordinate2;
                            SoTextureCoordinateBinding* m_pSoTextureCoordinateBinding;
                            SoTexture2* m_pSoTexture2;
                        };
                    }
                }
            }
        }
    }
}

