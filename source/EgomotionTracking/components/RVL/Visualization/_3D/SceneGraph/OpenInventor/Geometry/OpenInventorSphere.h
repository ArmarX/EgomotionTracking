/*
 * OpenInventorSphere.h
 */

#pragma once

#include "../../../../../Mathematics/_3D/Geometry/Sphere3D.h"
#include "OpenInventorGeometricPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        class COpenInventorSphere : public COpenInventorGeometricPrimitive
                        {
                        public:

                            COpenInventorSphere(Mathematics::_3D::Geometry::CSphere3D* pSphere);
                            ~COpenInventorSphere() override;

                        protected:

                            void Create() override;
                            void FullUpdate() override;
                            bool IsComposedVisible() override;

                        private:

                            SoTranslation* m_pSoTranslation;
                            SoSphere* m_pSoSphere;
                        };
                    }
                }
            }
        }
    }
}

