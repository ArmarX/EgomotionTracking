/*
 * OpenInventorVolumeRendering.cpp
 */

#include "OpenInventorVolumeRendering.h"

#ifdef _RVL_USE_OPEN_INVENTOR_SIMVOLEON_
namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace VolumeRendering
                    {
                        Real COpenInventorVolumeRendering::DetermineDiscretizationResolution(const Containers::_3D::CContinuousBoundingBox3D& BoundingBox, const int TotalDiscretizationCells)
                        {
                            if (BoundingBox.HasVolume())
                            {
                                return std::pow(BoundingBox.GetVolume() / Real(TotalDiscretizationCells), Real(0.33333333333333333333333333));
                            }
                            return Real(0);
                        }

                        int COpenInventorVolumeRendering::DetermineTotalDiscretizationCells(const Containers::_3D::CContinuousBoundingBox3D& BoundingBox, const Real DiscretizationResolution)
                        {
                            if (IsPositive(DiscretizationResolution))
                            {
                                return int(std::ceil(BoundingBox.GetWidth() / DiscretizationResolution)) * int(std::ceil(BoundingBox.GetHeight() / DiscretizationResolution)) * int(std::ceil(BoundingBox.GetDepth() / DiscretizationResolution));
                            }
                            return 0;
                        }

                        COpenInventorVolumeRendering::COpenInventorVolumeRendering(const Imaging::CDiscreteRGBAColorMap* pColorMap) :
                            Base::CContainedOpenInventorElement(COpenInventorElement::eVolumeRendering), m_pColorMap(pColorMap), m_DiscretizationResolution(Real(0)), m_pDiscreteDensityCells(NULL), m_pContinousDensityCells(NULL), m_pSoVolumeData(NULL), m_pSoTransferFunction(NULL), m_pSoVolumeRender(NULL), m_Width(0), m_Height(0), m_Depth(0), m_SlideCells(0), m_TotalCells(0), m_MaximalDensity(Real(0)), m_MinimalDensity(Real(0))
                        {
                            m_pSoVolumeData = new SoVolumeData;
                            m_pBaseSeparator->addChild(m_pSoVolumeData);
                            m_pSoTransferFunction = new SoTransferFunction;
                            m_pBaseSeparator->addChild(m_pSoTransferFunction);
                            m_pSoVolumeRender = new SoVolumeRender;
                            m_pBaseSeparator->addChild(m_pSoVolumeRender);
                            LoadColorMap();
                            SwitchVisibility(false);
                        }

                        COpenInventorVolumeRendering::~COpenInventorVolumeRendering()
                        {
                            delete[] m_pDiscreteDensityCells;
                            delete[] m_pContinousDensityCells;
                        }

                        void COpenInventorVolumeRendering::SetVisible(const bool Visible)
                        {
                            COpenInventorElement::SetVisible(Visible);
                            Update(false);
                        }

                        bool COpenInventorVolumeRendering::SetDensityAggregation(const Mathematics::_3D::Density::ConditioningMode ConditioningMode, const Mathematics::_3D::Density::CDensityComposition3D* pDensityComposition3D, const Real MinimalDensity, const Real DiscretizationResolution)
                        {
                            if (pDensityComposition3D && pDensityComposition3D->GetTotalPrimitives())
                            {
                                const Containers::_3D::CContinuousBoundingBox3D BoundingBox = pDensityComposition3D->GetDensityBoundingBox(MinimalDensity, ConditioningMode);
                                if (!BoundingBox.IsEmpty())
                                    if (SetConfiguration(BoundingBox, DiscretizationResolution))
                                        if (LoadDensity(ConditioningMode, pDensityComposition3D))
                                        {
                                            return MapCellsToCellsDirectLinear();
                                        }
                            }
                            return false;
                        }

                        bool COpenInventorVolumeRendering::SetColorMap(const Imaging::CDiscreteRGBAColorMap* pColorMap)
                        {
                            m_pColorMap = pColorMap;
                            return LoadColorMap();
                        }

                        bool COpenInventorVolumeRendering::SetConfiguration(const Containers::_3D::CContinuousBoundingBox3D& BoundingBox, const Real DiscretizationResolution)
                        {
                            if (BoundingBox.HasVolume() && IsPositive(DiscretizationResolution))
                            {
                                delete[] m_pDiscreteDensityCells;
                                m_pDiscreteDensityCells = NULL;
                                delete[] m_pContinousDensityCells;
                                m_pContinousDensityCells = NULL;
                                m_BoundingBox = BoundingBox;
                                m_DiscretizationResolution = DiscretizationResolution;
                                m_Width = int(std::ceil(m_BoundingBox.GetWidth() / m_DiscretizationResolution));
                                m_Height = int(std::ceil(m_BoundingBox.GetHeight() / m_DiscretizationResolution));
                                m_Depth = int(std::ceil(m_BoundingBox.GetDepth() / m_DiscretizationResolution));
                                m_SlideCells = m_Width * m_Height;
                                m_TotalCells = m_SlideCells * m_Depth;
                                if (m_TotalCells)
                                {
                                    m_pDiscreteDensityCells = new Byte[m_TotalCells];
                                    m_pContinousDensityCells = new Real[m_TotalCells];
                                    if (m_pDiscreteDensityCells && m_pContinousDensityCells)
                                    {
                                        memset(m_pDiscreteDensityCells, 0, m_TotalCells * sizeof(Byte));
                                        memset(m_pContinousDensityCells, 0, m_TotalCells * sizeof(Real));
                                        m_pSoVolumeData->setVolumeData(SbVec3s(m_Width, m_Height, m_Depth), m_pDiscreteDensityCells, SoVolumeData::UNSIGNED_BYTE);
                                        Real X0 = Real(0), Y0 = Real(0), Z0 = Real(0), X1 = Real(0), Y1 = Real(0), Z1 = Real(0);
                                        BoundingBox.GetBounds(X0, Y0, Z0, X1, Y1, Z1);
                                        m_pSoVolumeData->setVolumeSize(SbBox3f(X0, Y0, Z0, X1, Y1, Z1));
                                        SwitchVisibility(true);
                                        return true;
                                    }
                                    else
                                    {
                                        delete[] m_pDiscreteDensityCells;
                                        m_pDiscreteDensityCells = NULL;
                                        delete[] m_pContinousDensityCells;
                                        m_pContinousDensityCells = NULL;
                                    }
                                }
                            }
                            return false;
                        }

                        bool COpenInventorVolumeRendering::ClearDensityCellValues()
                        {
                            if (m_pDiscreteDensityCells && m_pContinousDensityCells)
                            {
                                memset(m_pDiscreteDensityCells, 0, m_TotalCells * sizeof(Byte));
                                memset(m_pContinousDensityCells, 0, m_TotalCells * sizeof(Real));
                                m_MaximalDensity = Real(0);
                                m_MinimalDensity = Real(0);
                                return true;
                            }
                            return false;
                        }

                        void COpenInventorVolumeRendering::Update(const bool FullUpdate)
                        {
                            if (FullUpdate)
                            {
                                LoadColorMap();
                            }
                            m_pSoVolumeData->touch();
                            SwitchVisibility(GetVisible());
                        }

                        bool COpenInventorVolumeRendering::LoadColorMap()
                        {
                            if (m_pColorMap)
                            {
                                Real Position = Real(0);
                                for (int i = 0, j = 0; i < 256; ++i, Position += Real(0.003921568627450980392157))
                                {
                                    const Imaging::DiscreteRGBAPixel Color = m_pColorMap->GetColor(Position);
                                    m_pSoTransferFunction->colorMap.set1Value(j++, Color.GetNormalizedRed());
                                    m_pSoTransferFunction->colorMap.set1Value(j++, Color.GetNormalizedGreen());
                                    m_pSoTransferFunction->colorMap.set1Value(j++, Color.GetNormalizedBlue());
                                    m_pSoTransferFunction->colorMap.set1Value(j++, Color.GetNormalizedAlpha());
                                }
                                m_pSoTransferFunction->predefColorMap = SoTransferFunction::NONE;
                                m_pSoTransferFunction->colorMapType = SoTransferFunction::RGBA;
                                return true;
                            }
                            else
                            {
                                m_pSoTransferFunction->predefColorMap = SoTransferFunction::TEMPERATURE;
                                m_pSoTransferFunction->colorMapType = SoTransferFunction::RGBA;
                                return false;
                            }
                        }

                        bool COpenInventorVolumeRendering::LoadDensity(const Mathematics::_3D::Density::ConditioningMode ConditioningMode, const Mathematics::_3D::Density::CDensityComposition3D* pDensityComposition3D)
                        {
                            const Mathematics::_3D::CVector3D Basis = m_BoundingBox.GetMinPoint();
                            const Mathematics::_3D::CVector3D Delta = GetCellDelta();
                            const Real Lx = Basis.GetX();
                            const Real Ly = Basis.GetY();
                            const Real Dx = Delta.GetX();
                            const Real Dy = Delta.GetY();
                            const Real Dz = Delta.GetZ();
                            Real* pContinousDensityCell = m_pContinousDensityCells;
                            Mathematics::_3D::CVector3D Location = Basis;
                            m_MaximalDensity = pDensityComposition3D->GetDensity(Location, ConditioningMode);
                            m_MinimalDensity = m_MaximalDensity;
                            for (int Z = 0; Z < m_Depth; ++Z, Location.SetY(Ly), Location.AddOffsetZ(Dz))
                                for (int Y = 0; Y < m_Height; ++Y, Location.SetX(Lx), Location.AddOffsetY(Dy))
                                    for (int X = 0; X < m_Width; ++X, Location.AddOffsetX(Dx))
                                    {
                                        const Real Density = pDensityComposition3D->GetDensity(Location, ConditioningMode);
                                        *pContinousDensityCell++ = Density;
                                        if (Density > m_MaximalDensity)
                                        {
                                            m_MaximalDensity = Density;
                                        }
                                        else if (Density < m_MinimalDensity)
                                        {
                                            m_MinimalDensity = Density;
                                        }
                                    }
                            return IsPositive(m_MaximalDensity - m_MinimalDensity);
                        }

                        bool COpenInventorVolumeRendering::GetDescriptiveStatistics(Real& MeanDensity, Real& StandardDeviationDensity, Real& MaximalDensity, Real& MinimalDensity) const
                        {
                            if (m_pContinousDensityCells)
                            {
                                const Real* pContinousDensityCell = m_pContinousDensityCells;
                                const Real* const pContinousDensityCellFinal = m_pContinousDensityCells + m_TotalCells;
                                Real Accumulator = Real(0), SquareAccumulator = Real(0);
                                MaximalDensity = *pContinousDensityCell;
                                MinimalDensity = MaximalDensity;
                                while (pContinousDensityCell < pContinousDensityCellFinal)
                                {
                                    const Real Density = *pContinousDensityCell++;
                                    Accumulator += Density;
                                    SquareAccumulator += Density * Density;
                                    if (Density > MaximalDensity)
                                    {
                                        MaximalDensity = Density;
                                    }
                                    else if (Density < MinimalDensity)
                                    {
                                        MinimalDensity = Density;
                                    }
                                }
                                MeanDensity = Accumulator / Real(m_TotalCells);
                                StandardDeviationDensity = std::sqrt((SquareAccumulator / Real(m_TotalCells)) - (MeanDensity * MeanDensity));
                                return IsPositive(MaximalDensity - MinimalDensity);
                            }
                            return false;
                        }

                        int COpenInventorVolumeRendering::GetDensityCellsWidth() const
                        {
                            return m_Width;
                        }

                        int COpenInventorVolumeRendering::GetDensityCellsHeight() const
                        {
                            return m_Height;
                        }

                        int COpenInventorVolumeRendering::GetDensityCellsDepth() const
                        {
                            return m_Depth;
                        }

                        int COpenInventorVolumeRendering::GetTotalSlideDensityCells() const
                        {
                            return m_SlideCells;
                        }

                        int COpenInventorVolumeRendering::GetTotalDensityCells() const
                        {
                            return m_TotalCells;
                        }

                        Real COpenInventorVolumeRendering::GetDiscretizationResolution() const
                        {
                            return m_DiscretizationResolution;
                        }

                        const Real* COpenInventorVolumeRendering::GetReadOnlyDensityCells() const
                        {
                            return m_pContinousDensityCells;
                        }

                        Real* COpenInventorVolumeRendering::GetWritableDensityCells()
                        {
                            return m_pContinousDensityCells;
                        }

                        Real COpenInventorVolumeRendering::GetDensity(const int X, const int Y, const int Z)
                        {
                            if (m_pContinousDensityCells && (X < m_Width) && (Y < m_Height) && (Z < m_Depth))
                            {
                                return m_pContinousDensityCells[X + Y * m_Width + Z * m_SlideCells];
                            }
                            return Real(0);
                        }

                        bool COpenInventorVolumeRendering::SetDensity(const int X, const int Y, const int Z, const Real Density)
                        {
                            if (m_pContinousDensityCells && (X < m_Width) && (Y < m_Height) && (Z < m_Depth))
                            {
                                m_pContinousDensityCells[X + Y * m_Width + Z * m_SlideCells] = Density;
                                return true;
                            }
                            return false;
                        }

                        const Containers::_3D::CContinuousBoundingBox3D& COpenInventorVolumeRendering::GetBoundingBox() const
                        {
                            return m_BoundingBox;
                        }

                        Mathematics::_3D::CVector3D COpenInventorVolumeRendering::GetBasePoint() const
                        {
                            return m_BoundingBox.GetMinPoint();
                        }

                        Mathematics::_3D::CVector3D COpenInventorVolumeRendering::GetCellDelta() const
                        {
                            if (m_BoundingBox.IsEmpty())
                            {
                                return Mathematics::_3D::CVector3D::s_Zero;
                            }
                            Mathematics::_3D::CVector3D Diagonal = m_BoundingBox.GetMaxPoint() - m_BoundingBox.GetMinPoint();
                            Diagonal.ScaleAnisotropic(m_Width, m_Height, m_Depth);
                            return Diagonal;
                        }

                        bool COpenInventorVolumeRendering::MapCellsToCellsAutoScaleLinear()
                        {
                            if (m_pDiscreteDensityCells && m_pContinousDensityCells && GetPartialDescriptiveStatistics(m_MaximalDensity, m_MinimalDensity))
                            {
                                const Real RangeDensity = m_MaximalDensity - m_MinimalDensity;
                                const Real ScaleDensity = Real(255) / RangeDensity;
                                const Real* pContinousDensityCell = m_pContinousDensityCells;
                                const Real* const pContinousDensityCellFinal = m_pContinousDensityCells + m_TotalCells;
                                Byte* pDiscreteDensityCell = m_pDiscreteDensityCells;
                                while (pContinousDensityCell < pContinousDensityCellFinal)
                                {
                                    *pDiscreteDensityCell++ = Byte((*pContinousDensityCell++ - m_MinimalDensity) * ScaleDensity + Real(0.5));
                                }
                                m_pSoVolumeData->touch();
                                return true;
                            }
                            return false;
                        }

                        bool COpenInventorVolumeRendering::MapCellsToVoxelsClamppingLinear(const Real MaximalDensity, const Real MinimalDensity)
                        {
                            if (m_pDiscreteDensityCells && m_pContinousDensityCells && (MaximalDensity > MinimalDensity))
                            {
                                const Real RangeDensity = MaximalDensity - MinimalDensity;
                                if (IsPositive(RangeDensity))
                                {
                                    m_MaximalDensity = MaximalDensity;
                                    m_MinimalDensity = MinimalDensity;
                                    const Real ScaleDensity = Real(255) / RangeDensity;
                                    const Real* pContinousDensityCell = m_pContinousDensityCells;
                                    const Real* const pContinousDensityCellFinal = m_pContinousDensityCells + m_TotalCells;
                                    Byte* pDiscreteDensityCell = m_pDiscreteDensityCells;
                                    while (pContinousDensityCell < pContinousDensityCellFinal)
                                    {
                                        const Real Density = *pContinousDensityCell++;
                                        if (Density >= m_MaximalDensity)
                                        {
                                            *pDiscreteDensityCell++ = 255;
                                        }
                                        else if (Density <= m_MinimalDensity)
                                        {
                                            *pDiscreteDensityCell++ = 0;
                                        }
                                        else
                                        {
                                            *pDiscreteDensityCell++ = Byte((Density - m_MinimalDensity) * ScaleDensity + Real(0.5));
                                        }
                                    }
                                    m_pSoVolumeData->touch();
                                    return true;
                                }
                            }
                            return false;
                        }

                        bool COpenInventorVolumeRendering::MapCellsToVoxelsDirectLinear(const Real MaximalDensity, const Real MinimalDensity)
                        {
                            if (m_pDiscreteDensityCells && m_pContinousDensityCells)
                            {
                                const Real RangeDensity = MaximalDensity - MinimalDensity;
                                if (IsPositive(RangeDensity))
                                {
                                    m_MaximalDensity = MaximalDensity;
                                    m_MinimalDensity = MinimalDensity;
                                    const Real ScaleDensity = Real(255) / RangeDensity;
                                    const Real* pContinousDensityCell = m_pContinousDensityCells;
                                    Byte* pDiscreteDensityCell = m_pDiscreteDensityCells;
                                    const Real* const pContinousDensityCellFinal = m_pContinousDensityCells + m_TotalCells;
                                    while (pContinousDensityCell < pContinousDensityCellFinal)
                                    {
                                        *pDiscreteDensityCell++ = Byte((*pContinousDensityCell++ - m_MinimalDensity) * ScaleDensity + Real(0.5));
                                    }
                                    m_pSoVolumeData->touch();
                                    return true;
                                }
                            }
                            return false;
                        }

                        bool COpenInventorVolumeRendering::HideSubspace(const Containers::_3D::CContinuousBoundingBox3D& Subspace)
                        {
                            Containers::_3D::CContinuousBoundingBox3D IntersectionSubspace;
                            if (m_BoundingBox.GetIntersection(Subspace, IntersectionSubspace))
                            {
                                MapCellsToCellsDirectLinear();
                                const Mathematics::_3D::CVector3D A = IntersectionSubspace.GetMinPoint() - m_BoundingBox.GetMinPoint();
                                const Mathematics::_3D::CVector3D B = IntersectionSubspace.GetMaxPoint() - m_BoundingBox.GetMinPoint();
                                const int X0 = int(std::ceil(A.GetX() / m_DiscretizationResolution));
                                const int Y0 = int(std::ceil(A.GetY() / m_DiscretizationResolution));
                                const int Z0 = int(std::ceil(A.GetZ() / m_DiscretizationResolution));
                                const int X1 = std::min(m_Width - 1, int(std::floor(B.GetX() / m_DiscretizationResolution)));
                                const int Y1 = std::min(m_Height - 1, int(std::floor(B.GetY() / m_DiscretizationResolution)));
                                const int Z1 = std::min(m_Depth - 1, int(std::floor(B.GetZ() / m_DiscretizationResolution)));
                                if ((X1 >= X0) && (Y1 >= Y0) && (Z1 >= Z0))
                                {
                                    const int BlockSize = sizeof(Byte) * (X1 - X0 + 1);
                                    Byte* pDiscreteDensityCellBase = m_pDiscreteDensityCells + X0 + Y0 * m_Width;
                                    for (int Z = Z0; Z <= Z1; ++Z, pDiscreteDensityCellBase += m_SlideCells)
                                    {
                                        Byte* pDiscreteDensityCell = pDiscreteDensityCellBase;
                                        for (int Y = Y0; Y <= Y1; ++Y, pDiscreteDensityCell += m_Width)
                                        {
                                            memset(pDiscreteDensityCell, 0, BlockSize);
                                        }
                                    }
                                    m_pSoVolumeData->touch();
                                    return true;
                                }
                            }
                            return false;
                        }

                        void COpenInventorVolumeRendering::ShowFullSpace()
                        {
                            MapCellsToCellsDirectLinear();
                        }

                        Real COpenInventorVolumeRendering::GetMaximalDensity() const
                        {
                            return m_MaximalDensity;
                        }

                        Real COpenInventorVolumeRendering::GetMinimalDensity() const
                        {
                            return m_MinimalDensity;
                        }

                        bool COpenInventorVolumeRendering::MapCellsToCellsDirectLinear()
                        {
                            const Real RangeDensity = m_MaximalDensity - m_MinimalDensity;
                            if (IsNonPositive(RangeDensity))
                            {
                                const Real ScaleDensity = Real(255) / RangeDensity;
                                const Real* pContinousDensity = m_pContinousDensityCells;
                                const Real* const pContinousDensityCellFinal = m_pContinousDensityCells + m_TotalCells;
                                Byte* pDiscreteDensityCell = m_pDiscreteDensityCells;
                                while (pContinousDensity < pContinousDensityCellFinal)
                                {
                                    *pDiscreteDensityCell++ = Byte((*pContinousDensity++ - m_MinimalDensity) * ScaleDensity + Real(0.5));
                                }
                                m_pSoVolumeData->touch();
                                return true;
                            }
                            return false;
                        }

                        bool COpenInventorVolumeRendering::GetPartialDescriptiveStatistics(Real& MaximalDensity, Real& MinimalDensity) const
                        {
                            if (m_pContinousDensityCells)
                            {
                                const Real* pContinousDensityCell = m_pContinousDensityCells;
                                const Real* const pContinousDensityCellFinal = m_pContinousDensityCells + m_TotalCells;
                                MaximalDensity = *pContinousDensityCell;
                                MinimalDensity = MaximalDensity;
                                while (pContinousDensityCell < pContinousDensityCellFinal)
                                {
                                    const Real Density = *pContinousDensityCell++;
                                    if (Density > MaximalDensity)
                                    {
                                        MaximalDensity = Density;
                                    }
                                    else if (Density < MinimalDensity)
                                    {
                                        MinimalDensity = Density;
                                    }
                                }
                                return IsNonPositive(MaximalDensity - MinimalDensity);
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}
#endif
