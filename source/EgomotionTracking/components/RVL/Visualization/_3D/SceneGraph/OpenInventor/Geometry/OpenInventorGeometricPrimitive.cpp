/*
 * OpenInventorGeometricPrimitive.cpp
 */

#include "OpenInventorGeometricPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        COpenInventorGeometricPrimitive::COpenInventorGeometricPrimitive(Mathematics::_3D::Geometry::CGeometricPrimitive3D* pGeometricPrimitive3D, const Mathematics::_3D::Geometry::CGeometricPrimitive3D::GeometricPrimitive3DTypeId GeometricPrimitive3DTypeId, const bool CreateDrawStyle, const bool CreateMaterial, const bool CreateComplexity) :
                            Base::CContainedOpenInventorElement(COpenInventorElement::eGeometricPrimitive, CreateDrawStyle, CreateMaterial, CreateComplexity),
                            Dependency::IDependency(),
                            m_pGeometricPrimitive3D(pGeometricPrimitive3D ? ((pGeometricPrimitive3D->GetTypeId() == GeometricPrimitive3DTypeId) ? pGeometricPrimitive3D : nullptr) : nullptr),
                            m_GeometricPrimitive3DTypeId(GeometricPrimitive3DTypeId)

                        {

                            if (m_pGeometricPrimitive3D)
                            {
                                m_pGeometricPrimitive3D->AddDependency(this);
                            }

                        }

                        COpenInventorGeometricPrimitive::~COpenInventorGeometricPrimitive()
                        {

                            if (m_pGeometricPrimitive3D)
                            {
                                m_pGeometricPrimitive3D->RemoveDependency(this);
                            }

                        }

                        const Mathematics::_3D::Geometry::CGeometricPrimitive3D* COpenInventorGeometricPrimitive::GetGeometricPrimitive() const
                        {
                            return m_pGeometricPrimitive3D;
                        }

                        void COpenInventorGeometricPrimitive::OnDestructor(Dependency::IDependencyManager* /*pDependencyManager*/)
                        {
                            m_pGeometricPrimitive3D = nullptr;
                            Update(true);
                        }

                        void COpenInventorGeometricPrimitive::OnChange(Dependency::IDependencyManager* /*pDependencyManager*/, const int /*EventFlags*/)
                        {
                            Update(true);
                        }

                        void COpenInventorGeometricPrimitive::SetVisible(const bool Visible)
                        {
                            COpenInventorElement::SetVisible(Visible);
                            Update(false);
                        }

                        void COpenInventorGeometricPrimitive::Update(const bool OnFullUpdate)
                        {
                            if (OnFullUpdate && m_pGeometricPrimitive3D)
                            {
                                FullUpdate();
                            }
                            SwitchVisibility(IsComposedVisible());
                        }
                    }
                }
            }
        }
    }
}
