/*
 * OpenInventorLine.cpp
 */

#include "OpenInventorLine.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        COpenInventorLine::COpenInventorLine(Mathematics::_3D::Geometry::CLine3D* pLine) :
                            COpenInventorGeometricPrimitive(pLine, Mathematics::_3D::Geometry::CGeometricPrimitive3D::eLine, true, true, true)
                        {
                            Create();
                            Update(false);
                        }

                        COpenInventorLine::~COpenInventorLine()
                            = default;

                        void COpenInventorLine::Create()
                        {
                            m_pSoCoordinate3 = new SoCoordinate3;
                            AddElement(m_pSoCoordinate3);

                            m_pSoLineSet = new SoLineSet;
                            AddElement(m_pSoLineSet);

                            m_pSoLineSet->setUserData(this);

                            SetLineWidth(_VISUALIZATION_LINE_DEFAULT_LINE_WIDTH_);
                            SetLinePattern(_VISUALIZATION_LINE_DEFAULT_LINE_PATTERN_);
                            SetTransparency(_VISUALIZATION_LINE_DEFAULT_TRANSPARENCY_);
                        }

                        void COpenInventorLine::FullUpdate()
                        {
                        }

                        bool COpenInventorLine::IsComposedVisible()
                        {
                            if (GetVisible() && m_pGeometricPrimitive3D)
                            {
                                Mathematics::_3D::Geometry::CLine3D* pLine = dynamic_cast<Mathematics::_3D::Geometry::CLine3D*>(m_pGeometricPrimitive3D);
                                if (m_pContainerSet)
                                {
                                    SbVec3f Ap, Bp;
                                    switch (m_pContainerSet->MapLine(SbLine(T(pLine->GetSupportPoint()), T(pLine->GetPointAtUnitaryDisplacement())), Ap, Bp))
                                    {
                                        case eEmptyInclusion:
                                        case eImproperInclusion:
                                            return false;
                                        case ePartialInclusion:
                                            m_pSoCoordinate3->point.set1Value(0, Ap);
                                            m_pSoCoordinate3->point.set1Value(1, Bp);
                                            m_pSoLineSet->numVertices.setValue(2);
                                            return true;
                                        case eFullInclusion:
                                            return true;
                                    }
                                    return false;
                                }
                                else
                                {
                                    Mathematics::_3D::CVector3D Aq, Bq;
                                    if (pLine->GetSourcePoints(Aq, Bq))
                                    {
                                        m_pSoCoordinate3->point.set1Value(0, T(Aq));
                                        m_pSoCoordinate3->point.set1Value(1, T(Bq));
                                        m_pSoLineSet->numVertices.setValue(2);
                                        return true;
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}
