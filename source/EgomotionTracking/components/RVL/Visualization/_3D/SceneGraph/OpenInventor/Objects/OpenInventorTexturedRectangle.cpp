/*
 * OpenInventorTexturedRectangle.cpp
 */

#include "OpenInventorTexturedRectangle.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Objects
                    {
                        COpenInventorTexturedRectangle::COpenInventorTexturedRectangle(const std::string& FileName, const SbVec3f& Origin, const SbVec3f& A, const SbVec3f& B, const SbVec2f& Repetitions) :
                            COpenInventorElement(COpenInventorElement::eObject),
                            m_TextureFileName(FileName)
                        {
                            Create();
                            const SbVec3f DeltaA = A - Origin;
                            const SbVec3f DeltaB = B - Origin;
                            const SbVec3f AB = Origin + DeltaA + DeltaB;
                            m_RectangleVertices[0] = AB;
                            m_RectangleVertices[1] = B;
                            m_RectangleVertices[2] = Origin;
                            m_RectangleVertices[3] = A;
                            m_TextureVertices[0].setValue(Repetitions[0], 0.0f);
                            m_TextureVertices[1].setValue(0.0f, 0.0f);
                            m_TextureVertices[2].setValue(0.0f, Repetitions[1]);
                            m_TextureVertices[3].setValue(Repetitions[0], Repetitions[1]);
                            SbVec3f Normal = DeltaB.cross(DeltaA);
                            Normal.normalize();
                            m_pSoNormal->vector.set1Value(0, Normal);
                            Update(true);
                        }

                        COpenInventorTexturedRectangle::COpenInventorTexturedRectangle(const std::string& FileName, const float LenghtX, const float LenghtY, const float OffsetZ, const float PatternRepetitionsX, const float PatternRepetitionsY) :
                            COpenInventorElement(COpenInventorElement::eObject),
                            m_TextureFileName(FileName)
                        {
                            Create();
                            const float X = LenghtX * 0.5f;
                            const float Y = LenghtY * 0.5f;
                            m_RectangleVertices[0].setValue(X, Y, OffsetZ);
                            m_RectangleVertices[1].setValue(-X, Y, OffsetZ);
                            m_RectangleVertices[2].setValue(-X, -Y, OffsetZ);
                            m_RectangleVertices[3].setValue(X, -Y, OffsetZ);
                            m_TextureVertices[0].setValue(PatternRepetitionsX, 0.0f);
                            m_TextureVertices[1].setValue(0.0f, 0.0f);
                            m_TextureVertices[2].setValue(0.0f, PatternRepetitionsY);
                            m_TextureVertices[3].setValue(PatternRepetitionsX, PatternRepetitionsY);
                            m_pSoNormal->vector.set1Value(0, SbVec3f(0.0f, 0.0f, -1.0f));
                            Update(true);
                        }

                        COpenInventorTexturedRectangle::~COpenInventorTexturedRectangle()
                            = default;

                        bool COpenInventorTexturedRectangle::Set(const SbVec3f& Origin, const SbVec3f& A, const SbVec3f& B, const SbVec2f& Repetitions)
                        {
                            const SbVec3f DeltaA = A - Origin;
                            const SbVec3f DeltaB = B - Origin;
                            const SbVec3f AB = Origin + DeltaA + DeltaB;
                            m_RectangleVertices[0] = AB;
                            m_RectangleVertices[1] = B;
                            m_RectangleVertices[2] = Origin;
                            m_RectangleVertices[3] = A;
                            m_TextureVertices[0].setValue(Repetitions[0], 0.0f);
                            m_TextureVertices[1].setValue(0.0f, 0.0f);
                            m_TextureVertices[2].setValue(0.0f, Repetitions[1]);
                            m_TextureVertices[3].setValue(Repetitions[0], Repetitions[1]);
                            SbVec3f Normal = DeltaB.cross(DeltaA);
                            Normal.normalize();
                            m_pSoNormal->vector.set1Value(0, Normal);
                            Update(true);
                            return true;
                        }

                        void COpenInventorTexturedRectangle::Create()
                        {
                            m_pSoTextureCoordinate2 = new SoTextureCoordinate2;
                            AddElement(m_pSoTextureCoordinate2);
                            m_pSoTextureCoordinateBinding = new SoTextureCoordinateBinding;
                            AddElement(m_pSoTextureCoordinateBinding);
                            m_pSoTextureCoordinateBinding->value.setValue(SoTextureCoordinateBinding::PER_VERTEX);
                            m_pSoTexture2 = new SoTexture2;
                            AddElement(m_pSoTexture2);
                            m_pSoTexture2->filename.setValue(m_TextureFileName.c_str());
                            m_pSoTexture2->wrapS = SoTexture2::REPEAT;
                            m_pSoTexture2->wrapT = SoTexture2::REPEAT;
                            m_pSoCoordinate3 = new SoCoordinate3;
                            AddElement(m_pSoCoordinate3);
                            m_pSoNormal = new SoNormal;
                            AddElement(m_pSoNormal);
                            m_pSoNormalBinding = new SoNormalBinding;
                            AddElement(m_pSoNormalBinding);
                            m_pSoNormalBinding->value.setValue(SoNormalBinding::OVERALL);
                            m_pSoFaceSet = new SoFaceSet;
                            AddElement(m_pSoFaceSet);
                            m_pSoFaceSet->numVertices.set1Value(0, 4);
                            m_pSoFaceSet->setUserData(this);
                        }

                        void COpenInventorTexturedRectangle::Update(const bool FullUpdate)
                        {
                            if (FullUpdate)
                            {
                                for (int i = 0 ; i < 4 ; ++i)
                                {
                                    m_pSoCoordinate3->point.set1Value(i, m_RectangleVertices[i]);
                                    m_pSoTextureCoordinate2->point.set1Value(i, m_TextureVertices[i]);
                                }
                            }
                            SwitchVisibility(GetVisible());
                        }
                    }
                }
            }
        }
    }
}
