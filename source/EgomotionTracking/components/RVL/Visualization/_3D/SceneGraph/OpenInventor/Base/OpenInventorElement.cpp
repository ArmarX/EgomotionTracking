/*
 * OpenInventorElement.cpp
 */

#include "OpenInventorElement.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Base
                    {
                        int COpenInventorElement::s_InstanceIdCounter = 0;

                        void COpenInventorElement::ResetInstanceIdCounter()
                        {
                            s_InstanceIdCounter = 0;
                        }

                        COpenInventorElement::COpenInventorElement(const TypeId TypeId, const bool CreateDrawStyle, const bool CreateMaterial, const bool CreateComplexity) :
                            m_pBaseSwitch(nullptr),
                            m_pBaseSeparator(nullptr),
                            m_pBaseDrawStyle(nullptr),
                            m_pBaseMaterial(nullptr),
                            m_pBaseComplexity(nullptr),
                            m_InstanceId(++s_InstanceIdCounter),
                            m_IsVisible(true),
                            m_VisualizationTypeId(TypeId)
                        {
                            m_pBaseSwitch = new SoSwitch;
                            m_pBaseSwitch->ref();

                            m_pBaseSeparator = new SoExtSelection;
                            m_pBaseSwitch->addChild(m_pBaseSeparator);

                            if (CreateDrawStyle)
                            {
                                m_pBaseDrawStyle = new SoDrawStyle;
                                m_pBaseSeparator->addChild(m_pBaseDrawStyle);
                            }

                            if (CreateMaterial)
                            {
                                m_pBaseMaterial = new SoMaterial;
                                m_pBaseSeparator->addChild(m_pBaseMaterial);
                            }

                            if (CreateComplexity)
                            {
                                m_pBaseComplexity = new SoComplexity;
                                m_pBaseSeparator->addChild(m_pBaseComplexity);
                            }

                            m_pBaseSwitch->whichChild = 0;
                        }

                        COpenInventorElement::~COpenInventorElement()
                        {
                            m_pBaseSwitch->removeAllChildren();
                            m_pBaseSwitch->unref();
                        }

                        COpenInventorElement::TypeId COpenInventorElement::GetTypeId() const
                        {
                            return m_VisualizationTypeId;
                        }

                        SoSwitch* COpenInventorElement::GetBaseSwitch() const
                        {
                            return m_pBaseSwitch;
                        }

                        void COpenInventorElement::SetVisible(const bool Visible)
                        {
                            m_IsVisible = Visible;
                        }

                        bool COpenInventorElement::GetVisible() const
                        {
                            return m_IsVisible;
                        }

                        void COpenInventorElement::SetColor(const COpenInventorElement::PredefinedColor Color)
                        {
                            if (m_pBaseMaterial)
                            {
                                m_pBaseMaterial->diffuseColor = COpenInventorElement::GetPredefinedColor(Color);
                            }
                        }

                        void COpenInventorElement::SetColor(const Byte R, const Byte G, const Byte B)
                        {
                            if (m_pBaseMaterial)
                            {
                                m_pBaseMaterial->diffuseColor.setValue(float(std::min(Byte(255), std::max(R, Byte(0)))) / float(Byte(255)), float(std::min(Byte(255), std::max(G, Byte(0)))) / float(Byte(255)), float(std::min(Byte(255), std::max(B, Byte(0)))) / float(Byte(255)));
                            }
                        }

                        void COpenInventorElement::SetColor(const float R, const float G, const float B)
                        {
                            if (m_pBaseMaterial)
                            {
                                m_pBaseMaterial->diffuseColor.setValue(std::min(1.0f, std::max(R, 0.0f)), std::min(1.0f, std::max(G, 0.0f)), std::min(1.0f, std::max(B, 0.0f)));
                            }
                        }

                        void COpenInventorElement::SetColor(const SbColor& Color)
                        {
                            if (m_pBaseMaterial)
                            {
                                m_pBaseMaterial->diffuseColor = Color;
                            }
                        }

                        SbColor COpenInventorElement::GetColor() const
                        {
                            return m_pBaseMaterial ? m_pBaseMaterial->diffuseColor[0] : SbColor();
                        }

                        void COpenInventorElement::SetTransparency(const float Transparency)
                        {
                            if (m_pBaseMaterial)
                            {
                                m_pBaseMaterial->transparency = Transparency;
                            }
                        }

                        float COpenInventorElement::GetTransparency() const
                        {
                            return m_pBaseMaterial ? m_pBaseMaterial->transparency[0] : -1.0f;
                        }

                        void COpenInventorElement::SetDrawStyle(const SoDrawStyle::Style Style)
                        {
                            if (m_pBaseDrawStyle)
                            {
                                m_pBaseDrawStyle->style = Style;
                            }
                        }

                        SoDrawStyle::Style COpenInventorElement::GetDrawStyle() const
                        {
                            return (SoDrawStyle::Style)(m_pBaseDrawStyle ? m_pBaseDrawStyle->style.getValue() : -1);
                        }

                        void COpenInventorElement::SetLineWidth(const float LineWidth)
                        {
                            if (m_pBaseDrawStyle)
                            {
                                m_pBaseDrawStyle->lineWidth = LineWidth;
                            }
                        }

                        float COpenInventorElement::GetLineWidth() const
                        {
                            return m_pBaseDrawStyle ? m_pBaseDrawStyle->lineWidth.getValue() : -1.0f;
                        }

                        void COpenInventorElement::SetPointSize(const float PointSize)
                        {
                            if (m_pBaseDrawStyle)
                            {
                                m_pBaseDrawStyle->pointSize = PointSize;
                            }
                        }

                        float COpenInventorElement::GetPointSize() const
                        {
                            return m_pBaseDrawStyle ? m_pBaseDrawStyle->pointSize.getValue() : -1.0f;
                        }

                        void COpenInventorElement::SetLinePattern(const ushort Pattern)
                        {
                            if (m_pBaseDrawStyle)
                            {
                                m_pBaseDrawStyle->linePattern = Pattern;
                            }
                        }

                        ushort COpenInventorElement::GetLinePattern() const
                        {
                            return m_pBaseDrawStyle ? m_pBaseDrawStyle->linePattern.getValue() : 0;
                        }

                        void COpenInventorElement::SetComplexityType(const SoComplexity::Type Type)
                        {
                            if (m_pBaseComplexity)
                            {
                                m_pBaseComplexity->type = Type;
                            }
                        }

                        SoComplexity::Type COpenInventorElement::GetComplexityType() const
                        {
                            return (SoComplexity::Type)(m_pBaseComplexity ? m_pBaseComplexity->type.getValue() : -1);
                        }

                        void COpenInventorElement::SetComplexityValue(const float Complexity)
                        {
                            if (m_pBaseComplexity)
                            {
                                m_pBaseComplexity->value = Complexity;
                            }
                        }

                        float COpenInventorElement::GetComplexityValue() const
                        {
                            return m_pBaseComplexity ? m_pBaseComplexity->value.getValue() : -1.0f;
                        }

                        const SbColor COpenInventorElement::GetPredefinedColor(COpenInventorElement::PredefinedColor Color)
                        {
                            switch (Color)
                            {
                                case eBlack:
                                    return SbColor(0.0f, 0.0f, 0.0f);
                                case eGray3_4:
                                    return SbColor(0.75f, 0.75f, 0.75f);
                                case eGray1_2:
                                    return SbColor(0.5f, 0.5f, 0.5f);
                                case eGray1_4:
                                    return SbColor(0.25f, 0.25f, 0.25f);
                                case eWhite:
                                    return SbColor(1.0f, 1.0f, 1.0f);
                                case eRed:
                                    return SbColor(1.0f, 0.0f, 0.0f);
                                case eRed3_4:
                                    return SbColor(0.75f, 0.0f, 0.0f);
                                case eRed1_2:
                                    return SbColor(0.5f, 0.0f, 0.0f);
                                case eRed1_4:
                                    return SbColor(0.25f, 0.0f, 0.0f);
                                case eGreen:
                                    return SbColor(0.0f, 1.0f, 0.0f);
                                case eGreen3_4:
                                    return SbColor(0.0f, 0.75f, 0.0f);
                                case eGreen1_2:
                                    return SbColor(0.0f, 0.5f, 0.0f);
                                case eGreen1_4:
                                    return SbColor(0.0f, 0.25f, 0.0f);
                                case eBlue:
                                    return SbColor(0.0f, 0.0f, 1.0f);
                                case eBlue3_4:
                                    return SbColor(0.0f, 0.0f, 0.75f);
                                case eBlue1_2:
                                    return SbColor(0.0f, 0.0f, 0.5f);
                                case eBlue1_4:
                                    return SbColor(0.0f, 0.0f, 0.25f);
                                case eYellow:
                                    return SbColor(1.0f, 1.0f, 0.0f);
                                case eYellow3_4:
                                    return SbColor(0.75f, 0.75f, 0.0f);
                                case eYellow1_2:
                                    return SbColor(0.5f, 0.5f, 0.0f);
                                case eYellow1_4:
                                    return SbColor(0.25f, 0.25f, 0.0f);
                                case eCyan:
                                    return SbColor(0.0f, 1.0f, 1.0f);
                                case eCyan3_4:
                                    return SbColor(0.0f, 0.75f, 0.75f);
                                case eCyan1_2:
                                    return SbColor(0.0f, 0.5f, 0.5f);
                                case eCyan1_4:
                                    return SbColor(0.0f, 0.25f, 0.25f);
                                case eMagenta:
                                    return SbColor(1.0f, 0.0f, 1.0f);
                                case eMagenta3_4:
                                    return SbColor(0.75f, 0.0f, 0.75f);
                                case eMagenta1_2:
                                    return SbColor(0.5f, 0.0f, 0.5f);
                                case eMagenta1_4:
                                    return SbColor(0.25f, 0.0f, 0.25f);
                            }
                            return SbColor();
                        }

                        void COpenInventorElement::SwitchVisibility(const bool Visible)
                        {
                            if (Visible)
                            {
                                if (m_pBaseSwitch->whichChild.getValue() == 0)
                                {
                                    return;
                                }
                                m_pBaseSwitch->whichChild = 0;
                            }
                            else
                            {
                                if (m_pBaseSwitch->whichChild.getValue() == SO_SWITCH_NONE)
                                {
                                    return;
                                }
                                m_pBaseSwitch->whichChild = SO_SWITCH_NONE;
                            }
                        }

                        void COpenInventorElement::AddElement(SoNode* pNode)
                        {
                            if (pNode)
                            {
                                m_pBaseSeparator->addChild(pNode);
                            }
                        }

                        void COpenInventorElement::RemoveElement(SoNode* pNode)
                        {
                            if (pNode)
                            {
                                m_pBaseSeparator->removeChild(pNode);
                            }
                        }
                    }
                }
            }
        }
    }
}

