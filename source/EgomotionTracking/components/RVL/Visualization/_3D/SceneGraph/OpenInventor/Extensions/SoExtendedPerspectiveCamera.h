/*
 * SoExtendedPerspectiveCamera.h
 */

#pragma once

#include "../../../../../Sensors/Cameras/Pasive/Calibration/Geometric/MonocularCameraGeometricCalibration.h"
#include "../Base/OpenInventor.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Extensions
                    {
                        class SoExtendedPerspectiveCamera : public SoPerspectiveCamera
                        {
                        public:

                            enum CameraRenderingMode
                            {
                                eVirtualCameraParameteres, eCalibrationCameraParameters
                            };

                            SoExtendedPerspectiveCamera();
                            ~SoExtendedPerspectiveCamera() override;

                            bool LoadCalibration(const Sensors::Cameras::Pasive::Calibration::Geometric::CMonocularCameraGeometricCalibration* pCameraCalibration, const float Near, const float Far);
                            bool SetRenderingMode(const CameraRenderingMode Mode);
                            SbViewVolume getViewVolume(const float UseAspectRatio) const override;
                            void GLRender(SoGLRenderAction* pAction) override;

                        protected:

                            CameraRenderingMode m_RenderingMode;
                            const Sensors::Cameras::Pasive::Calibration::Geometric::CMonocularCameraGeometricCalibration* m_pCameraCalibration;
                            SbMatrix m_ProjectionMatrix;
                            SbVec2f m_ImageSize;
                            SbVec2f m_FocalLenght;
                            SbVec2f m_PrincipalPoint;
                            SbVec2f m_FieldOfView;
                            float m_Near;
                            float m_Far;
                            float m_AspectRatio;
                        };
                    }
                }
            }
        }
    }
}

