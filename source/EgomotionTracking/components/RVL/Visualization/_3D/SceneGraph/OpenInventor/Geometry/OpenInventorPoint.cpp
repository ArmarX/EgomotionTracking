/*
 * OpenInventorPoint.cpp
 */

#include "OpenInventorPoint.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        COpenInventorPoint::COpenInventorPoint(Mathematics::_3D::Geometry::CPoint3D* pPoint, const ShapeMode Mode) :
                            COpenInventorGeometricPrimitive(pPoint, Mathematics::_3D::Geometry::CGeometricPrimitive3D::ePoint, true, true, true),
                            m_ShapeMode(Mode),
                            m_pSoTranslation(nullptr)
                        {
                            memset(&m_Shape, 0, sizeof(Shape));
                            Create();
                            Update(true);
                        }

                        COpenInventorPoint::~COpenInventorPoint()
                            = default;

                        void COpenInventorPoint::SetShapeMode(const ShapeMode Mode)
                        {
                            if ((Mode != m_ShapeMode) && ((Mode == eSphere) || (Mode == eCube)))
                            {
                                float Radius = 0.0f;
                                switch (m_ShapeMode)
                                {
                                    case eSphere:
                                        Radius = m_Shape.m_pSoSphere->radius.getValue();
                                        RemoveElement(m_Shape.m_pSoSphere);
                                        break;
                                    case eCube:
                                        Radius = m_Shape.m_pCube->width.getValue();
                                        RemoveElement(m_Shape.m_pCube);
                                        break;
                                }
                                m_ShapeMode = Mode;
                                switch (m_ShapeMode)
                                {
                                    case eSphere:
                                        m_Shape.m_pSoSphere = new SoSphere;
                                        AddElement(m_Shape.m_pSoSphere);
                                        m_Shape.m_pSoSphere->setUserData(this);
                                        m_Shape.m_pSoSphere->radius = Radius;
                                        break;
                                    case eCube:
                                        m_Shape.m_pCube = new SoCube;
                                        AddElement(m_Shape.m_pCube);
                                        m_Shape.m_pCube->setUserData(this);
                                        m_Shape.m_pCube->width = Radius;
                                        m_Shape.m_pCube->height = Radius;
                                        m_Shape.m_pCube->depth = Radius;
                                        break;
                                }
                            }
                        }

                        COpenInventorPoint::ShapeMode COpenInventorPoint::GetShapeMode() const
                        {
                            return m_ShapeMode;
                        }

                        void COpenInventorPoint::SetDisplayRadius(const float DisplayRadius)
                        {
                            const float InRangeDisplayRadius = std::min(std::max(DisplayRadius, _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_), _VISUALIZATION_POINT_MAXIMAL_DISPLAY_RADIUS_);
                            switch (m_ShapeMode)
                            {
                                case eSphere:
                                    if (m_Shape.m_pSoSphere->radius.getValue() != InRangeDisplayRadius)
                                    {
                                        m_Shape.m_pSoSphere->radius = InRangeDisplayRadius;
                                    }
                                    break;
                                case eCube:
                                    if (m_Shape.m_pCube->width.getValue() != InRangeDisplayRadius)
                                    {
                                        m_Shape.m_pCube->width = InRangeDisplayRadius;
                                        m_Shape.m_pCube->height = InRangeDisplayRadius;
                                        m_Shape.m_pCube->depth = InRangeDisplayRadius;
                                    }
                                    break;
                            }
                        }

                        float COpenInventorPoint::GetDisplayaRadius() const
                        {
                            switch (m_ShapeMode)
                            {
                                case eSphere:
                                    return m_Shape.m_pSoSphere->radius.getValue();
                                case eCube:
                                    return m_Shape.m_pCube->width.getValue();
                            }
                            return 0.0f;
                        }

                        void COpenInventorPoint::Create()
                        {
                            m_pSoTranslation = new SoTranslation;
                            AddElement(m_pSoTranslation);
                            switch (m_ShapeMode)
                            {
                                case eSphere:
                                    m_Shape.m_pSoSphere = new SoSphere;
                                    AddElement(m_Shape.m_pSoSphere);
                                    m_Shape.m_pSoSphere->setUserData(this);
                                    m_Shape.m_pSoSphere->radius = _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_;
                                    break;
                                case eCube:
                                    m_Shape.m_pCube = new SoCube;
                                    AddElement(m_Shape.m_pCube);
                                    m_Shape.m_pCube->setUserData(this);
                                    m_Shape.m_pCube->width = _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_;
                                    m_Shape.m_pCube->height = _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_;
                                    m_Shape.m_pCube->depth = _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_;
                                    break;
                            }

                            SetComplexityValue(_VISUALIZATION_POINT_DISPLAY_COMPLEXITY_);
                        }

                        void COpenInventorPoint::FullUpdate()
                        {
                            Mathematics::_3D::Geometry::CPoint3D* pPoint = dynamic_cast<Mathematics::_3D::Geometry::CPoint3D*>(m_pGeometricPrimitive3D);
                            m_pSoTranslation->translation.setValue(T(pPoint->GetPoint()));
                        }

                        bool COpenInventorPoint::IsComposedVisible()
                        {
                            if (m_pGeometricPrimitive3D && GetVisible())
                            {
                                Mathematics::_3D::Geometry::CPoint3D* pPoint3D = dynamic_cast<Mathematics::_3D::Geometry::CPoint3D*>(m_pGeometricPrimitive3D);
                                if (m_pContainerSet)
                                    switch (m_pContainerSet->MapPoint(T(pPoint3D->GetPoint())))
                                    {
                                        case eEmptyInclusion:
                                        case eImproperInclusion:
                                            return false;
                                        case ePartialInclusion:
                                        case eFullInclusion:
                                            return true;
                                    }
                                else
                                {
                                    return true;
                                }
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}
