/*
 * OpenInventorIsosurface.cpp
 */

#include "OpenInventorIsosurface.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Frames
                    {
                        COpenInventorIsosurface::COpenInventorIsosurface() :
                            Base::COpenInventorElement(Base::COpenInventorElement::eIsosurface),
                            m_pSoCoordinate3(nullptr),
                            m_pSoNormal(nullptr),
                            m_pSoIndexedFaceSet(nullptr)
                        {
                            m_pSoCoordinate3 = new SoCoordinate3;
                            AddElement(m_pSoCoordinate3);
                            m_pSoNormal = new SoNormal;
                            AddElement(m_pSoNormal);
                            m_pSoIndexedFaceSet = new SoIndexedFaceSet;
                            AddElement(m_pSoIndexedFaceSet);
                        }

                        COpenInventorIsosurface::~COpenInventorIsosurface()
                            = default;

                        void COpenInventorIsosurface::SetVisible(const bool Visible)
                        {
                            Base::COpenInventorElement::SetVisible(Visible);
                            Update(false);
                        }

                        bool COpenInventorIsosurface::SetDensityCellGrid(Mathematics::_3D::Density::DiscreteTopology::CDensityCellGrid3D* pDensityCellGrid)
                        {
                            if (pDensityCellGrid)
                            {
                                Mathematics::_3D::Density::Isodensity::CIsodensitySurface3D IsodensitySurface;
                                if (IsodensitySurface.Generate(pDensityCellGrid))
                                {
                                    int VertexIndex = 0;
                                    const std::list<Mathematics::_3D::Density::Isodensity::CIsodensityVertex3D*>& IsodensityVertices = IsodensitySurface.GetIsodensityVertices();
                                    m_pSoCoordinate3->point.setNum(IsodensityVertices.size());
                                    for (auto pIsodensityVertex3D : IsodensityVertices)
                                    {
                                        const Mathematics::_3D::CVector3D& Location = pIsodensityVertex3D->GetPoint();
                                        const Mathematics::_3D::CVector3D& Normal = pIsodensityVertex3D->GetNormal();
                                        m_pSoCoordinate3->point.set1Value(VertexIndex, Location.GetX(), Location.GetY(), Location.GetZ());
                                        m_pSoNormal->vector.set1Value(VertexIndex++, Normal.GetX(), Normal.GetY(), Normal.GetZ());
                                    }
                                    int FaceIndex = 0;
                                    const std::list<Mathematics::_3D::Density::Isodensity::CIsodensityTriangularFace3D*>& IsodensityFaces = IsodensitySurface.GetIsodensityTriangularFaces();
                                    for (auto pIsodensityFace : IsodensityFaces)
                                    {
                                        m_pSoIndexedFaceSet->coordIndex.set1Value(FaceIndex++, pIsodensityFace->GetIndexIsodensityVertexA());
                                        m_pSoIndexedFaceSet->coordIndex.set1Value(FaceIndex++, pIsodensityFace->GetIndexIsodensityVertexB());
                                        m_pSoIndexedFaceSet->coordIndex.set1Value(FaceIndex++, pIsodensityFace->GetIndexIsodensityVertexC());
                                        m_pSoIndexedFaceSet->coordIndex.set1Value(FaceIndex++, -1);
                                    }
                                    return true;
                                }
                            }
                            return false;
                        }

                        void COpenInventorIsosurface::Update(const bool /*FullUpdate*/)
                        {
                            Base::COpenInventorElement::SwitchVisibility(GetVisible());
                        }
                    }
                }
            }
        }
    }
}
