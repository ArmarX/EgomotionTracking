/*
 * OpenInventor.h
 */

#pragma once

#include "../../../../../Common/Switches.h"
#include "../../../../../Common/DataTypes.h"
#include "../../../../../Common/Includes.h"

#ifdef _RVL_USE_OPEN_INVENTOR_

//OPEN INVENTOR COIN INCLUDES
#include <Inventor/SbString.h>
#include <Inventor/SbLinear.h>
#include <Inventor/SbVec3f.h>
#include <Inventor/SbBox3f.h>
#include <Inventor/SbColor.h>
#include <Inventor/SoInput.h>
#include <Inventor/SoDB.h>
#include <Inventor/SoType.h>
#include <Inventor/nodes/SoGroup.h>
#include <Inventor/nodes/SoNormal.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoPointSet.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoIndexedLineSet.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoComplexity.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoExtSelection.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoTextureCoordinate2.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/actions/SoGLRenderAction.h>
#include <Inventor/misc/SoState.h>
#include <Inventor/elements/SoProjectionMatrixElement.h>
#include <Inventor/elements/SoViewingMatrixElement.h>
#include <Inventor/elements/SoFocalDistanceElement.h>
#include <Inventor/actions/SoBoxHighlightRenderAction.h>
#include <Inventor/actions/SoLineHighlightRenderAction.h>
#include <Inventor/actions/SoSearchAction.h>

#include <Inventor/nodes/SoMatrixTransform.h>

#ifdef _RVL_USE_OPEN_INVENTOR_SIMVOLEON_

#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>

#endif

#ifdef _RVL_USE_QT4_

#include <QCoreApplication>
#include <QWidget>

#endif

#ifdef _RVL_USE_OPEN_INVENTOR_QT_

#include <GL/gl.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

#endif

//RVL INCLUDES
#include "../../../../../Mathematics/_3D/Vector3D.h"
#include "../../../../../Mathematics/_3D/Matrix3D.h"

//DEFINITIONS
#undef _VISUALIZATION_PRIMITIVE_DEFAULT_COMPLEXITY_VALUE_
#undef _VISUALIZATION_SPHERE_DEFAULT_TRANSPARENCY_
#undef _VISUALIZATION_CIRCLE_MAXIMAL_SEGMENT_LINES_
#undef _VISUALIZATION_CIRCLE_DEFAULT_LINE_WIDTH_
#undef _VISUALIZATION_VOLUME_DEFAULT_LINE_COLOR_
#undef _VISUALIZATION_VOLUME_DEFAULT_LINE_WIDTH_
#undef _VISUALIZATION_VOLUME_DEFAULT_TRANSPARENCY_
#undef _VISUALIZATION_POINT_PAIR_MINIMAL_DISPLAY_RADIUS_
#undef _VISUALIZATION_POINT_PAIR_MAXIMAL_DISPLAY_RADIUS_
#undef _VISUALIZATION_POINT_PAIR_DEFAULT_CONNECTING_LINE_PATTERN_
#undef _VISUALIZATION_POINT_PAIR_DEFAULT_CONNECTING_LINE_WIDTH_
#undef _VISUALIZATION_POINT_PAIR_DEFAULT_TRANSPARENCY_
#undef _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_
#undef _VISUALIZATION_POINT_MAXIMAL_DISPLAY_RADIUS_
#undef _VISUALIZATION_PLANE_DEFAULT_TRANSPARENCY_
#undef _VISUALIZATION_LINE_DEFAULT_LINE_WIDTH_
#undef _VISUALIZATION_LINE_DEFAULT_TRANSPARENCY_
#undef _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_AXIS_LENGTH_
#undef _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_CONE_LENGTH_
#undef _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_CONE_WIDTH_
#undef _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_TRANSPARENCY_

#define _VISUALIZATION_EPSILON_                                     0.001f
#define _VISUALIZATION_PRIMITIVE_DEFAULT_COMPLEXITY_VALUE_          1.0f
#define _VISUALIZATION_SPHERE_DEFAULT_TRANSPARENCY_                 0.75f
#define _VISUALIZATION_CIRCLE_MINIMAL_SEGMENT_LINES_                6
#define _VISUALIZATION_CIRCLE_MAXIMAL_SEGMENT_LINES_                90
#define _VISUALIZATION_CIRCLE_DEFAULT_LINE_WIDTH_                   1.0f
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y0Z0_               0
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y0Z0_               1
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y0Z1_               2
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y0Z1_               3
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y1Z0_               4
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y1Z0_               5
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y1Z1_               6
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y1Z1_               7
#define _VISUALIZATION_VOLUME_TOTAL_BOUNDING_VERTEX_X0Y1Z1_         8
#define _VISUALIZATION_VOLUME_BOUNDING_PLANE_XY0_                   0
#define _VISUALIZATION_VOLUME_BOUNDING_PLANE_XY1_                   1
#define _VISUALIZATION_VOLUME_BOUNDING_PLANE_XZ0_                   2
#define _VISUALIZATION_VOLUME_BOUNDING_PLANE_XZ1_                   3
#define _VISUALIZATION_VOLUME_BOUNDING_PLANE_YZ0_                   4
#define _VISUALIZATION_VOLUME_BOUNDING_PLANE_YZ1_                   5
#define _VISUALIZATION_VOLUME_TOTAL_BOUNDING_PLANES_                6
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X0Y0Z0_X1Y0Z0_          0
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X1Y0Z0_X1Y1Z0_          1
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X1Y1Z0_X0Y1Z0_          2
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X0Y1Z0_X0Y0Z0_          3
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X0Y0Z1_X1Y0Z1_          4
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X1Y0Z1_X1Y1Z1_          5
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X1Y1Z1_X0Y1Z1_          6
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X0Y1Z1_X0Y0Z1_          7
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X0Y0Z0_X0Y0Z1_          8
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X1Y0Z0_X1Y0Z1_          9
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X1Y1Z0_X1Y1Z1_          10
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X0Y1Z0_X0Y1Z1_          11
#define _VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_                 12
#define _VISUALIZATION_VOLUME_TOTAL_BOUNDING_VERTICES_              8
#define _VISUALIZATION_VOLUME_DEFAULT_LINE_COLOR_                   0.0f,0.0f,0.0f
#define _VISUALIZATION_VOLUME_DEFAULT_LINE_WIDTH_                   1.0f
#define _VISUALIZATION_VOLUME_DEFAULT_LINE_PATTERN_                 0X3333
#define _VISUALIZATION_VOLUME_DEFAULT_TRANSPARENCY_                 0.5f
#define _VISUALIZATION_POINT_PAIR_MINIMAL_DISPLAY_RADIUS_           0.5f
#define _VISUALIZATION_POINT_PAIR_MAXIMAL_DISPLAY_RADIUS_           10.0f
#define _VISUALIZATION_POINT_PAIR_DEFAULT_CONNECTING_LINE_PATTERN_  0X3333
#define _VISUALIZATION_POINT_PAIR_DEFAULT_CONNECTING_LINE_WIDTH_    1.0f
#define _VISUALIZATION_POINT_PAIR_DEFAULT_TRANSPARENCY_             0.25f
#define _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_                1.0f
#define _VISUALIZATION_POINT_MAXIMAL_DISPLAY_RADIUS_                10.0f
#define _VISUALIZATION_POINT_DISPLAY_COMPLEXITY_                    0.5f
#define _VISUALIZATION_PLANE_DEFAULT_TRANSPARENCY_                  0.75f
#define _VISUALIZATION_LINE_DEFAULT_LINE_WIDTH_                     1.0f
#define _VISUALIZATION_LINE_DEFAULT_LINE_PATTERN_                   0XFFFF
#define _VISUALIZATION_LINE_DEFAULT_TRANSPARENCY_                   0.0f
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_AXIS_LENGTH_       100.0f
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_CONE_LENGTH_       5.0f
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_CONE_WIDTH_        2.5f
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_TRANSPARENCY_      0.0f
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_XP_     "X"
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_YP_     "Y"
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_ZP_     "Z"
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_XN_     "-X"
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_YN_     "-Y"
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_ZN_     "-Z"
namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    SbVec3f T(const Mathematics::_3D::CVector3D& Vector);
                    float T(const Real X);
                }
            }
        }
    }
}

#endif /* _RVL_USE_OPEN_INVENTOR_ */

