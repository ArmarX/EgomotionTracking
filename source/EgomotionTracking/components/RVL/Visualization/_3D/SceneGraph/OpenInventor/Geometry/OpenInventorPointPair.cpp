/*
 * OpenInventorPointPair.cpp
 */

#include "OpenInventorPointPair.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        COpenInventorPointPair::COpenInventorPointPair(Mathematics::_3D::Geometry::CPointPair3D* pPointPair, const ShapeMode ModeA, const ShapeMode ModeB) :
                            COpenInventorGeometricPrimitive(pPointPair, Mathematics::_3D::Geometry::CGeometricPrimitive3D::ePointPair, true, true, true),
                            m_ShapeModeA(ModeA),
                            m_ShapeModeB(ModeB),
                            m_ConnectingLineVisible(true),
                            m_pConnectingLineSwitch(nullptr),
                            m_pConnectingLineDrawStyle(nullptr),
                            m_pConnectingLineCoordinate3(nullptr),
                            m_pConnectingLineSet(nullptr),
                            m_pSoTranslationA(nullptr),
                            m_pSoTranslationB(nullptr),
                            m_pSphereASwitch(nullptr),
                            m_pSphereBSwitch(nullptr)
                        {
                            memset(&m_ShapeA, 0, sizeof(Shape));
                            memset(&m_ShapeB, 0, sizeof(Shape));
                            Create();
                            Update(true);
                        }

                        COpenInventorPointPair::~COpenInventorPointPair()
                            = default;

                        void COpenInventorPointPair::SetShapeModeA(const ShapeMode ModeA)
                        {
                            if ((ModeA != m_ShapeModeA) && ((ModeA == eSphere) || (ModeA == eCube)))
                            {
                                float Radius = 0.0f;
                                switch (m_ShapeModeA)
                                {
                                    case eSphere:
                                        Radius = m_ShapeA.m_pSoSphere->radius.getValue();
                                        RemoveElement(m_ShapeA.m_pSoSphere);
                                        break;
                                    case eCube:
                                        Radius = m_ShapeA.m_pCube->width.getValue();
                                        RemoveElement(m_ShapeA.m_pCube);
                                        break;
                                }
                                m_ShapeModeA = ModeA;
                                switch (m_ShapeModeA)
                                {
                                    case eSphere:
                                        m_ShapeA.m_pSoSphere = new SoSphere;
                                        AddElement(m_ShapeA.m_pSoSphere);
                                        m_ShapeA.m_pSoSphere->setUserData(this);
                                        m_ShapeA.m_pSoSphere->radius = Radius;
                                        break;
                                    case eCube:
                                        m_ShapeA.m_pCube = new SoCube;
                                        AddElement(m_ShapeA.m_pCube);
                                        m_ShapeA.m_pCube->setUserData(this);
                                        m_ShapeA.m_pCube->width = Radius;
                                        m_ShapeA.m_pCube->height = Radius;
                                        m_ShapeA.m_pCube->depth = Radius;
                                        break;
                                }
                            }
                        }

                        COpenInventorPointPair::ShapeMode COpenInventorPointPair::GetShapeModeA() const
                        {
                            return m_ShapeModeA;
                        }

                        void COpenInventorPointPair::SetShapeModeB(const ShapeMode ModeB)
                        {
                            if ((ModeB != m_ShapeModeB) && ((ModeB == eSphere) || (ModeB == eCube)))
                            {
                                float Radius = 0.0f;
                                switch (m_ShapeModeB)
                                {
                                    case eSphere:
                                        Radius = m_ShapeB.m_pSoSphere->radius.getValue();
                                        RemoveElement(m_ShapeB.m_pSoSphere);
                                        break;
                                    case eCube:
                                        Radius = m_ShapeB.m_pCube->width.getValue();
                                        RemoveElement(m_ShapeB.m_pCube);
                                        break;
                                }
                                m_ShapeModeB = ModeB;
                                switch (m_ShapeModeB)
                                {
                                    case eSphere:
                                        m_ShapeB.m_pSoSphere = new SoSphere;
                                        AddElement(m_ShapeB.m_pSoSphere);
                                        m_ShapeB.m_pSoSphere->setUserData(this);
                                        m_ShapeB.m_pSoSphere->radius = Radius;
                                        break;
                                    case eCube:
                                        m_ShapeB.m_pCube = new SoCube;
                                        AddElement(m_ShapeB.m_pCube);
                                        m_ShapeB.m_pCube->setUserData(this);
                                        m_ShapeB.m_pCube->width = Radius;
                                        m_ShapeB.m_pCube->height = Radius;
                                        m_ShapeB.m_pCube->depth = Radius;
                                        break;
                                }
                            }
                        }

                        COpenInventorPointPair::ShapeMode COpenInventorPointPair::GetShapeModeB() const
                        {
                            return m_ShapeModeB;
                        }

                        void COpenInventorPointPair::SetDisplayRadiusA(const float DisplayRadiusA)
                        {
                            const float InRangeDisplayRadius = std::min(std::max(DisplayRadiusA, _VISUALIZATION_POINT_PAIR_MINIMAL_DISPLAY_RADIUS_), _VISUALIZATION_POINT_PAIR_MAXIMAL_DISPLAY_RADIUS_);
                            switch (m_ShapeModeA)
                            {
                                case eSphere:
                                    if (m_ShapeA.m_pSoSphere->radius.getValue() != InRangeDisplayRadius)
                                    {
                                        m_ShapeA.m_pSoSphere->radius = InRangeDisplayRadius;
                                    }
                                    break;
                                case eCube:
                                    if (m_ShapeA.m_pCube->width.getValue() != InRangeDisplayRadius)
                                    {
                                        m_ShapeA.m_pCube->width = InRangeDisplayRadius;
                                        m_ShapeA.m_pCube->height = InRangeDisplayRadius;
                                        m_ShapeA.m_pCube->depth = InRangeDisplayRadius;
                                    }
                                    break;
                            }
                        }

                        float COpenInventorPointPair::GetDisplayRadiusA() const
                        {
                            switch (m_ShapeModeA)
                            {
                                case eSphere:
                                    return m_ShapeA.m_pSoSphere->radius.getValue();
                                case eCube:
                                    return m_ShapeA.m_pCube->width.getValue();
                            }
                            return 0.0f;
                        }

                        void COpenInventorPointPair::SetDisplayRadiusB(const float DisplayRadiusB)
                        {
                            const float InRangeDisplayRadius = std::min(std::max(DisplayRadiusB, _VISUALIZATION_POINT_PAIR_MINIMAL_DISPLAY_RADIUS_), _VISUALIZATION_POINT_PAIR_MAXIMAL_DISPLAY_RADIUS_);
                            switch (m_ShapeModeB)
                            {
                                case eSphere:
                                    if (m_ShapeB.m_pSoSphere->radius.getValue() != InRangeDisplayRadius)
                                    {
                                        m_ShapeB.m_pSoSphere->radius = InRangeDisplayRadius;
                                    }
                                    break;
                                case eCube:
                                    if (m_ShapeB.m_pCube->width.getValue() != InRangeDisplayRadius)
                                    {
                                        m_ShapeB.m_pCube->width = InRangeDisplayRadius;
                                        m_ShapeB.m_pCube->height = InRangeDisplayRadius;
                                        m_ShapeB.m_pCube->depth = InRangeDisplayRadius;
                                    }
                                    break;
                            }
                        }

                        float COpenInventorPointPair::GetDisplayRadiusB() const
                        {
                            switch (m_ShapeModeB)
                            {
                                case eSphere:
                                    return m_ShapeB.m_pSoSphere->radius.getValue();
                                case eCube:
                                    return m_ShapeB.m_pCube->width.getValue();
                            }
                            return 0.0f;
                        }

                        void COpenInventorPointPair::SetConnectingLineVisible(const bool Visible)
                        {
                            if (m_ConnectingLineVisible != Visible)
                            {
                                m_ConnectingLineVisible = Visible;
                                if (m_ConnectingLineVisible)
                                {
                                    if (m_pContainerSet)
                                    {
                                        bool FlagA = false, FlagB = false;
                                        Mathematics::_3D::Geometry::CPointPair3D* pPointPair = dynamic_cast<Mathematics::_3D::Geometry::CPointPair3D*>(m_pGeometricPrimitive3D);
                                        m_pContainerSet->MapPointPair(T(pPointPair->GetPointA()), T(pPointPair->GetPointB()), FlagA, FlagB);
                                        m_pConnectingLineSwitch->whichChild = (FlagA && FlagB) ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                                    }
                                    else
                                    {
                                        m_pConnectingLineSwitch->whichChild = SO_SWITCH_ALL;
                                    }
                                }
                                else
                                {
                                    m_pConnectingLineSwitch->whichChild = SO_SWITCH_NONE;
                                }
                            }
                        }

                        bool COpenInventorPointPair::GetConnectingLineVisible() const
                        {
                            return m_ConnectingLineVisible;
                        }

                        void COpenInventorPointPair::SetConnectingLineWidth(const float LineWidth)
                        {
                            m_pConnectingLineDrawStyle->lineWidth = LineWidth;
                        }

                        float COpenInventorPointPair::GetConnectingLineWidth() const
                        {
                            return m_pConnectingLineDrawStyle->lineWidth.getValue();
                        }

                        void COpenInventorPointPair::SetConnectingLinePattern(const ushort Pattern)
                        {
                            m_pConnectingLineDrawStyle->linePattern = Pattern;
                        }

                        ushort COpenInventorPointPair::GetConnectingLinePattern() const
                        {
                            return m_pConnectingLineDrawStyle->linePattern.getValue();
                        }

                        void COpenInventorPointPair::Create()
                        {
                            m_pSoTranslationA = new SoTranslation;
                            AddElement(m_pSoTranslationA);
                            m_pSphereASwitch = new SoSwitch;
                            AddElement(m_pSphereASwitch);
                            m_ShapeA.m_pSoSphere = new SoSphere;
                            m_pSphereASwitch->addChild(m_ShapeA.m_pSoSphere);
                            m_ShapeA.m_pSoSphere->setUserData(this);
                            m_pSoTranslationB = new SoTranslation;
                            AddElement(m_pSoTranslationB);
                            m_pSphereBSwitch = new SoSwitch;
                            AddElement(m_pSphereBSwitch);
                            m_ShapeB.m_pSoSphere = new SoSphere;
                            m_pSphereBSwitch->addChild(m_ShapeB.m_pSoSphere);
                            m_ShapeB.m_pSoSphere->setUserData(this);
                            m_pConnectingLineSwitch = new SoSwitch;
                            AddElement(m_pConnectingLineSwitch);
                            m_pConnectingLineDrawStyle = new SoDrawStyle;
                            m_pConnectingLineSwitch->addChild(m_pConnectingLineDrawStyle);
                            m_pConnectingLineCoordinate3 = new SoCoordinate3;
                            m_pConnectingLineSwitch->addChild(m_pConnectingLineCoordinate3);
                            m_pConnectingLineSet = new SoLineSet;
                            m_pConnectingLineSwitch->addChild(m_pConnectingLineSet);
                            m_pConnectingLineSet->setUserData(this);
                            m_ConnectingLineVisible = false;
                            m_pConnectingLineSwitch->whichChild = m_ConnectingLineVisible ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                            m_pSphereASwitch->whichChild = 0;
                            m_pSphereBSwitch->whichChild = 0;
                            m_ShapeA.m_pSoSphere->radius = _VISUALIZATION_POINT_PAIR_MINIMAL_DISPLAY_RADIUS_;
                            m_ShapeB.m_pSoSphere->radius = _VISUALIZATION_POINT_PAIR_MINIMAL_DISPLAY_RADIUS_;
                            m_pConnectingLineDrawStyle->linePattern = _VISUALIZATION_POINT_PAIR_DEFAULT_CONNECTING_LINE_PATTERN_;
                            m_pConnectingLineDrawStyle->lineWidth = _VISUALIZATION_POINT_PAIR_DEFAULT_CONNECTING_LINE_WIDTH_;
                            SetTransparency(_VISUALIZATION_POINT_PAIR_DEFAULT_TRANSPARENCY_);
                            SetComplexityValue(0.125f);
                        }

                        void COpenInventorPointPair::FullUpdate()
                        {
                            Mathematics::_3D::Geometry::CPointPair3D* pPointPair = reinterpret_cast<Mathematics::_3D::Geometry::CPointPair3D*>(m_pGeometricPrimitive3D);
                            const SbVec3f& A = T(pPointPair->GetPointA());
                            const SbVec3f& B = T(pPointPair->GetPointB());
                            const SbVec3f Delta = B - A;
                            m_pSoTranslationA->translation.setValue(A);
                            m_pSoTranslationB->translation.setValue(Delta);
                            m_pConnectingLineCoordinate3->point.set1Value(0, -Delta);
                            m_pConnectingLineCoordinate3->point.set1Value(1, SbVec3f(0.0f, 0.0f, 0.0f));
                            m_pConnectingLineSet->numVertices.setValue(2);
                        }

                        bool COpenInventorPointPair::IsComposedVisible()
                        {
                            if (GetVisible() && m_pGeometricPrimitive3D)
                            {
                                if (m_pContainerSet)
                                {
                                    bool FlagA = false, FlagB = false;
                                    Mathematics::_3D::Geometry::CPointPair3D* pPointPair = dynamic_cast<Mathematics::_3D::Geometry::CPointPair3D*>(m_pGeometricPrimitive3D);
                                    switch (m_pContainerSet->MapPointPair(T(pPointPair->GetPointA()), T(pPointPair->GetPointB()), FlagA, FlagB))
                                    {
                                        case eEmptyInclusion:
                                        case eImproperInclusion:
                                            return false;
                                        case ePartialInclusion:
                                            m_pSphereASwitch->whichChild = FlagA ? 0 : SO_SWITCH_NONE;
                                            m_pSphereBSwitch->whichChild = FlagB ? 0 : SO_SWITCH_NONE;
                                            m_pConnectingLineSwitch->whichChild = SO_SWITCH_NONE;
                                            return true;
                                        case eFullInclusion:
                                            m_pSphereASwitch->whichChild = 0;
                                            m_pSphereBSwitch->whichChild = 0;
                                            m_pConnectingLineSwitch->whichChild = m_ConnectingLineVisible ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                                            return true;
                                    }
                                }
                                else
                                {
                                    m_pConnectingLineSwitch->whichChild = m_ConnectingLineVisible ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                                    m_pSphereASwitch->whichChild = 0;
                                    m_pSphereBSwitch->whichChild = 0;
                                    return true;
                                }
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}
