/*
 * ContainedOpenInventorElement.h
 */

#pragma once

#include "OpenInventorElement.h"
#include "ContainerOpenInventorSet.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Base
                    {
                        class CContainedOpenInventorElement : public COpenInventorElement
                        {
                        public:

                            CContainedOpenInventorElement(const COpenInventorElement::TypeId TypeId, const bool CreateDrawStyle, const bool CreateMaterial, const bool CreateComplexity);
                            ~CContainedOpenInventorElement() override;

                            void SetContainerSet(const CContainerOpenInventorSet* pContainerSet);
                            const CContainerOpenInventorSet* GetContainerSet() const;

                        protected:

                            virtual bool IsComposedVisible() = 0;

                            const CContainerOpenInventorSet* m_pContainerSet;
                        };
                    }
                }
            }
        }
    }
}

