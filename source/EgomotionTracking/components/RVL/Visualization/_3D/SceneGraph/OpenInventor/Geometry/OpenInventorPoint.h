/*
 * OpenInventorPoint.h
 */

#pragma once

#include "../../../../../Mathematics/_3D/Geometry/Point3D.h"
#include "OpenInventorGeometricPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        class COpenInventorPoint : public COpenInventorGeometricPrimitive
                        {
                        public:

                            enum ShapeMode
                            {
                                eSphere, eCube
                            };

                            COpenInventorPoint(Mathematics::_3D::Geometry::CPoint3D* pPoint, const ShapeMode Mode = eSphere);
                            ~COpenInventorPoint() override;

                            void SetShapeMode(const ShapeMode Mode);
                            ShapeMode GetShapeMode() const;

                            void SetDisplayRadius(const float DisplayRadius);
                            float GetDisplayaRadius() const;

                        protected:

                            void Create() override;
                            void FullUpdate() override;
                            bool IsComposedVisible() override;

                        private:

                            union Shape
                            {
                                SoSphere* m_pSoSphere;
                                SoCube* m_pCube;
                            };

                            ShapeMode m_ShapeMode;
                            Shape m_Shape;
                            SoTranslation* m_pSoTranslation;
                        };
                    }
                }
            }
        }
    }
}

