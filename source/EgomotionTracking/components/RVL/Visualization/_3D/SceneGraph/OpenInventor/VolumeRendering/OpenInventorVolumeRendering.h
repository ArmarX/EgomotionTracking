/*
 * OpenInventorVolumeRendering.h
 */

#pragma once

#include "../../../../../Mathematics/_3D/Density/DensityComposition3D.h"
#include "../../../../../Containers/_3D/ContinuousBoundingBox3D.h"
#include "../../../../../Imaging/DiscreteRGBAColorMap.h"
#include "../../../../Color/Color.h"
#include "../Base/OpenInventor.h"
#include "../Base/OpenInventorElement.h"
#include "../Base/ContainedOpenInventorElement.h"

#ifdef _RVL_USE_OPEN_INVENTOR_SIMVOLEON_
namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace VolumeRendering
                    {
                        class COpenInventorVolumeRendering: public Base::CContainedOpenInventorElement
                        {
                        public:

                            static Real DetermineDiscretizationResolution(const Containers::_3D::CContinuousBoundingBox3D& BoundingBox, const int TotalDiscretizationCells);
                            static int DetermineTotalDiscretizationCells(const Containers::_3D::CContinuousBoundingBox3D& BoundingBox, const Real DiscretizationResolution);

                            COpenInventorVolumeRendering(const Imaging::CDiscreteRGBAColorMap* pColorMap);
                            ~COpenInventorVolumeRendering();

                            virtual void SetVisible(const bool Visible);
                            bool SetDensityAggregation(const Mathematics::_3D::Density::ConditioningMode ConditioningMode, const Mathematics::_3D::Density::CDensityComposition3D* pDensityComposition3D, const Real MinimalDensity, const Real DiscretizationResolution);
                            bool SetColorMap(const Imaging::CDiscreteRGBAColorMap* pColorMap);
                            bool SetConfiguration(const Containers::_3D::CContinuousBoundingBox3D& BoundingBox, const Real DiscretizationResolution);
                            bool ClearDensityCellValues();
                            void Update(const bool FullUpdate);
                            bool LoadColorMap();
                            bool LoadDensity(const Mathematics::_3D::Density::ConditioningMode ConditioningMode, const Mathematics::_3D::Density::CDensityComposition3D* pDensityComposition3D);
                            bool GetDescriptiveStatistics(Real& MeanDensity, Real& StandardDeviationDensity, Real& MaximalDensity, Real& MinimalDensity) const;
                            int GetDensityCellsWidth() const;
                            int GetDensityCellsHeight() const;
                            int GetDensityCellsDepth() const;
                            int GetTotalSlideDensityCells() const;
                            int GetTotalDensityCells() const;
                            Real GetDiscretizationResolution() const;
                            const Real* GetReadOnlyDensityCells() const;
                            Real* GetWritableDensityCells();
                            Real GetDensity(const int X, const int Y, const int Z);
                            bool SetDensity(const int X, const int Y, const int Z, const Real Density);
                            const Containers::_3D::CContinuousBoundingBox3D& GetBoundingBox() const;
                            Mathematics::_3D::CVector3D GetBasePoint() const;
                            Mathematics::_3D::CVector3D GetCellDelta() const;
                            bool MapCellsToCellsAutoScaleLinear();
                            bool MapCellsToVoxelsClamppingLinear(const Real MaximalDensity, const Real MinimalDensity);
                            bool MapCellsToVoxelsDirectLinear(const Real MaximalDensity, const Real MinimalDensity);
                            bool HideSubspace(const Containers::_3D::CContinuousBoundingBox3D& Subspace);
                            void ShowFullSpace();
                            Real GetMaximalDensity() const;
                            Real GetMinimalDensity() const;
                            bool MapCellsToCellsDirectLinear();
                            bool GetPartialDescriptiveStatistics(Real& MaximalDensity, Real& MinimalDensity) const;

                        protected:

                            const Imaging::CDiscreteRGBAColorMap* m_pColorMap;
                            Real m_DiscretizationResolution;
                            Byte* m_pDiscreteDensityCells;
                            Real* m_pContinousDensityCells;
                            SoVolumeData* m_pSoVolumeData;
                            SoTransferFunction* m_pSoTransferFunction;
                            SoVolumeRender* m_pSoVolumeRender;
                            Containers::_3D::CContinuousBoundingBox3D m_BoundingBox;
                            int m_Width;
                            int m_Height;
                            int m_Depth;
                            int m_SlideCells;
                            int m_TotalCells;
                            Real m_MaximalDensity;
                            Real m_MinimalDensity;
                        };
                    }
                }
            }
        }
    }
}
#endif
