/*
 * ContainedOpenInventorElement.cpp
 */

#include "ContainedOpenInventorElement.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Base
                    {
                        CContainedOpenInventorElement::CContainedOpenInventorElement(const COpenInventorElement::TypeId TypeId, const bool CreateDrawStyle, const bool CreateMaterial, const bool CreateComplexity) :
                            COpenInventorElement(TypeId, CreateDrawStyle, CreateMaterial, CreateComplexity),
                            m_pContainerSet(nullptr)
                        {
                        }

                        CContainedOpenInventorElement::~CContainedOpenInventorElement()
                            = default;

                        void CContainedOpenInventorElement::SetContainerSet(const CContainerOpenInventorSet* pContainerSet)
                        {
                            m_pContainerSet = pContainerSet;
                        }

                        const CContainerOpenInventorSet* CContainedOpenInventorElement::GetContainerSet() const
                        {
                            return m_pContainerSet;
                        }
                    }
                }
            }
        }
    }
}
