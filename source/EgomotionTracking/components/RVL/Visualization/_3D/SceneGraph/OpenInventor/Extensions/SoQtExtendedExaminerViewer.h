/*
 * SoQtExtendedExaminerViewer.h
 */

#pragma once

#include "../Base/OpenInventor.h"

#include <Image/ByteImage.h>

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Extensions
                    {
                        class SoQtExtendedExaminerViewer : public SoQtExaminerViewer
                        {
                        public:

                            class ISelectionDispatcher
                            {
                            public:

                                ISelectionDispatcher(): m_IsEnabled(true) {}
                                virtual ~ISelectionDispatcher() {}
                                void SetEnabled(const bool Enabled)
                                {
                                    m_IsEnabled = Enabled;
                                }
                                bool IsEnabled()const
                                {
                                    return m_IsEnabled;
                                }
                                virtual void OnAddToSelection(const SoQtExtendedExaminerViewer* pViewer, SoNode* pNode, void* pUserdata) = 0;
                                virtual void OnRemoveFromSelection(const SoQtExtendedExaminerViewer* pViewer, SoNode* pNode, void* pUserdata) = 0;

                            protected:

                                bool m_IsEnabled;
                            };


                            class IRenderDispatcher
                            {
                            public:

                                enum Channel
                                {
                                    eIntensity = 0x1, eRGB = 0x3
                                };

                                IRenderDispatcher(): m_IsEnabled(false), m_pBuffer(NULL), m_pImage(NULL), m_ViewportSizePixels(0, 0), m_Channels(eRGB) {}
                                virtual ~IRenderDispatcher()
                                {
                                    if (m_pBuffer)
                                    {
                                        delete[] m_pBuffer;
                                    }
                                    if (m_pImage)
                                    {
                                        delete m_pImage;
                                    }
                                }
                                inline void SetEnabled(const bool Enabled)
                                {
                                    m_IsEnabled = Enabled;
                                }
                                inline bool IsEnabled()const
                                {
                                    return m_IsEnabled;
                                }
                                inline void SetChannel(const Channel Channels)
                                {
                                    if (Channels != m_Channels)
                                    {
                                        ResizeBuffer(m_ViewportSizePixels, Channels);
                                    }
                                }
                                inline Channel GetChannel()const
                                {
                                    return m_Channels;
                                }
                                inline const CByteImage* GetImage()
                                {
                                    return m_pImage;
                                }

                                inline unsigned char* BeginDispatching(const SbVec2s& ViewportSizePixels, const Channel Channels)
                                {
                                    if ((ViewportSizePixels != m_ViewportSizePixels) || (Channels != m_Channels))
                                    {
                                        ResizeBuffer(ViewportSizePixels, Channels);
                                    }
                                    return m_pBuffer;
                                }

                                inline void EndDispatching(const SoQtExtendedExaminerViewer* pViewer)
                                {
                                    FlipY();
                                    OnRender(pViewer, m_pImage);
                                }

                                virtual void OnRender(const SoQtExtendedExaminerViewer* pViewer, const CByteImage* pImage) = 0;

                            protected:

                                void ResizeBuffer(const SbVec2s& ViewportSizePixels, const Channel Channels)
                                {
                                    if (m_pBuffer)
                                    {
                                        delete[] m_pBuffer;
                                        m_pBuffer = NULL;
                                        m_ViewportSizePixels.setValue(0, 0);
                                    }
                                    if (m_pImage)
                                    {
                                        delete m_pImage;
                                        m_pImage = NULL;
                                    }
                                    if ((ViewportSizePixels[0] > 0) && (ViewportSizePixels[1] > 0))
                                    {
                                        m_Channels = Channels;
                                        m_ViewportSizePixels = ViewportSizePixels;
                                        m_pBuffer = new unsigned char[m_ViewportSizePixels[0] * m_ViewportSizePixels[1] * int(m_Channels & 0xF)];
                                        m_pImage = new CByteImage(m_ViewportSizePixels[0], m_ViewportSizePixels[1], (m_Channels == eRGB) ? CByteImage::eRGB24 : CByteImage::eGrayScale);
                                    }
                                }

                                void FlipY()
                                {
                                    const int TotalStrides = m_ViewportSizePixels[1];
                                    const int StrideSize = m_ViewportSizePixels[0] * int(m_Channels & 0xF);
                                    unsigned char* pDestination = m_pImage->pixels;
                                    const unsigned char* pSource = m_pBuffer + StrideSize * (TotalStrides - 1);
                                    for (int i = 0; i < TotalStrides; ++i, pDestination += StrideSize, pSource -= StrideSize)
                                    {
                                        memcpy(pDestination, pSource, StrideSize);
                                    }
                                }

                                bool m_IsEnabled;
                                unsigned char* m_pBuffer;
                                CByteImage* m_pImage;
                                SbVec2s m_ViewportSizePixels;
                                Channel m_Channels;
                            };

                            enum CompositionRenderingMode
                            {
                                eNonComposing, eImageComposing
                            };

                            static void DeselectionCallBack(void* pUserdata, SoPath* pPath);
                            static void SelectionCallBack(void* pUserdata, SoPath* pPath);

                            SoQtExtendedExaminerViewer(QWidget* pSoQtWidget = NULL, SoExtSelection* pRoot = NULL);
                            ~SoQtExtendedExaminerViewer() override;

                            void SetSourceImage(const Byte* pImageBuffer, const int BytesPerPixel, const int ImageWidth, const int ImageHeight);
                            void SetCompositionRenderingMode(const CompositionRenderingMode Mode);
                            void Update();

                            void ViewAll();
                            void AddNode(SoNode* pNode);
                            void ClearNodes();

                            void ClearSelection();

                            void DisplayingSelection(const bool DisplayEnabled);
                            bool IsDisplayingSelection() const;

                            void SetSelectionDispatcher(ISelectionDispatcher* pSelectionDispatcher);
                            ISelectionDispatcher* GetSelectionDispatcher();

                            void SetRenderDispatcher(IRenderDispatcher* pRenderDispatcher);
                            IRenderDispatcher* GetRenderDispatcher();

                            QWidget* GetWidget();
                            SoExtSelection* GetRoot();
                            const std::set<SoNode*>& GetSelectedNodes() const;

                            void ExecuteLoop();

                            void SetBackgroundColor(const Byte R, const Byte G, const Byte B);

                            void SetCameraPose(const SbVec3f& Position, const  SbRotation& Orientation);
                            void GetCameraPose(SbVec3f& Position, SbRotation& Orientation) const;

                            void SetTransparencyType(const SoGLRenderAction::TransparencyType CurrentType);

                        protected:

                            static void SoGLRenderPassCB(void* pUserdata);
                            static void SoGLPreRenderCB(void* pUserdata, class SoGLRenderAction* pAction);

                            void actualRedraw() override;

                            void RemoveFromSelection(SoNode* pNode);
                            void AddToSelection(SoNode* pNode);



                            CompositionRenderingMode m_CompositionRenderingMode;
                            QWidget* m_pSoQtWidget;
                            SoExtSelection* m_pRoot;
                            SoExtSelection* m_pNodesContainer;
                            SoLineHighlightRenderAction* m_pSoBoxHighlightRenderAction;
                            SoLightModel* m_pLightModel;
                            SoFont* m_pFont;
                            SoSeparator* m_SeparatorBackGround;
                            SoImage* m_pFrameWorkImage;
                            SoCamera* m_pCamera;
                            const Byte* m_pInputImageBuffer;
                            Byte* m_pOutputImageBuffer;
                            int m_BytesPerPixel;
                            int m_ImageWidth;
                            int m_ImageHeight;
                            std::set<SoNode*> m_SelectedNodes;
                            ISelectionDispatcher* m_pSelectionDispatcher;
                            IRenderDispatcher* m_pIRenderDispatcher;
                        };
                    }
                }
            }
        }
    }
}

