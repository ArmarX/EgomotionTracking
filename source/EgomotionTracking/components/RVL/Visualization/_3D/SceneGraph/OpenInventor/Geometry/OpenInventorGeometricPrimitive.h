/*
 * OpenInventorGeometricPrimitive.h
 */

#pragma once

#include "../../../../../Mathematics/_3D/Geometry/GeometricPrimitive3D.h"
#include "../../../../../Dependency/Dependency.h"
#include "../../../../../Dependency/DependencyManager.h"
#include "../Base/ContainedOpenInventorElement.h"
#include "../Base/OpenInventorElement.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        class COpenInventorGeometricPrimitive : public Base::CContainedOpenInventorElement, public Dependency::IDependency
                        {
                        public:

                            COpenInventorGeometricPrimitive(Mathematics::_3D::Geometry::CGeometricPrimitive3D* pGeometricPrimitive3D, const Mathematics::_3D::Geometry::CGeometricPrimitive3D::GeometricPrimitive3DTypeId GeometricPrimitive3DTypeId, const bool CreateDrawStyle, const bool CreateMaterial, const bool CreateComplexity);
                            ~COpenInventorGeometricPrimitive() override;

                            const Mathematics::_3D::Geometry::CGeometricPrimitive3D* GetGeometricPrimitive() const;

                            void OnDestructor(Dependency::IDependencyManager* pDependencyManager) override;
                            void OnChange(Dependency::IDependencyManager* pDependencyManager, const int EventFlags) override;

                            void SetVisible(const bool Visible) override;

                        protected:

                            void Update(const bool OnFullUpdate) override;

                            virtual void Create() = 0;
                            virtual void FullUpdate() = 0;

                            Mathematics::_3D::Geometry::CGeometricPrimitive3D* m_pGeometricPrimitive3D;
                            const Mathematics::_3D::Geometry::CGeometricPrimitive3D::GeometricPrimitive3DTypeId m_GeometricPrimitive3DTypeId;
                        };
                    }
                }
            }
        }
    }
}

