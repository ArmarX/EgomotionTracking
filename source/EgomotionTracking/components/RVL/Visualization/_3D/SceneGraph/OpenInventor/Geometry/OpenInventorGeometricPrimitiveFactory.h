/*
 * OpenInventorGeometricPrimitiveFactory.h
 */

#pragma once

#include "../../../../../Mathematics/_3D/Geometry/GeometricPrimitive3D.h"
#include "../Base/ContainerOpenInventorSet.h"
#include "OpenInventorGeometricPrimitive.h"
#include "OpenInventorCircle.h"
#include "OpenInventorLine.h"
#include "OpenInventorPlane.h"
#include "OpenInventorPoint.h"
#include "OpenInventorPointPair.h"
#include "OpenInventorSphere.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        class COpenInventorGeometricPrimitiveFactory
                        {
                        public:

                            static COpenInventorGeometricPrimitive* CreateVisualization(Mathematics::_3D::Geometry::CGeometricPrimitive3D* pGeometricPrimitive3D, const Base::CContainerOpenInventorSet* pContainerOpenInventorSet);
                            static COpenInventorPoint* CreateVisualization(Mathematics::_3D::Geometry::CPoint3D* pPoint3D, const Base::CContainerOpenInventorSet* pContainerOpenInventorSet);
                            static COpenInventorPointPair* CreateVisualization(Mathematics::_3D::Geometry::CPointPair3D* pPointPair3D, const Base::CContainerOpenInventorSet* pContainerOpenInventorSet);
                            static COpenInventorLine* CreateVisualization(Mathematics::_3D::Geometry::CLine3D* pLine3D, const Base::CContainerOpenInventorSet* pContainerOpenInventorSet);
                            static COpenInventorCircle* CreateVisualization(Mathematics::_3D::Geometry::CCircle3D* pCircle3D, const Base::CContainerOpenInventorSet* pContainerOpenInventorSet);
                            static COpenInventorPlane* CreateVisualization(Mathematics::_3D::Geometry::CPlane3D* pPlane3D, const Base::CContainerOpenInventorSet* pContainerOpenInventorSet);
                            static COpenInventorSphere* CreateVisualization(Mathematics::_3D::Geometry::CSphere3D* pSphere3D, const Base::CContainerOpenInventorSet* pContainerOpenInventorSet);

                        private:

                            COpenInventorGeometricPrimitiveFactory();
                            virtual ~COpenInventorGeometricPrimitiveFactory();
                        };
                    }
                }
            }
        }
    }
}

