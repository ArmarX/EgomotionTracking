/*
 * OpenInventorKinematicFrame.h
 */

#pragma once

#include "../Base/OpenInventorElement.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Frames
                    {
                        class COpenInventorKinematicFrame : public Base::COpenInventorElement
                        {
                        public:

                            enum SubElement
                            {
                                eAxisXP = 0, eAxisYP, eAxisZP, eAxisXN, eAxisYN, eAxisZN/*, eSphere*/
                            };

                            COpenInventorKinematicFrame(const int SubElemetsBitMask = 0x3F);
                            ~COpenInventorKinematicFrame() override;

                            int GetSubElemetsBitMask() const;

                            void SetVisible(const bool Visible) override;
                            void SetVisible(const std::map<SubElement, bool>& Visibility);

                            void SetColors(const std::map<SubElement, SbColor>& Colors);
                            std::map<SubElement, SbColor> GetColors() const;

                            void SetTransparencies(const std::map<SubElement, float>& Transparencies);

                            void SetAxesLength(const float Lenght);
                            float GetAxesLength() const;

                            void SetConeLength(const float Lenght);
                            float GetConeLength() const;

                            void SetConeWidth(const float Width);
                            float GetConeWidth() const;

                            void Update(const bool FullUpdate) override;

                            void SetLabels(const std::map<SubElement, std::string>& Labels);

                            void SetDrawStyle(const SoDrawStyle::Style Style) override;

                        protected:

                            int m_SubElemetsBitMask;
                            float m_AxisLenght;
                            float m_ConeLenght;
                            float m_ConeWidth;
                            SoCoordinate3* m_pAxesCoordinate3;
                            SoLineSet* m_pAxesLineSet;
                            struct Axis
                            {
                                SoSwitch* m_pAxisSwitch;
                                SoTransform* m_pConeTransform;
                                SoMaterial* m_pConeMaterial;
                                SoCone* m_pCone;
                                SoTranslation* m_pLabelTranslation;
                                SoBaseColor* m_pLabelColor;
                                SoText2* m_pLabelText;
                            };
                            Axis m_Axes[6];
                        };
                    }
                }
            }
        }
    }
}

