/*
 * OpenInventorKinematicFrame.cpp
 */

#include "OpenInventorKinematicFrame.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Frames
                    {
                        COpenInventorKinematicFrame::COpenInventorKinematicFrame(const int SubElemetsBitMask) :
                            COpenInventorElement(COpenInventorElement::eKinematicFrame),
                            m_SubElemetsBitMask(SubElemetsBitMask),
                            m_AxisLenght(_VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_AXIS_LENGTH_),
                            m_ConeLenght(_VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_CONE_LENGTH_),
                            m_ConeWidth(_VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_CONE_WIDTH_),
                            m_pAxesCoordinate3(nullptr),
                            m_pAxesLineSet(nullptr)
                        {
                            memset(m_Axes, 0, sizeof(Axis) * 6);

                            SoMaterialBinding* pAxesMaterialBinding = new SoMaterialBinding;
                            AddElement(pAxesMaterialBinding);
                            pAxesMaterialBinding->value = SoMaterialBinding::PER_VERTEX;

                            m_pAxesCoordinate3 = new SoCoordinate3;
                            AddElement(m_pAxesCoordinate3);

                            m_pAxesLineSet = new SoLineSet;
                            AddElement(m_pAxesLineSet);
                            m_pAxesLineSet->setUserData(this);

                            SoMaterialBinding* pConesMaterialBinding = new SoMaterialBinding;
                            AddElement(pConesMaterialBinding);

                            for (int i = 0 ; i < 6 ; ++i)
                            {
                                m_Axes[i].m_pAxisSwitch = new SoSwitch;
                                AddElement(m_Axes[i].m_pAxisSwitch);
                                SoSeparator* pSoSeparator = new SoSeparator;
                                m_Axes[i].m_pAxisSwitch->addChild(pSoSeparator);
                                m_Axes[i].m_pConeMaterial = new SoMaterial;
                                pSoSeparator->addChild(m_Axes[i].m_pConeMaterial);
                                m_Axes[i].m_pConeMaterial->transparency = _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_TRANSPARENCY_;
                                m_Axes[i].m_pConeTransform = new SoTransform;
                                pSoSeparator->addChild(m_Axes[i].m_pConeTransform);
                                m_Axes[i].m_pCone = new SoCone;
                                pSoSeparator->addChild(m_Axes[i].m_pCone);
                                m_Axes[i].m_pCone->parts = SoCone::ALL;
                                m_Axes[i].m_pCone->setUserData(this);
                                m_Axes[i].m_pLabelTranslation = new SoTranslation;
                                pSoSeparator->addChild(m_Axes[i].m_pLabelTranslation);
                                m_Axes[i].m_pLabelColor = new SoBaseColor;
                                pSoSeparator->addChild(m_Axes[i].m_pLabelColor);
                                m_Axes[i].m_pLabelText = new SoText2;
                                pSoSeparator->addChild(m_Axes[i].m_pLabelText);
                                m_Axes[i].m_pLabelText->setUserData(this);
                                m_Axes[i].m_pAxisSwitch->whichChild = (m_SubElemetsBitMask & (0X1 << i)) ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                            }

                            std::map<SubElement, float> Transparencies;
                            Transparencies.insert(std::pair<SubElement, float>(eAxisXP, _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_TRANSPARENCY_));
                            Transparencies.insert(std::pair<SubElement, float>(eAxisYP, _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_TRANSPARENCY_));
                            Transparencies.insert(std::pair<SubElement, float>(eAxisZP, _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_TRANSPARENCY_));
                            Transparencies.insert(std::pair<SubElement, float>(eAxisXN, 0.75f));
                            Transparencies.insert(std::pair<SubElement, float>(eAxisYN, 0.75f));
                            Transparencies.insert(std::pair<SubElement, float>(eAxisZN, 0.75f));
                            SetTransparencies(Transparencies);

                            std::map<SubElement, std::string> Labels;
                            Labels.insert(std::pair<SubElement, std::string>(eAxisXP, std::string(_VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_XP_)));
                            Labels.insert(std::pair<SubElement, std::string>(eAxisYP, std::string(_VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_YP_)));
                            Labels.insert(std::pair<SubElement, std::string>(eAxisZP, std::string(_VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_ZP_)));
                            Labels.insert(std::pair<SubElement, std::string>(eAxisXN, std::string(_VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_XN_)));
                            Labels.insert(std::pair<SubElement, std::string>(eAxisYN, std::string(_VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_YN_)));
                            Labels.insert(std::pair<SubElement, std::string>(eAxisZN, std::string(_VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_ZN_)));
                            SetLabels(Labels);

                            std::map<SubElement, SbColor> Colors;
                            Colors.insert(std::pair<SubElement, SbColor>(eAxisXP, COpenInventorElement::GetPredefinedColor(COpenInventorElement::eRed)));
                            Colors.insert(std::pair<SubElement, SbColor>(eAxisYP, COpenInventorElement::GetPredefinedColor(COpenInventorElement::eGreen)));
                            Colors.insert(std::pair<SubElement, SbColor>(eAxisZP, COpenInventorElement::GetPredefinedColor(COpenInventorElement::eBlue)));
                            Colors.insert(std::pair<SubElement, SbColor>(eAxisXN, COpenInventorElement::GetPredefinedColor(COpenInventorElement::eRed1_4)));
                            Colors.insert(std::pair<SubElement, SbColor>(eAxisYN, COpenInventorElement::GetPredefinedColor(COpenInventorElement::eGreen1_4)));
                            Colors.insert(std::pair<SubElement, SbColor>(eAxisZN, COpenInventorElement::GetPredefinedColor(COpenInventorElement::eBlue1_4)));
                            SetColors(Colors);

                            Update(true);
                        }

                        COpenInventorKinematicFrame::~COpenInventorKinematicFrame()
                            = default;

                        int COpenInventorKinematicFrame::GetSubElemetsBitMask() const
                        {
                            return m_SubElemetsBitMask;
                        }

                        void COpenInventorKinematicFrame::SetVisible(const bool Visible)
                        {
                            SetVisible(Visible);
                            Update(false);
                        }

                        void COpenInventorKinematicFrame::SetVisible(const std::map<SubElement, bool>& Visibility)
                        {
                            for (auto pSubElementVisibility : Visibility)
                                if ((pSubElementVisibility.first >= eAxisXP) && (pSubElementVisibility.first <= eAxisZN))
                                {
                                    m_Axes[pSubElementVisibility.first].m_pAxisSwitch->whichChild = pSubElementVisibility.second ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                                }
                        }

                        void COpenInventorKinematicFrame::SetColors(const std::map<SubElement, SbColor>& Colors)
                        {
                            SoMaterial* pBaseMaterial = GetBaseMaterial();
                            for (auto Color : Colors)
                                if ((Color.first >= eAxisXP) && (Color.first <= eAxisZN))
                                {
                                    pBaseMaterial->diffuseColor.set1Value(Color.first * 2, Color.second);
                                    pBaseMaterial->diffuseColor.set1Value(Color.first * 2 + 1, Color.second);
                                    m_Axes[Color.first].m_pConeMaterial->diffuseColor = Color.second;
                                    m_Axes[Color.first].m_pLabelColor->rgb = Color.second;
                                }
                        }

                        std::map<COpenInventorKinematicFrame::SubElement, SbColor> COpenInventorKinematicFrame::GetColors() const
                        {
                            const SoMaterial* pBaseMaterial = GetBaseMaterial();
                            std::map<SubElement, SbColor> Colors;
                            const SbColor* pColors = pBaseMaterial->diffuseColor.getValues(0);
                            for (int i = 0 ; i < 6 ; ++i)
                            {
                                Colors.insert(std::pair<SubElement, SbColor>(SubElement(i), pColors[i * 2]));
                            }
                            return Colors;
                        }

                        void COpenInventorKinematicFrame::SetTransparencies(const std::map<SubElement, float>& Transparencies)
                        {
                            SoMaterial* pBaseMaterial = GetBaseMaterial();
                            for (auto Transparencie : Transparencies)
                                if ((Transparencie.first >= eAxisXP) && (Transparencie.first <= eAxisZN))
                                {
                                    pBaseMaterial->transparency.set1Value(Transparencie.first * 2, Transparencie.second);
                                    pBaseMaterial->transparency.set1Value(Transparencie.first * 2 + 1, Transparencie.second);
                                    m_Axes[Transparencie.first].m_pConeMaterial->transparency = Transparencie.second;
                                }
                        }

                        void COpenInventorKinematicFrame::SetAxesLength(const float Lenght)
                        {
                            if (m_AxisLenght != Lenght)
                            {
                                m_AxisLenght = Lenght;
                                Update(true);
                            }
                        }

                        float COpenInventorKinematicFrame::GetAxesLength() const
                        {
                            return m_AxisLenght;
                        }

                        void COpenInventorKinematicFrame::SetConeLength(const float Lenght)
                        {
                            if (m_ConeLenght != Lenght)
                            {
                                m_ConeLenght = Lenght;
                                Update(true);
                            }
                        }

                        float COpenInventorKinematicFrame::GetConeLength() const
                        {
                            return m_ConeLenght;
                        }

                        void COpenInventorKinematicFrame::SetConeWidth(const float Width)
                        {
                            if (m_ConeWidth != Width)
                            {
                                m_ConeWidth = Width;
                                Update(true);
                            }
                        }

                        float COpenInventorKinematicFrame::GetConeWidth() const
                        {
                            return m_ConeWidth;
                        }

                        void COpenInventorKinematicFrame::Update(const bool FullUpdate)
                        {
                            if (FullUpdate)
                            {
                                const float LabelOffset = m_ConeLenght * 0.5f;
                                const float ConeTranslation = m_AxisLenght - LabelOffset;
                                int VertexIndex = 0;
                                int LineSegmentIndex = 0;
                                const SbVec3f Origin(0.0f, 0.0f, 0.0f);
                                const SbVec3f AxisY(0.0f, 1.0f, 0.0f);
                                const SbVec3f LabelTranslation(0.0f, LabelOffset, 0.0f);
                                SbVec3f Axis(0.0f, 0.0f, 0.0f);
                                SbVec3f Translation(0.0f, 0.0f, 0.0f);
                                m_pAxesLineSet->numVertices.setNum(0);
                                for (int i = 0 ; i < 6 ; ++i)
                                    if (m_SubElemetsBitMask & (0x1 << i))
                                    {
                                        Translation = Axis = Origin;
                                        Axis[i % 3] = (i < 3) ? m_AxisLenght : -m_AxisLenght;
                                        Translation[i % 3] = (i < 3) ? ConeTranslation : -ConeTranslation;
                                        m_pAxesCoordinate3->point.set1Value(VertexIndex++, Origin);
                                        m_pAxesCoordinate3->point.set1Value(VertexIndex++, Axis);
                                        m_pAxesLineSet->numVertices.set1Value(LineSegmentIndex++, 2);
                                        m_Axes[i].m_pConeTransform->translation = Translation;
                                        Axis.normalize();
                                        m_Axes[i].m_pConeTransform->rotation = SbRotation(AxisY, Axis);
                                        m_Axes[i].m_pCone->height = m_ConeLenght;
                                        m_Axes[i].m_pCone->bottomRadius = m_ConeWidth;
                                        m_Axes[i].m_pLabelTranslation->translation = LabelTranslation;
                                    }
                            }
                            SwitchVisibility(GetVisible());
                        }

                        void COpenInventorKinematicFrame::SetLabels(const std::map<SubElement, std::string>& Labels)
                        {
                            for (const auto& Label : Labels)
                                if ((Label.first >= eAxisXP) && (Label.first <= eAxisZN))
                                {
                                    m_Axes[Label.first].m_pLabelText->string = Label.second.c_str();
                                }
                        }

                        void COpenInventorKinematicFrame::SetDrawStyle(const SoDrawStyle::Style Style)
                        {
                            const int Flag = ((Style == SoDrawStyle::LINES) || (Style == SoDrawStyle::POINTS)) ? SoCone::SIDES : SoCone::ALL;
                            for (auto& m_Axe : m_Axes)
                            {
                                m_Axe.m_pCone->parts = Flag;
                            }
                            COpenInventorElement::SetDrawStyle(Style);
                        }
                    }
                }
            }
        }
    }
}
