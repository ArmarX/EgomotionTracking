/*
 * SoQtExtendedExaminerViewer.cpp
 */

#include "SoQtExtendedExaminerViewer.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Extensions
                    {
                        void SoQtExtendedExaminerViewer::DeselectionCallBack(void* pUserdata, SoPath* pPath)
                        {
                            if (pUserdata)
                            {
                                SoQtExtendedExaminerViewer* pViewer = (SoQtExtendedExaminerViewer*) pUserdata;
                                pViewer->RemoveFromSelection(pPath->getTail());
                            }
                        }

                        void SoQtExtendedExaminerViewer::SelectionCallBack(void* pUserdata, SoPath* pPath)
                        {
                            if (pUserdata)
                            {
                                SoQtExtendedExaminerViewer* pViewer = (SoQtExtendedExaminerViewer*) pUserdata;
                                pViewer->AddToSelection(pPath->getTail());
                            }
                        }

                        SoQtExtendedExaminerViewer::SoQtExtendedExaminerViewer(QWidget* pSoQtWidget, SoExtSelection* pRoot) :
                            SoQtExaminerViewer((pSoQtWidget ? pSoQtWidget : (pSoQtWidget = SoQt::init("SoQtExtendedExaminerViewer")))),
                            m_CompositionRenderingMode(eNonComposing),
                            m_pSoQtWidget(pSoQtWidget),
                            m_pRoot(nullptr),
                            m_pNodesContainer(nullptr),
                            m_pSoBoxHighlightRenderAction(nullptr),
                            m_pLightModel(nullptr),
                            m_pFont(nullptr),
                            m_SeparatorBackGround(nullptr),
                            m_pFrameWorkImage(nullptr),
                            m_pCamera(nullptr),
                            m_pInputImageBuffer(nullptr),
                            m_pOutputImageBuffer(nullptr),
                            m_BytesPerPixel(0),
                            m_ImageWidth(0),
                            m_ImageHeight(0),
                            m_pSelectionDispatcher(nullptr),
                            m_pIRenderDispatcher(nullptr)
                        {
                            setClearBeforeRender(false, true);

                            if (pRoot)
                            {
                                m_pRoot = pRoot;
                                SoSearchAction Searcher;
                                Searcher.setType(SoFont::getClassTypeId());
                                Searcher.setInterest(SoSearchAction::FIRST);
                                Searcher.setSearchingAll(true);
                                Searcher.apply(m_pRoot);
                                SoPath* pPath = Searcher.getPath();
                                if (pPath)
                                {
                                    m_pFont = (SoFont*) pPath->getTail();
                                }
                            }
                            else
                            {
                                m_pRoot = new SoExtSelection;
                            }

                            setSceneGraph(m_pRoot);


                            m_pNodesContainer = new SoExtSelection;
                            m_pRoot->addChild(m_pNodesContainer);

                            if (!m_pFont)
                            {
                                m_pFont = new SoFont;
                                m_pRoot->addChild(m_pFont);
                            }
                            m_pFont->name.set("Ubuntu");
                            m_pFont->size.setValue(16);


                            m_pSoBoxHighlightRenderAction = new SoLineHighlightRenderAction;
                            m_pSoBoxHighlightRenderAction->setColor(SbColor(1.0f, 1.0f, 0.0f));
                            m_pSoBoxHighlightRenderAction->setLineWidth(3.0f);
                            m_pSoBoxHighlightRenderAction->setLinePattern(0xFFFF);
                            m_pSoBoxHighlightRenderAction->addPreRenderCallback(SoGLPreRenderCB, this);

                            //SWITCHING
                            m_pSoBoxHighlightRenderAction->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_SORTED_TRIANGLE_BLEND);

                            m_pLightModel = new SoLightModel;
                            m_pRoot->addChild(m_pLightModel);

                            setSeekTime(1);
                            setGLRenderAction(m_pSoBoxHighlightRenderAction);
                            setAntialiasing(true, 3);
                            setDecoration(false);
                            setBackgroundColor(SbColor(0.25f, 0.25f, 0.25f));

                            m_pRoot->policy = SoSelection::SHIFT;
                            m_pRoot->addSelectionCallback(SoQtExtendedExaminerViewer::SelectionCallBack, this);
                            m_pRoot->addDeselectionCallback(SoQtExtendedExaminerViewer::DeselectionCallBack, this);

                            m_pLightModel->model = SoLightModel::PHONG;

                            m_SeparatorBackGround = new SoSeparator;
                            m_SeparatorBackGround->ref();
                            SoOrthographicCamera* pOrthographicCamera = new SoOrthographicCamera;
                            m_SeparatorBackGround->addChild(pOrthographicCamera);
                            pOrthographicCamera->position = SbVec3f(0.0f, 0.f, 1.0f);
                            pOrthographicCamera->height = 1.0f;
                            pOrthographicCamera->nearDistance = 0.5f;
                            pOrthographicCamera->farDistance = 1.5f;

                            m_pFrameWorkImage = new SoImage;
                            m_SeparatorBackGround->addChild(m_pFrameWorkImage);
                            m_pFrameWorkImage->vertAlignment = SoImage::HALF;
                            m_pFrameWorkImage->horAlignment = SoImage::CENTER;

                            m_pSoQtWidget->setGeometry(0, 0, 800, 600);
                            m_pSoQtWidget->showMaximized();
                            SoQt::show(m_pSoQtWidget);
                        }

                        SoQtExtendedExaminerViewer::~SoQtExtendedExaminerViewer()
                        {
                            if (m_pOutputImageBuffer)
                            {
                                delete[] m_pOutputImageBuffer;
                            }
                            m_SeparatorBackGround->unref();
                        }

                        void SoQtExtendedExaminerViewer::SetBackgroundColor(const Byte R, const Byte G, const Byte B)
                        {
                            setBackgroundColor(SbColor(float(R) / 255.0f, float(G) / 255.0f, float(B) / 255.0f));
                        }

                        void SoQtExtendedExaminerViewer::SetCameraPose(const SbVec3f& Position, const  SbRotation& Orientation)
                        {
                            SoCamera* pCamera = getCamera();
                            pCamera->position.setValue(Position);
                            pCamera->orientation.setValue(Orientation);
                        }

                        void SoQtExtendedExaminerViewer::GetCameraPose(SbVec3f& Position, SbRotation& Orientation) const
                        {
                            SoCamera* pCamera = getCamera();
                            Position = pCamera->position.getValue();
                            Orientation = pCamera->orientation.getValue();
                        }

                        void SoQtExtendedExaminerViewer::SetTransparencyType(const SoGLRenderAction::TransparencyType CurrentType)
                        {
                            m_pSoBoxHighlightRenderAction->setTransparencyType(CurrentType);
                        }

                        void SoQtExtendedExaminerViewer::SetSourceImage(const Byte* pImageBuffer, const int BytesPerPixel, const int ImageWidth, const int ImageHeight)
                        {
                            if (pImageBuffer && (BytesPerPixel >= 1) && (BytesPerPixel <= 4) && (ImageWidth > 0) && (ImageHeight > 0))
                            {
                                if (m_pOutputImageBuffer && ((m_BytesPerPixel != BytesPerPixel) || (m_ImageWidth != ImageWidth) || (m_ImageHeight != ImageHeight)))
                                {
                                    delete[] m_pOutputImageBuffer;
                                    m_pOutputImageBuffer = nullptr;
                                }
                                m_pInputImageBuffer = pImageBuffer;
                                m_BytesPerPixel = BytesPerPixel;
                                m_ImageWidth = ImageWidth;
                                m_ImageHeight = ImageHeight;
                                if (!m_pOutputImageBuffer)
                                {
                                    m_pOutputImageBuffer = new Byte[m_ImageWidth * m_ImageHeight * m_BytesPerPixel];
                                }
                                m_pFrameWorkImage->image.setValue(SbVec2s(m_ImageWidth, m_ImageHeight), m_BytesPerPixel, m_pOutputImageBuffer, SoSFImage::NO_COPY);

                                SoGLRenderAction* pSoGLRenderAction = getGLRenderAction();
                                //pSoGLRenderAction->addPreRenderCallback(SoGLPreRenderCB, this);
                                pSoGLRenderAction->setPassCallback(SoGLRenderPassCB, this);
                                Update();
                            }
                        }

                        void SoQtExtendedExaminerViewer::SetCompositionRenderingMode(const CompositionRenderingMode Mode)
                        {
                            m_CompositionRenderingMode = Mode;
                        }

                        void SoQtExtendedExaminerViewer::Update()
                        {
                            if (m_pInputImageBuffer && m_pOutputImageBuffer)
                            {
                                const int BytesPerLine = m_ImageWidth * m_BytesPerPixel;
                                const Byte* InputImageBuffer = m_pInputImageBuffer + (m_ImageWidth * (m_ImageHeight - 1) * m_BytesPerPixel);
                                Byte* pOutputImageBuffer = m_pOutputImageBuffer;
                                for (int y = 0 ; y < m_ImageHeight ; ++y, pOutputImageBuffer += BytesPerLine, InputImageBuffer -= BytesPerLine)
                                {
                                    memcpy(pOutputImageBuffer, InputImageBuffer, BytesPerLine);
                                }
                            }
                        }

                        void SoQtExtendedExaminerViewer::ViewAll()
                        {
                            SoQtExaminerViewer::viewAll();
                        }

                        void SoQtExtendedExaminerViewer::AddNode(SoNode* pNode)
                        {
                            if (pNode)
                            {
                                m_pNodesContainer->addChild(pNode);
                            }
                        }

                        void SoQtExtendedExaminerViewer::ClearNodes()
                        {
                            m_SelectedNodes.clear();
                            m_pNodesContainer->removeAllChildren();
                            m_pRoot->deselectAll();
                        }

                        void SoQtExtendedExaminerViewer::ClearSelection()
                        {
                            m_pRoot->deselectAll();
                        }

                        void SoQtExtendedExaminerViewer::DisplayingSelection(const bool DisplayEnabled)
                        {
                            m_pSoBoxHighlightRenderAction->setVisible(DisplayEnabled);
                            render();
                        }

                        bool SoQtExtendedExaminerViewer::IsDisplayingSelection() const
                        {
                            return m_pSoBoxHighlightRenderAction->isVisible();
                        }

                        void SoQtExtendedExaminerViewer::SetSelectionDispatcher(ISelectionDispatcher* pSelectionDispatcher)
                        {
                            m_pSelectionDispatcher = pSelectionDispatcher;
                        }

                        SoQtExtendedExaminerViewer::ISelectionDispatcher* SoQtExtendedExaminerViewer::GetSelectionDispatcher()
                        {
                            return m_pSelectionDispatcher;
                        }

                        void SoQtExtendedExaminerViewer::SetRenderDispatcher(IRenderDispatcher* pRenderDispatcher)
                        {
                            m_pIRenderDispatcher = pRenderDispatcher;
                        }

                        SoQtExtendedExaminerViewer::IRenderDispatcher* SoQtExtendedExaminerViewer::GetRenderDispatcher()
                        {
                            return m_pIRenderDispatcher;
                        }

                        QWidget* SoQtExtendedExaminerViewer::GetWidget()
                        {
                            return m_pSoQtWidget;
                        }

                        SoExtSelection* SoQtExtendedExaminerViewer::GetRoot()
                        {
                            return m_pRoot;
                        }

                        const std::set<SoNode*>& SoQtExtendedExaminerViewer::GetSelectedNodes() const
                        {
                            return m_SelectedNodes;
                        }

                        void SoQtExtendedExaminerViewer::ExecuteLoop()
                        {
                            SoQt::mainLoop();
                        }

                        void SoQtExtendedExaminerViewer::SoGLRenderPassCB(void* pUserdata)
                        {
                            //std::cout << "Pass" << std::endl;
                        }

                        void SoQtExtendedExaminerViewer::SoGLPreRenderCB(void* pUserdata, class SoGLRenderAction* pAction)
                        {
                            //std::cout << "Pre" << std::endl;
                        }

                        void SoQtExtendedExaminerViewer::actualRedraw()
                        {
                            const SbViewportRegion& ViewportRegion = getViewportRegion();
                            const SbVec2s& ViewportOriginPixels = ViewportRegion.getViewportOriginPixels();
                            const SbVec2s& ViewportSizePixels = ViewportRegion.getViewportSizePixels();
                            glViewport(ViewportOriginPixels[0], ViewportOriginPixels[1], ViewportSizePixels[0], ViewportSizePixels[1]);
                            const SbColor BackgroundColor = getBackgroundColor();
                            glClearColor(BackgroundColor[0], BackgroundColor[1], BackgroundColor[2], 1.0f);
                            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
                            if ((m_CompositionRenderingMode == eImageComposing) && m_pOutputImageBuffer)
                            {
                                getGLRenderAction()->apply(m_SeparatorBackGround);
                            }
                            SoQtExaminerViewer::actualRedraw();

                            //glReadBuffer(GL_BACK);

                            if (m_pIRenderDispatcher && m_pIRenderDispatcher->IsEnabled())
                            {
                                const IRenderDispatcher::Channel TargetChannel = m_pIRenderDispatcher->GetChannel();
                                unsigned char* pPixelsBuffer = m_pIRenderDispatcher->BeginDispatching(ViewportSizePixels, TargetChannel);
                                glReadPixels(0, 0, ViewportSizePixels[0], ViewportSizePixels[1], GL_RGB, GL_UNSIGNED_BYTE, pPixelsBuffer);
                                m_pIRenderDispatcher->EndDispatching(this);
                            }
                        }

                        void SoQtExtendedExaminerViewer::RemoveFromSelection(SoNode* pNode)
                        {
                            if (pNode && (m_SelectedNodes.find(pNode) != m_SelectedNodes.end()))
                            {
                                m_SelectedNodes.erase(pNode);
                                void* pUserData = pNode->getUserData();
                                if (m_pSelectionDispatcher && m_pSelectionDispatcher->IsEnabled())
                                {
                                    m_pSelectionDispatcher->OnRemoveFromSelection(this, pNode, pUserData);
                                }
                                if (pUserData)
                                {
                                    //CIdentifiable* pIdentifiable = (CIdentifiable*) pUserData;
                                    //pIdentifiable->Unselected();
                                }
                            }
                            render();
                        }

                        void SoQtExtendedExaminerViewer::AddToSelection(SoNode* pNode)
                        {
                            if (!m_pSoBoxHighlightRenderAction->isVisible())
                            {
                                m_pRoot->deselect(pNode);
                                return;
                            }
                            if (pNode && (m_SelectedNodes.find(pNode) == m_SelectedNodes.end()))
                            {
                                pNode->touch();
                                m_SelectedNodes.insert(pNode);
                                void* pUserData = pNode->getUserData();
                                if (m_pSelectionDispatcher)
                                {
                                    m_pSelectionDispatcher->OnAddToSelection(this, pNode, pUserData);
                                }
                                if (pUserData)
                                {
                                    //CIdentifiable* pIdentifiable = (CIdentifiable*) pUserData;
                                    //pIdentifiable->Selected();
                                }
                            }
                            render();
                        }
                    }
                }
            }
        }
    }
}
