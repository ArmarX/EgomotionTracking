/*
 * OpenInventorPointPair.h
 */

#pragma once

#include "../../../../../Mathematics/_3D/Geometry/PointPair3D.h"
#include "OpenInventorGeometricPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        class COpenInventorPointPair : public COpenInventorGeometricPrimitive
                        {
                        public:

                            enum ShapeMode
                            {
                                eSphere, eCube
                            };

                            COpenInventorPointPair(Mathematics::_3D::Geometry::CPointPair3D* pPointPair, const ShapeMode ModeA = eSphere, const ShapeMode ModeB = eSphere);
                            ~COpenInventorPointPair() override;

                            void SetShapeModeA(const ShapeMode ModeA);
                            ShapeMode GetShapeModeA() const;

                            void SetShapeModeB(const ShapeMode ModeB);
                            ShapeMode GetShapeModeB() const;

                            void SetDisplayRadiusA(const float DisplayRadiusA);
                            float GetDisplayRadiusA() const;

                            void SetDisplayRadiusB(const float DisplayRadiusB);
                            float GetDisplayRadiusB() const;

                            void SetConnectingLineVisible(const bool Visible);
                            bool GetConnectingLineVisible() const;

                            void SetConnectingLineWidth(const float LineWidth);
                            float GetConnectingLineWidth() const;

                            void SetConnectingLinePattern(const ushort Pattern);
                            ushort GetConnectingLinePattern() const;

                        protected:

                            void Create() override;
                            void FullUpdate() override;
                            bool IsComposedVisible() override;

                        private:

                            union Shape
                            {
                                SoSphere* m_pSoSphere;
                                SoCube* m_pCube;
                            };

                            ShapeMode m_ShapeModeA;
                            ShapeMode m_ShapeModeB;
                            bool m_ConnectingLineVisible;
                            SoSwitch* m_pConnectingLineSwitch;
                            SoDrawStyle* m_pConnectingLineDrawStyle;
                            SoCoordinate3* m_pConnectingLineCoordinate3;
                            SoLineSet* m_pConnectingLineSet;
                            SoTranslation* m_pSoTranslationA;
                            SoTranslation* m_pSoTranslationB;
                            SoSwitch* m_pSphereASwitch;
                            SoSwitch* m_pSphereBSwitch;
                            Shape m_ShapeA;
                            Shape m_ShapeB;
                        };
                    }
                }
            }
        }
    }
}

