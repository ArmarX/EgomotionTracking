/*
 * ContainerOpenInventorSet.h
 */

#pragma once

#include "OpenInventor.h"
#include "OpenInventorElement.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Base
                    {
                        class CContainedOpenInventorElement;

                        class CContainerOpenInventorSet : public COpenInventorElement
                        {
                        public:

                            CContainerOpenInventorSet(const SbBox3f& BoundingBox);
                            ~CContainerOpenInventorSet() override;

                            void SetVisible(const bool Visible) override;

                            void SetBoundingBox(const SbBox3f& BoundingBox);
                            const SbBox3f& GetBoundingBox() const;

                            bool IsEmpty() const;

                            COpenInventorElement::InclusionType MapPoint(const SbVec3f& Point) const;
                            COpenInventorElement::InclusionType MapPointPair(const SbVec3f& PointA, const SbVec3f& PointB, bool& FlagA, bool& FlagB) const;
                            COpenInventorElement::InclusionType MapLine(const SbLine& Line, SbVec3f& PointA, SbVec3f& PointB) const;
                            COpenInventorElement::InclusionType MapCircle(const SbVec3f& Normal, const SbVec3f& Center, const float Radius) const;
                            COpenInventorElement::InclusionType MapPlane(const SbPlane& Plane, std::vector<SbVec3f>& Points) const;
                            COpenInventorElement::InclusionType MapSphere(const SbVec3f& Center, const float Radius) const;
                            COpenInventorElement::InclusionType MapBoundingBox(const SbBox3f& BoundingBox) const;

                            bool RegisterGeometricPrimitive(CContainedOpenInventorElement* pGeometricPrimitive);
                            bool UnRegisterGeometricPrimitive(CContainedOpenInventorElement* pGeometricPrimitive);

                        protected:

                            struct AxisIntersectionPoint
                            {
                                SbVec3f m_Location;
                                float m_Alpha;
                            };

                            void UpdateRegisteredGeometricPrimitives(const bool FullUpdate);
                            void Update(const bool FullUpdate) override;
                            bool IsInside(const SbVec3f& Point) const;
                            static bool SortAxisIntersectionPointByAlpha(const AxisIntersectionPoint& lhs, const AxisIntersectionPoint& rhs);

                            const SbVec3f m_ToleranceDelta;
                            SbBox3f m_BoundingBox;
                            SbPlane m_Planes[_VISUALIZATION_VOLUME_TOTAL_BOUNDING_PLANES_];
                            SbLine m_Lines[_VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_];
                            SbVec3f m_Vertices[_VISUALIZATION_VOLUME_TOTAL_BOUNDING_VERTICES_];
                            SbVec3f m_LineDirectionAB[_VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_];
                            SbVec3f m_LineBasePoint[_VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_];
                            float m_LineLengths[_VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_];
                            SoCoordinate3* m_pSoCoordinate3;
                            SoIndexedLineSet* m_pSoIndexedLineSet;
                            std::set<CContainedOpenInventorElement*> m_Set;
                        };
                    }
                }
            }
        }
    }
}

