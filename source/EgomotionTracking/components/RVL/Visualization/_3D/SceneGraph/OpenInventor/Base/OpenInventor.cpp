/*
 * OpenInventor.cpp
 */

#include "OpenInventor.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    SbVec3f T(const Mathematics::_3D::CVector3D& Vector)
                    {
                        return SbVec3f(float(Vector.GetX()), float(Vector.GetY()), float(Vector.GetZ()));
                    }

                    float T(const Real X)
                    {
                        return float(X);
                    }
                }
            }
        }
    }
}

