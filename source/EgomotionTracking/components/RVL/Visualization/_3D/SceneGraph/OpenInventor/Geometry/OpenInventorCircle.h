/*
 * COpenInventorCircle.h
 */

#pragma once

#include "../../../../../Mathematics/_3D/Geometry/Circle3D.h"
#include "OpenInventorGeometricPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        class COpenInventorCircle : public COpenInventorGeometricPrimitive
                        {
                        public:

                            COpenInventorCircle(Mathematics::_3D::Geometry::CCircle3D* pCircle3D);
                            ~COpenInventorCircle() override;

                        protected:

                            void Create() override;
                            void FullUpdate() override;
                            bool IsComposedVisible() override;

                        private:

                            SoCoordinate3* m_pSoCoordinate3;
                            SoLineSet* m_pSoLineSet;
                        };
                    }
                }
            }
        }
    }
}

