/*
 * OpenInventorSphere.cpp
 */

#include "OpenInventorSphere.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        COpenInventorSphere::COpenInventorSphere(Mathematics::_3D::Geometry::CSphere3D* pSphere) :
                            COpenInventorGeometricPrimitive(pSphere, Mathematics::_3D::Geometry::CGeometricPrimitive3D::eSphere, true, true, true),
                            m_pSoTranslation(nullptr),
                            m_pSoSphere(nullptr)
                        {
                            Create();
                            Update(true);
                        }

                        COpenInventorSphere::~COpenInventorSphere()
                            = default;

                        void COpenInventorSphere::Create()
                        {
                            m_pSoTranslation = new SoTranslation;
                            AddElement(m_pSoTranslation);

                            m_pSoSphere = new SoSphere;
                            AddElement(m_pSoSphere);
                            m_pSoSphere->setUserData(this);

                            SetTransparency(_VISUALIZATION_SPHERE_DEFAULT_TRANSPARENCY_);
                        }

                        void COpenInventorSphere::FullUpdate()
                        {
                            Mathematics::_3D::Geometry::CSphere3D* pSphere = dynamic_cast<Mathematics::_3D::Geometry::CSphere3D*>(m_pGeometricPrimitive3D);
                            m_pSoSphere->radius = T(pSphere->GetRadius());
                            m_pSoTranslation->translation.setValue(T(pSphere->GetCenter()));
                        }

                        bool COpenInventorSphere::IsComposedVisible()
                        {
                            if (GetVisible() && m_pGeometricPrimitive3D)
                            {
                                if (m_pContainerSet)
                                {
                                    Mathematics::_3D::Geometry::CSphere3D* pSphere = dynamic_cast<Mathematics::_3D::Geometry::CSphere3D*>(m_pGeometricPrimitive3D);
                                    switch (m_pContainerSet->MapSphere(T(pSphere->GetCenter()), T(pSphere->GetRadius())))
                                    {
                                        case eEmptyInclusion:
                                        case eImproperInclusion:
                                            return false;
                                        case ePartialInclusion:
                                        case eFullInclusion:
                                            return true;
                                    }
                                }
                                return true;
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}
