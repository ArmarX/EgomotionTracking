/*
 * OpenInventorLine.h
 */

#pragma once

#include "../../../../../Mathematics/_3D/Geometry/Line3D.h"
#include "OpenInventorGeometricPrimitive.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        class COpenInventorLine : public COpenInventorGeometricPrimitive
                        {
                        public:

                            COpenInventorLine(Mathematics::_3D::Geometry::CLine3D* pLine);
                            ~COpenInventorLine() override;

                        protected:

                            void Create() override;
                            void FullUpdate() override;
                            bool IsComposedVisible() override;

                        private:

                            SoCoordinate3* m_pSoCoordinate3;
                            SoLineSet* m_pSoLineSet;
                        };
                    }
                }
            }
        }
    }
}

