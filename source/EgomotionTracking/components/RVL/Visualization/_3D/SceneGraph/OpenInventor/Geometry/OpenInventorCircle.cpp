/*
 * OpenInventorCircle.cpp
 */

#include "OpenInventorCircle.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        COpenInventorCircle::COpenInventorCircle(Mathematics::_3D::Geometry::CCircle3D* pCircle3D) :
                            COpenInventorGeometricPrimitive(pCircle3D, Mathematics::_3D::Geometry::CGeometricPrimitive3D::eCircle, true, true, true),
                            m_pSoCoordinate3(nullptr),
                            m_pSoLineSet(nullptr)
                        {
                            Create();
                            Update(true);
                        }

                        COpenInventorCircle::~COpenInventorCircle()
                            = default;

                        void COpenInventorCircle::Create()
                        {
                            m_pSoCoordinate3 = new SoCoordinate3;
                            AddElement(m_pSoCoordinate3);

                            m_pSoLineSet = new SoLineSet;
                            AddElement(m_pSoLineSet);
                            m_pSoLineSet->setUserData(this);

                            SetLineWidth(_VISUALIZATION_CIRCLE_DEFAULT_LINE_WIDTH_);
                        }

                        void COpenInventorCircle::FullUpdate()
                        {
                            if (m_pGeometricPrimitive3D)
                            {
                                Mathematics::_3D::Geometry::CCircle3D* pCircle = dynamic_cast<Mathematics::_3D::Geometry::CCircle3D*>(m_pGeometricPrimitive3D);
                                const SbVec3f Normal = T(pCircle->GetNormal());
                                const SbVec3f Center = T(pCircle->GetCenter());
                                const float Radius = T(pCircle->GetRadius());
                                int TotalSegments = 0;
                                switch (GetComplexityType())
                                {
                                    case SoComplexity::OBJECT_SPACE:
                                    case SoComplexity::SCREEN_SPACE:
                                        TotalSegments = std::max(_VISUALIZATION_CIRCLE_MINIMAL_SEGMENT_LINES_, int(_VISUALIZATION_CIRCLE_MAXIMAL_SEGMENT_LINES_ * GetComplexityValue() + 0.5f));
                                        break;
                                    case SoComplexity::BOUNDING_BOX:
                                        TotalSegments = _VISUALIZATION_CIRCLE_MINIMAL_SEGMENT_LINES_;
                                        break;
                                }
                                const float Delta = float(g_Real2PI) / float(TotalSegments);
                                const SbRotation Rotation(SbVec3f(0.0f, 0.0f, 1.0f), Normal);
                                int Index = 0;
                                SbVec3f Point;
                                m_pSoLineSet->numVertices = 0;
                                m_pSoCoordinate3->point.deleteValues(0);
                                for (float Alpha = 0.0f ; Alpha <= float(g_Real2PI) ; Alpha += Delta)
                                {
                                    Point.setValue(std::cos(Alpha) * Radius, std::sin(Alpha) * Radius, 0.0f);
                                    Rotation.multVec(Point, Point);
                                    m_pSoCoordinate3->point.set1Value(Index++, Point + Center);
                                }
                                m_pSoCoordinate3->point.set1Value(Index++, m_pSoCoordinate3->point[0]);
                                m_pSoLineSet->numVertices.setValue(Index);
                            }
                        }

                        bool COpenInventorCircle::IsComposedVisible()
                        {
                            if (GetVisible() && m_pGeometricPrimitive3D)
                            {
                                if (m_pContainerSet)
                                {
                                    Mathematics::_3D::Geometry::CCircle3D* pCircle = dynamic_cast<Mathematics::_3D::Geometry::CCircle3D*>(m_pGeometricPrimitive3D);
                                    switch (m_pContainerSet->MapCircle(T(pCircle->GetNormal()), T(pCircle->GetCenter()), T(pCircle->GetRadius())))
                                    {
                                        case eEmptyInclusion:
                                        case eImproperInclusion:
                                            return false;
                                        case ePartialInclusion:
                                        case eFullInclusion:
                                            return true;
                                    }
                                }
                                return true;
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}
