/*
 * OpenInventorElement.h
 */

#pragma once

#include "OpenInventor.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Base
                    {
                        class COpenInventorElement
                        {
                        public:

                            static void ResetInstanceIdCounter();

                            enum InclusionType
                            {
                                eEmptyInclusion, ePartialInclusion, eFullInclusion, eImproperInclusion
                            };

                            enum TypeId
                            {
                                eVisualizationVolume, eKinematicFrame, eGeometricPrimitive, eVolumeRendering, eIsosurface, eModelElement, eObject
                            };

                            enum PredefinedColor
                            {
                                eBlack, eGray1_4, eGray1_2, eGray3_4, eWhite, eRed, eRed3_4, eRed1_2, eRed1_4, eGreen, eGreen3_4, eGreen1_2, eGreen1_4, eBlue, eBlue3_4, eBlue1_2, eBlue1_4, eYellow, eYellow3_4, eYellow1_2, eYellow1_4, eCyan, eCyan3_4, eCyan1_2, eCyan1_4, eMagenta, eMagenta3_4, eMagenta1_2, eMagenta1_4
                            };

                            static const SbColor GetPredefinedColor(COpenInventorElement::PredefinedColor Color);

                            COpenInventorElement(const TypeId TypeId, const bool CreateDrawStyle = true, const bool CreateMaterial = true, const bool CreateComplexity = true);
                            virtual ~COpenInventorElement();

                            TypeId GetTypeId() const;

                            SoSwitch* GetBaseSwitch() const;

                            virtual void SetVisible(const bool Visible);
                            virtual bool GetVisible() const;

                            virtual void SetColor(const COpenInventorElement::PredefinedColor Color);
                            virtual void SetColor(const Byte R, const Byte G, const Byte B);
                            virtual void SetColor(const float R, const float G, const float B);
                            virtual void SetColor(const SbColor& Color);
                            virtual SbColor GetColor() const;

                            virtual void SetTransparency(const float Transparency);
                            virtual float GetTransparency() const;

                            virtual void SetDrawStyle(const SoDrawStyle::Style Style);
                            virtual SoDrawStyle::Style GetDrawStyle() const;

                            virtual void SetLineWidth(const float LineWidth);
                            virtual float GetLineWidth() const;

                            virtual void SetPointSize(const float PointSize);
                            virtual float GetPointSize() const;

                            virtual void SetLinePattern(const ushort Pattern);
                            virtual ushort GetLinePattern() const;

                            virtual void SetComplexityType(const SoComplexity::Type Type);
                            virtual SoComplexity::Type GetComplexityType() const;

                            virtual void SetComplexityValue(const float Complexity);
                            virtual float GetComplexityValue() const;

                            virtual void Update(const bool FullUpdate) = 0;

                        protected:

                            void SwitchVisibility(const bool Visible);
                            void AddElement(SoNode* pNode);
                            void RemoveElement(SoNode* pNode);

                            inline SoMaterial* GetBaseMaterial()
                            {
                                return m_pBaseMaterial;
                            }

                            inline const SoMaterial* GetBaseMaterial() const
                            {
                                return m_pBaseMaterial;
                            }

                        private:

                            SoSwitch* m_pBaseSwitch;
                            SoExtSelection* m_pBaseSeparator;
                            SoDrawStyle* m_pBaseDrawStyle;
                            SoMaterial* m_pBaseMaterial;
                            SoComplexity* m_pBaseComplexity;

                            static int s_InstanceIdCounter;

                            int m_InstanceId;
                            bool m_IsVisible;
                            const TypeId m_VisualizationTypeId;
                        };
                    }
                }
            }
        }
    }
}

