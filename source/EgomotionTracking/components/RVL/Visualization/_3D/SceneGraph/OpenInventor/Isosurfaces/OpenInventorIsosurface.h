/*
 * OpenInventorIsosurface.h
 */

#pragma once

#include "../../../../../Mathematics/_3D/Density/DiscreteTopology/DensityCellGrid3D.h"
#include "../../../../../Mathematics/_3D/Density/Isodensity/IsodensitySurface3D.h"
#include "../Base/OpenInventorElement.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Frames
                    {
                        class COpenInventorIsosurface : public Base::COpenInventorElement
                        {
                        public:

                            COpenInventorIsosurface();
                            ~COpenInventorIsosurface() override;

                            void SetVisible(const bool Visible) override;
                            bool SetDensityCellGrid(Mathematics::_3D::Density::DiscreteTopology::CDensityCellGrid3D* pDensityCellGrid);

                        protected:

                            void Update(const bool /*FullUpdate*/) override;

                            SoCoordinate3* m_pSoCoordinate3;
                            SoNormal* m_pSoNormal;
                            SoIndexedFaceSet* m_pSoIndexedFaceSet;
                        };
                    }
                }
            }
        }
    }
}

