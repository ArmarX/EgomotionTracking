/*
 * OpenInventorGeometricPrimitiveFactory.cpp
 */

#include "OpenInventorGeometricPrimitiveFactory.h"

namespace RVL
{
    namespace Visualization
    {
        namespace _3D
        {
            namespace SceneGraph
            {
                namespace OpenInventor
                {
                    namespace Geometry
                    {
                        COpenInventorGeometricPrimitive* COpenInventorGeometricPrimitiveFactory::CreateVisualization(Mathematics::_3D::Geometry::CGeometricPrimitive3D* pGeometricPrimitive3D, const Base::CContainerOpenInventorSet* pContainerOpenInventorSet)
                        {
                            if (pGeometricPrimitive3D)
                                switch (pGeometricPrimitive3D->GetTypeId())
                                {
                                    case Mathematics::_3D::Geometry::CGeometricPrimitive3D::ePoint:
                                        return CreateVisualization(dynamic_cast<Mathematics::_3D::Geometry::CPoint3D*>(pGeometricPrimitive3D), pContainerOpenInventorSet);
                                    case Mathematics::_3D::Geometry::CGeometricPrimitive3D::ePointPair:
                                        return CreateVisualization(dynamic_cast<Mathematics::_3D::Geometry::CPointPair3D*>(pGeometricPrimitive3D), pContainerOpenInventorSet);
                                    case Mathematics::_3D::Geometry::CGeometricPrimitive3D::eLine:
                                        return CreateVisualization(dynamic_cast<Mathematics::_3D::Geometry::CLine3D*>(pGeometricPrimitive3D), pContainerOpenInventorSet);
                                    case Mathematics::_3D::Geometry::CGeometricPrimitive3D::eCircle:
                                        return CreateVisualization(dynamic_cast<Mathematics::_3D::Geometry::CCircle3D*>(pGeometricPrimitive3D), pContainerOpenInventorSet);
                                    case Mathematics::_3D::Geometry::CGeometricPrimitive3D::ePlane:
                                        return CreateVisualization(dynamic_cast<Mathematics::_3D::Geometry::CPlane3D*>(pGeometricPrimitive3D), pContainerOpenInventorSet);
                                    case Mathematics::_3D::Geometry::CGeometricPrimitive3D::eSphere:
                                        return CreateVisualization(dynamic_cast<Mathematics::_3D::Geometry::CSphere3D*>(pGeometricPrimitive3D), pContainerOpenInventorSet);
                                    case Mathematics::_3D::Geometry::CGeometricPrimitive3D::eNone:
                                        return nullptr;
                                }
                            return nullptr;
                        }

                        COpenInventorPoint* COpenInventorGeometricPrimitiveFactory::CreateVisualization(Mathematics::_3D::Geometry::CPoint3D* pPoint3D, const Base::CContainerOpenInventorSet* pContainerOpenInventorSet)
                        {
                            if (pPoint3D)
                            {
                                COpenInventorPoint* pOIPoint = new COpenInventorPoint(pPoint3D);
                                if (pContainerOpenInventorSet)
                                {
                                    pOIPoint->SetContainerSet(pContainerOpenInventorSet);
                                }
                                return pOIPoint;
                            }
                            return nullptr;
                        }

                        COpenInventorPointPair* COpenInventorGeometricPrimitiveFactory::CreateVisualization(Mathematics::_3D::Geometry::CPointPair3D* pPointPair3D, const Base::CContainerOpenInventorSet* pContainerOpenInventorSet)
                        {
                            if (pPointPair3D)
                            {
                                COpenInventorPointPair* pOIPointPair = new COpenInventorPointPair(pPointPair3D);
                                if (pContainerOpenInventorSet)
                                {
                                    pOIPointPair->SetContainerSet(pContainerOpenInventorSet);
                                }
                                return pOIPointPair;
                            }
                            return nullptr;
                        }

                        COpenInventorLine* COpenInventorGeometricPrimitiveFactory::CreateVisualization(Mathematics::_3D::Geometry::CLine3D* pLine3D, const Base::CContainerOpenInventorSet* pContainerOpenInventorSet)
                        {
                            if (pLine3D)
                            {
                                COpenInventorLine* pOILine = new COpenInventorLine(pLine3D);
                                if (pContainerOpenInventorSet)
                                {
                                    pOILine->SetContainerSet(pContainerOpenInventorSet);
                                }
                                return pOILine;
                            }
                            return nullptr;
                        }

                        COpenInventorCircle* COpenInventorGeometricPrimitiveFactory::CreateVisualization(Mathematics::_3D::Geometry::CCircle3D* pCircle3D, const Base::CContainerOpenInventorSet* pContainerOpenInventorSet)
                        {
                            if (pCircle3D)
                            {
                                COpenInventorCircle* pOICircle = new COpenInventorCircle(pCircle3D);
                                if (pContainerOpenInventorSet)
                                {
                                    pOICircle->SetContainerSet(pContainerOpenInventorSet);
                                }
                                return pOICircle;
                            }
                            return nullptr;
                        }

                        COpenInventorPlane* COpenInventorGeometricPrimitiveFactory::CreateVisualization(Mathematics::_3D::Geometry::CPlane3D* pPlane3D, const Base::CContainerOpenInventorSet* pContainerOpenInventorSet)
                        {
                            if (pPlane3D)
                            {
                                COpenInventorPlane* pOIPlane = new COpenInventorPlane(pPlane3D);
                                if (pContainerOpenInventorSet)
                                {
                                    pOIPlane->SetContainerSet(pContainerOpenInventorSet);
                                }
                                return pOIPlane;
                            }
                            return nullptr;
                        }

                        COpenInventorSphere* COpenInventorGeometricPrimitiveFactory::CreateVisualization(Mathematics::_3D::Geometry::CSphere3D* pSphere3D, const Base::CContainerOpenInventorSet* pContainerOpenInventorSet)
                        {
                            if (pSphere3D)
                            {
                                COpenInventorSphere* pOISphere = new COpenInventorSphere(pSphere3D);
                                if (pContainerOpenInventorSet)
                                {
                                    pOISphere->SetContainerSet(pContainerOpenInventorSet);
                                }
                                return pOISphere;
                            }
                            return nullptr;
                        }

                        COpenInventorGeometricPrimitiveFactory::COpenInventorGeometricPrimitiveFactory()
                            = default;

                        COpenInventorGeometricPrimitiveFactory::~COpenInventorGeometricPrimitiveFactory()
                            = default;
                    }
                }
            }
        }
    }
}
