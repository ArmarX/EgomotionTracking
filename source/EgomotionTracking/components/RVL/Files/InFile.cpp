/*
 * InFile.cpp
 */

#include "InFile.h"

namespace RVL
{
    namespace Files
    {
        bool CInFile::ReadBufferFromFile(const std::string& PathFileName, void* pBuffer, const int BufferSize, const CFile::FileMode Mode)
        {
            if (BufferSize && (CFile::Exists(PathFileName) >= BufferSize))
            {
                std::ifstream InStream;
                switch (Mode)
                {
                    case CFile::eText:
                        InStream.open(PathFileName.c_str(), std::ios::in);
                        break;
                    case CFile::eBinary:
                        InStream.open(PathFileName.c_str(), std::ios::in | std::ios::binary);
                        break;
                }
                if ((!InStream.fail()) && InStream.is_open())
                {
                    InStream.read((char*) pBuffer, BufferSize);
                    if (!InStream.fail())
                    {
                        InStream.close();
                        return !InStream.fail();
                    }
                }
            }
            return false;
        }

        bool CInFile::ReadStringFromFile(const std::string& PathFileName, std::string& Content)
        {
            Content.clear();
            const int FileSize = CFile::Exists(PathFileName);
            if (FileSize)
            {
                std::ifstream InStream(PathFileName.c_str(), std::ios::in);
                if ((!InStream.fail()) && InStream.is_open())
                {
                    const int BufferSize = FileSize + 1;
                    char* pBuffer = new char[BufferSize];
                    pBuffer[FileSize] = 0;
                    InStream.read(pBuffer, FileSize);
                    if (InStream.gcount())
                    {
                        Content = std::string(pBuffer);
                    }
                    delete[] pBuffer;
                    pBuffer = nullptr;
                    if (!InStream.fail())
                    {
                        InStream.close();
                        return !InStream.fail();
                    }
                }
            }
            return false;
        }

        CInFile::CInFile(const std::string& PathFileName, const CFile::FileMode Mode) :
            CFile(PathFileName, Mode, CFile::eInput)
        {
            m_FileStatus = Open() ? CFile::eReady : CFile::eErrorWhileOpening;
        }

        CInFile::~CInFile()
        {
            Close();
        }

        bool CInFile::Ignore(const int BufferSize)
        {
            if (IsReady() && BufferSize)
            {
                m_InStream.ignore(BufferSize);
                if (m_InStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileReading;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool CInFile::Read(void* pBuffer, const int BufferSize)
        {
            if (IsReady() && pBuffer && BufferSize)
            {
                m_InStream.read((char*) pBuffer, BufferSize);
                if (m_InStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileReading;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool CInFile::Read(std::string& Content)
        {
            if (IsReady())
            {
                std::getline(m_InStream, Content);
                if (m_InStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileReading;
                    return false;
                }
                if (!m_InStream.gcount())
                {
                    return false;
                }
                return Content.length();
            }
            return false;
        }

        bool CInFile::Read(int& Content)
        {
            if (IsReady())
            {
                m_InStream.read((char*) &Content, sizeof(int));
                if (m_InStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileReading;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool CInFile::Read(Real& Content)
        {
            if (IsReady())
            {
                m_InStream.read((char*) &Content, sizeof(Real));
                if (m_InStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileReading;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool CInFile::Close()
        {
            if (m_FileStatus != eClosed)
            {
                m_InStream.close();
                if (!m_InStream.fail())
                {
                    m_FileStatus = CFile::eClosed;
                    return true;
                }
            }
            return false;
        }

        bool CInFile::Open()
        {
            switch (m_Mode)
            {
                case CFile::eText:
                    m_InStream.open(m_PathFileName.c_str(), std::ios::in);
                    break;
                case CFile::eBinary:
                    m_InStream.open(m_PathFileName.c_str(), std::ios::in | std::ios::binary);
                    break;
            }
            return (!m_InStream.fail()) && m_InStream.is_open();
        }
    }
}
