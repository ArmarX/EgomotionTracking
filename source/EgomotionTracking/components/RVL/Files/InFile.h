/*
 * InFile.h
 */

#pragma once

#include "File.h"

namespace RVL
{
    namespace Files
    {
        class CInFile : public CFile
        {
        public:

            static bool ReadBufferFromFile(const std::string& PathFileName, void* pBuffer, const int BufferSize, const CFile::FileMode Mode);
            static bool ReadStringFromFile(const std::string& PathFileName, std::string& Content);

            CInFile(const std::string& PathFileName, const CFile::FileMode Mode);
            ~CInFile() override;

            bool Ignore(const int BufferSize);
            bool Read(void* pBuffer, const int BufferSize);
            bool Read(std::string& Content);
            bool Read(int& Content);
            bool Read(Real& Content);

            bool Close() override;

        protected:

            bool Open() override;
            std::ifstream m_InStream;
        };
    }
}

