/*
 * OutFile.cpp
 */

#include "OutFile.h"

namespace RVL
{
    namespace Files
    {
        bool COutFile::WriteBufferToFile(const std::string& PathFileName, const void* pBuffer, const int BufferSize, const CFile::FileMode Mode)
        {
            if (PathFileName.length() && pBuffer && BufferSize)
            {
                std::ofstream OutFile;
                switch (Mode)
                {
                    case CFile::eText:
                        OutFile.open(PathFileName.c_str(), std::ios::out);
                        break;
                    case CFile::eBinary:
                        OutFile.open(PathFileName.c_str(), std::ios::out | std::ios::binary);
                        break;
                }
                if ((!OutFile.fail()) && OutFile.is_open())
                {
                    OutFile.write((char*) pBuffer, BufferSize);
                    if (!OutFile.fail())
                    {
                        OutFile.close();
                        return (!OutFile.fail());
                    }
                }
            }
            return false;
        }

        bool COutFile::WriteStringToFile(const std::string& PathFileName, const std::string& Content)
        {
            if (Content.length())
            {
                return WriteBufferToFile(PathFileName, Content.c_str(), Content.length(), CFile::eText);
            }
            return false;
        }

        COutFile::COutFile(const std::string& PathFileName, const CFile::FileMode Mode) :
            CFile(PathFileName, Mode, CFile::eOutput)
        {
            m_FileStatus = Open() ? CFile::eReady : CFile::eErrorWhileOpening;
        }

        COutFile::~COutFile()
        {
            Close();
        }

        bool COutFile::Open()
        {
            switch (m_Mode)
            {
                case CFile::eText:
                    m_OutStream.open(m_PathFileName.c_str(), std::ios::out);
                    break;
                case CFile::eBinary:
                    m_OutStream.open(m_PathFileName.c_str(), std::ios::out | std::ios::binary);
                    break;
            }
            return (!m_OutStream.fail()) && m_OutStream.is_open();
        }

        bool COutFile::Write(const void* pBuffer, const int BufferSize)
        {
            if (pBuffer && BufferSize && IsReady())
            {
                m_OutStream.write((const char*) pBuffer, BufferSize);
                if (m_OutStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileWriting;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool COutFile::Write(const std::string& Content)
        {
            if (IsReady() && Content.length())
            {
                m_OutStream.write((const char*) Content.c_str(), Content.length());
                if (m_OutStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileWriting;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool COutFile::Write(const Real Value)
        {
            if (IsReady())
            {
                m_OutStream.write((const char*) &Value, sizeof(Real));
                if (m_OutStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileWriting;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool COutFile::Write(const int Value)
        {
            if (IsReady())
            {
                m_OutStream.write((const char*) &Value, sizeof(int));
                if (m_OutStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileWriting;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool COutFile::Close()
        {
            if (m_FileStatus != eClosed)
            {
                m_OutStream.close();
                if (!m_OutStream.fail())
                {
                    m_FileStatus = CFile::eClosed;
                    return true;
                }
            }
            return false;
        }
    }
}
