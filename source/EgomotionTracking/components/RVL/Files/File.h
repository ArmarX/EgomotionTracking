/*
 * File.h
 */

#pragma once

#include "../Common/Includes.h"
#include "../Common/DataTypes.h"

namespace RVL
{
    namespace Files
    {
        class CFile
        {
        public:

            static int Exists(const std::string& PathFileName, const int ConditionalSize = 0);

            enum FileMode
            {
                eText, eBinary
            };

            enum FileType
            {
                eInput, eOutput
            };

            enum FileStatus
            {
                eReady, eErrorWhileOpening, eErrorWhileClosing, eErrorWhileWriting, eErrorWhileReading, eClosed
            };

            CFile(const std::string& PathFileName, const FileMode Mode, const FileType Type);
            virtual ~CFile();

            const std::string& GetPathFileName() const;
            FileMode GetMode() const;
            FileType GetType() const;
            FileStatus GetFileStatus() const;
            bool IsReady() const;

        protected:

            virtual bool Open() = 0;
            virtual bool Close() = 0;

            const std::string m_PathFileName;
            const FileMode m_Mode;
            const FileType m_Type;
            FileStatus m_FileStatus;
        };
    }
}

