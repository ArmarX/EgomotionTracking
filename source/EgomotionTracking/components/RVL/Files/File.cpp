/*
 * File.cpp
 */

#include "File.h"

namespace RVL
{
    namespace Files
    {
        int CFile::Exists(const std::string& PathFileName, const int ConditionalSize)
        {
            if (PathFileName.length())
            {
                struct stat QueryResult;
                if (stat(PathFileName.c_str(), &QueryResult) == 0)
                {
                    if (ConditionalSize)
                    {
                        return (ConditionalSize == int(QueryResult.st_size)) ? ConditionalSize : 0;
                    }
                    return int(QueryResult.st_size);
                }
            }
            return 0;
        }

        CFile::CFile(const std::string& PathFileName, const FileMode Mode, const FileType Type) :
            m_PathFileName(PathFileName),
            m_Mode(Mode),
            m_Type(Type),
            m_FileStatus(eClosed)
        {
        }

        CFile::~CFile()
            = default;

        const std::string& CFile::GetPathFileName() const
        {
            return m_PathFileName;
        }

        CFile::FileMode CFile::GetMode() const
        {
            return m_Mode;
        }

        CFile::FileType CFile::GetType() const
        {
            return m_Type;
        }

        CFile::FileStatus CFile::GetFileStatus() const
        {
            return m_FileStatus;
        }

        bool CFile::IsReady() const
        {
            return (m_FileStatus == CFile::eReady);
        }
    }
}
