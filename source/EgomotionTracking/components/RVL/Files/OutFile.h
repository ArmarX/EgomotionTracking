/*
 * OutFile.h
 */

#pragma once

#include "File.h"

namespace RVL
{
    namespace Files
    {
        class COutFile : public CFile
        {
        public:

            static bool WriteBufferToFile(const std::string& PathFileName, const void* pBuffer, const int BufferSize, const CFile::FileMode Mode);
            static bool WriteStringToFile(const std::string& PathFileName, const std::string& Content);

            COutFile(const std::string& PathFileName, const CFile::FileMode Mode);
            ~COutFile() override;

            bool Write(const void* pBuffer, const int BufferSize);
            bool Write(const std::string& Content);
            bool Write(const Real Value);
            bool Write(const int Value);

            bool Close() override;

        protected:

            bool Open() override;
            std::ofstream m_OutStream;
        };
    }
}

