/*
 * ConsoleOutputManager.cpp
 */

#include "ConsoleOutputManager.h"

namespace RVL
{
    namespace Console
    {
        Processing::CPU::Threading::CMutex CConsoleOutputManager::s_ConsoleOutputManagerMutex;

        CConsoleOutputManager::CConsoleOutputManager(const int EnabledChannelsBitMask, const int PrefixeLevel) :
            m_EnabledChannelsBitMask(EnabledChannelsBitMask),
            m_PrefixeLevel(PrefixeLevel)
        {
            m_Prefixes.reserve(_RVL_CONSOLE_OUTPUT_MAXIMAL_PREFIX_LEVEL_);
            for (int i = 0 ; i < _RVL_CONSOLE_OUTPUT_MAXIMAL_PREFIX_LEVEL_ ; ++i)
            {
                std::string Prefix("\n");
                for (int j = 0 ; j < i ; ++j)
                {
                    Prefix += "\t";
                }
                m_Prefixes.push_back(Prefix);
            }
        }

        CConsoleOutputManager::~CConsoleOutputManager()
        {
            m_Prefixes.clear();
        }

        bool CConsoleOutputManager::SetEnabledChannel(const ChannelType Channel, const bool Enabled)
        {
            if ((Channel >= eErrors) && (Channel <= eGeneral) && (HammingWeight(Channel) == 1))
            {
                if (Enabled)
                {
                    m_EnabledChannelsBitMask |= (1 << Channel);
                }
                else
                {
                    m_EnabledChannelsBitMask &= ~(1 << Channel);
                }
                return true;
            }
            return false;
        }

        bool CConsoleOutputManager::SetEnabledChannelsBitMask(const int EnabledChannelsBitMask)
        {
            if ((EnabledChannelsBitMask >= eErrors) && (EnabledChannelsBitMask <= eAll))
            {
                m_EnabledChannelsBitMask = EnabledChannelsBitMask;
                return true;
            }
            return false;
        }

        bool CConsoleOutputManager::SetPrefixeLevel(const int PrefixeLevel)
        {
            if (PrefixeLevel < _RVL_CONSOLE_OUTPUT_MAXIMAL_PREFIX_LEVEL_)
            {
                m_PrefixeLevel = PrefixeLevel;
                return true;
            }
            return false;
        }

        bool CConsoleOutputManager::GetEnabledChannel(const ChannelType Channel) const
        {
            if ((Channel >= eErrors) && (Channel <= eGeneral) && (HammingWeight(Channel) == 1))
            {
                return m_EnabledChannelsBitMask & (1 << Channel);
            }
            return false;
        }

        int CConsoleOutputManager::GetEnabledChannelsBitMask() const
        {
            return m_EnabledChannelsBitMask;
        }

        int CConsoleOutputManager::GetPrefixeLevel() const
        {
            return m_PrefixeLevel;
        }

        const std::string& CConsoleOutputManager::GetCurrentPrefixLevel() const
        {
            return m_Prefixes[m_PrefixeLevel];
        }

        bool CConsoleOutputManager::Output(const std::string& Text, const ChannelType Channel, const int LevelOffset, const bool AddEndline, const bool CallFlushing)
        {
            const bool IsChannelEnabled = (m_EnabledChannelsBitMask & Channel) && (Channel >= eErrors) && (Channel <= eGeneral);
            if (IsChannelEnabled)
            {
                s_ConsoleOutputManagerMutex.BlockingLock();
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                switch (Channel)
                {
                    case eErrors:
                        std::cout << "\033[31m" << m_Prefixes[std::min(m_PrefixeLevel + LevelOffset, _RVL_CONSOLE_OUTPUT_MAXIMAL_PREFIX_LEVEL_ - 1)] << "[RVL::Error] " << Text << "\033[0m";
                        break;
                    case eWarnings:
                        std::cout << "\033[35m" << m_Prefixes[std::min(m_PrefixeLevel + LevelOffset, _RVL_CONSOLE_OUTPUT_MAXIMAL_PREFIX_LEVEL_ - 1)] << "[RVL::Warning] " << Text << "\033[0m";
                        break;
                    case eExceptions:
                        std::cout << "\033[36m" << m_Prefixes[std::min(m_PrefixeLevel + LevelOffset, _RVL_CONSOLE_OUTPUT_MAXIMAL_PREFIX_LEVEL_ - 1)] << "[RVL::Exception] " << Text << "\033[0m";
                        break;
                    case eSettings:
                        std::cout << "\033[34m" << m_Prefixes[std::min(m_PrefixeLevel + LevelOffset, _RVL_CONSOLE_OUTPUT_MAXIMAL_PREFIX_LEVEL_ - 1)] << "[RVL::Settings] " << Text << "\033[0m";
                        break;
                    case eDebugging:
                        std::cout << "\033[32m" << m_Prefixes[std::min(m_PrefixeLevel + LevelOffset, _RVL_CONSOLE_OUTPUT_MAXIMAL_PREFIX_LEVEL_ - 1)] << "[RVL::Debugging] " << Text << "\033[0m";
                        break;
                    case eGeneral:
                        std::cout << "\033[37m" << m_Prefixes[std::min(m_PrefixeLevel + LevelOffset, _RVL_CONSOLE_OUTPUT_MAXIMAL_PREFIX_LEVEL_ - 1)] << "[RVL::General] " << Text << "\033[0m";
                        break;
                    case eAll:
                        break;
                }
                if (AddEndline)
                {
                    std::cout << std::endl;
                }
                else if (CallFlushing)
                {
                    std::cout.flush();
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                s_ConsoleOutputManagerMutex.Unlock();
                return true;
            }
            return false;
        }

        bool CConsoleOutputManager::Output(const std::ostringstream& TextStream, const ChannelType Channel, const int LevelOffset, const bool AddEndline, const bool CallFlushing)
        {
            return Output(TextStream.str().c_str(), Channel, LevelOffset, AddEndline, CallFlushing);
        }

        int CConsoleOutputManager::HammingWeight(const int BitMask) const
        {
            int Auxiliar = BitMask - ((BitMask >> 1) & 0x55555555);
            Auxiliar = (Auxiliar & 0x33333333) + ((Auxiliar >> 2) & 0x33333333);
            return (((Auxiliar + (Auxiliar >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
        }
    }
}
