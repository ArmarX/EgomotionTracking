/*
 * ConsoleOutputManager.h
 */

#pragma once

#include "../Common/Includes.h"
#include "../Common/DataTypes.h"
#include "../Processing/CPU/Threading/Mutex.h"

#define _RVL_HEAD_FULL_INFORMATION_ "At -> " << __PRETTY_FUNCTION__ << " [ " << __FILE__ << " : " << __LINE__ << " ]" << "[ Date : " << __DATE__ << " , Time : " << __TIME__ << " ]"
#define _RVL_HEAD_INFORMATION_ "At -> " << __PRETTY_FUNCTION__ << " [ " << __FILE__ << " : " << __LINE__ << " ]"
#define _RVL_CONSOLE_OUTPUT_MAXIMAL_PREFIX_LEVEL_ 10

#define _RVL_CONSOLE_CURRENT_PREFIX_LEVEL_ RVL::Console::g_ConsoleOutputManager.GetCurrentPrefixLevel()
#define _RVL_CONSOLE_OUTPUT_ERROR_(Text,Offset) RVL::Console::g_ConsoleOutputManager.Output(Text,RVL::Console::CConsoleOutputManager::eErrors,Offset,true,true)
#define _RVL_CONSOLE_OUTPUT_WARNINGS_(Text,Offset) RVL::Console::g_ConsoleOutputManager.Output(Text,RVL::Console::CConsoleOutputManager::eWarnings,Offset,true,true)
#define _RVL_CONSOLE_OUTPUT_EXECEPTIONS_(Text,Offset) RVL::Console::g_ConsoleOutputManager.Output(Text,RVL::Console::CConsoleOutputManager::eExceptions,Offset,true,true)
#define _RVL_CONSOLE_OUTPUT_SETTINGS_(Text,Offset) RVL::Console::g_ConsoleOutputManager.Output(Text,RVL::Console::CConsoleOutputManager::eSettings,Offset,true,true)
#define _RVL_CONSOLE_OUTPUT_DEBUGGING_(Text,Offset) RVL::Console::g_ConsoleOutputManager.Output(Text,RVL::Console::CConsoleOutputManager::eDebugging,Offset,true,true)
#define _RVL_CONSOLE_OUTPUT_GENERAL_(Text,Offset) RVL::Console::g_ConsoleOutputManager.Output(Text,RVL::Console::CConsoleOutputManager::eGeneral,Offset,true,true)

namespace RVL
{
    namespace Console
    {
        class CConsoleOutputManager
        {
        public:

            enum ChannelType
            {
                eErrors = 0X01, eWarnings = 0X02, eExceptions = 0X04, eSettings = 0X08, eDebugging = 0X10, eGeneral = 0X20, eAll = 0X3F
            };

            CConsoleOutputManager(const int EnabledChannelsBitMask, const int PrefixeLevel);
            virtual ~CConsoleOutputManager();

            bool SetEnabledChannel(const ChannelType Channel, const bool Enabled);
            bool SetEnabledChannelsBitMask(const int EnabledChannelsBitMask);
            bool SetPrefixeLevel(const int PrefixeLevel);

            bool GetEnabledChannel(const ChannelType Channel) const;
            int GetEnabledChannelsBitMask() const;
            int GetPrefixeLevel() const;
            const std::string& GetCurrentPrefixLevel() const;

            bool Output(const std::string& Text, const ChannelType Channel, const int LevelOffset, const bool AddEndline, const bool CallFlushing);
            bool Output(const std::ostringstream& TextStream, const ChannelType Channel, const int LevelOffset, const bool AddEndline, const bool CallFlushing);

        protected:

            int HammingWeight(const int BitMask) const;
            static Processing::CPU::Threading::CMutex s_ConsoleOutputManagerMutex;
            std::vector<std::string> m_Prefixes;
            int m_EnabledChannelsBitMask;
            int m_PrefixeLevel;
        };

        static CConsoleOutputManager g_ConsoleOutputManager(CConsoleOutputManager::eAll, 1);
    }
}

