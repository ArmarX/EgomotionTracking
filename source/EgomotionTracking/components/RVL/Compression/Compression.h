/*
 * CCompression.h
 */

#pragma once

#include "../Common/Includes.h"
#include "../Common/DataTypes.h"

namespace RVL
{
    namespace Compression
    {
        class CCompression
        {
        public:

            static const Byte* Compress(const Byte* pNonCompressedBuffer, const unsigned long NonCompressedLength, unsigned long& CompressedLength);
            static const Byte* Decompress(const Byte* pCompressedBuffer, const unsigned long CompressedLength, const unsigned long NonCompressedLength);

        private:

            CCompression();
            virtual ~CCompression();
        };
    }
}

