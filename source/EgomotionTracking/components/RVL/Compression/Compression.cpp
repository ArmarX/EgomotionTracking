/*
 * CCompression.cpp
 */

#include "Compression.h"

namespace RVL
{
    namespace Compression
    {
        const Byte* CCompression::Compress(const Byte* pNonCompressedBuffer, const unsigned long NonCompressedLength, unsigned long& CompressedLength)
        {
            CompressedLength = 0;
            Byte* pCompressedBuffer = nullptr;
            if (pNonCompressedBuffer && NonCompressedLength)
            {
                CompressedLength = (unsigned long)(std::ceil(Real(NonCompressedLength) * Real(1.125) + 12));
                pCompressedBuffer = new Byte[CompressedLength];
                if (compress(pCompressedBuffer, &CompressedLength, pNonCompressedBuffer, NonCompressedLength) != Z_OK)
                {
                    delete[] pCompressedBuffer;
                    pCompressedBuffer = nullptr;
                }
            }
            return pCompressedBuffer;
        }

        const Byte* CCompression::Decompress(const Byte* pCompressedBuffer, const unsigned long CompressedLength, const unsigned long NonCompressedLength)
        {
            Byte* pNonCompressedBuffer = nullptr;
            if (pCompressedBuffer && CompressedLength && NonCompressedLength && (CompressedLength <= NonCompressedLength))
            {
                pNonCompressedBuffer = new Byte[NonCompressedLength];
                unsigned long Length = NonCompressedLength;
                if (uncompress(pNonCompressedBuffer, &Length, pCompressedBuffer, CompressedLength) != Z_OK)
                {
                    delete[] pNonCompressedBuffer;
                    pNonCompressedBuffer = nullptr;
                }
            }
            return pNonCompressedBuffer;
        }

        CCompression::CCompression()
            = default;

        CCompression::~CCompression()
            = default;
    }
}
