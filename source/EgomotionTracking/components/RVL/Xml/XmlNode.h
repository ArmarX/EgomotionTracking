/*
 * XmlNode.h
 */

#pragma once

#include "../Common/DataTypes.h"
#include "../Common/Includes.h"
#include "../Console/ConsoleOutputManager.h"
#include "../Files/File.h"
#include "../Files/InFile.h"
#include "../Files/OutFile.h"

namespace RVL
{
    namespace Xml
    {
        class CXmlNode
        {
        public:

            static void Initialize();
            static CXmlNode* CreateFromFile(const std::string& FileName);
            static CXmlNode* CreateFromContent(const std::string& Content);

            static std::string FormatAddTabs(const std::string& Content);

            CXmlNode(const std::string& Name);
            virtual ~CXmlNode();

            CXmlNode* CreateSubNode(const std::string& SubNodeName);
            bool DestroySubNode(CXmlNode*& pSubNode);

            bool LoadFromFile(const std::string& FileName);
            bool SetContent(const std::string& Content);

            bool SetText(const std::string& Text);
            bool SetAttribute(const std::string& AttributeName, const int64_t Value);
            bool SetAttribute(const std::string& AttributeName, const int Value);
            bool SetAttribute(const std::string& AttributeName, const bool Value);
            bool SetAttribute(const std::string& AttributeName, const std::string& Value);
            bool SetAttribute(const std::string& AttributeName, const Byte Value);
            bool SetAttribute(const std::string& AttributeName, const Real Value);
            bool SetAttribute(const std::string& AttributeName, const char* pString);

            void Clear();
            void ClearAttributes();
            void ClearSubNodes();

            void SetName(const std::string& Name);
            const std::string& GetName() const;

            bool HasAttribute(const std::string& AttributeName) const;

            const std::string& GetText() const;
            bool GetAttribute(const std::string& AttributeName, int64_t& Value) const;
            bool GetAttribute(const std::string& AttributeName, int& Value) const;
            bool GetAttribute(const std::string& AttributeName, bool& Value) const;
            bool GetAttribute(const std::string& AttributeName, std::string& Value) const;
            bool GetAttribute(const std::string& AttributeName, Byte& Value) const;
            bool GetAttribute(const std::string& AttributeName, Real& Value) const;

            bool AttributeEquals(const std::string& AttributeName, const int Value) const;
            bool AttributeEquals(const std::string& AttributeName, const std::string& Value) const;

            bool SaveToFile(const std::string& FileName, const bool OverWrite) const;
            std::string ToString() const;
            void ToString(std::ostringstream& OutputStream) const;

            int GetTotalSubNodes() const;
            const std::list<CXmlNode*>& GetSubNodes() const;
            CXmlNode* GetFirstSubNode();
            const CXmlNode* GetFirstSubNode() const;

        protected:

            CXmlNode();

            enum ParsingState
            {
                eParsingError, eParsingNode, eParsingAttributeName, eParsingAttributeValue, eParsingNodeContent, eFinishParsingNode, eAbortingParsingNode
            };

            static bool GetToken(std::istringstream& Stream, char& Token);
            static bool GetNonTrivialToken(std::istringstream& Stream, char& NonTrivialToken);
            static bool RemoveTrivialTokens(std::istringstream& Stream);
            static bool IsAlpha(const char Token);
            static bool IsDigit(const char Token);
            static bool ParseIdentifier(std::istringstream& Stream, std::string& Identifier);
            static bool ParseValue(std::istringstream& Stream, std::string& Value);
            static bool ParseText(std::istringstream& Stream, std::string& Text);
            static std::ostringstream s_OutputStream;

            ParsingState AbortParsing();
            ParsingState Parse(std::istringstream& Stream);

            std::string UtilityToString(const Byte Value) const;
            std::string UtilityToString(const Real Value) const;
            std::string UtilityToString(const int64_t Value) const;
            std::string UtilityToString(const int Value) const;
            std::string UtilityToString(const bool Value) const;

            std::string m_Name;
            std::string m_Text;

            struct StringCompare
            {
                bool operator()(const std::string& lhs, const std::string& rhs) const
                {
                    return rhs.compare(lhs) > 0;
                }
            };

            std::map<std::string, std::string, StringCompare> m_Attributes;
            std::list<CXmlNode*> m_SubNodes;   //TODO CHANGE FOR DEQUE
        };
    }
}

