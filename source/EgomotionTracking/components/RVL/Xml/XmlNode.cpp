/*
 * XmlNode
 */

#include "XmlNode.h"

namespace RVL
{
    namespace Xml
    {
        std::ostringstream CXmlNode::s_OutputStream;

        void CXmlNode::Initialize()
        {
            s_OutputStream.precision(g_RealDisplayDigits);
        }

        CXmlNode* CXmlNode::CreateFromFile(const std::string& FileName)
        {
            CXmlNode* pXmlNode = new CXmlNode();

            if (!pXmlNode)
            {
                std::ostringstream TextStream;
                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                return nullptr;
            }

            if (!pXmlNode->LoadFromFile(FileName))
            {
                std::ostringstream TextStream;
                TextStream << _RVL_HEAD_FULL_INFORMATION_ << " File name: " << FileName;
                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                delete pXmlNode;
                return nullptr;
            }

            return pXmlNode;
        }

        CXmlNode* CXmlNode::CreateFromContent(const std::string& Content)
        {
            if (!Content.length())
            {
                std::ostringstream TextStream;
                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                return nullptr;
            }

            CXmlNode* pXmlNode = new CXmlNode();

            if (!pXmlNode)
            {
                std::ostringstream TextStream;
                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                return nullptr;
            }

            if (!pXmlNode->SetContent(Content))
            {
                std::ostringstream TextStream;
                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                delete pXmlNode;
                return nullptr;
            }

            return pXmlNode;
        }

        std::string CXmlNode::FormatAddTabs(const std::string& Content)
        {
            const int Length = Content.length();
            if (!Length)
            {
                return Content;
            }

            int TotalOpen = 0, TotalClose = 0, TotalNewLines = 0, TotalTabs = 0;
            for (int i = 0; i < Length; ++i)
                switch (Content[i])
                {
                    case '<':
                        ++TotalOpen;
                        break;
                    case '>':
                        ++TotalClose;
                        break;
                    case '\n':
                        ++TotalNewLines;
                        break;
                    case '\t':
                        ++TotalTabs;
                        break;
                }

            if (!TotalOpen)
            {
                std::ostringstream TextStream;
                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                return Content;
            }

            if (TotalOpen != TotalClose)
            {
                std::ostringstream TextStream;
                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                return Content;
            }

            if (TotalNewLines)
            {
                std::ostringstream TextStream;
                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                return Content;
            }

            if (TotalTabs)
            {
                std::ostringstream TextStream;
                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                return Content;
            }

            std::string FormattedContent;
            int LastPreclosignLocation = g_IntegerMinusInfinity, TabLevels = 0;
            bool ClosingTag = false;
            for (int i = 0; i < Length; ++i)
                switch (Content[i])
                {
                    case '<':
                        ClosingTag = Content[i + 1] == '/';
                        if (ClosingTag)
                        {
                            --TabLevels;
                        }
                        for (int j = 0; j < TabLevels; ++j)
                        {
                            FormattedContent += '\t';
                        }
                        FormattedContent += Content[i];
                        break;
                    case '>':
                        FormattedContent += Content[i];
                        FormattedContent += '\n';
                        if (((i - 1) != LastPreclosignLocation) && (!ClosingTag))
                        {
                            ++TabLevels;
                        }
                        break;
                    case '/':
                        LastPreclosignLocation = i;
                        FormattedContent += Content[i];
                        break;
                    default:
                        FormattedContent += Content[i];
                        break;
                }

            return FormattedContent;
        }

        CXmlNode::CXmlNode() :
            m_Name(),
            m_Text()
        {
        }

        CXmlNode::CXmlNode(const std::string& Name) :
            m_Name(Name),
            m_Text()
        {
        }

        CXmlNode::~CXmlNode()
        {
            std::list<CXmlNode*>::iterator EndSubNodes = m_SubNodes.end();
            for (std::list<CXmlNode*>::iterator ppSubNode = m_SubNodes.begin() ; ppSubNode != EndSubNodes ; ++ppSubNode)
            {
                delete *ppSubNode;
            }
        }

        CXmlNode* CXmlNode::CreateSubNode(const std::string& SubNodeName)
        {
            if (!SubNodeName.length())
            {
                std::ostringstream TextStream;
                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                return nullptr;
            }

            CXmlNode* pXMLNode = new CXmlNode(SubNodeName);

            if (!pXMLNode)
            {
                std::ostringstream TextStream;
                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                return nullptr;
            }

            m_SubNodes.push_back(pXMLNode);

            return pXMLNode;
        }

        bool CXmlNode::DestroySubNode(CXmlNode*& pSubNode)
        {
            if (!pSubNode)
            {
                std::ostringstream TextStream;
                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                return false;
            }

            if (!m_SubNodes.size())
            {
                std::ostringstream TextStream;
                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                return false;
            }

            std::list<CXmlNode*>::iterator EndSubNodes = m_SubNodes.end();
            for (std::list<CXmlNode*>::iterator ppXmlNode = m_SubNodes.begin() ; ppXmlNode != EndSubNodes ; ++ppXmlNode)
                if ((*ppXmlNode) == pSubNode)
                {
                    m_SubNodes.erase(ppXmlNode);
                    delete pSubNode;
                    pSubNode = nullptr;
                    return true;
                }

            return false;
        }

        bool CXmlNode::GetToken(std::istringstream& Stream, char& Token)
        {
            return Stream.get(Token) && Stream.good();
        }

        bool CXmlNode::GetNonTrivialToken(std::istringstream& Stream, char& NonTrivialToken)
        {
            while (Stream.get(NonTrivialToken) && Stream.good())
                switch (NonTrivialToken)
                {
                    case ' ':
                    case '\t':
                    case '\n':
                        continue;
                    default:
                        return true;
                }
            return false;
        }

        bool CXmlNode::RemoveTrivialTokens(std::istringstream& Stream)
        {
            char Token;
            while (Stream.get(Token) && Stream.good())
                switch (Token)
                {
                    case ' ':
                    case '\t':
                    case '\n':
                        continue;
                    default:
                        Stream.unget();
                        return true;
                }
            return false;
        }

        bool CXmlNode::IsAlpha(const char Token)
        {
            return ((Token >= 'a') && (Token <= 'z')) || ((Token >= 'A') && (Token <= 'Z')) || (Token == '_');
        }

        bool CXmlNode::IsDigit(const char Token)
        {
            return (Token >= '0') && (Token <= '9');
        }

        bool CXmlNode::ParseIdentifier(std::istringstream& Stream, std::string& Identifier)
        {
            Identifier.clear();
            char Token;
            if (GetNonTrivialToken(Stream, Token) && IsAlpha(Token))
            {
                Identifier += Token;
                while (GetToken(Stream, Token))
                    switch (Token)
                    {
                        case ' ':
                        case '\t':
                        case '\n':
                            return RemoveTrivialTokens(Stream) ? Identifier.length() : false;
                        case '=':
                        case '/':
                        case '>':
                            Stream.unget();
                            return Identifier.length();
                        default:
                            if (IsAlpha(Token) || IsDigit(Token))
                            {
                                Identifier += Token;
                            }
                            else
                            {
                                return false;
                            }
                    }
            }
            return false;
        }

        bool CXmlNode::ParseValue(std::istringstream& Stream, std::string& Value)
        {
            Value.clear();
            char Token;
            if (GetNonTrivialToken(Stream, Token) && (Token == '\"'))
            {
                while (GetToken(Stream, Token))
                {
                    if (Token == '\"')
                    {
                        return true;
                    }
                    else
                    {
                        Value += Token;
                    }
                }
            }
            return false;
        }

        bool CXmlNode::ParseText(std::istringstream& Stream, std::string& Text)
        {
            Text.clear();
            char Token;
            while (GetToken(Stream, Token))
                if (Token == '<')
                {
                    Stream.unget();
                    return Text.length();
                }
                else
                {
                    Text += Token;
                }
            return false;
        }

        bool CXmlNode::LoadFromFile(const std::string& FileName)
        {
            Clear();
            std::string Content;
            if (Files::CInFile::ReadStringFromFile(FileName, Content))
            {
                return SetContent(Content);
            }
            return false;
        }

        bool CXmlNode::SetContent(const std::string& Content)
        {
            Clear();
            if (Content.length())
            {
                std::istringstream Stream(Content);
                return Parse(Stream) != eParsingError;
            }
            return false;
        }

        CXmlNode::ParsingState CXmlNode::AbortParsing()
        {
            ClearAttributes();
            ClearSubNodes();
            return eAbortingParsingNode;
        }

        //TODO Refactor this method, reduce code size and increase modularity
        CXmlNode::ParsingState CXmlNode::Parse(std::istringstream& Stream)
        {
            char Token;
            if (GetNonTrivialToken(Stream, Token) && (Token == '<'))
                if (ParseIdentifier(Stream, m_Name))
                    if (GetNonTrivialToken(Stream, Token))
                        switch (Token)
                        {
                            case '/':   //Empty Node
                                if (GetToken(Stream, Token) && (Token == '>'))
                                {
                                    return eFinishParsingNode;
                                }
                                else
                                {
                                    return AbortParsing();
                                }
                            case '>':   //Content Node
                                /**/
                                if (GetNonTrivialToken(Stream, Token))
                                    switch (Token)
                                    {
                                        case '<':   //Sub Node(s)
                                            Stream.unget();
                                            do
                                            {
                                                CXmlNode* pSubNode = new CXmlNode();
                                                if (pSubNode->Parse(Stream) == eFinishParsingNode)
                                                {
                                                    m_SubNodes.push_back(pSubNode);
                                                }
                                                else
                                                {
                                                    delete pSubNode;
                                                    return AbortParsing();
                                                }
                                                if (GetNonTrivialToken(Stream, Token) && (Token == '<'))
                                                {
                                                    if (GetToken(Stream, Token))
                                                    {
                                                        if (Token == '/')
                                                        {
                                                            std::string ClosingName;
                                                            if (ParseIdentifier(Stream, ClosingName) && (ClosingName == m_Name))
                                                                if (GetNonTrivialToken(Stream, Token) && (Token == '>'))
                                                                {
                                                                    return eFinishParsingNode;
                                                                }
                                                            return AbortParsing();
                                                        }
                                                        else
                                                        {
                                                            Stream.unget();
                                                            Stream.unget();
                                                            continue;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        return AbortParsing();
                                                    }
                                                }
                                                else
                                                {
                                                    Stream.unget();
                                                    if (ParseText(Stream, m_Text))
                                                    {
                                                        if (GetNonTrivialToken(Stream, Token) && (Token == '<'))
                                                        {
                                                            if (GetToken(Stream, Token) && (Token == '/'))
                                                            {
                                                                std::string ClosingName;
                                                                if (ParseIdentifier(Stream, ClosingName) && (ClosingName == m_Name))
                                                                    if (GetNonTrivialToken(Stream, Token) && (Token == '>'))
                                                                    {
                                                                        return eFinishParsingNode;
                                                                    }
                                                                return AbortParsing();
                                                            }
                                                            else
                                                            {
                                                                Stream.unget();
                                                            }
                                                        }
                                                        else
                                                        {
                                                            return AbortParsing();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        return AbortParsing();
                                                    }
                                                }
                                            }
                                            while (true);
                                            break;
                                        default:   //Text Content
                                            if (ParseText(Stream, m_Text))
                                            {
                                                if (GetNonTrivialToken(Stream, Token) && (Token == '<'))
                                                {
                                                    if (GetToken(Stream, Token) && (Token == '/'))
                                                    {
                                                        std::string ClosingName;
                                                        if (ParseIdentifier(Stream, ClosingName) && (ClosingName == m_Name))
                                                            if (GetNonTrivialToken(Stream, Token) && (Token == '>'))
                                                            {
                                                                return eFinishParsingNode;
                                                            }
                                                        return AbortParsing();
                                                    }
                                                    else
                                                    {
                                                        Stream.unget();
                                                    }
                                                }
                                                else
                                                {
                                                    return AbortParsing();
                                                }
                                            }
                                            else
                                            {
                                                return AbortParsing();
                                            }
                                            break;
                                    }
                                break;
                            default:   //Node Attributes
                                if (IsAlpha(Token))
                                {
                                    Stream.unget();
                                    std::string AttributeName, AttributeValue;
                                    do
                                    {
                                        if (ParseIdentifier(Stream, AttributeName))
                                            if (GetNonTrivialToken(Stream, Token) && (Token == '='))
                                                if (ParseValue(Stream, AttributeValue))
                                                {
                                                    SetAttribute(AttributeName, AttributeValue);
                                                }
                                                else
                                                {
                                                    return AbortParsing();
                                                }
                                            else
                                            {
                                                return AbortParsing();
                                            }
                                        else
                                        {
                                            return AbortParsing();
                                        }
                                        if (GetNonTrivialToken(Stream, Token))
                                            switch (Token)
                                            {
                                                case '/':
                                                    if (GetToken(Stream, Token) && (Token == '>'))
                                                    {
                                                        return eFinishParsingNode;
                                                    } [[fallthrough]];
                                                case '>':
                                                    //Nodes Content
                                                    if (GetNonTrivialToken(Stream, Token))
                                                        switch (Token)
                                                        {
                                                            case '<':   //Sub Node(s)
                                                                Stream.unget();
                                                                do
                                                                {
                                                                    CXmlNode* pSubNode = new CXmlNode();
                                                                    if (pSubNode->Parse(Stream) == eFinishParsingNode)
                                                                    {
                                                                        m_SubNodes.push_back(pSubNode);
                                                                    }
                                                                    else
                                                                    {
                                                                        delete pSubNode;
                                                                        return AbortParsing();
                                                                    }
                                                                    if (GetNonTrivialToken(Stream, Token) && (Token == '<'))
                                                                    {
                                                                        if (GetToken(Stream, Token))
                                                                        {
                                                                            if (Token == '/')
                                                                            {
                                                                                std::string ClosingName;
                                                                                if (ParseIdentifier(Stream, ClosingName) && (ClosingName == m_Name))
                                                                                    if (GetNonTrivialToken(Stream, Token) && (Token == '>'))
                                                                                    {
                                                                                        return eFinishParsingNode;
                                                                                    }
                                                                                return AbortParsing();
                                                                            }
                                                                            else
                                                                            {
                                                                                Stream.unget();
                                                                                Stream.unget();
                                                                                continue;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            return AbortParsing();
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        Stream.unget();
                                                                        if (ParseText(Stream, m_Text))
                                                                        {
                                                                            if (GetNonTrivialToken(Stream, Token) && (Token == '<'))
                                                                            {
                                                                                if (GetToken(Stream, Token) && (Token == '/'))
                                                                                {
                                                                                    std::string ClosingName;
                                                                                    if (ParseIdentifier(Stream, ClosingName) && (ClosingName == m_Name))
                                                                                        if (GetNonTrivialToken(Stream, Token) && (Token == '>'))
                                                                                        {
                                                                                            return eFinishParsingNode;
                                                                                        }
                                                                                    return AbortParsing();
                                                                                }
                                                                                else
                                                                                {
                                                                                    Stream.unget();
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                return AbortParsing();
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            return AbortParsing();
                                                                        }
                                                                    }
                                                                }
                                                                while (true);
                                                                break;
                                                            default:   //Text Content
                                                                if (ParseText(Stream, m_Text))
                                                                {
                                                                    if (GetNonTrivialToken(Stream, Token) && (Token == '<'))
                                                                    {
                                                                        if (GetToken(Stream, Token) && (Token == '/'))
                                                                        {
                                                                            std::string ClosingName;
                                                                            if (ParseIdentifier(Stream, ClosingName) && (ClosingName == m_Name))
                                                                                if (GetNonTrivialToken(Stream, Token) && (Token == '>'))
                                                                                {
                                                                                    return eFinishParsingNode;
                                                                                }
                                                                            return AbortParsing();
                                                                        }
                                                                        else
                                                                        {
                                                                            Stream.unget();
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        return AbortParsing();
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    return AbortParsing();
                                                                }
                                                                break;
                                                        }
                                                    break;
                                                default:
                                                    if (IsAlpha(Token))
                                                    {
                                                        Stream.unget();
                                                        continue;
                                                    }
                                            }
                                        else
                                        {
                                            return AbortParsing();
                                        }
                                    }
                                    while (true);
                                }
                                else
                                {
                                    return AbortParsing();
                                }
                        }
            return AbortParsing();
        }

        bool CXmlNode::SetText(const std::string& Text)
        {
            if (count(Text.begin(), Text.end(), '<') != count(Text.begin(), Text.end(), '>'))
            {
                return false;
            }
            m_Text = Text;
            return true;
        }

        bool CXmlNode::SetAttribute(const std::string& AttributeName, const int64_t Value)
        {
            if (!AttributeName.length())
            {
                return false;
            }
            m_Attributes[AttributeName] = UtilityToString(Value);
            return true;
        }

        bool CXmlNode::SetAttribute(const std::string& AttributeName, const int Value)
        {
            if (!AttributeName.length())
            {
                return false;
            }
            m_Attributes[AttributeName] = UtilityToString(Value);
            return true;
        }

        bool CXmlNode::SetAttribute(const std::string& AttributeName, const bool Value)
        {
            if (!AttributeName.length())
            {
                return false;
            }
            m_Attributes[AttributeName] = Value ? std::string("true") : std::string("false");
            return true;
        }

        bool CXmlNode::SetAttribute(const std::string& AttributeName, const std::string& Value)
        {
            if (!AttributeName.length())
            {
                return false;
            }
            m_Attributes[AttributeName] = Value;
            return true;
        }

        bool CXmlNode::SetAttribute(const std::string& AttributeName, const Byte Value)
        {
            if (!AttributeName.length())
            {
                return false;
            }
            m_Attributes[AttributeName] = UtilityToString(Value);
            return true;
        }

        bool CXmlNode::SetAttribute(const std::string& AttributeName, const Real Value)
        {
            if (!AttributeName.length())
            {
                return false;
            }
            m_Attributes[AttributeName] = UtilityToString(Value);
            return true;
        }

        bool CXmlNode::SetAttribute(const std::string& AttributeName, const char* pString)
        {
            if (!AttributeName.length())
            {
                return false;
            }
            m_Attributes[AttributeName] = std::string(pString);
            return true;
        }

        void CXmlNode::Clear()
        {
            ClearAttributes();
            ClearSubNodes();
        }

        void CXmlNode::ClearAttributes()
        {
            m_Attributes.clear();
        }

        void CXmlNode::ClearSubNodes()
        {
            if (!m_SubNodes.size())
            {
                return;
            }
            for (auto& m_SubNode : m_SubNodes)
            {
                delete m_SubNode;
            }
            m_SubNodes.clear();
        }

        void CXmlNode::SetName(const std::string& Name)
        {
            m_Name = Name;
        }

        const std::string& CXmlNode::GetName() const
        {
            return m_Name;
        }

        bool CXmlNode::HasAttribute(const std::string& AttributeName) const
        {
            return AttributeName.length() && (m_Attributes.find(AttributeName) != m_Attributes.end());
        }

        const std::string& CXmlNode::GetText() const
        {
            return m_Text;
        }

        bool CXmlNode::GetAttribute(const std::string& AttributeName, int64_t& Value) const
        {
            if (!AttributeName.length())
            {
                return false;
            }
            std::map<std::string, std::string>::const_iterator pLocation = m_Attributes.find(AttributeName);
            if (pLocation == m_Attributes.end())
            {
                return false;
            }
            Value = atoll(pLocation->second.c_str());
            return true;
        }

        bool CXmlNode::GetAttribute(const std::string& AttributeName, int& Value) const
        {
            if (!AttributeName.length())
            {
                return false;
            }
            std::map<std::string, std::string>::const_iterator pLocation = m_Attributes.find(AttributeName);
            if (pLocation == m_Attributes.end())
            {
                return false;
            }
            Value = atoi(pLocation->second.c_str());
            return true;
        }

        bool CXmlNode::GetAttribute(const std::string& AttributeName, bool& Value) const
        {
            if (!AttributeName.length())
            {
                return false;
            }
            std::map<std::string, std::string>::const_iterator pLocation = m_Attributes.find(AttributeName);
            if (pLocation == m_Attributes.end())
            {
                return false;
            }
            if (pLocation->second.length() == 1)
            {
                if (IsDigit(pLocation->second[0]))
                {
                    Value = (atoi(pLocation->second.c_str()) ? true : false);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                std::string TextValue(pLocation->second.c_str());
                for (char& pElement : TextValue)
                {
                    pElement = toupper(pElement);
                }
                if (TextValue == "TRUE")
                {
                    Value = true;
                    return true;
                }
                else if (TextValue == "FALSE")
                {
                    Value = false;
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        bool CXmlNode::GetAttribute(const std::string& AttributeName, std::string& Value) const
        {
            if (!AttributeName.length())
            {
                return false;
            }
            std::map<std::string, std::string>::const_iterator pLocation = m_Attributes.find(AttributeName);
            if (pLocation == m_Attributes.end())
            {
                return false;
            }
            Value = pLocation->second;
            return true;
        }

        bool CXmlNode::GetAttribute(const std::string& AttributeName, Byte& Value) const
        {
            if (!AttributeName.length())
            {
                return false;
            }
            std::map<std::string, std::string>::const_iterator pLocation = m_Attributes.find(AttributeName);
            if (pLocation == m_Attributes.end())
            {
                return false;
            }
            const int NumericalValue = atoi(pLocation->second.c_str());
            if ((NumericalValue < 0) || (NumericalValue > 255))
            {
                return false;
            }
            Value = Byte(NumericalValue);
            return true;
        }

        bool CXmlNode::GetAttribute(const std::string& AttributeName, Real& Value) const
        {
            if (!AttributeName.length())
            {
                return false;
            }
            std::map<std::string, std::string>::const_iterator pLocation = m_Attributes.find(AttributeName);
            if (pLocation == m_Attributes.end())
            {
                return false;
            }
            Value = Real(atof(pLocation->second.c_str()));
            return true;
        }

        bool CXmlNode::AttributeEquals(const std::string& AttributeName, const int Value) const
        {
            if (!AttributeName.length())
            {
                return false;
            }
            std::map<std::string, std::string>::const_iterator pLocation = m_Attributes.find(AttributeName);
            if (pLocation == m_Attributes.end())
            {
                return false;
            }
            return (Value == int(atoi(pLocation->second.c_str())));
        }

        bool CXmlNode::AttributeEquals(const std::string& AttributeName, const std::string& Value) const
        {
            if (!AttributeName.length())
            {
                return false;
            }
            std::map<std::string, std::string>::const_iterator pLocation = m_Attributes.find(AttributeName);
            if (pLocation == m_Attributes.end())
            {
                return false;
            }
            return (Value == pLocation->second);
        }

        bool CXmlNode::SaveToFile(const std::string& FileName, const bool OverWrite) const
        {
            if ((!OverWrite) && Files::CFile::Exists(FileName))
            {
                return false;
            }
            return Files::COutFile::WriteStringToFile(FileName, ToString());
        }

        std::string CXmlNode::ToString() const
        {
            std::ostringstream OutputStream;
            ToString(OutputStream);
            return OutputStream.str();
        }

        void CXmlNode::ToString(std::ostringstream& OutputStream) const
        {
            OutputStream << "<" << m_Name;

            std::map<std::string, std::string>::const_iterator EndAttributes = m_Attributes.end();
            for (std::map<std::string, std::string>::const_iterator pAttribute = m_Attributes.begin() ; pAttribute != EndAttributes; ++pAttribute)
            {
                OutputStream << " " << pAttribute->first << "=\"" << pAttribute->second << "\"";
            }

            if (m_SubNodes.size() || m_Text.length())
            {
                OutputStream << ">";

                std::list<CXmlNode*>::const_iterator EndSubNodes = m_SubNodes.end();
                for (std::list<CXmlNode*>::const_iterator ppSubNode = m_SubNodes.begin() ; ppSubNode != EndSubNodes; ++ppSubNode)
                {
                    (*ppSubNode)->ToString(OutputStream);
                }

                if (m_Text.length())
                {
                    OutputStream << m_Text;
                }

                OutputStream << "</" << m_Name << ">";
            }
            else
            {
                OutputStream << "/>";
            }
        }

        int CXmlNode::GetTotalSubNodes() const
        {
            return m_SubNodes.size();
        }

        const std::list<CXmlNode*>& CXmlNode::GetSubNodes() const
        {
            return m_SubNodes;
        }

        CXmlNode* CXmlNode::GetFirstSubNode()
        {
            if (!m_SubNodes.size())
            {
                return nullptr;
            }
            return m_SubNodes.front();
        }

        const CXmlNode* CXmlNode::GetFirstSubNode() const
        {
            if (!m_SubNodes.size())
            {
                return nullptr;
            }
            return m_SubNodes.front();
        }

        std::string CXmlNode::UtilityToString(const Byte Value) const
        {
            s_OutputStream.str("");
            s_OutputStream << int(Value);
            return s_OutputStream.str();
        }

        std::string CXmlNode::UtilityToString(const Real Value) const
        {
            s_OutputStream.str("");
            s_OutputStream << Value;
            return s_OutputStream.str();
        }

        std::string CXmlNode::UtilityToString(const int64_t Value) const
        {
            s_OutputStream.str("");
            s_OutputStream << Value;
            return s_OutputStream.str();
        }

        std::string CXmlNode::UtilityToString(const int Value) const
        {
            s_OutputStream.str("");
            s_OutputStream << Value;
            return s_OutputStream.str();
        }

        std::string CXmlNode::UtilityToString(const bool Value) const
        {
            return Value ? std::string("true") : std::string("false");
        }
    }
}
