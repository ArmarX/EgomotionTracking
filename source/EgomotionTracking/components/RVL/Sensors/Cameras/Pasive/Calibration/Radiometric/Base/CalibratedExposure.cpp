/*
 * CalibratedExposure.cpp
 */

#include "CalibratedExposure.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Base
                        {
                            const std::list<CCalibratedExposure> CCalibratedExposure::MergeExposureSets(const std::list<CCalibratedExposure>& Lhs, const std::list<CCalibratedExposure>& rhs)
                            {
                                std::list<CCalibratedExposure> MergedExposureSets;
                                MergedExposureSets.insert(MergedExposureSets.end(), Lhs.begin(), Lhs.end());
                                MergedExposureSets.insert(MergedExposureSets.end(), rhs.begin(), rhs.end());
                                MergedExposureSets.sort(CExposure::SortExposuresByParametricIndex);
                                MergedExposureSets.unique(CExposure::EqualsExposuresByParametricIndex);
                                return MergedExposureSets;
                            }

                            CCalibratedExposure::CCalibratedExposure(const int ParametricIndex, const Real MaximalSamplingDensity, const Real ScopeMinimalRadiance, const Real ScopeMaximalRadiance, const Real ScopeMinimalExposureTime, const Real CentralExposureTime, const Real ScopeMaximalExposureTime) :
                                CExposure(CentralExposureTime, ParametricIndex),
                                m_MaximalSamplingDensity(MaximalSamplingDensity),
                                m_ScopeMinimalRadiance(ScopeMinimalRadiance),
                                m_ScopeMaximalRadiance(ScopeMaximalRadiance),
                                m_ScopeMinimalExposureTime(ScopeMinimalExposureTime),
                                m_ScopeMaximalExposureTime(ScopeMaximalExposureTime)
                            {
                            }

                            CCalibratedExposure::~CCalibratedExposure()
                                = default;

                            Real CCalibratedExposure::GetMaximalSamplingDensity() const
                            {
                                return m_MaximalSamplingDensity;
                            }

                            Real CCalibratedExposure::GetScopeMinimalRadiance() const
                            {
                                return m_ScopeMinimalRadiance;
                            }

                            Real CCalibratedExposure::GetScopeMaximalRadiance() const
                            {
                                return m_ScopeMaximalRadiance;
                            }

                            Real CCalibratedExposure::GetScopeRangeRadiance() const
                            {
                                return m_ScopeMaximalRadiance - m_ScopeMinimalRadiance;
                            }

                            Real CCalibratedExposure::GetScopeMinimalExposureTime() const
                            {
                                return m_ScopeMinimalExposureTime;
                            }

                            Real CCalibratedExposure::GetScopeMaximalExposureTime() const
                            {
                                return m_ScopeMaximalExposureTime;
                            }

                            Real CCalibratedExposure::GetScopeRangeExposureTime() const
                            {
                                return m_ScopeMaximalExposureTime - m_ScopeMinimalExposureTime;
                            }

                            Real CCalibratedExposure::GetTimeDeltaToCentralExposureTime(const Real Time) const
                            {
                                return Time - m_Time;
                            }

                            Real CCalibratedExposure::GetAbsoluteTimeDeltaToCentralExposureTime(const Real Time) const
                            {
                                return std::abs(Time - m_Time);
                            }

                            bool CCalibratedExposure::IsContainedExposureTimeScope(const Real Time) const
                            {
                                return (Time >= m_ScopeMinimalExposureTime) && (Time <= m_ScopeMaximalExposureTime);
                            }

                            bool CCalibratedExposure::IsContainedExposureTimeScope(const Real T0, const Real T1) const
                            {
                                return (T0 <= m_ScopeMaximalExposureTime) && (T1 >= m_ScopeMinimalExposureTime);
                            }

                            bool CCalibratedExposure::IsContainedExposureRadianceScope(const Real Radiance) const
                            {
                                return (Radiance >= m_ScopeMinimalRadiance) && (Radiance <= m_ScopeMaximalRadiance);
                            }

                            bool CCalibratedExposure::HasCommonTemporalScope(const Real T0, const Real T1) const
                            {
                                return (m_ScopeMinimalExposureTime < T1) && (m_ScopeMaximalExposureTime > T0);
                            }

                            bool CCalibratedExposure::HasCommonTemporalScope(const CCalibratedExposure& CalibratedExposure) const
                            {
                                return (m_ScopeMinimalExposureTime < CalibratedExposure.m_ScopeMaximalExposureTime) && (m_ScopeMaximalExposureTime > CalibratedExposure.m_ScopeMinimalExposureTime);
                            }

                            Real CCalibratedExposure::GetCommonTemporalScope(const CCalibratedExposure& CalibratedExposure) const
                            {
                                if ((m_ScopeMinimalExposureTime < CalibratedExposure.m_ScopeMaximalExposureTime) && (m_ScopeMaximalExposureTime > CalibratedExposure.m_ScopeMinimalExposureTime))
                                {
                                    return std::min(m_ScopeMaximalExposureTime, CalibratedExposure.m_ScopeMaximalExposureTime) - std::max(m_ScopeMinimalExposureTime, CalibratedExposure.m_ScopeMinimalExposureTime);
                                }
                                return Real(0);
                            }

                            bool CCalibratedExposure::HasCommonRadianceScope(const CCalibratedExposure& CalibratedExposure) const
                            {
                                return (m_ScopeMinimalRadiance < CalibratedExposure.m_ScopeMaximalRadiance) && (m_ScopeMaximalRadiance > CalibratedExposure.m_ScopeMinimalRadiance);
                            }

                            Real CCalibratedExposure::GetCommonRadianceScope(const CCalibratedExposure& CalibratedExposure) const
                            {
                                if ((m_ScopeMinimalRadiance < CalibratedExposure.m_ScopeMaximalRadiance) && (m_ScopeMaximalRadiance > CalibratedExposure.m_ScopeMinimalExposureTime))
                                {
                                    return std::min(m_ScopeMaximalRadiance, CalibratedExposure.m_ScopeMaximalRadiance) - std::max(m_ScopeMinimalRadiance, CalibratedExposure.m_ScopeMinimalRadiance);
                                }
                                return Real(0);
                            }

                            void CCalibratedExposure::operator=(const CCalibratedExposure& CalibratedExposure)
                            {
                                m_Time = CalibratedExposure.m_Time;
                                m_ParametricIndex = CalibratedExposure.m_ParametricIndex;
                                m_MaximalSamplingDensity = CalibratedExposure.m_MaximalSamplingDensity;
                                m_ScopeMinimalRadiance = CalibratedExposure.m_ScopeMinimalRadiance;
                                m_ScopeMaximalRadiance = CalibratedExposure.m_ScopeMaximalRadiance;
                                m_ScopeMinimalExposureTime = CalibratedExposure.m_ScopeMinimalExposureTime;
                                m_ScopeMaximalExposureTime = CalibratedExposure.m_ScopeMaximalExposureTime;
                            }
                        }
                    }
                }
            }
        }
    }
}
