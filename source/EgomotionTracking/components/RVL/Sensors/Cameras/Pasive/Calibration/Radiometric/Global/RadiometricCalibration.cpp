/*
 * RadiometricCalibration.cpp
 */

#include "RadiometricCalibration.h"
#include "RadiometricResponceFunction.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            CRadiometricCalibration::CRadiometricCalibration(const uint64_t CameraId, const Imaging::TImage<Real>* pSourceImage, const Imaging::CBayerPattern::BayerPatternType BayerPattern, const Imaging::CBayerPattern::Channel ChannelContent) :
                                m_CameraId(CameraId),
                                m_BayerPattern(BayerPattern),
                                m_ChannelContent(ChannelContent),
                                m_pSourceImage(pSourceImage),
                                m_pRadiometricImage(nullptr),
                                m_pRadiometricResponceFunction(nullptr)
                            {
                                if (m_pSourceImage && m_pSourceImage->IsValid())
                                {
                                    Imaging::CBayerPattern MapLoader;
                                    if (MapLoader.SetType(m_BayerPattern))
                                    {
                                        Imaging::CBayerPattern::ChannelFlagMap FlagMap = MapLoader.GetBayerPatternMapByChannelContent(m_ChannelContent);
                                        const int Width = m_pSourceImage->GetWidth();
                                        const int Height = m_pSourceImage->GetHeight();
                                        m_pRadiometricImage = new Imaging::TImage<CRadiometricPixel>(Width, Height);
                                        const Real* pSourcePixel = m_pSourceImage->GetReadOnlyBuffer();
                                        CRadiometricPixel* pRadiometricPixel = m_pRadiometricImage->GetWritableBuffer();
                                        for (int Y = 0 ; Y < Height ; ++Y)
                                            for (int X = 0 ; X < Width ; ++X, ++pRadiometricPixel, ++pSourcePixel)
                                                if (FlagMap.m_YX[Y & 0X1][X & 0X1])
                                                {
                                                    m_RadiometricPixels.push_back(pRadiometricPixel->Initialize(X, Y, pSourcePixel));
                                                }
                                        m_pRadiometricResponceFunction = new CRadiometricResponceFunction(m_CameraId, m_BayerPattern, m_ChannelContent);
                                    }
                                }
                            }

                            CRadiometricCalibration::CRadiometricCalibration(const uint64_t CameraId, const Imaging::TImage<Real>* pSourceImage) :
                                m_CameraId(CameraId),
                                m_BayerPattern(Imaging::CBayerPattern::BayerPatternType(-1)),
                                m_ChannelContent(Imaging::CBayerPattern::Channel(-1)),
                                m_pSourceImage(pSourceImage),
                                m_pRadiometricImage(nullptr),
                                m_pRadiometricResponceFunction(nullptr)
                            {
                                if (m_pSourceImage && m_pSourceImage->IsValid())
                                {
                                    const int Width = m_pSourceImage->GetWidth();
                                    const int Height = m_pSourceImage->GetHeight();
                                    m_pRadiometricImage = new Imaging::TImage<CRadiometricPixel>(Width, Height);
                                    const Real* pSourcePixel = m_pSourceImage->GetReadOnlyBuffer();
                                    CRadiometricPixel* pRadiometricPixel = m_pRadiometricImage->GetWritableBuffer();
                                    for (int Y = 0 ; Y < Height ; ++Y)
                                        for (int X = 0 ; X < Width ; ++X, ++pRadiometricPixel, ++pSourcePixel)
                                        {
                                            m_RadiometricPixels.push_back(pRadiometricPixel->Initialize(X, Y, pSourcePixel));
                                        }
                                    m_pRadiometricResponceFunction = new CRadiometricResponceFunction();
                                }
                            }

                            CRadiometricCalibration::~CRadiometricCalibration()
                            {
                                delete m_pRadiometricImage;
                                delete m_pRadiometricResponceFunction;
                            }

                            bool CRadiometricCalibration::IsEnabled() const
                            {
                                return m_RadiometricPixels.size();
                            }

                            uint64_t CRadiometricCalibration::GetCameraId() const
                            {
                                return m_CameraId;
                            }

                            Imaging::CBayerPattern::BayerPatternType CRadiometricCalibration::GetBayerPattern() const
                            {
                                return m_BayerPattern;
                            }

                            Imaging::CBayerPattern::Channel CRadiometricCalibration::GetChannelContent() const
                            {
                                return m_ChannelContent;
                            }

                            bool CRadiometricCalibration::AddExposure(const Real ExposureTime)
                            {
                                if (m_RadiometricPixels.size())
                                {
                                    std::list<CRadiometricPixel*>::const_iterator RadiometricCellsEnd = m_RadiometricPixels.end();
                                    for (std::list<CRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin() ; ppRadiometricCell != RadiometricCellsEnd ; ++ppRadiometricCell)
                                    {
                                        (*ppRadiometricCell)->AddExposure();
                                    }
                                    m_Exposures.push_back(Base::CExposure(ExposureTime));
                                    return true;
                                }
                                return false;
                            }

                            bool CRadiometricCalibration::ClearExposures()
                            {
                                if (m_Exposures.size())
                                {
                                    std::list<CRadiometricPixel*>::const_iterator RadiometricCellsEnd = m_RadiometricPixels.end();
                                    for (std::list<CRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin() ; ppRadiometricCell != RadiometricCellsEnd ; ++ppRadiometricCell)
                                    {
                                        (*ppRadiometricCell)->ClearExposures();
                                    }
                                    m_Exposures.clear();
                                    return true;
                                }
                                return false;
                            }

                            int CRadiometricCalibration::GetTotalExposures() const
                            {
                                return m_Exposures.size();
                            }

                            bool CRadiometricCalibration::SetGaussianBoxKernelRadius(const Real GaussianBoxKernelRadius)
                            {
                                return m_RadianceWeigthingKernel.SetGaussianBoxKernelRadius(GaussianBoxKernelRadius);
                            }

                            bool CRadiometricCalibration::SetCommonGaussianKernelStandardDeviation(const Real StandardDeviation)
                            {
                                return m_RadianceWeigthingKernel.SetCommonGaussianKernelStandardDeviation(StandardDeviation);
                            }

                            bool CRadiometricCalibration::SetCommonGaussianKernelCutOff(const Real CutOffDensity, const Real CutOffNormalizedDeviation)
                            {
                                return m_RadianceWeigthingKernel.SetCommonGaussianKernelCutOff(CutOffDensity, CutOffNormalizedDeviation);
                            }

                            Real CRadiometricCalibration::GetGaussianBoxKernelRadius() const
                            {
                                return m_RadianceWeigthingKernel.GetGaussianBoxKernelRadius();
                            }

                            Real CRadiometricCalibration::GetCommonGaussianKernelStandardDeviation() const
                            {
                                return m_RadianceWeigthingKernel.GetCommonGaussianKernelStandardDeviation();
                            }

                            bool CRadiometricCalibration::Calibrate(const CRadiometricResponceFunction::PixelSelectionCriterion Criterion, const Real DominaceRadius, const std::list<CRadiometricPixel*>& SelectedPixels, const Real SmoothingLambda, const Base::CRadianceWeigthingKernel::KernelType Kernel)
                            {
                                const int TotalExposures = m_Exposures.size();
                                if (TotalExposures >= _RADIOMETRIC_CALIBRATION_MINIMAL_EXPOSURES_)
                                {
                                    const int TotalSelectedPixels = SelectedPixels.size();
                                    if (TotalSelectedPixels >= _RADIOMETRIC_CALIBRATION_MINIMAL_SELECTED_SAMPLING_CELLS_)
                                    {
                                        std::list<CRadiometricPixel*>::const_iterator ppRadiometricPixel = SelectedPixels.begin();
                                        const int TotalRows = TotalSelectedPixels * TotalExposures + 256 + 1;
                                        const int TotalCols = TotalSelectedPixels + 256;
                                        Mathematics::_ND::CMatrixND A = Mathematics::_ND::CreateZeroMatrix(TotalRows, TotalCols);
                                        Mathematics::_ND::CMatrixND B = Mathematics::_ND::CreateZeroMatrix(TotalRows, 1);
                                        int Row = 0;
                                        for (int i = 0, ip = 255 ; i < TotalSelectedPixels ; ++i, ++ip, ++ppRadiometricPixel)
                                        {
                                            std::list<Base::CExposure>::const_iterator pExposure = m_Exposures.begin();
                                            std::list<Real>::const_iterator pValue = (*ppRadiometricPixel)->GetValues().begin();
                                            for (int j = 0 ; j < TotalExposures ; ++j, ++pExposure, ++pValue, ++Row)
                                            {
                                                const Real Wij = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(*pValue, Kernel);
                                                A(Row, int(std::round(*pValue))) = Mathematics::_ND::MatrixReal(Wij);
                                                A(Row, ip) = Mathematics::_ND::MatrixReal(-Wij);
                                                B(Row, 0) = Mathematics::_ND::MatrixReal(Wij * std::log(pExposure->GetTime()));
                                            }
                                        }
                                        A(Row++, 128) = Mathematics::_ND::MatrixReal(1);
                                        for (int i = 0 ; i < 254 ; ++i, ++Row)
                                        {
                                            const Real Wij = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(Real(i), Kernel);
                                            A(Row, i) = Mathematics::_ND::MatrixReal(SmoothingLambda * Wij);
                                            A(Row, i + 1) = Mathematics::_ND::MatrixReal(Real(-2) * SmoothingLambda * Wij);
                                            A(Row, i + 2) = Mathematics::_ND::MatrixReal(SmoothingLambda * Wij);
                                        }
                                        //try
                                        //{
                                        Mathematics::_ND::CMatrixND X = Mathematics::_ND::PseudoInverseSVD(A) * B;
                                        Real ResponseFunction[256] = { Real(0) };
                                        Real RegressionWeightingFunction[256] = { Real(0) };
                                        for (int i = 0 ; i < 256 ; ++i)
                                        {
                                            ResponseFunction[i] = Real(X(i, 0));
                                            RegressionWeightingFunction[i] = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(Real(i), Kernel);
                                        }
                                        if (m_pRadiometricResponceFunction->LoadResponceFunctionFromParameters(m_CameraId, m_BayerPattern, m_ChannelContent, TotalExposures, TotalSelectedPixels, DominaceRadius, SmoothingLambda, Criterion, ResponseFunction, RegressionWeightingFunction, &m_RadianceWeigthingKernel))
                                        {
                                            return m_pRadiometricResponceFunction->EstimateIdealResponceFunction(Kernel);
                                        }
                                        //}
                                        //catch(Mathematics::_ND::CMatrixBaseException& E)
                                        //{
                                        //  std::ostringstream OutputText;
                                        //  OutputText << E.what() << "\n";
                                        //  _RVL_CONSOLE_OUTPUT_EXECEPTIONS_(OutputText.str(),0);
                                        //}
                                    }
                                }
                                return false;
                            }

                            bool CRadiometricCalibration::Calibrate(const CRadiometricResponceFunction::PixelSelectionCriterion Criterion, const Real DominaceRadius, const int MaximalSamples, const Real SmoothingLambda, const Base::CRadianceWeigthingKernel::KernelType Kernel)
                            {
                                if (m_Exposures.size() >= _RADIOMETRIC_CALIBRATION_MINIMAL_EXPOSURES_)
                                {
                                    return Calibrate(Criterion, DominaceRadius, SelectPixelsByCriterion(Criterion, DominaceRadius, std::max(MaximalSamples, _RADIOMETRIC_CALIBRATION_MINIMAL_SELECTED_SAMPLING_CELLS_)), SmoothingLambda, Kernel);
                                }
                                return false;
                            }

                            const std::list<CRadiometricPixel*> CRadiometricCalibration::SelectPixelsByCriterion(const CRadiometricResponceFunction::PixelSelectionCriterion Criterion, const Real DominaceRadius, const int MaximalSamples)
                            {
                                std::list<CRadiometricPixel*> SelectedPixels;
                                switch (Criterion)
                                {
                                    case CRadiometricResponceFunction::eUnknownPixelSelectionCriterion:
                                    case CRadiometricResponceFunction::eMaximalStandardDeviation:
                                    case CRadiometricResponceFunction::eMinimalStandardDeviation:
                                        for (std::list<CRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin() ; ppRadiometricCell != m_RadiometricPixels.end() ; ++ppRadiometricCell)
                                        {
                                            (*ppRadiometricCell)->CalculateStandardDeviationCriterion();
                                        }
                                        break;
                                    case CRadiometricResponceFunction::eMaximalRange:
                                    case CRadiometricResponceFunction::eMinimalRange:
                                        for (std::list<CRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin() ; ppRadiometricCell != m_RadiometricPixels.end() ; ++ppRadiometricCell)
                                        {
                                            (*ppRadiometricCell)->CalculateRangeCriterion();
                                        }
                                        break;
                                }
                                switch (Criterion)
                                {
                                    case CRadiometricResponceFunction::eUnknownPixelSelectionCriterion:
                                    case CRadiometricResponceFunction::eMaximalStandardDeviation:
                                    case CRadiometricResponceFunction::eMaximalRange:
                                        m_RadiometricPixels.sort(SortRadiometricPixelsByCriterionDescent);
                                        break;
                                    case CRadiometricResponceFunction::eMinimalStandardDeviation:
                                    case CRadiometricResponceFunction::eMinimalRange:
                                        m_RadiometricPixels.sort(SortRadiometricPixelsByCriterionAscent);
                                        break;
                                }
                                const std::list<std::pair<int, int> > LocationDeltas = ComputeLocationDeltas(DominaceRadius);
                                const int Width = m_pRadiometricImage->GetWidth();
                                const int Height = m_pRadiometricImage->GetHeight();
                                for (std::list<CRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin() ; ppRadiometricCell != m_RadiometricPixels.end() ; ++ppRadiometricCell)
                                    if ((*ppRadiometricCell)->IsActive())
                                    {
                                        SelectedPixels.push_back(*ppRadiometricCell);
                                        if (int(SelectedPixels.size()) <= MaximalSamples)
                                        {
                                            const std::pair<int, int> Location((*ppRadiometricCell)->GetX(), (*ppRadiometricCell)->GetY());
                                            for (const auto& LocationDelta : LocationDeltas)
                                            {
                                                const std::pair<int, int> DeactivatingLocation(Location.first + LocationDelta.first, Location.second + LocationDelta.second);
                                                if ((DeactivatingLocation.first >= 0) && (DeactivatingLocation.second >= 0) && (DeactivatingLocation.first < Width) && (DeactivatingLocation.second < Height))
                                                {
                                                    m_pRadiometricImage->GetWritableBufferAt(DeactivatingLocation.first, DeactivatingLocation.second)->SetActive(false);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                return SelectedPixels;
                            }

                            const CRadiometricResponceFunction* CRadiometricCalibration::GetRadiometricResponceFunction() const
                            {
                                return m_pRadiometricResponceFunction;
                            }

                            std::list<std::pair<int, int> > CRadiometricCalibration::ComputeLocationDeltas(const Real DominaceRadius)
                            {
                                std::list<std::pair<int, int> > DeltaLocations;
                                const int DiscreteRadius = std::max(int(std::ceil(DominaceRadius)), 1);
                                for (int Y = -DiscreteRadius ; Y <= DiscreteRadius ; ++Y)
                                    for (int X = -DiscreteRadius ; X <= DiscreteRadius ; ++X)
                                        if (Real(std::round(hypot(X, Y))) <= DominaceRadius)
                                        {
                                            DeltaLocations.push_back(std::pair<int, int>(X, Y));
                                        }
                                return DeltaLocations;
                            }

                            bool CRadiometricCalibration::SortRadiometricPixelsByCriterionAscent(const CRadiometricPixel* plhs, const CRadiometricPixel* rlhs)
                            {
                                return plhs->GetCriterion() < rlhs->GetCriterion();
                            }

                            bool CRadiometricCalibration::SortRadiometricPixelsByCriterionDescent(const CRadiometricPixel* plhs, const CRadiometricPixel* rlhs)
                            {
                                return plhs->GetCriterion() > rlhs->GetCriterion();
                            }
                        }
                    }
                }
            }
        }
    }
}
