/*
 * Exposure.cpp
 */

#include "Exposure.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Base
                        {
                            std::list<CExposure> CExposure::MergeExposureSets(const std::list<CExposure>& Lhs, const std::list<CExposure>& rhs)
                            {
                                std::list<CExposure> MergedExposureSets;
                                MergedExposureSets.insert(MergedExposureSets.end(), Lhs.begin(), Lhs.end());
                                MergedExposureSets.insert(MergedExposureSets.end(), rhs.begin(), rhs.end());
                                MergedExposureSets.sort(CExposure::SortExposuresByParametricIndex);
                                MergedExposureSets.unique(CExposure::EqualsExposuresByParametricIndex);
                                return MergedExposureSets;
                            }

                            bool CExposure::ExportExposureSetToFile(const std::list<CExposure>& ExposureSet, const std::string& FileName)
                            {
                                if (FileName.length() && ExposureSet.size())
                                {
                                    std::ostringstream Content;
                                    Content.precision(g_RealDisplayDigits);
                                    std::list<CExposure>::const_iterator EndExposures = ExposureSet.end();
                                    for (std::list<CExposure>::const_iterator pExposure = ExposureSet.begin() ; pExposure != EndExposures ; ++pExposure)
                                    {
                                        Content << pExposure->GetParametricIndex() << "\t" << pExposure->GetTime() << "\n";
                                    }
                                    return Files::COutFile::WriteStringToFile(FileName, Content.str());
                                }
                                return false;
                            }

                            bool CExposure::EqualsExposuresByParametricIndex(const CExposure& lhs, const CExposure& rhs)
                            {
                                return (lhs.m_ParametricIndex == rhs.m_ParametricIndex);
                            }

                            bool CExposure::SortExposuresByParametricIndex(const CExposure& lhs, const CExposure& rhs)
                            {
                                return lhs.m_ParametricIndex < rhs.m_ParametricIndex;
                            }

                            CExposure::CExposure(const Real Time, const int ParametricIndex) :
                                m_Time(Time),
                                m_ParametricIndex(ParametricIndex)
                            {
                            }

                            CExposure::CExposure() :
                                m_Time(Real(0)),
                                m_ParametricIndex(-1)
                            {
                            }

                            CExposure::~CExposure()
                                = default;

                            Real CExposure::GetTime() const
                            {
                                return m_Time;
                            }

                            bool CExposure::HasParametricIndex() const
                            {
                                return (m_ParametricIndex >= 0);
                            }

                            int CExposure::GetParametricIndex() const
                            {
                                return m_ParametricIndex;
                            }
                        }
                    }
                }
            }
        }
    }
}
