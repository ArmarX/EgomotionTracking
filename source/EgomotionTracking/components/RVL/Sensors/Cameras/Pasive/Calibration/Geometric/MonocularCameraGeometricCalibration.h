/*
 * MonocularCameraGeometricCalibration.h
 */

#pragma once

#define _CAMERA_GEOMETRIC_CALIBRATION_TOTAL_DISTORTION_COEFFICIENTS_ 4

#include "../../../../../Common/DataTypes.h"
#include "../../../../../Mathematics/_2D/Vector2D.h"
#include "../../../../../Mathematics/_3D/Vector3D.h"
#include "../../../../../Mathematics/_3D/Matrix3D.h"
#include "../../../../../Imaging/ImageSize.h"
#include "../../../../../Files/InFile.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Geometric
                    {
                        class CMonocularCameraGeometricCalibration
                        {
                        public:

                            enum OpticalAberrationMode
                            {
                                eDistortedImage, eUndistortedImage
                            };

                            CMonocularCameraGeometricCalibration();
                            CMonocularCameraGeometricCalibration(const CMonocularCameraGeometricCalibration* pMonocularCameraGeometricCalibration);
                            virtual ~CMonocularCameraGeometricCalibration();

                            bool SetMinimalObjectDistance(const Real MinimalObjectDistance);
                            virtual bool LoadFromFile(const std::string& FileName, const int TargetCalibrationIndex, const bool ResetExtrinsic);
                            void SetExtrinsic(const Mathematics::_3D::CMatrix3D& R, const Mathematics::_3D::CVector3D& Translation);
                            void operator=(const CMonocularCameraGeometricCalibration& CameraGeometricCalibration);

                            bool IsLoaded() const;

                            void MapPointFromSpaceToCameraFrame(const Mathematics::_3D::CVector3D& SpacePoint, Mathematics::_3D::CVector3D& CameraPoint) const;
                            void MapPointFromCameraFrameToSpace(const Mathematics::_3D::CVector3D& CameraPoint, Mathematics::_3D::CVector3D& SpacePoint) const;
                            bool MapPointFromImageToCameraFrame(const Mathematics::_2D::CVector2D& ImagePoint, const Real ZDepth, const OpticalAberrationMode SourceImagePointMode, Mathematics::_3D::CVector3D& CameraPoint) const;
                            bool MapPointFromSpaceToImage(const Mathematics::_3D::CVector3D& SpacePoint, const OpticalAberrationMode TargetImagePointMode, Mathematics::_2D::CVector2D& ImagePoint) const;
                            bool MapPointFromImageToSpace(const Mathematics::_2D::CVector2D& ImagePoint, const Real ZDepth, const OpticalAberrationMode SourceImagePointMode, Mathematics::_3D::CVector3D& SpacePoint) const;

                            Real GetDistanceToPrincipalPoint(const Mathematics::_2D::CVector2D& ImagePoint, const OpticalAberrationMode SourceImagePointMode) const;
                            Real GetMinimalObjectDistance() const;
                            const Imaging::CImageSize& GetImageSize() const;
                            int GetImageWidth() const;
                            int GetImageHeight() const;
                            const Mathematics::_2D::CVector2D& GetFocalLength() const;
                            Real GetFocalLengthX() const;
                            Real GetFocalLengthY() const;
                            const Real* GetDistortionCoefficients() const;
                            const Mathematics::_2D::CVector2D& GetImagePrincipalPoint() const;
                            Real GetImagePrincipalPointX() const;
                            Real GetImagePrincipalPointY() const;
                            const Mathematics::_3D::CVector3D& GetTranslation() const;
                            const Mathematics::_3D::CMatrix3D& GetRotation() const;
                            const Mathematics::_3D::CVector3D& GetTranslationInverse() const;
                            const Mathematics::_3D::CMatrix3D& GetRotationInverse() const;
                            Mathematics::_3D::CMatrix3D GetCalibrationMatrix() const;
                            virtual void Distortion(const Mathematics::_2D::CVector2D& UndistortedImagePoint, Mathematics::_2D::CVector2D& DistortedImagePoint) const;
                            virtual void Undistortion(const Mathematics::_2D::CVector2D& DistortedImagePoint, Mathematics::_2D::CVector2D& UndistortedImagePoint) const;
                            bool operator==(const CMonocularCameraGeometricCalibration& MonocularCameraGeometricCalibration);

                        protected:

                            Imaging::CImageSize m_ImageSize;
                            Mathematics::_2D::CVector2D m_FocalLength;
                            Mathematics::_2D::CVector2D m_ImagePrincipalPoint;
                            Mathematics::_3D::CVector3D m_Translation;
                            Mathematics::_3D::CMatrix3D m_Rotation;
                            Mathematics::_3D::CVector3D m_TranslationInverse;
                            Mathematics::_3D::CMatrix3D m_RotationInverse;
                            Real m_MinimalObjectDistance;
                            Real m_DistortionCoefficients[_CAMERA_GEOMETRIC_CALIBRATION_TOTAL_DISTORTION_COEFFICIENTS_];
                        };
                    }
                }
            }
        }
    }
}

