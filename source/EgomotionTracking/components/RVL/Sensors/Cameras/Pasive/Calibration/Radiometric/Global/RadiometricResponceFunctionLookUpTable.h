/*
 * RadiometricResponceFunctionLookUpTable.h
 */

#pragma once

#include "../../../../../../Imaging/TImage.hpp"
#include "../Base/Exposure.h"
#include "../Base/RadianceWeigthingKernel.h"
#include "RadiometricResponceFunction.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            class CRadiometricResponceFunctionLookUpTable
                            {
                            public:

                                CRadiometricResponceFunctionLookUpTable();
                                virtual ~CRadiometricResponceFunctionLookUpTable();

                                bool LoadLookUpTable(const CRadiometricResponceFunction* pResponceFunction, const std::vector<Base::CExposure>& Exposures, const int SamplesPerUnit, const Base::CRadianceWeigthingKernel::KernelType Kernel);

                                bool IsEnabled() const;
                                const CRadiometricResponceFunction* GetResponceFunction() const;
                                const Imaging::TImage<Real>* GetLookUpTable() const;
                                const Real* GetLookUpTableByExposure(const Base::CExposure& Exposure) const;
                                const Real* GetWeigthingKernel() const;
                                int GetSamplesPerUnit() const;
                                const std::vector<Base::CExposure>& GetExposures() const;

                            protected:

                                int LoadExposures(const std::vector<Base::CExposure>& Exposures);

                                const CRadiometricResponceFunction* m_pResponceFunction;
                                Imaging::TImage<Real>* m_pLookUpTable;
                                Real* m_pWeigthingKernel;
                                int* m_pParametricIndices;
                                int m_SamplesPerUnit;
                                std::vector<Base::CExposure> m_Exposures;
                            };
                        }
                    }
                }
            }
        }
    }
}

