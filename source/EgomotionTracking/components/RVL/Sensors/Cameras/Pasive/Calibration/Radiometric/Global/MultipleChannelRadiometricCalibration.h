/*
 * MultipleChannelRadiometricCalibration.h
 */

#pragma once

#include "../../../../../../Imaging/BayerPattern.h"
#include "../../../../../../Imaging/TImage.hpp"
#include "MultipleChannelRadiometricResponceFunction.h"
#include "RadiometricCalibration.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            class CMultipleChannelRadiometricCalibration
                            {
                            public:

                                CMultipleChannelRadiometricCalibration(const int64_t CameraId, const Imaging::TImage<Real>* pSourceImage, const Imaging::CBayerPattern::BayerPatternType BayerPattern);
                                virtual ~CMultipleChannelRadiometricCalibration();

                                bool IsEnabled() const;
                                int64_t GetCameraId() const;
                                Imaging::CBayerPattern::BayerPatternType GetBayerPattern() const;

                                bool AddExposure(const Real ExposureTime);
                                bool ClearExposures();
                                int GetTotalExposures() const;

                                bool SetGaussianBoxKernelRadius(const Real GaussianBoxKernelRadius);
                                bool SetCommonGaussianKernelStandardDeviation(const Real StandardDeviation);
                                bool SetCommonGaussianKernelCutOff(const Real CutOffDensity, const Real CutOffNormalizedDeviation);

                                Real GetGaussianBoxKernelRadius() const;
                                Real GetCommonGaussianKernelStandardDeviation() const;

                                bool Calibrate(const CRadiometricResponceFunction::PixelSelectionCriterion Criterion, const Real DominaceRadius, const int MaximalSamples, const Real SmoothingLambda, const Base::CRadianceWeigthingKernel::KernelType Kernel);
                                bool SaveResponceFunctionToFile(const std::string& pPathFileName, const Files::CFile::FileMode Mode) const;

                                const CRadiometricCalibration* GetRedChannelCalibration() const;
                                const CRadiometricCalibration* GetGreenChannelCalibration() const;
                                const CRadiometricCalibration* GetBlueChannelCalibration() const;
                                const CMultipleChannelRadiometricResponceFunction* GetMultipleChannelRadiometricResponceFunction() const;

                            protected:

                                CRadiometricCalibration* m_pRedChannelCalibration;
                                CRadiometricCalibration* m_pGreenChannelCalibration;
                                CRadiometricCalibration* m_pBlueChannelCalibration;
                                CMultipleChannelRadiometricResponceFunction* m_pMultipleChannelRadiometricResponceFunction;
                            };
                        }
                    }
                }
            }
        }
    }
}

