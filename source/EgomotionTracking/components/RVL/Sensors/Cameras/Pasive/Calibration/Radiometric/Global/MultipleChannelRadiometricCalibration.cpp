/*
 * MultipleChannelRadiometricCalibration.cpp
 */

#include "MultipleChannelRadiometricCalibration.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            CMultipleChannelRadiometricCalibration::CMultipleChannelRadiometricCalibration(const int64_t CameraId, const Imaging::TImage<Real>* pSourceImage, const Imaging::CBayerPattern::BayerPatternType BayerPattern) :
                                m_pRedChannelCalibration(nullptr),
                                m_pGreenChannelCalibration(nullptr),
                                m_pBlueChannelCalibration(nullptr),
                                m_pMultipleChannelRadiometricResponceFunction(nullptr)
                            {
                                m_pRedChannelCalibration = new CRadiometricCalibration(CameraId, pSourceImage, BayerPattern, Imaging::CBayerPattern::eRed);
                                m_pGreenChannelCalibration = new CRadiometricCalibration(CameraId, pSourceImage, BayerPattern, Imaging::CBayerPattern::eGreen);
                                m_pBlueChannelCalibration = new CRadiometricCalibration(CameraId, pSourceImage, BayerPattern, Imaging::CBayerPattern::eBlue);
                                m_pMultipleChannelRadiometricResponceFunction = new CMultipleChannelRadiometricResponceFunction();
                                m_pMultipleChannelRadiometricResponceFunction->LoadResponceFunctions(m_pRedChannelCalibration->GetRadiometricResponceFunction(), m_pGreenChannelCalibration->GetRadiometricResponceFunction(), m_pBlueChannelCalibration->GetRadiometricResponceFunction(), false);
                            }

                            CMultipleChannelRadiometricCalibration::~CMultipleChannelRadiometricCalibration()
                            {
                                delete m_pRedChannelCalibration;
                                delete m_pGreenChannelCalibration;
                                delete m_pBlueChannelCalibration;
                                delete m_pMultipleChannelRadiometricResponceFunction;
                            }

                            bool CMultipleChannelRadiometricCalibration::IsEnabled() const
                            {
                                return m_pRedChannelCalibration->IsEnabled() && m_pGreenChannelCalibration->IsEnabled() && m_pBlueChannelCalibration->IsEnabled();
                            }

                            int64_t CMultipleChannelRadiometricCalibration::GetCameraId() const
                            {
                                return m_pRedChannelCalibration->GetCameraId();
                            }

                            Imaging::CBayerPattern::BayerPatternType CMultipleChannelRadiometricCalibration::GetBayerPattern() const
                            {
                                return m_pRedChannelCalibration->GetBayerPattern();
                            }

                            bool CMultipleChannelRadiometricCalibration::AddExposure(const Real ExposureTime)
                            {
                                const bool RedChannel = m_pRedChannelCalibration->AddExposure(ExposureTime);
                                const bool GreenChannel = m_pGreenChannelCalibration->AddExposure(ExposureTime);
                                const bool BlueChannel = m_pBlueChannelCalibration->AddExposure(ExposureTime);
                                return (RedChannel && GreenChannel && BlueChannel);
                            }

                            bool CMultipleChannelRadiometricCalibration::ClearExposures()
                            {
                                const bool RedChannel = m_pRedChannelCalibration->ClearExposures();
                                const bool GreenChannel = m_pGreenChannelCalibration->ClearExposures();
                                const bool BlueChannel = m_pBlueChannelCalibration->ClearExposures();
                                return (RedChannel && GreenChannel && BlueChannel);
                            }

                            int CMultipleChannelRadiometricCalibration::GetTotalExposures() const
                            {
                                return m_pRedChannelCalibration->GetTotalExposures();
                            }

                            bool CMultipleChannelRadiometricCalibration::SetGaussianBoxKernelRadius(const Real GaussianBoxKernelRadius)
                            {
                                const bool RedChannel = m_pRedChannelCalibration->SetGaussianBoxKernelRadius(GaussianBoxKernelRadius);
                                const bool GreenChannel = m_pGreenChannelCalibration->SetGaussianBoxKernelRadius(GaussianBoxKernelRadius);
                                const bool BlueChannel = m_pBlueChannelCalibration->SetGaussianBoxKernelRadius(GaussianBoxKernelRadius);
                                return RedChannel && GreenChannel && BlueChannel;
                            }

                            bool CMultipleChannelRadiometricCalibration::SetCommonGaussianKernelStandardDeviation(const Real StandardDeviation)
                            {
                                const bool RedChannel = m_pRedChannelCalibration->SetCommonGaussianKernelStandardDeviation(StandardDeviation);
                                const bool GreenChannel = m_pGreenChannelCalibration->SetCommonGaussianKernelStandardDeviation(StandardDeviation);
                                const bool BlueChannel = m_pBlueChannelCalibration->SetCommonGaussianKernelStandardDeviation(StandardDeviation);
                                return RedChannel && GreenChannel && BlueChannel;
                            }

                            bool CMultipleChannelRadiometricCalibration::SetCommonGaussianKernelCutOff(const Real CutOffDensity, const Real CutOffNormalizedDeviation)
                            {
                                const bool RedChannel = m_pRedChannelCalibration->SetCommonGaussianKernelCutOff(CutOffDensity, CutOffNormalizedDeviation);
                                const bool GreenChannel = m_pGreenChannelCalibration->SetCommonGaussianKernelCutOff(CutOffDensity, CutOffNormalizedDeviation);
                                const bool BlueChannel = m_pBlueChannelCalibration->SetCommonGaussianKernelCutOff(CutOffDensity, CutOffNormalizedDeviation);
                                return RedChannel && GreenChannel && BlueChannel;
                            }

                            Real CMultipleChannelRadiometricCalibration::GetGaussianBoxKernelRadius() const
                            {
                                return m_pRedChannelCalibration->GetGaussianBoxKernelRadius();
                            }

                            Real CMultipleChannelRadiometricCalibration::GetCommonGaussianKernelStandardDeviation() const
                            {
                                return m_pRedChannelCalibration->GetCommonGaussianKernelStandardDeviation();
                            }

                            bool CMultipleChannelRadiometricCalibration::Calibrate(const CRadiometricResponceFunction::PixelSelectionCriterion Criterion, const Real DominaceRadius, const int MaximalSamples, const Real SmoothingLambda, const Base::CRadianceWeigthingKernel::KernelType Kernel)
                            {
                                const bool RedChannel = m_pRedChannelCalibration->Calibrate(Criterion, DominaceRadius, MaximalSamples, SmoothingLambda, Kernel);
                                const bool GreenChannel = m_pGreenChannelCalibration->Calibrate(Criterion, DominaceRadius, MaximalSamples, SmoothingLambda, Kernel);
                                const bool BlueChannel = m_pBlueChannelCalibration->Calibrate(Criterion, DominaceRadius, MaximalSamples, SmoothingLambda, Kernel);
                                return RedChannel && GreenChannel && BlueChannel;
                            }

                            bool CMultipleChannelRadiometricCalibration::SaveResponceFunctionToFile(const std::string& PathFileName, const Files::CFile::FileMode Mode) const
                            {
                                return m_pMultipleChannelRadiometricResponceFunction->SaveResponceFunctionToFile(PathFileName, Mode);
                            }

                            const CRadiometricCalibration* CMultipleChannelRadiometricCalibration::GetRedChannelCalibration() const
                            {
                                return m_pRedChannelCalibration;
                            }

                            const CRadiometricCalibration* CMultipleChannelRadiometricCalibration::GetGreenChannelCalibration() const
                            {
                                return m_pGreenChannelCalibration;
                            }

                            const CRadiometricCalibration* CMultipleChannelRadiometricCalibration::GetBlueChannelCalibration() const
                            {
                                return m_pBlueChannelCalibration;
                            }

                            const CMultipleChannelRadiometricResponceFunction* CMultipleChannelRadiometricCalibration::GetMultipleChannelRadiometricResponceFunction() const
                            {
                                return m_pMultipleChannelRadiometricResponceFunction;
                            }
                        }
                    }
                }
            }
        }
    }
}
