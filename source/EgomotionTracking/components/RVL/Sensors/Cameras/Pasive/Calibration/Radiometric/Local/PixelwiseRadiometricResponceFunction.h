/*
 * PixelwiseRadiometricResponceFunction.h
 */

#pragma once

#include "../../../../../../Console/ConsoleOutputManager.h"
#include "../../../../../../Common/DataTypes.h"
#include "../../../../../../Files/OutFile.h"
#include "../../../../../../Files/InFile.h"
#include "../../../../../../Imaging/BayerPattern.h"
#include "../../../../../../Imaging/PixelTypes.h"
#include "../../../../../../Imaging/DiscreteRGBColorMap.h"
#include "../../../../../../Imaging/TImage.hpp"
#include "../../../../../../Imaging/ImageSize.h"
#include "../../../../../../Imaging/ImageExporter.h"
#include "RadiometricResponceFunctionPixel.h"
#include "ReferencedRadiometricPixel.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Local
                        {
                            class CPixelwiseRadiometricResponceFunction
                            {
                            public:

                                static CPixelwiseRadiometricResponceFunction* MergeChannels(const CPixelwiseRadiometricResponceFunction* pResponceFunctionChannelRed, const CPixelwiseRadiometricResponceFunction* pResponceFunctionChannelGreen, const CPixelwiseRadiometricResponceFunction* pResponceFunctionChannelBlue);
                                static bool SaveMergedChannels(const std::string& PathFileName, const CPixelwiseRadiometricResponceFunction* pResponceFunctionChannelRed, const CPixelwiseRadiometricResponceFunction* pResponceFunctionChannelGreen, const CPixelwiseRadiometricResponceFunction* pResponceFunctionChannelBlue);
                                static CPixelwiseRadiometricResponceFunction* CreateFromFile(const std::string& PathFileName);

                                CPixelwiseRadiometricResponceFunction();
                                virtual ~CPixelwiseRadiometricResponceFunction();

                                bool IsEnabled() const;
                                bool LoadResponceFunctionFromParameters(const uint64_t CameraId, const Imaging::CBayerPattern::BayerPatternType BayerPattern, const Imaging::CBayerPattern::Channel ChannelContent, const Imaging::TImage<CReferencedRadiometricPixel>* pPixelwiseRadiometricImage);
                                bool LoadResponceFunctionFile(const std::string& PathFileName);
                                bool SaveResponceFunctionToFile(const std::string& PathFileName) const;

                                bool ExportSlopeToImage(const std::string& PathFileName, const Imaging::CDiscreteRGBColorMap* pDiscreteRGBColorMap) const;
                                bool ExportOffsetToImage(const std::string& PathFileName, const Imaging::CDiscreteRGBColorMap* pDiscreteRGBColorMap) const;
                                bool ExportRMSDeviationToImage(const std::string& PathFileName, const Imaging::CDiscreteRGBColorMap* pDiscreteRGBColorMap) const;
                                bool ExportPeakNegativeDeviationToImage(const std::string& PathFileName, const Imaging::CDiscreteRGBColorMap* pDiscreteRGBColorMap) const;
                                bool ExportPeakPositiveDeviationToImage(const std::string& PathFileName, const Imaging::CDiscreteRGBColorMap* pDiscreteRGBColorMap) const;
                                bool ExportRangeDeviationToImage(const std::string& PathFileName, const Imaging::CDiscreteRGBColorMap* pDiscreteRGBColorMap) const;

                                uint64_t GetCameraId() const;
                                Imaging::CBayerPattern::BayerPatternType GetBayerPattern() const;
                                Imaging::CBayerPattern::Channel GetChannelContent() const;
                                Imaging::TImage<CRadiometricResponceFunctionPixel>* GetPixelwiseRadiometricResponceImage() const;

                            protected:

                                bool ExportImage(const std::string& PathFileName, const Imaging::TImage<Real>& Image, const Real Maximal, const Real Minimal, const Imaging::CDiscreteRGBColorMap* pDiscreteRGBColorMap) const;

                                CPixelwiseRadiometricResponceFunction(const Imaging::CImageSize& Size);

                                uint64_t m_CameraId;
                                Imaging::CBayerPattern::BayerPatternType m_BayerPattern;
                                Imaging::CBayerPattern::Channel m_ChannelContent;
                                Imaging::TImage<CRadiometricResponceFunctionPixel>* m_pPixelResponceImage;
                            };
                        }
                    }
                }
            }
        }
    }
}

