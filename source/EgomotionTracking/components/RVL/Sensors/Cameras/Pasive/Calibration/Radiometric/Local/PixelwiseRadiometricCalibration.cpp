/*
 * PixelwiseRadiometricCalibration.cpp
 */

#include "PixelwiseRadiometricCalibration.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Local
                        {
                            CPixelwiseRadiometricCalibration::CPixelwiseRadiometricCalibration(const Global::CRadiometricResponceFunction* pGlobalRadiometricResponceFunction, const Imaging::TImage<Real>* pRadianceImage) :
                                m_pRadiometricResponceFunction(pGlobalRadiometricResponceFunction),
                                m_pRadianceImage(pRadianceImage),
                                m_pRadianceImageIntern(nullptr),
                                m_pRadianceReferenceImage(nullptr),
                                m_pWeightedRadianceAccumulatorImage(nullptr),
                                m_pWeightingAccumulatorImage(nullptr),
                                m_pRadiometricImage(nullptr),
                                m_pLocalRadiometricResponceFunction(nullptr)
                            {
                                if (m_pRadiometricResponceFunction && m_pRadiometricResponceFunction->IsIdealResponceFunctionReady() && m_pRadianceImage && m_pRadianceImage->IsValid())
                                {
                                    m_RadianceWeigthingKernel = *m_pRadiometricResponceFunction->GetRadianceWeigthingKernel();
                                    const int Width = m_pRadianceImage->GetWidth();
                                    const int Height = m_pRadianceImage->GetHeight();
                                    m_pRadianceImageIntern = new Imaging::TImage<Real>(Width, Height);
                                    m_pRadianceReferenceImage = new Imaging::TImage<Real>(Width, Height);
                                    m_pWeightedRadianceAccumulatorImage = new Imaging::TImage<Real>(Width, Height);
                                    m_pWeightingAccumulatorImage = new Imaging::TImage<Real>(Width, Height);
                                    m_pRadiometricImage = new Imaging::TImage<CReferencedRadiometricPixel>(Width, Height);
                                    const Real* pRadianceReferencePixel = m_pRadianceReferenceImage->GetReadOnlyBuffer();
                                    const Real* pRadiancePixel = m_pRadianceImageIntern->GetReadOnlyBuffer();
                                    CReferencedRadiometricPixel* pLocalRadiometricPixel = m_pRadiometricImage->GetWritableBuffer();
                                    const Imaging::CBayerPattern::BayerPatternType BayerPattern = m_pRadiometricResponceFunction->GetBayerPattern();
                                    if (BayerPattern == Imaging::CBayerPattern::BayerPatternType(-1))
                                    {
                                        for (int Y = 0 ; Y < Height ; ++Y)
                                            for (int X = 0 ; X < Width ; ++X, ++pLocalRadiometricPixel, ++pRadiancePixel, ++pRadianceReferencePixel)
                                            {
                                                m_RadiometricPixels.push_back(pLocalRadiometricPixel->Initialize(X, Y, pRadiancePixel, pRadianceReferencePixel));
                                            }
                                    }
                                    else
                                    {
                                        Imaging::CBayerPattern BayerPatternMapLoader;
                                        if (BayerPatternMapLoader.SetType(BayerPattern))
                                        {
                                            Imaging::CBayerPattern::ChannelContentMap FlagMap = BayerPatternMapLoader.GetBayerPatternMap();
                                            for (int Y = 0 ; Y < Height ; ++Y)
                                                for (int X = 0 ; X < Width ; ++X, ++pLocalRadiometricPixel, ++pRadiancePixel, ++pRadianceReferencePixel)
                                                    if (FlagMap.m_YX[Y & 0X1][X & 0X1])
                                                    {
                                                        m_RadiometricPixels.push_back(pLocalRadiometricPixel->Initialize(X, Y, pRadiancePixel, pRadianceReferencePixel));
                                                    }
                                        }
                                    }
                                }
                            }

                            CPixelwiseRadiometricCalibration::~CPixelwiseRadiometricCalibration()
                            {
                                ClearSamples();
                                delete m_pRadianceImageIntern;
                                delete m_pRadianceReferenceImage;
                                delete m_pWeightedRadianceAccumulatorImage;
                                delete m_pWeightingAccumulatorImage;
                                delete m_pRadiometricImage;
                                delete m_pLocalRadiometricResponceFunction;
                            }

                            bool CPixelwiseRadiometricCalibration::IsEnabled() const
                            {
                                return m_RadiometricPixels.size();
                            }

                            uint64_t CPixelwiseRadiometricCalibration::GetCameraId() const
                            {
                                return m_pRadiometricResponceFunction ? m_pRadiometricResponceFunction->GetCameraId() : 0;
                            }

                            Imaging::CBayerPattern::BayerPatternType CPixelwiseRadiometricCalibration::GetBayerPattern() const
                            {
                                return m_pRadiometricResponceFunction ? m_pRadiometricResponceFunction->GetBayerPattern() : Imaging::CBayerPattern::BayerPatternType(-1);
                            }

                            Imaging::CBayerPattern::Channel CPixelwiseRadiometricCalibration::GetChannelContent() const
                            {
                                return m_pRadiometricResponceFunction ? m_pRadiometricResponceFunction->GetChannelContent() : Imaging::CBayerPattern::Channel(-1);
                            }

                            const CPixelwiseRadiometricResponceFunction* CPixelwiseRadiometricCalibration::GetLocalRadiometricResponceFunction() const
                            {
                                return m_pLocalRadiometricResponceFunction;
                            }

                            bool CPixelwiseRadiometricCalibration::AddExposure(const int ReferenceIndex, const int ExposureIndex, const Real ExposureTime)
                            {
                                if (m_RadiometricPixels.size())
                                {
                                    ReferenceExposureImage Sample = { ReferenceIndex, ExposureIndex, ExposureTime, m_pRadianceImage->Clone() };
                                    m_ReferenceExposureImages.push_back(Sample);
                                    return true;
                                }
                                return false;
                            }

                            bool CPixelwiseRadiometricCalibration::ClearExposures()
                            {
                                ClearSamples();
                                if (m_RadiometricPixels.size())
                                {
                                    for (std::list<CReferencedRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin() ; ppRadiometricCell != m_RadiometricPixels.end() ; ++ppRadiometricCell)
                                    {
                                        (*ppRadiometricCell)->ClearExposures();
                                    }
                                    return true;
                                }
                                return false;
                            }

                            bool CPixelwiseRadiometricCalibration::SetGaussianBoxKernelRadius(const Real GaussianBoxKernelRadius)
                            {
                                return m_RadianceWeigthingKernel.SetGaussianBoxKernelRadius(GaussianBoxKernelRadius);
                            }

                            bool CPixelwiseRadiometricCalibration::SetCommonGaussianKernelStandardDeviation(const Real StandardDeviation)
                            {
                                return m_RadianceWeigthingKernel.SetCommonGaussianKernelStandardDeviation(StandardDeviation);
                            }

                            bool CPixelwiseRadiometricCalibration::SetCommonGaussianKernelCutOff(const Real CutOffDensity, const Real CutOffNormalizedDeviation)
                            {
                                return m_RadianceWeigthingKernel.SetCommonGaussianKernelCutOff(CutOffDensity, CutOffNormalizedDeviation);
                            }

                            Real CPixelwiseRadiometricCalibration::GetGaussianBoxKernelRadius() const
                            {
                                return m_RadianceWeigthingKernel.GetGaussianBoxKernelRadius();
                            }

                            Real CPixelwiseRadiometricCalibration::GetCommonGaussianKernelStandardDeviation() const
                            {
                                return m_RadianceWeigthingKernel.GetCommonGaussianKernelStandardDeviation();
                            }

                            bool CPixelwiseRadiometricCalibration::Calibrate(const Base::CRadianceWeigthingKernel::KernelType Kernel)
                            {
                                if (LoadSamples(Kernel))
                                {
                                    std::list<CReferencedRadiometricPixel*>::const_iterator RadiometricCellsEnd = m_RadiometricPixels.end();
                                    for (std::list<CReferencedRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin() ; ppRadiometricCell != RadiometricCellsEnd ; ++ppRadiometricCell)
                                    {
                                        (*ppRadiometricCell)->Calibrate(m_RadianceWeigthingKernel, Kernel);
                                    }
                                    ClearSamples();
                                    delete m_pLocalRadiometricResponceFunction;
                                    m_pLocalRadiometricResponceFunction = nullptr;
                                    m_pLocalRadiometricResponceFunction = new CPixelwiseRadiometricResponceFunction();
                                    return m_pLocalRadiometricResponceFunction->LoadResponceFunctionFromParameters(m_pRadiometricResponceFunction->GetCameraId(), m_pRadiometricResponceFunction->GetBayerPattern(), m_pRadiometricResponceFunction->GetChannelContent(), m_pRadiometricImage);
                                }
                                return false;
                            }

                            bool CPixelwiseRadiometricCalibration::LoadSamples(const Base::CRadianceWeigthingKernel::KernelType Kernel)
                            {
                                if (m_ReferenceExposureImages.size() >= _PIXELWISE_RADIOMETRIC_CALIBRATION_MINIMAL_EXPOSURES_)
                                {
                                    m_ReferenceExposureImages.sort(SortReferenceExposureImages);
                                    ClearSynthesize();
                                    std::list<ReferenceExposureImage> BlockExposureImages;
                                    int CurrentReferenceIndex = m_ReferenceExposureImages.front().m_ReferenceIndex;
                                    std::list<ReferenceExposureImage>::const_iterator ReferenceExposureImagesEnd = m_ReferenceExposureImages.end();
                                    for (std::list<ReferenceExposureImage>::const_iterator pReferenceExposureImage = m_ReferenceExposureImages.begin(), pNextReferenceExposureImage = ++m_ReferenceExposureImages.begin() ; pReferenceExposureImage != ReferenceExposureImagesEnd ; ++pReferenceExposureImage, ++pNextReferenceExposureImage)
                                    {
                                        AddSynthesizeExposure(pReferenceExposureImage->m_pRadianceImage, pReferenceExposureImage->m_ExposureTime, Kernel);
                                        BlockExposureImages.push_back(*pReferenceExposureImage);
                                        if ((pNextReferenceExposureImage->m_ReferenceIndex != CurrentReferenceIndex) || (pNextReferenceExposureImage == ReferenceExposureImagesEnd))
                                        {
                                            Synthesize();
                                            LoadReferencedSamples(BlockExposureImages);
                                            BlockExposureImages.clear();
                                            ClearSynthesize();
                                            if (pNextReferenceExposureImage != ReferenceExposureImagesEnd)
                                            {
                                                CurrentReferenceIndex = pNextReferenceExposureImage->m_ReferenceIndex;
                                            }
                                        }
                                    }
                                    return true;
                                }
                                return false;
                            }

                            void CPixelwiseRadiometricCalibration::ClearSamples()
                            {
                                for (auto& m_ReferenceExposureImage : m_ReferenceExposureImages)
                                {
                                    delete m_ReferenceExposureImage.m_pRadianceImage;
                                }
                                m_ReferenceExposureImages.clear();
                            }

                            void CPixelwiseRadiometricCalibration::LoadReferencedSamples(std::list<ReferenceExposureImage>& BlockExposureImages)
                            {
                                for (auto& BlockExposureImage : BlockExposureImages)
                                {
                                    const Real ExposureTime = BlockExposureImage.m_ExposureTime;
                                    m_pRadianceImageIntern->Copy(BlockExposureImage.m_pRadianceImage, true, nullptr);
                                    for (std::list<CReferencedRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin() ; ppRadiometricCell != m_RadiometricPixels.end() ; ++ppRadiometricCell)
                                    {
                                        (*ppRadiometricCell)->AddExposure(ExposureTime);
                                    }
                                }
                            }

                            void CPixelwiseRadiometricCalibration::AddSynthesizeExposure(const Imaging::TImage<Real>* pRadianceImage, const Real ExposureTime, const Base::CRadianceWeigthingKernel::KernelType Kernel)
                            {
                                const int Width = m_pRadianceImage->GetWidth();
                                const int Height = m_pRadianceImage->GetHeight();
                                const Real* pRadiancePixel = pRadianceImage->GetReadOnlyBuffer();
                                Real* pWeightedRadianceAccumulatorPixel = m_pWeightedRadianceAccumulatorImage->GetWritableBuffer();
                                Real* pWeightingAccumulatorPixel = m_pWeightingAccumulatorImage->GetWritableBuffer();
                                const Real ExposureSlope = m_pRadiometricResponceFunction->GetIntensitySlope() / ExposureTime;
                                const Real ExposureOffset = m_pRadiometricResponceFunction->GetIntensityOffset() / ExposureTime;
                                const Real MinimalIntensity = m_pRadiometricResponceFunction->GetMinimalIntensity();
                                const Imaging::CBayerPattern::BayerPatternType BayerPattern = m_pRadiometricResponceFunction->GetBayerPattern();
                                if (BayerPattern == Imaging::CBayerPattern::BayerPatternType(-1))
                                {
                                    for (int Y = 0 ; Y < Height ; ++Y)
                                        for (int X = 0 ; X < Width ; ++X, ++pRadiancePixel, ++pWeightedRadianceAccumulatorPixel, ++pWeightingAccumulatorPixel)
                                            if (*pRadiancePixel > MinimalIntensity)
                                            {
                                                const Real RegressionWeigthing = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(*pRadiancePixel, Kernel);
                                                *pWeightingAccumulatorPixel += RegressionWeigthing;
                                                *pWeightedRadianceAccumulatorPixel += ((*pRadiancePixel * ExposureSlope) + ExposureOffset) * RegressionWeigthing;
                                            }
                                }
                                else
                                {
                                    Imaging::CBayerPattern BayerPatternMapLoader;
                                    if (BayerPatternMapLoader.SetType(BayerPattern))
                                    {
                                        Imaging::CBayerPattern::ChannelContentMap FlagMap = BayerPatternMapLoader.GetBayerPatternMap();
                                        for (int Y = 0 ; Y < Height ; ++Y)
                                            for (int X = 0 ; X < Width ; ++X, ++pRadiancePixel, ++pWeightedRadianceAccumulatorPixel, ++pWeightingAccumulatorPixel)
                                                if (FlagMap.m_YX[Y & 0X1][X & 0X1] && (*pRadiancePixel > MinimalIntensity))
                                                {
                                                    const Real RegressionWeigthing = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(*pRadiancePixel, Kernel);
                                                    *pWeightingAccumulatorPixel += RegressionWeigthing;
                                                    *pWeightedRadianceAccumulatorPixel += ((*pRadiancePixel * ExposureSlope) + ExposureOffset) * RegressionWeigthing;
                                                }
                                    }
                                }
                            }

                            void CPixelwiseRadiometricCalibration::Synthesize()
                            {
                                const int Width = m_pRadianceImage->GetWidth();
                                const int Height = m_pRadianceImage->GetHeight();
                                const Real* pWeightedRadianceAccumulatorPixel = m_pWeightedRadianceAccumulatorImage->GetReadOnlyBuffer();
                                const Real* pWeightingAccumulatorPixel = m_pWeightingAccumulatorImage->GetReadOnlyBuffer();
                                Real* pRadianceReferencePixel = m_pRadianceReferenceImage->GetWritableBuffer();
                                const Imaging::CBayerPattern::BayerPatternType BayerPattern = m_pRadiometricResponceFunction->GetBayerPattern();
                                if (BayerPattern == Imaging::CBayerPattern::BayerPatternType(-1))
                                {
                                    for (int Y = 0 ; Y < Height ; ++Y)
                                        for (int X = 0 ; X < Width ; ++X, ++pWeightedRadianceAccumulatorPixel, ++pWeightingAccumulatorPixel, ++pRadianceReferencePixel)
                                        {
                                            *pRadianceReferencePixel = IsPositive(*pWeightingAccumulatorPixel) ? (*pWeightedRadianceAccumulatorPixel / *pWeightingAccumulatorPixel) : Real(1);
                                        }
                                }
                                else
                                {
                                    Imaging::CBayerPattern BayerPatternMapLoader;
                                    if (BayerPatternMapLoader.SetType(BayerPattern))
                                    {
                                        Imaging::CBayerPattern::ChannelContentMap FlagMap = BayerPatternMapLoader.GetBayerPatternMap();
                                        for (int Y = 0 ; Y < Height ; ++Y)
                                            for (int X = 0 ; X < Width ; ++X, ++pWeightedRadianceAccumulatorPixel, ++pWeightingAccumulatorPixel, ++pRadianceReferencePixel)
                                                if (FlagMap.m_YX[Y & 0X1][X & 0X1])
                                                {
                                                    *pRadianceReferencePixel = IsPositive(*pWeightingAccumulatorPixel) ? (*pWeightedRadianceAccumulatorPixel / *pWeightingAccumulatorPixel) : Real(1);
                                                }
                                    }
                                }
                            }

                            void CPixelwiseRadiometricCalibration::ClearSynthesize()
                            {
                                m_pWeightedRadianceAccumulatorImage->Clear();
                                m_pWeightingAccumulatorImage->Clear();
                                m_pRadianceReferenceImage->Clear();
                            }

                            bool CPixelwiseRadiometricCalibration::SortReferenceExposureImages(const ReferenceExposureImage& lhs, const ReferenceExposureImage& rhs)
                            {
                                if (lhs.m_ReferenceIndex == rhs.m_ReferenceIndex)
                                {
                                    return lhs.m_ExposureIndex < rhs.m_ExposureIndex;
                                }
                                return lhs.m_ReferenceIndex < rhs.m_ReferenceIndex;
                            }
                        }
                    }
                }
            }
        }
    }
}

