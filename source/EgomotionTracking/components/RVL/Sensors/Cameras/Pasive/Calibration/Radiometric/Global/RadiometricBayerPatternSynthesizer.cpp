/*
 * RadiometricBayerPatternSynthesizer.cpp
 */

#include "RadiometricBayerPatternSynthesizer.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            CRadiometricBayerPatternSynthesizer::CRadiometricBayerPatternSynthesizer(const Imaging::TImage<Real>* pContinousSourceImage, const CMultipleChannelRadiometricResponceFunction* pMultipleChannelRadiometricResponceFunction, const std::vector<Base::CExposure>& Exposures, const int SamplesPerUnit) :
                                m_Enabled(false),
                                m_Exposures(EnsureExposuresOrder(Exposures)),
                                m_pDiscreteSourceImage(nullptr),
                                m_pContinousSourceImage(pContinousSourceImage),
                                m_pMultipleChannelRadiometricResponceFunction(pMultipleChannelRadiometricResponceFunction),
                                m_pDensityWeightedLogRadianceImage(nullptr),
                                m_pDensityWeightedImage(nullptr),
                                m_pLogRadianceImage(nullptr),
                                m_pDensityWeightedExposureImage(nullptr),
                                m_pOptimalExposureImage(nullptr),
                                m_pRedChannelLookUpTable(nullptr),
                                m_pGreenChannelLookUpTable(nullptr),
                                m_pBlueChannelLookUpTable(nullptr),
                                m_pUnifiedExposureHistogram(nullptr),
                                m_pRedChannelExposureHistogram(nullptr),
                                m_pGreenChannelExposureHistogram(nullptr),
                                m_pBlueChannelExposureHistogram(nullptr),
                                m_TotalIntegratedExposures(0),
                                m_MinimalExposureParametricIndex(0),
                                m_MaximalExposureParametricIndex(0),
                                m_LastExposureTotalIntegratedPixels(0),
                                m_LastSynthesizeTotalUnsampledPixels(0),
                                m_MinimalIntensity(Real(0)),
                                m_MaximalIntensity(Real(255)),
                                m_LastExposureTotalIntegratedKernelDensity(Real(0)),
                                m_OptimalExposureWeighting(Real(0)),
                                m_OptimalExposureMode(eOptimalExposureExtractionDisabled),
                                m_Kernel(Base::CRadianceWeigthingKernel::eGaussian)
                            {
                                if ((m_Exposures.size() == Exposures.size()) && m_pContinousSourceImage && m_pContinousSourceImage->IsValid() && m_pMultipleChannelRadiometricResponceFunction && m_pMultipleChannelRadiometricResponceFunction->AreIdealResponceFunctionsReady())
                                {
                                    const Imaging::CImageSize& Size = m_pContinousSourceImage->GetSize();
                                    m_pDensityWeightedLogRadianceImage = new Imaging::TImage<Real>(Size);
                                    m_pDensityWeightedImage = new Imaging::TImage<Real>(Size);
                                    m_pLogRadianceImage = new Imaging::TImage<Real>(Size);
                                    m_pDensityWeightedExposureImage = new Imaging::TImage<Real>(Size);
                                    m_pOptimalExposureImage = new Imaging::TImage<Real>(Size);
                                    m_pUnifiedExposureHistogram = new int[256];
                                    m_pRedChannelExposureHistogram = new int[256];
                                    m_pGreenChannelExposureHistogram = new int[256];
                                    m_pBlueChannelExposureHistogram = new int[256];
                                    ClearImages();
                                    ClearHistograms();
                                    LoadLogTimesTable();
                                    LoadLookUpTables(SamplesPerUnit);
                                    LoadIntensityLimits(Real(1));
                                    m_Enabled = m_BayerPatternMapLoader.SetType(m_pMultipleChannelRadiometricResponceFunction->GetBayerPattern());
                                }
                            }

                            CRadiometricBayerPatternSynthesizer::CRadiometricBayerPatternSynthesizer(const Imaging::TImage<Byte>* pDiscreteSourceImage, const CMultipleChannelRadiometricResponceFunction* pMultipleChannelRadiometricResponceFunction, const std::vector<Base::CExposure>& Exposures) :
                                m_Enabled(false),
                                m_Exposures(EnsureExposuresOrder(Exposures)),
                                m_pDiscreteSourceImage(pDiscreteSourceImage),
                                m_pContinousSourceImage(nullptr),
                                m_pMultipleChannelRadiometricResponceFunction(pMultipleChannelRadiometricResponceFunction),
                                m_pDensityWeightedLogRadianceImage(nullptr),
                                m_pDensityWeightedImage(nullptr),
                                m_pLogRadianceImage(nullptr),
                                m_pDensityWeightedExposureImage(nullptr),
                                m_pOptimalExposureImage(nullptr),
                                m_pRedChannelLookUpTable(nullptr),
                                m_pGreenChannelLookUpTable(nullptr),
                                m_pBlueChannelLookUpTable(nullptr),
                                m_pUnifiedExposureHistogram(nullptr),
                                m_pRedChannelExposureHistogram(nullptr),
                                m_pGreenChannelExposureHistogram(nullptr),
                                m_pBlueChannelExposureHistogram(nullptr),
                                m_TotalIntegratedExposures(0),
                                m_MinimalExposureParametricIndex(0),
                                m_MaximalExposureParametricIndex(0),
                                m_LastExposureTotalIntegratedPixels(0),
                                m_LastSynthesizeTotalUnsampledPixels(0),
                                m_MinimalIntensity(Real(0)),
                                m_MaximalIntensity(Real(255)),
                                m_LastExposureTotalIntegratedKernelDensity(Real(0)),
                                m_OptimalExposureWeighting(Real(0)),
                                m_OptimalExposureMode(eOptimalExposureExtractionDisabled),
                                m_Kernel(Base::CRadianceWeigthingKernel::eGaussian)
                            {
                                if ((m_Exposures.size() == Exposures.size()) && m_pDiscreteSourceImage && m_pDiscreteSourceImage->IsValid() && m_pMultipleChannelRadiometricResponceFunction && m_pMultipleChannelRadiometricResponceFunction->AreIdealResponceFunctionsReady())
                                {
                                    const Imaging::CImageSize& Size = m_pDiscreteSourceImage->GetSize();
                                    m_pDensityWeightedLogRadianceImage = new Imaging::TImage<Real>(Size);
                                    m_pDensityWeightedImage = new Imaging::TImage<Real>(Size);
                                    m_pLogRadianceImage = new Imaging::TImage<Real>(Size);
                                    m_pDensityWeightedExposureImage = new Imaging::TImage<Real>(Size);
                                    m_pOptimalExposureImage = new Imaging::TImage<Real>(Size);
                                    m_pUnifiedExposureHistogram = new int[256];
                                    m_pRedChannelExposureHistogram = new int[256];
                                    m_pGreenChannelExposureHistogram = new int[256];
                                    m_pBlueChannelExposureHistogram = new int[256];
                                    ClearImages();
                                    ClearHistograms();
                                    LoadLogTimesTable();
                                    LoadLookUpTables(1);
                                    LoadIntensityLimits(Real(1));
                                    m_Enabled = m_BayerPatternMapLoader.SetType(m_pMultipleChannelRadiometricResponceFunction->GetBayerPattern());
                                }
                            }

                            CRadiometricBayerPatternSynthesizer::~CRadiometricBayerPatternSynthesizer()
                            {
                                delete m_pDensityWeightedLogRadianceImage;
                                delete m_pDensityWeightedImage;
                                delete m_pLogRadianceImage;
                                delete m_pDensityWeightedExposureImage;
                                delete m_pOptimalExposureImage;
                                delete m_pRedChannelLookUpTable;
                                delete m_pGreenChannelLookUpTable;
                                delete m_pBlueChannelLookUpTable;
                            }

                            bool CRadiometricBayerPatternSynthesizer::StartExposureBracketing()
                            {
                                if (m_Enabled)
                                {
                                    m_TotalIntegratedExposures = 0;
                                    m_OptimalExposureWeighting = Real(0);
                                    ClearImages();
                                    return true;
                                }
                                return false;
                            }

                            bool CRadiometricBayerPatternSynthesizer::AddExposure(const int ParametricIndex)
                            {
                                if (m_Enabled && (ParametricIndex >= m_MinimalExposureParametricIndex) && (ParametricIndex <= m_MaximalExposureParametricIndex))
                                {
                                    if (m_pDiscreteSourceImage)
                                    {
                                        AddDiscreteExposure(ParametricIndex);
                                        ++m_TotalIntegratedExposures;
                                        return true;
                                    }
                                    else if (m_pRedChannelLookUpTable && m_pGreenChannelLookUpTable && m_pBlueChannelLookUpTable)
                                    {
                                        AddSemiContinousExposure(ParametricIndex);
                                        ++m_TotalIntegratedExposures;
                                        return true;
                                    }
                                    else
                                    {
                                        AddContinousExposure(ParametricIndex);
                                        ++m_TotalIntegratedExposures;
                                        return true;
                                    }
                                }
                                return false;
                            }

                            bool CRadiometricBayerPatternSynthesizer::Synthesize()
                            {
                                if (m_Enabled && (m_TotalIntegratedExposures > _RADIOMETRIC_SYNTHESIZER_MINIMAL_INTEGRATED_EXPOSURES_))
                                {
                                    const Real* pDensityWeightedPixel = m_pDensityWeightedImage->GetReadOnlyBuffer();
                                    const Real* pDensityWeightedLogRadiancePixel = m_pDensityWeightedLogRadianceImage->GetReadOnlyBuffer();
                                    const Real* const pLogRadiancePixelEnd = m_pLogRadianceImage->GetBufferEnd();
                                    Real* pLogRadiancePixel = m_pLogRadianceImage->GetWritableBuffer();
                                    m_LastSynthesizeTotalUnsampledPixels = 0;
                                    m_pLogRadianceImage->Clear();
                                    while (pLogRadiancePixel < pLogRadiancePixelEnd)
                                        if (IsPositive(*pDensityWeightedPixel))
                                        {
                                            *pLogRadiancePixel++ = *pDensityWeightedLogRadiancePixel++ / *pDensityWeightedPixel++;
                                        }
                                        else
                                        {
                                            ++pDensityWeightedPixel;
                                            ++pLogRadiancePixel;
                                            ++pDensityWeightedLogRadiancePixel;
                                            ++m_LastSynthesizeTotalUnsampledPixels;
                                        }
                                    return true;
                                }
                                return false;
                            }

                            bool CRadiometricBayerPatternSynthesizer::GetSynthesizeDensityStatistics(Real& DensityAccumulator, Real& MaximalDensity, Real& MinimalDensity, Real& MeanDensity, Real& StandarDeviationDensity, int& TotalCriticalPixels, const Real CriticalDensity) const
                            {
                                if (m_Enabled)
                                {
                                    const Real* const ppDensityWeightedPixelEnd = m_pDensityWeightedImage->GetBufferEnd();
                                    const Real* pDensityWeightedPixel = m_pDensityWeightedImage->GetReadOnlyBuffer();
                                    DensityAccumulator = Real(0);
                                    MaximalDensity = g_RealMinusInfinity;
                                    MinimalDensity = g_RealPlusInfinity;
                                    TotalCriticalPixels = 0;
                                    Real DensitySquareAccumulator = Real(0);
                                    while (pDensityWeightedPixel < ppDensityWeightedPixelEnd)
                                    {
                                        const Real Density = *pDensityWeightedPixel++;
                                        if (Density < CriticalDensity)
                                        {
                                            ++TotalCriticalPixels;
                                        }
                                        if (Density > MaximalDensity)
                                        {
                                            MaximalDensity = Density;
                                        }
                                        if (Density < MinimalDensity)
                                        {
                                            MinimalDensity = Density;
                                        }
                                        DensityAccumulator += Density;
                                        DensitySquareAccumulator += Density * Density;
                                    }
                                    const int TotalPixels = m_pDensityWeightedImage->GetArea();
                                    MeanDensity = DensityAccumulator / Real(TotalPixels);
                                    StandarDeviationDensity = std::sqrt((DensitySquareAccumulator / Real(TotalPixels)) - (MeanDensity * MeanDensity));
                                    return true;
                                }
                                return false;
                            }

                            bool CRadiometricBayerPatternSynthesizer::GetSynthesizeRadianceStatistics(Real& MaximalRadiance, Real& MinimalRadiance, Real& MeanRadiance, Real& StandarDeviationRadiance, int& TotalEncodingBits) const
                            {
                                if (m_Enabled)
                                {
                                    const Real* const pLogRadiancePixelEnd = m_pLogRadianceImage->GetBufferEnd();
                                    const Real* pLogRadiancePixel = m_pLogRadianceImage->GetReadOnlyBuffer();
                                    MeanRadiance = Real(0);
                                    MaximalRadiance = g_RealMinusInfinity;
                                    MinimalRadiance = g_RealPlusInfinity;
                                    Real RadianceSquareAccumulator = Real(0);
                                    while (pLogRadiancePixel < pLogRadiancePixelEnd)
                                    {
                                        const Real Radiance = std::exp(*pLogRadiancePixel++);
                                        if (Radiance > MaximalRadiance)
                                        {
                                            MaximalRadiance = Radiance;
                                        }
                                        if (Radiance < MinimalRadiance)
                                        {
                                            MinimalRadiance = Radiance;
                                        }
                                        MeanRadiance += Radiance;
                                        RadianceSquareAccumulator += Radiance * Radiance;
                                    }
                                    const int TotalPixels = m_pDensityWeightedImage->GetArea();
                                    MeanRadiance /= Real(TotalPixels);
                                    StandarDeviationRadiance = std::sqrt((RadianceSquareAccumulator / Real(TotalPixels)) - (MeanRadiance * MeanRadiance));
                                    TotalEncodingBits = std::ceil(std::log2(MaximalRadiance - MinimalRadiance));
                                    return true;
                                }
                                return false;
                            }

                            bool CRadiometricBayerPatternSynthesizer::ExtractOptimalExposureImage()
                            {
                                if (m_Enabled && (m_TotalIntegratedExposures > _RADIOMETRIC_SYNTHESIZER_MINIMAL_INTEGRATED_EXPOSURES_) && (IsPositive(m_OptimalExposureWeighting)))
                                {
                                    switch (m_OptimalExposureMode)
                                    {
                                        case eOptimalExposureExtractionDisabled:
                                            return false;
                                        case eWeightingDensity:
                                        {
                                            const Real NormalizationFactor = Real(1) / m_OptimalExposureWeighting;
                                            const Real* const pDensityWeightedExposurePixelEnd = m_pDensityWeightedExposureImage->GetBufferEnd();
                                            const Real* pDensityWeightedExposurePixel = m_pDensityWeightedExposureImage->GetReadOnlyBuffer();
                                            Real* pOptimalExposurePixel = m_pOptimalExposureImage->GetWritableBuffer();
                                            while (pDensityWeightedExposurePixel < pDensityWeightedExposurePixelEnd)
                                            {
                                                *pOptimalExposurePixel++ = *pDensityWeightedExposurePixel++ * NormalizationFactor;
                                            }
                                        }
                                        break;
                                        case eMaximalDensity:
                                            m_pOptimalExposureImage->Copy(m_pDensityWeightedExposureImage, true, nullptr);
                                            break;
                                    }
                                    return true;
                                }
                                return false;
                            }

                            const std::vector<Base::CExposure>& CRadiometricBayerPatternSynthesizer::GetExposures() const
                            {
                                return m_Exposures;
                            }

                            bool CRadiometricBayerPatternSynthesizer::SetOptimalExposureExtractionMode(const OptimalExposureExtractionMode Mode)
                            {
                                if (m_Enabled && ((Mode == eWeightingDensity) || (Mode == eMaximalDensity)))
                                {
                                    m_OptimalExposureMode = Mode;
                                    return true;
                                }
                                return false;
                            }

                            const Imaging::TImage<Real>* CRadiometricBayerPatternSynthesizer::GetLogRadianceImage() const
                            {
                                return m_pLogRadianceImage;
                            }

                            const Imaging::TImage<Real>* CRadiometricBayerPatternSynthesizer::GetDensityWeightedImage() const
                            {
                                return m_pDensityWeightedImage;
                            }

                            const Imaging::TImage<Real>* CRadiometricBayerPatternSynthesizer::GetOptimalExposureImage() const
                            {
                                return m_pOptimalExposureImage;
                            }

                            bool CRadiometricBayerPatternSynthesizer::DisplayLinealScaledRadianceImage(Imaging::TImage<Byte>* pDisplay) const
                            {
                                if (m_Enabled && pDisplay && pDisplay->IsValid() && pDisplay->HasBufferOwnership() && (pDisplay->GetSize() == m_pLogRadianceImage->GetSize()))
                                {
                                    const Real* const pLogRadiancePixelEnd = m_pLogRadianceImage->GetBufferEnd();
                                    const Real* pLogRadiancePixel = m_pLogRadianceImage->GetReadOnlyBuffer();
                                    Real Maximal = *pLogRadiancePixel;
                                    Real Minimal = *pLogRadiancePixel++;
                                    while (pLogRadiancePixel < pLogRadiancePixelEnd)
                                    {
                                        if (*pLogRadiancePixel < Minimal)
                                        {
                                            Minimal = *pLogRadiancePixel;
                                        }
                                        else if (*pLogRadiancePixel > Maximal)
                                        {
                                            Maximal = *pLogRadiancePixel;
                                        }
                                        ++pLogRadiancePixel;
                                    }
                                    const Real Range = Maximal - Minimal;
                                    if (IsPositive(Range))
                                    {
                                        const Real ScaleFactor = Real(255) / Range;
                                        pLogRadiancePixel = m_pLogRadianceImage->GetReadOnlyBuffer();
                                        Byte* pDisplayExposurePixel = pDisplay->GetWritableBuffer();
                                        while (pLogRadiancePixel < pLogRadiancePixelEnd)
                                        {
                                            *pDisplayExposurePixel++ = Byte(std::round((*pLogRadiancePixel++ - Minimal) * ScaleFactor));
                                        }
                                        return true;
                                    }
                                }
                                return false;
                            }

                            bool CRadiometricBayerPatternSynthesizer::DisplayOptimalExposureImage(Imaging::TImage<Byte>* pDisplay) const
                            {
                                if (m_Enabled && pDisplay && pDisplay->IsValid() && pDisplay->HasBufferOwnership() && (pDisplay->GetSize() == m_pOptimalExposureImage->GetSize()))
                                {
                                    const Real* const pOptimalExposurePixelEnd = m_pOptimalExposureImage->GetBufferEnd();
                                    const Real* pOptimalExposurePixel = m_pOptimalExposureImage->GetReadOnlyBuffer();
                                    Byte* pDisplayExposurePixel = pDisplay->GetWritableBuffer();
                                    while (pOptimalExposurePixel < pOptimalExposurePixelEnd)
                                    {
                                        *pDisplayExposurePixel++ = Byte(std::round(*pOptimalExposurePixel++));
                                    }
                                    return true;
                                }
                                return false;
                            }

                            uint64_t CRadiometricBayerPatternSynthesizer::GetCameraId() const
                            {
                                return m_Enabled ? m_pMultipleChannelRadiometricResponceFunction->GetCameraId() : 0;
                            }

                            Imaging::CBayerPattern::BayerPatternType CRadiometricBayerPatternSynthesizer::GetBayerPattern() const
                            {
                                return m_Enabled ? m_pMultipleChannelRadiometricResponceFunction->GetBayerPattern() : Imaging::CBayerPattern::BayerPatternType(-1);
                            }

                            const int* CRadiometricBayerPatternSynthesizer::GetUnifiedExposureHistogram() const
                            {
                                return m_pUnifiedExposureHistogram;
                            }

                            const int* CRadiometricBayerPatternSynthesizer::GetRedChannelExposureHistogram() const
                            {
                                return m_pRedChannelExposureHistogram;
                            }

                            const int* CRadiometricBayerPatternSynthesizer::GetGreenChannelExposureHistogram() const
                            {
                                return m_pGreenChannelExposureHistogram;
                            }

                            const int* CRadiometricBayerPatternSynthesizer::GetBlueChannelExposureHistogram() const
                            {
                                return m_pBlueChannelExposureHistogram;
                            }

                            bool CRadiometricBayerPatternSynthesizer::IsEnabled() const
                            {
                                return m_Enabled;
                            }

                            const CMultipleChannelRadiometricResponceFunction* CRadiometricBayerPatternSynthesizer::GetMultipleChannelRadiometricResponceFunction() const
                            {
                                return m_pMultipleChannelRadiometricResponceFunction;
                            }

                            int CRadiometricBayerPatternSynthesizer::GetTotalIntegratedExposures() const
                            {
                                return m_TotalIntegratedExposures;
                            }

                            int CRadiometricBayerPatternSynthesizer::GetLastExposureTotalIntegratedPixels() const
                            {
                                return m_LastExposureTotalIntegratedPixels;
                            }

                            Real CRadiometricBayerPatternSynthesizer::GetLastExposureTotalIntegratedKernelDensity() const
                            {
                                return m_LastExposureTotalIntegratedKernelDensity;
                            }

                            int CRadiometricBayerPatternSynthesizer::GetLastSynthesizeTotalUnsampledPixels() const
                            {
                                return m_LastSynthesizeTotalUnsampledPixels;
                            }

                            const std::vector<Base::CExposure> CRadiometricBayerPatternSynthesizer::EnsureExposuresOrder(const std::vector<Base::CExposure>& Exposures)
                            {
                                std::vector<Base::CExposure> SafeOrderExposures = Exposures;
                                std::sort(SafeOrderExposures.begin(), SafeOrderExposures.end(), Base::CExposure::SortExposuresByParametricIndex);
                                std::vector<Base::CExposure>::iterator Location = std::unique(SafeOrderExposures.begin(), SafeOrderExposures.end(), Base::CExposure::EqualsExposuresByParametricIndex);
                                SafeOrderExposures.resize(std::distance(SafeOrderExposures.begin(), Location));
                                return SafeOrderExposures;
                            }

                            void CRadiometricBayerPatternSynthesizer::LoadIntensityLimits(const Real IntensityMargin)
                            {
                                const CRadiometricResponceFunction* pRedChannelResponceFunction = m_pMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction();
                                const CRadiometricResponceFunction* pGreenChannelResponceFunction = m_pMultipleChannelRadiometricResponceFunction->GetGreenChannelResponceFunction();
                                const CRadiometricResponceFunction* pBlueChannelResponceFunction = m_pMultipleChannelRadiometricResponceFunction->GetBlueChannelResponceFunction();
                                const Real RedMinimalIntensity = pRedChannelResponceFunction->GetMinimalIntensity();
                                const Real GreenMinimalIntensity = pGreenChannelResponceFunction->GetMinimalIntensity();
                                const Real BlueMinimalIntensity = pBlueChannelResponceFunction->GetMinimalIntensity();
                                m_MinimalIntensity = std::max(IntensityMargin, std::max(std::max(RedMinimalIntensity, GreenMinimalIntensity), BlueMinimalIntensity));
                                m_MaximalIntensity = Real(255) - IntensityMargin;
                            }

                            void CRadiometricBayerPatternSynthesizer::LoadLogTimesTable()
                            {
                                m_LogTimeIndexMap.clear();
                                if (m_Exposures.size())
                                {
                                    m_MinimalExposureParametricIndex = m_Exposures.front().GetParametricIndex();
                                    m_MaximalExposureParametricIndex = m_Exposures.back().GetParametricIndex();
                                    for (const auto& m_Exposure : m_Exposures)
                                    {
                                        m_LogTimeIndexMap[m_Exposure.GetParametricIndex()] = std::log(m_Exposure.GetTime());
                                    }
                                }
                            }

                            void CRadiometricBayerPatternSynthesizer::LoadLookUpTables(const int SamplesPerUnit)
                            {
                                if (m_pRedChannelLookUpTable)
                                {
                                    delete m_pRedChannelLookUpTable;
                                    m_pRedChannelLookUpTable = nullptr;
                                }
                                if (m_pGreenChannelLookUpTable)
                                {
                                    delete m_pGreenChannelLookUpTable;
                                    m_pGreenChannelLookUpTable = nullptr;
                                }
                                if (m_pBlueChannelLookUpTable)
                                {
                                    delete m_pBlueChannelLookUpTable;
                                    m_pBlueChannelLookUpTable = nullptr;
                                }
                                if (SamplesPerUnit && m_pMultipleChannelRadiometricResponceFunction && m_pMultipleChannelRadiometricResponceFunction->AreIdealResponceFunctionsReady())
                                {
                                    m_pRedChannelLookUpTable = new CRadiometricResponceFunctionLookUpTable();
                                    m_pGreenChannelLookUpTable = new CRadiometricResponceFunctionLookUpTable();
                                    m_pBlueChannelLookUpTable = new CRadiometricResponceFunctionLookUpTable();
                                    m_pRedChannelLookUpTable->LoadLookUpTable(m_pMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction(), m_Exposures, SamplesPerUnit, m_Kernel);
                                    m_pGreenChannelLookUpTable->LoadLookUpTable(m_pMultipleChannelRadiometricResponceFunction->GetGreenChannelResponceFunction(), m_Exposures, SamplesPerUnit, m_Kernel);
                                    m_pBlueChannelLookUpTable->LoadLookUpTable(m_pMultipleChannelRadiometricResponceFunction->GetBlueChannelResponceFunction(), m_Exposures, SamplesPerUnit, m_Kernel);
                                }
                            }

                            void CRadiometricBayerPatternSynthesizer::ClearHistograms()
                            {
                                memset(m_pUnifiedExposureHistogram, 0, sizeof(int) * 256);
                                memset(m_pRedChannelExposureHistogram, 0, sizeof(int) * 256);
                                memset(m_pGreenChannelExposureHistogram, 0, sizeof(int) * 256);
                                memset(m_pBlueChannelExposureHistogram, 0, sizeof(int) * 256);
                            }

                            void CRadiometricBayerPatternSynthesizer::ClearImages()
                            {
                                m_pDensityWeightedLogRadianceImage->Clear();
                                m_pDensityWeightedImage->Clear();
                                m_pLogRadianceImage->Clear();
                                m_pDensityWeightedExposureImage->Clear();
                                m_pOptimalExposureImage->Clear();
                            }

                            bool CRadiometricBayerPatternSynthesizer::AddDiscreteExposure(const int ParametricIndex)
                            {
                                const Real* ppChannelLUTs[3] = { nullptr };
                                const Real* ppDensityWeigthingKernels[3] = { nullptr };
                                int* ppHistogramChannels[3] = { nullptr };
                                ppChannelLUTs[Imaging::CBayerPattern::eRed] = m_pRedChannelLookUpTable->GetLookUpTable()->GetReadOnlyBufferAtLine(ParametricIndex);
                                ppChannelLUTs[Imaging::CBayerPattern::eGreen] = m_pGreenChannelLookUpTable->GetLookUpTable()->GetReadOnlyBufferAtLine(ParametricIndex);
                                ppChannelLUTs[Imaging::CBayerPattern::eBlue] = m_pBlueChannelLookUpTable->GetLookUpTable()->GetReadOnlyBufferAtLine(ParametricIndex);
                                ppDensityWeigthingKernels[Imaging::CBayerPattern::eRed] = m_pRedChannelLookUpTable->GetWeigthingKernel();
                                ppDensityWeigthingKernels[Imaging::CBayerPattern::eGreen] = m_pGreenChannelLookUpTable->GetWeigthingKernel();
                                ppDensityWeigthingKernels[Imaging::CBayerPattern::eBlue] = m_pBlueChannelLookUpTable->GetWeigthingKernel();
                                ppHistogramChannels[Imaging::CBayerPattern::eRed] = m_pRedChannelExposureHistogram;
                                ppHistogramChannels[Imaging::CBayerPattern::eGreen] = m_pGreenChannelExposureHistogram;
                                ppHistogramChannels[Imaging::CBayerPattern::eBlue] = m_pBlueChannelExposureHistogram;
                                ClearHistograms();
                                const int HalfWidth = m_pDiscreteSourceImage->GetWidth() / 2;
                                const int Height = m_pDiscreteSourceImage->GetHeight();
                                const Byte* pDiscreteSourcePixel = m_pDiscreteSourceImage->GetReadOnlyBuffer();
                                Real* pDensityWeightedPixel = m_pDensityWeightedImage->GetWritableBuffer();
                                Real* pDensityWeightedLogRadiancePixel = m_pDensityWeightedLogRadianceImage->GetWritableBuffer();
                                Imaging::CBayerPattern::ChannelContentMap BayerPatternMap = m_BayerPatternMapLoader.GetBayerPatternMap();
                                if (m_pDiscreteSourceImage->GetWidth() & 0X1)
                                    for (int Y = 0 ; Y < Height ; ++Y)
                                    {
                                        Imaging::CBayerPattern::Channel ChannelA = BayerPatternMap.m_YX[Y & 0X1][0];
                                        Imaging::CBayerPattern::Channel ChannelB = BayerPatternMap.m_YX[Y & 0X1][1];
                                        const Real* pLUTChannelA = ppChannelLUTs[ChannelA];
                                        const Real* pLUTChannelB = ppChannelLUTs[ChannelB];
                                        const Real* pDensityWeigthingKernelA = ppDensityWeigthingKernels[ChannelA];
                                        const Real* pDensityWeigthingKernelB = ppDensityWeigthingKernels[ChannelB];
                                        int* pHistogramChannelA = ppHistogramChannels[ChannelA];
                                        int* pHistogramChannelB = ppHistogramChannels[ChannelB];
                                        for (int X = 0 ; X < HalfWidth ; ++X)
                                        {
                                            if ((*pDiscreteSourcePixel >= m_MinimalIntensity) && (*pDiscreteSourcePixel <= m_MaximalIntensity))
                                            {
                                                ++pHistogramChannelA[*pDiscreteSourcePixel];
                                                *pDensityWeightedPixel++ += pDensityWeigthingKernelA[*pDiscreteSourcePixel];
                                                *pDensityWeightedLogRadiancePixel++ += pLUTChannelA[*pDiscreteSourcePixel++];
                                            }
                                            else
                                            {
                                                ++pDiscreteSourcePixel;
                                                ++pDensityWeightedPixel;
                                                ++pDensityWeightedLogRadiancePixel;
                                            }
                                            if ((*pDiscreteSourcePixel >= m_MinimalIntensity) && (*pDiscreteSourcePixel <= m_MaximalIntensity))
                                            {
                                                ++pHistogramChannelB[*pDiscreteSourcePixel];
                                                *pDensityWeightedPixel++ += pDensityWeigthingKernelB[*pDiscreteSourcePixel];
                                                *pDensityWeightedLogRadiancePixel++ += pLUTChannelB[*pDiscreteSourcePixel++];
                                            }
                                            else
                                            {
                                                ++pDiscreteSourcePixel;
                                                ++pDensityWeightedPixel;
                                                ++pDensityWeightedLogRadiancePixel;
                                            }
                                        }
                                        if ((*pDiscreteSourcePixel >= m_MinimalIntensity) && (*pDiscreteSourcePixel <= m_MaximalIntensity))
                                        {
                                            ++pHistogramChannelA[*pDiscreteSourcePixel];
                                            *pDensityWeightedPixel++ += pDensityWeigthingKernelA[*pDiscreteSourcePixel];
                                            *pDensityWeightedLogRadiancePixel++ += pLUTChannelA[*pDiscreteSourcePixel++];
                                        }
                                        else
                                        {
                                            ++pDiscreteSourcePixel;
                                            ++pDensityWeightedPixel;
                                            ++pDensityWeightedLogRadiancePixel;
                                        }
                                    }
                                else
                                    for (int Y = 0 ; Y < Height ; ++Y)
                                    {
                                        Imaging::CBayerPattern::Channel ChannelA = BayerPatternMap.m_YX[Y & 0X1][0];
                                        Imaging::CBayerPattern::Channel ChannelB = BayerPatternMap.m_YX[Y & 0X1][1];
                                        const Real* pLUTChannelA = ppChannelLUTs[ChannelA];
                                        const Real* pLUTChannelB = ppChannelLUTs[ChannelB];
                                        const Real* pDensityWeigthingKernelA = ppDensityWeigthingKernels[ChannelA];
                                        const Real* pDensityWeigthingKernelB = ppDensityWeigthingKernels[ChannelB];
                                        int* pHistogramChannelA = ppHistogramChannels[ChannelA];
                                        int* pHistogramChannelB = ppHistogramChannels[ChannelB];
                                        for (int X = 0 ; X < HalfWidth ; ++X)
                                        {
                                            if ((*pDiscreteSourcePixel >= m_MinimalIntensity) && (*pDiscreteSourcePixel <= m_MaximalIntensity))
                                            {
                                                ++pHistogramChannelA[*pDiscreteSourcePixel];
                                                *pDensityWeightedPixel++ += pDensityWeigthingKernelA[*pDiscreteSourcePixel];
                                                *pDensityWeightedLogRadiancePixel++ += pLUTChannelA[*pDiscreteSourcePixel++];
                                            }
                                            else
                                            {
                                                ++pDiscreteSourcePixel;
                                                ++pDensityWeightedPixel;
                                                ++pDensityWeightedLogRadiancePixel;
                                            }
                                            if ((*pDiscreteSourcePixel >= m_MinimalIntensity) && (*pDiscreteSourcePixel <= m_MaximalIntensity))
                                            {
                                                ++pHistogramChannelB[*pDiscreteSourcePixel];
                                                *pDensityWeightedPixel++ += pDensityWeigthingKernelB[*pDiscreteSourcePixel];
                                                *pDensityWeightedLogRadiancePixel++ += pLUTChannelB[*pDiscreteSourcePixel++];
                                            }
                                            else
                                            {
                                                ++pDiscreteSourcePixel;
                                                ++pDensityWeightedPixel;
                                                ++pDensityWeightedLogRadiancePixel;
                                            }
                                        }
                                    }
                                m_LastExposureTotalIntegratedPixels = 0;
                                m_LastExposureTotalIntegratedKernelDensity = Real(0);
                                const Real* pRedChannelDensityWeigthingKernel = ppDensityWeigthingKernels[Imaging::CBayerPattern::eRed];
                                const Real* pGreenChannelDensityWeigthingKernel = ppDensityWeigthingKernels[Imaging::CBayerPattern::eGreen];
                                const Real* pBlueChannelDensityWeigthingKernel = ppDensityWeigthingKernels[Imaging::CBayerPattern::eBlue];
                                for (int i = m_MinimalIntensity ; i <= m_MaximalIntensity ; ++i)
                                {
                                    m_LastExposureTotalIntegratedKernelDensity += Real(m_pRedChannelExposureHistogram[i]) * pRedChannelDensityWeigthingKernel[i];
                                    m_LastExposureTotalIntegratedKernelDensity += Real(m_pGreenChannelExposureHistogram[i]) * pGreenChannelDensityWeigthingKernel[i];
                                    m_LastExposureTotalIntegratedKernelDensity += Real(m_pBlueChannelExposureHistogram[i]) * pBlueChannelDensityWeigthingKernel[i];
                                    m_pUnifiedExposureHistogram[i] = m_pRedChannelExposureHistogram[i] + m_pGreenChannelExposureHistogram[i] + m_pBlueChannelExposureHistogram[i];
                                    m_LastExposureTotalIntegratedPixels += m_pUnifiedExposureHistogram[i];
                                }
                                if (m_LastExposureTotalIntegratedPixels)
                                    switch (m_OptimalExposureMode)
                                    {
                                        case eOptimalExposureExtractionDisabled:
                                            break;
                                        case eWeightingDensity:
                                            if (IsPositive(m_LastExposureTotalIntegratedKernelDensity))
                                            {
                                                const Real NormalizedIntegratedKernelDensity = m_LastExposureTotalIntegratedKernelDensity / Real(m_pDiscreteSourceImage->GetArea());
                                                m_OptimalExposureWeighting += NormalizedIntegratedKernelDensity;
                                                const Byte* pDiscreteSourcePixel = m_pDiscreteSourceImage->GetReadOnlyBuffer();
                                                const Real* const pDensityWeightedExposurePixelEnd = m_pDensityWeightedExposureImage->GetBufferEnd();
                                                Real* pDensityWeightedExposurePixel = m_pDensityWeightedExposureImage->GetWritableBuffer();
                                                while (pDensityWeightedExposurePixel < pDensityWeightedExposurePixelEnd)
                                                {
                                                    *pDensityWeightedExposurePixel += Real(*pDiscreteSourcePixel++) * NormalizedIntegratedKernelDensity;
                                                    ++pDensityWeightedExposurePixel;
                                                }
                                            }
                                            break;
                                        case eMaximalDensity:
                                            if (m_LastExposureTotalIntegratedKernelDensity > m_OptimalExposureWeighting)
                                            {
                                                m_OptimalExposureWeighting = m_LastExposureTotalIntegratedKernelDensity;
                                                const Byte* pDiscreteSourcePixel = m_pDiscreteSourceImage->GetReadOnlyBuffer();
                                                const Real* const pDensityWeightedExposurePixelEnd = m_pDensityWeightedExposureImage->GetBufferEnd();
                                                Real* pDensityWeightedExposurePixel = m_pDensityWeightedExposureImage->GetWritableBuffer();
                                                while (pDensityWeightedExposurePixel < pDensityWeightedExposurePixelEnd)
                                                {
                                                    *pDensityWeightedExposurePixel++ = *pDiscreteSourcePixel++;
                                                }
                                            }
                                            break;
                                    }
                                return true;
                            }

                            bool CRadiometricBayerPatternSynthesizer::AddSemiContinousExposure(const int ParametricIndex)
                            {
                                const Real* ppChannelLUTs[3] = { nullptr };
                                const Real* ppDensityWeigthingKernels[3] = { nullptr };
                                int* ppHistogramChannels[3] = { nullptr };
                                ppChannelLUTs[Imaging::CBayerPattern::eRed] = m_pRedChannelLookUpTable->GetLookUpTable()->GetReadOnlyBufferAtLine(ParametricIndex);
                                ppChannelLUTs[Imaging::CBayerPattern::eGreen] = m_pGreenChannelLookUpTable->GetLookUpTable()->GetReadOnlyBufferAtLine(ParametricIndex);
                                ppChannelLUTs[Imaging::CBayerPattern::eBlue] = m_pBlueChannelLookUpTable->GetLookUpTable()->GetReadOnlyBufferAtLine(ParametricIndex);
                                ppDensityWeigthingKernels[Imaging::CBayerPattern::eRed] = m_pRedChannelLookUpTable->GetWeigthingKernel();
                                ppDensityWeigthingKernels[Imaging::CBayerPattern::eGreen] = m_pGreenChannelLookUpTable->GetWeigthingKernel();
                                ppDensityWeigthingKernels[Imaging::CBayerPattern::eBlue] = m_pBlueChannelLookUpTable->GetWeigthingKernel();
                                ppHistogramChannels[Imaging::CBayerPattern::eRed] = m_pRedChannelExposureHistogram;
                                ppHistogramChannels[Imaging::CBayerPattern::eGreen] = m_pGreenChannelExposureHistogram;
                                ppHistogramChannels[Imaging::CBayerPattern::eBlue] = m_pBlueChannelExposureHistogram;
                                ClearHistograms();
                                const int HalfWidth = m_pContinousSourceImage->GetWidth() / 2;
                                const int Height = m_pContinousSourceImage->GetHeight();
                                const int SamplesPerUnit = m_pRedChannelLookUpTable->GetSamplesPerUnit();
                                const Real* pContinousSourcePixel = m_pContinousSourceImage->GetReadOnlyBuffer();
                                Real* pDensityWeightedPixel = m_pDensityWeightedImage->GetWritableBuffer();
                                Real* ppDensityWeightedLogRadiancePixel = m_pDensityWeightedLogRadianceImage->GetWritableBuffer();
                                Imaging::CBayerPattern::ChannelContentMap BayerPatternMap = m_BayerPatternMapLoader.GetBayerPatternMap();
                                if (m_pContinousSourceImage->GetWidth() & 0X1)
                                    for (int Y = 0 ; Y < Height ; ++Y)
                                    {
                                        Imaging::CBayerPattern::Channel ChannelA = BayerPatternMap.m_YX[Y & 0X1][0];
                                        Imaging::CBayerPattern::Channel ChannelB = BayerPatternMap.m_YX[Y & 0X1][1];
                                        const Real* pLUTChannelA = ppChannelLUTs[ChannelA];
                                        const Real* pLUTChannelB = ppChannelLUTs[ChannelB];
                                        const Real* pDensityWeigthingKernelA = ppDensityWeigthingKernels[ChannelA];
                                        const Real* pDensityWeigthingKernelB = ppDensityWeigthingKernels[ChannelB];
                                        int* pHistogramChannelA = ppHistogramChannels[ChannelA];
                                        int* pHistogramChannelB = ppHistogramChannels[ChannelB];
                                        for (int X = 0 ; X < HalfWidth ; ++X)
                                        {
                                            if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                            {
                                                ++pHistogramChannelA[int(std::round(*pContinousSourcePixel))];
                                                const int LUTIndex = int(std::round(*pContinousSourcePixel++ * SamplesPerUnit));
                                                const Real DensityWeight = pDensityWeigthingKernelA[LUTIndex];
                                                m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                                *pDensityWeightedPixel++ += DensityWeight;
                                                *ppDensityWeightedLogRadiancePixel++ += pLUTChannelA[LUTIndex];
                                            }
                                            else
                                            {
                                                ++pContinousSourcePixel;
                                                ++pDensityWeightedPixel;
                                                ++ppDensityWeightedLogRadiancePixel;
                                            }
                                            if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                            {
                                                ++pHistogramChannelB[int(std::round(*pContinousSourcePixel))];
                                                const int LUTIndex = int(std::round(*pContinousSourcePixel++ * SamplesPerUnit));
                                                const Real DensityWeight = pDensityWeigthingKernelB[LUTIndex];
                                                m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                                *pDensityWeightedPixel++ += DensityWeight;
                                                *ppDensityWeightedLogRadiancePixel++ += pLUTChannelB[LUTIndex];
                                            }
                                            else
                                            {
                                                ++pContinousSourcePixel;
                                                ++pDensityWeightedPixel;
                                                ++ppDensityWeightedLogRadiancePixel;
                                            }
                                        }
                                        if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                        {
                                            ++pHistogramChannelA[int(std::round(*pContinousSourcePixel))];
                                            const int LUTIndex = int(std::round(*pContinousSourcePixel++ * SamplesPerUnit));
                                            const Real DensityWeight = pDensityWeigthingKernelA[LUTIndex];
                                            m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                            *pDensityWeightedPixel++ += DensityWeight;
                                            *ppDensityWeightedLogRadiancePixel++ += pLUTChannelA[LUTIndex];
                                        }
                                        else
                                        {
                                            ++pContinousSourcePixel;
                                            ++pDensityWeightedPixel;
                                            ++ppDensityWeightedLogRadiancePixel;
                                        }
                                    }
                                else
                                    for (int Y = 0 ; Y < Height ; ++Y)
                                    {
                                        Imaging::CBayerPattern::Channel ChannelA = BayerPatternMap.m_YX[Y & 0X1][0];
                                        Imaging::CBayerPattern::Channel ChannelB = BayerPatternMap.m_YX[Y & 0X1][1];
                                        const Real* pLUTChannelA = ppChannelLUTs[ChannelA];
                                        const Real* pLUTChannelB = ppChannelLUTs[ChannelB];
                                        const Real* pDensityWeigthingKernelA = ppDensityWeigthingKernels[ChannelA];
                                        const Real* pDensityWeigthingKernelB = ppDensityWeigthingKernels[ChannelB];
                                        int* pHistogramChannelA = ppHistogramChannels[ChannelA];
                                        int* pHistogramChannelB = ppHistogramChannels[ChannelB];
                                        for (int X = 0 ; X < HalfWidth ; ++X)
                                        {
                                            if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                            {
                                                ++pHistogramChannelA[int(std::round(*pContinousSourcePixel))];
                                                const int LUTIndex = int(std::round(*pContinousSourcePixel++ * SamplesPerUnit));
                                                const Real DensityWeight = pDensityWeigthingKernelA[LUTIndex];
                                                m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                                *pDensityWeightedPixel++ += DensityWeight;
                                                *ppDensityWeightedLogRadiancePixel++ += pLUTChannelA[LUTIndex];
                                            }
                                            else
                                            {
                                                ++pContinousSourcePixel;
                                                ++pDensityWeightedPixel;
                                                ++ppDensityWeightedLogRadiancePixel;
                                            }
                                            if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                            {
                                                ++pHistogramChannelB[int(std::round(*pContinousSourcePixel))];
                                                const int LUTIndex = int(std::round(*pContinousSourcePixel++ * SamplesPerUnit));
                                                const Real DensityWeight = pDensityWeigthingKernelB[LUTIndex];
                                                m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                                *pDensityWeightedPixel++ += DensityWeight;
                                                *ppDensityWeightedLogRadiancePixel++ += pLUTChannelB[LUTIndex];
                                            }
                                            else
                                            {
                                                ++pContinousSourcePixel;
                                                ++pDensityWeightedPixel;
                                                ++ppDensityWeightedLogRadiancePixel;
                                            }
                                        }
                                    }
                                m_LastExposureTotalIntegratedPixels = 0;
                                for (int i = m_MinimalIntensity ; i <= m_MaximalIntensity ; ++i)
                                {
                                    m_pUnifiedExposureHistogram[i] = m_pRedChannelExposureHistogram[i] + m_pGreenChannelExposureHistogram[i] + m_pBlueChannelExposureHistogram[i];
                                    m_LastExposureTotalIntegratedPixels += m_pUnifiedExposureHistogram[i];
                                }
                                if (m_LastExposureTotalIntegratedPixels)
                                    switch (m_OptimalExposureMode)
                                    {
                                        case eOptimalExposureExtractionDisabled:
                                            break;
                                        case eWeightingDensity:
                                            if (IsPositive(m_LastExposureTotalIntegratedKernelDensity))
                                            {
                                                const Real NormalizedIntegratedKernelDensity = m_LastExposureTotalIntegratedKernelDensity / Real(m_pContinousSourceImage->GetArea());
                                                m_OptimalExposureWeighting += NormalizedIntegratedKernelDensity;
                                                const Real* pContinousSourcePixel = m_pContinousSourceImage->GetReadOnlyBuffer();
                                                const Real* const pDensityWeightedExposureEnd = m_pDensityWeightedExposureImage->GetBufferEnd();
                                                Real* pDensityWeightedExposure = m_pDensityWeightedExposureImage->GetWritableBuffer();
                                                while (pDensityWeightedExposure < pDensityWeightedExposureEnd)
                                                {
                                                    *pDensityWeightedExposure++ = *pContinousSourcePixel++ * NormalizedIntegratedKernelDensity;
                                                }
                                            }
                                            break;
                                        case eMaximalDensity:
                                            if (m_LastExposureTotalIntegratedKernelDensity > m_OptimalExposureWeighting)
                                            {
                                                m_OptimalExposureWeighting = m_LastExposureTotalIntegratedKernelDensity;
                                                m_pDensityWeightedExposureImage->Copy(m_pContinousSourceImage, true, nullptr);
                                            }
                                            break;
                                    }

                                return true;
                            }

                            bool CRadiometricBayerPatternSynthesizer::AddContinousExposure(const int ParametricIndex)
                            {
                                Real ChannelSlopes[3] = { Real(0) };
                                Real ChannelOffsets[3] = { Real(0) };
                                const Base::CRadianceWeigthingKernel* ppChannelRadianceWeigthingKernels[3] = { nullptr };
                                int* ppHistogramChannels[3] = { nullptr };
                                const CRadiometricResponceFunction* pRedChannelResponceFunction = m_pMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction();
                                const CRadiometricResponceFunction* pGreenChannelResponceFunction = m_pMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction();
                                const CRadiometricResponceFunction* pBlueChannelResponceFunction = m_pMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction();
                                ChannelSlopes[Imaging::CBayerPattern::eRed] = pRedChannelResponceFunction->GetIntensitySlope();
                                ChannelSlopes[Imaging::CBayerPattern::eGreen] = pGreenChannelResponceFunction->GetIntensitySlope();
                                ChannelSlopes[Imaging::CBayerPattern::eBlue] = pBlueChannelResponceFunction->GetIntensitySlope();
                                ChannelOffsets[Imaging::CBayerPattern::eRed] = pRedChannelResponceFunction->GetIntensityOffset();
                                ChannelOffsets[Imaging::CBayerPattern::eGreen] = pGreenChannelResponceFunction->GetIntensityOffset();
                                ChannelOffsets[Imaging::CBayerPattern::eBlue] = pBlueChannelResponceFunction->GetIntensityOffset();
                                ppChannelRadianceWeigthingKernels[Imaging::CBayerPattern::eRed] = pRedChannelResponceFunction->GetRadianceWeigthingKernel();
                                ppChannelRadianceWeigthingKernels[Imaging::CBayerPattern::eGreen] = pGreenChannelResponceFunction->GetRadianceWeigthingKernel();
                                ppChannelRadianceWeigthingKernels[Imaging::CBayerPattern::eBlue] = pBlueChannelResponceFunction->GetRadianceWeigthingKernel();
                                ppHistogramChannels[Imaging::CBayerPattern::eRed] = m_pRedChannelExposureHistogram;
                                ppHistogramChannels[Imaging::CBayerPattern::eGreen] = m_pGreenChannelExposureHistogram;
                                ppHistogramChannels[Imaging::CBayerPattern::eBlue] = m_pBlueChannelExposureHistogram;
                                ClearHistograms();
                                const Real LogTime = m_LogTimeIndexMap[ParametricIndex];
                                const int HalfWidth = m_pContinousSourceImage->GetWidth() / 2;
                                const int Height = m_pContinousSourceImage->GetHeight();
                                const Real* pContinousSourcePixel = m_pContinousSourceImage->GetReadOnlyBuffer();
                                Real* pDensityWeightedPixel = m_pDensityWeightedImage->GetWritableBuffer();
                                Real* DensityWeightedLogRadiancePixel = m_pDensityWeightedLogRadianceImage->GetWritableBuffer();
                                Imaging::CBayerPattern::ChannelContentMap BayerPatternMap = m_BayerPatternMapLoader.GetBayerPatternMap();
                                if (m_pContinousSourceImage->GetWidth() & 0X1)
                                    for (int Y = 0 ; Y < Height ; ++Y)
                                    {
                                        Imaging::CBayerPattern::Channel ChannelA = BayerPatternMap.m_YX[Y & 0X1][0];
                                        Imaging::CBayerPattern::Channel ChannelB = BayerPatternMap.m_YX[Y & 0X1][1];
                                        const Real SlopeChannelA = ChannelSlopes[ChannelA];
                                        const Real SlopeChannelB = ChannelSlopes[ChannelB];
                                        const Real OffsetChannelA = ChannelOffsets[ChannelA];
                                        const Real OffsetChannelB = ChannelOffsets[ChannelB];
                                        const Base::CRadianceWeigthingKernel* pRadianceWeigthingKernelA = ppChannelRadianceWeigthingKernels[ChannelA];
                                        const Base::CRadianceWeigthingKernel* pRadianceWeigthingKernelB = ppChannelRadianceWeigthingKernels[ChannelB];
                                        int* pHistogramChannelA = ppHistogramChannels[ChannelA];
                                        int* pHistogramChannelB = ppHistogramChannels[ChannelB];
                                        for (int X = 0 ; X < HalfWidth ; ++X)
                                        {
                                            if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))

                                            {
                                                ++pHistogramChannelA[int(std::round(*pContinousSourcePixel))];
                                                const Real DensityWeight = pRadianceWeigthingKernelA->GetRegressionWeigthingByKernel(*pContinousSourcePixel, m_Kernel);
                                                *pDensityWeightedPixel++ += DensityWeight;
                                                *DensityWeightedLogRadiancePixel++ += (std::log(SlopeChannelA * *pContinousSourcePixel++ + OffsetChannelA) - LogTime) * DensityWeight;
                                                m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                            }
                                            else
                                            {
                                                ++pContinousSourcePixel;
                                                ++pDensityWeightedPixel;
                                                ++DensityWeightedLogRadiancePixel;
                                            }
                                            if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                            {
                                                ++pHistogramChannelB[int(std::round(*pContinousSourcePixel))];
                                                const Real DensityWeight = pRadianceWeigthingKernelB->GetRegressionWeigthingByKernel(*pContinousSourcePixel, m_Kernel);
                                                *pDensityWeightedPixel++ += DensityWeight;
                                                *DensityWeightedLogRadiancePixel++ += (std::log(SlopeChannelB * *pContinousSourcePixel++ + OffsetChannelB) - LogTime) * DensityWeight;
                                                m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                            }
                                            else
                                            {
                                                ++pContinousSourcePixel;
                                                ++pDensityWeightedPixel;
                                                ++DensityWeightedLogRadiancePixel;
                                            }
                                        }
                                        if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                        {
                                            ++pHistogramChannelA[int(std::round(*pContinousSourcePixel))];
                                            const Real DensityWeight = pRadianceWeigthingKernelA->GetRegressionWeigthingByKernel(*pContinousSourcePixel, m_Kernel);
                                            *pDensityWeightedPixel++ += DensityWeight;
                                            *DensityWeightedLogRadiancePixel++ += (std::log(SlopeChannelA * *pContinousSourcePixel++ + OffsetChannelA) - LogTime) * DensityWeight;
                                            m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                        }
                                        else
                                        {
                                            ++pContinousSourcePixel;
                                            ++pDensityWeightedPixel;
                                            ++DensityWeightedLogRadiancePixel;
                                        }
                                    }
                                else
                                    for (int Y = 0 ; Y < Height ; ++Y)
                                    {
                                        Imaging::CBayerPattern::Channel ChannelA = BayerPatternMap.m_YX[Y & 0X1][0];
                                        Imaging::CBayerPattern::Channel ChannelB = BayerPatternMap.m_YX[Y & 0X1][1];
                                        const Real SlopeChannelA = ChannelSlopes[ChannelA];
                                        const Real SlopeChannelB = ChannelSlopes[ChannelB];
                                        const Real OffsetChannelA = ChannelOffsets[ChannelA];
                                        const Real OffsetChannelB = ChannelOffsets[ChannelB];
                                        const Base::CRadianceWeigthingKernel* pRadianceWeigthingKernelA = ppChannelRadianceWeigthingKernels[ChannelA];
                                        const Base::CRadianceWeigthingKernel* pRadianceWeigthingKernelB = ppChannelRadianceWeigthingKernels[ChannelB];
                                        int* pHistogramChannelA = ppHistogramChannels[ChannelA];
                                        int* pHistogramChannelB = ppHistogramChannels[ChannelB];
                                        for (int X = 0 ; X < HalfWidth ; ++X)
                                        {
                                            if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                            {
                                                ++pHistogramChannelA[int(std::round(*pContinousSourcePixel))];
                                                const Real DensityWeight = pRadianceWeigthingKernelA->GetRegressionWeigthingByKernel(*pContinousSourcePixel, m_Kernel);
                                                *pDensityWeightedPixel++ += DensityWeight;
                                                *DensityWeightedLogRadiancePixel++ += (std::log(SlopeChannelA * *pContinousSourcePixel++ + OffsetChannelA) - LogTime) * DensityWeight;
                                                m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                            }
                                            else
                                            {
                                                ++pContinousSourcePixel;
                                                ++pDensityWeightedPixel;
                                                ++DensityWeightedLogRadiancePixel;
                                            }
                                            if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                            {
                                                ++pHistogramChannelB[int(std::round(*pContinousSourcePixel))];
                                                const Real DensityWeight = pRadianceWeigthingKernelB->GetRegressionWeigthingByKernel(*pContinousSourcePixel, m_Kernel);
                                                *pDensityWeightedPixel++ += DensityWeight;
                                                *DensityWeightedLogRadiancePixel++ += (std::log(SlopeChannelB * *pContinousSourcePixel++ + OffsetChannelB) - LogTime) * DensityWeight;
                                                m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                            }
                                            else
                                            {
                                                ++pContinousSourcePixel;
                                                ++pDensityWeightedPixel;
                                                ++DensityWeightedLogRadiancePixel;
                                            }
                                        }
                                    }
                                m_LastExposureTotalIntegratedPixels = 0;
                                for (int i = m_MinimalIntensity ; i <= m_MaximalIntensity ; ++i)
                                {
                                    m_pUnifiedExposureHistogram[i] = m_pRedChannelExposureHistogram[i] + m_pGreenChannelExposureHistogram[i] + m_pBlueChannelExposureHistogram[i];
                                    m_LastExposureTotalIntegratedPixels += m_pUnifiedExposureHistogram[i];
                                }
                                if (m_LastExposureTotalIntegratedPixels)
                                    switch (m_OptimalExposureMode)
                                    {
                                        case eOptimalExposureExtractionDisabled:
                                            break;
                                        case eWeightingDensity:
                                            if (IsPositive(m_LastExposureTotalIntegratedKernelDensity))
                                            {
                                                const Real NormalizedIntegratedKernelDensity = m_LastExposureTotalIntegratedKernelDensity / Real(m_pContinousSourceImage->GetArea());
                                                m_OptimalExposureWeighting += NormalizedIntegratedKernelDensity;
                                                const Real* pContinousSourcePixel = m_pContinousSourceImage->GetReadOnlyBuffer();
                                                const Real* const pDensityWeightedExposureEnd = m_pDensityWeightedExposureImage->GetBufferEnd();
                                                Real* pDensityWeightedExposure = m_pDensityWeightedExposureImage->GetWritableBuffer();
                                                while (pDensityWeightedExposure < pDensityWeightedExposureEnd)
                                                {
                                                    *pDensityWeightedExposure++ = *pContinousSourcePixel++ * NormalizedIntegratedKernelDensity;
                                                }
                                            }
                                            break;
                                        case eMaximalDensity:
                                            if (m_LastExposureTotalIntegratedKernelDensity > m_OptimalExposureWeighting)
                                            {
                                                m_OptimalExposureWeighting = m_LastExposureTotalIntegratedKernelDensity;
                                                m_pDensityWeightedExposureImage->Copy(m_pContinousSourceImage, true, nullptr);
                                            }
                                            break;
                                    }
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }
}
