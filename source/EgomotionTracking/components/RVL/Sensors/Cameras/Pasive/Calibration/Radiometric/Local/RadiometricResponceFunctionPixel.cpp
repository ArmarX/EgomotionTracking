/*
 * RadiometricResponceFunctionPixel.cpp
 */

#include "RadiometricResponceFunctionPixel.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Local
                        {
                            CRadiometricResponceFunctionPixel::CRadiometricResponceFunctionPixel() :
                                m_Slope(Real(0)),
                                m_Offset(Real(0)),
                                m_RMSDeviation(Real(0)),
                                m_PeakNegativeDeviation(Real(0)),
                                m_PeakPositiveDeviation(Real(0))
                            {
                            }

                            CRadiometricResponceFunctionPixel::~CRadiometricResponceFunctionPixel()
                                = default;

                            bool CRadiometricResponceFunctionPixel::IsValid() const
                            {
                                return IsPositive(m_Slope);
                            }

                            bool CRadiometricResponceFunctionPixel::SetFromRadiometricPixel(const CReferencedRadiometricPixel* pReferencedRadiometricPixel)
                            {
                                if (pReferencedRadiometricPixel)
                                {
                                    m_Slope = pReferencedRadiometricPixel->GetSlope();
                                    m_Offset = pReferencedRadiometricPixel->GetOffset();
                                    m_RMSDeviation = pReferencedRadiometricPixel->GetRMSDeviation();
                                    m_PeakNegativeDeviation = pReferencedRadiometricPixel->GetPeakNegativeDeviation();
                                    m_PeakPositiveDeviation = pReferencedRadiometricPixel->GetPeakPositiveDeviation();
                                    return true;
                                }
                                return false;
                            }

                            Real CRadiometricResponceFunctionPixel::GetSlope() const
                            {
                                return m_Slope;
                            }

                            Real CRadiometricResponceFunctionPixel::GetOffset() const
                            {
                                return m_Offset;
                            }

                            Real CRadiometricResponceFunctionPixel::GetRMSDeviation() const
                            {
                                return m_RMSDeviation;
                            }

                            Real CRadiometricResponceFunctionPixel::GetRangeDeviation() const
                            {
                                return m_PeakPositiveDeviation - m_PeakNegativeDeviation;
                            }

                            Real CRadiometricResponceFunctionPixel::GetPeakNegativeDeviation() const
                            {
                                return m_PeakNegativeDeviation;
                            }

                            Real CRadiometricResponceFunctionPixel::GetPeakPositiveDeviation() const
                            {
                                return m_PeakPositiveDeviation;
                            }
                        }
                    }
                }
            }
        }
    }
}

