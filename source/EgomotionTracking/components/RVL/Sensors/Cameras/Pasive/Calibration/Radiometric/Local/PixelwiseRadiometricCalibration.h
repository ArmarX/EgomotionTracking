/*
 * PixelwiseRadiometricCalibration.h
 */

#pragma once

#include "../../../../../../Imaging/BayerPattern.h"
#include "../../../../../../Imaging/TImage.hpp"
#include "../Base/Exposure.h"
#include "../Base/CalibratedExposure.h"
#include "../Base/RadianceWeigthingKernel.h"
#include "../Global/RadiometricResponceFunction.h"
#include "RadiometricResponceFunctionPixel.h"
#include "ReferencedRadiometricPixel.h"
#include "PixelwiseRadiometricResponceFunction.h"

#define _PIXELWISE_RADIOMETRIC_CALIBRATION_MINIMAL_EXPOSURES_ 4
#define _PIXELWISE_RADIOMETRIC_CALIBRATION_MINIMAL_SELECTED_SAMPLING_CELLS_ 32
#define _PIXELWISE_RADIOMETRIC_CALIBRATION_MINIMAL_GAUSSIAN_BOX_RADIUS_ Real(8)
#define _PIXELWISE_RADIOMETRIC_CALIBRATION_MAXIMAL_GAUSSIAN_BOX_RADIUS_ Real(32)
#define _PIXELWISE_RADIOMETRIC_CALIBRATION_MINIMAL_DEFAULT_GAUSSIAN_BOX_RADIUS_ Real(16)
#define _PIXELWISE_RADIOMETRIC_CALIBRATION_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_DENSITY_ Real(0.01)
#define _PIXELWISE_RADIOMETRIC_CALIBRATION_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_NORMALIZED_DEVIATION_ Real(0.95)

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Local
                        {
                            class CPixelwiseRadiometricCalibration
                            {
                            public:

                                CPixelwiseRadiometricCalibration(const Global::CRadiometricResponceFunction* pGlobalRadiometricResponceFunction, const Imaging::TImage<Real>* pRadianceImage);
                                virtual ~CPixelwiseRadiometricCalibration();

                                bool IsEnabled() const;
                                uint64_t GetCameraId() const;
                                Imaging::CBayerPattern::BayerPatternType GetBayerPattern() const;
                                Imaging::CBayerPattern::Channel GetChannelContent() const;
                                const CPixelwiseRadiometricResponceFunction* GetLocalRadiometricResponceFunction() const;

                                bool AddExposure(const int ReferenceIndex, const int ExposureIndex, const Real ExposureTime);
                                bool ClearExposures();

                                bool SetGaussianBoxKernelRadius(const Real GaussianBoxKernelRadius);
                                bool SetCommonGaussianKernelStandardDeviation(const Real StandardDeviation);
                                bool SetCommonGaussianKernelCutOff(const Real CutOffDensity, const Real CutOffNormalizedDeviation);

                                Real GetGaussianBoxKernelRadius() const;
                                Real GetCommonGaussianKernelStandardDeviation() const;
                                bool Calibrate(const Base::CRadianceWeigthingKernel::KernelType Kernel);

                            protected:

                                struct ReferenceExposureImage
                                {
                                    int m_ReferenceIndex;
                                    int m_ExposureIndex;
                                    Real m_ExposureTime;
                                    Imaging::TImage<Real>* m_pRadianceImage;
                                };

                                bool LoadSamples(const Base::CRadianceWeigthingKernel::KernelType Kernel);
                                void ClearSamples();
                                void LoadReferencedSamples(std::list<ReferenceExposureImage>& BlockExposureImages);
                                void AddSynthesizeExposure(const Imaging::TImage<Real>* pRadianceImage, const Real ExposureTime, const Base::CRadianceWeigthingKernel::KernelType Kernel);
                                void Synthesize();
                                void ClearSynthesize();

                                static bool SortReferenceExposureImages(const ReferenceExposureImage& lhs, const ReferenceExposureImage& rhs);

                                const Global::CRadiometricResponceFunction* m_pRadiometricResponceFunction;
                                const Imaging::TImage<Real>* m_pRadianceImage;
                                Imaging::TImage<Real>* m_pRadianceImageIntern;
                                Imaging::TImage<Real>* m_pRadianceReferenceImage;
                                Imaging::TImage<Real>* m_pWeightedRadianceAccumulatorImage;
                                Imaging::TImage<Real>* m_pWeightingAccumulatorImage;
                                Imaging::TImage<CReferencedRadiometricPixel>* m_pRadiometricImage;
                                CPixelwiseRadiometricResponceFunction* m_pLocalRadiometricResponceFunction;
                                std::list<CReferencedRadiometricPixel*> m_RadiometricPixels;
                                Base::CRadianceWeigthingKernel m_RadianceWeigthingKernel;
                                std::list<ReferenceExposureImage> m_ReferenceExposureImages;
                            };
                        }
                    }
                }
            }
        }
    }
}

