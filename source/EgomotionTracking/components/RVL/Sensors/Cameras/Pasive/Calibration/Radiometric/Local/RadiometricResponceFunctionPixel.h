/*
 * RadiometricResponceFunctionPixel.h
 */

#pragma once

#include "ReferencedRadiometricPixel.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Local
                        {
                            class CRadiometricResponceFunctionPixel
                            {
                            public:

                                CRadiometricResponceFunctionPixel();
                                virtual ~CRadiometricResponceFunctionPixel();

                                bool IsValid() const;
                                bool SetFromRadiometricPixel(const CReferencedRadiometricPixel* pReferencedRadiometricPixel);

                                Real GetSlope() const;
                                Real GetOffset() const;
                                Real GetRMSDeviation() const;
                                Real GetRangeDeviation() const;
                                Real GetPeakNegativeDeviation() const;
                                Real GetPeakPositiveDeviation() const;

                            protected:

                                Real m_Slope;
                                Real m_Offset;
                                Real m_RMSDeviation;
                                Real m_PeakNegativeDeviation;
                                Real m_PeakPositiveDeviation;
                            };
                        }
                    }
                }
            }
        }
    }
}

