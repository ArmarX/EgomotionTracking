/*
 * RadiometricBayerPatternSynthesizer.h
 */

#pragma once

#include "../../../../../../Imaging/BayerPattern.h"
#include "../../../../../../Imaging/TImage.hpp"
#include "../Base/Exposure.h"
#include "../Base/CalibratedExposure.h"
#include "../Base/RadianceWeigthingKernel.h"
#include "MultipleChannelRadiometricResponceFunction.h"
#include "RadiometricResponceFunctionLookUpTable.h"

#define _RADIOMETRIC_SYNTHESIZER_MINIMAL_INTEGRATED_EXPOSURES_  4

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            class CRadiometricBayerPatternSynthesizer
                            {
                            public:

                                enum OptimalExposureExtractionMode
                                {
                                    eOptimalExposureExtractionDisabled, eWeightingDensity, eMaximalDensity
                                };

                                CRadiometricBayerPatternSynthesizer(const Imaging::TImage<Real>* pContinousSourceImage, const CMultipleChannelRadiometricResponceFunction* pMultipleChannelRadiometricResponceFunction, const std::vector<Base::CExposure>& Exposures, const int SamplesPerUnit = 0);
                                CRadiometricBayerPatternSynthesizer(const Imaging::TImage<Byte>* pDiscreteSourceImage, const CMultipleChannelRadiometricResponceFunction* pMultipleChannelRadiometricResponceFunction, const std::vector<Base::CExposure>& Exposures);
                                virtual ~CRadiometricBayerPatternSynthesizer();

                                bool StartExposureBracketing();
                                bool AddExposure(const int ParametricIndex);
                                bool Synthesize();
                                bool GetSynthesizeDensityStatistics(Real& DensityAccumulator, Real& MaximalDensity, Real& MinimalDensity, Real& MeanDensity, Real& StandarDeviationDensity, int& TotalCriticalPixels, const Real CriticalDensity = g_RealPlusEpsilon) const;
                                bool GetSynthesizeRadianceStatistics(Real& MaximalRadiance, Real& MinimalRadiance, Real& MeanRadiance, Real& StandarDeviationRadiance, int& TotalEncodingBits) const;
                                bool ExtractOptimalExposureImage();
                                const std::vector<Base::CExposure>& GetExposures() const;

                                bool SetOptimalExposureExtractionMode(const OptimalExposureExtractionMode Mode);

                                const Imaging::TImage<Real>* GetLogRadianceImage() const;
                                const Imaging::TImage<Real>* GetDensityWeightedImage() const;
                                const Imaging::TImage<Real>* GetOptimalExposureImage() const;
                                bool DisplayLinealScaledRadianceImage(Imaging::TImage<Byte>* pDisplay) const;
                                bool DisplayOptimalExposureImage(Imaging::TImage<Byte>* pDisplay) const;

                                uint64_t GetCameraId() const;
                                Imaging::CBayerPattern::BayerPatternType GetBayerPattern() const;

                                const int* GetUnifiedExposureHistogram() const;
                                const int* GetRedChannelExposureHistogram() const;
                                const int* GetGreenChannelExposureHistogram() const;
                                const int* GetBlueChannelExposureHistogram() const;

                                bool IsEnabled() const;
                                const CMultipleChannelRadiometricResponceFunction* GetMultipleChannelRadiometricResponceFunction() const;
                                int GetTotalIntegratedExposures() const;
                                int GetLastExposureTotalIntegratedPixels() const;
                                Real GetLastExposureTotalIntegratedKernelDensity() const;
                                int GetLastSynthesizeTotalUnsampledPixels() const;

                            protected:

                                static const std::vector<Base::CExposure> EnsureExposuresOrder(const std::vector<Base::CExposure>& Exposures);

                                void LoadIntensityLimits(const Real IntensityMargin);
                                void LoadLogTimesTable();
                                void LoadLookUpTables(const int SamplesPerUnit);
                                void ClearHistograms();
                                void ClearImages();
                                bool AddDiscreteExposure(const int ParametricIndex);
                                bool AddSemiContinousExposure(const int ParametricIndex);
                                bool AddContinousExposure(const int ParametricIndex);

                                bool m_Enabled;
                                const std::vector<Base::CExposure> m_Exposures;
                                const Imaging::TImage<Byte>* m_pDiscreteSourceImage;
                                const Imaging::TImage<Real>* m_pContinousSourceImage;
                                const CMultipleChannelRadiometricResponceFunction* m_pMultipleChannelRadiometricResponceFunction;
                                Imaging::TImage<Real>* m_pDensityWeightedLogRadianceImage;
                                Imaging::TImage<Real>* m_pDensityWeightedImage;
                                Imaging::TImage<Real>* m_pLogRadianceImage;
                                Imaging::TImage<Real>* m_pDensityWeightedExposureImage;
                                Imaging::TImage<Real>* m_pOptimalExposureImage;
                                CRadiometricResponceFunctionLookUpTable* m_pRedChannelLookUpTable;
                                CRadiometricResponceFunctionLookUpTable* m_pGreenChannelLookUpTable;
                                CRadiometricResponceFunctionLookUpTable* m_pBlueChannelLookUpTable;
                                std::map<int, Real> m_LogTimeIndexMap;
                                int* m_pUnifiedExposureHistogram;
                                int* m_pRedChannelExposureHistogram;
                                int* m_pGreenChannelExposureHistogram;
                                int* m_pBlueChannelExposureHistogram;
                                int m_TotalIntegratedExposures;
                                int m_MinimalExposureParametricIndex;
                                int m_MaximalExposureParametricIndex;
                                int m_LastExposureTotalIntegratedPixels;
                                int m_LastSynthesizeTotalUnsampledPixels;
                                Real m_MinimalIntensity;
                                Real m_MaximalIntensity;
                                Real m_LastExposureTotalIntegratedKernelDensity;
                                Real m_OptimalExposureWeighting;
                                OptimalExposureExtractionMode m_OptimalExposureMode;
                                Base::CRadianceWeigthingKernel::KernelType m_Kernel;
                                Imaging::CBayerPattern m_BayerPatternMapLoader;
                            };
                        }
                    }
                }
            }
        }
    }
}
