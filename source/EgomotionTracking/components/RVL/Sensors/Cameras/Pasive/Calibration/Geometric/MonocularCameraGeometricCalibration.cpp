/*
 * MonocularCameraGeometricCalibration.cpp
 */

#include "MonocularCameraGeometricCalibration.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Geometric
                    {

                        CMonocularCameraGeometricCalibration::CMonocularCameraGeometricCalibration() :
                            m_ImageSize(),
                            m_FocalLength(),
                            m_ImagePrincipalPoint(),
                            m_Translation(),
                            m_Rotation(Mathematics::_3D::CMatrix3D::s_Identity),
                            m_TranslationInverse(),
                            m_RotationInverse(Mathematics::_3D::CMatrix3D::s_Identity),
                            m_MinimalObjectDistance(Real(1))
                        {
                            memset(m_DistortionCoefficients, 0, sizeof(Real) * _CAMERA_GEOMETRIC_CALIBRATION_TOTAL_DISTORTION_COEFFICIENTS_);
                        }

                        CMonocularCameraGeometricCalibration::CMonocularCameraGeometricCalibration(const CMonocularCameraGeometricCalibration* pMonocularCameraGeometricCalibration) :
                            m_ImageSize(),
                            m_FocalLength(),
                            m_ImagePrincipalPoint(),
                            m_Translation(),
                            m_Rotation(Mathematics::_3D::CMatrix3D::s_Identity),
                            m_TranslationInverse(),
                            m_RotationInverse(Mathematics::_3D::CMatrix3D::s_Identity),
                            m_MinimalObjectDistance(Real(1))
                        {
                            if (pMonocularCameraGeometricCalibration)
                            {
                                m_ImageSize = pMonocularCameraGeometricCalibration->m_ImageSize;
                                m_FocalLength = pMonocularCameraGeometricCalibration->m_FocalLength;
                                m_ImagePrincipalPoint = pMonocularCameraGeometricCalibration->m_ImagePrincipalPoint;
                                m_Translation = pMonocularCameraGeometricCalibration->m_Translation;
                                m_Rotation = pMonocularCameraGeometricCalibration->m_Rotation;
                                m_TranslationInverse = pMonocularCameraGeometricCalibration->m_TranslationInverse;
                                m_RotationInverse = pMonocularCameraGeometricCalibration->m_RotationInverse;
                                m_MinimalObjectDistance = pMonocularCameraGeometricCalibration->m_MinimalObjectDistance;
                                memcpy(m_DistortionCoefficients, pMonocularCameraGeometricCalibration->m_DistortionCoefficients, sizeof(Real) * _CAMERA_GEOMETRIC_CALIBRATION_TOTAL_DISTORTION_COEFFICIENTS_);
                            }
                        }

                        CMonocularCameraGeometricCalibration::~CMonocularCameraGeometricCalibration()
                            = default;

                        bool CMonocularCameraGeometricCalibration::SetMinimalObjectDistance(const Real MinimalObjectDistance)
                        {
                            if (IsPositive(MinimalObjectDistance))
                            {
                                m_MinimalObjectDistance = MinimalObjectDistance;
                                return true;
                            }
                            return false;
                        }

                        bool CMonocularCameraGeometricCalibration::LoadFromFile(const std::string& FileName, const int TargetCalibrationIndex, const bool ResetExtrinsic)
                        {
                            if (Files::CFile::Exists(FileName))
                            {
                                Files::CInFile InputFile(FileName, Files::CFile::eText);
                                if (InputFile.IsReady())
                                {
                                    int TotalCalibrations = 0;
                                    if (!InputFile.Read(TotalCalibrations))
                                    {
                                        return false;
                                    }
                                    if (TargetCalibrationIndex >= TotalCalibrations)
                                    {
                                        return false;
                                    }
                                    if (TargetCalibrationIndex)
                                    {
                                        Real Value = Real(0);
                                        for (int CalibrationIndex = 0 ; CalibrationIndex < TargetCalibrationIndex ; ++CalibrationIndex)
                                            for (int DatumIndex = 0 ; DatumIndex < 27 ; ++DatumIndex)
                                                if (!InputFile.Read(Value))
                                                {
                                                    return false;
                                                }
                                    }
                                    Real ImageWidth = Real(0);
                                    if (!InputFile.Read(ImageWidth))
                                    {
                                        return false;
                                    }
                                    Real ImageHigth = Real(0);
                                    if (!InputFile.Read(ImageHigth))
                                    {
                                        return false;
                                    }
                                    m_ImageSize.Set(int(std::round(ImageWidth)), int(std::round(ImageHigth)));
                                    Mathematics::_3D::CMatrix3D K;
                                    for (int r = 0 ; r < 3 ; ++r)
                                        for (int c = 0 ; c < 3 ; ++c)
                                            if (!InputFile.Read(K[r][c]))
                                            {
                                                return false;
                                            }
                                    m_FocalLength.Set(K[0][0], K[1][1]);
                                    m_ImagePrincipalPoint.Set(K[0][2], K[1][2]);
                                    for (double& m_DistortionCoefficient : m_DistortionCoefficients)
                                        if (!InputFile.Read(m_DistortionCoefficient))
                                        {
                                            return false;
                                        }
                                    for (int r = 0 ; r < 3 ; ++r)
                                        for (int c = 0 ; c < 3 ; ++c)
                                            if (!InputFile.Read(m_Rotation[r][c]))
                                            {
                                                return false;
                                            }
                                    Real T[3] = { Real(0) };
                                    for (double& i : T)
                                        if (!InputFile.Read(i))
                                        {
                                            return false;
                                        }
                                    m_Translation.Set(Real(T[0]), Real(T[1]), Real(T[2]));
                                    if (!InputFile.Close())
                                    {
                                        return false;
                                    }
                                    if (ResetExtrinsic)
                                    {
                                        m_Rotation.MakeIdentity();
                                        m_RotationInverse.MakeIdentity();
                                        m_Translation.SetZero();
                                        m_TranslationInverse.SetZero();
                                    }
                                    else
                                    {
                                        m_RotationInverse = m_Rotation.GetTranspose();
                                        m_TranslationInverse = m_RotationInverse * m_Translation;
                                        m_TranslationInverse.Negate();
                                    }
                                    return true;
                                }
                            }
                            return false;
                        }

                        void CMonocularCameraGeometricCalibration::SetExtrinsic(const Mathematics::_3D::CMatrix3D& R, const Mathematics::_3D::CVector3D& Translation)
                        {
                            m_Rotation = R;
                            m_Translation = Translation;
                            m_RotationInverse = m_Rotation.GetTranspose();
                            m_TranslationInverse = m_RotationInverse * m_Translation;
                            m_TranslationInverse.Negate();
                        }

                        void CMonocularCameraGeometricCalibration::operator=(const CMonocularCameraGeometricCalibration& CameraGeometricCalibration)
                        {
                            m_ImageSize = CameraGeometricCalibration.m_ImageSize;
                            m_FocalLength = CameraGeometricCalibration.m_FocalLength;
                            m_ImagePrincipalPoint = CameraGeometricCalibration.m_ImagePrincipalPoint;
                            m_Translation = CameraGeometricCalibration.m_Translation;
                            m_Rotation = CameraGeometricCalibration.m_Rotation;
                            m_TranslationInverse = CameraGeometricCalibration.m_TranslationInverse;
                            m_RotationInverse = CameraGeometricCalibration.m_RotationInverse;
                            m_MinimalObjectDistance = CameraGeometricCalibration.m_MinimalObjectDistance;
                            memcpy(m_DistortionCoefficients, CameraGeometricCalibration.m_DistortionCoefficients, sizeof(Real) * _CAMERA_GEOMETRIC_CALIBRATION_TOTAL_DISTORTION_COEFFICIENTS_);
                        }

                        bool CMonocularCameraGeometricCalibration::IsLoaded() const
                        {
                            return m_ImageSize.IsValid();
                        }

                        void CMonocularCameraGeometricCalibration::MapPointFromSpaceToCameraFrame(const Mathematics::_3D::CVector3D& SpacePoint, Mathematics::_3D::CVector3D& CameraPoint) const
                        {
                            CameraPoint = (m_Rotation * SpacePoint) + m_Translation;
                        }

                        void CMonocularCameraGeometricCalibration::MapPointFromCameraFrameToSpace(const Mathematics::_3D::CVector3D& CameraPoint, Mathematics::_3D::CVector3D& SpacePoint) const
                        {
                            SpacePoint = (m_RotationInverse * CameraPoint) + m_TranslationInverse;
                        }

                        bool CMonocularCameraGeometricCalibration::MapPointFromImageToCameraFrame(const Mathematics::_2D::CVector2D& ImagePoint, const Real ZDepth, const OpticalAberrationMode SourceImagePointMode, Mathematics::_3D::CVector3D& CameraPoint) const
                        {
                            if (IsPositive(ZDepth))
                            {
                                if (SourceImagePointMode == eDistortedImage)
                                {
                                    Mathematics::_2D::CVector2D UndistortedImagePoint;
                                    Undistortion(ImagePoint, UndistortedImagePoint);
                                    CameraPoint.Set(ZDepth * ((UndistortedImagePoint.GetX() - m_ImagePrincipalPoint.GetX()) / m_FocalLength.GetX()), ZDepth * ((UndistortedImagePoint.GetY() - m_ImagePrincipalPoint.GetY()) / m_FocalLength.GetY()), ZDepth);
                                }
                                else
                                {
                                    CameraPoint.Set(ZDepth * ((ImagePoint.GetX() - m_ImagePrincipalPoint.GetX()) / m_FocalLength.GetX()), ZDepth * ((ImagePoint.GetY() - m_ImagePrincipalPoint.GetY()) / m_FocalLength.GetY()), ZDepth);
                                }
                                return true;
                            }
                            return false;
                        }

                        bool CMonocularCameraGeometricCalibration::MapPointFromSpaceToImage(const Mathematics::_3D::CVector3D& SpacePoint, const OpticalAberrationMode TargetImagePointMode, Mathematics::_2D::CVector2D& ImagePoint) const
                        {
                            Mathematics::_3D::CVector3D CameraPoint = (m_Rotation * SpacePoint) + m_Translation;
                            if (CameraPoint.GetZ() >= m_MinimalObjectDistance)
                            {
                                ImagePoint.Set((CameraPoint.GetX() * m_FocalLength.GetX()) / CameraPoint.GetZ() + m_ImagePrincipalPoint.GetX(), (CameraPoint.GetY() * m_FocalLength.GetY()) / CameraPoint.GetZ() + m_ImagePrincipalPoint.GetY());
                                if (TargetImagePointMode == eDistortedImage)
                                {
                                    Distortion(ImagePoint, ImagePoint);
                                }
                                return true;
                            }
                            return false;
                        }

                        bool CMonocularCameraGeometricCalibration::MapPointFromImageToSpace(const Mathematics::_2D::CVector2D& ImagePoint, const Real ZDepth, const OpticalAberrationMode SourceImagePointMode, Mathematics::_3D::CVector3D& SpacePoint) const
                        {
                            Mathematics::_3D::CVector3D CameraPoint;
                            if (MapPointFromImageToCameraFrame(ImagePoint, ZDepth, SourceImagePointMode, CameraPoint))
                            {
                                MapPointFromCameraFrameToSpace(CameraPoint, SpacePoint);
                                return true;
                            }
                            return false;
                        }

                        Real CMonocularCameraGeometricCalibration::GetDistanceToPrincipalPoint(const Mathematics::_2D::CVector2D& ImagePoint, const OpticalAberrationMode SourceImagePointMode) const
                        {
                            if (SourceImagePointMode == eDistortedImage)
                            {
                                Mathematics::_2D::CVector2D UndistortedImagePoint;
                                Undistortion(ImagePoint, UndistortedImagePoint);
                                return m_ImagePrincipalPoint.GetDistance(UndistortedImagePoint);
                            }
                            else
                            {
                                return m_ImagePrincipalPoint.GetDistance(ImagePoint);
                            }
                        }

                        Real CMonocularCameraGeometricCalibration::GetMinimalObjectDistance() const
                        {
                            return m_MinimalObjectDistance;
                        }

                        const Imaging::CImageSize& CMonocularCameraGeometricCalibration::GetImageSize() const
                        {
                            return m_ImageSize;
                        }

                        int CMonocularCameraGeometricCalibration::GetImageWidth() const
                        {
                            return m_ImageSize.GetWidth();
                        }

                        int CMonocularCameraGeometricCalibration::GetImageHeight() const
                        {
                            return m_ImageSize.GetHeight();
                        }

                        const Mathematics::_2D::CVector2D& CMonocularCameraGeometricCalibration::GetFocalLength() const
                        {
                            return m_FocalLength;
                        }

                        Real CMonocularCameraGeometricCalibration::GetFocalLengthX() const
                        {
                            return m_FocalLength.GetX();
                        }

                        Real CMonocularCameraGeometricCalibration::GetFocalLengthY() const
                        {
                            return m_FocalLength.GetY();
                        }

                        const Real* CMonocularCameraGeometricCalibration::GetDistortionCoefficients() const
                        {
                            return m_DistortionCoefficients;
                        }

                        const Mathematics::_2D::CVector2D& CMonocularCameraGeometricCalibration::GetImagePrincipalPoint() const
                        {
                            return m_ImagePrincipalPoint;
                        }

                        Real CMonocularCameraGeometricCalibration::GetImagePrincipalPointX() const
                        {
                            return m_ImagePrincipalPoint.GetX();
                        }

                        Real CMonocularCameraGeometricCalibration::GetImagePrincipalPointY() const
                        {
                            return m_ImagePrincipalPoint.GetY();
                        }

                        const Mathematics::_3D::CVector3D& CMonocularCameraGeometricCalibration::GetTranslation() const
                        {
                            return m_Translation;
                        }

                        const Mathematics::_3D::CMatrix3D& CMonocularCameraGeometricCalibration::GetRotation() const
                        {
                            return m_Rotation;
                        }

                        const Mathematics::_3D::CVector3D& CMonocularCameraGeometricCalibration::GetTranslationInverse() const
                        {
                            return m_TranslationInverse;
                        }

                        const Mathematics::_3D::CMatrix3D& CMonocularCameraGeometricCalibration::GetRotationInverse() const
                        {
                            return m_RotationInverse;
                        }

                        Mathematics::_3D::CMatrix3D CMonocularCameraGeometricCalibration::GetCalibrationMatrix() const
                        {
                            Mathematics::_3D::CMatrix3D K;
                            K[0][0] = m_FocalLength.GetX();
                            K[0][2] = m_ImagePrincipalPoint.GetX();
                            K[1][1] = m_FocalLength.GetY();
                            K[1][2] = m_ImagePrincipalPoint.GetY();
                            K[2][2] = Real(1);
                            return K;
                        }

                        void CMonocularCameraGeometricCalibration::Distortion(const Mathematics::_2D::CVector2D& UndistortedImagePoint, Mathematics::_2D::CVector2D& DistortedImagePoint) const
                        {
                            const Real Xn = (UndistortedImagePoint.GetX() - m_ImagePrincipalPoint.GetX()) / m_FocalLength.GetX();
                            const Real Yn = (UndistortedImagePoint.GetY() - m_ImagePrincipalPoint.GetY()) / m_FocalLength.GetY();
                            const Real Xn2 = Xn * Xn;
                            const Real Yn2 = Yn * Yn;
                            const Real DoubleXnYn = Real(2) * Xn * Yn;
                            const Real SquareRadius = Xn2 + Yn2;
                            const Real RadialDistortion = Real(1) + m_DistortionCoefficients[0] * SquareRadius + m_DistortionCoefficients[1] * SquareRadius * SquareRadius;
                            DistortedImagePoint.Set((m_FocalLength.GetX() * (RadialDistortion * Xn + (m_DistortionCoefficients[2] * DoubleXnYn + m_DistortionCoefficients[3] * (SquareRadius + Real(2) * Xn2)))) + m_ImagePrincipalPoint.GetX(), (m_FocalLength.GetY() * (RadialDistortion * Yn + (m_DistortionCoefficients[2] * (SquareRadius + Real(2) * Yn2) + m_DistortionCoefficients[3] * DoubleXnYn))) + m_ImagePrincipalPoint.GetY());
                        }

                        void CMonocularCameraGeometricCalibration::Undistortion(const Mathematics::_2D::CVector2D& DistortedImagePoint, Mathematics::_2D::CVector2D& UndistortedImagePoint) const
                        {
                            const Real Xr = (DistortedImagePoint.GetX() - m_ImagePrincipalPoint.GetX()) / m_FocalLength.GetX();
                            const Real Yr = (DistortedImagePoint.GetY() - m_ImagePrincipalPoint.GetY()) / m_FocalLength.GetY();
                            Real Xn = Xr;
                            Real Yn = Yr;
                            Real Da = Real(0);
                            do
                            {
                                const Real Xn2 = Xn * Xn;
                                const Real Yn2 = Yn * Yn;
                                const Real SquareRadius = Xn2 + Yn2;
                                const Real DoubleXnYn = Real(2) * Xn * Yn;
                                const Real RadialDistortion = Real(1) + m_DistortionCoefficients[0] * SquareRadius + m_DistortionCoefficients[1] * SquareRadius * SquareRadius;
                                const Real Xa = (Xr - (m_DistortionCoefficients[2] * DoubleXnYn + m_DistortionCoefficients[3] * (SquareRadius + Real(2) * Xn2))) / RadialDistortion;
                                const Real Ya = (Yr - (m_DistortionCoefficients[2] * (SquareRadius + Real(2) * Yn2) + m_DistortionCoefficients[3] * DoubleXnYn)) / RadialDistortion;
                                Da = hypot(Xn - Xa, Yn - Ya);
                                Xn = Xa;
                                Yn = Ya;
                            }
                            while (IsSignificant(Da));
                            UndistortedImagePoint.Set(Xn * m_FocalLength.GetX() + m_ImagePrincipalPoint.GetX(), Yn * m_FocalLength.GetY() + m_ImagePrincipalPoint.GetY());
                        }

                        bool CMonocularCameraGeometricCalibration::operator==(const CMonocularCameraGeometricCalibration& MonocularCameraGeometricCalibration)
                        {
                            if ((m_ImageSize == MonocularCameraGeometricCalibration.m_ImageSize) && (m_FocalLength == MonocularCameraGeometricCalibration.m_FocalLength) && (m_ImagePrincipalPoint == MonocularCameraGeometricCalibration.m_ImagePrincipalPoint) && (m_Translation == MonocularCameraGeometricCalibration.m_Translation) && (m_Rotation == MonocularCameraGeometricCalibration.m_Rotation))
                            {
                                for (int i = 0 ; i < _CAMERA_GEOMETRIC_CALIBRATION_TOTAL_DISTORTION_COEFFICIENTS_ ; ++i)
                                    if (IsNonZero(m_DistortionCoefficients[i] - MonocularCameraGeometricCalibration.m_DistortionCoefficients[i]))
                                    {
                                        return false;
                                    }
                                return true;
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}
