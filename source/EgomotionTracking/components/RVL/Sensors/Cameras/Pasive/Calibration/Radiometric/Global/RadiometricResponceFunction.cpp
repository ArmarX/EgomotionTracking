/*
 * RadiometricResponceFunction.cpp
 */

#include "RadiometricResponceFunction.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            CRadiometricResponceFunction* CRadiometricResponceFunction::CreateFromFile(const std::string& pPathFileName)
                            {
                                if (Files::CFile::Exists(pPathFileName))
                                {
                                    CRadiometricResponceFunction* pRadiometricResponceFunction = new CRadiometricResponceFunction();
                                    if (pRadiometricResponceFunction->LoadResponceFunctionFromBinaryFile(pPathFileName))
                                    {
                                        return pRadiometricResponceFunction;
                                    }
                                    delete pRadiometricResponceFunction;
                                }
                                return nullptr;
                            }

                            std::string CRadiometricResponceFunction::PixelSelectionCriterionToString(const PixelSelectionCriterion Criterion)
                            {
                                switch (Criterion)
                                {
                                    case eUnknownPixelSelectionCriterion:
                                        return std::string("Unknown");
                                    case eMaximalStandardDeviation:
                                        return std::string("Maximal Standard Deviation");
                                    case eMinimalStandardDeviation:
                                        return std::string("Minimal Standard Deviation");
                                    case eMaximalRange:
                                        return std::string("Maximal Range");
                                    case eMinimalRange:
                                        return std::string("Minimal Range");
                                }
                                return std::string("Error");
                            }

                            CRadiometricResponceFunction::CRadiometricResponceFunction() :
                                m_CameraId(0),
                                m_BayerPattern(Imaging::CBayerPattern::BayerPatternType(-1)),
                                m_ChannelContent(Imaging::CBayerPattern::Channel(-1)),
                                m_TotalExpositions(0),
                                m_TotalSelectedPixels(0),
                                m_DominanceRadius(Real(0)),
                                m_SmoothingLambda(Real(0)),
                                m_PixelSelectionCriterion(eUnknownPixelSelectionCriterion),
                                m_pRegressionResponseFunction(nullptr),
                                m_pRegressionWeightingFunction(nullptr),
                                m_pModelEstimationWeightingFunction(nullptr),
                                m_IntensitySlope(Real(0)),
                                m_IntensityOffset(Real(0)),
                                m_MinimalIntensity(Real(0))
                            {
                            }

                            CRadiometricResponceFunction::CRadiometricResponceFunction(const uint64_t CameraId, const Imaging::CBayerPattern::BayerPatternType BayerPattern, const Imaging::CBayerPattern::Channel ChannelContent) :
                                m_CameraId(CameraId),
                                m_BayerPattern(BayerPattern),
                                m_ChannelContent(ChannelContent),
                                m_TotalExpositions(0),
                                m_TotalSelectedPixels(0),
                                m_DominanceRadius(Real(0)),
                                m_SmoothingLambda(Real(0)),
                                m_PixelSelectionCriterion(eUnknownPixelSelectionCriterion),
                                m_pRegressionResponseFunction(nullptr),
                                m_pRegressionWeightingFunction(nullptr),
                                m_pModelEstimationWeightingFunction(nullptr),
                                m_IntensitySlope(Real(0)),
                                m_IntensityOffset(Real(0)),
                                m_MinimalIntensity(Real(0))
                            {
                            }

                            CRadiometricResponceFunction::~CRadiometricResponceFunction()
                            {
                                Destroy();
                            }

                            bool CRadiometricResponceFunction::LoadResponceFunctionFromParameters(const uint64_t CameraId, const Imaging::CBayerPattern::BayerPatternType BayerPattern, const Imaging::CBayerPattern::Channel ChannelContent, const int TotalExpositions, const int TotalSelectedPixels, const Real DominanceRadius, const Real SmoothingLambda, const PixelSelectionCriterion PixelSelectionCriterion, const Real* pRegressionResponseFunction, const Real* pRegressionWeightingFunction, const Base::CRadianceWeigthingKernel* pRadianceWeigthingKernel)
                            {
                                if (CameraId)
                                {
                                    Create();
                                    m_CameraId = CameraId;
                                    m_BayerPattern = BayerPattern;
                                    m_ChannelContent = ChannelContent;
                                    m_TotalExpositions = TotalExpositions;
                                    m_TotalSelectedPixels = TotalSelectedPixels;
                                    m_DominanceRadius = DominanceRadius;
                                    m_SmoothingLambda = SmoothingLambda;
                                    m_PixelSelectionCriterion = PixelSelectionCriterion;
                                    memcpy(m_pRegressionResponseFunction, pRegressionResponseFunction, sizeof(Real) * 256);
                                    memcpy(m_pRegressionWeightingFunction, pRegressionWeightingFunction, sizeof(Real) * 256);
                                    m_IntensitySlope = Real(0);
                                    m_IntensityOffset = Real(0);
                                    m_MinimalIntensity = Real(0);
                                    if (pRadianceWeigthingKernel)
                                    {
                                        m_RadianceWeigthingKernel = *pRadianceWeigthingKernel;
                                    }
                                    return true;
                                }
                                return false;
                            }

                            bool CRadiometricResponceFunction::LoadResponceFunctionFromBinaryFile(const std::string& pPathFileName)
                            {
                                if (Files::CFile::Exists(pPathFileName))
                                {
                                    Files::CInFile InputBinaryFile(pPathFileName, Files::CFile::eBinary);
                                    if (LoadResponceFunctionFromBinaryFile(InputBinaryFile))
                                    {
                                        return InputBinaryFile.Close();
                                    }
                                }
                                return false;
                            }

                            bool CRadiometricResponceFunction::SaveResponceFunctionToFile(const std::string& pPathFileName, const Files::CFile::FileMode Mode) const
                            {
                                switch (Mode)
                                {
                                    case Files::CFile::eText:
                                        return SaveResponceFunctionToTextFile(pPathFileName);
                                    case Files::CFile::eBinary:
                                        return SaveResponceFunctionToBinaryFile(pPathFileName);
                                }
                                return false;
                            }

                            bool CRadiometricResponceFunction::IsIdealResponceFunctionReady() const
                            {
                                return IsPositive(m_IntensitySlope);
                            }

                            bool CRadiometricResponceFunction::EstimateIdealResponceFunction(const Base::CRadianceWeigthingKernel::KernelType Kernel)
                            {
                                if (m_pRegressionResponseFunction)
                                {
                                    Mathematics::_ND::CMatrixND X(256, 2);
                                    Mathematics::_ND::CMatrixND Y(256, 1);
                                    Mathematics::_ND::CMatrixND W(256, 256);
                                    Real Deltas[256] = { Real(0) };
                                    Real Accumulator = Real(0);
                                    Real SquareAccumulator = Real(0);
                                    for (int i = 1 ; i < 255 ; ++i)
                                    {
                                        const Real AbsoluteDeviation = std::abs(m_pRegressionResponseFunction[i] - m_pRegressionResponseFunction[i + 1]) + std::abs(m_pRegressionResponseFunction[i] - m_pRegressionResponseFunction[i - 1]);
                                        Deltas[i] = AbsoluteDeviation;
                                        Accumulator += AbsoluteDeviation;
                                        SquareAccumulator += AbsoluteDeviation * AbsoluteDeviation;
                                    }
                                    const Real MeanDelta = Accumulator / Real(254.0);
                                    const Real VarianceDelta = (SquareAccumulator / Real(254.0)) - MeanDelta * MeanDelta;
                                    const Real ExponentFactor = -Real(1) / (Real(2) * VarianceDelta);
                                    for (int i = 1 ; i < 255 ; ++i)
                                    {
                                        const Real Deviation = Deltas[i] - MeanDelta;
                                        Deltas[i] = std::exp(Deviation * Deviation * ExponentFactor);
                                    }
                                    for (int i = 0 ; i < 256 ; ++i)
                                    {
                                        m_pModelEstimationWeightingFunction[i] = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(i, Kernel) * Deltas[i];
                                        X(i, 0) = Mathematics::_ND::MatrixReal(i);
                                        X(i, 1) = Mathematics::_ND::MatrixReal(Real(1));
                                        Y(i, 0) = Mathematics::_ND::MatrixReal(std::exp(m_pRegressionResponseFunction[i]));
                                        W(i, i) = Mathematics::_ND::MatrixReal(m_pModelEstimationWeightingFunction[i]);
                                    }
                                    //try
                                    //{
                                    Mathematics::_ND::CMatrixND Tmp = (X.transpose() * W * X);
                                    Mathematics::_ND::CMatrixND B = Tmp.inverse() * (X.transpose() * W * Y);
                                    m_IntensitySlope = Real(B(0, 0));
                                    m_IntensityOffset = Real(B(1, 0));
                                    m_MinimalIntensity = std::max(-m_IntensityOffset / m_IntensitySlope, g_RealPlusEpsilon);
                                    return true;
                                    //}
                                    //catch(Mathematics::_ND::CMatrixBaseException & E)
                                    //{
                                    //  std::ostringstream OutputText;
                                    //  OutputText << E.what() << "\n";
                                    //  _RVL_CONSOLE_OUTPUT_EXECEPTIONS_(OutputText.str(),0);
                                    //}
                                }
                                return false;
                            }

                            Real CRadiometricResponceFunction::GetRegressionResponceFunction(const Byte DiscreteIntensity) const
                            {
                                return m_pRegressionResponseFunction ? m_pRegressionResponseFunction[DiscreteIntensity] : Real(0);
                            }

                            Real CRadiometricResponceFunction::GetRegressionResponceFunction(const Real Intensity) const
                            {
                                if (m_pRegressionResponseFunction && (Intensity >= Real(0)) && (Intensity <= Real(255)))
                                {
                                    const int DiscreteIntensity = int(Intensity);
                                    return (DiscreteIntensity < 255) ? (m_pRegressionResponseFunction[DiscreteIntensity] + ((m_pRegressionResponseFunction[DiscreteIntensity + 1] - m_pRegressionResponseFunction[DiscreteIntensity]) * (Intensity - Real(DiscreteIntensity)))) : m_pRegressionResponseFunction[255];
                                }
                                return Real(0);
                            }

                            Real CRadiometricResponceFunction::GetIdealResponce(const Real Intensity) const
                            {
                                if (m_pRegressionResponseFunction && (Intensity >= m_MinimalIntensity) && (Intensity <= Real(255)))
                                {
                                    return m_IntensitySlope * Intensity + m_IntensityOffset;
                                }
                                return Real(0);
                            }

                            Real CRadiometricResponceFunction::GetIdealRadiance(const Real Intensity, const Real ExposureTime) const
                            {
                                if (m_pRegressionResponseFunction && (Intensity >= m_MinimalIntensity) && (Intensity <= Real(255)) && (IsPositive(ExposureTime)))
                                {
                                    return (m_IntensitySlope * Intensity + m_IntensityOffset) / ExposureTime;
                                }
                                return Real(0);
                            }

                            Real CRadiometricResponceFunction::GetIdealExposureTime(const Real Intensity, const Real Radiance) const
                            {
                                if (m_pRegressionResponseFunction && (Intensity >= m_MinimalIntensity) && (Intensity <= Real(255)))
                                {
                                    return (m_IntensitySlope * Intensity + m_IntensityOffset) / Radiance;
                                }
                                return Real(0);
                            }

                            Real CRadiometricResponceFunction::GetIdealIntensity(const Real Radiance, const Real ExposureTime) const
                            {
                                const Real IdealIntensity = Radiance * ExposureTime - m_IntensityOffset;
                                if (IdealIntensity >= Real(0))
                                {
                                    return IdealIntensity / m_IntensitySlope;
                                }
                                return Real(0);
                            }

                            uint64_t CRadiometricResponceFunction::GetCameraId() const
                            {
                                return m_CameraId;
                            }

                            Imaging::CBayerPattern::BayerPatternType CRadiometricResponceFunction::GetBayerPattern() const
                            {
                                return m_BayerPattern;
                            }

                            Imaging::CBayerPattern::Channel CRadiometricResponceFunction::GetChannelContent() const
                            {
                                return m_ChannelContent;
                            }

                            int CRadiometricResponceFunction::GetTotalExpositions() const
                            {
                                return m_TotalExpositions;
                            }

                            int CRadiometricResponceFunction::GetTotalSelectedPixels() const
                            {
                                return m_TotalSelectedPixels;
                            }

                            Real CRadiometricResponceFunction::GetDominanceRadius() const
                            {
                                return m_DominanceRadius;
                            }

                            Real CRadiometricResponceFunction::GetSmoothingLambda() const
                            {
                                return m_SmoothingLambda;
                            }

                            CRadiometricResponceFunction::PixelSelectionCriterion CRadiometricResponceFunction::GetPixelSelectionCriterion() const
                            {
                                return m_PixelSelectionCriterion;
                            }

                            const Real* CRadiometricResponceFunction::GetRegressionResponseFunction() const
                            {
                                return m_pRegressionResponseFunction;
                            }

                            const Real* CRadiometricResponceFunction::GetRegressionWeightingFunction() const
                            {
                                return m_pRegressionWeightingFunction;
                            }

                            const Real* CRadiometricResponceFunction::GetModelEstimationWeightingFunction() const
                            {
                                return m_pModelEstimationWeightingFunction;
                            }

                            Real CRadiometricResponceFunction::GetIntensitySlope() const
                            {
                                return m_IntensitySlope;
                            }

                            Real CRadiometricResponceFunction::GetIntensityOffset() const
                            {
                                return m_IntensityOffset;
                            }

                            Real CRadiometricResponceFunction::GetMinimalIntensity() const
                            {
                                return m_MinimalIntensity;
                            }

                            const Base::CRadianceWeigthingKernel* CRadiometricResponceFunction::GetRadianceWeigthingKernel() const
                            {
                                return &m_RadianceWeigthingKernel;
                            }

                            void CRadiometricResponceFunction::Create()
                            {
                                if (!m_pRegressionResponseFunction)
                                {
                                    m_pRegressionResponseFunction = new Real[256];
                                }
                                if (!m_pRegressionWeightingFunction)
                                {
                                    m_pRegressionWeightingFunction = new Real[256];
                                }
                                if (!m_pModelEstimationWeightingFunction)
                                {
                                    m_pModelEstimationWeightingFunction = new Real[256];
                                }
                                memset(m_pRegressionResponseFunction, 0, sizeof(Real) * 256);
                                memset(m_pRegressionWeightingFunction, 0, sizeof(Real) * 256);
                                memset(m_pModelEstimationWeightingFunction, 0, sizeof(Real) * 256);
                            }

                            void CRadiometricResponceFunction::Destroy()
                            {
                                delete[] m_pRegressionResponseFunction;
                                m_pRegressionResponseFunction = nullptr;
                                delete[] m_pRegressionWeightingFunction;
                                m_pRegressionWeightingFunction = nullptr;
                                delete[] m_pModelEstimationWeightingFunction;
                                m_pModelEstimationWeightingFunction = nullptr;
                            }

                            bool CRadiometricResponceFunction::SaveResponceFunctionToTextFile(const std::string& pPathFileName) const
                            {
                                if (m_CameraId && m_pRegressionResponseFunction && m_pRegressionWeightingFunction && m_pModelEstimationWeightingFunction)
                                {
                                    std::ostringstream OutputText;
                                    OutputText.precision(g_RealDisplayDigits);
                                    OutputText << " Radiometric Camera Calibration\n\n[General Information]\n\n";
                                    OutputText << "Camera Device Identifier" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_ << DeviceIdentifierToString(m_CameraId);
                                    OutputText << "\nBayer Pattern" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_ << Imaging::CBayerPattern::BayerPatternToString(m_BayerPattern);
                                    OutputText << "\nChannel Content" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    switch (m_ChannelContent)
                                    {
                                        case Imaging::CBayerPattern::eRed:
                                            OutputText << "Red\n";
                                            break;
                                        case Imaging::CBayerPattern::eGreen:
                                            OutputText << "Green\n";
                                            break;
                                        case Imaging::CBayerPattern::eBlue:
                                            OutputText << "Blue\n";
                                            break;
                                    }
                                    OutputText << "Total Expositions" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_ << m_TotalExpositions;
                                    OutputText << "\nTotal Selected Pixels" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_ << m_TotalSelectedPixels;
                                    OutputText << "\nPixel Selection Criterion" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_ << PixelSelectionCriterionToString(m_PixelSelectionCriterion);
                                    OutputText << "\nDominance Radius" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_ << m_DominanceRadius;
                                    OutputText << "\nSmoothing Lambda" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_ << m_SmoothingLambda;
                                    OutputText << "\nExponential Domain Slope" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_ << m_IntensitySlope;
                                    OutputText << "\nExponential Domain Offset" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_ << m_IntensityOffset;
                                    OutputText << "\nMinimal Integrable Continuous Intensity\n" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_ << m_MinimalIntensity;
                                    OutputText << "[Response Curves]\n\n";
                                    OutputText << "Continuous Intensity" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << "Discrete Intensity" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << "Regression Weights" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << "Model Estimation Weights" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << "G(I)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << "e^G(I)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << "Gp(I) = log((Slope*I)+Offset)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << "e^Gp(I)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << "e^G(I)-e^Gp(Iy)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << "|e^G(I)-e^Gp(I)|";
                                    const Real Increment = Real(1) / Real(_RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_INCREMENTS_PER_UNIT_);
                                    for (Real ContinuousIntensity = Real(0) ; ContinuousIntensity <= Real(255) ; ContinuousIntensity += Increment)
                                    {
                                        const int DiscreteIntensity = int(std::round(ContinuousIntensity));
                                        OutputText << "\n" << ContinuousIntensity << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                        OutputText << DiscreteIntensity << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                        OutputText << m_pRegressionWeightingFunction[DiscreteIntensity] << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                        OutputText << m_pModelEstimationWeightingFunction[DiscreteIntensity] << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                        OutputText << m_pRegressionResponseFunction[DiscreteIntensity] << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                        const Real eG = std::exp(m_pRegressionResponseFunction[DiscreteIntensity]);
                                        OutputText << eG << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                        if (ContinuousIntensity > m_MinimalIntensity)
                                        {
                                            const Real eGp = GetIdealResponce(ContinuousIntensity);
                                            const Real Gp = std::log(eGp);
                                            OutputText << Gp << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                            OutputText << eGp << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                            OutputText << eG - eGp << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                            OutputText << std::abs(eG - eGp);
                                        }
                                        else
                                        {
                                            OutputText << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_ << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_ << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_ << "NAN";
                                        }
                                    }
                                    OutputText << "\nFor more information see IEEE-RAS publication: Gonzalez-Aguirre, D.; Asfour, T.; Dillmann, R.; , \"Eccentricity edge-graphs from HDR images for object recognition by humanoid robots,\" Humanoid Robots (Humanoids), 2010 10th IEEE-RAS International Conference on , vol., no., pp.144-151, 6-8 Dec. 2010 doi: 10.1109/ICHR.2010.5686336";
                                    return Files::COutFile::WriteStringToFile(pPathFileName, OutputText.str());
                                }
                                return false;
                            }

                            bool CRadiometricResponceFunction::SaveResponceFunctionToBinaryFile(const std::string& pPathFileName) const
                            {
                                if (m_CameraId && m_pRegressionResponseFunction && m_pRegressionWeightingFunction && m_pModelEstimationWeightingFunction)
                                {
                                    Files::COutFile OutputBinaryFile(pPathFileName, Files::CFile::eBinary);
                                    if (SaveResponceFunctionToBinaryFile(OutputBinaryFile))
                                    {
                                        return OutputBinaryFile.Close();
                                    }
                                }
                                return false;
                            }

                            bool CRadiometricResponceFunction::SaveResponceFunctionToBinaryFile(Files::COutFile& OutputBinaryFile) const
                            {
                                if (m_CameraId && m_pRegressionResponseFunction && m_pRegressionWeightingFunction && m_pModelEstimationWeightingFunction)
                                {
                                    if (!OutputBinaryFile.IsReady())
                                    {
                                        return false;
                                    }
                                    if (!OutputBinaryFile.Write(&m_CameraId, sizeof(uint64_t)))
                                    {
                                        return false;
                                    }
                                    if (!OutputBinaryFile.Write(&m_BayerPattern, sizeof(Imaging::CBayerPattern::BayerPatternType)))
                                    {
                                        return false;
                                    }
                                    if (!OutputBinaryFile.Write(&m_ChannelContent, sizeof(Imaging::CBayerPattern::Channel)))
                                    {
                                        return false;
                                    }
                                    if (!OutputBinaryFile.Write(m_TotalExpositions))
                                    {
                                        return false;
                                    }
                                    if (!OutputBinaryFile.Write(m_TotalSelectedPixels))
                                    {
                                        return false;
                                    }
                                    if (!OutputBinaryFile.Write(m_DominanceRadius))
                                    {
                                        return false;
                                    }
                                    if (!OutputBinaryFile.Write(m_SmoothingLambda))
                                    {
                                        return false;
                                    }
                                    if (!OutputBinaryFile.Write(&m_PixelSelectionCriterion, sizeof(PixelSelectionCriterion)))
                                    {
                                        return false;
                                    }
                                    if (!OutputBinaryFile.Write(m_pRegressionResponseFunction, sizeof(Real) * 256))
                                    {
                                        return false;
                                    }
                                    if (!OutputBinaryFile.Write(m_pRegressionWeightingFunction, sizeof(Real) * 256))
                                    {
                                        return false;
                                    }
                                    if (!OutputBinaryFile.Write(m_pModelEstimationWeightingFunction, sizeof(Real) * 256))
                                    {
                                        return false;
                                    }
                                    if (!OutputBinaryFile.Write(m_IntensitySlope))
                                    {
                                        return false;
                                    }
                                    if (!OutputBinaryFile.Write(m_IntensityOffset))
                                    {
                                        return false;
                                    }
                                    if (!OutputBinaryFile.Write(m_MinimalIntensity))
                                    {
                                        return false;
                                    }
                                    return m_RadianceWeigthingKernel.SaveToFile(OutputBinaryFile);
                                }
                                return false;
                            }

                            bool CRadiometricResponceFunction::LoadResponceFunctionFromBinaryFile(Files::CInFile& InputBinaryFile)
                            {
                                if (InputBinaryFile.IsReady())
                                {
                                    Create();
                                    if (!InputBinaryFile.Read(&m_CameraId, sizeof(uint64_t)))
                                    {
                                        Destroy();
                                        return false;
                                    }
                                    if (!InputBinaryFile.Read(&m_BayerPattern, sizeof(Imaging::CBayerPattern::BayerPatternType)))
                                    {
                                        Destroy();
                                        return false;
                                    }
                                    if (!InputBinaryFile.Read(&m_ChannelContent, sizeof(Imaging::CBayerPattern::Channel)))
                                    {
                                        Destroy();
                                        return false;
                                    }
                                    if (!InputBinaryFile.Read(m_TotalExpositions))
                                    {
                                        Destroy();
                                        return false;
                                    }
                                    if (!InputBinaryFile.Read(m_TotalSelectedPixels))
                                    {
                                        Destroy();
                                        return false;
                                    }
                                    if (!InputBinaryFile.Read(m_DominanceRadius))
                                    {
                                        Destroy();
                                        return false;
                                    }
                                    if (!InputBinaryFile.Read(m_SmoothingLambda))
                                    {
                                        Destroy();
                                        return false;
                                    }
                                    if (!InputBinaryFile.Read(&m_PixelSelectionCriterion, sizeof(PixelSelectionCriterion)))
                                    {
                                        Destroy();
                                        return false;
                                    }
                                    if (!InputBinaryFile.Read(m_pRegressionResponseFunction, sizeof(Real) * 256))
                                    {
                                        Destroy();
                                        return false;
                                    }
                                    if (!InputBinaryFile.Read(m_pRegressionWeightingFunction, sizeof(Real) * 256))
                                    {
                                        Destroy();
                                        return false;
                                    }
                                    if (!InputBinaryFile.Read(m_pModelEstimationWeightingFunction, sizeof(Real) * 256))
                                    {
                                        Destroy();
                                        return false;
                                    }
                                    if (!InputBinaryFile.Read(m_IntensitySlope))
                                    {
                                        Destroy();
                                        return false;
                                    }
                                    if (!InputBinaryFile.Read(m_IntensityOffset))
                                    {
                                        Destroy();
                                        return false;
                                    }
                                    if (!InputBinaryFile.Read(m_MinimalIntensity))
                                    {
                                        Destroy();
                                        return false;
                                    }
                                    return m_RadianceWeigthingKernel.LoadFromFile(InputBinaryFile);

                                }
                                return false;
                            }
                        }
                    }
                }
            }
        }
    }
}
