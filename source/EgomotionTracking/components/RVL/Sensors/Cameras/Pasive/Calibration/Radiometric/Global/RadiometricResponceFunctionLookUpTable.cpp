/*
 * RadiometricResponceFunctionLookUpTable.cpp
 */

#include "RadiometricResponceFunctionLookUpTable.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            CRadiometricResponceFunctionLookUpTable::CRadiometricResponceFunctionLookUpTable() :
                                m_pResponceFunction(nullptr),
                                m_pLookUpTable(nullptr),
                                m_pWeigthingKernel(nullptr),
                                m_pParametricIndices(nullptr),
                                m_SamplesPerUnit(0)
                            {
                            }

                            CRadiometricResponceFunctionLookUpTable::~CRadiometricResponceFunctionLookUpTable()
                            {
                                delete m_pLookUpTable;
                                delete[] m_pWeigthingKernel;
                                delete[] m_pParametricIndices;
                            }

                            bool CRadiometricResponceFunctionLookUpTable::LoadLookUpTable(const CRadiometricResponceFunction* pResponceFunction, const std::vector<Base::CExposure>& Exposures, const int SamplesPerUnit, const Base::CRadianceWeigthingKernel::KernelType Kernel)
                            {
                                if (pResponceFunction && pResponceFunction->IsIdealResponceFunctionReady() && Exposures.size() && (SamplesPerUnit >= 1) && (Kernel != Base::CRadianceWeigthingKernel::eUnknownKernelType))
                                {
                                    const int TotalExposures = LoadExposures(Exposures);
                                    if (TotalExposures == int(Exposures.size()))
                                    {
                                        m_pResponceFunction = pResponceFunction;
                                        m_SamplesPerUnit = SamplesPerUnit;
                                        const int TotalSamples = (m_SamplesPerUnit * 255) + 1;

                                        if (m_pLookUpTable)
                                        {
                                            delete m_pLookUpTable;
                                        }
                                        m_pLookUpTable = new Imaging::TImage<Real>(TotalSamples, TotalExposures);
                                        m_pLookUpTable->Clear();

                                        if (m_pWeigthingKernel)
                                        {
                                            delete[] m_pWeigthingKernel;
                                        }
                                        m_pWeigthingKernel = new Real[TotalSamples];
                                        memset(m_pWeigthingKernel, 0, sizeof(Real) * TotalSamples);

                                        if (m_pParametricIndices)
                                        {
                                            delete[] m_pParametricIndices;
                                        }
                                        m_pParametricIndices = new int[TotalExposures];
                                        memset(m_pParametricIndices, 0, sizeof(int) * TotalExposures);

                                        const Base::CRadianceWeigthingKernel* pRadianceWeigthingKernel = m_pResponceFunction->GetRadianceWeigthingKernel();
                                        const Real ContinousIntensityDelta = Real(1) / Real(m_SamplesPerUnit);
                                        Real ContinousIntensity = Real(0);
                                        for (int i = 0 ; i < TotalSamples ; ++i, ContinousIntensity += ContinousIntensityDelta)
                                        {
                                            m_pWeigthingKernel[i] = pRadianceWeigthingKernel->GetRegressionWeigthingByKernel(ContinousIntensity, Kernel);
                                        }

                                        Real* pResponceFunctionSample = m_pLookUpTable->GetWritableBuffer();
                                        const Real Slope = m_pResponceFunction->GetIntensitySlope();
                                        const Real Offset = m_pResponceFunction->GetIntensityOffset();
                                        const Real MinimalIntensity = m_pResponceFunction->GetMinimalIntensity();
                                        int ExposureIndex = 0;
                                        for (std::vector<Base::CExposure>::const_iterator pExposure = m_Exposures.begin() ; pExposure != m_Exposures.end() ; ++pExposure, ++ExposureIndex)
                                        {
                                            m_pParametricIndices[ExposureIndex] = pExposure->GetParametricIndex();
                                            const Real LogExposureTime = std::log(Real(pExposure->GetTime()));
                                            Real ContinousIntensity = Real(0);
                                            for (int i = 0 ; i < TotalSamples ; ++i, ContinousIntensity += ContinousIntensityDelta, ++pResponceFunctionSample)
                                                if (ContinousIntensity > MinimalIntensity)
                                                {
                                                    *pResponceFunctionSample = m_pWeigthingKernel[i] * (std::log(ContinousIntensity * Slope + Offset) - LogExposureTime);
                                                }
                                        }
                                        return true;
                                    }
                                }
                                return false;
                            }

                            bool CRadiometricResponceFunctionLookUpTable::IsEnabled() const
                            {
                                return m_pLookUpTable;
                            }

                            const CRadiometricResponceFunction* CRadiometricResponceFunctionLookUpTable::GetResponceFunction() const
                            {
                                return m_pResponceFunction;
                            }

                            const Imaging::TImage<Real>* CRadiometricResponceFunctionLookUpTable::GetLookUpTable() const
                            {
                                return m_pLookUpTable;
                            }

                            const Real* CRadiometricResponceFunctionLookUpTable::GetLookUpTableByExposure(const Base::CExposure& Exposure) const
                            {
                                if (m_pLookUpTable && Exposure.HasParametricIndex())
                                {
                                    const int ParametricIndex = Exposure.GetParametricIndex();
                                    const int TotalExposures = m_pLookUpTable->GetHeight();
                                    for (int i = 0 ; i < TotalExposures ; ++i)
                                        if (ParametricIndex == m_pParametricIndices[i])
                                        {
                                            return m_pLookUpTable->GetReadOnlyBufferAtLine(i);
                                        }
                                }
                                return nullptr;
                            }

                            const Real* CRadiometricResponceFunctionLookUpTable::GetWeigthingKernel() const
                            {
                                return m_pWeigthingKernel;
                            }

                            int CRadiometricResponceFunctionLookUpTable::GetSamplesPerUnit() const
                            {
                                return m_SamplesPerUnit;
                            }

                            const std::vector<Base::CExposure>& CRadiometricResponceFunctionLookUpTable::GetExposures() const
                            {
                                return m_Exposures;
                            }

                            int CRadiometricResponceFunctionLookUpTable::LoadExposures(const std::vector<Base::CExposure>& Exposures)
                            {
                                m_Exposures.clear();
                                if (Exposures.size())
                                {
                                    m_Exposures = Exposures;
                                    std::sort(m_Exposures.begin(), m_Exposures.end(), Base::CExposure::SortExposuresByParametricIndex);
                                    std::vector<Base::CExposure>::iterator Location = std::unique(m_Exposures.begin(), m_Exposures.end(), Base::CExposure::EqualsExposuresByParametricIndex);
                                    m_Exposures.resize(std::distance(m_Exposures.begin(), Location));
                                }
                                return m_Exposures.size();
                            }
                        }
                    }
                }
            }
        }
    }
}
