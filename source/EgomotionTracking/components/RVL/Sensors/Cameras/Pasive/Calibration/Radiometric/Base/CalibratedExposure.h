/*
 * CalibratedExposure.h
 */

#pragma once

#include "Exposure.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Base
                        {
                            class CCalibratedExposure : public CExposure
                            {
                            public:

                                static const std::list<CCalibratedExposure> MergeExposureSets(const std::list<CCalibratedExposure>& Lhs, const std::list<CCalibratedExposure>& rhs);

                                CCalibratedExposure(const int ParametricIndex, const Real MaximalSamplingDensity, const Real ScopeMinimalRadiance, const Real ScopeMaximalRadiance, const Real ScopeMinimalExposureTime, const Real CentralExposureTime, const Real ScopeMaximalExposureTime);
                                ~CCalibratedExposure() override;

                                Real GetMaximalSamplingDensity() const;
                                Real GetScopeMinimalRadiance() const;
                                Real GetScopeMaximalRadiance() const;
                                Real GetScopeRangeRadiance() const;
                                Real GetScopeMinimalExposureTime() const;
                                Real GetScopeMaximalExposureTime() const;
                                Real GetScopeRangeExposureTime() const;

                                Real GetTimeDeltaToCentralExposureTime(const Real Time) const;
                                Real GetAbsoluteTimeDeltaToCentralExposureTime(const Real Time) const;
                                bool IsContainedExposureTimeScope(const Real Time) const;
                                bool IsContainedExposureTimeScope(const Real T0, const Real T1) const;
                                bool IsContainedExposureRadianceScope(const Real Radiance) const;

                                bool HasCommonTemporalScope(const Real T0, const Real T1) const;
                                bool HasCommonTemporalScope(const CCalibratedExposure& CalibratedExposure) const;
                                Real GetCommonTemporalScope(const CCalibratedExposure& CalibratedExposure) const;

                                bool HasCommonRadianceScope(const CCalibratedExposure& CalibratedExposure) const;
                                Real GetCommonRadianceScope(const CCalibratedExposure& CalibratedExposure) const;

                                void operator=(const CCalibratedExposure& CalibratedExposure);

                            protected:

                                Real m_MaximalSamplingDensity;
                                Real m_ScopeMinimalRadiance;
                                Real m_ScopeMaximalRadiance;
                                Real m_ScopeMinimalExposureTime;
                                Real m_ScopeMaximalExposureTime;
                            };
                        }
                    }
                }
            }
        }
    }
}
