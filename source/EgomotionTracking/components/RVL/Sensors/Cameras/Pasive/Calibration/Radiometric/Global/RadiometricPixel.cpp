/*
 * RadiometricPixel.cpp
 */

#include "RadiometricPixel.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            CRadiometricPixel::CRadiometricPixel() :
                                m_pRadiometricPixel(nullptr)
                            {
                            }

                            CRadiometricPixel::~CRadiometricPixel()
                            {
                                if (m_pRadiometricPixel)
                                {
                                    delete m_pRadiometricPixel;
                                }
                            }

                            CRadiometricPixel* CRadiometricPixel::Initialize(const int X, const int Y, const Real* pValueSource)
                            {
                                if (!m_pRadiometricPixel)
                                {
                                    m_pRadiometricPixel = new RadiometricPixel;
                                }
                                m_pRadiometricPixel->m_IsActive = true;
                                m_pRadiometricPixel->m_pSource = pValueSource;
                                m_pRadiometricPixel->m_Criterion = Real(0);
                                m_pRadiometricPixel->m_X = X;
                                m_pRadiometricPixel->m_Y = Y;
                                return this;
                            }

                            void CRadiometricPixel::CalculateStandardDeviationCriterion()
                            {
                                const int TotalSamples = m_pRadiometricPixel->m_Values.size();
                                if (TotalSamples)
                                {
                                    Real Accumulator = Real(0);
                                    Real SquareAccumulator = Real(0);
                                    for (std::list<Real>::const_iterator pValue = m_pRadiometricPixel->m_Values.begin() ; pValue != m_pRadiometricPixel->m_Values.end() ; ++pValue)
                                    {
                                        Accumulator += *pValue;
                                        SquareAccumulator += *pValue * *pValue;
                                    }
                                    const Real Mean = Accumulator / Real(m_pRadiometricPixel->m_Values.size());
                                    m_pRadiometricPixel->m_Criterion = std::sqrt(SquareAccumulator / Real(TotalSamples) - Mean * Mean);
                                    m_pRadiometricPixel->m_IsActive = true;
                                }
                            }

                            void CRadiometricPixel::CalculateRangeCriterion()
                            {
                                if (m_pRadiometricPixel->m_Values.size())
                                {
                                    Real Maximal = m_pRadiometricPixel->m_Values.back();
                                    Real Minimal = Maximal;
                                    for (std::list<Real>::const_iterator pValue = m_pRadiometricPixel->m_Values.begin() ; pValue != m_pRadiometricPixel->m_Values.end() ; ++pValue)
                                        if (*pValue > Maximal)
                                        {
                                            Maximal = *pValue;
                                        }
                                        else if (*pValue < Minimal)
                                        {
                                            Minimal = *pValue;
                                        }
                                    m_pRadiometricPixel->m_Criterion = Maximal - Minimal;
                                    m_pRadiometricPixel->m_IsActive = true;
                                }
                            }

                            bool CRadiometricPixel::IsInitialized() const
                            {
                                return m_pRadiometricPixel;
                            }

                            bool CRadiometricPixel::IsActive() const
                            {
                                return m_pRadiometricPixel->m_IsActive;
                            }

                            void CRadiometricPixel::SetActive(const bool Active)
                            {
                                if (m_pRadiometricPixel)
                                {
                                    m_pRadiometricPixel->m_IsActive = Active;
                                }
                            }

                            void CRadiometricPixel::AddExposure()
                            {
                                m_pRadiometricPixel->m_Values.push_back(*m_pRadiometricPixel->m_pSource);
                            }

                            void CRadiometricPixel::ClearExposures()
                            {
                                m_pRadiometricPixel->m_IsActive = true;
                                m_pRadiometricPixel->m_Values.clear();
                                m_pRadiometricPixel->m_Criterion = Real(0);
                            }

                            Real CRadiometricPixel::GetCriterion() const
                            {
                                return m_pRadiometricPixel->m_Criterion;
                            }

                            int CRadiometricPixel::GetX() const
                            {
                                return m_pRadiometricPixel->m_X;
                            }

                            int CRadiometricPixel::GetY() const
                            {
                                return m_pRadiometricPixel->m_Y;
                            }

                            const std::list<Real>& CRadiometricPixel::GetValues() const
                            {
                                return m_pRadiometricPixel->m_Values;
                            }
                        }
                    }
                }
            }
        }
    }
}
