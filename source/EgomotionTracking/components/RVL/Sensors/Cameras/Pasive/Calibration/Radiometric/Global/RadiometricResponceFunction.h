/*
 * RadiometricResponceFunction.h
 */

#pragma once

#include "../../../../../../Console/ConsoleOutputManager.h"
#include "../../../../../../Common/DataTypes.h"
#include "../../../../../../Files/OutFile.h"
#include "../../../../../../Files/InFile.h"
#include "../../../../../../Imaging/BayerPattern.h"
#include "../../../../../../Mathematics/_ND/MatrixND.h"
#include "../Base/RadianceWeigthingKernel.h"
#include "RadiometricResponceFunction.h"

#define _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_INCREMENTS_PER_UNIT_ 1
#define _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_ ","

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            class CRadiometricResponceFunction
                            {
                            public:

                                enum PixelSelectionCriterion
                                {
                                    eUnknownPixelSelectionCriterion, eMaximalStandardDeviation, eMinimalStandardDeviation, eMaximalRange, eMinimalRange
                                };

                                static CRadiometricResponceFunction* CreateFromFile(const std::string& pPathFileName);
                                static std::string PixelSelectionCriterionToString(const PixelSelectionCriterion Criterion);

                                CRadiometricResponceFunction();
                                CRadiometricResponceFunction(const uint64_t CameraId, const Imaging::CBayerPattern::BayerPatternType BayerPattern, const Imaging::CBayerPattern::Channel ChannelContent);
                                virtual ~CRadiometricResponceFunction();

                                bool LoadResponceFunctionFromParameters(const uint64_t CameraId, const Imaging::CBayerPattern::BayerPatternType BayerPattern, const Imaging::CBayerPattern::Channel ChannelContent, const int TotalExpositions, const int TotalSelectedPixels, const Real DominanceRadius, const Real SmoothingLambda, const PixelSelectionCriterion PixelSelectionCriterion, const Real* pRegressionResponseFunction, const Real* pRegressionWeightingFunction, const Base::CRadianceWeigthingKernel* pRadianceWeigthingKernel);
                                bool LoadResponceFunctionFromBinaryFile(const std::string& pPathFileName);
                                bool SaveResponceFunctionToFile(const std::string& pPathFileName, const Files::CFile::FileMode Mode) const;

                                bool IsIdealResponceFunctionReady() const;
                                bool EstimateIdealResponceFunction(const Base::CRadianceWeigthingKernel::KernelType Kernel);
                                Real GetRegressionResponceFunction(const Byte DiscreteIntensity) const;
                                Real GetRegressionResponceFunction(const Real Intensity) const;

                                Real GetIdealResponce(const Real Intensity) const;
                                Real GetIdealRadiance(const Real Intensity, const Real ExposureTime) const;
                                Real GetIdealExposureTime(const Real Intensity, const Real Radiance) const;
                                Real GetIdealIntensity(const Real Radiance, const Real ExposureTime) const;

                                uint64_t GetCameraId() const;
                                Imaging::CBayerPattern::BayerPatternType GetBayerPattern() const;
                                Imaging::CBayerPattern::Channel GetChannelContent() const;
                                int GetTotalExpositions() const;
                                int GetTotalSelectedPixels() const;
                                Real GetDominanceRadius() const;
                                Real GetSmoothingLambda() const;
                                PixelSelectionCriterion GetPixelSelectionCriterion() const;
                                const Real* GetRegressionResponseFunction() const;
                                const Real* GetRegressionWeightingFunction() const;
                                const Real* GetModelEstimationWeightingFunction() const;
                                Real GetIntensitySlope() const;
                                Real GetIntensityOffset() const;
                                Real GetMinimalIntensity() const;
                                const Base::CRadianceWeigthingKernel* GetRadianceWeigthingKernel() const;

                            protected:

                                friend class CMultipleChannelRadiometricResponceFunction;

                                void Create();
                                void Destroy();
                                bool SaveResponceFunctionToTextFile(const std::string& pPathFileName) const;
                                bool SaveResponceFunctionToBinaryFile(const std::string& pPathFileName) const;
                                bool SaveResponceFunctionToBinaryFile(Files::COutFile& OutputBinaryFile) const;
                                bool LoadResponceFunctionFromBinaryFile(Files::CInFile& InputBinaryFile);

                                uint64_t m_CameraId;
                                Imaging::CBayerPattern::BayerPatternType m_BayerPattern;
                                Imaging::CBayerPattern::Channel m_ChannelContent;
                                int m_TotalExpositions;
                                int m_TotalSelectedPixels;
                                Real m_DominanceRadius;
                                Real m_SmoothingLambda;
                                PixelSelectionCriterion m_PixelSelectionCriterion;
                                Real* m_pRegressionResponseFunction;
                                Real* m_pRegressionWeightingFunction;
                                Real* m_pModelEstimationWeightingFunction;
                                Real m_IntensitySlope;
                                Real m_IntensityOffset;
                                Real m_MinimalIntensity;
                                Base::CRadianceWeigthingKernel m_RadianceWeigthingKernel;
                            };
                        }
                    }
                }
            }
        }
    }
}

