/*
 * RadianceWeigthingKernel.h
 */

#pragma once

#include "../../../../../../Common/DataTypes.h"
#include "../../../../../../Mathematics/_1D/NormalDistribution.h"
#include "../../../../../../Files/OutFile.h"
#include "../../../../../../Files/InFile.h"

#define _RADIANCE_WEIGTHING_KERNEL_MINIMAL_GAUSSIAN_BOX_RADIUS_ Real(8.0)
#define _RADIANCE_WEIGTHING_KERNEL_MAXIMAL_GAUSSIAN_BOX_RADIUS_ Real(64.0)
#define _RADIANCE_WEIGTHING_KERNEL_MINIMAL_DEFAULT_GAUSSIAN_BOX_RADIUS_ Real(64.0)
#define _RADIANCE_WEIGTHING_KERNEL_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_DENSITY_ Real(0.01)
#define _RADIANCE_WEIGTHING_KERNEL_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_NORMALIZED_DEVIATION_ Real(0.95)

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Base
                        {
                            class CRadianceWeigthingKernel
                            {
                            public:

                                enum KernelType
                                {
                                    eUnknownKernelType = 0, eUniform, eTriangular, eEpanechnikov, eBiweight, eTriweight, eTricube, eCosine, eGaussian, eGaussianBox
                                };

                                static std::string RegressionWeigthingKernelToString(const KernelType Kernel);

                                CRadianceWeigthingKernel();
                                CRadianceWeigthingKernel(const CRadianceWeigthingKernel& RadianceWeigthingKernel);
                                virtual ~CRadianceWeigthingKernel();

                                bool SetGaussianBoxKernelRadius(const Real GaussianBoxKernelRadius);
                                bool SetCommonGaussianKernelStandardDeviation(const Real StandardDeviation);
                                bool SetCommonGaussianKernelCutOff(const Real CutOffDensity, const Real CutOffNormalizedDeviation);

                                Real GetGaussianBoxKernelRadius() const;
                                Real GetCommonGaussianKernelExponentFactor() const;
                                Real GetCommonGaussianKernelStandardDeviation() const;

                                Real GetRegressionWeigthingByUniformKernel(const Real Intensity) const;
                                Real GetRegressionWeigthingByTriangularKernel(const Real Intensity) const;
                                Real GetRegressionWeigthingByEpanechnikovKernel(const Real Intensity) const;
                                Real GetRegressionWeigthingByBiweightKernel(const Real Intensity) const;
                                Real GetRegressionWeigthingByTriweightKernel(const Real Intensity) const;
                                Real GetRegressionWeigthingByTricubeKernel(const Real Intensity) const;
                                Real GetRegressionWeigthingByCosineKernel(const Real Intensity) const;
                                Real GetRegressionWeigthingByGaussianKernel(const Real Intensity) const;
                                Real GetRegressionWeigthingByGaussianBoxKernel(const Real Intensity) const;

                                Real GetRegressionWeigthingByKernel(const Real Intensity, const KernelType Kernel) const;
                                Real GetUpperIntensityAtDensity(const KernelType Kernel, const Real MinimalKernelDensity) const;
                                Real GetLowerIntensityAtDensity(const KernelType Kernel, const Real MinimalKernelDensity) const;

                                void operator=(const CRadianceWeigthingKernel& RadianceWeigthingKernel);
                                bool LoadFromFile(Files::CInFile& InputBinaryFile);
                                bool SaveToFile(Files::COutFile& OutputBinaryFile) const;

                            protected:

                                Real m_GaussianBoxKernelRadius;
                                Real m_CommonGaussianKernelExponentFactor;
                            };
                        }
                    }
                }
            }
        }
    }
}

