/*
 * RadiometricCalibration.h
 */

#pragma once

#include "../../../../../../Console/ConsoleOutputManager.h"
#include "../../../../../../Mathematics/_ND/MatrixND.h"
#include "../../../../../../Imaging/BayerPattern.h"
#include "../../../../../../Common/DataTypes.h"
#include "../../../../../../Imaging/TImage.hpp"
#include "../Base/Exposure.h"
#include "../Base/RadianceWeigthingKernel.h"
#include "RadiometricPixel.h"
#include "RadiometricResponceFunction.h"

#define _RADIOMETRIC_CALIBRATION_MINIMAL_EXPOSURES_ 4
#define _RADIOMETRIC_CALIBRATION_MINIMAL_SELECTED_SAMPLING_CELLS_ 32
#define _RADIOMETRIC_CALIBRATION_MINIMAL_GAUSSIAN_BOX_RADIUS_ Real(8)
#define _RADIOMETRIC_CALIBRATION_MAXIMAL_GAUSSIAN_BOX_RADIUS_ Real(32)
#define _RADIOMETRIC_CALIBRATION_MINIMAL_DEFAULT_GAUSSIAN_BOX_RADIUS_ Real(16)
#define _RADIOMETRIC_CALIBRATION_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_DENSITY_ Real(0.01)
#define _RADIOMETRIC_CALIBRATION_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_NORMALIZED_DEVIATION_ Real(0.95)

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            class CRadiometricResponceFunction;

                            class CRadiometricCalibration
                            {
                            public:

                                CRadiometricCalibration(const uint64_t CameraId, const Imaging::TImage<Real>* pSourceImage, const Imaging::CBayerPattern::BayerPatternType BayerPattern, const Imaging::CBayerPattern::Channel ChannelContent);
                                CRadiometricCalibration(const uint64_t CameraId, const Imaging::TImage<Real>* pSourceImage);
                                virtual ~CRadiometricCalibration();

                                bool IsEnabled() const;
                                uint64_t GetCameraId() const;
                                Imaging::CBayerPattern::BayerPatternType GetBayerPattern() const;
                                Imaging::CBayerPattern::Channel GetChannelContent() const;

                                bool AddExposure(const Real ExposureTime);
                                bool ClearExposures();
                                int GetTotalExposures() const;

                                bool SetGaussianBoxKernelRadius(const Real GaussianBoxKernelRadius);
                                bool SetCommonGaussianKernelStandardDeviation(const Real StandardDeviation);
                                bool SetCommonGaussianKernelCutOff(const Real CutOffDensity, const Real CutOffNormalizedDeviation);

                                Real GetGaussianBoxKernelRadius() const;
                                Real GetCommonGaussianKernelStandardDeviation() const;

                                bool Calibrate(const CRadiometricResponceFunction::PixelSelectionCriterion Criterion, const Real DominaceRadius, const std::list<CRadiometricPixel*>& SelectedPixels, const Real SmoothingLambda, const Base::CRadianceWeigthingKernel::KernelType Kernel);
                                bool Calibrate(const CRadiometricResponceFunction::PixelSelectionCriterion Criterion, const Real DominaceRadius, const int MaximalSamples, const Real SmoothingLambda, const Base::CRadianceWeigthingKernel::KernelType Kernel);

                                const CRadiometricResponceFunction* GetRadiometricResponceFunction() const;

                            protected:

                                const std::list<CRadiometricPixel*> SelectPixelsByCriterion(const CRadiometricResponceFunction::PixelSelectionCriterion Criterion, const Real DominaceRadius, const int MaximalSamples);

                                static std::list<std::pair<int, int> > ComputeLocationDeltas(const Real DominaceRadius);
                                static bool SortRadiometricPixelsByCriterionAscent(const CRadiometricPixel* plhs, const CRadiometricPixel* rlhs);
                                static bool SortRadiometricPixelsByCriterionDescent(const CRadiometricPixel* plhs, const CRadiometricPixel* rlhs);

                                const uint64_t m_CameraId;
                                Imaging::CBayerPattern::BayerPatternType m_BayerPattern;
                                Imaging::CBayerPattern::Channel m_ChannelContent;
                                const Imaging::TImage<Real>* m_pSourceImage;
                                Imaging::TImage<CRadiometricPixel>* m_pRadiometricImage;
                                std::list<CRadiometricPixel*> m_RadiometricPixels;
                                std::list<Base::CExposure> m_Exposures;
                                Base::CRadianceWeigthingKernel m_RadianceWeigthingKernel;
                                CRadiometricResponceFunction* m_pRadiometricResponceFunction;
                            };
                        }
                    }
                }
            }
        }
    }
}

