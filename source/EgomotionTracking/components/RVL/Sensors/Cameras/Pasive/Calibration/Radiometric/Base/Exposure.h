/*
 * Exposure.h
 */

#pragma once

#include "../../../../../../Common/DataTypes.h"
#include "../../../../../../Files/OutFile.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Base
                        {
                            class CExposure
                            {
                            public:

                                static std::list<CExposure> MergeExposureSets(const std::list<CExposure>& Lhs, const std::list<CExposure>& rhs);
                                static bool ExportExposureSetToFile(const std::list<CExposure>& ExposureSet, const std::string& pFileName);
                                static bool EqualsExposuresByParametricIndex(const CExposure& lhs, const CExposure& rhs);
                                static bool SortExposuresByParametricIndex(const CExposure& lhs, const CExposure& rhs);

                                CExposure(const Real Time, const int ParametricIndex = -1);
                                CExposure();
                                virtual ~CExposure();

                                Real GetTime() const;

                                bool HasParametricIndex() const;
                                int GetParametricIndex() const;

                            protected:

                                Real m_Time;
                                int m_ParametricIndex;
                            };
                        }
                    }
                }
            }
        }
    }
}

