/*
 * RadiometricDistribution.h
 */

#pragma once

#include "../../../../../../Imaging/BayerPattern.h"
#include "../../../../../../Common/DataTypes.h"
#include "../../../../../../Imaging/TImage.hpp"
#include "../Base/Exposure.h"
#include "../Base/CalibratedExposure.h"
#include "../Base/RadianceWeigthingKernel.h"
#include "MultipleChannelRadiometricResponceFunction.h"
#include "RadiometricResponceFunctionLookUpTable.h"
#include "RadiometricBayerPatternSynthesizer.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            class CRadiometricDistribution
                            {
                            public:

                                enum OptimizationCriterion
                                {
                                    eUnkownSelectionCriterion, eEnsureMinimalDensity, eEnsureMinimalExposures
                                };

                                CRadiometricDistribution();
                                ~CRadiometricDistribution();

                                bool AddSynthesizer(const CRadiometricBayerPatternSynthesizer* pBayerPatternSynthesizer);
                                bool RemoveAllSynthesizers();
                                bool LoadConfiguration(const Base::CRadianceWeigthingKernel::KernelType Kernel, const Real ScopeKernelDensity = Real(0), const Real IntensityMargin = Real(1));
                                std::vector<Base::CCalibratedExposure> GetMinimalExposureSet() const;

                                bool LoadDistributionFromSynthesizers(const Real CriticallKernelDensity);
                                std::list<Base::CCalibratedExposure> SelectExposureIndicesByCriterion(const OptimizationCriterion Criterion);

                                Real GetMaximalIntensity() const;
                                Real GetMinimalIntensity() const;
                                Real GetUpperIntensityAtScopeDensity() const;
                                Real GetLowerIntensityAtScopeDensity() const;
                                Real GetScopeKernelDensity() const;
                                Real GetConsistentSafeMaximalKernelDensity() const;
                                int GetTotalUnderExposedPixels() const;
                                int GetTotalOverExposedPixels() const;
                                const std::list<Base::CExposure>& GetExposures() const;
                                const std::vector<Base::CCalibratedExposure>& GetCalibratedExposures() const;
                                const std::map<std::uint64_t, const CRadiometricBayerPatternSynthesizer*>& GetSynthesizers() const;

                            protected:

                                struct ExposureBin
                                {
                                    bool m_Active;
                                    Real m_LogTime;
                                    short int m_Index;
                                    short int m_ScopeIndexA;
                                    short int m_ScopeIndexB;
                                    Real m_SamplingDensityAccumulator;
                                    Real m_ScopeSamplingDensityAccumulator;
                                    Real m_MinimalDensity;
                                };

                                struct ExposureDualIndex
                                {
                                    short int m_IndexA;
                                    short int m_IndexB;
                                    Real m_ExposureTimeThreshold;
                                };

                                bool ClearDistribution();
                                bool LoadExposuresFromSynthesizers();
                                bool UpdateMinimalIntensity(const Real IntensityMargin);
                                bool UpdateConsistentSafeMaximalKernelDensity(const Base::CRadianceWeigthingKernel::KernelType Kernel);
                                bool CreateCalibratedExposures(const Base::CRadianceWeigthingKernel::KernelType Kernel);
                                bool CreateExposureDistribution();
                                bool CreateExposureDualIndices();
                                std::list<ExposureBin*> LoadExposureScopeFrequencyDistribution();
                                std::list<ExposureBin*> DetermineExposureScopeFrequencyDistribution(const std::list<ExposureBin*>& ExposureDistribution);
                                ExposureBin* GetExposureBinMinimalIntegrationTime(const std::list<ExposureBin*>& ExposureScopeFrequencyDistribution);
                                ExposureBin* GetExposureBinByMinimalDensity(const std::list<ExposureBin*>& ExposureScopeFrequencyDistribution);
                                ExposureBin* GetExposureBinByMaximalScopeFrequency(const std::list<ExposureBin*>& ExposureScopeFrequencyDistribution);
                                bool ConfigureExposureScopeByDensity(const Real ScopeKernelDensity);
                                bool MapExposureFrequencyDistribution(const CMultipleChannelRadiometricResponceFunction* pMultipleChannelRadiometricResponceFunction, const Imaging::TImage<Real>* pLogRadianceImage, const Imaging::TImage<Real>* pDensityWeightedImage, const Imaging::CBayerPattern::BayerPatternType BayerPattern, const Real CriticallKernelDensity);

                                Base::CRadianceWeigthingKernel::KernelType m_Kernel;
                                ExposureBin* m_pExposureDistribution;
                                ExposureDualIndex* m_pExposureDualIndices;
                                Real m_MaximalIntensity;
                                Real m_MinimalIntensity;
                                Real m_UpperIntensityAtScopeDensity;
                                Real m_LowerIntensityAtScopeDensity;
                                Real m_ScopeKernelDensity;
                                Real m_ConsistentSafeMaximalKernelDensity;
                                Real m_ScaleFactorLogExposureTime;
                                int m_TotalUnderExposedPixels;
                                int m_TotalOverExposedPixels;
                                std::list<Base::CExposure> m_Exposures;
                                std::vector<Base::CCalibratedExposure> m_CalibratedExposures;
                                std::list<const CRadiometricResponceFunction*> m_RadiometricResponceFunctions;
                                Base::CRadianceWeigthingKernel m_RadianceWeigthingKernel;
                                std::map<std::uint64_t, const CRadiometricBayerPatternSynthesizer*> m_Synthesizers;
                            };
                        }
                    }
                }
            }
        }
    }
}
