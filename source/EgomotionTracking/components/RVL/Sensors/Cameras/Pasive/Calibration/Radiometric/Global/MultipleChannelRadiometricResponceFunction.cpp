/*
 * MultipleChannelRadiometricResponceFunction.cpp
 */

#include "MultipleChannelRadiometricResponceFunction.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            CMultipleChannelRadiometricResponceFunction* CMultipleChannelRadiometricResponceFunction::CreateFromFile(const std::string& PathFileName)
                            {
                                if (Files::CFile::Exists(PathFileName))
                                {
                                    CMultipleChannelRadiometricResponceFunction* pMultipleChannelResponceFunction = new CMultipleChannelRadiometricResponceFunction();
                                    if (pMultipleChannelResponceFunction->LoadResponceFunctionFromBinaryFile(PathFileName))
                                    {
                                        return pMultipleChannelResponceFunction;
                                    }
                                    delete pMultipleChannelResponceFunction;
                                }
                                return nullptr;
                            }

                            CMultipleChannelRadiometricResponceFunction::CMultipleChannelRadiometricResponceFunction() :
                                m_OwnerShip(false),
                                m_pRedChannelResponceFunction(nullptr),
                                m_pGreenChannelResponceFunction(nullptr),
                                m_pBlueChannelResponceFunction(nullptr)
                            {
                            }

                            CMultipleChannelRadiometricResponceFunction::~CMultipleChannelRadiometricResponceFunction()
                            {
                                DestroyResponceFunctions();
                            }

                            bool CMultipleChannelRadiometricResponceFunction::AreIdealResponceFunctionsReady() const
                            {
                                return m_pRedChannelResponceFunction && m_pGreenChannelResponceFunction && m_pBlueChannelResponceFunction && m_pRedChannelResponceFunction->IsIdealResponceFunctionReady() && m_pGreenChannelResponceFunction->IsIdealResponceFunctionReady() && m_pBlueChannelResponceFunction->IsIdealResponceFunctionReady();
                            }

                            bool CMultipleChannelRadiometricResponceFunction::LoadResponceFunctions(const CRadiometricResponceFunction* pRedChannelResponceFunction, const CRadiometricResponceFunction* pGreenChannelResponceFunction, const CRadiometricResponceFunction* pBlueRadiometricResponceFunction, const bool OwnerShip)
                            {
                                if (pRedChannelResponceFunction && pGreenChannelResponceFunction && pBlueRadiometricResponceFunction && (pRedChannelResponceFunction->GetChannelContent() == Imaging::CBayerPattern::eRed) && (pGreenChannelResponceFunction->GetChannelContent() == Imaging::CBayerPattern::eGreen) && (pBlueRadiometricResponceFunction->GetChannelContent() == Imaging::CBayerPattern::eBlue) && (pRedChannelResponceFunction->GetBayerPattern() == pGreenChannelResponceFunction->GetBayerPattern()) && (pGreenChannelResponceFunction->GetBayerPattern() == pBlueRadiometricResponceFunction->GetBayerPattern()) && (pRedChannelResponceFunction->GetCameraId() == pGreenChannelResponceFunction->GetCameraId()) && (pGreenChannelResponceFunction->GetCameraId() == pBlueRadiometricResponceFunction->GetCameraId()))
                                {
                                    DestroyResponceFunctions();
                                    m_OwnerShip = OwnerShip;
                                    m_pRedChannelResponceFunction = pRedChannelResponceFunction;
                                    m_pGreenChannelResponceFunction = pGreenChannelResponceFunction;
                                    m_pBlueChannelResponceFunction = pBlueRadiometricResponceFunction;
                                    return true;
                                }
                                return false;
                            }

                            bool CMultipleChannelRadiometricResponceFunction::LoadResponceFunctionFromBinaryFile(const std::string& PathFileName)
                            {
                                DestroyResponceFunctions();
                                if (Files::CFile::Exists(PathFileName))
                                {
                                    Files::CInFile InputBinaryFile(PathFileName, Files::CFile::eBinary);
                                    if (InputBinaryFile.IsReady())
                                    {
                                        CRadiometricResponceFunction* pRedChannelResponceFunction = new CRadiometricResponceFunction();
                                        if (!pRedChannelResponceFunction->LoadResponceFunctionFromBinaryFile(InputBinaryFile))
                                        {
                                            delete pRedChannelResponceFunction;
                                            return false;
                                        }
                                        CRadiometricResponceFunction* pGreenChannelResponceFunction = new CRadiometricResponceFunction();
                                        if (!pGreenChannelResponceFunction->LoadResponceFunctionFromBinaryFile(InputBinaryFile))
                                        {
                                            delete pRedChannelResponceFunction;
                                            delete pGreenChannelResponceFunction;
                                            return false;
                                        }
                                        CRadiometricResponceFunction* pBlueChannelResponceFunction = new CRadiometricResponceFunction();
                                        if (!pBlueChannelResponceFunction->LoadResponceFunctionFromBinaryFile(InputBinaryFile))
                                        {
                                            delete pRedChannelResponceFunction;
                                            delete pGreenChannelResponceFunction;
                                            delete pBlueChannelResponceFunction;
                                            return false;
                                        }
                                        m_pRedChannelResponceFunction = pRedChannelResponceFunction;
                                        m_pGreenChannelResponceFunction = pGreenChannelResponceFunction;
                                        m_pBlueChannelResponceFunction = pBlueChannelResponceFunction;
                                        m_OwnerShip = true;
                                        return true;
                                    }
                                }
                                return false;
                            }

                            bool CMultipleChannelRadiometricResponceFunction::SaveResponceFunctionToFile(const std::string& PathFileName, const Files::CFile::FileMode Mode) const
                            {
                                if (m_pRedChannelResponceFunction && m_pGreenChannelResponceFunction && m_pBlueChannelResponceFunction)
                                    switch (Mode)
                                    {
                                        case Files::CFile::eText:
                                            return SaveResponceFunctionToTextFile(PathFileName);
                                        case Files::CFile::eBinary:
                                            return SaveResponceFunctionToBinaryFile(PathFileName);
                                    }
                                return false;
                            }

                            uint64_t CMultipleChannelRadiometricResponceFunction::GetCameraId() const
                            {
                                return m_pRedChannelResponceFunction ? m_pRedChannelResponceFunction->GetCameraId() : 0;
                            }

                            Imaging::CBayerPattern::BayerPatternType CMultipleChannelRadiometricResponceFunction::GetBayerPattern() const
                            {
                                return m_pRedChannelResponceFunction ? m_pRedChannelResponceFunction->GetBayerPattern() : Imaging::CBayerPattern::BayerPatternType(-1);
                            }

                            const CRadiometricResponceFunction* CMultipleChannelRadiometricResponceFunction::GetRedChannelResponceFunction() const
                            {
                                return m_pRedChannelResponceFunction;
                            }

                            const CRadiometricResponceFunction* CMultipleChannelRadiometricResponceFunction::GetGreenChannelResponceFunction() const
                            {
                                return m_pGreenChannelResponceFunction;
                            }

                            const CRadiometricResponceFunction* CMultipleChannelRadiometricResponceFunction::GetBlueChannelResponceFunction() const
                            {
                                return m_pBlueChannelResponceFunction;
                            }

                            void CMultipleChannelRadiometricResponceFunction::DestroyResponceFunctions()
                            {
                                if (m_OwnerShip)
                                {
                                    if (m_pRedChannelResponceFunction)
                                    {
                                        delete m_pRedChannelResponceFunction;
                                    }
                                    if (m_pGreenChannelResponceFunction)
                                    {
                                        delete m_pGreenChannelResponceFunction;
                                    }
                                    if (m_pBlueChannelResponceFunction)
                                    {
                                        delete m_pBlueChannelResponceFunction;
                                    }
                                }
                                m_pRedChannelResponceFunction = nullptr;
                                m_pGreenChannelResponceFunction = nullptr;
                                m_pBlueChannelResponceFunction = nullptr;
                            }

                            bool CMultipleChannelRadiometricResponceFunction::SaveResponceFunctionToTextFile(const std::string& PathFileName) const
                            {
                                std::ostringstream OutputText;
                                OutputText.precision(g_RealDisplayDigits);
                                const Real MinimalIntensities[3] = { m_pRedChannelResponceFunction->GetMinimalIntensity(), m_pGreenChannelResponceFunction->GetMinimalIntensity(), m_pBlueChannelResponceFunction->GetMinimalIntensity() };
                                OutputText << " Multiple Channel Radiometric Camera Calibration\n";
                                OutputText << "\n" << "[General Information]" << "\n\n";
                                OutputText << "Camera Device Identifier" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << DeviceIdentifierToString(m_pRedChannelResponceFunction->GetCameraId()) << "\n";
                                OutputText << "Bayer Pattern" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << Imaging::CBayerPattern::BayerPatternToString(m_pRedChannelResponceFunction->GetBayerPattern()) << "\n";
                                OutputText << "Channel" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "Red" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "Green" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "Blue" << "\n";
                                OutputText << "Total Expositions" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pRedChannelResponceFunction->GetTotalExpositions() << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pGreenChannelResponceFunction->GetTotalExpositions() << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pBlueChannelResponceFunction->GetTotalExpositions() << "\n";
                                OutputText << "Total Selected Pixels" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pRedChannelResponceFunction->GetTotalSelectedPixels() << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pGreenChannelResponceFunction->GetTotalSelectedPixels() << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pBlueChannelResponceFunction->GetTotalSelectedPixels() << "\n";
                                OutputText << "Pixel Selection Criterion" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << CRadiometricResponceFunction::PixelSelectionCriterionToString(m_pRedChannelResponceFunction->GetPixelSelectionCriterion()) << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << CRadiometricResponceFunction::PixelSelectionCriterionToString(m_pGreenChannelResponceFunction->GetPixelSelectionCriterion()) << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << CRadiometricResponceFunction::PixelSelectionCriterionToString(m_pBlueChannelResponceFunction->GetPixelSelectionCriterion()) << "\n";
                                OutputText << "Dominance Radius" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pRedChannelResponceFunction->GetDominanceRadius() << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pGreenChannelResponceFunction->GetDominanceRadius() << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pBlueChannelResponceFunction->GetDominanceRadius() << "\n";
                                OutputText << "Smoothing Lambda" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pRedChannelResponceFunction->GetSmoothingLambda() << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pGreenChannelResponceFunction->GetSmoothingLambda() << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pBlueChannelResponceFunction->GetSmoothingLambda() << "\n";
                                OutputText << "Exponential Domain Slope" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pRedChannelResponceFunction->GetIntensitySlope() << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pGreenChannelResponceFunction->GetIntensitySlope() << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pBlueChannelResponceFunction->GetIntensitySlope() << "\n";
                                OutputText << "Exponential Domain Offset" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pRedChannelResponceFunction->GetIntensityOffset() << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pGreenChannelResponceFunction->GetIntensityOffset() << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << m_pBlueChannelResponceFunction->GetIntensityOffset() << "\n";
                                OutputText << "Minimal Integrable Continuous Intensity" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << MinimalIntensities[0] << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << MinimalIntensities[1] << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << MinimalIntensities[2] << "\n";
                                OutputText << "\n" << "[Response Curves]" << "\n" << "\n";
                                OutputText << "Continuous Intensity" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "Discrete Intensity" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "Regression Weights Red Channel" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "Regression Weights Green Channel" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "Regression Weights Blue Channel" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "Model Estimation Weights Red Channel" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "Model Estimation Weights Green Channel" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "Model Estimation Weights Blue Channel" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "G_R(I)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "G_G(I)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "G_B(I)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "e^G_R(I)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "e^G_G(I)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "e^G_B(I)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "Gp_R(I) = ln((Slope_R*I)+Offset_R)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "Gp_G(I) = ln((Slope_G*I)+Offset_G)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "Gp_B(I) = ln((Slope_B*I)+Offset_B)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "e^Gp_R(I)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "e^Gp_G(I)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "e^Gp_B(I)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "e^G_R(I)-e^Gp_R(Continuous Intensity)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "e^G_G(I)-e^Gp_G(Continuous Intensity)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "e^G_B(I)-e^Gp_B(Continuous Intensity)" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "|e^G_R(Continuous Intensity)-e^Gp_R(Continuous Intensity)|" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "|e^G_G(Continuous Intensity)-e^Gp_G(Continuous Intensity)|" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                OutputText << "|e^G_B(Continuous Intensity)-e^Gp_B(Continuous Intensity)|";
                                const Real Increment = Real(1) / Real(_RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_INCREMENTS_PER_UNIT_);
                                const Real* pRegressionResponseFunctionRed = m_pRedChannelResponceFunction->GetRegressionResponseFunction();
                                const Real* pRegressionResponseFunctionGreen = m_pGreenChannelResponceFunction->GetRegressionResponseFunction();
                                const Real* pRegressionResponseFunctionBlue = m_pBlueChannelResponceFunction->GetRegressionResponseFunction();
                                const Real* pRegressionWeightingFunctionRed = m_pRedChannelResponceFunction->GetRegressionWeightingFunction();
                                const Real* pRegressionWeightingFunctionGreen = m_pGreenChannelResponceFunction->GetRegressionWeightingFunction();
                                const Real* pRegressionWeightingFunctionBlue = m_pBlueChannelResponceFunction->GetRegressionWeightingFunction();
                                const Real* pModelEstimationWeightingFunctionRed = m_pRedChannelResponceFunction->GetModelEstimationWeightingFunction();
                                const Real* pModelEstimationWeightingFunctionGreen = m_pGreenChannelResponceFunction->GetModelEstimationWeightingFunction();
                                const Real* pModelEstimationWeightingFunctionBlue = m_pBlueChannelResponceFunction->GetModelEstimationWeightingFunction();
                                for (Real ContinuousIntensity = Real(0) ; ContinuousIntensity <= Real(255) ; ContinuousIntensity += Increment)
                                {
                                    const int DiscreteIntensity = int(std::round(ContinuousIntensity));
                                    const Real eG_R = std::exp(pRegressionResponseFunctionRed[DiscreteIntensity]);
                                    const Real eG_G = std::exp(pRegressionResponseFunctionGreen[DiscreteIntensity]);
                                    const Real eG_B = std::exp(pRegressionResponseFunctionBlue[DiscreteIntensity]);
                                    const Real eGp_R = m_pRedChannelResponceFunction->GetIdealResponce(ContinuousIntensity);
                                    const Real eGp_G = m_pGreenChannelResponceFunction->GetIdealResponce(ContinuousIntensity);
                                    const Real eGp_B = m_pBlueChannelResponceFunction->GetIdealResponce(ContinuousIntensity);
                                    const Real Gp_R = std::log(eGp_R);
                                    const Real Gp_G = std::log(eGp_G);
                                    const Real Gp_B = std::log(eGp_B);
                                    OutputText << "\n" << ContinuousIntensity << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << DiscreteIntensity << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << pRegressionWeightingFunctionRed[DiscreteIntensity] << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << pRegressionWeightingFunctionGreen[DiscreteIntensity] << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << pRegressionWeightingFunctionBlue[DiscreteIntensity] << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << pModelEstimationWeightingFunctionRed[DiscreteIntensity] << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << pModelEstimationWeightingFunctionGreen[DiscreteIntensity] << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << pModelEstimationWeightingFunctionBlue[DiscreteIntensity] << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << pRegressionResponseFunctionRed[DiscreteIntensity] << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << pRegressionResponseFunctionGreen[DiscreteIntensity] << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << pRegressionResponseFunctionBlue[DiscreteIntensity] << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << eG_R << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << eG_G << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    OutputText << eG_B << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    if (ContinuousIntensity > MinimalIntensities[0])
                                    {
                                        OutputText << Gp_R << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    else
                                    {
                                        OutputText << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    if (ContinuousIntensity > MinimalIntensities[1])
                                    {
                                        OutputText << Gp_G << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    else
                                    {
                                        OutputText << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    if (ContinuousIntensity > MinimalIntensities[2])
                                    {
                                        OutputText << Gp_B << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    else
                                    {
                                        OutputText << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    if (ContinuousIntensity > MinimalIntensities[0])
                                    {
                                        OutputText << eGp_R << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    else
                                    {
                                        OutputText << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    if (ContinuousIntensity > MinimalIntensities[1])
                                    {
                                        OutputText << eGp_G << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    else
                                    {
                                        OutputText << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    if (ContinuousIntensity > MinimalIntensities[2])
                                    {
                                        OutputText << eGp_B << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    else
                                    {
                                        OutputText << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    if (ContinuousIntensity > MinimalIntensities[0])
                                    {
                                        OutputText << eG_R - eGp_R << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    else
                                    {
                                        OutputText << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    if (ContinuousIntensity > MinimalIntensities[1])
                                    {
                                        OutputText << eG_G - eGp_G << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    else
                                    {
                                        OutputText << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    if (ContinuousIntensity > MinimalIntensities[2])
                                    {
                                        OutputText << eG_B - eGp_B << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    else
                                    {
                                        OutputText << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    if (ContinuousIntensity > MinimalIntensities[0])
                                    {
                                        OutputText << std::abs(eG_R - eGp_R) << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    else
                                    {
                                        OutputText << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    if (ContinuousIntensity > MinimalIntensities[1])
                                    {
                                        OutputText << std::abs(eG_G - eGp_G) << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    else
                                    {
                                        OutputText << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                    if (ContinuousIntensity > MinimalIntensities[2])
                                    {
                                        OutputText << std::abs(eG_B - eGp_B);
                                    }
                                    else
                                    {
                                        OutputText << "NAN" << _RADIOMETRIC_RESPONCE_FUNCTION_DISPLAY_SEPARATOR_;
                                    }
                                }
                                OutputText << "\nFor more information see IEEE-RAS publication: Gonzalez-Aguirre, D.; Asfour, T.; Dillmann, R.; , \"Eccentricity edge-graphs from HDR images for object recognition by humanoid robots,\" Humanoid Robots (Humanoids), 2010 10th IEEE-RAS International Conference on , vol., no., pp.144-151, 6-8 Dec. 2010 doi: 10.1109/ICHR.2010.5686336";
                                return Files::COutFile::WriteStringToFile(PathFileName, OutputText.str());
                            }

                            bool CMultipleChannelRadiometricResponceFunction::SaveResponceFunctionToBinaryFile(const std::string& PathFileName) const
                            {
                                Files::COutFile OutputBinaryFile(PathFileName, Files::CFile::eBinary);
                                if (OutputBinaryFile.IsReady())
                                {
                                    if (!m_pRedChannelResponceFunction->SaveResponceFunctionToBinaryFile(OutputBinaryFile))
                                    {
                                        return false;
                                    }
                                    if (!m_pGreenChannelResponceFunction->SaveResponceFunctionToBinaryFile(OutputBinaryFile))
                                    {
                                        return false;
                                    }
                                    if (!m_pBlueChannelResponceFunction->SaveResponceFunctionToBinaryFile(OutputBinaryFile))
                                    {
                                        return false;
                                    }
                                    return OutputBinaryFile.Close();
                                }
                                return false;
                            }
                        }
                    }
                }
            }
        }
    }
}
