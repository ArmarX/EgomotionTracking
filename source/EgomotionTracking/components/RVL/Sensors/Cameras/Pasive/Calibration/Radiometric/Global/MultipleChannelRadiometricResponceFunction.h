/*
 * MultipleChannelRadiometricResponceFunction.h
 */

#pragma once

#include "RadiometricResponceFunction.h"
#include "../../../../../../Imaging/BayerPattern.h"
#include "../../../../../../Files/File.h"
#include "../../../../../../Files/InFile.h"
#include "../../../../../../Files/OutFile.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            class CMultipleChannelRadiometricResponceFunction
                            {
                            public:

                                static CMultipleChannelRadiometricResponceFunction* CreateFromFile(const std::string& pPathFileName);

                                CMultipleChannelRadiometricResponceFunction();
                                virtual ~CMultipleChannelRadiometricResponceFunction();

                                bool AreIdealResponceFunctionsReady() const;
                                bool LoadResponceFunctions(const CRadiometricResponceFunction* pRedRadiometricResponceFunction, const CRadiometricResponceFunction* pGreenRadiometricResponceFunction, const CRadiometricResponceFunction* pBlueRadiometricResponceFunction, const bool OwnerShip);
                                bool LoadResponceFunctionFromBinaryFile(const std::string& pPathFileName);
                                bool SaveResponceFunctionToFile(const std::string& pPathFileName, const Files::CFile::FileMode Mode) const;

                                uint64_t GetCameraId() const;
                                Imaging::CBayerPattern::BayerPatternType GetBayerPattern() const;
                                const CRadiometricResponceFunction* GetRedChannelResponceFunction() const;
                                const CRadiometricResponceFunction* GetGreenChannelResponceFunction() const;
                                const CRadiometricResponceFunction* GetBlueChannelResponceFunction() const;

                            protected:

                                void DestroyResponceFunctions();
                                bool SaveResponceFunctionToTextFile(const std::string& PathFileName) const;
                                bool SaveResponceFunctionToBinaryFile(const std::string& PathFileName) const;

                                bool m_OwnerShip;
                                const CRadiometricResponceFunction* m_pRedChannelResponceFunction;
                                const CRadiometricResponceFunction* m_pGreenChannelResponceFunction;
                                const CRadiometricResponceFunction* m_pBlueChannelResponceFunction;
                            };
                        }
                    }
                }
            }
        }
    }
}

