/*
 * PixelwiseRadiometricResponceFunction.cpp
 */

#include "PixelwiseRadiometricResponceFunction.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Local
                        {
                            CPixelwiseRadiometricResponceFunction* CPixelwiseRadiometricResponceFunction::MergeChannels(const CPixelwiseRadiometricResponceFunction* pResponceFunctionChannelRed, const CPixelwiseRadiometricResponceFunction* pResponceFunctionChannelGreen, const CPixelwiseRadiometricResponceFunction* pResponceFunctionChannelBlue)
                            {
                                if (pResponceFunctionChannelRed && pResponceFunctionChannelGreen && pResponceFunctionChannelBlue && pResponceFunctionChannelRed->IsEnabled() && pResponceFunctionChannelGreen->IsEnabled() && pResponceFunctionChannelBlue->IsEnabled() && (pResponceFunctionChannelRed->GetChannelContent() == Imaging::CBayerPattern::eRed) && (pResponceFunctionChannelGreen->GetChannelContent() == Imaging::CBayerPattern::eGreen) && (pResponceFunctionChannelBlue->GetChannelContent() == Imaging::CBayerPattern::eBlue) && (pResponceFunctionChannelRed->GetBayerPattern() != Imaging::CBayerPattern::BayerPatternType(-1)) && (pResponceFunctionChannelRed->GetBayerPattern() == pResponceFunctionChannelGreen->GetBayerPattern()) && (pResponceFunctionChannelGreen->GetBayerPattern() == pResponceFunctionChannelBlue->GetBayerPattern()) && pResponceFunctionChannelRed->GetCameraId() && (pResponceFunctionChannelRed->GetCameraId() == pResponceFunctionChannelGreen->GetCameraId()) && (pResponceFunctionChannelGreen->GetCameraId() == pResponceFunctionChannelBlue->GetCameraId()))
                                {
                                    const Imaging::TImage<CRadiometricResponceFunctionPixel>* pPixelResponceImageChannelRed = pResponceFunctionChannelRed->GetPixelwiseRadiometricResponceImage();
                                    const Imaging::TImage<CRadiometricResponceFunctionPixel>* pPixelResponceImageChannelGreen = pResponceFunctionChannelGreen->GetPixelwiseRadiometricResponceImage();
                                    const Imaging::TImage<CRadiometricResponceFunctionPixel>* pPixelResponceImageChannelBlue = pResponceFunctionChannelBlue->GetPixelwiseRadiometricResponceImage();
                                    if ((pPixelResponceImageChannelRed->GetSize() == pPixelResponceImageChannelGreen->GetSize()) && (pPixelResponceImageChannelGreen->GetSize() == pPixelResponceImageChannelBlue->GetSize()))
                                    {
                                        Imaging::CBayerPattern BayerPatternMapLoader;
                                        if (BayerPatternMapLoader.SetType(pResponceFunctionChannelRed->GetBayerPattern()))
                                        {
                                            Imaging::CBayerPattern::ChannelContentMap BayerPatternMap = BayerPatternMapLoader.GetBayerPatternMap();
                                            const Imaging::CImageSize& Size = pPixelResponceImageChannelRed->GetSize();
                                            const int Width = Size.GetWidth();
                                            const int Height = Size.GetHeight();
                                            CPixelwiseRadiometricResponceFunction* pMergeResponceFunction = new CPixelwiseRadiometricResponceFunction(Size);
                                            const CRadiometricResponceFunctionPixel* pPixelResponcePerChannel[3] = { nullptr };
                                            pPixelResponcePerChannel[Imaging::CBayerPattern::eRed] = pPixelResponceImageChannelRed->GetReadOnlyBuffer();
                                            pPixelResponcePerChannel[Imaging::CBayerPattern::eGreen] = pPixelResponceImageChannelGreen->GetReadOnlyBuffer();
                                            pPixelResponcePerChannel[Imaging::CBayerPattern::eBlue] = pPixelResponceImageChannelBlue->GetReadOnlyBuffer();
                                            CRadiometricResponceFunctionPixel* pMergePixelResponce = pMergeResponceFunction->m_pPixelResponceImage->GetWritableBuffer();
                                            for (int Y = 0 ; Y < Height ; ++Y)
                                                for (int X = 0 ; X < Width ; ++X, ++pPixelResponcePerChannel[Imaging::CBayerPattern::eRed], ++pPixelResponcePerChannel[Imaging::CBayerPattern::eGreen], ++pPixelResponcePerChannel[Imaging::CBayerPattern::eBlue])
                                                {
                                                    *pMergePixelResponce++ = *pPixelResponcePerChannel[BayerPatternMap.m_YX[Y & 0X1][X & 0X1]];
                                                }
                                            return pMergeResponceFunction;
                                        }
                                    }
                                }
                                return nullptr;
                            }

                            bool CPixelwiseRadiometricResponceFunction::SaveMergedChannels(const std::string& PathFileName, const CPixelwiseRadiometricResponceFunction* pResponceFunctionChannelRed, const CPixelwiseRadiometricResponceFunction* pResponceFunctionChannelGreen, const CPixelwiseRadiometricResponceFunction* pResponceFunctionChannelBlue)
                            {
                                const CPixelwiseRadiometricResponceFunction* pMergeResponceFunction = MergeChannels(pResponceFunctionChannelRed, pResponceFunctionChannelGreen, pResponceFunctionChannelBlue);
                                if (pMergeResponceFunction)
                                {
                                    const bool Result = pMergeResponceFunction->SaveResponceFunctionToFile(PathFileName);
                                    delete pMergeResponceFunction;
                                    pMergeResponceFunction = nullptr;
                                    return Result;
                                }
                                return false;
                            }

                            CPixelwiseRadiometricResponceFunction* CPixelwiseRadiometricResponceFunction::CreateFromFile(const std::string& PathFileName)
                            {
                                if (Files::CFile::Exists(PathFileName))
                                {
                                    CPixelwiseRadiometricResponceFunction* pPixelwiseRadiometricResponceFunction = new CPixelwiseRadiometricResponceFunction();
                                    if (pPixelwiseRadiometricResponceFunction->LoadResponceFunctionFile(PathFileName))
                                    {
                                        return pPixelwiseRadiometricResponceFunction;
                                    }
                                    delete pPixelwiseRadiometricResponceFunction;
                                    pPixelwiseRadiometricResponceFunction = nullptr;
                                }
                                return nullptr;
                            }

                            CPixelwiseRadiometricResponceFunction::CPixelwiseRadiometricResponceFunction() :
                                m_CameraId(0),
                                m_BayerPattern(Imaging::CBayerPattern::BayerPatternType(-1)),
                                m_ChannelContent(Imaging::CBayerPattern::Channel(-1)),
                                m_pPixelResponceImage(nullptr)
                            {
                            }

                            CPixelwiseRadiometricResponceFunction::CPixelwiseRadiometricResponceFunction(const Imaging::CImageSize& Size) :
                                m_CameraId(0),
                                m_BayerPattern(Imaging::CBayerPattern::BayerPatternType(-1)),
                                m_ChannelContent(Imaging::CBayerPattern::Channel(-1)),
                                m_pPixelResponceImage(nullptr)
                            {
                                if (Size.IsValid())
                                {
                                    m_pPixelResponceImage = new Imaging::TImage<CRadiometricResponceFunctionPixel>(Size);
                                }
                            }

                            CPixelwiseRadiometricResponceFunction::~CPixelwiseRadiometricResponceFunction()
                            {
                                delete m_pPixelResponceImage;
                                m_pPixelResponceImage = nullptr;
                            }

                            bool CPixelwiseRadiometricResponceFunction::IsEnabled() const
                            {
                                return m_pPixelResponceImage && m_pPixelResponceImage->IsValid();
                            }

                            bool CPixelwiseRadiometricResponceFunction::LoadResponceFunctionFromParameters(const uint64_t CameraId, const Imaging::CBayerPattern::BayerPatternType BayerPattern, const Imaging::CBayerPattern::Channel ChannelContent, const Imaging::TImage<CReferencedRadiometricPixel>* pPixelwiseRadiometricImage)
                            {
                                if (CameraId && (((BayerPattern != Imaging::CBayerPattern::BayerPatternType(-1)) && (ChannelContent != Imaging::CBayerPattern::Channel(-1))) || ((BayerPattern == Imaging::CBayerPattern::BayerPatternType(-1)) && (ChannelContent == Imaging::CBayerPattern::Channel(-1)))))
                                {
                                    m_CameraId = CameraId;
                                    m_BayerPattern = BayerPattern;
                                    m_ChannelContent = ChannelContent;

                                    delete m_pPixelResponceImage;
                                    m_pPixelResponceImage = nullptr;

                                    m_pPixelResponceImage = new Imaging::TImage<CRadiometricResponceFunctionPixel>(pPixelwiseRadiometricImage->GetSize());
                                    const CReferencedRadiometricPixel* pPixelwiseRadiometricPixel = pPixelwiseRadiometricImage->GetReadOnlyBuffer();
                                    const CRadiometricResponceFunctionPixel* const pPixelResponceEnd = m_pPixelResponceImage->GetBufferEnd();
                                    CRadiometricResponceFunctionPixel* pPixelResponce = m_pPixelResponceImage->GetWritableBuffer();
                                    while (pPixelResponce < pPixelResponceEnd)
                                    {
                                        pPixelResponce->SetFromRadiometricPixel(pPixelwiseRadiometricPixel++);
                                        ++pPixelResponce;
                                    }
                                    return true;
                                }
                                return false;
                            }

                            bool CPixelwiseRadiometricResponceFunction::LoadResponceFunctionFile(const std::string& PathFileName)
                            {
                                if (Files::CFile::Exists(PathFileName))
                                {
                                    Files::CInFile InputBinaryFile(PathFileName, Files::CFile::eBinary);
                                    if (InputBinaryFile.IsReady())
                                        if (InputBinaryFile.Read(&m_CameraId, sizeof(uint64_t)))
                                            if (InputBinaryFile.Read(&m_BayerPattern, sizeof(Imaging::CBayerPattern::BayerPatternType)))
                                                if (InputBinaryFile.Read(&m_ChannelContent, sizeof(Imaging::CBayerPattern::Channel)))
                                                {
                                                    Imaging::CImageSize Size;
                                                    if (InputBinaryFile.Read(&Size, sizeof(Imaging::CImageSize)) && Size.IsValid())
                                                    {
                                                        delete m_pPixelResponceImage;
                                                        m_pPixelResponceImage = nullptr;
                                                        m_pPixelResponceImage = new Imaging::TImage<CRadiometricResponceFunctionPixel>(Size);
                                                        if (InputBinaryFile.Read(m_pPixelResponceImage->GetWritableBuffer(), m_pPixelResponceImage->GetBufferSize()))
                                                        {
                                                            return InputBinaryFile.Close();
                                                        }
                                                    }
                                                }
                                }
                                return false;
                            }

                            bool CPixelwiseRadiometricResponceFunction::SaveResponceFunctionToFile(const std::string& PathFileName) const
                            {
                                if (m_pPixelResponceImage && m_pPixelResponceImage->IsValid())
                                {
                                    Files::COutFile OutputBinaryFile(PathFileName, Files::CFile::eBinary);
                                    if (OutputBinaryFile.IsReady())
                                        if (OutputBinaryFile.Write(&m_CameraId, sizeof(uint64_t)))
                                            if (OutputBinaryFile.Write(&m_BayerPattern, sizeof(Imaging::CBayerPattern::BayerPatternType)))
                                                if (OutputBinaryFile.Write(&m_ChannelContent, sizeof(Imaging::CBayerPattern::Channel)))
                                                    if (OutputBinaryFile.Write(&m_pPixelResponceImage->GetSize(), sizeof(Imaging::CImageSize)))
                                                        if (OutputBinaryFile.Write(m_pPixelResponceImage->GetReadOnlyBuffer(), m_pPixelResponceImage->GetBufferSize()))
                                                        {
                                                            return OutputBinaryFile.Close();
                                                        }
                                }
                                return false;
                            }

                            bool CPixelwiseRadiometricResponceFunction::ExportSlopeToImage(const std::string& PathFileName, const Imaging::CDiscreteRGBColorMap* pDiscreteRGBColorMap) const
                            {
                                if (m_pPixelResponceImage && m_pPixelResponceImage->IsValid())
                                {
                                    Imaging::TImage<Real> Image(m_pPixelResponceImage->GetSize());
                                    Real Maximal = g_RealMinusInfinity, Minimal = g_RealPlusInfinity;
                                    const Real* const pIntensityPixelEnd = Image.GetBufferEnd();
                                    Real* pIntensityPixel = Image.GetWritableBuffer();
                                    const CRadiometricResponceFunctionPixel* pPixelwisePixelResponce = m_pPixelResponceImage->GetReadOnlyBuffer();
                                    while (pIntensityPixel < pIntensityPixelEnd)
                                    {
                                        if (pPixelwisePixelResponce->IsValid())
                                        {
                                            const Real Value = pPixelwisePixelResponce->GetSlope();
                                            if (Value > Maximal)
                                            {
                                                Maximal = Value;
                                            }
                                            if (Value < Minimal)
                                            {
                                                Minimal = Value;
                                            }
                                            *pIntensityPixel++ = Value;
                                        }
                                        else
                                        {
                                            *pIntensityPixel++ = g_RealPlusInfinity;
                                        }
                                        ++pPixelwisePixelResponce;
                                    }
                                    return ExportImage(PathFileName, Image, Maximal, Minimal, pDiscreteRGBColorMap);
                                }
                                return false;
                            }

                            bool CPixelwiseRadiometricResponceFunction::ExportOffsetToImage(const std::string& PathFileName, const Imaging::CDiscreteRGBColorMap* pDiscreteRGBColorMap) const
                            {
                                if (m_pPixelResponceImage && m_pPixelResponceImage->IsValid())
                                {
                                    Imaging::TImage<Real> Image(m_pPixelResponceImage->GetSize());
                                    Real Maximal = g_RealMinusInfinity, Minimal = g_RealPlusInfinity;
                                    const Real* const pIntensityPixelEnd = Image.GetBufferEnd();
                                    Real* pIntensityPixel = Image.GetWritableBuffer();
                                    const CRadiometricResponceFunctionPixel* pPixelwisePixelResponce = m_pPixelResponceImage->GetReadOnlyBuffer();
                                    while (pIntensityPixel < pIntensityPixelEnd)
                                    {
                                        if (pPixelwisePixelResponce->IsValid())
                                        {
                                            const Real Value = pPixelwisePixelResponce->GetOffset();
                                            if (Value > Maximal)
                                            {
                                                Maximal = Value;
                                            }
                                            if (Value < Minimal)
                                            {
                                                Minimal = Value;
                                            }
                                            *pIntensityPixel++ = Value;
                                        }
                                        else
                                        {
                                            *pIntensityPixel++ = g_RealPlusInfinity;
                                        }
                                        ++pPixelwisePixelResponce;
                                    }
                                    return ExportImage(PathFileName, Image, Maximal, Minimal, pDiscreteRGBColorMap);
                                }
                                return false;
                            }

                            bool CPixelwiseRadiometricResponceFunction::ExportRMSDeviationToImage(const std::string& PathFileName, const Imaging::CDiscreteRGBColorMap* pDiscreteRGBColorMap) const
                            {
                                if (m_pPixelResponceImage && m_pPixelResponceImage->IsValid())
                                {
                                    Imaging::TImage<Real> Image(m_pPixelResponceImage->GetSize());
                                    Real Maximal = g_RealMinusInfinity, Minimal = g_RealPlusInfinity;
                                    const Real* const pIntensityPixelEnd = Image.GetBufferEnd();
                                    Real* pIntensityPixel = Image.GetWritableBuffer();
                                    const CRadiometricResponceFunctionPixel* pPixelwisePixelResponce = m_pPixelResponceImage->GetReadOnlyBuffer();
                                    while (pIntensityPixel < pIntensityPixelEnd)
                                    {
                                        if (pPixelwisePixelResponce->IsValid())
                                        {
                                            const Real Value = pPixelwisePixelResponce->GetRMSDeviation();
                                            if (Value > Maximal)
                                            {
                                                Maximal = Value;
                                            }
                                            if (Value < Minimal)
                                            {
                                                Minimal = Value;
                                            }
                                            *pIntensityPixel++ = Value;
                                        }
                                        else
                                        {
                                            *pIntensityPixel++ = g_RealPlusInfinity;
                                        }
                                        ++pPixelwisePixelResponce;
                                    }
                                    return ExportImage(PathFileName, Image, Maximal, Minimal, pDiscreteRGBColorMap);
                                }
                                return false;
                            }

                            bool CPixelwiseRadiometricResponceFunction::ExportPeakNegativeDeviationToImage(const std::string& PathFileName, const Imaging::CDiscreteRGBColorMap* pDiscreteRGBColorMap) const
                            {
                                if (m_pPixelResponceImage && m_pPixelResponceImage->IsValid())
                                {
                                    Imaging::TImage<Real> Image(m_pPixelResponceImage->GetSize());
                                    Real Maximal = g_RealMinusInfinity, Minimal = g_RealPlusInfinity;
                                    const Real* const pIntensityPixelEnd = Image.GetBufferEnd();
                                    Real* pIntensityPixel = Image.GetWritableBuffer();
                                    const CRadiometricResponceFunctionPixel* pPixelwisePixelResponce = m_pPixelResponceImage->GetReadOnlyBuffer();
                                    while (pIntensityPixel < pIntensityPixelEnd)
                                    {
                                        if (pPixelwisePixelResponce->IsValid())
                                        {
                                            const Real Value = pPixelwisePixelResponce->GetPeakNegativeDeviation();
                                            if (Value > Maximal)
                                            {
                                                Maximal = Value;
                                            }
                                            if (Value < Minimal)
                                            {
                                                Minimal = Value;
                                            }
                                            *pIntensityPixel++ = Value;
                                        }
                                        else
                                        {
                                            *pIntensityPixel++ = g_RealPlusInfinity;
                                        }
                                        ++pPixelwisePixelResponce;
                                    }
                                    return ExportImage(PathFileName, Image, Maximal, Minimal, pDiscreteRGBColorMap);
                                }
                                return false;
                            }

                            bool CPixelwiseRadiometricResponceFunction::ExportPeakPositiveDeviationToImage(const std::string& PathFileName, const Imaging::CDiscreteRGBColorMap* pDiscreteRGBColorMap) const
                            {
                                if (m_pPixelResponceImage && m_pPixelResponceImage->IsValid())
                                {
                                    Imaging::TImage<Real> Image(m_pPixelResponceImage->GetSize());
                                    Real Maximal = g_RealMinusInfinity, Minimal = g_RealPlusInfinity;
                                    const Real* const pIntensityPixelEnd = Image.GetBufferEnd();
                                    Real* pIntensityPixel = Image.GetWritableBuffer();
                                    const CRadiometricResponceFunctionPixel* pPixelwisePixelResponce = m_pPixelResponceImage->GetReadOnlyBuffer();
                                    while (pIntensityPixel < pIntensityPixelEnd)
                                    {
                                        if (pPixelwisePixelResponce->IsValid())
                                        {
                                            const Real Value = pPixelwisePixelResponce->GetPeakPositiveDeviation();
                                            if (Value > Maximal)
                                            {
                                                Maximal = Value;
                                            }
                                            if (Value < Minimal)
                                            {
                                                Minimal = Value;
                                            }
                                            *pIntensityPixel++ = Value;
                                        }
                                        else
                                        {
                                            *pIntensityPixel++ = g_RealPlusInfinity;
                                        }
                                        ++pPixelwisePixelResponce;
                                    }
                                    return ExportImage(PathFileName, Image, Maximal, Minimal, pDiscreteRGBColorMap);
                                }
                                return false;
                            }

                            bool CPixelwiseRadiometricResponceFunction::ExportRangeDeviationToImage(const std::string& PathFileName, const Imaging::CDiscreteRGBColorMap* pDiscreteRGBColorMap) const
                            {
                                if (m_pPixelResponceImage && m_pPixelResponceImage->IsValid())
                                {
                                    Imaging::TImage<Real> Image(m_pPixelResponceImage->GetSize());
                                    Real Maximal = g_RealMinusInfinity, Minimal = g_RealPlusInfinity;
                                    const Real* const pIntensityPixelEnd = Image.GetBufferEnd();
                                    Real* pIntensityPixel = Image.GetWritableBuffer();
                                    const CRadiometricResponceFunctionPixel* pPixelwisePixelResponce = m_pPixelResponceImage->GetReadOnlyBuffer();
                                    while (pIntensityPixel < pIntensityPixelEnd)
                                    {
                                        if (pPixelwisePixelResponce->IsValid())
                                        {
                                            const Real Value = pPixelwisePixelResponce->GetRangeDeviation();
                                            if (Value > Maximal)
                                            {
                                                Maximal = Value;
                                            }
                                            if (Value < Minimal)
                                            {
                                                Minimal = Value;
                                            }
                                            *pIntensityPixel++ = Value;
                                        }
                                        else
                                        {
                                            *pIntensityPixel++ = g_RealPlusInfinity;
                                        }
                                        ++pPixelwisePixelResponce;
                                    }
                                    return ExportImage(PathFileName, Image, Maximal, Minimal, pDiscreteRGBColorMap);
                                }
                                return false;
                            }

                            uint64_t CPixelwiseRadiometricResponceFunction::GetCameraId() const
                            {
                                return m_CameraId;
                            }

                            Imaging::CBayerPattern::BayerPatternType CPixelwiseRadiometricResponceFunction::GetBayerPattern() const
                            {
                                return m_BayerPattern;
                            }

                            Imaging::CBayerPattern::Channel CPixelwiseRadiometricResponceFunction::GetChannelContent() const
                            {
                                return m_ChannelContent;
                            }

                            Imaging::TImage<CRadiometricResponceFunctionPixel>* CPixelwiseRadiometricResponceFunction::GetPixelwiseRadiometricResponceImage() const
                            {
                                return m_pPixelResponceImage;
                            }

                            bool CPixelwiseRadiometricResponceFunction::ExportImage(const std::string& PathFileName, const Imaging::TImage<Real>& Image, const Real Maximal, const Real Minimal, const Imaging::CDiscreteRGBColorMap* pDiscreteRGBColorMap) const
                            {
                                const Real Range = Maximal - Minimal;
                                if (PathFileName.length() && Image.IsValid() && pDiscreteRGBColorMap && IsPositive(Range))
                                {
                                    Imaging::TImage<Imaging::DiscreteRGBPixel> ExportingImage(Image.GetSize());
                                    ExportingImage.Clear();
                                    const Imaging::DiscreteRGBPixel* const pRGBPixelEnd = ExportingImage.GetBufferEnd();
                                    Imaging::DiscreteRGBPixel* pRGBPixel = ExportingImage.GetWritableBuffer();
                                    const Imaging::DiscreteRGBPixel* pLUT = pDiscreteRGBColorMap->GetLUT();
                                    const Real* pIntenistyPixel = Image.GetReadOnlyBuffer();
                                    const Real Scale = pDiscreteRGBColorMap->GetLUTScaleFactor() / Range;
                                    while (pRGBPixel < pRGBPixelEnd)
                                    {
                                        if (*pIntenistyPixel != g_RealPlusInfinity)
                                        {
                                            *pRGBPixel = pLUT[int((*pIntenistyPixel - Minimal) * Scale + Real(0.5))];
                                        }
                                        ++pIntenistyPixel;
                                        ++pRGBPixel;
                                    }
                                    return Imaging::CImageExporter::Export(&ExportingImage, PathFileName);
                                }
                                return false;
                            }
                        }
                    }
                }
            }
        }
    }
}

