/*
 * RadiometricDistribution.cpp
 */

#include "RadiometricDistribution.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            CRadiometricDistribution::CRadiometricDistribution() :
                                m_Kernel(Base::CRadianceWeigthingKernel::eGaussian),
                                m_pExposureDistribution(nullptr),
                                m_pExposureDualIndices(nullptr),
                                m_MaximalIntensity(Real(255)),
                                m_MinimalIntensity(Real(0)),
                                m_UpperIntensityAtScopeDensity(Real(255)),
                                m_LowerIntensityAtScopeDensity(Real(0)),
                                m_ScopeKernelDensity(g_RealPlusEpsilon),
                                m_ConsistentSafeMaximalKernelDensity(g_RealPlusEpsilon),
                                m_ScaleFactorLogExposureTime(Real(0)),
                                m_TotalUnderExposedPixels(0),
                                m_TotalOverExposedPixels(0)
                            {
                            }

                            CRadiometricDistribution::~CRadiometricDistribution()
                            {
                                delete[] m_pExposureDistribution;
                                delete[] m_pExposureDualIndices;
                            }

                            bool CRadiometricDistribution::AddSynthesizer(const CRadiometricBayerPatternSynthesizer* pBayerPatternSynthesizer)
                            {
                                if (pBayerPatternSynthesizer)
                                {
                                    const std::uint64_t CameraId = pBayerPatternSynthesizer->GetCameraId();
                                    std::map<std::uint64_t, const CRadiometricBayerPatternSynthesizer*>::const_iterator pLocation = m_Synthesizers.find(CameraId);
                                    if (pLocation == m_Synthesizers.end())
                                    {
                                        m_Synthesizers[CameraId] = pBayerPatternSynthesizer;
                                        const CMultipleChannelRadiometricResponceFunction* pMultipleChannelRadiometricResponceFunction = pBayerPatternSynthesizer->GetMultipleChannelRadiometricResponceFunction();
                                        m_RadiometricResponceFunctions.push_back(pMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction());
                                        m_RadiometricResponceFunctions.push_back(pMultipleChannelRadiometricResponceFunction->GetGreenChannelResponceFunction());
                                        m_RadiometricResponceFunctions.push_back(pMultipleChannelRadiometricResponceFunction->GetBlueChannelResponceFunction());
                                        return true;
                                    }
                                }
                                return false;
                            }

                            bool CRadiometricDistribution::RemoveAllSynthesizers()
                            {
                                if (m_Synthesizers.size())
                                {
                                    m_Synthesizers.clear();
                                    return true;
                                }
                                return false;
                            }

                            bool CRadiometricDistribution::LoadConfiguration(const Base::CRadianceWeigthingKernel::KernelType Kernel, const Real ScopeKernelDensity, const Real IntensityMargin)
                            {
                                if (!LoadExposuresFromSynthesizers())
                                {
                                    return false;
                                }
                                if (!UpdateMinimalIntensity(IntensityMargin))
                                {
                                    return false;
                                }
                                if (!UpdateConsistentSafeMaximalKernelDensity(Kernel))
                                {
                                    return false;
                                }
                                if (!CreateCalibratedExposures(Kernel))
                                {
                                    return false;
                                }
                                if (!CreateExposureDistribution())
                                {
                                    return false;
                                }
                                if (!CreateExposureDualIndices())
                                {
                                    return false;
                                }
                                return ConfigureExposureScopeByDensity(ScopeKernelDensity);
                            }

                            bool CRadiometricDistribution::LoadDistributionFromSynthesizers(const Real CriticallKernelDensity)
                            {
                                if (m_Synthesizers.size() && m_CalibratedExposures.size())
                                {
                                    m_TotalUnderExposedPixels = 0;
                                    m_TotalOverExposedPixels = 0;
                                    bool Result = ClearDistribution();
                                    if (Result)
                                        for (std::map<std::uint64_t, const CRadiometricBayerPatternSynthesizer*>::const_iterator pKeySynthesizer = m_Synthesizers.begin() ; pKeySynthesizer != m_Synthesizers.end() ; ++pKeySynthesizer)
                                        {
                                            Result &= MapExposureFrequencyDistribution(pKeySynthesizer->second->GetMultipleChannelRadiometricResponceFunction(), pKeySynthesizer->second->GetLogRadianceImage(), pKeySynthesizer->second->GetDensityWeightedImage(), pKeySynthesizer->second->GetBayerPattern(), CriticallKernelDensity);
                                        }
                                    return Result;
                                }
                                return false;
                            }

                            bool CRadiometricDistribution::ClearDistribution()
                            {
                                const int TotalExposures = m_CalibratedExposures.size();
                                if (TotalExposures)
                                {
                                    ExposureBin* pExposureBin = m_pExposureDistribution;
                                    for (int i = 0 ; i < TotalExposures ; ++i, ++pExposureBin)
                                    {
                                        pExposureBin->m_Active = true;
                                        pExposureBin->m_SamplingDensityAccumulator = Real(0);
                                        pExposureBin->m_ScopeSamplingDensityAccumulator = Real(0);
                                        pExposureBin->m_MinimalDensity = g_RealPlusInfinity;
                                    }
                                    return true;
                                }
                                return false;
                            }

                            bool CRadiometricDistribution::LoadExposuresFromSynthesizers()
                            {
                                m_Exposures.clear();
                                for (std::map<std::uint64_t, const CRadiometricBayerPatternSynthesizer*>::const_iterator pKeySynthesizer = m_Synthesizers.begin() ; pKeySynthesizer != m_Synthesizers.end() ; ++pKeySynthesizer)
                                {
                                    m_Exposures.insert(m_Exposures.end(), pKeySynthesizer->second->GetExposures().begin(), pKeySynthesizer->second->GetExposures().end());
                                }
                                m_Exposures.sort(Base::CExposure::SortExposuresByParametricIndex);
                                m_Exposures.unique(Base::CExposure::EqualsExposuresByParametricIndex);
                                return m_Exposures.size();
                            }

                            bool CRadiometricDistribution::UpdateMinimalIntensity(const Real IntensityMargin)
                            {
                                if (m_RadiometricResponceFunctions.size())
                                {
                                    m_MaximalIntensity = Real(255) - IntensityMargin;
                                    m_MinimalIntensity = IntensityMargin;
                                    for (std::list<const CRadiometricResponceFunction*>::const_iterator ppRadiometricResponceFunction = m_RadiometricResponceFunctions.begin() ; ppRadiometricResponceFunction != m_RadiometricResponceFunctions.end() ; ++ppRadiometricResponceFunction)
                                    {
                                        const Real MinimalIntensity = (*ppRadiometricResponceFunction)->GetMinimalIntensity();
                                        if (MinimalIntensity > m_MinimalIntensity)
                                        {
                                            m_MinimalIntensity = MinimalIntensity;
                                        }
                                    }
                                    m_MinimalIntensity += g_RealPlusEpsilon;
                                    return true;
                                }
                                return false;
                            }

                            bool CRadiometricDistribution::UpdateConsistentSafeMaximalKernelDensity(const Base::CRadianceWeigthingKernel::KernelType Kernel)
                            {
                                m_Kernel = Kernel;
                                m_ConsistentSafeMaximalKernelDensity = Real(0);
                                Real PreviousTime = m_Exposures.front().GetTime();
                                for (std::list<Base::CExposure>::const_iterator pExposure = ++m_Exposures.begin() ; pExposure != m_Exposures.end() ; ++pExposure)
                                {
                                    const Real CurrentTime = pExposure->GetTime();
                                    for (std::list<const CRadiometricResponceFunction*>::const_iterator ppRadiometricResponceFunction = m_RadiometricResponceFunctions.begin() ; ppRadiometricResponceFunction != m_RadiometricResponceFunctions.end() ; ++ppRadiometricResponceFunction)
                                    {
                                        const CRadiometricResponceFunction* pResponceFunction = *ppRadiometricResponceFunction;
                                        const Real EA1 = pResponceFunction->GetIdealRadiance(m_MaximalIntensity, PreviousTime);
                                        const Real EA0 = pResponceFunction->GetIdealRadiance(m_MinimalIntensity, PreviousTime);
                                        const Real EB1 = pResponceFunction->GetIdealRadiance(m_MaximalIntensity, CurrentTime);
                                        const Real EB0 = pResponceFunction->GetIdealRadiance(m_MinimalIntensity, CurrentTime);
                                        if ((EA0 > EB1) || (EA1 < EB0))
                                        {
                                            m_ConsistentSafeMaximalKernelDensity = Real(0);
                                            return false;
                                        }
                                        const Real E1 = std::min(EA1, EB1);
                                        const Real E0 = std::max(EA0, EB0);

                                        const Real D0 = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(pResponceFunction->GetIdealIntensity(E1, PreviousTime), Kernel);
                                        const Real D1 = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(pResponceFunction->GetIdealIntensity(E0, PreviousTime), Kernel);
                                        const Real D2 = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(pResponceFunction->GetIdealIntensity(E1, CurrentTime), Kernel);
                                        const Real D3 = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(pResponceFunction->GetIdealIntensity(E0, CurrentTime), Kernel);
                                        m_ConsistentSafeMaximalKernelDensity = std::max(m_ConsistentSafeMaximalKernelDensity, std::max(std::max(D0, D1), std::max(D2, D3)));

                                        /*const Real E = (E1 + E0) * Real(0.5);
                                         const Real IA = std::max(std::floor(pResponceFunction->GetIdealIntensity(E, PreviousTime)) - Real(1), m_MinimalIntensity);
                                         const Real IB = std::min(std::ceil(pResponceFunction->GetIdealIntensity(E, CurrentTime)) + Real(1), m_MaximalIntensity);
                                         const Real DIA = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(IA, Kernel);
                                         const Real DIB = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(IB, Kernel);
                                         m_ConsistentSafeMaximalKernelDensity = std::min(m_ConsistentSafeMaximalKernelDensity, std::min(DIA, DIB));*/
                                    }
                                    PreviousTime = CurrentTime;
                                }
                                return true;
                            }

                            bool CRadiometricDistribution::CreateCalibratedExposures(const Base::CRadianceWeigthingKernel::KernelType Kernel)
                            {
                                m_CalibratedExposures.clear();
                                m_CalibratedExposures.reserve(m_Exposures.size());
                                const Real DensityConditionedUpperIntensity = std::min(std::ceil(m_RadianceWeigthingKernel.GetUpperIntensityAtDensity(Kernel, m_ConsistentSafeMaximalKernelDensity)) + Real(1), m_MaximalIntensity);
                                const Real DensityConditionedLowerIntensity = std::max(std::floor(m_RadianceWeigthingKernel.GetLowerIntensityAtDensity(Kernel, m_ConsistentSafeMaximalKernelDensity)) - Real(1), m_MinimalIntensity);
                                for (std::list<Base::CExposure>::const_iterator pExposure = m_Exposures.begin() ; pExposure != m_Exposures.end() ; ++pExposure)
                                {
                                    const Real ExposureTime = pExposure->GetTime();
                                    Real CommonE1 = g_RealPlusInfinity, CommonE0 = g_IntegerMinusInfinity, CommonT0 = g_IntegerMinusInfinity, CommonT1 = g_RealPlusInfinity;
                                    for (std::list<const CRadiometricResponceFunction*>::const_iterator ppRadiometricResponceFunction = m_RadiometricResponceFunctions.begin() ; ppRadiometricResponceFunction != m_RadiometricResponceFunctions.end() ; ++ppRadiometricResponceFunction)
                                    {
                                        const CRadiometricResponceFunction* pResponceFunction = *ppRadiometricResponceFunction;
                                        const Real E1 = pResponceFunction->GetIdealRadiance(DensityConditionedUpperIntensity, ExposureTime);
                                        const Real E0 = pResponceFunction->GetIdealRadiance(DensityConditionedLowerIntensity, ExposureTime);
                                        const Real T0 = pResponceFunction->GetIdealExposureTime(DensityConditionedLowerIntensity, E1);
                                        const Real T1 = pResponceFunction->GetIdealExposureTime(DensityConditionedUpperIntensity, E0);
                                        if (E1 < CommonE1)
                                        {
                                            CommonE1 = E1;
                                        }
                                        if (E0 > CommonE0)
                                        {
                                            CommonE0 = E0;
                                        }
                                        if (T1 < CommonT1)
                                        {
                                            CommonT1 = T1;
                                        }
                                        if (T0 > CommonT0)
                                        {
                                            CommonT0 = T0;
                                        }
                                    }
                                    if ((CommonT0 >= CommonT1) || (CommonE0 >= CommonE1))
                                    {
                                        m_CalibratedExposures.clear();
                                        return false;
                                    }
                                    m_CalibratedExposures.push_back(Base::CCalibratedExposure(pExposure->GetParametricIndex(), m_ConsistentSafeMaximalKernelDensity, CommonE0, CommonE1, CommonT0, ExposureTime, CommonT1));
                                }
                                return true;
                            }

                            std::vector<Base::CCalibratedExposure> CRadiometricDistribution::GetMinimalExposureSet() const
                            {
                                const int TotalCalibratedExposures = m_CalibratedExposures.size();
                                std::vector<Base::CCalibratedExposure> SelectedCalibratedExposures;
                                if (TotalCalibratedExposures)
                                {
                                    Base::CCalibratedExposure SelectedCalibratedExposure = m_CalibratedExposures[0];
                                    SelectedCalibratedExposures.push_back(SelectedCalibratedExposure);
                                    for (int i = 1 ; i < TotalCalibratedExposures ; ++i)
                                        if (SelectedCalibratedExposure.HasCommonRadianceScope(m_CalibratedExposures[i]))
                                        {
                                            Base::CCalibratedExposure MinimalOverlappingExposure = m_CalibratedExposures[i];
                                            Real MinimalOverlapping = SelectedCalibratedExposure.GetCommonRadianceScope(MinimalOverlappingExposure);
                                            for (int j = i + 1 ; j < TotalCalibratedExposures ; ++j)
                                                if (SelectedCalibratedExposure.HasCommonRadianceScope(m_CalibratedExposures[j]))
                                                {
                                                    const Real Overlapping = SelectedCalibratedExposure.GetCommonRadianceScope(m_CalibratedExposures[j]);
                                                    if (Overlapping < MinimalOverlapping)
                                                    {
                                                        MinimalOverlapping = Overlapping;
                                                        MinimalOverlappingExposure = m_CalibratedExposures[j];
                                                        i = j;
                                                    }
                                                }
                                                else
                                                {
                                                    break;
                                                }
                                            SelectedCalibratedExposures.push_back(MinimalOverlappingExposure);
                                            SelectedCalibratedExposure = MinimalOverlappingExposure;
                                        }
                                        else
                                        {
                                            SelectedCalibratedExposure = m_CalibratedExposures[i];
                                            SelectedCalibratedExposures.push_back(SelectedCalibratedExposure);
                                        }
                                }
                                return SelectedCalibratedExposures;
                            }

                            bool CRadiometricDistribution::CreateExposureDistribution()
                            {
                                if (m_pExposureDistribution)
                                {
                                    delete[] m_pExposureDistribution;
                                    m_pExposureDistribution = nullptr;
                                }
                                const int TotalExposures = m_CalibratedExposures.size();
                                if (TotalExposures)
                                {
                                    m_pExposureDistribution = new ExposureBin[TotalExposures];
                                    memset(m_pExposureDistribution, 0, sizeof(ExposureBin) * TotalExposures);
                                    return true;
                                }
                                return false;
                            }

                            bool CRadiometricDistribution::CreateExposureDualIndices()
                            {
                                if (m_pExposureDualIndices)
                                {
                                    delete[] m_pExposureDualIndices;
                                    m_pExposureDualIndices = nullptr;
                                }
                                const int TotalExposures = m_CalibratedExposures.size();
                                if (TotalExposures)
                                {
                                    Real MinimalTemporalSegment = g_RealPlusInfinity;
                                    for (int i = 1 ; i < TotalExposures ; ++i)
                                    {
                                        const Real DeltaLogTime = std::abs(std::log(m_CalibratedExposures[i].GetTime()) - std::log(m_CalibratedExposures[i - 1].GetTime()));
                                        if (DeltaLogTime > g_RealPlusEpsilon)
                                        {
                                            MinimalTemporalSegment = std::min(DeltaLogTime, MinimalTemporalSegment);
                                        }
                                    }
                                    const Real MinimalExposureTime = std::log(m_CalibratedExposures.front().GetScopeMinimalExposureTime());
                                    const Real MaximalExposureTime = std::log(m_CalibratedExposures.back().GetScopeMaximalExposureTime());
                                    const Real TemporalRange = MaximalExposureTime - MinimalExposureTime;
                                    const int TotalIndices = int(std::ceil((TemporalRange * Real(3.0)) / MinimalTemporalSegment));
                                    m_pExposureDualIndices = new ExposureDualIndex[TotalIndices + 1];
                                    memset(m_pExposureDualIndices, 0, sizeof(ExposureDualIndex) * (TotalIndices + 1));
                                    ExposureDualIndex* pExposureIndex = m_pExposureDualIndices + TotalIndices;
                                    pExposureIndex->m_IndexB = pExposureIndex->m_IndexA = TotalExposures - 1;
                                    pExposureIndex->m_ExposureTimeThreshold = g_RealPlusInfinity;
                                    pExposureIndex = m_pExposureDualIndices;
                                    const Real TemporalDelta = TemporalRange / Real(TotalIndices);
                                    m_ScaleFactorLogExposureTime = Real(TotalIndices) / TemporalRange;
                                    Real LTA = MinimalExposureTime, LTB = MinimalExposureTime + TemporalDelta;
                                    int SearchIndex = 0;
                                    for (int i = 0 ; i < TotalIndices ; ++i, LTA += TemporalDelta, LTB += TemporalDelta, ++pExposureIndex)
                                    {
                                        const Real TA = Real(std::exp(LTA));
                                        const Real TB = Real(std::exp(LTB));
                                        for (int j = SearchIndex ; j < TotalExposures ; ++j)
                                            if (m_CalibratedExposures[j].HasCommonTemporalScope(TA, TB))
                                            {
                                                Real MinimalDeviationA = m_CalibratedExposures[j].GetAbsoluteTimeDeltaToCentralExposureTime(TA);
                                                Real MinimalDeviationB = m_CalibratedExposures[j].GetAbsoluteTimeDeltaToCentralExposureTime(TB);
                                                int BestMatchIndexA = j, BestMatchIndexB = j;
                                                for (int k = j + 1 ; k < TotalExposures ; ++k)
                                                    if (m_CalibratedExposures[k].HasCommonTemporalScope(TA, TB))
                                                    {
                                                        const Real DeviationA = m_CalibratedExposures[k].GetAbsoluteTimeDeltaToCentralExposureTime(TA);
                                                        if (DeviationA < MinimalDeviationA)
                                                        {
                                                            MinimalDeviationA = DeviationA;
                                                            BestMatchIndexA = k;
                                                        }
                                                        const Real DeviationB = m_CalibratedExposures[k].GetAbsoluteTimeDeltaToCentralExposureTime(TB);
                                                        if (DeviationB < MinimalDeviationB)
                                                        {
                                                            MinimalDeviationB = DeviationB;
                                                            BestMatchIndexB = k;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        break;
                                                    }
                                                pExposureIndex->m_IndexA = BestMatchIndexA;
                                                pExposureIndex->m_IndexB = BestMatchIndexB;
                                                pExposureIndex->m_ExposureTimeThreshold = (BestMatchIndexA != BestMatchIndexB) ? (std::log(m_CalibratedExposures[BestMatchIndexA].GetTime()) + std::log(m_CalibratedExposures[BestMatchIndexB].GetTime())) * Real(0.5) : g_RealPlusInfinity;
                                                SearchIndex = BestMatchIndexA;
                                                break;
                                            }
                                    }
                                    return true;
                                }
                                return false;
                            }

                            std::list<CRadiometricDistribution::ExposureBin*> CRadiometricDistribution::LoadExposureScopeFrequencyDistribution()
                            {
                                std::list<ExposureBin*> ExposureScopeFrequencyDistribution;
                                ExposureBin* pExposureBin = m_pExposureDistribution;
                                const int TotalExposures = m_CalibratedExposures.size();
                                for (int i = 0 ; i < TotalExposures ; ++i, ++pExposureBin)
                                {
                                    pExposureBin->m_ScopeSamplingDensityAccumulator = Real(0);
                                    pExposureBin->m_Active = (pExposureBin->m_SamplingDensityAccumulator && pExposureBin->m_Active);
                                    if (pExposureBin->m_Active)
                                    {
                                        for (int i = pExposureBin->m_ScopeIndexA ; i <= pExposureBin->m_ScopeIndexB ; ++i)
                                            if (m_pExposureDistribution[i].m_Active)
                                            {
                                                pExposureBin->m_ScopeSamplingDensityAccumulator += m_pExposureDistribution[i].m_SamplingDensityAccumulator;
                                            }
                                        ExposureScopeFrequencyDistribution.push_back(pExposureBin);
                                    }
                                }
                                return ExposureScopeFrequencyDistribution;
                            }

                            std::list<CRadiometricDistribution::ExposureBin*> CRadiometricDistribution::DetermineExposureScopeFrequencyDistribution(const std::list<ExposureBin*>& ExposureDistribution)
                            {
                                std::list<ExposureBin*> ExposureScopeFrequencyDistribution;
                                for (auto pExposureBin : ExposureDistribution)
                                {
                                    pExposureBin->m_ScopeSamplingDensityAccumulator = Real(0);
                                    pExposureBin->m_Active = ((pExposureBin->m_SamplingDensityAccumulator > Real(1)) && pExposureBin->m_Active);
                                    if (pExposureBin->m_Active)
                                    {
                                        for (int i = pExposureBin->m_ScopeIndexA ; i <= pExposureBin->m_ScopeIndexB ; ++i)
                                            if (m_pExposureDistribution[i].m_Active)
                                            {
                                                pExposureBin->m_ScopeSamplingDensityAccumulator += m_pExposureDistribution[i].m_SamplingDensityAccumulator;
                                            }
                                        ExposureScopeFrequencyDistribution.push_back(pExposureBin);
                                    }
                                }
                                return ExposureScopeFrequencyDistribution;
                            }

                            CRadiometricDistribution::ExposureBin* CRadiometricDistribution::GetExposureBinMinimalIntegrationTime(const std::list<ExposureBin*>& ExposureScopeFrequencyDistribution)
                            {
                                ExposureBin* pSelectedExposureBin = nullptr;
                                int MinimalIndex = m_CalibratedExposures.size();
                                for (auto pExposureBin : ExposureScopeFrequencyDistribution)
                                {
                                    if (pExposureBin->m_Active && (pExposureBin->m_ScopeSamplingDensityAccumulator > Real(1)) && (pExposureBin->m_Index < MinimalIndex))
                                    {
                                        pSelectedExposureBin = pExposureBin;
                                        MinimalIndex = pExposureBin->m_Index;
                                    }
                                }
                                return pSelectedExposureBin;
                            }

                            CRadiometricDistribution::ExposureBin* CRadiometricDistribution::GetExposureBinByMinimalDensity(const std::list<ExposureBin*>& ExposureScopeFrequencyDistribution)
                            {
                                ExposureBin* pSelectedExposureBin = nullptr;
                                Real MinimalDensity = g_RealPlusInfinity;
                                for (auto pExposureBin : ExposureScopeFrequencyDistribution)
                                {
                                    if (pExposureBin->m_Active && (pExposureBin->m_ScopeSamplingDensityAccumulator > Real(1)) && (pExposureBin->m_MinimalDensity < MinimalDensity))
                                    {
                                        pSelectedExposureBin = pExposureBin;
                                        MinimalDensity = pExposureBin->m_MinimalDensity;
                                    }
                                }
                                return pSelectedExposureBin;
                            }

                            CRadiometricDistribution::ExposureBin* CRadiometricDistribution::GetExposureBinByMaximalScopeFrequency(const std::list<ExposureBin*>& ExposureScopeFrequencyDistribution)
                            {
                                ExposureBin* pSelectedExposureBin = nullptr;
                                int MaximalScopeFrequency = 0;
                                for (auto pExposureBin : ExposureScopeFrequencyDistribution)
                                {
                                    if (pExposureBin->m_Active && (pExposureBin->m_ScopeSamplingDensityAccumulator > Real(1)) && (pExposureBin->m_ScopeSamplingDensityAccumulator > MaximalScopeFrequency))
                                    {
                                        pSelectedExposureBin = pExposureBin;
                                        MaximalScopeFrequency = pExposureBin->m_ScopeSamplingDensityAccumulator;
                                    }
                                }
                                return pSelectedExposureBin;
                            }

                            std::list<Base::CCalibratedExposure> CRadiometricDistribution::SelectExposureIndicesByCriterion(const OptimizationCriterion Criterion)
                            {
                                std::list<Base::CCalibratedExposure> SelectedExposures;
                                std::list<ExposureBin*> ExposureScopeFrequencyDistribution = LoadExposureScopeFrequencyDistribution();
                                if (ExposureScopeFrequencyDistribution.size())
                                {
                                    ExposureBin* pExposureBin = nullptr;
                                    do
                                    {
                                        switch (Criterion)
                                        {
                                            case eUnkownSelectionCriterion:
                                                pExposureBin = GetExposureBinMinimalIntegrationTime(ExposureScopeFrequencyDistribution);
                                                break;
                                            case eEnsureMinimalDensity:
                                                pExposureBin = GetExposureBinByMinimalDensity(ExposureScopeFrequencyDistribution);
                                                break;
                                            case eEnsureMinimalExposures:
                                                pExposureBin = GetExposureBinByMaximalScopeFrequency(ExposureScopeFrequencyDistribution);
                                                break;
                                        }
                                        if (pExposureBin)
                                        {
                                            ExposureBin* pExposureBinA = m_pExposureDistribution + pExposureBin->m_ScopeIndexA;
                                            ExposureBin* pExposureBinB = m_pExposureDistribution + pExposureBin->m_ScopeIndexB;
                                            const Real BandWidth = std::max(pExposureBin->m_LogTime - pExposureBinA->m_LogTime, pExposureBinB->m_LogTime - pExposureBin->m_LogTime);
                                            for (ExposureBin* pExposureBinSliding = pExposureBinA ; pExposureBinSliding <= pExposureBinB ; ++pExposureBinSliding)
                                                if (pExposureBinSliding->m_Active)
                                                {
                                                    const Real NormalizedTimeDelta = (pExposureBinSliding->m_LogTime - pExposureBin->m_LogTime) / BandWidth;
                                                    pExposureBinSliding->m_SamplingDensityAccumulator *= (NormalizedTimeDelta * NormalizedTimeDelta);
                                                }
                                            SelectedExposures.push_back(m_CalibratedExposures[pExposureBin->m_Index]);
                                            ExposureScopeFrequencyDistribution = DetermineExposureScopeFrequencyDistribution(ExposureScopeFrequencyDistribution);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    while (ExposureScopeFrequencyDistribution.size());
                                    SelectedExposures.sort(Base::CExposure::SortExposuresByParametricIndex);
                                }
                                return SelectedExposures;
                            }

                            Real CRadiometricDistribution::GetMaximalIntensity() const
                            {
                                return m_MaximalIntensity;
                            }

                            Real CRadiometricDistribution::GetMinimalIntensity() const
                            {
                                return m_MinimalIntensity;
                            }

                            Real CRadiometricDistribution::GetUpperIntensityAtScopeDensity() const
                            {
                                return m_UpperIntensityAtScopeDensity;
                            }

                            Real CRadiometricDistribution::GetLowerIntensityAtScopeDensity() const
                            {
                                return m_LowerIntensityAtScopeDensity;
                            }

                            Real CRadiometricDistribution::GetScopeKernelDensity() const
                            {
                                return m_ScopeKernelDensity;
                            }

                            Real CRadiometricDistribution::GetConsistentSafeMaximalKernelDensity() const
                            {
                                return m_ConsistentSafeMaximalKernelDensity;
                            }

                            int CRadiometricDistribution::GetTotalUnderExposedPixels() const
                            {
                                return m_TotalUnderExposedPixels;
                            }

                            int CRadiometricDistribution::GetTotalOverExposedPixels() const
                            {
                                return m_TotalOverExposedPixels;
                            }

                            const std::list<Base::CExposure>& CRadiometricDistribution::GetExposures() const
                            {
                                return m_Exposures;
                            }

                            const std::vector<Base::CCalibratedExposure>& CRadiometricDistribution::GetCalibratedExposures() const
                            {
                                return m_CalibratedExposures;
                            }

                            const std::map<std::uint64_t, const CRadiometricBayerPatternSynthesizer*>& CRadiometricDistribution::GetSynthesizers() const
                            {
                                return m_Synthesizers;
                            }

                            bool CRadiometricDistribution::ConfigureExposureScopeByDensity(const Real ScopeKernelDensity)
                            {
                                m_ScopeKernelDensity = std::min(std::max(ScopeKernelDensity, m_ConsistentSafeMaximalKernelDensity), Real(0.999));
                                const int TotalExposures = m_CalibratedExposures.size();
                                const Real MinimalLogExposureTime = std::log(m_CalibratedExposures.front().GetScopeMinimalExposureTime());
                                const Real MaximalLogExposureTime = std::log(m_CalibratedExposures.back().GetScopeMaximalExposureTime());
                                m_UpperIntensityAtScopeDensity = std::min(std::ceil(m_RadianceWeigthingKernel.GetUpperIntensityAtDensity(m_Kernel, m_ScopeKernelDensity)) + Real(1), m_MaximalIntensity);
                                m_LowerIntensityAtScopeDensity = std::max(std::floor(m_RadianceWeigthingKernel.GetLowerIntensityAtDensity(m_Kernel, m_ScopeKernelDensity)) - Real(1), m_MinimalIntensity);
                                memset(m_pExposureDistribution, 0, sizeof(ExposureBin) * TotalExposures);
                                ExposureBin* pExposureBin = m_pExposureDistribution;
                                for (int i = 0 ; i < TotalExposures ; ++i, ++pExposureBin)
                                {
                                    pExposureBin->m_Active = true;
                                    pExposureBin->m_Index = i;
                                    pExposureBin->m_MinimalDensity = g_RealPlusInfinity;
                                    const Real Time = m_CalibratedExposures[i].GetTime();
                                    Real CommonT0 = g_RealMinusInfinity, CommonT1 = g_RealPlusInfinity;
                                    for (std::list<const CRadiometricResponceFunction*>::const_iterator ppRadiometricResponceFunction = m_RadiometricResponceFunctions.begin() ; ppRadiometricResponceFunction != m_RadiometricResponceFunctions.end() ; ++ppRadiometricResponceFunction)
                                    {
                                        const CRadiometricResponceFunction* pResponceFunction = *ppRadiometricResponceFunction;
                                        const Real T0 = pResponceFunction->GetIdealExposureTime(m_LowerIntensityAtScopeDensity, pResponceFunction->GetIdealRadiance(m_UpperIntensityAtScopeDensity, Time));
                                        const Real T1 = pResponceFunction->GetIdealExposureTime(m_UpperIntensityAtScopeDensity, pResponceFunction->GetIdealRadiance(m_LowerIntensityAtScopeDensity, Time));
                                        if (T0 > CommonT0)
                                        {
                                            CommonT0 = T0;
                                        }
                                        if (T1 < CommonT1)
                                        {
                                            CommonT1 = T1;
                                        }
                                    }
                                    if (CommonT0 >= CommonT1)
                                    {
                                        memset(m_pExposureDistribution, 0, sizeof(ExposureBin) * TotalExposures);
                                        return false;
                                    }
                                    const ExposureDualIndex* pExposureDualIndex0 = m_pExposureDualIndices + int(std::round((std::min(std::max(std::log(CommonT0), MinimalLogExposureTime), MaximalLogExposureTime) - MinimalLogExposureTime) * m_ScaleFactorLogExposureTime));
                                    const ExposureDualIndex* pExposureDualIndex1 = m_pExposureDualIndices + int(std::round((std::max(std::min(std::log(CommonT1), MaximalLogExposureTime), MinimalLogExposureTime) - MinimalLogExposureTime) * m_ScaleFactorLogExposureTime));
                                    pExposureBin->m_LogTime = std::log(Time);
                                    pExposureBin->m_ScopeIndexA = (CommonT0 > pExposureDualIndex0->m_ExposureTimeThreshold) ? pExposureDualIndex0->m_IndexB : pExposureDualIndex0->m_IndexA;
                                    pExposureBin->m_ScopeIndexB = (CommonT1 > pExposureDualIndex1->m_ExposureTimeThreshold) ? pExposureDualIndex1->m_IndexB : pExposureDualIndex1->m_IndexA;
                                }
                                return true;
                            }

                            bool CRadiometricDistribution::MapExposureFrequencyDistribution(const CMultipleChannelRadiometricResponceFunction* pMultipleChannelRadiometricResponceFunction, const Imaging::TImage<Real>* pLogRadianceImage, const Imaging::TImage<Real>* pDensityWeightedImage, const Imaging::CBayerPattern::BayerPatternType BayerPattern, const Real CriticallKernelDensity)
                            {
                                if (pMultipleChannelRadiometricResponceFunction && pLogRadianceImage && pDensityWeightedImage && pMultipleChannelRadiometricResponceFunction->AreIdealResponceFunctionsReady() && pLogRadianceImage->IsValid() && pDensityWeightedImage->IsValid())
                                {
                                    Imaging::CBayerPattern BayerPatternMapLoader;
                                    if (BayerPatternMapLoader.SetType(BayerPattern))
                                    {
                                        Imaging::CBayerPattern::ChannelContentMap BayerPatternMap = BayerPatternMapLoader.GetBayerPatternMap();
                                        Real OptimalResponces[3] = { Real(0) };
                                        OptimalResponces[Imaging::CBayerPattern::eRed] = std::log(pMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction()->GetIdealResponce(Real(127.5)));
                                        OptimalResponces[Imaging::CBayerPattern::eGreen] = std::log(pMultipleChannelRadiometricResponceFunction->GetGreenChannelResponceFunction()->GetIdealResponce(Real(127.5)));
                                        OptimalResponces[Imaging::CBayerPattern::eBlue] = std::log(pMultipleChannelRadiometricResponceFunction->GetBlueChannelResponceFunction()->GetIdealResponce(Real(127.5)));
                                        const Real MinimalLogExposureTime = std::log(m_CalibratedExposures.front().GetScopeMinimalExposureTime());
                                        const Real MaximalLogExposureTime = std::log(m_CalibratedExposures.back().GetScopeMaximalExposureTime());
                                        const int Width = pLogRadianceImage->GetWidth();
                                        const int Height = pLogRadianceImage->GetHeight();
                                        const Real* pLogRadiancePixel = pLogRadianceImage->GetReadOnlyBuffer();
                                        const Real* pDensityWeightedPixel = pDensityWeightedImage->GetReadOnlyBuffer();
                                        for (int Y = 0 ; Y < Height ; ++Y)
                                        {
                                            const Real SelectedOptimalResponce[2] = { OptimalResponces[BayerPatternMap.m_YX[Y & 0X1][0]], OptimalResponces[BayerPatternMap.m_YX[Y & 0X1][1]] };
                                            for (int X = 0 ; X < Width ; ++X)
                                            {
                                                const Real DensityWeight = *pDensityWeightedPixel++;
                                                const Real LogRadiance = *pLogRadiancePixel++;
                                                if (DensityWeight < CriticallKernelDensity)
                                                {
                                                    Real OptimalLogExposureTime = SelectedOptimalResponce[X & 0X1] - LogRadiance;
                                                    if (OptimalLogExposureTime > MaximalLogExposureTime)
                                                    {
                                                        OptimalLogExposureTime = MaximalLogExposureTime;
                                                        ++m_TotalUnderExposedPixels;
                                                    }
                                                    else if (OptimalLogExposureTime < MinimalLogExposureTime)
                                                    {
                                                        OptimalLogExposureTime = MinimalLogExposureTime;
                                                        ++m_TotalOverExposedPixels;
                                                    }
                                                    const ExposureDualIndex* pOptimalExposureIndex = m_pExposureDualIndices + int(std::round((OptimalLogExposureTime - MinimalLogExposureTime) * m_ScaleFactorLogExposureTime));
                                                    ExposureBin* pOptimalExposureBin = m_pExposureDistribution + (OptimalLogExposureTime > pOptimalExposureIndex->m_ExposureTimeThreshold ? pOptimalExposureIndex->m_IndexB : pOptimalExposureIndex->m_IndexA);
                                                    pOptimalExposureBin->m_SamplingDensityAccumulator += Real(1);
                                                    if (DensityWeight < pOptimalExposureBin->m_MinimalDensity)
                                                    {
                                                        pOptimalExposureBin->m_MinimalDensity = DensityWeight;
                                                    }
                                                }
                                            }
                                        }
                                        return true;
                                    }
                                }
                                return false;
                            }
                        }
                    }
                }
            }
        }
    }
}
