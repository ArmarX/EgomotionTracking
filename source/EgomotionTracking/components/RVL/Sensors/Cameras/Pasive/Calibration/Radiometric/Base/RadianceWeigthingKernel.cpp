/*
 * RadianceWeigthingKernel.cpp
 */

#include "RadianceWeigthingKernel.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Base
                        {
                            std::string CRadianceWeigthingKernel::RegressionWeigthingKernelToString(const KernelType Kernel)
                            {
                                switch (Kernel)
                                {
                                    case eUnknownKernelType:
                                        return std::string("Unknown");
                                    case eUniform:
                                        return std::string("Uniform");
                                    case eTriangular:
                                        return std::string("Triangular");
                                    case eEpanechnikov:
                                        return std::string("Epanechnikov");
                                    case eBiweight:
                                        return std::string("Biweight");
                                    case eTriweight:
                                        return std::string("Triweight");
                                    case eTricube:
                                        return std::string("Tricube");
                                    case eCosine:
                                        return std::string("Cosine");
                                    case eGaussian:
                                        return std::string("Gaussian");
                                    case eGaussianBox:
                                        return std::string("Gaussian-Box");
                                }
                                return std::string("Error");
                            }

                            CRadianceWeigthingKernel::CRadianceWeigthingKernel() :
                                m_GaussianBoxKernelRadius(_RADIANCE_WEIGTHING_KERNEL_MINIMAL_DEFAULT_GAUSSIAN_BOX_RADIUS_),
                                m_CommonGaussianKernelExponentFactor(Mathematics::_1D::CNormalDistribution::DetermineExponentFactor(Mathematics::_1D::CNormalDistribution::DetermineNonNormalizedStandardDeviation(_RADIANCE_WEIGTHING_KERNEL_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_DENSITY_, _RADIANCE_WEIGTHING_KERNEL_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_NORMALIZED_DEVIATION_)))
                            {
                            }

                            CRadianceWeigthingKernel::CRadianceWeigthingKernel(const CRadianceWeigthingKernel& RadianceWeigthingKernel)

                                = default;

                            CRadianceWeigthingKernel::~CRadianceWeigthingKernel()
                                = default;

                            bool CRadianceWeigthingKernel::SetGaussianBoxKernelRadius(const Real GaussianBoxKernelRadius)
                            {
                                if ((GaussianBoxKernelRadius >= _RADIANCE_WEIGTHING_KERNEL_MINIMAL_GAUSSIAN_BOX_RADIUS_) && (GaussianBoxKernelRadius <= _RADIANCE_WEIGTHING_KERNEL_MAXIMAL_GAUSSIAN_BOX_RADIUS_))
                                {
                                    m_GaussianBoxKernelRadius = GaussianBoxKernelRadius;
                                    return true;
                                }
                                return false;
                            }

                            bool CRadianceWeigthingKernel::SetCommonGaussianKernelStandardDeviation(const Real StandardDeviation)
                            {
                                if (IsPositive(StandardDeviation) && (StandardDeviation <= Real(4)))
                                {
                                    m_CommonGaussianKernelExponentFactor = Mathematics::_1D::CNormalDistribution::DetermineExponentFactor(StandardDeviation);
                                    return true;
                                }
                                return false;
                            }

                            bool CRadianceWeigthingKernel::SetCommonGaussianKernelCutOff(const Real CutOffDensity, const Real CutOffNormalizedDeviation)
                            {
                                if (IsPositive(CutOffDensity) && (CutOffDensity < Real(1)) && IsPositive(CutOffNormalizedDeviation) && (CutOffNormalizedDeviation < Real(1)))
                                {
                                    m_CommonGaussianKernelExponentFactor = Mathematics::_1D::CNormalDistribution::DetermineExponentFactor(Mathematics::_1D::CNormalDistribution::DetermineNonNormalizedStandardDeviation(CutOffDensity, CutOffNormalizedDeviation));
                                    return true;
                                }
                                return false;
                            }

                            Real CRadianceWeigthingKernel::GetGaussianBoxKernelRadius() const
                            {
                                return m_GaussianBoxKernelRadius;
                            }

                            Real CRadianceWeigthingKernel::GetCommonGaussianKernelExponentFactor() const
                            {
                                return m_CommonGaussianKernelExponentFactor;
                            }

                            Real CRadianceWeigthingKernel::GetCommonGaussianKernelStandardDeviation() const
                            {
                                return std::sqrt(Real(1) / (-Real(2) * m_CommonGaussianKernelExponentFactor));
                            }

                            Real CRadianceWeigthingKernel::GetRegressionWeigthingByUniformKernel(const Real /*Intensity*/) const
                            {
                                return Real(1);
                            }

                            Real CRadianceWeigthingKernel::GetRegressionWeigthingByTriangularKernel(const Real Intensity) const
                            {
                                return Real(1) - std::abs((Intensity - Real(127.5)) * Real(0.007843137254901960784314));
                            }

                            Real CRadianceWeigthingKernel::GetRegressionWeigthingByEpanechnikovKernel(const Real Intensity) const
                            {
                                const Real NormalizedDeviation = (Intensity - Real(127.5)) * Real(0.007843137254901960784314);
                                return Real(1) - (NormalizedDeviation * NormalizedDeviation);
                            }

                            Real CRadianceWeigthingKernel::GetRegressionWeigthingByBiweightKernel(const Real Intensity) const
                            {
                                const Real NormalizedDeviation = (Intensity - Real(127.5)) * Real(0.007843137254901960784314);
                                const Real Inner = Real(1) - (NormalizedDeviation * NormalizedDeviation);
                                return Inner * Inner;
                            }

                            Real CRadianceWeigthingKernel::GetRegressionWeigthingByTriweightKernel(const Real Intensity) const
                            {
                                const Real NormalizedDeviation = (Intensity - Real(127.5)) * Real(0.007843137254901960784314);
                                const Real Inner = Real(1) - (NormalizedDeviation * NormalizedDeviation);
                                return Inner * Inner * Inner;
                            }

                            Real CRadianceWeigthingKernel::GetRegressionWeigthingByTricubeKernel(const Real Intensity) const
                            {
                                const Real AbsoluteNormalizedDeviation = std::abs((Intensity - Real(127.5)) * Real(0.007843137254901960784314));
                                const Real Inner = Real(1) - (AbsoluteNormalizedDeviation * AbsoluteNormalizedDeviation * AbsoluteNormalizedDeviation);
                                return Inner * Inner * Inner;
                            }

                            Real CRadianceWeigthingKernel::GetRegressionWeigthingByCosineKernel(const Real Intensity) const
                            {
                                return std::cos((Intensity - Real(127.5)) * Real(0.003921568627450980392157));
                            }

                            Real CRadianceWeigthingKernel::GetRegressionWeigthingByGaussianKernel(const Real Intensity) const
                            {
                                const Real NormalizedDeviation = (Intensity - Real(127.5)) * Real(0.007843137254901960784314);
                                return std::exp(NormalizedDeviation * NormalizedDeviation * m_CommonGaussianKernelExponentFactor);
                            }

                            Real CRadianceWeigthingKernel::GetRegressionWeigthingByGaussianBoxKernel(const Real Intensity) const
                            {
                                if ((Intensity >= m_GaussianBoxKernelRadius) && (Intensity <= (Real(255) - m_GaussianBoxKernelRadius)))
                                {
                                    return Real(1);
                                }
                                const Real GaussianBoxRadiusNormalizedDeviation = (Intensity < m_GaussianBoxKernelRadius) ? ((m_GaussianBoxKernelRadius - Intensity) / m_GaussianBoxKernelRadius) : (Real(1) - ((Real(255) - Intensity) / m_GaussianBoxKernelRadius));
                                return std::exp(GaussianBoxRadiusNormalizedDeviation * GaussianBoxRadiusNormalizedDeviation * m_CommonGaussianKernelExponentFactor);
                            }

                            Real CRadianceWeigthingKernel::GetRegressionWeigthingByKernel(const Real Intensity, const KernelType Kernel) const
                            {
                                switch (Kernel)
                                {
                                    case eGaussian:
                                    {
                                        const Real NormalizedDeviation = (Intensity - Real(127.5)) * Real(0.007843137254901960784314);
                                        return std::exp(NormalizedDeviation * NormalizedDeviation * m_CommonGaussianKernelExponentFactor);
                                    }
                                    case eEpanechnikov:
                                    {
                                        const Real NormalizedDeviation = (Intensity - Real(127.5)) * Real(0.007843137254901960784314);
                                        return Real(1) - (NormalizedDeviation * NormalizedDeviation);
                                    }
                                    case eGaussianBox:
                                    {
                                        if ((Intensity >= m_GaussianBoxKernelRadius) && (Intensity <= (Real(255) - m_GaussianBoxKernelRadius)))
                                        {
                                            return Real(1);
                                        }
                                        const Real GaussianBoxRadiusNormalizedDeviation = (Intensity < m_GaussianBoxKernelRadius) ? ((m_GaussianBoxKernelRadius - Intensity) / m_GaussianBoxKernelRadius) : (Real(1) - ((Real(255) - Intensity) / m_GaussianBoxKernelRadius));
                                        return std::exp(GaussianBoxRadiusNormalizedDeviation * GaussianBoxRadiusNormalizedDeviation * m_CommonGaussianKernelExponentFactor);
                                    }
                                    case eTriangular:
                                        return Real(1) - std::abs((Intensity - Real(127.5)) * Real(0.007843137254901960784314));
                                    case eBiweight:
                                    {
                                        const Real NormalizedDeviation = (Intensity - Real(127.5)) * Real(0.007843137254901960784314);
                                        const Real Inner = Real(1) - (NormalizedDeviation * NormalizedDeviation);
                                        return Inner * Inner;
                                    }

                                    case eTriweight:
                                    {
                                        const Real NormalizedDeviation = (Intensity - Real(127.5)) * Real(0.007843137254901960784314);
                                        const Real Inner = Real(1) - (NormalizedDeviation * NormalizedDeviation);
                                        return Inner * Inner * Inner;
                                    }
                                    case eTricube:
                                    {
                                        const Real AbsoluteNormalizedDeviation = std::abs((Intensity - Real(127.5)) * Real(0.007843137254901960784314));
                                        const Real Inner = Real(1) - (AbsoluteNormalizedDeviation * AbsoluteNormalizedDeviation * AbsoluteNormalizedDeviation);
                                        return Inner * Inner * Inner;
                                    }
                                    case eCosine:
                                    {
                                        return std::cos((Intensity - Real(127.5)) * Real(0.007843137254901960784314) * g_RealHalfPI);
                                    }
                                    case eUniform:
                                        return Real(1);
                                    case eUnknownKernelType:
                                        return Real(0);
                                }
                                return Real(0);
                            }

                            Real CRadianceWeigthingKernel::GetUpperIntensityAtDensity(const KernelType Kernel, const Real MinimalKernelDensity) const
                            {
                                if ((MinimalKernelDensity >= Real(0)) && (MinimalKernelDensity <= Real(1)))
                                    switch (Kernel)
                                    {
                                        case eUnknownKernelType:
                                            return Real(0);
                                        case eUniform:
                                            return Real(255);
                                        case eTriangular:
                                            return Real(127.5) + (Real(1) - MinimalKernelDensity) * Real(127.5);
                                        case eEpanechnikov:
                                            return Real(127.5) + std::sqrt((Real(1) - MinimalKernelDensity)) * Real(127.5);
                                        case eBiweight:
                                            return Real(127.5) + std::sqrt(Real(1) - std::sqrt(MinimalKernelDensity)) * Real(127.5);
                                        case eTriweight:
                                            return Real(127.5) + std::sqrt(Real(1) - std::pow(MinimalKernelDensity, (Real(1) / Real(3)))) * Real(127.5);
                                        case eTricube:
                                            return Real(127.5) + std::pow(Real(1) - std::pow(MinimalKernelDensity, (Real(1) / Real(3))), (Real(1) / Real(3))) * Real(127.5);
                                        case eCosine:
                                            return Real(127.5) + (std::sin(MinimalKernelDensity) / g_RealHalfPI) * Real(127.5);
                                        case eGaussian:
                                            return Real(127.5) + std::sqrt(std::log(MinimalKernelDensity) / m_CommonGaussianKernelExponentFactor) * Real(127.5);
                                        case eGaussianBox:
                                            return (MinimalKernelDensity == Real(1)) ? (Real(255) - m_GaussianBoxKernelRadius) : Real(255) + (std::sqrt(std::log(MinimalKernelDensity) / m_CommonGaussianKernelExponentFactor) - Real(1)) * m_GaussianBoxKernelRadius;
                                    }
                                return Real(0);
                            }

                            Real CRadianceWeigthingKernel::GetLowerIntensityAtDensity(const KernelType Kernel, const Real MinimalKernelDensity) const
                            {
                                if ((MinimalKernelDensity >= Real(0)) && (MinimalKernelDensity <= Real(1)))
                                    switch (Kernel)
                                    {
                                        case eUnknownKernelType:
                                            return Real(0);
                                        case eUniform:
                                            return Real(255);
                                        case eTriangular:
                                            return Real(127.5) - (Real(1) - MinimalKernelDensity) * Real(127.5);
                                        case eEpanechnikov:
                                            return Real(127.5) - std::sqrt((Real(1) - MinimalKernelDensity)) * Real(127.5);
                                        case eBiweight:
                                            return Real(127.5) - std::sqrt(Real(1) - std::sqrt(MinimalKernelDensity)) * Real(127.5);
                                        case eTriweight:
                                            return Real(127.5) - std::sqrt(Real(1) - std::pow(MinimalKernelDensity, (Real(1) / Real(3)))) * Real(127.5);
                                        case eTricube:
                                            return Real(127.5) - std::pow(Real(1) - std::pow(MinimalKernelDensity, (Real(1) / Real(3))), (Real(1) / Real(3))) * Real(127.5);
                                        case eCosine:
                                            return Real(127.5) - (std::cos(MinimalKernelDensity) / g_RealHalfPI) * Real(127.5);
                                        case eGaussian:
                                            return Real(127.5) - std::sqrt(std::log(MinimalKernelDensity) / m_CommonGaussianKernelExponentFactor) * Real(127.5);
                                        case eGaussianBox:
                                            return (MinimalKernelDensity == Real(1)) ? m_GaussianBoxKernelRadius : (Real(1) - std::sqrt(std::log(MinimalKernelDensity) / m_CommonGaussianKernelExponentFactor)) * m_GaussianBoxKernelRadius;
                                    }
                                return Real(0);
                            }

                            void CRadianceWeigthingKernel::operator=(const CRadianceWeigthingKernel& RadianceWeigthingKernel)
                            {
                                m_GaussianBoxKernelRadius = RadianceWeigthingKernel.m_GaussianBoxKernelRadius;
                                m_CommonGaussianKernelExponentFactor = RadianceWeigthingKernel.m_CommonGaussianKernelExponentFactor;
                            }

                            bool CRadianceWeigthingKernel::LoadFromFile(Files::CInFile& InputBinaryFile)
                            {
                                if (InputBinaryFile.IsReady())
                                    if (InputBinaryFile.Read(m_GaussianBoxKernelRadius))
                                    {
                                        return InputBinaryFile.Read(m_CommonGaussianKernelExponentFactor);
                                    }
                                return false;
                            }

                            bool CRadianceWeigthingKernel::SaveToFile(Files::COutFile& OutputBinaryFile) const
                            {
                                if (OutputBinaryFile.IsReady())
                                    if (OutputBinaryFile.Write(m_GaussianBoxKernelRadius))
                                    {
                                        return OutputBinaryFile.Write(m_CommonGaussianKernelExponentFactor);
                                    }
                                return false;
                            }
                        }
                    }
                }
            }
        }
    }
}

