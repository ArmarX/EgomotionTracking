/*
 * ReferencedRadiometricPixel.cpp
 */

#include "ReferencedRadiometricPixel.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Local
                        {
                            CReferencedRadiometricPixel::CReferencedRadiometricPixel() :
                                m_X(g_IntegerPlusInfinity),
                                m_Y(g_IntegerPlusInfinity),
                                m_pRadiance(nullptr),
                                m_pRadianceReference(nullptr),
                                m_Slope(Real(0)),
                                m_Offset(Real(0)),
                                m_RMSDeviation(Real(0)),
                                m_PeakNegativeDeviation(Real(0)),
                                m_PeakPositiveDeviation(Real(0))
                            {
                            }

                            CReferencedRadiometricPixel::~CReferencedRadiometricPixel()
                                = default;

                            CReferencedRadiometricPixel* CReferencedRadiometricPixel::Initialize(const int X, const int Y, const Real* pRadiance, const Real* pRadianceReference)
                            {
                                m_pRadiance = pRadiance;
                                m_pRadianceReference = pRadianceReference;
                                m_X = X;
                                m_Y = Y;
                                return this;
                            }

                            void CReferencedRadiometricPixel::AddExposure(const Real ExposureTime)
                            {
                                if ((*m_pRadiance > Real(2.0)) && (*m_pRadiance < Real(253.0)))
                                {
                                    RadianceReferencedSample Sample = { *m_pRadiance, *m_pRadianceReference, ExposureTime };
                                    m_RadianceReferencedSamples.push_back(Sample);
                                }
                            }

                            void CReferencedRadiometricPixel::ClearExposures()
                            {
                                m_RadianceReferencedSamples.clear();
                            }

                            bool CReferencedRadiometricPixel::Calibrate(const Base::CRadianceWeigthingKernel& RadianceWeigthingKernel, const Base::CRadianceWeigthingKernel::KernelType Kernel)
                            {
                                const int TotalSamples = m_RadianceReferencedSamples.size();
                                if (TotalSamples >= 4)
                                {
                                    std::list<RadianceReferencedSample>::const_iterator pRadianceReferencedSample = m_RadianceReferencedSamples.begin();
                                    Mathematics::_ND::CMatrixND A(TotalSamples, 2);
                                    Mathematics::_ND::CMatrixND B(TotalSamples, 1);
                                    Mathematics::_ND::CMatrixND W(TotalSamples, TotalSamples);
                                    for (int Row = 0 ; Row < TotalSamples ; ++Row, ++pRadianceReferencedSample)
                                    {
                                        const Real X = pRadianceReferencedSample->m_RadianceReference * pRadianceReferencedSample->m_ExposureTime;
                                        const Real I = pRadianceReferencedSample->m_Radiance;
                                        A(Row, 0) = X;
                                        A(Row, 1) = Real(1);
                                        B(Row, 0) = I;
                                        W(Row, Row) = RadianceWeigthingKernel.GetRegressionWeigthingByKernel(pRadianceReferencedSample->m_Radiance, Kernel);
                                    }
                                    //try
                                    //{
                                    Mathematics::_ND::CMatrixND Tmp = (A.transpose() * W * A);
                                    Mathematics::_ND::CMatrixND X = Tmp.inverse() * (A.transpose() * W * B);
                                    m_Slope = X(0, 0);
                                    m_Offset = X(1, 0);
                                    m_RMSDeviation = Real(0);
                                    m_PeakNegativeDeviation = Real(0);
                                    m_PeakPositiveDeviation = Real(0);
                                    pRadianceReferencedSample = m_RadianceReferencedSamples.begin();
                                    for (int i = 0 ; i < TotalSamples ; ++i, ++pRadianceReferencedSample)
                                    {
                                        const Real Energy = pRadianceReferencedSample->m_RadianceReference * pRadianceReferencedSample->m_ExposureTime;
                                        const Real ModelRadiance = (m_Slope * Energy + m_Offset);
                                        const Real Deviation = ModelRadiance - pRadianceReferencedSample->m_Radiance;
                                        //g_ConsoleStringOutput << Energy << "\t" << pRadianceReferencedSample->m_Radiance << "\t" << ModelRadiance << "\t" << RealAbs(Deviation) << g_EndLine;
                                        if (Deviation < m_PeakNegativeDeviation)
                                        {
                                            m_PeakNegativeDeviation = Deviation;
                                        }
                                        if (Deviation > m_PeakPositiveDeviation)
                                        {
                                            m_PeakPositiveDeviation = Deviation;
                                        }
                                        m_RMSDeviation += Deviation * Deviation;
                                    }
                                    m_RMSDeviation = std::sqrt(m_RMSDeviation / Real(TotalSamples));
                                    return true;
                                    //}
                                    //catch(Mathematics::_ND::CMatrixBaseException& E)
                                    //{
                                    //  std::ostringstream OutputText;
                                    //  OutputText << E.what() << "\n";
                                    //  _RVL_CONSOLE_OUTPUT_EXECEPTIONS_(OutputText.str(),0);
                                    //}
                                }
                                m_Slope = Real(0);
                                m_Offset = Real(0);
                                m_RMSDeviation = Real(0);
                                return false;
                            }

                            int CReferencedRadiometricPixel::GetX() const
                            {
                                return m_X;
                            }

                            int CReferencedRadiometricPixel::GetY() const
                            {
                                return m_Y;
                            }

                            Real CReferencedRadiometricPixel::GetSlope() const
                            {
                                return m_Slope;
                            }

                            Real CReferencedRadiometricPixel::GetOffset() const
                            {
                                return m_Offset;
                            }

                            Real CReferencedRadiometricPixel::GetRMSDeviation() const
                            {
                                return m_RMSDeviation;
                            }

                            Real CReferencedRadiometricPixel::GetRangeDeviation() const
                            {
                                return m_PeakPositiveDeviation - m_PeakNegativeDeviation;
                            }

                            Real CReferencedRadiometricPixel::GetPeakNegativeDeviation() const
                            {
                                return m_PeakNegativeDeviation;
                            }

                            Real CReferencedRadiometricPixel::GetPeakPositiveDeviation() const
                            {
                                return m_PeakPositiveDeviation;
                            }
                        }
                    }
                }
            }
        }
    }
}
