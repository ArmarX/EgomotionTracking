/*
 * ReferencedRadiometricPixel.h
 */

#pragma once

#include "../../../../../../Console/ConsoleOutputManager.h"
#include "../../../../../../Common/DataTypes.h"
#include "../../../../../../Mathematics/_ND/MatrixND.h"
#include "../../../../../../Mathematics/_1D/NormalDistribution.h"
#include "../Base/Exposure.h"
#include "../Base/RadianceWeigthingKernel.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Local
                        {
                            class CReferencedRadiometricPixel
                            {
                            public:

                                CReferencedRadiometricPixel();
                                ~CReferencedRadiometricPixel();

                                CReferencedRadiometricPixel* Initialize(const int X, const int Y, const Real* pRadiance, const Real* pRadianceReference);

                                void AddExposure(const Real ExposureTime);
                                void ClearExposures();
                                bool Calibrate(const Base::CRadianceWeigthingKernel& RadianceWeigthingKernel, const Base::CRadianceWeigthingKernel::KernelType Kernel);

                                int GetX() const;
                                int GetY() const;
                                Real GetSlope() const;
                                Real GetOffset() const;
                                Real GetRMSDeviation() const;
                                Real GetRangeDeviation() const;
                                Real GetPeakNegativeDeviation() const;
                                Real GetPeakPositiveDeviation() const;

                            protected:

                                struct RadianceReferencedSample
                                {
                                    Real m_Radiance;
                                    Real m_RadianceReference;
                                    Real m_ExposureTime;
                                };

                                int m_X;
                                int m_Y;
                                const Real* m_pRadiance;
                                const Real* m_pRadianceReference;
                                std::list<RadianceReferencedSample> m_RadianceReferencedSamples;
                                Real m_Slope;
                                Real m_Offset;
                                Real m_RMSDeviation;
                                Real m_PeakNegativeDeviation;
                                Real m_PeakPositiveDeviation;
                            };
                        }
                    }
                }
            }
        }
    }
}
