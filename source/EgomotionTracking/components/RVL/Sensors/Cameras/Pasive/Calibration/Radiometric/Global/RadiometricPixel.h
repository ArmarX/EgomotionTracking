/*
 * RadiometricPixel.h
 */

#pragma once

#include "../../../../../../Common/DataTypes.h"
#include "../../../../../../Mathematics/_1D/NormalDistribution.h"
#include "../Base/Exposure.h"

namespace RVL
{
    namespace Sensors
    {
        namespace Cameras
        {
            namespace Pasive
            {
                namespace Calibration
                {
                    namespace Radiometric
                    {
                        namespace Global
                        {
                            class CRadiometricPixel
                            {
                            public:

                                CRadiometricPixel();
                                ~CRadiometricPixel();

                                CRadiometricPixel* Initialize(const int X, const int Y, const Real* pValueSource);

                                void CalculateStandardDeviationCriterion();
                                void CalculateRangeCriterion();

                                bool IsInitialized() const;
                                bool IsActive() const;
                                void SetActive(const bool Active);

                                void AddExposure();
                                void ClearExposures();

                                Real GetCriterion() const;
                                int GetX() const;
                                int GetY() const;
                                const std::list<Real>& GetValues() const;

                            protected:

                                struct RadiometricPixel
                                {
                                    bool m_IsActive;
                                    int m_X;
                                    int m_Y;
                                    Real m_Criterion;
                                    const Real* m_pSource;
                                    std::list<Real> m_Values;
                                };

                                RadiometricPixel* m_pRadiometricPixel;
                            };
                        }
                    }
                }
            }
        }
    }
}

