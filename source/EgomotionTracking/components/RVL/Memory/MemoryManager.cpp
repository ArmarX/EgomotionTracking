/*
 * MemoryManager.cpp
 */

#include "MemoryManager.h"

namespace RVL
{
    namespace Memory
    {
        CMemoryManager::CMemoryManager()
            = default;

        CMemoryManager::~CMemoryManager()
            = default;

        unsigned long int CMemoryManager::GetTotalMemory()
        {

#ifdef _RVL_USE_WIN32_

            MEMORYSTATUSEX status;
            status.dwLength = sizeof(status);
            GlobalMemoryStatusEx(&status);
            return status.ullTotalPhys;

#endif

#ifdef _RVL_USE_WIN64_

#endif

#ifdef _RVL_USE_LINUX32_

            return sysconf(_SC_PHYS_PAGES) * sysconf(_SC_PAGE_SIZE);

#endif

#ifdef _RVL_USE_LINUX64_

            return sysconf(_SC_PHYS_PAGES) * sysconf(_SC_PAGE_SIZE);

#endif

#ifdef _RVL_USE_MACOS_

#endif

        }
    }
}
