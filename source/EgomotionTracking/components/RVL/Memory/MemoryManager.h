/*
 * MemoryManager.h
 */

#pragma once

#include "../Common/Includes.h"
#include "../Common/Switches.h"

namespace RVL
{
    namespace Memory
    {
        class CMemoryManager
        {
        public:

            static unsigned long int GetTotalMemory();

        private:

            CMemoryManager();
            virtual ~CMemoryManager();
        };
    }
}

