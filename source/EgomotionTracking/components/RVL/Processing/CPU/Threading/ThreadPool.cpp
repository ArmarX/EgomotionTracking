/*
 * ThreadPool.cpp
 */

#include "ThreadPool.h"
#include "ExecutionUnit.h"

namespace RVL
{
    namespace Processing
    {
        namespace CPU
        {
            namespace Threading
            {
                void CThreadPool::QueryCPURegisters(unsigned Index, unsigned Registers[4])
                {

#ifdef _RVL_USE_WIN32_
                    __cpuid((int*) Registers, (int) Index);
#endif

#ifdef _RVL_USE_LINUX32_
                    asm volatile("cpuid" : "=a"(Registers[0]), "=b"(Registers[1]), "=c"(Registers[2]), "=d"(Registers[3]): "a"(Index), "c"(0));
#endif
                }

                const std::string CThreadPool::GetCPUBBrandName()
                {
                    unsigned int Registers[4] = { 0 };
                    QueryCPURegisters(0, Registers);
                    char CPUBBrandName[16] = { 0 };
                    memcpy(&CPUBBrandName[0], &Registers[1], sizeof(unsigned int));
                    memcpy(&CPUBBrandName[4], &Registers[3], sizeof(unsigned int));
                    memcpy(&CPUBBrandName[8], &Registers[2], sizeof(unsigned int));
                    return std::string(CPUBBrandName);
                }

                int CThreadPool::GetAvailableLogicalCPUs()
                {
                    unsigned int Registers[4] = { 0 };
                    QueryCPURegisters(1, Registers);
                    return (Registers[1] >> 16) & 0xFF;
                }

                int CThreadPool::GetAvailableLogicalCPUCores()
                {
                    unsigned int Registers[4] = { 0 };
                    const std::string CPUBBrand = CThreadPool::GetCPUBBrandName();
                    int AvailableLogicalCPUCores = 0;
                    if (CPUBBrand == "GenuineIntel")
                    {
                        QueryCPURegisters(4, Registers);
                        AvailableLogicalCPUCores = ((Registers[0] >> 26) & 0x3F) + 1;
                    }
                    else if (CPUBBrand == "AuthenticAMD")
                    {
                        QueryCPURegisters(0x80000008, Registers);
                        AvailableLogicalCPUCores = ((unsigned)(Registers[2] & 0xFF)) + 1;
                    }
                    return AvailableLogicalCPUCores;
                }

                int CThreadPool::GetAvailablePhysicalCPUCores()
                {
                    unsigned int Registers[4] = { 0 };
                    const std::string CPUBBrand = CThreadPool::GetCPUBBrandName();
                    int AvailablePhysicalCPUCores = 0;
                    if (CPUBBrand == "GenuineIntel")
                    {
                        QueryCPURegisters(4, Registers);
                        AvailablePhysicalCPUCores = ((Registers[0] >> 26) & 0x3F) + 1;
                    }
                    else if (CPUBBrand == "AuthenticAMD")
                    {
                        QueryCPURegisters(0x80000008, Registers);
                        AvailablePhysicalCPUCores = ((unsigned)(Registers[2] & 0xFF)) + 1;
                    }
                    QueryCPURegisters(1, Registers);
                    if ((Registers[3] & (1 << 28)) && (AvailablePhysicalCPUCores < GetAvailableLogicalCPUs()))
                    {
                        return AvailablePhysicalCPUCores / 2;
                    }
                    return AvailablePhysicalCPUCores;
                }

                bool CThreadPool::IsHyperThreadingAvailable()
                {
                    const int AvailableLogicalCPUs = GetAvailableLogicalCPUs();
                    const int AvailablePhysicalCPUCores = GetAvailablePhysicalCPUCores();
                    unsigned int Registers[4] = { 0 };
                    QueryCPURegisters(1, Registers);
                    return ((Registers[3] & (1 << 28)) && (AvailablePhysicalCPUCores < AvailableLogicalCPUs));
                }

                CThreadPool::CThreadPool(const StartingThreadSet Set, const int MaximalTotalThreads, const CThreadBase::ContentionScope Scope) :
                    m_IsDispatchingEvents(false)
                {
                    int TotalThreads = 0;
                    switch (Set)
                    {
                        case eEmpty:
                            break;
                        case eAvailablePhysicalCPUCores:
                            TotalThreads = CThreadPool::GetAvailablePhysicalCPUCores();
                            break;
                        case eAvailableLogicalCPUCores:
                            TotalThreads = CThreadPool::GetAvailableLogicalCPUCores();
                            break;
                        case eAvailableLogicalCPUs:
                            TotalThreads = CThreadPool::GetAvailableLogicalCPUs();
                            break;
                    }
                    StartUp(std::min(MaximalTotalThreads, TotalThreads), Scope);
                }

                CThreadPool::CThreadPool(const int TotalThreads, const CThreadBase::ContentionScope Scope) :
                    m_IsDispatchingEvents(false)
                {
                    StartUp(TotalThreads, Scope);
                }

                CThreadPool::~CThreadPool()
                {
                    FinalizePool();
                }

                void CThreadPool::StartUp(const int TotalThreads, const CThreadBase::ContentionScope Scope)
                {
                    for (int i = 0 ; i < TotalThreads ; ++i)
                    {
                        CPoolThread* pPoolThread = new CPoolThread(i, this);
                        m_Threads.push_back(pPoolThread);
                        pPoolThread->Start(Scope);
                    }
                }

                bool CThreadPool::AddThread(const CThreadBase::ContentionScope Scope)
                {
                    CPoolThread* pPoolThread = new CPoolThread(m_Threads.size(), this);
                    m_Threads.push_back(pPoolThread);
                    return pPoolThread->Start(Scope);
                }

                int CThreadPool::GetTotalThreads()
                {
                    return m_Threads.size();
                }

                int CThreadPool::GetTotalIdleThreads()
                {
                    m_IdleListCondition.BlockingLock();
                    const int TotalIdleThreads = m_IdleThreads.size();
                    m_IdleListCondition.Unlock();
                    return TotalIdleThreads;
                }

                bool CThreadPool::DispatchExecutionUnit(CExecutionUnit* pExecutionUnit, const bool ThreadedMode, const int PriorityOnlyWithRootPermisions)
                {
                    if (pExecutionUnit)
                    {
                        if (ThreadedMode)
                        {
                            CPoolThread* pPoolThread = GetThreadOnIdle();
                            if (pPoolThread)
                            {
                                if (PriorityOnlyWithRootPermisions)
                                {
                                    pPoolThread->SetPriority(PriorityOnlyWithRootPermisions);
                                }
                                if (m_IsDispatchingEvents)
                                {
                                    OnThreadOnWorking(pPoolThread, pExecutionUnit);
                                }
                                return pPoolThread->SetExecutionUnit(pExecutionUnit);
                            }
                        }
                        else
                        {
                            pExecutionUnit->BlockingLock();
                            const bool SequentiaExecutionResult = pExecutionUnit->Distpatch(nullptr);
                            pExecutionUnit->Unlock();
                            return SequentiaExecutionResult;
                        }
                    }
                    return false;
                }

                void CThreadPool::FinalizePool()
                {
                    if (m_Threads.size())
                    {
                        if (m_IsDispatchingEvents)
                        {
                            OnBeginFinalizePool();
                        }
                        for (auto pPoolThread : m_Threads)
                        {
                            pPoolThread->Finalize();
                            pPoolThread->GetDeletionMutex()->BlockingLock();
                            delete pPoolThread;
                        }
                        m_Threads.clear();
                        m_IdleThreads.clear();
                        if (m_IsDispatchingEvents)
                        {
                            OnEndFinalizePool();
                        }
                    }
                }

                void CThreadPool::Synchronize()
                {
                    const unsigned int TotalThreads = m_Threads.size();
                    if (TotalThreads)
                    {
                        if (m_IsDispatchingEvents)
                        {
                            OnBeginSynchronize();
                        }

                        m_IdleListCondition.BlockingLock();

                        while (m_IdleThreads.size() != TotalThreads)
                        {
                            m_IdleListCondition.Wait();
                        }

                        m_IdleListCondition.Unlock();

                        if (m_IsDispatchingEvents)
                        {
                            OnEndSynchronize();
                        }
                    }
                }

                void CThreadPool::SetDispatchingEvents(const bool Active)
                {
                    m_IsDispatchingEvents = Active;
                }

                bool CThreadPool::IsDispatchingEvents() const
                {
                    return m_IsDispatchingEvents;
                }

                int CThreadPool::GetConcurrencyLevel()
                {
                    return pthread_getconcurrency();
                }

                bool CThreadPool::SetConcurrencyLevel(const int ConcurrencyLevel)
                {
                    return !pthread_setconcurrency(ConcurrencyLevel);
                }

                bool CThreadPool::FinalizeThreadById(const int ThreadId)
                {
                    std::vector<CPoolThread*>::const_iterator EndThreads = m_Threads.end();
                    for (std::vector<CPoolThread*>::const_iterator ppPoolThread = m_Threads.begin() ; ppPoolThread != EndThreads ; ++ppPoolThread)
                        if ((*ppPoolThread)->GetThreadId() == ThreadId)
                        {
                            (*ppPoolThread)->Finalize();
                            return true;
                        }
                    return false;
                }

                void CThreadPool::OnBeginFinalizePool() const
                {
                }

                void CThreadPool::OnEndFinalizePool() const
                {
                }

                void CThreadPool::OnBeginSynchronize() const
                {
                }

                void CThreadPool::OnEndSynchronize() const
                {
                }

                void CThreadPool::OnThreadOnIdle(const CPoolThread* /*pPoolThread*/) const
                {
                }

                void CThreadPool::OnThreadOnWorking(const CPoolThread* /*pPoolThread*/, const CExecutionUnit* /*pExecutionUnit*/) const
                {
                }

                bool CThreadPool::SetThreadOnIdle(CPoolThread* pPoolThread)
                {
                    if (pPoolThread && (pPoolThread->GetThreadPool() == this))
                    {
                        m_IdleListCondition.BlockingLock();
                        for (std::list<CPoolThread*>::const_iterator ppPoolThread = m_IdleThreads.begin() ; ppPoolThread != m_IdleThreads.end() ; ++ppPoolThread)
                            if ((*ppPoolThread) == pPoolThread)
                            {
                                m_IdleListCondition.Unlock();
                                return false;
                            }
                        m_IdleThreads.push_back(pPoolThread);
                        if (m_IsDispatchingEvents)
                        {
                            OnThreadOnIdle(pPoolThread);
                        }
                        m_IdleListCondition.Broadcast();
                        m_IdleListCondition.Unlock();
                        return true;
                    }
                    return true;
                }

                int CThreadPool::GetTotalThreadsOnIdle()
                {
                    return m_IdleThreads.size();
                }

                CPoolThread* CThreadPool::GetThreadOnIdle()
                {
                    if (m_Threads.size())
                    {
                        m_IdleListCondition.BlockingLock();
                        while (m_IdleThreads.empty())
                        {
                            m_IdleListCondition.Wait();
                        }
                        CPoolThread* pPoolThread = m_IdleThreads.front();
                        m_IdleThreads.pop_front();
                        m_IdleListCondition.Unlock();
                        return pPoolThread;
                    }
                    return nullptr;
                }
            }
        }
    }
}
