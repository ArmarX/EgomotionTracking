/*
 * Mutex.h
 */

#pragma once

#include "../../../Common/Includes.h"
#include "../../../Common/DataTypes.h"

namespace RVL
{
    namespace Processing
    {
        namespace CPU
        {
            namespace Threading
            {
                class CMutex
                {
                public:

                    enum OperationResult
                    {
                        eSuccess = 0, eMutexErrorStatusNotCallingThreadAlreadyLocked = EBUSY, eMutexErrorStatusUninitialized = EINVAL, eMutexErrorStatusCallingThreadAlreadyLocked = EDEADLK, eMutexErrorStatusNotOwner = EPERM
                    };

                    CMutex();
                    virtual ~CMutex();

                    bool IsLocked();
                    OperationResult BlockingLock();
                    OperationResult NonBlockingLock();
                    OperationResult Unlock();

                protected:

                    pthread_mutex_t m_Mutex;
                    pthread_mutexattr_t m_Attributes;
                };
            }
        }
    }
}

