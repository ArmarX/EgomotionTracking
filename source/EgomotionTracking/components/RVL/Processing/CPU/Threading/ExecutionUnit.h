/*
 * ExecutionUnit.h
 */

#pragma once

#include "../../../Common/DataTypes.h"
#include "Mutex.h"

namespace RVL
{
    namespace Processing
    {
        namespace CPU
        {
            namespace Threading
            {
                class CPoolThread;

                class CExecutionUnit : public CMutex
                {
                public:

                    CExecutionUnit();
                    ~CExecutionUnit() override;

                    virtual bool Distpatch(volatile const bool* pKeepRunning = NULL);
                    virtual void SetExecuterPoolThread(CPoolThread* pPoolThread);
                    virtual void ClearExecuterPoolThread();
                    CPoolThread* GetExecuterPoolThread();

                    int GetTrialId() const;
                    int GetSubprocessId() const;

                protected:

                    bool KeepRunning() const;
                    virtual bool Execute() = 0;

                    volatile const bool* m_pKeepRunning;
                    CPoolThread* m_pPoolThread;

                    int m_TrialId;
                    int m_SubprocessId;
                };
            }
        }
    }
}

