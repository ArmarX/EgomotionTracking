/*
 * Condition.h
 */

#pragma once

#include "Mutex.h"

namespace RVL
{
    namespace Processing
    {
        namespace CPU
        {
            namespace Threading
            {
                class CCondition : public CMutex
                {
                public:

                    enum OperationResult
                    {
                        eSuccess = 0, eConditionErrorStatusInvalidHandler = EINVAL, eConditionErrorStatusCallingThreadNotMutexOwner = EPERM
                    };

                    CCondition();
                    ~CCondition() override;

                    OperationResult Wait();
                    OperationResult Signal();
                    OperationResult Broadcast();

                protected:

                    pthread_cond_t m_Condition;
                };
            }
        }
    }
}

