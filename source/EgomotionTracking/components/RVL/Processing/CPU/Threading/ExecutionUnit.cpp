/*
 * ExecutionUnit.cpp
 */

#include "ExecutionUnit.h"
#include "PoolThread.h"

namespace RVL
{
    namespace Processing
    {
        namespace CPU
        {
            namespace Threading
            {
                CExecutionUnit::CExecutionUnit() :
                    CMutex(),
                    m_pKeepRunning(nullptr),
                    m_pPoolThread(nullptr),
                    m_TrialId(0),
                    m_SubprocessId(0)
                {
                }

                CExecutionUnit::~CExecutionUnit()
                    = default;

                bool CExecutionUnit::Distpatch(volatile const bool* pKeepRunning)
                {
                    m_pKeepRunning = nullptr;
                    if (pKeepRunning)
                    {
                        m_pKeepRunning = pKeepRunning;
                        const bool Result = Execute();
                        ++m_TrialId;
                        m_pKeepRunning = nullptr;
                        return Result;
                    }
                    else
                    {
                        const bool Result = Execute();
                        ++m_TrialId;
                        return Result;
                    }
                }

                void CExecutionUnit::SetExecuterPoolThread(CPoolThread* pPoolThread)
                {
                    m_pPoolThread = pPoolThread;
                }

                void CExecutionUnit::ClearExecuterPoolThread()
                {
                    m_pPoolThread = nullptr;
                }

                CPoolThread* CExecutionUnit::GetExecuterPoolThread()
                {
                    return m_pPoolThread;
                }

                int CExecutionUnit::GetTrialId() const
                {
                    return m_TrialId;
                }

                int CExecutionUnit::GetSubprocessId() const
                {
                    return m_SubprocessId;
                }

                bool CExecutionUnit::KeepRunning() const
                {
                    return m_pKeepRunning ? *m_pKeepRunning : true;
                }
            }
        }
    }
}
