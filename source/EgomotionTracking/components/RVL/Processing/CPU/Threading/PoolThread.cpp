/*
 * PoolThread.cpp
 */

#include "PoolThread.h"
#include "ThreadPool.h"
#include "ExecutionUnit.h"

namespace RVL
{
    namespace Processing
    {
        namespace CPU
        {
            namespace Threading
            {
                CPoolThread::CPoolThread(const int ThreadId, CThreadPool* pThreadPool) :
                    CThreadBase(ThreadId),
                    m_KeepRunning(false),
                    m_pThreadPool(pThreadPool),
                    m_pExecutionUnit(nullptr),
                    m_DeletionMutex(),
                    m_WorkCondition()
                {
                }

                CPoolThread::~CPoolThread()
                    = default;

                CThreadPool* CPoolThread::GetThreadPool()
                {
                    return m_pThreadPool;
                }

                bool CPoolThread::Start(const ContentionScope Scope)
                {
                    return CThreadBase::Start(CThreadBase::eJoinable, Scope);
                }

                bool CPoolThread::SetExecutionUnit(CExecutionUnit* pExecutionUnit)
                {
                    if (pExecutionUnit)
                    {
                        m_WorkCondition.BlockingLock();
                        m_pExecutionUnit = pExecutionUnit;
                        m_WorkCondition.Signal();
                        m_WorkCondition.Unlock();
                        return true;
                    }
                    return false;
                }

                CExecutionUnit* CPoolThread::GetExecutionUnit()
                {
                    return m_pExecutionUnit;
                }

                CMutex* CPoolThread::GetDeletionMutex()
                {
                    return &m_DeletionMutex;
                }

                CCondition* CPoolThread::GetWorkCondition()
                {
                    return &m_WorkCondition;
                }

                void CPoolThread::Finalize()
                {
                    m_KeepRunning = false;
                    m_WorkCondition.BlockingLock();
                    m_Status = eStopped;
                    m_pExecutionUnit = nullptr;
                    m_WorkCondition.Signal();
                    m_WorkCondition.Unlock();
                }

                void* CPoolThread::Run()
                {
                    m_DeletionMutex.BlockingLock();

                    while (IsRunning())
                    {
                        m_pThreadPool->SetThreadOnIdle(this);

                        m_WorkCondition.BlockingLock();
                        while (IsRunning() && (!m_pExecutionUnit))
                        {
                            m_WorkCondition.Wait();
                        }

                        if (m_pExecutionUnit)
                        {
                            m_KeepRunning = true;
                            m_pExecutionUnit->BlockingLock();
                            m_pExecutionUnit->SetExecuterPoolThread(this);
                            m_pExecutionUnit->Distpatch(&m_KeepRunning);
                            m_pExecutionUnit->ClearExecuterPoolThread();
                            m_pExecutionUnit->Unlock();
                            m_pExecutionUnit = nullptr;
                            if (m_KeepRunning)
                            {
                                IcrementTrialId();
                            }
                            m_KeepRunning = false;
                        }

                        m_WorkCondition.Unlock();
                    }

                    m_DeletionMutex.Unlock();
                    return nullptr;
                }
            }
        }
    }
}
