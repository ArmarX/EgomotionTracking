/*
 * PoolThread.h
 */

#pragma once

#include "ThreadBase.h"
#include "Mutex.h"
#include "Condition.h"

namespace RVL
{
    namespace Processing
    {
        namespace CPU
        {
            namespace Threading
            {
                class CThreadPool;
                class CExecutionUnit;

                class CPoolThread : public CThreadBase
                {
                public:

                    CPoolThread(const int ThreadId, CThreadPool* pThreadPool);
                    ~CPoolThread() override;

                    CThreadPool* GetThreadPool();

                    virtual bool Start(const ContentionScope Scope = eSystemContention);

                    bool SetExecutionUnit(CExecutionUnit* pExecutionUnit);
                    CExecutionUnit* GetExecutionUnit();

                    CMutex* GetDeletionMutex();
                    CCondition* GetWorkCondition();

                    void Finalize();

                protected:

                    void* Run() override;

                    volatile bool m_KeepRunning;
                    CThreadPool* m_pThreadPool;
                    CExecutionUnit* m_pExecutionUnit;
                    CMutex m_DeletionMutex;
                    CCondition m_WorkCondition;
                };
            }
        }
    }
}

