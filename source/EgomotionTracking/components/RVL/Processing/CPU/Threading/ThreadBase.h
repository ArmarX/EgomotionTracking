/*
 * ThreadBase.h
 */

#pragma once

#include "Mutex.h"

namespace RVL
{
    namespace Processing
    {
        namespace CPU
        {
            namespace Threading
            {
                class CThreadBase
                {

                public:

                    enum Status
                    {
                        eFree, eStopped, eRunning
                    };

                    enum AttachableMode
                    {
                        eJoinable, eDetached
                    };

                    enum ContentionScope
                    {
                        eProcessContention, eSystemContention
                    };

                    static int GetMaximalPriorityAvailable();
                    static int GetMinimalPriorityAvailable();

                    CThreadBase(const int ThreadId);
                    virtual ~CThreadBase();

                    //Notice: In Unix/Linux To change priority it is necessary to have super user permission for the RR scheduling http://en.wikipedia.org/wiki/Setuid
                    bool SetPriority(const int Priority);
                    int GetPriority();

                    bool IsRunning() const;
                    bool IsStopped() const;
                    Status GetStatus() const;
                    AttachableMode GetAttachableMode() const;
                    ContentionScope GetContentionScope() const;
                    int GetThreadId() const;

                    void IcrementTrialId();
                    void SetTrialId(const int TrialId);
                    int GetTrialId() const;

                    virtual bool Start(const AttachableMode Mode = eJoinable, const ContentionScope Scope = eSystemContention);
                    virtual bool Stop();

                    bool Detach();
                    bool Join(void** ppReturnedValue = NULL);

                    int GetCPUAffinityMask();

                protected:

                    bool Create(const AttachableMode Mode, const ContentionScope Scope);

                    bool Yield();
                    bool Sleep(const Real TimeIntervalInMilliSeconds);
                    void Exit(void* pReturnValue);

                    virtual bool Starting();
                    virtual bool Stopping();
                    virtual bool Finishing();
                    virtual void* Run() = 0;

                    int m_InstanceId;
                    Status m_Status;
                    AttachableMode m_AttachableMode;
                    ContentionScope m_ContentionScope;
                    int m_ThreadId;
                    int m_TrialId;
                    pthread_t m_Thread;
                    pthread_attr_t m_Attributes;

                private:

                    static void* Dispatch(void* pData);
                };
            }
        }
    }
}

