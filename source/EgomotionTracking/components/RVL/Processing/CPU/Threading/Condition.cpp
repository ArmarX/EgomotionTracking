/*
 * Condition.cpp
 */

#include "Condition.h"

namespace RVL
{
    namespace Processing
    {
        namespace CPU
        {
            namespace Threading
            {
                CCondition::CCondition() :
                    CMutex()
                {
                    pthread_cond_init(&m_Condition, nullptr);
                }

                CCondition::~CCondition()
                {
                    pthread_cond_destroy(&m_Condition);
                }

                //Note: Inner mutex should be locked before calling this method, it will return in a locked state after the counter part has called the signal and unlock in this fixed order.
                CCondition::OperationResult CCondition::Wait()
                {
                    return CCondition::OperationResult(pthread_cond_wait(&m_Condition, &m_Mutex));
                }

                //Note: Inner mutex should be locked before calling this method, the waiting thread(s) will resume when the mutex is unlocked.
                CCondition::OperationResult CCondition::Signal()
                {
                    return CCondition::OperationResult(pthread_cond_signal(&m_Condition));
                }

                CCondition::OperationResult CCondition::Broadcast()
                {
                    return CCondition::OperationResult(pthread_cond_broadcast(&m_Condition));
                }
            }
        }
    }
}
