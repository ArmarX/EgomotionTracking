/*
 * ThreadPool.h
 */

#pragma once

#include "PoolThread.h"
#include "Condition.h"

namespace RVL
{
    namespace Processing
    {
        namespace CPU
        {
            namespace Threading
            {
                class CExecutionUnit;

                class CThreadPool
                {
                public:

                    static const std::string GetCPUBBrandName();
                    static int GetAvailableLogicalCPUs();
                    static int GetAvailableLogicalCPUCores();
                    static int GetAvailablePhysicalCPUCores();
                    static bool IsHyperThreadingAvailable();

                    enum StartingThreadSet
                    {
                        eEmpty, eAvailablePhysicalCPUCores, eAvailableLogicalCPUCores, eAvailableLogicalCPUs
                    };

                    CThreadPool(const StartingThreadSet Set, const int MaximalTotalThreads, const CThreadBase::ContentionScope Scope = CThreadBase::eSystemContention);
                    CThreadPool(const int TotalThreads, const CThreadBase::ContentionScope Scope = CThreadBase::eSystemContention);
                    virtual ~CThreadPool();

                    bool AddThread(const CThreadBase::ContentionScope Scope);

                    int GetTotalThreads();
                    int GetTotalIdleThreads();

                    bool DispatchExecutionUnit(CExecutionUnit* pExecutionUnit, const bool ThreadedMode, const int PriorityOnlyWithRootPermisions = 0);
                    void FinalizePool();
                    void Synchronize();

                    void SetDispatchingEvents(const bool Active);
                    bool IsDispatchingEvents() const;

                    int GetConcurrencyLevel();
                    bool SetConcurrencyLevel(const int ConcurrencyLevel);

                    bool FinalizeThreadById(const int ThreadId);

                protected:

                    friend class CPoolThread;

                    virtual void OnBeginFinalizePool() const;
                    virtual void OnEndFinalizePool() const;
                    virtual void OnBeginSynchronize() const;
                    virtual void OnEndSynchronize() const;
                    virtual void OnThreadOnIdle(const CPoolThread* pPoolThread) const;
                    virtual void OnThreadOnWorking(const CPoolThread* pPoolThread, const CExecutionUnit* pExecutionUnit) const;

                    bool SetThreadOnIdle(CPoolThread* pPoolThread);
                    int GetTotalThreadsOnIdle();
                    CPoolThread* GetThreadOnIdle();

                private:

                    void StartUp(const int TotalThreads, const CThreadBase::ContentionScope Scope);

                    bool m_IsDispatchingEvents;
                    std::vector<CPoolThread*> m_Threads;
                    std::list<CPoolThread*> m_IdleThreads;
                    CCondition m_IdleListCondition;

                    static void QueryCPURegisters(unsigned Index, unsigned Registers[4]);
                };
            }
        }
    }
}

