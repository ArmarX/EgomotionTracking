/*
 * OperationProgressInterface.cpp
 */

#include "OperationProgressInterface.h"

namespace RVL
{
    namespace Processing
    {
        IOperationProgressInterface::IOperationProgressInterface() :
            m_ExectionMode(eSynchronous),
            m_OperationId(0),
            m_InitialStage(0),
            m_CurrentStage(0),
            m_FinalStage(0),
            m_TotalStages(0)
        {
        }

        IOperationProgressInterface::~IOperationProgressInterface()
            = default;

        bool IOperationProgressInterface::SetOperationSettings(const ExectionMode Mode, const int OperationId, const int InitialStage, const int FinalStage)
        {
            m_ExectionMode = Mode;
            m_OperationId = OperationId;
            m_InitialStage = InitialStage;
            m_FinalStage = FinalStage;
            m_TotalStages = (m_FinalStage - m_InitialStage) + 1;
            return (m_TotalStages >= 1);
        }

        bool IOperationProgressInterface::StartOperation(const int OperationId)
        {
            if (m_OperationId == OperationId)
            {
                m_CurrentStage = m_InitialStage;
                OnProgress(m_OperationId, m_CurrentStage);
                return true;
            }
            return false;
        }

        bool IOperationProgressInterface::SetOperationProgress(const int OperationId, const int CurrentStage)
        {
            if (m_OperationId == OperationId)
            {
                m_CurrentStage = CurrentStage;
                OnProgress(m_OperationId, m_CurrentStage);
                return true;
            }
            return false;
        }

        bool IOperationProgressInterface::SetOperationStepProgress(const int OperationId)
        {
            if (m_OperationId == OperationId)
            {
                OnProgress(m_OperationId, ++m_CurrentStage);
                return true;
            }
            return false;
        }

        bool IOperationProgressInterface::FinishOperation(const int OperationId)
        {
            if (m_OperationId == OperationId)
            {
                m_CurrentStage = m_FinalStage;
                OnProgress(m_OperationId, m_CurrentStage);
                return true;
            }
            return false;
        }
    }
}
