/*
 * OperationProgressInterface.h
 */

#pragma once

#include "../../Common/Includes.h"
#include "../../Common/DataTypes.h"

namespace RVL
{
    namespace Processing
    {
        class IOperationProgressInterface
        {
        public:

            enum ExectionMode
            {
                eSynchronous, eAsynchronous
            };

            IOperationProgressInterface();
            virtual ~IOperationProgressInterface();

            virtual bool SetOperationSettings(const ExectionMode Mode, const int OperationId, const int InitialStage, const int FinalStage);
            virtual bool StartOperation(const int OperationId);
            virtual bool SetOperationProgress(const int OperationId, const int CurrentStage);
            virtual bool SetOperationStepProgress(const int OperationId);
            virtual bool FinishOperation(const int OperationId);

        protected:

            virtual void OnProgress(const int OperationId, const int Stage) = 0;

            ExectionMode m_ExectionMode;
            int m_OperationId;
            int m_InitialStage;
            int m_CurrentStage;
            int m_FinalStage;
            int m_TotalStages;
        };
    }
}

