/*
 * ContinuousBoundingBox2D.cpp
 */

#include "ContinuousBoundingBox2D.h"

namespace RVL
{
    namespace Containers
    {
        namespace _2D
        {
            CContinuousBoundingBox2D::CContinuousBoundingBox2D() :
                m_X0(g_RealPlusInfinity),
                m_Y0(g_RealPlusInfinity),
                m_X1(g_RealMinusInfinity),
                m_Y1(g_RealMinusInfinity)
            {
            }

            CContinuousBoundingBox2D::~CContinuousBoundingBox2D()
                = default;

            void CContinuousBoundingBox2D::SetEmpty()
            {
                m_X0 = g_RealPlusInfinity;
                m_Y0 = g_RealPlusInfinity;
                m_X1 = g_RealMinusInfinity;
                m_Y1 = g_RealMinusInfinity;
            }

            void CContinuousBoundingBox2D::SetInitial(const Mathematics::_2D::CVector2D& Point)
            {
                m_X0 = Point.GetX();
                m_Y0 = Point.GetY();
                m_X1 = Point.GetX();
                m_Y1 = Point.GetY();
            }

            void CContinuousBoundingBox2D::SetInitial(const Real X, const Real Y)
            {
                m_X0 = X;
                m_Y0 = Y;
                m_X1 = X;
                m_Y1 = Y;
            }

            void CContinuousBoundingBox2D::Extend(const CContinuousBoundingBox2D& BoundingBox2D)
            {
                if (BoundingBox2D.m_X0 < m_X0)
                {
                    m_X0 = BoundingBox2D.m_X0;
                }
                if (BoundingBox2D.m_X1 > m_X1)
                {
                    m_X1 = BoundingBox2D.m_X1;
                }
                if (BoundingBox2D.m_Y0 < m_Y0)
                {
                    m_Y0 = BoundingBox2D.m_Y0;
                }
                if (BoundingBox2D.m_Y1 > m_Y1)
                {
                    m_Y1 = BoundingBox2D.m_Y1;
                }
            }

            void CContinuousBoundingBox2D::Extend(const Mathematics::_2D::CVector2D& Point)
            {
                if (IsEmpty())
                {
                    m_X1 = m_X0 = Point.GetX();
                    m_Y1 = m_Y0 = Point.GetY();
                }
                else
                {
                    if (Point.GetX() < m_X0)
                    {
                        m_X0 = Point.GetX();
                    }
                    else if (Point.GetX() > m_X1)
                    {
                        m_X1 = Point.GetX();
                    }
                    if (Point.GetY() < m_Y0)
                    {
                        m_Y0 = Point.GetY();
                    }
                    else if (Point.GetY() > m_Y1)
                    {
                        m_Y1 = Point.GetY();
                    }
                }
            }

            void CContinuousBoundingBox2D::Extend(const Real X, const Real Y)
            {
                if (IsEmpty())
                {
                    m_X0 = X;
                    m_Y0 = Y;
                    m_X1 = X;
                    m_Y1 = Y;
                }
                else
                {
                    if (X < m_X0)
                    {
                        m_X0 = X;
                    }
                    else if (X > m_X1)
                    {
                        m_X1 = X;
                    }
                    if (Y < m_Y0)
                    {
                        m_Y0 = Y;
                    }
                    else if (Y > m_Y1)
                    {
                        m_Y1 = Y;
                    }
                }
            }

            bool CContinuousBoundingBox2D::Expand(const Real Delta)
            {
                if (IsEmpty())
                {
                    return false;
                }
                m_X0 -= Delta;
                m_Y0 -= Delta;
                m_X1 += Delta;
                m_Y1 += Delta;
                return true;
            }

            void CContinuousBoundingBox2D::operator=(const CContinuousBoundingBox2D& BoundingBox2D)
            {
                m_X0 = BoundingBox2D.m_X0;
                m_Y0 = BoundingBox2D.m_Y0;
                m_X1 = BoundingBox2D.m_X1;
                m_Y1 = BoundingBox2D.m_Y1;
            }

            void CContinuousBoundingBox2D::SetBounds(const Real X0, const Real Y0, const Real X1, const Real Y1)
            {
                m_X0 = std::min(X0, X1);
                m_Y0 = std::min(Y0, Y1);
                m_X1 = std::max(X0, X1);
                m_Y1 = std::max(Y0, Y1);
            }

            bool CContinuousBoundingBox2D::GetBounds(Real& X0, Real& Y0, Real& X1, Real& Y1) const
            {
                if (IsEmpty())
                {
                    X0 = g_RealPlusInfinity;
                    Y0 = g_RealPlusInfinity;
                    X1 = g_RealMinusInfinity;
                    Y1 = g_RealMinusInfinity;
                    return false;
                }
                else
                {
                    X0 = m_X0;
                    Y0 = m_Y0;
                    X1 = m_X1;
                    Y1 = m_Y1;
                    return true;
                }
            }

            Mathematics::_2D::CVector2D CContinuousBoundingBox2D::GetMinPoint() const
            {
                return Mathematics::_2D::CVector2D(m_X0, m_Y0);
            }

            Mathematics::_2D::CVector2D CContinuousBoundingBox2D::GetMaxPoint() const
            {
                return Mathematics::_2D::CVector2D(m_X1, m_Y1);
            }

            Real CContinuousBoundingBox2D::GetX0() const
            {
                return m_X0;
            }

            Real CContinuousBoundingBox2D::GetY0() const
            {
                return m_Y0;
            }

            Real CContinuousBoundingBox2D::GetX1() const
            {
                return m_X1;
            }

            Real CContinuousBoundingBox2D::GetY1() const
            {
                return m_Y1;
            }

            Real CContinuousBoundingBox2D::GetWidth() const
            {
                return (m_X0 <= m_X1) ? m_X1 - m_X0 : 0;
            }

            Real CContinuousBoundingBox2D::GetHeight() const
            {
                return (m_Y0 <= m_Y1) ? m_Y1 - m_Y0 : 0;
            }

            Real CContinuousBoundingBox2D::GetArea() const
            {
                return ((m_X0 <= m_X1) && (m_Y0 <= m_Y1)) ? (m_X1 - m_X0) * (m_Y1 - m_Y0) : 0;
            }

            bool CContinuousBoundingBox2D::GetDirectedDistance(const CContinuousBoundingBox2D& BoundingBox2D, Real& DX, Real& DY) const
            {
                if (IsEmpty() || BoundingBox2D.IsEmpty())
                {
                    DX = g_RealPlusInfinity;
                    DY = g_RealPlusInfinity;
                    return false;
                }
                else if (&BoundingBox2D == this)
                {
                    DX = 0;
                    DY = 0;
                    return false;
                }
                else if (Intersects(BoundingBox2D))
                {
                    DX = 0;
                    DY = 0;
                    return true;
                }
                else
                {
                    DX = GetDirectedDistance(m_X0, m_X1, BoundingBox2D.m_X0, BoundingBox2D.m_X1);
                    DY = GetDirectedDistance(m_Y0, m_Y1, BoundingBox2D.m_Y0, BoundingBox2D.m_Y1);
                    return true;
                }
            }

            bool CContinuousBoundingBox2D::GetCenterPoint(Mathematics::_2D::CVector2D C) const
            {
                if (IsEmpty())
                {
                    C = Mathematics::_2D::CVector2D::s_PlusInfinity;
                    return false;
                }
                else
                {
                    C.Set(Real(m_X1 + m_X0) * Real(0.5), Real(m_Y1 + m_Y0) * Real(0.5));
                    return true;
                }
            }

            bool CContinuousBoundingBox2D::GetCenterPoint(Real& CX, Real& CY) const
            {
                if (IsEmpty())
                {
                    CX = g_RealPlusInfinity;
                    CY = g_RealPlusInfinity;
                    return false;
                }
                else
                {
                    CX = Real(m_X1 + m_X0) * Real(0.5);
                    CY = Real(m_Y1 + m_Y0) * Real(0.5);
                    return true;
                }
            }

            bool CContinuousBoundingBox2D::Intersects(const CContinuousBoundingBox2D& BoundingBox2D) const
            {
                if (IsEmpty() || BoundingBox2D.IsEmpty())
                {
                    return false;
                }
                if (BoundingBox2D.m_X0 < m_X0)
                {
                    if (BoundingBox2D.m_X1 < m_X0)
                    {
                        return false;
                    }
                }
                else if (m_X1 < BoundingBox2D.m_X0)
                {
                    return false;
                }
                if (BoundingBox2D.m_Y0 < m_Y0)
                {
                    if (BoundingBox2D.m_Y1 < m_Y0)
                    {
                        return false;
                    }
                }
                else if (m_Y1 < BoundingBox2D.m_Y0)
                {
                    return false;
                }
                return true;
            }

            bool CContinuousBoundingBox2D::GetIntersection(const CContinuousBoundingBox2D& BoundingBox2D, CContinuousBoundingBox2D& IntersectionBox) const
            {
                if (Intersects(BoundingBox2D))
                {
                    const Real X0 = std::max(BoundingBox2D.m_X0, m_X0);
                    const Real Y0 = std::max(BoundingBox2D.m_Y0, m_Y0);
                    const Real X1 = std::min(BoundingBox2D.m_X0, m_X0);
                    const Real Y1 = std::min(BoundingBox2D.m_Y0, m_Y0);
                    IntersectionBox.m_X0 = X0;
                    IntersectionBox.m_Y0 = Y0;
                    IntersectionBox.m_X1 = X1;
                    IntersectionBox.m_Y1 = Y1;
                    return true;
                }
                else
                {
                    IntersectionBox.SetEmpty();
                    return false;
                }
            }

            bool CContinuousBoundingBox2D::IsEmpty() const
            {
                return ((m_X0 > m_X1) || (m_Y0 > m_Y1));
            }

            bool CContinuousBoundingBox2D::HasArea() const
            {
                return ((m_X0 < m_X1) && (m_Y0 < m_Y1));
            }

            bool CContinuousBoundingBox2D::IsInside(const Real X, const Real Y) const
            {
                return HasArea() && (X >= m_X0) && (X <= m_X1) && (Y >= m_Y0) && (Y <= m_Y1);
            }

            Real CContinuousBoundingBox2D::GetDiagonalLenght() const
            {
                if (HasArea())
                {
                    const Real DX = m_X1 - m_X0;
                    const Real DY = m_Y1 - m_Y0;
                    return std::sqrt(DX * DX + DY * DY);
                }
                return Real(0);
            }

            bool CContinuousBoundingBox2D::operator!=(const CContinuousBoundingBox2D& BoundingBox2D) const
            {
                return NonEquals(m_X0, BoundingBox2D.m_X0) || NonEquals(m_Y0, BoundingBox2D.m_Y0) || NonEquals(m_X1, BoundingBox2D.m_X1) || NonEquals(m_Y1, BoundingBox2D.m_Y1);
            }

            bool CContinuousBoundingBox2D::operator==(const CContinuousBoundingBox2D& BoundingBox2D) const
            {
                return Equals(m_X0, BoundingBox2D.m_X0) && Equals(m_Y0, BoundingBox2D.m_Y0) && Equals(m_X1, BoundingBox2D.m_X1) && Equals(m_Y1, BoundingBox2D.m_Y1);
            }

            const std::string CContinuousBoundingBox2D::ToString() const
            {
                std::ostringstream OutputString;
                OutputString.precision(g_RealDisplayDigits);
                OutputString << "<ContinuousBoundingBox2D X0=\"" << m_X0 << "\" Y0=\"" << m_Y0 << "\" X1=\"" << m_X1 << "\" Y1=\"" << m_Y1 << "\"/>";
                return OutputString.str();
            }

            Real CContinuousBoundingBox2D::GetDirectedDistance(const Real A, const Real B, const Real P, const Real Q) const
            {
                return (A <= P) ? ((B <= P) ? P - B : Real(0)) : ((Q <= A) ? A - Q : Real(0));
            }
        }
    }
}
