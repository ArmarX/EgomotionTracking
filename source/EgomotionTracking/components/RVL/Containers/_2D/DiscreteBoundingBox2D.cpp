/*
 * DiscreteBoundingBox2D.cpp
 */

#include "DiscreteBoundingBox2D.h"

namespace RVL
{
    namespace Containers
    {
        namespace _2D
        {
            CDiscreteBoundingBox2D::CDiscreteBoundingBox2D() :
                m_X0(g_IntegerPlusInfinity),
                m_Y0(g_IntegerPlusInfinity),
                m_X1(g_IntegerMinusInfinity),
                m_Y1(g_IntegerMinusInfinity)
            {
            }

            CDiscreteBoundingBox2D::~CDiscreteBoundingBox2D()
                = default;

            void CDiscreteBoundingBox2D::SetEmpty()
            {
                m_X0 = g_IntegerPlusInfinity;
                m_Y0 = g_IntegerPlusInfinity;
                m_X1 = g_IntegerMinusInfinity;
                m_Y1 = g_IntegerMinusInfinity;
            }

            void CDiscreteBoundingBox2D::SetInitial(const int X, const int Y)
            {
                m_X0 = X;
                m_Y0 = Y;
                m_X1 = X;
                m_Y1 = Y;
            }

            void CDiscreteBoundingBox2D::Extend(const CDiscreteBoundingBox2D& BoundingBox2D)
            {
                if (BoundingBox2D.m_X0 < m_X0)
                {
                    m_X0 = BoundingBox2D.m_X0;
                }
                if (BoundingBox2D.m_X1 > m_X1)
                {
                    m_X1 = BoundingBox2D.m_X1;
                }
                if (BoundingBox2D.m_Y0 < m_Y0)
                {
                    m_Y0 = BoundingBox2D.m_Y0;
                }
                if (BoundingBox2D.m_Y1 > m_Y1)
                {
                    m_Y1 = BoundingBox2D.m_Y1;
                }
            }

            void CDiscreteBoundingBox2D::Extend(const int X, const int Y)
            {
                if (IsEmpty())
                {
                    m_X0 = X;
                    m_Y0 = Y;
                    m_X1 = X;
                    m_Y1 = Y;
                }
                else
                {
                    if (X < m_X0)
                    {
                        m_X0 = X;
                    }
                    else if (X > m_X1)
                    {
                        m_X1 = X;
                    }
                    if (Y < m_Y0)
                    {
                        m_Y0 = Y;
                    }
                    else if (Y > m_Y1)
                    {
                        m_Y1 = Y;
                    }
                }
            }

            bool CDiscreteBoundingBox2D::Expand(const int Delta)
            {
                if (IsEmpty())
                {
                    return false;
                }
                m_X0 -= Delta;
                m_Y0 -= Delta;
                m_X1 += Delta;
                m_Y1 += Delta;
                return true;
            }

            void CDiscreteBoundingBox2D::operator=(const CDiscreteBoundingBox2D& BoundingBox2D)
            {
                m_X0 = BoundingBox2D.m_X0;
                m_Y0 = BoundingBox2D.m_Y0;
                m_X1 = BoundingBox2D.m_X1;
                m_Y1 = BoundingBox2D.m_Y1;
            }

            void CDiscreteBoundingBox2D::SetBounds(const int X0, const int Y0, const int X1, const int Y1)
            {
                m_X0 = std::min(X0, X1);
                m_Y0 = std::min(Y0, Y1);
                m_X1 = std::max(X0, X1);
                m_Y1 = std::max(Y0, Y1);
            }

            bool CDiscreteBoundingBox2D::GetBounds(int& X0, int& Y0, int& X1, int& Y1) const
            {
                if (IsEmpty())
                {
                    X0 = g_IntegerPlusInfinity;
                    Y0 = g_IntegerPlusInfinity;
                    X1 = g_IntegerMinusInfinity;
                    Y1 = g_IntegerMinusInfinity;
                    return false;
                }
                else
                {
                    X0 = m_X0;
                    Y0 = m_Y0;
                    X1 = m_X1;
                    Y1 = m_Y1;
                    return true;
                }
            }

            int CDiscreteBoundingBox2D::GetX0() const
            {
                return m_X0;
            }

            int CDiscreteBoundingBox2D::GetY0() const
            {
                return m_Y0;
            }

            int CDiscreteBoundingBox2D::GetX1() const
            {
                return m_X1;
            }

            int CDiscreteBoundingBox2D::GetY1() const
            {
                return m_Y1;
            }

            int CDiscreteBoundingBox2D::GetWidth() const
            {
                return (m_X0 <= m_X1) ? m_X1 - m_X0 + 1 : 0;
            }

            int CDiscreteBoundingBox2D::GetHeight() const
            {
                return (m_Y0 <= m_Y1) ? m_Y1 - m_Y0 + 1 : 0;
            }

            int CDiscreteBoundingBox2D::GetArea() const
            {
                return ((m_X0 <= m_X1) && (m_Y0 <= m_Y1)) ? (m_X1 - m_X0 + 1) * (m_Y1 - m_Y0 + 1) : 0;
            }

            bool CDiscreteBoundingBox2D::GetDirectedDistance(const CDiscreteBoundingBox2D& BoundingBox2D, int& DX, int& DY) const
            {
                if (IsEmpty() || BoundingBox2D.IsEmpty())
                {
                    DX = g_IntegerPlusInfinity;
                    DY = g_IntegerPlusInfinity;
                    return false;
                }
                else if (&BoundingBox2D == this)
                {
                    DX = 0;
                    DY = 0;
                    return false;
                }
                else if (Intersects(BoundingBox2D))
                {
                    DX = 0;
                    DY = 0;
                    return true;
                }
                else
                {
                    DX = GetDirectedDistance(m_X0, m_X1, BoundingBox2D.m_X0, BoundingBox2D.m_X1);
                    DY = GetDirectedDistance(m_Y0, m_Y1, BoundingBox2D.m_Y0, BoundingBox2D.m_Y1);
                    return true;
                }
            }

            bool CDiscreteBoundingBox2D::GetCenterPoint(Real& CX, Real& CY) const
            {
                if (IsEmpty())
                {
                    CX = g_RealPlusInfinity;
                    CY = g_RealPlusInfinity;
                    return false;
                }
                else
                {
                    CX = Real(m_X1 + m_X0) * Real(0.5);
                    CY = Real(m_Y1 + m_Y0) * Real(0.5);
                    return true;
                }
            }

            bool CDiscreteBoundingBox2D::Intersects(const CDiscreteBoundingBox2D& BoundingBox2D) const
            {
                if (IsEmpty() || BoundingBox2D.IsEmpty())
                {
                    return false;
                }
                if (BoundingBox2D.m_X0 < m_X0)
                {
                    if (BoundingBox2D.m_X1 < m_X0)
                    {
                        return false;
                    }
                }
                else if (m_X1 < BoundingBox2D.m_X0)
                {
                    return false;
                }
                if (BoundingBox2D.m_Y0 < m_Y0)
                {
                    if (BoundingBox2D.m_Y1 < m_Y0)
                    {
                        return false;
                    }
                }
                else if (m_Y1 < BoundingBox2D.m_Y0)
                {
                    return false;
                }
                return true;
            }

            bool CDiscreteBoundingBox2D::GetIntersection(const CDiscreteBoundingBox2D& BoundingBox2D, CDiscreteBoundingBox2D& IntersectionBox) const
            {
                if (Intersects(BoundingBox2D))
                {
                    const int X0 = std::max(BoundingBox2D.m_X0, m_X0);
                    const int Y0 = std::max(BoundingBox2D.m_Y0, m_Y0);
                    const int X1 = std::min(BoundingBox2D.m_X0, m_X0);
                    const int Y1 = std::min(BoundingBox2D.m_Y0, m_Y0);
                    IntersectionBox.m_X0 = X0;
                    IntersectionBox.m_Y0 = Y0;
                    IntersectionBox.m_X1 = X1;
                    IntersectionBox.m_Y1 = Y1;
                    return true;
                }
                else
                {
                    IntersectionBox.SetEmpty();
                    return false;
                }
            }

            bool CDiscreteBoundingBox2D::IsEmpty() const
            {
                return ((m_X0 > m_X1) || (m_Y0 > m_Y1));
            }

            bool CDiscreteBoundingBox2D::HasArea() const
            {
                return ((m_X0 <= m_X1) && (m_Y0 <= m_Y1));
            }

            bool CDiscreteBoundingBox2D::IsInside(const int X, const int Y, const bool Inclusive) const
            {
                return HasArea() && (Inclusive ? (X > m_X0) && (X < m_X1) && (Y > m_Y0) && (Y < m_Y1) : (X >= m_X0) && (X <= m_X1) && (Y >= m_Y0) && (Y <= m_Y1));
            }

            Real CDiscreteBoundingBox2D::GetDiagonalLenght() const
            {
                if (HasArea())
                {
                    const int DX = m_X1 - m_X0 + 1;
                    const int DY = m_Y1 - m_Y0 + 1;
                    return std::sqrt(DX * DX + DY * DY);
                }
                return Real(0);
            }

            bool CDiscreteBoundingBox2D::operator!=(const CDiscreteBoundingBox2D& BoundingBox2D) const
            {
                return (m_X0 != BoundingBox2D.m_X0) || (m_Y0 != BoundingBox2D.m_Y0) || (m_X1 != BoundingBox2D.m_X1) || (m_Y1 != BoundingBox2D.m_Y1);
            }

            bool CDiscreteBoundingBox2D::operator==(const CDiscreteBoundingBox2D& BoundingBox2D) const
            {
                return (m_X0 == BoundingBox2D.m_X0) && (m_Y0 == BoundingBox2D.m_Y0) && (m_X1 == BoundingBox2D.m_X1) && (m_Y1 == BoundingBox2D.m_Y1);
            }

            const std::string CDiscreteBoundingBox2D::ToString() const
            {
                std::ostringstream OutputString;
                OutputString << "<DiscreteBoundingBox2D X0=\"" << m_X0 << "\" Y0=\"" << m_Y0 << "\" X1=\"" << m_X1 << "\" Y1=\"" << m_Y1 << "\"/>";
                return OutputString.str();
            }

            int CDiscreteBoundingBox2D::GetDirectedDistance(const int A, const int B, const int P, const int Q) const
            {
                return (A <= P) ? ((B <= P) ? P - B : 0) : ((Q <= A) ? A - Q : 0);
            }
        }
    }
}
