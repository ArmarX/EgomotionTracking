/*
 * DiscreteBoundingBox2D.h
 */

#pragma once

#include "../../Common/Includes.h"
#include "../../Common/DataTypes.h"

namespace RVL
{
    namespace Containers
    {
        namespace _2D
        {
            class CDiscreteBoundingBox2D
            {
            public:

                CDiscreteBoundingBox2D();
                virtual ~CDiscreteBoundingBox2D();

                void SetEmpty();
                void SetInitial(const int X, const int Y);
                void Extend(const CDiscreteBoundingBox2D& BoundingBox2D);
                void Extend(const int X, const int Y);
                bool Expand(const int Delta);
                void SetBounds(const int X0, const int Y0, const int X1, const int Y1);

                bool GetBounds(int& X0, int& Y0, int& X1, int& Y1) const;
                int GetX0() const;
                int GetY0() const;
                int GetX1() const;
                int GetY1() const;
                int GetWidth() const;
                int GetHeight() const;
                int GetArea() const;
                bool GetDirectedDistance(const CDiscreteBoundingBox2D& BoundingBox2D, int& DX, int& DY) const;
                bool GetCenterPoint(Real& CX, Real& CY) const;
                bool Intersects(const CDiscreteBoundingBox2D& BoundingBox2D) const;
                bool GetIntersection(const CDiscreteBoundingBox2D& BoundingBox2D, CDiscreteBoundingBox2D& IntersectionBox) const;
                bool IsEmpty() const;
                bool HasArea() const;
                bool IsInside(const int X, const int Y, const bool Inclusive) const;
                Real GetDiagonalLenght() const;

                void operator=(const CDiscreteBoundingBox2D& BoundingBox2D);
                bool operator!=(const CDiscreteBoundingBox2D& BoundingBox2D) const;
                bool operator==(const CDiscreteBoundingBox2D& BoundingBox2D) const;

                const std::string ToString() const;

            protected:

                int GetDirectedDistance(const int A, const int B, const int P, const int Q) const;

            private:

                int m_X0, m_Y0, m_X1, m_Y1;
            };
        }
    }
}

