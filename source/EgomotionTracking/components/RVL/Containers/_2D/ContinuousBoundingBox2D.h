/*
 * ContinuousBoundingBox2D.h
 */

#pragma once

#include "../../Common/Includes.h"
#include "../../Common/DataTypes.h"
#include "../../Mathematics/_2D/Vector2D.h"

namespace RVL
{
    namespace Containers
    {
        namespace _2D
        {
            class CContinuousBoundingBox2D
            {
            public:

                CContinuousBoundingBox2D();
                virtual ~CContinuousBoundingBox2D();

                void SetEmpty();
                void SetInitial(const Mathematics::_2D::CVector2D& Point);
                void SetInitial(const Real X, const Real Y);
                void Extend(const CContinuousBoundingBox2D& BoundingBox2D);
                void Extend(const Mathematics::_2D::CVector2D& Point);
                void Extend(const Real X, const Real Y);
                bool Expand(const Real Delta);
                void SetBounds(const Real X0, const Real Y0, const Real X1, const Real Y1);

                bool GetBounds(Real& X0, Real& Y0, Real& X1, Real& Y1) const;
                Mathematics::_2D::CVector2D GetMinPoint() const;
                Mathematics::_2D::CVector2D GetMaxPoint() const;
                Real GetX0() const;
                Real GetY0() const;
                Real GetX1() const;
                Real GetY1() const;
                Real GetWidth() const;
                Real GetHeight() const;
                Real GetArea() const;
                bool GetDirectedDistance(const CContinuousBoundingBox2D& BoundingBox2D, Real& DX, Real& DY) const;
                bool GetCenterPoint(Mathematics::_2D::CVector2D C) const;
                bool GetCenterPoint(Real& CX, Real& CY) const;
                bool Intersects(const CContinuousBoundingBox2D& BoundingBox2D) const;
                bool GetIntersection(const CContinuousBoundingBox2D& BoundingBox2D, CContinuousBoundingBox2D& IntersectionBox) const;
                bool IsEmpty() const;
                bool HasArea() const;
                bool IsInside(const Real X, const Real Y) const;
                Real GetDiagonalLenght() const;

                void operator=(const CContinuousBoundingBox2D& BoundingBox2D);
                bool operator!=(const CContinuousBoundingBox2D& BoundingBox2D) const;
                bool operator==(const CContinuousBoundingBox2D& BoundingBox2D) const;

                const std::string ToString() const;

            protected:

                Real GetDirectedDistance(const Real A, const Real B, const Real P, const Real Q) const;

            private:

                Real m_X0, m_Y0, m_X1, m_Y1;
            };
        }
    }
}

