/*
 * ContinuousBoundingBox3D.cpp
 */

#include "ContinuousBoundingBox3D.h"

namespace RVL
{
    namespace Containers
    {
        namespace _3D
        {
            CContinuousBoundingBox3D::CContinuousBoundingBox3D() :
                m_MinPoint(Mathematics::_3D::CVector3D::s_PlusInfinity),
                m_MaxPoint(Mathematics::_3D::CVector3D::s_MinusInfinity)
            {
            }

            CContinuousBoundingBox3D::CContinuousBoundingBox3D(const Mathematics::_3D::CVector3D& InitialPoint) :
                m_MinPoint(InitialPoint),
                m_MaxPoint(InitialPoint)
            {
            }

            CContinuousBoundingBox3D::CContinuousBoundingBox3D(const CContinuousBoundingBox3D& BoundingBox3D)

                = default;

            CContinuousBoundingBox3D::CContinuousBoundingBox3D(const Real X0, const Real Y0, const Real Z0, const Real X1, const Real Y1, const Real Z1) :
                m_MinPoint(std::min(X0, X1), std::min(Y0, Y1), std::min(Z0, Z1)),
                m_MaxPoint(std::max(X0, X1), std::max(Y0, Y1), std::max(Z0, Z1))
            {
            }

            CContinuousBoundingBox3D::CContinuousBoundingBox3D(const Mathematics::_3D::CVector3D& MinPoint, const Mathematics::_3D::CVector3D& MaxPoint) :
                m_MinPoint(std::min(MinPoint.GetX(), MaxPoint.GetX()), std::min(MinPoint.GetY(), MaxPoint.GetY()), std::min(MinPoint.GetZ(), MaxPoint.GetZ())),
                m_MaxPoint(std::max(MinPoint.GetX(), MaxPoint.GetX()), std::max(MinPoint.GetY(), MaxPoint.GetY()), std::max(MinPoint.GetZ(), MaxPoint.GetZ()))
            {
            }

            CContinuousBoundingBox3D::~CContinuousBoundingBox3D()
                = default;

            void CContinuousBoundingBox3D::SetBounds(const CContinuousBoundingBox3D& BoundingBox3D)
            {
                m_MinPoint = BoundingBox3D.m_MinPoint;
                m_MaxPoint = BoundingBox3D.m_MaxPoint;
            }

            void CContinuousBoundingBox3D::SetBounds(const Real X0, const Real Y0, const Real Z0, const Real X1, const Real Y1, const Real Z1)
            {
                m_MinPoint.Set(std::min(X0, X1), std::min(Y0, Y1), std::min(Z0, Z1));
                m_MaxPoint.Set(std::max(X0, X1), std::max(Y0, Y1), std::max(Z0, Z1));
            }

            void CContinuousBoundingBox3D::SetBounds(const Mathematics::_3D::CVector3D& MinPoint, const Mathematics::_3D::CVector3D& MaxPoint)
            {
                m_MinPoint.Set(std::min(MinPoint.GetX(), MaxPoint.GetX()), std::min(MinPoint.GetX(), MaxPoint.GetX()), std::min(MinPoint.GetZ(), MaxPoint.GetZ()));
                m_MaxPoint.Set(std::max(MinPoint.GetX(), MaxPoint.GetX()), std::max(MinPoint.GetX(), MaxPoint.GetX()), std::max(MinPoint.GetZ(), MaxPoint.GetZ()));
            }

            bool CContinuousBoundingBox3D::GetBounds(Real& X0, Real& Y0, Real& Z0, Real& X1, Real& Y1, Real& Z1) const
            {
                if (IsEmpty())
                {

                    X0 = g_RealPlusInfinity;
                    Y0 = g_RealPlusInfinity;
                    Z0 = g_RealPlusInfinity;
                    X1 = g_RealMinusInfinity;
                    Y1 = g_RealMinusInfinity;
                    Z1 = g_RealMinusInfinity;
                    return false;
                }
                else
                {
                    X0 = m_MinPoint.GetX();
                    Y0 = m_MinPoint.GetY();
                    Z0 = m_MinPoint.GetZ();
                    X1 = m_MaxPoint.GetX();
                    Y1 = m_MaxPoint.GetY();
                    Z1 = m_MaxPoint.GetZ();
                    return true;
                }
            }

            void CContinuousBoundingBox3D::GetBounds(Mathematics::_3D::CVector3D& MinPoint, Mathematics::_3D::CVector3D& MaxPoint) const
            {
                MinPoint = m_MinPoint;
                MaxPoint = m_MaxPoint;
            }

            const Mathematics::_3D::CVector3D& CContinuousBoundingBox3D::GetMinPoint() const
            {
                return m_MinPoint;
            }

            const Mathematics::_3D::CVector3D& CContinuousBoundingBox3D::GetMaxPoint() const
            {
                return m_MaxPoint;
            }

            bool CContinuousBoundingBox3D::GetDirectedDistance(const CContinuousBoundingBox3D& BoundingBox3D, Mathematics::_3D::CVector3D& Distance) const
            {
                if ((&BoundingBox3D == this) || IsEmpty() || BoundingBox3D.IsEmpty() || Intersects(BoundingBox3D))
                {
                    Distance.SetZero();
                    return false;
                }
                else
                {
                    Distance.Set(GetDirectedDistance(m_MinPoint.GetX(), m_MaxPoint.GetX(), BoundingBox3D.m_MinPoint.GetX(), BoundingBox3D.m_MaxPoint.GetX()), GetDirectedDistance(m_MinPoint.GetY(), m_MaxPoint.GetY(), BoundingBox3D.m_MinPoint.GetY(), BoundingBox3D.m_MaxPoint.GetY()), GetDirectedDistance(m_MinPoint.GetZ(), m_MaxPoint.GetZ(), BoundingBox3D.m_MinPoint.GetZ(), BoundingBox3D.m_MaxPoint.GetZ()));
                    return true;
                }
            }

            Mathematics::_3D::CVector3D CContinuousBoundingBox3D::GetCenterPoint() const
            {
                if (IsEmpty())
                {
                    return Mathematics::_3D::CVector3D::s_PlusInfinity;
                }
                return Mathematics::_3D::CVector3D::CreateMidPoint(m_MaxPoint, m_MinPoint);
            }

            bool CContinuousBoundingBox3D::Intersects(const CContinuousBoundingBox3D& BoundingBox3D) const
            {
                if (IsEmpty() || BoundingBox3D.IsEmpty())
                {
                    return false;
                }
                if (BoundingBox3D.m_MinPoint.GetX() < m_MinPoint.GetX())
                {
                    if (BoundingBox3D.m_MaxPoint.GetX() < m_MinPoint.GetX())
                    {
                        return false;
                    }
                }
                else if (m_MaxPoint.GetX() < BoundingBox3D.m_MinPoint.GetX())
                {
                    return false;
                }
                if (BoundingBox3D.m_MinPoint.GetY() < m_MinPoint.GetY())
                {
                    if (BoundingBox3D.m_MaxPoint.GetY() < m_MinPoint.GetY())
                    {
                        return false;
                    }
                }
                else if (m_MaxPoint.GetY() < BoundingBox3D.m_MinPoint.GetY())
                {
                    return false;
                }
                if (BoundingBox3D.m_MinPoint.GetZ() < m_MinPoint.GetZ())
                {
                    if (BoundingBox3D.m_MaxPoint.GetZ() < m_MinPoint.GetZ())
                    {
                        return false;
                    }
                }
                else if (m_MaxPoint.GetZ() < BoundingBox3D.m_MinPoint.GetZ())
                {
                    return false;
                }
                return true;
            }

            bool CContinuousBoundingBox3D::GetIntersection(const CContinuousBoundingBox3D& BoundingBox3D, CContinuousBoundingBox3D& IntersectionBox) const
            {
                if (Intersects(BoundingBox3D))
                {
                    Mathematics::_3D::CVector3D MinPoint(std::max(BoundingBox3D.m_MinPoint.GetX(), m_MinPoint.GetX()), std::max(BoundingBox3D.m_MinPoint.GetY(), m_MinPoint.GetY()), std::max(BoundingBox3D.m_MinPoint.GetZ(), m_MinPoint.GetZ()));
                    Mathematics::_3D::CVector3D MaxPoint(std::min(BoundingBox3D.m_MaxPoint.GetX(), m_MaxPoint.GetX()), std::min(BoundingBox3D.m_MaxPoint.GetY(), m_MaxPoint.GetY()), std::min(BoundingBox3D.m_MaxPoint.GetZ(), m_MaxPoint.GetZ()));
                    IntersectionBox.m_MinPoint = MinPoint;
                    IntersectionBox.m_MaxPoint = MaxPoint;
                    return true;
                }
                else
                {
                    IntersectionBox.SetEmpty();
                    return false;
                }
            }

            bool CContinuousBoundingBox3D::IsEmpty() const
            {
                return ((m_MinPoint.GetX() > m_MaxPoint.GetX()) || (m_MinPoint.GetY() > m_MaxPoint.GetY()) || (m_MinPoint.GetZ() > m_MaxPoint.GetZ()));
            }

            bool CContinuousBoundingBox3D::HasVolume() const
            {
                return ((m_MinPoint.GetX() < m_MaxPoint.GetX()) && (m_MinPoint.GetY() < m_MaxPoint.GetY()) && (m_MinPoint.GetZ() < m_MaxPoint.GetZ()));
            }

            void CContinuousBoundingBox3D::SetEmpty()
            {
                m_MinPoint = Mathematics::_3D::CVector3D::s_PlusInfinity;
                m_MaxPoint = Mathematics::_3D::CVector3D::s_MinusInfinity;
            }

            bool CContinuousBoundingBox3D::IsInside(const Mathematics::_3D::CVector3D& Point) const
            {
                if (IsEmpty())
                {
                    return false;
                }
                const Mathematics::_3D::CVector3D Delta0 = Point - m_MinPoint;
                if (IsPositive(Delta0.GetX()) && IsPositive(Delta0.GetY()) && IsPositive(Delta0.GetZ()))
                {
                    const Mathematics::_3D::CVector3D Delta1 = m_MaxPoint - Point;
                    return IsPositive(Delta1.GetX()) && IsPositive(Delta1.GetY()) && IsPositive(Delta1.GetZ());
                }
                return false;
            }

            Real CContinuousBoundingBox3D::GetVolume() const
            {
                if (IsEmpty())
                {
                    return Real(0);
                }
                const Mathematics::_3D::CVector3D Delta = m_MaxPoint - m_MinPoint;
                return Delta.GetX() * Delta.GetY() * Delta.GetZ();
            }

            Real CContinuousBoundingBox3D::GetWidth() const
            {
                const Real Width = m_MaxPoint.GetX() - m_MinPoint.GetX();
                return IsNonPositive(Width) ? Real(0) : Width;
            }

            Real CContinuousBoundingBox3D::GetHeight() const
            {
                const Real Height = m_MaxPoint.GetY() - m_MinPoint.GetY();
                return IsNonPositive(Height) ? Real(0) : Height;
            }

            Real CContinuousBoundingBox3D::GetDepth() const
            {
                const Real Depth = m_MaxPoint.GetZ() - m_MinPoint.GetZ();
                return IsNonPositive(Depth) ? Real(0) : Depth;
            }

            Real CContinuousBoundingBox3D::GetDiagonalLenght() const
            {
                const Mathematics::_3D::CVector3D Delta = m_MaxPoint - m_MinPoint;
                return Delta.GetLength();
            }

            void CContinuousBoundingBox3D::SetInitial(const Mathematics::_3D::CVector3D& Point)
            {
                m_MinPoint = Point;
                m_MaxPoint = Point;
            }

            void CContinuousBoundingBox3D::Extend(const CContinuousBoundingBox3D& BoundingBox3D)
            {
                m_MaxPoint.SetMaximalComponentValue(BoundingBox3D.m_MaxPoint);
                m_MinPoint.SetMinimalComponentValue(BoundingBox3D.m_MinPoint);
            }

            void CContinuousBoundingBox3D::Extend(const Mathematics::_3D::CVector3D& Point)
            {
                m_MaxPoint.SetMaximalComponentValue(Point);
                m_MinPoint.SetMinimalComponentValue(Point);
            }

            void CContinuousBoundingBox3D::Extend(const Real X, const Real Y, const Real Z)
            {
                if (IsEmpty())
                {
                    m_MinPoint.Set(X, Y, Z);
                    m_MaxPoint.Set(X, Y, Z);
                }
                else
                {
                    m_MaxPoint.SetMaximalComponentValue(X, Y, Z);
                    m_MinPoint.SetMinimalComponentValue(X, Y, Z);
                }
            }

            bool CContinuousBoundingBox3D::Expand(const Real Delta)
            {
                if (IsEmpty())
                {
                    return false;
                }
                m_MaxPoint.AddOffsetX(Delta);
                m_MaxPoint.AddOffsetY(Delta);
                m_MaxPoint.AddOffsetZ(Delta);
                m_MinPoint.SubtractOffsetX(Delta);
                m_MinPoint.SubtractOffsetY(Delta);
                m_MinPoint.SubtractOffsetZ(Delta);
                return true;
            }

            void CContinuousBoundingBox3D::operator=(const CContinuousBoundingBox3D& BoundingBox3D)
            {
                m_MaxPoint = BoundingBox3D.m_MaxPoint;
                m_MinPoint = BoundingBox3D.m_MinPoint;
            }

            bool CContinuousBoundingBox3D::operator==(const CContinuousBoundingBox3D& BoundingBox3D) const
            {
                return (m_MaxPoint == BoundingBox3D.m_MaxPoint) && (m_MinPoint == BoundingBox3D.m_MinPoint);
            }

            bool CContinuousBoundingBox3D::operator!=(const CContinuousBoundingBox3D& BoundingBox3D) const
            {
                return (m_MaxPoint != BoundingBox3D.m_MaxPoint) || (m_MinPoint != BoundingBox3D.m_MinPoint);
            }

            const std::string CContinuousBoundingBox3D::ToString() const
            {
                std::ostringstream OutputString;
                OutputString.precision(g_RealDisplayDigits);
                OutputString << "<BoundingBox3D X0=\"" << m_MinPoint.GetX() << "\" Y0=\"" << m_MinPoint.GetY() << "\" Z0=\"" << m_MinPoint.GetZ() << "\" X1=\"" << m_MaxPoint.GetX() << "\" Y1=\"" << m_MaxPoint.GetY() << "\" Z1=\"" << m_MaxPoint.GetZ() << "\"/>";
                return OutputString.str();
            }

            Real CContinuousBoundingBox3D::GetX0() const
            {
                return m_MinPoint.GetX();
            }

            Real CContinuousBoundingBox3D::GetY0() const
            {
                return m_MinPoint.GetY();
            }

            Real CContinuousBoundingBox3D::GetZ0() const
            {
                return m_MinPoint.GetZ();
            }

            Real CContinuousBoundingBox3D::GetX1() const
            {
                return m_MaxPoint.GetX();
            }

            Real CContinuousBoundingBox3D::GetY1() const
            {
                return m_MaxPoint.GetY();
            }

            Real CContinuousBoundingBox3D::GetZ1() const
            {
                return m_MaxPoint.GetZ();
            }

            Real CContinuousBoundingBox3D::GetDirectedDistance(const Real A, const Real B, const Real X, const Real Y) const
            {
                return (A <= X) ? ((B <= X) ? X - B : Real(0)) : ((Y <= A) ? A - Y : Real(0));
            }
        }
    }
}
