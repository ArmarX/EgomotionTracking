/*
 * ContinuousBoundingBox3D.h
 */

#pragma once

#include "../../Common/Includes.h"
#include "../../Common/DataTypes.h"
#include "../../Mathematics/_3D/Vector3D.h"

namespace RVL
{
    namespace Containers
    {
        namespace _3D
        {
            class CContinuousBoundingBox3D
            {
            public:

                CContinuousBoundingBox3D();
                CContinuousBoundingBox3D(const Mathematics::_3D::CVector3D& InitialPoint);
                CContinuousBoundingBox3D(const CContinuousBoundingBox3D& BoundingBox3D);
                CContinuousBoundingBox3D(const Real X0, const Real Y0, const Real Z0, const Real X1, const Real Y1, const Real Z1);
                CContinuousBoundingBox3D(const Mathematics::_3D::CVector3D& MinPoint, const Mathematics::_3D::CVector3D& MaxPoint);
                virtual ~CContinuousBoundingBox3D();

                void SetBounds(const CContinuousBoundingBox3D& BoundingBox3D);
                void SetBounds(const Real X0, const Real Y0, const Real Z0, const Real X1, const Real Y1, const Real Z1);
                void SetBounds(const Mathematics::_3D::CVector3D& MinPoint, const Mathematics::_3D::CVector3D& MaxPoint);
                bool GetBounds(Real& X0, Real& Y0, Real& Z0, Real& X1, Real& Y1, Real& Z1) const;
                void GetBounds(Mathematics::_3D::CVector3D& MinPoint, Mathematics::_3D::CVector3D& MaxPoint) const;
                const Mathematics::_3D::CVector3D& GetMinPoint() const;
                const Mathematics::_3D::CVector3D& GetMaxPoint() const;
                bool GetDirectedDistance(const CContinuousBoundingBox3D& BoundingBox3D, Mathematics::_3D::CVector3D& Distance) const;
                Mathematics::_3D::CVector3D GetCenterPoint() const;
                bool Intersects(const CContinuousBoundingBox3D& BoundingBox3D) const;
                bool GetIntersection(const CContinuousBoundingBox3D& BoundingBox3D, CContinuousBoundingBox3D& IntersectionBox) const;
                bool IsEmpty() const;
                bool HasVolume() const;
                void SetEmpty();
                bool IsInside(const Mathematics::_3D::CVector3D& Point) const;
                Real GetVolume() const;
                Real GetWidth() const;
                Real GetHeight() const;
                Real GetDepth() const;
                Real GetDiagonalLenght() const;
                void SetInitial(const Mathematics::_3D::CVector3D& Point);
                void Extend(const CContinuousBoundingBox3D& BoundingBox3D);
                void Extend(const Mathematics::_3D::CVector3D& Point);
                void Extend(const Real X, const Real Y, const Real Z);
                bool Expand(const Real Delta);
                void operator=(const CContinuousBoundingBox3D& BoundingBox3D);
                bool operator==(const CContinuousBoundingBox3D& BoundingBox3D) const;
                bool operator!=(const CContinuousBoundingBox3D& BoundingBox3D) const;
                const std::string ToString() const;
                Real GetX0() const;
                Real GetY0() const;
                Real GetZ0() const;
                Real GetX1() const;
                Real GetY1() const;
                Real GetZ1() const;

            protected:

                Real GetDirectedDistance(const Real A, const Real B, const Real X, const Real Y) const;

            private:

                Mathematics::_3D::CVector3D m_MinPoint;
                Mathematics::_3D::CVector3D m_MaxPoint;
            };
        }
    }
}
