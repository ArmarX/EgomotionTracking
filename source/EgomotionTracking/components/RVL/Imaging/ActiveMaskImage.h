/*
 * ActiveMaskImage.h
 */

#pragma once

#include "TImage.hpp"

namespace RVL
{
    namespace Imaging
    {
        class CActiveMaskImage : public TImage<bool>
        {
        public:

            CActiveMaskImage();
            CActiveMaskImage(const CActiveMaskImage* pSource);
            CActiveMaskImage(const int Width, const int Height);
            CActiveMaskImage(const CImageSize& Size);
            CActiveMaskImage(const int Width, const int Height, const bool* pBuffer);
            CActiveMaskImage(const CImageSize& Size, const bool* pBuffer);

            virtual ~CActiveMaskImage();

            bool ImportFromFile(const std::string& FileName);

            virtual bool SetSource(const CActiveMaskImage* pSource, const bool Copy);
            bool UpdateCopyFromSource();
            bool UpdateErodeFromSource(const int Radius);
            void SetContentId(const int ContentId);

            int GetContentId() const;
            bool SourceHasChanged(const CActiveMaskImage* pSource) const;
            int GetTotalActivePixels() const;

        protected:

            int m_ContentId;
            const CActiveMaskImage* m_pSource;
        };
    }
}

