/*
 * BayerPattern.h
 */

#pragma once

#include "../Common/Includes.h"
#include "../Common/DataTypes.h"

namespace RVL
{
    namespace Imaging
    {
        class CBayerPattern
        {
        public:

            // Encoding
            // 0 x  M N L P
            // R = 1
            // G = 2
            // B = 4
            // Bit "On" for "Index" of the color channel, example: for e_ _ _ R, (R = 1)  =>  0x_ _ _ 1
            enum Channel
            {
                eRed = 0, eGreen = 1, eBlue = 2
            };

            enum BayerPatternType
            {
                eGRBG = 0x2412, eRGGB = 0x4221, eBGGR = 0x1224, eGBRG = 0x2142
            };

            struct ChannelContents
            {
                Channel m_C[2];
            };

            struct ChannelFlags
            {
                bool m_C[2];
            };

            union ChannelContentMap
            {
                Channel m_Array[4];
                Channel m_YX[2][2];
                ChannelContents m_R[2];
            };

            union ChannelFlagMap
            {
                bool m_Array[4];
                bool m_YX[2][2];
                ChannelFlags m_R[2];
            };

            static std::string BayerPatternToString(const BayerPatternType BayerPattern);

            CBayerPattern();
            CBayerPattern(const BayerPatternType PatternType);
            ~CBayerPattern();

            bool SetType(const BayerPatternType PatternType);
            ChannelContentMap GetBayerPatternMap() const;
            ChannelFlagMap GetBayerPatternMapByChannelContent(const int Content) const;
            const std::string ToString() const;

        protected:

            BayerPatternType m_BayerPatternType;
            ChannelContentMap m_ChannelContentMap;
            ChannelFlagMap m_RedChannelFlagMap;
            ChannelFlagMap m_GreenChannelFlagMap;
            ChannelFlagMap m_BlueChannelFlagMap;
        };
    }
}

