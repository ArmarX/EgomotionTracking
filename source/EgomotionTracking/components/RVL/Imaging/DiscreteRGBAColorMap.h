/*
 * DiscreteRGBAColorMap.h
 */

#pragma once

#include "PixelTypes.h"

namespace RVL
{
    namespace Imaging
    {
        class CDiscreteRGBAColorMap
        {
        public:

            enum PredefinedColorMap
            {
                eRGBA_Red, eRGBA_Green, eRGBA_Blue, eRGBA_Intensity, eRGBA_InverseIntensity, eRGBA_Hot
            };

            CDiscreteRGBAColorMap();
            CDiscreteRGBAColorMap(const PredefinedColorMap ColorMap);
            virtual ~CDiscreteRGBAColorMap();

            bool IsValid() const;
            bool AddColorKey(const DiscreteRGBAPixel& ColorKey, const Real PositionKey);
            bool LoadColorMap();
            void Clear();
            int GetLUTScaleFactor() const;
            const DiscreteRGBAPixel* GetLUT() const;
            const DiscreteRGBAPixel& GetColor(const Real Position) const;
            void LoadPredefinedColorMap(const PredefinedColorMap ColorMap);

        protected:

            Real GetChebyshevColorKeyDistance(const DiscreteRGBAPixel& ColorA, const DiscreteRGBAPixel& ColorB) const;
            Real GetMaximalKeyRatio() const;

            int m_LUTScaleFactor;
            DiscreteRGBAPixel* m_pLUT;
            std::map<Real, DiscreteRGBAPixel> m_ColorKeys;
        };

        const CDiscreteRGBAColorMap g_DiscreteRGBAHotColorMap(CDiscreteRGBAColorMap::eRGBA_Hot);
    }
}
