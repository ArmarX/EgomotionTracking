/*
 * DiscreteRGBAColorMap.cpp
 */

#include "DiscreteRGBAColorMap.h"

namespace RVL
{
    namespace Imaging
    {
        CDiscreteRGBAColorMap::CDiscreteRGBAColorMap() :
            m_LUTScaleFactor(Real(0)),
            m_pLUT(nullptr),
            m_ColorKeys()
        {
        }

        CDiscreteRGBAColorMap::CDiscreteRGBAColorMap(const PredefinedColorMap ColorMap) :
            m_LUTScaleFactor(Real(0)),
            m_pLUT(nullptr),
            m_ColorKeys()
        {
            LoadPredefinedColorMap(ColorMap);
        }

        CDiscreteRGBAColorMap::~CDiscreteRGBAColorMap()
        {
            Clear();
        }

        bool CDiscreteRGBAColorMap::IsValid() const
        {
            return m_pLUT;
        }

        bool CDiscreteRGBAColorMap::AddColorKey(const DiscreteRGBAPixel& ColorKey, const Real PositionKey)
        {
            if ((PositionKey >= Real(0)) && (PositionKey <= Real(1)) && (m_ColorKeys.find(PositionKey) == m_ColorKeys.end()))
            {
                m_ColorKeys[PositionKey] = ColorKey;
                return true;
            }
            return false;
        }

        bool CDiscreteRGBAColorMap::LoadColorMap()
        {
            if (m_ColorKeys.size() > 1)
            {
                if (m_pLUT)
                {
                    delete[] m_pLUT;
                    m_pLUT = nullptr;
                }
                const Real TotalColorDistance = GetMaximalKeyRatio();
                const int LUTQuantizationLevels = int(std::ceil(TotalColorDistance)) + 1;
                m_LUTScaleFactor = Real(LUTQuantizationLevels - 1);
                m_pLUT = new DiscreteRGBAPixel[LUTQuantizationLevels + 1];
                const Real Delta = Real(1) / m_LUTScaleFactor;
                Real Position = Real(0);
                std::map<Real, DiscreteRGBAPixel>::const_iterator pColorKeyA = m_ColorKeys.begin();
                std::map<Real, DiscreteRGBAPixel>::const_iterator pColorKeyB = pColorKeyA;
                ++pColorKeyB;
                DiscreteRGBAPixel* pColor = m_pLUT;
                while (Position <= Real(1))
                {
                    if ((Position >= pColorKeyA->first) && (Position <= pColorKeyB->first))
                    {
                        const Real IntervalLength = pColorKeyB->first - pColorKeyA->first;
                        const Real ContributionA = (pColorKeyB->first - Position) / IntervalLength;
                        const Real ContributionB = (Position - pColorKeyA->first) / IntervalLength;
                        for (int i = 0 ; i < 3 ; ++i)
                        {
                            pColor->m_Channels[i] = Byte(std::round(Real(pColorKeyA->second.m_Channels[i]) * ContributionA + Real(pColorKeyB->second.m_Channels[i]) * ContributionB));
                        }
                        ++pColor;
                        Position += Delta;
                    }
                    else
                    {
                        ++pColorKeyA;
                        ++pColorKeyB;
                    }
                }
                return true;
            }
            return false;
        }

        void CDiscreteRGBAColorMap::Clear()
        {
            if (m_pLUT)
            {
                delete[] m_pLUT;
                m_pLUT = nullptr;
            }
            m_ColorKeys.clear();
            m_LUTScaleFactor = Real(0);
        }

        int CDiscreteRGBAColorMap::GetLUTScaleFactor() const
        {
            return m_LUTScaleFactor;
        }

        const DiscreteRGBAPixel* CDiscreteRGBAColorMap::GetLUT() const
        {
            return m_pLUT;
        }

        const DiscreteRGBAPixel& CDiscreteRGBAColorMap::GetColor(const Real Position) const
        {
            return m_pLUT[int((Position * m_LUTScaleFactor) + Real(0.5))];
        }

        void CDiscreteRGBAColorMap::LoadPredefinedColorMap(const PredefinedColorMap ColorMap)
        {
            Clear();
            switch (ColorMap)
            {
                case eRGBA_Red:
                    AddColorKey(DiscreteRGBAPixel(0, 0, 0, 255), Real(0));
                    AddColorKey(DiscreteRGBAPixel(255, 0, 0, 255), Real(1));
                    LoadColorMap();
                    break;
                case eRGBA_Green:
                    AddColorKey(DiscreteRGBAPixel(0, 0, 0, 255), Real(0));
                    AddColorKey(DiscreteRGBAPixel(0, 255, 0, 255), Real(1));
                    LoadColorMap();
                    break;
                case eRGBA_Blue:
                    AddColorKey(DiscreteRGBAPixel(0, 0, 0, 255), Real(0));
                    AddColorKey(DiscreteRGBAPixel(0, 0, 255, 255), Real(1));
                    LoadColorMap();
                    break;
                case eRGBA_Intensity:
                    AddColorKey(DiscreteRGBAPixel(0, 0, 0, 255), Real(0));
                    AddColorKey(DiscreteRGBAPixel(255, 255, 255, 255), Real(1));
                    LoadColorMap();
                    break;
                case eRGBA_InverseIntensity:
                    AddColorKey(DiscreteRGBAPixel(255, 255, 255, 255), Real(0));
                    AddColorKey(DiscreteRGBAPixel(0, 0, 0, 255), Real(1));
                    LoadColorMap();
                    break;
                case eRGBA_Hot:
                {
                    DiscreteRGBAPixel ColorKeys[16];
                    ColorKeys[0].Set(0, 0, 189, 255);
                    ColorKeys[1].Set(0, 0, 255, 255);
                    ColorKeys[2].Set(0, 66, 255, 255);
                    ColorKeys[3].Set(0, 132, 255, 255);
                    ColorKeys[4].Set(0, 189, 255, 255);
                    ColorKeys[5].Set(0, 255, 255, 255);
                    ColorKeys[6].Set(66, 255, 189, 255);
                    ColorKeys[7].Set(132, 255, 132, 255);
                    ColorKeys[8].Set(189, 255, 66, 255);
                    ColorKeys[9].Set(255, 255, 0, 255);
                    ColorKeys[10].Set(255, 189, 0, 255);
                    ColorKeys[11].Set(255, 132, 0, 255);
                    ColorKeys[12].Set(255, 66, 0, 255);
                    ColorKeys[13].Set(189, 0, 0, 255);
                    ColorKeys[14].Set(132, 0, 0, 255);
                    ColorKeys[15].Set(128, 0, 0, 255);
                    for (int i = 0 ; i < 16 ; ++i)
                    {
                        AddColorKey(ColorKeys[i], Real(i) * Real(0.06666666666666666666667));
                    }
                    LoadColorMap();
                }
                break;
            }
        }

        Real CDiscreteRGBAColorMap::GetChebyshevColorKeyDistance(const DiscreteRGBAPixel& ColorA, const DiscreteRGBAPixel& ColorB) const
        {
            return Real(std::max(std::max(abs(int(ColorA.m_Elements.m_R) - int(ColorB.m_Elements.m_R)), abs(int(ColorA.m_Elements.m_G) - int(ColorB.m_Elements.m_G))), abs(int(ColorA.m_Elements.m_B) - int(ColorB.m_Elements.m_B))));
        }

        Real CDiscreteRGBAColorMap::GetMaximalKeyRatio() const
        {
            std::map<Real, DiscreteRGBAPixel>::const_iterator pColorKeyA = m_ColorKeys.begin();
            std::map<Real, DiscreteRGBAPixel>::const_iterator pColorKeyB = pColorKeyA;
            ++pColorKeyB;
            Real MaximalKeyRatio = Real(0);
            while (pColorKeyB != m_ColorKeys.end())
            {
                const Real KeyRatio = GetChebyshevColorKeyDistance(pColorKeyA->second, pColorKeyB->second) / std::abs(pColorKeyA->first - pColorKeyB->first);
                if (KeyRatio > MaximalKeyRatio)
                {
                    MaximalKeyRatio = KeyRatio;
                }
                ++pColorKeyA;
                ++pColorKeyB;
            }
            return MaximalKeyRatio;
        }
    }
}
