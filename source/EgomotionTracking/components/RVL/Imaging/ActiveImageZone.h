/*
 * ActiveImageZone.h
 */

#pragma once

#include "../Common/Includes.h"
#include "../Common/DataTypes.h"
#include "ImageSize.h"

namespace RVL
{
    namespace Imaging
    {
        class CActiveImageZone
        {
        public:

            CActiveImageZone();
            CActiveImageZone(const CActiveImageZone& ActiveImageZone);
            CActiveImageZone(const CImageSize& Size);
            CActiveImageZone(const int Width, const int Height);
            CActiveImageZone(const int X0, const int Y0, const int X1, const int Y1);
            ~CActiveImageZone();

            bool operator!=(const CActiveImageZone& ActiveImageZone) const;
            bool operator==(const CActiveImageZone& ActiveImageZone) const;
            void operator=(const CActiveImageZone& ActiveImageZone);

            bool IsPostSynchronized(const CActiveImageZone& SourceActiveImageZone, const int Margin) const;
            bool IsValid() const;

            int GetX0() const;
            int GetY0() const;
            int GetX1() const;
            int GetY1() const;

            int GetWidth() const;
            int GetHeight() const;
            int GetArea() const;

            bool Set(const int Width, const int Height, const int Margin = 0);
            bool Set(const CImageSize& Size, const int Margin = 0);
            bool Set(const int X0, const int Y0, const int X1, const int Y1);
            bool Set(const int X0, const int Y0, const int X1, const int Y1, const int ContentId);
            bool Set(const CActiveImageZone& ActiveImageZone, const int Margin);

            bool Scaling(const CActiveImageZone& ActiveImageZone, const Real Scaling);

            void SetContentId(const int ContentId);
            int GetContentId() const;

        protected:

            int m_X0;
            int m_Y0;
            int m_X1;
            int m_Y1;
            int m_ContentId;
        };
    }
}

