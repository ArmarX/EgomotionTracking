/*
 * TImageCharacterization.hpp
 */

#pragma once

#include "../Common/DataTypes.h"
#include "PixelTypes.h"
#include "TImage.hpp"

namespace RVL
{
    namespace Imaging
    {
        struct MagnitudeCharacterization
        {
            enum RelativeMagnitude
            {
                eEquals = 0X00, eBigger = 0X01, eSmaller = 0X02, eUndefined = 0X03
            };

            void Set(const RelativeMagnitude Magnitude, const int Index)
            {
                const int Shifting = Index << 1;
                m_Bits &= ~(0X3 << Shifting);
                m_Bits |= Magnitude << Shifting;
            }

            RelativeMagnitude Get(const int Index) const
            {
                const int Shifting = Index << 1;
                return RelativeMagnitude((m_Bits & (0X3 << Shifting)) >> Shifting);
            }

            bool HasUndefinedLinks() const
            {
                for (int Shifting = 0 ; Shifting < 16 ; Shifting += 2)
                    if (((m_Bits & (0X3 << Shifting)) >> Shifting) == 0X3)
                    {
                        return true;
                    }
                return false;
            }

            unsigned short m_Bits;
        };

        template <typename GenericPixel> class TImageCharacterization
        {
        public:

            TImageCharacterization(const TImage<GenericPixel>* pSourceImage, const Real Tolerance = g_RealMinusEpsilon) :
                m_pSourceImage(pSourceImage)
            {
                if (m_pSourceImage && m_pSourceImage->IsValid())
                {
                    m_CharacterizationImage.Create(m_pSourceImage->GetSize(), true);
                    Characterize(Tolerance);
                }
            }

            virtual ~TImageCharacterization()
            {
            }

            bool Display(TImage<DiscreteRGBPixel>* pDisplayImage) const
            {
                if (pDisplayImage && pDisplayImage->IsValid() && (pDisplayImage->GetSize() == m_CharacterizationImage.GetSize()))
                {
                    pDisplayImage->Clear();
                    const int Width = m_CharacterizationImage.GetWidth();
                    const int Height = m_CharacterizationImage.GetHeight();
                    DiscreteRGBPixel* pDisplayPixel = pDisplayImage->GetWritableBuffer();
                    const MagnitudeCharacterization* pCharacterizationPixel = m_CharacterizationImage.GetReadOnlyBuffer();
                    for (int Y = 0 ; Y < Height ; ++Y)
                        for (int X = 0 ; X < Width ; ++X, ++pDisplayPixel, ++pCharacterizationPixel)
                            if (pCharacterizationPixel->HasUndefinedLinks())
                            {
                                pDisplayPixel->Set(128, 128, 128);
                            }
                            else
                            {
                                const Byte R = (pCharacterizationPixel->m_Bits & 0X003F);
                                const Byte G = (pCharacterizationPixel->m_Bits & 0X0FC0) >> 6;
                                const Byte B = (pCharacterizationPixel->m_Bits & 0XF000) >> 12;
                                pDisplayPixel->Set(R * 4, G * 4, B * 16);
                            }
                    return true;
                }
                return false;
            }

            const TImage<MagnitudeCharacterization>* GetReadOnlyCharacterizationImage() const
            {
                return m_CharacterizationImage;
            }

            bool SaveToFile(const std::string FileName, const int Flags = 0, const bool OverWrite = true)
            {
                return m_CharacterizationImage.SaveToFile(FileName, Flags, OverWrite);
            }

        protected:

            virtual MagnitudeCharacterization::RelativeMagnitude Compare(const GenericPixel* pConnectedPixel, const GenericPixel* pBasePixel, const Real Tolerance)
            {
                const Real Delta = Real(*pConnectedPixel) - Real(*pBasePixel);
                if (std::abs(Delta) <= Tolerance)
                {
                    return MagnitudeCharacterization::eEquals;
                }
                if (Delta > Tolerance)
                {
                    return MagnitudeCharacterization::eBigger;
                }
                if (Delta < -Tolerance)
                {
                    return MagnitudeCharacterization::eSmaller;
                }
                return MagnitudeCharacterization::eUndefined;
            }

            void Characterize(const GenericPixel Tolerance)
            {
                const int Width = m_pSourceImage->GetWidth();
                const int Height = m_pSourceImage->GetHeight();
                const int SubWidth = Width - 1;
                const int SubHeight = Height - 1;
                const int Deltas[8] = { 1, 1 - Width, -Width, -Width - 1, -1, Width - 1, Width, Width + 1 };
                const GenericPixel* pSourcePixel = m_pSourceImage->GetReadOnlyBuffer();
                MagnitudeCharacterization* pCharacterizationPixel = m_CharacterizationImage.GetWritableBuffer();
                for (int Y = 0 ; Y < Height ; ++Y)
                    for (int X = 0 ; X < Width ; ++X, ++pSourcePixel, ++pCharacterizationPixel)
                        if (X && Y && (X < SubWidth) && (Y < SubHeight))
                            for (int i = 0 ; i < 8 ; ++i)
                            {
                                pCharacterizationPixel->Set(Compare(pSourcePixel + Deltas[i], pSourcePixel, Tolerance), i);
                            }
                        else
                            for (int i = 0 ; i < 8 ; ++i)
                                switch (i)
                                {
                                    case 0:
                                        pCharacterizationPixel->Set((X < SubWidth) ? Compare(pSourcePixel + Deltas[i], pSourcePixel, Tolerance) : MagnitudeCharacterization::eUndefined, i);
                                        break;
                                    case 1:
                                        pCharacterizationPixel->Set(((X < SubWidth) && Y) ? Compare(pSourcePixel + Deltas[i], pSourcePixel, Tolerance) : MagnitudeCharacterization::eUndefined, i);
                                        break;
                                    case 2:
                                        pCharacterizationPixel->Set(Y ? Compare(pSourcePixel + Deltas[i], pSourcePixel, Tolerance) : MagnitudeCharacterization::eUndefined, i);
                                        break;
                                    case 3:
                                        pCharacterizationPixel->Set((X && Y) ? Compare(pSourcePixel + Deltas[i], pSourcePixel, Tolerance) : MagnitudeCharacterization::eUndefined, i);
                                        break;
                                    case 4:
                                        pCharacterizationPixel->Set(X ? Compare(pSourcePixel + Deltas[i], pSourcePixel, Tolerance) : MagnitudeCharacterization::eUndefined, i);
                                        break;
                                    case 5:
                                        pCharacterizationPixel->Set((X && (Y < SubHeight)) ? Compare(pSourcePixel + Deltas[i], pSourcePixel, Tolerance) : MagnitudeCharacterization::eUndefined, i);
                                        break;
                                    case 6:
                                        pCharacterizationPixel->Set((Y < SubHeight) ? Compare(pSourcePixel + Deltas[i], pSourcePixel, Tolerance) : MagnitudeCharacterization::eUndefined, i);
                                        break;
                                    case 7:
                                        pCharacterizationPixel->Set(((X < SubWidth) && (Y < SubHeight)) ? Compare(pSourcePixel + Deltas[i], pSourcePixel, Tolerance) : MagnitudeCharacterization::eUndefined, i);
                                        break;
                                }
            }

            const TImage<GenericPixel>* m_pSourceImage;
            TImage<MagnitudeCharacterization> m_CharacterizationImage;

        };
    }
}

