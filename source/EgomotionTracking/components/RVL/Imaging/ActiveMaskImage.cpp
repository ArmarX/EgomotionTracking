/*
 * ActiveMaskImage.cpp
 */

#include "ActiveMaskImage.h"
#include <Image/ByteImage.h>

namespace RVL
{
    namespace Imaging
    {
        CActiveMaskImage::CActiveMaskImage() :
            TImage<bool>(),
            m_ContentId(-1),
            m_pSource(nullptr)
        {
        }

        CActiveMaskImage::CActiveMaskImage(const CActiveMaskImage* pSource) :
            TImage<bool>(),
            m_ContentId(-1),
            m_pSource(nullptr)
        {
            SetSource(pSource, true);
        }

        CActiveMaskImage::CActiveMaskImage(const int Width, const int Height) :
            TImage<bool>(Width, Height),
            m_ContentId(-1),
            m_pSource(nullptr)
        {
            Set(true);
        }

        CActiveMaskImage::CActiveMaskImage(const CImageSize& Size) :
            TImage<bool>(Size),
            m_ContentId(-1),
            m_pSource(nullptr)
        {
            Set(true);
        }

        CActiveMaskImage::CActiveMaskImage(const int Width, const int Height, const bool* pBuffer) :
            TImage<bool>(Width, Height, pBuffer),
            m_ContentId(-1),
            m_pSource(nullptr)
        {
        }

        CActiveMaskImage::CActiveMaskImage(const CImageSize& Size, const bool* pBuffer) :
            TImage<bool>(Size, pBuffer),
            m_ContentId(-1),
            m_pSource(nullptr)
        {
        }

        CActiveMaskImage::~CActiveMaskImage()
            = default;

        bool CActiveMaskImage::ImportFromFile(const std::string& FileName)
        {
            if (FileName.length())
            {
                CByteImage Loader;
                if (Loader.LoadFromFile(FileName.c_str()) && (Loader.type == CByteImage::eGrayScale) && (Loader.width) && (Loader.height))
                {
                    if (Create(Loader.width, Loader.height, false))
                    {
                        return Copy((bool*) Loader.pixels, true, nullptr);
                    }
                }
            }
            return false;
        }

        bool CActiveMaskImage::SetSource(const CActiveMaskImage* pSource, const bool Copy)
        {
            if (pSource && pSource->IsValid() && (pSource->GetSize() == GetSize()))
            {
                m_pSource = pSource;
                if (Copy)
                {
                    return UpdateCopyFromSource();
                }
                return true;
            }
            else if (m_pSource)
            {
                m_pSource = nullptr;
                m_ContentId = -1;
            }
            return Set(true);
        }

        bool CActiveMaskImage::UpdateCopyFromSource()
        {
            if (m_pSource)
            {
                m_ContentId = m_pSource->m_ContentId;
                memcpy(GetWritableBuffer(), m_pSource->GetReadOnlyBuffer(), m_pSource->GetBufferSize());
                return true;
            }
            return false;
        }

        bool CActiveMaskImage::UpdateErodeFromSource(const int Radius)
        {
            if (m_pSource)
            {
                m_ContentId = m_pSource->m_ContentId;
                memcpy(GetWritableBuffer(), m_pSource->GetReadOnlyBuffer(), m_pSource->GetBufferSize());
                const int SubWidth = GetWidth() - 1;
                const int SubHeight = GetHeight() - 1;
                const bool* pSourcePixel = m_pSource->GetReadOnlyBuffer();
                for (int Y = 0 ; Y <= SubHeight ; ++Y)
                    for (int X = 0 ; X <= SubWidth ; ++X)
                        if (!*pSourcePixel++)
                        {
                            const int EX0 = std::max(X - Radius, 0);
                            const int EY0 = std::max(Y - Radius, 0);
                            const int EX1 = std::min(X + Radius, SubWidth);
                            const int EY1 = std::min(Y + Radius, SubHeight);
                            for (int EY = EY0 ; EY <= EY1 ; ++EY)
                            {
                                bool* pMaskPixel = GetWritableBufferAt(EX0, EY);
                                for (int EX = EX0 ; EX <= EX1 ; ++EX)
                                {
                                    *pMaskPixel++ = false;
                                }
                            }
                        }
                return true;
            }
            return false;
        }

        void CActiveMaskImage::SetContentId(const int ContentId)
        {
            m_ContentId = ContentId;
        }

        int CActiveMaskImage::GetContentId() const
        {
            return m_ContentId;
        }

        bool CActiveMaskImage::SourceHasChanged(const CActiveMaskImage* pSource) const
        {
            if (m_pSource)
            {
                if (m_pSource != pSource)
                {
                    return true;
                }
                return (m_ContentId != m_pSource->m_ContentId);
            }
            else if (pSource)
            {
                return true;
            }
            return false;
        }

        int CActiveMaskImage::GetTotalActivePixels() const
        {
            const int X0 = 0;
            const int Y0 = 0;
            const int X1 = GetWidth() - 1;
            const int Y1 = GetHeight() - 1;
            const int Offset = 0;
            int TotalActivePixels = 0;
            const bool* pActivePixel = GetReadOnlyBufferAt(X0, Y0);
            if (Offset)
            {
                for (int Y = Y0 ; Y <= Y1 ; ++Y, pActivePixel += Offset)
                    for (int X = X0 ; X <= X1 ; ++X)
                        if (*pActivePixel++)
                        {
                            ++TotalActivePixels;
                        }
            }
            else
            {
                for (int Y = Y0 ; Y <= Y1 ; ++Y)
                    for (int X = X0 ; X <= X1 ; ++X)
                        if (*pActivePixel++)
                        {
                            ++TotalActivePixels;
                        }
            }
            return TotalActivePixels;
        }
    }
}
