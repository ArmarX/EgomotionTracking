/*
 * BayerPattern.cpp
 */

#include "BayerPattern.h"

namespace RVL
{
    namespace Imaging
    {
        std::string CBayerPattern::BayerPatternToString(const BayerPatternType BayerPattern)
        {
            switch (BayerPattern)
            {
                case eGRBG:
                    return std::string("GRBG");
                case eRGGB:
                    return std::string("RGGB");
                case eBGGR:
                    return std::string("BGGR");
                case eGBRG:
                    return std::string("GBRG");
            }
            return std::string("Error");
        }

        CBayerPattern::CBayerPattern() :
            m_BayerPatternType(BayerPatternType(0))
        {
            for (int Y = 0 ; Y < 2 ; ++Y)
                for (int X = 0 ; X < 2 ; ++X)
                {
                    m_ChannelContentMap.m_YX[Y][X] = Channel(-1);
                    m_RedChannelFlagMap.m_YX[Y][X] = false;
                    m_GreenChannelFlagMap.m_YX[Y][X] = false;
                    m_BlueChannelFlagMap.m_YX[Y][X] = false;
                }
        }

        CBayerPattern::CBayerPattern(const BayerPatternType PatternType) :
            m_BayerPatternType(PatternType)
        {
            for (int Y = 0, ShiftingBits = 0 ; Y < 2 ; ++Y)
                for (int X = 0 ; X < 2 ; ++X, ShiftingBits += 4)
                {
                    const Channel Content = Channel((m_BayerPatternType & (0xF << ShiftingBits)) >> (ShiftingBits + 1));
                    m_ChannelContentMap.m_YX[Y][X] = Content;
                    m_RedChannelFlagMap.m_YX[Y][X] = (Content == eRed);
                    m_GreenChannelFlagMap.m_YX[Y][X] = (Content == eGreen);
                    m_BlueChannelFlagMap.m_YX[Y][X] = (Content == eBlue);
                }
        }

        CBayerPattern::~CBayerPattern()
            = default;

        bool CBayerPattern::SetType(const BayerPatternType PatternType)
        {
            if ((PatternType == eGRBG) || (PatternType == eRGGB) || (PatternType == eBGGR) || (PatternType == eGBRG))
            {
                m_BayerPatternType = PatternType;
                for (int Y = 0, ShiftingBits = 0 ; Y < 2 ; ++Y)
                    for (int X = 0 ; X < 2 ; ++X, ShiftingBits += 4)
                    {
                        const Channel Content = Channel((m_BayerPatternType & (0xF << ShiftingBits)) >> (ShiftingBits + 1));
                        m_ChannelContentMap.m_YX[Y][X] = Content;
                        m_RedChannelFlagMap.m_YX[Y][X] = (Content == eRed);
                        m_GreenChannelFlagMap.m_YX[Y][X] = (Content == eGreen);
                        m_BlueChannelFlagMap.m_YX[Y][X] = (Content == eBlue);
                    }
                return true;
            }
            return false;
        }

        CBayerPattern::ChannelContentMap CBayerPattern::GetBayerPatternMap() const
        {
            return m_ChannelContentMap;
        }

        CBayerPattern::ChannelFlagMap CBayerPattern::GetBayerPatternMapByChannelContent(const int Content) const
        {
            switch (Content)
            {
                case 0:
                    return m_RedChannelFlagMap;
                case 1:
                    return m_GreenChannelFlagMap;
                case 2:
                    return m_BlueChannelFlagMap;
            }
            CBayerPattern::ChannelFlagMap NullMap;
            memset(&NullMap, 0, sizeof(ChannelFlagMap));
            return m_RedChannelFlagMap;
        }

        const std::string CBayerPattern::ToString() const
        {
            switch (m_BayerPatternType)
            {
                case eGRBG:
                    return std::string("GRBG");
                case eRGGB:
                    return std::string("RGGB");
                case eBGGR:
                    return std::string("BGGR");
                case eGBRG:
                    return std::string("GBRG");
            }
            return std::string("Error");
        }
    }
}
