/*
 * ActiveImageZone.cpp
 */

#include "ActiveImageZone.h"

namespace RVL
{
    namespace Imaging
    {
        CActiveImageZone::CActiveImageZone() :
            m_X0(g_IntegerPlusInfinity),
            m_Y0(g_IntegerPlusInfinity),
            m_X1(g_IntegerMinusInfinity),
            m_Y1(g_IntegerMinusInfinity),
            m_ContentId(-1)
        {
        }

        CActiveImageZone::CActiveImageZone(const CActiveImageZone& ActiveImageZone)

            = default;

        CActiveImageZone::CActiveImageZone(const CImageSize& Size) :
            m_X0(0),
            m_Y0(0),
            m_X1(Size.GetWidth() - 1),
            m_Y1(Size.GetHeight() - 1),
            m_ContentId(-1)
        {

        }

        CActiveImageZone::CActiveImageZone(const int Width, const int Height) :
            m_X0(0),
            m_Y0(0),
            m_X1(Width - 1),
            m_Y1(Height - 1),
            m_ContentId(-1)
        {
        }

        CActiveImageZone::CActiveImageZone(const int X0, const int Y0, const int X1, const int Y1) :
            m_X0(X0),
            m_Y0(Y0),
            m_X1(X1),
            m_Y1(Y1),
            m_ContentId(-1)
        {
        }

        CActiveImageZone::~CActiveImageZone()
            = default;

        bool CActiveImageZone::operator!=(const CActiveImageZone& ActiveImageZone) const
        {
            return (m_X0 != ActiveImageZone.m_X0) || (m_Y0 != ActiveImageZone.m_Y0) || (m_X1 != ActiveImageZone.m_X1) || (m_Y1 != ActiveImageZone.m_Y1);
        }

        bool CActiveImageZone::operator==(const CActiveImageZone& ActiveImageZone) const
        {
            return (m_X0 == ActiveImageZone.m_X0) && (m_Y0 == ActiveImageZone.m_Y0) && (m_X1 == ActiveImageZone.m_X1) && (m_Y1 == ActiveImageZone.m_Y1);
        }

        void CActiveImageZone::operator=(const CActiveImageZone& ActiveImageZone)
        {
            m_X0 = ActiveImageZone.m_X0;
            m_Y0 = ActiveImageZone.m_Y0;
            m_X1 = ActiveImageZone.m_X1;
            m_Y1 = ActiveImageZone.m_Y1;
            m_ContentId = ActiveImageZone.m_ContentId;
        }

        bool CActiveImageZone::IsPostSynchronized(const CActiveImageZone& SourceActiveImageZone, const int Margin) const
        {
            return (m_ContentId == SourceActiveImageZone.m_ContentId) && (m_X0 == (SourceActiveImageZone.m_X0 + Margin)) && (m_Y0 == (SourceActiveImageZone.m_Y0 + Margin)) && (m_X1 == (SourceActiveImageZone.m_X1 - Margin)) && (m_Y1 == (SourceActiveImageZone.m_Y1 - Margin));
        }

        bool CActiveImageZone::IsValid() const
        {
            return (m_X0 <= m_X1) && (m_Y0 <= m_Y1);
        }

        int CActiveImageZone::GetX0() const
        {
            return m_X0;
        }

        int CActiveImageZone::GetY0() const
        {
            return m_Y0;
        }

        int CActiveImageZone::GetX1() const
        {
            return m_X1;
        }

        int CActiveImageZone::GetY1() const
        {
            return m_Y1;
        }

        int CActiveImageZone::GetWidth() const
        {
            return (m_X0 <= m_X1) ? m_X1 - m_X0 + 1 : 0;
        }

        int CActiveImageZone::GetHeight() const
        {
            return (m_Y0 <= m_Y1) ? m_Y1 - m_Y0 + 1 : 0;
        }

        int CActiveImageZone::GetArea() const
        {
            return ((m_X0 <= m_X1) && (m_Y0 <= m_Y1)) ? (m_X1 - m_X0 + 1) * (m_Y1 - m_Y0 + 1) : 0;
        }

        bool CActiveImageZone::Set(const int Width, const int Height, const int Margin)
        {
            m_X0 = Margin;
            m_Y0 = Margin;
            m_X1 = (Width - 1) - Margin;
            m_Y1 = (Height - 1) - Margin;
            m_ContentId = -1;
            return (m_X0 <= m_X1) && (m_Y0 <= m_Y1);
        }

        bool CActiveImageZone::Set(const CImageSize& Size, const int Margin)
        {
            m_X0 = Margin;
            m_Y0 = Margin;
            m_X1 = (Size.GetWidth() - 1) - Margin;
            m_Y1 = (Size.GetHeight() - 1) - Margin;
            m_ContentId = -1;
            return (m_X0 <= m_X1) && (m_Y0 <= m_Y1);
        }

        bool CActiveImageZone::Set(const int X0, const int Y0, const int X1, const int Y1)
        {
            m_X0 = X0;
            m_Y0 = Y0;
            m_X1 = X1;
            m_Y1 = Y1;
            return (m_X0 <= m_X1) && (m_Y0 <= m_Y1);
        }

        bool CActiveImageZone::Set(const int X0, const int Y0, const int X1, const int Y1, const int ContentId)
        {
            m_X0 = X0;
            m_Y0 = Y0;
            m_X1 = X1;
            m_Y1 = Y1;
            m_ContentId = ContentId;
            return (m_X0 <= m_X1) && (m_Y0 <= m_Y1);
        }

        bool CActiveImageZone::Set(const CActiveImageZone& ActiveImageZone, const int Margin)
        {
            m_X0 = ActiveImageZone.m_X0 + Margin;
            m_Y0 = ActiveImageZone.m_Y0 + Margin;
            m_X1 = ActiveImageZone.m_X1 - Margin;
            m_Y1 = ActiveImageZone.m_Y1 - Margin;
            m_ContentId = ActiveImageZone.m_ContentId;
            return (m_X0 <= m_X1) && (m_Y0 <= m_Y1);
        }

        bool CActiveImageZone::Scaling(const CActiveImageZone& ActiveImageZone, const Real Scaling)
        {
            m_X0 = std::ceil(Real(ActiveImageZone.m_X0) * Scaling);
            m_Y0 = std::ceil(Real(ActiveImageZone.m_Y0) * Scaling);
            m_X1 = std::floor(Real(ActiveImageZone.m_X1) * Scaling);
            m_Y1 = std::floor(Real(ActiveImageZone.m_Y1) * Scaling);
            m_ContentId = ActiveImageZone.m_ContentId;
            return (m_X0 <= m_X1) && (m_Y0 <= m_Y1);
        }

        void CActiveImageZone::SetContentId(const int ContentId)
        {
            m_ContentId = ContentId;
        }

        int CActiveImageZone::GetContentId() const
        {
            return m_ContentId;
        }
    }
}
