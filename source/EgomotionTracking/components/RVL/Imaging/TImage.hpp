/*
 * TImage.hpp
 */

#pragma once

#include "../Common/Includes.h"
#include "../Files/File.h"
#include "../Files/OutFile.h"
#include "../Files/InFile.h"
#include "ImageSize.h"
#include "ActiveImageZone.h"

namespace RVL
{
    namespace Imaging
    {
        template <typename GenericPixel> class TImage
        {
        public:

            template <typename AuxiliarGenericPixel> static TImage<GenericPixel>* Map(const TImage<AuxiliarGenericPixel>* pSourceImage)
            {
                if (pSourceImage && pSourceImage->IsValid())
                {
                    TImage<GenericPixel>* pDestinyImage = new TImage<GenericPixel>(pSourceImage->GetSize());
                    const AuxiliarGenericPixel* pSourcePixel = pSourceImage->GetReadOnlyBuffer();
                    GenericPixel* pDestinyPixel = pDestinyImage->GetWritableBuffer();
                    const int Area = pSourceImage->GetArea();
                    for (int i = 0 ; i < Area ; ++i)
                    {
                        pDestinyPixel[i] = pSourcePixel[i];
                    }
                    return pDestinyImage;
                }
                return NULL;
            }

            TImage() :
                m_Ownership(true),
                m_pBuffer(NULL),
                m_Size()
            {
            }

            TImage(const int Width, const int Height) :
                m_Ownership(true),
                m_pBuffer(NULL),
                m_Size(Width, Height)
            {
                if (m_Size.IsValid())
                {
                    m_pBuffer = new GenericPixel[m_Size.GetArea()];
                }
            }

            TImage(const CImageSize& Size) :
                m_Ownership(true),
                m_pBuffer(NULL),
                m_Size(Size)
            {
                if (m_Size.IsValid())
                {
                    m_pBuffer = new GenericPixel[m_Size.GetArea()];
                }
            }

            TImage(const int Width, const int Height, const GenericPixel* pBuffer) :
                m_Ownership(false),
                m_pBuffer(NULL),
                m_Size(Width, Height)
            {
                if (m_Size.IsValid() && pBuffer)
                {
                    m_pBuffer = const_cast<GenericPixel*>(pBuffer);
                }
            }

            TImage(const CImageSize& Size, const GenericPixel* pBuffer) :
                m_Ownership(false),
                m_pBuffer(NULL),
                m_Size(Size)
            {
                if (m_Size.IsValid() && pBuffer)
                {
                    m_pBuffer = const_cast<GenericPixel*>(pBuffer);
                }
            }

            ~TImage()
            {
                Destroy();
            }

            void Destroy()
            {
                if (m_pBuffer)
                {
                    if (m_Ownership)
                    {
                        delete[] m_pBuffer;
                    }
                    m_pBuffer = NULL;
                    m_Size.SetZero();
                }
            }

            bool Wrap(TImage<GenericPixel>* pImage)
            {
                Destroy();
                if (pImage && pImage->m_pBuffer)
                {
                    m_Ownership = false;
                    m_Size = pImage->m_Size;
                    m_pBuffer = pImage->m_pBuffer;
                    return true;
                }
                return false;
            }

            bool Create(const CImageSize& Size, const bool Clear)
            {
                Destroy();
                if (Size.IsValid())
                {
                    m_Size = Size;
                    m_pBuffer = new GenericPixel[m_Size.GetArea()];
                    m_Ownership = true;
                    if (Clear)
                    {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
                        memset(m_pBuffer, 0, m_Size.GetArea() * sizeof(GenericPixel));
#pragma GCC diagnostic pop
                    }
                    return true;
                }
                return false;
            }

            bool Create(const int Width, const int Height, const bool Clear)
            {
                Destroy();
                if ((Width > 0) && (Height > 0))
                {
                    m_Size.Set(Width, Height);
                    m_pBuffer = new GenericPixel[m_Size.GetArea()];
                    m_Ownership = true;
                    if (Clear)
                    {
                        memset(m_pBuffer, 0, m_Size.GetArea() * sizeof(GenericPixel));
                    }
                    return true;
                }
                return false;
            }

            TImage<GenericPixel>* Clone() const
            {
                if (m_pBuffer)
                {
                    TImage<GenericPixel>* pCloneImage = new TImage<GenericPixel>(m_Size);
                    memcpy(pCloneImage->m_pBuffer, m_pBuffer, m_Size.GetArea() * sizeof(GenericPixel));
                    return pCloneImage;
                }
                return NULL;
            }

            inline bool IsInside(const CActiveImageZone* pActiveImageZone) const
            {
                return m_Size.IsInside(pActiveImageZone);
            }

            inline bool HasBufferOwnership() const
            {
                return m_Ownership;
            }

            inline bool SizeEquals(const int Width, const int Height) const
            {
                return m_Size.Equals(Width, Height);
            }

            inline bool SizeEquals(const CImageSize& Size) const
            {
                return m_Size == Size;
            }

            inline bool IsValid() const
            {
                return m_pBuffer;
            }

            inline int GetWidth() const
            {
                return m_Size.GetWidth();
            }

            inline int GetHeight() const
            {
                return m_Size.GetHeight();
            }

            inline int GetArea() const
            {
                return m_Size.GetArea();
            }

            inline int GetBufferSize() const
            {
                return m_pBuffer ? m_Size.GetArea() * sizeof(GenericPixel) : 0;
            }

            inline const CImageSize& GetSize() const
            {
                return m_Size;
            }

            inline const GenericPixel* GetReadOnlyBuffer() const
            {
                return m_pBuffer;
            }

            inline const GenericPixel* GetReadOnlyBufferAt(const int X, const int Y) const
            {
                return m_pBuffer + m_Size.GetOffset(X, Y);
            }

            inline const GenericPixel* GetReadOnlyBufferAtLine(const int Y) const
            {
                return m_pBuffer + m_Size.GetWidth() * Y;
            }

            inline const GenericPixel* GetBufferEnd() const
            {
                return m_pBuffer + m_Size.GetArea();
            }

            inline GenericPixel* GetWritableBuffer(const bool Force = false)
            {
                return (m_Ownership || Force) ? m_pBuffer : NULL;
            }

            inline GenericPixel* GetWritableBufferAt(const int X, const int Y, const bool Force = false)
            {
                return (m_Ownership || Force) ? m_pBuffer + m_Size.GetOffset(X, Y) : NULL;
            }

            inline GenericPixel* GetWritableBufferAtLine(const int Y, const bool Force = false)
            {
                return (m_Ownership || Force) ? m_pBuffer + m_Size.GetWidth() * Y : NULL;
            }

            bool Clear()
            {
                if (m_pBuffer && m_Ownership)
                {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
                    memset(m_pBuffer, 0, m_Size.GetArea() * sizeof(GenericPixel));
#pragma GCC diagnostic pop
                    return true;
                }
                return false;
            }

            bool Set(const GenericPixel Value, const CActiveImageZone* pActiveImageZone = NULL)
            {
                if (m_pBuffer && m_Ownership)
                {
                    if (pActiveImageZone)
                    {
                        if (m_Size.IsInside(pActiveImageZone))
                        {
                            const int X0 = pActiveImageZone->GetX0();
                            const int Y0 = pActiveImageZone->GetY0();
                            const int X1 = pActiveImageZone->GetX1();
                            const int Y1 = pActiveImageZone->GetY1();
                            const int Offset = m_Size.GetWidth() - pActiveImageZone->GetWidth();
                            GenericPixel* pPixel = m_pBuffer + m_Size.GetOffset(X0, Y0);
                            if (Offset)
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pPixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X)
                                    {
                                        *pPixel++ = Value;
                                    }
                            else
                                for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                    for (int X = X0 ; X <= X1 ; ++X)
                                    {
                                        *pPixel++ = Value;
                                    }
                        }
                    }
                    else
                    {
                        const GenericPixel* const pPixelEnd = m_pBuffer + m_Size.GetArea();
                        GenericPixel* pPixel = m_pBuffer;
                        while (pPixel < pPixelEnd)
                        {
                            *pPixel++ = Value;
                        }
                        return true;
                    }
                }
                return false;
            }

            bool Copy(const TImage<GenericPixel>* pSourceImage, const bool UseBlockMemoryCopy, const CActiveImageZone* pActiveImageZone = NULL)
            {
                if (m_pBuffer && m_Ownership && pSourceImage && pSourceImage->IsValid() && (pSourceImage->GetSize() == m_Size))
                {
                    int X0, Y0, X1, Y1;
                    if (pActiveImageZone)
                    {
                        if (m_Size.IsInside(pActiveImageZone))
                        {
                            X0 = pActiveImageZone->GetX0();
                            Y0 = pActiveImageZone->GetY0();
                            X1 = pActiveImageZone->GetX1();
                            Y1 = pActiveImageZone->GetY1();
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        X0 = 0;
                        Y0 = 0;
                        X1 = m_Size.GetWidth() - 1;
                        Y1 = m_Size.GetHeight() - 1;
                    }
                    const GenericPixel* pSourcePixel = pSourceImage->GetReadOnlyBufferAt(X0, Y0);
                    GenericPixel* pDestinyPixel = m_pBuffer + m_Size.GetOffset(X0, Y0);
                    if (UseBlockMemoryCopy)
                    {
                        const int Width = m_Size.GetWidth();
                        const int BlockSize = (X1 - X0 + 1) * int(sizeof(GenericPixel));
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pSourcePixel += Width, pDestinyPixel += Width)
                        {
                            memcpy(pDestinyPixel, pSourcePixel, BlockSize);
                        }
                    }
                    else
                    {
                        const int ComplementaryOffset = m_Size.GetWidth() - (X1 - X0 + 1);
                        if (ComplementaryOffset)
                        {
                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pDestinyPixel += ComplementaryOffset, pSourcePixel += ComplementaryOffset)
                                for (int X = X0 ; X <= X1 ; ++X, ++pDestinyPixel, ++pSourcePixel)
                                {
                                    *pDestinyPixel = *pSourcePixel;
                                }
                        }
                        else
                        {
                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                for (int X = X0 ; X <= X1 ; ++X, ++pDestinyPixel, ++pSourcePixel)
                                {
                                    *pDestinyPixel = *pSourcePixel;
                                }
                        }
                    }
                    return true;
                }
                return false;
            }

            bool Copy(const GenericPixel* pSourceBuffer, const bool UseBlockMemoryCopy, const CActiveImageZone* pActiveImageZone = NULL)
            {
                if (m_pBuffer && m_Ownership && pSourceBuffer)
                {
                    int X0, Y0, X1, Y1;
                    if (pActiveImageZone)
                    {
                        if (m_Size.IsInside(pActiveImageZone))
                        {
                            X0 = pActiveImageZone->GetX0();
                            Y0 = pActiveImageZone->GetY0();
                            X1 = pActiveImageZone->GetX1();
                            Y1 = pActiveImageZone->GetY1();
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        X0 = 0;
                        Y0 = 0;
                        X1 = m_Size.GetWidth() - 1;
                        Y1 = m_Size.GetHeight() - 1;
                    }
                    const GenericPixel* pSourcePixel = pSourceBuffer + m_Size.GetOffset(X0, Y0);
                    GenericPixel* pDestinyPixel = m_pBuffer + m_Size.GetOffset(X0, Y0);
                    if (UseBlockMemoryCopy)
                    {
                        const int Width = m_Size.GetWidth();
                        const int BlockSize = (X1 - X0 + 1) * int(sizeof(GenericPixel));
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pSourcePixel += Width, pDestinyPixel += Width)
                        {
                            memcpy(pDestinyPixel, pSourcePixel, BlockSize);
                        }
                    }
                    else
                    {
                        const int Offset = m_Size.GetWidth() - (X1 - X0 + 1);
                        if (Offset)
                        {
                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pDestinyPixel += Offset, pSourcePixel += Offset)
                                for (int X = X0 ; X <= X1 ; ++X, ++pDestinyPixel, ++pSourcePixel)
                                {
                                    *pDestinyPixel = *pSourcePixel;
                                }
                        }
                        else
                        {
                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                for (int X = X0 ; X <= X1 ; ++X, ++pDestinyPixel, ++pSourcePixel)
                                {
                                    *pDestinyPixel = *pSourcePixel;
                                }
                        }
                    }
                    return true;
                }
                return false;
            }

            bool SaveToFile(const std::string& FileName, const int Flags = 0, const bool OverWrite = true) const
            {
                if (!m_pBuffer)
                {
                    return false;
                }
                if (!FileName.length())
                {
                    return false;
                }
                if (Files::CFile::Exists(FileName) && (!OverWrite))
                {
                    return false;
                }
                Files::COutFile OutputBinaryFile(FileName, Files::CFile::eBinary);
                if (!OutputBinaryFile.IsReady())
                {
                    return false;
                }
                if (!OutputBinaryFile.Write(Flags))
                {
                    return false;
                }
                if (!OutputBinaryFile.Write(m_Size.GetWidth()))
                {
                    return false;
                }
                if (!OutputBinaryFile.Write(m_Size.GetHeight()))
                {
                    return false;
                }
                if (!OutputBinaryFile.Write(int(sizeof(GenericPixel))))
                {
                    return false;
                }
                if (!OutputBinaryFile.Write(m_pBuffer, GetBufferSize()))
                {
                    return false;
                }
                return OutputBinaryFile.Close();
            }

            bool LoadFromFile(const std::string& FileName, int& Flags)
            {
                const int StoredFileSize = Files::CFile::Exists(FileName);
                if (!StoredFileSize)
                {
                    return false;
                }
                Files::CInFile InputBinaryFile(FileName, Files::CFile::eBinary);
                if (!InputBinaryFile.IsReady())
                {
                    return false;
                }
                if (!InputBinaryFile.Read(Flags))
                {
                    return false;
                }
                int Width = 0;
                if (!InputBinaryFile.Read(Width))
                {
                    return false;
                }
                if (Width <= 0)
                {
                    return false;
                }
                int Height = 0;
                if (!InputBinaryFile.Read(Height))
                {
                    return false;
                }
                if (Height <= 0)
                {
                    return false;
                }
                int Depth = 0;
                if (!InputBinaryFile.Read(Depth))
                {
                    return false;
                }
                if (Depth != sizeof(GenericPixel))
                {
                    return false;
                }
                const int BufferSize = Width * Height * Depth;
                if (StoredFileSize != (int(sizeof(int)) * 4 + BufferSize))
                {
                    return false;
                }
                if (!Create(Width, Height, false))
                {
                    return false;
                }
                return InputBinaryFile.Read(m_pBuffer, BufferSize);
            }

        protected:

            bool m_Ownership;
            GenericPixel* m_pBuffer;
            CImageSize m_Size;
        };
    }
}

