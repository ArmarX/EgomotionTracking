/*
 * DiscreteRGBColorMap.h
 */

#pragma once

#include "PixelTypes.h"

namespace RVL
{
    namespace Imaging
    {
        class CDiscreteRGBColorMap
        {
        public:

            enum PredefinedColorMap
            {
                eRGB_Red, eRGB_Green, eRGB_Blue, eRGB_Intensity, eRGB_InverseIntensity, eRGB_Hot
            };

            CDiscreteRGBColorMap();
            CDiscreteRGBColorMap(const PredefinedColorMap ColorMap);
            virtual ~CDiscreteRGBColorMap();

            bool IsValid() const;
            bool AddColorKey(const DiscreteRGBPixel& ColorKey, const Real PositionKey);
            bool LoadColorMap();
            void Clear();
            int GetLUTScaleFactor() const;
            const DiscreteRGBPixel* GetLUT() const;
            const DiscreteRGBPixel& GetColor(const Real Position) const;
            void LoadPredefinedColorMap(const PredefinedColorMap ColorMap);

        protected:

            Real GetChebyshevColorKeyDistance(const DiscreteRGBPixel& ColorA, const DiscreteRGBPixel& ColorB) const;
            Real GetMaximalKeyRatio() const;

            int m_LUTScaleFactor;
            DiscreteRGBPixel* m_pLUT;
            std::map<Real, DiscreteRGBPixel> m_ColorKeys;
        };

        const CDiscreteRGBColorMap g_DiscreteRGBHotColorMap(CDiscreteRGBColorMap::eRGB_Hot);
    }
}
