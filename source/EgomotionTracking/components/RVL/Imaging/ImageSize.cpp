/*
 * ImageSize.cpp
 */

#include "ImageSize.h"
#include "ActiveImageZone.h"

namespace RVL
{
    namespace Imaging
    {
        CImageSize::CImageSize(const CImageSize& Size)

            = default;

        CImageSize::CImageSize(const int Width, int Height) :
            m_Width(Width),
            m_Height(Height)
        {
        }

        CImageSize::CImageSize() :
            m_Width(0),
            m_Height(0)
        {
        }

        void CImageSize::operator=(const CImageSize& Size)
        {
            m_Width = Size.m_Width;
            m_Height = Size.m_Height;
        }

        bool CImageSize::IsValid() const
        {
            return (m_Width > 0) && (m_Height > 0);
        }

        bool CImageSize::operator==(const CImageSize& Size) const
        {
            return (Size.m_Width == m_Width) && (Size.m_Height == m_Height);
        }

        bool CImageSize::operator!=(const CImageSize& Size) const
        {
            return (Size.m_Width != m_Width) || (Size.m_Height != m_Height);
        }

        int CImageSize::GetArea() const
        {
            return m_Width * m_Height;
        }

        int CImageSize::GetOffset(const int X, const int Y) const
        {
            return m_Width * Y + X;
        }

        void CImageSize::Set(const int Width, int Height)
        {
            m_Width = Width;
            m_Height = Height;
        }

        void CImageSize::SetZero()
        {
            m_Width = 0;
            m_Height = 0;
        }

        int CImageSize::GetWidth() const
        {
            return m_Width;
        }

        int CImageSize::GetHeight() const
        {
            return m_Height;
        }

        bool CImageSize::Equals(const int Width, const int Height) const
        {
            return (Width == m_Width) && (Height == m_Height);
        }

        bool CImageSize::IsInside(const CActiveImageZone* pActiveImageZone) const
        {
            return (m_Width > 0) && (m_Height > 0) && pActiveImageZone && pActiveImageZone->IsValid() && (pActiveImageZone->GetX0() >= 0) && (pActiveImageZone->GetX0() < m_Width) && (pActiveImageZone->GetX1() < m_Width) && (pActiveImageZone->GetY0() >= 0) && (pActiveImageZone->GetY0() < m_Height) && (pActiveImageZone->GetY1() < m_Height);
        }

        CImageSize CImageSize::GetScaled(const Real Scaling) const
        {
            if ((m_Width > 0) && (m_Height > 0) && IsPositive(Scaling))
            {
                return CImageSize(int(std::ceil(Real(m_Width) * Scaling)), int(std::ceil(Real(m_Height) * Scaling)));
            }
            return CImageSize();
        }
    }
}
