/*
 * ImageSize.h
 */

#pragma once

#include "../Common/DataTypes.h"

namespace RVL
{
    namespace Imaging
    {
        class CActiveImageZone;

        class CImageSize
        {
        public:

            CImageSize(const CImageSize& Size);
            CImageSize(const int Width, int Height);
            CImageSize();

            void operator=(const CImageSize& Size);
            bool IsValid() const;
            bool operator==(const CImageSize& Size) const;
            bool operator!=(const CImageSize& Size) const;
            int GetArea() const;
            int GetOffset(const int X, const int Y) const;
            void Set(const int Width, int Height);
            void SetZero();
            int GetWidth() const;
            int GetHeight() const;
            bool Equals(const int Width, const int Height) const;
            bool IsInside(const CActiveImageZone* pActiveImageZone) const;

            CImageSize GetScaled(const Real Scaling) const;

        protected:

            int m_Width;
            int m_Height;
        };
    }
}

