/*
 * TemporalFusion.h
 */

#pragma once

#define _INTENSITY_HISTOGRAM_FUSION_MAXIMAL_RANGE_PRECISION_ Real(0.0625)
#define _INTENSITY_HISTOGRAM_FUSION_DEFAULT_AUTO_RESET_ true
#define _INTENSITY_HISTOGRAM_FUSION_DEFAULT_SAMPLING_MODE_ eDirect
#define _INTENSITY_HISTOGRAM_FUSION_DEFAULT_FUSION_METHOD_ eContinuousGaussian
#define _INTENSITY_HISTOGRAM_FUSION_DEFAULT_RANGE_PRECISION_ Real(0.000976563)
#define _INTENSITY_HISTOGRAM_FUSION_DEFAULT_TOTAL_SAMPLES_PER_FUSION_ 30
#define _INTENSITY_HISTOGRAM_FUSION_MINIMAL_TOTAL_SAMPLES_PER_FUSION_ 8

#include "../../PixelTypes.h"
#include "../../DiscreteRGBColorMap.h"
#include "../TImageProcess.hpp"
#include "TemporalFusionPixel.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Fusion
            {
                class CTemporalFusion : public TImageProcess<Byte, Real>
                {
                public:

                    enum SamplingMode
                    {
                        eDirect, eIndirect
                    };

                    enum FusionMethod
                    {
                        eMean, eContinuousEpanechnikov, eContinuousGaussian, eDiscreteEpanechnikov, eDiscreteGaussian
                    };

                    enum DisplayChannel
                    {
                        eChannelMaximalFrequency, eChannelMean, eChannelRange, eChannelStandardDeviation, eChannelDispersion, eChannelSignalToNoiseRatio, eChannelFusion, eChannelMaximal, eChannelMinimal, eChannelGlobalMaxima
                    };

                    struct FusionMultipleLayerPixel
                    {
                        Real m_Maximal;
                        Real m_Mean;
                        Real m_Fusion;
                        Real m_Minimal;
                    };

                    CTemporalFusion(const TImage<Byte>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone);
                    ~CTemporalFusion() override;

                    void SetAutoReset(const bool AutoReset);
                    bool SetSamplingMode(const SamplingMode TargetSamplingMode);
                    bool SetFusionMethod(const FusionMethod TargetFusionMethod);
                    bool SetRangePrecision(const Real RangePrecision);
                    bool SetTotalSamplesPerFusion(const int TotalSamplesPerFusion);

                    bool IsAutoReset() const;
                    SamplingMode GetSamplingMode() const;
                    FusionMethod GetFusionMethod() const;
                    Real GetRangeEpsilon() const;
                    int GetTotalLoadedSamples() const;
                    int GetTotalSamplesPerFusion() const;

                    const TImage<CTemporalFusionPixel>* GetTemporalFusionImage() const;
                    const TImage<FusionMultipleLayerPixel>* CreateFusionMultipleLayerImage() const;
                    bool Display(TImage<DiscreteRGBPixel>* pDisplayImage, const DisplayChannel Channel, const CDiscreteRGBColorMap* pColorMap, Real& MaximalChannelValue, Real& MinimalChannelValue, Real& ChannelMean, Real& ChannelStandardDeviation) const;

                    bool Execute(const int Trial) override;

                    void Reset();

                protected:

                    void LoadImage(const TImage<Byte>* pSampleImage);
                    void Clear();

                    bool m_AutoReset;
                    SamplingMode m_SamplingMode;
                    FusionMethod m_FusionMethod;
                    Real m_RangePrecision;
                    int m_TotalLoadedSamples;
                    int m_TotalSamplesPerFusion;
                    TImage<CTemporalFusionPixel> m_TemporalFusionImage;
                    std::list<const TImage<Byte>*> m_PendingSampleImages;
                };
            }
        }
    }
}

