/*
 * TemporalFusionPixel.h
 */

#pragma once

#include "../../../Common/Includes.h"
#include "../../../Common/DataTypes.h"
#include "../../../Mathematics/_1D/NormalDistribution.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            class CTemporalFusionPixel
            {
            public:

                typedef short int Bin;

                static int GetMaximalImagesPerFusion();

                CTemporalFusionPixel();
                ~CTemporalFusionPixel();

                void AddIntialSample(const Byte Sample, const int TotalSamples);
                void AddSuccessiveSample(const Byte Sample);
                void Reset();

                int GetMaximalFrequency() const;
                int GetRange() const;
                int GetGlobalMaxima(int* pGlobalMaxima) const;
                int GetMinimal() const;
                int GetMaximal() const;
                int GetAccumulator() const;
                int GetSquareAccumulator() const;

                Real GetMean() const;
                Real GetStandardDeviation() const;
                Real GetDispersion() const;
                Real GetSignalToNoiseRatio() const;
                const Bin* GetReadOnlyHistogram() const;
                Real GetFusion() const;

                Real MeanFusion();
                Real DiscreteEpanechnikovFusion(int* pGlobalMaxima);
                Real ContinuousEpanechnikovFusion(int* pGlobalMaxima, const Real RangeEpsilon);
                Real DiscreteGaussianFusion(int* pGlobalMaxima);
                Real ContinuousGaussianFusion(int* pGlobalMaxima, const Real RangeEpsilon);

                std::string ExportToString(const int X, const int Y) const;

            protected:

                Real EstimateNonNormalizedEpanechnikovDensity(const Real ValueLocation, const Real AdaptiveBandWidth, const Real SquareBandWidthNormalization);
                Real EstimateNonNormalizedGaussianDensity(const Real ValueLocation, const Real BandWidthExponentFactor);

                Bin m_TotalSamples;
                Bin* m_pHistogram;
                Real m_Fusion;
                Byte m_Minimal;
                Byte m_Maximal;
                int m_Accumulator;
                int m_SquareAccumulator;
            };
        }
    }
}

