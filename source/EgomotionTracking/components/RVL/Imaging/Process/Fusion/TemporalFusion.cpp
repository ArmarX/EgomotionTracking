/*
 * TemporalFusion.cpp
 */

#include "TemporalFusion.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Fusion
            {
                CTemporalFusion::CTemporalFusion(const TImage<Byte>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone) :
                    TImageProcess<Byte, Real>(pInImage, pInActiveMaskImage, pInActiveImageZone),
                    m_AutoReset(_INTENSITY_HISTOGRAM_FUSION_DEFAULT_AUTO_RESET_),
                    m_SamplingMode(_INTENSITY_HISTOGRAM_FUSION_DEFAULT_SAMPLING_MODE_),
                    m_FusionMethod(_INTENSITY_HISTOGRAM_FUSION_DEFAULT_FUSION_METHOD_),
                    m_RangePrecision(_INTENSITY_HISTOGRAM_FUSION_DEFAULT_RANGE_PRECISION_),
                    m_TotalLoadedSamples(0),
                    m_TotalSamplesPerFusion(_INTENSITY_HISTOGRAM_FUSION_DEFAULT_TOTAL_SAMPLES_PER_FUSION_),
                    m_TemporalFusionImage(),
                    m_PendingSampleImages()
                {
                    if (IsEnabled())
                    {
                        m_OutImage.Create(m_pInImage->GetSize(), true);
                        m_TemporalFusionImage.Create(m_pInImage->GetSize(), false);
                        InitializeActiveImageZone();
                        InitializeActiveMaskImage();
                    }
                }

                CTemporalFusion::~CTemporalFusion()
                {
                    Clear();
                }

                void CTemporalFusion::SetAutoReset(const bool AutoReset)
                {
                    m_AutoReset = AutoReset;
                }

                bool CTemporalFusion::SetSamplingMode(const SamplingMode TargetSamplingMode)
                {
                    if ((TargetSamplingMode == eDirect) || (TargetSamplingMode == eIndirect))
                    {
                        m_SamplingMode = TargetSamplingMode;
                        return true;
                    }
                    return false;
                }

                bool CTemporalFusion::SetFusionMethod(const FusionMethod TargetFusionMethod)
                {
                    if ((TargetFusionMethod == eMean) || (TargetFusionMethod == eContinuousEpanechnikov) || (TargetFusionMethod == eContinuousGaussian) || (TargetFusionMethod == eDiscreteEpanechnikov) || (TargetFusionMethod == eDiscreteGaussian))
                    {
                        m_FusionMethod = TargetFusionMethod;
                        return true;
                    }
                    return false;
                }

                bool CTemporalFusion::SetRangePrecision(const Real RangePrecision)
                {
                    if (IsPositive(RangePrecision) && (RangePrecision < _INTENSITY_HISTOGRAM_FUSION_MAXIMAL_RANGE_PRECISION_))
                    {
                        m_RangePrecision = RangePrecision;
                        return true;
                    }
                    return false;
                }

                bool CTemporalFusion::SetTotalSamplesPerFusion(const int TotalSamplesPerFusion)
                {
                    if ((TotalSamplesPerFusion >= _INTENSITY_HISTOGRAM_FUSION_MINIMAL_TOTAL_SAMPLES_PER_FUSION_) && (TotalSamplesPerFusion < CTemporalFusionPixel::GetMaximalImagesPerFusion()))
                    {
                        Reset();
                        m_TotalSamplesPerFusion = TotalSamplesPerFusion;
                        return true;
                    }
                    return false;
                }

                bool CTemporalFusion::IsAutoReset() const
                {
                    return m_AutoReset;
                }

                CTemporalFusion::SamplingMode CTemporalFusion::GetSamplingMode() const
                {
                    return m_SamplingMode;
                }

                CTemporalFusion::FusionMethod CTemporalFusion::GetFusionMethod() const
                {
                    return m_FusionMethod;
                }

                Real CTemporalFusion::GetRangeEpsilon() const
                {
                    return m_RangePrecision;
                }

                int CTemporalFusion::GetTotalLoadedSamples() const
                {
                    return m_TotalLoadedSamples;
                }

                int CTemporalFusion::GetTotalSamplesPerFusion() const
                {
                    return m_TotalSamplesPerFusion;
                }

                const TImage<CTemporalFusionPixel>* CTemporalFusion::GetTemporalFusionImage() const
                {
                    return &m_TemporalFusionImage;
                }

                const TImage<CTemporalFusion::FusionMultipleLayerPixel>* CTemporalFusion::CreateFusionMultipleLayerImage() const
                {
                    if (IsEnabled())
                    {
                        TImage<FusionMultipleLayerPixel>* pFusionMultipleLayerImage = new TImage<FusionMultipleLayerPixel>(m_TemporalFusionImage.GetSize());
                        pFusionMultipleLayerImage->Clear();
                        const int X0 = m_OutActiveImageZone.GetX0();
                        const int Y0 = m_OutActiveImageZone.GetY0();
                        const int X1 = m_OutActiveImageZone.GetX1();
                        const int Y1 = m_OutActiveImageZone.GetY1();
                        const int Offset = m_TemporalFusionImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                        FusionMultipleLayerPixel* pFusionMultipleLayerPixel = pFusionMultipleLayerImage->GetWritableBufferAt(X0, Y0);
                        const CTemporalFusionPixel* pTemporalFusionPixel = m_TemporalFusionImage.GetReadOnlyBufferAt(X0, Y0);
                        if (IsMasking())
                        {
                            const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pFusionMultipleLayerPixel += Offset, pTemporalFusionPixel += Offset)
                                for (int X = X0 ; X <= X1 ; ++X, ++pFusionMultipleLayerPixel, ++pTemporalFusionPixel)
                                    if (*pOutActiveMaskPixel++)
                                    {
                                        pFusionMultipleLayerPixel->m_Maximal = pTemporalFusionPixel->GetMaximal();
                                        pFusionMultipleLayerPixel->m_Mean = pTemporalFusionPixel->GetMean();
                                        pFusionMultipleLayerPixel->m_Fusion = pTemporalFusionPixel->GetFusion();
                                        pFusionMultipleLayerPixel->m_Minimal = pTemporalFusionPixel->GetMinimal();
                                    }
                        }
                        else
                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pFusionMultipleLayerPixel += Offset, pTemporalFusionPixel += Offset)
                                for (int X = X0 ; X <= X1 ; ++X, ++pFusionMultipleLayerPixel, ++pTemporalFusionPixel)
                                {
                                    pFusionMultipleLayerPixel->m_Maximal = pTemporalFusionPixel->GetMaximal();
                                    pFusionMultipleLayerPixel->m_Mean = pTemporalFusionPixel->GetMean();
                                    pFusionMultipleLayerPixel->m_Fusion = pTemporalFusionPixel->GetFusion();
                                    pFusionMultipleLayerPixel->m_Minimal = pTemporalFusionPixel->GetMinimal();
                                }
                        return pFusionMultipleLayerImage;
                    }
                    return nullptr;
                }

                bool CTemporalFusion::Display(TImage<DiscreteRGBPixel>* pDisplayImage, const DisplayChannel Channel, const CDiscreteRGBColorMap* pColorMap, Real& MaximalChannelValue, Real& MinimalChannelValue, Real& ChannelMean, Real& ChannelStandardDeviation) const
                {
                    if (pDisplayImage && pDisplayImage->IsValid() && pDisplayImage->HasBufferOwnership() && pDisplayImage->SizeEquals(m_TemporalFusionImage.GetSize()) && pDisplayImage->HasBufferOwnership() && pColorMap && pColorMap->IsValid())
                    {
                        pDisplayImage->Clear();
                        const int X0 = m_OutActiveImageZone.GetX0();
                        const int Y0 = m_OutActiveImageZone.GetY0();
                        const int X1 = m_OutActiveImageZone.GetX1();
                        const int Y1 = m_OutActiveImageZone.GetY1();
                        const int Offset = m_TemporalFusionImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                        int TotalPixels = 0;
                        TImage<Real> ChannelImage(pDisplayImage->GetSize());
                        Real* pChannelPixel = ChannelImage.GetWritableBufferAt(X0, Y0);
                        bool CheckForNaN = false;
                        Real ChannelAccumulator = Real(0), ChannelSquareAccumulator = Real(0);
                        const CTemporalFusionPixel* pTemporalFusionPixel = m_TemporalFusionImage.GetReadOnlyBufferAt(X0, Y0);
                        switch (Channel)
                        {
                            case eChannelMaximalFrequency:
                            {
                                if (IsMasking())
                                {
                                    const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    MinimalChannelValue = g_RealPlusInfinity;
                                    MaximalChannelValue = g_RealMinusInfinity;
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetMaximalFrequency();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetMaximalFrequency();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                }
                                else
                                {
                                    TotalPixels = m_OutActiveImageZone.GetArea();
                                    MinimalChannelValue = MaximalChannelValue = pTemporalFusionPixel->GetMaximalFrequency();
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetMaximalFrequency();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetMaximalFrequency();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                }
                            }
                            break;
                            case eChannelMean:
                            {
                                if (IsMasking())
                                {
                                    const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    MinimalChannelValue = g_RealPlusInfinity;
                                    MaximalChannelValue = g_RealMinusInfinity;
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetMean();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetMean();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                }
                                else
                                {
                                    TotalPixels = m_OutActiveImageZone.GetArea();
                                    MinimalChannelValue = MaximalChannelValue = pTemporalFusionPixel->GetMean();
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetMean();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetMean();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                }
                            }
                            break;
                            case eChannelRange:
                            {
                                if (IsMasking())
                                {
                                    const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    MinimalChannelValue = g_RealPlusInfinity;
                                    MaximalChannelValue = g_RealMinusInfinity;
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetRange();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetRange();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                }
                                else
                                {
                                    TotalPixels = m_OutActiveImageZone.GetArea();
                                    MinimalChannelValue = MaximalChannelValue = pTemporalFusionPixel->GetRange();
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetRange();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetRange();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                }
                            }
                            break;
                            case eChannelStandardDeviation:
                            {
                                if (IsMasking())
                                {
                                    const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    MinimalChannelValue = g_RealPlusInfinity;
                                    MaximalChannelValue = g_RealMinusInfinity;
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetStandardDeviation();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetStandardDeviation();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                }
                                else
                                {
                                    TotalPixels = m_OutActiveImageZone.GetArea();
                                    MinimalChannelValue = MaximalChannelValue = pTemporalFusionPixel->GetStandardDeviation();
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetStandardDeviation();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetStandardDeviation();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                }
                            }
                            break;
                            case eChannelDispersion:
                            {
                                if (IsMasking())
                                {
                                    const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    MinimalChannelValue = g_RealPlusInfinity;
                                    MaximalChannelValue = g_RealMinusInfinity;
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetDispersion();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetDispersion();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                }
                                else
                                {
                                    TotalPixels = m_OutActiveImageZone.GetArea();
                                    MinimalChannelValue = MaximalChannelValue = pTemporalFusionPixel->GetDispersion();
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetDispersion();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetDispersion();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                }
                            }
                            break;
                            case eChannelSignalToNoiseRatio:
                            {
                                if (IsMasking())
                                {
                                    const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    MinimalChannelValue = g_RealPlusInfinity;
                                    MaximalChannelValue = g_RealMinusInfinity;
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    if (IsPositive(pTemporalFusionPixel->GetRange() > 1))
                                                    {
                                                        const Real Value = pTemporalFusionPixel->GetSignalToNoiseRatio();
                                                        if (Value > MaximalChannelValue)
                                                        {
                                                            MaximalChannelValue = Value;
                                                        }
                                                        if (Value < MinimalChannelValue)
                                                        {
                                                            MinimalChannelValue = Value;
                                                        }
                                                        ChannelAccumulator += Value;
                                                        ChannelSquareAccumulator += Value * Value;
                                                        *pChannelPixel = Value;
                                                        ++TotalPixels;
                                                    }
                                                    else
                                                    {
                                                        *pChannelPixel = g_RealNaN;
                                                        CheckForNaN = true;
                                                    }
                                                }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    if (IsPositive(pTemporalFusionPixel->GetRange() > 1))
                                                    {
                                                        const Real Value = pTemporalFusionPixel->GetSignalToNoiseRatio();
                                                        if (Value > MaximalChannelValue)
                                                        {
                                                            MaximalChannelValue = Value;
                                                        }
                                                        if (Value < MinimalChannelValue)
                                                        {
                                                            MinimalChannelValue = Value;
                                                        }
                                                        ChannelAccumulator += Value;
                                                        ChannelSquareAccumulator += Value * Value;
                                                        *pChannelPixel = Value;
                                                        ++TotalPixels;
                                                    }
                                                    else
                                                    {
                                                        *pChannelPixel = g_RealNaN;
                                                        CheckForNaN = true;
                                                    }
                                                }
                                    }
                                }
                                else
                                {
                                    TotalPixels = m_OutActiveImageZone.GetArea();
                                    MinimalChannelValue = MaximalChannelValue = pTemporalFusionPixel->GetSignalToNoiseRatio();
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                                if (IsPositive(pTemporalFusionPixel->GetRange() > 1))
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetSignalToNoiseRatio();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    else if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel++ = Value;
                                                }
                                                else
                                                {
                                                    *pChannelPixel = g_RealNaN;
                                                    CheckForNaN = true;
                                                }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                                if (IsPositive(pTemporalFusionPixel->GetRange() > 1))
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetSignalToNoiseRatio();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    else if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel++ = Value;
                                                }
                                                else
                                                {
                                                    *pChannelPixel = g_RealNaN;
                                                    CheckForNaN = true;
                                                }
                                    }
                                }
                            }
                            break;
                            case eChannelFusion:
                            {
                                if (IsMasking())
                                {
                                    const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    MinimalChannelValue = g_RealPlusInfinity;
                                    MaximalChannelValue = g_RealMinusInfinity;
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetFusion();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetFusion();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                }
                                else
                                {
                                    TotalPixels = m_OutActiveImageZone.GetArea();
                                    MinimalChannelValue = MaximalChannelValue = pTemporalFusionPixel->GetFusion();
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetFusion();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetFusion();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                }
                            }
                            break;
                            case eChannelMaximal:
                            {
                                if (IsMasking())
                                {
                                    const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    MinimalChannelValue = g_RealPlusInfinity;
                                    MaximalChannelValue = g_RealMinusInfinity;
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetMaximal();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetMaximal();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                }
                                else
                                {
                                    TotalPixels = m_OutActiveImageZone.GetArea();
                                    MinimalChannelValue = MaximalChannelValue = pTemporalFusionPixel->GetMaximal();
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetMaximal();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetMaximal();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                }
                            }
                            break;
                            case eChannelMinimal:
                            {
                                if (IsMasking())
                                {
                                    const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    MinimalChannelValue = g_RealPlusInfinity;
                                    MaximalChannelValue = g_RealMinusInfinity;
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetMinimal();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetMinimal();
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                }
                                else
                                {
                                    TotalPixels = m_OutActiveImageZone.GetArea();
                                    MinimalChannelValue = MaximalChannelValue = pTemporalFusionPixel->GetMinimal();
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetMinimal();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetMinimal();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                }
                            }
                            break;
                            case eChannelGlobalMaxima:
                            {
                                int* pGlobalMaxima = new int[0x1 << (sizeof(Byte) << 3)];
                                if (IsMasking())
                                {
                                    const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    MinimalChannelValue = g_RealPlusInfinity;
                                    MaximalChannelValue = g_RealMinusInfinity;
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetGlobalMaxima(pGlobalMaxima);
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    const Real Value = pTemporalFusionPixel->GetGlobalMaxima(pGlobalMaxima);
                                                    if (Value > MaximalChannelValue)
                                                    {
                                                        MaximalChannelValue = Value;
                                                    }
                                                    if (Value < MinimalChannelValue)
                                                    {
                                                        MinimalChannelValue = Value;
                                                    }
                                                    ChannelAccumulator += Value;
                                                    ChannelSquareAccumulator += Value * Value;
                                                    *pChannelPixel = Value;
                                                    ++TotalPixels;
                                                }
                                    }
                                }
                                else
                                {
                                    TotalPixels = m_OutActiveImageZone.GetArea();
                                    MinimalChannelValue = MaximalChannelValue = pTemporalFusionPixel->GetGlobalMaxima(pGlobalMaxima);
                                    if (Offset)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pChannelPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetGlobalMaxima(pGlobalMaxima);
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                            {
                                                const Real Value = pTemporalFusionPixel->GetGlobalMaxima(pGlobalMaxima);
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                            }
                                    }
                                }
                                delete[] pGlobalMaxima;
                                pGlobalMaxima = nullptr;
                            }
                            break;
                            default:
                                return false;
                        }
                        ChannelMean = ChannelAccumulator / Real(TotalPixels);
                        const Real Range = MaximalChannelValue - MinimalChannelValue;
                        if (IsPositive(Range))
                        {
                            ChannelStandardDeviation = std::sqrt((ChannelSquareAccumulator / Real(TotalPixels)) - (ChannelMean * ChannelMean));
                            const DiscreteRGBPixel* pLUT = pColorMap->GetLUT();
                            const Real NormalizationFactor = Real(pColorMap->GetLUTScaleFactor()) / Range;
                            const Real* pChannelPixel = ChannelImage.GetReadOnlyBufferAt(X0, Y0);
                            DiscreteRGBPixel* pDiscreteRGBPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                            if (IsMasking())
                            {
                                const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                if (Offset)
                                {
                                    if (CheckForNaN)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pChannelPixel += Offset, pDiscreteRGBPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pDiscreteRGBPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    if (*pChannelPixel != *pChannelPixel)
                                                    {
                                                        pDiscreteRGBPixel->Set(0, 0, 0);
                                                    }
                                                    else
                                                    {
                                                        *pDiscreteRGBPixel = pLUT[int(NormalizationFactor * (*pChannelPixel - MinimalChannelValue) + Real(0.5))];
                                                    }
                                                }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pChannelPixel += Offset, pDiscreteRGBPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pDiscreteRGBPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    *pDiscreteRGBPixel = pLUT[int(NormalizationFactor * (*pChannelPixel - MinimalChannelValue) + Real(0.5))];
                                                }
                                    }
                                }
                                else
                                {
                                    if (CheckForNaN)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pDiscreteRGBPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    if (*pChannelPixel != *pChannelPixel)
                                                    {
                                                        pDiscreteRGBPixel->Set(0, 0, 0);
                                                    }
                                                    else
                                                    {
                                                        *pDiscreteRGBPixel = pLUT[int(NormalizationFactor * (*pChannelPixel - MinimalChannelValue) + Real(0.5))];
                                                    }
                                                }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pDiscreteRGBPixel, ++pChannelPixel)
                                                if (*pOutActiveMaskPixel++)
                                                {
                                                    *pDiscreteRGBPixel = pLUT[int(NormalizationFactor * (*pChannelPixel - MinimalChannelValue) + Real(0.5))];
                                                }
                                    }
                                }
                            }
                            else
                            {
                                if (Offset)
                                {
                                    if (CheckForNaN)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pChannelPixel += Offset, pDiscreteRGBPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pDiscreteRGBPixel)
                                            {
                                                if (*pChannelPixel != *pChannelPixel)
                                                {
                                                    pDiscreteRGBPixel->Set(0, 0, 0);
                                                }
                                                else
                                                {
                                                    *pDiscreteRGBPixel = pLUT[int(NormalizationFactor * (*pChannelPixel++ - MinimalChannelValue) + Real(0.5))];
                                                }
                                            }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pChannelPixel += Offset, pDiscreteRGBPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X)
                                            {
                                                *pDiscreteRGBPixel++ = pLUT[int(NormalizationFactor * (*pChannelPixel++ - MinimalChannelValue) + Real(0.5))];
                                            }
                                    }
                                }
                                else
                                {
                                    if (CheckForNaN)
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pDiscreteRGBPixel)
                                            {
                                                if (*pChannelPixel != *pChannelPixel)
                                                {
                                                    pDiscreteRGBPixel->Set(0, 0, 0);
                                                }
                                                else
                                                {
                                                    *pDiscreteRGBPixel = pLUT[int(NormalizationFactor * (*pChannelPixel++ - MinimalChannelValue) + Real(0.5))];
                                                }
                                            }
                                    }
                                    else
                                    {
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                            for (int X = X0 ; X <= X1 ; ++X)
                                            {
                                                *pDiscreteRGBPixel++ = pLUT[int(NormalizationFactor * (*pChannelPixel++ - MinimalChannelValue) + Real(0.5))];
                                            }
                                    }
                                }
                            }
                            return true;
                        }
                    }
                    return false;
                }

                bool CTemporalFusion::Execute(const int Trial)
                {
                    _RVL_TIME_LOGGER_BEGIN_(0, "CTemporalFusion::Execute");
                    bool Result = false;
                    if (IsEnabled())
                    {
                        if (IsMasking() && (!UpdateActiveMaskImage(false)))
                        {
                            _RVL_TIME_LOGGER_VERBOSE_END_(0, "CTemporalFusion::Execute");
                            return false;
                        }
                        switch (m_SamplingMode)
                        {
                            case eDirect:
                                LoadImage(m_pInImage);
                                break;
                            case eIndirect:
                                m_PendingSampleImages.push_back(m_pInImage->Clone());
                                break;
                        }
                        if ((m_TotalLoadedSamples + int(m_PendingSampleImages.size())) == m_TotalSamplesPerFusion)
                        {
                            if (m_PendingSampleImages.size())
                            {
                                for (auto& m_PendingSampleImage : m_PendingSampleImages)
                                {
                                    LoadImage(m_PendingSampleImage);
                                    delete m_PendingSampleImage;
                                }
                                m_PendingSampleImages.clear();
                            }
                            if (!UpdateActiveImageZone(false))
                            {
                                _RVL_TIME_LOGGER_VERBOSE_END_(0, "CTemporalFusion::Execute");
                                return false;
                            }
                            int* pGlobalMaxima = new int[0x1 << (sizeof(Byte) << 3)];
                            const int X0 = m_OutActiveImageZone.GetX0();
                            const int Y0 = m_OutActiveImageZone.GetY0();
                            const int X1 = m_OutActiveImageZone.GetX1();
                            const int Y1 = m_OutActiveImageZone.GetY1();
                            const int Offset = m_TemporalFusionImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                            CTemporalFusionPixel* pTemporalFusionPixel = m_TemporalFusionImage.GetWritableBufferAt(X0, Y0);
                            Real* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y0);
                            m_OutImage.Clear();
                            if (IsMasking())
                            {
                                const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                switch (m_FusionMethod)
                                {
                                    case eMean:
                                        if (Offset)
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pOutPixel += Offset)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pOutPixel)
                                                    if (*pOutActiveMaskPixel++)
                                                    {
                                                        *pOutPixel = pTemporalFusionPixel->MeanFusion();
                                                    }
                                        }
                                        else
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pOutPixel)
                                                    if (*pOutActiveMaskPixel++)
                                                    {
                                                        *pOutPixel = pTemporalFusionPixel->MeanFusion();
                                                    }
                                        }
                                        break;
                                    case eContinuousEpanechnikov:
                                        if (Offset)
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pOutPixel += Offset)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pOutPixel)
                                                    if (*pOutActiveMaskPixel++)
                                                    {
                                                        *pOutPixel = pTemporalFusionPixel->ContinuousEpanechnikovFusion(pGlobalMaxima, m_RangePrecision);
                                                    }
                                        }
                                        else
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pOutPixel)
                                                    if (*pOutActiveMaskPixel++)
                                                    {
                                                        *pOutPixel = pTemporalFusionPixel->ContinuousEpanechnikovFusion(pGlobalMaxima, m_RangePrecision);
                                                    }
                                        }
                                        break;
                                    case eContinuousGaussian:
                                        if (Offset)
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pOutPixel += Offset)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pOutPixel)
                                                    if (*pOutActiveMaskPixel++)
                                                    {
                                                        pTemporalFusionPixel->ContinuousGaussianFusion(pGlobalMaxima, m_RangePrecision);
                                                    }
                                        }
                                        else
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pOutPixel)
                                                    if (*pOutActiveMaskPixel++)
                                                    {
                                                        *pOutPixel = pTemporalFusionPixel->ContinuousGaussianFusion(pGlobalMaxima, m_RangePrecision);
                                                    }
                                        }
                                        break;
                                    case eDiscreteEpanechnikov:
                                        if (Offset)
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pOutPixel += Offset)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pOutPixel)
                                                    if (*pOutActiveMaskPixel++)
                                                    {
                                                        *pOutPixel = pTemporalFusionPixel->DiscreteEpanechnikovFusion(pGlobalMaxima);
                                                    }
                                        }
                                        else
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pOutPixel)
                                                    if (*pOutActiveMaskPixel++)
                                                    {
                                                        *pOutPixel = pTemporalFusionPixel->DiscreteEpanechnikovFusion(pGlobalMaxima);
                                                    }
                                        }
                                        break;
                                    case eDiscreteGaussian:
                                        if (Offset)
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pOutPixel += Offset)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pOutPixel)
                                                    if (*pOutActiveMaskPixel++)
                                                    {
                                                        *pOutPixel = pTemporalFusionPixel->DiscreteGaussianFusion(pGlobalMaxima);
                                                    }
                                        }
                                        else
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pOutPixel)
                                                    if (*pOutActiveMaskPixel++)
                                                    {
                                                        *pOutPixel = pTemporalFusionPixel->DiscreteGaussianFusion(pGlobalMaxima);
                                                    }
                                        }
                                        break;
                                }
                            }
                            else
                                switch (m_FusionMethod)
                                {
                                    case eMean:
                                        if (Offset)
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pOutPixel += Offset)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                                {
                                                    *pOutPixel++ = pTemporalFusionPixel->MeanFusion();
                                                }
                                        }
                                        else
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                                {
                                                    *pOutPixel++ = pTemporalFusionPixel->MeanFusion();
                                                }
                                        }
                                        break;
                                    case eContinuousEpanechnikov:
                                        if (Offset)
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pOutPixel += Offset)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                                {
                                                    *pOutPixel++ = pTemporalFusionPixel->ContinuousEpanechnikovFusion(pGlobalMaxima, m_RangePrecision);
                                                }
                                        }
                                        else
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                                {
                                                    *pOutPixel++ = pTemporalFusionPixel->ContinuousEpanechnikovFusion(pGlobalMaxima, m_RangePrecision);
                                                }
                                        }
                                        break;
                                    case eContinuousGaussian:
                                        if (Offset)
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pOutPixel += Offset)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                                {
                                                    *pOutPixel++ = pTemporalFusionPixel->ContinuousGaussianFusion(pGlobalMaxima, m_RangePrecision);
                                                }
                                        }
                                        else
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                                {
                                                    *pOutPixel++ = pTemporalFusionPixel->ContinuousGaussianFusion(pGlobalMaxima, m_RangePrecision);
                                                }
                                        }
                                        break;
                                    case eDiscreteEpanechnikov:
                                        if (Offset)
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pOutPixel += Offset)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                                {
                                                    *pOutPixel++ = pTemporalFusionPixel->DiscreteGaussianFusion(pGlobalMaxima);
                                                }
                                        }
                                        else
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                                {
                                                    *pOutPixel++ = pTemporalFusionPixel->DiscreteGaussianFusion(pGlobalMaxima);
                                                }
                                        }
                                        break;
                                    case eDiscreteGaussian:
                                        if (Offset)
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pOutPixel += Offset)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                                {
                                                    *pOutPixel++ = pTemporalFusionPixel->DiscreteGaussianFusion(pGlobalMaxima);
                                                }
                                        }
                                        else
                                        {
                                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                                {
                                                    *pOutPixel++ = pTemporalFusionPixel->DiscreteGaussianFusion(pGlobalMaxima);
                                                }
                                        }
                                        break;
                                }
                            delete[] pGlobalMaxima;
                            pGlobalMaxima = nullptr;
                            if (m_AutoReset)
                            {
                                Reset();
                            }
                        }
                        Result = true;
                    }
                    _RVL_TIME_LOGGER_VERBOSE_END_(0, "CTemporalFusion::Execute");
                    return Result;
                }

                void CTemporalFusion::Reset()
                {
                    Clear();
                    if (m_TotalLoadedSamples)
                    {
                        const int X0 = m_OutActiveImageZone.GetX0();
                        const int Y0 = m_OutActiveImageZone.GetY0();
                        const int X1 = m_OutActiveImageZone.GetX1();
                        const int Y1 = m_OutActiveImageZone.GetY1();
                        const int Offset = m_TemporalFusionImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                        CTemporalFusionPixel* pTemporalFusionPixel = m_TemporalFusionImage.GetWritableBufferAt(X0, Y0);
                        if (Offset)
                        {
                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset)
                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                {
                                    pTemporalFusionPixel->Reset();
                                }
                        }
                        else
                        {
                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                {
                                    pTemporalFusionPixel->Reset();
                                }
                        }
                        m_TotalLoadedSamples = 0;
                    }
                }

                void CTemporalFusion::LoadImage(const TImage<Byte>* pSampleImage)
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_pInImage->GetWidth() - m_OutActiveImageZone.GetWidth();
                    const Byte* pSamplePixel = pSampleImage->GetReadOnlyBufferAt(X0, Y0);
                    CTemporalFusionPixel* pTemporalFusionPixel = m_TemporalFusionImage.GetWritableBufferAt(X0, Y0);
                    if (m_TotalLoadedSamples++)
                    {
                        if (IsMasking())
                        {
                            const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                            if (Offset)
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pSamplePixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pSamplePixel)
                                        if (*pOutActiveMaskPixel++)
                                        {
                                            pTemporalFusionPixel->AddSuccessiveSample(*pSamplePixel);
                                        }
                            }
                            else
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pSamplePixel)
                                        if (*pOutActiveMaskPixel++)
                                        {
                                            pTemporalFusionPixel->AddSuccessiveSample(*pSamplePixel);
                                        }
                            }
                        }
                        else
                        {
                            if (Offset)
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pSamplePixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                    {
                                        pTemporalFusionPixel->AddSuccessiveSample(*pSamplePixel++);
                                    }
                            }
                            else
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                    {
                                        pTemporalFusionPixel->AddSuccessiveSample(*pSamplePixel++);
                                    }
                            }
                        }
                    }
                    else
                    {
                        if (IsMasking())
                        {
                            const bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                            if (Offset)
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutActiveMaskPixel += Offset, pTemporalFusionPixel += Offset, pSamplePixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pSamplePixel)
                                        if (*pOutActiveMaskPixel++)
                                        {
                                            pTemporalFusionPixel->AddIntialSample(*pSamplePixel, m_TotalSamplesPerFusion);
                                        }
                            }
                            else
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel, ++pSamplePixel)
                                        if (*pOutActiveMaskPixel++)
                                        {
                                            pTemporalFusionPixel->AddIntialSample(*pSamplePixel, m_TotalSamplesPerFusion);
                                        }
                            }
                        }
                        else
                        {
                            if (Offset)
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pTemporalFusionPixel += Offset, pSamplePixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                    {
                                        pTemporalFusionPixel->AddIntialSample(*pSamplePixel++, m_TotalSamplesPerFusion);
                                    }
                            }
                            else
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pTemporalFusionPixel)
                                    {
                                        pTemporalFusionPixel->AddIntialSample(*pSamplePixel++, m_TotalSamplesPerFusion);
                                    }
                            }
                        }
                    }
                }

                void CTemporalFusion::Clear()
                {
                    if (m_PendingSampleImages.size())
                    {
                        for (auto& m_PendingSampleImage : m_PendingSampleImages)
                        {
                            delete m_PendingSampleImage;
                        }
                        m_PendingSampleImages.clear();
                    }
                }
            }
        }
    }
}
