/*
 * TemporalFusionPixel.cpp
 */

#include "TemporalFusionPixel.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            int CTemporalFusionPixel::GetMaximalImagesPerFusion()
            {
                return (0X1 << (sizeof(Bin) * 8)) - 1;
            }

            CTemporalFusionPixel::CTemporalFusionPixel() :
                m_TotalSamples(0),
                m_pHistogram(nullptr),
                m_Fusion(Real(0)),
                m_Minimal(Byte(0)),
                m_Maximal(Byte(0)),
                m_Accumulator(0),
                m_SquareAccumulator(0)
            {
                m_pHistogram = new Bin[sizeof(Byte) << 8];
                memset(m_pHistogram, 0, sizeof(Bin) * sizeof(Byte) << 8);
            }

            CTemporalFusionPixel::~CTemporalFusionPixel()
            {
                if (m_pHistogram)
                {
                    delete[] m_pHistogram;
                }
            }

            void CTemporalFusionPixel::AddIntialSample(const Byte Sample, const int TotalSamples)
            {
                m_Accumulator = m_Maximal = m_Minimal = Sample;
                m_SquareAccumulator = Sample * Sample;
                ++m_pHistogram[Sample];
                m_TotalSamples = TotalSamples;
            }

            void CTemporalFusionPixel::AddSuccessiveSample(const Byte Sample)
            {
                m_Accumulator += Sample;
                m_SquareAccumulator += Sample * Sample;
                if (Sample > m_Maximal)
                {
                    m_Maximal = Sample;
                }
                else if (Sample < m_Minimal)
                {
                    m_Minimal = Sample;
                }
                ++m_pHistogram[Sample];
            }

            void CTemporalFusionPixel::Reset()
            {
                memset(m_pHistogram + m_Minimal, 0, (m_Maximal - m_Minimal + 1) * sizeof(Bin));
            }

            int CTemporalFusionPixel::GetMaximalFrequency() const
            {
                int MaximalFrequency = m_pHistogram[m_Minimal];
                for (int i = m_Minimal + 1 ; i <= m_Maximal ; ++i)
                    if (m_pHistogram[i] > MaximalFrequency)
                    {
                        MaximalFrequency = m_pHistogram[i];
                    }
                return MaximalFrequency;
            }

            int CTemporalFusionPixel::GetRange() const
            {
                return (m_Maximal - m_Minimal) + 1;
            }

            int CTemporalFusionPixel::GetGlobalMaxima(int* pGlobalMaxima) const
            {
                int MaximalFrequency = m_pHistogram[m_Minimal];
                for (int i = m_Minimal + 1 ; i <= m_Maximal ; ++i)
                    if (m_pHistogram[i] > MaximalFrequency)
                    {
                        MaximalFrequency = m_pHistogram[i];
                    }
                int TotalMaxima = 0;
                for (int i = m_Minimal ; i <= m_Maximal ; ++i)
                    if (m_pHistogram[i] == MaximalFrequency)
                    {
                        pGlobalMaxima[TotalMaxima++] = i;
                    }
                return TotalMaxima;
            }

            int CTemporalFusionPixel::GetMinimal() const
            {
                return m_Minimal;
            }

            int CTemporalFusionPixel::GetMaximal() const
            {
                return m_Maximal;
            }

            int CTemporalFusionPixel::GetAccumulator() const
            {
                return m_Accumulator;
            }

            int CTemporalFusionPixel::GetSquareAccumulator() const
            {
                return m_SquareAccumulator;
            }

            Real CTemporalFusionPixel::GetMean() const
            {
                return Real(m_Accumulator) / Real(m_TotalSamples);
            }

            Real CTemporalFusionPixel::GetStandardDeviation() const
            {
                const Real Mean = Real(m_Accumulator) / Real(m_TotalSamples);
                return sqrt((Real(m_SquareAccumulator) / Real(m_TotalSamples)) - (Mean * Mean));
            }

            Real CTemporalFusionPixel::GetDispersion() const
            {
                return m_Fusion - (Real(m_Accumulator) / Real(m_TotalSamples));
            }

            Real CTemporalFusionPixel::GetSignalToNoiseRatio() const
            {
                const Real Mean = Real(m_Accumulator) / Real(m_TotalSamples);
                const Real StandardDeviation = sqrt((Real(m_SquareAccumulator) / Real(m_TotalSamples)) - (Mean * Mean));
                return Mean / StandardDeviation;
            }

            const CTemporalFusionPixel::Bin* CTemporalFusionPixel::GetReadOnlyHistogram() const
            {
                return m_pHistogram;
            }

            Real CTemporalFusionPixel::GetFusion() const
            {
                return m_Fusion;
            }

            Real CTemporalFusionPixel::MeanFusion()
            {
                m_Fusion = Real(m_Accumulator) / Real(m_TotalSamples);
                return m_Fusion;
            }

            Real CTemporalFusionPixel::DiscreteEpanechnikovFusion(int* pGlobalMaxima)
            {
                if (m_Maximal == m_Minimal)
                {
                    m_Fusion = m_Maximal;
                }
                else
                {
                    const int TotalGlobalMaxima = GetGlobalMaxima(pGlobalMaxima);
                    if (TotalGlobalMaxima)
                    {
                        if (TotalGlobalMaxima > 1)
                        {
                            int SelectedIndex = 0;
                            const Real AdaptiveBandWidth = Mathematics::_1D::CNormalDistribution::DetermineBandWidthByStandardDeviation(GetStandardDeviation(), m_TotalSamples);
                            const Real SquareBandWidthNormalization = Real(1) / (AdaptiveBandWidth * AdaptiveBandWidth);
                            Real MaximalDensity = EstimateNonNormalizedEpanechnikovDensity(pGlobalMaxima[0], AdaptiveBandWidth, SquareBandWidthNormalization);
                            for (int i = 1 ; i < TotalGlobalMaxima ; ++i)
                            {
                                const Real Density = EstimateNonNormalizedEpanechnikovDensity(pGlobalMaxima[i], AdaptiveBandWidth, SquareBandWidthNormalization);
                                if (Density > MaximalDensity)
                                {
                                    MaximalDensity = Density;
                                    SelectedIndex = i;
                                }
                            }
                            m_Fusion = pGlobalMaxima[SelectedIndex];
                        }
                        else
                        {
                            m_Fusion = pGlobalMaxima[0];
                        }
                    }
                    else
                    {
                        m_Fusion = std::round(Real(m_Accumulator) / Real(m_TotalSamples));
                    }
                }
                return m_Fusion;
            }

            Real CTemporalFusionPixel::ContinuousEpanechnikovFusion(int* pGlobalMaxima, const Real RangeEpsilon)
            {
                if (m_Maximal == m_Minimal)
                {
                    m_Fusion = m_Maximal;
                }
                else
                {
                    const int TotalGlobalMaxima = GetGlobalMaxima(pGlobalMaxima);
                    if (TotalGlobalMaxima)
                    {
                        int SelectedIndex = 0;
                        const Real AdaptiveBandWidth = Mathematics::_1D::CNormalDistribution::DetermineBandWidthByStandardDeviation(GetStandardDeviation(), m_TotalSamples);
                        const Real SquareBandWidthNormalization = Real(1) / (AdaptiveBandWidth * AdaptiveBandWidth);
                        if (TotalGlobalMaxima > 1)
                        {
                            Real MaximalDensity = EstimateNonNormalizedEpanechnikovDensity(pGlobalMaxima[0], AdaptiveBandWidth, SquareBandWidthNormalization);
                            for (int i = 1 ; i < TotalGlobalMaxima ; ++i)
                            {
                                const Real Density = EstimateNonNormalizedEpanechnikovDensity(pGlobalMaxima[i], AdaptiveBandWidth, SquareBandWidthNormalization);
                                if (Density > MaximalDensity)
                                {
                                    MaximalDensity = Density;
                                    SelectedIndex = i;
                                }
                            }
                        }
                        Real FusionValueA = std::max(Real(pGlobalMaxima[SelectedIndex]) - Real(0.5), Real(m_Minimal));
                        Real FusionValueB = std::min(Real(pGlobalMaxima[SelectedIndex]) + Real(0.5), Real(m_Maximal));
                        Real DensityA = EstimateNonNormalizedEpanechnikovDensity(FusionValueA, AdaptiveBandWidth, SquareBandWidthNormalization);
                        Real DensityB = EstimateNonNormalizedEpanechnikovDensity(FusionValueB, AdaptiveBandWidth, SquareBandWidthNormalization);
                        do
                            if (DensityB > DensityA)
                            {
                                FusionValueA = (FusionValueA + FusionValueB) * Real(0.5);
                                DensityA = EstimateNonNormalizedEpanechnikovDensity(FusionValueA, AdaptiveBandWidth, SquareBandWidthNormalization);
                            }
                            else
                            {
                                FusionValueB = (FusionValueA + FusionValueB) * Real(0.5);
                                DensityB = EstimateNonNormalizedEpanechnikovDensity(FusionValueB, AdaptiveBandWidth, SquareBandWidthNormalization);
                            }
                        while ((FusionValueB - FusionValueA) > RangeEpsilon);
                        m_Fusion = (DensityB > DensityA) ? FusionValueB : FusionValueA;
                    }
                    else
                    {
                        m_Fusion = Real(m_Accumulator) / Real(m_TotalSamples);
                    }
                }
                return m_Fusion;
            }

            Real CTemporalFusionPixel::DiscreteGaussianFusion(int* pGlobalMaxima)
            {
                if (m_Maximal == m_Minimal)
                {
                    m_Fusion = m_Maximal;
                }
                else
                {
                    const int TotalGlobalMaxima = GetGlobalMaxima(pGlobalMaxima);
                    if (TotalGlobalMaxima)
                    {
                        if (TotalGlobalMaxima > 1)
                        {
                            int SelectedIndex = 0;
                            const Real AdaptiveBandWidth = Mathematics::_1D::CNormalDistribution::DetermineBandWidthByStandardDeviation(GetStandardDeviation(), m_TotalSamples);
                            const Real BandWidthExponentFactor = Mathematics::_1D::CNormalDistribution::DetermineExponentFactor(AdaptiveBandWidth);
                            Real MaximalDensity = EstimateNonNormalizedGaussianDensity(pGlobalMaxima[0], BandWidthExponentFactor);
                            for (int i = 1 ; i < TotalGlobalMaxima ; ++i)
                            {
                                const Real Density = EstimateNonNormalizedGaussianDensity(pGlobalMaxima[i], BandWidthExponentFactor);
                                if (Density > MaximalDensity)
                                {
                                    MaximalDensity = Density;
                                    SelectedIndex = i;
                                }
                            }
                            m_Fusion = pGlobalMaxima[SelectedIndex];
                        }
                        else
                        {
                            m_Fusion = pGlobalMaxima[0];
                        }
                    }
                    else
                    {
                        m_Fusion = std::round(Real(m_Accumulator) / Real(m_TotalSamples));
                    }
                }
                return m_Fusion;
            }

            Real CTemporalFusionPixel::ContinuousGaussianFusion(int* pGlobalMaxima, const Real RangeEpsilon)
            {
                if (m_Maximal == m_Minimal)
                {
                    m_Fusion = m_Maximal;
                }
                else
                {
                    const int TotalGlobalMaxima = GetGlobalMaxima(pGlobalMaxima);
                    if (TotalGlobalMaxima)
                    {
                        int SelectedIndex = 0;
                        const Real AdaptiveBandWidth = Mathematics::_1D::CNormalDistribution::DetermineBandWidthByStandardDeviation(GetStandardDeviation(), m_TotalSamples);
                        const Real BandWidthExponentFactor = Mathematics::_1D::CNormalDistribution::DetermineExponentFactor(AdaptiveBandWidth);
                        if (TotalGlobalMaxima > 1)
                        {
                            Real MaximalDensity = EstimateNonNormalizedGaussianDensity(pGlobalMaxima[0], BandWidthExponentFactor);
                            for (int i = 1 ; i < TotalGlobalMaxima ; ++i)
                            {
                                const Real Density = EstimateNonNormalizedGaussianDensity(pGlobalMaxima[i], BandWidthExponentFactor);
                                if (Density > MaximalDensity)
                                {
                                    MaximalDensity = Density;
                                    SelectedIndex = i;
                                }
                            }
                        }
                        Real FusionValueA = std::max(Real(pGlobalMaxima[SelectedIndex]) - Real(0.5), Real(m_Minimal));
                        Real FusionValueB = std::min(Real(pGlobalMaxima[SelectedIndex]) + Real(0.5), Real(m_Maximal));
                        Real DensityA = EstimateNonNormalizedGaussianDensity(FusionValueA, BandWidthExponentFactor);
                        Real DensityB = EstimateNonNormalizedGaussianDensity(FusionValueB, BandWidthExponentFactor);
                        do
                            if (DensityB > DensityA)
                            {
                                FusionValueA = (FusionValueA + FusionValueB) * Real(0.5);
                                DensityA = EstimateNonNormalizedGaussianDensity(FusionValueA, BandWidthExponentFactor);
                            }
                            else
                            {
                                FusionValueB = (FusionValueA + FusionValueB) * Real(0.5);
                                DensityB = EstimateNonNormalizedGaussianDensity(FusionValueB, BandWidthExponentFactor);
                            }
                        while ((FusionValueB - FusionValueA) > RangeEpsilon);
                        m_Fusion = (DensityB > DensityA) ? FusionValueB : FusionValueA;
                    }
                    else
                    {
                        m_Fusion = Real(m_Accumulator) / Real(m_TotalSamples);
                    }
                }
                return m_Fusion;
            }

            std::string CTemporalFusionPixel::ExportToString(const int X, const int Y) const
            {
                std::ostringstream Stream;
                Stream.precision(g_RealDisplayDigits);
                Stream << "Temporal Fusion Pixel\n";
                Stream << "\tLocation\tX = " << X << "\tY = " << Y;
                Stream << "\tMinimal Value\t" << m_Maximal;
                Stream << "\tMaximal Value\t" << m_Minimal;
                Stream << "\tValue Range\t" << GetRange();
                Stream << "\tMean Value\t" << GetMean();
                Stream << "\tMaximal Frequency\t" << GetMaximalFrequency();
                Stream << "\tStandard Deviation\t" << GetStandardDeviation();
                int GlobalMaxima[256];
                const int TotalGlobalMaxima = GetGlobalMaxima(GlobalMaxima);
                Stream << "\tTotal Global Maxima\t" << TotalGlobalMaxima;
                Stream << "\tMaxima\tIntensity Location";
                for (int i = 0 ; i < TotalGlobalMaxima ; ++i)
                {
                    Stream << "\t" << i << GlobalMaxima[i];
                }
                Stream << "\tFusion\t" << m_Fusion;
                Stream << "\tDispersion\t" << GetDispersion();
                Stream << "\tAbsolute Dispersion\t" << std::abs(GetDispersion());
                Stream << "\tSignal-to-Noise Ratio\t" << GetSignalToNoiseRatio();
                Stream << "\tIntensity\tFrequency";
                for (int i = m_Minimal ; i <= m_Maximal ; ++i)
                {
                    Stream << "\t" << i << "\t" << m_pHistogram[i];
                }
                return Stream.str();
            }

            Real CTemporalFusionPixel::EstimateNonNormalizedEpanechnikovDensity(const Real ValueLocation, const Real AdaptiveBandWidth, const Real SquareBandWidthNormalization)
            {
                const int FinalSupportValue = std::min(int(ValueLocation + AdaptiveBandWidth + Real(0.5)), int(m_Maximal));
                Real Density = Real(0);
                for (int i = std::max(int(ValueLocation - AdaptiveBandWidth - Real(0.5)), int(m_Minimal)) ; i < FinalSupportValue ; ++i)
                    if (m_pHistogram[i])
                    {
                        const Real Delta = std::abs(Real(i) - ValueLocation);
                        if (Delta < AdaptiveBandWidth)
                        {
                            Density += (Real(1) - (Delta * Delta * SquareBandWidthNormalization)) * Real(m_pHistogram[i]);
                        }
                    }
                return Density;
            }

            Real CTemporalFusionPixel::EstimateNonNormalizedGaussianDensity(const Real ValueLocation, const Real BandWidthExponentFactor)
            {
                Real Density = Real(0);
                for (int i = m_Minimal ; i <= m_Maximal ; ++i)
                    if (m_pHistogram[i])
                    {
                        const Real Delta = Real(i) - ValueLocation;
                        Density += exp(Delta * Delta * BandWidthExponentFactor) * Real(m_pHistogram[i]);
                    }
                return Density;
            }
        }
    }
}
