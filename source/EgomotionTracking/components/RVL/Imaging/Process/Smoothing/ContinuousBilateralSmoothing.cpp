/*
 * ContinuousBilateralSmoothing.cpp
 */

#include "ContinuousBilateralSmoothing.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Smoothing
            {
                CContinuousBilateralSmoothing::CContinuousBilateralSmoothing(const TImage<Real>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone) :
                    TImageProcess<Real, Real>(pInImage, pInActiveMaskImage, pInActiveImageZone),
                    m_RangeStandardDeviation(_CONTINUOUS_BILATERAL_SMOOTHING_DEFAULT_RANGE_STANDARD_DEVIATION_),
                    m_ConvolutionGaussianKernel(_CONTINUOUS_BILATERAL_SMOOTHING_DEFAULT_SPATIAL_STANDARD_DEVIATION_)
                {
                    if (IsEnabled())
                    {
                        m_OutImage.Create(m_pInImage->GetSize(), true);
                        UpdateActiveImageZones();
                    }
                }

                CContinuousBilateralSmoothing::~CContinuousBilateralSmoothing()
                    = default;

                bool CContinuousBilateralSmoothing::SetSpatialStandardDeviation(const Real SpatialStandardDeviation)
                {
                    if (m_ConvolutionGaussianKernel.SetStandardDeviation(SpatialStandardDeviation))
                    {
                        return UpdateActiveImageZones();
                    }
                    return false;
                }

                bool CContinuousBilateralSmoothing::SetRangeStandardDeviation(const Real RangeStandardDeviation)
                {
                    if (IsPositive(RangeStandardDeviation))
                    {
                        m_RangeStandardDeviation = RangeStandardDeviation;
                        return true;
                    }
                    return false;
                }

                Real CContinuousBilateralSmoothing::GetSpatialStandardDeviation() const
                {
                    return m_ConvolutionGaussianKernel.GetStandardDeviation();
                }

                Real CContinuousBilateralSmoothing::GetRangeStandardDeviation() const
                {
                    return m_RangeStandardDeviation;
                }

                bool CContinuousBilateralSmoothing::Execute(const int Trial)
                {
                    if (IsEnabled() && ((!m_pInActiveImageZone) || (m_pInActiveImageZone->IsValid() && m_pInImage->IsInside(m_pInActiveImageZone))))
                    {
                        const int KernelDiameter = m_ConvolutionGaussianKernel.GetDiameter();
                        const Real RangeExponentFactor = Real(-1) / (Real(2) * m_RangeStandardDeviation * m_RangeStandardDeviation);
                        const int X0 = m_OutActiveImageZone.GetX0();
                        const int Y0 = m_OutActiveImageZone.GetY0();
                        const int XB = m_OutActiveImageZone.GetX0() - m_ConvolutionGaussianKernel.GetRadius();
                        const int YB = m_OutActiveImageZone.GetY0() - m_ConvolutionGaussianKernel.GetRadius();
                        const int X1 = m_OutActiveImageZone.GetX1();
                        const int Y1 = m_OutActiveImageZone.GetY1();
                        const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                        Real* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y0);
                        const Real* pInCentralPixel = m_pInImage->GetReadOnlyBufferAt(X0, Y0);
                        if (IsMasking())
                        {
                            const bool* pActiveMaskPixel = m_pInActiveMaskImage->GetReadOnlyBufferAt(X0, Y0);
                            for (int Y = Y0, YS = YB ; Y <= Y1 ; ++Y, ++YS, pInCentralPixel += Offset, pOutPixel += Offset)
                                for (int X = X0, XS = XB ; X <= X1 ; ++X, ++XS, ++pInCentralPixel, ++pOutPixel)
                                    if (*pActiveMaskPixel++)
                                    {
                                        const Real* pSpatialKernel = m_ConvolutionGaussianKernel.GetReadOnlyKernel();
                                        const Real* pInNeighborhoodPixel = m_pInImage->GetReadOnlyBufferAt(XS, YS);
                                        const Real CentralPixel = *pInCentralPixel;
                                        Real KernelAccumulator = Real(0), KernelWeightedAccumulator = Real(0);
                                        for (int i = 0 ; i < KernelDiameter ; ++i)
                                            for (int j = 0 ; j < KernelDiameter ; ++j)
                                            {
                                                const Real RangeDistance = *pInNeighborhoodPixel - CentralPixel;
                                                const Real AdaptiveKernel = *pSpatialKernel++ * std::exp(RangeExponentFactor * RangeDistance * RangeDistance);
                                                KernelWeightedAccumulator += AdaptiveKernel * *pInNeighborhoodPixel++;
                                                KernelAccumulator += AdaptiveKernel;
                                            }
                                        *pOutPixel = KernelWeightedAccumulator / KernelAccumulator;
                                    }
                        }
                        else
                            for (int Y = Y0, YS = YB ; Y <= Y1 ; ++Y, ++YS, pInCentralPixel += Offset, pOutPixel += Offset)
                                for (int X = X0, XS = XB ; X <= X1 ; ++X, ++XS)
                                {
                                    const Real* pSpatialKernel = m_ConvolutionGaussianKernel.GetReadOnlyKernel();
                                    const Real* pInNeighborhoodPixel = m_pInImage->GetReadOnlyBufferAt(XS, YS);
                                    const Real CentralPixel = *pInCentralPixel++;
                                    Real KernelAccumulator = Real(0), KernelWeightedAccumulator = Real(0);
                                    for (int i = 0 ; i < KernelDiameter ; ++i)
                                        for (int j = 0 ; j < KernelDiameter ; ++j)
                                        {
                                            const Real RangeDistance = *pInNeighborhoodPixel - CentralPixel;
                                            const Real AdaptiveKernel = *pSpatialKernel++ * std::exp(RangeExponentFactor * RangeDistance * RangeDistance);
                                            KernelWeightedAccumulator += AdaptiveKernel * *pInNeighborhoodPixel++;
                                            KernelAccumulator += AdaptiveKernel;
                                        }
                                    *pOutPixel++ = KernelWeightedAccumulator / KernelAccumulator;
                                }
                        return true;
                    }
                    return false;
                }

                bool CContinuousBilateralSmoothing::UpdateActiveImageZones()
                {
                    if (m_pInActiveImageZone)
                    {
                        if (m_pInActiveImageZone->IsValid())
                        {
                            m_OutActiveImageZone.Set(*m_pInActiveImageZone, m_ConvolutionGaussianKernel.GetRadius());
                            return true;
                        }
                    }
                    else
                    {
                        if (m_pInImage && m_pInImage->IsValid())
                        {
                            const int Radius = m_ConvolutionGaussianKernel.GetRadius();
                            return m_OutActiveImageZone.Set(Radius, Radius, m_pInImage->GetWidth() - 1 - Radius, m_pInImage->GetHeight() - 1 - Radius);
                        }
                    }
                    return false;
                }
            }
        }
    }
}
