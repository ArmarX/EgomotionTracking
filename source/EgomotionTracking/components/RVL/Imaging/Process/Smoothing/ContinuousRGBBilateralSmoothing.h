/*
 * ContinuousRGBBilateralSmoothing.h
 */

#pragma once

#include "../../../Mathematics/_2D/Kernels/ConvolutionGaussianKernel2D.h"
#include "../../PixelTypes.h"
#include "../TImageProcess.hpp"

#define _CONTINUOUS_RGB__BILATERAL_SMOOTHING_DEFAULT_RANGE_STANDARD_DEVIATION_ Real(8)
#define _CONTINUOUS_RGB__BILATERAL_SMOOTHING_DEFAULT_SPATIAL_STANDARD_DEVIATION_ Real(1)

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Smoothing
            {
                class CContinuousRGBBilateralSmoothing : public TImageProcess<ContinuousRGBPixel, ContinuousRGBPixel>
                {
                public:

                    CContinuousRGBBilateralSmoothing(const TImage<ContinuousRGBPixel>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone);
                    ~CContinuousRGBBilateralSmoothing() override;

                    bool SetSpatialStandardDeviation(const Real SpatialStandardDeviation);
                    bool SetRangeStandardDeviation(const Real RangeStandardDeviation);

                    Real GetSpatialStandardDeviation() const;
                    Real GetRangeStandardDeviation() const;

                    bool Execute(const int Trial) override;

                protected:

                    virtual bool UpdateActiveImageZones();

                    Real m_RangeStandardDeviation;
                    Mathematics::_2D::Kernels::CConvolutionGaussianKernel2D m_ConvolutionGaussianKernel;
                };
            }
        }
    }
}

