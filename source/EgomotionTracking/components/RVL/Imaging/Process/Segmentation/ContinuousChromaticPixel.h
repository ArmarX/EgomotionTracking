/*
 * ContinuousChromaticPixel.h
 */

#pragma once

#include "../../PixelTypes.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Segmentation
            {
                class CContinuousChromaticSegment;
                class CContinuousChromaticLink;
                class CContinuousChromaticPixel
                {
                public:

                    CContinuousChromaticPixel();
                    ~CContinuousChromaticPixel();

                    void SetSource(const bool IsEnabled, const int X, const int Y, const ContinuousTristimulusPixel* pSourcePixel);
                    void SetLink(CContinuousChromaticLink* pLink, const int Index);
                    void UpdateConnectivityFlag();

                    Real IntegratePropagationDistance();
                    Real PropagateDistance();

                    CContinuousChromaticLink* GetLink(const int Index);

                    bool IsEnabled() const;
                    bool IsEnabledFullyConnected() const;

                    int GetX() const;
                    int GetY() const;
                    Real GetPropagationDistance() const;
                    const CContinuousChromaticSegment* GetReadOnlySegment() const;
                    const ContinuousTristimulusPixel* GetSourcePixel() const;

                protected:

                    friend class CContinuousChromaticLink;

                    Byte m_StatusBitFlags;
                    int m_X;
                    int m_Y;
                    Real m_PropagationDistance;
                    CContinuousChromaticSegment* m_pSegment;
                    CContinuousChromaticLink* m_Neighborhood[8];
                    const ContinuousTristimulusPixel* m_pSourcePixel;
                };
            }
        }
    }
}

