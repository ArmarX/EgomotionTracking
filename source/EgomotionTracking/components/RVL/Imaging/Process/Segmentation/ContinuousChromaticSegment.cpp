/*
 * ContinuousChromaticSegment.cpp
 */

#include "ContinuousChromaticSegment.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Segmentation
            {
                CContinuousChromaticSegment::CContinuousChromaticSegment()
                    = default;

                CContinuousChromaticSegment::~CContinuousChromaticSegment()
                    = default;
            }
        }
    }
}
