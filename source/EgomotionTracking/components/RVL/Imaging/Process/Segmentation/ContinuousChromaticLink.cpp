/*
 * ContinuousChromaticLink.cpp
 */

#include "ContinuousChromaticLink.h"
#include "ContinuousChromaticPixel.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Segmentation
            {
                bool CContinuousChromaticLink::SortByDistanceAscendant(const CContinuousChromaticLink* plhs, const CContinuousChromaticLink* prhs)
                {
                    return plhs->m_Distance > prhs->m_Distance;
                }

                bool CContinuousChromaticLink::SortByDistanceDescendant(const CContinuousChromaticLink* plhs, const CContinuousChromaticLink* prhs)
                {
                    return plhs->m_Distance < prhs->m_Distance;
                }

                bool CContinuousChromaticLink::SortByPropagationDistanceAscendant(const CContinuousChromaticLink* plhs, const CContinuousChromaticLink* prhs)
                {
                    return plhs->m_PropagationDistance > prhs->m_PropagationDistance;
                }

                bool CContinuousChromaticLink::SortByPropagationDistanceDescendant(const CContinuousChromaticLink* plhs, const CContinuousChromaticLink* prhs)
                {
                    return plhs->m_PropagationDistance < prhs->m_PropagationDistance;
                }

                CContinuousChromaticLink::CContinuousChromaticLink(CContinuousChromaticPixel* pPixelA, CContinuousChromaticPixel* pPixelB) :
                    m_pPixelAlpha(pPixelA),
                    m_pPixelBeta(pPixelB),
                    m_Distance(g_RealPlusInfinity),
                    m_PropagationDistance(g_RealPlusInfinity)
                {
                }

                CContinuousChromaticLink::~CContinuousChromaticLink()
                    = default;

                bool CContinuousChromaticLink::IsEnabled() const
                {
                    return (m_pPixelAlpha->m_StatusBitFlags & 0X1) && (m_pPixelBeta->m_StatusBitFlags & 0X1);
                }

                bool CContinuousChromaticLink::IsRadialDefined() const
                {
                    return (m_pPixelAlpha->m_pSourcePixel->m_Elements.m_A != Real(-1)) && (m_pPixelBeta->m_pSourcePixel->m_Elements.m_A != Real(-1));
                }

                Real CContinuousChromaticLink::UpdateManhatanDistance()
                {
                    m_Distance = std::abs(m_pPixelAlpha->m_pSourcePixel->m_Elements.m_A - m_pPixelBeta->m_pSourcePixel->m_Elements.m_A) + std::abs(m_pPixelAlpha->m_pSourcePixel->m_Elements.m_B - m_pPixelBeta->m_pSourcePixel->m_Elements.m_B) + std::abs(m_pPixelAlpha->m_pSourcePixel->m_Elements.m_C - m_pPixelBeta->m_pSourcePixel->m_Elements.m_C);
                    return m_Distance;
                }

                Real CContinuousChromaticLink::UpdateSquareEuclideanDistance()
                {
                    const Real DA = m_pPixelAlpha->m_pSourcePixel->m_Elements.m_A - m_pPixelBeta->m_pSourcePixel->m_Elements.m_A;
                    const Real DB = m_pPixelAlpha->m_pSourcePixel->m_Elements.m_B - m_pPixelBeta->m_pSourcePixel->m_Elements.m_B;
                    const Real DC = m_pPixelAlpha->m_pSourcePixel->m_Elements.m_C - m_pPixelBeta->m_pSourcePixel->m_Elements.m_C;
                    m_Distance = DA * DA + DB * DB + DC * DC;
                    return m_Distance;
                }

                Real CContinuousChromaticLink::UpdateEuclideanDistance()
                {
                    const Real DA = m_pPixelAlpha->m_pSourcePixel->m_Elements.m_A - m_pPixelBeta->m_pSourcePixel->m_Elements.m_A;
                    const Real DB = m_pPixelAlpha->m_pSourcePixel->m_Elements.m_B - m_pPixelBeta->m_pSourcePixel->m_Elements.m_B;
                    const Real DC = m_pPixelAlpha->m_pSourcePixel->m_Elements.m_C - m_pPixelBeta->m_pSourcePixel->m_Elements.m_C;
                    m_Distance = std::sqrt(DA * DA + DB * DB + DC * DC);
                    return m_Distance;
                }

                Real CContinuousChromaticLink::UpdateMinkowskiDistance(const Real P, const Real IP)
                {
                    m_Distance = std::pow(std::pow(m_pPixelAlpha->m_pSourcePixel->m_Elements.m_A - m_pPixelBeta->m_pSourcePixel->m_Elements.m_A, P) + std::pow(m_pPixelAlpha->m_pSourcePixel->m_Elements.m_B - m_pPixelBeta->m_pSourcePixel->m_Elements.m_B, P) + std::pow(m_pPixelAlpha->m_pSourcePixel->m_Elements.m_C - m_pPixelBeta->m_pSourcePixel->m_Elements.m_C, P), IP);
                    return m_Distance;
                }

                Real CContinuousChromaticLink::UpdateChebyshevDistance()
                {
                    m_Distance = std::max(std::max(std::abs(m_pPixelAlpha->m_pSourcePixel->m_Elements.m_A - m_pPixelBeta->m_pSourcePixel->m_Elements.m_A), std::abs(m_pPixelAlpha->m_pSourcePixel->m_Elements.m_B - m_pPixelBeta->m_pSourcePixel->m_Elements.m_B)), std::abs(m_pPixelAlpha->m_pSourcePixel->m_Elements.m_C - m_pPixelBeta->m_pSourcePixel->m_Elements.m_C));
                    return m_Distance;
                }

                Real CContinuousChromaticLink::UpdatePropagationDistance()
                {
                    m_PropagationDistance = m_pPixelAlpha->m_PropagationDistance + m_pPixelBeta->m_PropagationDistance;
                    return m_PropagationDistance;
                }

                const CContinuousChromaticPixel* CContinuousChromaticLink::GetReadOnlyPixelAlpha() const
                {
                    return m_pPixelAlpha;
                }

                const CContinuousChromaticPixel* CContinuousChromaticLink::GetReadOnlyPixelBeta() const
                {
                    return m_pPixelBeta;
                }

                Real CContinuousChromaticLink::GetDistance() const
                {
                    return m_Distance;
                }

                Real CContinuousChromaticLink::GetPropagationDistance() const
                {
                    return m_PropagationDistance;
                }
            }
        }
    }
}
