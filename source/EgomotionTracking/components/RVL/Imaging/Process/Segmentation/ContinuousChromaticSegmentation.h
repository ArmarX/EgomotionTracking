/*
 * ContinuousChromaticSegmentation.h
 */

#pragma once

#include "../../PixelTypes.h"
#include "../TImageProcess.hpp"
#include "ContinuousChromaticPixel.h"
#include "ContinuousChromaticLink.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Segmentation
            {
                class CContinuousChromaticSegmentation : public TImageProcess<ContinuousRGBPixel, CContinuousChromaticPixel>
                {
                public:

                    enum ChromaticSpace
                    {
                        eRGB, eHSL, eHSI, eHSV, eXYZ, eLAB_D50, eLAB_D55, eLAB_D65, eLAB_D75
                    };

                    enum DistanceMetric
                    {
                        eManhatan, eSquareEuclidean, eEuclidean, eMinkowski, eChebyshev
                    };

                    CContinuousChromaticSegmentation(const TImage<ContinuousRGBPixel>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone);
                    ~CContinuousChromaticSegmentation() override;

                    bool SetChromaticSpace(const ChromaticSpace TargetSpace);
                    bool SetDistanceMetric(const DistanceMetric TargetMetric);
                    bool SetMinkowskiOrder(const Real MinkowskiOrder);

                    ChromaticSpace GetChromaticSpace() const;
                    DistanceMetric GetDistanceMetric() const;
                    Real GetMinkowskiOrder() const;

                    bool Execute(const int Trial) override;

                protected:

                    bool MapColorSpace();
                    void CopyInput();
                    void ConvertInputToHSL();
                    void ConvertInputToHSI();
                    void ConvertInputToHSV();
                    void ConvertInputToXYZ();
                    void ConvertInputToLAB_D50();
                    void ConvertInputToLAB_D55();
                    void ConvertInputToLAB_D65();
                    void ConvertInputToLAB_D75();

                    bool UpdateDistances(Real& MaximalDistance, Real& MinimalDistance, Real& MeanDistance, Real& DistanceStandardDeviation);
                    void PropagateDistances(const int TotalLevels);

                    ChromaticSpace m_ChromaticSpace;
                    DistanceMetric m_DistanceMetric;
                    Real m_MinkowskiOrder;
                    TImage<ContinuousTristimulusPixel> m_ContinuousTristimulusImage;
                    std::list<CContinuousChromaticLink*> m_Links;
                    std::list<CContinuousChromaticPixel*> m_Pixels;
                };
            }
        }
    }
}

