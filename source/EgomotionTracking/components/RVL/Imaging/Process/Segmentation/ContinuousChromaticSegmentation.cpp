/*
 * ContinuousChromaticSegmentation.cpp
 */

#include "ContinuousChromaticSegmentation.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Segmentation
            {
                CContinuousChromaticSegmentation::CContinuousChromaticSegmentation(const TImage<ContinuousRGBPixel>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone) :
                    TImageProcess<ContinuousRGBPixel, CContinuousChromaticPixel>(pInImage, pInActiveMaskImage, pInActiveImageZone),
                    m_ChromaticSpace(eRGB),
                    m_DistanceMetric(eEuclidean),
                    m_MinkowskiOrder(Real(1))
                {
                    if (IsEnabled())
                    {
                        m_OutImage.Create(m_pInImage->GetSize(), true);
                        m_ContinuousTristimulusImage.Create(m_pInImage->GetSize(), true);
                        InitializeActiveImageZone();

                        const int Width = m_OutImage.GetWidth();
                        const int Height = m_OutImage.GetHeight();
                        const ContinuousTristimulusPixel* pSourcePixel = m_ContinuousTristimulusImage.GetWritableBuffer();
                        CContinuousChromaticPixel* pBasePixel = m_OutImage.GetWritableBuffer();
                        const bool* pActiveMaskPixel = IsMasking() ? m_pInActiveMaskImage->GetReadOnlyBuffer() : nullptr;
                        const int IndexLinkageMap[3][3] = { { 5, 6, 7 }, { 4, -1, 0 }, { 3, 2, 1 } };
                        for (int Y = 0 ; Y < Height ; ++Y)
                        {
                            const int YS0 = std::max(Y - 1, 0);
                            const int YS1 = std::min(Y + 1, Height - 1);
                            for (int X = 0 ; X < Width ; ++X, ++pBasePixel)
                            {
                                pBasePixel->SetSource((!pActiveMaskPixel) || (*pActiveMaskPixel++), X, Y, pSourcePixel++);
                                const int XS0 = std::max(X - 1, 0);
                                const int XS1 = std::min(X + 1, Width - 1);
                                for (int YS = YS0 ; YS <= YS1 ; ++YS)
                                {
                                    const int DY = YS - Y;
                                    for (int XS = XS0 ; XS <= XS1 ; ++XS)
                                    {
                                        const int DX = XS - X;
                                        if (DY || DX)
                                        {
                                            const int Index = IndexLinkageMap[DY + 1][DX + 1];
                                            CContinuousChromaticPixel* pExtensionPixel = m_OutImage.GetWritableBufferAt(XS, YS);
                                            if (pBasePixel > pExtensionPixel)
                                            {
                                                CContinuousChromaticLink* pLink = pExtensionPixel->GetLink(Index);
                                                pBasePixel->SetLink(pLink, (Index + 4) % 8);
                                            }
                                            else if (pBasePixel < pExtensionPixel)
                                            {
                                                CContinuousChromaticLink* pLink = new CContinuousChromaticLink(pBasePixel, pExtensionPixel);
                                                pBasePixel->SetLink(pLink, Index);
                                                pExtensionPixel->SetLink(pLink, (Index + 4) % 8);
                                                m_Links.push_back(pLink);
                                            }
                                        }
                                    }
                                }
                                m_Pixels.push_back(pBasePixel);
                            }
                        }
                        for (auto& m_Pixel : m_Pixels)
                        {
                            m_Pixel->UpdateConnectivityFlag();
                        }
                    }
                }

                CContinuousChromaticSegmentation::~CContinuousChromaticSegmentation()
                {
                    for (auto& m_Link : m_Links)
                    {
                        delete m_Link;
                    }
                    m_Links.clear();
                }

                bool CContinuousChromaticSegmentation::SetChromaticSpace(const ChromaticSpace TargetSpace)
                {
                    if ((TargetSpace == eRGB) || (TargetSpace == eHSL) || (TargetSpace == eHSI) || (TargetSpace == eHSV) || (TargetSpace == eXYZ) || (TargetSpace == eLAB_D50) || (TargetSpace == eLAB_D55) || (TargetSpace == eLAB_D65) || (TargetSpace == eLAB_D75))
                    {
                        m_ChromaticSpace = TargetSpace;
                        return true;
                    }
                    return false;
                }

                bool CContinuousChromaticSegmentation::SetDistanceMetric(const DistanceMetric TargetMetric)
                {
                    if ((TargetMetric == eManhatan) || (TargetMetric == eSquareEuclidean) || (TargetMetric == eEuclidean) || (TargetMetric == eMinkowski) || (TargetMetric == eChebyshev))
                    {
                        m_DistanceMetric = TargetMetric;
                        return true;
                    }
                    return false;
                }

                bool CContinuousChromaticSegmentation::SetMinkowskiOrder(const Real MinkowskiOrder)
                {
                    if (IsPositive(MinkowskiOrder))
                    {
                        m_MinkowskiOrder = MinkowskiOrder;
                        return true;
                    }
                    return false;
                }

                CContinuousChromaticSegmentation::ChromaticSpace CContinuousChromaticSegmentation::GetChromaticSpace() const
                {
                    return m_ChromaticSpace;
                }

                CContinuousChromaticSegmentation::DistanceMetric CContinuousChromaticSegmentation::GetDistanceMetric() const
                {
                    return m_DistanceMetric;
                }

                Real CContinuousChromaticSegmentation::GetMinkowskiOrder() const
                {
                    return m_MinkowskiOrder;
                }

                bool CContinuousChromaticSegmentation::Execute(const int Trial)
                {
                    if (IsEnabled() && ((!m_pInActiveImageZone) || (m_pInActiveImageZone->IsValid() && m_pInImage->IsInside(m_pInActiveImageZone))))
                    {
                        if (MapColorSpace())
                        {
                            Real MaximalDistance, MinimalDistance, MeanDistance, DistanceStandardDeviation;
                            if (UpdateDistances(MaximalDistance, MinimalDistance, MeanDistance, DistanceStandardDeviation))
                            {
                                PropagateDistances(1);
                                m_Links.sort(CContinuousChromaticLink::SortByDistanceAscendant);
                                return true;
                            }
                        }
                    }
                    return false;
                }

                bool CContinuousChromaticSegmentation::MapColorSpace()
                {
                    switch (m_ChromaticSpace)
                    {
                        case eRGB:
                            CopyInput();
                            break;
                        case eHSL:
                            ConvertInputToHSL();
                            break;
                        case eHSI:
                            ConvertInputToHSI();
                            break;
                        case eHSV:
                            ConvertInputToHSV();
                            break;
                        case eXYZ:
                            ConvertInputToXYZ();
                            break;
                        case eLAB_D50:
                            ConvertInputToLAB_D50();
                            break;
                        case eLAB_D55:
                            ConvertInputToLAB_D55();
                            break;
                        case eLAB_D65:
                            ConvertInputToLAB_D65();
                            break;
                        case eLAB_D75:
                            ConvertInputToLAB_D65();
                            break;
                        default:
                            return false;
                    }
                    return true;
                }

                void CContinuousChromaticSegmentation::CopyInput()
                {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
                    memcpy(m_ContinuousTristimulusImage.GetWritableBuffer(), m_pInImage->GetReadOnlyBuffer(), m_ContinuousTristimulusImage.GetBufferSize());
#pragma GCC diagnostic pop
                }

                void CContinuousChromaticSegmentation::ConvertInputToHSL()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    const ContinuousRGBPixel* pInPixel = m_pInImage->GetReadOnlyBufferAt(X0, Y0);
                    ContinuousTristimulusPixel* pOutPixel = m_ContinuousTristimulusImage.GetWritableBufferAt(X0, Y0);
                    if (IsMasking())
                    {
                        const bool* pActiveMaskPixel = m_pInActiveMaskImage->GetReadOnlyBufferAt(X0, Y0);
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset, pActiveMaskPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pInPixel, ++pOutPixel)
                                if (*pActiveMaskPixel++)
                                {
                                    pOutPixel->LoadHSL(pInPixel);
                                }
                    }
                    else
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel)
                            {
                                pOutPixel->LoadHSL(pInPixel++);
                            }
                }

                void CContinuousChromaticSegmentation::ConvertInputToHSI()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    const ContinuousRGBPixel* pInPixel = m_pInImage->GetReadOnlyBufferAt(X0, Y0);
                    ContinuousTristimulusPixel* pOutPixel = m_ContinuousTristimulusImage.GetWritableBufferAt(X0, Y0);
                    if (IsMasking())
                    {
                        const bool* pActiveMaskPixel = m_pInActiveMaskImage->GetReadOnlyBufferAt(X0, Y0);
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset, pActiveMaskPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pInPixel, ++pOutPixel)
                                if (*pActiveMaskPixel++)
                                {
                                    pOutPixel->LoadHSI(pInPixel);
                                }
                    }
                    else
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel)
                            {
                                pOutPixel->LoadHSI(pInPixel++);
                            }
                }

                void CContinuousChromaticSegmentation::ConvertInputToHSV()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    const ContinuousRGBPixel* pInPixel = m_pInImage->GetReadOnlyBufferAt(X0, Y0);
                    ContinuousTristimulusPixel* pOutPixel = m_ContinuousTristimulusImage.GetWritableBufferAt(X0, Y0);
                    if (IsMasking())
                    {
                        const bool* pActiveMaskPixel = m_pInActiveMaskImage->GetReadOnlyBufferAt(X0, Y0);
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset, pActiveMaskPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pInPixel, ++pOutPixel)
                                if (*pActiveMaskPixel++)
                                {
                                    pOutPixel->LoadHSV(pInPixel);
                                }
                    }
                    else
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel)
                            {
                                pOutPixel->LoadHSV(pInPixel++);
                            }
                }

                void CContinuousChromaticSegmentation::ConvertInputToXYZ()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    const ContinuousRGBPixel* pInPixel = m_pInImage->GetReadOnlyBufferAt(X0, Y0);
                    ContinuousTristimulusPixel* pOutPixel = m_ContinuousTristimulusImage.GetWritableBufferAt(X0, Y0);
                    if (IsMasking())
                    {
                        const bool* pActiveMaskPixel = m_pInActiveMaskImage->GetReadOnlyBufferAt(X0, Y0);
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset, pActiveMaskPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pInPixel, ++pOutPixel)
                                if (*pActiveMaskPixel++)
                                {
                                    pOutPixel->LoadXYZ(pInPixel);
                                }
                    }
                    else
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel)
                            {
                                pOutPixel->LoadXYZ(pInPixel++);
                            }
                }

                void CContinuousChromaticSegmentation::ConvertInputToLAB_D50()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    const ContinuousRGBPixel* pInPixel = m_pInImage->GetReadOnlyBufferAt(X0, Y0);
                    ContinuousTristimulusPixel* pOutPixel = m_ContinuousTristimulusImage.GetWritableBufferAt(X0, Y0);
                    if (IsMasking())
                    {
                        const bool* pActiveMaskPixel = m_pInActiveMaskImage->GetReadOnlyBufferAt(X0, Y0);
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset, pActiveMaskPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pInPixel, ++pOutPixel)
                                if (*pActiveMaskPixel++)
                                {
                                    pOutPixel->LoadLAB_D50(pInPixel);
                                }
                    }
                    else
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel)
                            {
                                pOutPixel->LoadLAB_D50(pInPixel++);
                            }
                }

                void CContinuousChromaticSegmentation::ConvertInputToLAB_D55()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    const ContinuousRGBPixel* pInPixel = m_pInImage->GetReadOnlyBufferAt(X0, Y0);
                    ContinuousTristimulusPixel* pOutPixel = m_ContinuousTristimulusImage.GetWritableBufferAt(X0, Y0);
                    if (IsMasking())
                    {
                        const bool* pActiveMaskPixel = m_pInActiveMaskImage->GetReadOnlyBufferAt(X0, Y0);
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset, pActiveMaskPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pInPixel, ++pOutPixel)
                                if (*pActiveMaskPixel++)
                                {
                                    pOutPixel->LoadLAB_D55(pInPixel);
                                }
                    }
                    else
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel)
                            {
                                pOutPixel->LoadLAB_D55(pInPixel++);
                            }
                }

                void CContinuousChromaticSegmentation::ConvertInputToLAB_D65()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    const ContinuousRGBPixel* pInPixel = m_pInImage->GetReadOnlyBufferAt(X0, Y0);
                    ContinuousTristimulusPixel* pOutPixel = m_ContinuousTristimulusImage.GetWritableBufferAt(X0, Y0);
                    if (IsMasking())
                    {
                        const bool* pActiveMaskPixel = m_pInActiveMaskImage->GetReadOnlyBufferAt(X0, Y0);
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset, pActiveMaskPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pInPixel, ++pOutPixel)
                                if (*pActiveMaskPixel++)
                                {
                                    pOutPixel->LoadLAB_D65(pInPixel);
                                }
                    }
                    else
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel)
                            {
                                pOutPixel->LoadLAB_D65(pInPixel++);
                            }
                }

                void CContinuousChromaticSegmentation::ConvertInputToLAB_D75()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    const ContinuousRGBPixel* pInPixel = m_pInImage->GetReadOnlyBufferAt(X0, Y0);
                    ContinuousTristimulusPixel* pOutPixel = m_ContinuousTristimulusImage.GetWritableBufferAt(X0, Y0);
                    if (IsMasking())
                    {
                        const bool* pActiveMaskPixel = m_pInActiveMaskImage->GetReadOnlyBufferAt(X0, Y0);
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset, pActiveMaskPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pInPixel, ++pOutPixel)
                                if (*pActiveMaskPixel++)
                                {
                                    pOutPixel->LoadLAB_D75(pInPixel);
                                }
                    }
                    else
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel)
                            {
                                pOutPixel->LoadLAB_D75(pInPixel++);
                            }
                }

                bool CContinuousChromaticSegmentation::UpdateDistances(Real& MaximalDistance, Real& MinimalDistance, Real& MeanDistance, Real& DistanceStandardDeviation)
                {
                    int TotalActiveLinks = 0;
                    Real DistanceAccumulator = Real(0), DistanceSquareAccumulator = Real(0);
                    MaximalDistance = g_RealMinusInfinity, MinimalDistance = g_RealPlusInfinity;
                    if ((m_ChromaticSpace == eHSL) || (m_ChromaticSpace == eHSI) || (m_ChromaticSpace == eHSV))
                        switch (m_DistanceMetric)
                        {
                            case eManhatan:
                                for (auto& m_Link : m_Links)
                                    if (m_Link->IsEnabled() && m_Link->IsRadialDefined())
                                    {
                                        const Real Distance = m_Link->UpdateManhatanDistance();
                                        if (Distance > MaximalDistance)
                                        {
                                            MaximalDistance = Distance;
                                        }
                                        if (Distance < MinimalDistance)
                                        {
                                            MinimalDistance = Distance;
                                        }
                                        DistanceAccumulator += Distance;
                                        DistanceSquareAccumulator += Distance * Distance;
                                        ++TotalActiveLinks;
                                    }
                                break;
                            case eSquareEuclidean:
                                for (auto& m_Link : m_Links)
                                    if (m_Link->IsEnabled() && m_Link->IsRadialDefined())
                                    {
                                        const Real Distance = m_Link->UpdateSquareEuclideanDistance();
                                        if (Distance > MaximalDistance)
                                        {
                                            MaximalDistance = Distance;
                                        }
                                        if (Distance < MinimalDistance)
                                        {
                                            MinimalDistance = Distance;
                                        }
                                        DistanceAccumulator += Distance;
                                        DistanceSquareAccumulator += Distance * Distance;
                                        ++TotalActiveLinks;
                                    }
                                break;
                            case eEuclidean:
                                for (auto& m_Link : m_Links)
                                    if (m_Link->IsEnabled() && m_Link->IsRadialDefined())
                                    {
                                        const Real Distance = m_Link->UpdateEuclideanDistance();
                                        if (Distance > MaximalDistance)
                                        {
                                            MaximalDistance = Distance;
                                        }
                                        if (Distance < MinimalDistance)
                                        {
                                            MinimalDistance = Distance;
                                        }
                                        DistanceAccumulator += Distance;
                                        DistanceSquareAccumulator += Distance * Distance;
                                        ++TotalActiveLinks;
                                    }
                                break;
                            case eMinkowski:
                            {
                                const Real InverseMinkowskiOrder = Real(1) / m_MinkowskiOrder;
                                for (auto& m_Link : m_Links)
                                    if (m_Link->IsEnabled() && m_Link->IsRadialDefined())
                                    {
                                        const Real Distance = m_Link->UpdateMinkowskiDistance(m_MinkowskiOrder, InverseMinkowskiOrder);
                                        if (Distance > MaximalDistance)
                                        {
                                            MaximalDistance = Distance;
                                        }
                                        if (Distance < MinimalDistance)
                                        {
                                            MinimalDistance = Distance;
                                        }
                                        DistanceAccumulator += Distance;
                                        DistanceSquareAccumulator += Distance * Distance;
                                        ++TotalActiveLinks;
                                    }
                            }
                            break;
                            case eChebyshev:
                                for (auto& m_Link : m_Links)
                                    if (m_Link->IsEnabled() && m_Link->IsRadialDefined())
                                    {
                                        const Real Distance = m_Link->UpdateChebyshevDistance();
                                        if (Distance > MaximalDistance)
                                        {
                                            MaximalDistance = Distance;
                                        }
                                        if (Distance < MinimalDistance)
                                        {
                                            MinimalDistance = Distance;
                                        }
                                        DistanceAccumulator += Distance;
                                        DistanceSquareAccumulator += Distance * Distance;
                                        ++TotalActiveLinks;
                                    }
                                break;
                            default:
                                return false;
                        }
                    else
                        switch (m_DistanceMetric)
                        {
                            case eManhatan:
                                for (auto& m_Link : m_Links)
                                    if (m_Link->IsEnabled())
                                    {
                                        const Real Distance = m_Link->UpdateManhatanDistance();
                                        if (Distance > MaximalDistance)
                                        {
                                            MaximalDistance = Distance;
                                        }
                                        if (Distance < MinimalDistance)
                                        {
                                            MinimalDistance = Distance;
                                        }
                                        DistanceAccumulator += Distance;
                                        DistanceSquareAccumulator += Distance * Distance;
                                        ++TotalActiveLinks;
                                    }
                                break;
                            case eSquareEuclidean:
                                for (auto& m_Link : m_Links)
                                    if (m_Link->IsEnabled())
                                    {
                                        const Real Distance = m_Link->UpdateSquareEuclideanDistance();
                                        if (Distance > MaximalDistance)
                                        {
                                            MaximalDistance = Distance;
                                        }
                                        if (Distance < MinimalDistance)
                                        {
                                            MinimalDistance = Distance;
                                        }
                                        DistanceAccumulator += Distance;
                                        DistanceSquareAccumulator += Distance * Distance;
                                        ++TotalActiveLinks;
                                    }
                                break;
                            case eEuclidean:
                                for (auto& m_Link : m_Links)
                                    if (m_Link->IsEnabled())
                                    {
                                        const Real Distance = m_Link->UpdateEuclideanDistance();
                                        if (Distance > MaximalDistance)
                                        {
                                            MaximalDistance = Distance;
                                        }
                                        if (Distance < MinimalDistance)
                                        {
                                            MinimalDistance = Distance;
                                        }
                                        DistanceAccumulator += Distance;
                                        DistanceSquareAccumulator += Distance * Distance;
                                        ++TotalActiveLinks;
                                    }
                                break;
                            case eMinkowski:
                            {
                                const Real InverseMinkowskiOrder = Real(1) / m_MinkowskiOrder;
                                for (auto& m_Link : m_Links)
                                    if (m_Link->IsEnabled())
                                    {
                                        const Real Distance = m_Link->UpdateMinkowskiDistance(m_MinkowskiOrder, InverseMinkowskiOrder);
                                        if (Distance > MaximalDistance)
                                        {
                                            MaximalDistance = Distance;
                                        }
                                        if (Distance < MinimalDistance)
                                        {
                                            MinimalDistance = Distance;
                                        }
                                        DistanceAccumulator += Distance;
                                        DistanceSquareAccumulator += Distance * Distance;
                                        ++TotalActiveLinks;
                                    }
                            }
                            break;
                            case eChebyshev:
                                for (auto& m_Link : m_Links)
                                    if (m_Link->IsEnabled())
                                    {
                                        const Real Distance = m_Link->UpdateChebyshevDistance();
                                        if (Distance > MaximalDistance)
                                        {
                                            MaximalDistance = Distance;
                                        }
                                        if (Distance < MinimalDistance)
                                        {
                                            MinimalDistance = Distance;
                                        }
                                        DistanceAccumulator += Distance;
                                        DistanceSquareAccumulator += Distance * Distance;
                                        ++TotalActiveLinks;
                                    }
                                break;
                            default:
                                return false;
                        }
                    MeanDistance = DistanceAccumulator / Real(TotalActiveLinks);
                    DistanceStandardDeviation = std::sqrt((DistanceSquareAccumulator / Real(TotalActiveLinks)) - MeanDistance * MeanDistance);
                    return true;
                }

                void CContinuousChromaticSegmentation::PropagateDistances(const int TotalLevels)
                {
                    for (int i = 0 ; i < TotalLevels ; ++i)
                    {
                        for (auto& m_Pixel : m_Pixels)
                            if (m_Pixel->IsEnabledFullyConnected())
                            {
                                m_Pixel->IntegratePropagationDistance();
                            }
                        if ((m_ChromaticSpace == eHSL) || (m_ChromaticSpace == eHSI) || (m_ChromaticSpace == eHSV))
                        {
                            for (auto& m_Link : m_Links)
                                if (m_Link->IsEnabled() && m_Link->IsRadialDefined())
                                {
                                    m_Link->UpdatePropagationDistance();
                                }
                        }
                        else
                            for (auto& m_Link : m_Links)
                                if (m_Link->IsEnabled())
                                {
                                    m_Link->UpdatePropagationDistance();
                                }
                    }
                }
            }
        }
    }
}
