/*
 * ContinuousChromaticPixel.cpp
 */

#include "ContinuousChromaticPixel.h"
#include "ContinuousChromaticLink.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Segmentation
            {
                CContinuousChromaticPixel::CContinuousChromaticPixel() :
                    m_StatusBitFlags(0),
                    m_X(g_IntegerPlusInfinity),
                    m_Y(g_IntegerPlusInfinity),
                    m_PropagationDistance(g_RealPlusInfinity),
                    m_pSegment(nullptr),
                    m_pSourcePixel(nullptr)
                {
                    memset(m_Neighborhood, 0, sizeof(CContinuousChromaticLink*) * 8);
                }

                CContinuousChromaticPixel::~CContinuousChromaticPixel()
                    = default;

                void CContinuousChromaticPixel::SetSource(const bool IsEnabled, const int X, const int Y, const ContinuousTristimulusPixel* pSourcePixel)
                {
                    if (IsEnabled)
                    {
                        m_StatusBitFlags |= 0X1;
                    }
                    else
                    {
                        m_StatusBitFlags &= 0XFE;
                    }
                    m_X = X;
                    m_Y = Y;
                    m_pSourcePixel = pSourcePixel;
                }

                void CContinuousChromaticPixel::SetLink(CContinuousChromaticLink* pLink, const int Index)
                {
                    m_Neighborhood[Index] = pLink;
                }

                void CContinuousChromaticPixel::UpdateConnectivityFlag()
                {
                    m_StatusBitFlags |= 0X2;
                    for (auto& i : m_Neighborhood)
                        if (!i)
                        {
                            m_StatusBitFlags &= 0XFD;
                        }
                }

                Real CContinuousChromaticPixel::IntegratePropagationDistance()
                {
                    m_PropagationDistance = Real(0);
                    for (auto& i : m_Neighborhood)
                        if (i)
                        {
                            m_PropagationDistance += i->GetDistance();
                        }
                    return m_PropagationDistance;
                }

                Real CContinuousChromaticPixel::PropagateDistance()
                {
                    m_PropagationDistance = Real(0);
                    for (auto& i : m_Neighborhood)
                        if (i)
                        {
                            m_PropagationDistance += i->GetPropagationDistance();
                        }
                    return m_PropagationDistance;
                }

                CContinuousChromaticLink* CContinuousChromaticPixel::GetLink(const int Index)
                {
                    return m_Neighborhood[Index];
                }

                bool CContinuousChromaticPixel::IsEnabled() const
                {
                    return m_StatusBitFlags & 0X1;
                }

                bool CContinuousChromaticPixel::IsEnabledFullyConnected() const
                {
                    return m_StatusBitFlags & 0X3;
                }

                int CContinuousChromaticPixel::GetX() const
                {
                    return m_X;
                }

                int CContinuousChromaticPixel::GetY() const
                {
                    return m_Y;
                }

                Real CContinuousChromaticPixel::GetPropagationDistance() const
                {
                    return m_PropagationDistance;
                }

                const CContinuousChromaticSegment* CContinuousChromaticPixel::GetReadOnlySegment() const
                {
                    return m_pSegment;
                }

                const ContinuousTristimulusPixel* CContinuousChromaticPixel::GetSourcePixel() const
                {
                    return m_pSourcePixel;
                }
            }
        }
    }
}
