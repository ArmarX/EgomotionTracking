/*
 * ContinuousChromaticSegment.h
 */

#pragma once

#include "../../../Common/DataTypes.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Segmentation
            {
                class CContinuousChromaticPixel;
                class CContinuousChromaticLink;
                class CContinuousChromaticSegment
                {
                public:

                    CContinuousChromaticSegment();
                    ~CContinuousChromaticSegment();

                protected:

                };
            }
        }
    }
}

