/*
 * ContinuousChromaticLink.h
 */

#pragma once

#include "../../../Common/DataTypes.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Segmentation
            {
                class CContinuousChromaticPixel;
                class CContinuousChromaticLink
                {
                public:

                    static bool SortByDistanceAscendant(const CContinuousChromaticLink* plhs, const CContinuousChromaticLink* prhs);
                    static bool SortByDistanceDescendant(const CContinuousChromaticLink* plhs, const CContinuousChromaticLink* prhs);

                    static bool SortByPropagationDistanceAscendant(const CContinuousChromaticLink* plhs, const CContinuousChromaticLink* prhs);
                    static bool SortByPropagationDistanceDescendant(const CContinuousChromaticLink* plhs, const CContinuousChromaticLink* prhs);

                    CContinuousChromaticLink(CContinuousChromaticPixel* pPixelA, CContinuousChromaticPixel* pPixelB);
                    ~CContinuousChromaticLink();

                    bool IsEnabled() const;
                    bool IsRadialDefined() const;

                    Real UpdateManhatanDistance();
                    Real UpdateSquareEuclideanDistance();
                    Real UpdateEuclideanDistance();
                    Real UpdateMinkowskiDistance(const Real P, const Real IP);
                    Real UpdateChebyshevDistance();

                    Real UpdatePropagationDistance();

                    const CContinuousChromaticPixel* GetReadOnlyPixelAlpha() const;
                    const CContinuousChromaticPixel* GetReadOnlyPixelBeta() const;
                    Real GetDistance() const;
                    Real GetPropagationDistance() const;

                protected:

                    CContinuousChromaticPixel* m_pPixelAlpha;
                    CContinuousChromaticPixel* m_pPixelBeta;
                    Real m_Distance;
                    Real m_PropagationDistance;
                };
            }
        }
    }
}

