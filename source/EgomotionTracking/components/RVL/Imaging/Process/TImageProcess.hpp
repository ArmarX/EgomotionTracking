/*
 * TImageProcess.hpp
 */

#pragma once

#include "../../Time/TimeLogger.h"
#include "../TImage.hpp"
#include "../ActiveImageZone.h"
#include "../ActiveMaskImage.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            template <typename InPixel, typename OutPixel> class TImageProcess
            {
            public:

                TImageProcess(const TImage<InPixel>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone) :
                    m_IsEnabled(false),
                    m_IsMasking(false),
                    m_pInImage(NULL),
                    m_pInActiveMaskImage(NULL),
                    m_pInActiveImageZone(NULL),
                    m_OutImage(),
                    m_OutActiveMaskImage(),
                    m_OutActiveImageZone()
                {
                    if ((!pInImage) || (!pInImage->IsValid()))
                    {
                        return;
                    }

                    if (pInActiveImageZone && ((!pInActiveImageZone->IsValid()) || (!pInImage->IsInside(pInActiveImageZone))))
                    {
                        return;
                    }

                    if (pInActiveMaskImage && (!pInActiveMaskImage->IsValid() || (!pInImage->SizeEquals(pInActiveMaskImage->GetSize()))))
                    {
                        return;
                    }

                    m_IsEnabled = true;
                    m_IsMasking = pInActiveMaskImage;

                    m_pInImage = pInImage;
                    m_pInActiveMaskImage = pInActiveMaskImage;
                    m_pInActiveImageZone = pInActiveImageZone;
                }

                virtual ~TImageProcess()
                {
                }

                virtual void SetMasking(const bool Masking)
                {
                    m_IsMasking = Masking;
                }

                inline void SetEnabled(const bool Enabled)
                {
                    m_IsEnabled = Enabled;
                }

                inline bool IsEnabled() const
                {
                    return m_IsEnabled;
                }

                inline bool IsMasking() const
                {
                    return m_IsMasking && m_pInActiveImageZone && m_pInActiveImageZone->IsValid();
                }

                inline const CActiveImageZone* GetInActiveImageZone() const
                {
                    return m_pInActiveImageZone;
                }

                inline const CActiveImageZone* GetOutActiveImageZone() const
                {
                    return &m_OutActiveImageZone;
                }

                inline const TImage<InPixel>* GetInImage() const
                {
                    return m_pInImage;
                }

                inline const CActiveMaskImage* GetInActiveMaskImage() const
                {
                    return m_pInActiveMaskImage;
                }

                inline const CActiveMaskImage* GetOutActiveMaskImage() const
                {
                    return &m_OutActiveMaskImage;
                }

                inline const TImage<OutPixel>* GetOutImage() const
                {
                    return &m_OutImage;
                }

                virtual void InitializeActiveImageZone()
                {
                    if (m_pInActiveImageZone)
                    {
                        m_OutActiveImageZone = *m_pInActiveImageZone;
                    }
                    else
                    {
                        m_OutActiveImageZone.Set(m_pInImage->GetSize());
                    }
                }

                virtual void InitializeActiveMaskImage()
                {
                    m_OutActiveMaskImage.Create(m_pInImage->GetSize(), false);
                    m_OutActiveMaskImage.SetSource(m_pInActiveMaskImage, true);
                }

                virtual bool UpdateActiveMaskImage(const bool /*Force*/)
                {
                    if (m_pInActiveMaskImage && m_pInActiveMaskImage->IsValid() && m_pInImage->SizeEquals(m_pInActiveMaskImage->GetSize()))
                    {
                        if (m_OutActiveMaskImage.SourceHasChanged(m_pInActiveMaskImage))
                        {
                            return m_OutActiveMaskImage.UpdateCopyFromSource();
                        }
                        return true;
                    }
                    return m_OutActiveMaskImage.Set(true);
                }

                virtual bool UpdateActiveImageZone(const bool /*Force*/)
                {
                    if (m_pInActiveImageZone && m_pInActiveImageZone->IsValid() && m_pInImage->IsInside(m_pInActiveImageZone))
                    {
                        if ((m_OutActiveImageZone != *m_pInActiveImageZone) || (m_OutActiveImageZone.GetContentId() != m_pInActiveImageZone->GetContentId()))
                        {
                            m_OutActiveImageZone = *m_pInActiveImageZone;
                        }
                        return true;
                    }
                    else if (m_OutActiveImageZone.Set(m_pInImage->GetSize()))
                    {
                        m_OutActiveImageZone.SetContentId(0);
                        return true;
                    }
                    return false;
                }

                virtual bool Execute(const int Trial) = 0;

            protected:

                bool m_IsEnabled;
                bool m_IsMasking;

                const TImage<InPixel>* m_pInImage;
                const CActiveMaskImage* m_pInActiveMaskImage;
                const CActiveImageZone* m_pInActiveImageZone;

                TImage<OutPixel> m_OutImage;
                CActiveMaskImage m_OutActiveMaskImage;
                CActiveImageZone m_OutActiveImageZone;
            };
        }
    }
}

