/*
 * EdgeExtraction.cpp
 */

#include "EdgeExtraction.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Edge
            {
                CEdgeExtraction::CEdgeExtraction(const TImage<Saliency::CSaliencyPixel>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone) :
                    TImageProcess<Saliency::CSaliencyPixel, Saliency::CSaliencyPixel>(pInImage, pInActiveMaskImage, pInActiveImageZone),
                    m_EnableCoherence(false),
                    m_EnableMagnitudeFiltering(false),
                    m_Coherence(Real(0)),
                    m_Percentile(Real(0))
                {
                    if (IsEnabled())
                    {
                        m_OutImage.Wrap(const_cast<TImage<Saliency::CSaliencyPixel>*>(m_pInImage));
                        InitializeActiveMaskImage();
                        SetEnabled(UpdateActiveImageZone(true) && UpdateActiveMaskImage(true));
                    }
                }

                CEdgeExtraction::~CEdgeExtraction()
                    = default;

                void CEdgeExtraction::SetEnableCoherence(const bool EnableCoherence)
                {
                    m_EnableCoherence = EnableCoherence;
                }

                void CEdgeExtraction::SetEnableMagnitudeFiltering(const bool EnableMagnitudeFiltering)
                {
                    m_EnableMagnitudeFiltering = EnableMagnitudeFiltering;
                }

                bool CEdgeExtraction::SetCoherence(const Real Coherence)
                {
                    if (IsPositive(Coherence) && (Coherence < Real(1)))
                    {
                        m_Coherence = Coherence;
                        return true;
                    }
                    return false;
                }

                bool CEdgeExtraction::SetPercentile(const Real Percentile)
                {
                    if (IsPositive(Percentile) && (Percentile < Real(1)))
                    {
                        m_Percentile = Percentile;
                        return true;
                    }
                    return false;
                }

                bool CEdgeExtraction::GetEnableCoherence() const
                {
                    return m_EnableCoherence;
                }

                bool CEdgeExtraction::GetEnableMagnitudeFiltering() const
                {
                    return m_EnableMagnitudeFiltering;
                }

                Real CEdgeExtraction::GetCoherence() const
                {
                    return m_Coherence;
                }

                Real CEdgeExtraction::GetPercentile() const
                {
                    return m_Percentile;
                }

                bool CEdgeExtraction::Execute(const int Trial)
                {
                    _RVL_TIME_LOGGER_BEGIN_(0, "CEdgeExtraction::Execute");
                    bool Result = false;
                    if (IsEnabled() && ((!m_pInActiveImageZone) || (m_pInActiveImageZone->IsValid() && m_pInImage->IsInside(m_pInActiveImageZone))))
                    {
                        if (IsMasking() && (!UpdateActiveMaskImage(false)))
                        {
                            _RVL_TIME_LOGGER_VERBOSE_END_(0, "CEdgeExtraction::Execute");
                            return false;
                        }
                        if (!UpdateActiveImageZone(false))
                        {
                            _RVL_TIME_LOGGER_VERBOSE_END_(0, "CEdgeExtraction::Execute");
                            return false;
                        }
                        const int TotalPixels = SelectPixels();
                        if (m_EnableMagnitudeFiltering)
                        {
                            FilterPixelsByMagnitude(TotalPixels);
                        }
                        Result = true;
                    }
                    _RVL_TIME_LOGGER_VERBOSE_END_(0, "CGaborSaliencyExtraction::Execute");
                    return Result;
                }

                bool CEdgeExtraction::Display(TImage<DiscreteRGBPixel>* pDisplayImage, const DisplayChannel Channel, const CDiscreteRGBColorMap* pColorMap, Real& MaximalChannelValue, Real& MinimalChannelValue, Real& ChannelMean, Real& ChannelStandardDeviation) const
                {
                    if (pDisplayImage && pDisplayImage->IsValid() && pDisplayImage->HasBufferOwnership() && pDisplayImage->SizeEquals(m_OutImage.GetSize()) && pColorMap && pColorMap->IsValid())
                    {
                        pDisplayImage->Clear();
                        const int X0 = m_OutActiveImageZone.GetX0();
                        const int Y0 = m_OutActiveImageZone.GetY0();
                        const int X1 = m_OutActiveImageZone.GetX1();
                        const int Y1 = m_OutActiveImageZone.GetY1();
                        const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                        int TotalActivePixels = 0;
                        Real ChannelAccumulator = Real(0), ChannelSquareAccumulator = Real(0);
                        const Saliency::CSaliencyPixel* pSaliencyPixel = m_OutImage.GetReadOnlyBufferAt(X0, Y0);
                        switch (Channel)
                        {
                            case eChannelActive:
                            {
                                const DiscreteRGBPixel OnColor(255, 255, 255);
                                DiscreteRGBPixel* pDiscreteRGBPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                                if (IsMasking())
                                {
                                    MinimalChannelValue = g_RealPlusInfinity;
                                    MaximalChannelValue = g_RealMinusInfinity;
                                    const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pActiveMaskPixel += Offset, pSaliencyPixel += Offset, pDiscreteRGBPixel += Offset)
                                        for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel, ++pDiscreteRGBPixel)
                                            if (*pActiveMaskPixel++ && pSaliencyPixel->GetStatusBit0())
                                            {
                                                const Real Value = pSaliencyPixel->GetMagnitude();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pDiscreteRGBPixel = OnColor;
                                                ++TotalActivePixels;
                                            }
                                }
                                else
                                {
                                    MinimalChannelValue = MaximalChannelValue = pSaliencyPixel->GetMagnitude();
                                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset, pDiscreteRGBPixel += Offset)
                                        for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel)
                                            if (pSaliencyPixel->GetStatusBit0())
                                            {
                                                const Real Value = pSaliencyPixel->GetMagnitude();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pDiscreteRGBPixel++ = OnColor;
                                                ++TotalActivePixels;
                                            }
                                }
                                ChannelMean = ChannelAccumulator / Real(TotalActivePixels);
                                const Real Range = MaximalChannelValue - MinimalChannelValue;
                                if (IsPositive(Range))
                                {
                                    ChannelStandardDeviation = std::sqrt((ChannelSquareAccumulator / Real(TotalActivePixels)) - (ChannelMean * ChannelMean));
                                    return true;
                                }
                            }
                            break;
                            case eChannelActiveMagnitude:
                            {
                                TImage<Real> ChannelImage(pDisplayImage->GetSize());
                                Real* pChannelPixel = ChannelImage.GetWritableBufferAt(X0, Y0);
                                if (IsMasking())
                                {
                                    MinimalChannelValue = g_RealPlusInfinity;
                                    MaximalChannelValue = g_RealMinusInfinity;
                                    const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset, pChannelPixel += Offset)
                                        for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel, ++pChannelPixel)
                                            if (*pActiveMaskPixel++ && pSaliencyPixel->GetStatusBit0())
                                            {
                                                const Real Value = pSaliencyPixel->GetPhase();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel = Value;
                                                ++TotalActivePixels;
                                            }
                                }
                                else
                                {
                                    MinimalChannelValue = MaximalChannelValue = pSaliencyPixel->GetPhase();
                                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset, pChannelPixel += Offset)
                                        for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel)
                                            if (pSaliencyPixel->GetStatusBit0())
                                            {
                                                const Real Value = pSaliencyPixel->GetPhase();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                                ++TotalActivePixels;
                                            }
                                }
                                ChannelMean = ChannelAccumulator / Real(TotalActivePixels);
                                ChannelStandardDeviation = std::sqrt((ChannelSquareAccumulator / Real(TotalActivePixels)) - (ChannelMean * ChannelMean));
                                const DiscreteRGBPixel* pLUT = pColorMap->GetLUT();
                                const Real NormalizationFactor = Real(pColorMap->GetLUTScaleFactor()) / g_Real2PI;
                                pChannelPixel = ChannelImage.GetWritableBufferAt(X0, Y0);
                                DiscreteRGBPixel* pDiscreteRGBPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                                if (IsMasking())
                                {
                                    const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pChannelPixel += Offset, pDiscreteRGBPixel += Offset)
                                        for (int X = X0 ; X <= X1 ; ++X, ++pDiscreteRGBPixel, ++pChannelPixel)
                                            if (*pActiveMaskPixel++)
                                            {
                                                *pDiscreteRGBPixel = pLUT[int(NormalizationFactor * *pChannelPixel + Real(0.5))];
                                            }
                                }
                                else
                                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pChannelPixel += Offset, pDiscreteRGBPixel += Offset)
                                        for (int X = X0 ; X <= X1 ; ++X)
                                        {
                                            *pDiscreteRGBPixel++ = pLUT[int(NormalizationFactor * *pChannelPixel++ + Real(0.5))];
                                        }
                                return true;
                            }
                            break;
                            default:
                                return false;
                        }
                    }
                    return false;
                }

                int CEdgeExtraction::SelectPixels()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    Saliency::CSaliencyPixel* pSaliencyPixel = m_OutImage.GetWritableBufferAt(X0, Y0, true);
                    const int Deltas[8] = { 1, m_OutImage.GetWidth() + 1, m_OutImage.GetWidth(), m_OutImage.GetWidth() - 1, -1, -m_OutImage.GetWidth() - 1, -m_OutImage.GetWidth(), 1 - m_OutImage.GetWidth() };
                    int TotalPixels = 0;
                    if (IsMasking())
                    {
                        const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                        if (m_EnableCoherence)
                        {
                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset, pActiveMaskPixel += Offset)
                                for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel)
                                    if (*pActiveMaskPixel++ && pSaliencyPixel->HasMagnitude())
                                    {
                                        const int Delta = Deltas[pSaliencyPixel->GetDiscreteDirectionIndexFullConnectivity()];
                                        if (pActiveMaskPixel[Delta] && pActiveMaskPixel[-Delta] && (pSaliencyPixel->GetMagnitude() > pSaliencyPixel[Delta].GetMagnitude()) && (pSaliencyPixel->GetMagnitude() > pSaliencyPixel[-Delta].GetMagnitude()) && (pSaliencyPixel->GetDirection().ScalarProduct(pSaliencyPixel[Delta].GetDirection()) > m_Coherence) && (pSaliencyPixel->GetDirection().ScalarProduct(pSaliencyPixel[-Delta].GetDirection()) > m_Coherence))
                                        {
                                            pSaliencyPixel->SetStatusBit0();
                                            ++TotalPixels;
                                        }
                                    }
                        }
                        else
                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset, pActiveMaskPixel += Offset)
                                for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel)
                                    if (*pActiveMaskPixel++ && pSaliencyPixel->HasMagnitude())
                                    {
                                        const int Delta = Deltas[pSaliencyPixel->GetDiscreteDirectionIndexFullConnectivity()];
                                        if (pActiveMaskPixel[Delta] && pActiveMaskPixel[-Delta] && (pSaliencyPixel->GetMagnitude() > pSaliencyPixel[Delta].GetMagnitude()) && (pSaliencyPixel->GetMagnitude() > pSaliencyPixel[-Delta].GetMagnitude()))
                                        {
                                            pSaliencyPixel->SetStatusBit0();
                                            ++TotalPixels;
                                        }
                                    }
                    }
                    else
                    {
                        if (m_EnableCoherence)
                        {
                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset)
                                for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel)
                                    if (pSaliencyPixel->HasMagnitude())
                                    {
                                        const int Delta = Deltas[pSaliencyPixel->GetDiscreteDirectionIndexFullConnectivity()];
                                        if ((pSaliencyPixel->GetMagnitude() > pSaliencyPixel[Delta].GetMagnitude()) && (pSaliencyPixel->GetMagnitude() > pSaliencyPixel[-Delta].GetMagnitude()) && (pSaliencyPixel->GetDirection().ScalarProduct(pSaliencyPixel[Delta].GetDirection()) > m_Coherence) && (pSaliencyPixel->GetDirection().ScalarProduct(pSaliencyPixel[-Delta].GetDirection()) > m_Coherence))
                                        {
                                            pSaliencyPixel->SetStatusBit0();
                                            ++TotalPixels;
                                        }
                                    }
                        }
                        else
                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset)
                                for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel)
                                    if (pSaliencyPixel->HasMagnitude())
                                    {
                                        const int Delta = Deltas[pSaliencyPixel->GetDiscreteDirectionIndexFullConnectivity()];
                                        if ((pSaliencyPixel->GetMagnitude() > pSaliencyPixel[Delta].GetMagnitude()) && (pSaliencyPixel->GetMagnitude() > pSaliencyPixel[-Delta].GetMagnitude()))
                                        {
                                            pSaliencyPixel->SetStatusBit0();
                                            ++TotalPixels;
                                        }
                                    }
                    }
                    return TotalPixels;
                }

                void CEdgeExtraction::FilterPixelsByMagnitude(const int TotalPixels)
                {
                    if (TotalPixels)
                    {
                        const int X0 = m_OutActiveImageZone.GetX0();
                        const int Y0 = m_OutActiveImageZone.GetY0();
                        const int X1 = m_OutActiveImageZone.GetX1();
                        const int Y1 = m_OutActiveImageZone.GetY1();
                        const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                        Saliency::CSaliencyPixel* pSaliencyPixel = m_OutImage.GetWritableBufferAt(X0, Y0, true);
                        std::vector<Saliency::CSaliencyPixel*> SelectedPixels;
                        SelectedPixels.reserve(TotalPixels);
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel)
                                if (pSaliencyPixel->GetStatusBit0())
                                {
                                    SelectedPixels.push_back(pSaliencyPixel);
                                }
                        std::stable_sort(SelectedPixels.begin(), SelectedPixels.end(), Saliency::CSaliencyPixel::SortByMagnitudeDescent);
                        const int Active = std::min(int(std::ceil(m_Percentile * Real(TotalPixels))), TotalPixels);
                        for (int i = 0 ; i < Active ; ++i)
                        {
                            SelectedPixels[i]->SetStatusBit1();
                        }
                    }
                }

                void CEdgeExtraction::InitializeActiveImageZone()
                {
                    if (m_pInActiveImageZone)
                    {
                        m_OutActiveImageZone.Set(*m_pInActiveImageZone, 1);
                    }
                    else
                    {
                        m_OutActiveImageZone.Set(m_pInImage->GetSize(), 1);
                    }
                }

                void CEdgeExtraction::InitializeActiveMaskImage()
                {
                    m_OutActiveMaskImage.Create(m_pInImage->GetSize(), false);
                    m_OutActiveMaskImage.SetSource(m_pInActiveMaskImage, false);
                }

                bool CEdgeExtraction::UpdateActiveImageZone(const bool Force)
                {
                    if (m_pInActiveImageZone && m_pInActiveImageZone->IsValid() && m_pInImage->IsInside(m_pInActiveImageZone))
                    {
                        if (Force || (!m_OutActiveImageZone.IsPostSynchronized(*m_pInActiveImageZone, 1)))
                        {
                            return m_OutActiveImageZone.Set(*m_pInActiveImageZone, 1);
                        }
                        return true;
                    }
                    return m_OutActiveImageZone.Set(0, 0, m_pInImage->GetWidth() - 1, m_pInImage->GetHeight() - 1);
                }

                bool CEdgeExtraction::UpdateActiveMaskImage(const bool Force)
                {
                    if (m_pInActiveMaskImage && m_pInActiveMaskImage->IsValid() && m_pInImage->SizeEquals(m_pInActiveMaskImage->GetSize()))
                    {
                        if (Force || m_OutActiveMaskImage.SourceHasChanged(m_pInActiveMaskImage))
                        {
                            return m_OutActiveMaskImage.UpdateErodeFromSource(1);
                        }
                        return true;
                    }
                    return m_OutActiveMaskImage.Set(true);
                }
            }
        }
    }
}
