/*
 * EdgeExtraction.h
 */

#pragma once

#include "../TImageProcess.hpp"
#include "../Saliency/SaliencyPixel.h"
#include "../../PixelTypes.h"
#include "../../DiscreteRGBColorMap.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Edge
            {
                class CEdgeExtraction : public TImageProcess<Saliency::CSaliencyPixel, Saliency::CSaliencyPixel>
                {
                public:

                    enum DisplayChannel
                    {
                        eChannelActive, eChannelActiveMagnitude
                    };

                    CEdgeExtraction(const TImage<Saliency::CSaliencyPixel>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone);
                    ~CEdgeExtraction() override;

                    void SetEnableCoherence(const bool EnableCoherence);
                    void SetEnableMagnitudeFiltering(const bool EnableMagnitudeFiltering);
                    bool SetCoherence(const Real Coherence);
                    bool SetPercentile(const Real Percentile);

                    bool GetEnableCoherence() const;
                    bool GetEnableMagnitudeFiltering() const;
                    Real GetCoherence() const;
                    Real GetPercentile() const;

                    bool Execute(const int Trial) override;

                    bool Display(TImage<DiscreteRGBPixel>* pDisplayImage, const DisplayChannel Channel, const CDiscreteRGBColorMap* pColorMap, Real& MaximalChannelValue, Real& MinimalChannelValue, Real& ChannelMean, Real& ChannelStandardDeviation) const;

                protected:

                    void InitializeActiveImageZone() override;
                    void InitializeActiveMaskImage() override;
                    bool UpdateActiveImageZone(const bool Force) override;
                    bool UpdateActiveMaskImage(const bool Force) override;

                    int SelectPixels();
                    void FilterPixelsByMagnitude(const int TotalPixels);

                    bool m_EnableCoherence;
                    bool m_EnableMagnitudeFiltering;
                    Real m_Coherence;
                    Real m_Percentile;
                };
            }
        }
    }
}

