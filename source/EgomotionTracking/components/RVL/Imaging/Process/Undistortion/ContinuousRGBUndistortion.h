/*
 * ContinuousRGBUndistortion.h
 */

#pragma once

#include "../../../Sensors/Cameras/Pasive/Calibration/Geometric/MonocularCameraGeometricCalibration.h"
#include "../../../Mathematics/_2D/Vector2D.h"
#include "../../../Containers/_2D/ContinuousBoundingBox2D.h"
#include "../../PixelTypes.h"
#include "../TImageProcess.hpp"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Undistortion
            {
                class CContinuousRGBUndistortion : public TImageProcess<ContinuousRGBPixel, ContinuousRGBPixel>
                {
                public:

                    CContinuousRGBUndistortion(const TImage<ContinuousRGBPixel>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone, const Sensors::Cameras::Pasive::Calibration::Geometric::CMonocularCameraGeometricCalibration* pMonocularCameraGeometricCalibration);
                    ~CContinuousRGBUndistortion() override;

                    bool LoadCalibration(const Sensors::Cameras::Pasive::Calibration::Geometric::CMonocularCameraGeometricCalibration* pMonocularCameraGeometricCalibration);
                    bool LoadCalibration(const std::string& FileName, const int TargetCalibrationIndex, const bool ResetExtrinsic);

                    bool Execute(const int Trial) override;

                    bool Display(TImage<DiscreteRGBPixel>* pDisplayImage);

                protected:

                    struct MappingPixel
                    {
                        int m_Offset;
                        int m_X0;
                        int m_Y0;
                        int m_X1;
                        int m_Y1;
                        Real m_Wij[4][4];
                    };

                    virtual bool UpdateActiveImageZones(const bool Force);
                    bool UpdateActiveMaskImage(const bool Force) override;

                    Real BicubicKernel(const Real X) const;
                    bool IntializeStructures();

                    Containers::_2D::CContinuousBoundingBox2D DetermineUndistortedRegion(const int X0, const int Y0, const int X1, const int Y1, const Real Margin);

                    Real m_ScalingFactor;
                    CActiveImageZone m_InActiveImageZone;
                    Containers::_2D::CContinuousBoundingBox2D m_ImageBoundingBox;
                    Mathematics::_2D::CVector2D m_MinScaledPoint;
                    Mathematics::_2D::CVector2D m_MaxScaledPoint;
                    CImageSize m_ScaledImageSize;
                    Sensors::Cameras::Pasive::Calibration::Geometric::CMonocularCameraGeometricCalibration m_MonocularCameraGeometricCalibration;
                    TImage<MappingPixel> m_MappingImage;
                };
            }
        }
    }
}

