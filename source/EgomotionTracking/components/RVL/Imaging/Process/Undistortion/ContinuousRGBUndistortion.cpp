/*
 * ContinuousRGBUndistortion.cpp
 */

#include "ContinuousRGBUndistortion.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Undistortion
            {
                CContinuousRGBUndistortion::CContinuousRGBUndistortion(const TImage<ContinuousRGBPixel>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone, const Sensors::Cameras::Pasive::Calibration::Geometric::CMonocularCameraGeometricCalibration* pMonocularCameraGeometricCalibration) :
                    TImageProcess<ContinuousRGBPixel, ContinuousRGBPixel>(pInImage, pInActiveMaskImage, pInActiveImageZone),
                    m_ScalingFactor(Real(1)),
                    m_MonocularCameraGeometricCalibration(pMonocularCameraGeometricCalibration)
                {
                    if (IsEnabled())
                    {
                        SetEnabled(IntializeStructures());
                    }
                }

                CContinuousRGBUndistortion::~CContinuousRGBUndistortion()
                    = default;

                bool CContinuousRGBUndistortion::LoadCalibration(const Sensors::Cameras::Pasive::Calibration::Geometric::CMonocularCameraGeometricCalibration* pMonocularCameraGeometricCalibration)
                {
                    if (pMonocularCameraGeometricCalibration)
                    {
                        if (m_MonocularCameraGeometricCalibration == *pMonocularCameraGeometricCalibration)
                        {
                            return false;
                        }
                        return IntializeStructures();
                    }
                    return false;
                }

                bool CContinuousRGBUndistortion::LoadCalibration(const std::string& FileName, const int TargetCalibrationIndex, const bool ResetExtrinsic)
                {
                    if (m_MonocularCameraGeometricCalibration.LoadFromFile(FileName, TargetCalibrationIndex, ResetExtrinsic))
                    {
                        return IntializeStructures();
                    }
                    return false;
                }

                bool CContinuousRGBUndistortion::Execute(const int Trial)
                {
                    _RVL_TIME_LOGGER_BEGIN_(0, "CContinuousRGBUndistortion::Execute");
                    bool Result = false;
                    if (IsEnabled() && ((!m_pInActiveImageZone) || (m_pInActiveImageZone->IsValid() && m_pInImage->IsInside(m_pInActiveImageZone))))
                    {
                        if (IsMasking() && (!UpdateActiveMaskImage(false)))
                        {
                            _RVL_TIME_LOGGER_VERBOSE_END_(0, "CContinuousRGBUndistortion::Execute");
                            return false;
                        }
                        if (!UpdateActiveImageZones(false))
                        {
                            _RVL_TIME_LOGGER_VERBOSE_END_(0, "CContinuousRGBUndistortion::Execute");
                            return false;
                        }
                        const int SX0 = m_InActiveImageZone.GetX0();
                        const int SY0 = m_InActiveImageZone.GetY0();
                        const int SX1 = m_InActiveImageZone.GetX1();
                        const int SY1 = m_InActiveImageZone.GetY1();
                        const int X0 = m_OutActiveImageZone.GetX0();
                        const int Y0 = m_OutActiveImageZone.GetY0();
                        const int X1 = m_OutActiveImageZone.GetX1();
                        const int Y1 = m_OutActiveImageZone.GetY1();
                        const int Offset = m_pInImage->GetWidth() - 4;
                        const ContinuousRGBPixel* pInPixelBase = m_pInImage->GetReadOnlyBuffer();
                        if (IsMasking())
                        {
                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                            {
                                const MappingPixel* pMappingPixel = m_MappingImage.GetReadOnlyBufferAt(X0, Y);
                                ContinuousRGBPixel* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y);
                                const bool* pActivePixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y);
                                for (int X = X0 ; X <= X1 ; ++X, ++pMappingPixel, ++pOutPixel, ++pActivePixel)
                                    if (*pActivePixel && pMappingPixel->m_Offset && (pMappingPixel->m_X1 <= SX1) && (pMappingPixel->m_Y1 <= SY1) && (pMappingPixel->m_X0 >= SX0) && (pMappingPixel->m_Y0 >= SY0))
                                    {
                                        Real R = Real(0), G = Real(0), B = Real(0);
                                        const ContinuousRGBPixel* pInPixel = pInPixelBase + pMappingPixel->m_Offset;
                                        for (int i = 0 ; i < 4 ; ++i, pInPixel += Offset)
                                            for (int j = 0 ; j < 4 ; ++j, ++pInPixel)
                                            {
                                                R += pMappingPixel->m_Wij[i][j] * pInPixel->m_Elements.m_R;
                                                G += pMappingPixel->m_Wij[i][j] * pInPixel->m_Elements.m_G;
                                                B += pMappingPixel->m_Wij[i][j] * pInPixel->m_Elements.m_B;
                                            }
                                        pOutPixel->Set(R, G, B);
                                    }
                            }
                        }
                        else
                        {
                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                            {
                                const MappingPixel* pMappingPixel = m_MappingImage.GetReadOnlyBufferAt(X0, Y);
                                ContinuousRGBPixel* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y);
                                for (int X = X0 ; X <= X1 ; ++X, ++pMappingPixel, ++pOutPixel)
                                    if (pMappingPixel->m_Offset && (pMappingPixel->m_X1 <= SX1) && (pMappingPixel->m_Y1 <= SY1) && (pMappingPixel->m_X0 >= SX0) && (pMappingPixel->m_Y0 >= SY0))
                                    {
                                        Real R = Real(0), G = Real(0), B = Real(0);
                                        const ContinuousRGBPixel* pInPixel = pInPixelBase + pMappingPixel->m_Offset;
                                        for (int i = 0 ; i < 4 ; ++i, pInPixel += Offset)
                                            for (int j = 0 ; j < 4 ; ++j, ++pInPixel)
                                            {
                                                R += pMappingPixel->m_Wij[i][j] * pInPixel->m_Elements.m_R;
                                                G += pMappingPixel->m_Wij[i][j] * pInPixel->m_Elements.m_G;
                                                B += pMappingPixel->m_Wij[i][j] * pInPixel->m_Elements.m_B;
                                            }
                                        pOutPixel->Set(R, G, B);
                                    }
                            }
                        }
                        Result = true;
                    }
                    _RVL_TIME_LOGGER_VERBOSE_END_(0, "CContinuousRGBUndistortion::Execute");
                    return Result;
                }

                bool CContinuousRGBUndistortion::Display(TImage<DiscreteRGBPixel>* pDisplayImage)
                {
                    if (pDisplayImage && pDisplayImage->IsValid() && pDisplayImage->HasBufferOwnership() && pDisplayImage->SizeEquals(m_OutImage.GetSize()))
                    {
                        pDisplayImage->Clear();
                        const int X0 = m_OutActiveImageZone.GetX0();
                        const int Y0 = m_OutActiveImageZone.GetY0();
                        const int X1 = m_OutActiveImageZone.GetX1();
                        const int Y1 = m_OutActiveImageZone.GetY1();
                        const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                        DiscreteRGBPixel* pDisplayPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                        const ContinuousRGBPixel* pOutPixel = m_OutImage.GetReadOnlyBufferAt(X0, Y0);
                        if (Offset)
                        {
                            for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutPixel += Offset, pDisplayPixel += Offset)
                                for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel, ++pDisplayPixel)
                                {
                                    *pDisplayPixel = *pOutPixel;
                                }
                        }
                        else
                        {
                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel, ++pDisplayPixel)
                                {
                                    *pDisplayPixel = *pOutPixel;
                                }
                        }
                        return true;
                    }
                    return false;
                }

                bool CContinuousRGBUndistortion::UpdateActiveImageZones(const bool Force)
                {
                    if (m_pInActiveImageZone && m_pInActiveImageZone->IsValid())
                    {
                        if (Force || (m_InActiveImageZone != *m_pInActiveImageZone) || (m_InActiveImageZone.GetContentId() != m_pInActiveImageZone->GetContentId()))
                        {
                            m_InActiveImageZone = *m_pInActiveImageZone;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else if (m_pInImage && m_pInImage->IsValid())
                    {
                        if (Force || m_InActiveImageZone.GetX0() || m_InActiveImageZone.GetY0() || (m_InActiveImageZone.GetWidth() != m_pInImage->GetWidth()) || (m_InActiveImageZone.GetHeight() != m_pInImage->GetHeight()))
                        {
                            m_InActiveImageZone.Set(m_pInImage->GetSize());
                            m_InActiveImageZone.SetContentId(0);
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    const Containers::_2D::CContinuousBoundingBox2D BoundingBox = DetermineUndistortedRegion(m_InActiveImageZone.GetX0(), m_InActiveImageZone.GetY0(), m_InActiveImageZone.GetX1(), m_InActiveImageZone.GetY1(), Real(0));
                    const Mathematics::_2D::CVector2D MinMappedPoint = (BoundingBox.GetMinPoint() * m_ScalingFactor) - m_MinScaledPoint;
                    const Mathematics::_2D::CVector2D MaxMappedPoint = (BoundingBox.GetMaxPoint() * m_ScalingFactor) - m_MinScaledPoint;
                    return m_OutActiveImageZone.Set(std::floor(MinMappedPoint.GetX()) + 1, std::floor(MinMappedPoint.GetY()) + 1, std::ceil(MaxMappedPoint.GetX()) - 1, std::ceil(MaxMappedPoint.GetY()) - 1, m_InActiveImageZone.GetContentId());
                }

                bool CContinuousRGBUndistortion::UpdateActiveMaskImage(const bool Force)
                {
                    if (m_pInActiveMaskImage && m_pInActiveMaskImage->IsValid() && m_pInImage->SizeEquals(m_pInActiveMaskImage->GetSize()))
                    {
                        if (Force || (m_OutActiveMaskImage.GetContentId() != m_pInActiveMaskImage->GetContentId()))
                        {
                            m_OutActiveMaskImage.Clear();
                            const int Width = m_OutActiveMaskImage.GetWidth();
                            const int Height = m_OutActiveMaskImage.GetHeight();
                            const MappingPixel* pMappingPixel = m_MappingImage.GetReadOnlyBuffer();
                            bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetWritableBuffer();
                            for (int Y = 0 ; Y < Height ; ++Y)
                                for (int X = 0 ; X < Width ; ++X, ++pMappingPixel, ++pOutActiveMaskPixel)
                                    if (pMappingPixel->m_Offset)
                                    {
                                        bool IsActive = true;
                                        for (int MY = pMappingPixel->m_Y0 ; IsActive && (MY <= pMappingPixel->m_Y1) ; ++MY)
                                        {
                                            const bool* pInActivePixel = m_pInActiveMaskImage->GetReadOnlyBufferAt(pMappingPixel->m_X0, MY);
                                            for (int MX = pMappingPixel->m_X0 ; MX <= pMappingPixel->m_X1 ; ++MX)
                                                if (!*pInActivePixel++)
                                                {
                                                    IsActive = false;
                                                    break;
                                                }
                                        }
                                        *pOutActiveMaskPixel = IsActive;
                                    }
                            m_OutActiveMaskImage.SetContentId(m_pInActiveMaskImage->GetContentId());
                        }
                    }
                    else
                    {
                        m_OutActiveMaskImage.Clear();
                        const int Width = m_OutActiveMaskImage.GetWidth();
                        const int Height = m_OutActiveMaskImage.GetHeight();
                        const MappingPixel* pMappingPixel = m_MappingImage.GetReadOnlyBuffer();
                        bool* pOutActiveMaskPixel = m_OutActiveMaskImage.GetWritableBuffer();
                        for (int Y = 0 ; Y < Height ; ++Y)
                            for (int X = 0 ; X < Width ; ++X, ++pMappingPixel)
                            {
                                *pOutActiveMaskPixel++ = pMappingPixel->m_Offset;
                            }
                        m_OutActiveMaskImage.SetContentId(0);
                    }
                    return true;
                }

                Real CContinuousRGBUndistortion::BicubicKernel(const Real X) const
                {
                    Real Result = (X < Real(0)) ? Real(0) : Real(6.0) * X * X * X;
                    Real Xmm = X + Real(2);
                    Result += (Xmm < Real(0)) ? Real(0) : Xmm * Xmm * Xmm;
                    Xmm = X + Real(1);
                    Result += (Xmm < Real(0)) ? Real(0) : Real(-4.0) * Xmm * Xmm * Xmm;
                    Xmm = X - Real(1);
                    Result += (Xmm < Real(0)) ? Real(0) : Real(-4.0) * Xmm * Xmm * Xmm;
                    return Result * Real(0.1666666666666666666);
                }

                bool CContinuousRGBUndistortion::IntializeStructures()
                {
                    if (m_pInImage->GetSize() == m_MonocularCameraGeometricCalibration.GetImageSize())
                    {
                        const int SourceWidth = m_pInImage->GetWidth();
                        const int SourceHeight = m_pInImage->GetHeight();
                        const Real InverseScalingFactor = Real(1) / m_ScalingFactor;
                        m_ImageBoundingBox = DetermineUndistortedRegion(0, 0, SourceWidth - 1, SourceHeight - 1, InverseScalingFactor);
                        if (m_ImageBoundingBox.HasArea())
                        {
                            const Mathematics::_2D::CVector2D MinPoint = m_ImageBoundingBox.GetMinPoint();
                            const Mathematics::_2D::CVector2D MaxPoint = m_ImageBoundingBox.GetMaxPoint();
                            m_MinScaledPoint = MinPoint * m_ScalingFactor;
                            m_MaxScaledPoint = MaxPoint * m_ScalingFactor;
                            const int X0 = std::floor(m_MinScaledPoint.GetX());
                            const int Y0 = std::floor(m_MinScaledPoint.GetY());
                            const int X1 = std::ceil(m_MaxScaledPoint.GetX());
                            const int Y1 = std::ceil(m_MaxScaledPoint.GetY());
                            const int ScaledWidth = (X1 - X0) + 1;
                            const int ScaledHeight = (Y1 - Y0) + 1;
                            m_ScaledImageSize.Set(ScaledWidth, ScaledHeight);
                            m_OutImage.Create(m_ScaledImageSize, true);
                            m_MappingImage.Create(m_ScaledImageSize, true);
                            m_OutActiveMaskImage.Create(m_ScaledImageSize, false);
                            MappingPixel* pMappingPixel = m_MappingImage.GetWritableBuffer();
                            Mathematics::_2D::CVector2D UndistortedImagePoint, DistortedImagePoint;
                            const int SourceSubWidth = SourceWidth - 1;
                            const int SourceSubHeight = SourceHeight - 1;
                            const Real MinX = MinPoint.GetX();
                            const Real MinY = MinPoint.GetY();
                            for (int Y = 0 ; Y < ScaledHeight ; ++Y)
                                for (int X = 0 ; X < ScaledWidth ; ++X, ++pMappingPixel)
                                {
                                    UndistortedImagePoint.Set(Real(X) * InverseScalingFactor + MinX, Real(Y) * InverseScalingFactor + MinY);
                                    m_MonocularCameraGeometricCalibration.Distortion(UndistortedImagePoint, DistortedImagePoint);
                                    const int XB = int(std::floor(DistortedImagePoint.GetX()));
                                    const int YB = int(std::floor(DistortedImagePoint.GetY()));
                                    pMappingPixel->m_X0 = XB - 1;
                                    pMappingPixel->m_Y0 = YB - 1;
                                    pMappingPixel->m_X1 = XB + 2;
                                    pMappingPixel->m_Y1 = YB + 2;
                                    if ((pMappingPixel->m_X0 >= 0) && (pMappingPixel->m_Y0 >= 0) && (pMappingPixel->m_X1 <= SourceSubWidth) && (pMappingPixel->m_Y1 <= SourceSubHeight))
                                    {
                                        pMappingPixel->m_Offset = (SourceWidth * pMappingPixel->m_Y0) + pMappingPixel->m_X0;
                                        const Real Dx = DistortedImagePoint.GetX() - XB;
                                        const Real Dy = DistortedImagePoint.GetY() - YB;
                                        const Real Kmy[4] = { BicubicKernel(Dy + Real(1)), BicubicKernel(Dy), BicubicKernel(Dy - Real(1)), BicubicKernel(Dy - Real(2)) };
                                        const Real Kmx[4] = { BicubicKernel(-Dx - Real(1)), BicubicKernel(-Dx), BicubicKernel(-Dx + Real(1)), BicubicKernel(-Dx + Real(2)) };
                                        Real Accumulator = Real(0);
                                        for (int i = 0 ; i < 4 ; ++i)
                                            for (int j = 0 ; j < 4 ; ++j)
                                            {
                                                pMappingPixel->m_Wij[i][j] = Kmy[i] * Kmx[j];
                                                Accumulator += pMappingPixel->m_Wij[i][j];
                                            }
                                        for (auto& i : pMappingPixel->m_Wij)
                                            for (int j = 0 ; j < 4 ; ++j)
                                            {
                                                i[j] /= Accumulator;
                                            }
                                    }
                                }
                            if (UpdateActiveImageZones(true))
                            {
                                return UpdateActiveMaskImage(true);
                            }
                        }
                    }
                    return false;
                }

                Containers::_2D::CContinuousBoundingBox2D CContinuousRGBUndistortion::DetermineUndistortedRegion(const int X0, const int Y0, const int X1, const int Y1, const Real Margin)
                {
                    Containers::_2D::CContinuousBoundingBox2D BoundingBox;
                    Mathematics::_2D::CVector2D P(X0, Y0), Q(X0, Y1), A, B;
                    for (int X = X0 ; X <= X1 ; ++X)
                    {
                        P.SetX(X);
                        Q.SetX(X);
                        m_MonocularCameraGeometricCalibration.Undistortion(P, A);
                        m_MonocularCameraGeometricCalibration.Undistortion(Q, B);
                        BoundingBox.Extend(A);
                        BoundingBox.Extend(B);
                    }
                    P.Set(X0, Y0);
                    Q.Set(X1, Y0);
                    for (int Y = Y0 ; Y <= Y1 ; ++Y)
                    {
                        P.SetY(Y);
                        Q.SetY(Y);
                        m_MonocularCameraGeometricCalibration.Undistortion(P, A);
                        m_MonocularCameraGeometricCalibration.Undistortion(Q, B);
                        BoundingBox.Extend(A);
                        BoundingBox.Extend(B);
                    }
                    if (IsPositive(Margin))
                    {
                        BoundingBox.Expand(Margin);
                    }
                    return BoundingBox;
                }
            }
        }
    }
}
