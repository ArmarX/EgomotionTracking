/*
 * ContinuousDemosaicing.cpp
 */

#include "../../../Files/OutFile.h"
#include "ContinuousDemosaicing.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Demosaicing
            {
                const TImage<Real>* CContinuousDemosaicing::Mosaicing(const TImage<DiscreteRGBPixel>* pDiscreteRGBImage, const CBayerPattern::BayerPatternType PatternType)
                {
                    if (pDiscreteRGBImage && pDiscreteRGBImage->IsValid())
                    {
                        CBayerPattern SelectedPattern(PatternType);
                        CBayerPattern::ChannelContentMap ChannelContentMap = SelectedPattern.GetBayerPatternMap();
                        TImage<Real>* pMosaicImage = new TImage<Real>(pDiscreteRGBImage->GetSize());
                        Real* pMosaicPixel = pMosaicImage->GetWritableBuffer();
                        const DiscreteRGBPixel* pDiscreteRGBPixel = pDiscreteRGBImage->GetReadOnlyBuffer();
                        const int Width = pDiscreteRGBImage->GetWidth();
                        const int Height = pDiscreteRGBImage->GetHeight();
                        for (int Y = 0 ; Y < Height ; ++Y)
                            for (int X = 0 ; X < Width ; ++X, ++pDiscreteRGBPixel)
                            {
                                *pMosaicPixel++ = pDiscreteRGBPixel->m_Channels[ChannelContentMap.m_YX[Y & 0X1][X & 0X1]];
                            }
                        return pMosaicImage;
                    }
                    return nullptr;
                }

                Real CContinuousDemosaicing::PeakSignalToNoiseRatio(const TImage<DiscreteRGBPixel>* pDiscreteRGBImage, const TImage<ContinuousRGBPixel>* pContinuousRGBImage, const int X0, const int Y0, const int X1, const int Y1)
                {
                    if (pDiscreteRGBImage && pContinuousRGBImage && pDiscreteRGBImage->IsValid() && pContinuousRGBImage->IsValid() && pDiscreteRGBImage->SizeEquals(pContinuousRGBImage->GetWidth(), pContinuousRGBImage->GetHeight()) && (X0 >= 0) && (Y0 >= 0) && (X1 >= X0) && (Y1 >= Y0) && (X1 < pContinuousRGBImage->GetWidth()) && (Y1 < pContinuousRGBImage->GetHeight()))
                    {
                        Real SquareAccumulator = Real(0);
                        for (int Y = Y0 ; Y <= Y1 ; ++Y)
                        {
                            const DiscreteRGBPixel* pDiscreteRGBPixel = pDiscreteRGBImage->GetReadOnlyBufferAt(X0, Y0);
                            const ContinuousRGBPixel* pContinuousRGBPixel = pContinuousRGBImage->GetReadOnlyBufferAt(X0, Y0);
                            for (int X = X0 ; X <= X1 ; ++X, ++pDiscreteRGBPixel, ++pContinuousRGBPixel)
                            {
                                const Real DR = pContinuousRGBPixel->m_Elements.m_R - pDiscreteRGBPixel->m_Elements.m_R;
                                const Real DG = pContinuousRGBPixel->m_Elements.m_G - pDiscreteRGBPixel->m_Elements.m_G;
                                const Real DB = pContinuousRGBPixel->m_Elements.m_B - pDiscreteRGBPixel->m_Elements.m_B;
                                SquareAccumulator += (DR * DR + DG * DG + DB * DB);
                            }
                        }
                        const Real MSE = SquareAccumulator / Real(3 * (X1 - X0 + 1) * (Y1 - Y0 + 1));
                        return Real(10) * std::log10(Real(65025) / MSE);
                    }
                    return Real(0);
                }

#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_

                bool CContinuousDemosaicing::SortParametrizedTrialByPSNR(const ParametrizedTrial& lhs, const ParametrizedTrial& rhs)
                {
                    return lhs.m_PSNR > rhs.m_PSNR;
                }

                std::list<CContinuousDemosaicing::ParametrizedTrial> CContinuousDemosaicing::EstimateOptimalParameters(const std::list<const TImage<DiscreteRGBPixel>*>& DiscreteRGBImages, const CContinuousDemosaicing::DemosaicingMethod Method, const CBayerPattern::BayerPatternType PatternType, const bool FullDisplay)
                {
                    std::list<ParametrizedTrial> ParametrizedTrials;
                    ParametrizedTrial CurrentParametrizedTrial;
                    for (std::list<const TImage<DiscreteRGBPixel>*>::const_iterator ppDiscreteRGBImage = DiscreteRGBImages.begin() ; ppDiscreteRGBImage != DiscreteRGBImages.end() ; ++ppDiscreteRGBImage, ++CurrentParametrizedTrial.m_ImageIndex)
                    {
                        const TImage<Real>* pMosaicImage = Mosaicing(*ppDiscreteRGBImage, PatternType);
                        CContinuousDemosaicing ContinuousDemosaicing(pMosaicImage, nullptr, nullptr, PatternType);
                        ContinuousDemosaicing.SetDemosaicingMethod(Method);
                        CurrentParametrizedTrial.m_Method = Method;
                        Real MaximalPSNR = Real(0);
                        switch (Method)
                        {
                            case eBilinear:
                            {
                                if (ContinuousDemosaicing.BilinearDemosaicing())
                                {
                                    const CActiveImageZone* pOutActiveImageZone = ContinuousDemosaicing.GetOutActiveImageZone();
                                    CurrentParametrizedTrial.m_PSNR = PeakSignalToNoiseRatio(*ppDiscreteRGBImage, ContinuousDemosaicing.GetOutImage(), pOutActiveImageZone->GetX0(), pOutActiveImageZone->GetY0(), pOutActiveImageZone->GetX1(), pOutActiveImageZone->GetY1());
                                }
                                else
                                {
                                    CurrentParametrizedTrial.m_PSNR = Real(-1);
                                }
                                CurrentParametrizedTrial.ToConsole();
                                ParametrizedTrials.push_back(CurrentParametrizedTrial);
                            }
                            break;
                            case eDirectionalGradient:
                            {
                                const Real DeltaParameterA = Real(1) / Real(40);
                                const Real DeltaParameterB = Real(1) / Real(40);
                                for (CurrentParametrizedTrial.m_Mode = 0; CurrentParametrizedTrial.m_Mode <= 4 ; ++CurrentParametrizedTrial.m_Mode)
                                {
                                    if (CurrentParametrizedTrial.m_Mode >= 4)
                                    {
                                        Real ModeParameter0, ModeParameter1, DeltaModeParameter;
                                        switch (CurrentParametrizedTrial.m_Mode)
                                        {
                                            case 4:
                                                ModeParameter0 = Real(8);
                                                ModeParameter1 = Real(64);
                                                DeltaModeParameter = Real(1) / Real(2);
                                                break;
                                        }
                                        for (CurrentParametrizedTrial.m_ModeParameter = ModeParameter0; CurrentParametrizedTrial.m_ModeParameter <= ModeParameter1 ; CurrentParametrizedTrial.m_ModeParameter += DeltaModeParameter)
                                            for (CurrentParametrizedTrial.m_ParameterA = Real(0); CurrentParametrizedTrial.m_ParameterA <= Real(1) ; CurrentParametrizedTrial.m_ParameterA += DeltaParameterA)
                                                for (CurrentParametrizedTrial.m_ParameterB = Real(0); CurrentParametrizedTrial.m_ParameterB <= Real(1) ; CurrentParametrizedTrial.m_ParameterB += DeltaParameterB)
                                                {
                                                    _RVL_TIME_LOGGER_BEGIN_(0, "A");
                                                    const bool Result = ContinuousDemosaicing.DirectionalGradientDemosaicing(&ContinuousDemosaicing.m_OutImage, &ContinuousDemosaicing.m_OutActiveImageZone, CurrentParametrizedTrial.m_ParameterA, CurrentParametrizedTrial.m_ParameterB, CurrentParametrizedTrial.m_Mode, CurrentParametrizedTrial.m_ModeParameter);
                                                    CurrentParametrizedTrial.m_ElapsedTime = _RVL_TIME_LOGGER_END_(0, "A");

                                                    if (Result)
                                                    {
                                                        const CActiveImageZone* pOutActiveImageZone = ContinuousDemosaicing.GetOutActiveImageZone();
                                                        CurrentParametrizedTrial.m_PSNR = PeakSignalToNoiseRatio(*ppDiscreteRGBImage, ContinuousDemosaicing.GetOutImage(), pOutActiveImageZone->GetX0(), pOutActiveImageZone->GetY0(), pOutActiveImageZone->GetX1(), pOutActiveImageZone->GetY1());
                                                    }
                                                    else
                                                    {
                                                        CurrentParametrizedTrial.m_PSNR = Real(-1);
                                                    }

                                                    if (FullDisplay || (std::isnormal(CurrentParametrizedTrial.m_PSNR) && (CurrentParametrizedTrial.m_PSNR > MaximalPSNR)))
                                                    {
                                                        CurrentParametrizedTrial.ToConsole();
                                                        MaximalPSNR = CurrentParametrizedTrial.m_PSNR;
                                                        ParametrizedTrials.push_back(CurrentParametrizedTrial);
                                                    }
                                                }
                                    }
                                    else
                                        for (CurrentParametrizedTrial.m_ParameterA = Real(0); CurrentParametrizedTrial.m_ParameterA <= Real(1) ; CurrentParametrizedTrial.m_ParameterA += DeltaParameterA)
                                            for (CurrentParametrizedTrial.m_ParameterB = Real(0); CurrentParametrizedTrial.m_ParameterB <= Real(1) ; CurrentParametrizedTrial.m_ParameterB += DeltaParameterB)
                                            {
                                                _RVL_TIME_LOGGER_BEGIN_(0, "A");
                                                const bool Result = ContinuousDemosaicing.DirectionalGradientDemosaicing(&ContinuousDemosaicing.m_OutImage, &ContinuousDemosaicing.m_OutActiveImageZone, CurrentParametrizedTrial.m_ParameterA, CurrentParametrizedTrial.m_ParameterB, CurrentParametrizedTrial.m_Mode, CurrentParametrizedTrial.m_ModeParameter);
                                                CurrentParametrizedTrial.m_ElapsedTime = _RVL_TIME_LOGGER_END_(0, "A");

                                                if (Result)
                                                {
                                                    const CActiveImageZone* pOutActiveImageZone = ContinuousDemosaicing.GetOutActiveImageZone();
                                                    CurrentParametrizedTrial.m_PSNR = PeakSignalToNoiseRatio(*ppDiscreteRGBImage, ContinuousDemosaicing.GetOutImage(), pOutActiveImageZone->GetX0(), pOutActiveImageZone->GetY0(), pOutActiveImageZone->GetX1(), pOutActiveImageZone->GetY1());
                                                }
                                                else
                                                {
                                                    CurrentParametrizedTrial.m_PSNR = Real(-1);
                                                }

                                                if (FullDisplay || (std::isnormal(CurrentParametrizedTrial.m_PSNR) && (CurrentParametrizedTrial.m_PSNR > MaximalPSNR)))
                                                {
                                                    CurrentParametrizedTrial.ToConsole();
                                                    MaximalPSNR = CurrentParametrizedTrial.m_PSNR;
                                                    ParametrizedTrials.push_back(CurrentParametrizedTrial);
                                                }
                                            }
                                }
                            }
                            break;
                            case eBilateralAdaptive:
                            {
                                const Real DeltaParameterA = Real(1) / Real(10);
                                const Real DeltaParameterB = Real(1) / Real(10);
                                for (CurrentParametrizedTrial.m_ParameterA = Real(1) / Real(10); CurrentParametrizedTrial.m_ParameterA <= Real(10) ; CurrentParametrizedTrial.m_ParameterA += DeltaParameterA)
                                    for (CurrentParametrizedTrial.m_ParameterB = Real(1) / Real(10); CurrentParametrizedTrial.m_ParameterB <= Real(10) ; CurrentParametrizedTrial.m_ParameterB += DeltaParameterB)
                                    {
                                        if (ContinuousDemosaicing.BilateralAdaptiveDemosaicing(CurrentParametrizedTrial.m_ParameterA, CurrentParametrizedTrial.m_ParameterB, CurrentParametrizedTrial.m_Mode, CurrentParametrizedTrial.m_ModeParameter))
                                        {
                                            const CActiveImageZone* pOutActiveImageZone = ContinuousDemosaicing.GetOutActiveImageZone();
                                            CurrentParametrizedTrial.m_PSNR = PeakSignalToNoiseRatio(*ppDiscreteRGBImage, ContinuousDemosaicing.GetOutImage(), pOutActiveImageZone->GetX0(), pOutActiveImageZone->GetY0(), pOutActiveImageZone->GetX1(), pOutActiveImageZone->GetY1());
                                        }
                                        else
                                        {
                                            CurrentParametrizedTrial.m_PSNR = Real(-1);
                                        }

                                        if (FullDisplay || (std::isnormal(CurrentParametrizedTrial.m_PSNR) && (CurrentParametrizedTrial.m_PSNR > MaximalPSNR)))
                                        {
                                            CurrentParametrizedTrial.ToConsole();
                                            MaximalPSNR = CurrentParametrizedTrial.m_PSNR;
                                            ParametrizedTrials.push_back(CurrentParametrizedTrial);
                                        }
                                    }
                            }
                            break;
                        }
                        delete pMosaicImage;
                        pMosaicImage = nullptr;
                    }
                    ParametrizedTrials.sort(SortParametrizedTrialByPSNR);
                    return ParametrizedTrials;
                }

                bool CContinuousDemosaicing::ExportParametrizedTrialsToFile(const std::list<ParametrizedTrial>& ParametrizedTrials, const std::string FileName)
                {
                    if ((!ParametrizedTrials.size()) || FileName.empty())
                    {
                        return false;
                    }
                    std::string Content;
                    for (const auto& pParametrizedTrial : ParametrizedTrials)
                    {
                        Content += pParametrizedTrial.ToString();
                    }
                    return Files::COutFile::WriteStringToFile(FileName, Content);
                }
#endif

                CContinuousDemosaicing::CContinuousDemosaicing(const TImage<Real>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone, const CBayerPattern::BayerPatternType PatternType) :
                    TImageProcess<Real, ContinuousRGBPixel>(pInImage, pInActiveMaskImage, pInActiveImageZone),
                    m_DemosaicingMethod(eDirectionalGradient),
                    m_ExtractHomogenityImage(true),
                    m_ApproximationImage(),
                    m_BayerPattern(PatternType),
                    m_ConvolutionGaussianKernel()
                {
                    if (IsEnabled())
                    {
                        m_OutImage.Create(m_pInImage->GetSize(), true);
                        m_ApproximationImage.Create(m_pInImage->GetSize(), true);
                        m_HomogenityImage.Create(m_pInImage->GetSize(), true);
                        InitializeActiveMaskImage();
                        SetEnabled(UpdateActiveMaskImage(true) && UpdateActiveImageZone(true));
                    }
                }

                CContinuousDemosaicing::~CContinuousDemosaicing()
                    = default;

                bool CContinuousDemosaicing::SetDemosaicingMethod(const DemosaicingMethod Method)
                {
                    if ((Method == eBilinear) || (Method == eDirectionalGradient) || (Method == eBilateralAdaptive))
                    {
                        if (Method != m_DemosaicingMethod)
                        {
                            UpdateActiveMaskImage(true);
                            UpdateActiveImageZone(true);
                            m_DemosaicingMethod = Method;
                        }
                        return true;
                    }
                    return false;
                }

                bool CContinuousDemosaicing::SetBayerPatternType(const CBayerPattern::BayerPatternType PatternType)
                {
                    return m_BayerPattern.SetType(PatternType);
                }

                void CContinuousDemosaicing::SetExtractHomogenityImage(const bool ExtractHomogenityImage)
                {
                    m_ExtractHomogenityImage = ExtractHomogenityImage;
                }

                void CContinuousDemosaicing::SetMasking(const bool Masking)
                {
                    TImageProcess<Real, ContinuousRGBPixel>::SetMasking(Masking);
                    UpdateActiveMaskImage(true);
                }

                bool CContinuousDemosaicing::Execute(const int Trial)
                {
                    _RVL_TIME_LOGGER_BEGIN_(0, "CContinuousDemosaicing::Execute");
                    bool Result = false;
                    if (IsEnabled() && ((!m_pInActiveImageZone) || (m_pInActiveImageZone->IsValid() && m_pInImage->IsInside(m_pInActiveImageZone))))
                    {
                        if (IsMasking() && (!UpdateActiveMaskImage(false)))
                        {
                            _RVL_TIME_LOGGER_VERBOSE_END_(0, "CContinuousDemosaicing::Execute");
                            return false;
                        }
                        if (!UpdateActiveImageZone(false))
                        {
                            _RVL_TIME_LOGGER_VERBOSE_END_(0, "CContinuousDemosaicing::Execute");
                            return false;
                        }
                        switch (m_DemosaicingMethod)
                        {
                            case eBilinear:
                                Result = BilinearDemosaicing();
                                break;
                            case eDirectionalGradient:
                                Result = DirectionalGradientDemosaicing(&m_OutImage, &m_OutActiveImageZone);
                                break;
                            case eBilateralAdaptive:
                                Result = BilateralAdaptiveDemosaicing();
                                break;
                        }
                    }
                    _RVL_TIME_LOGGER_VERBOSE_END_(0, "CContinuousDemosaicing::Execute");
                    return Result;
                }

                bool CContinuousDemosaicing::Display(TImage<DiscreteRGBPixel>* pDisplayImage)
                {
                    if (pDisplayImage && pDisplayImage->IsValid() && pDisplayImage->HasBufferOwnership() && pDisplayImage->SizeEquals(m_OutImage.GetSize()))
                    {
                        pDisplayImage->Clear();
                        const int X0 = m_OutActiveImageZone.GetX0();
                        const int Y0 = m_OutActiveImageZone.GetY0();
                        const int X1 = m_OutActiveImageZone.GetX1();
                        const int Y1 = m_OutActiveImageZone.GetY1();
                        const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                        DiscreteRGBPixel* pDisplayPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                        const ContinuousRGBPixel* pOutPixel = m_OutImage.GetReadOnlyBufferAt(X0, Y0);
                        if (IsMasking())
                        {
                            const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                            if (Offset)
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pActiveMaskPixel += Offset, pOutPixel += Offset, pDisplayPixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel, ++pDisplayPixel)
                                        if (*pActiveMaskPixel++)
                                        {
                                            *pDisplayPixel = *pOutPixel;
                                        }
                            }
                            else
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel, ++pDisplayPixel)
                                        if (*pActiveMaskPixel++)
                                        {
                                            *pDisplayPixel = *pOutPixel;
                                        }
                            }
                        }
                        else
                        {
                            if (Offset)
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutPixel += Offset, pDisplayPixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel, ++pDisplayPixel)
                                    {
                                        *pDisplayPixel = *pOutPixel;
                                    }
                            }
                            else
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel, ++pDisplayPixel)
                                    {
                                        *pDisplayPixel = *pOutPixel;
                                    }
                            }
                        }
                        return true;
                    }
                    return false;
                }

                const TImage<Real>* CContinuousDemosaicing::GetHomogenityImage() const
                {
                    return &m_HomogenityImage;
                }

                bool CContinuousDemosaicing::BilinearDemosaicing()
                {
                    if ((m_pInActiveImageZone ? m_OutActiveImageZone.Set(*m_pInActiveImageZone, 1) : m_OutActiveImageZone.Set(1, 1, m_pInImage->GetWidth() - 2, m_pInImage->GetHeight() - 2)))
                    {
                        const int X0 = m_OutActiveImageZone.GetX0();
                        const int Y0 = m_OutActiveImageZone.GetY0();
                        const int X1 = m_OutActiveImageZone.GetX1();
                        const int Y1 = m_OutActiveImageZone.GetY1();
                        const int Width = m_pInImage->GetWidth();
                        const int NE = 1 - Width, NW = -Width - 1, SW = Width - 1, SE = Width + 1;
                        CBayerPattern::ChannelFlagMap GreenFlagMap = m_BayerPattern.GetBayerPatternMapByChannelContent(CBayerPattern::eGreen);
                        CBayerPattern::ChannelFlagMap RedFlagMap = m_BayerPattern.GetBayerPatternMapByChannelContent(CBayerPattern::eRed);
                        if (IsMasking())
                        {
                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                            {
                                const int XB = GreenFlagMap.m_YX[Y & 0X1][X0 & 0X1] ? X0 : X0 + 1;
                                const Real* pInPixel = m_pInImage->GetReadOnlyBufferAt(XB, Y);
                                ContinuousRGBPixel* pOutPixel = m_OutImage.GetWritableBufferAt(XB, Y);
                                const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(XB, Y);
                                if (RedFlagMap.m_YX[Y & 0X1][(XB + 1) & 0X1])
                                {
                                    for (int X = XB ; X <= X1 ; X += 2, pActiveMaskPixel += 2, pOutPixel += 2, pInPixel += 2)
                                        if (*pActiveMaskPixel)
                                        {
                                            pOutPixel->Set((pInPixel[-1] + pInPixel[1]) * Real(0.5), pInPixel[0], (pInPixel[-Width] + pInPixel[Width]) * Real(0.5));
                                        }
                                }
                                else
                                {
                                    for (int X = XB ; X <= X1 ; X += 2, pActiveMaskPixel += 2, pOutPixel += 2, pInPixel += 2)
                                        if (*pActiveMaskPixel)
                                        {
                                            pOutPixel->Set((pInPixel[-Width] + pInPixel[Width]) * Real(0.5), pInPixel[0], (pInPixel[-1] + pInPixel[1]) * Real(0.5));
                                        }
                                }
                            }
                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                            {
                                const int XB = GreenFlagMap.m_YX[Y & 0X1][X0 & 0X1] ? X0 + 1 : X0;
                                const Real* pInPixel = m_pInImage->GetReadOnlyBufferAt(XB, Y);
                                ContinuousRGBPixel* pOutPixel = m_OutImage.GetWritableBufferAt(XB, Y);
                                const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(XB, Y);
                                if (RedFlagMap.m_YX[Y & 0X1][XB & 0X1])
                                {
                                    for (int X = XB ; X <= X1 ; X += 2, pActiveMaskPixel += 2, pOutPixel += 2, pInPixel += 2)
                                        if (*pActiveMaskPixel)
                                        {
                                            pOutPixel->Set(pInPixel[0], (pInPixel[1] + pInPixel[-1] + pInPixel[-Width] + pInPixel[Width]) * Real(0.25), (pInPixel[NE] + pInPixel[NW] + pInPixel[SW] + pInPixel[SE]) * Real(0.25));
                                        }
                                }
                                else
                                {
                                    for (int X = XB ; X <= X1 ; X += 2, pActiveMaskPixel += 2, pOutPixel += 2, pInPixel += 2)
                                        if (*pActiveMaskPixel)
                                        {
                                            pOutPixel->Set((pInPixel[NE] + pInPixel[NW] + pInPixel[SW] + pInPixel[SE]) * Real(0.25), (pInPixel[1] + pInPixel[-1] + pInPixel[-Width] + pInPixel[Width]) * Real(0.25), pInPixel[0]);
                                        }
                                }
                            }
                        }
                        else
                        {
                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                            {
                                const int XB = GreenFlagMap.m_YX[Y & 0X1][X0 & 0X1] ? X0 : X0 + 1;
                                const Real* pInPixel = m_pInImage->GetReadOnlyBufferAt(XB, Y);
                                ContinuousRGBPixel* pOutPixel = m_OutImage.GetWritableBufferAt(XB, Y);
                                if (RedFlagMap.m_YX[Y & 0X1][(XB + 1) & 0X1])
                                    for (int X = XB ; X <= X1 ; X += 2, pOutPixel += 2, pInPixel += 2)
                                    {
                                        pOutPixel->Set((pInPixel[-1] + pInPixel[1]) * Real(0.5), pInPixel[0], (pInPixel[-Width] + pInPixel[Width]) * Real(0.5));
                                    }
                                else
                                    for (int X = XB ; X <= X1 ; X += 2, pOutPixel += 2, pInPixel += 2)
                                    {
                                        pOutPixel->Set((pInPixel[-Width] + pInPixel[Width]) * Real(0.5), pInPixel[0], (pInPixel[-1] + pInPixel[1]) * Real(0.5));
                                    }
                            }
                            for (int Y = Y0 ; Y <= Y1 ; ++Y)
                            {
                                const int XB = GreenFlagMap.m_YX[Y & 0X1][X0 & 0X1] ? X0 + 1 : X0;
                                const Real* pInPixel = m_pInImage->GetReadOnlyBufferAt(XB, Y);
                                ContinuousRGBPixel* pOutPixel = m_OutImage.GetWritableBufferAt(XB, Y);
                                if (RedFlagMap.m_YX[Y & 0X1][XB & 0X1])
                                    for (int X = XB ; X <= X1 ; X += 2, pOutPixel += 2, pInPixel += 2)
                                    {
                                        pOutPixel->Set(pInPixel[0], (pInPixel[1] + pInPixel[-1] + pInPixel[-Width] + pInPixel[Width]) * Real(0.25), (pInPixel[NE] + pInPixel[NW] + pInPixel[SW] + pInPixel[SE]) * Real(0.25));
                                    }
                                else
                                    for (int X = XB ; X <= X1 ; X += 2, pOutPixel += 2, pInPixel += 2)
                                    {
                                        pOutPixel->Set((pInPixel[NE] + pInPixel[NW] + pInPixel[SW] + pInPixel[SE]) * Real(0.25), (pInPixel[1] + pInPixel[-1] + pInPixel[-Width] + pInPixel[Width]) * Real(0.25), pInPixel[0]);
                                    }
                            }
                        }
                        return true;
                    }
                    return false;
                }

#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                bool CContinuousDemosaicing::DirectionalGradientDemosaicing(TImage<ContinuousRGBPixel>* pSelectedOutImage, CActiveImageZone* pOutActiveImageZone, const Real LuminanceFactor, const Real ChromaticFactor, const int Mode, const Real ModeParameter)
#else
                bool CContinuousDemosaicing::DirectionalGradientDemosaicing(TImage<ContinuousRGBPixel>* pSelectedOutImage, CActiveImageZone* pOutActiveImageZone, const bool UseOutImage, const Real LuminanceFactor, const Real ChromaticFactor)
#endif
                {
                    int AX0, AY0, AX1, AY1;
                    if (m_pInActiveImageZone)
                    {
                        AX0 = m_pInActiveImageZone->GetX0();
                        AY0 = m_pInActiveImageZone->GetY0();
                        AX1 = m_pInActiveImageZone->GetX1();
                        AY1 = m_pInActiveImageZone->GetY1();
                    }
                    else
                    {
                        AX0 = 0;
                        AY0 = 0;
                        AX1 = m_pInImage->GetWidth() - 1;
                        AY1 = m_pInImage->GetHeight() - 1;
                    }
                    if (pOutActiveImageZone->Set(AX0 + 4, AY0 + 4, AX1 - 4, AY1 - 4))
                    {
                        const int Width = m_pInImage->GetWidth();
                        CBayerPattern::ChannelContentMap ChannelContentMap = m_BayerPattern.GetBayerPatternMap();
                        if (IsMasking())
                        {
                            for (int Y = AY0 ; Y <= AY1 ; ++Y)
                            {
                                const Real* pInPixel = m_pInImage->GetReadOnlyBufferAt(AX0, Y);
                                ContinuousRGBPixel* pOutPixel = pSelectedOutImage->GetWritableBufferAt(AX0, Y);
                                const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(AX0, Y);
                                const CBayerPattern::Channel* pRowMap = ChannelContentMap.m_YX[Y & 0x1];
                                for (int X = AX0 ; X <= AX1 ; ++X, ++pOutPixel, ++pInPixel)
                                    if (*pActiveMaskPixel++)
                                    {
                                        pOutPixel->m_Channels[pRowMap[X & 0x1]] = *pInPixel;
                                    }
                            }
                        }
                        else
                        {
                            for (int Y = AY0 ; Y <= AY1 ; ++Y)
                            {
                                const Real* pInPixel = m_pInImage->GetReadOnlyBufferAt(AX0, Y);
                                ContinuousRGBPixel* pOutPixel = pSelectedOutImage->GetWritableBufferAt(AX0, Y);
                                const CBayerPattern::Channel* pRowMap = ChannelContentMap.m_YX[Y & 0x1];
                                for (int X = AX0 ; X <= AX1 ; ++X, ++pOutPixel)
                                {
                                    pOutPixel->m_Channels[pRowMap[X & 0x1]] = *pInPixel++;
                                }
                            }
                        }

                        const Real DW0 = Real(1);
                        const Real DW2 = Real(0.5);
                        const Real DW11 = Real(1.0) / g_RealSquareRoot2;
                        const Real DW22 = Real(1.0) / g_RealSquareRoot2;
                        const Real DW0_LuminanceFactor = DW0 * LuminanceFactor;
                        const Real DW0_ChromaticFactor = DW0 * ChromaticFactor;
                        const Real DW2_ChromaticFactor = DW2 * ChromaticFactor;
                        const Real DW2_LuminanceFactor = DW2 * LuminanceFactor;
                        const Real DW22_ChromaticFactor = DW22 * ChromaticFactor;
                        const Real DW11_LuminanceFactor = DW11 * LuminanceFactor;

                        const int BX0 = AX0 + 2;
                        const int BY0 = AY0 + 2;
                        const int BX1 = AX1 - 2;
                        const int BY1 = AY1 - 2;

                        const int N = -Width, S = Width, E = 1, W = -1, N2 = Width * -2, S2 = Width * 2, E2 = 2, W2 = -2;
                        if (IsMasking())
                        {
                            for (int Y = BY0 ; Y <= BY1 ; ++Y)
                            {
                                const int XB = (ChannelContentMap.m_YX[Y & 0X1][BX0 & 0X1] == CBayerPattern::eGreen) ? BX0 + 1 : BX0;
                                const Real* pInPixel = m_pInImage->GetReadOnlyBufferAt(XB, Y);
                                ContinuousRGBPixel* pOutPixel = pSelectedOutImage->GetWritableBufferAt(XB, Y);
                                const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(XB, Y);
                                for (int X = XB ; X <= BX1 ; X += 2, pActiveMaskPixel += 2, pInPixel += 2, pOutPixel += 2)
                                    if (*pActiveMaskPixel)
                                    {
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                                        const Real GV = std::abs(pInPixel[N] - pInPixel[S]) * DW0_LuminanceFactor;
                                        const Real GN = GradientWeightingKernel(GV + std::abs(*pInPixel - pInPixel[N2]) * DW2_ChromaticFactor, Mode, ModeParameter);
                                        const Real GS = GradientWeightingKernel(GV + std::abs(*pInPixel - pInPixel[S2]) * DW2_ChromaticFactor, Mode, ModeParameter);
                                        const Real GH = std::abs(pInPixel[E] - pInPixel[W]) * DW0_LuminanceFactor;
                                        const Real GE = GradientWeightingKernel(GH + std::abs(*pInPixel - pInPixel[E2]) * DW2_ChromaticFactor, Mode, ModeParameter);
                                        const Real GW = GradientWeightingKernel(GH + std::abs(*pInPixel - pInPixel[W2]) * DW2_ChromaticFactor, Mode, ModeParameter);
#else
                                        const Real GV = std::abs(pInPixel[N] - pInPixel[S]) * DW0_LuminanceFactor;
                                        const Real GN = GradientWeightingKernel(GV + std::abs(*pInPixel - pInPixel[N2]) * DW2_ChromaticFactor);
                                        const Real GS = GradientWeightingKernel(GV + std::abs(*pInPixel - pInPixel[S2]) * DW2_ChromaticFactor);
                                        const Real GH = std::abs(pInPixel[E] - pInPixel[W]) * DW0_LuminanceFactor;
                                        const Real GE = GradientWeightingKernel(GH + std::abs(*pInPixel - pInPixel[E2]) * DW2_ChromaticFactor);
                                        const Real GW = GradientWeightingKernel(GH + std::abs(*pInPixel - pInPixel[W2]) * DW2_ChromaticFactor);
#endif
                                        pOutPixel->m_Elements.m_G = (GN * pInPixel[N] + GS * pInPixel[S] + GE * pInPixel[E] + GW * pInPixel[W]) / (GN + GS + GE + GW);
                                    }
                            }
                        }
                        else
                        {
                            for (int Y = BY0 ; Y <= BY1 ; ++Y)
                            {
                                const int XB = (ChannelContentMap.m_YX[Y & 0X1][BX0 & 0X1] == CBayerPattern::eGreen) ? BX0 + 1 : BX0;
                                const Real* pInPixel = m_pInImage->GetReadOnlyBufferAt(XB, Y);
                                ContinuousRGBPixel* pOutPixel = pSelectedOutImage->GetWritableBufferAt(XB, Y);
                                for (int X = XB ; X <= BX1 ; X += 2, pInPixel += 2, pOutPixel += 2)
                                {
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                                    const Real GV = std::abs(pInPixel[N] - pInPixel[S]) * DW0_LuminanceFactor;
                                    const Real GN = GradientWeightingKernel(GV + std::abs(*pInPixel - pInPixel[N2]) * DW2_ChromaticFactor, Mode, ModeParameter);
                                    const Real GS = GradientWeightingKernel(GV + std::abs(*pInPixel - pInPixel[S2]) * DW2_ChromaticFactor, Mode, ModeParameter);
                                    const Real GH = std::abs(pInPixel[E] - pInPixel[W]) * DW0_LuminanceFactor;
                                    const Real GE = GradientWeightingKernel(GH + std::abs(*pInPixel - pInPixel[E2]) * DW2_ChromaticFactor, Mode, ModeParameter);
                                    const Real GW = GradientWeightingKernel(GH + std::abs(*pInPixel - pInPixel[W2]) * DW2_ChromaticFactor, Mode, ModeParameter);
#else
                                    const Real GV = std::abs(pInPixel[N] - pInPixel[S]) * DW0_LuminanceFactor;
                                    const Real GN = GradientWeightingKernel(GV + std::abs(*pInPixel - pInPixel[N2]) * DW2_ChromaticFactor);
                                    const Real GS = GradientWeightingKernel(GV + std::abs(*pInPixel - pInPixel[S2]) * DW2_ChromaticFactor);
                                    const Real GH = std::abs(pInPixel[E] - pInPixel[W]) * DW0_LuminanceFactor;
                                    const Real GE = GradientWeightingKernel(GH + std::abs(*pInPixel - pInPixel[E2]) * DW2_ChromaticFactor);
                                    const Real GW = GradientWeightingKernel(GH + std::abs(*pInPixel - pInPixel[W2]) * DW2_ChromaticFactor);
#endif
                                    pOutPixel->m_Elements.m_G = (GN * pInPixel[N] + GS * pInPixel[S] + GE * pInPixel[E] + GW * pInPixel[W]) / (GN + GS + GE + GW);
                                }
                            }
                        }

                        const int CX0 = BX0 + 1;
                        const int CY0 = BY0 + 1;
                        const int CX1 = BX1 - 1;
                        const int CY1 = BY1 - 1;
                        const int NE = 1 - Width, NW = -Width - 1, SE = Width + 1, SW = Width - 1;
                        if (IsMasking())
                        {
                            for (int Y = CY0 ; Y <= CY1 ; ++Y)
                            {
                                const int XB = (ChannelContentMap.m_YX[Y & 0X1][CX0 & 0X1] == CBayerPattern::eGreen) ? CX0 + 1 : CX0;
                                const Real* pInPixel = m_pInImage->GetReadOnlyBufferAt(XB, Y);
                                ContinuousRGBPixel* pOutPixel = pSelectedOutImage->GetWritableBufferAt(XB, Y);
                                const CBayerPattern::Channel CurrentChannel = (ChannelContentMap.m_YX[Y & 0x1][XB & 0x1] == CBayerPattern::eRed) ? CBayerPattern::eBlue : CBayerPattern::eRed;
                                const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(XB, Y);
                                for (int X = XB ; X <= CX1 ; X += 2, pActiveMaskPixel += 2, pInPixel += 2, pOutPixel += 2)
                                    if (*pActiveMaskPixel)
                                    {
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                                        const Real GF = std::abs(pInPixel[NE] - pInPixel[SW]) * DW22_ChromaticFactor;
                                        const Real GNE = GradientWeightingKernel(GF + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[NE].m_Elements.m_G) * DW11_LuminanceFactor, Mode, ModeParameter);
                                        const Real GSW = GradientWeightingKernel(GF + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[SW].m_Elements.m_G) * DW11_LuminanceFactor, Mode, ModeParameter);
                                        const Real GB = std::abs(pInPixel[NW] - pInPixel[SE]) * DW22_ChromaticFactor;
                                        const Real GNW = GradientWeightingKernel(GB + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[NW].m_Elements.m_G) * DW11_LuminanceFactor, Mode, ModeParameter);
                                        const Real GSE = GradientWeightingKernel(GB + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[SE].m_Elements.m_G) * DW11_LuminanceFactor, Mode, ModeParameter);
#else
                                        const Real GF = std::abs(pInPixel[NE] - pInPixel[SW]) * DW22_ChromaticFactor;
                                        const Real GNE = GradientWeightingKernel(GF + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[NE].m_Elements.m_G) * DW11_LuminanceFactor);
                                        const Real GSW = GradientWeightingKernel(GF + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[SW].m_Elements.m_G) * DW11_LuminanceFactor);
                                        const Real GB = std::abs(pInPixel[NW] - pInPixel[SE]) * DW22_ChromaticFactor;
                                        const Real GNW = GradientWeightingKernel(GB + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[NW].m_Elements.m_G) * DW11_LuminanceFactor);
                                        const Real GSE = GradientWeightingKernel(GB + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[SE].m_Elements.m_G) * DW11_LuminanceFactor);
#endif
                                        pOutPixel->m_Channels[CurrentChannel] = (GNE * pInPixel[NE] + GNW * pInPixel[NW] + GSE * pInPixel[SE] + GSW * pInPixel[SW]) / (GNE + GSE + GNW + GSW);
                                    }
                            }
                        }
                        else
                        {
                            for (int Y = CY0 ; Y <= CY1 ; ++Y)
                            {
                                const int XB = (ChannelContentMap.m_YX[Y & 0X1][CX0 & 0X1] == CBayerPattern::eGreen) ? CX0 + 1 : CX0;
                                const Real* pInPixel = m_pInImage->GetReadOnlyBufferAt(XB, Y);
                                ContinuousRGBPixel* pOutPixel = pSelectedOutImage->GetWritableBufferAt(XB, Y);
                                const CBayerPattern::Channel CurrentChannel = (ChannelContentMap.m_YX[Y & 0x1][XB & 0x1] == CBayerPattern::eRed) ? CBayerPattern::eBlue : CBayerPattern::eRed;
                                for (int X = XB ; X <= CX1 ; X += 2, pInPixel += 2, pOutPixel += 2)
                                {
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                                    const Real GF = std::abs(pInPixel[NE] - pInPixel[SW]) * DW22_ChromaticFactor;
                                    const Real GNE = GradientWeightingKernel(GF + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[NE].m_Elements.m_G) * DW11_LuminanceFactor, Mode, ModeParameter);
                                    const Real GSW = GradientWeightingKernel(GF + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[SW].m_Elements.m_G) * DW11_LuminanceFactor, Mode, ModeParameter);
                                    const Real GB = std::abs(pInPixel[NW] - pInPixel[SE]) * DW22_ChromaticFactor;
                                    const Real GNW = GradientWeightingKernel(GB + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[NW].m_Elements.m_G) * DW11_LuminanceFactor, Mode, ModeParameter);
                                    const Real GSE = GradientWeightingKernel(GB + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[SE].m_Elements.m_G) * DW11_LuminanceFactor, Mode, ModeParameter);
#else
                                    const Real GF = std::abs(pInPixel[NE] - pInPixel[SW]) * DW22_ChromaticFactor;
                                    const Real GNE = GradientWeightingKernel(GF + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[NE].m_Elements.m_G) * DW11_LuminanceFactor);
                                    const Real GSW = GradientWeightingKernel(GF + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[SW].m_Elements.m_G) * DW11_LuminanceFactor);
                                    const Real GB = std::abs(pInPixel[NW] - pInPixel[SE]) * DW22_ChromaticFactor;
                                    const Real GNW = GradientWeightingKernel(GB + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[NW].m_Elements.m_G) * DW11_LuminanceFactor);
                                    const Real GSE = GradientWeightingKernel(GB + std::abs(pOutPixel->m_Elements.m_G - pOutPixel[SE].m_Elements.m_G) * DW11_LuminanceFactor);
#endif
                                    pOutPixel->m_Channels[CurrentChannel] = (GNE * pInPixel[NE] + GNW * pInPixel[NW] + GSE * pInPixel[SE] + GSW * pInPixel[SW]) / (GNE + GSE + GNW + GSW);
                                }
                            }
                        }

                        const int DX0 = CX0 + 1;
                        const int DY0 = CY0 + 1;
                        const int DX1 = CX1 - 1;
                        const int DY1 = CY1 - 1;
                        if (IsMasking())
                        {
                            for (int Y = DY0 ; Y <= DY1 ; ++Y)
                            {
                                const int XB = (ChannelContentMap.m_YX[Y & 0X1][DX0 & 0X1] == CBayerPattern::eGreen) ? DX0 : DX0 + 1;
                                ContinuousRGBPixel* pOutPixel = pSelectedOutImage->GetWritableBufferAt(XB, Y);
                                const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(XB, Y);
                                for (int X = XB ; X <= DX1 ; X += 2, pActiveMaskPixel += 2, pOutPixel += 2)
                                    if (*pActiveMaskPixel)
                                    {
                                        const Real WGN = std::abs(pOutPixel->m_Elements.m_G - pOutPixel[N2].m_Elements.m_G) * DW2_LuminanceFactor;
                                        const Real WGS = std::abs(pOutPixel->m_Elements.m_G - pOutPixel[S2].m_Elements.m_G) * DW2_LuminanceFactor;
                                        const Real WGE = std::abs(pOutPixel->m_Elements.m_G - pOutPixel[E2].m_Elements.m_G) * DW2_LuminanceFactor;
                                        const Real WGW = std::abs(pOutPixel->m_Elements.m_G - pOutPixel[W2].m_Elements.m_G) * DW2_LuminanceFactor;
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                                        Real GV = std::abs(pOutPixel[N].m_Elements.m_R - pOutPixel[S].m_Elements.m_R) * DW0_ChromaticFactor;
                                        Real GN = GradientWeightingKernel(WGN + GV, Mode, ModeParameter);
                                        Real GS = GradientWeightingKernel(WGS + GV, Mode, ModeParameter);
                                        Real GH = std::abs(pOutPixel[E].m_Elements.m_R - pOutPixel[W].m_Elements.m_R) * DW0_ChromaticFactor;
                                        Real GE = GradientWeightingKernel(WGE + GH, Mode, ModeParameter);
                                        Real GW = GradientWeightingKernel(WGW + GH, Mode, ModeParameter);
#else
                                        Real GV = std::abs(pOutPixel[N].m_Elements.m_R - pOutPixel[S].m_Elements.m_R) * DW0_ChromaticFactor;
                                        Real GN = GradientWeightingKernel(WGN + GV);
                                        Real GS = GradientWeightingKernel(WGS + GV);
                                        Real GH = std::abs(pOutPixel[E].m_Elements.m_R - pOutPixel[W].m_Elements.m_R) * DW0_ChromaticFactor;
                                        Real GE = GradientWeightingKernel(WGE + GH);
                                        Real GW = GradientWeightingKernel(WGW + GH);
#endif
                                        pOutPixel->m_Elements.m_R = (GN * pOutPixel[N].m_Elements.m_R + GS * pOutPixel[S].m_Elements.m_R + GE * pOutPixel[E].m_Elements.m_R + GW * pOutPixel[W].m_Elements.m_R) / (GN + GS + GE + GW);
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                                        GV = std::abs(pOutPixel[N].m_Elements.m_B - pOutPixel[S].m_Elements.m_B) * DW0_ChromaticFactor;
                                        GN = GradientWeightingKernel(WGN + GV, Mode, ModeParameter);
                                        GS = GradientWeightingKernel(WGS + GV, Mode, ModeParameter);
                                        GH = std::abs(pOutPixel[E].m_Elements.m_B - pOutPixel[W].m_Elements.m_B) * DW0_ChromaticFactor;
                                        GE = GradientWeightingKernel(WGE + GH, Mode, ModeParameter);
                                        GW = GradientWeightingKernel(WGW + GH, Mode, ModeParameter);
#else
                                        GV = std::abs(pOutPixel[N].m_Elements.m_B - pOutPixel[S].m_Elements.m_B) * DW0_ChromaticFactor;
                                        GN = GradientWeightingKernel(WGN + GV);
                                        GS = GradientWeightingKernel(WGS + GV);
                                        GH = std::abs(pOutPixel[E].m_Elements.m_B - pOutPixel[W].m_Elements.m_B) * DW0_ChromaticFactor;
                                        GE = GradientWeightingKernel(WGE + GH);
                                        GW = GradientWeightingKernel(WGW + GH);
#endif
                                        pOutPixel->m_Elements.m_B = (GN * pOutPixel[N].m_Elements.m_B + GS * pOutPixel[S].m_Elements.m_B + GE * pOutPixel[E].m_Elements.m_B + GW * pOutPixel[W].m_Elements.m_B) / (GN + GS + GE + GW);
                                    }
                            }
                        }
                        else
                        {
                            for (int Y = DY0 ; Y <= DY1 ; ++Y)
                            {
                                const int XB = (ChannelContentMap.m_YX[Y & 0X1][DX0 & 0X1] == CBayerPattern::eGreen) ? DX0 : DX0 + 1;
                                ContinuousRGBPixel* pOutPixel = pSelectedOutImage->GetWritableBufferAt(XB, Y);
                                for (int X = XB ; X <= DX1 ; X += 2, pOutPixel += 2)
                                {
                                    const Real WGN = std::abs(pOutPixel->m_Elements.m_G - pOutPixel[N2].m_Elements.m_G) * DW2_LuminanceFactor;
                                    const Real WGS = std::abs(pOutPixel->m_Elements.m_G - pOutPixel[S2].m_Elements.m_G) * DW2_LuminanceFactor;
                                    const Real WGE = std::abs(pOutPixel->m_Elements.m_G - pOutPixel[E2].m_Elements.m_G) * DW2_LuminanceFactor;
                                    const Real WGW = std::abs(pOutPixel->m_Elements.m_G - pOutPixel[W2].m_Elements.m_G) * DW2_LuminanceFactor;
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                                    Real GV = std::abs(pOutPixel[N].m_Elements.m_R - pOutPixel[S].m_Elements.m_R) * DW0_ChromaticFactor;
                                    Real GN = GradientWeightingKernel(WGN + GV, Mode, ModeParameter);
                                    Real GS = GradientWeightingKernel(WGS + GV, Mode, ModeParameter);
                                    Real GH = std::abs(pOutPixel[E].m_Elements.m_R - pOutPixel[W].m_Elements.m_R) * DW0_ChromaticFactor;
                                    Real GE = GradientWeightingKernel(WGE + GH, Mode, ModeParameter);
                                    Real GW = GradientWeightingKernel(WGW + GH, Mode, ModeParameter);
#else
                                    Real GV = std::abs(pOutPixel[N].m_Elements.m_R - pOutPixel[S].m_Elements.m_R) * DW0_ChromaticFactor;
                                    Real GN = GradientWeightingKernel(WGN + GV);
                                    Real GS = GradientWeightingKernel(WGS + GV);
                                    Real GH = std::abs(pOutPixel[E].m_Elements.m_R - pOutPixel[W].m_Elements.m_R) * DW0_ChromaticFactor;
                                    Real GE = GradientWeightingKernel(WGE + GH);
                                    Real GW = GradientWeightingKernel(WGW + GH);
#endif
                                    pOutPixel->m_Elements.m_R = (GN * pOutPixel[N].m_Elements.m_R + GS * pOutPixel[S].m_Elements.m_R + GE * pOutPixel[E].m_Elements.m_R + GW * pOutPixel[W].m_Elements.m_R) / (GN + GS + GE + GW);
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                                    GV = std::abs(pOutPixel[N].m_Elements.m_B - pOutPixel[S].m_Elements.m_B) * DW0_ChromaticFactor;
                                    GN = GradientWeightingKernel(WGN + GV, Mode, ModeParameter);
                                    GS = GradientWeightingKernel(WGS + GV, Mode, ModeParameter);
                                    GH = std::abs(pOutPixel[E].m_Elements.m_B - pOutPixel[W].m_Elements.m_B) * DW0_ChromaticFactor;
                                    GE = GradientWeightingKernel(WGE + GH, Mode, ModeParameter);
                                    GW = GradientWeightingKernel(WGW + GH, Mode, ModeParameter);
#else
                                    GV = std::abs(pOutPixel[N].m_Elements.m_B - pOutPixel[S].m_Elements.m_B) * DW0_ChromaticFactor;
                                    GN = GradientWeightingKernel(WGN + GV);
                                    GS = GradientWeightingKernel(WGS + GV);
                                    GH = std::abs(pOutPixel[E].m_Elements.m_B - pOutPixel[W].m_Elements.m_B) * DW0_ChromaticFactor;
                                    GE = GradientWeightingKernel(WGE + GH);
                                    GW = GradientWeightingKernel(WGW + GH);
#endif
                                    pOutPixel->m_Elements.m_B = (GN * pOutPixel[N].m_Elements.m_B + GS * pOutPixel[S].m_Elements.m_B + GE * pOutPixel[E].m_Elements.m_B + GW * pOutPixel[W].m_Elements.m_B) / (GN + GS + GE + GW);
                                }
                            }
                        }
                        return true;
                    }
                    return false;
                }

#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                bool CContinuousDemosaicing::BilateralAdaptiveDemosaicing(const Real SigmaSpace, const Real SigmaRange, const int Mode, const Real ModeParameter)
#else
                bool CContinuousDemosaicing::BilateralAdaptiveDemosaicing(const Real SigmaSpace, const Real SigmaRange)
#endif
                {
                    if (m_ConvolutionGaussianKernel.SetStandardDeviation(SigmaSpace))
                    {
                        m_ConvolutionGaussianKernel.ScaleMaximal(Real(1));
                    }
                    if (DirectionalGradientDemosaicing(&m_ApproximationImage, &m_ApproximationActiveImageZone))
                    {
                        const int KernelDiameter = m_ConvolutionGaussianKernel.GetDiameter();
                        const int KernelRadius = m_ConvolutionGaussianKernel.GetRadius();
                        const int X0 = m_ApproximationActiveImageZone.GetX0() + KernelRadius;
                        const int Y0 = m_ApproximationActiveImageZone.GetY0() + KernelRadius;
                        const int X1 = m_ApproximationActiveImageZone.GetX1() - KernelRadius;
                        const int Y1 = m_ApproximationActiveImageZone.GetY1() - KernelRadius;
                        const int OffsetKernelSubLine = m_pInImage->GetWidth() - KernelDiameter;
                        const int OfssetApproximation = (m_pInImage->GetWidth() + 1) * (-KernelRadius);
                        const CBayerPattern::ChannelContentMap ChannelContentMap = m_BayerPattern.GetBayerPatternMap();
                        const Real RangeExponentFactor = Real(-1) / (Real(2) * SigmaRange * SigmaRange);
                        if (m_ExtractHomogenityImage)
                        {
                            m_HomogenityImage.Clear();
                            if (IsMasking())
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                {
                                    const ContinuousRGBPixel* pCentralApproximationPixel = m_ApproximationImage.GetReadOnlyBufferAt(X0, Y);
                                    Real* pHomogenityPixel = m_HomogenityImage.GetWritableBufferAt(X0, Y);
                                    ContinuousRGBPixel* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y);
                                    const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y);
                                    for (int X = X0 ; X <= X1 ; ++X, ++pCentralApproximationPixel, ++pHomogenityPixel, ++pOutPixel)
                                        if (*pActiveMaskPixel++)
                                        {
                                            const Real* pKernel = m_ConvolutionGaussianKernel.GetReadOnlyKernel();
                                            const ContinuousRGBPixel* pApproximationPixel = pCentralApproximationPixel + OfssetApproximation;
                                            ContinuousRGBPixel HomogenityPixel;
                                            const int XS0 = X - KernelRadius;
                                            const int XS1 = X + KernelRadius;
                                            const int YS1 = Y + KernelRadius;
                                            for (int YS = Y - KernelRadius ; YS <= YS1 ; ++YS, pApproximationPixel += OffsetKernelSubLine)
                                                for (int XS = XS0 ; XS <= XS1 ; ++XS, ++pApproximationPixel)
                                                {
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                                                    const Real AdaptiveKernel = *pKernel++ * RangeDistance(pApproximationPixel, pCentralApproximationPixel, Mode, RangeExponentFactor);
#else
                                                    const Real AdaptiveKernel = *pKernel++ * RangeDistance(pApproximationPixel, pCentralApproximationPixel, RangeExponentFactor);
#endif
                                                    *pHomogenityPixel += AdaptiveKernel;
                                                    const int CurrentChannel = ChannelContentMap.m_YX[YS & 0X1][XS & 0x1];
                                                    HomogenityPixel.m_Channels[CurrentChannel] += AdaptiveKernel;
                                                    pOutPixel->m_Channels[CurrentChannel] += pApproximationPixel->m_Channels[CurrentChannel] * AdaptiveKernel;
                                                }
                                            pOutPixel->InverseScale(HomogenityPixel);
                                        }
                                }
                            }
                            else
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                {
                                    const ContinuousRGBPixel* pCentralApproximationPixel = m_ApproximationImage.GetReadOnlyBufferAt(X0, Y);
                                    Real* pHomogenityPixel = m_HomogenityImage.GetWritableBufferAt(X0, Y);
                                    ContinuousRGBPixel* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y);
                                    for (int X = X0 ; X <= X1 ; ++X, ++pCentralApproximationPixel, ++pHomogenityPixel, ++pOutPixel)
                                    {
                                        const Real* pKernel = m_ConvolutionGaussianKernel.GetReadOnlyKernel();
                                        const ContinuousRGBPixel* pApproximationPixel = pCentralApproximationPixel + OfssetApproximation;
                                        ContinuousRGBPixel HomogenityPixel;
                                        const int XS0 = X - KernelRadius;
                                        const int XS1 = X + KernelRadius;
                                        const int YS1 = Y + KernelRadius;
                                        for (int YS = Y - KernelRadius ; YS <= YS1 ; ++YS, pApproximationPixel += OffsetKernelSubLine)
                                            for (int XS = XS0 ; XS <= XS1 ; ++XS, ++pApproximationPixel)
                                            {
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                                                const Real AdaptiveKernel = *pKernel++ * RangeDistance(pApproximationPixel, pCentralApproximationPixel, Mode, RangeExponentFactor);
#else
                                                const Real AdaptiveKernel = *pKernel++ * RangeDistance(pApproximationPixel, pCentralApproximationPixel, RangeExponentFactor);
#endif
                                                *pHomogenityPixel += AdaptiveKernel;
                                                const int CurrentChannel = ChannelContentMap.m_YX[YS & 0X1][XS & 0x1];
                                                HomogenityPixel.m_Channels[CurrentChannel] += AdaptiveKernel;
                                                pOutPixel->m_Channels[CurrentChannel] += pApproximationPixel->m_Channels[CurrentChannel] * AdaptiveKernel;
                                            }
                                        pOutPixel->InverseScale(HomogenityPixel);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (IsMasking())
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                {
                                    const ContinuousRGBPixel* pCentralApproximationPixel = m_ApproximationImage.GetReadOnlyBufferAt(X0, Y);
                                    ContinuousRGBPixel* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y);
                                    const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y);
                                    for (int X = X0 ; X <= X1 ; ++X, ++pCentralApproximationPixel, ++pOutPixel)
                                        if (*pActiveMaskPixel++)
                                        {
                                            const Real* pKernel = m_ConvolutionGaussianKernel.GetReadOnlyKernel();
                                            const ContinuousRGBPixel* pApproximationPixel = pCentralApproximationPixel + OfssetApproximation;
                                            ContinuousRGBPixel HomogenityPixel;

                                            const int XS0 = X - KernelRadius;
                                            const int XS1 = X + KernelRadius;
                                            const int YS1 = Y + KernelRadius;
                                            for (int YS = Y - KernelRadius ; YS <= YS1 ; ++YS, pApproximationPixel += OffsetKernelSubLine)
                                                for (int XS = XS0 ; XS <= XS1 ; ++XS, ++pApproximationPixel)
                                                {
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                                                    const Real AdaptiveKernel = *pKernel++ * RangeDistance(pApproximationPixel, pCentralApproximationPixel, Mode, RangeExponentFactor);
#else
                                                    const Real AdaptiveKernel = *pKernel++ * RangeDistance(pApproximationPixel, pCentralApproximationPixel, RangeExponentFactor);
#endif
                                                    const int CurrentChannel = ChannelContentMap.m_YX[YS & 0X1][XS & 0x1];
                                                    HomogenityPixel.m_Channels[CurrentChannel] += AdaptiveKernel;
                                                    pOutPixel->m_Channels[CurrentChannel] += pApproximationPixel->m_Channels[CurrentChannel] * AdaptiveKernel;
                                                }
                                            pOutPixel->InverseScale(HomogenityPixel);
                                        }
                                }
                            }
                            else
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                {
                                    const ContinuousRGBPixel* pCentralApproximationPixel = m_ApproximationImage.GetReadOnlyBufferAt(X0, Y);
                                    ContinuousRGBPixel* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y);
                                    for (int X = X0 ; X <= X1 ; ++X, ++pCentralApproximationPixel, ++pOutPixel)
                                    {
                                        const Real* pKernel = m_ConvolutionGaussianKernel.GetReadOnlyKernel();
                                        const ContinuousRGBPixel* pApproximationPixel = pCentralApproximationPixel + OfssetApproximation;
                                        ContinuousRGBPixel HomogenityPixel;
                                        const int XS0 = X - KernelRadius;
                                        const int XS1 = X + KernelRadius;
                                        const int YS1 = Y + KernelRadius;
                                        for (int YS = Y - KernelRadius ; YS <= YS1 ; ++YS, pApproximationPixel += OffsetKernelSubLine)
                                            for (int XS = XS0 ; XS <= XS1 ; ++XS, ++pApproximationPixel)
                                            {
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                                                const Real AdaptiveKernel = *pKernel++ * RangeDistance(pApproximationPixel, pCentralApproximationPixel, Mode, RangeExponentFactor);
#else
                                                const Real AdaptiveKernel = *pKernel++ * RangeDistance(pApproximationPixel, pCentralApproximationPixel, RangeExponentFactor);
#endif
                                                const int CurrentChannel = ChannelContentMap.m_YX[YS & 0X1][XS & 0x1];
                                                HomogenityPixel.m_Channels[CurrentChannel] += AdaptiveKernel;
                                                pOutPixel->m_Channels[CurrentChannel] += pApproximationPixel->m_Channels[CurrentChannel] * AdaptiveKernel;
                                            }
                                        pOutPixel->InverseScale(HomogenityPixel);
                                    }
                                }
                            }
                        }
                        return m_OutActiveImageZone.Set(X0, Y0, X1, Y1);
                    }
                    return false;
                }

#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                Real CContinuousDemosaicing::GradientWeightingKernel(const Real Gradient, const int Mode, const Real ModeParameter)
#else
                Real CContinuousDemosaicing::GradientWeightingKernel(const Real Gradient)
#endif
                {
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                    switch (Mode)
                    {
                        case 0:
                            return Real(1) / (Real(1) + std::abs(Gradient));
                        case 1:
                            return Real(1) / (Real(1) + (Gradient * Gradient));
                        case 2:
                            return Real(1) / (Real(1) + (Gradient * Gradient * std::abs(Gradient)));
                        case 3:
                            return Real(1) / (Real(1) + (Gradient * Gradient * Gradient * Gradient));
                        case 4:
                        {
                            if (std::abs(Gradient) > ModeParameter)
                            {
                                return Real(0);
                            }
                            const Real Deviation = Gradient / ModeParameter;
                            return Real(1) - (Deviation * Deviation);
                        }
                    }
#endif
                    return Real(1) / (Real(1) + std::abs(Gradient));
                }

#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                Real CContinuousDemosaicing::RangeDistance(const ContinuousRGBPixel* pContinuousRGBPixelA, const ContinuousRGBPixel* pContinuousRGBPixelB, const int Mode, const Real ModeParameter)
#else
                Real CContinuousDemosaicing::RangeDistance(const ContinuousRGBPixel* pContinuousRGBPixelA, const ContinuousRGBPixel* pContinuousRGBPixelB, const Real ExponentFactor)
#endif
                {
                    const Real DR = pContinuousRGBPixelA->m_Elements.m_R - pContinuousRGBPixelB->m_Elements.m_R;
                    const Real DG = pContinuousRGBPixelA->m_Elements.m_G - pContinuousRGBPixelB->m_Elements.m_G;
                    const Real DB = pContinuousRGBPixelA->m_Elements.m_B - pContinuousRGBPixelB->m_Elements.m_B;
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                    switch (Mode)
                    {
                        case 0:
                            return std::exp((DR * DR + DG * DG + DB * DB) * ModeParameter);
                    }
                    return std::exp((DR * DR + DG * DG + DB * DB) * ModeParameter);
#else
                    return std::exp((DR * DR + DG * DG + DB * DB) * ExponentFactor);
#endif
                }

                void CContinuousDemosaicing::InitializeActiveMaskImage()
                {
                    m_OutActiveMaskImage.Create(m_pInImage->GetSize(), false);
                    m_OutActiveMaskImage.SetSource(m_pInActiveMaskImage, false);
                }

                bool CContinuousDemosaicing::UpdateActiveMaskImage(const bool Force)
                {
                    if (m_pInActiveMaskImage && m_pInActiveMaskImage->IsValid())
                    {
                        if (Force || m_OutActiveMaskImage.SourceHasChanged(m_pInActiveMaskImage))
                            switch (m_DemosaicingMethod)
                            {
                                case eBilinear:
                                    return m_OutActiveMaskImage.UpdateErodeFromSource(1);
                                    break;
                                case eDirectionalGradient:
                                    return m_OutActiveMaskImage.UpdateErodeFromSource(4);
                                    break;
                                case eBilateralAdaptive:
                                    return m_OutActiveMaskImage.UpdateErodeFromSource(4 + m_ConvolutionGaussianKernel.GetRadius());
                                    break;
                            }
                    }
                    else
                    {
                        return m_OutActiveMaskImage.SetSource(nullptr, true);
                    }
                    return false;
                }

                bool CContinuousDemosaicing::UpdateActiveImageZone(const bool Force)
                {
                    if (m_pInActiveImageZone && m_pInActiveImageZone->IsValid() && m_pInImage->IsInside(m_pInActiveImageZone))
                    {
                        if (Force || (m_OutActiveImageZone != *m_pInActiveImageZone) || (m_OutActiveImageZone.GetContentId() != m_pInActiveImageZone->GetContentId()))
                            switch (m_DemosaicingMethod)
                            {
                                case eBilinear:
                                    return m_OutActiveImageZone.Set(*m_pInActiveImageZone, 1);
                                    break;
                                case eDirectionalGradient:
                                    return m_OutActiveImageZone.Set(*m_pInActiveImageZone, 4);
                                    break;
                                case eBilateralAdaptive:
                                    return m_OutActiveImageZone.Set(*m_pInActiveImageZone, 4 + m_ConvolutionGaussianKernel.GetRadius());
                                    break;
                            }
                        return true;
                    }
                    return m_OutActiveImageZone.Set(0, 0, m_pInImage->GetWidth() - 1, m_pInImage->GetHeight() - 1);
                }
            }
        }
    }
}
