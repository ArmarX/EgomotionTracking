/*
 * ContinuousDemosaicing.h
 */

#pragma once

#include "../../../Mathematics/_2D/Kernels/ConvolutionGaussianKernel2D.h"
#include "../../PixelTypes.h"
#include "../../BayerPattern.h"
#include "../../DiscreteRGBColorMap.h"
#include "../TImageProcess.hpp"

#define _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
#define _CONTINUOUS_DEMOSAICING_LUMINANCE_FACTOR_ Real(0.592261911700928)
#define _CONTINUOUS_DEMOSAICING_CHROMATIC_FACTOR_ Real(0.431867688121978)
#define _CONTINUOUS_DEMOSAICING_SIGMA_SPACE_ Real(1)
#define _CONTINUOUS_DEMOSAICING_SIGMA_RANGE_ Real(6)

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Demosaicing
            {
                class CContinuousDemosaicing : public TImageProcess<Real, ContinuousRGBPixel>
                {
                public:

                    enum DemosaicingMethod
                    {
                        eBilinear = 0, eDirectionalGradient, eBilateralAdaptive
                    };

                    static const TImage<Real>* Mosaicing(const TImage<DiscreteRGBPixel>* pDiscreteRGBImage, const CBayerPattern::BayerPatternType PatternType);
                    static Real PeakSignalToNoiseRatio(const TImage<DiscreteRGBPixel>* pDiscreteRGBImage, const TImage<ContinuousRGBPixel>* pContinuousRGBImage, const int X0, const int Y0, const int X1, const int Y1);

#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                    struct ParametrizedTrial
                    {
                        ParametrizedTrial()
                        {
                            m_ImageIndex = 0;
                            m_ParameterA = Real(0);
                            m_ParameterB = Real(0);
                            m_Mode = 0;
                            m_ModeParameter = Real(0);
                            m_Method = DemosaicingMethod(-1);
                            m_ElapsedTime = Real(0);
                            m_PSNR = Real(0);
                        }

                        int m_ImageIndex;
                        Real m_ParameterA;
                        Real m_ParameterB;
                        int m_Mode;
                        Real m_ModeParameter;
                        DemosaicingMethod m_Method;
                        Real m_ElapsedTime;
                        Real m_PSNR;

                        std::string ToString() const
                        {
                            std::ostringstream OutputString;
                            OutputString.precision(g_RealDisplayDigits);
                            OutputString << m_ImageIndex << "\t" << m_ParameterA << "\t" << m_ParameterB << "\t" << m_Mode << "\t" << m_ModeParameter << "\t" << m_Method << "\t" << m_ElapsedTime << "\t" << m_PSNR << std::endl;
                            return OutputString.str();
                        }

                        void ToConsole() const
                        {
                            std::cout << ToString();
                        }
                    };
                    static bool SortParametrizedTrialByPSNR(const ParametrizedTrial& lhs, const ParametrizedTrial& rhs);
                    static std::list<ParametrizedTrial> EstimateOptimalParameters(const std::list<const TImage<DiscreteRGBPixel>*>& DiscreteRGBImages, const DemosaicingMethod, const CBayerPattern::BayerPatternType PatternType, const bool FullDisplay = true);
                    static bool ExportParametrizedTrialsToFile(const std::list<ParametrizedTrial>& ParametrizedTrials, const std::string FileName);
#endif

                    CContinuousDemosaicing(const TImage<Real>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone, const CBayerPattern::BayerPatternType PatternType);
                    ~CContinuousDemosaicing() override;

                    bool SetDemosaicingMethod(const DemosaicingMethod Method);
                    bool SetBayerPatternType(const CBayerPattern::BayerPatternType PatternType);
                    void SetExtractHomogenityImage(const bool ExtractHomogenityImage);

                    void SetMasking(const bool Masking) override;
                    bool Execute(const int Trial) override;

                    bool Display(TImage<DiscreteRGBPixel>* pDisplayImage);

                    const TImage<Real>* GetHomogenityImage() const;

                protected:

                    bool BilinearDemosaicing();
#ifdef _CONTINUOUS_DEMOSAICING_ENABLE_ESTIMATE_PARAMETERS_
                    bool DirectionalGradientDemosaicing(TImage<ContinuousRGBPixel>* pSelectedOutImage, CActiveImageZone* pOutActiveImageZone, const Real LuminanceFactor = _CONTINUOUS_DEMOSAICING_LUMINANCE_FACTOR_, const Real ChromaticFactor = _CONTINUOUS_DEMOSAICING_CHROMATIC_FACTOR_, const int Mode = 0, const Real ModeParameter = Real(0));
                    bool BilateralAdaptiveDemosaicing(const Real SigmaSpace = _CONTINUOUS_DEMOSAICING_SIGMA_SPACE_, const Real SigmaRange = _CONTINUOUS_DEMOSAICING_SIGMA_RANGE_, const int Mode = 0, const Real ModeParameter = Real(0));
                    Real GradientWeightingKernel(const Real Gradient, const int Mode = 0, const Real ModeParameter = Real(0));
                    Real RangeDistance(const ContinuousRGBPixel* pContinuousRGBPixelA, const ContinuousRGBPixel* pContinuousRGBPixelB, const int Mode = 0, const Real ModeParameter = Real(0));
#else
                    bool DirectionalGradientDemosaicing(TImage<ContinuousRGBPixel>* pSelectedOutImage, CActiveImageZone* pOutActiveImageZone, const bool UseOutImage = true, const Real LuminanceFactor = _CONTINUOUS_DEMOSAICING_LUMINANCE_FACTOR_, const Real ChromaticFactor = _CONTINUOUS_DEMOSAICING_CHROMATIC_FACTOR_);
                    bool BilateralAdaptiveDemosaicing(const Real SigmaSpace = _CONTINUOUS_DEMOSAICING_SIGMA_SPACE_, const Real SigmaRange = _CONTINUOUS_DEMOSAICING_SIGMA_RANGE_);
                    Real GradientWeightingKernel(const Real Gradient);
                    Real RangeDistance(const ContinuousRGBPixel* pContinuousRGBPixelA, const ContinuousRGBPixel* pContinuousRGBPixelB, const Real ExponentFactor);
#endif

                    void InitializeActiveMaskImage() override;
                    bool UpdateActiveMaskImage(const bool Force) override;
                    bool UpdateActiveImageZone(const bool Force) override;

                    DemosaicingMethod m_DemosaicingMethod;
                    bool m_ExtractHomogenityImage;
                    TImage<ContinuousRGBPixel> m_ApproximationImage;
                    TImage<Real> m_HomogenityImage;
                    CBayerPattern m_BayerPattern;
                    CActiveImageZone m_ApproximationActiveImageZone;
                    Mathematics::_2D::Kernels::CConvolutionGaussianKernel2D m_ConvolutionGaussianKernel;
                };
            }
        }
    }
}

