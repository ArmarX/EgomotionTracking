/*
 * MonocularPipeLine.cpp
 */

#include "MonocularPipeLine.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace PipeLine
            {
                CMonocularPipeLine::CMonocularPipeLine(const int Id, const TImage<Byte>* pBayerPatternInImage, const CBayerPattern::BayerPatternType PatternType, Sensors::Cameras::Pasive::Calibration::Geometric::CMonocularCameraGeometricCalibration* pMonocularCameraGeometricCalibration) :
                    m_IsEnabled(false),
                    m_Id(Id),
                    m_PatternType(PatternType),
                    m_pBayerPatternInImage(pBayerPatternInImage),
                    m_SynthesizingHDR(true),
                    m_pMonocularCameraGeometricCalibration(pMonocularCameraGeometricCalibration),
                    m_ControlActiveMaskImage(),
                    m_ControlActiveImageZone(),
                    m_pTemporalFusion(nullptr),
                    m_pRadiometricBayerPatternSynthesizer(nullptr),
                    m_pContinuousDemosaicing(nullptr),
                    m_pContinuousRGBUndistortion(nullptr),
                    m_pContinuousChromaticSegmentation(nullptr),
                    m_pContinuousRGBDesaturation(nullptr),
                    m_pGaborSaliencyExtraction(nullptr),
                    m_pEdgeExtraction(nullptr)
                {
                    if (Initialize())
                    {
                        m_IsEnabled = SetConfiguration();
                    }
                }

                CMonocularPipeLine::~CMonocularPipeLine()
                {
                    Finalize();
                }

                int CMonocularPipeLine::GetId() const
                {
                    return m_Id;
                }

                bool CMonocularPipeLine::IsEnabled() const
                {
                    return m_IsEnabled;
                }

                bool CMonocularPipeLine::SetDisplayDirectory(const std::string& DisplayDirectory)
                {
                    const int Length = DisplayDirectory.length();
                    if (Length)
                    {
                        m_DisplayDirectory = DisplayDirectory;
                        if (DisplayDirectory[Length - 1] != '/')
                        {
                            m_DisplayDirectory += '/';
                        }
                        return true;
                    }
                    return false;
                }

                const std::string& CMonocularPipeLine::GetDisplayDirectory() const
                {
                    return m_DisplayDirectory;
                }

                bool CMonocularPipeLine::SaveActiveMaskImages() const
                {
                    if (m_DisplayDirectory.length())
                    {
                        bool SaveResult = true;
                        std::ostringstream OutputStream;

                        OutputStream << m_DisplayDirectory << "ControlActive_InMask_(" << m_Id << ").bmp";
                        SaveResult &= CImageExporter::Export(&m_ControlActiveMaskImage, OutputStream.str());
                        OutputStream.str("");

                        OutputStream << m_DisplayDirectory << "TemporalFusion_OutMask_(" << m_Id << ").bmp";
                        SaveResult &= CImageExporter::Export(m_pTemporalFusion->GetOutActiveMaskImage(), OutputStream.str());
                        OutputStream.str("");

                        OutputStream << m_DisplayDirectory << "ContinuousDemosaicing_OutMask_(" << m_Id << ").bmp";
                        SaveResult &= CImageExporter::Export(m_pContinuousDemosaicing->GetOutActiveMaskImage(), OutputStream.str());
                        OutputStream.str("");

                        OutputStream << m_DisplayDirectory << "ContinuousRGBUndistortion_OutMask_(" << m_Id << ").bmp";
                        SaveResult &= CImageExporter::Export(m_pContinuousRGBUndistortion->GetOutActiveMaskImage(), OutputStream.str());
                        OutputStream.str("");

                        OutputStream << m_DisplayDirectory << "ContinuousRGBDesaturation_OutMask_(" << m_Id << ").bmp";
                        SaveResult &= CImageExporter::Export(m_pContinuousRGBDesaturation->GetOutActiveMaskImage(), OutputStream.str());
                        OutputStream.str("");

                        OutputStream << m_DisplayDirectory << "GaborSaliencyExtraction_OutMask_(" << m_Id << ").bmp";
                        SaveResult &= CImageExporter::Export(m_pGaborSaliencyExtraction->GetOutActiveMaskImage(), OutputStream.str());
                        OutputStream.str("");

                        OutputStream << m_DisplayDirectory << "EdgeExtraction_OutMask_(" << m_Id << ").bmp";
                        SaveResult &= CImageExporter::Export(m_pEdgeExtraction->GetOutActiveMaskImage(), OutputStream.str());
                        OutputStream.str("");

                        return SaveResult;
                    }
                    return false;
                }

                bool CMonocularPipeLine::PreAcquisition()
                {
                    return false;
                }

                bool CMonocularPipeLine::Acquisition(const int Trial, const bool SaveSample)
                {
                    return false;
                }

                bool CMonocularPipeLine::PostAcquisition()
                {
                    return false;
                }

                bool CMonocularPipeLine::PreRun(const int Trial, const int AcquisitionIndex, const bool SaveSamples)
                {
                    if (m_IsEnabled && m_pTemporalFusion->Execute(Trial))
                    {
                        if (SaveSamples)
                        {
                            const int CurrentLoadedSampleIndex = m_pTemporalFusion->GetTotalLoadedSamples() - 1;
                            std::ostringstream OutputStream;
                            OutputStream << m_DisplayDirectory << "BayerPattern_[" << CurrentLoadedSampleIndex << "]_(" << m_Id << ")" << ".byte_rvl";
                            return m_pBayerPatternInImage->SaveToFile(OutputStream.str(), CurrentLoadedSampleIndex);
                        }
                        return true;
                    }
                    return false;
                }

                bool CMonocularPipeLine::Run(const int Trial)
                {
                    if (m_IsEnabled)
                    {
                        if (!m_pContinuousDemosaicing->Execute(Trial))
                        {
                            return false;
                        }

                        if (!m_pContinuousRGBUndistortion->Execute(Trial))
                        {
                            return false;
                        }

                        if (!m_pContinuousChromaticSegmentation->Execute(Trial))
                        {
                            return false;
                        }

                        if (!m_pContinuousRGBDesaturation->Execute(Trial))
                        {
                            return false;
                        }

                        if (!m_pGaborSaliencyExtraction->Execute(Trial))
                        {
                            return false;
                        }

                        if (!m_pEdgeExtraction->Execute(Trial))
                        {
                            return false;
                        }

                        std::ostringstream OutputStream;
                        OutputStream.precision(g_RealDisplayDigits);

                        OutputStream << m_DisplayDirectory << "ContinuousDemosaicing_OutActiveMask_(" << m_Id << ").bmp";
                        CImageExporter::Export((const TImage<bool>*) m_pContinuousDemosaicing->GetOutActiveMaskImage(), OutputStream.str());
                        OutputStream.str("");

                        OutputStream << m_DisplayDirectory << "ContinuousRGBUndistortion_OutActiveMask_(" << m_Id << ").bmp";
                        CImageExporter::Export((const TImage<bool>*) m_pContinuousRGBUndistortion->GetOutActiveMaskImage(), OutputStream.str());
                        OutputStream.str("");

                        OutputStream << m_DisplayDirectory << "ContinuousRGBDesaturation_OutActiveMask_(" << m_Id << ").bmp";
                        CImageExporter::Export((const TImage<bool>*) m_pContinuousRGBDesaturation->GetOutActiveMaskImage(), OutputStream.str());
                        OutputStream.str("");

                        OutputStream << m_DisplayDirectory << "GaborSaliencyExtraction_OutActiveMask_(" << m_Id << ").bmp";
                        CImageExporter::Export((const TImage<bool>*) m_pGaborSaliencyExtraction->GetOutActiveMaskImage(), OutputStream.str());
                        OutputStream.str("");

                        DisplayResults();
                        SerializeResults();
                        return true;
                    }
                    return false;
                }

                bool CMonocularPipeLine::Initialize()
                {
                    if (m_pBayerPatternInImage && m_pBayerPatternInImage->IsValid() && m_pMonocularCameraGeometricCalibration && m_pMonocularCameraGeometricCalibration->IsLoaded() && (m_pMonocularCameraGeometricCalibration->GetImageSize() == m_pBayerPatternInImage->GetSize()))
                    {
                        //Control
                        bool InitializeResult = true;
                        InitializeResult &= m_ControlActiveMaskImage.Create(m_pBayerPatternInImage->GetSize(), false);
                        InitializeResult &= m_ControlActiveMaskImage.Set(true);
                        InitializeResult &= m_ControlActiveImageZone.Set(m_pBayerPatternInImage->GetSize());

                        //TemporalFusion
                        m_pTemporalFusion = new Fusion::CTemporalFusion(m_pBayerPatternInImage, &m_ControlActiveMaskImage, &m_ControlActiveImageZone);
                        InitializeResult &= m_pTemporalFusion->IsEnabled();

                        //HDR Synthesis
                        m_pRadiometricBayerPatternSynthesizer = new Sensors::Cameras::Pasive::Calibration::Radiometric::Global::CRadiometricBayerPatternSynthesizer(m_pTemporalFusion->GetOutImage(), &m_MultipleChannelRadiometricResponceFunction, m_CameraExposures);
                        InitializeResult &= m_pRadiometricBayerPatternSynthesizer->IsEnabled();

                        //ContinuousDemosaicing
                        m_pContinuousDemosaicing = new Demosaicing::CContinuousDemosaicing(m_pTemporalFusion->GetOutImage(), m_pTemporalFusion->GetOutActiveMaskImage(), m_pTemporalFusion->GetOutActiveImageZone(), m_PatternType);
                        InitializeResult &= m_pContinuousDemosaicing->IsEnabled();

                        //ContinuousRGBUndistortion
                        m_pContinuousRGBUndistortion = new Undistortion::CContinuousRGBUndistortion(m_pContinuousDemosaicing->GetOutImage(), m_pContinuousDemosaicing->GetOutActiveMaskImage(), m_pContinuousDemosaicing->GetOutActiveImageZone(), m_pMonocularCameraGeometricCalibration);
                        InitializeResult &= m_pContinuousRGBUndistortion->IsEnabled();

                        //ContinuousChromaticSegmentation
                        m_pContinuousChromaticSegmentation = new Segmentation::CContinuousChromaticSegmentation(m_pContinuousRGBUndistortion->GetOutImage(), m_pContinuousRGBUndistortion->GetOutActiveMaskImage(), m_pContinuousRGBUndistortion->GetOutActiveImageZone());
                        InitializeResult &= m_pContinuousChromaticSegmentation->IsEnabled();

                        //ContinuousRGBDesaturation
                        m_pContinuousRGBDesaturation = new Desaturation::CContinuousRGBDesaturation(m_pContinuousRGBUndistortion->GetOutImage(), m_pContinuousRGBUndistortion->GetOutActiveMaskImage(), m_pContinuousRGBUndistortion->GetOutActiveImageZone());
                        InitializeResult &= m_pContinuousRGBDesaturation->IsEnabled();

                        //GaborSaliencyExtraction
                        m_pGaborSaliencyExtraction = new Saliency::CGaborSaliencyExtraction(m_pContinuousRGBDesaturation->GetOutImage(), m_pContinuousRGBDesaturation->GetOutActiveMaskImage(), m_pContinuousRGBDesaturation->GetOutActiveImageZone());
                        InitializeResult &= m_pGaborSaliencyExtraction->IsEnabled();

                        //GaborSaliencyExtraction
                        m_pEdgeExtraction = new Edge::CEdgeExtraction(m_pGaborSaliencyExtraction->GetOutImage(), m_pGaborSaliencyExtraction->GetOutActiveMaskImage(), m_pGaborSaliencyExtraction->GetOutActiveImageZone());
                        InitializeResult &= m_pEdgeExtraction->IsEnabled();

                        return InitializeResult;
                    }
                    return false;
                }

                bool CMonocularPipeLine::SetConfiguration()
                {
                    //Control
                    bool ConfigurationResult = true;
                    m_ControlActiveMaskImage.SetContentId(1);
                    m_ControlActiveImageZone.SetContentId(1);

                    //TemporalFusion
                    m_pTemporalFusion->SetMasking(true);
                    m_pTemporalFusion->SetAutoReset(false);
                    ConfigurationResult &= m_pTemporalFusion->SetSamplingMode(Fusion::CTemporalFusion::eDirect);
                    ConfigurationResult &= m_pTemporalFusion->SetFusionMethod(Fusion::CTemporalFusion::eContinuousGaussian);
                    ConfigurationResult &= m_pTemporalFusion->SetRangePrecision(Real(1) / std::pow(Real(2), Real(16)));
                    ConfigurationResult &= m_pTemporalFusion->SetTotalSamplesPerFusion(30);

                    //ContinuousDemosaicing
                    m_pContinuousDemosaicing->SetExtractHomogenityImage(false);
                    ConfigurationResult &= m_pContinuousDemosaicing->SetDemosaicingMethod(Demosaicing::CContinuousDemosaicing::eDirectionalGradient);
                    ConfigurationResult &= m_pContinuousDemosaicing->SetBayerPatternType(m_PatternType);

                    //ContinuousChromaticSegmentation
                    ConfigurationResult &= m_pContinuousChromaticSegmentation->SetChromaticSpace(Segmentation::CContinuousChromaticSegmentation::eHSL);
                    ConfigurationResult &= m_pContinuousChromaticSegmentation->SetDistanceMetric(Segmentation::CContinuousChromaticSegmentation::eEuclidean);

                    //ContinuousRGBDesaturation
                    ConfigurationResult &= m_pContinuousRGBDesaturation->SetDesaturationMethod(Desaturation::CContinuousRGBDesaturation::eLumaCCIR_601);

                    //GaborSaliencyExtraction
                    ConfigurationResult &= m_pGaborSaliencyExtraction->SetTotalKernelPairs(4);
                    ConfigurationResult &= m_pGaborSaliencyExtraction->SetStandardDeviation(Real(0.707106781));
                    ConfigurationResult &= m_pGaborSaliencyExtraction->SetLambda(Real(0.5));
                    ConfigurationResult &= m_pGaborSaliencyExtraction->SetGamma1(Real(1.0));
                    ConfigurationResult &= m_pGaborSaliencyExtraction->SetGamma2(Real(1.0));

                    //EdgeExtraction
                    m_pEdgeExtraction->SetEnableCoherence(false);
                    m_pEdgeExtraction->SetEnableMagnitudeFiltering(false);
                    ConfigurationResult &= m_pEdgeExtraction->SetCoherence(std::cos(g_RealHalfPI * Real(0.5)));
                    ConfigurationResult &= m_pEdgeExtraction->SetPercentile(Real(0.95));

                    return ConfigurationResult;
                }

                void CMonocularPipeLine::DisplayResults()
                {
                    TImage<DiscreteRGBPixel> NativeSizeDisplayImage(m_pBayerPatternInImage->GetSize());
                    std::ostringstream OutputStream;
                    OutputStream.precision(g_RealDisplayDigits);
                    Real MinimalChannelValue, ChannelMean, ChannelStandardDeviation, MaximalChannelValue;

                    //Mean
                    m_pTemporalFusion->Display(&NativeSizeDisplayImage, RVL::Imaging::Process::Fusion::CTemporalFusion::eChannelMean, &g_DiscreteRGBHotColorMap, MaximalChannelValue, MinimalChannelValue, ChannelMean, ChannelStandardDeviation);
                    OutputStream << m_DisplayDirectory << "Mean_[ " << MinimalChannelValue << "," << ChannelMean << "," << ChannelStandardDeviation << "," << MaximalChannelValue << "]_(" << m_Id << ").bmp";
                    CImageExporter::Export(&NativeSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                    //Range
                    m_pTemporalFusion->Display(&NativeSizeDisplayImage, RVL::Imaging::Process::Fusion::CTemporalFusion::eChannelRange, &g_DiscreteRGBHotColorMap, MaximalChannelValue, MinimalChannelValue, ChannelMean, ChannelStandardDeviation);
                    OutputStream << m_DisplayDirectory << "Range_[ " << MinimalChannelValue << "," << ChannelMean << "," << ChannelStandardDeviation << "," << MaximalChannelValue << "]_(" << m_Id << ").bmp";
                    CImageExporter::Export(&NativeSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                    //Standard Deviation
                    m_pTemporalFusion->Display(&NativeSizeDisplayImage, RVL::Imaging::Process::Fusion::CTemporalFusion::eChannelStandardDeviation, &g_DiscreteRGBHotColorMap, MaximalChannelValue, MinimalChannelValue, ChannelMean, ChannelStandardDeviation);
                    OutputStream << m_DisplayDirectory << "StandardDeviation_[ " << MinimalChannelValue << "," << ChannelMean << "," << ChannelStandardDeviation << "," << MaximalChannelValue << "]_(" << m_Id << ").bmp";
                    CImageExporter::Export(&NativeSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                    //Dispersion
                    m_pTemporalFusion->Display(&NativeSizeDisplayImage, RVL::Imaging::Process::Fusion::CTemporalFusion::eChannelDispersion, &g_DiscreteRGBHotColorMap, MaximalChannelValue, MinimalChannelValue, ChannelMean, ChannelStandardDeviation);
                    OutputStream << m_DisplayDirectory << "Dispersion_[ " << MinimalChannelValue << "," << ChannelMean << "," << ChannelStandardDeviation << "," << MaximalChannelValue << "]_(" << m_Id << ").bmp";
                    CImageExporter::Export(&NativeSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                    //SignalToNoiseRatio
                    m_pTemporalFusion->Display(&NativeSizeDisplayImage, RVL::Imaging::Process::Fusion::CTemporalFusion::eChannelSignalToNoiseRatio, &g_DiscreteRGBHotColorMap, MaximalChannelValue, MinimalChannelValue, ChannelMean, ChannelStandardDeviation);
                    OutputStream << m_DisplayDirectory << "SignalToNoiseRatio_[ " << MinimalChannelValue << "," << ChannelMean << "," << ChannelStandardDeviation << "," << MaximalChannelValue << "]_(" << m_Id << ").bmp";
                    CImageExporter::Export(&NativeSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                    //Fusion
                    m_pTemporalFusion->Display(&NativeSizeDisplayImage, RVL::Imaging::Process::Fusion::CTemporalFusion::eChannelFusion, &g_DiscreteRGBHotColorMap, MaximalChannelValue, MinimalChannelValue, ChannelMean, ChannelStandardDeviation);
                    OutputStream << m_DisplayDirectory << "Fusion_[ " << MinimalChannelValue << "," << ChannelMean << "," << ChannelStandardDeviation << "," << MaximalChannelValue << "]_(" << m_Id << ").bmp";
                    CImageExporter::Export(&NativeSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                    //Maximal
                    m_pTemporalFusion->Display(&NativeSizeDisplayImage, RVL::Imaging::Process::Fusion::CTemporalFusion::eChannelMaximal, &g_DiscreteRGBHotColorMap, MaximalChannelValue, MinimalChannelValue, ChannelMean, ChannelStandardDeviation);
                    OutputStream << m_DisplayDirectory << "Maximal_[ " << MinimalChannelValue << "," << ChannelMean << "," << ChannelStandardDeviation << "," << MaximalChannelValue << "]_(" << m_Id << ").bmp";
                    CImageExporter::Export(&NativeSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                    //Minimal
                    m_pTemporalFusion->Display(&NativeSizeDisplayImage, RVL::Imaging::Process::Fusion::CTemporalFusion::eChannelMinimal, &g_DiscreteRGBHotColorMap, MaximalChannelValue, MinimalChannelValue, ChannelMean, ChannelStandardDeviation);
                    OutputStream << m_DisplayDirectory << "Minimal_[ " << MinimalChannelValue << "," << ChannelMean << "," << ChannelStandardDeviation << "," << MaximalChannelValue << "]_(" << m_Id << ").bmp";
                    CImageExporter::Export(&NativeSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                    if (!m_pTemporalFusion->IsAutoReset())
                    {
                        //Maximal Frequency
                        m_pTemporalFusion->Display(&NativeSizeDisplayImage, RVL::Imaging::Process::Fusion::CTemporalFusion::eChannelMaximalFrequency, &g_DiscreteRGBHotColorMap, MaximalChannelValue, MinimalChannelValue, ChannelMean, ChannelStandardDeviation);
                        OutputStream << m_DisplayDirectory << "MaximalFrequency_[ " << MinimalChannelValue << "," << ChannelMean << "," << ChannelStandardDeviation << "," << MaximalChannelValue << "]_(" << m_Id << ").bmp";
                        CImageExporter::Export(&NativeSizeDisplayImage, OutputStream.str());
                        OutputStream.str("");

                        //GlobalMaxima
                        m_pTemporalFusion->Display(&NativeSizeDisplayImage, RVL::Imaging::Process::Fusion::CTemporalFusion::eChannelGlobalMaxima, &g_DiscreteRGBHotColorMap, MaximalChannelValue, MinimalChannelValue, ChannelMean, ChannelStandardDeviation);
                        OutputStream << m_DisplayDirectory << "GlobalMaxima_[ " << MinimalChannelValue << "," << ChannelMean << "," << ChannelStandardDeviation << "," << MaximalChannelValue << "]_(" << m_Id << ").bmp";
                        CImageExporter::Export(&NativeSizeDisplayImage, OutputStream.str());
                        OutputStream.str("");
                    }

                    TImage<DiscreteRGBPixel> UndistortedSizeDisplayImage(m_pContinuousRGBUndistortion->GetOutImage()->GetSize());

                    m_pContinuousDemosaicing->Display(&NativeSizeDisplayImage);
                    OutputStream << m_DisplayDirectory << "Demosaicing_(" << m_Id << ").bmp";
                    CImageExporter::Export(&NativeSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                    m_pContinuousRGBUndistortion->Display(&UndistortedSizeDisplayImage);
                    OutputStream << m_DisplayDirectory << "Undistorted_(" << m_Id << ").bmp";
                    CImageExporter::Export(&UndistortedSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                    m_pContinuousRGBDesaturation->Display(&UndistortedSizeDisplayImage);
                    OutputStream << m_DisplayDirectory << "Desaturated_(" << m_Id << ").bmp";
                    CImageExporter::Export(&UndistortedSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                    m_pGaborSaliencyExtraction->Display(&UndistortedSizeDisplayImage, Saliency::CGaborSaliencyExtraction::eChannelSaliencyMagnitude, &g_DiscreteRGBHotColorMap, MaximalChannelValue, MinimalChannelValue, ChannelMean, ChannelStandardDeviation);
                    OutputStream << m_DisplayDirectory << "GaborSaliencyMagnitude_[ " << MinimalChannelValue << "," << ChannelMean << "," << ChannelStandardDeviation << "," << MaximalChannelValue << "]_(" << m_Id << ").bmp";
                    CImageExporter::Export(&UndistortedSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                    m_pGaborSaliencyExtraction->Display(&UndistortedSizeDisplayImage, Saliency::CGaborSaliencyExtraction::eChannelSaliencyPhase, &g_DiscreteRGBHotColorMap, MaximalChannelValue, MinimalChannelValue, ChannelMean, ChannelStandardDeviation);
                    OutputStream << m_DisplayDirectory << "GaborSaliencyPhase_[ " << MinimalChannelValue << "," << ChannelMean << "," << ChannelStandardDeviation << "," << MaximalChannelValue << "]_(" << m_Id << ").bmp";
                    CImageExporter::Export(&UndistortedSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                    m_pEdgeExtraction->Display(&UndistortedSizeDisplayImage, Edge::CEdgeExtraction::eChannelActive, &g_DiscreteRGBHotColorMap, MaximalChannelValue, MinimalChannelValue, ChannelMean, ChannelStandardDeviation);
                    OutputStream << m_DisplayDirectory << "ActiveEdge_[ " << MinimalChannelValue << "," << ChannelMean << "," << ChannelStandardDeviation << "," << MaximalChannelValue << "]_(" << m_Id << ").bmp";
                    CImageExporter::Export(&UndistortedSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                    m_pEdgeExtraction->Display(&UndistortedSizeDisplayImage, Edge::CEdgeExtraction::eChannelActiveMagnitude, &g_DiscreteRGBHotColorMap, MaximalChannelValue, MinimalChannelValue, ChannelMean, ChannelStandardDeviation);
                    OutputStream << m_DisplayDirectory << "MagnitudeEdge_[ " << MinimalChannelValue << "," << ChannelMean << "," << ChannelStandardDeviation << "," << MaximalChannelValue << "]_(" << m_Id << ").bmp";
                    CImageExporter::Export(&UndistortedSizeDisplayImage, OutputStream.str());
                    OutputStream.str("");

                }

                void CMonocularPipeLine::SerializeResults()
                {

                }

                void CMonocularPipeLine::Finalize()
                {
                    delete m_pEdgeExtraction;
                    m_pEdgeExtraction = nullptr;

                    delete m_pGaborSaliencyExtraction;
                    m_pGaborSaliencyExtraction = nullptr;

                    delete m_pContinuousChromaticSegmentation;
                    m_pContinuousChromaticSegmentation = nullptr;

                    delete m_pContinuousRGBDesaturation;
                    m_pContinuousRGBDesaturation = nullptr;

                    delete m_pContinuousRGBUndistortion;
                    m_pContinuousRGBUndistortion = nullptr;

                    delete m_pContinuousDemosaicing;
                    m_pContinuousDemosaicing = nullptr;

                    delete m_pRadiometricBayerPatternSynthesizer;
                    m_pRadiometricBayerPatternSynthesizer = nullptr;

                    delete m_pTemporalFusion;
                    m_pTemporalFusion = nullptr;
                }
            }
        }
    }
}
