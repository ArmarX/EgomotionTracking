/*
 * MonocularPipeLine.h
 */

#pragma once

#include "../TImageProcess.hpp"
#include "../Fusion/TemporalFusion.h"
#include "../Demosaicing/ContinuousDemosaicing.h"
#include "../Undistortion/ContinuousRGBUndistortion.h"
#include "../Desaturation/ContinuousRGBDesaturation.h"
#include "../Saliency/GaborSaliencyExtraction.h"
#include "../Edge/EdgeExtraction.h"
#include "../Segmentation/ContinuousChromaticSegmentation.h"
#include "../../ImageExporter.h"
#include "../../DiscreteRGBColorMap.h"
#include "../../BayerPattern.h"
#include "../../../Sensors/Cameras/Pasive/Calibration/Geometric/MonocularCameraGeometricCalibration.h"
#include "../../../Sensors/Cameras/Pasive/Calibration/Radiometric/Base/Exposure.h"
#include "../../../Sensors/Cameras/Pasive/Calibration/Radiometric/Global/RadiometricBayerPatternSynthesizer.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace PipeLine
            {
                class CMonocularPipeLine
                {
                public:

                    CMonocularPipeLine(const int Id, const TImage<Byte>* pBayerPatternInImage, const CBayerPattern::BayerPatternType PatternType, Sensors::Cameras::Pasive::Calibration::Geometric::CMonocularCameraGeometricCalibration* pMonocularCameraGeometricCalibration);
                    virtual ~CMonocularPipeLine();

                    int GetId() const;
                    bool IsEnabled() const;

                    bool SetDisplayDirectory(const std::string& DisplayDirectory);
                    const std::string& GetDisplayDirectory() const;

                    bool SaveActiveMaskImages() const;

                    bool PreAcquisition();
                    bool Acquisition(const int Trial, const bool SaveSample);
                    bool PostAcquisition();

                    bool PreRun(const int Trial, const int AcquisitionIndex, const bool SaveSamples);
                    bool Run(const int Trial);

                protected:

                    bool Initialize();
                    bool SetConfiguration();
                    void DisplayResults();
                    void SerializeResults();
                    void Finalize();

                    bool m_IsEnabled;
                    const int m_Id;
                    const CBayerPattern::BayerPatternType m_PatternType;
                    const TImage<Byte>* m_pBayerPatternInImage;

                    bool m_SynthesizingHDR;
                    const Sensors::Cameras::Pasive::Calibration::Geometric::CMonocularCameraGeometricCalibration* m_pMonocularCameraGeometricCalibration;
                    std::string m_DisplayDirectory;
                    std::string m_ExposureSetFile;
                    std::string m_RadiometricCalibrationFile;

                    CActiveMaskImage m_ControlActiveMaskImage;
                    CActiveImageZone m_ControlActiveImageZone;
                    Fusion::CTemporalFusion* m_pTemporalFusion;

                    std::vector<Sensors::Cameras::Pasive::Calibration::Radiometric::Base::CExposure> m_CameraExposures;
                    Sensors::Cameras::Pasive::Calibration::Radiometric::Global::CMultipleChannelRadiometricResponceFunction m_MultipleChannelRadiometricResponceFunction;
                    Sensors::Cameras::Pasive::Calibration::Radiometric::Global::CRadiometricBayerPatternSynthesizer* m_pRadiometricBayerPatternSynthesizer;

                    TImage<Real>* m_pMultiplexerImage;
                    Demosaicing::CContinuousDemosaicing* m_pContinuousDemosaicing;
                    Undistortion::CContinuousRGBUndistortion* m_pContinuousRGBUndistortion;
                    Segmentation::CContinuousChromaticSegmentation* m_pContinuousChromaticSegmentation;
                    Desaturation::CContinuousRGBDesaturation* m_pContinuousRGBDesaturation;
                    Saliency::CGaborSaliencyExtraction* m_pGaborSaliencyExtraction;
                    Edge::CEdgeExtraction* m_pEdgeExtraction;

                    //TODO Implement RIM EXTRACTOR
                    //TODO Implement EDGE-RIM FUSER
                    //TODO Implement GEOMETRY EXTRACTOR
                };
            }
        }
    }
}

