/*
 * GaborSaliencyExtraction.h
 */

#pragma once

#include "../../../Common/Includes.h"
#include "../../../Common/DataTypes.h"
#include "../../../Mathematics/_2D/Kernels/ConvolutionGaborKernel2D.h"
#include "../../../Mathematics/_2D/Vector2D.h"
#include "../../PixelTypes.h"
#include "../../DiscreteRGBColorMap.h"
#include "../TImageProcess.hpp"
#include "SaliencyPixel.h"

#define _GABOR_SALIENCY_EXTRACTOR_DEFAULT_TOTAL_KERNEL_PAIRS_ 2
#define _GABOR_SALIENCY_EXTRACTOR_DEFAULT_STANDARD_DEVIATION_ Real(0.707106781)
#define _GABOR_SALIENCY_EXTRACTOR_DEFAULT_LAMBDA_ Real(0.5)
#define _GABOR_SALIENCY_EXTRACTOR_DEFAULT_GAMMA1_ Real(1.0)
#define _GABOR_SALIENCY_EXTRACTOR_DEFAULT_GAMMA2_ Real(1.0)

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Saliency
            {
                class CGaborSaliencyExtraction : public TImageProcess<Real, CSaliencyPixel>
                {
                public:

                    enum DisplayChannel
                    {
                        eChannelSaliencyMagnitude, eChannelSaliencyPhase
                    };

                    CGaborSaliencyExtraction(const TImage<Real>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone);
                    ~CGaborSaliencyExtraction() override;

                    bool SetTotalKernelPairs(const int TotalKernelPairs);
                    bool SetStandardDeviation(const Real StandardDeviation);
                    bool SetLambda(const Real Lambda);
                    bool SetGamma1(const Real Gamma1);
                    bool SetGamma2(const Real Gamma2);

                    int GetTotalKernelPairs() const;
                    Real GetStandardDeviation() const;
                    Real GetLambda() const;
                    Real GetGamma1() const;
                    Real GetGamma2() const;

                    bool Display(TImage<DiscreteRGBPixel>* pDisplayImage, const DisplayChannel Channel, const CDiscreteRGBColorMap* pColorMap, Real& MaximalChannelValue, Real& MinimalChannelValue, Real& ChannelMean, Real& ChannelStandardDeviation) const;

                    bool Execute(const int Trial) override;

                protected:

                    void MaskedClear();
                    void MaskedGaborConvolution(const Mathematics::_2D::Kernels::CConvolutionGaborKernel2D* pGaborKernelA, const Mathematics::_2D::Kernels::CConvolutionGaborKernel2D* pGaborKernelB);
                    void MaskedNormalize();
                    void Clear();
                    void GaborConvolution(const Mathematics::_2D::Kernels::CConvolutionGaborKernel2D* pGaborKernelA, const Mathematics::_2D::Kernels::CConvolutionGaborKernel2D* pGaborKernelB);
                    void Normalize();
                    void CreateKernels(const int KernelPairs);
                    void DestroyKernels();
                    void InitializeSaliencyPixels();

                    void InitializeActiveImageZone() override;
                    void InitializeActiveMaskImage() override;
                    bool UpdateActiveImageZone(const bool Force) override;
                    bool UpdateActiveMaskImage(const bool Force) override;

                    Real m_StandardDeviation;
                    Real m_Lambda;
                    Real m_Gamma1;
                    Real m_Gamma2;
                    std::vector<std::pair<Mathematics::_2D::Kernels::CConvolutionGaborKernel2D*, Mathematics::_2D::Kernels::CConvolutionGaborKernel2D*> > m_KernelPairs;
                };
            }
        }
    }
}

