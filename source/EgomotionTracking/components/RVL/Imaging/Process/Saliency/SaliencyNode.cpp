/*
 * SaliencyNode.cpp
 */

#include "SaliencyNode.h"
#include "SaliencyPixel.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Saliency
            {
                CSaliencyNode::CSaliencyNode() :
                    m_pPixel(nullptr),
                    m_OptimizedMagnitude(Real(0)),
                    m_SubpixelLocation(g_RealPlusInfinity, g_RealPlusInfinity),
                    m_Status(0),
                    m_pParent(nullptr),
                    m_pBroadCastingNode(nullptr),
                    m_BroadCastingGraphDistance(0),
                    m_BroadCastingPathDistance(Real(0)),
                    m_AWXX(Real(0)),
                    m_AWXY(Real(0)),
                    m_AWYY(Real(0)),
                    m_Lambda0(Real(0)),
                    m_Lambda1(Real(0)),
                    m_Ratio(Real(0)),
                    m_Eccentricity(Real(0)),
                    m_pComposedNode(nullptr)
                {
                }

                CSaliencyNode::CSaliencyNode(CSaliencyPixel* pPixel) :
                    m_pPixel(pPixel),
                    m_OptimizedMagnitude(pPixel->GetMagnitude()),
                    m_SubpixelLocation(pPixel->GetX(), pPixel->GetY()),
                    m_Status(0),
                    m_pParent(nullptr),
                    m_pBroadCastingNode(nullptr),
                    m_BroadCastingGraphDistance(0),
                    m_BroadCastingPathDistance(Real(0)),
                    m_AWXX(Real(0)),
                    m_AWXY(Real(0)),
                    m_AWYY(Real(0)),
                    m_Lambda0(Real(0)),
                    m_Lambda1(Real(0)),
                    m_Ratio(Real(0)),
                    m_Eccentricity(Real(0)),
                    m_pComposedNode(nullptr)
                {
                }

                CSaliencyNode::~CSaliencyNode()
                    = default;

                CSaliencyPixel* CSaliencyNode::GetWritablePixel()
                {
                    return m_pPixel;
                }

                const CSaliencyPixel* CSaliencyNode::GetReadOnlyPixel() const
                {
                    return m_pPixel;
                }

                Real CSaliencyNode::GetOptimizedMagnitude() const
                {
                    return m_OptimizedMagnitude;
                }

                const Mathematics::_2D::CVector2D& CSaliencyNode::GetSubpixelLocation()
                {
                    return m_SubpixelLocation;
                }

                Byte CSaliencyNode::GetStatus() const
                {
                    return m_Status;
                }

                void CSaliencyNode::Optimize(const CSaliencyPixel* pSaliencyBuffer, const int Width, const Real Precision)
                {
                    m_Status = 1;
                    const CSaliencyPixel* pBaseSaliencyBuffer = nullptr;
                    switch (m_pPixel->GetDiscreteDirectionIndexHalfConnectivity())
                    {
                        case 0:
                            pBaseSaliencyBuffer = pSaliencyBuffer + Width * (m_pPixel->GetY() - 2) + (m_pPixel->GetX() - 1);
                            break;
                        case 1:
                            pBaseSaliencyBuffer = pSaliencyBuffer + Width * (m_pPixel->GetY() - 2) + (m_pPixel->GetX() - 2);
                            break;
                        case 2:
                            pBaseSaliencyBuffer = pSaliencyBuffer + Width * (m_pPixel->GetY() - 1) + (m_pPixel->GetX() - 2);
                            break;
                        case 3:
                            pBaseSaliencyBuffer = pSaliencyBuffer + Width * (m_pPixel->GetY() - 1) + (m_pPixel->GetX() - 1);
                            break;
                    }
                    Real MagnitudeSupportPoles[4][4];
                    const int Offset = Width - 4;
                    for (int i = 0 ; i < 4 ; ++i, pBaseSaliencyBuffer += Offset)
                        for (int j = 0 ; j < 4 ; ++j, ++pBaseSaliencyBuffer)
                        {
                            MagnitudeSupportPoles[i][j] = pBaseSaliencyBuffer->GetMagnitude();
                        }
                    Real Coefficients[16];
                    Mathematics::_2D::Interpolation::CBicubicInterpolation::EstimateBicubicCoefficients(MagnitudeSupportPoles, Coefficients);
                    Mathematics::_2D::CVector2D A = m_SubpixelLocation;
                    Mathematics::_2D::CVector2D B = m_SubpixelLocation + m_pPixel->GetDirection();
                    Real MagnitudeA = Mathematics::_2D::Interpolation::CBicubicInterpolation::EvaluateBicubic(A.GetX(), A.GetY(), Coefficients);
                    Real MagnitudeB = Mathematics::_2D::Interpolation::CBicubicInterpolation::EvaluateBicubic(B.GetX(), B.GetY(), Coefficients);
                    Real Lenght = Real(1);
                    do
                    {
                        Lenght *= Real(0.5);
                        const Mathematics::_2D::CVector2D C = Mathematics::_2D::CVector2D::CreateMidPoint(A, B);
                        const Real MagnitudeC = Mathematics::_2D::Interpolation::CBicubicInterpolation::EvaluateBicubic(C.GetX(), C.GetY(), Coefficients);
                        if (MagnitudeC > MagnitudeB)
                        {
                            B = C;
                            MagnitudeB = MagnitudeC;
                        }
                        else
                        {
                            A = C;
                            MagnitudeA = MagnitudeC;
                        }
                    }
                    while ((Lenght > Precision) && (std::abs(MagnitudeA - MagnitudeB) > g_RealPlusEpsilon));
                    if (MagnitudeA > MagnitudeB)
                    {
                        m_SubpixelLocation = A;
                        m_OptimizedMagnitude = MagnitudeA;
                    }
                    else
                    {
                        m_SubpixelLocation = B;
                        m_OptimizedMagnitude = MagnitudeB;
                    }
                }

                bool CSaliencyNode::operator()(const CSaliencyNode* plhs, const CSaliencyNode* prhs) const
                {
                    return (plhs->m_OptimizedMagnitude < prhs->m_OptimizedMagnitude);
                }

                void CSaliencyNode::SetParent(CSaliencyNode* pParent)
                {
                    m_pParent = pParent;
                }

                void CSaliencyNode::AddChild(CSaliencyNode* pChild)
                {
                    m_Children.push_back(pChild);
                }

                bool CSaliencyNode::HasParent() const
                {
                    return m_pParent;
                }

                bool CSaliencyNode::CheckRedundant()
                {
                    if (m_Children.size())
                    {
                        m_Status = 1;
                        return false;
                    }
                    else
                    {
                        m_Status = 0;
                        return true;
                    }
                }

                void CSaliencyNode::StartBroadcasting()
                {
                    m_pBroadCastingNode = this;
                    m_BroadCastingGraphDistance = 0;
                    m_BroadCastingPathDistance = Real(0);
                }

                bool CSaliencyNode::Broadcast(CSaliencyNode* pBroadCastingNode, CSaliencyNode* pPreviousBroadCastingNode)
                {
                    if (m_pBroadCastingNode != pBroadCastingNode)
                    {
                        m_pBroadCastingNode = pBroadCastingNode;
                        m_BroadCastingGraphDistance = pPreviousBroadCastingNode->m_BroadCastingGraphDistance + 1;
                        m_BroadCastingPathDistance = pPreviousBroadCastingNode->m_BroadCastingPathDistance + m_SubpixelLocation.GetSquareDistance(pPreviousBroadCastingNode->m_SubpixelLocation);
                        return true;
                    }
                    return false;
                }

                void CSaliencyNode::SetComposedNode(Graphs::CSaliencyComposedNode* pComposedNode)
                {
                    m_pComposedNode = pComposedNode;
                }

                bool CSaliencyNode::HasComposedNode() const
                {
                    return m_pComposedNode;
                }

                const Graphs::CSaliencyComposedNode* CSaliencyNode::GetComposedNode() const
                {
                    return m_pComposedNode;
                }
            }
        }
    }
}
