/*
 * SaliencyPixel.cpp
 */

#include "SaliencyNode.h"
#include "SaliencyPixel.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Saliency
            {
                bool CSaliencyPixel::SortByMagnitudeAscent(const CSaliencyPixel* plhs, const CSaliencyPixel* prhs)
                {
                    return plhs->m_Magnitude < prhs->m_Magnitude;
                }

                bool CSaliencyPixel::SortByMagnitudeDescent(const CSaliencyPixel* plhs, const CSaliencyPixel* prhs)
                {
                    return plhs->m_Magnitude > prhs->m_Magnitude;
                }

                bool CSaliencyPixel::SortByNodeMagnitudeAscent(const CSaliencyPixel* plhs, const CSaliencyPixel* prhs)
                {
                    return plhs->m_pNode->GetOptimizedMagnitude() < prhs->m_pNode->GetOptimizedMagnitude();
                }

                bool CSaliencyPixel::SortByNodeMagnitudeDescent(const CSaliencyPixel* plhs, const CSaliencyPixel* prhs)
                {
                    return plhs->m_pNode->GetOptimizedMagnitude() > prhs->m_pNode->GetOptimizedMagnitude();
                }

                CSaliencyPixel::CSaliencyPixel() :
                    m_X(g_IntegerPlusInfinity),
                    m_Y(g_IntegerPlusInfinity),
                    m_pNode(nullptr),
                    m_Direction(),
                    m_Magnitude(Real(0)),
                    m_StatusBits(0)
                {
                }

                CSaliencyPixel::~CSaliencyPixel()
                {
                    if (m_pNode)
                    {
                        delete m_pNode;
                        m_pNode = nullptr;
                    }
                }

                void CSaliencyPixel::Clear()
                {
                    if (m_pNode)
                    {
                        delete m_pNode;
                        m_pNode = nullptr;
                    }
                    m_Direction.SetZero();
                    m_Magnitude = Real(0);
                    m_StatusBits = 0;
                }

                void CSaliencyPixel::SetLocation(const int X, const int Y)
                {
                    m_X = X;
                    m_Y = Y;
                }

                void CSaliencyPixel::SetStatusBit0()
                {
                    m_StatusBits |= 0X1;
                }

                void CSaliencyPixel::SetStatusBit1()
                {
                    m_StatusBits |= 0X2;
                }

                void CSaliencyPixel::SetStatusBit2()
                {
                    m_StatusBits |= 0X4;
                }

                void CSaliencyPixel::SetStatusBit3()
                {
                    m_StatusBits |= 0X8;
                }

                void CSaliencyPixel::SetStatusBit4()
                {
                    m_StatusBits |= 0X10;
                }

                void CSaliencyPixel::SetStatusBit5()
                {
                    m_StatusBits |= 0X20;
                }

                void CSaliencyPixel::SetStatusBit6()
                {
                    m_StatusBits |= 0X40;
                }

                void CSaliencyPixel::SetStatusBit7()
                {
                    m_StatusBits |= 0X80;
                }

                void CSaliencyPixel::SetStatusBits(const Byte StatusBits)
                {
                    m_StatusBits = StatusBits;
                }

                void CSaliencyPixel::Normalize()
                {
                    m_Magnitude = m_Direction.Normalize();
                }

                void CSaliencyPixel::AddOffset(const Real DX, const Real DY)
                {
                    m_Direction.AddOffset(DX, DY);
                }

                CSaliencyNode* CSaliencyPixel::CreateNode()
                {
                    m_pNode = new CSaliencyNode(this);
                    return m_pNode;
                }

                int CSaliencyPixel::GetX() const
                {
                    return m_X;
                }

                int CSaliencyPixel::GetY() const
                {
                    return m_Y;
                }

                int CSaliencyPixel::GetDiscreteDirectionIndexFullConnectivity() const
                {
                    if (m_Direction.GetX() >= Real(0.38268343236509))
                    {
                        if (m_Direction.GetY() >= Real(0.38268343236509))
                        {
                            return 1;
                        }
                        else if (m_Direction.GetY() <= Real(-0.38268343236509))
                        {
                            return 7;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else if (m_Direction.GetX() <= Real(-0.38268343236509))
                    {
                        if (m_Direction.GetY() >= Real(0.38268343236509))
                        {
                            return 3;
                        }
                        else if (m_Direction.GetY() <= Real(-0.38268343236509))
                        {
                            return 5;
                        }
                        else
                        {
                            return 4;
                        }
                    }
                    else
                    {
                        if (m_Direction.GetY() >= Real(0.38268343236509))
                        {
                            return 2;
                        }
                        else if (m_Direction.GetY() <= Real(-0.38268343236509))
                        {
                            return 6;
                        }
                        else
                        {
                            return -1;
                        }
                    }
                }

                int CSaliencyPixel::GetDiscreteDirectionIndexHalfConnectivity() const
                {
                    if (m_Direction.GetX() >= Real(0))
                    {
                        if (m_Direction.GetY() >= Real(0))
                        {
                            return 0;
                        }
                        else
                        {
                            return 3;
                        }
                    }
                    else
                    {
                        if (m_Direction.GetY() >= Real(0))
                        {
                            return 1;
                        }
                        else
                        {
                            return 2;
                        }
                    }
                }

                int CSaliencyPixel::GetDiscreteDirectionX() const
                {
                    if (m_Direction.GetX() >= Real(0.38268343236509))
                    {
                        return 1;
                    }
                    else if (m_Direction.GetX() <= Real(-0.38268343236509))
                    {
                        return -1;
                    }
                    return 0;
                }

                int CSaliencyPixel::GetDiscreteDirectionY() const
                {
                    if (m_Direction.GetY() >= Real(0.38268343236509))
                    {
                        return 1;
                    }
                    else if (m_Direction.GetY() <= Real(-0.38268343236509))
                    {
                        return -1;
                    }
                    return 0;
                }

                CSaliencyNode* CSaliencyPixel::GetWritableNode()
                {
                    return m_pNode;
                }

                const CSaliencyNode* CSaliencyPixel::GetReadOnlyNode() const
                {
                    return m_pNode;
                }

                const Mathematics::_2D::CVector2D& CSaliencyPixel::GetDirection() const
                {
                    return m_Direction;
                }

                bool CSaliencyPixel::HasMagnitude() const
                {
                    return IsPositive(m_Magnitude);
                }

                Real CSaliencyPixel::GetMagnitude() const
                {
                    return m_Magnitude;
                }

                Real CSaliencyPixel::GetPhase() const
                {
                    return m_Direction.GetAngle();
                }

                bool CSaliencyPixel::GetStatusBit0() const
                {
                    return m_StatusBits & 0X1;
                }

                bool CSaliencyPixel::GetStatusBit1() const
                {
                    return m_StatusBits & 0X2;
                }

                bool CSaliencyPixel::GetStatusBit2() const
                {
                    return m_StatusBits & 0X4;
                }

                bool CSaliencyPixel::GetStatusBit3() const
                {
                    return m_StatusBits & 0X8;
                }

                bool CSaliencyPixel::GetStatusBit4() const
                {
                    return m_StatusBits & 0X10;
                }

                bool CSaliencyPixel::GetStatusBit5() const
                {
                    return m_StatusBits & 0X20;
                }

                bool CSaliencyPixel::GetStatusBit6() const
                {
                    return m_StatusBits & 0X40;
                }

                bool CSaliencyPixel::GetStatusBit7() const
                {
                    return m_StatusBits & 0X80;
                }

                Byte CSaliencyPixel::GetStatusBits() const
                {
                    return m_StatusBits;
                }
            }
        }
    }
}
