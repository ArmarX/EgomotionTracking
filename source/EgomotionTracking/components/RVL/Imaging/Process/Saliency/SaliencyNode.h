/*
 * SaliencyNode.h
 */

#pragma once

#include "../../../Common/DataTypes.h"
#include "../../../Mathematics/_2D/Vector2D.h"
#include "../../../Mathematics/_2D/Interpolation/BicubicInterpolation.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Saliency
            {
                namespace Graphs
                {
                    class CSaliencyGraph;
                    class CSaliencyComposedNode;
                }

                class CSaliencyPixel;

                class CSaliencyNode
                {
                public:

                    CSaliencyNode();
                    CSaliencyNode(CSaliencyPixel* pPixel);
                    virtual ~CSaliencyNode();

                    CSaliencyPixel* GetWritablePixel();
                    const CSaliencyPixel* GetReadOnlyPixel() const;
                    Real GetOptimizedMagnitude() const;
                    const Mathematics::_2D::CVector2D& GetSubpixelLocation();
                    Byte GetStatus() const;

                    void Optimize(const CSaliencyPixel* pSaliencyBuffer, const int Width, const Real Precision);

                    bool operator()(const CSaliencyNode* plhs, const CSaliencyNode* prhs) const;

                    void SetParent(CSaliencyNode* pParent);
                    void AddChild(CSaliencyNode* pChild);

                    bool HasParent() const;
                    bool CheckRedundant();

                    void StartBroadcasting();
                    bool Broadcast(CSaliencyNode* pBroadCastingNode, CSaliencyNode* pPreviousBroadCastingNode);

                    void SetComposedNode(Graphs::CSaliencyComposedNode* pComposedNode);
                    bool HasComposedNode() const;
                    const Graphs::CSaliencyComposedNode* GetComposedNode() const;

                protected:

                    friend class Graphs::CSaliencyGraph;

                    CSaliencyPixel* m_pPixel;

                    Real m_OptimizedMagnitude;
                    Mathematics::_2D::CVector2D m_SubpixelLocation;
                    Byte m_Status;

                    CSaliencyNode* m_pParent;
                    std::list<CSaliencyNode*> m_Children;

                    CSaliencyNode* m_pBroadCastingNode;
                    int m_BroadCastingGraphDistance;
                    Real m_BroadCastingPathDistance;

                    Real m_AWXX;
                    Real m_AWXY;
                    Real m_AWYY;
                    Real m_Lambda0;
                    Real m_Lambda1;
                    Real m_Ratio;
                    Real m_Eccentricity;

                    Graphs::CSaliencyComposedNode* m_pComposedNode;
                };
            }
        }
    }
}

