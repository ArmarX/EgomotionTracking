/*
 * SaliencyPixel.h
 */

#pragma once

#include "../../../Common/DataTypes.h"
#include "../../../Mathematics/_2D/Vector2D.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Saliency
            {
                class CSaliencyNode;
                class CSaliencyPixel
                {
                public:

                    static bool SortByMagnitudeAscent(const CSaliencyPixel* plhs, const CSaliencyPixel* prhs);
                    static bool SortByMagnitudeDescent(const CSaliencyPixel* plhs, const CSaliencyPixel* prhs);

                    static bool SortByNodeMagnitudeAscent(const CSaliencyPixel* plhs, const CSaliencyPixel* prhs);
                    static bool SortByNodeMagnitudeDescent(const CSaliencyPixel* plhs, const CSaliencyPixel* prhs);

                    CSaliencyPixel();
                    ~CSaliencyPixel();

                    void SetLocation(const int X, const int Y);
                    void SetStatusBit0();
                    void SetStatusBit1();
                    void SetStatusBit2();
                    void SetStatusBit3();
                    void SetStatusBit4();
                    void SetStatusBit5();
                    void SetStatusBit6();
                    void SetStatusBit7();
                    void SetStatusBits(const Byte StatusBits);
                    void Clear();
                    void Normalize();
                    void AddOffset(const Real DX, const Real DY);
                    CSaliencyNode* CreateNode();

                    int GetX() const;
                    int GetY() const;
                    int GetDiscreteDirectionIndexFullConnectivity() const;
                    int GetDiscreteDirectionIndexHalfConnectivity() const;
                    int GetDiscreteDirectionX() const;
                    int GetDiscreteDirectionY() const;
                    CSaliencyNode* GetWritableNode();
                    const CSaliencyNode* GetReadOnlyNode() const;
                    const Mathematics::_2D::CVector2D& GetDirection() const;
                    bool HasMagnitude() const;
                    Real GetMagnitude() const;
                    Real GetPhase() const;
                    bool GetStatusBit0() const;
                    bool GetStatusBit1() const;
                    bool GetStatusBit2() const;
                    bool GetStatusBit3() const;
                    bool GetStatusBit4() const;
                    bool GetStatusBit5() const;
                    bool GetStatusBit6() const;
                    bool GetStatusBit7() const;
                    Byte GetStatusBits() const;

                protected:

                    int m_X;
                    int m_Y;
                    CSaliencyNode* m_pNode;
                    Mathematics::_2D::CVector2D m_Direction;
                    Real m_Magnitude;
                    Byte m_StatusBits;
                };
            }
        }
    }
}

