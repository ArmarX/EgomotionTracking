/*
 * EdgeExtraction.cpp
 */

#include "EdgeExtraction.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Saliency
            {
                namespace Edge
                {
                    CEdgeExtraction::CEdgeExtraction(const TImage<CSaliencyPixel>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone) :
                        TImageProcess<CSaliencyPixel, CSaliencyPixel>(pInImage, pInActiveMaskImage, pInActiveImageZone),
                        m_EnableCoherence(false),
                        m_EnableOptimazingMagnitude(true),
                        m_EnableMagnitudeFiltering(false),
                        m_Coherence(Real(0)),
                        m_Percentile(Real(0)),
                        m_Precision(Real(0.000488281))
                    {
                        if (IsEnabled())
                        {
                            UpdateActiveImageZones();
                            m_OutImage.Wrap(const_cast<TImage<CSaliencyPixel>*>(m_pInImage));
                        }
                    }

                    CEdgeExtraction::~CEdgeExtraction()
                    {
                        Clear();
                    }

                    void CEdgeExtraction::SetEnableCoherence(const bool EnableCoherence)
                    {
                        m_EnableCoherence = EnableCoherence;
                    }

                    void CEdgeExtraction::SetEnableOptimazingMagnitude(const bool EnableOptimazingMagnitude)
                    {
                        m_EnableOptimazingMagnitude = EnableOptimazingMagnitude;
                    }

                    void CEdgeExtraction::SetEnableMagnitudeFiltering(const bool EnableMagnitudeFiltering)
                    {
                        m_EnableMagnitudeFiltering = EnableMagnitudeFiltering;
                    }

                    void CEdgeExtraction::SetCoherence(const bool Coherence)
                    {
                        m_Coherence = Coherence;
                    }

                    void CEdgeExtraction::SetPercentile(const bool Percentile)
                    {
                        if (IsPositive(Percentile) && (Percentile <= Real(1)))
                        {
                            m_Percentile = Percentile;
                        }
                    }

                    void CEdgeExtraction::SetPrecision(const bool Precision)
                    {
                        if (IsPositive(Precision) && (Precision <= Real(0.0625)))
                        {
                            m_Precision = Precision;
                        }
                    }

                    bool CEdgeExtraction::GetEnableCoherence() const
                    {
                        return m_EnableCoherence;
                    }

                    bool CEdgeExtraction::GetEnableOptimazingMagnitude() const
                    {
                        return m_EnableOptimazingMagnitude;
                    }

                    bool CEdgeExtraction::GetEnableMagnitudeFiltering() const
                    {
                        return m_EnableMagnitudeFiltering;
                    }

                    Real CEdgeExtraction::GetCoherence() const
                    {
                        return m_Coherence;
                    }

                    Real CEdgeExtraction::GetPercentile() const
                    {
                        return m_Percentile;
                    }

                    Real CEdgeExtraction::GetPrecision() const
                    {
                        return m_Precision;
                    }

                    bool CEdgeExtraction::Execute(const int Trial)
                    {
                        if (IsEnabled() && ((!m_pInActiveImageZone) || (m_pInActiveImageZone->IsValid() && m_pInImage->IsInside(m_pInActiveImageZone))))
                        {
                            if (SelectPixels())
                            {
                                if (m_EnableOptimazingMagnitude)
                                {
                                    OptimazingMagnitude();
                                }
                                if (m_EnableMagnitudeFiltering)
                                {
                                    FilterPixelsByMagnitude();
                                }
                                return true;
                            }
                        }
                        return false;
                    }

                    bool CEdgeExtraction::Display(TImage<DiscreteRGBPixel>* pDisplayImage, const CDiscreteRGBColorMap* pColorMap, Real& MaximalChannelValue, Real& MinimalChannelValue, Real& ChannelMean, Real& ChannelStandardDeviation) const
                    {
                        if (pDisplayImage && pDisplayImage->IsValid() && pDisplayImage->HasBufferOwnership() && pDisplayImage->SizeEquals(m_OutImage.GetSize()) && pDisplayImage->HasBufferOwnership())
                        {
                            pDisplayImage->Clear();
                            const int X0 = m_OutActiveImageZone.GetX0();
                            const int Y0 = m_OutActiveImageZone.GetY0();
                            const int X1 = m_OutActiveImageZone.GetX1();
                            const int Y1 = m_OutActiveImageZone.GetY1();
                            const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                            TImage<Real> ChannelImage(pDisplayImage->GetSize());
                            Real* pChannelPixel = ChannelImage.GetWritableBufferAt(X0, Y0);
                            Real ChannelAccumulator = Real(0), ChannelSquareAccumulator = Real(0);
                            const CSaliencyPixel* pSaliencyPixel = m_OutImage.GetReadOnlyBufferAt(X0, Y0);
                            MinimalChannelValue = g_RealPlusInfinity;
                            MaximalChannelValue = g_RealMinusInfinity;
                            int TotalEdgePixels = 0;
                            if (m_EnableMagnitudeFiltering)
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset, pChannelPixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel, ++pChannelPixel)
                                        if (pSaliencyPixel->GetStatusBit1())
                                        {
                                            const Real Value = pSaliencyPixel->GetMagnitude();
                                            if (Value > MaximalChannelValue)
                                            {
                                                MaximalChannelValue = Value;
                                            }
                                            if (Value < MinimalChannelValue)
                                            {
                                                MinimalChannelValue = Value;
                                            }
                                            ChannelAccumulator += Value;
                                            ChannelSquareAccumulator += Value * Value;
                                            *pChannelPixel = Value;
                                            ++TotalEdgePixels;
                                        }
                            }
                            else
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset, pChannelPixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel, ++pChannelPixel)
                                        if (pSaliencyPixel->GetStatusBit0())
                                        {
                                            const Real Value = pSaliencyPixel->GetMagnitude();
                                            if (Value > MaximalChannelValue)
                                            {
                                                MaximalChannelValue = Value;
                                            }
                                            if (Value < MinimalChannelValue)
                                            {
                                                MinimalChannelValue = Value;
                                            }
                                            ChannelAccumulator += Value;
                                            ChannelSquareAccumulator += Value * Value;
                                            *pChannelPixel = Value;
                                            ++TotalEdgePixels;
                                        }
                            }
                            ChannelMean = ChannelAccumulator / Real(TotalEdgePixels);
                            const Real Range = MaximalChannelValue - MinimalChannelValue;
                            if (IsPositive(Range))
                            {
                                ChannelStandardDeviation = std::sqrt((ChannelSquareAccumulator / Real(TotalEdgePixels)) - (ChannelMean * ChannelMean));
                                if (pColorMap && pColorMap->IsValid())
                                {
                                    const DiscreteRGBPixel* pLUT = pColorMap->GetLUT();
                                    const Real NormalizationFactor = Real(pColorMap->GetLUTScaleFactor()) / Range;
                                    const Real* pChannelPixel = ChannelImage.GetReadOnlyBufferAt(X0, Y0);
                                    DiscreteRGBPixel* pDiscreteRGBPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pChannelPixel += Offset, pDiscreteRGBPixel += Offset)
                                        for (int X = X0 ; X <= X1 ; ++X, ++pDiscreteRGBPixel, ++pChannelPixel)
                                            if (*pChannelPixel)
                                            {
                                                *pDiscreteRGBPixel = pLUT[int(NormalizationFactor * (*pChannelPixel - MinimalChannelValue) + Real(0.5))];
                                            }
                                }
                                else
                                {
                                    const Real NormalizationFactor = Real(255) / Range;
                                    const Real* pChannelPixel = ChannelImage.GetReadOnlyBufferAt(X0, Y0);
                                    DiscreteRGBPixel* pDiscreteRGBPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pChannelPixel += Offset, pDiscreteRGBPixel += Offset)
                                        for (int X = X0 ; X <= X1 ; ++X, ++pDiscreteRGBPixel, ++pChannelPixel)
                                            if (*pChannelPixel)
                                            {
                                                pDiscreteRGBPixel->Set(Byte(NormalizationFactor * (*pChannelPixel - MinimalChannelValue) + Real(0.5)));
                                            }
                                }
                                return true;
                            }
                        }
                        return false;
                    }

                    bool CEdgeExtraction::SelectPixels()
                    {
                        const int X0 = m_OutActiveImageZone.GetX0();
                        const int Y0 = m_OutActiveImageZone.GetY0();
                        const int X1 = m_OutActiveImageZone.GetX1();
                        const int Y1 = m_OutActiveImageZone.GetY1();
                        const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                        CSaliencyPixel* pSaliencyPixel = m_OutImage.GetWritableBufferAt(X0, Y0, true);
                        const int Deltas[8] = { 1, 1 - m_OutImage.GetWidth(), -m_OutImage.GetWidth(), -m_OutImage.GetWidth() - 1, -1, m_OutImage.GetWidth() - 1, m_OutImage.GetWidth(), m_OutImage.GetWidth() + 1 };
                        Clear();
                        if (IsMasking())
                        {
                            const bool* pActiveMaskPixel = m_pInActiveMaskImage->GetReadOnlyBufferAt(X0, Y0);
                            if (m_EnableCoherence)
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel, ++pActiveMaskPixel)
                                        if (*pActiveMaskPixel)
                                        {
                                            const int DiscreteDirectionIndex = pSaliencyPixel->GetDiscreteDirectionIndexFullConnectivity();
                                            const int DeltaForward = Deltas[DiscreteDirectionIndex];
                                            const int DeltaBackwards = Deltas[(DiscreteDirectionIndex + 4) & 0XF];
                                            if (pActiveMaskPixel[DeltaForward] && pActiveMaskPixel[DeltaBackwards] && (pSaliencyPixel->GetMagnitude() > pSaliencyPixel[DeltaForward].GetMagnitude()) && (pSaliencyPixel->GetMagnitude() > pSaliencyPixel[DeltaBackwards].GetMagnitude()) && (pSaliencyPixel->GetDirection().ScalarProduct(pSaliencyPixel[DeltaForward].GetDirection()) > m_Coherence) && (pSaliencyPixel->GetDirection().ScalarProduct(pSaliencyPixel[DeltaBackwards].GetDirection()) > m_Coherence))
                                            {
                                                pSaliencyPixel->SetStatusBit0();
                                                m_SelectedEdgePixels.push_back(pSaliencyPixel);
                                            }
                                        }
                            }
                            else
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel, ++pActiveMaskPixel)
                                        if (*pActiveMaskPixel)
                                        {
                                            const int DiscreteDirectionIndex = pSaliencyPixel->GetDiscreteDirectionIndexFullConnectivity();
                                            const int DeltaForward = Deltas[DiscreteDirectionIndex];
                                            const int DeltaBackwards = Deltas[(DiscreteDirectionIndex + 4) & 0XF];
                                            if (pActiveMaskPixel[DeltaForward] && pActiveMaskPixel[DeltaBackwards] && (pSaliencyPixel->GetMagnitude() > pSaliencyPixel[DeltaForward].GetMagnitude()) && (pSaliencyPixel->GetMagnitude() > pSaliencyPixel[DeltaBackwards].GetMagnitude()))
                                            {
                                                pSaliencyPixel->SetStatusBit0();
                                                m_SelectedEdgePixels.push_back(pSaliencyPixel);
                                            }
                                        }
                        }
                        else
                        {
                            if (m_EnableCoherence)
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel)
                                    {
                                        const int DiscreteDirectionIndex = pSaliencyPixel->GetDiscreteDirectionIndexFullConnectivity();
                                        const int DeltaForward = Deltas[DiscreteDirectionIndex];
                                        const int DeltaBackwards = Deltas[(DiscreteDirectionIndex + 4) & 0XF];
                                        if ((pSaliencyPixel->GetMagnitude() > pSaliencyPixel[DeltaForward].GetMagnitude()) && (pSaliencyPixel->GetMagnitude() > pSaliencyPixel[DeltaBackwards].GetMagnitude()) && (pSaliencyPixel->GetDirection().ScalarProduct(pSaliencyPixel[DeltaForward].GetDirection()) > m_Coherence) && (pSaliencyPixel->GetDirection().ScalarProduct(pSaliencyPixel[DeltaBackwards].GetDirection()) > m_Coherence))
                                        {
                                            pSaliencyPixel->SetStatusBit0();
                                            m_SelectedEdgePixels.push_back(pSaliencyPixel);
                                        }
                                    }
                            }
                            else
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel)
                                    {
                                        const int DiscreteDirectionIndex = pSaliencyPixel->GetDiscreteDirectionIndexFullConnectivity();
                                        if ((pSaliencyPixel->GetMagnitude() > pSaliencyPixel[Deltas[DiscreteDirectionIndex]].GetMagnitude()) && (pSaliencyPixel->GetMagnitude() > pSaliencyPixel[Deltas[(DiscreteDirectionIndex + 4) & 0XF]].GetMagnitude()))
                                        {
                                            pSaliencyPixel->SetStatusBit0();
                                            m_SelectedEdgePixels.push_back(pSaliencyPixel);
                                        }
                                    }
                        }
                        return m_SelectedEdgePixels.size();
                    }

                    void CEdgeExtraction::OptimazingMagnitude()
                    {
                        const CSaliencyPixel* pSaliencyBuffer = m_pInImage->GetReadOnlyBuffer();
                        const int Width = m_pInImage->GetWidth();
                        for (auto& m_SelectedEdgePixel : m_SelectedEdgePixels)
                        {
                            CSaliencyNode* pNode = m_SelectedEdgePixel->CreateNode();
                            pNode->Optimize(pSaliencyBuffer, Width, m_Precision);
                            m_SelectedEdgeNodes.push_back(pNode);
                        }
                    }

                    void CEdgeExtraction::FilterPixelsByMagnitude()
                    {
                        if (m_SelectedEdgePixels.size())
                        {
                            if (m_EnableOptimazingMagnitude)
                            {
                                m_SelectedEdgePixels.sort(CSaliencyPixel::SortByMagnitudeDescent);
                            }
                            else
                            {
                                m_SelectedEdgePixels.sort(CSaliencyPixel::SortByMagnitudeDescent);
                            }
                            const int PercentilIndex = std::min(int(std::ceil(m_Percentile * Real(m_SelectedEdgePixels.size()))), int(m_SelectedEdgePixels.size()));
                            int Index = 0;
                            for (std::list<CSaliencyPixel*>::iterator ppSaliencyPixel = m_SelectedEdgePixels.begin() ; (Index < PercentilIndex) && (ppSaliencyPixel != m_SelectedEdgePixels.end()) ; ++ppSaliencyPixel)
                            {
                                (*ppSaliencyPixel)->SetStatusBit1();
                            }
                        }
                    }

                    void CEdgeExtraction::Clear()
                    {
                        if (m_SelectedEdgePixels.size())
                        {
                            for (auto& m_SelectedEdgePixel : m_SelectedEdgePixels)
                            {
                                m_SelectedEdgePixel->Clear();
                            }
                            m_SelectedEdgePixels.clear();
                            m_SelectedEdgeNodes.clear();
                        }
                    }

                    bool CEdgeExtraction::UpdateActiveImageZones()
                    {
                        const int Radius = m_EnableOptimazingMagnitude ? 2 : 1;
                        if (m_pInActiveImageZone)
                        {
                            if (m_pInActiveImageZone->IsValid())
                            {
                                m_OutActiveImageZone.Set(*m_pInActiveImageZone, Radius);
                                return true;
                            }
                        }
                        else
                        {
                            if (m_pInImage && m_pInImage->IsValid())
                            {
                                return m_OutActiveImageZone.Set(Radius, Radius, m_pInImage->GetWidth() - Radius - 1, m_pInImage->GetHeight() - Radius - 1);
                            }
                        }
                        return false;
                    }
                }
            }
        }
    }
}
