/*
 * EdgeExtraction.h
 */

#pragma once

#include "../../../PixelTypes.h"
#include "../../../DiscreteRGBColorMap.h"
#include "../../TImageProcess.hpp"
#include "../SaliencyPixel.h"
#include "../SaliencyNode.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Saliency
            {
                namespace Edge
                {
                    class CEdgeExtraction : public TImageProcess<CSaliencyPixel, CSaliencyPixel>
                    {
                    public:

                        CEdgeExtraction(const TImage<CSaliencyPixel>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone);
                        ~CEdgeExtraction() override;

                        void SetEnableCoherence(const bool EnableCoherence);
                        void SetEnableOptimazingMagnitude(const bool EnableOptimazingMagnitude);
                        void SetEnableMagnitudeFiltering(const bool EnableMagnitudeFiltering);
                        void SetCoherence(const bool Coherence);
                        void SetPercentile(const bool Percentile);
                        void SetPrecision(const bool Precision);

                        bool GetEnableCoherence() const;
                        bool GetEnableOptimazingMagnitude() const;
                        bool GetEnableMagnitudeFiltering() const;
                        Real GetCoherence() const;
                        Real GetPercentile() const;
                        Real GetPrecision() const;

                        bool Execute(const int Trial) override;

                        bool Display(TImage<DiscreteRGBPixel>* pDisplayImage, const CDiscreteRGBColorMap* pColorMap, Real& MaximalChannelValue, Real& MinimalChannelValue, Real& ChannelMean, Real& ChannelStandardDeviation) const;

                    protected:

                        virtual bool UpdateActiveImageZones();

                        bool SelectPixels();
                        void OptimazingMagnitude();
                        void FilterPixelsByMagnitude();
                        void Clear();

                        bool m_EnableCoherence;
                        bool m_EnableOptimazingMagnitude;
                        bool m_EnableMagnitudeFiltering;
                        Real m_Coherence;
                        Real m_Percentile;
                        Real m_Precision;

                        std::list<CSaliencyPixel*> m_SelectedEdgePixels;
                        std::list<CSaliencyNode*> m_SelectedEdgeNodes;
                    };
                }
            }
        }
    }
}

