/*
 * GaborSaliencyExtraction.cpp
 */

#include "GaborSaliencyExtraction.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Saliency
            {
                CGaborSaliencyExtraction::CGaborSaliencyExtraction(const TImage<Real>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone) :
                    TImageProcess<Real, CSaliencyPixel>(pInImage, pInActiveMaskImage, pInActiveImageZone),
                    m_StandardDeviation(_GABOR_SALIENCY_EXTRACTOR_DEFAULT_STANDARD_DEVIATION_),
                    m_Lambda(_GABOR_SALIENCY_EXTRACTOR_DEFAULT_LAMBDA_),
                    m_Gamma1(_GABOR_SALIENCY_EXTRACTOR_DEFAULT_GAMMA1_),
                    m_Gamma2(_GABOR_SALIENCY_EXTRACTOR_DEFAULT_GAMMA2_),
                    m_KernelPairs()
                {
                    if (IsEnabled())
                    {
                        CreateKernels(_GABOR_SALIENCY_EXTRACTOR_DEFAULT_TOTAL_KERNEL_PAIRS_);
                        InitializeSaliencyPixels();
                        InitializeActiveMaskImage();
                        SetEnabled(UpdateActiveImageZone(true) && UpdateActiveMaskImage(true));
                    }
                }

                CGaborSaliencyExtraction::~CGaborSaliencyExtraction()
                {
                    DestroyKernels();
                }

                bool CGaborSaliencyExtraction::SetTotalKernelPairs(const int TotalKernelPairs)
                {
                    if (TotalKernelPairs > 0)
                    {
                        if (TotalKernelPairs != int(m_KernelPairs.size()))
                        {
                            CreateKernels(TotalKernelPairs);
                        }
                        return true;
                    }
                    return false;
                }

                bool CGaborSaliencyExtraction::SetStandardDeviation(const Real StandardDeviation)
                {
                    if (IsPositive(StandardDeviation))
                    {
                        if (NonEquals(StandardDeviation, m_StandardDeviation))
                        {
                            m_StandardDeviation = StandardDeviation;
                            CreateKernels(m_KernelPairs.size());
                        }
                        return true;
                    }
                    return false;
                }

                bool CGaborSaliencyExtraction::SetLambda(const Real Lambda)
                {
                    if (IsPositive(Lambda) && (Lambda < g_RealPI))
                    {
                        if (NonEquals(Lambda, m_Lambda))
                        {
                            m_Lambda = Lambda;
                            CreateKernels(m_KernelPairs.size());
                        }
                        return true;
                    }
                    return false;
                }

                bool CGaborSaliencyExtraction::SetGamma1(const Real Gamma1)
                {
                    if (IsPositive(Gamma1))
                    {
                        if (NonEquals(Gamma1, m_Gamma1))
                        {
                            m_Gamma1 = Gamma1;
                            CreateKernels(m_KernelPairs.size());
                        }
                        return true;
                    }
                    return false;
                }

                bool CGaborSaliencyExtraction::SetGamma2(const Real Gamma2)
                {
                    if (IsPositive(Gamma2))
                    {
                        if (NonEquals(Gamma2, m_Gamma2))
                        {
                            m_Gamma2 = Gamma2;
                            CreateKernels(m_KernelPairs.size());
                        }
                        return true;
                    }
                    return false;
                }

                int CGaborSaliencyExtraction::GetTotalKernelPairs() const
                {
                    return m_KernelPairs.size();
                }

                Real CGaborSaliencyExtraction::GetStandardDeviation() const
                {
                    return m_StandardDeviation;
                }

                Real CGaborSaliencyExtraction::GetLambda() const
                {
                    return m_Lambda;
                }

                Real CGaborSaliencyExtraction::GetGamma1() const
                {
                    return m_Gamma1;
                }

                Real CGaborSaliencyExtraction::GetGamma2() const
                {
                    return m_Gamma2;
                }

                bool CGaborSaliencyExtraction::Display(TImage<DiscreteRGBPixel>* pDisplayImage, const DisplayChannel Channel, const CDiscreteRGBColorMap* pColorMap, Real& MaximalChannelValue, Real& MinimalChannelValue, Real& ChannelMean, Real& ChannelStandardDeviation) const
                {
                    if (pDisplayImage && pDisplayImage->IsValid() && pDisplayImage->HasBufferOwnership() && pDisplayImage->SizeEquals(m_OutImage.GetSize()) && pDisplayImage->HasBufferOwnership() && pColorMap && pColorMap->IsValid())
                    {
                        pDisplayImage->Clear();
                        const int X0 = m_OutActiveImageZone.GetX0();
                        const int Y0 = m_OutActiveImageZone.GetY0();
                        const int X1 = m_OutActiveImageZone.GetX1();
                        const int Y1 = m_OutActiveImageZone.GetY1();
                        const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                        int TotalActivePixels = 0;
                        TImage<Real> ChannelImage(pDisplayImage->GetSize());
                        Real* pChannelPixel = ChannelImage.GetWritableBufferAt(X0, Y0);
                        Real ChannelAccumulator = Real(0), ChannelSquareAccumulator = Real(0);
                        const CSaliencyPixel* pSaliencyPixel = m_OutImage.GetReadOnlyBufferAt(X0, Y0);
                        switch (Channel)
                        {
                            case eChannelSaliencyMagnitude:
                            {
                                /*if (IsMasking())
                                 {
                                 MinimalChannelValue = g_RealPlusInfinity;
                                 MaximalChannelValue = g_RealMinusInfinity;
                                 const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                 for (int Y = Y0; Y <= Y1; ++Y, pSaliencyPixel += Offset, pChannelPixel += Offset, pActiveMaskPixel += Offset)
                                 for (int X = X0; X <= X1; ++X, ++pSaliencyPixel, ++pChannelPixel)
                                 if (*pActiveMaskPixel++)
                                 {
                                 const Real Value = pSaliencyPixel->GetMagnitude();
                                 if (Value > MaximalChannelValue)
                                 MaximalChannelValue = Value;
                                 if (Value < MinimalChannelValue)
                                 MinimalChannelValue = Value;
                                 ChannelAccumulator += Value;
                                 ChannelSquareAccumulator += Value * Value;
                                 *pChannelPixel = Value;
                                 ++TotalActivePixels;
                                 }
                                 }
                                 else*/
                                {
                                    TotalActivePixels = m_OutActiveImageZone.GetArea();
                                    MinimalChannelValue = MaximalChannelValue = pSaliencyPixel->GetMagnitude();
                                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset, pChannelPixel += Offset)
                                        for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel)
                                        {
                                            const Real Value = pSaliencyPixel->GetMagnitude();
                                            if (Value > MaximalChannelValue)
                                            {
                                                MaximalChannelValue = Value;
                                            }
                                            else if (Value < MinimalChannelValue)
                                            {
                                                MinimalChannelValue = Value;
                                            }
                                            ChannelAccumulator += Value;
                                            ChannelSquareAccumulator += Value * Value;
                                            *pChannelPixel++ = Value;
                                        }
                                }
                                ChannelMean = ChannelAccumulator / Real(TotalActivePixels);
                                const Real Range = MaximalChannelValue - MinimalChannelValue;
                                if (IsPositive(Range))
                                {
                                    ChannelStandardDeviation = std::sqrt((ChannelSquareAccumulator / Real(TotalActivePixels)) - (ChannelMean * ChannelMean));
                                    const DiscreteRGBPixel* pLUT = pColorMap->GetLUT();
                                    const Real NormalizationFactor = Real(pColorMap->GetLUTScaleFactor()) / Range;
                                    const Real* pChannelPixel = ChannelImage.GetReadOnlyBufferAt(X0, Y0);
                                    DiscreteRGBPixel* pDiscreteRGBPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                                    if (IsMasking())
                                    {
                                        const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pChannelPixel += Offset, pDiscreteRGBPixel += Offset, pActiveMaskPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X, ++pDiscreteRGBPixel, ++pChannelPixel)
                                                if (*pActiveMaskPixel++)
                                                {
                                                    *pDiscreteRGBPixel = pLUT[int(NormalizationFactor * (*pChannelPixel - MinimalChannelValue) + Real(0.5))];
                                                }
                                    }
                                    else
                                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pChannelPixel += Offset, pDiscreteRGBPixel += Offset)
                                            for (int X = X0 ; X <= X1 ; ++X)
                                            {
                                                *pDiscreteRGBPixel++ = pLUT[int(NormalizationFactor * (*pChannelPixel++ - MinimalChannelValue) + Real(0.5))];
                                            }
                                    return true;
                                }
                            }
                            break;
                            case eChannelSaliencyPhase:
                            {
                                if (IsMasking())
                                {
                                    MinimalChannelValue = g_RealPlusInfinity;
                                    MaximalChannelValue = g_RealMinusInfinity;
                                    const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset, pChannelPixel += Offset, pActiveMaskPixel += Offset)
                                        for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel, ++pChannelPixel)
                                            if (*pActiveMaskPixel++ && pSaliencyPixel->HasMagnitude())
                                            {
                                                const Real Value = pSaliencyPixel->GetPhase();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel = Value;
                                                ++TotalActivePixels;
                                            }
                                }
                                else
                                {
                                    //TotalActivePixels = m_OutActiveImageZone.GetArea();
                                    MinimalChannelValue = MaximalChannelValue = pSaliencyPixel->GetPhase();
                                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pSaliencyPixel += Offset, pChannelPixel += Offset)
                                        for (int X = X0 ; X <= X1 ; ++X, ++pSaliencyPixel)
                                            if (pSaliencyPixel->HasMagnitude())
                                            {
                                                const Real Value = pSaliencyPixel->GetPhase();
                                                if (Value > MaximalChannelValue)
                                                {
                                                    MaximalChannelValue = Value;
                                                }
                                                else if (Value < MinimalChannelValue)
                                                {
                                                    MinimalChannelValue = Value;
                                                }
                                                ChannelAccumulator += Value;
                                                ChannelSquareAccumulator += Value * Value;
                                                *pChannelPixel++ = Value;
                                                ++TotalActivePixels;
                                            }
                                }
                                ChannelMean = ChannelAccumulator / Real(TotalActivePixels);
                                ChannelStandardDeviation = std::sqrt((ChannelSquareAccumulator / Real(TotalActivePixels)) - (ChannelMean * ChannelMean));
                                const DiscreteRGBPixel* pLUT = pColorMap->GetLUT();
                                const Real NormalizationFactor = Real(pColorMap->GetLUTScaleFactor()) / g_Real2PI;
                                const Real* pChannelPixel = ChannelImage.GetReadOnlyBufferAt(X0, Y0);
                                DiscreteRGBPixel* pDiscreteRGBPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                                if (IsMasking())
                                {
                                    const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pChannelPixel += Offset, pDiscreteRGBPixel += Offset, pActiveMaskPixel += Offset)
                                        for (int X = X0 ; X <= X1 ; ++X, ++pDiscreteRGBPixel, ++pChannelPixel)
                                            if (*pActiveMaskPixel++)
                                            {
                                                *pDiscreteRGBPixel = pLUT[int(NormalizationFactor * *pChannelPixel + Real(0.5))];
                                            }
                                }
                                else
                                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pChannelPixel += Offset, pDiscreteRGBPixel += Offset)
                                        for (int X = X0 ; X <= X1 ; ++X)
                                        {
                                            *pDiscreteRGBPixel++ = pLUT[int(NormalizationFactor * *pChannelPixel++ + Real(0.5))];
                                        }
                                return true;
                            }
                            break;
                            default:
                                return false;
                        }
                    }
                    return false;
                }

                bool CGaborSaliencyExtraction::Execute(const int Trial)
                {
                    _RVL_TIME_LOGGER_BEGIN_(0, "CGaborSaliencyExtraction::Execute");
                    bool Result = false;
                    if (IsEnabled() && ((!m_pInActiveImageZone) || (m_pInActiveImageZone->IsValid() && m_pInImage->IsInside(m_pInActiveImageZone))))
                    {
                        if (IsMasking() && (!UpdateActiveMaskImage(false)))
                        {
                            _RVL_TIME_LOGGER_VERBOSE_END_(0, "CGaborSaliencyExtraction::Execute");
                            return false;
                        }
                        if (!UpdateActiveImageZone(false))
                        {
                            _RVL_TIME_LOGGER_VERBOSE_END_(0, "CGaborSaliencyExtraction::Execute");
                            return false;
                        }
                        if (IsMasking())
                        {
                            MaskedClear();
                            for (auto& m_KernelPair : m_KernelPairs)
                            {
                                MaskedGaborConvolution(m_KernelPair.first, m_KernelPair.second);
                            }
                            MaskedNormalize();
                        }
                        else
                        {
                            Clear();
                            for (auto& m_KernelPair : m_KernelPairs)
                            {
                                GaborConvolution(m_KernelPair.first, m_KernelPair.second);
                            }
                            Normalize();
                        }
                        Result = true;
                    }
                    _RVL_TIME_LOGGER_VERBOSE_END_(0, "CGaborSaliencyExtraction::Execute");
                    return Result;
                }

                void CGaborSaliencyExtraction::MaskedClear()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                    CSaliencyPixel* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y0);
                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutPixel += Offset, pActiveMaskPixel += Offset)
                        for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel)
                            if (*pActiveMaskPixel++)
                            {
                                pOutPixel->Clear();
                            }
                }

                void CGaborSaliencyExtraction::MaskedGaborConvolution(const Mathematics::_2D::Kernels::CConvolutionGaborKernel2D* pGaborKernelA, const Mathematics::_2D::Kernels::CConvolutionGaborKernel2D* pGaborKernelB)
                {
                    const int KernelRadius = pGaborKernelA->GetRadius();
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int XB = X0 - KernelRadius;
                    const int YB = Y0 - KernelRadius;
                    const int Offset = m_OutImage.GetWidth() - pGaborKernelA->GetDiameter();
                    const Real DXA = pGaborKernelA->GetOrientation().GetX();
                    const Real DYA = pGaborKernelA->GetOrientation().GetY();
                    const Real DXB = pGaborKernelB->GetOrientation().GetX();
                    const Real DYB = pGaborKernelB->GetOrientation().GetY();
                    for (int Y = Y0, YK = YB ; Y <= Y1 ; ++Y, ++YK)
                    {
                        const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y);
                        CSaliencyPixel* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y);
                        for (int X = X0, XK = XB ; X <= X1 ; ++X, ++XK, ++pOutPixel)
                            if (*pActiveMaskPixel++)
                            {
                                const Real* pInSupportPixel = m_pInImage->GetReadOnlyBufferAt(XK, YK);
                                const Real* pKernelA = pGaborKernelA->GetReadOnlyKernel();
                                const Real* pKernelB = pGaborKernelB->GetReadOnlyKernel();
                                Real AccumulatorA = Real(0), AccumulatorB = Real(0);
                                for (int i = -KernelRadius ; i <= KernelRadius ; ++i, pInSupportPixel += Offset)
                                    for (int j = -KernelRadius ; j <= KernelRadius ; ++j)
                                    {
                                        AccumulatorA += *pInSupportPixel * *pKernelA++;
                                        AccumulatorB += *pInSupportPixel++ * *pKernelB++;
                                    }
                                pOutPixel->AddOffset(DXA * AccumulatorA + DXB * AccumulatorB, DYA * AccumulatorA + DYB * AccumulatorB);
                            }
                    }
                }

                void CGaborSaliencyExtraction::MaskedNormalize()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                    CSaliencyPixel* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y0);
                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutPixel += Offset, pActiveMaskPixel += Offset)
                        for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel)
                            if (*pActiveMaskPixel++)
                            {
                                pOutPixel->Normalize();
                            }
                }

                void CGaborSaliencyExtraction::Clear()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    CSaliencyPixel* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y0);
                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutPixel += Offset)
                        for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel)
                        {
                            pOutPixel->Clear();
                        }
                }

                void CGaborSaliencyExtraction::GaborConvolution(const Mathematics::_2D::Kernels::CConvolutionGaborKernel2D* pGaborKernelA, const Mathematics::_2D::Kernels::CConvolutionGaborKernel2D* pGaborKernelB)
                {
                    const int KernelRadius = pGaborKernelA->GetRadius();
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int XB = X0 - KernelRadius;
                    const int YB = Y0 - KernelRadius;
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - pGaborKernelA->GetDiameter();
                    const Real DXA = pGaborKernelA->GetOrientation().GetX();
                    const Real DYA = pGaborKernelA->GetOrientation().GetY();
                    const Real DXB = pGaborKernelB->GetOrientation().GetX();
                    const Real DYB = pGaborKernelB->GetOrientation().GetY();
                    for (int Y = Y0, YK = YB ; Y <= Y1 ; ++Y, ++YK)
                    {
                        CSaliencyPixel* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y);
                        for (int X = X0, XK = XB ; X <= X1 ; ++X, ++XK, ++pOutPixel)
                        {
                            const Real* pInSupportPixel = m_pInImage->GetReadOnlyBufferAt(XK, YK);
                            const Real* pKernelA = pGaborKernelA->GetReadOnlyKernel();
                            const Real* pKernelB = pGaborKernelB->GetReadOnlyKernel();
                            Real AccumulatorA = Real(0), AccumulatorB = Real(0);
                            for (int i = -KernelRadius ; i <= KernelRadius ; ++i, pInSupportPixel += Offset)
                                for (int j = -KernelRadius ; j <= KernelRadius ; ++j)
                                {
                                    AccumulatorA += *pInSupportPixel * *pKernelA++;
                                    AccumulatorB += *pInSupportPixel++ * *pKernelB++;
                                }
                            pOutPixel->AddOffset(DXA * AccumulatorA + DXB * AccumulatorB, DYA * AccumulatorA + DYB * AccumulatorB);
                        }
                    }
                }

                void CGaborSaliencyExtraction::Normalize()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    CSaliencyPixel* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y0);
                    for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutPixel += Offset)
                        for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel)
                        {
                            pOutPixel->Normalize();
                        }
                }

                void CGaborSaliencyExtraction::CreateKernels(const int KernelPairs)
                {
                    DestroyKernels();
                    const Real Delta = g_RealHalfPI / Real(KernelPairs);
                    Mathematics::_2D::CVector2D OrientationA, OrientationB;
                    m_KernelPairs.reserve(KernelPairs);
                    for (int i = 0 ; i < KernelPairs ; ++i)
                    {
                        OrientationA.Set(std::cos(Delta * Real(i)), std::sin(Delta * Real(i)));
                        OrientationB.Set(std::cos(Delta * Real(i) + g_RealHalfPI), std::sin(Delta * Real(i) + g_RealHalfPI));
                        m_KernelPairs.push_back(std::pair<Mathematics::_2D::Kernels::CConvolutionGaborKernel2D*, Mathematics::_2D::Kernels::CConvolutionGaborKernel2D*>(new Mathematics::_2D::Kernels::CConvolutionGaborKernel2D(OrientationA, m_StandardDeviation, m_Lambda, m_Gamma1, m_Gamma2), new Mathematics::_2D::Kernels::CConvolutionGaborKernel2D(OrientationB, m_StandardDeviation, m_Lambda, m_Gamma1, m_Gamma2)));
                    }
                }

                void CGaborSaliencyExtraction::DestroyKernels()
                {
                    if (m_KernelPairs.size())
                    {
                        for (auto& m_KernelPair : m_KernelPairs)
                        {
                            delete m_KernelPair.first;
                            delete m_KernelPair.second;
                        }
                        m_KernelPairs.clear();
                    }
                }

                void CGaborSaliencyExtraction::InitializeSaliencyPixels()
                {
                    m_OutImage.Create(m_pInImage->GetSize(), true);
                    const int Width = m_pInImage->GetWidth();
                    const int Height = m_pInImage->GetHeight();
                    CSaliencyPixel* pSaliencyPixel = m_OutImage.GetWritableBuffer();
                    for (int Y = 0 ; Y < Height ; ++Y)
                        for (int X = 0 ; X < Width ; ++X, ++pSaliencyPixel)
                        {
                            pSaliencyPixel->SetLocation(X, Y);
                        }
                }

                void CGaborSaliencyExtraction::InitializeActiveImageZone()
                {
                    const int KernelRadius = std::max(int(std::ceil(m_StandardDeviation * Real(3))), 1);
                    if (m_pInActiveImageZone)
                    {
                        m_OutActiveImageZone.Set(*m_pInActiveImageZone, KernelRadius);
                    }
                    else
                    {
                        m_OutActiveImageZone.Set(m_pInImage->GetSize(), KernelRadius);
                    }
                }

                void CGaborSaliencyExtraction::InitializeActiveMaskImage()
                {
                    m_OutActiveMaskImage.Create(m_pInImage->GetSize(), false);
                    m_OutActiveMaskImage.SetSource(m_pInActiveMaskImage, false);
                }

                bool CGaborSaliencyExtraction::UpdateActiveImageZone(const bool Force)
                {
                    if (m_pInActiveImageZone && m_pInActiveImageZone->IsValid() && m_pInImage->IsInside(m_pInActiveImageZone))
                    {
                        const int KernelRadius = std::max(int(std::ceil(m_StandardDeviation * Real(3))), 1);
                        if (Force || (!m_OutActiveImageZone.IsPostSynchronized(*m_pInActiveImageZone, KernelRadius)))
                        {
                            return m_OutActiveImageZone.Set(*m_pInActiveImageZone, KernelRadius);
                        }
                        return true;
                    }
                    return m_OutActiveImageZone.Set(0, 0, m_pInImage->GetWidth() - 1, m_pInImage->GetHeight() - 1);
                }

                bool CGaborSaliencyExtraction::UpdateActiveMaskImage(const bool Force)
                {
                    if (m_pInActiveMaskImage && m_pInActiveMaskImage->IsValid() && m_pInImage->SizeEquals(m_pInActiveMaskImage->GetSize()))
                    {
                        if (Force || m_OutActiveMaskImage.SourceHasChanged(m_pInActiveMaskImage))
                        {
                            const int KernelRadius = std::max(int(std::ceil(m_StandardDeviation * Real(3))), 1);
                            return m_OutActiveMaskImage.UpdateErodeFromSource(KernelRadius);
                        }
                        return true;
                    }
                    return m_OutActiveMaskImage.Set(true);
                }
            }
        }
    }
}
