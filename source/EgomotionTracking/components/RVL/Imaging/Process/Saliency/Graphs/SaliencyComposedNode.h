/*
 * SaliencyComposedNode.h
 */

#pragma once

#include "../SaliencyNode.h"
#include "SaliencyGraph.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Saliency
            {
                namespace Graphs
                {
                    class CSaliencyComposedNode
                    {
                    public:

                        CSaliencyComposedNode(CSaliencyGraph* pSaliencyGraph, CSaliencyNode* pSaliencyNode, const int* pDeltas);
                        virtual ~CSaliencyComposedNode();

                        const CSaliencyGraph* GetSaliencyGraph() const;
                        const Containers::_2D::CContinuousBoundingBox2D& GetBoundingBox() const;
                        const Mathematics::_2D::CVector2D& GetGeometricCentroid() const;
                        const Mathematics::_2D::CVector2D& GetSaliencyCentroid() const;
                        const std::list<CSaliencyNode*>& GetNodes() const;

                    protected:

                        void AddNode(CSaliencyNode* pSaliencyNode);
                        void Expand(CSaliencyNode* pSaliencyNode, const int* pDeltas);

                        CSaliencyGraph* m_pSaliencyGraph;
                        Byte m_Type;
                        Containers::_2D::CContinuousBoundingBox2D m_BoundingBox;
                        Mathematics::_2D::CVector2D m_GeometricCentroid;
                        Mathematics::_2D::CVector2D m_SaliencyCentroid;
                        std::list<CSaliencyNode*> m_Nodes;
                    };
                }
            }
        }
    }
}

