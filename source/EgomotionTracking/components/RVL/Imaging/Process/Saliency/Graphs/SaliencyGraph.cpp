/*
 * SaliencyGraph.cpp
 */

#include "SaliencyGraph.h"
#include "SaliencyComposedNode.h"
#include "../SaliencyPixel.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Saliency
            {
                namespace Graphs
                {
                    CSaliencyGraph::CSaliencyGraph(CSaliencyNode* pSaliencyNode, const int* pDeltas) :
                        m_SaliencyAccumulator(Real(0)),
                        m_MaximalSaliency(pSaliencyNode->GetOptimizedMagnitude()),
                        m_MinimalSaliency(m_MaximalSaliency)
                    {
                        Expand(pSaliencyNode, pDeltas);
                    }

                    CSaliencyGraph::~CSaliencyGraph()
                    {
                        for (auto& m_ComposedNode : m_ComposedNodes)
                        {
                            delete m_ComposedNode;
                        }
                        m_ComposedNodes.clear();
                    }

                    Real CSaliencyGraph::GetMeanSaliency() const
                    {
                        return m_SaliencyAccumulator / Real(m_Nodes.size());
                    }

                    Real CSaliencyGraph::GetMaximalSaliency() const
                    {
                        return m_MaximalSaliency;
                    }

                    Real CSaliencyGraph::GetMinimalSaliency() const
                    {
                        return m_MinimalSaliency;
                    }

                    int CSaliencyGraph::GetTotalNodes() const
                    {
                        return m_Nodes.size();
                    }

                    const Containers::_2D::CContinuousBoundingBox2D& CSaliencyGraph::GetBoundingBox() const
                    {
                        return m_BoundingBox;
                    }

                    const Mathematics::_2D::CVector2D& CSaliencyGraph::GetGeometricCentroid() const
                    {
                        return m_GeometricCentroid;
                    }

                    const Mathematics::_2D::CVector2D& CSaliencyGraph::GetSaliencyCentroid() const
                    {
                        return m_SaliencyCentroid;
                    }

                    const std::list<CSaliencyNode*>& CSaliencyGraph::GetNodes() const
                    {
                        return m_Nodes;
                    }

                    void CSaliencyGraph::AddNode(CSaliencyNode* pSaliencyNode)
                    {
                        m_Nodes.push_back(pSaliencyNode);
                        m_BoundingBox.Extend(pSaliencyNode->GetSubpixelLocation());
                        m_GeometricCentroid += pSaliencyNode->GetSubpixelLocation();
                        const Real Saliency = pSaliencyNode->GetOptimizedMagnitude();
                        m_SaliencyCentroid.AddWeightedOffset(pSaliencyNode->GetSubpixelLocation(), Saliency);
                        m_SaliencyAccumulator += Saliency;
                        if (Saliency > m_MaximalSaliency)
                        {
                            m_MaximalSaliency = Saliency;
                        }
                        else if (Saliency < m_MinimalSaliency)
                        {
                            m_MinimalSaliency = Saliency;
                        }
                    }

                    void CSaliencyGraph::Expand(CSaliencyNode* pSaliencyNode, const int* pDeltas)
                    {
                        std::priority_queue<CSaliencyNode*, std::vector<CSaliencyNode*>, CSaliencyNode> LinkageExpansion;
                        LinkageExpansion.push(pSaliencyNode);
                        m_Nodes.push_back(pSaliencyNode);
                        while (LinkageExpansion.size())
                        {
                            CSaliencyNode* pExpandingNode = LinkageExpansion.top();
                            LinkageExpansion.pop();
                            for (int i = 0 ; i < 8 ; ++i)
                            {
                                CSaliencyNode* pLinkingNode = (pExpandingNode->GetWritablePixel() + pDeltas[i])->GetWritableNode();
                                if (pLinkingNode && (!pLinkingNode->HasParent()))
                                {
                                    pExpandingNode->AddChild(pLinkingNode);
                                    pLinkingNode->SetParent(pExpandingNode);
                                    LinkageExpansion.push(pLinkingNode);
                                    m_Nodes.push_back(pLinkingNode);
                                }
                            }
                            pExpandingNode->CheckRedundant();
                        }
                        pSaliencyNode->SetParent(nullptr);
                        m_SaliencyCentroid /= m_SaliencyAccumulator;
                        m_GeometricCentroid /= Real(m_Nodes.size());
                    }

                    void CSaliencyGraph::CharacterizeNodes(const int MaximalGraphDistance, const int* pDeltas, const Real LowerEccentricity, const Real MiddleEccentricity)
                    {
                        std::list<CSaliencyNode*> BroadCastingList;
                        const Real BandWidthNormalization = Real(1) / (Real(MaximalGraphDistance + 1) * g_RealSquareRoot2);
                        for (auto& m_Node : m_Nodes)
                            if (m_Node->m_Status)
                            {
                                CSaliencyNode* pBroadCastingNode = m_Node;
                                pBroadCastingNode->StartBroadcasting();
                                BroadCastingList.push_back(pBroadCastingNode);
                                while (BroadCastingList.size())
                                {
                                    CSaliencyNode* pTransmitterNode = BroadCastingList.front();
                                    BroadCastingList.pop_front();

                                    if (pTransmitterNode->m_BroadCastingGraphDistance < MaximalGraphDistance)
                                        for (int i = 0 ; i < 8 ; ++i)
                                        {
                                            CSaliencyNode* pListenerNode = pTransmitterNode->m_pPixel[pDeltas[i]].GetWritableNode();
                                            if (pListenerNode && pListenerNode->m_Status && (pListenerNode->m_pBroadCastingNode != pBroadCastingNode))
                                                if (pListenerNode->Broadcast(pBroadCastingNode, pTransmitterNode))
                                                {
                                                    if (pBroadCastingNode < pListenerNode)
                                                    {
                                                        const Real D = pListenerNode->m_BroadCastingPathDistance * BandWidthNormalization;
                                                        const Real W = Real(1) - D * D;
                                                        const Real DX = pListenerNode->m_SubpixelLocation.GetX() - pBroadCastingNode->m_SubpixelLocation.GetX();
                                                        const Real DY = pListenerNode->m_SubpixelLocation.GetY() - pBroadCastingNode->m_SubpixelLocation.GetY();
                                                        const Real WXX = W * DX * DX;
                                                        const Real WXY = W * DX * DY;
                                                        const Real WYY = W * DY * DY;
                                                        pBroadCastingNode->m_AWXX += WXX;
                                                        pBroadCastingNode->m_AWXY += WXY;
                                                        pBroadCastingNode->m_AWYY += WYY;
                                                        pListenerNode->m_AWXX += WXX;
                                                        pListenerNode->m_AWXY += WXY;
                                                        pListenerNode->m_AWYY += WYY;
                                                    }
                                                    BroadCastingList.push_back(pListenerNode);
                                                }
                                        }
                                }
                            }
                        for (auto& m_Node : m_Nodes)
                            if (m_Node->m_Status)
                            {
                                CSaliencyNode* pCharacterizingNode = m_Node;
                                const Real Alpha = pCharacterizingNode->m_AWXX - pCharacterizingNode->m_AWYY;
                                const Real Beta = std::sqrt(Alpha * Alpha + (Real(4) * pCharacterizingNode->m_AWXY * pCharacterizingNode->m_AWXY));
                                const Real Theta = pCharacterizingNode->m_AWXX + pCharacterizingNode->m_AWYY;
                                const Real LambdaA = std::abs(Theta + Beta);
                                const Real LambdaB = std::abs(Theta - Beta);
                                if (LambdaB > LambdaA)
                                {
                                    pCharacterizingNode->m_Lambda0 = LambdaA;
                                    pCharacterizingNode->m_Lambda1 = LambdaB;
                                }
                                else
                                {
                                    pCharacterizingNode->m_Lambda0 = LambdaB;
                                    pCharacterizingNode->m_Lambda1 = LambdaA;
                                }
                                pCharacterizingNode->m_Ratio = pCharacterizingNode->m_Lambda0 / pCharacterizingNode->m_Lambda1;
                                pCharacterizingNode->m_Eccentricity = std::atan(pCharacterizingNode->m_Ratio) * Real(1.273239545);
                                if (pCharacterizingNode->m_Eccentricity < LowerEccentricity)
                                {
                                    pCharacterizingNode->m_Status = 5;
                                }
                                else if (pCharacterizingNode->m_Eccentricity < MiddleEccentricity)
                                {
                                    pCharacterizingNode->m_Status = 6;
                                }
                                else
                                {
                                    pCharacterizingNode->m_Status = 7;
                                }
                            }
                    }

                    void CSaliencyGraph::SegmentComposedNodes(const int* pDeltas)
                    {
                        for (auto& m_Node : m_Nodes)
                            if (m_Node->m_Status && (!m_Node->HasComposedNode()))
                            {
                                m_ComposedNodes.push_back(new CSaliencyComposedNode(this, m_Node, pDeltas));
                            }
                    }
                }
            }
        }
    }
}
