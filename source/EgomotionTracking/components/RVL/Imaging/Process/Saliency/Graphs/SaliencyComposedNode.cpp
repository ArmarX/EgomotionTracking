/*
 * SaliencyComposedNode.cpp
 */

#include "SaliencyComposedNode.h"
#include "../SaliencyPixel.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Saliency
            {
                namespace Graphs
                {
                    CSaliencyComposedNode::CSaliencyComposedNode(CSaliencyGraph* pSaliencyGraph, CSaliencyNode* pSaliencyNode, const int* pDeltas) :
                        m_pSaliencyGraph(pSaliencyGraph),
                        m_Type(pSaliencyNode->GetStatus()),
                        m_BoundingBox(),
                        m_GeometricCentroid(),
                        m_SaliencyCentroid()
                    {
                        Expand(pSaliencyNode, pDeltas);
                    }

                    CSaliencyComposedNode::~CSaliencyComposedNode()
                        = default;

                    const CSaliencyGraph* CSaliencyComposedNode::GetSaliencyGraph() const
                    {
                        return m_pSaliencyGraph;
                    }

                    const Containers::_2D::CContinuousBoundingBox2D& CSaliencyComposedNode::GetBoundingBox() const
                    {
                        return m_BoundingBox;
                    }

                    const Mathematics::_2D::CVector2D& CSaliencyComposedNode::GetGeometricCentroid() const
                    {
                        return m_GeometricCentroid;
                    }

                    const Mathematics::_2D::CVector2D& CSaliencyComposedNode::GetSaliencyCentroid() const
                    {
                        return m_SaliencyCentroid;
                    }

                    const std::list<CSaliencyNode*>& CSaliencyComposedNode::GetNodes() const
                    {
                        return m_Nodes;
                    }

                    void CSaliencyComposedNode::AddNode(CSaliencyNode* pSaliencyNode)
                    {
                        m_Nodes.push_back(pSaliencyNode);
                        m_BoundingBox.Extend(pSaliencyNode->GetSubpixelLocation());
                        m_GeometricCentroid += pSaliencyNode->GetSubpixelLocation();
                        m_SaliencyCentroid.AddWeightedOffset(pSaliencyNode->GetSubpixelLocation(), pSaliencyNode->GetOptimizedMagnitude());
                    }

                    void CSaliencyComposedNode::Expand(CSaliencyNode* pSaliencyNode, const int* pDeltas)
                    {
                        std::list<CSaliencyNode*> LinkageExpansion;
                        LinkageExpansion.push_back(pSaliencyNode);
                        m_Nodes.push_back(pSaliencyNode);
                        Real SaliencyAccumulator = Real(0);
                        while (LinkageExpansion.size())
                        {
                            CSaliencyNode* pExpandingNode = LinkageExpansion.front();
                            LinkageExpansion.pop_front();
                            for (int i = 0 ; i < 8 ; ++i)
                            {
                                CSaliencyNode* pLinkingNode = (pExpandingNode->GetWritablePixel() + pDeltas[i])->GetWritableNode();
                                if (pLinkingNode && (pLinkingNode->GetStatus() == m_Type) && (!pLinkingNode->HasComposedNode()))
                                {
                                    pLinkingNode->SetComposedNode((CSaliencyComposedNode*) this);
                                    LinkageExpansion.push_back(pLinkingNode);
                                    m_Nodes.push_back(pLinkingNode);
                                }
                            }
                        }
                        pSaliencyNode->SetParent(nullptr);
                        m_SaliencyCentroid /= SaliencyAccumulator;
                        m_GeometricCentroid /= Real(m_Nodes.size());
                    }
                }
            }
        }
    }
}
