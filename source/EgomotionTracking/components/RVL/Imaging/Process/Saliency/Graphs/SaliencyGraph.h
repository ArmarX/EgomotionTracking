/*
 * SaliencyGraph.h
 */

#pragma once

#include "../SaliencyNode.h"
#include "../../../../Containers/_2D/ContinuousBoundingBox2D.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Saliency
            {
                namespace Graphs
                {
                    class CSaliencyGraph
                    {
                    public:

                        CSaliencyGraph(CSaliencyNode* pSaliencyNode, const int* pDeltas);
                        virtual ~CSaliencyGraph();

                        Real GetMeanSaliency() const;
                        Real GetMaximalSaliency() const;
                        Real GetMinimalSaliency() const;

                        int GetTotalNodes() const;

                        const Containers::_2D::CContinuousBoundingBox2D& GetBoundingBox() const;
                        const Mathematics::_2D::CVector2D& GetGeometricCentroid() const;
                        const Mathematics::_2D::CVector2D& GetSaliencyCentroid() const;
                        const std::list<CSaliencyNode*>& GetNodes() const;

                        void CharacterizeNodes(const int MaximalGraphDistance, const int* pDeltas, const Real LowerEccentricity, const Real MiddleEccentricity);
                        void SegmentComposedNodes(const int* pDeltas);

                    protected:

                        void AddNode(CSaliencyNode* pSaliencyNode);
                        void Expand(CSaliencyNode* pSaliencyNode, const int* pDeltas);

                        Real m_SaliencyAccumulator;
                        Real m_MaximalSaliency;
                        Real m_MinimalSaliency;
                        Containers::_2D::CContinuousBoundingBox2D m_BoundingBox;
                        Mathematics::_2D::CVector2D m_GeometricCentroid;
                        Mathematics::_2D::CVector2D m_SaliencyCentroid;
                        std::list<CSaliencyNode*> m_Nodes;
                        std::list<CSaliencyComposedNode*> m_ComposedNodes;
                    };
                }
            }
        }
    }
}

