/*
 * ContinuousRGBDesaturation.cpp
 */

#include "ContinuousRGBDesaturation.h"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Desaturation
            {
                CContinuousRGBDesaturation::CContinuousRGBDesaturation(const TImage<ContinuousRGBPixel>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone) :
                    TImageProcess<ContinuousRGBPixel, Real>(pInImage, pInActiveMaskImage, pInActiveImageZone),
                    m_DesaturationMethod(eLumaCCIR_601)
                {
                    if (IsEnabled())
                    {
                        m_OutImage.Create(m_pInImage->GetSize(), true);
                        InitializeActiveImageZone();
                        InitializeActiveMaskImage();
                    }
                }

                CContinuousRGBDesaturation::~CContinuousRGBDesaturation()
                    = default;

                bool CContinuousRGBDesaturation::SetDesaturationMethod(const DesaturationMethod Method)
                {
                    if ((Method == eLumaCCIR_601) || (Method == eLumaCCIR_709) || (Method == LumaSMPTE_240M) || (Method == eBrightness) || (Method == eLightness))
                    {
                        m_DesaturationMethod = Method;
                        return true;
                    }
                    return false;
                }

                bool CContinuousRGBDesaturation::Execute(const int Trial)
                {
                    _RVL_TIME_LOGGER_BEGIN_(0, "CContinuousRGBDesaturation::Execute");
                    bool Result = false;
                    if (IsEnabled() && ((!m_pInActiveImageZone) || (m_pInActiveImageZone->IsValid() && m_pInImage->IsInside(m_pInActiveImageZone))))
                    {
                        if (IsMasking() && (!UpdateActiveMaskImage(false)))
                        {
                            _RVL_TIME_LOGGER_VERBOSE_END_(0, "CContinuousRGBDesaturation::Execute");
                            return false;
                        }
                        if (!UpdateActiveImageZone(false))
                        {
                            _RVL_TIME_LOGGER_VERBOSE_END_(0, "CContinuousRGBDesaturation::Execute");
                            return false;
                        }
                        switch (m_DesaturationMethod)
                        {
                            case eLumaCCIR_601:
                                LumaDesaturation(Real(0.299), Real(0.587), Real(0.114));
                                break;
                            case eLumaCCIR_709:
                                LumaDesaturation(Real(0.2126), Real(0.7152), Real(0.0722));
                                break;
                            case LumaSMPTE_240M:
                                LumaDesaturation(Real(0.212), Real(0.701), Real(0.087));
                                break;
                            case eBrightness:
                                BrightnessDesaturation();
                                break;
                            case eLightness:
                                LightnessDesaturation();
                                break;
                        }
                        Result = true;
                    }
                    _RVL_TIME_LOGGER_VERBOSE_END_(0, "CContinuousRGBDesaturation::Execute");
                    return Result;
                }

                bool CContinuousRGBDesaturation::Display(TImage<DiscreteRGBPixel>* pDisplayImage)
                {
                    if (pDisplayImage && pDisplayImage->IsValid() && pDisplayImage->HasBufferOwnership() && pDisplayImage->SizeEquals(m_OutImage.GetSize()))
                    {
                        pDisplayImage->Clear();
                        const int X0 = m_OutActiveImageZone.GetX0();
                        const int Y0 = m_OutActiveImageZone.GetY0();
                        const int X1 = m_OutActiveImageZone.GetX1();
                        const int Y1 = m_OutActiveImageZone.GetY1();
                        const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                        DiscreteRGBPixel* pDisplayPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                        const Real* pOutPixel = m_OutImage.GetReadOnlyBufferAt(X0, Y0);
                        if (IsMasking())
                        {
                            const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                            if (Offset)
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pActiveMaskPixel += Offset, pOutPixel += Offset, pDisplayPixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel, ++pDisplayPixel)
                                        if (*pActiveMaskPixel++)
                                        {
                                            *pDisplayPixel = *pOutPixel;
                                        }
                            }
                            else
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel, ++pDisplayPixel)
                                        if (*pActiveMaskPixel++)
                                        {
                                            *pDisplayPixel = *pOutPixel;
                                        }
                            }
                        }
                        else
                        {
                            if (Offset)
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y, pOutPixel += Offset, pDisplayPixel += Offset)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel, ++pDisplayPixel)
                                    {
                                        *pDisplayPixel = *pOutPixel;
                                    }
                            }
                            else
                            {
                                for (int Y = Y0 ; Y <= Y1 ; ++Y)
                                    for (int X = X0 ; X <= X1 ; ++X, ++pOutPixel, ++pDisplayPixel)
                                    {
                                        *pDisplayPixel = *pOutPixel;
                                    }
                            }
                        }
                        return true;
                    }
                    return false;
                }

                void CContinuousRGBDesaturation::LumaDesaturation(const Real WR, const Real WG, const Real WB)
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    const ContinuousRGBPixel* pInPixel = m_pInImage->GetReadOnlyBufferAt(X0, Y0);
                    Real* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y0);
                    if (IsMasking())
                    {
                        const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset, pActiveMaskPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pInPixel, ++pOutPixel)
                                if (*pActiveMaskPixel++)
                                {
                                    *pOutPixel = pInPixel->m_Elements.m_R * WR + pInPixel->m_Elements.m_G * WG + pInPixel->m_Elements.m_B * WB;
                                }
                    }
                    else
                    {
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pInPixel)
                            {
                                *pOutPixel++ = pInPixel->m_Elements.m_R * WR + pInPixel->m_Elements.m_G * WG + pInPixel->m_Elements.m_B * WB;
                            }
                    }
                }

                void CContinuousRGBDesaturation::BrightnessDesaturation()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    const ContinuousRGBPixel* pInPixel = m_pInImage->GetReadOnlyBufferAt(X0, Y0);
                    Real* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y0);
                    const Real Factor = Real(0.3333333333333333333333);
                    if (IsMasking())
                    {
                        const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset, pActiveMaskPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pInPixel, ++pOutPixel)
                                if (*pActiveMaskPixel++)
                                {
                                    *pOutPixel = (pInPixel->m_Elements.m_R + pInPixel->m_Elements.m_G + pInPixel->m_Elements.m_B) * Factor;
                                }
                    }
                    else
                    {
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pInPixel)
                            {
                                *pOutPixel++ = (pInPixel->m_Elements.m_R + pInPixel->m_Elements.m_G + pInPixel->m_Elements.m_B) * Factor;
                            }
                    }
                }

                void CContinuousRGBDesaturation::LightnessDesaturation()
                {
                    const int X0 = m_OutActiveImageZone.GetX0();
                    const int Y0 = m_OutActiveImageZone.GetY0();
                    const int X1 = m_OutActiveImageZone.GetX1();
                    const int Y1 = m_OutActiveImageZone.GetY1();
                    const int Offset = m_OutImage.GetWidth() - m_OutActiveImageZone.GetWidth();
                    const ContinuousRGBPixel* pInPixel = m_pInImage->GetReadOnlyBufferAt(X0, Y0);
                    Real* pOutPixel = m_OutImage.GetWritableBufferAt(X0, Y0);
                    const Real Factor = Real(0.5);
                    if (IsMasking())
                    {
                        const bool* pActiveMaskPixel = m_OutActiveMaskImage.GetReadOnlyBufferAt(X0, Y0);
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset, pActiveMaskPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pInPixel, ++pOutPixel)
                                if (*pActiveMaskPixel++)
                                {
                                    *pOutPixel = (std::max(std::max(pInPixel->m_Elements.m_R, pInPixel->m_Elements.m_G), pInPixel->m_Elements.m_B) + std::min(std::min(pInPixel->m_Elements.m_R, pInPixel->m_Elements.m_G), pInPixel->m_Elements.m_B)) * Factor;
                                }
                    }
                    else
                    {
                        for (int Y = Y0 ; Y <= Y1 ; ++Y, pInPixel += Offset, pOutPixel += Offset)
                            for (int X = X0 ; X <= X1 ; ++X, ++pInPixel)
                            {
                                *pOutPixel++ = (std::max(std::max(pInPixel->m_Elements.m_R, pInPixel->m_Elements.m_G), pInPixel->m_Elements.m_B) + std::min(std::min(pInPixel->m_Elements.m_R, pInPixel->m_Elements.m_G), pInPixel->m_Elements.m_B)) * Factor;
                            }
                    }
                }
            }
        }
    }
}
