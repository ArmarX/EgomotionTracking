/*
 * ContinuousRGBDesaturation.h
 */

#pragma once

#include "../../PixelTypes.h"
#include "../TImageProcess.hpp"

namespace RVL
{
    namespace Imaging
    {
        namespace Process
        {
            namespace Desaturation
            {
                class CContinuousRGBDesaturation : public TImageProcess<ContinuousRGBPixel, Real>
                {
                public:

                    enum DesaturationMethod
                    {
                        eLumaCCIR_601, eLumaCCIR_709, LumaSMPTE_240M, eBrightness, eLightness
                    };

                    CContinuousRGBDesaturation(const TImage<ContinuousRGBPixel>* pInImage, const CActiveMaskImage* pInActiveMaskImage, const CActiveImageZone* pInActiveImageZone);
                    ~CContinuousRGBDesaturation() override;

                    bool SetDesaturationMethod(const DesaturationMethod Method);

                    bool Execute(const int Trial) override;

                    bool Display(TImage<DiscreteRGBPixel>* pDisplayImage);

                protected:

                    void LumaDesaturation(const Real WR, const Real WG, const Real WB);
                    void BrightnessDesaturation();
                    void LightnessDesaturation();

                    DesaturationMethod m_DesaturationMethod;
                };
            }
        }
    }
}

