/*
 * ImageExporter.cpp
 */

#include "ImageExporter.h"
#include <Image/ByteImage.h>

namespace RVL
{
    namespace Imaging
    {
        bool CImageExporter::Export(const TImage<bool>* pBoolImage, const std::string& FileName)
        {
            if (pBoolImage && pBoolImage->IsValid() && pBoolImage->HasBufferOwnership() && FileName.length())
            {
                CByteImage Exporter(pBoolImage->GetWidth(), pBoolImage->GetHeight(), CByteImage::eGrayScale);
                const bool* pInPixel = pBoolImage->GetReadOnlyBuffer();
                const bool* pInPixelEnd = pBoolImage->GetBufferEnd();
                Byte* pOutPixel = Exporter.pixels;
                while (pInPixel < pInPixelEnd)
                {
                    *pOutPixel++ = *pInPixel++ ? 255 : 0;
                }
                return Exporter.SaveToFile(FileName.c_str());
            }
            return false;
        }

        bool CImageExporter::Export(const TImage<Byte>* pByteImage, const std::string& FileName)
        {
            if (pByteImage && pByteImage->IsValid() && pByteImage->HasBufferOwnership() && FileName.length())
            {
                CByteImage Exporter(pByteImage->GetWidth(), pByteImage->GetHeight(), CByteImage::eGrayScale, true);
                Exporter.pixels = (Byte*) pByteImage->GetReadOnlyBuffer();
                return Exporter.SaveToFile(FileName.c_str());
            }
            return false;
        }

        bool CImageExporter::Export(const TImage<DiscreteRGBPixel>* pDiscreteRGBImage, const std::string& FileName)
        {
            if (pDiscreteRGBImage && pDiscreteRGBImage->IsValid() && pDiscreteRGBImage->HasBufferOwnership() && FileName.length())
            {
                CByteImage Exporter(pDiscreteRGBImage->GetWidth(), pDiscreteRGBImage->GetHeight(), CByteImage::eRGB24, true);
                Exporter.pixels = (Byte*) pDiscreteRGBImage->GetReadOnlyBuffer();
                return Exporter.SaveToFile(FileName.c_str());
            }
            return false;
        }

        CImageExporter::CImageExporter()
            = default;

        CImageExporter::~CImageExporter()
            = default;
    }
}
