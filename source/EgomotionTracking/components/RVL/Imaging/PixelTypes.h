/*
 * PixelTypes.h
 */

#pragma once

#include "../Common/DataTypes.h"

namespace RVL
{
    namespace Imaging
    {
        struct DebugContinuousIntensityPixel
        {
            DebugContinuousIntensityPixel()
            {
                m_Intensity = Real(0);
                m_X = 0;
                m_Y = 0;
                m_Channels = 0;
            }

            void operator +=(const Real X)
            {
                m_Intensity += X;
            }

            Real m_Intensity;
            int m_X;
            int m_Y;
            int m_Channels;
        };

        struct DebugContinuousTristimulusPixel
        {
            DebugContinuousTristimulusPixel()
            {
                m_Elements.m_R = Real(0);
                m_Elements.m_G = Real(0);
                m_Elements.m_B = Real(0);
                m_X = 0;
                m_Y = 0;
            }

            inline void Set(const Real R, const Real G, const Real B)
            {
                m_Elements.m_R = R;
                m_Elements.m_G = G;
                m_Elements.m_B = B;
            }

            void InverseScale(const DebugContinuousTristimulusPixel Scale)
            {
                m_Elements.m_R /= Scale.m_Elements.m_R;
                m_Elements.m_G /= Scale.m_Elements.m_G;
                m_Elements.m_B /= Scale.m_Elements.m_B;
            }

            void AddWeighted(const DebugContinuousTristimulusPixel* pContinuousRGBPixel, const Real Weight)
            {
                m_Elements.m_R += pContinuousRGBPixel->m_Elements.m_R * Weight;
                m_Elements.m_G += pContinuousRGBPixel->m_Elements.m_G * Weight;
                m_Elements.m_B += pContinuousRGBPixel->m_Elements.m_B * Weight;
            }

            struct Elements
            {
                Real m_R;
                Real m_G;
                Real m_B;
            };
            Elements m_Elements;

            int m_X;
            int m_Y;
        };

        union ContinuousRGBAPixel
        {
            struct Elements
            {
                Real m_R;
                Real m_G;
                Real m_B;
                Real m_A;
            };

            Elements m_Elements;
            Real m_Channels[4];
        };

        union DiscreteRGBPixel;
        union ContinuousRGBPixel
        {
            ContinuousRGBPixel()
            {
                m_Elements.m_R = Real(0);
                m_Elements.m_G = Real(0);
                m_Elements.m_B = Real(0);
            }

            ContinuousRGBPixel(const Real R, const Real G, const Real B)
            {
                m_Elements.m_R = R;
                m_Elements.m_G = G;
                m_Elements.m_B = B;
            }

            void AddWeighted(const ContinuousRGBPixel* pContinuousRGBPixel, const Real Weight)
            {
                m_Elements.m_R += pContinuousRGBPixel->m_Elements.m_R * Weight;
                m_Elements.m_G += pContinuousRGBPixel->m_Elements.m_G * Weight;
                m_Elements.m_B += pContinuousRGBPixel->m_Elements.m_B * Weight;
            }

            void SetInverseWeighted(const ContinuousRGBPixel& WeightedContinuousRGBPixel, const Real Weight)
            {
                m_Elements.m_R = WeightedContinuousRGBPixel.m_Elements.m_R / Weight;
                m_Elements.m_G = WeightedContinuousRGBPixel.m_Elements.m_G / Weight;
                m_Elements.m_B = WeightedContinuousRGBPixel.m_Elements.m_B / Weight;
            }

            void InverseScale(const Real Scale)
            {
                m_Elements.m_R /= Scale;
                m_Elements.m_G /= Scale;
                m_Elements.m_B /= Scale;
            }

            void InverseScale(const ContinuousRGBPixel Scale)
            {
                m_Elements.m_R /= Scale.m_Elements.m_R;
                m_Elements.m_G /= Scale.m_Elements.m_G;
                m_Elements.m_B /= Scale.m_Elements.m_B;
            }

            inline void Set(const Real R, const Real G, const Real B)
            {
                m_Elements.m_R = R;
                m_Elements.m_G = G;
                m_Elements.m_B = B;
            }

            Real GetSquareEuclideanDistance(const ContinuousRGBPixel& Pixel) const
            {
                const Real DR = m_Elements.m_R - Pixel.m_Elements.m_R;
                const Real DG = m_Elements.m_G - Pixel.m_Elements.m_G;
                const Real DB = m_Elements.m_B - Pixel.m_Elements.m_B;
                return (DR * DR + DG * DG + DB * DB);
            }

            void operator=(const DiscreteRGBPixel& Pixel);

            struct Elements
            {
                Real m_R;
                Real m_G;
                Real m_B;
            };

            Elements m_Elements;
            Real m_Channels[3];
        };

        union ContinuousHSVPixel
        {
            struct Elements
            {
                Real m_H;
                Real m_S;
                Real m_V;
            };

            Elements m_Elements;
            Real m_Channels[3];
        };

        const Real g_RGBToXYZ_TransformationMatrix[/*3][3*/9] = { /*{*/0.4124, 0.3576, 0.1805 /*}*/, /*{*/0.2126, 0.7152, 0.0722 /*}*/, /*{*/0.0193, 0.1192, 0.9505 /*}*/};
        const Real g_RGBToLAB_D50_TransformationMatrix[/*3][3*/9] = { /*{*/0.0042770676212, 0.0037087278888, 0.0018719949215 /*}*/, /*{*/0.002126, 0.007152, 0.000722 /*}*/, /*{*/0.0002338861043, 0.0014445193592, 0.0115185876755 /*}*/};
        const Real g_RGBToLAB_D55_TransformationMatrix[/*3][3*/9] = { /*{*/0.010451538, 0.0037374699888, 0.001886502609 /*}*/, /*{*/0.002126, 0.007152, 0.000722 /*}*/, /*{*/0.0002094454528, 0.0012935698432, 0.010314917248 /*}*/};
        const Real g_RGBToLAB_D65_TransformationMatrix[/*3][3*/9] = { /*{*/0.0043390929936, 0.0037625112864, 0.001899142302 /*}*/, /*{*/0.002126, 0.007152, 0.000722 /*}*/, /*{*/0.000177243094, 0.001094682736, 0.00872899279 /*}*/};
        const Real g_RGBToLAB_D75_TransformationMatrix[/*3][3*/9] = { /*{*/0.0042770676212, 0.0037653123672, 0.0019005561585 /*}*/, /*{*/0.002126, 0.007152, 0.000722 /*}*/, /*{*/0.0001573719298, 0.0009719551312, 0.007750363693 /*}*/};

        union ContinuousTristimulusPixel
        {
            ContinuousTristimulusPixel()
            {
                m_Elements.m_A = Real(0);
                m_Elements.m_B = Real(0);
                m_Elements.m_C = Real(0);
            }

            void LoadHSL(const ContinuousRGBPixel* pContinuousRGBPixel)
            {
                const Real NR = pContinuousRGBPixel->m_Elements.m_R * Real(0.003921568627450980392157);
                const Real NG = pContinuousRGBPixel->m_Elements.m_G * Real(0.003921568627450980392157);
                const Real NB = pContinuousRGBPixel->m_Elements.m_B * Real(0.003921568627450980392157);
                const Real M = std::max(std::max(NR, NG), NB);
                const Real m = std::min(std::min(NR, NG), NB);
                const Real C = M - m;
                m_Elements.m_C = (M + m) * Real(0.5);
                if (C > g_RealPlusEpsilon)
                {
                    if (NR == M)
                    {
                        m_Elements.m_A = (NG - NB) / C;
                        m_Elements.m_A = m_Elements.m_A - int(std::round(m_Elements.m_A * Real(0.1666666666666666666667))) * 6;
                    }
                    else if (NG == M)
                    {
                        m_Elements.m_A = Real(2) + (NB - NR) / C;
                    }
                    else
                    {
                        m_Elements.m_A = Real(4) + (NR - NG) / C;
                    }
                    m_Elements.m_A *= g_RealThirdPI;
                    m_Elements.m_B = C / (Real(1) - std::abs(Real(2) * m_Elements.m_C - Real(1)));
                }
                else
                {
                    m_Elements.m_A = Real(-1);
                }
            }

            void LoadHSI(const ContinuousRGBPixel* pContinuousRGBPixel)
            {
                const Real NR = pContinuousRGBPixel->m_Elements.m_R * Real(0.003921568627450980392157);
                const Real NG = pContinuousRGBPixel->m_Elements.m_G * Real(0.003921568627450980392157);
                const Real NB = pContinuousRGBPixel->m_Elements.m_B * Real(0.003921568627450980392157);
                const Real M = std::max(std::max(NR, NG), NB);
                const Real m = std::min(std::min(NR, NG), NB);
                const Real C = M - m;
                m_Elements.m_C = (NR + NG + NB) * (Real(0.333333333333333333333333333));
                if (C > g_RealPlusEpsilon)
                {
                    if (NR == M)
                    {
                        m_Elements.m_A = (NG - NB) / C;
                        m_Elements.m_A = m_Elements.m_A - int(std::round(m_Elements.m_A * Real(0.1666666666666666666667))) * 6;
                    }
                    else if (NG == M)
                    {
                        m_Elements.m_A = Real(2) + (NB - NR) / C;
                    }
                    else
                    {
                        m_Elements.m_A = Real(4) + (NR - NG) / C;
                    }
                    m_Elements.m_A *= g_RealThirdPI;
                    m_Elements.m_B = C / (Real(1) - std::abs(Real(2) * m_Elements.m_C - Real(1)));
                }
                else
                {
                    m_Elements.m_A = Real(-1);
                }
            }

            void LoadHSV(const ContinuousRGBPixel* pContinuousRGBPixel)
            {
                const Real NR = pContinuousRGBPixel->m_Elements.m_R * Real(0.003921568627450980392157);
                const Real NG = pContinuousRGBPixel->m_Elements.m_G * Real(0.003921568627450980392157);
                const Real NB = pContinuousRGBPixel->m_Elements.m_B * Real(0.003921568627450980392157);
                const Real M = std::max(std::max(NR, NG), NB);
                const Real m = std::min(std::min(NR, NG), NB);
                const Real C = M - m;
                m_Elements.m_C = M;
                if (C > g_RealPlusEpsilon)
                {
                    if (NR == M)
                    {
                        m_Elements.m_A = (NG - NB) / C;
                        m_Elements.m_A = m_Elements.m_A - int(std::round(m_Elements.m_A * Real(0.1666666666666666666667))) * Real(6);
                    }
                    else if (NG == M)
                    {
                        m_Elements.m_A = Real(2) + (NB - NR) / C;
                    }
                    else
                    {
                        m_Elements.m_A = Real(4) + (NR - NG) / C;
                    }
                    m_Elements.m_A *= g_RealThirdPI;
                    m_Elements.m_B = C / (Real(1) - std::abs(Real(2) * m_Elements.m_C - Real(1)));
                }
                else
                {
                    m_Elements.m_A = Real(-1);
                }
            }

            void LoadXYZ(const ContinuousRGBPixel* pContinuousRGBPixel)
            {
                Real Intermediate[3];
                for (int i = 0 ; i < 3 ; ++i)
                {
                    const Real NC = pContinuousRGBPixel->m_Channels[i] * Real(0.003921568627450980392157);
                    Intermediate[i] = (NC <= Real(0.04045) ? NC * Real(0.077399381) : std::pow((NC + Real(0.055)) * Real(0.947867299), Real(2.4))) * Real(100);
                }
                const Real* p_RGBToXYZ_TransformationMatrix = g_RGBToXYZ_TransformationMatrix;
                for (int i = 0 ; i < 3 ; ++i)
                {
                    Real Accumulator = Real(0);
                    for (int j = 0 ; j < 3 ; ++j)
                    {
                        Accumulator += *p_RGBToXYZ_TransformationMatrix++ * Intermediate[j];
                    }
                    m_Channels[i] = Accumulator;
                }
            }

            void LoadLAB_D50(const ContinuousRGBPixel* pContinuousRGBPixel)
            {
                Real Intermediate[3];
                for (int i = 0 ; i < 3 ; ++i)
                {
                    const Real NC = pContinuousRGBPixel->m_Channels[i] * Real(0.003921568627450980392157);
                    Intermediate[i] = (NC <= Real(0.04045) ? NC * Real(0.077399381) : std::pow((NC + Real(0.055)) * Real(0.947867299), Real(2.4))) * Real(100);
                }
                Real XYZ[3];
                const Real* pTransformationMatrixLabD50 = g_RGBToLAB_D50_TransformationMatrix;
                for (int i = 0 ; i < 3 ; ++i)
                {
                    Real Accumulator = Real(0);
                    for (int j = 0 ; j < 3 ; ++j)
                    {
                        Accumulator += *pTransformationMatrixLabD50++ * Intermediate[j];
                    }
                    XYZ[i] = (Accumulator > Real(0.008856)) ? std::pow(Accumulator, (Real(0.333333333333333333333333333))) : Accumulator * Real(7.787) + Real(0.137931034);
                }
                m_Elements.m_A = XYZ[1] * Real(116) - Real(16);
                m_Elements.m_B = (XYZ[0] - XYZ[1]) * Real(500);
                m_Elements.m_C = (XYZ[1] - XYZ[2]) * Real(200);
            }

            void LoadLAB_D55(const ContinuousRGBPixel* pContinuousRGBPixel)
            {
                Real Intermediate[3];
                for (int i = 0 ; i < 3 ; ++i)
                {
                    const Real NC = pContinuousRGBPixel->m_Channels[i] * Real(0.003921568627450980392157);
                    Intermediate[i] = (NC <= Real(0.04045) ? NC * Real(0.077399381) : std::pow((NC + Real(0.055)) * Real(0.947867299), Real(2.4))) * Real(100);
                }
                Real XYZ[3];
                const Real* pRGBToLAB_D55_TransformationMatrix = g_RGBToLAB_D55_TransformationMatrix;
                for (int i = 0 ; i < 3 ; ++i)
                {
                    Real Accumulator = Real(0);
                    for (int j = 0 ; j < 3 ; ++j)
                    {
                        Accumulator += *pRGBToLAB_D55_TransformationMatrix++ * Intermediate[j];
                    }
                    XYZ[i] = (Accumulator > Real(0.008856)) ? std::pow(Accumulator, (Real(0.333333333333333333333333333))) : Accumulator * Real(7.787) + Real(0.137931034);
                }
                m_Elements.m_A = XYZ[1] * Real(116) - Real(16);
                m_Elements.m_B = (XYZ[0] - XYZ[1]) * Real(500);
                m_Elements.m_C = (XYZ[1] - XYZ[2]) * Real(200);
            }

            void LoadLAB_D65(const ContinuousRGBPixel* pContinuousRGBPixel)
            {
                Real Intermediate[3];
                for (int i = 0 ; i < 3 ; ++i)
                {
                    const Real NC = pContinuousRGBPixel->m_Channels[i] * Real(0.003921568627450980392157);
                    Intermediate[i] = (NC <= Real(0.04045) ? NC * Real(0.077399381) : std::pow((NC + Real(0.055)) * Real(0.947867299), Real(2.4))) * Real(100);
                }
                Real XYZ[3];
                const Real* pRGBToLAB_D65_TransformationMatrix = g_RGBToLAB_D65_TransformationMatrix;
                for (int i = 0 ; i < 3 ; ++i)
                {
                    Real Accumulator = Real(0);
                    for (int j = 0 ; j < 3 ; ++j)
                    {
                        Accumulator += *pRGBToLAB_D65_TransformationMatrix++ * Intermediate[j];
                    }
                    XYZ[i] = (Accumulator > Real(0.008856)) ? std::pow(Accumulator, (Real(0.333333333333333333333333333))) : Accumulator * Real(7.787) + Real(0.137931034);
                }
                m_Elements.m_A = XYZ[1] * Real(116) - Real(16);
                m_Elements.m_B = (XYZ[0] - XYZ[1]) * Real(500);
                m_Elements.m_C = (XYZ[1] - XYZ[2]) * Real(200);
            }

            void LoadLAB_D75(const ContinuousRGBPixel* pContinuousRGBPixel)
            {
                Real Intermediate[3];
                for (int i = 0 ; i < 3 ; ++i)
                {
                    const Real NC = pContinuousRGBPixel->m_Channels[i] * Real(0.003921568627450980392157);
                    Intermediate[i] = (NC <= Real(0.04045) ? NC * Real(0.077399381) : std::pow((NC + Real(0.055)) * Real(0.947867299), Real(2.4))) * Real(100);
                }
                Real XYZ[3];
                const Real* pRGBToLAB_D75_TransformationMatrix = g_RGBToLAB_D75_TransformationMatrix;
                for (int i = 0 ; i < 3 ; ++i)
                {
                    Real Accumulator = Real(0);
                    for (int j = 0 ; j < 3 ; ++j)
                    {
                        Accumulator += *pRGBToLAB_D75_TransformationMatrix++ * Intermediate[j];
                    }
                    XYZ[i] = (Accumulator > Real(0.008856)) ? std::pow(Accumulator, (Real(0.333333333333333333333333333))) : Accumulator * Real(7.787) + Real(0.137931034);
                }
                m_Elements.m_A = XYZ[1] * Real(116) - Real(16);
                m_Elements.m_B = (XYZ[0] - XYZ[1]) * Real(500);
                m_Elements.m_C = (XYZ[1] - XYZ[2]) * Real(200);
            }

            struct Elements
            {
                Real m_A;
                Real m_B;
                Real m_C;
            };
            Elements m_Elements;
            Real m_Channels[3];
        };

        union DiscreteRGBAPixel
        {
            DiscreteRGBAPixel()
            {
                m_Elements.m_R = 0;
                m_Elements.m_G = 0;
                m_Elements.m_B = 0;
                m_Elements.m_A = 0;
            }

            DiscreteRGBAPixel(const Byte R, const Byte G, const Byte B, const Byte A)
            {
                m_Elements.m_R = R;
                m_Elements.m_G = G;
                m_Elements.m_B = B;
                m_Elements.m_A = A;
            }

            void Set(const Byte R, const Byte G, const Byte B, const Byte A)
            {
                m_Elements.m_R = R;
                m_Elements.m_G = G;
                m_Elements.m_B = B;
                m_Elements.m_B = A;
            }

            Real GetNormalizedRed() const
            {
                return Real(m_Elements.m_R) * Real(0.003921568627450980392157);
            }

            Real GetNormalizedGreen() const
            {
                return Real(m_Elements.m_G) * Real(0.003921568627450980392157);
            }

            Real GetNormalizedBlue() const
            {
                return Real(m_Elements.m_B) * Real(0.003921568627450980392157);
            }

            Real GetNormalizedAlpha() const
            {
                return Real(m_Elements.m_A) * Real(0.003921568627450980392157);
            }

            struct Elements
            {
                Byte m_R;
                Byte m_G;
                Byte m_B;
                Byte m_A;
            };

            Elements m_Elements;
            Byte m_Channels[4];
        };

        union DiscreteRGBPixel
        {
            DiscreteRGBPixel()
            {
                m_Elements.m_R = 0;
                m_Elements.m_G = 0;
                m_Elements.m_B = 0;
            }

            DiscreteRGBPixel(const Byte R, const Byte G, const Byte B)
            {
                m_Elements.m_R = R;
                m_Elements.m_G = G;
                m_Elements.m_B = B;
            }

            inline void Set(const Byte I)
            {
                m_Elements.m_R = I;
                m_Elements.m_G = I;
                m_Elements.m_B = I;
            }

            inline void Set(const Byte R, const Byte G, const Byte B)
            {
                m_Elements.m_R = R;
                m_Elements.m_G = G;
                m_Elements.m_B = B;
            }

            void operator=(const ContinuousRGBPixel& Value)
            {
                m_Elements.m_R = Byte(Value.m_Elements.m_R + Real(0.5));
                m_Elements.m_G = Byte(Value.m_Elements.m_G + Real(0.5));
                m_Elements.m_B = Byte(Value.m_Elements.m_B + Real(0.5));
            }

            void operator=(const Real Value)
            {
                const Byte MappedValue = Byte(Value + Real(0.5));
                m_Elements.m_R = MappedValue;
                m_Elements.m_G = MappedValue;
                m_Elements.m_B = MappedValue;
            }

            void operator=(const DebugContinuousTristimulusPixel& Value)
            {
                m_Elements.m_R = Byte(Value.m_Elements.m_R);
                m_Elements.m_G = Byte(Value.m_Elements.m_G);
                m_Elements.m_B = Byte(Value.m_Elements.m_B);
            }

            void operator=(const bool Value)
            {
                if (Value)
                {
                    m_Elements.m_R = 255;
                    m_Elements.m_G = 255;
                    m_Elements.m_B = 255;
                }
                else
                {
                    m_Elements.m_R = 0;
                    m_Elements.m_G = 0;
                    m_Elements.m_B = 0;
                }
            }

            struct Elements
            {
                Byte m_R;
                Byte m_G;
                Byte m_B;
            };

            Elements m_Elements;
            Byte m_Channels[3];
        };

        union DiscreteHSVPixel
        {
            struct Elements
            {
                Byte m_H;
                Byte m_S;
                Byte m_V;
            };

            Elements m_Elements;
            Byte m_Channels[3];
        };

    }
}
