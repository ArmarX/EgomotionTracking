/*
 * DiscreteRGBColorMap.cpp
 */

#include "DiscreteRGBColorMap.h"

namespace RVL
{
    namespace Imaging
    {
        CDiscreteRGBColorMap::CDiscreteRGBColorMap() :
            m_LUTScaleFactor(Real(0)),
            m_pLUT(nullptr),
            m_ColorKeys()
        {
        }

        CDiscreteRGBColorMap::CDiscreteRGBColorMap(const PredefinedColorMap ColorMap) :
            m_LUTScaleFactor(Real(0)),
            m_pLUT(nullptr),
            m_ColorKeys()
        {
            LoadPredefinedColorMap(ColorMap);
        }

        CDiscreteRGBColorMap::~CDiscreteRGBColorMap()
        {
            Clear();
        }

        bool CDiscreteRGBColorMap::IsValid() const
        {
            return m_pLUT;
        }

        bool CDiscreteRGBColorMap::AddColorKey(const DiscreteRGBPixel& ColorKey, const Real PositionKey)
        {
            if ((PositionKey >= Real(0)) && (PositionKey <= Real(1)) && (m_ColorKeys.find(PositionKey) == m_ColorKeys.end()))
            {
                m_ColorKeys[PositionKey] = ColorKey;
                return true;
            }
            return false;
        }

        bool CDiscreteRGBColorMap::LoadColorMap()
        {
            if (m_ColorKeys.size() > 1)
            {
                if (m_pLUT)
                {
                    delete[] m_pLUT;
                    m_pLUT = nullptr;
                }
                const Real TotalColorDistance = GetMaximalKeyRatio();
                const int LUTQuantizationLevels = int(std::ceil(TotalColorDistance)) + 1;
                m_LUTScaleFactor = Real(LUTQuantizationLevels - 1);
                m_pLUT = new DiscreteRGBPixel[LUTQuantizationLevels + 1];
                const Real Delta = Real(1) / m_LUTScaleFactor;
                Real Position = Real(0);
                std::map<Real, DiscreteRGBPixel>::const_iterator pColorKeyA = m_ColorKeys.begin();
                std::map<Real, DiscreteRGBPixel>::const_iterator pColorKeyB = pColorKeyA;
                ++pColorKeyB;
                DiscreteRGBPixel* pColor = m_pLUT;
                while (Position <= Real(1))
                {
                    if ((Position >= pColorKeyA->first) && (Position <= pColorKeyB->first))
                    {
                        const Real IntervalLength = pColorKeyB->first - pColorKeyA->first;
                        const Real ContributionA = (pColorKeyB->first - Position) / IntervalLength;
                        const Real ContributionB = (Position - pColorKeyA->first) / IntervalLength;
                        for (int i = 0 ; i < 3 ; ++i)
                        {
                            pColor->m_Channels[i] = Byte(std::round(Real(pColorKeyA->second.m_Channels[i]) * ContributionA + Real(pColorKeyB->second.m_Channels[i]) * ContributionB));
                        }
                        ++pColor;
                        Position += Delta;
                    }
                    else
                    {
                        ++pColorKeyA;
                        ++pColorKeyB;
                    }
                }
                return true;
            }
            return false;
        }

        void CDiscreteRGBColorMap::Clear()
        {
            if (m_pLUT)
            {
                delete[] m_pLUT;
                m_pLUT = nullptr;
            }
            m_ColorKeys.clear();
            m_LUTScaleFactor = Real(0);
        }

        int CDiscreteRGBColorMap::GetLUTScaleFactor() const
        {
            return m_LUTScaleFactor;
        }

        const DiscreteRGBPixel* CDiscreteRGBColorMap::GetLUT() const
        {
            return m_pLUT;
        }

        const DiscreteRGBPixel& CDiscreteRGBColorMap::GetColor(const Real Position) const
        {
            return m_pLUT[int((Position * m_LUTScaleFactor) + Real(0.5))];
        }

        void CDiscreteRGBColorMap::LoadPredefinedColorMap(const PredefinedColorMap ColorMap)
        {
            Clear();
            switch (ColorMap)
            {
                case eRGB_Red:
                    AddColorKey(DiscreteRGBPixel(0, 0, 0), Real(0));
                    AddColorKey(DiscreteRGBPixel(255, 0, 0), Real(1));
                    LoadColorMap();
                    break;
                case eRGB_Green:
                    AddColorKey(DiscreteRGBPixel(0, 0, 0), Real(0));
                    AddColorKey(DiscreteRGBPixel(0, 255, 0), Real(1));
                    LoadColorMap();
                    break;
                case eRGB_Blue:
                    AddColorKey(DiscreteRGBPixel(0, 0, 0), Real(0));
                    AddColorKey(DiscreteRGBPixel(0, 0, 255), Real(1));
                    LoadColorMap();
                    break;
                case eRGB_Intensity:
                    AddColorKey(DiscreteRGBPixel(0, 0, 0), Real(0));
                    AddColorKey(DiscreteRGBPixel(255, 255, 255), Real(1));
                    LoadColorMap();
                    break;
                case eRGB_InverseIntensity:
                    AddColorKey(DiscreteRGBPixel(255, 255, 255), Real(0));
                    AddColorKey(DiscreteRGBPixel(0, 0, 0), Real(1));
                    LoadColorMap();
                    break;
                case eRGB_Hot:
                {
                    DiscreteRGBPixel ColorKeys[16];
                    ColorKeys[0].Set(0, 0, 189);
                    ColorKeys[1].Set(0, 0, 255);
                    ColorKeys[2].Set(0, 66, 255);
                    ColorKeys[3].Set(0, 132, 255);
                    ColorKeys[4].Set(0, 189, 255);
                    ColorKeys[5].Set(0, 255, 255);
                    ColorKeys[6].Set(66, 255, 189);
                    ColorKeys[7].Set(132, 255, 132);
                    ColorKeys[8].Set(189, 255, 66);
                    ColorKeys[9].Set(255, 255, 0);
                    ColorKeys[10].Set(255, 189, 0);
                    ColorKeys[11].Set(255, 132, 0);
                    ColorKeys[12].Set(255, 66, 0);
                    ColorKeys[13].Set(189, 0, 0);
                    ColorKeys[14].Set(132, 0, 0);
                    ColorKeys[15].Set(128, 0, 0);
                    for (int i = 0 ; i < 16 ; ++i)
                    {
                        AddColorKey(ColorKeys[i], Real(i) * Real(1) / Real(15));
                    }
                    LoadColorMap();
                }
                break;
            }
        }

        Real CDiscreteRGBColorMap::GetChebyshevColorKeyDistance(const DiscreteRGBPixel& ColorA, const DiscreteRGBPixel& ColorB) const
        {
            return Real(std::max(std::max(abs(int(ColorA.m_Elements.m_R) - int(ColorB.m_Elements.m_R)), abs(int(ColorA.m_Elements.m_G) - int(ColorB.m_Elements.m_G))), abs(int(ColorA.m_Elements.m_B) - int(ColorB.m_Elements.m_B))));
        }

        Real CDiscreteRGBColorMap::GetMaximalKeyRatio() const
        {
            std::map<Real, DiscreteRGBPixel>::const_iterator pColorKeyA = m_ColorKeys.begin();
            std::map<Real, DiscreteRGBPixel>::const_iterator pColorKeyB = pColorKeyA;
            ++pColorKeyB;
            Real MaximalKeyRatio = Real(0);
            while (pColorKeyB != m_ColorKeys.end())
            {
                const Real KeyRatio = GetChebyshevColorKeyDistance(pColorKeyA->second, pColorKeyB->second) / std::abs(pColorKeyA->first - pColorKeyB->first);
                if (KeyRatio > MaximalKeyRatio)
                {
                    MaximalKeyRatio = KeyRatio;
                }
                ++pColorKeyA;
                ++pColorKeyB;
            }
            return MaximalKeyRatio;
        }
    }
}
