/*
 * ImageExporter.h
 */

#pragma once

#include "TImage.hpp"
#include "PixelTypes.h"

namespace RVL
{
    namespace Imaging
    {
        class CImageExporter
        {
        public:

            static bool Export(const TImage<bool>* pBoolImage, const std::string& FileName);
            static bool Export(const TImage<Byte>* pByteImage, const std::string& FileName);
            static bool Export(const TImage<DiscreteRGBPixel>* pDiscreteRGBImage, const std::string& FileName);

        protected:

            CImageExporter();
            virtual ~CImageExporter();
        };
    }
}

