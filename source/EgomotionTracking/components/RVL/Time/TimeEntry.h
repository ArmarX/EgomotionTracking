/*
 * TimeEntry.h
 */

#pragma once

#include "../Common/DataTypes.h"
#include "../Common/Includes.h"

namespace RVL
{
    namespace Time
    {
        class CTimeEntry
        {
        public:

            struct TrialInformation
            {
                TrialInformation() :
                    m_Id(0),
                    m_Ticks(0),
                    m_NaturalTimeInMicroseconds(Real(0.0)),
                    m_TicksTimeInMicroseconds(Real(0.0))
                {
                }

                int m_Id;
                int m_Ticks;
                Real m_NaturalTimeInMicroseconds;
                Real m_TicksTimeInMicroseconds;
            };

            static int64_t Now();

            CTimeEntry(const int OwnerId, const std::string& Name);
            ~CTimeEntry();

            void Start();
            void Stop(const bool StoreTrial);
            void Stop(const timeval& T1, const clock_t& C1, const bool StoreTrial);
            void SetName(const std::string& Name);
            const std::string& GetName() const;
            int GetOwnerId() const;
            Real GetCurrentElapsedNaturalTimeInMicroseconds() const;
            Real GetCurrentElapsedNaturalTimeInMilliseconds() const;
            Real GetCurrentElapsedTicksTimeInMicroseconds() const;
            Real GetCurrentElapsedTicksTimeInMilliseconds() const;
            int GetElapsedTicks() const;
            Real GetElapsedDeviationInMicroseconds() const;
            Real GetElapsedDeviationInMilliseconds() const;
            const TrialInformation& GetTrialInformation() const;
            const std::list<TrialInformation>& GetTrials() const;

        protected:

            int m_OwnerId;
            std::string m_Name;
            timeval m_T0;
            timeval m_T1;
            clock_t m_C0;
            clock_t m_C1;
            TrialInformation m_ElapsedTime;
            std::list<TrialInformation> m_Trials;
        };
    }
}

