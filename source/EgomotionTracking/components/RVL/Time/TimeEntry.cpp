/*
 * TimeEntry.cpp
 */

#include "TimeEntry.h"

namespace RVL
{
    namespace Time
    {
        int64_t CTimeEntry::Now()
        {
            timeval T;
            gettimeofday(&T, nullptr);
            return int64_t(T.tv_sec) * int64_t(1000000) + int64_t(T.tv_usec);
        }

        CTimeEntry::CTimeEntry(const int OwnerId, const std::string& Name) :
            m_OwnerId(OwnerId),
            m_Name(Name),
            m_C0(0),
            m_C1(0)
        {
            memset(&m_T0, 0, sizeof(timeval));
            memset(&m_T1, 0, sizeof(timeval));
        }

        CTimeEntry::~CTimeEntry()
            = default;

        void CTimeEntry::Start()
        {
            gettimeofday(&m_T0, nullptr);
            m_C0 = clock();
        }

        void CTimeEntry::Stop(const bool StoreTrial)
        {
            gettimeofday(&m_T1, nullptr);
            m_C1 = clock();
            ++m_ElapsedTime.m_Id;
            m_ElapsedTime.m_Ticks = m_C1 - m_C0;
            m_ElapsedTime.m_TicksTimeInMicroseconds = Real(m_ElapsedTime.m_Ticks) * (Real(1000000) / Real(CLOCKS_PER_SEC));
            m_ElapsedTime.m_NaturalTimeInMicroseconds = ((m_T1.tv_sec - m_T0.tv_sec) * 1000000) + m_T1.tv_usec - m_T0.tv_usec;
            if (StoreTrial)
            {
                m_Trials.push_back(m_ElapsedTime);
            }
        }

        void CTimeEntry::Stop(const timeval& T1, const clock_t& C1, const bool StoreTrial)
        {
            m_T1 = T1;
            m_C1 = C1;
            ++m_ElapsedTime.m_Id;
            m_ElapsedTime.m_Ticks = m_C1 - m_C0;
            m_ElapsedTime.m_TicksTimeInMicroseconds = Real(m_ElapsedTime.m_Ticks) * (Real(1000000) / Real(CLOCKS_PER_SEC));
            m_ElapsedTime.m_NaturalTimeInMicroseconds = ((m_T1.tv_sec - m_T0.tv_sec) * 1000000) + m_T1.tv_usec - m_T0.tv_usec;
            if (StoreTrial)
            {
                m_Trials.push_back(m_ElapsedTime);
            }
        }

        void CTimeEntry::SetName(const std::string& Name)
        {
            m_Name = Name;
        }

        const std::string& CTimeEntry::GetName() const
        {
            return m_Name;
        }

        int CTimeEntry::GetOwnerId() const
        {
            return m_OwnerId;
        }

        Real CTimeEntry::GetCurrentElapsedNaturalTimeInMicroseconds() const
        {
            return m_ElapsedTime.m_NaturalTimeInMicroseconds;
        }

        Real CTimeEntry::GetCurrentElapsedNaturalTimeInMilliseconds() const
        {
            return m_ElapsedTime.m_NaturalTimeInMicroseconds / Real(1000.0);
        }

        Real CTimeEntry::GetCurrentElapsedTicksTimeInMicroseconds() const
        {
            return m_ElapsedTime.m_TicksTimeInMicroseconds;
        }

        Real CTimeEntry::GetCurrentElapsedTicksTimeInMilliseconds() const
        {
            return m_ElapsedTime.m_TicksTimeInMicroseconds / Real(1000.0);
        }

        int CTimeEntry::GetElapsedTicks() const
        {
            return m_ElapsedTime.m_Ticks;
        }

        Real CTimeEntry::GetElapsedDeviationInMicroseconds() const
        {
            return m_ElapsedTime.m_NaturalTimeInMicroseconds - m_ElapsedTime.m_TicksTimeInMicroseconds;
        }

        Real CTimeEntry::GetElapsedDeviationInMilliseconds() const
        {
            return (m_ElapsedTime.m_NaturalTimeInMicroseconds - m_ElapsedTime.m_TicksTimeInMicroseconds) / Real(1000.0);
        }

        const CTimeEntry::TrialInformation& CTimeEntry::GetTrialInformation() const
        {
            return m_ElapsedTime;
        }

        const std::list<CTimeEntry::TrialInformation>& CTimeEntry::GetTrials() const
        {
            return m_Trials;
        }
    }
}
