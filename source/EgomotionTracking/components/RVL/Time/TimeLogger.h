/*
 * TimeLogger.h
 */

#pragma once

#include "../Common/DataTypes.h"
#include "../Common/Includes.h"
#include "../Console/ConsoleOutputManager.h"
#include "../Processing/CPU/Threading/Mutex.h"
#include "TimeEntry.h"

#define _RVL_TIME_LOGGER_BEGIN_(ID,X) RVL::Time::CTimeLogger::StartTimeEntry(ID,X,true)
#define _RVL_TIME_LOGGER_END_(ID,X)  RVL::Time::CTimeLogger::StopTimeEntry(ID,X,false,true,false)
#define _RVL_TIME_LOGGER_VERBOSE_END_(ID,X)  RVL::Time::CTimeLogger::StopTimeEntry(ID,X,true,true,true)
#define _RVL_TIME_LOGGER_CLEAR_ALL RVL::Time::CTimeLogger::ClearAll();

namespace RVL
{
    namespace Time
    {
        class CTimeLogger
        {
        public:

            static bool StartTimeEntry(const int OwnerId, const char* pName, const bool Reset);
            static Real StopTimeEntry(const int OwnerId, const char* pName, const bool StoreTrial, const bool DeleteEntry, const bool Verbose = true);
            static const std::list<CTimeEntry*>* GetEntries(const int OwnerId);

            static void Clear(const int OwnerId);
            static void ClearAll();

        protected:

            CTimeLogger();
            ~CTimeLogger();

            static Processing::CPU::Threading::CMutex s_TimeEntriesMapMutex;
            static std::map<int, std::list<CTimeEntry*> > s_TimeEntriesMap;
        };
    }
}

