/*
 * TimeLogger.cpp
 */

#include "TimeLogger.h"

namespace RVL
{
    namespace Time
    {
        Processing::CPU::Threading::CMutex CTimeLogger::s_TimeEntriesMapMutex;

        std::map<int, std::list<CTimeEntry*> > CTimeLogger::s_TimeEntriesMap;

        CTimeLogger::CTimeLogger()
            = default;

        CTimeLogger::~CTimeLogger()
        {
            ClearAll();
        }

        bool CTimeLogger::StartTimeEntry(const int OwnerId, const char* pName, const bool Reset)
        {
            if (pName)
            {
                std::string Name(pName);
                if (Name.length())
                {
                    s_TimeEntriesMapMutex.BlockingLock();
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    std::map<int, std::list<CTimeEntry*> >::iterator pProcessEntriesLocation = s_TimeEntriesMap.find(OwnerId);
                    if (pProcessEntriesLocation != s_TimeEntriesMap.end())
                    {
                        for (auto& ppTimeEntry : pProcessEntriesLocation->second)
                            if (ppTimeEntry->GetName() == Name)
                            {
                                if (Reset)
                                {
                                    ppTimeEntry->Start();
                                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    s_TimeEntriesMapMutex.Unlock();
                                    return true;
                                }
                                else
                                {
                                    std::ostringstream WarningInformation;
                                    WarningInformation << _RVL_HEAD_FULL_INFORMATION_ << _RVL_CONSOLE_CURRENT_PREFIX_LEVEL_;
                                    WarningInformation << "TimeLogger [ OwnerId = " << OwnerId << ", Name = " << Name << " ] [Trying to start existing time entry without \"Reset\" flags]";
                                    _RVL_CONSOLE_OUTPUT_WARNINGS_(WarningInformation, 0);
                                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    s_TimeEntriesMapMutex.Unlock();
                                    return false;
                                }
                            }
                    }
                    CTimeEntry* pTimeEntry = new CTimeEntry(OwnerId, Name);
                    std::list<CTimeEntry*> ProcessEntries;
                    ProcessEntries.push_back(pTimeEntry);
                    s_TimeEntriesMap[OwnerId] = ProcessEntries;
                    pTimeEntry->Start();
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    s_TimeEntriesMapMutex.Unlock();
                    return true;
                }
            }
            return false;
        }

        Real CTimeLogger::StopTimeEntry(const int OwnerId, const char* pName, const bool StoreTrial, const bool DeleteEntry, const bool Verbose)
        {
            if (pName)
            {
                timeval T1;
                gettimeofday(&T1, nullptr);
                clock_t C1 = clock();
                std::string Name(pName);

                s_TimeEntriesMapMutex.BlockingLock();
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                std::map<int, std::list<CTimeEntry*> >::iterator pProcessEntriesLocation = s_TimeEntriesMap.find(OwnerId);
                if (pProcessEntriesLocation == s_TimeEntriesMap.end())
                {
                    std::ostringstream WarningInformation;
                    WarningInformation << _RVL_HEAD_FULL_INFORMATION_ << _RVL_CONSOLE_CURRENT_PREFIX_LEVEL_;
                    WarningInformation << "TimeLogger [ OwnerId = " << OwnerId << ", Name = " << Name << " ] [Non registered Owner is trying to stop time entry]";
                    _RVL_CONSOLE_OUTPUT_WARNINGS_(WarningInformation, 0);
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    s_TimeEntriesMapMutex.Unlock();
                    return Real(0);
                }
                for (std::list<CTimeEntry*>::iterator ppTimeEntry = pProcessEntriesLocation->second.begin() ; ppTimeEntry != pProcessEntriesLocation->second.end() ; ++ppTimeEntry)
                    if ((*ppTimeEntry)->GetName() == Name)
                    {
                        (*ppTimeEntry)->Stop(T1, C1, StoreTrial && (!DeleteEntry));
                        const Real ElapsedNaturalTime = (*ppTimeEntry)->GetCurrentElapsedNaturalTimeInMilliseconds();
                        if (Verbose)
                        {
                            std::ostringstream ElapsedTimeInformation;
                            ElapsedTimeInformation.precision(g_RealDisplayDigits);
                            ElapsedTimeInformation << "TimeLogger [ OwnerId = " << OwnerId << ", Name = \"" << Name << "\" ] [ Elapsed Natural Time = " << ElapsedNaturalTime << ", Elapsed Ticks Time  = " << (*ppTimeEntry)->GetCurrentElapsedTicksTimeInMilliseconds() << ", Deviation = " << (*ppTimeEntry)->GetElapsedDeviationInMilliseconds() << ", Ticks = " << (*ppTimeEntry)->GetElapsedTicks() << " ]";
                            _RVL_CONSOLE_OUTPUT_GENERAL_(ElapsedTimeInformation, 0);
                        }
                        if (DeleteEntry)
                        {
                            pProcessEntriesLocation->second.erase(ppTimeEntry);
                            delete *ppTimeEntry;
                        }
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        s_TimeEntriesMapMutex.Unlock();
                        return ElapsedNaturalTime;
                    }
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                s_TimeEntriesMapMutex.Unlock();
            }
            return Real(0);
        }

        const std::list<CTimeEntry*>* CTimeLogger::GetEntries(const int OwnerId)
        {
            s_TimeEntriesMapMutex.BlockingLock();
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            const std::list<CTimeEntry*>* pEntries = nullptr;
            std::map<int, std::list<CTimeEntry*> >::const_iterator pProcessEntriesLocation = s_TimeEntriesMap.find(OwnerId);
            if (pProcessEntriesLocation != s_TimeEntriesMap.end())
            {
                pEntries = &pProcessEntriesLocation->second;
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            s_TimeEntriesMapMutex.Unlock();
            return pEntries;
        }

        void CTimeLogger::Clear(const int OwnerId)
        {
            s_TimeEntriesMapMutex.BlockingLock();
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            std::map<int, std::list<CTimeEntry*> >::iterator pProcessEntriesLocation = s_TimeEntriesMap.find(OwnerId);
            if (pProcessEntriesLocation != s_TimeEntriesMap.end())
            {
                for (auto& ppTimeEntry : pProcessEntriesLocation->second)
                {
                    delete ppTimeEntry;
                }
                pProcessEntriesLocation->second.clear();
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            s_TimeEntriesMapMutex.Unlock();
        }

        void CTimeLogger::ClearAll()
        {
            s_TimeEntriesMapMutex.BlockingLock();
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            for (auto& pProcessEntriesLocation : s_TimeEntriesMap)
            {
                for (std::list<CTimeEntry*>::iterator ppTimeEntry = pProcessEntriesLocation.second.begin() ; ppTimeEntry != pProcessEntriesLocation.second.end() ; ++ppTimeEntry)
                {
                    delete *ppTimeEntry;
                }
                pProcessEntriesLocation.second.clear();
            }
            s_TimeEntriesMap.clear();
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            s_TimeEntriesMapMutex.Unlock();
        }
    }
}
