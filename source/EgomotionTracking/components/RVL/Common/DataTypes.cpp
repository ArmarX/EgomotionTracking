/*
 * DataTypes.cpp
 */

#include "DataTypes.h"

namespace RVL
{
    bool IsInRangeInclusive(const Real X, const Real LowerBound, const Real UpperBound)
    {
        return (X >= LowerBound) && (X <= UpperBound);
    }

    bool IsInRangeExclusive(const Real X, const Real LowerBound, const Real UpperBound)
    {
        return (X > LowerBound) && (X < UpperBound);
    }

    bool IsAtInfinity(const Real X)
    {
        return (X == g_RealPlusInfinity) || (X == g_RealMinusInfinity);
    }

    bool IsNotAtInfinity(const Real X)
    {
        return (X != g_RealPlusInfinity) && (X != g_RealMinusInfinity);
    }

    bool IsZero(const Real X)
    {
        return (std::abs(X) <= g_RealPlusEpsilon);
    }

    bool IsOne(const Real X)
    {
        return (std::abs(Real(1) - X) <= g_RealPlusEpsilon);
    }

    bool IsNonZero(const Real X)
    {
        return (std::abs(X) > g_RealPlusEpsilon);
    }

    bool IsSignificant(const Real X)
    {
        return (X > g_RealPlusEpsilon);
    }

    bool IsPositive(const Real X)
    {
        return (X >= g_RealPlusEpsilon);
    }

    bool IsNonPositive(const Real X)
    {
        return (X < g_RealPlusEpsilon);
    }

    bool IsNegative(const Real X)
    {
        return (X <= g_RealMinusEpsilon);
    }

    bool IsNonNegative(const Real X)
    {
        return (X > g_RealMinusEpsilon);
    }

    bool Equals(const Real X, const Real Y)
    {
        return (std::abs(X - Y) <= g_RealPlusEpsilon);
    }

    bool NonEquals(const Real X, const Real Y)
    {
        return (std::abs(X - Y) > g_RealPlusEpsilon);
    }

    Real MapToTextDisplay(const Real Value)
    {
        return (std::abs(Value) <= g_RealPlusEpsilon) ? Real(0) : Value;
    }

    Real AngleInRadians(const Real AlphaInDegrees)
    {
        return AlphaInDegrees * Real(0.01745329251994329508812465664990831726299802539870);
    }

    Real AngleInDegrees(const Real AlphaInRadians)
    {
        return AlphaInRadians * Real(57.29577951308232311097845546044027287280187010765076);
    }

    std::string IdentifierToString(const uint32_t Id, const int Lenght)
    {
        std::ostringstream Stream;
        Stream << std::uppercase << std::hex << Id;
        std::string Content = Stream.str();
        const int TotalPadding = int(Lenght) - int(Content.length());
        std::string Padding;
        for (int i = 0 ; i < TotalPadding ; ++i)
        {
            Padding += "0";
        }
        return Padding + Content;
    }

    std::string DeviceIdentifierToString(const uint64_t Id)
    {
        return IdentifierToString(uint32_t(Id >> 32), 8) + IdentifierToString(uint32_t(Id), 8);
    }
}
