/*
 * DataTypes.h
 */

#pragma once

#include "Includes.h"

namespace RVL
{
    //typedef unsigned char Byte;
    typedef double Real;

    const int g_IntegerPlusInfinity = std::numeric_limits<int>::max();
    const int g_IntegerMinusInfinity = std::numeric_limits<int>::min();
    const Real g_RealPlusInfinity = std::numeric_limits<Real>::max();
    const Real g_RealPlusEpsilon = std::numeric_limits<Real>::epsilon() * Real(100);
    const Real g_RealMinusEpsilon = -std::numeric_limits<Real>::epsilon() * Real(100);
    const Real g_RealMinusInfinity = -std::numeric_limits<Real>::max();
    const Real g_RealNaN = std::numeric_limits<float>::quiet_NaN();
    const Real g_RealHalfPI = Real(1.57079632679489655799898173427209258079528808593750);
    const Real g_RealThirdPI = Real(1.047197551);
    const Real g_RealInverseHalfPI = Real(0.63661977236758136790457929254927194051560945808887);
    const Real g_RealPI = Real(3.141592653589793238462643383279502884197169399375);
    const Real g_RealInversePI = Real(0.31830988618379068395228964627463597025780472904444);
    const Real g_Real2PI = Real(6.283185307179586231995926937088370323181152343750);
    const Real g_RealSquareRoot2 = Real(1.41421356237309504876378807303183293697657063603401);
    const int g_RealDisplayDigits = std::numeric_limits<Real>::digits10;

    bool IsInRangeInclusive(const Real X, const Real LowerBound, const Real UpperBound);
    bool IsInRangeExclusive(const Real X, const Real LowerBound, const Real UpperBound);
    bool IsAtInfinity(const Real X);
    bool IsNotAtInfinity(const Real X);
    bool IsZero(const Real X);
    bool IsOne(const Real X);
    bool IsNonZero(const Real X);
    bool IsSignificant(const Real X);
    bool IsPositive(const Real X);
    bool IsNonPositive(const Real X);
    bool IsNegative(const Real X);
    bool IsNonNegative(const Real X);
    bool Equals(const Real X, const Real Y);
    bool NonEquals(const Real X, const Real Y);
    Real MapToTextDisplay(const Real Value);
    Real AngleInRadians(const Real AlphaInDegrees);
    Real AngleInDegrees(const Real AlphaInRadians);

    std::string IdentifierToString(const uint32_t Id, const int Lenght);
    std::string DeviceIdentifierToString(const uint64_t Id);

}

namespace std
{
    template <typename T> T round(const T Value)
    {
        return (Value > T(0)) ? std::floor(Value + T(0.5)) : std::ceil(Value - T(0.5));
    }

    template <typename T> T log2(const T x)
    {
        return std::log(x) * T(1.44269504088896340739);
    }
}

