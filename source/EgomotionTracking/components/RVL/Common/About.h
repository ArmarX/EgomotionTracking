/*
 * RVL.h
 */

#pragma once

//GENERAL INFORMATION
/////////////////////////////////////////////////////////////////////////////

#define _LIBRARY_ACRONYM_ "RVL"
#define _LIBRARY_NAME_ "Robot Vision Library"
#define _MAJOR_VERSION_ 0
#define _MINOR_VERSION_ 1
#define _REVISION_NUMBER_ 0
#define _BUILD_NUMBER_ 0
#define _AUTHOR_ "M.Sc.-Ing. David Israel González-Aguirre"
#define _AUTHOR_MAIL_ "david.gonzalez@kit.edu"
#define _CONTRIBUTOR_0_ "Dipl-Inf. Julian Hoch"
#define _CONTRIBUTOR_1_ "Dipl-Inf. Michael Vollert"
#define _THANKS_0_ "Prof. Dr. R. Dillmann"
#define _THANKS_1_ "Prof. Dr. T. Asfour"
#define _THANKS_2_ "Prof. Dr. E. Bayro"
#define _THANKS_3_ "Dr. P. Azad"
#define _SUPPORTER_0_ "DAAD-CONACYT"
#define _SUPPORTER_1_ "KIT-Karlsruhe Institute for Technology"
#define _SUPPORTER_2_ "DFG-Project SFB 588"
#define _SUPPORTER_3_ "EU-Project GRASP"
#define _SUPPORTER_4_ "EU-Project Xperience"

/////////////////////////////////////////////////////////////////////////////

