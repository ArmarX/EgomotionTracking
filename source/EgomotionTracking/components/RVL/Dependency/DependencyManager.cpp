/*
 * DependencyManager.cpp
 */

#include "DependencyManager.h"

namespace RVL
{
    namespace Dependency
    {
        IDependencyManager::IDependencyManager() :
            m_DispatchDestructorEvent(true),
            m_DispatchChangeEvent(true)
        {
        }

        IDependencyManager::~IDependencyManager()
        {
            if (m_DispatchDestructorEvent && m_Dependencies.size())
                for (auto m_Dependencie : m_Dependencies)
                {
                    m_Dependencie->OnDestructor(this);
                }
        }

        void IDependencyManager::SetDestructorEventEnabled(const bool Enabled)
        {
            m_DispatchDestructorEvent = Enabled;
        }

        bool IDependencyManager::IsDestructorEventEnabled()
        {
            return m_DispatchDestructorEvent;
        }

        void IDependencyManager::SetChangeEventEnabled(const bool Enabled)
        {
            m_DispatchChangeEvent = Enabled;
        }

        bool IDependencyManager::IsChangeEventEnabled()
        {
            return m_DispatchChangeEvent;
        }

        bool IDependencyManager::AddDependency(IDependency* pDependency)
        {
            if (pDependency && (m_Dependencies.find(pDependency) == m_Dependencies.end()))
            {
                m_Dependencies.insert(pDependency);
                return true;
            }
            return false;
        }

        bool IDependencyManager::RemoveDependency(IDependency* pDependency)
        {
            if (pDependency)
            {
                std::set<IDependency*>::iterator ppLocation = m_Dependencies.find(pDependency);
                if (ppLocation != m_Dependencies.end())
                {
                    m_Dependencies.erase(ppLocation);
                    return true;
                }
            }
            return false;
        }

        void IDependencyManager::OnChange(const int EventFlags)
        {
            if (m_DispatchChangeEvent && m_Dependencies.size())
                for (auto m_Dependencie : m_Dependencies)
                {
                    m_Dependencie->OnChange(this, EventFlags);
                }
        }
    }
}
