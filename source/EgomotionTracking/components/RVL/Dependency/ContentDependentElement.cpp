/*
 * ActiveElement.cpp
 */

#include "ContentDependentElement.h"

namespace RVL
{
    namespace Dependency
    {
        CContentDependentElement::CContentDependentElement(const CContentDependentElement* pSource) :
            m_pSource(pSource),
            m_ContentId(0)
        {
        }

        CContentDependentElement::~CContentDependentElement()
            = default;

        bool CContentDependentElement::SetSource(const CContentDependentElement* pSource)
        {
            m_pSource = pSource;
            return true;
        }

        bool CContentDependentElement::SynchronizeContentId()
        {
            if (m_pSource)
            {
                m_ContentId = m_pSource->m_ContentId;
                return true;
            }
            return false;
        }

        int CContentDependentElement::GetContentId() const
        {
            return m_ContentId;
        }

        bool CContentDependentElement::IsContentIdSynchronized() const
        {
            if (m_pSource)
            {
                return (m_ContentId == m_pSource->m_ContentId);
            }
            return false;
        }
    }
}
