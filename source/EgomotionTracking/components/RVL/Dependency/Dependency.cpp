/*
 * Dependency.cpp
 */

#include "Dependency.h"

namespace RVL
{
    namespace Dependency
    {
        IDependency::IDependency()
            = default;

        IDependency::~IDependency()
            = default;
    }
}
