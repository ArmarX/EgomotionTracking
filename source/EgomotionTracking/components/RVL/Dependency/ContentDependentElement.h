/*
 * ContentDependentElement.h
 */

#pragma once

namespace RVL
{
    namespace Dependency
    {
        class CContentDependentElement
        {
        public:

            CContentDependentElement(const CContentDependentElement* pSource);
            virtual ~CContentDependentElement();

            virtual bool SetSource(const CContentDependentElement* pSource);
            bool SynchronizeContentId();

            int GetContentId() const;
            bool IsContentIdSynchronized() const;

        protected:

            const CContentDependentElement* m_pSource;
            int m_ContentId;
        };
    }
}

