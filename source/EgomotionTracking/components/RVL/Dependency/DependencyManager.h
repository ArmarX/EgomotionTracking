/*
 * DependencyManager.h
 */

#pragma once

#include "../Common/Includes.h"
#include "Dependency.h"

namespace RVL
{
    namespace Dependency
    {
        class IDependencyManager
        {
        public:

            IDependencyManager();
            virtual ~IDependencyManager();

            void SetDestructorEventEnabled(const bool Enabled);
            bool IsDestructorEventEnabled();

            void SetChangeEventEnabled(const bool Enabled);
            bool IsChangeEventEnabled();

            bool AddDependency(IDependency* pDependency);
            bool RemoveDependency(IDependency* pDependency);

        protected:

            void OnChange(const int EventFlags = 0);

            bool m_DispatchDestructorEvent;
            bool m_DispatchChangeEvent;
            std::set<IDependency*> m_Dependencies;
        };
    }
}

