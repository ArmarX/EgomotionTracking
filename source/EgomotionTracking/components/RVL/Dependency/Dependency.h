/*
 * Dependency.h
 */

#pragma once

namespace RVL
{
    namespace Dependency
    {
        class IDependencyManager;

        class IDependency
        {
        public:

            IDependency();
            virtual ~IDependency();

            virtual void OnDestructor(IDependencyManager* pDependencyManager) = 0;
            virtual void OnChange(IDependencyManager* pDependencyManager, const int EventFlags) = 0;
        };
    }
}

