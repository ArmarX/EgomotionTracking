/*
 * ModelVertex.h
 */

#pragma once

#include "ModelElement.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    class CModelEdge;
                    class CModelFace;
                    class CModelMesh;

                    class CModelVertex : public CModelElement
                    {
                    public:

                        static CModelVertex* CreateFromXml(const Xml::CXmlNode* pXmlModelVertex, CModelMesh* pModelMesh);

                        CModelVertex(const Mathematics::_3D::CVector3D& Point, CModelMesh* pModelMesh, const bool AssignId);

                        ~CModelVertex() override;

                        Mathematics::_3D::CVector3D GetPoint(const bool ConsiderSuperior) const;

                        Mathematics::_3D::CVector3D GetDynamicPoint() const;

                        bool HasIndentifiableTransformation(const int Id) const;

                        bool GetDynamicPointByIndentifiableTransformation(const int Id, Mathematics::_3D::CVector3D& Point) const;

                        void GetDynamicPoint(Mathematics::_3D::CVector3D& Point) const;

                        const std::map<int, std::pair<CModelVertex*, CModelEdge*> >& GetCoVertexEdges() const;

                        const std::map<int, CModelEdge*>& GetEdges() const;

                        const std::map<int, CModelFace*>& GetFaces() const;

                        Real GetDistance(const Mathematics::_3D::CVector3D& Point, const bool ConsiderSuperior) const;

                        bool AddEdge(CModelEdge* pModelEdge);

                        CModelEdge* GetWritableLinkingEdge(CModelVertex* pModelVertex);

                        bool AddFace(CModelFace* pModelFace);

                        void Clear() override;

                        CModelMesh* GetMesh();

                        virtual Containers::_3D::CContinuousBoundingBox3D GetBoundingBox(const bool ConsiderSuperior) const;

                        bool Deserialize(const Xml::CXmlNode* pXmlCurrentNode) override;

                        Xml::CXmlNode* Serialize(Xml::CXmlNode* pXmlParentNode) const override;

                        void SetVertexInMeshIndex(const int VertexInMeshIndex);

                        int GetVertexInMeshIndex() const;

                    protected:

                        friend class CModelMesh;

                        CModelVertex(CModelMesh* pModelMesh, const bool AssignId);

                        bool DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode) override;

                        bool SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const override;

                        int m_VertexInMeshIndex;
                        Mathematics::_3D::CVector3D m_Point;
                        std::map<int, std::pair<CModelVertex*, CModelEdge*> > m_CoVertexKeyEdges;
                        std::map<int, CModelEdge*> m_Edges;
                        std::map<int, CModelFace*> m_Faces;
                    };
                }
            }
        }
    }
}

