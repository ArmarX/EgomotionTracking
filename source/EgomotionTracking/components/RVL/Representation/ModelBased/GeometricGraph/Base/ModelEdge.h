/*
 * ModelEdge.h
 */

#pragma once

#include "ModelElement.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    class CModelVertex;
                    class CModelFace;
                    class CModelMesh;

                    class CModelEdge : public CModelElement
                    {
                    public:

                        static CModelEdge* CreateFromXml(const Xml::CXmlNode* pXmlModelEdge, CModelMesh* pModelMesh);

                        CModelEdge(CModelVertex* pModelVertexA, CModelVertex* pModelVertexB, CModelMesh* pModelMesh, const bool AssignId);
                        ~CModelEdge() override;


                        bool SetFace(CModelFace* pModelFace);
                        bool HasBothFaces() const;
                        bool IsAuxiliar() const;
                        CModelVertex* GetWritableVertexA();
                        CModelVertex* GetWritableVertexB();
                        const CModelVertex* GetReadOnlyVertexA() const;
                        const CModelVertex* GetReadOnlyVertexB() const;
                        CModelFace* GetWritableFaceP();
                        CModelFace* GetWritableFaceQ();
                        const CModelFace* GetReadOnlyFaceP() const;
                        const CModelFace* GetReadOnlyFaceQ() const;
                        Real GetLength() const;
                        Mathematics::_3D::CVector3D GetDirection(const bool ConsiderSuperior) const;
                        Mathematics::_3D::CVector3D GetMidPoint(const bool ConsiderSuperior) const;
                        void GetEndPoints(Mathematics::_3D::CVector3D& A, Mathematics::_3D::CVector3D& B, const bool ConsiderSuperior);
                        void Clear() override;
                        CModelMesh* GetMesh();
                        bool SolveIndirectReferences();

                        virtual Containers::_3D::CContinuousBoundingBox3D GetBoundingBox(const bool ConsiderSuperior) const;

                        void GetDynamicEndPoints(Mathematics::_3D::CVector3D& PointA, Mathematics::_3D::CVector3D& PointB) const;

                    protected:

                        friend class CModelMesh;

                        struct ModelFaceReference
                        {
                            union DualModelFaceReference
                            {
                                CModelFace* m_pModelFace;
                                int m_Id;
                            };

                            enum ReferenceMode
                            {
                                eUnkonw, eDirect, eIndirect
                            };

                            ModelFaceReference()
                            {
                                m_DualModelFaceReference.m_pModelFace = 0;
                                m_ReferenceMode = eUnkonw;
                            }

                            ModelFaceReference(CModelFace* pModelFace)
                            {
                                m_DualModelFaceReference.m_pModelFace = pModelFace;
                                m_ReferenceMode = eDirect;
                            }

                            ModelFaceReference(const int Id)
                            {
                                m_DualModelFaceReference.m_Id = Id;
                                m_ReferenceMode = eIndirect;
                            }

                            bool SetDirect(CModelFace* pModelFace)
                            {
                                if (pModelFace)
                                {
                                    m_DualModelFaceReference.m_pModelFace = pModelFace;
                                    m_ReferenceMode = eDirect;
                                    return true;
                                }
                                return false;
                            }

                            bool SetIndirect(const int Id)
                            {
                                if (Id > 0)
                                {
                                    m_DualModelFaceReference.m_Id = Id;
                                    m_ReferenceMode = eIndirect;
                                    return true;
                                }
                                return false;
                            }

                            bool IsSet() const
                            {
                                return m_ReferenceMode != eUnkonw;
                            }

                            bool IsSetDirect() const
                            {
                                return (m_ReferenceMode == eDirect) && m_DualModelFaceReference.m_pModelFace;
                            }

                            bool IsSetIndirect() const
                            {
                                return (m_ReferenceMode == eIndirect) && (m_DualModelFaceReference.m_Id > 0);
                            }

                            CModelFace* GetDirect()
                            {
                                return m_DualModelFaceReference.m_pModelFace;
                            }

                            const CModelFace* GetDirect() const
                            {
                                return m_DualModelFaceReference.m_pModelFace;
                            }

                            void Clear()
                            {
                                m_DualModelFaceReference.m_pModelFace = 0;
                                m_ReferenceMode = eUnkonw;
                            }

                            DualModelFaceReference m_DualModelFaceReference;
                            ReferenceMode m_ReferenceMode;
                        };

                        CModelEdge(CModelMesh* pModelMesh, const bool AssignId);
                        bool DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode) override;
                        bool SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const override;

                        CModelVertex* m_pModelVertexA;
                        CModelVertex* m_pModelVertexB;
                        ModelFaceReference m_ModelFaceReferenceP;
                        ModelFaceReference m_ModelFaceReferenceQ;
                        Real m_Length;
                    };
                }
            }
        }
    }
}

