/*
 * ModelOrientedVertex.h
 */

#pragma once

#include "ModelElement.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    class CModelVertex;
                    class CModelFace;
                    class CModelMesh;

                    class CModelOrientedVertex : public CModelElement
                    {
                    public:

                        static CModelOrientedVertex* CreateFromXml(const Xml::CXmlNode* pXmlModelOrientedVertex, CModelMesh* pModelMesh);

                        CModelOrientedVertex(CModelVertex* pModelVertex, const Mathematics::_3D::CVector3D& Normal, CModelMesh* pModelMesh, const bool AssignId);
                        ~CModelOrientedVertex() override;

                        const CModelVertex* GetReadOnlyModelVertex() const;
                        CModelVertex* GetWritableModelVertex();
                        Mathematics::_3D::CVector3D GetNormal(const bool ConsiderSuperior) const;

                        void Clear() override;
                        CModelMesh* GetMesh();
                        virtual Containers::_3D::CContinuousBoundingBox3D GetBoundingBox(const bool ConsiderSuperior) const;

                        bool IsEnabled(const bool ConsiderSuperior) const override;
                        void SetEnabled(const bool Enabled) override;

                    protected:

                        friend class CModelFace;
                        friend class CModelMesh;

                        CModelOrientedVertex(CModelMesh* pModelMesh, const bool AssignId);

                        bool DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode) override;
                        bool SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const override;

                        int m_NormalInMeshIndex;
                        CModelVertex* m_pModelVertex;
                        Mathematics::_3D::CVector3D m_Normal;
                    };
                }
            }
        }
    }
}

