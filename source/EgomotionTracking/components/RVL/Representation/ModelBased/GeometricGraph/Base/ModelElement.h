/*
 * ModelElement.h
 */

#pragma once

#include "../../../../Console/ConsoleOutputManager.h"
#include "../../../../Common/Includes.h"
#include "../../../../Common/About.h"
#include "../../../../Common/Tags.h"
#include "../../../../Containers/_3D/ContinuousBoundingBox3D.h"
#include "../../../../Dependency/Dependency.h"
#include "../../../../Dependency/DependencyManager.h"
#include "../../../../Mathematics/_3D/Geometry/Geometry3D.h"
#include "../../../../Mathematics/_3D/Vector3D.h"
#include "../../../../Mathematics/_4D/Matrix4D.h"
#include "../../../../Files/File.h"
#include "../../../../Files/OutFile.h"
#include "../../../../Files/InFile.h"
#include "../../../../Xml/XmlNode.h"
#include "../../../../Time/TimeEntry.h"

#define _RVL_MODEL_ELEMENT_CHANGED_ENABLED_EVENT_ID_ 0X01
#define _RVL_MODEL_ELEMENT_CHANGED_INDEXABLE_EVENT_ID_ 0X02
#define _RVL_MODEL_ELEMENT_CHANGED_VISIBILITY_EVENT_ID_ 0X04
#define _RVL_MODEL_ELEMENT_CHANGED_DATA_EVENT_ID_ 0X08
#define _RVL_MODEL_ELEMENT_CHANGED_SUPERIOR_EVENT_ID_ 0X10

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    class CModelElement : public Dependency::IDependencyManager
                    {
                    public:

                        enum TypeId
                        {
                            eVertex = 0, eOrientedVertex = 1, eEdge = 2, eFace = 3, eComposedFace = 4, eMesh = 5, eMultipleMesh = 6
                        };

                        enum Visibility
                        {
                            eStaticOccluded = 0, eStaticVisible = 1, eDynamicVisible = 2
                        };

                        static std::string TypeIdToCollectionString(const TypeId Id);
                        static std::string TypeIdToString(const TypeId Id);
                        static TypeId StringToTypeId(const std::string& TypeId);

                        static std::string VisibilityToString(const Visibility CurrentVisibility);
                        static Visibility StringToVisibility(const std::string& CurrentVisibility);

                        CModelElement(const TypeId Id, CModelElement* pSuperior, const bool AssignId);
                        ~CModelElement() override;

                        int GetInstanceId() const;

                        void SetLabel(const std::string& Label);
                        void ClearLabel();
                        const std::string& GetLabel() const;

                        std::string GetTypeName() const;
                        TypeId GetTypeId() const;
                        bool TypeEquals(const CModelElement* pModelPrimitive) const;

                        virtual bool IsEnabled(const bool ConsiderSuperior) const;
                        virtual void SetEnabled(const bool Enabled);

                        virtual bool IsIndexable(const bool ConsiderSuperior) const;
                        virtual void SetIndexable(const bool Indexable);

                        virtual void SetVisibility(const Visibility Visible);
                        virtual Visibility GetVisibility(const bool ConsiderSuperior) const;

                        bool SetSaliencyWeight(const float SaliencyWeight);
                        float GetSaliencyWeight() const;

                        const void* GetElementData() const;
                        void SetElementData(const void* pElementData);
                        void ClearElementData();

                        const CModelElement* GetSuperior() const;
                        CModelElement* GetSuperior();
                        virtual void SetSuperior(CModelElement* pSuperior);

                        virtual std::string ToString() const;
                        virtual bool SaveToFile(const std::string& FileName, const bool OverrideFlag) const;
                        virtual bool LoadFromFile(const std::string& FileName);

                        virtual void Clear() = 0;
                        virtual bool Deserialize(const Xml::CXmlNode* pXmlCurrentNode);
                        virtual Xml::CXmlNode* Serialize(Xml::CXmlNode* pXmlParentNode) const;
                        virtual Xml::CXmlNode* SerializeReference(Xml::CXmlNode* pXmlContainerNode) const;

                        static void ResetInstanceIdCounter();

                    protected:

                        virtual bool DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode);
                        virtual bool SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const;

                        static int s_InstanceIdCounter;
                        int m_InstanceId;
                        bool m_Enabled;
                        bool m_Indexable;
                        TypeId m_TypeId;
                        Visibility m_Visibility;
                        Real m_SaliencyWeight;
                        std::string m_Label;
                        const void* m_pElementData;
                        CModelElement* m_pSuperior;
                    };
                }
            }
        }
    }
}

