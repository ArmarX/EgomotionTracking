/*
 * ModelMesh.cpp
 */

#include "ModelMesh.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    int CModelTransformation::s_IdCounter = 0;

                    CModelMesh* CModelMesh::CreateFromXml(const Xml::CXmlNode* pXmlModelMesh, CModelMultipleMesh* pModelMultipleMesh)
                    {
                        if (!pXmlModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelMultipleMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        CModelMesh* pModelMesh = new CModelMesh(pModelMultipleMesh, false);

                        if (!pModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelMesh->Deserialize(pXmlModelMesh))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pModelMesh;
                            return nullptr;
                        }

                        if (!pModelMultipleMesh->AddMesh(pModelMesh))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pModelMesh;
                            return nullptr;
                        }

                        return pModelMesh;
                    }

                    CModelMesh::CModelMesh(CModelMultipleMesh* pModelMultipleMesh, const bool AssignId) :
                        CModelElement(CModelElement::eMesh, pModelMultipleMesh, AssignId),
                        m_pModelTransformation(nullptr),
                        m_StaticFrame(Mathematics::_4D::CMatrix4D::s_Identity),
                        m_DynamicFrame(Mathematics::_4D::CMatrix4D::s_Identity)
                    {
                        memset(m_Material, 0, sizeof(Real) * 4);
                    }

                    CModelMesh::~CModelMesh()
                    {
                        Clear();
                    }

                    bool CModelMesh::AddVertex(CModelVertex* pModelVertex)
                    {
                        if (!pModelVertex)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pModelVertex->GetSuperior() != this)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const int Id = pModelVertex->GetInstanceId();

                        if (Id < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_Vertices.find(Id) != m_Vertices.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!m_Vertices.insert(std::pair<int, CModelVertex*>(Id, pModelVertex)).second)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    bool CModelMesh::AddEdge(CModelEdge* pModelEdge)
                    {
                        if (!pModelEdge)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pModelEdge->GetSuperior() != this)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const int Id = pModelEdge->GetInstanceId();

                        if (Id < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_Edges.find(Id) != m_Edges.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!m_Edges.insert(std::pair<int, CModelEdge*>(Id, pModelEdge)).second)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    bool CModelMesh::AddFace(CModelFace* pModelFace)
                    {
                        if (!pModelFace)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pModelFace->GetSuperior() != this)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const int Id = pModelFace->GetInstanceId();

                        if (Id < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_Faces.find(Id) != m_Faces.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!m_Faces.insert(std::pair<int, CModelFace*>(Id, pModelFace)).second)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    bool CModelMesh::AddComposedFaces(CModelComposedFace* pModelComposedFace)
                    {
                        if (!pModelComposedFace)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pModelComposedFace->GetSuperior() != this)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const int Id = pModelComposedFace->GetInstanceId();

                        if (Id < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_ComposedFaces.find(Id) != m_ComposedFaces.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!m_ComposedFaces.insert(std::pair<int, CModelComposedFace*>(Id, pModelComposedFace)).second)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    CModelVertex* CModelMesh::GetVertexById(const int Id) const
                    {
                        if (Id < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        std::map<int, CModelVertex*>::const_iterator pLocation = m_Vertices.find(Id);
                        if (pLocation != m_Vertices.end())
                        {
                            return pLocation->second;
                        }
                        else
                        {
                            return nullptr;
                        }
                    }

                    const std::map<int, CModelVertex*>& CModelMesh::GetVertices() const
                    {
                        return m_Vertices;
                    }

                    CModelEdge* CModelMesh::GetEdgeById(const int Id) const
                    {
                        if (Id < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        std::map<int, CModelEdge*>::const_iterator pLocation = m_Edges.find(Id);
                        if (pLocation != m_Edges.end())
                        {
                            return pLocation->second;
                        }
                        else
                        {
                            return nullptr;
                        }
                    }

                    const std::map<int, CModelEdge*>& CModelMesh::GetEdges() const
                    {
                        return m_Edges;
                    }

                    CModelFace* CModelMesh::GetFaceById(const int Id)
                    {
                        if (Id < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        std::map<int, CModelFace*>::const_iterator pLocation = m_Faces.find(Id);
                        if (pLocation == m_Faces.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        return pLocation->second;
                    }

                    const std::map<int, CModelFace*>& CModelMesh::GetFaces() const
                    {
                        return m_Faces;
                    }

                    CModelComposedFace* CModelMesh::GetComposedFaceById(const int Id)
                    {
                        if (Id < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        std::map<int, CModelComposedFace*>::const_iterator pLocation = m_ComposedFaces.find(Id);
                        if (pLocation != m_ComposedFaces.end())
                        {
                            return pLocation->second;
                        }
                        else
                        {
                            return nullptr;
                        }
                    }

                    const std::map<int, CModelComposedFace*>& CModelMesh::GetComposedFaces() const
                    {
                        return m_ComposedFaces;
                    }

                    void CModelMesh::Transform(const Mathematics::_4D::CMatrix4D& T)
                    {
                        m_StaticFrame = T * m_StaticFrame;
                        UpdateTransformation(-2);
                        OnChange();
                    }

                    int CModelMesh::GetTotalVertices() const
                    {
                        return m_Vertices.size();
                    }

                    int CModelMesh::GetTotalVisibleVertices(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const
                    {
                        int TotalVisibleVertices = 0;
                        std::map<int, CModelVertex*>::const_iterator EndVertices = m_Vertices.end();
                        for (std::map<int, CModelVertex*>::const_iterator pKeyModelVertex = m_Vertices.begin() ; pKeyModelVertex != EndVertices ; ++pKeyModelVertex)
                            if (pKeyModelVertex->second->GetVisibility(ConsiderSuperior) == VisibilityState)
                            {
                                ++TotalVisibleVertices;
                            }
                        return TotalVisibleVertices;
                    }

                    int CModelMesh::GetTotalEnabledVertices(const bool ConsiderSuperior) const
                    {
                        int TotalEnabledVertices = 0;
                        std::map<int, CModelVertex*>::const_iterator EndVertices = m_Vertices.end();
                        for (std::map<int, CModelVertex*>::const_iterator pKeyModelVertex = m_Vertices.begin() ; pKeyModelVertex != EndVertices ; ++pKeyModelVertex)
                            if (pKeyModelVertex->second->IsEnabled(ConsiderSuperior))
                            {
                                ++TotalEnabledVertices;
                            }
                        return TotalEnabledVertices;
                    }

                    int CModelMesh::GetTotalIndexableVertices(const bool ConsiderSuperior) const
                    {
                        int TotalIndexableVertices = 0;
                        std::map<int, CModelVertex*>::const_iterator EndVertices = m_Vertices.end();
                        for (std::map<int, CModelVertex*>::const_iterator pKeyModelVertex = m_Vertices.begin() ; pKeyModelVertex != EndVertices; ++pKeyModelVertex)
                            if (pKeyModelVertex->second->IsIndexable(ConsiderSuperior))
                            {
                                ++TotalIndexableVertices;
                            }
                        return TotalIndexableVertices;
                    }

                    int CModelMesh::GetTotalEdges() const
                    {
                        return m_Edges.size();
                    }

                    int CModelMesh::GetTotalVisibleEdges(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const
                    {
                        int TotalVisibleEdges = 0;
                        std::map<int, CModelEdge*>::const_iterator EndEdges = m_Edges.end();
                        for (std::map<int, CModelEdge*>::const_iterator pKeyModelEdge = m_Edges.begin() ; pKeyModelEdge != EndEdges ; ++pKeyModelEdge)
                            if (pKeyModelEdge->second->GetVisibility(ConsiderSuperior) == VisibilityState)
                            {
                                ++TotalVisibleEdges;
                            }
                        return TotalVisibleEdges;
                    }

                    int CModelMesh::GetTotalEnabledEdges(const bool ConsiderSuperior) const
                    {
                        int TotalEnabledEdges = 0;
                        std::map<int, CModelEdge*>::const_iterator EndEdges = m_Edges.end();
                        for (std::map<int, CModelEdge*>::const_iterator pKeyModelEdge = m_Edges.begin() ; pKeyModelEdge != EndEdges ; ++pKeyModelEdge)
                            if (pKeyModelEdge->second->IsEnabled(ConsiderSuperior))
                            {
                                ++TotalEnabledEdges;
                            }
                        return TotalEnabledEdges;
                    }

                    int CModelMesh::GetTotalIndexableEdges(const bool ConsiderSuperior) const
                    {
                        int TotalIndexableEdges = 0;
                        std::map<int, CModelEdge*>::const_iterator EndEdges = m_Edges.end();
                        for (std::map<int, CModelEdge*>::const_iterator pKeyModelEdge = m_Edges.begin() ; pKeyModelEdge != EndEdges ; ++pKeyModelEdge)
                            if (pKeyModelEdge->second->IsIndexable(ConsiderSuperior))
                            {
                                ++TotalIndexableEdges;
                            }
                        return TotalIndexableEdges;
                    }

                    int CModelMesh::GetTotalFaces() const
                    {
                        return m_Faces.size();
                    }

                    int CModelMesh::GetTotalVisibleFaces(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const
                    {
                        int TotalVisibleFaces = 0;
                        std::map<int, CModelFace*>::const_iterator EndFaces =  m_Faces.end();
                        for (std::map<int, CModelFace*>::const_iterator pKeyModelFace = m_Faces.begin() ; pKeyModelFace != EndFaces ; ++pKeyModelFace)
                            if (pKeyModelFace->second->GetVisibility(ConsiderSuperior) == VisibilityState)
                            {
                                ++TotalVisibleFaces;
                            }
                        return TotalVisibleFaces;
                    }

                    int CModelMesh::GetTotalEnabledFaces(const bool ConsiderSuperior)
                    {
                        int TotalEnabledFaces = 0;
                        std::map<int, CModelFace*>::const_iterator EndFaces =  m_Faces.end();
                        for (std::map<int, CModelFace*>::iterator pKeyModelFace = m_Faces.begin() ; pKeyModelFace != EndFaces ; ++pKeyModelFace)
                            if (pKeyModelFace->second->IsEnabled(ConsiderSuperior))
                            {
                                ++TotalEnabledFaces;
                            }
                        return TotalEnabledFaces;
                    }

                    int CModelMesh::GetTotalIndexableFaces(const bool ConsiderSuperior) const
                    {
                        int TotalIndexableFaces = 0;
                        std::map<int, CModelFace*>::const_iterator EndFaces =  m_Faces.end();
                        for (std::map<int, CModelFace*>::const_iterator pKeyModelFace = m_Faces.begin() ; pKeyModelFace != EndFaces ; ++pKeyModelFace)
                            if (pKeyModelFace->second->IsIndexable(ConsiderSuperior))
                            {
                                ++TotalIndexableFaces;
                            }
                        return TotalIndexableFaces;
                    }

                    int CModelMesh::GetTotalMultiFaces() const
                    {
                        return m_ComposedFaces.size();
                    }

                    int CModelMesh::GetTotalVisibleMultiFaces(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const
                    {
                        int TotalVisibleMultiFaces = 0;
                        std::map<int, CModelComposedFace*>::const_iterator EndComposedFaces = m_ComposedFaces.end() ;
                        for (std::map<int, CModelComposedFace*>::const_iterator pKeyModelComposedFace = m_ComposedFaces.begin() ; pKeyModelComposedFace != EndComposedFaces ; ++pKeyModelComposedFace)
                            if (pKeyModelComposedFace->second->GetVisibility(ConsiderSuperior) == VisibilityState)
                            {
                                ++TotalVisibleMultiFaces;
                            }
                        return TotalVisibleMultiFaces;
                    }

                    int CModelMesh::GetTotalEnabledMultiFaces(const bool ConsiderSuperior) const
                    {
                        int TotalEnabledMultiFaces = 0;
                        std::map<int, CModelComposedFace*>::const_iterator EndComposedFaces = m_ComposedFaces.end() ;
                        for (std::map<int, CModelComposedFace*>::const_iterator pKeyModelComposedFace = m_ComposedFaces.begin() ; pKeyModelComposedFace != EndComposedFaces ; ++pKeyModelComposedFace)
                            if (pKeyModelComposedFace->second->IsEnabled(ConsiderSuperior))
                            {
                                ++TotalEnabledMultiFaces;
                            }
                        return TotalEnabledMultiFaces;
                    }

                    int CModelMesh::GetTotalIndexableMultiFaces(const bool ConsiderSuperior) const
                    {
                        int TotalIndexableMultiFaces = 0;
                        std::map<int, CModelComposedFace*>::const_iterator EndComposedFaces = m_ComposedFaces.end() ;
                        for (std::map<int, CModelComposedFace*>::const_iterator pKeyModelComposedFace = m_ComposedFaces.begin() ; pKeyModelComposedFace != EndComposedFaces ; ++pKeyModelComposedFace)
                            if (pKeyModelComposedFace->second->IsIndexable(ConsiderSuperior))
                            {
                                ++TotalIndexableMultiFaces;
                            }
                        return TotalIndexableMultiFaces;
                    }

                    const CModelMultipleMesh* CModelMesh::GetMultipleMesh() const
                    {
                        return dynamic_cast<const CModelMultipleMesh*>(CModelElement::GetSuperior());
                    }

                    CModelMultipleMesh* CModelMesh::GetMultipleMesh()
                    {
                        return dynamic_cast<CModelMultipleMesh*>(CModelElement::GetSuperior());
                    }

                    void CModelMesh::Clear()
                    {
                        m_pModelTransformation = nullptr;
                        m_StaticFrame.MakeIdentity();
                        m_DynamicFrame.MakeIdentity();
                        memset(m_Material, 0, sizeof(Real) * 4);
                        Clear(m_Vertices);
                        Clear(m_Edges);
                        Clear(m_Faces);
                        Clear(m_ComposedFaces);
                    }

                    bool CModelMesh::Deserialize(const Xml::CXmlNode* pXmlCurrentNode)
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::Deserialize(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pXmlCurrentNode->GetTotalSubNodes() < 5)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        std::list<Xml::CXmlNode*>::const_iterator ppXmlNode = pXmlCurrentNode->GetSubNodes().begin();

                        Xml::CXmlNode* pXmlVertices = *ppXmlNode++;

                        if (pXmlVertices->GetName() != _RVL_TAG_VERTEX_COLLECTION_)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Xml::CXmlNode* pXmlEdges = *ppXmlNode++;

                        if (pXmlEdges->GetName() != _RVL_TAG_EDGE_COLLECTION_)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Xml::CXmlNode* pXmlFaces = *ppXmlNode++;

                        if (pXmlFaces->GetName() != _RVL_TAG_FACE_COLLECTION_)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Xml::CXmlNode* pXmlComposedFaces = *ppXmlNode++;

                        if (pXmlComposedFaces->GetName() != _RVL_TAG_COMPOSED_FACE_COLLECTION_)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Xml::CXmlNode* pXmlFrame = *ppXmlNode;

                        if (pXmlFrame->GetName() != _RVL_TAG_FRAME_)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        std::list<Xml::CXmlNode*>::const_iterator XmlSubNodesEnd = pXmlVertices->GetSubNodes().end();
                        for (std::list<Xml::CXmlNode*>::const_iterator ppXmlModelVertex = pXmlVertices->GetSubNodes().begin() ; ppXmlModelVertex != XmlSubNodesEnd ; ++ppXmlModelVertex)
                            if (!CModelVertex::CreateFromXml(*ppXmlModelVertex, this))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                        if (!m_Vertices.size())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        XmlSubNodesEnd = pXmlEdges->GetSubNodes().end();
                        for (std::list<Xml::CXmlNode*>::const_iterator ppXmlModelEdge = pXmlEdges->GetSubNodes().begin() ; ppXmlModelEdge != XmlSubNodesEnd ; ++ppXmlModelEdge)
                            if (!CModelEdge::CreateFromXml(*ppXmlModelEdge, this))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                        if (!m_Edges.size())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        XmlSubNodesEnd = pXmlFaces->GetSubNodes().end();
                        for (std::list<Xml::CXmlNode*>::const_iterator ppXmlModelFace = pXmlFaces->GetSubNodes().begin() ; ppXmlModelFace != XmlSubNodesEnd ; ++ppXmlModelFace)
                            if (!CModelFace::CreateFromXml(*ppXmlModelFace, this))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                        if (!m_Faces.size())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        std::map<int, CModelEdge*>::iterator EndEdges = m_Edges.end();
                        for (std::map<int, CModelEdge*>::iterator ppKeyEdge = m_Edges.begin(); ppKeyEdge != EndEdges; ++ppKeyEdge)
                            if (!ppKeyEdge->second->SolveIndirectReferences())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                        XmlSubNodesEnd = pXmlComposedFaces->GetSubNodes().end();
                        for (std::list<Xml::CXmlNode*>::const_iterator ppXmlModelComposedFace = pXmlComposedFaces->GetSubNodes().begin() ; ppXmlModelComposedFace != XmlSubNodesEnd ; ++ppXmlModelComposedFace)
                            if (!CModelComposedFace::CreateFromXml(*ppXmlModelComposedFace, this))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                        std::ostringstream Stream;
                        Real Element = Real(0);
                        for (int r = 0 ; r < 4 ; ++r)
                            for (int c = 0 ; c < 4 ; ++c)
                            {
                                Stream << _RVL_TAG_FRAME_COMPONENT_ << r << c;
                                if (!pXmlFrame->GetAttribute(Stream.str(), Element))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                                m_StaticFrame[r][c] = Element;
                                Stream.str("");
                            }

                        return true;
                    }

                    Xml::CXmlNode* CModelMesh::Serialize(Xml::CXmlNode* pXmlParentNode) const
                    {
                        if (!pXmlParentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        Xml::CXmlNode* pXmlCurrentNode = CModelElement::Serialize(pXmlParentNode);

                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        ///---------------------------------------------------------------------------------------
                        /// Vertices
                        ///---------------------------------------------------------------------------------------

                        Xml::CXmlNode* pXmlVertices = pXmlCurrentNode->CreateSubNode(_RVL_TAG_VERTEX_COLLECTION_);

                        if (!pXmlVertices)
                        {
                            pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        std::map<int, CModelVertex*>::const_iterator VerticesEnd = m_Vertices.end();
                        for (std::map<int, CModelVertex*>::const_iterator ppModelElement = m_Vertices.begin() ; ppModelElement != VerticesEnd ; ++ppModelElement)
                            if (!ppModelElement->second->Serialize(pXmlVertices))
                            {
                                pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return nullptr;
                            }

                        ///---------------------------------------------------------------------------------------
                        /// Edges
                        ///---------------------------------------------------------------------------------------

                        Xml::CXmlNode* pXmlEdges = pXmlCurrentNode->CreateSubNode(_RVL_TAG_EDGE_COLLECTION_);

                        if (!pXmlEdges)
                        {
                            pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        std::map<int, CModelEdge*>::const_iterator EdgesEnd = m_Edges.end();
                        for (std::map<int, CModelEdge*>::const_iterator ppModelElement = m_Edges.begin() ; ppModelElement != EdgesEnd ; ++ppModelElement)
                            if (!ppModelElement->second->Serialize(pXmlEdges))
                            {
                                pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return nullptr;
                            }

                        ///---------------------------------------------------------------------------------------
                        /// Faces
                        ///---------------------------------------------------------------------------------------

                        Xml::CXmlNode* pXmlFaces = pXmlCurrentNode->CreateSubNode(_RVL_TAG_FACE_COLLECTION_);

                        if (!pXmlFaces)
                        {
                            pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        std::map<int, CModelFace*>::const_iterator FacesEnd = m_Faces.end();
                        for (std::map<int, CModelFace*>::const_iterator ppModelElement = m_Faces.begin() ; ppModelElement != FacesEnd ; ++ppModelElement)
                            if (!ppModelElement->second->Serialize(pXmlFaces))
                            {
                                pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return nullptr;
                            }

                        ///---------------------------------------------------------------------------------------
                        /// Composed Faces
                        ///---------------------------------------------------------------------------------------

                        Xml::CXmlNode* pXmlComposedFaces = pXmlCurrentNode->CreateSubNode(_RVL_TAG_COMPOSED_FACE_COLLECTION_);

                        if (!pXmlComposedFaces)
                        {
                            pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        std::map<int, CModelComposedFace*>::const_iterator ComposedFacesEnd = m_ComposedFaces.end();
                        for (std::map<int, CModelComposedFace*>::const_iterator ppModelElement = m_ComposedFaces.begin() ; ppModelElement != ComposedFacesEnd ; ++ppModelElement)
                            if (!ppModelElement->second->Serialize(pXmlComposedFaces))
                            {
                                pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return nullptr;
                            }

                        ///---------------------------------------------------------------------------------------
                        /// Frame
                        ///---------------------------------------------------------------------------------------


                        Xml::CXmlNode* pXmlFrame = pXmlCurrentNode->CreateSubNode(_RVL_TAG_FRAME_);

                        if (!pXmlFrame)
                        {
                            pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        std::ostringstream Stream;
                        for (int r = 0 ; r < 4 ; ++r)
                            for (int c = 0 ; c < 4 ; ++c)
                            {
                                Stream << _RVL_TAG_FRAME_COMPONENT_ << r << c;
                                if (!pXmlFrame->SetAttribute(Stream.str(), m_StaticFrame[r][c]))
                                {
                                    pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return nullptr;
                                }
                                Stream.str("");
                            }

                        ///---------------------------------------------------------------------------------------
                        /// Transformations
                        ///---------------------------------------------------------------------------------------


                        return pXmlCurrentNode;
                    }

                    void CModelMesh::SetReferenceFrame(const Mathematics::_4D::CMatrix4D& Frame)
                    {
                        m_StaticFrame = Frame;
                    }

                    void CModelMesh::SetDynamicFrame(const Mathematics::_4D::CMatrix4D& Frame)
                    {
                        m_DynamicFrame = Frame;
                    }

                    const Mathematics::_4D::CMatrix4D& CModelMesh::GetFrame() const
                    {
                        return m_pModelTransformation ? m_DynamicFrame : m_StaticFrame;
                    }

                    bool CModelMesh::SetFrameTransformation(const Mathematics::_3D::CMatrix3D& Rotation, const Mathematics::_3D::CVector3D& Translation)
                    {
                        if (Rotation.IsOrthonormal() && Translation.IsNotAtInfinity())
                        {
                            return m_StaticFrame.SetTransformation(Rotation, Translation, Real(1));
                        }
                        return false;
                    }

                    Containers::_3D::CContinuousBoundingBox3D CModelMesh::GetBoundingBox() const
                    {
                        Containers::_3D::CContinuousBoundingBox3D BoundingBox;
                        std::map<int, CModelVertex*>::const_iterator EndVertices = m_Vertices.end();
                        for (std::map<int, CModelVertex*>::const_iterator pKeyVertex = m_Vertices.begin() ; pKeyVertex != EndVertices ; ++pKeyVertex)
                        {
                            BoundingBox.Extend(pKeyVertex->second->GetPoint(true));
                        }
                        return BoundingBox;
                    }

                    bool CModelMesh::SetMaterial(const Real* pMaterial)
                    {
                        if (pMaterial && (pMaterial[0] > Real(0)) && (pMaterial[1] > Real(0)) && (pMaterial[2] > Real(0)) && (pMaterial[3] > Real(0)) && (pMaterial[0] <= Real(1)) && (pMaterial[1] <= Real(1)) && (pMaterial[2] <= Real(1)) && (pMaterial[3] <= Real(1)))
                        {
                            memcpy(m_Material, pMaterial, sizeof(Real) * 4);
                            return true;
                        }
                        return false;
                    }

                    bool CModelMesh::SetMaterial(const Real R, const Real G, const Real B, const Real A)
                    {
                        if ((R >= Real(0)) && (G >= Real(0)) && (B >= Real(0)) && (A >= Real(0)) && (R <= Real(1)) && (G <= Real(1)) && (B <= Real(1)) && (A <= Real(1)))
                        {
                            m_Material[0] = R;
                            m_Material[1] = G;
                            m_Material[2] = B;
                            m_Material[3] = A;
                            return true;
                        }
                        return false;
                    }

                    void CModelMesh::GetMaterial(Real& R, Real& G, Real& B, Real& A) const
                    {
                        R = m_Material[0];
                        G = m_Material[1];
                        B = m_Material[2];
                        A = m_Material[3];
                    }

                    void CModelMesh::GetDiscreteMaterial(Byte& R, Byte& G, Byte& B, Byte& A) const
                    {
                        R = Byte(m_Material[0] * Real(255.0) + Real(0.5));
                        G = Byte(m_Material[1] * Real(255.0) + Real(0.5));
                        B = Byte(m_Material[2] * Real(255.0) + Real(0.5));
                        A = Byte(m_Material[3] * Real(255.0) + Real(0.5));
                    }

                    bool CModelMesh::IsDynamic() const
                    {
                        return m_pModelTransformation;
                    }

                    bool CModelMesh::SetTransformation(CModelTransformation* pModelTransformation)
                    {
                        if (!pModelTransformation)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pModelTransformation->IsValid())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_pModelTransformation != pModelTransformation)
                        {
                            m_pModelTransformation = pModelTransformation;
                            if (!m_pModelTransformation->AddMesh(this))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!UpdateTransformation(-1))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }
                        }

                        return true;
                    }

                    bool CModelMesh::HasTransformation() const
                    {
                        return m_pModelTransformation;
                    }

                    CModelTransformation* CModelMesh::GetTransformation()
                    {
                        return m_pModelTransformation;
                    }

                    bool CModelMesh::RemoveTransformation()
                    {
                        if (m_pModelTransformation)
                        {
                            m_pModelTransformation->RemoveMesh(this);
                            m_pModelTransformation = nullptr;
                            m_DynamicFrame = m_StaticFrame;
                            OnChange();
                            return true;
                        }
                        return false;
                    }

                    bool CModelMesh::UpdateTransformation(const int Id)
                    {
                        if (m_pModelTransformation)
                        {
                            switch (m_pModelTransformation->GetTransformationType())
                            {
                                case CModelTransformation::eTranslation:
                                {
                                    const CModelTranslation* pModelTranslation = dynamic_cast<CModelTranslation*>(m_pModelTransformation);
                                    const Mathematics::_3D::CVector3D& Translation = pModelTranslation->GetTranslation(Id < 0 ? -1 : Id);
                                    bool Success = false;
                                    if (Id == -1)
                                    {
                                        m_DynamicFrame = m_StaticFrame;
                                        m_DynamicFrame.AddTranslation(Translation);
                                        Success = true;
                                    }
                                    else if (Id == -2)
                                    {
                                        m_DynamicFrame = m_StaticFrame;
                                        m_DynamicFrame.AddTranslation(Translation);
                                        for (auto& m_IdentifiableDynamicFrame : m_IdentifiableDynamicFrames)
                                        {
                                            m_IdentifiableDynamicFrame.second = m_StaticFrame;
                                            m_IdentifiableDynamicFrame.second.AddTranslation(Translation);
                                        }
                                        Success = true;
                                    }
                                    else
                                    {
                                        std::map<int, Mathematics::_4D::CMatrix4D>::iterator pIdFrame =  m_IdentifiableDynamicFrames.find(Id);
                                        if (pIdFrame != m_IdentifiableDynamicFrames.end())
                                        {
                                            pIdFrame->second = m_StaticFrame;
                                            pIdFrame->second.AddTranslation(Translation);
                                            Success = true;
                                        }
                                        else
                                        {
                                            std::ostringstream TextStream;
                                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                        }
                                    }
                                    if (Success)
                                    {
                                        OnChange();
                                    }

                                    return Success;
                                }
                                break;

                                case CModelTransformation::eRotation:
                                {
                                    const CModelRotation* pModelRotation = dynamic_cast<CModelRotation*>(m_pModelTransformation);

                                    Mathematics::_3D::CVector3D PointA;
                                    Mathematics::_3D::CMatrix3D RotationMatrix;
                                    bool Success = false;

                                    if (!pModelRotation->GetRotation(Id < 0 ? -1 : Id, PointA, RotationMatrix))
                                    {
                                        std::ostringstream TextStream;
                                        TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                        _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                        return Success;
                                    }

                                    if (Id == -1)
                                    {
                                        Success = CModelRotation::RotateFrame(PointA, RotationMatrix, m_StaticFrame, m_DynamicFrame);
                                    }
                                    else if (Id == -2)
                                    {
                                        Success = CModelRotation::RotateFrame(PointA, RotationMatrix, m_StaticFrame, m_DynamicFrame);
                                        for (auto& m_IdentifiableDynamicFrame : m_IdentifiableDynamicFrames)
                                        {
                                            Success &= CModelRotation::RotateFrame(PointA, RotationMatrix, m_StaticFrame, m_IdentifiableDynamicFrame.second);
                                        }
                                    }
                                    else
                                    {
                                        std::map<int, Mathematics::_4D::CMatrix4D>::iterator pIdFrame =  m_IdentifiableDynamicFrames.find(Id);
                                        if (pIdFrame != m_IdentifiableDynamicFrames.end())
                                        {
                                            Success = CModelRotation::RotateFrame(PointA, RotationMatrix, m_StaticFrame, pIdFrame->second);
                                        }
                                        else
                                        {
                                            std::ostringstream TextStream;
                                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                        }
                                    }
                                    if (Success)
                                    {
                                        OnChange();
                                    }

                                    return Success;
                                }
                                break;

                                case CModelTransformation::eFrameToFrameRigid:
                                    break;

                                case CModelTransformation::eGeneralRigid:
                                    break;

                            }
                            return true;
                        }
                        return false;
                    }

                    bool CModelMesh::HasIdentifiableDynamicFrame(const int Id) const
                    {
                        return m_IdentifiableDynamicFrames.find(Id) != m_IdentifiableDynamicFrames.end();
                    }

                    bool CModelMesh::CreateIdentifiableDynamicFrame(const int Id)
                    {
                        if (m_IdentifiableDynamicFrames.find(Id) != m_IdentifiableDynamicFrames.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Mathematics::_4D::CMatrix4D Frame = m_StaticFrame;

                        if (!m_IdentifiableDynamicFrames.insert(std::pair<int, Mathematics::_4D::CMatrix4D>(Id, Frame)).second)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    bool CModelMesh::SetIdentifiableDynamicFrame(const int Id, const Mathematics::_4D::CMatrix4D& Frame)
                    {
                        if (Id < 0)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_IdentifiableDynamicFrames.find(Id) == m_IdentifiableDynamicFrames.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        m_IdentifiableDynamicFrames[Id] = Frame;

                        return true;
                    }

                    void CModelMesh::ClearIdentifiableDynamicFrame()
                    {
                        m_IdentifiableDynamicFrames.clear();
                    }

                    CModelMesh::CModelMesh(const bool AssignId) :
                        CModelElement(CModelElement::eMesh, nullptr, AssignId),
                        m_pModelTransformation(nullptr),
                        m_StaticFrame(Mathematics::_4D::CMatrix4D::s_Identity),
                        m_DynamicFrame(Mathematics::_4D::CMatrix4D::s_Identity)
                    {
                        memset(m_Material, 0, sizeof(Real) * 4);
                    }

                    bool CModelMesh::DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode)
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::DeserializeAttributes(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real R = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_MATERIAL_COMPONENT_R_, R))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real G = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_MATERIAL_COMPONENT_G_, G))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real B = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_MATERIAL_COMPONENT_B_, B))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real A = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_MATERIAL_COMPONENT_A_, A))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        bool IsDynamic = false;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_DYNAMIC_, IsDynamic))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        m_Material[0] = R;
                        m_Material[1] = G;
                        m_Material[2] = B;
                        m_Material[3] = A;

                        return true;
                    }

                    bool CModelMesh::SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::SerializeAttributes(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_MATERIAL_COMPONENT_R_, m_Material[0]))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_MATERIAL_COMPONENT_G_, m_Material[1]))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_MATERIAL_COMPONENT_B_, m_Material[2]))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_MATERIAL_COMPONENT_A_, m_Material[3]))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_DYNAMIC_, bool(m_pModelTransformation)))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_pModelTransformation)
                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_MODEL_TRANSFORMATION_INSTANCE_ID_, m_pModelTransformation->GetInstanceId()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                        return true;
                    }

                    CModelTransformation* CModelTransformation::CreateFromXML(const Xml::CXmlNode* pXmlModelTransformation, CModelMultipleMesh* pModelMultipleMesh)
                    {
                        if (!pXmlModelTransformation)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelMultipleMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        std::string TypeId;

                        if (!pXmlModelTransformation->GetAttribute(_RVL_TAG_TYPE_ID_, TypeId))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        const CModelTransformation::TransformationType Type = CModelTransformation::StringToTransformationType(TypeId);

                        if ((Type < eTranslation) || (Type > eGeneralRigid))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }


                        switch (Type)
                        {
                            case eTranslation:
                                return CModelTranslation::CreateFromXML(pXmlModelTransformation, pModelMultipleMesh);
                            case eRotation:
                                return CModelRotation::CreateFromXML(pXmlModelTransformation, pModelMultipleMesh);
                                break;
                            case eFrameToFrameRigid:
                                break;
                            case eGeneralRigid:
                                break;
                        }

                        return nullptr;

                    }
                }
            }
        }
    }
}
