/*
 * ModelMultipleMesh.h
 */

#pragma once

#include "ModelElement.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    class CModelMesh;
                    class CModelTransformation;

                    class CModelMultipleMesh : public CModelElement
                    {
                    public:

                        static bool CreateTransformation(const Mathematics::_3D::CVector3D& Origin, const Mathematics::_3D::CVector3D& A, const Mathematics::_3D::CVector3D& B, const Mathematics::_3D::CVector3D& C, Mathematics::_4D::CMatrix4D& Transformation);

                        static CModelMultipleMesh* CreateFromXml(const Xml::CXmlNode* pXmlModelMultipleMesh);

                        CModelMultipleMesh(const bool AssignId);
                        ~CModelMultipleMesh() override;

                        bool AddMesh(CModelMesh* pModelMesh);
                        const std::map<int, CModelMesh*>& GetMeshes() const;
                        CModelMesh* GetMeshById(const int Id);

                        bool AddTransformation(CModelTransformation* pModelTransformation);
                        bool DeleteTransformation(CModelTransformation*& pModelTransformation);
                        bool HasTransformations() const;
                        const std::map<int, CModelTransformation*>& GetTransformations() const;
                        CModelTransformation* GetTransformationById(const int Id) const;

                        void Transform(const Mathematics::_4D::CMatrix4D& T);

                        int GetTotalVertices() const;
                        int GetTotalVisibleVertices(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const;
                        int GetTotalEnabledVertices(const bool ConsiderSuperior) const;
                        int GetTotalIndexableVertices(const bool ConsiderSuperior) const;

                        int GetTotalEdges() const;
                        int GetTotalVisibleEdges(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const;
                        int GetTotalEnabledEdges(const bool ConsiderSuperior) const;
                        int GetTotalIndexableEdges(const bool ConsiderSuperior) const;

                        int GetTotalFaces() const;
                        int GetTotalVisibleFaces(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const;
                        int GetTotalEnabledFaces(const bool ConsiderSuperior) const;
                        int GetTotalIndexableFaces(const bool ConsiderSuperior) const;

                        int GetTotalMultiFaces() const;
                        int GetTotalVisibleMultiFaces(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const;
                        int GetTotalEnabledMultiFaces(const bool ConsiderSuperior) const;
                        int GetTotalIndexableMultiFaces(const bool ConsiderSuperior) const;

                        void Clear() override;
                        bool Deserialize(const Xml::CXmlNode* pXmlCurrentNode) override;
                        Xml::CXmlNode* Serialize(Xml::CXmlNode* pXmlParentNode) const override;
                        virtual Containers::_3D::CContinuousBoundingBox3D GetBoundingBox() const;

                        bool DeleteMesh(CModelMesh*& pMesh);

                    protected:

                        std::map<int, CModelMesh*> m_Meshes;
                        std::map<int, CModelTransformation*> m_Transformations;
                    };
                }
            }
        }
    }
}

