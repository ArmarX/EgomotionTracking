/*
 * ModelFace.h
 */

#pragma once

#include "ModelElement.h"
#include "ModelOrientedVertex.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    class CModelVertex;
                    class CModelOrientedVertex;
                    class CModelEdge;
                    class CModelComposedFace;
                    class CModelMesh;

                    class CModelFace : public CModelElement
                    {
                    public:

                        static CModelFace* CreateFromXml(const Xml::CXmlNode* pXmlModelFace, CModelMesh* pModelMesh);

                        CModelFace(CModelVertex* pModelVertexA, const Mathematics::_3D::CVector3D& NormalA, CModelVertex* pModelVertexB, const Mathematics::_3D::CVector3D& NormalB, CModelVertex* pModelVertexC, const Mathematics::_3D::CVector3D& NormalC, CModelMesh* pModelMesh, const bool AssignId);
                        ~CModelFace() override;

                        void SetSuperior(CModelElement* pSuperior) override;
                        void SetComposedFace(CModelComposedFace* pComposedFace);
                        bool HasComposedFace() const;
                        const CModelComposedFace* GetReadOnlyComposedFace() const;
                        Mathematics::_3D::CVector3D GetNormal(const bool ConsiderSuperior) const;
                        Mathematics::_3D::CVector3D GetCentroid(const bool ConsiderSuperior) const;
                        Real GetArea() const;
                        Real GetPerimeter() const;
                        std::vector<CModelOrientedVertex*> GetOrientedVertices();
                        std::vector<Mathematics::_3D::CVector3D> GetVertexPoints(const bool ConsiderSuperior) const;
                        std::vector<CModelEdge*> GetEdges() const;
                        void Clear() override;
                        bool Deserialize(const Xml::CXmlNode* pXmlCurrentNode) override;
                        Xml::CXmlNode* Serialize(Xml::CXmlNode* pXmlParentNode) const override;
                        CModelMesh* GetMesh();
                        virtual Containers::_3D::CContinuousBoundingBox3D GetBoundingBox(const bool ConsiderSuperior) const;
                        CModelOrientedVertex* GetWritableOrientedVertexA();
                        CModelOrientedVertex* GetWritableOrientedVertexB();
                        CModelOrientedVertex* GetWritableOrientedVertexC();
                        const CModelOrientedVertex* GetReadOnlyOrientedVertexA() const;
                        const CModelOrientedVertex* GetReadOnlyOrientedVertexB() const;
                        const CModelOrientedVertex* GetReadOnlyOrientedVertexC() const;

                        void GetDynamicEndPoints(Mathematics::_3D::CVector3D& PointA, Mathematics::_3D::CVector3D& PointB, Mathematics::_3D::CVector3D& PointC) const;


                    protected:

                        friend class CModelMesh;

                        CModelFace(CModelMesh* pModelMesh, const bool AssignId);
                        bool DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode) override;
                        bool SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const override;
                        bool ComputeTriangleNormalBySortedDirections(const Mathematics::_3D::CVector3D& DirectionA, const Mathematics::_3D::CVector3D& DirectionB, const Mathematics::_3D::CVector3D& NormalA, const Mathematics::_3D::CVector3D& NormalB, const Mathematics::_3D::CVector3D& NormalC, Mathematics::_3D::CVector3D& Normal);
                        Real ComputeTriangleAreaBySides(const Real A, const Real B, const Real C);
                        Mathematics::_3D::CVector3D ComputeTriangleCentroidByVertices(const Mathematics::_3D::CVector3D& Normal, const Mathematics::_3D::CVector3D& A, const Mathematics::_3D::CVector3D& B, const Mathematics::_3D::CVector3D& C);

                        CModelOrientedVertex m_OrientedVertexA;
                        CModelOrientedVertex m_OrientedVertexB;
                        CModelOrientedVertex m_OrientedVertexC;
                        CModelEdge* m_pEdgeAB;
                        CModelEdge* m_pEdgeBC;
                        CModelEdge* m_pEdgeCA;
                        CModelComposedFace* m_pComposedFace;
                        Mathematics::_3D::CVector3D m_Normal;
                        Real m_Area;
                        Real m_Perimeter;
                    };
                }
            }
        }
    }
}

