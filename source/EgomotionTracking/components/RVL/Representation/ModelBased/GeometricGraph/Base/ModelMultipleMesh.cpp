/*
 * ModelMultipleMesh.cpp
 */

#include "ModelMultipleMesh.h"
#include "ModelMesh.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    bool CModelMultipleMesh::CreateTransformation(const Mathematics::_3D::CVector3D& Origin, const Mathematics::_3D::CVector3D& A, const Mathematics::_3D::CVector3D& B, const Mathematics::_3D::CVector3D& C, Mathematics::_4D::CMatrix4D& Transformation)
                    {
                        Mathematics::_3D::CVector3D DeltaA = A - Origin;
                        if (DeltaA.Normalize() < g_RealPlusEpsilon)
                        {
                            return false;
                        }

                        Mathematics::_3D::CVector3D DeltaB = B - Origin;
                        if (DeltaB.Normalize() < g_RealPlusEpsilon)
                        {
                            return false;
                        }

                        Mathematics::_3D::CVector3D DeltaC = C - Origin;
                        if (DeltaC.Normalize() < g_RealPlusEpsilon)
                        {
                            return false;
                        }

                        const Real ProjectionAB = DeltaA.ScalarProduct(DeltaB);

                        if (IsOne(std::abs(ProjectionAB)))
                        {
                            return false;
                        }

                        DeltaB -= DeltaA * ProjectionAB;

                        if (DeltaB.Normalize() < g_RealPlusEpsilon)
                        {
                            return false;
                        }

                        Mathematics::_3D::CVector3D OrthoDeltaC = DeltaA.VectorProduct(DeltaB);
                        OrthoDeltaC.Normalize();

                        if (OrthoDeltaC.ScalarProduct(DeltaC) < Real(0))
                        {
                            return false;
                        }

                        Transformation[0][0] = DeltaA.GetX();
                        Transformation[1][0] = DeltaA.GetY();
                        Transformation[2][0] = DeltaA.GetZ();
                        Transformation[3][0] = 0.0f;

                        Transformation[0][1] = DeltaB.GetX();
                        Transformation[1][1] = DeltaB.GetY();
                        Transformation[2][1] = DeltaB.GetZ();
                        Transformation[3][1] = 0.0f;

                        Transformation[0][2] = DeltaC.GetX();
                        Transformation[1][2] = DeltaC.GetY();
                        Transformation[2][2] = DeltaC.GetZ();
                        Transformation[3][2] = 0.0f;

                        Transformation[0][3] = Origin.GetX();
                        Transformation[1][3] = Origin.GetY();
                        Transformation[2][3] = Origin.GetZ();
                        Transformation[3][3] = 1.0f;

                        return true;
                    }

                    CModelMultipleMesh* CModelMultipleMesh::CreateFromXml(const Xml::CXmlNode* pXmlModelMultipleMesh)
                    {
                        if (!pXmlModelMultipleMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        CModelMultipleMesh* pModelMultipleMesh = new CModelMultipleMesh(false);

                        if (!pModelMultipleMesh->Deserialize(pXmlModelMultipleMesh))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pModelMultipleMesh;
                            return nullptr;
                        }

                        return pModelMultipleMesh;
                    }

                    CModelMultipleMesh::CModelMultipleMesh(const bool AssignId) :
                        CModelElement(CModelElement::eMultipleMesh, nullptr, AssignId)
                    {
                    }

                    CModelMultipleMesh::~CModelMultipleMesh()
                    {
                        Clear();
                    }

                    bool CModelMultipleMesh::AddMesh(CModelMesh* pModelMesh)
                    {
                        if (!pModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pModelMesh->GetSuperior() != this)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const int Id = pModelMesh->GetInstanceId();

                        if (Id < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_Meshes.find(Id) != m_Meshes.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!m_Meshes.insert(std::pair<int, CModelMesh*>(pModelMesh->GetInstanceId(), pModelMesh)).second)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    const std::map<int, CModelMesh*>& CModelMultipleMesh::GetMeshes() const
                    {
                        return m_Meshes;
                    }

                    CModelMesh* CModelMultipleMesh::GetMeshById(const int Id)
                    {
                        if (Id < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!m_Meshes.size())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        std::map<int, CModelMesh*>::iterator pKeyMesh = m_Meshes.find(Id);

                        if (pKeyMesh == m_Meshes.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        return pKeyMesh->second;
                    }

                    bool CModelMultipleMesh::AddTransformation(CModelTransformation* pModelTransformation)
                    {
                        if (!pModelTransformation)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pModelTransformation->IsValid())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_Transformations.find(pModelTransformation->GetInstanceId()) != m_Transformations.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!m_Transformations.insert(std::pair<int, CModelTransformation*>(pModelTransformation->GetInstanceId(), pModelTransformation)).second)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;

                    }

                    bool CModelMultipleMesh::DeleteTransformation(CModelTransformation*& pModelTransformation)
                    {
                        if (!pModelTransformation)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        std::map<int, CModelTransformation*>::iterator pKeyTransformation = m_Transformations.find(pModelTransformation->GetInstanceId());

                        if (pKeyTransformation == m_Transformations.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        m_Transformations.erase(pKeyTransformation);
                        delete pModelTransformation;
                        pModelTransformation = nullptr;

                        return true;
                    }

                    bool CModelMultipleMesh::HasTransformations() const
                    {
                        return m_Transformations.size();
                    }

                    const std::map<int, CModelTransformation*>& CModelMultipleMesh::GetTransformations() const
                    {
                        return m_Transformations;
                    }

                    CModelTransformation* CModelMultipleMesh::GetTransformationById(const int Id) const
                    {
                        if (!m_Transformations.size())
                        {
                            return nullptr;
                        }

                        if (Id < 1)
                        {
                            return nullptr;
                        }

                        std::map<int, CModelTransformation*>::const_iterator pKeyTransformation = m_Transformations.find(Id);

                        if (pKeyTransformation == m_Transformations.end())
                        {
                            return nullptr;
                        }

                        return pKeyTransformation->second;
                    }

                    void CModelMultipleMesh::Transform(const Mathematics::_4D::CMatrix4D& T)
                    {
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            pKeyModelMesh->second->Transform(T);
                        }
                    }

                    int CModelMultipleMesh::GetTotalVertices() const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalVertices();
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalVisibleVertices(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalVisibleVertices(VisibilityState, ConsiderSuperior);
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalEnabledVertices(const bool ConsiderSuperior) const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalEnabledVertices(ConsiderSuperior);
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalIndexableVertices(const bool ConsiderSuperior) const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalIndexableVertices(ConsiderSuperior);
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalEdges() const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalEdges();
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalVisibleEdges(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalVisibleEdges(VisibilityState, ConsiderSuperior);
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalEnabledEdges(const bool ConsiderSuperior) const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalEnabledEdges(ConsiderSuperior);
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalIndexableEdges(const bool ConsiderSuperior) const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalIndexableEdges(ConsiderSuperior);
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalFaces() const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalFaces();
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalVisibleFaces(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalVisibleFaces(VisibilityState, ConsiderSuperior);
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalEnabledFaces(const bool ConsiderSuperior) const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalEnabledFaces(ConsiderSuperior);
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalIndexableFaces(const bool ConsiderSuperior) const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalIndexableFaces(ConsiderSuperior);
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalMultiFaces() const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalMultiFaces();
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalVisibleMultiFaces(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalVisibleMultiFaces(VisibilityState, ConsiderSuperior);
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalEnabledMultiFaces(const bool ConsiderSuperior) const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalEnabledMultiFaces(ConsiderSuperior);
                        }
                        return Total;
                    }

                    int CModelMultipleMesh::GetTotalIndexableMultiFaces(const bool ConsiderSuperior) const
                    {
                        int Total = 0;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            Total += pKeyModelMesh->second->GetTotalIndexableMultiFaces(ConsiderSuperior);
                        }
                        return Total;
                    }

                    void CModelMultipleMesh::Clear()
                    {
                        if (m_Meshes.size())
                        {
                            std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                            for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                            {
                                delete pKeyModelMesh->second;
                            }
                            m_Meshes.clear();
                        }

                        if (m_Transformations.size())
                        {
                            std::map<int, CModelTransformation*>::const_iterator EndTransformations = m_Transformations.end();
                            for (std::map<int, CModelTransformation*>::const_iterator pKeyTransformation = m_Transformations.begin(); pKeyTransformation != EndTransformations; ++pKeyTransformation)
                            {
                                delete pKeyTransformation->second;
                            }
                            m_Transformations.clear();
                        }
                    }

                    bool CModelMultipleMesh::Deserialize(const Xml::CXmlNode* pXmlCurrentNode)
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::Deserialize(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pXmlCurrentNode->GetTotalSubNodes() != 2)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const std::list<Xml::CXmlNode*>& SubNodes = pXmlCurrentNode->GetSubNodes();

                        std::list<Xml::CXmlNode*>::const_iterator pSubNode = SubNodes.begin();


                        Xml::CXmlNode* pXmlMeshes = *pSubNode++;

                        if (pXmlMeshes->GetName() != _RVL_TAG_MESH_COLLECTION_)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const std::list<Xml::CXmlNode*>& Meshes = pXmlMeshes->GetSubNodes();

                        std::list<Xml::CXmlNode*>::const_iterator EndMeshes = Meshes.end();
                        for (std::list<Xml::CXmlNode*>::const_iterator ppXmlMesh = Meshes.begin() ; ppXmlMesh != EndMeshes ; ++ppXmlMesh)
                            if (!CModelMesh::CreateFromXml(*ppXmlMesh, this))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                        Xml::CXmlNode* pXmlTransformations = *pSubNode;

                        if (pXmlTransformations->GetName() != _RVL_TAG_TRANSFORMATION_COLLECTION_)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const std::list<Xml::CXmlNode*>& Transformations = pXmlTransformations->GetSubNodes();

                        std::list<Xml::CXmlNode*>::const_iterator EndTransformations = Transformations.end();
                        for (std::list<Xml::CXmlNode*>::const_iterator ppXmlTransformation = Transformations.begin() ; ppXmlTransformation != EndTransformations ; ++ppXmlTransformation)
                        {
                            CModelTransformation* pModelTransformation = CModelTransformation::CreateFromXML(*ppXmlTransformation, this);

                            if (!pModelTransformation)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!AddTransformation(pModelTransformation))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }
                        }

                        return true;
                    }

                    Xml::CXmlNode* CModelMultipleMesh::Serialize(Xml::CXmlNode* pXmlParentNode) const
                    {
                        if (!pXmlParentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        Xml::CXmlNode* pXmlCurrentNode = CModelElement::Serialize(pXmlParentNode);

                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        Xml::CXmlNode* pXmlMeshes = pXmlCurrentNode->CreateSubNode(_RVL_TAG_MESH_COLLECTION_);

                        std::map<int, CModelMesh*>::const_iterator MeshesEnd = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != MeshesEnd ; ++pKeyModelMesh)
                            if (!pKeyModelMesh->second->Serialize(pXmlMeshes))
                            {
                                pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return nullptr;
                            }

                        Xml::CXmlNode* pXmlTransformations = pXmlCurrentNode->CreateSubNode(_RVL_TAG_TRANSFORMATION_COLLECTION_);

                        std::map<int, CModelTransformation*>::const_iterator TransformationEnd = m_Transformations.end();
                        for (std::map<int, CModelTransformation*>::const_iterator pKeyTransformation = m_Transformations.begin() ; pKeyTransformation != TransformationEnd ; ++pKeyTransformation)
                            if (!pKeyTransformation->second->Serialize(pXmlTransformations))
                            {
                                pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return nullptr;
                            }

                        return pXmlCurrentNode;
                    }

                    Containers::_3D::CContinuousBoundingBox3D CModelMultipleMesh::GetBoundingBox() const
                    {
                        Containers::_3D::CContinuousBoundingBox3D BoundingBox;
                        std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                        for (std::map<int, CModelMesh*>::const_iterator pKeyModelMesh = m_Meshes.begin() ; pKeyModelMesh != EndMeshes ; ++pKeyModelMesh)
                        {
                            BoundingBox.Extend(pKeyModelMesh->second->GetBoundingBox());
                        }
                        return BoundingBox;
                    }

                    bool CModelMultipleMesh::DeleteMesh(CModelMesh*& pMesh)
                    {
                        if (!pMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pMesh->GetSuperior() != this)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const int Id  = pMesh->GetInstanceId();

                        if (Id < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!m_Meshes.size())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        std::map<int, CModelMesh*>::iterator pKeyMesh = m_Meshes.find(Id);

                        if (pKeyMesh == m_Meshes.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        m_Meshes.erase(pKeyMesh);

                        if (pMesh->HasTransformation())
                        {
                            CModelTransformation* pModelTransformation = pMesh->GetTransformation();
                            if (pModelTransformation->GetGeometricCueSource().m_MeshId == Id)
                            {
                                DeleteTransformation(pModelTransformation);
                            }
                            else
                            {
                                pMesh->RemoveTransformation();
                            }
                        }

                        delete pMesh;
                        pMesh = nullptr;

                        return true;
                    }
                }
            }
        }
    }
}
