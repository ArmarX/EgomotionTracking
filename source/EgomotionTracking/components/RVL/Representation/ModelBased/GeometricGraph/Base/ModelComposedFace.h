/*
 * ModelComposedFace.h
 */

#pragma once

#include "ModelElement.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    class CModelMesh;
                    class CModelFace;
                    class CModelEdge;
                    class CModelVertex;

                    class CModelComposedFace : public CModelElement
                    {
                    public:

                        static CModelComposedFace* CreateFromXml(const Xml::CXmlNode* pXmlModelComposedFace, CModelMesh* pModelMesh);

                        CModelComposedFace(CModelFace* pModelFace, CModelMesh* pModelMesh, const Real OrientationTolerance, const bool AssignId);
                        ~CModelComposedFace() override;

                        bool IsTrivial() const;
                        int GetTotalFaces() const;
                        Mathematics::_3D::CVector3D GetNormal(const bool ConsiderSuperior) const;
                        Mathematics::_3D::CVector3D GetCentroid(const bool ConsiderSuperior) const;
                        Real GetArea() const;
                        Real GetPerimeter() const;
                        const std::map<int, CModelFace*>& GetFaces() const;
                        const std::map<int, CModelEdge*>& GetEdges() const;
                        const std::map<int, CModelVertex*>& GetVeritices() const;
                        void Clear() override;
                        bool Deserialize(const Xml::CXmlNode* pXmlCurrentNode) override;
                        Xml::CXmlNode* Serialize(Xml::CXmlNode* pXmlParentNode) const override;
                        CModelMesh* GetMesh();
                        virtual Containers::_3D::CContinuousBoundingBox3D GetBoundingBox() const;

                    protected:

                        CModelComposedFace(CModelMesh* pModelMesh, const bool AssignId);
                        bool DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode) override;
                        bool SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const override;
                        bool AddVertex(CModelVertex* pModelVertex);
                        bool AddEdge(CModelEdge* pModelEdge);
                        bool AddFace(CModelFace* pModelFace);
                        bool Extraction(CModelFace* pModelFace, const Real OrientationTolerance);
                        bool Insertion();

                        Mathematics::_3D::CVector3D m_Normal;
                        Real m_Area;
                        Real m_Perimeter;
                        std::map<int, CModelFace*> m_Faces;
                        std::map<int, CModelEdge*> m_Edges;
                        std::map<int, CModelVertex*> m_Vertices;
                    };
                }
            }
        }
    }
}

