/*
 * ModelVertex.cpp
 */

#include "ModelEdge.h"
#include "ModelFace.h"
#include "ModelMesh.h"
#include "ModelVertex.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    CModelVertex* CModelVertex::CreateFromXml(const Xml::CXmlNode* pXmlModelVertex, CModelMesh* pModelMesh)
                    {
                        if (!pXmlModelVertex)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        CModelVertex* pModelVertex = new CModelVertex(pModelMesh, false);

                        if (!pModelVertex->Deserialize(pXmlModelVertex))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pModelVertex;
                            return nullptr;
                        }

                        if (!pModelMesh->AddVertex(pModelVertex))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pModelVertex;
                            return nullptr;
                        }

                        return pModelVertex;
                    }

                    CModelVertex::CModelVertex(const Mathematics::_3D::CVector3D& Point, CModelMesh* pModelMesh, const bool AssignId) :
                        CModelElement(CModelElement::eVertex, pModelMesh, AssignId),
                        m_VertexInMeshIndex(-1),
                        m_Point(Point)
                    {
                        CModelMesh* pContainerModelMesh = GetMesh();

                        if (!pContainerModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                        }

                        if (!pContainerModelMesh->AddVertex(this))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                        }
                    }

                    CModelVertex::~CModelVertex()
                        = default;

                    Mathematics::_3D::CVector3D CModelVertex::GetPoint(const bool ConsiderSuperior) const
                    {
                        if (ConsiderSuperior)
                        {
                            const CModelMesh* pModelMesh = dynamic_cast<const CModelMesh*>(CModelElement::GetSuperior());
                            return pModelMesh->GetFrame().RigidTransform(m_Point);
                        }
                        else
                        {
                            return m_Point;
                        }
                    }

                    Mathematics::_3D::CVector3D CModelVertex::GetDynamicPoint() const
                    {
                        return ((const CModelMesh*)m_pSuperior)->m_DynamicFrame.RigidTransform(m_Point);
                    }

                    bool CModelVertex::HasIndentifiableTransformation(const int Id) const
                    {
                        const std::map<int, Mathematics::_4D::CMatrix4D>& IdentifiableDynamicFrames = ((const CModelMesh*)m_pSuperior)->m_IdentifiableDynamicFrames;

                        return (IdentifiableDynamicFrames.end() != IdentifiableDynamicFrames.find(Id));
                    }

                    bool CModelVertex::GetDynamicPointByIndentifiableTransformation(const int Id, Mathematics::_3D::CVector3D& Point) const
                    {
                        const std::map<int, Mathematics::_4D::CMatrix4D>& IdentifiableDynamicFrames = ((const CModelMesh*)m_pSuperior)->m_IdentifiableDynamicFrames;

                        std::map<int, Mathematics::_4D::CMatrix4D>::const_iterator pIdMatrix =  IdentifiableDynamicFrames.find(Id);

                        if (pIdMatrix == IdentifiableDynamicFrames.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            TextStream << "No matrix transformation with Id = " << Id << " could be found.";
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Point = pIdMatrix->second.RigidTransform(m_Point);

                        return true;
                    }

                    void CModelVertex::GetDynamicPoint(Mathematics::_3D::CVector3D& Point) const
                    {
                        Point = ((const CModelMesh*)m_pSuperior)->m_DynamicFrame.RigidTransform(m_Point);
                    }

                    const std::map<int, std::pair<CModelVertex*, CModelEdge*> >& CModelVertex::GetCoVertexEdges() const
                    {
                        return m_CoVertexKeyEdges;
                    }

                    const std::map<int, CModelEdge*>& CModelVertex::GetEdges() const
                    {
                        return m_Edges;
                    }

                    const std::map<int, CModelFace*>& CModelVertex::GetFaces() const
                    {
                        return m_Faces;
                    }

                    Real CModelVertex::GetDistance(const Mathematics::_3D::CVector3D& Point, const bool ConsiderSuperior) const
                    {
                        return GetPoint(ConsiderSuperior).GetDistance(Point);
                    }

                    bool CModelVertex::AddEdge(CModelEdge* pModelEdge)
                    {
                        if (!pModelEdge)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pModelEdge->GetSuperior() != GetSuperior())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelVertex* pModelVertexA = pModelEdge->GetWritableVertexA();

                        if (!pModelVertexA)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelVertex* pModelVertexB = pModelEdge->GetWritableVertexB();

                        if (!pModelVertexB)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pModelVertexA == pModelVertexB)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if ((pModelVertexA != this) && (pModelVertexB != this))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_Edges.find(pModelEdge->GetInstanceId()) != m_Edges.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!m_Edges.insert(std::pair<int, CModelEdge*>(pModelEdge->GetInstanceId(), pModelEdge)).second)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelVertex* pModelVertexC = (pModelVertexA == this) ? pModelVertexB : pModelVertexA;

                        if (!m_CoVertexKeyEdges.insert(std::pair<int, std::pair<CModelVertex*, CModelEdge*> >(pModelVertexC->GetInstanceId(), std::pair<CModelVertex*, CModelEdge*>(pModelVertexC, pModelEdge))).second)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    CModelEdge* CModelVertex::GetWritableLinkingEdge(CModelVertex* pModelVertex)
                    {
                        if (!pModelVertex)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (pModelVertex == this)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_WARNINGS_(TextStream.str(), 0);
                            return nullptr;
                        }

                        std::map<int, std::pair<CModelVertex*, CModelEdge*> >::iterator pLocation = m_CoVertexKeyEdges.find(pModelVertex->GetInstanceId());

                        if (pLocation != m_CoVertexKeyEdges.end())
                        {
                            return pLocation->second.second;
                        }
                        else
                        {
                            return nullptr;
                        }
                    }

                    bool CModelVertex::AddFace(CModelFace* pModelFace)
                    {
                        if (!pModelFace)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pModelFace->GetSuperior() != GetSuperior())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_Faces.find(pModelFace->GetInstanceId()) != m_Faces.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!m_Faces.insert(std::pair<int, CModelFace*>(pModelFace->GetInstanceId(), pModelFace)).second)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    void CModelVertex::Clear()
                    {
                        m_VertexInMeshIndex = -1;
                        m_Point.SetAtPlusInfinity();
                        m_CoVertexKeyEdges.clear();
                        m_Edges.clear();
                        m_Faces.clear();
                    }

                    CModelMesh* CModelVertex::GetMesh()
                    {
                        return dynamic_cast<CModelMesh*>(CModelElement::GetSuperior());
                    }

                    Containers::_3D::CContinuousBoundingBox3D CModelVertex::GetBoundingBox(const bool ConsiderSuperior) const
                    {
                        return Containers::_3D::CContinuousBoundingBox3D(GetPoint(ConsiderSuperior));
                    }

                    bool CModelVertex::Deserialize(const Xml::CXmlNode* pXmlCurrentNode)
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::Deserialize(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    Xml::CXmlNode* CModelVertex::Serialize(Xml::CXmlNode* pXmlParentNode) const
                    {
                        if (!pXmlParentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        Xml::CXmlNode* pXmlCurrentNode = CModelElement::Serialize(pXmlParentNode);

                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        return pXmlCurrentNode;
                    }

                    void CModelVertex::SetVertexInMeshIndex(const int VertexInMeshIndex)
                    {
                        m_VertexInMeshIndex = VertexInMeshIndex;
                    }

                    int CModelVertex::GetVertexInMeshIndex() const
                    {
                        return m_VertexInMeshIndex;
                    }

                    CModelVertex::CModelVertex(CModelMesh* pModelMesh, const bool AssignId) :
                        CModelElement(CModelElement::eVertex, pModelMesh, AssignId),
                        m_VertexInMeshIndex(-1),
                        m_Point(Mathematics::_3D::CVector3D::s_PlusInfinity)
                    {
                    }

                    bool CModelVertex::DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode)
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::DeserializeAttributes(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        int MeshIndex = 0;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_VERTEX_IN_MESH_INDEX_, MeshIndex))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real X = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_COORDINATE_X_, X))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real Y = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_COORDINATE_Y_, Y))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real Z = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_COORDINATE_Z_, Z))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        m_VertexInMeshIndex = MeshIndex;
                        m_Point.Set(X, Y, Z);

                        return true;
                    }

                    bool CModelVertex::SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const
                    {
                        if (!CModelElement::SerializeAttributes(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_VERTEX_IN_MESH_INDEX_, m_VertexInMeshIndex))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_COORDINATE_X_, m_Point.GetX()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_COORDINATE_Y_, m_Point.GetY()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_COORDINATE_Z_, m_Point.GetZ()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }
                }
            }
        }
    }
}
