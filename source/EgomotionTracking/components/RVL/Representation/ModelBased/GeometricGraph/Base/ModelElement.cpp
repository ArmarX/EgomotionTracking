/*
 * ModelElement.cpp
 */

#include "ModelElement.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    int CModelElement::s_InstanceIdCounter = 0;

                    std::string CModelElement::TypeIdToCollectionString(const TypeId CurrentTypeId)
                    {
                        switch (CurrentTypeId)
                        {
                            case eVertex:
                                return _RVL_TAG_VERTEX_COLLECTION_;
                            case eOrientedVertex:
                                return _RVL_TAG_ORIENTED_VERTEX_COLLECTION_;
                            case eEdge:
                                return _RVL_TAG_EDGE_COLLECTION_;
                            case eFace:
                                return _RVL_TAG_FACE_COLLECTION_;
                            case eComposedFace:
                                return _RVL_TAG_COMPOSED_FACE_COLLECTION_;
                            case eMesh:
                                return _RVL_TAG_MESH_COLLECTION_;
                            case eMultipleMesh:
                                return _RVL_TAG_MULTIPLE_MESH_COLLECTION_;
                        }
                        return _RVL_TAG_UNKNOWN_;
                    }

                    std::string CModelElement::TypeIdToString(const TypeId CurrentTypeId)
                    {
                        switch (CurrentTypeId)
                        {
                            case eVertex:
                                return _RVL_TAG_VERTEX_;
                            case eOrientedVertex:
                                return _RVL_TAG_ORIENTED_VERTEX_;
                            case eEdge:
                                return _RVL_TAG_EDGE_;
                            case eFace:
                                return _RVL_TAG_FACE_;
                            case eComposedFace:
                                return _RVL_TAG_COMPOSED_FACE_;
                            case eMesh:
                                return _RVL_TAG_MESH_;
                            case eMultipleMesh:
                                return _RVL_TAG_MULTIPLE_MESH_;
                        }
                        return _RVL_TAG_UNKNOWN_;
                    }

                    CModelElement::TypeId CModelElement::StringToTypeId(const std::string& CurrentTypeId)
                    {
                        if (CurrentTypeId == _RVL_TAG_VERTEX_)
                        {
                            return eVertex;
                        }
                        else if (CurrentTypeId == _RVL_TAG_ORIENTED_VERTEX_)
                        {
                            return eOrientedVertex;
                        }
                        else if (CurrentTypeId == _RVL_TAG_EDGE_)
                        {
                            return eEdge;
                        }
                        else if (CurrentTypeId == _RVL_TAG_FACE_)
                        {
                            return eFace;
                        }
                        else if (CurrentTypeId == _RVL_TAG_COMPOSED_FACE_)
                        {
                            return eComposedFace;
                        }
                        else if (CurrentTypeId == _RVL_TAG_MESH_)
                        {
                            return eMesh;
                        }
                        else if (CurrentTypeId == _RVL_TAG_MULTIPLE_MESH_)
                        {
                            return eMultipleMesh;
                        }
                        return CModelElement::TypeId(eMultipleMesh + 1);
                    }

                    std::string CModelElement::VisibilityToString(const Visibility CurrentVisibility)
                    {
                        switch (CurrentVisibility)
                        {
                            case eStaticOccluded:
                                return _RVL_TAG_VISIBILITY_STATIC_OCCLUDED_;
                            case eStaticVisible:
                                return _RVL_TAG_VISIBILITY_STATIC_VISIBLE_;
                            case eDynamicVisible:
                                return _RVL_TAG_VISIBILITY_DYNAMIC_VISIBLE_;
                        }
                        return _RVL_TAG_UNKNOWN_;
                    }

                    CModelElement::Visibility CModelElement::StringToVisibility(const std::string& CurrentVisibility)
                    {
                        if (CurrentVisibility == _RVL_TAG_VISIBILITY_STATIC_OCCLUDED_)
                        {
                            return eStaticOccluded;
                        }
                        else if (CurrentVisibility == _RVL_TAG_VISIBILITY_STATIC_VISIBLE_)
                        {
                            return eStaticVisible;
                        }
                        else if (CurrentVisibility == _RVL_TAG_VISIBILITY_DYNAMIC_VISIBLE_)
                        {
                            return eDynamicVisible;
                        }
                        return CModelElement::Visibility(eDynamicVisible + 1);
                    }

                    CModelElement::CModelElement(const TypeId Id, CModelElement* pSuperior, const bool AssignId) :
                        Dependency::IDependencyManager(),
                        m_InstanceId(AssignId ? ++s_InstanceIdCounter : -1),
                        m_Enabled(true),
                        m_Indexable(true),
                        m_TypeId(Id),
                        m_Visibility(eStaticVisible),
                        m_SaliencyWeight(Real(1)),
                        m_pElementData(nullptr),
                        m_pSuperior(pSuperior)
                    {
                    }

                    CModelElement::~CModelElement()
                        = default;

                    int CModelElement::GetInstanceId() const
                    {
                        return m_InstanceId;
                    }

                    void CModelElement::SetLabel(const std::string& Label)
                    {
                        m_Label = Label;
                    }

                    void CModelElement::ClearLabel()
                    {
                        m_Label.clear();
                    }

                    const std::string& CModelElement::GetLabel() const
                    {
                        return m_Label;
                    }

                    std::string CModelElement::GetTypeName() const
                    {
                        return CModelElement::TypeIdToString(m_TypeId);
                    }

                    CModelElement::TypeId CModelElement::GetTypeId() const
                    {
                        return m_TypeId;
                    }

                    bool CModelElement::TypeEquals(const CModelElement* pModelPrimitive) const
                    {
                        return (pModelPrimitive) && (pModelPrimitive->m_TypeId == m_TypeId);
                    }

                    bool CModelElement::IsEnabled(const bool ConsiderSuperior) const
                    {
                        if (m_pSuperior && ConsiderSuperior)
                        {
                            return m_pSuperior->IsEnabled(ConsiderSuperior) && m_Enabled;
                        }
                        return m_Enabled;
                    }

                    void CModelElement::SetEnabled(const bool Enabled)
                    {
                        if (Enabled != m_Enabled)
                        {
                            m_Enabled = Enabled;
                            OnChange(_RVL_MODEL_ELEMENT_CHANGED_ENABLED_EVENT_ID_);
                        }
                    }

                    bool CModelElement::IsIndexable(const bool ConsiderSuperior) const
                    {
                        if (m_pSuperior && ConsiderSuperior)
                        {
                            return m_pSuperior->IsIndexable(ConsiderSuperior) && m_Indexable;
                        }
                        return m_Indexable;
                    }

                    void CModelElement::SetIndexable(const bool Indexable)
                    {
                        if (Indexable != m_Indexable)
                        {
                            m_Indexable = Indexable;
                            OnChange(_RVL_MODEL_ELEMENT_CHANGED_INDEXABLE_EVENT_ID_);
                        }
                    }

                    void CModelElement::SetVisibility(const Visibility Visible)
                    {
                        if (Visible != m_Visibility)
                        {
                            m_Visibility = Visible;
                            OnChange(_RVL_MODEL_ELEMENT_CHANGED_VISIBILITY_EVENT_ID_);
                        }
                    }

                    CModelElement::Visibility CModelElement::GetVisibility(const bool ConsiderSuperior) const
                    {
                        if (ConsiderSuperior && m_pSuperior)
                            switch (m_pSuperior->GetVisibility(ConsiderSuperior))
                            {
                                case eStaticOccluded:
                                    return eStaticOccluded;
                                case eStaticVisible:
                                case eDynamicVisible:
                                    return m_Visibility;
                            }
                        return m_Visibility;
                    }

                    bool CModelElement::SetSaliencyWeight(const float SaliencyWeight)
                    {
                        if (SaliencyWeight >= 0.0f)
                        {
                            if (m_SaliencyWeight != SaliencyWeight)
                            {
                                m_SaliencyWeight = SaliencyWeight;
                                OnChange(_RVL_MODEL_ELEMENT_CHANGED_DATA_EVENT_ID_);
                            }
                            return true;
                        }
                        return false;
                    }

                    float CModelElement::GetSaliencyWeight() const
                    {
                        return m_SaliencyWeight;
                    }


                    const void* CModelElement::GetElementData() const
                    {
                        return m_pElementData;
                    }

                    void CModelElement::SetElementData(const void* pElementData)
                    {
                        if (pElementData != m_pElementData)
                        {
                            m_pElementData = pElementData;
                            OnChange(_RVL_MODEL_ELEMENT_CHANGED_DATA_EVENT_ID_);
                        }
                    }

                    void CModelElement::ClearElementData()
                    {
                        m_pElementData = nullptr;
                    }

                    const CModelElement* CModelElement::GetSuperior() const
                    {
                        return m_pSuperior;
                    }

                    CModelElement* CModelElement::GetSuperior()
                    {
                        return m_pSuperior;
                    }

                    void CModelElement::SetSuperior(CModelElement* pSuperior)
                    {
                        if (pSuperior != m_pSuperior)
                        {
                            m_pSuperior = pSuperior;
                            OnChange(_RVL_MODEL_ELEMENT_CHANGED_SUPERIOR_EVENT_ID_);
                        }
                    }

                    std::string CModelElement::ToString() const
                    {
                        Xml::CXmlNode XmlDocument(_LIBRARY_ACRONYM_);
                        if ((m_TypeId == eMultipleMesh) || ((m_TypeId == eMesh) && (!m_pSuperior)))
                        {
                            if (!XmlDocument.SetAttribute(_RVL_TAG_SERIALIZATION_TIME_STAMP_, Time::CTimeEntry::Now()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return std::string();
                            }

                            if (!XmlDocument.SetAttribute(_RVL_TAG_CONTENT_, std::string(_RVL_TAG_MODEL_CONTENT_)))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return std::string();
                            }

                            if (!XmlDocument.SetAttribute(_RVL_TAG_VERSION_, _MAJOR_VERSION_))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return std::string();
                            }

                            if (!XmlDocument.SetAttribute(_RVL_TAG_SUBVERSION_, _MINOR_VERSION_))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return std::string();
                            }

                            if (!XmlDocument.SetAttribute(_RVL_TAG_REVISION_, _REVISION_NUMBER_))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return std::string();
                            }

                            if (!XmlDocument.SetAttribute(_RVL_TAG_BUILD_, _BUILD_NUMBER_))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return std::string();
                            }
                        }

                        if (!Serialize(&XmlDocument))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return std::string();
                        }

                        std::string Content = XmlDocument.ToString();

                        return Content;
                    }

                    bool CModelElement::SaveToFile(const std::string& FileName, const bool OverrideFlag) const
                    {
                        if ((!OverrideFlag) && Files::CFile::Exists(FileName))
                        {
                            return false;
                        }
                        Xml::CXmlNode::Initialize();
                        return Files::COutFile::WriteStringToFile(FileName, ToString());
                    }

                    bool CModelElement::LoadFromFile(const std::string& FileName)
                    {
                        if (!Files::CFile::Exists(FileName))
                        {
                            return false;
                        }
                        Xml::CXmlNode* pXmlDocument = Xml::CXmlNode::CreateFromFile(FileName);
                        bool Result = false;
                        if (pXmlDocument)
                        {
                            Result = (pXmlDocument->GetName() == _LIBRARY_ACRONYM_) && Deserialize(pXmlDocument);
                            delete pXmlDocument;
                        }
                        return Result;
                    }

                    bool CModelElement::Deserialize(const Xml::CXmlNode* pXmlCurrentNode)
                    {
                        Clear();

                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if ((pXmlCurrentNode->GetName() != CModelElement::TypeIdToString(m_TypeId)))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!DeserializeAttributes(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    Xml::CXmlNode* CModelElement::Serialize(Xml::CXmlNode* pXmlParentNode) const
                    {
                        if (!pXmlParentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        Xml::CXmlNode* pXmlCurrentNode = pXmlParentNode->CreateSubNode(CModelElement::TypeIdToString(m_TypeId));

                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!SerializeAttributes(pXmlCurrentNode))
                        {
                            pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        return pXmlCurrentNode;
                    }

                    Xml::CXmlNode* CModelElement::SerializeReference(Xml::CXmlNode* pXmlContainerNode) const
                    {
                        if (!pXmlContainerNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        Xml::CXmlNode* pXmlCurrentReferenceNode = pXmlContainerNode->CreateSubNode(CModelElement::TypeIdToString(m_TypeId));

                        if (!pXmlCurrentReferenceNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pXmlCurrentReferenceNode->SetAttribute(_RVL_TAG_INSTANCE_ID_, GetInstanceId()))
                        {
                            pXmlContainerNode->DestroySubNode(pXmlCurrentReferenceNode);
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        return pXmlCurrentReferenceNode;
                    }

                    void CModelElement::ResetInstanceIdCounter()
                    {
                        s_InstanceIdCounter = 0;
                    }

                    bool CModelElement::DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode)
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        std::string TypeId;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_TYPE_ID_, TypeId))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelElement::TypeId CurrentType = CModelElement::StringToTypeId(TypeId);

                        if ((CurrentType < eVertex) || (CurrentType > eMultipleMesh))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        int InstanceId = 0;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_INSTANCE_ID_, InstanceId))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (InstanceId < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        bool Enabled = false;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_ENABLED_, Enabled))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        bool Indexable = false;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_INDEXABLE_, Indexable))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real SaliencyWeight = Real(0);

                        if (pXmlCurrentNode->HasAttribute(_RVL_RAG_SALIENCY_WEIGHT_))
                        {
                            if (!pXmlCurrentNode->GetAttribute(_RVL_RAG_SALIENCY_WEIGHT_, SaliencyWeight))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }
                        }
                        else
                        {
                            SaliencyWeight = Real(1);
                        }

                        std::string Visibility;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_VISIBILITY_, Visibility))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const CModelElement::Visibility CurrentVisibility = CModelElement::StringToVisibility(Visibility);

                        if ((CurrentVisibility < eStaticOccluded) || (CurrentVisibility > eDynamicVisible))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        std::string Label;

                        if (pXmlCurrentNode->HasAttribute(_RVL_TAG_LABEL_) && (!pXmlCurrentNode->GetAttribute(_RVL_TAG_LABEL_, Label)))
                        {
                            return false;
                        }

                        m_TypeId = CurrentType;
                        m_InstanceId = InstanceId;
                        m_Enabled = Enabled;
                        m_Indexable = Indexable;
                        m_Visibility = CurrentVisibility;
                        m_SaliencyWeight = SaliencyWeight;
                        m_Label = Label;

                        if (InstanceId > s_InstanceIdCounter)
                        {
                            s_InstanceIdCounter = InstanceId + 1;
                        }

                        return true;
                    }

                    bool CModelElement::SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if ((m_TypeId < eVertex) || (m_TypeId > eMultipleMesh))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_InstanceId < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if ((m_Visibility < CModelElement::eStaticOccluded) || (m_Visibility > CModelElement::eDynamicVisible))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_TYPE_ID_, CModelElement::TypeIdToString(m_TypeId)))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_INSTANCE_ID_, m_InstanceId))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_ENABLED_, m_Enabled))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_INDEXABLE_, m_Indexable))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_RAG_SALIENCY_WEIGHT_, m_SaliencyWeight))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_VISIBILITY_, CModelElement::VisibilityToString(m_Visibility)))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_Label.length())
                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_LABEL_, m_Label))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                        return true;
                    }
                }
            }
        }
    }
}
