/*
 * ModelEdge.cpp
 */

#include "ModelVertex.h"
#include "ModelEdge.h"
#include "ModelFace.h"
#include "ModelMesh.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    CModelEdge* CModelEdge::CreateFromXml(const Xml::CXmlNode* pXmlModelEdge, CModelMesh* pModelMesh)
                    {
                        if (!pXmlModelEdge)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        CModelEdge* pModelEdge = new CModelEdge(pModelMesh, false);

                        if (!pModelEdge)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelEdge->Deserialize(pXmlModelEdge))
                        {
                            delete pModelEdge;
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelEdge->GetWritableVertexA()->AddEdge(pModelEdge))
                        {
                            delete pModelEdge;
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelEdge->GetWritableVertexB()->AddEdge(pModelEdge))
                        {
                            delete pModelEdge;
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelMesh->AddEdge(pModelEdge))
                        {
                            delete pModelEdge;
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        return pModelEdge;
                    }

                    CModelEdge::CModelEdge(CModelVertex* pModelVertexA, CModelVertex* pModelVertexB, CModelMesh* pModelMesh, const bool AssignId) :
                        CModelElement(CModelElement::eEdge, pModelMesh, AssignId),
                        m_pModelVertexA(pModelVertexA),
                        m_pModelVertexB(pModelVertexB),
                        m_ModelFaceReferenceP(),
                        m_ModelFaceReferenceQ(),
                        m_Length(Real(0))
                    {
                        if (!m_pModelVertexA)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                        }

                        if (!m_pModelVertexB)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                        }

                        if (m_pModelVertexA == m_pModelVertexB)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                        }

                        CModelMesh* pContainerModelMesh = GetMesh();

                        if (!pContainerModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                        }

                        if (!pContainerModelMesh->AddEdge(this))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                        }

                        if (!m_pModelVertexA->AddEdge(this))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                        }

                        if (!m_pModelVertexB->AddEdge(this))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                        }

                        Mathematics::_3D::CVector3D Direction = m_pModelVertexB->GetPoint(false) - m_pModelVertexA->GetPoint(false);
                        m_Length = Direction.Normalize();
                    }

                    CModelEdge::~CModelEdge()
                        = default;

                    bool CModelEdge::SetFace(CModelFace* pModelFace)
                    {
                        if (!pModelFace)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_ModelFaceReferenceP.IsSet() && m_ModelFaceReferenceQ.IsSet())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_WARNINGS_(TextStream.str(), 0);
                            //return false;
                        }

                        if (m_ModelFaceReferenceP.IsSet())
                        {
                            m_ModelFaceReferenceQ.SetDirect(pModelFace);
                        }
                        else
                        {
                            m_ModelFaceReferenceP.SetDirect(pModelFace);
                        }

                        return true;
                    }

                    bool CModelEdge::HasBothFaces() const
                    {
                        return (m_ModelFaceReferenceP.IsSet() && m_ModelFaceReferenceQ.IsSet());
                    }

                    bool CModelEdge::IsAuxiliar() const
                    {
                        return (m_ModelFaceReferenceP.IsSetDirect() && m_ModelFaceReferenceQ.IsSetDirect() && m_ModelFaceReferenceP.GetDirect()->HasComposedFace() && (m_ModelFaceReferenceP.GetDirect()->GetReadOnlyComposedFace() == m_ModelFaceReferenceQ.GetDirect()->GetReadOnlyComposedFace()));
                    }

                    CModelVertex* CModelEdge::GetWritableVertexA()
                    {
                        return m_pModelVertexA;
                    }

                    CModelVertex* CModelEdge::GetWritableVertexB()
                    {
                        return m_pModelVertexB;
                    }

                    const CModelVertex* CModelEdge::GetReadOnlyVertexA() const
                    {
                        return m_pModelVertexA;
                    }

                    const CModelVertex* CModelEdge::GetReadOnlyVertexB() const
                    {
                        return m_pModelVertexB;
                    }

                    CModelFace* CModelEdge::GetWritableFaceP()
                    {
                        return m_ModelFaceReferenceP.GetDirect();
                    }

                    CModelFace* CModelEdge::GetWritableFaceQ()
                    {
                        return m_ModelFaceReferenceQ.GetDirect();
                    }

                    const CModelFace* CModelEdge::GetReadOnlyFaceP() const
                    {
                        return m_ModelFaceReferenceP.GetDirect();
                    }

                    const CModelFace* CModelEdge::GetReadOnlyFaceQ() const
                    {
                        return m_ModelFaceReferenceQ.GetDirect();
                    }

                    Real CModelEdge::GetLength() const
                    {
                        return m_Length;
                    }

                    Mathematics::_3D::CVector3D CModelEdge::GetDirection(const bool ConsiderSuperior) const
                    {
                        return m_pModelVertexB->GetPoint(ConsiderSuperior) - m_pModelVertexA->GetPoint(ConsiderSuperior);
                    }

                    Mathematics::_3D::CVector3D CModelEdge::GetMidPoint(const bool ConsiderSuperior) const
                    {
                        return Mathematics::_3D::CVector3D::CreateMidPoint(m_pModelVertexB->GetPoint(ConsiderSuperior), m_pModelVertexA->GetPoint(ConsiderSuperior));
                    }

                    void CModelEdge::GetEndPoints(Mathematics::_3D::CVector3D& A, Mathematics::_3D::CVector3D& B, const bool ConsiderSuperior)
                    {
                        A = m_pModelVertexA->GetPoint(ConsiderSuperior);
                        B = m_pModelVertexB->GetPoint(ConsiderSuperior);
                    }

                    void CModelEdge::Clear()
                    {
                        m_pModelVertexA = nullptr;
                        m_pModelVertexB = nullptr;
                        m_ModelFaceReferenceP.Clear();
                        m_ModelFaceReferenceQ.Clear();
                        m_Length = Real(0);
                    }

                    CModelMesh* CModelEdge::GetMesh()
                    {
                        return dynamic_cast<CModelMesh*>(CModelElement::GetSuperior());
                    }

                    bool CModelEdge::SolveIndirectReferences()
                    {
                        if (!(m_ModelFaceReferenceP.IsSetIndirect() || m_ModelFaceReferenceQ.IsSetIndirect()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelMesh* pMesh = dynamic_cast<CModelMesh*>(CModelElement::GetSuperior());

                        if (!pMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_ModelFaceReferenceP.IsSetIndirect())
                            if (!m_ModelFaceReferenceP.SetDirect(pMesh->GetFaceById(m_ModelFaceReferenceP.m_DualModelFaceReference.m_Id)))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                        if (m_ModelFaceReferenceQ.IsSetIndirect())
                            if (!m_ModelFaceReferenceQ.SetDirect(pMesh->GetFaceById(m_ModelFaceReferenceQ.m_DualModelFaceReference.m_Id)))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                        return true;
                    }

                    Containers::_3D::CContinuousBoundingBox3D CModelEdge::GetBoundingBox(const bool ConsiderSuperior) const
                    {
                        if (m_pModelVertexA && m_pModelVertexB)
                        {
                            Containers::_3D::CContinuousBoundingBox3D BoundingBox(m_pModelVertexA->GetPoint(ConsiderSuperior));
                            BoundingBox.Extend(m_pModelVertexB->GetPoint(ConsiderSuperior));
                            return BoundingBox;
                        }
                        return Containers::_3D::CContinuousBoundingBox3D();
                    }

                    void CModelEdge::GetDynamicEndPoints(Mathematics::_3D::CVector3D& PointA, Mathematics::_3D::CVector3D& PointB) const
                    {
                        m_pModelVertexA->GetDynamicPoint(PointA);
                        m_pModelVertexB->GetDynamicPoint(PointB);
                    }

                    CModelEdge::CModelEdge(CModelMesh* pModelMesh, const bool AssignId) :
                        CModelElement(CModelElement::eEdge, pModelMesh, AssignId),
                        m_pModelVertexA(nullptr),
                        m_pModelVertexB(nullptr),
                        m_ModelFaceReferenceP(),
                        m_ModelFaceReferenceQ(),
                        m_Length(Real(0))
                    {
                    }

                    bool CModelEdge::DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode)
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::DeserializeAttributes(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelMesh* pContainerModelMesh = GetMesh();

                        if (!pContainerModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        int VertexAId = 0;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_VERTEX_A_ID_, VertexAId))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelVertex* pModelVertexA = pContainerModelMesh->GetVertexById(VertexAId);

                        if (!pModelVertexA)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        int VertexBId = 0;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_VERTEX_B_ID_, VertexBId))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelVertex* pModelVertexB = pContainerModelMesh->GetVertexById(VertexBId);

                        if (!pModelVertexB)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pModelVertexA == pModelVertexB)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        int FacePId = 0;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_FACE_P_ID_, FacePId))
                        {
                            FacePId = -1;
                            //WARNING THIS SITUATION IS A TOPOLOGICAL ERROR
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_WARNINGS_(TextStream.str(), 0);
                        }

                        int FaceQId = 0;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_FACE_Q_ID_, FaceQId))
                        {
                            FaceQId = -1;
                            //WARNING THIS SITUATION IS A TOPOLOGICAL ERROR
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_WARNINGS_(TextStream.str(), 0);
                        }

                        m_pModelVertexA = pModelVertexA;
                        m_pModelVertexB = pModelVertexB;
                        Mathematics::_3D::CVector3D Direction = m_pModelVertexB->GetPoint(false) - m_pModelVertexA->GetPoint(false);
                        m_Length = Direction.Normalize();
                        m_ModelFaceReferenceP.SetIndirect(FacePId);
                        m_ModelFaceReferenceQ.SetIndirect(FaceQId);

                        return true;
                    }

                    bool CModelEdge::SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::SerializeAttributes(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_VERTEX_A_ID_, m_pModelVertexA->GetInstanceId()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_VERTEX_B_ID_, m_pModelVertexB->GetInstanceId()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const int FacePId = m_ModelFaceReferenceP.IsSetDirect() ? m_ModelFaceReferenceP.GetDirect()->GetInstanceId() : 0;
                        const int FaceQId = m_ModelFaceReferenceQ.IsSetDirect() ? m_ModelFaceReferenceQ.GetDirect()->GetInstanceId() : 0;

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_FACE_P_ID_, FacePId))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_FACE_Q_ID_, FaceQId))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }
                }
            }
        }
    }
}
