/*
 * ModelMesh.h
 */

#pragma once

#include "../../../../Mathematics/_3D/Geometry/Geometry3D.h"
#include "ModelElement.h"
#include "ModelVertex.h"
#include "ModelOrientedVertex.h"
#include "ModelEdge.h"
#include "ModelFace.h"
#include "ModelComposedFace.h"
#include "ModelMultipleMesh.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    class CModelTransformation;

                    class CModelMesh : public CModelElement
                    {
                    public:

                        static CModelMesh* CreateFromXml(const Xml::CXmlNode* pXmlModelMesh, CModelMultipleMesh* pModelMultipleMesh);

                        CModelMesh(CModelMultipleMesh* pModelMultipleMesh, const bool AssignId);
                        ~CModelMesh() override;

                        bool AddVertex(CModelVertex* pModelVertex);
                        bool AddEdge(CModelEdge* pModelEdge);
                        bool AddFace(CModelFace* pModelFace);
                        bool AddComposedFaces(CModelComposedFace* pModelComposedFace);

                        CModelVertex* GetVertexById(const int Id) const;
                        const std::map<int, CModelVertex*>& GetVertices() const;

                        CModelEdge* GetEdgeById(const int Id) const;
                        const std::map<int, CModelEdge*>& GetEdges() const;

                        CModelFace* GetFaceById(const int Id);
                        const std::map<int, CModelFace*>& GetFaces() const;

                        CModelComposedFace* GetComposedFaceById(const int Id);
                        const std::map<int, CModelComposedFace*>& GetComposedFaces() const;

                        void Transform(const Mathematics::_4D::CMatrix4D& T);

                        int GetTotalVertices() const;
                        int GetTotalVisibleVertices(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const;
                        int GetTotalEnabledVertices(const bool ConsiderSuperior) const;
                        int GetTotalIndexableVertices(const bool ConsiderSuperior) const;

                        int GetTotalEdges() const;
                        int GetTotalVisibleEdges(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const;
                        int GetTotalEnabledEdges(const bool ConsiderSuperior) const;
                        int GetTotalIndexableEdges(const bool ConsiderSuperior) const;

                        int GetTotalFaces() const;
                        int GetTotalVisibleFaces(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const;
                        int GetTotalEnabledFaces(const bool ConsiderSuperior);
                        int GetTotalIndexableFaces(const bool ConsiderSuperior) const;

                        int GetTotalMultiFaces() const;
                        int GetTotalVisibleMultiFaces(CModelElement::Visibility VisibilityState, const bool ConsiderSuperior) const;
                        int GetTotalEnabledMultiFaces(const bool ConsiderSuperior) const;
                        int GetTotalIndexableMultiFaces(const bool ConsiderSuperior) const;

                        const CModelMultipleMesh* GetMultipleMesh() const;
                        CModelMultipleMesh* GetMultipleMesh();

                        void Clear() override;
                        bool Deserialize(const Xml::CXmlNode* pXmlCurrentNode) override;
                        Xml::CXmlNode* Serialize(Xml::CXmlNode* pXmlParentNode) const override;

                        void SetReferenceFrame(const Mathematics::_4D::CMatrix4D& Frame);
                        void SetDynamicFrame(const Mathematics::_4D::CMatrix4D& Frame);

                        const Mathematics::_4D::CMatrix4D& GetFrame() const;
                        bool SetFrameTransformation(const Mathematics::_3D::CMatrix3D& Rotation, const Mathematics::_3D::CVector3D& Translation);
                        virtual Containers::_3D::CContinuousBoundingBox3D GetBoundingBox() const;

                        bool SetMaterial(const Real* pMaterial);
                        bool SetMaterial(const Real R, const Real G, const Real B, const Real A);

                        void GetMaterial(Real& R, Real& G, Real& B, Real& A) const;
                        void GetDiscreteMaterial(Byte& R, Byte& G, Byte& B, Byte& A) const;

                        bool IsDynamic() const;

                        bool SetTransformation(CModelTransformation* pModelTransformation);
                        bool HasTransformation() const;
                        CModelTransformation* GetTransformation();
                        bool RemoveTransformation();
                        //Usage
                        //Set -1 to apply to the base dynamic transformation, Id>=0 otherwise.
                        bool UpdateTransformation(const int Id = -1);

                        bool HasIdentifiableDynamicFrame(const int Id) const;
                        bool CreateIdentifiableDynamicFrame(const int Id);
                        bool SetIdentifiableDynamicFrame(const int Id, const Mathematics::_4D::CMatrix4D& Frame);
                        void ClearIdentifiableDynamicFrame();


                    protected:

                        friend class CModelMultipleMesh;
                        friend class CModelVertex;

                        CModelMesh(const bool AssignId);

                        template <typename GenericModelElement> void Clear(std::map<int, GenericModelElement*>& Collection)
                        {
                            if (!Collection.size())
                            {
                                return;
                            }
                            typename std::map<int, GenericModelElement*>::iterator EndCollection = Collection.end();
                            for (typename std::map<int, GenericModelElement*>::iterator ppModelElement = Collection.begin() ; ppModelElement != EndCollection ; ++ppModelElement)
                            {
                                delete ppModelElement->second;
                            }
                            Collection.clear();
                        }

                        bool DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode) override;
                        bool SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const override;

                        CModelTransformation* m_pModelTransformation;
                        Mathematics::_4D::CMatrix4D m_StaticFrame;
                        Mathematics::_4D::CMatrix4D m_DynamicFrame;
                        std::map<int, Mathematics::_4D::CMatrix4D> m_IdentifiableDynamicFrames;
                        Real m_Material[4];
                        std::map<int, CModelVertex*> m_Vertices;
                        std::map<int, CModelEdge*> m_Edges;
                        std::map<int, CModelFace*> m_Faces;
                        std::map<int, CModelComposedFace*> m_ComposedFaces;
                    };

                    class CModelTransformation
                    {
                    public:

                        enum TransformationType
                        {
                            eTranslation, eRotation, eFrameToFrameRigid, eGeneralRigid
                        };

                        static std::string TransformationTypeToString(const TransformationType CurrentTransformationType)
                        {
                            switch (CurrentTransformationType)
                            {
                                case eTranslation:
                                    return std::string("Translation");

                                case eRotation:
                                    return std::string("Rotation");

                                case eFrameToFrameRigid:
                                    return std::string("FrameToFrameRigid");

                                case eGeneralRigid:
                                    return std::string("GeneralRigid");
                            }

                            return std::string(_RVL_TAG_UNKNOWN_);
                        }

                        static TransformationType StringToTransformationType(const std::string& Content)
                        {
                            if (Content == std::string("Translation"))
                            {
                                return eTranslation;
                            }

                            else if (Content == std::string("Rotation"))
                            {
                                return eRotation;
                            }

                            else if (Content == std::string("FrameToFrameRigid"))
                            {
                                return eFrameToFrameRigid;
                            }

                            else if (Content == std::string("GeneralRigid"))
                            {
                                return eGeneralRigid;
                            }

                            return TransformationType(-1);
                        }

                        struct GeometricCueSource
                        {
                            enum CueType
                            {
                                eVertexAB, eVertexABC, eVertexABCD, eEdgeA, EdgeAB
                            };

                            static std::string CueTypeToString(const CueType CurrentCueType)
                            {
                                switch (CurrentCueType)
                                {
                                    case eVertexAB:
                                        return std::string("VertexAB");

                                    case eVertexABC:
                                        return std::string("VertexABC");

                                    case eVertexABCD:
                                        return std::string("VertexABCD");

                                    case eEdgeA:
                                        return std::string("EdgeA");

                                    case EdgeAB:
                                        return std::string("EdgeAB");

                                }

                                return std::string(_RVL_TAG_UNKNOWN_);
                            }

                            static CueType StringToCueType(const std::string& Content)
                            {
                                if (Content == "VertexAB")
                                {
                                    return eVertexAB;
                                }

                                else if (Content == "VertexABC")
                                {
                                    return eVertexABC;
                                }

                                else if (Content == "VertexABCD")
                                {
                                    return eVertexABCD;
                                }

                                else if (Content == "EdgeA")
                                {
                                    return eEdgeA;
                                }

                                else if (Content == "EdgeAB")
                                {
                                    return EdgeAB;
                                }

                                return CueType(-1);
                            }

                            GeometricCueSource():
                                m_CueType(CueType(-1)), m_MeshId(-1)
                            {
                                for (int i = 0; i < 4; ++i)
                                {
                                    m_Ids[i] = -1;
                                }
                            }

                            GeometricCueSource(const CueType CurrentCueType, const int MeshId, const int IdA, const int IdB = -1, const int IdC = -1, const int IdD = -1):
                                m_CueType(CurrentCueType), m_MeshId(MeshId)
                            {
                                m_Ids[0] = IdA;
                                m_Ids[1] = IdB;
                                m_Ids[2] = IdC;
                                m_Ids[3] = IdD;
                            }

                            inline bool IsValid() const
                            {
                                switch (m_CueType)
                                {
                                    case eVertexAB:
                                        return (m_Ids[0] > 0) && (m_Ids[1] > 0);
                                    case eVertexABC:
                                        return (m_Ids[0] > 0) && (m_Ids[1] > 0) && (m_Ids[2] > 0);
                                    case eVertexABCD:
                                        return (m_Ids[0] > 0) && (m_Ids[1] > 0) && (m_Ids[2] > 0) && (m_Ids[3] > 0);
                                    case eEdgeA:
                                        return (m_Ids[0] > 0);
                                    case EdgeAB:
                                        return (m_Ids[0] > 0) && (m_Ids[1] > 0);
                                    default:
                                        break;
                                }
                                return false;
                            }

                            CueType m_CueType;
                            int m_MeshId;
                            int m_Ids[4];
                        };


                        static CModelTransformation* CreateFromXML(const Xml::CXmlNode* pXmlModelTransformation, CModelMultipleMesh* pModelMultipleMesh);


                        CModelTransformation(const TransformationType CurrentTransformationType, const int DoFs, const GeometricCueSource::CueType CurrentCueType, const int MeshId, const int IdA, const int IdB = -1, const int IdC = -1, const int IdD = -1):
                            m_InstanceId(++s_IdCounter),
                            m_IsValid(false),
                            m_DoFs(DoFs),
                            m_TransformationType(CurrentTransformationType),
                            m_GeometricCueSource_(CurrentCueType, MeshId, IdA, IdB, IdC, IdD)
                        {
                        }

                        CModelTransformation(const int InstanceId, const TransformationType CurrentTransformationType, const int DoFs):
                            m_InstanceId(InstanceId),
                            m_IsValid(false),
                            m_DoFs(DoFs),
                            m_TransformationType(CurrentTransformationType),
                            m_GeometricCueSource_()
                        {
                        }

                        virtual ~CModelTransformation()
                        {
                            Clear();
                        }

                        inline int GetInstanceId() const
                        {
                            return m_InstanceId;
                        }

                        inline bool IsValid() const
                        {
                            return m_IsValid;
                        }

                        inline int GetDoFs() const
                        {
                            return m_DoFs;
                        }

                        inline TransformationType GetTransformationType() const
                        {
                            return m_TransformationType;
                        }

                        inline void SetLabel(const std::string& Label)
                        {
                            m_Label = Label;
                        }

                        inline const std::string& GetLabel() const
                        {
                            return m_Label;
                        }

                        inline const GeometricCueSource& GetGeometricCueSource() const
                        {
                            return m_GeometricCueSource_;
                        }

                        virtual void UpdateFromAligment(const Mathematics::_4D::CMatrix4D& T) = 0;


                        bool AddMesh(CModelMesh* pMesh)
                        {
                            if (!pMesh)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            const int InstanceId = pMesh->GetInstanceId();

                            if (InstanceId < 1)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            std::map<int, CModelMesh*>::iterator pKeyMesh = m_Meshes.find(InstanceId);

                            if (pKeyMesh != m_Meshes.end())
                            {
                                return true;
                            }

                            if (!m_Meshes.insert(std::pair<int, CModelMesh*>(InstanceId, pMesh)).second)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            return true;
                        }

                        bool RemoveMesh(CModelMesh* pMesh)
                        {
                            if (!pMesh)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            const int InstanceId = pMesh->GetInstanceId();

                            if (InstanceId < 1)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            std::map<int, CModelMesh*>::iterator pKeyMesh = m_Meshes.find(InstanceId);

                            if (pKeyMesh == m_Meshes.end())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            m_Meshes.erase(pKeyMesh);

                            return true;
                        }

                        bool HasMeshes() const
                        {
                            return m_Meshes.size();
                        }

                        inline const std::map<int, CModelMesh*>& GetMeshes() const
                        {
                            return m_Meshes;
                        }

                        bool SolveReferences(CModelMultipleMesh* pModelMultipleMesh)
                        {
                            if (!pModelMultipleMesh)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!m_Meshes.size())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            std::map<int, CModelMesh*>::iterator EndMeshes = m_Meshes.end();
                            for (std::map<int, CModelMesh*>::iterator pKeymesh = m_Meshes.begin(); pKeymesh != EndMeshes; ++pKeymesh)
                            {
                                CModelMesh* pMesh = pModelMultipleMesh->GetMeshById(pKeymesh->first);
                                if (!pMesh)
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }
                                m_Meshes[pMesh->GetInstanceId()] = pMesh;
                                pMesh->SetTransformation(this);
                            }

                            return true;
                        }

                        virtual void Clear()
                        {
                            if (m_Meshes.size())
                            {
                                std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                                for (std::map<int, CModelMesh*>::const_iterator pKeymersh = m_Meshes.begin(); pKeymersh != EndMeshes; ++pKeymersh)
                                {
                                    pKeymersh->second->RemoveTransformation();
                                }
                                m_Meshes.clear();
                            }

                            m_InstanceId = -1;
                            m_IsValid = false;
                            m_DoFs = -1;
                            m_TransformationType = TransformationType(-1);
                        }

                        virtual bool Deserialize(const Xml::CXmlNode* pXmlCurrentNode)
                        {
                            Clear();

                            if (!pXmlCurrentNode)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if ((pXmlCurrentNode->GetName() != _RVL_TAG_MODEL_TRANSFORMATION_))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!DeserializeAttributes(pXmlCurrentNode))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            const Xml::CXmlNode* pXmlMeshesNode = pXmlCurrentNode->GetFirstSubNode();

                            if (!pXmlMeshesNode)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (pXmlMeshesNode->GetName() != _RVL_TAG_MESH_COLLECTION_)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            const std::list<Xml::CXmlNode*>& XmlMeshes = pXmlMeshesNode->GetSubNodes();

                            std::list<Xml::CXmlNode*>::const_iterator EndMeshes = XmlMeshes.end();
                            for (std::list<Xml::CXmlNode*>::const_iterator pXmlMesh = XmlMeshes.begin(); pXmlMesh != EndMeshes; ++pXmlMesh)
                            {
                                int InstanceId = -1;

                                if (!(*pXmlMesh)->GetAttribute(_RVL_TAG_INSTANCE_ID_, InstanceId))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                                if (InstanceId < 1)
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                                if (!m_Meshes.insert(std::pair<int, CModelMesh*>(InstanceId, NULL)).second)
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }
                            }

                            return true;
                        }

                        inline int GetTotalMeshes() const
                        {
                            return m_Meshes.size();
                        }

                        virtual Xml::CXmlNode* Serialize(Xml::CXmlNode* pXmlParentNode) const
                        {
                            if (!pXmlParentNode)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            Xml::CXmlNode* pXmlCurrentNode = pXmlParentNode->CreateSubNode(_RVL_TAG_MODEL_TRANSFORMATION_);

                            if (!pXmlCurrentNode)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            if (!SerializeAttributes(pXmlCurrentNode))
                            {
                                pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            Xml::CXmlNode* pXmlMeshesNode = pXmlCurrentNode->CreateSubNode(_RVL_TAG_MESH_COLLECTION_);

                            if (!pXmlMeshesNode)
                            {
                                pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            if (!m_Meshes.size())
                            {
                                pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                            for (std::map<int, CModelMesh*>::const_iterator pKeymesh = m_Meshes.begin(); pKeymesh != EndMeshes; ++pKeymesh)
                            {
                                Xml::CXmlNode* pXmlMeshNode = pXmlMeshesNode->CreateSubNode(_RVL_TAG_MESH_);

                                if (!pXmlMeshNode)
                                {
                                    pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return NULL;
                                }

                                if (!pXmlMeshNode->SetAttribute(_RVL_TAG_INSTANCE_ID_, pKeymesh->first))
                                {
                                    pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return NULL;
                                }
                            }

                            return pXmlCurrentNode;
                        }

                    protected:

                        virtual bool DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode)
                        {
                            if (!pXmlCurrentNode)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            std::string TypeId;

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_TYPE_ID_, TypeId))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            CModelTransformation::TransformationType CurrentType = CModelTransformation::StringToTransformationType(TypeId);

                            if ((CurrentType < CModelTransformation::eTranslation) || (CurrentType > CModelTransformation::eGeneralRigid))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            int InstanceId = -1;

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_INSTANCE_ID_, InstanceId))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (InstanceId < 1)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            int DoFs = 0;

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_DOF_ID_, DoFs))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (DoFs < 1)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            std::string Label;
                            if (pXmlCurrentNode->HasAttribute(_RVL_TAG_LABEL_))
                                if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_LABEL_, Label))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                            std::string CueType;

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_GEOMETRIC_CUE_TYPE_, CueType))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            GeometricCueSource::CueType CurrentCueType =  GeometricCueSource::StringToCueType(CueType);

                            if ((CurrentCueType < GeometricCueSource::eVertexAB) || (CurrentCueType > GeometricCueSource::EdgeAB))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            int MeshId = -1;

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_CUE_MESH_ID_, MeshId))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (MeshId < 1)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            int Ids[4] = {-1, -1, -1, -1};

                            if (pXmlCurrentNode->HasAttribute(_RVL_TAG_CUE_ELEMENT_IDA_))
                            {
                                if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_CUE_ELEMENT_IDA_, Ids[0]))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                                if (Ids[0] < 1)
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }
                            }

                            if (pXmlCurrentNode->HasAttribute(_RVL_TAG_CUE_ELEMENT_IDB_))
                            {
                                if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_CUE_ELEMENT_IDB_, Ids[1]))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                                if (Ids[1] < 1)
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }
                            }

                            if (pXmlCurrentNode->HasAttribute(_RVL_TAG_CUE_ELEMENT_IDC_))
                            {
                                if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_CUE_ELEMENT_IDC_, Ids[2]))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                                if (Ids[2] < 1)
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }
                            }

                            if (pXmlCurrentNode->HasAttribute(_RVL_TAG_CUE_ELEMENT_IDD_))
                            {
                                if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_CUE_ELEMENT_IDD_, Ids[3]))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                                if (Ids[3] < 1)
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }
                            }

                            m_InstanceId = InstanceId;
                            m_DoFs = DoFs;
                            m_TransformationType = CurrentType;
                            m_Label = Label;

                            m_GeometricCueSource_.m_CueType = CurrentCueType;
                            m_GeometricCueSource_.m_MeshId = MeshId;
                            m_GeometricCueSource_.m_Ids[0] = Ids[0];
                            m_GeometricCueSource_.m_Ids[1] = Ids[1];
                            m_GeometricCueSource_.m_Ids[2] = Ids[2];
                            m_GeometricCueSource_.m_Ids[3] = Ids[3];

                            return true;
                        }

                        virtual bool SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const
                        {
                            if (!pXmlCurrentNode)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_INSTANCE_ID_, m_InstanceId))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_DOF_ID_, m_DoFs))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_TYPE_ID_, CModelTransformation::TransformationTypeToString(m_TransformationType)))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (m_Label.length())
                                if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_LABEL_, m_Label))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_GEOMETRIC_CUE_TYPE_, GeometricCueSource::CueTypeToString(m_GeometricCueSource_.m_CueType)))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_CUE_MESH_ID_, m_GeometricCueSource_.m_MeshId))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (m_GeometricCueSource_.m_Ids[0] > 0)
                                if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_CUE_ELEMENT_IDA_, m_GeometricCueSource_.m_Ids[0]))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                            if (m_GeometricCueSource_.m_Ids[1] > 0)
                                if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_CUE_ELEMENT_IDB_, m_GeometricCueSource_.m_Ids[1]))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                            if (m_GeometricCueSource_.m_Ids[2] > 0)
                                if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_CUE_ELEMENT_IDC_, m_GeometricCueSource_.m_Ids[2]))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                            if (m_GeometricCueSource_.m_Ids[3] > 0)
                                if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_CUE_ELEMENT_IDD_, m_GeometricCueSource_.m_Ids[3]))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                            return true;
                        }

                        //Usage
                        //Set -1 to apply to the base dynamic transformation, Id>=0 otherwise.
                        bool UpdateMeshes(const int Id = -1)
                        {
                            if (!m_Meshes.size())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                            for (std::map<int, CModelMesh*>::const_iterator pKeymesh = m_Meshes.begin(); pKeymesh != EndMeshes; ++pKeymesh)
                                if (!pKeymesh->second->UpdateTransformation(Id))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                            return true;
                        }

                        virtual bool VerifyValidity() = 0;

                        int m_InstanceId;
                        bool m_IsValid;
                        int m_DoFs;
                        TransformationType m_TransformationType;
                        std::string m_Label;
                        GeometricCueSource m_GeometricCueSource_;
                        std::map<int, CModelMesh*> m_Meshes;

                    private:

                        static int s_IdCounter;

                    };

                    class CModelTranslation : public CModelTransformation
                    {
                    public:

                        static CModelTranslation* CreateFromXML(const Xml::CXmlNode* pXmlModelTransformation, CModelMultipleMesh* pModelMultipleMesh)
                        {
                            if (!pXmlModelTransformation)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            if (!pModelMultipleMesh)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            CModelTranslation* pModelTranslation = new CModelTranslation();

                            if (!pModelTranslation)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            if (!pModelTranslation->Deserialize(pXmlModelTransformation))
                            {
                                delete pModelTranslation;
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            if (!pModelTranslation->IsValid())
                            {
                                delete pModelTranslation;
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            if (!pModelTranslation->SolveReferences(pModelMultipleMesh))
                            {
                                delete pModelTranslation;
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            return pModelTranslation;
                        }

                        CModelTranslation():
                            CModelTransformation(CModelTransformation::eTranslation, 1, GeometricCueSource::CueType(-1), -1, -1),
                            m_PointA(Mathematics::_3D::CVector3D::s_PlusInfinity),
                            m_PointB(Mathematics::_3D::CVector3D::s_PlusInfinity),
                            m_UnitaryTranslationDirectionAB(Mathematics::_3D::CVector3D::s_Zero),
                            m_MinimalDisplacement(RVL::g_RealPlusInfinity),
                            m_MaximalDisplacement(RVL::g_RealMinusInfinity),
                            m_DefaultDisplacement(Real(0.0)),
                            m_DisplacementRange(Real(0.0)),
                            m_CurrentNormalizedDisplacement(Real(-1.0)),
                            m_IdetifiableNormalizedDisplacement()
                        {
                        }

                        CModelTranslation(CModelMesh* pModelMesh, const CModelVertex* pModelVertexA, const CModelVertex* pModelVertexB, const Real MinimalDisplacement, const Real MaximalDisplacement, const Real DefaultDisplacement, const Real CurrentNormalizedDisplacement):
                            CModelTransformation(CModelTransformation::eTranslation, 1, GeometricCueSource::eVertexAB, pModelMesh->GetInstanceId(), pModelVertexA->GetInstanceId(), pModelVertexB->GetInstanceId()),
                            m_PointA(pModelVertexA->GetPoint(true)),
                            m_PointB(pModelVertexB->GetPoint(true)),
                            m_UnitaryTranslationDirectionAB(Mathematics::_3D::Geometry::CGeometry3D::ComputeUnitaryDirectionVector(m_PointA, m_PointB)),
                            m_MinimalDisplacement(MinimalDisplacement),
                            m_MaximalDisplacement(MaximalDisplacement),
                            m_DefaultDisplacement(DefaultDisplacement),
                            m_DisplacementRange(MaximalDisplacement - MinimalDisplacement),
                            m_CurrentNormalizedDisplacement(CurrentNormalizedDisplacement),
                            m_IdetifiableNormalizedDisplacement()
                        {
                            if (!VerifyValidity())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return;
                            }

                            if (!AddMesh(pModelMesh))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            }
                        }

                        CModelTranslation(CModelMesh* pModelMesh, const CModelEdge* pModelEdge, const Real MinimalDisplacement, const Real MaximalDisplacement, const Real DefaultDisplacement, const Real CurrentNormalizedDisplacement):
                            CModelTransformation(CModelTransformation::eTranslation, 1, GeometricCueSource::eEdgeA, pModelMesh->GetInstanceId(), pModelEdge->GetInstanceId()),
                            m_PointA(pModelEdge->GetReadOnlyVertexA()->GetPoint(false)),
                            m_PointB(pModelEdge->GetReadOnlyVertexB()->GetPoint(false)),
                            m_UnitaryTranslationDirectionAB(Mathematics::_3D::Geometry::CGeometry3D::ComputeUnitaryDirectionVector(m_PointA, m_PointB)),
                            m_MinimalDisplacement(MinimalDisplacement),
                            m_MaximalDisplacement(MaximalDisplacement),
                            m_DefaultDisplacement(DefaultDisplacement),
                            m_DisplacementRange(MaximalDisplacement - MinimalDisplacement),
                            m_CurrentNormalizedDisplacement(CurrentNormalizedDisplacement),
                            m_IdetifiableNormalizedDisplacement()
                        {
                            if (!VerifyValidity())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return;
                            }

                            if (!AddMesh(pModelMesh))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            }
                        }

                        bool Set(const Real MinimalDisplacement, const Real MaximalDisplacement, const Real DefaultDisplacement, const Real CurrentNormalizedDisplacement)
                        {
                            m_MinimalDisplacement = MinimalDisplacement;
                            m_MaximalDisplacement = MaximalDisplacement;
                            m_DefaultDisplacement = DefaultDisplacement;
                            m_DisplacementRange = MaximalDisplacement - MinimalDisplacement;
                            m_CurrentNormalizedDisplacement = CurrentNormalizedDisplacement;
                            if (!VerifyValidity())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }
                            return true;
                        }

                        bool SetParametricTranslation(const Real Displacement, const int TargetFrameId = -1)
                        {
                            if (!m_IsValid)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!IsInRangeInclusive(Displacement, m_MinimalDisplacement, m_MaximalDisplacement))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (TargetFrameId == -1)
                            {
                                m_CurrentNormalizedDisplacement = (Displacement - m_MinimalDisplacement) / (m_MaximalDisplacement - m_MinimalDisplacement);
                            }
                            else
                            {
                                const Real NormalizedDisplacement = (Displacement - m_MinimalDisplacement) / (m_MaximalDisplacement - m_MinimalDisplacement);

                                if (!SetIdetifiableNormalizedDisplacement(TargetFrameId, NormalizedDisplacement))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }
                            }

                            if (!UpdateMeshes(TargetFrameId))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            return true;
                        }

                        bool SetParametricNormalizedTranslation(const Real NormalizedDisplacement, const int TargetFrameId = -1)
                        {
                            if (!m_IsValid)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!IsInRangeInclusive(NormalizedDisplacement, Real(0), Real(1)))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (TargetFrameId == -1)
                            {
                                m_CurrentNormalizedDisplacement = NormalizedDisplacement;
                            }
                            else
                            {
                                if (!SetIdetifiableNormalizedDisplacement(TargetFrameId, NormalizedDisplacement))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }
                            }


                            if (!UpdateMeshes(TargetFrameId))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            return true;
                        }

                        Mathematics::_3D::CVector3D GetTranslation(const int TargetFrameId) const
                        {
                            if (!m_IsValid)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return Mathematics::_3D::CVector3D::s_PlusInfinity;
                            }

                            if (TargetFrameId == -1)
                            {
                                return m_UnitaryTranslationDirectionAB * GetDisplacement(m_CurrentNormalizedDisplacement);
                            }
                            else
                            {
                                Real NormalizedDisplacement = Real(-1.0);

                                if (!GetIdetifiableNormalizedDisplacement(TargetFrameId, NormalizedDisplacement))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return Mathematics::_3D::CVector3D::s_PlusInfinity;
                                }

                                return m_UnitaryTranslationDirectionAB * GetDisplacement(NormalizedDisplacement);
                            }
                        }

                        Mathematics::_3D::CVector3D ComputeTranslation(const Real NormalizedDisplacement) const
                        {
                            if (!m_IsValid)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return Mathematics::_3D::CVector3D::s_PlusInfinity;
                            }

                            if (!IsInRangeInclusive(NormalizedDisplacement, Real(0), Real(1)))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return Mathematics::_3D::CVector3D::s_PlusInfinity;
                            }
                            return m_UnitaryTranslationDirectionAB * GetDisplacement(NormalizedDisplacement);
                        }

                        inline const Mathematics::_3D::CVector3D& GetPointA() const
                        {
                            return m_PointA;
                        }

                        inline const Mathematics::_3D::CVector3D& GetPointB() const
                        {
                            return m_PointB;
                        }

                        inline Real GetMinimalDisplacement() const
                        {
                            return m_MinimalDisplacement;
                        }

                        inline Real GetMaximalDisplacement() const
                        {
                            return m_MaximalDisplacement;
                        }

                        inline Real GetDefaultDisplacement() const
                        {
                            return m_DefaultDisplacement;
                        }

                        inline Real GetDisplacementRange() const
                        {
                            return m_DisplacementRange;
                        }

                        inline Real GetCurrentNormalizedDisplacement() const
                        {
                            return m_CurrentNormalizedDisplacement;
                        }

                        inline Real GetCurrentDisplacement() const
                        {
                            return GetDisplacement(m_CurrentNormalizedDisplacement);
                        }

                        bool DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode) override
                        {
                            if (!pXmlCurrentNode)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!CModelTransformation::DeserializeAttributes(pXmlCurrentNode))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real AX = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_A_X_, AX))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real AY = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_A_Y_, AY))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real AZ = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_A_Z_, AZ))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real BX = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_B_X_, BX))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real BY = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_B_Y_, BY))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real BZ = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_B_Z_, BZ))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real UDX = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_UNITARY_DIRECTION_X_, UDX))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real UDY = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_UNITARY_DIRECTION_Y_, UDY))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real UDZ = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_UNITARY_DIRECTION_Z_, UDZ))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real MinimalDisplacement = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_MINIMAL_DISPLACEMENT_, MinimalDisplacement))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real MaximalDisplacement = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_MAXIMAL_DISPLACEMENT_, MaximalDisplacement))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real DefaultDisplacement = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_DEFAULT_DISPLACEMENT_, DefaultDisplacement))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real DisplacementRange = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_DISPLACEMENT_RANGE_, DisplacementRange))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real CurrentNormalizedDisplacement = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_CURRENT_NORMALIZED_DISPLACEMENT_, CurrentNormalizedDisplacement))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }


                            m_PointA.Set(AX, AY, AZ);
                            m_PointB.Set(BX, BY, BZ);
                            m_UnitaryTranslationDirectionAB.Set(UDX, UDY, UDZ);
                            m_MinimalDisplacement = MinimalDisplacement;
                            m_MaximalDisplacement = MaximalDisplacement;
                            m_DefaultDisplacement = DefaultDisplacement;
                            m_DisplacementRange = DisplacementRange;
                            m_CurrentNormalizedDisplacement = CurrentNormalizedDisplacement;

                            if (!VerifyValidity())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            return true;
                        }

                        bool SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const override
                        {
                            if (!pXmlCurrentNode)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!CModelTransformation::SerializeAttributes(pXmlCurrentNode))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_A_X_, m_PointA.GetX()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_A_Y_, m_PointA.GetY()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_A_Z_, m_PointA.GetZ()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_B_X_, m_PointB.GetX()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_B_Y_, m_PointB.GetY()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_B_Z_, m_PointB.GetZ()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_UNITARY_DIRECTION_X_, m_UnitaryTranslationDirectionAB.GetX()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_UNITARY_DIRECTION_Y_, m_UnitaryTranslationDirectionAB.GetY()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_UNITARY_DIRECTION_Z_, m_UnitaryTranslationDirectionAB.GetZ()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_MINIMAL_DISPLACEMENT_, m_MinimalDisplacement))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_MAXIMAL_DISPLACEMENT_, m_MaximalDisplacement))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_DEFAULT_DISPLACEMENT_, m_DefaultDisplacement))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_DISPLACEMENT_RANGE_, m_DisplacementRange))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_CURRENT_NORMALIZED_DISPLACEMENT_, m_CurrentNormalizedDisplacement))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            return true;
                        }

                        bool HasIdetifiableNormalizedDisplacement(const int TargetFrameId) const
                        {
                            if (TargetFrameId < 0)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            return m_IdetifiableNormalizedDisplacement.find(TargetFrameId) != m_IdetifiableNormalizedDisplacement.end();
                        }

                        bool CreateIdetifiableNormalizedDisplacement(const int TargetFrameId)
                        {
                            if (TargetFrameId < 0)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (m_IdetifiableNormalizedDisplacement.find(TargetFrameId) != m_IdetifiableNormalizedDisplacement.end())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            m_IdetifiableNormalizedDisplacement[TargetFrameId] = m_CurrentNormalizedDisplacement;

                            if (m_Meshes.size())
                            {
                                std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                                for (std::map<int, CModelMesh*>::const_iterator pKeymesh = m_Meshes.begin(); pKeymesh != EndMeshes; ++pKeymesh)
                                {
                                    if (!pKeymesh->second->HasIdentifiableDynamicFrame(TargetFrameId))
                                    {
                                        if (!pKeymesh->second->CreateIdentifiableDynamicFrame(TargetFrameId))
                                        {
                                            std::ostringstream TextStream;
                                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                            return false;
                                        }
                                    }
                                }
                            }

                            return true;
                        }

                        bool GetIdetifiableNormalizedDisplacement(const int TargetFrameId, Real& Displacement) const
                        {
                            if (TargetFrameId < 0)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            std::map<int, Real>::const_iterator pIdNormalizedDisplacement = m_IdetifiableNormalizedDisplacement.find(TargetFrameId);

                            if (pIdNormalizedDisplacement == m_IdetifiableNormalizedDisplacement.end())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Displacement = pIdNormalizedDisplacement->second;

                            return true;
                        }

                        bool SetIdetifiableNormalizedDisplacement(const int TargetFrameId, const Real Displacement)
                        {
                            if (TargetFrameId < 0)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (m_IdetifiableNormalizedDisplacement.find(TargetFrameId) == m_IdetifiableNormalizedDisplacement.end())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            m_IdetifiableNormalizedDisplacement[TargetFrameId] = Displacement;

                            return true;
                        }

                        void ClearIdetifiableNormalizedDisplacements()
                        {
                            m_IdetifiableNormalizedDisplacement.clear();
                        }

                        int GetTotalIdetifiableNormalizedDisplacements() const
                        {
                            return int(m_IdetifiableNormalizedDisplacement.size());
                        }

                        void UpdateFromAligment(const Mathematics::_4D::CMatrix4D& T) override
                        {
                            T.RigidTransform(m_PointA, m_PointA);
                            T.RigidTransform(m_PointB, m_PointB);
                            m_UnitaryTranslationDirectionAB = Mathematics::_3D::Geometry::CGeometry3D::ComputeUnitaryDirectionVector(m_PointA, m_PointB);
                        }

                    protected:

                        inline Real GetDisplacement(const Real NormalizedDisplacement) const
                        {
                            return m_DisplacementRange * NormalizedDisplacement + m_MinimalDisplacement;
                        }

                        bool VerifyValidity() override
                        {
                            m_IsValid = m_GeometricCueSource_.IsValid() &&
                                        m_PointA.IsNotAtInfinity() &&
                                        m_PointB.IsNotAtInfinity() &&
                                        m_UnitaryTranslationDirectionAB.IsUnitary() &&
                                        IsNotAtInfinity(m_MinimalDisplacement) &&
                                        IsNotAtInfinity(m_MaximalDisplacement) &&
                                        IsInRangeInclusive(m_DefaultDisplacement, m_MinimalDisplacement, m_MaximalDisplacement) &&
                                        IsPositive(m_DisplacementRange) &&
                                        IsInRangeInclusive(m_CurrentNormalizedDisplacement, Real(0), Real(1));
                            return m_IsValid;
                        }

                        Mathematics::_3D::CVector3D m_PointA;
                        Mathematics::_3D::CVector3D m_PointB;
                        Mathematics::_3D::CVector3D m_UnitaryTranslationDirectionAB;
                        Real m_MinimalDisplacement;
                        Real m_MaximalDisplacement;
                        Real m_DefaultDisplacement;
                        Real m_DisplacementRange;
                        Real m_CurrentNormalizedDisplacement;
                        std::map<int, Real> m_IdetifiableNormalizedDisplacement;
                    };

                    /*------------------------------------------------------------------------------------------------*/

                    class CModelRotation : public CModelTransformation
                    {
                    public:

                        static bool RotateFrame(const Mathematics::_3D::CVector3D& BasePoint, const Mathematics::_3D::CMatrix3D& RotationMatrix, const Mathematics::_4D::CMatrix4D& SourceFrame, Mathematics::_4D::CMatrix4D& DestinationFrame)
                        {

                            const Mathematics::_3D::CVector3D C_ = Mathematics::_3D::CVector3D(SourceFrame[0][3], SourceFrame[1][3], SourceFrame[2][3]);
                            const Mathematics::_3D::CVector3D PX_ = Mathematics::_3D::CVector3D(SourceFrame[0][0], SourceFrame[1][0], SourceFrame[2][0]) + C_;
                            const Mathematics::_3D::CVector3D PY_ = Mathematics::_3D::CVector3D(SourceFrame[0][1], SourceFrame[1][1], SourceFrame[2][1]) + C_;
                            const Mathematics::_3D::CVector3D PZ_ = Mathematics::_3D::CVector3D(SourceFrame[0][2], SourceFrame[1][2], SourceFrame[2][2]) + C_;

                            const Mathematics::_3D::CVector3D RC = C_ - BasePoint;
                            const Mathematics::_3D::CVector3D RPX = PX_ - BasePoint;
                            const Mathematics::_3D::CVector3D RPY = PY_ - BasePoint;
                            const Mathematics::_3D::CVector3D RPZ = PZ_ - BasePoint;

                            const Mathematics::_3D::CVector3D& MRC = RotationMatrix * RC;
                            const Mathematics::_3D::CVector3D& MRPX = RotationMatrix * RPX;
                            const Mathematics::_3D::CVector3D& MRPY = RotationMatrix * RPY;
                            const Mathematics::_3D::CVector3D& MRPZ = RotationMatrix * RPZ;

                            const Mathematics::_3D::CVector3D C = MRC + BasePoint;
                            const Mathematics::_3D::CVector3D PX = MRPX + BasePoint;
                            const Mathematics::_3D::CVector3D PY = MRPY + BasePoint;
                            const Mathematics::_3D::CVector3D PZ = MRPZ + BasePoint;

                            const Mathematics::_3D::CVector3D DPX = PX - C;
                            const Mathematics::_3D::CVector3D DPY = PY - C;
                            const Mathematics::_3D::CVector3D DPZ = PZ - C;

                            DestinationFrame[0][0] = DPX.GetX();
                            DestinationFrame[1][0] = DPX.GetY();
                            DestinationFrame[2][0] = DPX.GetZ();
                            DestinationFrame[3][0] = Real(0);

                            DestinationFrame[0][1] = DPY.GetX();
                            DestinationFrame[1][1] = DPY.GetY();
                            DestinationFrame[2][1] = DPY.GetZ();
                            DestinationFrame[3][1] = Real(0);

                            DestinationFrame[0][2] = DPZ.GetX();
                            DestinationFrame[1][2] = DPZ.GetY();
                            DestinationFrame[2][2] = DPZ.GetZ();
                            DestinationFrame[3][2] = Real(0);

                            DestinationFrame[0][3] = C.GetX();
                            DestinationFrame[1][3] = C.GetY();
                            DestinationFrame[2][3] = C.GetZ();
                            DestinationFrame[3][3] = Real(1);

                            return true;
                        }

                        static CModelRotation* CreateFromXML(const Xml::CXmlNode* pXmlModelTransformation, CModelMultipleMesh* pModelMultipleMesh)
                        {
                            if (!pXmlModelTransformation)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            if (!pModelMultipleMesh)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            CModelRotation* pModelRotation = new CModelRotation();

                            if (!pModelRotation)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            if (!pModelRotation->Deserialize(pXmlModelTransformation))
                            {
                                delete pModelRotation;
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            if (!pModelRotation->IsValid())
                            {
                                delete pModelRotation;
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            if (!pModelRotation->SolveReferences(pModelMultipleMesh))
                            {
                                delete pModelRotation;
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return NULL;
                            }

                            return pModelRotation;
                        }

                        CModelRotation():
                            CModelTransformation(CModelTransformation::eRotation, 1, GeometricCueSource::CueType(-1), -1, -1),
                            m_PointA(Mathematics::_3D::CVector3D::s_PlusInfinity),
                            m_PointB(Mathematics::_3D::CVector3D::s_PlusInfinity),
                            m_UnitaryRotationAxisAB(Mathematics::_3D::CVector3D::s_Zero),
                            m_MinimalRotation(RVL::g_RealPlusInfinity),
                            m_MaximalRotation(RVL::g_RealMinusInfinity),
                            m_DefaultRotation(Real(0)),
                            m_RotationRange(Real(0)),
                            m_CurrentNormalizedRotation(Real(-1)),
                            m_IdetifiableNormalizedRotation()
                        {
                        }

                        CModelRotation(CModelMesh* pModelMesh,
                                       const CModelVertex* pModelVertexA,
                                       const CModelVertex* pModelVertexB,
                                       const Real MinimalRotation,
                                       const Real MaximalRotation,
                                       const Real DefaultRotation,
                                       const Real CurrentNormalizedRotation):
                            CModelTransformation(CModelTransformation::eRotation, 1, GeometricCueSource::eVertexAB, pModelMesh->GetInstanceId(), pModelVertexA->GetInstanceId(), pModelVertexB->GetInstanceId()),
                            m_PointA(pModelVertexA->GetPoint(true)),
                            m_PointB(pModelVertexB->GetPoint(true)),
                            m_UnitaryRotationAxisAB(Mathematics::_3D::Geometry::CGeometry3D::ComputeUnitaryDirectionVector(m_PointA, m_PointB)),
                            m_MinimalRotation(MinimalRotation),
                            m_MaximalRotation(MaximalRotation),
                            m_DefaultRotation(DefaultRotation),
                            m_RotationRange(MaximalRotation - MinimalRotation),
                            m_CurrentNormalizedRotation(CurrentNormalizedRotation),
                            m_IdetifiableNormalizedRotation()
                        {
                            if (!VerifyValidity())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return;
                            }

                            if (!AddMesh(pModelMesh))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            }
                        }

                        CModelRotation(CModelMesh* pModelMesh,
                                       const CModelEdge* pModelEdge,
                                       const Real MinimalRotation,
                                       const Real MaximalRotation,
                                       const Real DefaultRotation,
                                       const Real CurrentNormalizedRotation):
                            CModelTransformation(CModelTransformation::eRotation, 1, GeometricCueSource::eEdgeA, pModelMesh->GetInstanceId(), pModelEdge->GetInstanceId()),
                            m_PointA(pModelEdge->GetReadOnlyVertexA()->GetPoint(true)),
                            m_PointB(pModelEdge->GetReadOnlyVertexB()->GetPoint(true)),
                            m_UnitaryRotationAxisAB(Mathematics::_3D::Geometry::CGeometry3D::ComputeUnitaryDirectionVector(m_PointA, m_PointB)),
                            m_MinimalRotation(MinimalRotation),
                            m_MaximalRotation(MaximalRotation),
                            m_DefaultRotation(DefaultRotation),
                            m_RotationRange(MaximalRotation - MinimalRotation),
                            m_CurrentNormalizedRotation(CurrentNormalizedRotation),
                            m_IdetifiableNormalizedRotation()
                        {
                            if (!VerifyValidity())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return;
                            }

                            if (!AddMesh(pModelMesh))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            }
                        }

                        bool Set(const Real MinimalRotation, const Real MaximalRotation, const Real DefaultRotation, const Real CurrentNormalizedRotation)
                        {
                            if (MinimalRotation < Real(0))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (MaximalRotation < Real(0))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (DefaultRotation < Real(0))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if ((CurrentNormalizedRotation < Real(0)) || (CurrentNormalizedRotation > Real(1)))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (DefaultRotation < MinimalRotation)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (DefaultRotation > MaximalRotation)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            m_MinimalRotation = MinimalRotation;
                            m_MaximalRotation = MaximalRotation;
                            m_DefaultRotation = DefaultRotation;
                            m_RotationRange = MaximalRotation - MinimalRotation;
                            m_CurrentNormalizedRotation = CurrentNormalizedRotation;

                            if (!VerifyValidity())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            return true;
                        }

                        bool SetParametricRotation(const Real Rotation, const int TargetFrameId = -1)
                        {
                            if (!m_IsValid)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!IsInRangeInclusive(Rotation, m_MinimalRotation, m_MaximalRotation))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (TargetFrameId == -1)
                            {
                                m_CurrentNormalizedRotation = (Rotation - m_MinimalRotation) / (m_MaximalRotation - m_MinimalRotation);
                            }
                            else
                            {
                                const Real NormalizedRotation = (Rotation - m_MinimalRotation) / (m_MaximalRotation - m_MinimalRotation);

                                if (!SetIdetifiableNormalizedRotation(TargetFrameId, NormalizedRotation))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }
                            }

                            if (!UpdateMeshes(TargetFrameId))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            return true;
                        }

                        bool SetParametricNormalizedRotation(const Real NormalizedRotation, const int TargetFrameId = -1)
                        {
                            if (!m_IsValid)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!IsInRangeInclusive(NormalizedRotation, Real(0), Real(1)))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (TargetFrameId == -1)
                            {
                                m_CurrentNormalizedRotation = NormalizedRotation;
                            }
                            else
                            {
                                if (!SetIdetifiableNormalizedRotation(TargetFrameId, NormalizedRotation))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }
                            }

                            if (!UpdateMeshes(TargetFrameId))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            return true;
                        }



                        bool GetRotation(const int TargetFrameId, Mathematics::_3D::CVector3D& PointA, Mathematics::_3D::CMatrix3D& RotationMatrix) const
                        {
                            if (!m_IsValid)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real RotationAngle = Real(0);

                            if (TargetFrameId == -1)
                            {
                                RotationAngle = GetRotation(m_CurrentNormalizedRotation);
                            }
                            else
                            {
                                Real NormalizedRotation = Real(-1.0);

                                if (!GetIdetifiableNormalizedRotation(TargetFrameId, NormalizedRotation))
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return false;
                                }

                                RotationAngle = GetRotation(NormalizedRotation);
                            }

                            PointA = m_PointA;

                            RotationMatrix.SetByAngleAxis(RotationAngle, m_UnitaryRotationAxisAB);

                            return true;
                        }

                        bool ComputeRotation(const Real NormalizedRotation, Mathematics::_3D::CVector3D& PointA, Mathematics::_3D::CMatrix3D& RotationMatrix) const
                        {
                            if (!m_IsValid)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!IsInRangeInclusive(NormalizedRotation, Real(0), Real(1)))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real RotationAngle = GetRotation(NormalizedRotation);

                            PointA = m_PointA;

                            RotationMatrix.SetByAngleAxis(RotationAngle, m_UnitaryRotationAxisAB);

                            return true;
                        }

                        inline const Mathematics::_3D::CVector3D& GetPointA() const
                        {
                            return m_PointA;
                        }

                        inline const Mathematics::_3D::CVector3D& GetPointB() const
                        {
                            return m_PointB;
                        }

                        inline Real GetMinimalRotation() const
                        {
                            return m_MinimalRotation;
                        }

                        inline Real GetMaximalRotation() const
                        {
                            return m_MaximalRotation;
                        }

                        inline Real GetDefaultRotation() const
                        {
                            return m_DefaultRotation;
                        }

                        inline Real GetRotationRange() const
                        {
                            return m_RotationRange;
                        }

                        inline Real GetCurrentNormalizedRotation() const
                        {
                            return m_CurrentNormalizedRotation;
                        }

                        inline Real GetCurrentRotation() const
                        {
                            return GetRotation(m_CurrentNormalizedRotation);
                        }

                        bool DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode) override
                        {
                            if (!pXmlCurrentNode)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!CModelTransformation::DeserializeAttributes(pXmlCurrentNode))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real AX = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_A_X_, AX))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real AY = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_A_Y_, AY))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real AZ = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_A_Z_, AZ))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real BX = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_B_X_, BX))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real BY = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_B_Y_, BY))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real BZ = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_POINT_B_Z_, BZ))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real UDX = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_UNITARY_DIRECTION_X_, UDX))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real UDY = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_UNITARY_DIRECTION_Y_, UDY))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real UDZ = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_UNITARY_DIRECTION_Z_, UDZ))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real MinimalRotation = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_MINIMAL_ROTATION_, MinimalRotation))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real MaximalRotation = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_MAXIMAL_ROTATION_, MaximalRotation))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real DefaultRotation = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_DEFAULT_ROTATION_, DefaultRotation))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real RotationRange = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_ROTATION_RANGE_, RotationRange))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Real CurrentNormalizedRotation = Real(0.0);

                            if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_CURRENT_NORMALIZED_ROTATION_, CurrentNormalizedRotation))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }


                            m_PointA.Set(AX, AY, AZ);
                            m_PointB.Set(BX, BY, BZ);
                            m_UnitaryRotationAxisAB.Set(UDX, UDY, UDZ);
                            m_MinimalRotation = MinimalRotation;
                            m_MaximalRotation = MaximalRotation;
                            m_DefaultRotation = DefaultRotation;
                            m_RotationRange = RotationRange;
                            m_CurrentNormalizedRotation = CurrentNormalizedRotation;

                            if (!VerifyValidity())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            return true;
                        }

                        bool SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const override
                        {
                            if (!pXmlCurrentNode)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!CModelTransformation::SerializeAttributes(pXmlCurrentNode))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_A_X_, m_PointA.GetX()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_A_Y_, m_PointA.GetY()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_A_Z_, m_PointA.GetZ()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_B_X_, m_PointB.GetX()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_B_Y_, m_PointB.GetY()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_POINT_B_Z_, m_PointB.GetZ()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_UNITARY_DIRECTION_X_, m_UnitaryRotationAxisAB.GetX()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_UNITARY_DIRECTION_Y_, m_UnitaryRotationAxisAB.GetY()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_UNITARY_DIRECTION_Z_, m_UnitaryRotationAxisAB.GetZ()))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_MINIMAL_ROTATION_, m_MinimalRotation))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_MAXIMAL_ROTATION_, m_MaximalRotation))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_DEFAULT_ROTATION_, m_DefaultRotation))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_ROTATION_RANGE_, m_RotationRange))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_CURRENT_NORMALIZED_ROTATION_, m_CurrentNormalizedRotation))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            return true;
                        }

                        bool HasIdetifiableNormalizedRotation(const int TargetFrameId) const
                        {
                            if (TargetFrameId < 0)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            return m_IdetifiableNormalizedRotation.find(TargetFrameId) != m_IdetifiableNormalizedRotation.end();
                        }

                        bool CreateIdetifiableNormalizedRotation(const int TargetFrameId)
                        {
                            if (TargetFrameId < 0)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (m_IdetifiableNormalizedRotation.find(TargetFrameId) != m_IdetifiableNormalizedRotation.end())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            m_IdetifiableNormalizedRotation[TargetFrameId] = m_CurrentNormalizedRotation;

                            if (m_Meshes.size())
                            {
                                std::map<int, CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                                for (std::map<int, CModelMesh*>::const_iterator pKeymesh = m_Meshes.begin(); pKeymesh != EndMeshes; ++pKeymesh)
                                {
                                    if (!pKeymesh->second->HasIdentifiableDynamicFrame(TargetFrameId))
                                    {
                                        if (!pKeymesh->second->CreateIdentifiableDynamicFrame(TargetFrameId))
                                        {
                                            std::ostringstream TextStream;
                                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                            return false;
                                        }
                                    }
                                }
                            }

                            return true;
                        }

                        bool GetIdetifiableNormalizedRotation(const int TargetFrameId, Real& Rotation) const
                        {
                            if (TargetFrameId < 0)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            std::map<int, Real>::const_iterator pIdNormalizedRotation = m_IdetifiableNormalizedRotation.find(TargetFrameId);

                            if (pIdNormalizedRotation == m_IdetifiableNormalizedRotation.end())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            Rotation = pIdNormalizedRotation->second;

                            return true;
                        }

                        bool SetIdetifiableNormalizedRotation(const int TargetFrameId, const Real Rotation)
                        {
                            if (TargetFrameId < 0)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (m_IdetifiableNormalizedRotation.find(TargetFrameId) == m_IdetifiableNormalizedRotation.end())
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            m_IdetifiableNormalizedRotation[TargetFrameId] = Rotation;

                            return true;
                        }

                        void ClearIdetifiableNormalizedRotations()
                        {
                            m_IdetifiableNormalizedRotation.clear();
                        }

                        int GetTotalIdetifiableNormalizedRotations() const
                        {
                            return int(m_IdetifiableNormalizedRotation.size());
                        }


                        void UpdateFromAligment(const Mathematics::_4D::CMatrix4D& T) override
                        {
                            T.RigidTransform(m_PointA, m_PointA);
                            T.RigidTransform(m_PointB, m_PointB);
                            m_UnitaryRotationAxisAB = Mathematics::_3D::Geometry::CGeometry3D::ComputeUnitaryDirectionVector(m_PointA, m_PointB);
                        }

                    protected:

                        inline Real GetRotation(const Real NormalizedRotation) const
                        {
                            return m_RotationRange * NormalizedRotation + m_MinimalRotation;
                        }

                        bool VerifyValidity() override
                        {
                            m_IsValid = false;

                            if (!m_GeometricCueSource_.IsValid())
                            {
                                return false;
                            }

                            if (m_PointA.IsAtInfinity())
                            {
                                return false;
                            }

                            if (m_PointB.IsAtInfinity())
                            {
                                return false;
                            }

                            if (!m_UnitaryRotationAxisAB.IsUnitary())
                            {
                                return false;
                            }

                            if (IsAtInfinity(m_MinimalRotation))
                            {
                                return false;
                            }

                            if (IsAtInfinity(m_MaximalRotation))
                            {
                                return false;
                            }

                            if (!IsInRangeInclusive(m_DefaultRotation, m_MinimalRotation, m_MaximalRotation))
                            {
                                return false;
                            }

                            if (!IsPositive(m_RotationRange))
                            {
                                return false;
                            }

                            if (! IsInRangeInclusive(m_CurrentNormalizedRotation, Real(0), Real(1)))
                            {
                                return false;
                            }

                            m_IsValid = true;

                            return true;
                        }

                        Mathematics::_3D::CVector3D m_PointA;
                        Mathematics::_3D::CVector3D m_PointB;
                        Mathematics::_3D::CVector3D m_UnitaryRotationAxisAB;
                        Real m_MinimalRotation;
                        Real m_MaximalRotation;
                        Real m_DefaultRotation;
                        Real m_RotationRange;
                        Real m_CurrentNormalizedRotation;
                        std::map<int, Real> m_IdetifiableNormalizedRotation;
                    };
                }
            }
        }
    }
}

