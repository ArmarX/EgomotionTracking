/*
 * ModelOrientedVertex.cpp
 */

#include "ModelVertex.h"
#include "ModelOrientedVertex.h"
#include "ModelMesh.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    CModelOrientedVertex* CModelOrientedVertex::CreateFromXml(const Xml::CXmlNode* pXmlModelOrientedVertex, CModelMesh* pModelMesh)
                    {
                        if (!pXmlModelOrientedVertex)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        CModelOrientedVertex* pModelOrientedVertex = new CModelOrientedVertex(pModelMesh, false);

                        if (!pModelOrientedVertex->Deserialize(pXmlModelOrientedVertex))
                        {
                            delete pModelOrientedVertex;
                            return nullptr;
                        }

                        return pModelOrientedVertex;
                    }

                    CModelOrientedVertex::CModelOrientedVertex(CModelVertex* pModelVertex, const Mathematics::_3D::CVector3D& Normal, CModelMesh* pModelMesh, const bool AssignId) :
                        CModelElement(CModelElement::eOrientedVertex, pModelMesh, AssignId),
                        m_NormalInMeshIndex(-1),
                        m_pModelVertex(pModelVertex),
                        m_Normal(Normal)
                    {
                    }

                    CModelOrientedVertex::~CModelOrientedVertex()
                        = default;

                    const CModelVertex* CModelOrientedVertex::GetReadOnlyModelVertex() const
                    {
                        return m_pModelVertex;
                    }

                    CModelVertex* CModelOrientedVertex::GetWritableModelVertex()
                    {
                        return m_pModelVertex;
                    }

                    Mathematics::_3D::CVector3D CModelOrientedVertex::GetNormal(const bool ConsiderSuperior) const
                    {
                        if (ConsiderSuperior)
                        {
                            const CModelMesh* pModelMesh = dynamic_cast<const CModelMesh*>(CModelElement::GetSuperior());
                            return pModelMesh->GetFrame().Rotation(m_Normal);
                        }
                        else
                        {
                            return m_Normal;
                        }
                    }

                    void CModelOrientedVertex::Clear()
                    {
                        m_NormalInMeshIndex = -1;
                        m_pModelVertex = nullptr;
                        m_Normal.SetZero();
                    }

                    CModelMesh* CModelOrientedVertex::GetMesh()
                    {
                        return dynamic_cast<CModelMesh*>(CModelElement::GetSuperior());
                    }

                    Containers::_3D::CContinuousBoundingBox3D CModelOrientedVertex::GetBoundingBox(const bool ConsiderSuperior) const
                    {
                        return m_pModelVertex ? Containers::_3D::CContinuousBoundingBox3D(m_pModelVertex->GetBoundingBox(ConsiderSuperior)) : Containers::_3D::CContinuousBoundingBox3D();
                    }

                    bool CModelOrientedVertex::IsEnabled(const bool ConsiderSuperior) const
                    {
                        return m_pModelVertex ? m_pModelVertex->IsEnabled(ConsiderSuperior) : false;
                    }

                    void CModelOrientedVertex::SetEnabled(const bool Enabled)
                    {
                        if (m_pModelVertex)
                        {
                            m_pModelVertex->SetEnabled(Enabled);
                        }
                    }

                    CModelOrientedVertex::CModelOrientedVertex(CModelMesh* pModelMesh, const bool AssignId) :
                        CModelElement(CModelElement::eOrientedVertex, pModelMesh, AssignId),
                        m_NormalInMeshIndex(-1),
                        m_pModelVertex(nullptr),
                        m_Normal(Mathematics::_3D::CVector3D::s_Zero)
                    {
                    }

                    bool CModelOrientedVertex::DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode)
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::DeserializeAttributes(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelMesh* pContainerMesh = GetMesh();

                        if (!pContainerMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        int VertexId = -1;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_VERTEX_ID_, VertexId))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelVertex* pModelVertex = pContainerMesh->GetVertexById(VertexId);

                        if (!pModelVertex)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real NX = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_NORMAL_COORDINATE_X_, NX))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real NY = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_NORMAL_COORDINATE_Y_, NY))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real NZ = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_NORMAL_COORDINATE_Z_, NZ))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        m_pModelVertex = pModelVertex;
                        m_Normal.Set(NX, NY, NZ);

                        return true;
                    }

                    bool CModelOrientedVertex::SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::SerializeAttributes(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_VERTEX_ID_, m_pModelVertex->GetInstanceId()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_NORMAL_COORDINATE_X_, m_Normal.GetX()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_NORMAL_COORDINATE_Y_, m_Normal.GetY()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_NORMAL_COORDINATE_Z_, m_Normal.GetZ()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }
                }
            }
        }
    }
}
