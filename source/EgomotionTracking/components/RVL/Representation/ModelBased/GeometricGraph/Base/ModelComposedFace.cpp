/*
 * ModelComposedFace.cpp
 */

#include "ModelVertex.h"
#include "ModelOrientedVertex.h"
#include "ModelEdge.h"
#include "ModelFace.h"
#include "ModelComposedFace.h"
#include "ModelMesh.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    CModelComposedFace* CModelComposedFace::CreateFromXml(const Xml::CXmlNode* pXmlModelComposedFace, CModelMesh* pModelMesh)
                    {
                        if (!pXmlModelComposedFace)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        CModelComposedFace* pModelComposedFace = new CModelComposedFace(pModelMesh, false);

                        if (!pModelComposedFace)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelComposedFace->Deserialize(pXmlModelComposedFace))
                        {
                            delete pModelComposedFace;
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelMesh->AddComposedFaces(pModelComposedFace))
                        {
                            delete pModelComposedFace;
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        return pModelComposedFace;
                    }

                    CModelComposedFace::CModelComposedFace(CModelFace* pModelFace, CModelMesh* pModelMesh, const Real OrientationTolerance, const bool AssignId) :
                        CModelElement(CModelElement::eComposedFace, pModelMesh, AssignId),
                        m_Normal(Mathematics::_3D::CVector3D::s_Zero),
                        m_Area(Real(0)),
                        m_Perimeter(Real(0))
                    {
                        if (!pModelFace)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_WARNINGS_(TextStream.str(), 0);
                        }

                        CModelMesh* pContainerModelMesh = GetMesh();

                        if (!pContainerModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_WARNINGS_(TextStream.str(), 0);
                            return;
                        }

                        if (!Extraction(pModelFace, OrientationTolerance))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_WARNINGS_(TextStream.str(), 0);
                            return;
                        }

                        if (m_Faces.size() <= 1)
                        {
                            return;
                        }

                        if (!Insertion())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_WARNINGS_(TextStream.str(), 0);
                            return;
                        }

                        if (!pContainerModelMesh->AddComposedFaces(this))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_WARNINGS_(TextStream.str(), 0);
                            return;
                        }
                    }

                    CModelComposedFace::~CModelComposedFace()
                        = default;

                    bool CModelComposedFace::IsTrivial() const
                    {
                        return (m_Faces.size() < 2);
                    }

                    int CModelComposedFace::GetTotalFaces() const
                    {
                        return m_Faces.size();
                    }

                    Mathematics::_3D::CVector3D CModelComposedFace::GetNormal(const bool ConsiderSuperior) const
                    {
                        if (ConsiderSuperior)
                        {
                            const CModelMesh* pModelMesh = dynamic_cast<const CModelMesh*>(CModelElement::GetSuperior());
                            return pModelMesh->GetFrame().Rotation(m_Normal);
                        }
                        else
                        {
                            return m_Normal;
                        }
                    }

                    Mathematics::_3D::CVector3D CModelComposedFace::GetCentroid(const bool ConsiderSuperior) const
                    {
                        Mathematics::_3D::CVector3D Accumulator;
                        Real AreaAccumulator = Real(0.0);
                        std::map<int, CModelFace*>::const_iterator EndFaces = m_Faces.end();
                        for (std::map<int, CModelFace*>::const_iterator pKeyFace = m_Faces.begin(); pKeyFace != EndFaces; ++pKeyFace)
                        {
                            AreaAccumulator += pKeyFace->second->GetArea();
                            Accumulator += pKeyFace->second->GetCentroid(ConsiderSuperior) * pKeyFace->second->GetArea();
                        }
                        return Accumulator / AreaAccumulator;
                    }

                    Real CModelComposedFace::GetArea() const
                    {
                        return m_Area;
                    }

                    Real CModelComposedFace::GetPerimeter() const
                    {
                        return m_Perimeter;
                    }

                    const std::map<int, CModelFace*>& CModelComposedFace::GetFaces() const
                    {
                        return m_Faces;
                    }

                    const std::map<int, CModelEdge*>& CModelComposedFace::GetEdges() const
                    {
                        return m_Edges;
                    }

                    const std::map<int, CModelVertex*>& CModelComposedFace::GetVeritices() const
                    {
                        return m_Vertices;
                    }

                    void CModelComposedFace::Clear()
                    {
                        m_Normal.SetZero();
                        m_Area = Real(0);
                        m_Perimeter = Real(0);
                        m_Faces.clear();
                        m_Edges.clear();
                        m_Vertices.clear();
                    }

                    bool CModelComposedFace::Deserialize(const Xml::CXmlNode* pXmlCurrentNode)
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::Deserialize(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pXmlCurrentNode->GetTotalSubNodes() != 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Xml::CXmlNode* pXmlFaceReferences = pXmlCurrentNode->GetSubNodes().front();

                        if (!pXmlFaceReferences)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pXmlFaceReferences->GetName() != _RVL_TAG_FACE_COLLECTION_)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelMesh* pModelMesh = GetMesh();

                        if (!pModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const std::list<Xml::CXmlNode*>& SubNodes = pXmlFaceReferences->GetSubNodes();

                        if (!SubNodes.size())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        std::list<Xml::CXmlNode*>::const_iterator EndSubNodes = SubNodes.end();
                        for (std::list<Xml::CXmlNode*>::const_iterator ppXmlModelFace = SubNodes.begin() ; ppXmlModelFace != EndSubNodes; ++ppXmlModelFace)
                        {
                            int Id = -1;
                            if (!(*ppXmlModelFace)->GetAttribute(_RVL_TAG_INSTANCE_ID_, Id))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (Id < 1)
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                            if (!AddFace(pModelMesh->GetFaceById(Id)))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }
                        }

                        if (!Insertion())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    Xml::CXmlNode* CModelComposedFace::Serialize(Xml::CXmlNode* pXmlParentNode) const
                    {
                        if (!pXmlParentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        Xml::CXmlNode* pXmlCurrentNode = CModelElement::Serialize(pXmlParentNode);

                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        Xml::CXmlNode* pXmlFaceReferences = pXmlCurrentNode->CreateSubNode(_RVL_TAG_FACE_COLLECTION_);

                        std::map<int, CModelFace*>::const_iterator EndFaces =  m_Faces.end();
                        for (std::map<int, CModelFace*>::const_iterator pKeyModelFace = m_Faces.begin() ; pKeyModelFace != EndFaces ; ++pKeyModelFace)
                            if (!pKeyModelFace->second->SerializeReference(pXmlFaceReferences))
                            {
                                pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return nullptr;
                            }

                        return pXmlCurrentNode;
                    }

                    CModelMesh* CModelComposedFace::GetMesh()
                    {
                        return dynamic_cast<CModelMesh*>(CModelElement::GetSuperior());
                    }

                    Containers::_3D::CContinuousBoundingBox3D CModelComposedFace::GetBoundingBox() const
                    {
                        Containers::_3D::CContinuousBoundingBox3D BoundingBox;

                        for (auto m_Vertice : m_Vertices)
                        {
                            BoundingBox.Extend(m_Vertice.second->GetPoint(true));
                        }

                        return BoundingBox;
                    }

                    CModelComposedFace::CModelComposedFace(CModelMesh* pModelMesh, const bool AssignId) :
                        CModelElement(CModelElement::eComposedFace, pModelMesh, AssignId),
                        m_Normal(Mathematics::_3D::CVector3D::s_Zero),
                        m_Area(Real(0)),
                        m_Perimeter(Real(0))
                    {
                    }

                    bool CModelComposedFace::DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode)
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::DeserializeAttributes(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real NX = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_NORMAL_COORDINATE_X_, NX))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real NY = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_NORMAL_COORDINATE_Y_, NY))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real NZ = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_NORMAL_COORDINATE_Z_, NZ))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real Area = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_AREA_, Area))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real Perimeter = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_PERIMETER_, Perimeter))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        m_Normal.Set(NX, NY, NZ);
                        m_Area = Area;
                        m_Perimeter = Perimeter;

                        return true;
                    }

                    bool CModelComposedFace::SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::SerializeAttributes(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_NORMAL_COORDINATE_X_, m_Normal.GetX()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (! pXmlCurrentNode->SetAttribute(_RVL_TAG_NORMAL_COORDINATE_Y_, m_Normal.GetY()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_NORMAL_COORDINATE_Z_, m_Normal.GetZ()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_AREA_, m_Area))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_PERIMETER_, m_Perimeter))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    bool CModelComposedFace::AddVertex(CModelVertex* pModelVertex)
                    {
                        if (!pModelVertex)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const int Id = pModelVertex->GetInstanceId();

                        if (Id < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_Vertices.find(Id) != m_Vertices.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!m_Vertices.insert(std::pair<int, CModelVertex*>(Id, pModelVertex)).second)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    bool CModelComposedFace::AddEdge(CModelEdge* pModelEdge)
                    {
                        if (!pModelEdge)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const int Id = pModelEdge->GetInstanceId();

                        if (Id < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_Edges.find(Id) != m_Edges.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!m_Edges.insert(std::pair<int, CModelEdge*>(Id, pModelEdge)).second)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    bool CModelComposedFace::AddFace(CModelFace* pModelFace)
                    {
                        if (!pModelFace)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const int Id = pModelFace->GetInstanceId();

                        if (Id < 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (m_Faces.find(Id) != m_Faces.end())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!m_Faces.insert(std::pair<int, CModelFace*>(Id, pModelFace)).second)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        pModelFace->SetComposedFace(this);
                        m_Area += pModelFace->GetArea();

                        return true;
                    }

                    bool CModelComposedFace::Extraction(CModelFace* pModelFace, const Real OrientationTolerance)
                    {
                        if (!pModelFace)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pModelFace->HasComposedFace())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!AddFace(pModelFace))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        m_Normal = pModelFace->GetNormal(false);

                        std::queue<CModelFace*> ExpansionQueue;
                        ExpansionQueue.push(pModelFace);

                        while (ExpansionQueue.size())
                        {
                            CModelFace* pExpandingModelFace = ExpansionQueue.front();
                            ExpansionQueue.pop();
                            std::vector<CModelEdge*> Edges = pExpandingModelFace->GetEdges();
                            for (std::vector<CModelEdge*>::const_iterator ppModelEdge = Edges.begin() ; ppModelEdge != Edges.end() ; ++ppModelEdge)
                                if ((*ppModelEdge)->HasBothFaces())
                                {
                                    CModelFace* pModelFaceP = (*ppModelEdge)->GetWritableFaceP();

                                    if ((!pModelFaceP->HasComposedFace()) && (pModelFaceP->GetNormal(false).ScalarProduct(m_Normal) > OrientationTolerance))
                                    {
                                        if (AddFace(pModelFaceP))
                                        {
                                            ExpansionQueue.push(pModelFaceP);
                                        }
                                        else
                                        {
                                            std::ostringstream TextStream;
                                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                            return false;
                                        }
                                    }

                                    CModelFace* pModelFaceQ = (*ppModelEdge)->GetWritableFaceQ();
                                    if ((!pModelFaceQ->HasComposedFace()) && (pModelFaceQ->GetNormal(false).ScalarProduct(m_Normal) > OrientationTolerance))
                                    {
                                        if (AddFace(pModelFaceQ))
                                        {
                                            ExpansionQueue.push(pModelFaceQ);
                                        }
                                        else
                                        {
                                            std::ostringstream TextStream;
                                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                            return false;
                                        }
                                    }
                                }
                        }

                        return true;
                    }

                    bool CModelComposedFace::Insertion()
                    {
                        std::map<int, CModelFace*>::const_iterator EndFaces = m_Faces.end();
                        for (std::map<int, CModelFace*>::const_iterator ppModelFace = m_Faces.begin() ; ppModelFace != EndFaces ; ++ppModelFace)
                        {
                            std::vector<CModelEdge*> Edges = ppModelFace->second->GetEdges();
                            for (std::vector<CModelEdge*>::const_iterator ppModelEdge = Edges.begin() ; ppModelEdge != Edges.end() ; ++ppModelEdge)
                                if (!(*ppModelEdge)->IsAuxiliar())
                                {
                                    if (!AddEdge(*ppModelEdge))
                                    {
                                        std::ostringstream TextStream;
                                        TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                        _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                        return false;
                                    }
                                    m_Perimeter += (*ppModelEdge)->GetLength();
                                }
                        }

                        std::list<CModelVertex*> Vertices;
                        std::map<int, CModelEdge*>::const_iterator EndEdges = m_Edges.end();
                        for (std::map<int, CModelEdge*>::const_iterator ppModelEdge = m_Edges.begin() ; ppModelEdge != EndEdges ; ++ppModelEdge)
                        {
                            Vertices.push_back(ppModelEdge->second->GetWritableVertexA());
                            Vertices.push_back(ppModelEdge->second->GetWritableVertexB());
                        }

                        Vertices.sort();
                        Vertices.unique();

                        std::list<CModelVertex*>::const_iterator EndVertex = Vertices.end();
                        for (std::list<CModelVertex*>::const_iterator ppModelVertex = Vertices.begin() ; ppModelVertex != EndVertex ; ++ppModelVertex)
                            if (!AddVertex(*ppModelVertex))
                            {
                                std::ostringstream TextStream;
                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                return false;
                            }

                        return true;
                    }
                }
            }
        }
    }
}
