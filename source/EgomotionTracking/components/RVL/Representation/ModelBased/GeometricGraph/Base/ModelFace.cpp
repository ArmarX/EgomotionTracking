/*
 * ModelFace.cpp
 */

#include "ModelVertex.h"
#include "ModelOrientedVertex.h"
#include "ModelEdge.h"
#include "ModelFace.h"
#include "ModelMesh.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    CModelFace* CModelFace::CreateFromXml(const Xml::CXmlNode* pXmlModelFace, CModelMesh* pModelMesh)
                    {
                        if (!pXmlModelFace)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        CModelFace* pModelFace = new CModelFace(pModelMesh, false);

                        if (!pModelFace)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!pModelFace->Deserialize(pXmlModelFace))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pModelFace;
                            return nullptr;
                        }

                        if (!pModelMesh->AddFace(pModelFace))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pModelFace;
                            return nullptr;
                        }

                        return pModelFace;
                    }

                    CModelFace::CModelFace(CModelVertex* pModelVertexA, const Mathematics::_3D::CVector3D& NormalA, CModelVertex* pModelVertexB, const Mathematics::_3D::CVector3D& NormalB, CModelVertex* pModelVertexC, const Mathematics::_3D::CVector3D& NormalC, CModelMesh* pModelMesh, const bool AssignId) :
                        CModelElement(CModelElement::eFace, pModelMesh, AssignId),
                        m_OrientedVertexA(pModelVertexA, NormalA, pModelMesh, AssignId),
                        m_OrientedVertexB(pModelVertexB, NormalB, pModelMesh, AssignId),
                        m_OrientedVertexC(pModelVertexC, NormalC, pModelMesh, AssignId),
                        m_pEdgeAB(nullptr),
                        m_pEdgeBC(nullptr),
                        m_pEdgeCA(nullptr),
                        m_pComposedFace(nullptr),
                        m_Area(Real(0)),
                        m_Perimeter(Real(0))
                    {
                        if (!pModelVertexA)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return;
                        }

                        if (!pModelVertexB)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return;
                        }

                        if (!pModelVertexC)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return;
                        }

                        if (pModelVertexA == pModelVertexB)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return;
                        }

                        if (pModelVertexA == pModelVertexC)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return;
                        }

                        if (pModelVertexB == pModelVertexC)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return;
                        }

                        m_pEdgeAB = pModelVertexA->GetWritableLinkingEdge(pModelVertexB);

                        if (!m_pEdgeAB)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return;
                        }

                        m_pEdgeBC = pModelVertexB->GetWritableLinkingEdge(pModelVertexC);

                        if (!m_pEdgeBC)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return;
                        }

                        m_pEdgeCA = pModelVertexC->GetWritableLinkingEdge(pModelVertexA);

                        if (!m_pEdgeCA)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return;
                        }

                        CModelMesh* pContainerModelMesh = GetMesh();

                        if (!pContainerModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return;
                        }

                        if (!pContainerModelMesh->AddFace(this))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return;
                        }

                        if (!m_pEdgeAB->SetFace(this))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return;
                        }

                        if (!m_pEdgeBC->SetFace(this))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return;
                        }

                        if (!m_pEdgeCA->SetFace(this))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return;
                        }

                        if (!ComputeTriangleNormalBySortedDirections(m_pEdgeAB->GetDirection(false), m_pEdgeBC->GetDirection(false), NormalA, NormalB, NormalC, m_Normal))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_WARNINGS_(TextStream.str(), 0);
                            return;
                        }

                        m_Area = ComputeTriangleAreaBySides(m_pEdgeAB->GetLength(), m_pEdgeBC->GetLength(), m_pEdgeCA->GetLength());
                        m_Perimeter = m_pEdgeAB->GetLength() + m_pEdgeBC->GetLength() + m_pEdgeCA->GetLength();
                        //ComputeTriangleCentroidByVertices(m_Normal,pModelVertexA->GetPoint(false),pModelVertexB->GetPoint(false),pModelVertexC->GetPoint(false));

                    }

                    CModelFace::~CModelFace()
                        = default;

                    void CModelFace::SetSuperior(CModelElement* pSuperior)
                    {
                        CModelElement::SetSuperior(pSuperior);
                        m_OrientedVertexA.SetSuperior(pSuperior);
                        m_OrientedVertexB.SetSuperior(pSuperior);
                        m_OrientedVertexC.SetSuperior(pSuperior);
                    }

                    void CModelFace::SetComposedFace(CModelComposedFace* pComposedFace)
                    {
                        m_pComposedFace = pComposedFace;
                    }

                    bool CModelFace::HasComposedFace() const
                    {
                        return m_pComposedFace;
                    }

                    const CModelComposedFace* CModelFace::GetReadOnlyComposedFace() const
                    {
                        return m_pComposedFace;
                    }

                    Mathematics::_3D::CVector3D CModelFace::GetNormal(const bool ConsiderSuperior) const
                    {
                        if (ConsiderSuperior)
                        {
                            const CModelMesh* pModelMesh = dynamic_cast<const CModelMesh*>(CModelElement::GetSuperior());
                            return pModelMesh->GetFrame().Rotation(m_Normal);
                        }
                        else
                        {
                            return m_Normal;
                        }
                    }

                    Mathematics::_3D::CVector3D CModelFace::GetCentroid(const bool ConsiderSuperior) const
                    {
                        Mathematics::_3D::CVector3D Accumulator;
                        Accumulator += m_OrientedVertexA.GetReadOnlyModelVertex()->GetPoint(ConsiderSuperior);
                        Accumulator += m_OrientedVertexB.GetReadOnlyModelVertex()->GetPoint(ConsiderSuperior);
                        Accumulator += m_OrientedVertexC.GetReadOnlyModelVertex()->GetPoint(ConsiderSuperior);
                        return Accumulator / Real(3);
                    }

                    Real CModelFace::GetArea() const
                    {
                        return m_Area;
                    }

                    Real CModelFace::GetPerimeter() const
                    {
                        return m_Perimeter;
                    }

                    std::vector<CModelOrientedVertex*> CModelFace::GetOrientedVertices()
                    {
                        std::vector<CModelOrientedVertex*> OrientedVertices;
                        OrientedVertices.reserve(3);
                        OrientedVertices.push_back(&m_OrientedVertexA);
                        OrientedVertices.push_back(&m_OrientedVertexB);
                        OrientedVertices.push_back(&m_OrientedVertexC);
                        return OrientedVertices;
                    }

                    std::vector<Mathematics::_3D::CVector3D> CModelFace::GetVertexPoints(const bool ConsiderSuperior) const
                    {
                        std::vector<Mathematics::_3D::CVector3D> VertexPoints;
                        VertexPoints.reserve(3);
                        VertexPoints.push_back(m_OrientedVertexA.GetReadOnlyModelVertex()->GetPoint(ConsiderSuperior));
                        VertexPoints.push_back(m_OrientedVertexB.GetReadOnlyModelVertex()->GetPoint(ConsiderSuperior));
                        VertexPoints.push_back(m_OrientedVertexC.GetReadOnlyModelVertex()->GetPoint(ConsiderSuperior));
                        return VertexPoints;
                    }

                    std::vector<CModelEdge*> CModelFace::GetEdges() const
                    {
                        std::vector<CModelEdge*> Edges;
                        Edges.reserve(3);
                        Edges.push_back(m_pEdgeAB);
                        Edges.push_back(m_pEdgeBC);
                        Edges.push_back(m_pEdgeCA);
                        return Edges;
                    }

                    void CModelFace::Clear()
                    {
                        m_OrientedVertexA.Clear();
                        m_OrientedVertexB.Clear();
                        m_OrientedVertexC.Clear();
                        m_pEdgeAB = nullptr;
                        m_pEdgeBC = nullptr;
                        m_pEdgeCA = nullptr;
                        m_pComposedFace = nullptr;
                        m_Normal.SetZero();
                        m_Area = Real(0);
                        m_Perimeter = Real(0);
                    }

                    bool CModelFace::Deserialize(const Xml::CXmlNode* pXmlCurrentNode)
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::Deserialize(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pXmlCurrentNode->GetTotalSubNodes() != 1)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const Xml::CXmlNode* pXmlOrientedVertices = pXmlCurrentNode->GetFirstSubNode();
                        if (pXmlOrientedVertices->GetTotalSubNodes() != 3)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (pXmlOrientedVertices->GetName() != _RVL_TAG_ORIENTED_VERTEX_COLLECTION_)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        const std::list<Xml::CXmlNode*>& SubNodes = pXmlOrientedVertices->GetSubNodes();
                        std::list<Xml::CXmlNode*>::const_iterator ppXmlModelOrientedVertex = SubNodes.begin();

                        if (!m_OrientedVertexA.Deserialize(*ppXmlModelOrientedVertex++))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!m_OrientedVertexB.Deserialize(*ppXmlModelOrientedVertex++))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!m_OrientedVertexC.Deserialize(*ppXmlModelOrientedVertex))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    Xml::CXmlNode* CModelFace::Serialize(Xml::CXmlNode* pXmlParentNode) const
                    {
                        if (!pXmlParentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        Xml::CXmlNode* pXmlCurrentNode = CModelElement::Serialize(pXmlParentNode);

                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        Xml::CXmlNode* pXmlOrientedVertices = pXmlCurrentNode->CreateSubNode(_RVL_TAG_ORIENTED_VERTEX_COLLECTION_);

                        if (!pXmlOrientedVertices)
                        {
                            pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!m_OrientedVertexA.Serialize(pXmlOrientedVertices))
                        {
                            pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!m_OrientedVertexB.Serialize(pXmlOrientedVertices))
                        {
                            pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (!m_OrientedVertexC.Serialize(pXmlOrientedVertices))
                        {
                            pXmlParentNode->DestroySubNode(pXmlCurrentNode);
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        return pXmlCurrentNode;
                    }

                    CModelMesh* CModelFace::GetMesh()
                    {
                        return dynamic_cast<CModelMesh*>(CModelElement::GetSuperior());
                    }

                    Containers::_3D::CContinuousBoundingBox3D CModelFace::GetBoundingBox(const bool ConsiderSuperior) const
                    {
                        const CModelVertex* pModelVertexA = m_OrientedVertexA.GetReadOnlyModelVertex();

                        if (!pModelVertexA)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return Containers::_3D::CContinuousBoundingBox3D();
                        }

                        const CModelVertex* pModelVertexB = m_OrientedVertexB.GetReadOnlyModelVertex();

                        if (!pModelVertexB)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return Containers::_3D::CContinuousBoundingBox3D();
                        }

                        const CModelVertex* pModelVertexC = m_OrientedVertexC.GetReadOnlyModelVertex();

                        if (!pModelVertexC)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return Containers::_3D::CContinuousBoundingBox3D();
                        }

                        Containers::_3D::CContinuousBoundingBox3D BoundingBox(pModelVertexA->GetPoint(ConsiderSuperior));
                        BoundingBox.Extend(pModelVertexB->GetPoint(ConsiderSuperior));
                        BoundingBox.Extend(pModelVertexC->GetPoint(ConsiderSuperior));
                        return BoundingBox;
                    }

                    CModelOrientedVertex* CModelFace::GetWritableOrientedVertexA()
                    {
                        return &m_OrientedVertexA;
                    }

                    CModelOrientedVertex* CModelFace::GetWritableOrientedVertexB()
                    {
                        return &m_OrientedVertexB;
                    }

                    CModelOrientedVertex* CModelFace::GetWritableOrientedVertexC()
                    {
                        return &m_OrientedVertexC;
                    }

                    const CModelOrientedVertex* CModelFace::GetReadOnlyOrientedVertexA() const
                    {
                        return &m_OrientedVertexA;
                    }

                    const CModelOrientedVertex* CModelFace::GetReadOnlyOrientedVertexB() const
                    {
                        return &m_OrientedVertexB;
                    }

                    const CModelOrientedVertex* CModelFace::GetReadOnlyOrientedVertexC() const
                    {
                        return &m_OrientedVertexC;
                    }

                    void CModelFace::GetDynamicEndPoints(Mathematics::_3D::CVector3D& PointA, Mathematics::_3D::CVector3D& PointB, Mathematics::_3D::CVector3D& PointC) const
                    {
                        m_OrientedVertexA.m_pModelVertex->GetDynamicPoint(PointA);
                        m_OrientedVertexB.m_pModelVertex->GetDynamicPoint(PointB);
                        m_OrientedVertexC.m_pModelVertex->GetDynamicPoint(PointC);
                    }

                    CModelFace::CModelFace(CModelMesh* pModelMesh, const bool AssignId) :
                        CModelElement(CModelElement::eFace, pModelMesh, AssignId),
                        m_OrientedVertexA(pModelMesh, AssignId),
                        m_OrientedVertexB(pModelMesh, AssignId),
                        m_OrientedVertexC(pModelMesh, AssignId),
                        m_pEdgeAB(nullptr),
                        m_pEdgeBC(nullptr),
                        m_pEdgeCA(nullptr),
                        m_pComposedFace(nullptr),
                        m_Normal(Mathematics::_3D::CVector3D::s_Zero),
                        m_Area(Real(0)),
                        m_Perimeter(Real(0))
                    {
                    }

                    bool CModelFace::DeserializeAttributes(const Xml::CXmlNode* pXmlCurrentNode)
                    {
                        if (!pXmlCurrentNode)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!CModelElement::DeserializeAttributes(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelMesh* pModelMesh = GetMesh();

                        if (!pModelMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real NX = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_NORMAL_COORDINATE_X_, NX))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real NY = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_NORMAL_COORDINATE_Y_, NY))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real NZ = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_NORMAL_COORDINATE_Z_, NZ))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real Area = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_AREA_, Area))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        Real Perimeter = Real(0);

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_PERIMETER_, Perimeter))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        int EdgeABId = 0;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_EDGE_AB_ID_, EdgeABId))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelEdge* pEdgeAB = pModelMesh->GetEdgeById(EdgeABId);

                        if (!pEdgeAB)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        int EdgeBCId = 0;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_EDGE_BC_ID_, EdgeBCId))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelEdge* pEdgeBC = pModelMesh->GetEdgeById(EdgeBCId);

                        if (!pEdgeBC)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        int EdgeACId = 0;

                        if (!pXmlCurrentNode->GetAttribute(_RVL_TAG_EDGE_CA_ID_, EdgeACId))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        CModelEdge* pEdgeCA = pModelMesh->GetEdgeById(EdgeACId);

                        if (!pEdgeCA)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        m_Normal.Set(NX, NY, NZ);
                        m_Area = Area;
                        m_Perimeter = Perimeter;
                        m_pEdgeAB = pEdgeAB;
                        m_pEdgeBC = pEdgeBC;
                        m_pEdgeCA = pEdgeCA;

                        return true;
                    }

                    bool CModelFace::SerializeAttributes(Xml::CXmlNode* pXmlCurrentNode) const
                    {
                        if (!CModelElement::SerializeAttributes(pXmlCurrentNode))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_NORMAL_COORDINATE_X_, m_Normal.GetX()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_NORMAL_COORDINATE_Y_, m_Normal.GetY()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_NORMAL_COORDINATE_Z_, m_Normal.GetZ()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_AREA_, m_Area))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_PERIMETER_, m_Perimeter))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_EDGE_AB_ID_, m_pEdgeAB->GetInstanceId()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_EDGE_BC_ID_, m_pEdgeBC->GetInstanceId()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        if (!pXmlCurrentNode->SetAttribute(_RVL_TAG_EDGE_CA_ID_, m_pEdgeCA->GetInstanceId()))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return false;
                        }

                        return true;
                    }

                    bool CModelFace::ComputeTriangleNormalBySortedDirections(const Mathematics::_3D::CVector3D& DirectionA, const Mathematics::_3D::CVector3D& DirectionB, const Mathematics::_3D::CVector3D& NormalA, const Mathematics::_3D::CVector3D& NormalB, const Mathematics::_3D::CVector3D& NormalC, Mathematics::_3D::CVector3D& Normal)
                    {
                        Normal = DirectionA.VectorProduct(DirectionB);
                        if (IsPositive(Normal.Normalize()))
                        {
                            if (IsNonPositive(Normal.ScalarProduct(NormalA)) || IsNonPositive(Normal.ScalarProduct(NormalB)) || IsNonPositive(Normal.ScalarProduct(NormalC)))
                            {
                                Normal.Negate();
                            }
                            return true;
                        }
                        return false;
                    }

                    Real CModelFace::ComputeTriangleAreaBySides(const Real A, const Real B, const Real C)
                    {
                        const Real Semiperimeter = (A + B + C) * Real(0.5);
                        return std::sqrt(Semiperimeter * (Semiperimeter - A) * (Semiperimeter - B) * (Semiperimeter - C));
                    }

                    Mathematics::_3D::CVector3D CModelFace::ComputeTriangleCentroidByVertices(const Mathematics::_3D::CVector3D& Normal, const Mathematics::_3D::CVector3D& A, const Mathematics::_3D::CVector3D& B, const Mathematics::_3D::CVector3D& C)
                    {
                        const Mathematics::_3D::CVector3D MidPointAB = Mathematics::_3D::CVector3D::CreateMidPoint(A, B);
                        const Mathematics::_3D::CVector3D MidPointBC = Mathematics::_3D::CVector3D::CreateMidPoint(B, C);
                        const Mathematics::_3D::CVector3D Centroid = (A + B + C) * Real(0.33333333333333333333333333);
                        const Mathematics::_3D::CVector3D NormalAB = Mathematics::_3D::Geometry::CGeometry3D::ComputeUnitaryDirectionVector(MidPointAB, C);
                        const Mathematics::_3D::CVector3D NormalBC = Mathematics::_3D::Geometry::CGeometry3D::ComputeUnitaryDirectionVector(MidPointBC, A);
                        Mathematics::_3D::CVector3D XA, XB;
                        if (Mathematics::_3D::Geometry::CGeometry3D::ComputeLineToLineClosestPoints(MidPointAB, NormalAB, MidPointBC, NormalBC, XA, XB))
                        {
                            return Mathematics::_3D::Geometry::CGeometry3D::ComputeProjectionPointToPlane(Normal, Centroid, Mathematics::_3D::CVector3D::CreateMidPoint(XA, XB));
                        }
                        else
                        {
                            return Centroid;
                        }
                    }
                }
            }
        }
    }
}
