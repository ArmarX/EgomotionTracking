/*
 * XmlModelLoader.h
 */

#pragma once

#include "../Base/ModelMultipleMesh.h"
#include "ModelLoader.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Loaders
                {
                    class CXmlModelLoader : public CModelLoader
                    {
                    public:

                        CXmlModelLoader();
                        ~CXmlModelLoader() override;

                        Base::CModelMultipleMesh* LoadFromFile(const std::string& FileName, const CModelLoader::UnitsType DestinyUnits = eMillimeters, Processing::IOperationProgressInterface* pOperationProgressInterface = NULL) override;
                    };
                }
            }
        }
    }
}

