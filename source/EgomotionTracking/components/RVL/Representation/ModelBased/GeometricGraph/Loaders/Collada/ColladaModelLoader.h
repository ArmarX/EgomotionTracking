#pragma once


#include "../OpenInventor/OpenInventorModelLoader.h"
#include <VirtualRobot/Import/COLLADA-light/inventor.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/actions/SoWriteAction.h>


namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Loaders
                {
                    namespace SimpleCollada
                    {

                        class SimpleColladaModelLoader : public CModelLoader
                        {
                        public:
                            SimpleColladaModelLoader();

                            Base::CModelMultipleMesh* LoadFromFile(const std::string& FileName, const CModelLoader::UnitsType DestinyUnits, Processing::IOperationProgressInterface* pOperationProgressInterface) override;

                        };

                    }
                }
            }
        }
    }
}

