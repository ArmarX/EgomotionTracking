/*
 * ModelLoader.cpp
 */

#include "ModelLoader.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Loaders
                {
                    CModelLoader::CModelLoader()
                        = default;

                    CModelLoader::~CModelLoader()
                        = default;
                }
            }
        }
    }
}
