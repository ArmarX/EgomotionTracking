/*
 * OpenInventorModelLoader.cpp
 */

#include "OpenInventorModelLoader.h"



namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Loaders
                {
                    namespace OpenInventor
                    {
                        COpenInventorModelLoader::COpenInventorModelLoader(const Real VertexPointDistanceTolerance, const Real NormalOrientationTolerance, const Real MinimalEdgeLength) :
                            CModelLoader(),
                            m_VertexPointDistanceTolerance(VertexPointDistanceTolerance),
                            m_NormalOrientationTolerance(NormalOrientationTolerance),
                            m_MinimalEdgeLength(MinimalEdgeLength)
                        {
                            if (!SoDB::isInitialized())
                            {
                                SoDB::init();
                            }
                            m_ShapeHintsClassTypeId = SoShapeHints::getClassTypeId();
                            m_UnitsClassTypeId = SoUnits::getClassTypeId();
                            m_MaterialsClassTypeId = SoMaterial::getClassTypeId();
                            m_NormalClassTypeId = SoNormal::getClassTypeId();
                            m_NormalBindingClassTypeId = SoNormalBinding::getClassTypeId();
                            m_Coordinate3ClassTypeId = SoCoordinate3::getClassTypeId();
                            m_FaceSetClassTypeId = SoFaceSet::getClassTypeId();
                            m_IndexedFaceSetClassTypeId = SoIndexedFaceSet::getClassTypeId();
                            m_SeparatorClassTypeId = SoSeparator::getClassTypeId();
                            m_MatrixTransformTypeId = SoMatrixTransform::getClassTypeId();
                        }

                        COpenInventorModelLoader::~COpenInventorModelLoader()
                            = default;

                        Base::CModelMultipleMesh* COpenInventorModelLoader::LoadFromFile(const std::string& FileName, const CModelLoader::UnitsType DestinyUnits, Processing::IOperationProgressInterface* pOperationProgressInterface)
                        {
                            if (!Files::CFile::Exists(FileName))
                            {
                                return nullptr;
                            }

                            SoInput InputFile;
                            if (!InputFile.openFile(FileName.c_str()))
                            {
                                return nullptr;
                            }

                            SoSeparator* pFileRoot = SoDB::readAll(&InputFile);
                            if (!pFileRoot)
                            {
                                return nullptr;
                            }

                            pFileRoot->ref();

                            Base::CModelMultipleMesh* pModelMultipleMesh = nullptr;

                            switch (GetFileVersion(FileName))
                            {
                                case eV_1_0:

                                    pModelMultipleMesh = LoadFile_V_1_0(pFileRoot, DestinyUnits, pOperationProgressInterface);

                                    break;

                                case eV_2_1:

                                    pModelMultipleMesh = LoadFile_V_2_1(pFileRoot, DestinyUnits, pOperationProgressInterface);

                                    break;

                                default:
                                    break;
                            }

                            pFileRoot->unref();
                            return pModelMultipleMesh;
                        }

                        COpenInventorModelLoader::FileVersion COpenInventorModelLoader::GetFileVersion(const std::string& FileName)
                        {
                            std::string Content;

                            if (!Files::CInFile::ReadStringFromFile(FileName, Content))
                            {
                                return e_V_Unknown;
                            }

                            if (Content.find("#Inventor V1.0 ascii") != std::string::npos)
                            {
                                return eV_1_0;
                            }

                            if (Content.find("#Inventor V2.1 ascii") != std::string::npos)
                            {
                                return eV_2_1;
                            }

                            return e_V_Unknown;
                        }

                        Base::CModelMultipleMesh* COpenInventorModelLoader::LoadFile_V_1_0(SoSeparator* pFileRoot, const CModelLoader::UnitsType DestinyUnits, Processing::IOperationProgressInterface* pOperationProgressInterface)
                        {
                            Base::CModelMultipleMesh* pModelMultipleMesh = nullptr;
                            CModelLoader::UnitsType SourceUnits;
                            if (LoadGlobalContext_V_1_0(pFileRoot, SourceUnits))
                            {
                                std::deque<SoMesh_V_1_0> SoMeshes = ExtractSoMeshes_V_1_0(pFileRoot);
                                const int TotalMeshes = SoMeshes.size();
                                if (TotalMeshes)
                                {
                                    if (pOperationProgressInterface)
                                    {
                                        pOperationProgressInterface->SetOperationSettings(Processing::IOperationProgressInterface::eSynchronous, 0, 0, TotalMeshes - 1);
                                        pOperationProgressInterface->StartOperation(0);
                                    }

                                    Base::CModelElement::ResetInstanceIdCounter();

                                    pModelMultipleMesh = new Base::CModelMultipleMesh(true);
                                    for (auto& SoMeshe : SoMeshes)
                                    {
                                        Base::CModelMesh* pModelMesh = new Base::CModelMesh(pModelMultipleMesh, true);

                                        if (!pModelMultipleMesh->AddMesh(pModelMesh))
                                        {
                                            std::ostringstream TextStream;
                                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                        }

                                        LoadMaterial(SoMeshe.m_pMaterial, pModelMesh);

                                        std::vector<Base::CModelVertex*> Vertices = LoadMetric_V_1_0(SoMeshe.m_pCoordinate3, pModelMesh, SourceUnits, DestinyUnits);

                                        if (Vertices.size())
                                        {
                                            std::vector<Base::CModelEdge*> Edges;
                                            std::vector<Base::CModelFace*> Faces = LoadTopology_V_1_0(Vertices, SoMeshe.m_pNormalBinding, SoMeshe.m_pNormal, SoMeshe.m_pFaceSet, pModelMesh, Edges);
                                            if (Faces.size())
                                            {
                                                std::vector<Base::CModelComposedFace*> ComposedFaces = LoadMultiFaces(Faces, pModelMesh);
                                                SetVisibility(Edges);
                                                //SetVisibility(Faces);
                                                //SetVisibility(ComposedFaces);
                                            }
                                        }

                                        if (pOperationProgressInterface)
                                        {
                                            pOperationProgressInterface->SetOperationStepProgress(0);
                                        }
                                    }

                                    if (pOperationProgressInterface)
                                    {
                                        pOperationProgressInterface->FinishOperation(0);
                                    }
                                }
                            }
                            return pModelMultipleMesh;
                        }

                        Base::CModelMultipleMesh* COpenInventorModelLoader::LoadFile_V_2_1(SoSeparator* pFileRoot, const CModelLoader::UnitsType DestinyUnits, Processing::IOperationProgressInterface* pOperationProgressInterface)
                        {
                            Base::CModelMultipleMesh* pModelMultipleMesh = nullptr;

                            std::deque<SoMesh_V_2_1> SoMeshes = ExtractSoMeshes_V_2_1(pFileRoot);
                            const int TotalMeshes = SoMeshes.size();
                            if (TotalMeshes)
                            {
                                if (pOperationProgressInterface)
                                {
                                    pOperationProgressInterface->SetOperationSettings(Processing::IOperationProgressInterface::eSynchronous, 0, 0, TotalMeshes - 1);
                                    pOperationProgressInterface->StartOperation(0);
                                }

                                Base::CModelElement::ResetInstanceIdCounter();

                                pModelMultipleMesh = new Base::CModelMultipleMesh(true);

                                for (auto& SoMeshe : SoMeshes)
                                {
                                    Base::CModelMesh* pModelMesh = new Base::CModelMesh(pModelMultipleMesh, true);
                                    pModelMesh->SetLabel(SoMeshe.m_Name);

                                    if (!pModelMultipleMesh->AddMesh(pModelMesh))
                                    {
                                        std::ostringstream TextStream;
                                        TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                        _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    }

                                    LoadMaterial(SoMeshe.m_pMaterial, pModelMesh);

                                    CModelLoader::UnitsType SourceUnits = CModelLoader::eMillimeters;
                                    if (SoMeshe.m_pUnits)
                                    {
                                        switch (SoMeshe.m_pUnits->units.getValue())
                                        {
                                            case SoUnits::METERS:
                                                SourceUnits = CModelLoader::eMeters;
                                                break;
                                            case SoUnits::CENTIMETERS:
                                                SourceUnits = CModelLoader::eCentimeters;
                                                break;
                                            case SoUnits::MILLIMETERS:
                                                SourceUnits = CModelLoader::eMillimeters;
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                    std::vector<Base::CModelVertex*> Vertices = LoadMetric_V_2_1(SoMeshe.m_pMatrixTransform, SoMeshe.m_pCoordinate3, pModelMesh, SourceUnits, DestinyUnits);

                                    if (Vertices.size())
                                    {
                                        std::vector<Base::CModelEdge*> Edges;
                                        std::vector<Base::CModelFace*> Faces = LoadTopology_V_2_1(Vertices, SoMeshe.m_pIndexedFaceSet, pModelMesh, Edges);
                                        if (Faces.size())
                                        {
                                            std::vector<Base::CModelComposedFace*> ComposedFaces = LoadMultiFaces(Faces, pModelMesh);
                                            SetVisibility(Edges);
                                            //SetVisibility(Faces);
                                            //SetVisibility(ComposedFaces);
                                        }
                                    }

                                }

                                if (pOperationProgressInterface)
                                {
                                    pOperationProgressInterface->FinishOperation(0);
                                }

                            }
                            return pModelMultipleMesh;
                        }

                        bool COpenInventorModelLoader::LoadGlobalContext_V_1_0(SoSeparator* pFileRoot, CModelLoader::UnitsType& Units/*, Real* pMaterial*/) const
                        {
                            if (pFileRoot)
                            {
                                const int TotalChildren = pFileRoot->getNumChildren();
                                int LoadedElementFlags = 0;
                                for (int i = 0 ; i < TotalChildren ; ++i)
                                {
                                    SoNode* pNode = pFileRoot->getChild(i);
                                    const SoType CurrentType = pNode->getTypeId();
                                    if (CurrentType == m_ShapeHintsClassTypeId)
                                    {
                                        if (LoadedElementFlags & 0x1)
                                        {
                                            std::ostringstream TextStream;
                                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                            TextStream << "\nInternal Error in COpenInventorModelLoader::LoadGlobalContext, multiple shape hints nodes.\n";
                                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                            return false;
                                        }
                                        else
                                        {
                                            LoadedElementFlags |= 0x1;
                                            SoShapeHints* pShapeHints = dynamic_cast<SoShapeHints*>(pNode);
                                            if (pShapeHints->vertexOrdering.getValue() != SoShapeHints::COUNTERCLOCKWISE)
                                            {
                                                std::ostringstream TextStream;
                                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                                TextStream << "\nInternal Error in COpenInventorModelLoader::LoadGlobalContext, unordered vertices are not supported.\n";
                                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                                return false;
                                            }
                                            if (pShapeHints->shapeType.getValue() != SoShapeHints::SOLID)
                                            {
                                                std::ostringstream TextStream;
                                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                                TextStream << "\nInternal Error in COpenInventorModelLoader::LoadGlobalContext, non-solid objects are not supported.\n";
                                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                                return false;
                                            }
                                            if (pShapeHints->faceType.getValue() != SoShapeHints::CONVEX)
                                            {
                                                std::ostringstream TextStream;
                                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                                TextStream << "\nInternal Error in COpenInventorModelLoader::LoadGlobalContext, non-convex surfaces are not supported.\n";
                                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                                return false;
                                            }
                                        }
                                    }
                                    else if (CurrentType == m_UnitsClassTypeId)
                                    {
                                        if (LoadedElementFlags & 0x2)
                                        {
                                            std::ostringstream TextStream;
                                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                            TextStream << "\nInternal Error in COpenInventorModelLoader::LoadGlobalContext, multiple unit nodes.\n";
                                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                            return false;
                                        }
                                        else
                                        {
                                            LoadedElementFlags |= 0x2;
                                            SoUnits* pUnits = dynamic_cast<SoUnits*>(pNode);
                                            switch (pUnits->units.getValue())
                                            {
                                                case SoUnits::MILLIMETERS:
                                                    Units = eMillimeters;
                                                    break;
                                                case SoUnits::CENTIMETERS:
                                                    Units = eCentimeters;
                                                    break;
                                                case SoUnits::METERS:
                                                    Units = eMeters;
                                                    break;
                                                default:
                                                {
                                                    std::ostringstream TextStream;
                                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                                    TextStream << "\nInternal Error in COpenInventorModelLoader::LoadGlobalContext, current file units not supported.\n";
                                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                                    return false;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                                return (LoadedElementFlags == 0x3);
                            }
                            return false;
                        }

                        std::deque<COpenInventorModelLoader::SoMesh_V_1_0> COpenInventorModelLoader::ExtractSoMeshes_V_1_0(SoSeparator* pFileRoot) const
                        {
                            SoSearchAction Searcher;
                            Searcher.setFind(SoSearchAction::NODE);
                            Searcher.setInterest(SoSearchAction::FIRST);

                            std::deque<SoMesh_V_1_0> SoMeshes;
                            std::queue<SoMesh_V_1_0> SoMeshesExpansionQueue;
                            SoMeshesExpansionQueue.push(SoMesh_V_1_0(pFileRoot));
                            while (SoMeshesExpansionQueue.size())
                            {
                                SoMesh_V_1_0& CurrenMesh = SoMeshesExpansionQueue.front();
                                SoMeshesExpansionQueue.pop();
                                const int TotalChildren = CurrenMesh.m_pSoSeparator->getNumChildren();
                                if (TotalChildren == 4)
                                {
                                    int LoadedElementFlags = 0;
                                    for (int i = 0 ; i < 4 ; ++i)
                                    {
                                        SoNode* pNode = CurrenMesh.m_pSoSeparator->getChild(i);

                                        const SoType CurrentType = pNode->getTypeId();

                                        if (CurrentType == m_SeparatorClassTypeId)
                                        {
                                            SoMeshesExpansionQueue.push(SoMesh_V_1_0(dynamic_cast<SoSeparator*>(pNode)));
                                            continue;
                                        }

                                        /*std::cout << CurrentType.getName().getString() << std::endl;
                                        std::cout << m_NormalClassTypeId.getName().getString() << std::endl;
                                        std::cout << m_NormalBindingClassTypeId.getName().getString() << std::endl;
                                        std::cout << m_Coordinate3ClassTypeId.getName().getString() << std::endl;
                                        std::cout << m_FaceSetClassTypeId.getName().getString() << std::endl;

                                        SoNormal* pA =  dynamic_cast<SoNormal*>(pNode);
                                        SoNormalBinding* pB =  dynamic_cast<SoNormalBinding*>(pNode);
                                        SoCoordinate3* pC =  dynamic_cast<SoCoordinate3*>(pNode);
                                        SoFaceSet* pD =  dynamic_cast<SoFaceSet*>(pNode);*/

                                        if (CurrentType == m_NormalClassTypeId)
                                        {
                                            if (LoadedElementFlags & 0x1)
                                            {
                                                std::ostringstream TextStream;
                                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                                TextStream << "\nInternal Error in COpenInventorModelLoader::ExtractSoMeshes, internal graph structure non supported, various normal nodes.\n";
                                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                                SoMeshes.clear();
                                                return SoMeshes;
                                            }
                                            else
                                            {
                                                LoadedElementFlags |= 0x1;
                                                CurrenMesh.m_pNormal = dynamic_cast<SoNormal*>(pNode);
                                            }
                                        }
                                        else if (CurrentType == m_NormalBindingClassTypeId)
                                        {
                                            if (LoadedElementFlags & 0x2)
                                            {
                                                std::ostringstream TextStream;
                                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                                TextStream << "\nInternal Error in COpenInventorModelLoader::ExtractSoMeshes, internal graph structure non supported, various normal binding nodes.\n";
                                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                                SoMeshes.clear();
                                                return SoMeshes;
                                            }
                                            else
                                            {
                                                LoadedElementFlags |= 0x2;
                                                CurrenMesh.m_pNormalBinding = dynamic_cast<SoNormalBinding*>(pNode);
                                            }
                                        }
                                        else if (CurrentType == m_Coordinate3ClassTypeId)
                                        {
                                            if (LoadedElementFlags & 0x4)
                                            {
                                                std::ostringstream TextStream;
                                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                                TextStream << "\nInternal Error in COpenInventorModelLoader::ExtractSoMeshes, internal graph structure non supported, various coordinate nodes.\n";
                                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                                SoMeshes.clear();
                                                return SoMeshes;
                                            }
                                            else
                                            {
                                                LoadedElementFlags |= 0x4;
                                                CurrenMesh.m_pCoordinate3 = dynamic_cast<SoCoordinate3*>(pNode);
                                            }
                                        }
                                        else if (CurrentType == m_FaceSetClassTypeId)
                                        {
                                            if (LoadedElementFlags & 0x8)
                                            {
                                                std::ostringstream TextStream;
                                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                                TextStream << "\nInternal Error in COpenInventorModelLoader::ExtractSoMeshes, internal graph structure non supported, various face set nodes.\n";
                                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                                SoMeshes.clear();
                                                return SoMeshes;
                                            }
                                            else
                                            {
                                                LoadedElementFlags |= 0x8;
                                                CurrenMesh.m_pFaceSet = dynamic_cast<SoFaceSet*>(pNode);
                                            }
                                        }
                                    }
                                    if (LoadedElementFlags == 0XF)
                                    {
                                        Searcher.setNode(CurrenMesh.m_pSoSeparator);
                                        Searcher.apply(pFileRoot);
                                        SoPath* pPath = Searcher.getPath();
                                        const int Length = pPath->getLength();
                                        SoSeparator* pInferior = CurrenMesh.m_pSoSeparator;
                                        for (int i = (Length - 2); i >= 0; --i)
                                        {
                                            SoNode* pNode = pPath->getNode(i);
                                            if (pNode->getTypeId() == m_SeparatorClassTypeId)
                                            {
                                                SoSeparator* pSuperior = dynamic_cast<SoSeparator*>(pNode);
                                                const int Index = pSuperior->findChild(pInferior);
                                                for (int j = Index - 1; j >= 0; --j)
                                                    if (pSuperior->getChild(j)->getTypeId() == m_MaterialsClassTypeId)
                                                    {
                                                        CurrenMesh.m_pMaterial = dynamic_cast<SoMaterial*>(pSuperior->getChild(j));
                                                        break;
                                                    }
                                                if (CurrenMesh.m_pMaterial)
                                                {
                                                    break;
                                                }
                                                pInferior = pSuperior;
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }
                                        SoMeshes.push_back(CurrenMesh);
                                    }
                                }
                                else
                                {
                                    for (int i = 0 ; i < TotalChildren ; ++i)
                                    {
                                        SoNode* pNode = CurrenMesh.m_pSoSeparator->getChild(i);
                                        if (pNode->getTypeId() == m_SeparatorClassTypeId)
                                        {
                                            SoMeshesExpansionQueue.push(SoMesh_V_1_0(dynamic_cast<SoSeparator*>(pNode)));
                                        }
                                    }
                                }
                            }
                            return SoMeshes;
                        }

                        void COpenInventorModelLoader::processMesh(SoNode* pNode, std::deque<SoMesh_V_2_1>& SoMeshes) const
                        {
                            SoMesh_V_2_1 CurrentMesh;

                            CurrentMesh.m_pSoSeparator = dynamic_cast<SoSeparator*>(pNode);

                            const int TotalSubNodes = CurrentMesh.m_pSoSeparator->getNumChildren();

                            for (int j = 0; j < TotalSubNodes; ++j)
                            {
                                SoNode* pSubNode = CurrentMesh.m_pSoSeparator->getChild(j);

                                if (pSubNode->getTypeId() == m_MatrixTransformTypeId)
                                {
                                    CurrentMesh.m_pMatrixTransform = dynamic_cast<SoMatrixTransform*>(pSubNode);

                                    continue;
                                }

                                if (pSubNode->getTypeId() == m_MaterialsClassTypeId)
                                {
                                    CurrentMesh.m_pMaterial = dynamic_cast<SoMaterial*>(pSubNode);

                                    continue;
                                }

                                if (pSubNode->getTypeId() == m_Coordinate3ClassTypeId)
                                {
                                    CurrentMesh.m_pCoordinate3 = dynamic_cast<SoCoordinate3*>(pSubNode);

                                    continue;
                                }

                                if (pSubNode->getTypeId() == m_IndexedFaceSetClassTypeId)
                                {
                                    CurrentMesh.m_pIndexedFaceSet = dynamic_cast<SoIndexedFaceSet*>(pSubNode);

                                    continue;
                                }

                                if (pSubNode->getTypeId() == m_UnitsClassTypeId)
                                {
                                    CurrentMesh.m_pUnits = dynamic_cast<SoUnits*>(pSubNode);

                                    continue;
                                }
                            }

                            if (CurrentMesh.IsFullyLoaded())
                            {
                                CurrentMesh.m_Name = std::string(CurrentMesh.m_pSoSeparator->getName().getString());

                                SoMeshes.push_back(CurrentMesh);
                            }
                        }

                        std::deque<COpenInventorModelLoader::SoMesh_V_2_1> COpenInventorModelLoader::ExtractSoMeshes_V_2_1(SoSeparator* pFileRoot) const
                        {
                            std::deque<SoMesh_V_2_1> SoMeshes;

                            const int TotalNodes = pFileRoot->getNumChildren();

                            for (int i = 0; i < TotalNodes; ++i)
                            {
                                SoNode* pNode = pFileRoot->getChild(i);
                                std::cout << "type: " << pNode->getTypeId().getName() << std::endl;
                                if (pNode->getTypeId() == m_SeparatorClassTypeId)
                                {
                                    processMesh(pNode, SoMeshes);
                                }
                            }
                            // If none found, try to extract mesh from toplevel
                            if (SoMeshes.size() == 0)
                            {
                                processMesh(pFileRoot, SoMeshes);
                            }

                            return SoMeshes;
                        }


                        std::vector<Base::CModelVertex*> COpenInventorModelLoader::LoadMetric_V_1_0(SoCoordinate3* pCoordinate3, Base::CModelMesh* pModelMesh, const CModelLoader::UnitsType SourceUnits, const CModelLoader::UnitsType DestinyUnits) const
                        {
                            const int TotalVertices = pCoordinate3->point.getNum();

                            SbVec3f Accumulator(0.0f, 0.0f, 0.0f);
                            for (int i = 0 ; i < TotalVertices ; ++i)
                            {
                                const SbVec3f& X = pCoordinate3->point[i];
                                Accumulator += X;
                            }
                            SbVec3f Centroid = Accumulator / TotalVertices;
                            std::vector<SbVec3f> RelativePoints;
                            RelativePoints.reserve(TotalVertices);
                            for (int i = 0 ; i < TotalVertices ; ++i)
                            {
                                RelativePoints.push_back(pCoordinate3->point[i] - Centroid);
                            }

                            std::vector<Base::CModelVertex*> Vertices;
                            Vertices.reserve(TotalVertices);
                            int MeshIndex = 0;
                            const Real ScaleFactor = Real(SourceUnits) * Real(DestinyUnits);
                            for (int i = 0 ; i < TotalVertices ; ++i)
                            {
                                const SbVec3f& X = RelativePoints[i];
                                const Mathematics::_3D::CVector3D Point(Real
                                {
                                    X[0]
                                } * ScaleFactor, Real(X[1]) * ScaleFactor, Real(X[2]) * ScaleFactor);
                                Base::CModelVertex* pModelVertex = nullptr;
                                const int CurrentVerticesCardinality = Vertices.size();
                                for (int j = 0 ; j < CurrentVerticesCardinality ; ++j)
                                    if (Vertices[j]->GetDistance(Point, false) < m_VertexPointDistanceTolerance)
                                    {
                                        pModelVertex = Vertices[j];
                                        break;
                                    }
                                if (!pModelVertex)
                                {
                                    pModelVertex = new Base::CModelVertex(Point, pModelMesh, true);
                                    pModelVertex->SetVertexInMeshIndex(MeshIndex++);
                                }
                                Vertices.push_back(pModelVertex);
                            }

                            const Mathematics::_3D::CVector3D Translation(Real {Centroid[0]} * ScaleFactor, Real(Centroid[1]) * ScaleFactor, Real(Centroid[2]) * ScaleFactor);
                            Mathematics::_4D::CMatrix4D Frame;
                            Frame.MakeIdentity();
                            Frame.SetTranslation(Translation);
                            pModelMesh->SetReferenceFrame(Frame);
                            pModelMesh->SetDynamicFrame(Frame);

                            return Vertices;
                        }

                        std::vector<Base::CModelVertex*> COpenInventorModelLoader::LoadMetric_V_2_1(SoMatrixTransform* pMatrixTransform, SoCoordinate3* pCoordinate3, Base::CModelMesh* pModelMesh, const CModelLoader::UnitsType SourceUnits, const CModelLoader::UnitsType DestinyUnits) const
                        {

                            SbMatrix Transformation;
                            if (pMatrixTransform)
                            {
                                Transformation = pMatrixTransform->matrix.getValue();
                            }
                            else
                            {
                                Transformation.makeIdentity();
                            }


                            const int TotalVertices = pCoordinate3->point.getNum();

                            SbVec3f Accumulator(0.0f, 0.0f, 0.0f);
                            SbVec3f X;
                            for (int i = 0 ; i < TotalVertices ; ++i)
                            {
                                Transformation.multVecMatrix(pCoordinate3->point[i], X);
                                Accumulator += X;
                            }
                            SbVec3f Centroid = Accumulator / TotalVertices;
                            std::vector<SbVec3f> RelativePoints;
                            RelativePoints.reserve(TotalVertices);
                            for (int i = 0 ; i < TotalVertices ; ++i)
                            {
                                Transformation.multVecMatrix(pCoordinate3->point[i], X);
                                RelativePoints.push_back(X - Centroid);
                            }

                            std::vector<Base::CModelVertex*> Vertices;
                            Vertices.reserve(TotalVertices);
                            int MeshIndex = 0;
                            const Real ScaleFactor = (SourceUnits == DestinyUnits) ? 1.0f : Real(SourceUnits) * Real(DestinyUnits);
                            for (int i = 0 ; i < TotalVertices ; ++i)
                            {
                                const SbVec3f& X = RelativePoints[i];
                                const Mathematics::_3D::CVector3D Point(Real
                                {
                                    X[0]
                                } * ScaleFactor, Real(X[1]) * ScaleFactor, Real(X[2]) * ScaleFactor);
                                Base::CModelVertex* pModelVertex = new Base::CModelVertex(Point, pModelMesh, true);
                                pModelVertex->SetVertexInMeshIndex(MeshIndex++);
                                Vertices.push_back(pModelVertex);
                            }

                            const Mathematics::_3D::CVector3D Translation(Real {Centroid[0]} * ScaleFactor, Real(Centroid[1]) * ScaleFactor, Real(Centroid[2]) * ScaleFactor);
                            Mathematics::_4D::CMatrix4D Frame;
                            Frame.MakeIdentity();
                            Frame.SetTranslation(Translation);
                            pModelMesh->SetReferenceFrame(Frame);
                            pModelMesh->SetDynamicFrame(Frame);

                            return Vertices;
                        }

                        std::vector<Base::CModelFace*> COpenInventorModelLoader::LoadTopology_V_1_0(const std::vector<Base::CModelVertex*>& Vertices, SoNormalBinding* pNormalBinding, SoNormal* pNormal, SoFaceSet* pFaceSet, Base::CModelMesh* pModelMesh, std::vector<Base::CModelEdge*>& Edges) const
                        {
                            const int TotalFaces = pFaceSet->numVertices.getNum();
                            std::vector<Base::CModelFace*> Faces;
                            Faces.reserve(TotalFaces);
                            Edges.clear();
                            Edges.reserve(TotalFaces * 3);
                            switch (pNormalBinding->value.getValue())
                            {
                                case SoNormalBinding::PER_VERTEX:
                                    for (int i = 0, j = 0 ; i < TotalFaces ; ++i)
                                        switch (pFaceSet->numVertices[i])
                                        {
                                            case 3:
                                            {
                                                const SbVec3f& NormalA = pNormal->vector[j];
                                                Base::CModelVertex* pModelVertexA = Vertices[j++];
                                                const SbVec3f& NormalB = pNormal->vector[j];
                                                Base::CModelVertex* pModelVertexB = Vertices[j++];
                                                const SbVec3f& NormalC = pNormal->vector[j];
                                                Base::CModelVertex* pModelVertexC = Vertices[j++];
                                                if (!((pModelVertexA == pModelVertexB) || (pModelVertexA == pModelVertexC) || (pModelVertexB == pModelVertexC)))
                                                {
                                                    Base::CModelEdge* pModelEdgeAB = pModelVertexA->GetWritableLinkingEdge(pModelVertexB);
                                                    if (!pModelEdgeAB)
                                                    {
                                                        pModelEdgeAB = new Base::CModelEdge(pModelVertexA, pModelVertexB, pModelMesh, true);
                                                        Edges.push_back(pModelEdgeAB);
                                                    }
                                                    Base::CModelEdge* pModelEdgeBC = pModelVertexB->GetWritableLinkingEdge(pModelVertexC);
                                                    if (!pModelEdgeBC)
                                                    {
                                                        pModelEdgeBC = new Base::CModelEdge(pModelVertexB, pModelVertexC, pModelMesh, true);
                                                        Edges.push_back(pModelEdgeBC);
                                                    }
                                                    Base::CModelEdge* pModelEdgeCA = pModelVertexC->GetWritableLinkingEdge(pModelVertexA);
                                                    if (!pModelEdgeCA)
                                                    {
                                                        pModelEdgeCA = new Base::CModelEdge(pModelVertexC, pModelVertexA, pModelMesh, true);
                                                        Edges.push_back(pModelEdgeCA);
                                                    }
                                                    const Mathematics::_3D::CVector3D NA(NormalA[0], NormalA[1], NormalA[2]);
                                                    const Mathematics::_3D::CVector3D NB(NormalB[0], NormalB[1], NormalB[2]);
                                                    const Mathematics::_3D::CVector3D NC(NormalC[0], NormalC[1], NormalC[2]);
                                                    Faces.push_back(new Base::CModelFace(pModelVertexA, NA, pModelVertexB, NB, pModelVertexC, NC, pModelMesh, true));
                                                }
                                            }
                                            break;
                                            default:
                                            {
                                                std::ostringstream TextStream;
                                                TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                                TextStream << "\nInternal Error in COpenInventorModelLoader::LoadTopology, non triangular faces will not be loaded!!!\n";
                                                _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                            }
                                            break;
                                        }
                                    break;
                                default:
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    TextStream << "\nInternal Error in COpenInventorModelLoader::LoadTopology, normal to face association not implemented!!!\n";
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                }
                                break;
                            }
                            return Faces;
                        }

                        std::vector<Base::CModelFace*> COpenInventorModelLoader::LoadTopology_V_2_1(const std::vector<Base::CModelVertex*>& Vertices, SoIndexedFaceSet* pIndexedFaceSet, Base::CModelMesh* pModelMesh, std::vector<Base::CModelEdge*>& Edges) const
                        {
                            std::vector<Base::CModelFace*> Faces;

                            const int TotalIndices = pIndexedFaceSet->coordIndex.getNum();
                            int FaceIndexCount = 0;
                            int TotalFaces = 0;
                            for (int i = 0; i < TotalIndices; ++i)
                            {
                                const int Index = pIndexedFaceSet->coordIndex[i];
                                if (Index < 0)
                                {
                                    if (FaceIndexCount != 3)
                                    {
                                        std::ostringstream TextStream;
                                        TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                        TextStream << "\nInternal Error in COpenInventorModelLoader::LoadTopology, non triangular faces will not be loaded!!!\n";
                                        _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                        return Faces;
                                    }
                                    FaceIndexCount = 0;
                                    ++TotalFaces;
                                }
                                else
                                {
                                    ++FaceIndexCount;
                                }
                            }

                            Faces.reserve(TotalFaces);

                            Edges.clear();
                            Edges.reserve(TotalFaces * 3);

                            for (int i = 0, j = 0 ; i < TotalFaces ; ++i)
                            {
                                const int IndexA = pIndexedFaceSet->coordIndex[j++];
                                const int IndexB = pIndexedFaceSet->coordIndex[j++];
                                const int IndexC = pIndexedFaceSet->coordIndex[j++];
                                ++j;

                                Base::CModelVertex* pModelVertexA = Vertices[IndexA];
                                Base::CModelVertex* pModelVertexB = Vertices[IndexB];
                                Base::CModelVertex* pModelVertexC = Vertices[IndexC];

                                if (!((pModelVertexA == pModelVertexB) || (pModelVertexA == pModelVertexC) || (pModelVertexB == pModelVertexC)))
                                {
                                    Base::CModelEdge* pModelEdgeAB = pModelVertexA->GetWritableLinkingEdge(pModelVertexB);
                                    if (!pModelEdgeAB)
                                    {
                                        pModelEdgeAB = new Base::CModelEdge(pModelVertexA, pModelVertexB, pModelMesh, true);
                                        Edges.push_back(pModelEdgeAB);
                                    }
                                    Base::CModelEdge* pModelEdgeBC = pModelVertexB->GetWritableLinkingEdge(pModelVertexC);
                                    if (!pModelEdgeBC)
                                    {
                                        pModelEdgeBC = new Base::CModelEdge(pModelVertexB, pModelVertexC, pModelMesh, true);
                                        Edges.push_back(pModelEdgeBC);
                                    }
                                    Base::CModelEdge* pModelEdgeCA = pModelVertexC->GetWritableLinkingEdge(pModelVertexA);
                                    if (!pModelEdgeCA)
                                    {
                                        pModelEdgeCA = new Base::CModelEdge(pModelVertexC, pModelVertexA, pModelMesh, true);
                                        Edges.push_back(pModelEdgeCA);
                                    }

                                    const Mathematics::_3D::CVector3D& A = pModelVertexA->GetPoint(true);
                                    const Mathematics::_3D::CVector3D D0 = pModelVertexB->GetPoint(true) - A;
                                    const Mathematics::_3D::CVector3D D1 = pModelVertexC->GetPoint(true) - A;
                                    Mathematics::_3D::CVector3D N = D0.VectorProduct(D1);
                                    N.Normalize();
                                    Faces.push_back(new Base::CModelFace(pModelVertexA, N, pModelVertexB, N, pModelVertexC, N, pModelMesh, true));
                                }
                            }

                            return Faces;
                        }

                        std::vector<Base::CModelComposedFace*> COpenInventorModelLoader::LoadMultiFaces(const std::vector<Base::CModelFace*>& Faces, Base::CModelMesh* pModelMesh) const
                        {
                            const int TotalFaces = Faces.size();
                            std::vector<Base::CModelComposedFace*> ComposedFaces;
                            ComposedFaces.reserve((TotalFaces / 2) + 1);
                            for (int i = 0 ; i < TotalFaces ; ++i)
                                if (!Faces[i]->HasComposedFace())
                                {
                                    Base::CModelComposedFace* pModelMultipleFace = new Base::CModelComposedFace(Faces[i], pModelMesh, m_NormalOrientationTolerance, true);
                                    if (pModelMultipleFace->IsTrivial())
                                    {
                                        delete pModelMultipleFace;
                                    }
                                    else
                                    {
                                        ComposedFaces.push_back(pModelMultipleFace);
                                    }
                                }
                            return ComposedFaces;
                        }

                        void COpenInventorModelLoader::LoadMaterial(SoMaterial* pSoMaterial, Base::CModelMesh* pModelMesh) const
                        {
                            if (pSoMaterial)
                            {
                                if (pSoMaterial->diffuseColor.getNum() != 1)
                                {
                                    std::ostringstream TextStream;
                                    TextStream << _RVL_HEAD_FULL_INFORMATION_;
                                    TextStream << "\nInternal Error in COpenInventorModelLoader::LoadMaterial, multiple diffuse color per material node are not supported.\n";
                                    _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                                    return;
                                }
                                const SbColor Color = pSoMaterial->diffuseColor[0];
                                pModelMesh->SetMaterial(Real(Color[0]), Real(Color[1]), Real(Color[2]), Real(1.0) - Real(pSoMaterial->transparency[0]));
                            }
                            else
                            {
                                pModelMesh->SetMaterial(Real(0.5), Real(0.5), Real(0.5), Real(1.0));
                            }
                        }

                        void COpenInventorModelLoader::SetVisibility(const std::vector<Base::CModelEdge*>& Edges) const
                        {
                            const int TotalEdges = Edges.size();
                            for (int i = 0 ; i < TotalEdges ; ++i)
                                if (Edges[i]->IsAuxiliar() || (Edges[i]->GetLength() < m_MinimalEdgeLength))
                                {
                                    Edges[i]->SetVisibility(Base::CModelElement::eStaticOccluded);
                                }
                        }

                        void COpenInventorModelLoader::SetVisibility(const std::vector<Base::CModelFace*>& Faces) const
                        {
                        }

                        void COpenInventorModelLoader::SetVisibility(const std::vector<Base::CModelComposedFace*>& ComposedFaces) const
                        {
                        }
                    }
                }
            }
        }
    }
}
