/*
 * OpenInventorModelLoader.h
 */

#pragma once

#include "../../../../../Visualization/_3D/SceneGraph/OpenInventor/Base/OpenInventor.h"
#include "../../../../../Visualization/Color/Color.h"
#include "../../Base/ModelVertex.h"
#include "../../Base/ModelFace.h"
#include "../../Base/ModelMesh.h"
#include "../ModelLoader.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Loaders
                {
                    namespace OpenInventor
                    {

                        class COpenInventorModelLoader : public CModelLoader
                        {
                        public:

                            COpenInventorModelLoader(const Real VertexPointDistanceTolerance = Real(0.001), const Real NormalOrientationTolerance = Real(0.999847695), const Real MinimalEdgeLength = Real(50.0));
                            ~COpenInventorModelLoader() override;

                            Base::CModelMultipleMesh* LoadFromFile(const std::string& FileName, const CModelLoader::UnitsType DestinyUnits = CModelLoader::eMillimeters, Processing::IOperationProgressInterface* pOperationProgressInterface = NULL) override;

                            Base::CModelMultipleMesh* LoadFile_V_2_1(SoSeparator* pFileRoot, const CModelLoader::UnitsType DestinyUnits, Processing::IOperationProgressInterface* pOperationProgressInterface);


                        protected:

                            enum FileVersion
                            {
                                e_V_Unknown,

                                eV_1_0,

                                eV_2_1
                            };

                            FileVersion GetFileVersion(const std::string& FileName);

                            Base::CModelMultipleMesh* LoadFile_V_1_0(SoSeparator* pFileRoot, const CModelLoader::UnitsType DestinyUnits, Processing::IOperationProgressInterface* pOperationProgressInterface);


                            struct SoMesh_V_1_0
                            {
                                SoMesh_V_1_0() :
                                    m_pSoSeparator(NULL),
                                    m_pMaterial(NULL),
                                    m_pNormal(NULL),
                                    m_pNormalBinding(NULL),
                                    m_pCoordinate3(NULL),
                                    m_pFaceSet(NULL)
                                {
                                }

                                SoMesh_V_1_0(SoSeparator* pSoSeparator) :
                                    m_pSoSeparator(pSoSeparator),
                                    m_pMaterial(NULL),
                                    m_pNormal(NULL),
                                    m_pNormalBinding(NULL),
                                    m_pCoordinate3(NULL),
                                    m_pFaceSet(NULL)
                                {
                                }

                                SoSeparator* m_pSoSeparator;
                                SoMaterial* m_pMaterial;
                                SoNormal* m_pNormal;
                                SoNormalBinding* m_pNormalBinding;
                                SoCoordinate3* m_pCoordinate3;
                                SoFaceSet* m_pFaceSet;
                            };

                            struct SoMesh_V_2_1
                            {
                                SoMesh_V_2_1() :
                                    m_Name(""),
                                    m_pSoSeparator(NULL),
                                    m_pMatrixTransform(NULL),
                                    m_pMaterial(NULL),
                                    //m_pNormal(NULL),
                                    //m_pNormalBinding(NULL),
                                    m_pUnits(NULL),
                                    m_pCoordinate3(NULL),
                                    m_pIndexedFaceSet(NULL)
                                {

                                }

                                bool IsFullyLoaded() const
                                {
                                    return m_pSoSeparator && m_pMaterial && m_pCoordinate3 && m_pIndexedFaceSet;
                                }

                                std::string m_Name;
                                SoSeparator* m_pSoSeparator;
                                SoMatrixTransform* m_pMatrixTransform;
                                SoMaterial* m_pMaterial;

                                //SoNormal* m_pNormal;
                                //SoNormalBinding* m_pNormalBinding;
                                SoUnits* m_pUnits;
                                SoCoordinate3* m_pCoordinate3;
                                SoIndexedFaceSet* m_pIndexedFaceSet;
                            };

                            bool LoadGlobalContext_V_1_0(SoSeparator* pFileRoot, CModelLoader::UnitsType& Units/*, Real* pMaterial*/) const;
                            std::deque<SoMesh_V_1_0> ExtractSoMeshes_V_1_0(SoSeparator* pFileRoot) const;
                            std::deque<SoMesh_V_2_1> ExtractSoMeshes_V_2_1(SoSeparator* pFileRoot) const;
                            std::vector<Base::CModelVertex*> LoadMetric_V_1_0(SoCoordinate3* pCoordinate3, Base::CModelMesh* pModelMesh, const CModelLoader::UnitsType SourceUnits, const CModelLoader::UnitsType DestinyUnits) const;
                            std::vector<Base::CModelVertex*> LoadMetric_V_2_1(SoMatrixTransform* pMatrixTransform, SoCoordinate3* pCoordinate3, Base::CModelMesh* pModelMesh, const CModelLoader::UnitsType SourceUnits, const CModelLoader::UnitsType DestinyUnits) const;
                            std::vector<Base::CModelFace*> LoadTopology_V_1_0(const std::vector<Base::CModelVertex*>& Vertices, SoNormalBinding* pNormalBinding, SoNormal* pNormal, SoFaceSet* pFaceSet, Base::CModelMesh* pModelMesh, std::vector<Base::CModelEdge*>& Edges) const;
                            std::vector<Base::CModelFace*> LoadTopology_V_2_1(const std::vector<Base::CModelVertex*>& Vertices, SoIndexedFaceSet* pIndexedFaceSet, Base::CModelMesh* pModelMesh, std::vector<Base::CModelEdge*>& Edges) const;
                            std::vector<Base::CModelComposedFace*> LoadMultiFaces(const std::vector<Base::CModelFace*>& Faces, Base::CModelMesh* pModelMesh) const;
                            void LoadMaterial(SoMaterial* pSoMaterial, Base::CModelMesh* pModelMesh) const;
                            void SetVisibility(const std::vector<Base::CModelEdge*>& Edges) const;
                            void SetVisibility(const std::vector<Base::CModelFace*>& Faces) const;
                            void SetVisibility(const std::vector<Base::CModelComposedFace*>& ComposedFaces) const;
                            void processMesh(SoNode* pNode, std::deque<SoMesh_V_2_1>& SoMeshes) const;


                            SoType m_ShapeHintsClassTypeId;
                            SoType m_UnitsClassTypeId;
                            SoType m_MaterialsClassTypeId;
                            SoType m_NormalClassTypeId;
                            SoType m_NormalBindingClassTypeId;
                            SoType m_Coordinate3ClassTypeId;
                            SoType m_FaceSetClassTypeId;
                            SoType m_IndexedFaceSetClassTypeId;
                            SoType m_SeparatorClassTypeId;
                            SoType m_MatrixTransformTypeId;

                            const Real m_VertexPointDistanceTolerance;
                            const Real m_NormalOrientationTolerance;
                            const Real m_MinimalEdgeLength;
                        };
                    }
                }
            }
        }
    }
}

