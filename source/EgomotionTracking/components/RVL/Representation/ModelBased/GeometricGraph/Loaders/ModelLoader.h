/*
 * ModelLoader.h
 */

#pragma once

#include "../../../../Processing/Common/OperationProgressInterface.h"
#include "../Base/ModelMultipleMesh.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Loaders
                {
                    class CModelLoader
                    {
                    public:

                        enum UnitsType
                        {
                            eMillimeters = 1, eCentimeters = 10, eMeters = 1000
                        };

                        virtual Base::CModelMultipleMesh* LoadFromFile(const std::string& FileName, const CModelLoader::UnitsType DestinyUnits = eMillimeters, Processing::IOperationProgressInterface* pOperationProgressInterface = NULL) = 0;

                    protected:

                        CModelLoader();
                        virtual ~CModelLoader();
                    };
                }
            }
        }
    }
}

