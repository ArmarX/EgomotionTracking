#include "ColladaModelLoader.h"

using namespace RVL::Representation::ModelBased::GeometricGraph::Loaders::OpenInventor;

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Loaders
                {
                    namespace SimpleCollada
                    {
                        SimpleColladaModelLoader::SimpleColladaModelLoader()
                            = default;

                        Base::CModelMultipleMesh* SimpleColladaModelLoader::LoadFromFile(const std::string& FileName, const CModelLoader::UnitsType DestinyUnits, Processing::IOperationProgressInterface* pOperationProgressInterface)
                        {

                            const Real VertexPointDistanceTolerance = Real(0.001);
                            const Real NormalOrientationTolerance = Real(0.999847695);
                            const Real MinimalEdgeLength = VertexPointDistanceTolerance * Real(2);
                            COpenInventorModelLoader OILoader(VertexPointDistanceTolerance, NormalOrientationTolerance, MinimalEdgeLength);

                            Base::CModelMultipleMesh* modelMultipleMesh = nullptr;
                            SoSeparator* root = new SoSeparator;
                            root->ref();
                            Collada::InventorRobot robot(root);
                            robot.parse(FileName.c_str());
                            modelMultipleMesh = OILoader.LoadFile_V_2_1(root, DestinyUnits, pOperationProgressInterface);
                            root->unref();
                            return modelMultipleMesh;
                        }
                    }
                }
            }
        }
    }
}
