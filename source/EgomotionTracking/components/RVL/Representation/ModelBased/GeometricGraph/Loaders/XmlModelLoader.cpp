/*
 * XmlModelLoader.cpp
 */

#include "XmlModelLoader.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Loaders
                {
                    CXmlModelLoader::CXmlModelLoader() :
                        CModelLoader()
                    {
                    }

                    CXmlModelLoader::~CXmlModelLoader()
                        = default;

                    Base::CModelMultipleMesh* CXmlModelLoader::LoadFromFile(const std::string& FileName, const CModelLoader::UnitsType /*DestinyUnits*/, Processing::IOperationProgressInterface* /*pOperationProgressInterface*/)
                    {
                        if (!FileName.length())
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        Xml::CXmlNode* pFileNodeXML = Xml::CXmlNode::CreateFromFile(FileName);

                        if (!pFileNodeXML)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            return nullptr;
                        }

                        if (pFileNodeXML->GetName() != _LIBRARY_ACRONYM_)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pFileNodeXML;
                            return nullptr;
                        }

                        if (!pFileNodeXML->AttributeEquals(_RVL_TAG_CONTENT_, _RVL_TAG_MODEL_CONTENT_))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pFileNodeXML;
                            return nullptr;
                        }

                        if (!pFileNodeXML->AttributeEquals(_RVL_TAG_VERSION_, _MAJOR_VERSION_))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pFileNodeXML;
                            return nullptr;
                        }

                        if (!pFileNodeXML->AttributeEquals(_RVL_TAG_SUBVERSION_, _MINOR_VERSION_))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pFileNodeXML;
                            return nullptr;
                        }

                        if (!pFileNodeXML->AttributeEquals(_RVL_TAG_REVISION_, _REVISION_NUMBER_))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pFileNodeXML;
                            return nullptr;
                        }

                        if (!pFileNodeXML->AttributeEquals(_RVL_TAG_BUILD_, _BUILD_NUMBER_))
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pFileNodeXML;
                            return nullptr;
                        }

                        Xml::CXmlNode* pMultimeshNodeXML = pFileNodeXML->GetFirstSubNode();

                        if (!pMultimeshNodeXML)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pFileNodeXML;
                            return nullptr;
                        }

                        Base::CModelMultipleMesh* pModelMultipleMesh = Base::CModelMultipleMesh::CreateFromXml(pMultimeshNodeXML);

                        if (!pModelMultipleMesh)
                        {
                            std::ostringstream TextStream;
                            TextStream << _RVL_HEAD_FULL_INFORMATION_;
                            _RVL_CONSOLE_OUTPUT_ERROR_(TextStream.str(), 0);
                            delete pFileNodeXML;
                            return nullptr;
                        }

                        return pModelMultipleMesh;
                    }
                }
            }
        }
    }
}
