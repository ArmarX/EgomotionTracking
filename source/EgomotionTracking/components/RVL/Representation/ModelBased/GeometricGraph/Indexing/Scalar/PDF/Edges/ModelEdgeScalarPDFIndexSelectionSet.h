/*
 * ModelEdgeScalarPDFIndexSelectionSet.h
 */

#pragma once

#include "../../../../Base/ModelEdge.h"
#include "../Generic/TModelElementScalarPDFIndexSelectionSet.hpp"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Edges
                            {
                                class CModelEdgeScalarPDFIndexSelectionSet : public Generic::TModelElementScalarPDFIndexSelectionSet<Base::CModelElement>
                                {
                                public:

                                    CModelEdgeScalarPDFIndexSelectionSet(const int QueryId);
                                    ~CModelEdgeScalarPDFIndexSelectionSet() override;
                                };
                            }
                        }
                    }
                }
            }
        }
    }
}

