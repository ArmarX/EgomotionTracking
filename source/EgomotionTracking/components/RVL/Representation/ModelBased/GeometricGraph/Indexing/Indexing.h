/*
 * Indexing.h
 */

#pragma once

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    enum SortingOrder
                    {
                        eUnsorted, eAscendant, eDescendant
                    };

                    enum ScaleMode
                    {
                        eUnscaled, eMaximal, eIntegral
                    };
                }
            }
        }
    }
}

