/*
 * ModelEdgeScalarPDFIndexComposedSelection.h
 */

#pragma once

#include "../../../../Base/ModelEdge.h"
#include "../Generic/TModelElementScalarPDFIndexComposedSelection.hpp"
#include "ModelEdgeScalarPDFIndexSelection.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Edges
                            {
                                class CModelEdgeScalarPDFIndexComposedSelection : public Generic::TScalarPDFIndexComposedSelection<Base::CModelEdge, Base::CModelEdge>
                                {
                                public:

                                    CModelEdgeScalarPDFIndexComposedSelection(const CModelEdgeScalarPDFIndexSelection* pSelectionA, const CModelEdgeScalarPDFIndexSelection* pSelectionB);
                                    ~CModelEdgeScalarPDFIndexComposedSelection() override;
                                };
                            }
                        }
                    }
                }
            }
        }
    }
}

