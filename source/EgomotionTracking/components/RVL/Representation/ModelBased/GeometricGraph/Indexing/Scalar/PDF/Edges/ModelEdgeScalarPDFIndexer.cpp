/*
 * ModelEdgeScalarPDFIndexer.cpp
 */

#include "ModelEdgeScalarPDFIndexer.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Edges
                            {
                                CModelEdgeScalarPDFIndexer::CModelEdgeScalarPDFIndexer() :
                                    Generic::TModelElementScalarPDFIndexer<Base::CModelEdge>()
                                {
                                }

                                CModelEdgeScalarPDFIndexer::~CModelEdgeScalarPDFIndexer()
                                    = default;
                            }
                        }
                    }
                }
            }
        }
    }
}
