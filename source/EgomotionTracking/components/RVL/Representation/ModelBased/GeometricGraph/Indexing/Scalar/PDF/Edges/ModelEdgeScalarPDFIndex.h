/*
 * ModelEdgeScalarPDFIndex.h
 */

#pragma once

#include "../../../../Base/ModelEdge.h"
#include "../Generic/TModelElementScalarPDFIndex.hpp"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Edges
                            {
                                class CModelEdgeScalarPDFIndex: public Generic::TModelElementScalarPDFIndex<Base::CModelEdge>
                                {
                                public:

                                    CModelEdgeScalarPDFIndex(const Base::CModelEdge* pModelEdge);
                                    ~CModelEdgeScalarPDFIndex();
                                };
                            }
                        }
                    }
                }
            }
        }
    }
}

