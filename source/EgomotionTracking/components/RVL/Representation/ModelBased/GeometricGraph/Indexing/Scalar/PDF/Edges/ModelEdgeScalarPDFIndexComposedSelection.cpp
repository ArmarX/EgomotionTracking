/*
 * ModelEdgeScalarPDFIndexComposedSelection.cpp
 */

#include "ModelEdgeScalarPDFIndexComposedSelection.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Edges
                            {
                                CModelEdgeScalarPDFIndexComposedSelection::CModelEdgeScalarPDFIndexComposedSelection(const CModelEdgeScalarPDFIndexSelection* pSelectionA, const CModelEdgeScalarPDFIndexSelection* pSelectionB) :
                                    Generic::TScalarPDFIndexComposedSelection<Base::CModelEdge, Base::CModelEdge>(pSelectionA, pSelectionB)
                                {
                                }

                                CModelEdgeScalarPDFIndexComposedSelection::~CModelEdgeScalarPDFIndexComposedSelection()
                                    = default;
                            }
                        }
                    }
                }
            }
        }
    }
}
