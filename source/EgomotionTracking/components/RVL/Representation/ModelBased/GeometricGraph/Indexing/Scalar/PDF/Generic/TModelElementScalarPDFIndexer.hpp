/*
 * TModelElementScalarPDFIndexer.hpp
 */

#pragma once

#include "../../../../../../../Mathematics/_1D/NormalDistribution.h"
#include "../../../Indexing.h"
#include "TModelElementScalarPDFIndex.hpp"
#include "TModelElementScalarPDFIndexInterval.hpp"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Generic
                            {
                                template <typename GenericModelElement> class TModelElementScalarPDFIndexer
                                {
                                public:

                                    TModelElementScalarPDFIndexer() :
                                        m_SortingOrder(eUnsorted),
                                        m_MaximalScalar(Real(0.0)),
                                        m_MinimalScalar(Real(0.0)),
                                        m_ScalarRange(Real(0.0)),
                                        m_ScalarMedian(Real(0.0)),
                                        m_ScalarMean(Real(0.0)),
                                        m_ScalarStandardDeviation(Real(0.0))
                                    {
                                    }

                                    virtual ~TModelElementScalarPDFIndexer()
                                    {
                                        ClearIndices();
                                    }

                                    void AddElement(const GenericModelElement* pModelElement)
                                    {
                                        if (pModelElement && (m_ModelElements.find(pModelElement) == m_ModelElements.end()))
                                        {
                                            m_ModelElements.insert(pModelElement);
                                        }
                                    }

                                    bool Indexing(const SortingOrder SortingOrder)
                                    {
                                        if (LoadTargets())
                                        {
                                            if (Sorting(SortingOrder))
                                            {
                                                EstimatePDF();
                                                return true;
                                            }
                                        }
                                        return false;
                                    }

                                    inline SortingOrder GetSortingOrder() const
                                    {
                                        return m_SortingOrder;
                                    }

                                    inline Real GetMaximalScalar() const
                                    {
                                        return m_MaximalScalar;
                                    }

                                    inline Real GetMinimalScalar() const
                                    {
                                        return m_MinimalScalar;
                                    }

                                    inline Real GetScalarRange() const
                                    {
                                        return m_ScalarRange;
                                    }

                                    inline Real GetScalarMedian() const
                                    {
                                        return m_ScalarMedian;
                                    }

                                    inline Real GetScalarMean() const
                                    {
                                        return m_ScalarMean;
                                    }

                                    inline Real GetScalarStandardDeviation() const
                                    {
                                        return m_ScalarStandardDeviation;
                                    }

                                    inline const std::vector<TModelElementScalarPDFIndex<GenericModelElement>*>& GetIndices() const
                                    {
                                        return m_Indices;
                                    }

                                    inline const std::map<const GenericModelElement*, TModelElementScalarPDFIndex<GenericModelElement>*>& GetInverseIndices() const
                                    {
                                        return m_InverseIndices;
                                    }

                                    const TModelElementScalarPDFIndexInterval<GenericModelElement>* GetIndexInterval(const int QueryId, const Real ValueA, const Real ValueB)
                                    {
                                        if ((m_SortingOrder != eUnsorted) && IsNonZero(ValueB - ValueA))
                                        {
                                            const Real Maximal = std::max(ValueA, ValueB);
                                            const Real Minimal = std::min(ValueA, ValueB);
                                            if ((Minimal <= m_MaximalScalar) && (Maximal >= m_MinimalScalar))
                                            {
                                                const Real RightBound = (m_SortingOrder == eAscendant) ? Maximal : Minimal;
                                                const Real LeftBound = (m_SortingOrder == eAscendant) ? Minimal : Maximal;
                                                int A = int(0), B = int(m_Indices.size()) - int(1);
                                                if (LeftBound > m_MinimalScalar)
                                                    do
                                                    {
                                                        const int Mid = int(Real(B - A) / Real(2)) + A;
                                                        TModelElementScalarPDFIndex<GenericModelElement>* pIndexMid = m_Indices[Mid];
                                                        //if (m_Indices[Mid]->GetScalar() > Minimal)
                                                        if (pIndexMid->GetScalar() > LeftBound)
                                                        {
                                                            B = Mid;
                                                        }
                                                        else
                                                        {
                                                            A = Mid;
                                                        }
                                                    }
                                                    while ((B - A) > int(1));

                                                ///////////////////////////////////////////////////////////////////
                                                TModelElementScalarPDFIndex<GenericModelElement>* pIndexEvaluationA = m_Indices[A];
                                                TModelElementScalarPDFIndex<GenericModelElement>* pIndexEvaluationB = m_Indices[B];
                                                ///////////////////////////////////////////////////////////////////
                                                if (m_Indices[A] < LeftBound)
                                                {
                                                    A = B;
                                                }
                                                TModelElementScalarPDFIndex<GenericModelElement>* pIndexA = m_Indices[A];
                                                B = int(m_Indices.size()) - int(1);
                                                if (RightBound < m_MaximalScalar)
                                                    do
                                                    {
                                                        const int Mid = int(Real(B - A) / Real(2)) + A;
                                                        TModelElementScalarPDFIndex<GenericModelElement>* pIndexMid = m_Indices[Mid];
                                                        //if (m_Indices[Mid]->GetScalar() > Maximal)
                                                        if (pIndexMid->GetScalar() > RightBound)
                                                        {
                                                            B = Mid;
                                                        }
                                                        else
                                                        {
                                                            A = Mid;
                                                        }
                                                    }
                                                    while ((B - A) > int(1));
                                                ///////////////////////////////////////////////////////////////////
                                                pIndexEvaluationA = m_Indices[A];
                                                pIndexEvaluationB = m_Indices[B];
                                                ///////////////////////////////////////////////////////////////////
                                                TModelElementScalarPDFIndex<GenericModelElement>* pIndexB = m_Indices[(m_Indices[B]->GetScalar() > RightBound) ? A : B];
                                                return TModelElementScalarPDFIndexInterval<GenericModelElement>(QueryId, pIndexA, pIndexB, this);
                                            }
                                        }
                                        return NULL;
                                    }

                                protected:

                                    bool LoadTargets()
                                    {
                                        const int TotalElements = m_ModelElements.size();
                                        if (TotalElements)
                                        {
                                            ClearIndices();
                                            m_InverseIndices.clear();
                                            m_MaximalScalar = g_RealMinusInfinity;
                                            m_MinimalScalar = g_RealPlusInfinity;
                                            Real ScalarAccumulator = Real(0);
                                            Real ScalarSquareAccumulator = Real(0);
                                            for (typename std::set<const GenericModelElement*>::const_iterator ppTarget = m_ModelElements.begin() ; ppTarget != m_ModelElements.end() ; ++ppTarget)
                                            {
                                                TModelElementScalarPDFIndex<GenericModelElement>* pIndex = new TModelElementScalarPDFIndex<GenericModelElement>(*ppTarget);
                                                const Real Scalar = Real(pIndex->GetScalar());
                                                ScalarAccumulator += Scalar;
                                                ScalarSquareAccumulator += Scalar * Scalar;
                                                if (Scalar > m_MaximalScalar)
                                                {
                                                    m_MaximalScalar = Scalar;
                                                }
                                                if (Scalar < m_MinimalScalar)
                                                {
                                                    m_MinimalScalar = Scalar;
                                                }
                                                m_Indices.push_back(pIndex);
                                                m_InverseIndices.Insert(std::pair<const GenericModelElement*, TModelElementScalarPDFIndex<GenericModelElement>*>(*ppTarget, pIndex));
                                            }
                                            m_ModelElements.clear();
                                            m_ScalarRange = m_MaximalScalar - m_MinimalScalar;
                                            const Real Mean = ScalarAccumulator / Real(TotalElements);
                                            m_ScalarMean = Mean;
                                            m_ScalarStandardDeviation = std::sqrt((ScalarSquareAccumulator / Real(TotalElements)) - (Mean * Mean));
                                            return true;
                                        }
                                        return false;
                                    }

                                    bool Sorting(const SortingOrder SortingOrder)
                                    {
                                        const int TotalElements = m_ModelElements.size();
                                        if (TotalElements)
                                        {
                                            m_SortingOrder = SortingOrder;
                                            switch (m_SortingOrder)
                                            {
                                                case eUnsorted:
                                                    return false;
                                                case eAscendant:
                                                    std::sort(m_Indices.begin(), m_Indices.end(), TModelElementScalarPDFIndex<GenericModelElement>::SortPredicateAscendant);
                                                    break;
                                                case eDescendant:
                                                    std::sort(m_Indices.begin(), m_Indices.end(), TModelElementScalarPDFIndex<GenericModelElement>::SortPredicateDescendant);
                                                    break;
                                            }
                                            const int TotalElements = m_ModelElements.size();
                                            for (int Index = 0 ; Index < TotalElements ; ++Index)
                                            {
                                                m_Indices[Index]->SetIndex(Index);
                                            }
                                            const Real MedianIndex = Real(TotalElements) / Real(2);
                                            const int IndexA = int(std::floor(MedianIndex));
                                            const int IndexB = std::min(int(std::ceil(MedianIndex)), int(TotalElements) - int(1));
                                            const Real ScalarA = Real(m_Indices[IndexA]->GetScalar());
                                            const Real ScalarB = Real(m_Indices[IndexB]->GetScalar());
                                            m_ScalarMedian = int((ScalarB - ScalarA) * (MedianIndex - Real(IndexA)) + ScalarA);
                                            return true;
                                        }
                                        return false;
                                    }

                                    void EstimatePDF()
                                    {
                                        const int TotalElements = m_ModelElements.size();
                                        const Real BandWidth = Mathematics::_1D::CNormalDistribution::DetermineBandWidthByStandardDeviation(m_ScalarStandardDeviation, TotalElements);
                                        const Real BandWidthExponentFactor = Mathematics::_1D::CNormalDistribution::DetermineExponentFactor(BandWidth);
                                        Real GlobalDensityAccumulator = Real(0);
                                        Real GlobalMaximalDensity = Real(0);
                                        for (typename std::vector<TModelElementScalarPDFIndex<GenericModelElement>*>::iterator ppIndex = m_Indices.begin() ; ppIndex != m_Indices.end() ; ++ppIndex)
                                        {
                                            const Real Scalar = (*ppIndex)->GetScalar();
                                            Real LocalDensityAccumulator = Real(0);
                                            for (typename std::vector<TModelElementScalarPDFIndex<GenericModelElement>*>::iterator ppAuxiliarIndex = m_Indices.begin() ; ppAuxiliarIndex != m_Indices.end() ; ++ppAuxiliarIndex)
                                            {
                                                const Real ScalarDeviation = (*ppAuxiliarIndex)->GetScalar() - Scalar;
                                                LocalDensityAccumulator += std::exp(ScalarDeviation * ScalarDeviation * BandWidthExponentFactor);
                                            }
                                            (*ppIndex)->SetDensity(LocalDensityAccumulator);
                                            GlobalDensityAccumulator += LocalDensityAccumulator;
                                            if (LocalDensityAccumulator > GlobalMaximalDensity)
                                            {
                                                GlobalMaximalDensity = LocalDensityAccumulator;
                                            }
                                        }
                                        const Real ScaleFactor = Real(1) / GlobalMaximalDensity;
                                        const Real NormalizationFactor = Real(1) / GlobalMaximalDensity;
                                        for (typename std::vector<TModelElementScalarPDFIndex<GenericModelElement>*>::iterator ppIndex = m_Indices.begin() ; ppIndex != m_Indices.end() ; ++ppIndex)
                                        {
                                            (*ppIndex)->ScaleDensity(ScaleFactor);
                                            (*ppIndex)->NormalizeDensity(NormalizationFactor);
                                        }
                                    }

                                    void ClearIndices()
                                    {
                                        if (m_Indices.size())
                                        {
                                            for (typename std::vector<TModelElementScalarPDFIndex<GenericModelElement>*>::iterator ppIndex = m_Indices.begin() ; ppIndex != m_Indices.end() ; ++ppIndex)
                                            {
                                                delete *ppIndex;
                                            }
                                            m_Indices.clear();
                                        }
                                    }

                                    SortingOrder m_SortingOrder;
                                    Real m_MaximalScalar;
                                    Real m_MinimalScalar;
                                    Real m_ScalarRange;
                                    Real m_ScalarMedian;
                                    Real m_ScalarMean;
                                    Real m_ScalarStandardDeviation;
                                    std::vector<TModelElementScalarPDFIndex<GenericModelElement>*> m_Indices;
                                    std::map<const GenericModelElement*, TModelElementScalarPDFIndex<GenericModelElement>*> m_InverseIndices;
                                    std::set<const GenericModelElement*> m_ModelElements;
                                };
                            }
                        }
                    }
                }
            }
        }
    }
}

