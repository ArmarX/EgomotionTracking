/*
 * ModelElementScalarPDFIndex.h
 */

#pragma once

#include "../../../../Base/ModelElement.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Generic
                            {
                                template <typename GenericModelElement> class TModelElementScalarPDFIndex
                                {
                                public:

                                    static bool SortPredicateAscendant(const TModelElementScalarPDFIndex<GenericModelElement>* plhs, const TModelElementScalarPDFIndex<GenericModelElement>* prhs)
                                    {
                                        return plhs->m_Scalar > prhs->m_Scalar;
                                    }

                                    static bool SortPredicateDescendant(const TModelElementScalarPDFIndex<GenericModelElement>* plhs, const TModelElementScalarPDFIndex<GenericModelElement>* prhs)
                                    {
                                        return plhs->m_Scalar < prhs->m_Scalar;
                                    }

                                    TModelElementScalarPDFIndex(const GenericModelElement* pModelElement) :
                                        m_pModelElement(pModelElement),
                                        m_Scalar(Real(0)),
                                        m_Density(Real(0)),
                                        m_ScaledDensity(Real(0)),
                                        m_NormalizedDensity(Real(0)),
                                        m_Index(0)
                                    {
                                    }

                                    ~TModelElementScalarPDFIndex()
                                    {
                                    }

                                    inline void SetScalar(const Real Scalar)
                                    {
                                        m_Scalar = Scalar;
                                    }

                                    inline void SetDensity(const Real Density)
                                    {
                                        m_Density = Density;
                                    }

                                    inline void SetIndex(const int Index)
                                    {
                                        m_Index = Index;
                                    }

                                    inline void ScaleDensity(const Real ScaleFactor)
                                    {
                                        m_ScaledDensity = m_Density * ScaleFactor;
                                    }

                                    inline void NormalizeDensity(const Real NormalizationFactor)
                                    {
                                        m_NormalizedDensity = m_Density * NormalizationFactor;
                                    }

                                    inline const GenericModelElement* GetModelElement() const
                                    {
                                        return m_pModelElement;
                                    }

                                    inline int GetIndex() const
                                    {
                                        return m_Index;
                                    }

                                    inline Real GetScalar() const
                                    {
                                        return m_Scalar;
                                    }

                                    inline Real GetDensity() const
                                    {
                                        return m_Density;
                                    }

                                    inline Real GetScaledDensity() const
                                    {
                                        return m_ScaledDensity;
                                    }

                                    Real GetNormalizedDensity() const
                                    {
                                        return m_NormalizedDensity;
                                    }

                                protected:

                                    const GenericModelElement* m_pModelElement;
                                    Real m_Scalar;
                                    Real m_Density;
                                    Real m_ScaledDensity;
                                    Real m_NormalizedDensity;
                                    int m_Index;
                                };
                            }
                        }
                    }
                }
            }
        }
    }
}

