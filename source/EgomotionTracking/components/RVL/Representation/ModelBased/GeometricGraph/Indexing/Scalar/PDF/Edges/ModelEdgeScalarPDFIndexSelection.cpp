/*
 * ModelEdgeScalarPDFIndexSelection.cpp
 */

#include "ModelEdgeScalarPDFIndexSelection.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Edges
                            {
                                CModelEdgeScalarPDFIndexSelection::CModelEdgeScalarPDFIndexSelection(const int QueryId, const Real SelectionScalar, const Real SelectionDensity, const CModelEdgeScalarPDFIndex* pIndex) :
                                    Generic::TModelElementScalarPDFIndexSelection<Base::CModelEdge>(QueryId, SelectionScalar, SelectionDensity, pIndex)
                                {
                                }

                                CModelEdgeScalarPDFIndexSelection::~CModelEdgeScalarPDFIndexSelection()
                                    = default;
                            }
                        }
                    }
                }
            }
        }
    }
}
