/*
 * ModelEdgeScalarPDFIndex.cpp
 */

#include "ModelEdgeScalarPDFIndex.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Edges
                            {
                                CModelEdgeScalarPDFIndex::CModelEdgeScalarPDFIndex(const Base::CModelEdge* pModelEdge) :
                                    Generic::TModelElementScalarPDFIndex<Base::CModelEdge>(pModelEdge)
                                {
                                }

                                CModelEdgeScalarPDFIndex::~CModelEdgeScalarPDFIndex()
                                    = default;
                            }
                        }
                    }
                }
            }
        }
    }
}
