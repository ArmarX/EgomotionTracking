/*
 * TModelElementScalarPDFIndexComposedSelection.hpp
 */

#pragma once

#include "TModelElementScalarPDFIndexSelection.hpp"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Generic
                            {
                                template <typename GenericModelElement> class TModelElementScalarPDFIndexSelection;

                                template <typename GenericModelElementA, typename GenericModelElementB> class TScalarPDFIndexComposedSelection
                                {
                                public:

                                    TScalarPDFIndexComposedSelection(const TModelElementScalarPDFIndexSelection<GenericModelElementA>* pSelectionA, const TModelElementScalarPDFIndexSelection<GenericModelElementB>* pSelectionB) :
                                        m_pSelectionA(pSelectionA),
                                        m_pSelectionB(pSelectionB)
                                    {
                                    }

                                    virtual ~TScalarPDFIndexComposedSelection()
                                    {
                                    }

                                    inline const TModelElementScalarPDFIndexSelection<GenericModelElementA>* GetSelectionA() const
                                    {
                                        return m_pSelectionA;
                                    }

                                    inline const TModelElementScalarPDFIndexSelection<GenericModelElementB>* GetSelectionB() const
                                    {
                                        return m_pSelectionB;
                                    }

                                    inline Real GetConditionalDensity() const
                                    {
                                        return m_pSelectionA->GetSelectionDensity() * m_pSelectionB->GetSelectionDensity();
                                    }

                                protected:

                                    const TModelElementScalarPDFIndexSelection<GenericModelElementA>* m_pSelectionA;
                                    const TModelElementScalarPDFIndexSelection<GenericModelElementB>* m_pSelectionB;
                                };
                            }
                        }
                    }
                }
            }
        }
    }
}

