/*
 * TModelElementScalarPDFIndexInterval.hpp
 */

#pragma once

#include "TModelElementScalarPDFIndex.hpp"
#include "TModelElementScalarPDFIndexer.hpp"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Generic
                            {
                                template <typename GenericModelElement> class TModelElementScalarPDFIndexer;

                                template <typename GenericModelElement> class TModelElementScalarPDFIndexInterval
                                {
                                public:

                                    TModelElementScalarPDFIndexInterval(const int QueryId, const TModelElementScalarPDFIndex<GenericModelElement>* pIndexA, const TModelElementScalarPDFIndex<GenericModelElement>* pIndexB, const TModelElementScalarPDFIndexer<GenericModelElement>* pIndexer) :
                                        m_QueryId(QueryId),
                                        m_pIndexA(NULL),
                                        m_pIndexB(NULL),
                                        m_pIndexer(pIndexer)
                                    {
                                        if (m_pIndexA && m_pIndexB)
                                        {
                                            if (m_pIndexA->GetIndex() <= m_pIndexB->GetIndex())
                                            {
                                                m_pIndexA = pIndexA;
                                                m_pIndexB = pIndexB;
                                            }
                                            else
                                            {
                                                m_pIndexA = pIndexB;
                                                m_pIndexB = pIndexA;
                                            }
                                        }
                                    }

                                    ~TModelElementScalarPDFIndexInterval()
                                    {
                                    }

                                    inline bool IsEmpty() const
                                    {
                                        return !(m_pIndexA && m_pIndexB);
                                    }

                                    inline bool HasIndices() const
                                    {
                                        return (m_pIndexA && m_pIndexB);
                                    }

                                    inline int GetQueryId() const
                                    {
                                        return m_QueryId;
                                    }

                                    inline int GetLength() const
                                    {
                                        return (m_pIndexA && m_pIndexB) ? (m_pIndexB->GetIndex() - m_pIndexA->GetIndex() + 1) : 0;
                                    }

                                    inline const TModelElementScalarPDFIndex<GenericModelElement>* GetIndexA() const
                                    {
                                        return m_pIndexA;
                                    }

                                    inline const TModelElementScalarPDFIndex<GenericModelElement>* GetIndexB() const
                                    {
                                        return m_pIndexB;
                                    }

                                    inline const TModelElementScalarPDFIndexer<GenericModelElement>* GetIndexer() const
                                    {
                                        return m_pIndexer;
                                    }

                                    inline Real GetIntervalDensity() const
                                    {
                                        Real IntervalDensity = Real(0);
                                        if (m_pIndexA && m_pIndexB && m_pIndexer)
                                        {
                                            const std::vector<TModelElementScalarPDFIndex<GenericModelElement>*>& Indices = m_pIndexer->GetIndices();
                                            const int IndexA = m_pIndexA->GetIndex();
                                            const int IndexB = m_pIndexB->GetIndex();
                                            for (int Index = IndexA ; Index <= IndexB ; ++Index)
                                            {
                                                IntervalDensity += Indices[Index]->GetDensity();
                                            }
                                        }
                                        return IntervalDensity;
                                    }

                                protected:

                                    const int m_QueryId;
                                    const TModelElementScalarPDFIndex<GenericModelElement>* m_pIndexA;
                                    const TModelElementScalarPDFIndex<GenericModelElement>* m_pIndexB;
                                    const TModelElementScalarPDFIndexer<GenericModelElement>* m_pIndexer;
                                };
                            }
                        }
                    }
                }
            }
        }
    }
}

