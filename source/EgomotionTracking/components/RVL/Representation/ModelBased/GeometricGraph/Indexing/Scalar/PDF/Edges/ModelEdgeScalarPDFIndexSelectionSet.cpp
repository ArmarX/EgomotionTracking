/*
 * ModelEdgeScalarPDFIndexSelectionSet.cpp
 */

#include "ModelEdgeScalarPDFIndexSelectionSet.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Edges
                            {
                                CModelEdgeScalarPDFIndexSelectionSet::CModelEdgeScalarPDFIndexSelectionSet(const int QueryId) :
                                    Generic::TModelElementScalarPDFIndexSelectionSet<Base::CModelElement>(QueryId)
                                {
                                }

                                CModelEdgeScalarPDFIndexSelectionSet::~CModelEdgeScalarPDFIndexSelectionSet()
                                    = default;
                            }
                        }
                    }
                }
            }
        }
    }
}
