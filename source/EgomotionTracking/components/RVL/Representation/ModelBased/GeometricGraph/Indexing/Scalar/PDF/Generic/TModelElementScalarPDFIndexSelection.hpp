/*
 * TModelElementScalarPDFIndexSelection.hpp
 */

#pragma once

#include "TModelElementScalarPDFIndex.hpp"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Generic
                            {
                                template <typename GenericModelElement> class TModelElementScalarPDFIndex;

                                template <typename GenericModelElement> class TModelElementScalarPDFIndexSelection
                                {
                                public:

                                    static bool SortPredicateAscendant(const TModelElementScalarPDFIndexSelection<GenericModelElement>* plhs, const TModelElementScalarPDFIndexSelection<GenericModelElement>* prhs)
                                    {
                                        return plhs->m_SelectionDensity > prhs->m_SelectionDensity;
                                    }

                                    static bool SortPredicateDescendant(const TModelElementScalarPDFIndexSelection<GenericModelElement>* plhs, const TModelElementScalarPDFIndexSelection<GenericModelElement>* prhs)
                                    {
                                        return plhs->m_SelectionDensity < prhs->m_SelectionDensity;
                                    }

                                    TModelElementScalarPDFIndexSelection(const int QueryId, const Real SelectionScalar, const Real SelectionDensity, const TModelElementScalarPDFIndex<GenericModelElement>* pIndex) :
                                        m_QueryId(QueryId),
                                        m_SelectionScalar(SelectionScalar),
                                        m_SelectionDensity(SelectionDensity),
                                        m_pIndex(pIndex)
                                    {
                                    }

                                    virtual ~TModelElementScalarPDFIndexSelection()
                                    {
                                    }

                                    inline int GetQueryId() const
                                    {
                                        return m_QueryId;
                                    }

                                    inline Real GetSelectionScalar() const
                                    {
                                        return m_SelectionScalar;
                                    }

                                    inline Real GetSelectionDensity() const
                                    {
                                        return m_SelectionDensity;
                                    }

                                    inline Real GetConjuctedDensity() const
                                    {
                                        return m_SelectionDensity * m_pIndex->GetNormalizedDensity();
                                    }

                                    inline const TModelElementScalarPDFIndex<GenericModelElement>* GetIndex() const
                                    {
                                        return m_pIndex;
                                    }

                                protected:

                                    const int m_QueryId;
                                    const Real m_SelectionScalar;
                                    const Real m_SelectionDensity;
                                    const TModelElementScalarPDFIndex<GenericModelElement>* m_pIndex;
                                };
                            }
                        }
                    }
                }
            }
        }
    }
}

