/*
 * ModelEdgeScalarPDFIndexer.h
 */

#pragma once

#include "../../../../Base/ModelEdge.h"
#include "../Generic/TModelElementScalarPDFIndexer.hpp"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Edges
                            {
                                class CModelEdgeScalarPDFIndexer : public Generic::TModelElementScalarPDFIndexer<Base::CModelEdge>
                                {
                                public:

                                    CModelEdgeScalarPDFIndexer();
                                    ~CModelEdgeScalarPDFIndexer() override;
                                };
                            }
                        }
                    }
                }
            }
        }
    }
}

