/*
 * TModelElementScalarPDFIndexSelectionSet.hpp
 */

#pragma once

#include "TModelElementScalarPDFIndexSelection.hpp"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Generic
                            {
                                template <typename GenericModelElement> class TModelElementScalarPDFIndexSelection;

                                template <typename GenericModelElement> class TModelElementScalarPDFIndexSelectionSet
                                {
                                public:

                                    TModelElementScalarPDFIndexSelectionSet(const int QueryId) :
                                        m_QueryId(QueryId)
                                    {
                                    }

                                    virtual ~TModelElementScalarPDFIndexSelectionSet()
                                    {
                                    }

                                    void AddElement(const TModelElementScalarPDFIndexSelection<GenericModelElement>* pElement)
                                    {
                                        if (pElement && (m_Set.find(pElement) == m_Set.end()))
                                        {
                                            m_Set.push_back(pElement);
                                        }
                                    }

                                    const std::set<const TModelElementScalarPDFIndexSelection<GenericModelElement>*>& GetElements() const
                                    {
                                        return m_Set;
                                    }

                                protected:

                                    const int m_QueryId;
                                    std::set<const TModelElementScalarPDFIndexSelection<GenericModelElement>*> m_Set;
                                };
                            }
                        }
                    }
                }
            }
        }
    }
}

