/*
 * ModelEdgeScalarPDFIndexSelection.h
 */

#pragma once

#include "../../../../Base/ModelEdge.h"
#include "../Generic/TModelElementScalarPDFIndexSelection.hpp"
#include "ModelEdgeScalarPDFIndex.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Indexing
                {
                    namespace Scalar
                    {
                        namespace PDF
                        {
                            namespace Edges
                            {
                                class CModelEdgeScalarPDFIndexSelection : public Generic::TModelElementScalarPDFIndexSelection<Base::CModelEdge>
                                {
                                public:

                                    CModelEdgeScalarPDFIndexSelection(const int QueryId, const Real SelectionScalar, const Real SelectionDensity, const CModelEdgeScalarPDFIndex* pIndex);
                                    ~CModelEdgeScalarPDFIndexSelection() override;
                                };
                            }
                        }
                    }
                }
            }
        }
    }
}

