/*
 * OpenInventorModelFace.cpp
 */

#include "OpenInventorModelFace.h"
#include "../../Base/ModelOrientedVertex.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Visualization
                {
                    namespace OpenInventor
                    {
                        COpenInventorModelFace::COpenInventorModelFace(Base::CModelFace* pModelFace) :
                            COpenInventorModelPrimitive(pModelFace, true, true, true),
                            m_pVisualizationModeSoSwitch(nullptr),
                            m_pCoordinate3(nullptr),
                            m_pSoFaceSet(nullptr)
                        {
                            Create();
                            Base::CModelElement* pModelElement = COpenInventorModelPrimitive::GetModelElement();
                            pModelElement->SetElementData(this);
                        }

                        COpenInventorModelFace::~COpenInventorModelFace()
                        {
                            Base::CModelElement* pModelElement = COpenInventorModelPrimitive::GetModelElement();
                            pModelElement->ClearElementData();
                        }

                        const Base::CModelFace* COpenInventorModelFace::GetModelFace() const
                        {
                            return dynamic_cast<const Base::CModelFace*>(GetModelElement());
                        }

                        Base::CModelFace* COpenInventorModelFace::GetModelFace()
                        {
                            return dynamic_cast<Base::CModelFace*>(GetModelElement());
                        }

                        void COpenInventorModelFace::Create()
                        {
                            m_pVisualizationModeSoSwitch = new SoSwitch;
                            AddElement(m_pVisualizationModeSoSwitch);

                            SoExtSelection* pFaceSelectionSeparator = new SoExtSelection;
                            m_pVisualizationModeSoSwitch->addChild(pFaceSelectionSeparator);

                            m_pCoordinate3 = new SoCoordinate3;
                            pFaceSelectionSeparator->addChild(m_pCoordinate3);

                            m_pSoFaceSet = new SoFaceSet;
                            pFaceSelectionSeparator->addChild(m_pSoFaceSet);
                            m_pSoFaceSet->setUserData(this);

                            m_pVisualizationModeSoSwitch->whichChild = 0;
                        }

                        void COpenInventorModelFace::FullUpdate()
                        {
                            const Base::CModelFace* pModelFace = dynamic_cast<const Base::CModelFace*>(COpenInventorModelPrimitive::GetModelElement());
                            if (pModelFace)
                            {
                                std::vector<Mathematics::_3D::CVector3D> VertexPoints = pModelFace->GetVertexPoints(true);
                                m_pCoordinate3->point.set1Value(0, float(VertexPoints[0].GetX()), float(VertexPoints[0].GetY()), float(VertexPoints[0].GetZ()));
                                m_pCoordinate3->point.set1Value(1, float(VertexPoints[1].GetX()), float(VertexPoints[1].GetY()), float(VertexPoints[1].GetZ()));
                                m_pCoordinate3->point.set1Value(2, float(VertexPoints[2].GetX()), float(VertexPoints[2].GetY()), float(VertexPoints[2].GetZ()));
                                switch (GetVisualizationMode())
                                {
                                    case COpenInventorModelPrimitive::eDefault:
                                        m_pVisualizationModeSoSwitch->whichChild = SO_SWITCH_NONE;
                                        break;
                                    case COpenInventorModelPrimitive::eSelected:
                                        m_pVisualizationModeSoSwitch->whichChild = 0;
                                        break;
                                    case COpenInventorModelPrimitive::eEdition:
                                        m_pVisualizationModeSoSwitch->whichChild = 1;
                                        break;
                                    case COpenInventorModelPrimitive::eFull:
                                        m_pVisualizationModeSoSwitch->whichChild = 1;
                                        break;
                                }

                                const Real* pMaterial = s_MaterialStatePrimitiveTable[pModelFace->GetTypeId()][pModelFace->GetVisibility(false)];
                                SetColor(float(pMaterial[0]), float(pMaterial[1]), float(pMaterial[2]));
                                SetTransparency(1.0f - float(pMaterial[3]));
                            }
                        }

                        bool COpenInventorModelFace::IsComposedVisible()
                        {
                            if (COpenInventorModelPrimitive::IsComposedVisible())
                            {
                                const Base::CModelFace* pModelFace = dynamic_cast<const Base::CModelFace*>(COpenInventorModelPrimitive::GetModelElement());
                                if (pModelFace)
                                {
                                    const RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::CContainerOpenInventorSet* pContainerSet = COpenInventorModelPrimitive::GetContainerSet();
                                    if (pContainerSet)
                                    {
                                        const Base::CModelFace* pModelFace = dynamic_cast<const Base::CModelFace*>(COpenInventorModelPrimitive::GetModelElement());
                                        std::vector<Mathematics::_3D::CVector3D> VertexPoints = pModelFace->GetVertexPoints(true);

                                        const SbVec3f VisualizationPointA(float(VertexPoints[0].GetX()), float(VertexPoints[0].GetY()), float(VertexPoints[0].GetZ()));
                                        const SbVec3f VisualizationPointB(float(VertexPoints[1].GetX()), float(VertexPoints[1].GetY()), float(VertexPoints[1].GetZ()));
                                        const SbVec3f VisualizationPointC(float(VertexPoints[2].GetX()), float(VertexPoints[2].GetY()), float(VertexPoints[2].GetZ()));

                                        RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::InclusionType InclusionTypeA = pContainerSet->MapPoint(VisualizationPointA);
                                        RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::InclusionType InclusionTypeB = pContainerSet->MapPoint(VisualizationPointB);
                                        RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::InclusionType InclusionTypeC = pContainerSet->MapPoint(VisualizationPointC);

                                        const bool VisibleA = (InclusionTypeA == RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::ePartialInclusion) || (InclusionTypeA == RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::eFullInclusion);
                                        const bool VisibleB = (InclusionTypeB == RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::ePartialInclusion) || (InclusionTypeB == RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::eFullInclusion);
                                        const bool VisibleC = (InclusionTypeC == RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::ePartialInclusion) || (InclusionTypeC == RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::eFullInclusion);

                                        return (VisibleA || VisibleB || VisibleC);
                                    }
                                    return true;
                                }
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}
