/*
 * OpenInventorModelMesh.cpp
 */

#include "OpenInventorModelMesh.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Visualization
                {
                    namespace OpenInventor
                    {
                        COpenInventorModelMesh::COpenInventorModelMesh(Base::CModelMesh* pModelMesh) :
                            COpenInventorModelPrimitive(pModelMesh, true, true, true),
                            m_BoundingBoxOffset(Real(1.0)),
                            m_pContentSwitch(nullptr),
                            m_pBoundingBoxSwitch(nullptr),
                            m_pBoundingBoxCoordinates(nullptr),
                            m_pCubeDrawStyle(nullptr),
                            m_pBoundingBooxMaterial(nullptr),
                            m_pIndexedLineSetCube(nullptr),
                            m_pMeshCoordinates(nullptr),
                            m_pNormals(nullptr),
                            m_pPrimitiveSetSwitch(nullptr),
                            m_pVisibleIndexedFaceSetSwitch(nullptr),
                            m_pVisibleIndexedFaceSetDrawStyle(nullptr),
                            m_pVisibleIndexedFaceSetMaterial(nullptr),
                            m_pVisibleIndexedFaceSet(nullptr),
                            m_pNonVisibleIndexedFaceSetSwitch(nullptr),
                            m_pNonVisibleIndexedFaceSetDrawStyle(nullptr),
                            m_pNonVisibleIndexedFaceSetMaterial(nullptr),
                            m_pNonVisibleIndexedFaceSet(nullptr),
                            m_pDynamicVisibleIndexedFaceSetSwitch(nullptr),
                            m_pDynamicVisibleIndexedFaceSetDrawStyle(nullptr),
                            m_pDynamicVisibleIndexedFaceSetMaterial(nullptr),
                            m_pDynamicVisibleIndexedFaceSet(nullptr),
                            m_pVisibleIndexedLineSetSwitch(nullptr),
                            m_pVisibleIndexedLineSetDrawStyle(nullptr),
                            m_pVisibleIndexedLineSetMaterial(nullptr),
                            m_pVisibleIndexedLineSet(nullptr),
                            m_pNonVisibleIndexedLineSetSwitch(nullptr),
                            m_pNonVisibleIndexedLineSetDrawStyle(nullptr),
                            m_pNonVisibleIndexedLineSetMaterial(nullptr),
                            m_pNonVisibleIndexedLineSet(nullptr),
                            m_pDynamicVisibleIndexedLineSetSwitch(nullptr),
                            m_pDynamicVisibleIndexedLineSetDrawStyle(nullptr),
                            m_pDynamicVisibleIndexedLineSetMaterial(nullptr),
                            m_pDynamicVisibleIndexedLineSet(nullptr),
                            m_VisibleVertexSetVisualization(),
                            m_NonVisibleVertexSetVisualization(),
                            m_DynamicVisibleVertexSetVisualization()

                        {
                            Create();
                            Base::CModelElement* pModelElement = COpenInventorModelPrimitive::GetModelElement();
                            pModelElement->SetElementData(this);

                            SetFacesSetMaterialFromMeshMaterial(Base::CModelElement::eStaticVisible);
                            SetFacesSetMaterial(Base::CModelElement::eDynamicVisible, 255, 153, 0, 128);
                            SetFacesSetMaterial(Base::CModelElement::eStaticOccluded, 0, 0, 0, 128);
                        }

                        COpenInventorModelMesh::~COpenInventorModelMesh()
                        {
                            Base::CModelElement* pModelElement = COpenInventorModelPrimitive::GetModelElement();
                            pModelElement->ClearElementData();
                        }

                        const Base::CModelMesh* COpenInventorModelMesh::GetModelMesh() const
                        {
                            return dynamic_cast<const Base::CModelMesh*>(GetModelElement());
                        }

                        Base::CModelMesh* COpenInventorModelMesh::GetModelMesh()
                        {
                            return dynamic_cast<Base::CModelMesh*>(GetModelElement());
                        }

                        bool COpenInventorModelMesh::GetBoundingBoxVisible() const
                        {
                            return m_pBoundingBoxSwitch->whichChild.getValue() == SO_SWITCH_ALL;
                        }

                        void COpenInventorModelMesh::SetBoundingBoxVisible(const bool Visible)
                        {
                            m_pBoundingBoxSwitch->whichChild = Visible ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                        }

                        Real COpenInventorModelMesh::GetBoundingOffset() const
                        {
                            return m_BoundingBoxOffset;
                        }

                        void COpenInventorModelMesh::SetBoundingOffset(const Real Offset)
                        {
                            if (NonEquals(Offset, m_BoundingBoxOffset))
                            {
                                m_BoundingBoxOffset = Offset;
                                UpdateBoundingBox();
                            }
                        }

                        void COpenInventorModelMesh::GetBoundingBoxLineMaterial(Byte& R, Byte& G, Byte& B, Byte& A) const
                        {
                            SbColor CurrentColor = m_pBoundingBooxMaterial->emissiveColor[0];
                            R = Byte(round(CurrentColor[0] * 255.0f));
                            G = Byte(round(CurrentColor[1] * 255.0f));
                            B = Byte(round(CurrentColor[2] * 255.0f));
                            A = Byte(round((1.0f - m_pBoundingBooxMaterial->transparency[0]) * 255.0f));
                        }

                        void COpenInventorModelMesh::SetBoundingBoxLineMaterial(const Byte R, const Byte G, const Byte B, const Byte A)
                        {
                            m_pBoundingBooxMaterial->emissiveColor.setValue(float(R) / 255.0f, float(G) / 255.0f, float(B) / 255.0f);
                            m_pBoundingBooxMaterial->transparency.setValue(1.0f - (float(A) / 255.0f));
                        }

                        Real COpenInventorModelMesh::GetBoundingBoxLineWidth() const
                        {
                            return m_pCubeDrawStyle->lineWidth.getValue();
                        }

                        void COpenInventorModelMesh::SetBoundingBoxLineWidth(const Real Width)
                        {
                            m_pCubeDrawStyle->lineWidth.setValue(float(Width));
                        }

                        ushort COpenInventorModelMesh::GetBoundingBoxLinePattern() const
                        {
                            return  m_pCubeDrawStyle->linePattern.getValue();
                        }

                        void COpenInventorModelMesh::SetBoundingBoxLinePattern(const ushort Pattern)
                        {
                            m_pCubeDrawStyle->linePattern.setValue(Pattern);
                        }

                        bool COpenInventorModelMesh::GetMultipleFacesSetVisible(const Base::CModelElement::Visibility Type) const
                        {
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:

                                    break;
                                case Base::CModelElement::eStaticVisible:

                                    break;
                                case Base::CModelElement::eDynamicVisible:

                                    break;
                            }
                            return false;
                        }

                        void COpenInventorModelMesh::SetMultipleFacesSetVisible(const Base::CModelElement::Visibility Type, const bool /*Visible*/)
                        {
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:

                                    break;
                                case Base::CModelElement::eStaticVisible:

                                    break;
                                case Base::CModelElement::eDynamicVisible:

                                    break;
                            }
                        }

                        void COpenInventorModelMesh::GetMultipleFacesSetMaterial(const Base::CModelElement::Visibility Type, Byte& /*R*/, Byte& /*G*/, Byte& /*B*/, Byte& /*A*/) const
                        {
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:

                                    break;
                                case Base::CModelElement::eStaticVisible:

                                    break;
                                case Base::CModelElement::eDynamicVisible:

                                    break;
                            }
                        }

                        void COpenInventorModelMesh::SetMultipleFacesSetMaterial(const Base::CModelElement::Visibility Type, const Byte /*R*/, const Byte /*G*/, const Byte /*B*/, const Byte /*A*/)
                        {
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:

                                    break;
                                case Base::CModelElement::eStaticVisible:

                                    break;
                                case Base::CModelElement::eDynamicVisible:

                                    break;
                            }
                        }

                        bool COpenInventorModelMesh::GetFaceSetVisible(const Base::CModelElement::Visibility Type) const
                        {
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    return m_pNonVisibleIndexedFaceSetSwitch->whichChild.getValue() == SO_SWITCH_ALL;
                                case Base::CModelElement::eStaticVisible:
                                    return m_pVisibleIndexedFaceSetSwitch->whichChild.getValue() == SO_SWITCH_ALL;
                                case Base::CModelElement::eDynamicVisible:
                                    return m_pDynamicVisibleIndexedFaceSetSwitch->whichChild.getValue() == SO_SWITCH_ALL;
                            }
                            return false;
                        }

                        void COpenInventorModelMesh::SetFaceSetVisible(const Base::CModelElement::Visibility Type, const bool Visible)
                        {
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    m_pNonVisibleIndexedFaceSetSwitch->whichChild.setValue(Visible ? SO_SWITCH_ALL : SO_SWITCH_NONE);
                                    break;
                                case Base::CModelElement::eStaticVisible:
                                    m_pVisibleIndexedFaceSetSwitch->whichChild.setValue(Visible ? SO_SWITCH_ALL : SO_SWITCH_NONE);
                                    break;
                                case Base::CModelElement::eDynamicVisible:
                                    m_pDynamicVisibleIndexedFaceSetSwitch->whichChild.setValue(Visible ? SO_SWITCH_ALL : SO_SWITCH_NONE);
                                    break;
                            }
                        }

                        void COpenInventorModelMesh::GetFacesSetMaterial(const Base::CModelElement::Visibility Type, Byte& R, Byte& G, Byte& B, Byte& A) const
                        {
                            SbColor CurrentColor;
                            float Alpha = 0.0f;
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    CurrentColor = m_pNonVisibleIndexedFaceSetMaterial->diffuseColor[0];
                                    Alpha = m_pNonVisibleIndexedFaceSetMaterial->transparency[0];
                                    break;
                                case Base::CModelElement::eStaticVisible:
                                    CurrentColor = m_pVisibleIndexedFaceSetMaterial->diffuseColor[0];
                                    Alpha = m_pVisibleIndexedFaceSetMaterial->transparency[0];
                                    break;
                                case Base::CModelElement::eDynamicVisible:
                                    CurrentColor = m_pDynamicVisibleIndexedFaceSetMaterial->diffuseColor[0];
                                    Alpha = m_pDynamicVisibleIndexedFaceSetMaterial->transparency[0];
                                    break;
                            }
                            R = Byte(round(CurrentColor[0] * 255.0f));
                            G = Byte(round(CurrentColor[1] * 255.0f));
                            B = Byte(round(CurrentColor[2] * 255.0f));
                            A = Byte(round((1.0f - Alpha) * 255.0f));
                        }

                        void COpenInventorModelMesh::SetFacesSetMaterial(const Base::CModelElement::Visibility Type, const Byte R, const Byte G, const Byte B, const Byte A)
                        {
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    m_pNonVisibleIndexedFaceSetMaterial->diffuseColor.setValue(float(R) / 255.0f, float(G) / 255.0f, float(B) / 255.0f);
                                    m_pNonVisibleIndexedFaceSetMaterial->transparency = 1.0f - (float(A) / 255.0f);
                                    break;
                                case Base::CModelElement::eStaticVisible:
                                    m_pVisibleIndexedFaceSetMaterial->diffuseColor.setValue(float(R) / 255.0f, float(G) / 255.0f, float(B) / 255.0f);
                                    m_pVisibleIndexedFaceSetMaterial->transparency = 1.0f - (float(A) / 255.0f);
                                    break;
                                case Base::CModelElement::eDynamicVisible:
                                    m_pDynamicVisibleIndexedFaceSetMaterial->diffuseColor.setValue(float(R) / 255.0f, float(G) / 255.0f, float(B) / 255.0f);
                                    m_pDynamicVisibleIndexedFaceSetMaterial->transparency = 1.0f - (float(A) / 255.0f);
                                    break;
                            }
                        }

                        void COpenInventorModelMesh::SetFacesSetMaterialFromMeshMaterial(const Base::CModelElement::Visibility Type)
                        {
                            SoMaterial* pMeshMaterial = GetBaseMaterial();
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    m_pNonVisibleIndexedFaceSetMaterial->diffuseColor = pMeshMaterial->diffuseColor[0];
                                    m_pNonVisibleIndexedFaceSetMaterial->transparency = pMeshMaterial->transparency[0];
                                    break;
                                case Base::CModelElement::eStaticVisible:
                                    m_pVisibleIndexedFaceSetMaterial->diffuseColor = pMeshMaterial->diffuseColor[0];
                                    m_pVisibleIndexedFaceSetMaterial->transparency = pMeshMaterial->transparency[0];
                                    break;
                                case Base::CModelElement::eDynamicVisible:
                                    m_pDynamicVisibleIndexedFaceSetMaterial->diffuseColor = pMeshMaterial->diffuseColor[0];
                                    m_pDynamicVisibleIndexedFaceSetMaterial->transparency = pMeshMaterial->transparency[0];
                                    break;
                            }
                        }

                        bool COpenInventorModelMesh::GetEdgesSetVisible(const Base::CModelElement::Visibility Type) const
                        {
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    return m_pNonVisibleIndexedLineSetSwitch->whichChild.getValue() == SO_SWITCH_ALL;
                                case Base::CModelElement::eStaticVisible:
                                    return  m_pVisibleIndexedLineSetSwitch->whichChild.getValue() == SO_SWITCH_ALL;
                                case Base::CModelElement::eDynamicVisible:
                                    return m_pDynamicVisibleIndexedLineSetSwitch->whichChild.getValue() == SO_SWITCH_ALL;
                            }
                            return false;
                        }

                        void COpenInventorModelMesh::SetEdgesSetVisible(const Base::CModelElement::Visibility Type, const bool Visible)
                        {
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    m_pNonVisibleIndexedLineSetSwitch->whichChild.setValue(Visible ? SO_SWITCH_ALL : SO_SWITCH_NONE);
                                    break;
                                case Base::CModelElement::eStaticVisible:
                                    m_pVisibleIndexedLineSetSwitch->whichChild.setValue(Visible ? SO_SWITCH_ALL : SO_SWITCH_NONE);
                                    break;
                                case Base::CModelElement::eDynamicVisible:
                                    m_pDynamicVisibleIndexedLineSetSwitch->whichChild.setValue(Visible ? SO_SWITCH_ALL : SO_SWITCH_NONE);
                                    break;
                            }
                        }

                        void COpenInventorModelMesh::GetEdgesSetMaterial(const Base::CModelElement::Visibility Type, Byte& R, Byte& G, Byte& B, Byte& A) const
                        {
                            SbColor CurrentColor;
                            float Alpha = 0.0f;
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    CurrentColor = m_pNonVisibleIndexedLineSetMaterial->diffuseColor[0];
                                    Alpha = m_pNonVisibleIndexedLineSetMaterial->transparency[0];
                                    break;
                                case Base::CModelElement::eStaticVisible:
                                    CurrentColor = m_pVisibleIndexedLineSetMaterial->diffuseColor[0];
                                    Alpha = m_pVisibleIndexedLineSetMaterial->transparency[0];
                                    break;
                                case Base::CModelElement::eDynamicVisible:
                                    CurrentColor = m_pDynamicVisibleIndexedLineSetMaterial->diffuseColor[0];
                                    Alpha = m_pDynamicVisibleIndexedLineSetMaterial->transparency[0];
                                    break;
                            }
                            R = Byte(round(CurrentColor[0] * 255.0f));
                            G = Byte(round(CurrentColor[1] * 255.0f));
                            B = Byte(round(CurrentColor[2] * 255.0f));
                            A = Byte(round(Alpha * 255.0f));
                        }

                        void COpenInventorModelMesh::SetEdgesSetMaterial(const Base::CModelElement::Visibility Type, const Byte R, const Byte G, const Byte B, const Byte A)
                        {
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    m_pNonVisibleIndexedLineSetMaterial->diffuseColor.setValue(float(R) / 255.0f, float(G) / 255.0f, float(B) / 255.0f);
                                    m_pNonVisibleIndexedLineSetMaterial->transparency = float(A) / 255.0f;
                                    break;
                                case Base::CModelElement::eStaticVisible:
                                    m_pVisibleIndexedLineSetMaterial->diffuseColor.setValue(float(R) / 255.0f, float(G) / 255.0f, float(B) / 255.0f);
                                    m_pVisibleIndexedLineSetMaterial->transparency = float(A) / 255.0f;
                                    break;
                                case Base::CModelElement::eDynamicVisible:
                                    m_pDynamicVisibleIndexedLineSetMaterial->diffuseColor.setValue(float(R) / 255.0f, float(G) / 255.0f, float(B) / 255.0f);
                                    m_pDynamicVisibleIndexedLineSetMaterial->transparency = float(A) / 255.0f;
                                    break;
                            }
                        }

                        void COpenInventorModelMesh::SetEdgesSetMaterialFromMeshMaterial(const Base::CModelElement::Visibility Type)
                        {
                            SoMaterial* pMeshMaterial = GetBaseMaterial();
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    m_pNonVisibleIndexedLineSetMaterial->diffuseColor = pMeshMaterial->diffuseColor[0];
                                    m_pNonVisibleIndexedLineSetMaterial->transparency = pMeshMaterial->transparency[0];
                                    break;
                                case Base::CModelElement::eStaticVisible:
                                    m_pVisibleIndexedLineSetMaterial->diffuseColor = pMeshMaterial->diffuseColor[0];
                                    m_pVisibleIndexedLineSetMaterial->transparency = pMeshMaterial->transparency[0];
                                    break;
                                case Base::CModelElement::eDynamicVisible:
                                    m_pDynamicVisibleIndexedLineSetMaterial->diffuseColor = pMeshMaterial->diffuseColor[0];
                                    m_pDynamicVisibleIndexedLineSetMaterial->transparency = pMeshMaterial->transparency[0];
                                    break;
                            }
                        }

                        bool COpenInventorModelMesh::GetVerticesSetVisible(const Base::CModelElement::Visibility Type) const
                        {
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    return m_NonVisibleVertexSetVisualization.IsDisplaying();
                                case Base::CModelElement::eStaticVisible:
                                    return m_VisibleVertexSetVisualization.IsDisplaying();
                                case Base::CModelElement::eDynamicVisible:
                                    return m_DynamicVisibleVertexSetVisualization.IsDisplaying();
                            }
                            return false;
                        }

                        void COpenInventorModelMesh::SetVerticesSetVisible(const Base::CModelElement::Visibility Type, const bool Visible)
                        {
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    m_NonVisibleVertexSetVisualization.SetDisplaying(Visible);
                                    break;
                                case Base::CModelElement::eStaticVisible:
                                    m_VisibleVertexSetVisualization.SetDisplaying(Visible);
                                    break;
                                case Base::CModelElement::eDynamicVisible:
                                    m_DynamicVisibleVertexSetVisualization.SetDisplaying(Visible);
                                    break;
                            }
                        }

                        void COpenInventorModelMesh::GetVerticesSetMaterial(const Base::CModelElement::Visibility Type, Byte& R, Byte& G, Byte& B, Byte& A) const
                        {
                            SbColor CurrentColor;
                            float Alpha = 0.0f;
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    CurrentColor = m_NonVisibleVertexSetVisualization.GetColor();
                                    Alpha = m_NonVisibleVertexSetVisualization.GetAlpha();
                                    break;
                                case Base::CModelElement::eStaticVisible:
                                    CurrentColor = m_VisibleVertexSetVisualization.GetColor();
                                    Alpha = m_VisibleVertexSetVisualization.GetAlpha();
                                    break;
                                case Base::CModelElement::eDynamicVisible:
                                    CurrentColor = m_DynamicVisibleVertexSetVisualization.GetColor();
                                    Alpha = m_DynamicVisibleVertexSetVisualization.GetAlpha();
                                    break;
                            }
                            R = Byte(round(CurrentColor[0] * 255.0f));
                            G = Byte(round(CurrentColor[1] * 255.0f));
                            B = Byte(round(CurrentColor[2] * 255.0f));
                            A = Byte(round((1.0f - Alpha) * 255.0f));
                        }

                        void COpenInventorModelMesh::SetVerticesSetMaterial(const Base::CModelElement::Visibility Type, const Byte R, const Byte G, const Byte B, const Byte A)
                        {
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    m_NonVisibleVertexSetVisualization.SetMaterial(R, G, B, A);
                                    break;
                                case Base::CModelElement::eStaticVisible:
                                    m_VisibleVertexSetVisualization.SetMaterial(R, G, B, A);
                                    break;
                                case Base::CModelElement::eDynamicVisible:
                                    m_DynamicVisibleVertexSetVisualization.SetMaterial(R, G, B, A);
                                    break;
                            }
                        }

                        void COpenInventorModelMesh::SetVerticesSetMaterialFromMeshMaterial(const Base::CModelElement::Visibility Type)
                        {
                            SoMaterial* pMeshMaterial = GetBaseMaterial();
                            switch (Type)
                            {
                                case Base::CModelElement::eStaticOccluded:
                                    m_NonVisibleVertexSetVisualization.SetMaterial(pMeshMaterial);
                                    break;
                                case Base::CModelElement::eStaticVisible:
                                    m_VisibleVertexSetVisualization.SetMaterial(pMeshMaterial);
                                    break;
                                case Base::CModelElement::eDynamicVisible:
                                    m_DynamicVisibleVertexSetVisualization.SetMaterial(pMeshMaterial);
                                    break;
                            }
                        }

                        void COpenInventorModelMesh::CreateManipulableObjects()
                        {
                            ClearManipulableObjects();

                            const Base::CModelMesh* pModelMesh = dynamic_cast<const Base::CModelMesh*>(GetModelElement());

                            //Vertices
                            const std::map<int, Base::CModelVertex*>& Vertices = pModelMesh->GetVertices();
                            if (Vertices.size())
                            {
                                std::map<int, Base::CModelVertex*>::const_iterator VericesEnd = Vertices.end();
                                for (std::map<int, Base::CModelVertex*>::const_iterator pKeyModelVertex = Vertices.begin() ; pKeyModelVertex != VericesEnd ; ++pKeyModelVertex)
                                {
                                    COpenInventorModelVertex* pOpenInventorModelVertex = new COpenInventorModelVertex(pKeyModelVertex->second);
                                    pOpenInventorModelVertex->SetDisplayRadius(1.0f);

                                    m_pObjectsVerticesSetSwitch->addChild(pOpenInventorModelVertex->GetBaseSwitch());
                                    pOpenInventorModelVertex->SetVisualizationMode(COpenInventorModelPrimitive::eSelected);
                                    m_OpenInventorModelVertices.push_back(pOpenInventorModelVertex);
                                }
                                m_pObjectsVerticesSetSwitch->whichChild = SO_SWITCH_ALL;
                            }

                            //Edges
                            const std::map<int, Base::CModelEdge*>& Edges = pModelMesh->GetEdges();
                            if (Edges.size())
                            {
                                std::map<int, Base::CModelEdge*>::const_iterator EdgesEnd = Edges.end();
                                for (std::map<int, Base::CModelEdge*>::const_iterator pKeyModelEdge = Edges.begin() ; pKeyModelEdge != EdgesEnd ; ++pKeyModelEdge)
                                {
                                    COpenInventorModelEdge* pOpenInventorModelEdge = new COpenInventorModelEdge(pKeyModelEdge->second);
                                    m_pObjectsEdgesSetSwitch->addChild(pOpenInventorModelEdge->GetBaseSwitch());
                                    pOpenInventorModelEdge->SetVisualizationMode(COpenInventorModelPrimitive::eSelected);
                                    m_OpenInventorModelEdges.push_back(pOpenInventorModelEdge);
                                }
                                m_pObjectsEdgesSetSwitch->whichChild = SO_SWITCH_ALL;
                            }

                            //Faces
                            const std::map<int, Base::CModelFace*>& Faces = pModelMesh->GetFaces();
                            if (Faces.size())
                            {
                                std::map<int, Base::CModelFace*>::const_iterator FacesEnd = Faces.end();
                                for (std::map<int, Base::CModelFace*>::const_iterator pKeyModelFace = Faces.begin() ; pKeyModelFace != FacesEnd ; ++pKeyModelFace)
                                {
                                    COpenInventorModelFace* pOpenInventorModelFace = new COpenInventorModelFace(pKeyModelFace->second);
                                    m_pObjectsFacesSetSwitch->addChild(pOpenInventorModelFace->GetBaseSwitch());
                                    pOpenInventorModelFace->SetVisualizationMode(COpenInventorModelPrimitive::eSelected);
                                    m_OpenInventorModelFaces.push_back(pOpenInventorModelFace);
                                }
                                m_pObjectsFacesSetSwitch->whichChild = SO_SWITCH_ALL;
                            }

                        }

                        SoSwitch* COpenInventorModelMesh::GetManipulableObjectsSwitch()
                        {
                            return m_pObjectsSetSwitch;
                        }

                        void COpenInventorModelMesh::ClearManipulableObjects()
                        {
                            if (m_OpenInventorModelVertices.size())
                            {
                                m_pObjectsVerticesSetSwitch->removeAllChildren();
                                std::deque<COpenInventorModelVertex*>::const_iterator OpenInventorModelVerticesEnd = m_OpenInventorModelVertices.end();
                                for (std::deque<COpenInventorModelVertex*>::const_iterator ppOpenInventorModelVertex = m_OpenInventorModelVertices.begin() ; ppOpenInventorModelVertex != OpenInventorModelVerticesEnd ; ++ppOpenInventorModelVertex)
                                {
                                    delete *ppOpenInventorModelVertex;
                                }
                                m_OpenInventorModelVertices.clear();
                            }

                            if (m_OpenInventorModelEdges.size())
                            {
                                m_pObjectsEdgesSetSwitch->removeAllChildren();
                                std::deque<COpenInventorModelEdge*>::const_iterator OpenInventorModelEdgesEnd = m_OpenInventorModelEdges.end();
                                for (std::deque<COpenInventorModelEdge*>::const_iterator ppOpenInventorModelEdge = m_OpenInventorModelEdges.begin() ; ppOpenInventorModelEdge != OpenInventorModelEdgesEnd ; ++ppOpenInventorModelEdge)
                                {
                                    delete *ppOpenInventorModelEdge;
                                }
                                m_OpenInventorModelEdges.clear();
                            }

                            if (m_OpenInventorModelFaces.size())
                            {
                                m_pObjectsFacesSetSwitch->removeAllChildren();
                                std::deque<COpenInventorModelFace*>::const_iterator OpenInventorModelFacesEnd = m_OpenInventorModelFaces.end();
                                for (std::deque<COpenInventorModelFace*>::const_iterator ppOpenInventorModelFace = m_OpenInventorModelFaces.begin() ; ppOpenInventorModelFace != OpenInventorModelFacesEnd ; ++ppOpenInventorModelFace)
                                {
                                    delete *ppOpenInventorModelFace;
                                }
                                m_OpenInventorModelFaces.clear();
                            }

                            m_pObjectsMultipleFacesSetSwitch->removeAllChildren();
                        }

                        void COpenInventorModelMesh::UpdateManipulableObjects()
                        {
                            if (m_OpenInventorModelVertices.size())
                            {
                                std::deque<COpenInventorModelVertex*>::const_iterator OpenInventorModelVerticesEnd = m_OpenInventorModelVertices.end();
                                for (std::deque<COpenInventorModelVertex*>::const_iterator ppOpenInventorModelVertex = m_OpenInventorModelVertices.begin() ; ppOpenInventorModelVertex != OpenInventorModelVerticesEnd ; ++ppOpenInventorModelVertex)
                                {
                                    (*ppOpenInventorModelVertex)->Update(true);
                                }
                            }

                            if (m_OpenInventorModelEdges.size())
                            {
                                std::deque<COpenInventorModelEdge*>::const_iterator OpenInventorModelEdgesEnd = m_OpenInventorModelEdges.end();
                                for (std::deque<COpenInventorModelEdge*>::const_iterator ppOpenInventorModelEdge = m_OpenInventorModelEdges.begin() ; ppOpenInventorModelEdge != OpenInventorModelEdgesEnd ; ++ppOpenInventorModelEdge)
                                {
                                    (*ppOpenInventorModelEdge)->Update(true);
                                }
                            }

                            if (m_OpenInventorModelFaces.size())
                            {
                                std::deque<COpenInventorModelFace*>::const_iterator OpenInventorModelFacesEnd = m_OpenInventorModelFaces.end();
                                for (std::deque<COpenInventorModelFace*>::const_iterator ppOpenInventorModelFace = m_OpenInventorModelFaces.begin() ; ppOpenInventorModelFace != OpenInventorModelFacesEnd ; ++ppOpenInventorModelFace)
                                {
                                    (*ppOpenInventorModelFace)->Update(true);
                                }
                            }
                        }

                        void COpenInventorModelMesh::Create()
                        {
                            m_pContentSwitch = new SoSwitch;
                            AddElement(m_pContentSwitch);

                            SoExtSelection* pMeshSelectionSeparator = new SoExtSelection;
                            m_pContentSwitch->addChild(pMeshSelectionSeparator);

                            m_pMeshCoordinates = new SoCoordinate3;
                            pMeshSelectionSeparator->addChild(m_pMeshCoordinates);

                            m_pNormals = new SoNormal;
                            pMeshSelectionSeparator->addChild(m_pNormals);

                            m_pPrimitiveSetSwitch = new SoSwitch;
                            pMeshSelectionSeparator->addChild(m_pPrimitiveSetSwitch);
                            m_pPrimitiveSetSwitch->whichChild = SO_SWITCH_ALL;

                            m_pObjectsSetSwitch = new SoSwitch;
                            pMeshSelectionSeparator->addChild(m_pObjectsSetSwitch);
                            m_pObjectsSetSwitch->whichChild = SO_SWITCH_ALL;

                            m_pObjectsFacesSetSwitch = new SoSwitch;
                            m_pObjectsSetSwitch->addChild(m_pObjectsFacesSetSwitch);
                            m_pObjectsFacesSetSwitch->whichChild = SO_SWITCH_ALL;

                            m_pObjectsMultipleFacesSetSwitch = new SoSwitch;
                            m_pObjectsSetSwitch->addChild(m_pObjectsMultipleFacesSetSwitch);
                            m_pObjectsMultipleFacesSetSwitch->whichChild = SO_SWITCH_ALL;

                            m_pObjectsEdgesSetSwitch = new SoSwitch;
                            m_pObjectsSetSwitch->addChild(m_pObjectsEdgesSetSwitch);
                            m_pObjectsEdgesSetSwitch->whichChild = SO_SWITCH_ALL;

                            m_pObjectsVerticesSetSwitch = new SoSwitch;
                            m_pObjectsSetSwitch->addChild(m_pObjectsVerticesSetSwitch);
                            m_pObjectsVerticesSetSwitch->whichChild = SO_SWITCH_ALL;

                            /// Edge Set

                            //Visible Edge Set
                            m_pVisibleIndexedLineSetSwitch = new SoSwitch;
                            m_pPrimitiveSetSwitch->addChild(m_pVisibleIndexedLineSetSwitch);
                            m_pVisibleIndexedLineSetSwitch->whichChild.setValue(SO_SWITCH_ALL);

                            m_pVisibleIndexedLineSetDrawStyle = new SoDrawStyle;
                            m_pVisibleIndexedLineSetSwitch->addChild(m_pVisibleIndexedLineSetDrawStyle);
                            m_pVisibleIndexedLineSetDrawStyle->lineWidth.setValue(3.0f);
                            m_pVisibleIndexedLineSetDrawStyle->linePattern.setValue(0XFFFF);

                            m_pVisibleIndexedLineSetMaterial = new SoMaterial;
                            m_pVisibleIndexedLineSetSwitch->addChild(m_pVisibleIndexedLineSetMaterial);
                            m_pVisibleIndexedLineSetMaterial->diffuseColor.setValue(0.0f, 0.0f, 0.0f);

                            m_pVisibleIndexedLineSet = new SoIndexedLineSet;
                            m_pVisibleIndexedLineSetSwitch->addChild(m_pVisibleIndexedLineSet);
                            m_pVisibleIndexedLineSet->setUserData(this);

                            //Dynamic Visible Edge Set
                            m_pDynamicVisibleIndexedLineSetSwitch = new SoSwitch;
                            m_pPrimitiveSetSwitch->addChild(m_pDynamicVisibleIndexedLineSetSwitch);
                            m_pDynamicVisibleIndexedLineSetSwitch->whichChild.setValue(SO_SWITCH_ALL);

                            m_pDynamicVisibleIndexedLineSetDrawStyle = new SoDrawStyle;
                            m_pDynamicVisibleIndexedLineSetSwitch->addChild(m_pDynamicVisibleIndexedLineSetDrawStyle);
                            m_pDynamicVisibleIndexedLineSetDrawStyle->lineWidth.setValue(3.0f);
                            m_pDynamicVisibleIndexedLineSetDrawStyle->linePattern.setValue(0X3F3F);

                            m_pDynamicVisibleIndexedLineSetMaterial = new SoMaterial;
                            m_pDynamicVisibleIndexedLineSetSwitch->addChild(m_pDynamicVisibleIndexedLineSetMaterial);
                            m_pDynamicVisibleIndexedLineSetMaterial->diffuseColor.setValue(1.0f, 1.0f, 0.0f);

                            m_pDynamicVisibleIndexedLineSet = new SoIndexedLineSet;
                            m_pDynamicVisibleIndexedLineSetSwitch->addChild(m_pDynamicVisibleIndexedLineSet);
                            m_pDynamicVisibleIndexedLineSet->setUserData(this);

                            //Non Visible Edge Set
                            m_pNonVisibleIndexedLineSetSwitch = new SoSwitch;
                            m_pPrimitiveSetSwitch->addChild(m_pNonVisibleIndexedLineSetSwitch);
                            m_pNonVisibleIndexedLineSetSwitch->whichChild.setValue(SO_SWITCH_ALL);

                            m_pNonVisibleIndexedLineSetDrawStyle = new SoDrawStyle;
                            m_pNonVisibleIndexedLineSetSwitch->addChild(m_pNonVisibleIndexedLineSetDrawStyle);
                            m_pNonVisibleIndexedLineSetDrawStyle->lineWidth.setValue(3.0f);
                            m_pNonVisibleIndexedLineSetDrawStyle->linePattern.setValue(0X0F0F);

                            m_pNonVisibleIndexedLineSetMaterial = new SoMaterial;
                            m_pNonVisibleIndexedLineSetSwitch->addChild(m_pNonVisibleIndexedLineSetMaterial);
                            m_pNonVisibleIndexedLineSetMaterial->diffuseColor.setValue(1.0f, 0.0f, 0.0f);
                            m_pNonVisibleIndexedLineSetMaterial->transparency.setValue(0.0f);

                            m_pNonVisibleIndexedLineSet = new SoIndexedLineSet;
                            m_pNonVisibleIndexedLineSetSwitch->addChild(m_pNonVisibleIndexedLineSet);
                            m_pNonVisibleIndexedLineSet->setUserData(this);

                            /// Face Set

                            //Visible Face Set
                            m_pVisibleIndexedFaceSetSwitch = new SoSwitch;
                            m_pPrimitiveSetSwitch->addChild(m_pVisibleIndexedFaceSetSwitch);
                            m_pVisibleIndexedFaceSetSwitch->whichChild.setValue(SO_SWITCH_ALL);

                            m_pVisibleIndexedFaceSetDrawStyle = new SoDrawStyle;
                            m_pVisibleIndexedFaceSetSwitch->addChild(m_pVisibleIndexedFaceSetDrawStyle);

                            m_pVisibleIndexedFaceSetMaterial = new SoMaterial;
                            m_pVisibleIndexedFaceSetSwitch->addChild(m_pVisibleIndexedFaceSetMaterial);

                            m_pVisibleIndexedFaceSet = new SoIndexedFaceSet;
                            m_pVisibleIndexedFaceSetSwitch->addChild(m_pVisibleIndexedFaceSet);
                            m_pVisibleIndexedFaceSet->setUserData(this);

                            //Non Visible Face Set
                            m_pNonVisibleIndexedFaceSetSwitch = new SoSwitch;
                            m_pPrimitiveSetSwitch->addChild(m_pNonVisibleIndexedFaceSetSwitch);
                            m_pNonVisibleIndexedFaceSetSwitch->whichChild.setValue(SO_SWITCH_ALL);

                            m_pNonVisibleIndexedFaceSetDrawStyle = new SoDrawStyle;
                            m_pNonVisibleIndexedFaceSetSwitch->addChild(m_pNonVisibleIndexedFaceSetDrawStyle);

                            m_pNonVisibleIndexedFaceSetMaterial = new SoMaterial;
                            m_pNonVisibleIndexedFaceSetSwitch->addChild(m_pNonVisibleIndexedFaceSetMaterial);

                            m_pNonVisibleIndexedFaceSet = new SoIndexedFaceSet;
                            m_pNonVisibleIndexedFaceSetSwitch->addChild(m_pNonVisibleIndexedFaceSet);
                            m_pNonVisibleIndexedFaceSet->setUserData(this);

                            //Dynamic Visible Face Set
                            m_pDynamicVisibleIndexedFaceSetSwitch = new SoSwitch;
                            m_pPrimitiveSetSwitch->addChild(m_pDynamicVisibleIndexedFaceSetSwitch);
                            m_pDynamicVisibleIndexedFaceSetSwitch->whichChild.setValue(SO_SWITCH_ALL);

                            m_pDynamicVisibleIndexedFaceSetDrawStyle = new SoDrawStyle;
                            m_pDynamicVisibleIndexedFaceSetSwitch->addChild(m_pDynamicVisibleIndexedFaceSetDrawStyle);

                            m_pDynamicVisibleIndexedFaceSetMaterial = new SoMaterial;
                            m_pDynamicVisibleIndexedFaceSetSwitch->addChild(m_pDynamicVisibleIndexedFaceSetMaterial);

                            m_pDynamicVisibleIndexedFaceSet = new SoIndexedFaceSet;
                            m_pDynamicVisibleIndexedFaceSetSwitch->addChild(m_pDynamicVisibleIndexedFaceSet);
                            m_pDynamicVisibleIndexedFaceSet->setUserData(this);



                            /// Bounding Box

                            m_pBoundingBoxSwitch = new SoSwitch;
                            m_pContentSwitch->addChild(m_pBoundingBoxSwitch);
                            m_pBoundingBoxSwitch->whichChild = SO_SWITCH_NONE;

                            m_pBoundingBoxCoordinates = new SoCoordinate3;
                            m_pBoundingBoxSwitch->addChild(m_pBoundingBoxCoordinates);

                            m_pCubeDrawStyle = new SoDrawStyle;
                            m_pBoundingBoxSwitch->addChild(m_pCubeDrawStyle);
                            m_pCubeDrawStyle->lineWidth.setValue(1.0f);
                            m_pCubeDrawStyle->linePattern.setValue(0X3FFF);

                            m_pBoundingBooxMaterial = new SoMaterial;
                            m_pBoundingBoxSwitch->addChild(m_pBoundingBooxMaterial);
                            m_pBoundingBooxMaterial->emissiveColor.setValue(1.0f, 0.0f, 1.0f);
                            m_pBoundingBooxMaterial->diffuseColor.setValue(1.0f, 0.0f, 1.0f);

                            m_pIndexedLineSetCube = new SoIndexedLineSet;
                            m_pBoundingBoxSwitch->addChild(m_pIndexedLineSetCube);
                            m_pIndexedLineSetCube->setUserData(this);

                            /// Vertices Set

                            //Visible Vertices Set
                            m_VisibleVertexSetVisualization.Create(m_pPrimitiveSetSwitch, this, 8, 0.0f, 0.0f, 0.0f);
                            m_NonVisibleVertexSetVisualization.Create(m_pPrimitiveSetSwitch, this, 8, 1.0f, 0.0f, 0.0f);
                            m_DynamicVisibleVertexSetVisualization.Create(m_pPrimitiveSetSwitch, this, 8, 1.0f, 0.6f, 0.0f);
                        }

                        void COpenInventorModelMesh::FullUpdate()
                        {
                            UpdateMaterial();
                            UpdateBoundingBox();
                            UpdateCoordinates();
                            UpdateFaces();
                            UpdateEdges();
                            switch (GetVisualizationMode())
                            {
                                case COpenInventorModelPrimitive::eDefault:
                                    m_pContentSwitch->whichChild = SO_SWITCH_ALL;
                                    m_pPrimitiveSetSwitch->whichChild = SO_SWITCH_ALL;
                                    break;
                                case COpenInventorModelPrimitive::eSelected:
                                    m_pContentSwitch->whichChild = SO_SWITCH_ALL;
                                    m_pPrimitiveSetSwitch->whichChild = SO_SWITCH_ALL;
                                    break;
                                case COpenInventorModelPrimitive::eEdition:
                                    m_pContentSwitch->whichChild = SO_SWITCH_ALL;
                                    m_pPrimitiveSetSwitch->whichChild = SO_SWITCH_NONE;
                                    break;
                                case COpenInventorModelPrimitive::eFull:
                                    m_pContentSwitch->whichChild = SO_SWITCH_ALL;
                                    m_pPrimitiveSetSwitch->whichChild = SO_SWITCH_ALL;
                                    break;
                            }
                            UpdateManipulableObjects();
                        }

                        void COpenInventorModelMesh::UpdateBoundingBox()
                        {
                            const Base::CModelMesh* pModelMesh = dynamic_cast<const Base::CModelMesh*>(GetModelElement());
                            const Containers::_3D::CContinuousBoundingBox3D BoundingBox = pModelMesh->GetBoundingBox();
                            Real X0 = Real(0.0), Y0 = Real(0.0), Z0 = Real(0.0), X1 = Real(0.0), Y1 = Real(0.0), Z1 = Real(0.0);
                            BoundingBox.GetBounds(X0, Y0, Z0, X1, Y1, Z1);
                            if (IsNonZero(m_BoundingBoxOffset))
                            {
                                X0 -= m_BoundingBoxOffset;
                                Y0 -= m_BoundingBoxOffset;
                                Z0 -= m_BoundingBoxOffset;
                                X1 += m_BoundingBoxOffset;
                                Y1 += m_BoundingBoxOffset;
                                Z1 += m_BoundingBoxOffset;
                            }
                            const float Vertices[24] = { float(X0), float(Y0), float(Z0), float(X1), float(Y0), float(Z0), float(X1), float(Y1), float(Z0), float(X0), float(Y1), float(Z0), float(X0), float(Y0), float(Z1), float(X1), float(Y0), float(Z1), float(X1), float(Y1), float(Z1), float(X0), float(Y1), float(Z1) };
                            m_pBoundingBoxCoordinates->point.setValues(0, 8, (const float (*)[3]) Vertices);
                            const int Indices[20] = { 0, 1, 2, 3, 0, 4, 5, 6, 7, 4, -1, 1, 5, -1, 2, 6, -1, 3, 7, -1 };
                            m_pIndexedLineSetCube->coordIndex.setValues(0, 20, Indices);
                        }

                        void COpenInventorModelMesh::UpdateCoordinates()
                        {
                            const Base::CModelMesh* pModelMesh = dynamic_cast<const Base::CModelMesh*>(GetModelElement());
                            const std::map<int, Base::CModelVertex*>& Vertices = pModelMesh->GetVertices();
                            const int TotalVertices = Vertices.size();

                            const int TotalCoordinates = TotalVertices * 3;
                            float* const pGeneralVerticesBuffer = new float[TotalCoordinates];
                            float* const pVisibleVerticesBuffer = new float[TotalCoordinates];
                            float* const pNonVisibleVerticesBuffer = new float[TotalCoordinates];
                            float* const pDynamicVisibleVerticesBuffer = new float[TotalCoordinates];

                            float* pGeneralVertex = pGeneralVerticesBuffer;
                            float* pVisibleVertex = pVisibleVerticesBuffer;
                            float* pNonVisibleVertex = pNonVisibleVerticesBuffer;
                            float* pDynamicVertex = pDynamicVisibleVerticesBuffer;

                            int TotalVisible = 0, TotalNonVisible = 0, TotalDynamicVisible = 0;

                            std::map<int, Base::CModelVertex*>::const_iterator VerticesEnd = Vertices.end();
                            for (std::map<int, Base::CModelVertex*>::const_iterator pKeyModelVertex = Vertices.begin() ; pKeyModelVertex != VerticesEnd ; ++pKeyModelVertex)
                            {
                                const Mathematics::_3D::CVector3D& Point = pKeyModelVertex->second->GetPoint(true);

                                const float X = float(Point.GetX());
                                const float Y = float(Point.GetY());
                                const float Z = float(Point.GetZ());

                                *pGeneralVertex++ = X;
                                *pGeneralVertex++ = Y;
                                *pGeneralVertex++ = Z;

                                switch (pKeyModelVertex->second->GetVisibility(false))
                                {
                                    case Base::CModelElement::eStaticVisible:
                                        *pVisibleVertex++ = X;
                                        *pVisibleVertex++ = Y;
                                        *pVisibleVertex++ = Z;
                                        ++TotalVisible;
                                        break;
                                    case Base::CModelElement::eStaticOccluded:
                                        *pNonVisibleVertex++ = X;
                                        *pNonVisibleVertex++ = Y;
                                        *pNonVisibleVertex++ = Z;
                                        ++TotalNonVisible;
                                        break;
                                    case Base::CModelElement::eDynamicVisible:
                                        *pDynamicVertex++ = X;
                                        *pDynamicVertex++ = Y;
                                        *pDynamicVertex++ = Z;
                                        ++TotalDynamicVisible;
                                        break;
                                }
                            }

                            m_pMeshCoordinates->point.setValues(0, TotalVertices, (const float (*)[3]) pGeneralVerticesBuffer);

                            if (TotalVisible)
                            {
                                m_VisibleVertexSetVisualization.m_pCoordinates->point.setValues(0, TotalVisible, (const float (*)[3]) pVisibleVerticesBuffer);
                            }
                            else
                            {
                                m_VisibleVertexSetVisualization.m_pCoordinates->point.setNum(0);
                            }


                            if (TotalNonVisible)
                            {
                                m_NonVisibleVertexSetVisualization.m_pCoordinates->point.setValues(0, TotalNonVisible, (const float (*)[3]) pNonVisibleVerticesBuffer);
                            }
                            else
                            {
                                m_NonVisibleVertexSetVisualization.m_pCoordinates->point.setNum(0);
                            }


                            if (TotalDynamicVisible)
                            {
                                m_DynamicVisibleVertexSetVisualization.m_pCoordinates->point.setValues(0, TotalDynamicVisible, (const float (*)[3]) pDynamicVisibleVerticesBuffer);
                            }
                            else
                            {
                                m_DynamicVisibleVertexSetVisualization.m_pCoordinates->point.setNum(0);
                            }


                            m_VisibleVertexSetVisualization.m_pPointSet->numPoints.setValue(TotalVisible);
                            m_NonVisibleVertexSetVisualization.m_pPointSet->numPoints.setValue(TotalNonVisible);
                            m_DynamicVisibleVertexSetVisualization.m_pPointSet->numPoints.setValue(TotalDynamicVisible);

                            delete[] pGeneralVerticesBuffer;
                            delete[] pVisibleVerticesBuffer;
                            delete[] pNonVisibleVerticesBuffer;
                            delete[] pDynamicVisibleVerticesBuffer;
                        }

                        void COpenInventorModelMesh::UpdateFaces()
                        {
                            m_pVisibleIndexedFaceSet->coordIndex.setNum(0);
                            m_pNonVisibleIndexedFaceSet->coordIndex.setNum(0);
                            m_pDynamicVisibleIndexedFaceSet->coordIndex.setNum(0);
                            const Base::CModelMesh* pModelMesh = dynamic_cast<const Base::CModelMesh*>(GetModelElement());
                            const std::map<int, Base::CModelFace*>& Faces = pModelMesh->GetFaces();
                            if (Faces.size())
                            {
                                int TotalVisibleFaces = 0, TotalNonVisibleFaces = 0, TotalDynamicVisibleFaces = 0;
                                std::map<int, Base::CModelFace*>::const_iterator FacesEnd = Faces.end();
                                for (std::map<int, Base::CModelFace*>::const_iterator pKeyModelFace = Faces.begin() ; pKeyModelFace != FacesEnd ; ++pKeyModelFace)
                                    switch (pKeyModelFace->second->GetVisibility(true))
                                    {
                                        case Base::CModelElement::eStaticOccluded:
                                            ++TotalNonVisibleFaces;
                                            break;
                                        case Base::CModelElement::eStaticVisible:
                                            ++TotalVisibleFaces;
                                            break;
                                        case Base::CModelElement::eDynamicVisible:
                                            ++TotalDynamicVisibleFaces;
                                            break;
                                    }
                                int* const pVisibleFacesIndicesBuffer = TotalVisibleFaces ? new int[TotalVisibleFaces * 4] : nullptr;
                                int* pVisibleFacesIndices = pVisibleFacesIndicesBuffer;
                                int* const pNonFacesIndicesBuffer = TotalNonVisibleFaces ? new int[TotalNonVisibleFaces * 4] : nullptr;
                                int* pNonVisibleFacesIndices = pNonFacesIndicesBuffer;
                                int* const pDynamicFacesIndicesBuffer = TotalDynamicVisibleFaces ? new int[TotalDynamicVisibleFaces * 4] : nullptr;
                                int* pDynamicVisibleFacesIndices = pDynamicFacesIndicesBuffer;
                                for (std::map<int, Base::CModelFace*>::const_iterator pKeyModelFace = Faces.begin() ; pKeyModelFace != FacesEnd ; ++pKeyModelFace)
                                    switch (pKeyModelFace->second->GetVisibility(true))
                                    {
                                        case Base::CModelElement::eStaticOccluded:
                                            *pNonVisibleFacesIndices++ = pKeyModelFace->second->GetReadOnlyOrientedVertexA()->GetReadOnlyModelVertex()->GetVertexInMeshIndex();
                                            *pNonVisibleFacesIndices++ = pKeyModelFace->second->GetReadOnlyOrientedVertexB()->GetReadOnlyModelVertex()->GetVertexInMeshIndex();
                                            *pNonVisibleFacesIndices++ = pKeyModelFace->second->GetReadOnlyOrientedVertexC()->GetReadOnlyModelVertex()->GetVertexInMeshIndex();
                                            *pNonVisibleFacesIndices++ = -1;
                                            break;
                                        case Base::CModelElement::eStaticVisible:
                                            *pVisibleFacesIndices++ = pKeyModelFace->second->GetReadOnlyOrientedVertexA()->GetReadOnlyModelVertex()->GetVertexInMeshIndex();
                                            *pVisibleFacesIndices++ = pKeyModelFace->second->GetReadOnlyOrientedVertexB()->GetReadOnlyModelVertex()->GetVertexInMeshIndex();
                                            *pVisibleFacesIndices++ = pKeyModelFace->second->GetReadOnlyOrientedVertexC()->GetReadOnlyModelVertex()->GetVertexInMeshIndex();
                                            *pVisibleFacesIndices++ = -1;
                                            break;
                                        case Base::CModelElement::eDynamicVisible:
                                            *pDynamicVisibleFacesIndices++ = pKeyModelFace->second->GetReadOnlyOrientedVertexA()->GetReadOnlyModelVertex()->GetVertexInMeshIndex();
                                            *pDynamicVisibleFacesIndices++ = pKeyModelFace->second->GetReadOnlyOrientedVertexB()->GetReadOnlyModelVertex()->GetVertexInMeshIndex();
                                            *pDynamicVisibleFacesIndices++ = pKeyModelFace->second->GetReadOnlyOrientedVertexC()->GetReadOnlyModelVertex()->GetVertexInMeshIndex();
                                            *pDynamicVisibleFacesIndices++ = -1;
                                            break;
                                    }
                                if (pVisibleFacesIndicesBuffer)
                                {
                                    m_pVisibleIndexedFaceSet->coordIndex.setValues(0, pVisibleFacesIndices - pVisibleFacesIndicesBuffer, pVisibleFacesIndicesBuffer);
                                    delete[] pVisibleFacesIndicesBuffer;
                                }
                                if (pNonFacesIndicesBuffer)
                                {
                                    m_pNonVisibleIndexedFaceSet->coordIndex.setValues(0, pNonVisibleFacesIndices - pNonFacesIndicesBuffer, pNonFacesIndicesBuffer);
                                    delete[] pNonFacesIndicesBuffer;
                                }
                                if (pDynamicFacesIndicesBuffer)
                                {
                                    m_pDynamicVisibleIndexedFaceSet->coordIndex.setValues(0, pDynamicVisibleFacesIndices - pDynamicFacesIndicesBuffer, pDynamicFacesIndicesBuffer);
                                    delete[] pDynamicFacesIndicesBuffer;
                                }
                            }
                        }

                        void COpenInventorModelMesh::UpdateEdges()
                        {
                            m_pVisibleIndexedLineSet->coordIndex.setNum(0);
                            m_pNonVisibleIndexedLineSet->coordIndex.setNum(0);
                            m_pDynamicVisibleIndexedLineSet->coordIndex.setNum(0);
                            const Base::CModelMesh* pModelMesh = dynamic_cast<const Base::CModelMesh*>(GetModelElement());
                            const std::map<int, Base::CModelEdge*>& Edges = pModelMesh->GetEdges();
                            if (Edges.size())
                            {
                                int TotalVisibleEdges = 0, TotalNonVisibleEdges = 0, TotalDynamicVisibleEdges = 0;
                                std::map<int, Base::CModelEdge*>::const_iterator EdgesEnd = Edges.end();
                                for (std::map<int, Base::CModelEdge*>::const_iterator pKeyModelEdge = Edges.begin() ; pKeyModelEdge != EdgesEnd ; ++pKeyModelEdge)
                                    switch (pKeyModelEdge->second->GetVisibility(true))
                                    {
                                        case Base::CModelElement::eStaticOccluded:
                                            ++TotalNonVisibleEdges;
                                            break;
                                        case Base::CModelElement::eStaticVisible:
                                            ++TotalVisibleEdges;
                                            break;
                                        case Base::CModelElement::eDynamicVisible:
                                            ++TotalDynamicVisibleEdges;
                                            break;
                                    }
                                int* const pVisibleEdgesIndicesBuffer = TotalVisibleEdges ? new int[TotalVisibleEdges * 3] : nullptr;
                                int* pVisibleEdgesIndices = pVisibleEdgesIndicesBuffer;
                                int* const pNonVisibleEdgesIndicesBuffer = TotalNonVisibleEdges ? new int[TotalNonVisibleEdges * 3] : nullptr;
                                int* pNonVisibleEdgesIndices = pNonVisibleEdgesIndicesBuffer;
                                int* const pDynamicVisibleEdgesIndicesBuffer = TotalDynamicVisibleEdges ? new int[TotalDynamicVisibleEdges * 3] : nullptr;
                                int* pDynamicVisibleEdgesIndices = pDynamicVisibleEdgesIndicesBuffer;
                                for (std::map<int, Base::CModelEdge*>::const_iterator pKeyModelEdge = Edges.begin() ; pKeyModelEdge != EdgesEnd ; ++pKeyModelEdge)
                                    switch (pKeyModelEdge->second->GetVisibility(true))
                                    {
                                        case Base::CModelElement::eStaticOccluded:
                                            *pNonVisibleEdgesIndices++ = pKeyModelEdge->second->GetReadOnlyVertexA()->GetVertexInMeshIndex();
                                            *pNonVisibleEdgesIndices++ = pKeyModelEdge->second->GetReadOnlyVertexB()->GetVertexInMeshIndex();
                                            *pNonVisibleEdgesIndices++ = -1;
                                            break;
                                        case Base::CModelElement::eStaticVisible:
                                            *pVisibleEdgesIndices++ = pKeyModelEdge->second->GetReadOnlyVertexA()->GetVertexInMeshIndex();
                                            *pVisibleEdgesIndices++ = pKeyModelEdge->second->GetReadOnlyVertexB()->GetVertexInMeshIndex();
                                            *pVisibleEdgesIndices++ = -1;
                                            break;
                                        case Base::CModelElement::eDynamicVisible:
                                            *pDynamicVisibleEdgesIndices++ = pKeyModelEdge->second->GetReadOnlyVertexA()->GetVertexInMeshIndex();
                                            *pDynamicVisibleEdgesIndices++ = pKeyModelEdge->second->GetReadOnlyVertexB()->GetVertexInMeshIndex();
                                            *pDynamicVisibleEdgesIndices++ = -1;

                                            break;
                                    }
                                if (pVisibleEdgesIndicesBuffer)
                                {
                                    m_pVisibleIndexedLineSet->coordIndex.setValues(0, pVisibleEdgesIndices - pVisibleEdgesIndicesBuffer, pVisibleEdgesIndicesBuffer);
                                    delete[] pVisibleEdgesIndicesBuffer;
                                }
                                if (pNonVisibleEdgesIndicesBuffer)
                                {
                                    m_pNonVisibleIndexedLineSet->coordIndex.setValues(0, pNonVisibleEdgesIndices - pNonVisibleEdgesIndicesBuffer, pNonVisibleEdgesIndicesBuffer);
                                    delete[] pNonVisibleEdgesIndicesBuffer;
                                }
                                if (pDynamicVisibleEdgesIndicesBuffer)
                                {
                                    m_pDynamicVisibleIndexedLineSet->coordIndex.setValues(0, pDynamicVisibleEdgesIndices - pDynamicVisibleEdgesIndicesBuffer, pDynamicVisibleEdgesIndicesBuffer);
                                    delete[] pDynamicVisibleEdgesIndicesBuffer;
                                }
                            }
                        }

                        void COpenInventorModelMesh::UpdateMaterial()
                        {
                            const Base::CModelMesh* pModelMesh = dynamic_cast<const Base::CModelMesh*>(GetModelElement());
                            Real R = Real(0.0), G = Real(0.0), B = Real(0.0), A = Real(0.0);
                            pModelMesh->GetMaterial(R, G, B, A);
                            SetColor(float(R), float(G), float(B));
                            SetTransparency(1.0f - float(A));
                        }

                        bool COpenInventorModelMesh::IsComposedVisible()
                        {
                            if (COpenInventorModelPrimitive::IsComposedVisible())
                            {
                                const Base::CModelMesh* pModelMesh = dynamic_cast<const Base::CModelMesh*>(GetModelElement());
                                if (pModelMesh)
                                {
                                    const RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::CContainerOpenInventorSet* pContainerSet = COpenInventorModelPrimitive::GetContainerSet();
                                    if (pContainerSet)
                                    {
                                        const std::map<int, Base::CModelVertex*>& Vertices = pModelMesh->GetVertices();
                                        std::map<int, Base::CModelVertex*>::const_iterator VerticesEnd = Vertices.end();
                                        for (std::map<int, Base::CModelVertex*>::const_iterator pKeyModelVertex = Vertices.begin() ; pKeyModelVertex != VerticesEnd ; ++pKeyModelVertex)
                                        {
                                            const Mathematics::_3D::CVector3D& Point = pKeyModelVertex->second->GetPoint(true);
                                            const SbVec3f VisualizationPoint(float(Point.GetX()), float(Point.GetY()), float(Point.GetZ()));
                                            switch (pContainerSet->MapPoint(VisualizationPoint))
                                            {
                                                case RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::eEmptyInclusion:
                                                case RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::eImproperInclusion:
                                                    continue;
                                                    break;
                                                case RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::ePartialInclusion:
                                                case RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::eFullInclusion:
                                                    return true;
                                            }
                                        }
                                        return false;
                                    }
                                    return true;
                                }
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}
