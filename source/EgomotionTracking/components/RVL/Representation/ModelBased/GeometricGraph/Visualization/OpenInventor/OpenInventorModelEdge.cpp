/*
 * OpenInventorModelEdge.cpp
 */

#include "OpenInventorModelEdge.h"
#include "../../Base/ModelVertex.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Visualization
                {
                    namespace OpenInventor
                    {
                        COpenInventorModelEdge::COpenInventorModelEdge(Base::CModelEdge* pModelEdge) :
                            COpenInventorModelPrimitive(pModelEdge, true, true, true),
                            m_pVisualizationModeSoSwitch(nullptr),
                            m_pSoCoordinate3(nullptr),
                            m_pSoLineSet(nullptr),
                            m_pSoTransform(nullptr),
                            m_pSelectedCylinder(nullptr)
                        {
                            Create();
                            Base::CModelElement* pModelElement = COpenInventorModelPrimitive::GetModelElement();
                            pModelElement->SetElementData(this);
                        }

                        COpenInventorModelEdge::~COpenInventorModelEdge()
                        {
                            Base::CModelElement* pModelElement = COpenInventorModelPrimitive::GetModelElement();
                            pModelElement->ClearElementData();
                        }

                        const Base::CModelEdge* COpenInventorModelEdge::GetModelEdge() const
                        {
                            return dynamic_cast<const Base::CModelEdge*>(GetModelElement());
                        }

                        Base::CModelEdge* COpenInventorModelEdge::GetModelEdge()
                        {
                            return dynamic_cast<Base::CModelEdge*>(GetModelElement());
                        }

                        void COpenInventorModelEdge::Create()
                        {
                            m_pVisualizationModeSoSwitch = new SoSwitch;
                            AddElement(m_pVisualizationModeSoSwitch);

                            SoExtSelection* pLineSelectionSeparator = new SoExtSelection;
                            m_pVisualizationModeSoSwitch->addChild(pLineSelectionSeparator);

                            m_pSoCoordinate3 = new SoCoordinate3;
                            pLineSelectionSeparator->addChild(m_pSoCoordinate3);

                            m_pSoLineSet = new SoLineSet;
                            pLineSelectionSeparator->addChild(m_pSoLineSet);
                            m_pSoLineSet->setUserData(this);

                            SoExtSelection* pCylinderSelectionSeparator = new SoExtSelection;
                            m_pVisualizationModeSoSwitch->addChild(pCylinderSelectionSeparator);

                            m_pSoTransform = new SoTransform;
                            pCylinderSelectionSeparator->addChild(m_pSoTransform);

                            m_pSelectedCylinder = new SoCylinder;
                            pCylinderSelectionSeparator->addChild(m_pSelectedCylinder);

                            m_pSelectedCylinder->radius = _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_ * 0.25f;
                            m_pSelectedCylinder->parts = SoCylinder::SIDES;
                            m_pSelectedCylinder->setUserData(this);

                            m_pVisualizationModeSoSwitch->whichChild = 0;
                        }

                        void COpenInventorModelEdge::FullUpdate()
                        {
                            const Base::CModelEdge* pModelEdge = dynamic_cast<const Base::CModelEdge*>(COpenInventorModelPrimitive::GetModelElement());
                            if (pModelEdge)
                            {
                                const Mathematics::_3D::CVector3D& VertexPointA = pModelEdge->GetReadOnlyVertexA()->GetPoint(true);
                                const Mathematics::_3D::CVector3D& VertexPointB = pModelEdge->GetReadOnlyVertexB()->GetPoint(true);
                                const Mathematics::_3D::CVector3D Midpoint = pModelEdge->GetMidPoint(true);
                                const Mathematics::_3D::CVector3D Direction = pModelEdge->GetDirection(true);
                                m_pSelectedCylinder->height.setValue(float(pModelEdge->GetLength()));
                                m_pSoTransform->translation.setValue(float(Midpoint.GetX()), float(Midpoint.GetY()), float(Midpoint.GetZ()));
                                m_pSoTransform->rotation.setValue(SbRotation(SbVec3f(0.0f, 1.0f, 0.0f), SbVec3f(float(Direction.GetX()), float(Direction.GetY()), float(Direction.GetZ()))));
                                m_pSoCoordinate3->point.setNum(2);
                                m_pSoCoordinate3->point.set1Value(0, float(VertexPointA.GetX()), float(VertexPointA.GetY()), float(VertexPointA.GetZ()));
                                m_pSoCoordinate3->point.set1Value(1, float(VertexPointB.GetX()), float(VertexPointB.GetY()), float(VertexPointB.GetZ()));
                                switch (GetVisualizationMode())
                                {
                                    case COpenInventorModelPrimitive::eDefault:
                                        m_pVisualizationModeSoSwitch->whichChild = SO_SWITCH_NONE;
                                        break;
                                    case COpenInventorModelPrimitive::eSelected:
                                        m_pVisualizationModeSoSwitch->whichChild = 1;
                                        break;
                                    case COpenInventorModelPrimitive::eEdition:
                                        m_pVisualizationModeSoSwitch->whichChild = 1;
                                        break;
                                    case COpenInventorModelPrimitive::eFull:
                                        m_pVisualizationModeSoSwitch->whichChild = 1;
                                        break;
                                }

                                const Real* pMaterial = s_MaterialStatePrimitiveTable[pModelEdge->GetTypeId()][pModelEdge->GetVisibility(false)];
                                SetColor(float(pMaterial[0]), float(pMaterial[1]), float(pMaterial[2]));
                                SetTransparency(1.0f - float(pMaterial[3]));
                            }
                        }

                        bool COpenInventorModelEdge::IsComposedVisible()
                        {
                            if (COpenInventorModelPrimitive::IsComposedVisible() || (GetVisualizationMode() != COpenInventorModelPrimitive::eDefault))
                            {
                                const Base::CModelEdge* pModelEdge = dynamic_cast<const Base::CModelEdge*>(COpenInventorModelPrimitive::GetModelElement());
                                if (pModelEdge)
                                {
                                    const RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::CContainerOpenInventorSet* pContainerSet = COpenInventorModelPrimitive::GetContainerSet();
                                    if (pContainerSet)
                                    {
                                        const Mathematics::_3D::CVector3D& VertexPointA = pModelEdge->GetReadOnlyVertexA()->GetPoint(true);
                                        const Mathematics::_3D::CVector3D& VertexPointB = pModelEdge->GetReadOnlyVertexB()->GetPoint(true);
                                        const SbVec3f VisualizationPointA(float(VertexPointA.GetX()), float(VertexPointA.GetY()), float(VertexPointA.GetZ()));
                                        const SbVec3f VisualizationPointB(float(VertexPointB.GetX()), float(VertexPointB.GetY()), float(VertexPointB.GetZ()));
                                        RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::InclusionType InclusionTypeA = pContainerSet->MapPoint(VisualizationPointA);
                                        RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::InclusionType InclusionTypeB = pContainerSet->MapPoint(VisualizationPointB);
                                        const bool VisibleA = (InclusionTypeA == RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::ePartialInclusion) || (InclusionTypeA == RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::eFullInclusion);
                                        const bool VisibleB = (InclusionTypeB == RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::ePartialInclusion) || (InclusionTypeB == RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::eFullInclusion);
                                        return VisibleA || VisibleB;
                                    }
                                    return true;
                                }
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}
