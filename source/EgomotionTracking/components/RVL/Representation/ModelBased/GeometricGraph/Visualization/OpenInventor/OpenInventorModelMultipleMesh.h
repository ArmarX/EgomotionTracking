/*
 * OpenInventorModelMultipleMesh.h
 */

#pragma once

#include "../../Base/ModelMultipleMesh.h"
#include "../../Base/ModelMesh.h"
#include "OpenInventorModelPrimitive.h"
#include "OpenInventorModelMesh.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Visualization
                {
                    namespace OpenInventor
                    {
                        class COpenInventorModelMultipleMesh : public COpenInventorModelPrimitive
                        {
                        public:

                            COpenInventorModelMultipleMesh(Base::CModelMultipleMesh* pModelMultipleMesh, const bool LoadMeshes);
                            ~COpenInventorModelMultipleMesh() override;

                            const Base::CModelMultipleMesh* GetMultipleMesh() const;
                            Base::CModelMultipleMesh* GetMultipleMesh();

                            bool GetBoundingBoxVisible() const;
                            void SetBoundingBoxVisible(const bool Visible);

                            Real GetBoundingOffset() const;
                            void SetBoundingOffset(const Real Offset);

                            void GetBoundingBoxLineMaterial(Byte& R, Byte& G, Byte& B, Byte& A) const;
                            void SetBoundingBoxLineMaterial(const Byte R, const Byte G, const Byte B, const Byte A);

                            Real GetBoundingBoxLineWidth() const;
                            void SetBoundingBoxLineWidth(const Real Width);

                            ushort GetBoundingBoxLinePattern() const;
                            void SetBoundingBoxLinePattern(const ushort Pattern);

                            const std::list<COpenInventorModelMesh*>& GetMeshes() const;

                            bool DeleteMeshVisualization(COpenInventorModelMesh*& pOpenInventorModelMesh);

                        protected:

                            void Create() override;
                            void FullUpdate() override;
                            bool IsComposedVisible() override;

                            void UpdateBoundingBox();

                            bool CreateMeshes();
                            void DestroyMeshes();

                        private:

                            Real m_BoundingBoxOffset;
                            SoSwitch* m_pBoundingBoxSwitch;
                            SoCoordinate3* m_pBoundingBoxCoordinates;
                            SoDrawStyle* m_pCubeDrawStyle;
                            SoMaterial* m_pBoundingBooxMaterial;
                            SoIndexedLineSet* m_pIndexedLineSetCube;
                            SoSwitch* m_pMeshesSwitch;

                            std::list<COpenInventorModelMesh*> m_Meshes;
                        };
                    }
                }
            }
        }
    }
}

