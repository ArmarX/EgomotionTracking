/*
 * OpenInventorModelMesh.h
 */

#pragma once

#include "../../Base/ModelMesh.h"
#include "OpenInventorModelPrimitive.h"
#include "OpenInventorModelVertex.h"
#include "OpenInventorModelEdge.h"
#include "OpenInventorModelFace.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Visualization
                {
                    namespace OpenInventor
                    {
                        class COpenInventorModelMultipleMesh;

                        class COpenInventorModelMesh : public COpenInventorModelPrimitive
                        {
                        public:

                            COpenInventorModelMesh(Base::CModelMesh* pModelMesh);
                            ~COpenInventorModelMesh() override;

                            const Base::CModelMesh* GetModelMesh() const;
                            Base::CModelMesh* GetModelMesh();

                            //BoundingBox
                            bool GetBoundingBoxVisible() const;
                            void SetBoundingBoxVisible(const bool Visible);

                            Real GetBoundingOffset() const;
                            void SetBoundingOffset(const Real Offset);

                            void GetBoundingBoxLineMaterial(Byte& R, Byte& G, Byte& B, Byte& A) const;
                            void SetBoundingBoxLineMaterial(const Byte R, const Byte G, const Byte B, const Byte A);

                            Real GetBoundingBoxLineWidth() const;
                            void SetBoundingBoxLineWidth(const Real Width);

                            ushort GetBoundingBoxLinePattern() const;
                            void SetBoundingBoxLinePattern(const ushort Pattern);

                            //Primitives

                            //Multiple Faces
                            bool GetMultipleFacesSetVisible(const Base::CModelElement::Visibility Type) const;
                            void SetMultipleFacesSetVisible(const Base::CModelElement::Visibility Type, const bool Visible);

                            void GetMultipleFacesSetMaterial(const Base::CModelElement::Visibility Type, Byte& R, Byte& G, Byte& B, Byte& A) const;
                            void SetMultipleFacesSetMaterial(const Base::CModelElement::Visibility Type, const Byte R, const Byte G, const Byte B, const Byte A);

                            //Faces
                            bool GetFaceSetVisible(const Base::CModelElement::Visibility Type) const;
                            void SetFaceSetVisible(const Base::CModelElement::Visibility Type, const bool Visible);

                            void GetFacesSetMaterial(const Base::CModelElement::Visibility Type, Byte& R, Byte& G, Byte& B, Byte& A) const;
                            void SetFacesSetMaterial(const Base::CModelElement::Visibility Type, const Byte R, const Byte G, const Byte B, const Byte A);
                            void SetFacesSetMaterialFromMeshMaterial(const Base::CModelElement::Visibility Type);

                            //Edges
                            bool GetEdgesSetVisible(const Base::CModelElement::Visibility Type) const;
                            void SetEdgesSetVisible(const Base::CModelElement::Visibility Type, const bool Visible);

                            void GetEdgesSetMaterial(const Base::CModelElement::Visibility Type, Byte& R, Byte& G, Byte& B, Byte& A) const;
                            void SetEdgesSetMaterial(const Base::CModelElement::Visibility Type, const Byte R, const Byte G, const Byte B, const Byte A);
                            void SetEdgesSetMaterialFromMeshMaterial(const Base::CModelElement::Visibility Type);

                            //Vertices
                            bool GetVerticesSetVisible(const Base::CModelElement::Visibility Type) const;
                            void SetVerticesSetVisible(const Base::CModelElement::Visibility Type, const bool Visible);

                            void GetVerticesSetMaterial(const Base::CModelElement::Visibility Type, Byte& R, Byte& G, Byte& B, Byte& A) const;
                            void SetVerticesSetMaterial(const Base::CModelElement::Visibility Type, const Byte R, const Byte G, const Byte B, const Byte A);
                            void SetVerticesSetMaterialFromMeshMaterial(const Base::CModelElement::Visibility Type);

                            //Manipulable Objects
                            void CreateManipulableObjects();
                            SoSwitch* GetManipulableObjectsSwitch();
                            void ClearManipulableObjects();
                            void UpdateManipulableObjects();

                        protected:

                            friend class COpenInventorModelMultipleMesh;

                            void Create() override;
                            void FullUpdate() override;
                            bool IsComposedVisible() override;

                            void UpdateBoundingBox();
                            void UpdateCoordinates();
                            void UpdateFaces();
                            void UpdateEdges();
                            void UpdateMaterial();

                        private:

                            Real m_BoundingBoxOffset;

                            SoSwitch* m_pContentSwitch;

                            /// Bounding

                            SoSwitch* m_pBoundingBoxSwitch;
                            SoCoordinate3* m_pBoundingBoxCoordinates;
                            SoDrawStyle* m_pCubeDrawStyle;
                            SoMaterial* m_pBoundingBooxMaterial;
                            SoIndexedLineSet* m_pIndexedLineSetCube;

                            /// Common

                            SoCoordinate3* m_pMeshCoordinates;
                            SoNormal* m_pNormals;
                            SoSwitch* m_pPrimitiveSetSwitch;

                            SoSwitch* m_pObjectsSetSwitch;
                            SoSwitch* m_pObjectsVerticesSetSwitch;
                            SoSwitch* m_pObjectsEdgesSetSwitch;
                            SoSwitch* m_pObjectsFacesSetSwitch;
                            SoSwitch* m_pObjectsMultipleFacesSetSwitch;

                            /// Face Set
                            //Visible Face Set
                            SoSwitch* m_pVisibleIndexedFaceSetSwitch;
                            SoDrawStyle* m_pVisibleIndexedFaceSetDrawStyle;
                            SoMaterial* m_pVisibleIndexedFaceSetMaterial;
                            SoIndexedFaceSet* m_pVisibleIndexedFaceSet;

                            //Non Visible Face Set
                            SoSwitch* m_pNonVisibleIndexedFaceSetSwitch;
                            SoDrawStyle* m_pNonVisibleIndexedFaceSetDrawStyle;
                            SoMaterial* m_pNonVisibleIndexedFaceSetMaterial;
                            SoIndexedFaceSet* m_pNonVisibleIndexedFaceSet;

                            //Dynamic Visible Face Set
                            SoSwitch* m_pDynamicVisibleIndexedFaceSetSwitch;
                            SoDrawStyle* m_pDynamicVisibleIndexedFaceSetDrawStyle;
                            SoMaterial* m_pDynamicVisibleIndexedFaceSetMaterial;
                            SoIndexedFaceSet* m_pDynamicVisibleIndexedFaceSet;

                            /// Edge Set
                            //Visible Edge Set
                            SoSwitch* m_pVisibleIndexedLineSetSwitch;
                            SoDrawStyle* m_pVisibleIndexedLineSetDrawStyle;
                            SoMaterial* m_pVisibleIndexedLineSetMaterial;
                            SoIndexedLineSet* m_pVisibleIndexedLineSet;

                            //Non Visible Edge Set
                            SoSwitch* m_pNonVisibleIndexedLineSetSwitch;
                            SoDrawStyle* m_pNonVisibleIndexedLineSetDrawStyle;
                            SoMaterial* m_pNonVisibleIndexedLineSetMaterial;
                            SoIndexedLineSet* m_pNonVisibleIndexedLineSet;

                            //Dynamic Visible Edge Set
                            SoSwitch* m_pDynamicVisibleIndexedLineSetSwitch;
                            SoDrawStyle* m_pDynamicVisibleIndexedLineSetDrawStyle;
                            SoMaterial* m_pDynamicVisibleIndexedLineSetMaterial;
                            SoIndexedLineSet* m_pDynamicVisibleIndexedLineSet;


                            struct VertexSetVisualization
                            {
                                VertexSetVisualization():
                                    m_pSwitch(NULL),
                                    m_pCoordinates(NULL),
                                    m_pDrawStyle(NULL),
                                    m_pMaterial(NULL),
                                    m_pPointSet(NULL)
                                {
                                }

                                void Create(SoSwitch* pParent, void* pData, const float PointSize, const float R, const float G, const float B)
                                {
                                    m_pSwitch = new SoSwitch;
                                    pParent->addChild(m_pSwitch);

                                    m_pCoordinates = new SoCoordinate3;
                                    m_pSwitch->addChild(m_pCoordinates);

                                    m_pDrawStyle = new SoDrawStyle;
                                    m_pSwitch->addChild(m_pDrawStyle);
                                    m_pDrawStyle->pointSize.setValue(PointSize);

                                    m_pMaterial = new SoMaterial;
                                    m_pSwitch->addChild(m_pMaterial);
                                    m_pMaterial->diffuseColor.setValue(R, G, B);

                                    m_pPointSet = new SoPointSet;
                                    m_pSwitch->addChild(m_pPointSet);
                                    m_pPointSet->setUserData(pData);

                                    m_pSwitch->whichChild = SO_SWITCH_ALL;
                                }

                                void SetDisplaying(const bool Displaying)
                                {
                                    m_pSwitch->whichChild = Displaying ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                                }

                                bool IsDisplaying() const
                                {
                                    return m_pSwitch->whichChild.getValue() == SO_SWITCH_ALL;
                                }

                                SbColor GetColor() const
                                {
                                    return m_pMaterial->diffuseColor[0];
                                }

                                float GetAlpha() const
                                {
                                    return m_pMaterial->transparency[0];
                                }

                                void SetMaterial(const Byte R, const Byte G, const Byte B, const Byte A)
                                {
                                    m_pMaterial->diffuseColor.setValue(float(R) / 255.0f, float(G) / 255.0f, float(B) / 255.0f);
                                    m_pMaterial->transparency = float(A) / 255.0f;
                                }

                                void SetMaterial(SoMaterial* pMaterial)
                                {
                                    m_pMaterial->diffuseColor = pMaterial->diffuseColor[0];
                                    m_pMaterial->transparency = pMaterial->transparency[0];
                                }

                                SoSwitch* m_pSwitch;
                                SoCoordinate3* m_pCoordinates;
                                SoDrawStyle* m_pDrawStyle;
                                SoMaterial* m_pMaterial;
                                SoPointSet* m_pPointSet;
                            };

                            /// Vertices Set

                            VertexSetVisualization m_VisibleVertexSetVisualization;
                            VertexSetVisualization m_NonVisibleVertexSetVisualization;
                            VertexSetVisualization m_DynamicVisibleVertexSetVisualization;

                            std::deque<COpenInventorModelVertex*> m_OpenInventorModelVertices;
                            std::deque<COpenInventorModelEdge*> m_OpenInventorModelEdges;
                            std::deque<COpenInventorModelFace*> m_OpenInventorModelFaces;
                        };
                    }
                }
            }
        }
    }
}

