/*
 * OpenInventorModelFace.h
 */

#pragma once

#include "../../Base/ModelFace.h"
#include "OpenInventorModelPrimitive.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Visualization
                {
                    namespace OpenInventor
                    {
                        class COpenInventorModelFace : public COpenInventorModelPrimitive
                        {
                        public:

                            COpenInventorModelFace(Base::CModelFace* pModelFace);
                            ~COpenInventorModelFace() override;

                            const Base::CModelFace* GetModelFace() const;
                            Base::CModelFace* GetModelFace();

                        protected:

                            void Create() override;
                            void FullUpdate() override;
                            bool IsComposedVisible() override;

                        private:

                            SoSwitch* m_pVisualizationModeSoSwitch;
                            SoCoordinate3* m_pCoordinate3;
                            SoFaceSet* m_pSoFaceSet;
                        };
                    }
                }
            }
        }
    }
}

