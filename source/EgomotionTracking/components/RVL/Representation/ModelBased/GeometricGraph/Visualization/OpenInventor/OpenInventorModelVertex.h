/*
 * OpenInventorModelVertex.h
 */

#pragma once

#include "../../Base/ModelVertex.h"
#include "OpenInventorModelPrimitive.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Visualization
                {
                    namespace OpenInventor
                    {
                        class COpenInventorModelVertex : public COpenInventorModelPrimitive
                        {
                        public:

                            COpenInventorModelVertex(Base::CModelVertex* pModelVertex);
                            ~COpenInventorModelVertex() override;

                            const Base::CModelVertex* GetModelVertex() const;
                            Base::CModelVertex* GetModelVertex();

                            bool SetDisplayRadius(const float Radius);

                        protected:

                            void Create() override;
                            void FullUpdate() override;
                            bool IsComposedVisible() override;

                        private:

                            SoSwitch* m_pVisualizationModeSoSwitch;
                            SoTranslation* m_pSoTranslation;
                            SoCube* m_pSelectedSoCube;
                            SoSphere* m_pEditionSoSphere;
                        };
                    }
                }
            }
        }
    }
}

