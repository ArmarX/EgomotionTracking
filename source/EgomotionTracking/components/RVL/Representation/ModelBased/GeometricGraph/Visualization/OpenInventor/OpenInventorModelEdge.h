/*
 * OpenInventorModelEdge.h
 */

#pragma once

#include "../../Base/ModelEdge.h"
#include "OpenInventorModelPrimitive.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Visualization
                {
                    namespace OpenInventor
                    {
                        class COpenInventorModelEdge : public COpenInventorModelPrimitive
                        {
                        public:

                            COpenInventorModelEdge(Base::CModelEdge* pModelEdge);
                            ~COpenInventorModelEdge() override;

                            const Base::CModelEdge* GetModelEdge() const;
                            Base::CModelEdge* GetModelEdge();

                        protected:

                            void Create() override;
                            void FullUpdate() override;
                            bool IsComposedVisible() override;

                        private:

                            SoSwitch* m_pVisualizationModeSoSwitch;
                            SoCoordinate3* m_pSoCoordinate3;
                            SoLineSet* m_pSoLineSet;
                            SoTransform* m_pSoTransform;
                            SoCylinder* m_pSelectedCylinder;
                        };
                    }
                }
            }
        }
    }
}

