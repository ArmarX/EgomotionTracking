/*
 * OpenInventorModelPrimitive.cpp
 */

#include "OpenInventorModelPrimitive.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Visualization
                {
                    namespace OpenInventor
                    {
                        Real COpenInventorModelPrimitive::s_MaterialStatePrimitiveTable[5][3][4];
                        bool COpenInventorModelPrimitive::s_LoadTable = true;

                        COpenInventorModelPrimitive::COpenInventorModelPrimitive(Base::CModelElement* pModelElement, const bool CreateDrawStyle, const bool CreateMaterial, const bool CreateComplexity) :
                            CContainedOpenInventorElement(COpenInventorElement::eModelElement, CreateDrawStyle, CreateMaterial, CreateComplexity),
                            Dependency::IDependency(),
                            m_VisualizationMode(eDefault),
                            m_pModelElement(pModelElement)
                        {
                            m_pModelElement->AddDependency(this);
                            if (s_LoadTable)
                            {
                                LoadMaterialStatePrimitiveTable();
                            }

                            SetComplexityType(SoComplexity::OBJECT_SPACE);
                            SetComplexityValue(0.15f);
                        }

                        COpenInventorModelPrimitive::~COpenInventorModelPrimitive()
                        {
                            m_pModelElement->RemoveDependency(this);
                        }

                        void COpenInventorModelPrimitive::SetVisualizationMode(const VisualizationMode Mode)
                        {
                            if (Mode != m_VisualizationMode)
                            {
                                m_VisualizationMode = Mode;
                                Update(true);
                            }
                        }

                        COpenInventorModelPrimitive::VisualizationMode COpenInventorModelPrimitive::GetVisualizationMode() const
                        {
                            return m_VisualizationMode;
                        }

                        const Base::CModelElement* COpenInventorModelPrimitive::GetModelElement() const
                        {
                            return m_pModelElement;
                        }

                        Base::CModelElement* COpenInventorModelPrimitive::GetModelElement()
                        {
                            return m_pModelElement;
                        }

                        void COpenInventorModelPrimitive::OnDestructor(Dependency::IDependencyManager* pDependencyManager)
                        {
                            m_pModelElement = nullptr;
                            Update(true);
                        }

                        void COpenInventorModelPrimitive::OnChange(Dependency::IDependencyManager* pDependencyManager, const int EventFlags)
                        {
                            Update(true);
                        }

                        bool COpenInventorModelPrimitive::IsComposedVisible()
                        {
                            if (GetVisible())
                                if (m_pModelElement)
                                    switch (m_pModelElement->GetVisibility(true))
                                    {
                                        case Base::CModelElement::eStaticOccluded:
                                        case Base::CModelElement::eStaticVisible:
                                        case Base::CModelElement::eDynamicVisible:
                                            return true;
                                    }
                            return false;
                        }

                        void COpenInventorModelPrimitive::Update(const bool OnFullUpdate)
                        {
                            if (OnFullUpdate)
                            {
                                FullUpdate();
                            }
                            SwitchVisibility(IsComposedVisible());
                        }

                        void COpenInventorModelPrimitive::LoadMaterialStatePrimitiveTable()
                        {
                            /// Vertex
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eVertex][Base::CModelElement::eStaticOccluded][0] = Real(1);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eVertex][Base::CModelElement::eStaticOccluded][1] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eVertex][Base::CModelElement::eStaticOccluded][2] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eVertex][Base::CModelElement::eStaticOccluded][3] = Real(0.25);

                            s_MaterialStatePrimitiveTable[Base::CModelElement::eVertex][Base::CModelElement::eStaticVisible][0] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eVertex][Base::CModelElement::eStaticVisible][1] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eVertex][Base::CModelElement::eStaticVisible][2] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eVertex][Base::CModelElement::eStaticVisible][3] = Real(0.75);

                            s_MaterialStatePrimitiveTable[Base::CModelElement::eVertex][Base::CModelElement::eDynamicVisible][0] = Real(1);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eVertex][Base::CModelElement::eDynamicVisible][1] = Real(0.75);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eVertex][Base::CModelElement::eDynamicVisible][2] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eVertex][Base::CModelElement::eDynamicVisible][3] = Real(0.75);

                            /// Oriented Vertex
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eOrientedVertex][Base::CModelElement::eStaticOccluded][0] = Real(1);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eOrientedVertex][Base::CModelElement::eStaticOccluded][1] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eOrientedVertex][Base::CModelElement::eStaticOccluded][2] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eOrientedVertex][Base::CModelElement::eStaticOccluded][3] = Real(0.25);

                            s_MaterialStatePrimitiveTable[Base::CModelElement::eOrientedVertex][Base::CModelElement::eStaticVisible][0] = Real(0.5);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eOrientedVertex][Base::CModelElement::eStaticVisible][1] = Real(0.5);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eOrientedVertex][Base::CModelElement::eStaticVisible][2] = Real(0.5);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eOrientedVertex][Base::CModelElement::eStaticVisible][3] = Real(0.75);

                            s_MaterialStatePrimitiveTable[Base::CModelElement::eOrientedVertex][Base::CModelElement::eDynamicVisible][0] = Real(1);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eOrientedVertex][Base::CModelElement::eDynamicVisible][1] = Real(0.75);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eOrientedVertex][Base::CModelElement::eDynamicVisible][2] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eOrientedVertex][Base::CModelElement::eDynamicVisible][3] = Real(0.75);

                            /// Edge
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eEdge][Base::CModelElement::eStaticOccluded][0] = Real(1);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eEdge][Base::CModelElement::eStaticOccluded][1] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eEdge][Base::CModelElement::eStaticOccluded][2] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eEdge][Base::CModelElement::eStaticOccluded][3] = Real(0.25);

                            s_MaterialStatePrimitiveTable[Base::CModelElement::eEdge][Base::CModelElement::eStaticVisible][0] = Real(1);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eEdge][Base::CModelElement::eStaticVisible][1] = Real(1);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eEdge][Base::CModelElement::eStaticVisible][2] = Real(1);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eEdge][Base::CModelElement::eStaticVisible][3] = Real(0.75);

                            s_MaterialStatePrimitiveTable[Base::CModelElement::eEdge][Base::CModelElement::eDynamicVisible][0] = Real(1);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eEdge][Base::CModelElement::eDynamicVisible][1] = Real(0.75);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eEdge][Base::CModelElement::eDynamicVisible][2] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eEdge][Base::CModelElement::eDynamicVisible][3] = Real(0.75);

                            /// Face
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eFace][Base::CModelElement::eStaticOccluded][0] = Real(1);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eFace][Base::CModelElement::eStaticOccluded][1] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eFace][Base::CModelElement::eStaticOccluded][2] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eFace][Base::CModelElement::eStaticOccluded][3] = Real(0.25);

                            s_MaterialStatePrimitiveTable[Base::CModelElement::eFace][Base::CModelElement::eStaticVisible][0] = Real(0.5);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eFace][Base::CModelElement::eStaticVisible][1] = Real(0.5);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eFace][Base::CModelElement::eStaticVisible][2] = Real(0.5);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eFace][Base::CModelElement::eStaticVisible][3] = Real(0.75);

                            s_MaterialStatePrimitiveTable[Base::CModelElement::eFace][Base::CModelElement::eDynamicVisible][0] = Real(1);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eFace][Base::CModelElement::eDynamicVisible][1] = Real(0.75);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eFace][Base::CModelElement::eDynamicVisible][2] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eFace][Base::CModelElement::eDynamicVisible][3] = Real(0.75);

                            /// ComposedFace
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eComposedFace][Base::CModelElement::eStaticOccluded][0] = Real(1);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eComposedFace][Base::CModelElement::eStaticOccluded][1] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eComposedFace][Base::CModelElement::eStaticOccluded][2] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eComposedFace][Base::CModelElement::eStaticOccluded][3] = Real(0.25);

                            s_MaterialStatePrimitiveTable[Base::CModelElement::eComposedFace][Base::CModelElement::eStaticVisible][0] = Real(0.5);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eComposedFace][Base::CModelElement::eStaticVisible][1] = Real(0.5);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eComposedFace][Base::CModelElement::eStaticVisible][2] = Real(0.5);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eComposedFace][Base::CModelElement::eStaticVisible][3] = Real(0.75);

                            s_MaterialStatePrimitiveTable[Base::CModelElement::eComposedFace][Base::CModelElement::eDynamicVisible][0] = Real(1);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eComposedFace][Base::CModelElement::eDynamicVisible][1] = Real(0.75);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eComposedFace][Base::CModelElement::eDynamicVisible][2] = Real(0);
                            s_MaterialStatePrimitiveTable[Base::CModelElement::eComposedFace][Base::CModelElement::eDynamicVisible][3] = Real(0.75);

                            s_LoadTable = false;
                        }
                    }
                }
            }
        }
    }
}
