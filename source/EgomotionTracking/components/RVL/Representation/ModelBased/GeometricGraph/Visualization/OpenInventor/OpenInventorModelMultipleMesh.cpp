/*
 * OpenInventorModelMultipleMesh.cpp
 */

#include "OpenInventorModelMultipleMesh.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Visualization
                {
                    namespace OpenInventor
                    {
                        COpenInventorModelMultipleMesh::COpenInventorModelMultipleMesh(Base::CModelMultipleMesh* pModelMultipleMesh, const bool LoadMeshes) :
                            COpenInventorModelPrimitive(pModelMultipleMesh, false, false, false),
                            m_BoundingBoxOffset(Real(2.0)),
                            m_pBoundingBoxSwitch(nullptr),
                            m_pBoundingBoxCoordinates(nullptr),
                            m_pCubeDrawStyle(nullptr),
                            m_pBoundingBooxMaterial(nullptr),
                            m_pIndexedLineSetCube(nullptr),
                            m_pMeshesSwitch(nullptr)
                        {
                            Create();
                            if (LoadMeshes)
                            {
                                CreateMeshes();
                            }
                            Base::CModelElement* pModelElement = COpenInventorModelPrimitive::GetModelElement();
                            pModelElement->SetElementData(this);
                            FullUpdate();
                        }

                        COpenInventorModelMultipleMesh::~COpenInventorModelMultipleMesh()
                        {
                            DestroyMeshes();
                            Base::CModelElement* pModelElement = COpenInventorModelPrimitive::GetModelElement();
                            pModelElement->ClearElementData();
                        }

                        const Base::CModelMultipleMesh* COpenInventorModelMultipleMesh::GetMultipleMesh() const
                        {
                            return dynamic_cast<const Base::CModelMultipleMesh*>(GetModelElement());
                        }

                        Base::CModelMultipleMesh* COpenInventorModelMultipleMesh::GetMultipleMesh()
                        {
                            return dynamic_cast<Base::CModelMultipleMesh*>(GetModelElement());
                        }

                        bool COpenInventorModelMultipleMesh::GetBoundingBoxVisible() const
                        {
                            return m_pBoundingBoxSwitch->whichChild.getValue() == SO_SWITCH_ALL;
                        }

                        void COpenInventorModelMultipleMesh::SetBoundingBoxVisible(const bool Visible)
                        {
                            m_pBoundingBoxSwitch->whichChild = Visible ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                        }

                        Real COpenInventorModelMultipleMesh::GetBoundingOffset() const
                        {
                            return m_BoundingBoxOffset;
                        }

                        void COpenInventorModelMultipleMesh::SetBoundingOffset(const Real Offset)
                        {
                            if (NonEquals(Offset, m_BoundingBoxOffset))
                            {
                                m_BoundingBoxOffset = Offset;
                                UpdateBoundingBox();
                            }
                        }

                        void COpenInventorModelMultipleMesh::GetBoundingBoxLineMaterial(Byte& R, Byte& G, Byte& B, Byte& A) const
                        {
                            SbColor CurrentColor = m_pBoundingBooxMaterial->emissiveColor[0];
                            R = Byte(round(CurrentColor[0] * 255.0f));
                            G = Byte(round(CurrentColor[1] * 255.0f));
                            B = Byte(round(CurrentColor[2] * 255.0f));
                            A = Byte(round((1.0f - m_pBoundingBooxMaterial->transparency[0]) * 255.0f));
                        }

                        void COpenInventorModelMultipleMesh::SetBoundingBoxLineMaterial(const Byte R, const Byte G, const Byte B, const Byte A)
                        {
                            m_pBoundingBooxMaterial->emissiveColor.setValue(float(R) / 255.0f, float(G) / 255.0f, float(B) / 255.0f);
                            m_pBoundingBooxMaterial->transparency.setValue(1.0f - (float(A) / 255.0f));
                        }

                        Real COpenInventorModelMultipleMesh::GetBoundingBoxLineWidth() const
                        {
                            return m_pCubeDrawStyle->lineWidth.getValue();
                        }

                        void COpenInventorModelMultipleMesh::SetBoundingBoxLineWidth(const Real Width)
                        {
                            m_pCubeDrawStyle->lineWidth.setValue(float(Width));
                        }

                        ushort COpenInventorModelMultipleMesh::GetBoundingBoxLinePattern() const
                        {
                            return  m_pCubeDrawStyle->linePattern.getValue();
                        }

                        void COpenInventorModelMultipleMesh::SetBoundingBoxLinePattern(const ushort Pattern)
                        {
                            m_pCubeDrawStyle->linePattern.setValue(Pattern);
                        }

                        const std::list<COpenInventorModelMesh*>& COpenInventorModelMultipleMesh::GetMeshes() const
                        {
                            return m_Meshes;
                        }

                        bool COpenInventorModelMultipleMesh::DeleteMeshVisualization(COpenInventorModelMesh*& pOpenInventorModelMesh)
                        {
                            if (!pOpenInventorModelMesh)
                            {
                                return false;
                            }

                            for (std::list<COpenInventorModelMesh*>::iterator ppMesh = m_Meshes.begin() ; ppMesh != m_Meshes.end() ; ++ppMesh)
                                if (*ppMesh == pOpenInventorModelMesh)
                                {
                                    m_Meshes.erase(ppMesh);
                                    m_pMeshesSwitch->removeChild(pOpenInventorModelMesh->GetBaseSwitch());
                                    delete pOpenInventorModelMesh;
                                    pOpenInventorModelMesh = nullptr;
                                    return true;
                                }

                            return false;
                        }

                        void COpenInventorModelMultipleMesh::Create()
                        {
                            m_pMeshesSwitch = new SoSwitch;
                            AddElement(m_pMeshesSwitch);
                            m_pMeshesSwitch->whichChild = SO_SWITCH_NONE;

                            m_pBoundingBoxSwitch = new SoSwitch;
                            AddElement(m_pBoundingBoxSwitch);
                            m_pBoundingBoxSwitch->whichChild = SO_SWITCH_NONE;
                            m_pBoundingBoxCoordinates = new SoCoordinate3;
                            m_pBoundingBoxSwitch->addChild(m_pBoundingBoxCoordinates);
                            m_pCubeDrawStyle = new SoDrawStyle;
                            m_pBoundingBoxSwitch->addChild(m_pCubeDrawStyle);
                            m_pCubeDrawStyle->lineWidth.setValue(1.0f);
                            m_pCubeDrawStyle->linePattern.setValue(0X3FFF);
                            m_pBoundingBooxMaterial = new SoMaterial;
                            m_pBoundingBoxSwitch->addChild(m_pBoundingBooxMaterial);
                            m_pBoundingBooxMaterial->emissiveColor.setValue(1.0f, 1.0f, 1.0f);
                            //m_pBoundingBooxMaterial->diffuseColor.setValue(1.0f,1.0f,1.0f);
                            m_pIndexedLineSetCube = new SoIndexedLineSet;
                            m_pBoundingBoxSwitch->addChild(m_pIndexedLineSetCube);
                            m_pIndexedLineSetCube->setUserData(this);

                        }

                        void COpenInventorModelMultipleMesh::FullUpdate()
                        {
                            for (auto& m_Meshe : m_Meshes)
                            {
                                m_Meshe->FullUpdate();
                            }
                            UpdateBoundingBox();
                        }

                        bool COpenInventorModelMultipleMesh::IsComposedVisible()
                        {
                            if (COpenInventorModelPrimitive::IsComposedVisible())
                            {
                                const RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::CContainerOpenInventorSet* pContainerSet = COpenInventorModelPrimitive::GetContainerSet();
                                if (pContainerSet)
                                {
                                    for (auto& m_Meshe : m_Meshes)
                                    {
                                        Containers::_3D::CContinuousBoundingBox3D BoundingBox = m_Meshe->GetModelMesh()->GetBoundingBox();
                                        switch (pContainerSet->MapBoundingBox(SbBox3f(BoundingBox.GetX0(), BoundingBox.GetY0(), BoundingBox.GetZ0(), BoundingBox.GetX1(), BoundingBox.GetY1(), BoundingBox.GetZ1())))
                                        {
                                            case RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::eEmptyInclusion:
                                            case RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::eImproperInclusion:
                                                continue;
                                                break;
                                            case RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::ePartialInclusion:
                                            case RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::eFullInclusion:
                                                return true;
                                        }
                                    }
                                    return false;
                                }
                                return true;
                            }
                            return false;
                        }

                        void COpenInventorModelMultipleMesh::UpdateBoundingBox()
                        {
                            const Containers::_3D::CContinuousBoundingBox3D BoundingBox = GetMultipleMesh()->GetBoundingBox();
                            Real X0 = Real(0.0), Y0 = Real(0.0), Z0 = Real(0.0), X1 = Real(0.0), Y1 = Real(0.0), Z1 = Real(0.0);
                            BoundingBox.GetBounds(X0, Y0, Z0, X1, Y1, Z1);
                            if (IsNonZero(m_BoundingBoxOffset))
                            {
                                X0 -= m_BoundingBoxOffset;
                                Y0 -= m_BoundingBoxOffset;
                                Z0 -= m_BoundingBoxOffset;
                                X1 += m_BoundingBoxOffset;
                                Y1 += m_BoundingBoxOffset;
                                Z1 += m_BoundingBoxOffset;
                            }
                            const float Vertices[24] = { float(X0), float(Y0), float(Z0), float(X1), float(Y0), float(Z0), float(X1), float(Y1), float(Z0), float(X0), float(Y1), float(Z0), float(X0), float(Y0), float(Z1), float(X1), float(Y0), float(Z1), float(X1), float(Y1), float(Z1), float(X0), float(Y1), float(Z1) };
                            m_pBoundingBoxCoordinates->point.setValues(0, 8, (const float (*)[3]) Vertices);
                            const int Indices[20] = { 0, 1, 2, 3, 0, 4, 5, 6, 7, 4, -1, 1, 5, -1, 2, 6, -1, 3, 7, -1 };
                            m_pIndexedLineSetCube->coordIndex.setValues(0, 20, Indices);
                        }

                        bool COpenInventorModelMultipleMesh::CreateMeshes()
                        {
                            const Base::CModelMultipleMesh* pModelMultipleMesh = dynamic_cast<const Base::CModelMultipleMesh*>(GetModelElement());
                            if (pModelMultipleMesh)
                            {
                                const std::map<int, Base::CModelMesh*>& Meshes = pModelMultipleMesh->GetMeshes();
                                std::map<int, Base::CModelMesh*>::const_iterator MeshesEnd = Meshes.end();
                                for (std::map<int, Base::CModelMesh*>::const_iterator pKeyModelMeshes = Meshes.begin() ; pKeyModelMeshes != MeshesEnd ; ++pKeyModelMeshes)
                                {
                                    COpenInventorModelMesh* pOpenInventorModelMesh = new COpenInventorModelMesh(pKeyModelMeshes->second);
                                    m_Meshes.push_back(pOpenInventorModelMesh);
                                    m_pMeshesSwitch->addChild(pOpenInventorModelMesh->GetBaseSwitch());
                                }
                            }
                            m_pMeshesSwitch->whichChild = m_pMeshesSwitch->getNumChildren() ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                            return m_Meshes.size();
                        }

                        void COpenInventorModelMultipleMesh::DestroyMeshes()
                        {
                            m_pMeshesSwitch->removeAllChildren();
                            for (auto& m_Meshe : m_Meshes)
                            {
                                delete m_Meshe;
                            }
                            m_Meshes.clear();
                        }
                    }
                }
            }
        }
    }
}
