/*
 * OpenInventorModelVertex.cpp
 */

#include "OpenInventorModelVertex.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Visualization
                {
                    namespace OpenInventor
                    {
                        COpenInventorModelVertex::COpenInventorModelVertex(Base::CModelVertex* pModelVertex) :
                            COpenInventorModelPrimitive(pModelVertex, true, true, true),
                            m_pVisualizationModeSoSwitch(nullptr),
                            m_pSoTranslation(nullptr),
                            m_pSelectedSoCube(nullptr),
                            m_pEditionSoSphere(nullptr)
                        {
                            Create();
                            Base::CModelElement* pModelElement = COpenInventorModelPrimitive::GetModelElement();
                            pModelElement->SetElementData(this);
                        }

                        COpenInventorModelVertex::~COpenInventorModelVertex()
                        {
                            Base::CModelElement* pModelElement = COpenInventorModelPrimitive::GetModelElement();
                            pModelElement->ClearElementData();
                        }

                        const Base::CModelVertex* COpenInventorModelVertex::GetModelVertex() const
                        {
                            return dynamic_cast<const Base::CModelVertex*>(GetModelElement());
                        }

                        Base::CModelVertex* COpenInventorModelVertex::GetModelVertex()
                        {
                            return dynamic_cast<Base::CModelVertex*>(GetModelElement());
                        }

                        bool COpenInventorModelVertex::SetDisplayRadius(const float Radius)
                        {
                            if ((Radius >= _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_) && (Radius <= _VISUALIZATION_POINT_MAXIMAL_DISPLAY_RADIUS_))
                            {
                                m_pSelectedSoCube->width = Radius;
                                m_pSelectedSoCube->height = Radius;
                                m_pSelectedSoCube->depth = Radius;
                                m_pEditionSoSphere->radius = Radius;
                                return true;
                            }
                            return false;
                        }

                        void COpenInventorModelVertex::Create()
                        {
                            m_pSoTranslation = new SoTranslation;
                            AddElement(m_pSoTranslation);

                            m_pVisualizationModeSoSwitch = new SoSwitch;
                            AddElement(m_pVisualizationModeSoSwitch);

                            m_pSelectedSoCube = new SoCube;
                            m_pVisualizationModeSoSwitch->addChild(m_pSelectedSoCube);
                            m_pSelectedSoCube->setUserData(this);

                            m_pEditionSoSphere = new SoSphere;
                            m_pVisualizationModeSoSwitch->addChild(m_pEditionSoSphere);
                            m_pEditionSoSphere->setUserData(this);

                            m_pSelectedSoCube->width = _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_;
                            m_pSelectedSoCube->height = _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_;
                            m_pSelectedSoCube->depth = _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_;
                            m_pEditionSoSphere->radius = _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_;

                            m_pVisualizationModeSoSwitch->whichChild = 0;

                        }

                        void COpenInventorModelVertex::FullUpdate()
                        {
                            const Base::CModelVertex* pModelVertex = dynamic_cast<const Base::CModelVertex*>(COpenInventorModelPrimitive::GetModelElement());
                            if (pModelVertex)
                            {
                                const Mathematics::_3D::CVector3D VertexPoint = pModelVertex->GetPoint(true);
                                m_pSoTranslation->translation.setValue(float(VertexPoint.GetX()), float(VertexPoint.GetY()), float(VertexPoint.GetZ()));
                                switch (GetVisualizationMode())
                                {
                                    case COpenInventorModelPrimitive::eDefault:
                                        m_pVisualizationModeSoSwitch->whichChild = SO_SWITCH_NONE;
                                        break;
                                    case COpenInventorModelPrimitive::eSelected:
                                        m_pVisualizationModeSoSwitch->whichChild = 1;
                                        break;
                                    case COpenInventorModelPrimitive::eEdition:
                                        m_pVisualizationModeSoSwitch->whichChild = 1;
                                        break;
                                    case COpenInventorModelPrimitive::eFull:
                                        m_pVisualizationModeSoSwitch->whichChild = 1;
                                        break;
                                }

                                const Real* pMaterial = s_MaterialStatePrimitiveTable[pModelVertex->GetTypeId()][pModelVertex->GetVisibility(false)];
                                SetColor(float(pMaterial[0]), float(pMaterial[1]), float(pMaterial[2]));
                                SetTransparency(1.0f - float(pMaterial[3]));
                            }
                        }

                        bool COpenInventorModelVertex::IsComposedVisible()
                        {
                            if (COpenInventorModelPrimitive::IsComposedVisible())
                            {
                                const Base::CModelVertex* pModelVertex = dynamic_cast<const Base::CModelVertex*>(COpenInventorModelPrimitive::GetModelElement());
                                if (pModelVertex)
                                {
                                    const RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::CContainerOpenInventorSet* pContainerSet = COpenInventorModelPrimitive::GetContainerSet();
                                    if (pContainerSet)
                                    {
                                        const Mathematics::_3D::CVector3D& VertexPoint = pModelVertex->GetPoint(true);
                                        const SbVec3f VisualizationPoint(float(VertexPoint.GetX()), float(VertexPoint.GetY()), float(VertexPoint.GetZ()));
                                        switch (pContainerSet->MapPoint(VisualizationPoint))
                                        {
                                            case RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::eEmptyInclusion:
                                            case RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::eImproperInclusion:
                                                return false;
                                            case RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::ePartialInclusion:
                                            case RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::COpenInventorElement::eFullInclusion:
                                                return true;
                                        }
                                    }
                                    return true;
                                }
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}
