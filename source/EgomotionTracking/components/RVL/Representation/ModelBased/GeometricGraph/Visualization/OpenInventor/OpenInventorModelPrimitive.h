/*
 * OpenInventorModelPrimitive.h
 */

#pragma once

#include "../../../../../Visualization/_3D/SceneGraph/OpenInventor/Base/ContainedOpenInventorElement.h"
#include "../../../../../Dependency/Dependency.h"
#include "../../../../../Dependency/DependencyManager.h"
#include "../../Base/ModelElement.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Visualization
                {
                    namespace OpenInventor
                    {
                        class COpenInventorModelPrimitive : public RVL::Visualization::_3D::SceneGraph::OpenInventor::Base::CContainedOpenInventorElement, public Dependency::IDependency
                        {
                        public:

                            enum VisualizationMode
                            {
                                eDefault, eSelected, eEdition, eFull
                            };

                            COpenInventorModelPrimitive(Base::CModelElement* pModelElement, const bool CreateDrawStyle, const bool CreateMaterial, const bool CreateComplexity);
                            ~COpenInventorModelPrimitive() override;

                            virtual void SetVisualizationMode(const VisualizationMode Mode);
                            VisualizationMode GetVisualizationMode() const;

                            const Base::CModelElement* GetModelElement() const;
                            Base::CModelElement* GetModelElement();

                            void OnDestructor(Dependency::IDependencyManager* pDependencyManager) override;
                            void OnChange(Dependency::IDependencyManager* pDependencyManager, const int EventFlags) override;

                            bool IsComposedVisible() override;
                            void Update(const bool OnFullUpdate) override;

                        protected:

                            virtual void Create() = 0;
                            virtual void FullUpdate() = 0;

                            VisualizationMode m_VisualizationMode;
                            Base::CModelElement* m_pModelElement;

                            static Real s_MaterialStatePrimitiveTable[5][3][4];

                            static bool s_LoadTable;
                            static void LoadMaterialStatePrimitiveTable();
                        };
                    }
                }
            }
        }
    }
}

