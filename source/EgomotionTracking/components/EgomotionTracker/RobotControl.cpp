#include "RobotControl.h"

#include "Image/ByteImage.h"

#include <VisionX/tools/ImageUtil.h>
#include <Image/ImageProcessor.h>

#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <Eigen/Core>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/TypeConversions.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/DebuggingPrimitives.h>

#include <iostream>

using namespace armarx;
using namespace visionx;

RobotControl::RobotControl(EgomotionTracker* egomotionTracker, ImageProviderInfo imageProviderInfo, RobotStateComponentInterfacePrx robotStateProxy,
                           KinematicUnitInterfacePrx kinematicUnitProxy, PlatformUnitObserverInterfacePrx platformUnitObserverProxy, bool imagesAreUndistorted,
                           KinematicUnitObserverInterfacePrx kinematicUnitObserverProxy)
{
    this->egomotionTracker = egomotionTracker;
    this->robotStateProxy = robotStateProxy;
    this->kinematicUnitProxy = kinematicUnitProxy;
    this->platformUnitObserverProxy = platformUnitObserverProxy;
    this->kinematicUnitObserverProxy = kinematicUnitObserverProxy;

    this->imagesAreUndistorted = imagesAreUndistorted;

    identifierVelocityX = new DataFieldIdentifierBase(egomotionTracker->platformUnitObserverProxyName, egomotionTracker->platformVelocityChannelName,
            egomotionTracker->platformVelocityXDataFieldName);
    identifierVelocityY = new DataFieldIdentifierBase(egomotionTracker->platformUnitObserverProxyName, egomotionTracker->platformVelocityChannelName,
            egomotionTracker->platformVelocityYDataFieldName);
    identifierVelocityRotation = new DataFieldIdentifierBase(egomotionTracker->platformUnitObserverProxyName, egomotionTracker->platformVelocityChannelName,
            egomotionTracker->platformVelocityRotationDataFieldName);
    identifierNeckRoll = new DataFieldIdentifierBase(egomotionTracker->kinematicUnitObserverProxyName, egomotionTracker->jointAnglesChannelName,
            egomotionTracker->neckRollNodeName);
    identifierNeckPitch = new DataFieldIdentifierBase(egomotionTracker->kinematicUnitObserverProxyName, egomotionTracker->jointAnglesChannelName,
            egomotionTracker->neckPitchNodeName);
    identifierNeckYaw = new DataFieldIdentifierBase(egomotionTracker->kinematicUnitObserverProxyName, egomotionTracker->jointAnglesChannelName,
            egomotionTracker->neckYawNodeName);

    neck1NodeProxy = robotStateProxy->getSynchronizedRobot()->getRobotNode(egomotionTracker->m_neck1NodeName);
    neck2NodeProxy = robotStateProxy->getSynchronizedRobot()->getRobotNode(egomotionTracker->m_neck2NodeName);
    neck3NodeProxy = robotStateProxy->getSynchronizedRobot()->getRobotNode(egomotionTracker->m_neck3NodeName);

    robotPtr = RemoteRobot::createLocalClone(robotStateProxy);
    headBaseNodePtr = robotPtr->getRobotNode(egomotionTracker->headBaseNodeName);
    leftCameraNodePtr = robotPtr->getRobotNode(egomotionTracker->leftCameraNodeName);
    rightCameraNodePtr = robotPtr->getRobotNode(egomotionTracker->rightCameraNodeName);
    neck3NodePtr = robotPtr->getRobotNode(egomotionTracker->m_neck3NodeName);

    PosePtr neck1LocalTransformPtr = PosePtr::dynamicCast(neck1NodeProxy->getLocalTransformation());
    neck1LocalTransform = TypeConversions::ConvertEigenToSbMatrix(neck1LocalTransformPtr->toEigen());
    PosePtr neck2LocalTransformPtr = PosePtr::dynamicCast(neck2NodeProxy->getLocalTransformation());
    neck2LocalTransform = TypeConversions::ConvertEigenToSbMatrix(neck2LocalTransformPtr->toEigen());
    PosePtr neck3LocalTransformPtr = PosePtr::dynamicCast(neck3NodeProxy->getLocalTransformation());
    neck3LocalTransform = TypeConversions::ConvertEigenToSbMatrix(neck3LocalTransformPtr->toEigen());

    VirtualRobot::RobotNodePtr neck1NodePtr = robotPtr->getRobotNode(egomotionTracker->m_neck1NodeName);
    VirtualRobot::RobotNodeRevolutePtr neck1NodeRevolutePtr = std::dynamic_pointer_cast<VirtualRobot::RobotNodeRevolute>(neck1NodePtr);
    neck1RotationAxis = TypeConversions::ConvertEigenToSbVector(neck1NodeRevolutePtr->getJointRotationAxisInJointCoordSystem());
    VirtualRobot::RobotNodePtr neck2NodePtr = robotPtr->getRobotNode(egomotionTracker->m_neck2NodeName);
    VirtualRobot::RobotNodeRevolutePtr neck2NodeRevolutePtr = std::dynamic_pointer_cast<VirtualRobot::RobotNodeRevolute>(neck2NodePtr);
    neck2RotationAxis = TypeConversions::ConvertEigenToSbVector(neck2NodeRevolutePtr->getJointRotationAxisInJointCoordSystem());
    VirtualRobot::RobotNodeRevolutePtr neck3NodeRevolutePtr = std::dynamic_pointer_cast<VirtualRobot::RobotNodeRevolute>(neck3NodePtr);
    neck3RotationAxis = TypeConversions::ConvertEigenToSbVector(neck3NodeRevolutePtr->getJointRotationAxisInJointCoordSystem());

    m_ppUndistortedImages = new CByteImage*[2];
    m_ppUndistortedImages[0] = visionx::tools::createByteImage(imageProviderInfo);
    m_ppUndistortedImages[1] = visionx::tools::createByteImage(imageProviderInfo);

    m_cameraImages[0].Set(m_ppUndistortedImages[0]->width, m_ppUndistortedImages[0]->height, m_ppUndistortedImages[0]->type, true);
    m_cameraImages[1].Set(m_ppUndistortedImages[1]->width, m_ppUndistortedImages[1]->height, m_ppUndistortedImages[1]->type, true);

    m_cameraImages[0].pixels = m_ppUndistortedImages[0]->pixels;
    m_cameraImages[1].pixels = m_ppUndistortedImages[1]->pixels;

    m_robotTimingProfiler.StartProfiling();

    /* Use the code below when testing this component on a still image, not on the robot or in a simulation environment. Set the neck angles to those with which the image was captured. */
    //    try
    //    {
    //        kinematicUnitProxy->request();
    //        NameValueMap targetMap;
    //        targetMap[neck1NodeProxy->getName()] = Conversions::DegreeToRadians(24);
    //        targetMap[neck2NodeProxy->getName()] = 0;
    //        targetMap[neck3NodeProxy->getName()] = 0;
    //        kinematicUnitProxy->setJointAngles(targetMap);
    //        kinematicUnitProxy->release();
    //    }
    //    catch(ResourceUnavailableException &e)
    //    {

    //    }
}

RobotControl::~RobotControl()
{
    if (m_ppUndistortedImages)
    {
        delete m_ppUndistortedImages[0];
        delete m_ppUndistortedImages[1];
        delete[] m_ppUndistortedImages;
    }
}

bool RobotControl::DispatchRobot()
{
    if (imagesAreUndistorted)
    {
        ::ImageProcessor::CopyImage(egomotionTracker->robotImages[0], m_ppUndistortedImages[0]);
        ::ImageProcessor::CopyImage(egomotionTracker->robotImages[1], m_ppUndistortedImages[1]);
    }
    else
    {
        m_stereoCalibrationGL.GetUndistortion().Undistort(egomotionTracker->robotImages, m_ppUndistortedImages);
    }

    /* When executing on the robot or in a simulation environment. */
    float velocityX = platformUnitObserverProxy->getDataField(identifierVelocityX)->getFloat();
    float velocityY = platformUnitObserverProxy->getDataField(identifierVelocityY)->getFloat();
    float velocityAlpha = platformUnitObserverProxy->getDataField(identifierVelocityRotation)->getFloat();
    m_robotControlState.SetPlatformVelocityX(velocityX);
    m_robotControlState.SetPlatformVelocityY(velocityY);
    m_robotControlState.SetPlatformVelocityAlpha(velocityAlpha);

    /* When executing using a still image. */
    //    m_robotControlState.SetPlatformVelocityX(0);
    //    m_robotControlState.SetPlatformVelocityY(0);
    //    m_robotControlState.SetPlatformVelocityAlpha(0);

    /* Get the elapsed time (in MS) since the last iteration. */
    const float deltaT = m_robotTimingProfiler.GetElapsedMS();
    m_robotTimingProfiler.StartProfiling();
    m_robotControlState.SetDeltaT(deltaT);

    /* Update current head configuration. */
    float neckRoll = kinematicUnitObserverProxy->getDataField(identifierNeckRoll)->getFloat();
    float neckPitch = kinematicUnitObserverProxy->getDataField(identifierNeckPitch)->getFloat();
    float neckYaw = kinematicUnitObserverProxy->getDataField(identifierNeckYaw)->getFloat();
    m_currentHeadConfiguration.SetNeckRoll(neckRoll);
    m_currentHeadConfiguration.SetNeckPitch(neckPitch);
    m_currentHeadConfiguration.SetNeckYaw(neckYaw);

    /* Update kinematic chain. */
    m_rootToHeadBase = TypeConversions::ConvertEigenToSbMatrix(headBaseNodePtr->getPoseInRootFrame());

    if (GetCameraId() == CRobotControlInterface::eLeftCamera)
    {
        m_neck3ToCamera = TypeConversions::ConvertEigenToSbMatrix(leftCameraNodePtr->getTransformationFrom(neck3NodePtr));
    }
    else
    {
        m_neck3ToCamera = TypeConversions::ConvertEigenToSbMatrix(rightCameraNodePtr->getTransformationFrom(neck3NodePtr));
    }

    return true;
}

bool RobotControl::LockRobotMovement()
{
    return true;
}

void RobotControl::ReleaseRobotMovementLock()
{
    return;
}

bool RobotControl::SetTargetRobotControlState(const CRobotControlState& robotControlState)
{
    return false;
}

bool RobotControl::SetTargetRobotHeadConfiguration(const CHeadConfiguration& targetHeadConfiguration)
{
    return false;
}

void RobotControl::GetNeckRPYBounds(float& neckRollMin, float& neckRollMax, float& neckPitchMin, float& neckPitchMax, float& neckYawMin, float& neckYawMax) const
{
    return;
}

CHeadConfiguration RobotControl::GetHeadConfiguration() const
{
    return m_currentHeadConfiguration;
}

bool RobotControl::CalculateCameraPosition(const CLocationState& location, SbVec3f& cameraTranslation, SbRotation& cameraRotation, const CHeadConfiguration& headConfiguration) const
{
    SbMatrix neck1Transform;
    neck1Transform.setRotate(SbRotation(neck1RotationAxis, headConfiguration.GetNeckPitchWithError()));
    SbMatrix neck2Transform;
    neck2Transform.setRotate(SbRotation(neck2RotationAxis, headConfiguration.GetNeckRollWithError()));
    SbMatrix neck3Transform;
    neck3Transform.setRotate(SbRotation(neck3RotationAxis, headConfiguration.GetNeckYawWithError()));

    SbMatrix rootToHeadBase = m_rootToHeadBase;

    SbMatrix neck1Local = rootToHeadBase.multLeft(neck1LocalTransform);
    SbMatrix neck1Pose = neck1Local.multLeft(neck1Transform);
    SbMatrix neck2Local = neck1Pose.multLeft(neck2LocalTransform);
    SbMatrix neck2Pose = neck2Local.multLeft(neck2Transform);
    SbMatrix neck3Local = neck2Pose.multLeft(neck3LocalTransform);
    SbMatrix neck3Pose = neck3Local.multLeft(neck3Transform);
    SbMatrix rootToCameraTransform = neck3Pose.multLeft(m_neck3ToCamera);

    return Calculations::CalculateRobotCameraPosition(location, rootToCameraTransform, cameraTranslation, cameraRotation);
}
