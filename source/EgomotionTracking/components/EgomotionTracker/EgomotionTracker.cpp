/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    EgomotionTracker::components::EgomotionTracker
 * @author     Thomas von der Heyde ( tvh242 at hotmail dot com )
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "EgomotionTracker.h"

#include <VisionX/interface/components/Calibration.h>
#include <VisionX/tools/ImageUtil.h>
#include <VisionX/interface/core/DataTypes.h>
#include <VisionX/tools/TypeMapping.h>

#include <MemoryX/libraries/memorytypes/entity/AgentInstance.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationSwarmParticle.h>
#include <EgomotionTracking/components/EgomotionTracking/LocationEstimator/TrackingFilter/EgomotionTrackingPF/PlaneScatteringDirections.h>

#include <EgomotionTracking/components/EgomotionTrackingConfiguration/TrackingConfiguration.h>

using namespace armarx;
using namespace visionx;
using namespace memoryx;

void EgomotionTracker::onInitImageProcessor()
{
    robotImages = new CByteImage*[2];
    robotImages[0] = robotImages[1] = NULL;
    resultImages = new CByteImage*[2];
    resultImages[0] =  resultImages[2] = NULL;

    imageProviderProxyName = getProperty<std::string>("imageProviderProxyName").getValue();
    robotStateProxyName = getProperty<std::string>("robotStateProxyName").getValue();
    kinematicUnitProxyName = getProperty<std::string>("kinematicUnitProxyName").getValue();
    platformUnitObserverProxyName = getProperty<std::string>("platformUnitObserverProxyName").getValue();
    workingMemoryProxyName = getProperty<std::string>("workingMemoryProxyName").getValue();
    kinematicUnitObserverProxyName = getProperty<std::string>("kinematicUnitObserverProxyName").getValue();

    agentInstanceName = getProperty<std::string>("agentInstanceName").getValue();

    jointAnglesChannelName = getProperty<std::string>("jointAnglesChannelName").getValue();
    neckRollNodeName = getProperty<std::string>("neckRollNodeName").getValue();
    neckPitchNodeName = getProperty<std::string>("neckPitchNodeName").getValue();
    neckYawNodeName = getProperty<std::string>("neckYawNodeName").getValue();

    m_neck1NodeName = getProperty<std::string>("neck1NodeName").getValue();
    m_neck2NodeName = getProperty<std::string>("neck2NodeName").getValue();
    m_neck3NodeName = getProperty<std::string>("neck3NodeName").getValue();

    platformVelocityChannelName = getProperty<std::string>("platformVelocityChannelName").getValue();
    platformVelocityXDataFieldName = getProperty<std::string>("platformVelocityXDataFieldName").getValue();
    platformVelocityYDataFieldName = getProperty<std::string>("platformVelocityYDataFieldName").getValue();
    platformVelocityRotationDataFieldName = getProperty<std::string>("platformVelocityRotationDataFieldName").getValue();

    leftCameraNodeName = getProperty<std::string>("leftCameraNodeName").getValue();
    rightCameraNodeName = getProperty<std::string>("rightCameraNodeName").getValue();
    headBaseNodeName = getProperty<std::string>("headBaseNodeName").getValue();

    maximumTrackingExecutionTime = getProperty<float>("maximumTrackingExecutionTime").getValue();

    automaticSwitchToTrackingEnabled = false;
    m_automaticExecution = true;
    m_cycleTrackingStatusEstimation = false;

    loadConfiguration();

    usingImageProvider(imageProviderProxyName);
    usingProxy(robotStateProxyName);
    usingProxy(kinematicUnitProxyName);
    usingProxy(platformUnitObserverProxyName);
    usingProxy(kinematicUnitObserverProxyName);
    usingProxy(workingMemoryProxyName);
}

void EgomotionTracker::onConnectImageProcessor()
{
    ImageProviderInfo imageProviderInfo = getImageProvider(imageProviderProxyName);
    imageFormat = imageProviderInfo.imageFormat;

    robotImages[0] = visionx::tools::createByteImage(imageProviderInfo);
    robotImages[1] = visionx::tools::createByteImage(imageProviderInfo);
    resultImages[0] = visionx::tools::createByteImage(imageProviderInfo);
    resultImages[1] = visionx::tools::createByteImage(imageProviderInfo);

    enableResultImages(2, ImageDimension(imageFormat.dimension.width, imageFormat.dimension.height), visionx::eRgb);

    imageProviderProxy = getProxy<ImageProviderInterfacePrx>(imageProviderProxyName);
    robotStateProxy = getProxy<RobotStateComponentInterfacePrx>(robotStateProxyName);
    kinematicUnitProxy = getProxy<KinematicUnitInterfacePrx>(kinematicUnitProxyName);
    platformUnitObserverProxy = getProxy<PlatformUnitObserverInterfacePrx>(platformUnitObserverProxyName);
    kinematicUnitObserverProxy = getProxy<KinematicUnitObserverInterfacePrx>(kinematicUnitObserverProxyName);
    workingMemoryProxy = getProxy<WorkingMemoryInterfacePrx>(workingMemoryProxyName);
    agentInstancesSegment = workingMemoryProxy->getAgentInstancesSegment();

    StereoCalibrationProviderInterfacePrx calibrationProviderProxy = StereoCalibrationProviderInterfacePrx::checkedCast(imageProviderProxy);

    if (!calibrationProviderProxy)
    {
        ARMARX_ERROR << "Image provider with name " << imageProviderProxyName << " is not a StereoCalibrationProvider" << std::endl;
        return;
    }

    imagesAreUndistorted = calibrationProviderProxy->getImagesAreUndistorted();

    /* Convert to IVT calibration. */
    stereoCalibration = visionx::tools::convert(calibrationProviderProxy->getStereoCalibration());
    m_stereoCalibrationGL.SetNearFarValues(10, 10000);
    m_stereoCalibrationGL.SetStereoCalibration(stereoCalibration);

    robotControl = new RobotControl(this, imageProviderInfo, robotStateProxy, kinematicUnitProxy, platformUnitObserverProxy, imagesAreUndistorted, kinematicUnitObserverProxy);
    robotControl->SetStereoCalibrationGL(m_stereoCalibrationGL);
    trackingDispatchingThread = new CTrackingDispatchingThread(robotControl, imageProviderInfo, this);

    status = eStopped;
}

void EgomotionTracker::onDisconnectImageProcessor()
{

}

void EgomotionTracker::onExitImageProcessor()
{
    if (trackingDispatchingThread)
    {
        delete trackingDispatchingThread;
    }

    if (robotControl)
    {
        delete robotControl;
    }

    if (robotImages)
    {
        delete robotImages[0];
        delete robotImages[1];
        delete[] robotImages;
    }

    if (resultImages)
    {
        delete resultImages[0];
        delete resultImages[1];
        delete[] resultImages;
    }
}

void EgomotionTracker::process()
{
    if (m_automaticExecution)
    {
        step();
    }
}

void EgomotionTracker::startSelfLocalisationProcess(const Ice::Current&)
{
    ARMARX_VERBOSE << "Starting self-localisation process" << std::endl;

    trackingDispatchingThread->InitializeLocalisation();

    status = ePositionInitialisation;
    automaticSwitchToTrackingEnabled = true;
    m_cycleTrackingStatusEstimation = true;
    trackingDispatchingThread->UpdateCycleTrackingStatusEstimation();
}

void EgomotionTracker::stopSelfLocalisationProcess(const ::Ice::Current&)
{
    ARMARX_VERBOSE << "Stopping self-localisation process" << std::endl;

    status = eStopped;

    m_automaticExecution = true;
    m_cycleTrackingStatusEstimation = false;
    automaticSwitchToTrackingEnabled = false;
    trackingDispatchingThread->UpdateCycleTrackingStatusEstimation();
}

void EgomotionTracker::beginLocalisation(const Ice::Current&)
{
    ARMARX_VERBOSE << "Beginning initialisation phase" << std::endl;

    trackingDispatchingThread->InitializeLocalisation();

    status = ePositionInitialisation;
    automaticSwitchToTrackingEnabled = false;
}

void EgomotionTracker::beginTracking(const Ice::Current&)
{
    ARMARX_VERBOSE << "Beginning tracking phase" << std::endl;

    trackingDispatchingThread->StartTracking();

    status = eTracking;
}

void EgomotionTracker::beginStatusEstimation(const Ice::Current&)
{
    ARMARX_VERBOSE << "Beginning status estimation phase - LOGIC TO BE IMPLEMENTED" << std::endl;

    trackingDispatchingThread->StartStatusEstimation();

    status = eStatusEstimate;
}

void EgomotionTracker::step(const Ice::Current&)
{
    if (!waitForImages())
    {
        ARMARX_WARNING << "No images from provider available" << std::endl;
    }
    else
    {
        getImages(robotImages);

        if (status == eStopped)
        {
            ::ImageProcessor::CopyImage(robotImages[0], resultImages[0]);
            ::ImageProcessor::CopyImage(robotImages[1], resultImages[1]);

            provideResultImages(resultImages);
        }
        else if (status == ePositionInitialisation || status == eTracking || status == eStatusEstimate)
        {
            trackingDispatchingThread->Dispatch();
            trackingDispatchingThread->WaitUntilIdle();

            ::ImageProcessor::CopyImage(trackingDispatchingThread->GetOMVisualizationImage(), resultImages[0]);
            ::ImageProcessor::ConvertImage(&(trackingDispatchingThread->GetTrackingControl()->GetTrackingImageProcessor()->GetEdgeImage()), resultImages[1]);
            provideResultImages(resultImages);

            if (status == ePositionInitialisation && automaticSwitchToTrackingEnabled == true)
            {
                if (trackingDispatchingThread->GetTrackingControl()->GetEstimatedImportanceWeight() > 0.95)
                {
                    ARMARX_VERBOSE << "Position initialised, automatically switching to tracking mode" << std::endl;
                    trackingDispatchingThread->StartTracking();
                    status = eTracking;
                }
            }
            else if (status == eTracking)
            {
                CLocationState locationEstimate = trackingDispatchingThread->GetTrackingControl()->GetLocationEstimate();
                Eigen::Vector3f positionVector(locationEstimate.GetX(), locationEstimate.GetY(), 0.0);
                FramedPositionPtr currentPosition = new FramedPosition(positionVector, GlobalFrame, "");
                Eigen::Quaternionf quaternion(locationEstimate.GetAlpha(), 0.0, 0.0, 1.0);
                FramedOrientationBasePtr currentOrientation = new FramedOrientation(quaternion.toRotationMatrix(), GlobalFrame, "");

                AgentInstanceBasePtr agentInstance = agentInstancesSegment->getAgentInstanceByName(agentInstanceName);

                if (!agentInstance)
                {
                    AgentInstancePtr agent = new AgentInstance(agentInstanceName);
                    agent->setPosition(currentPosition);
                    agent->setOrientation(currentOrientation);
                    auto robot = robotStateProxy->getSynchronizedRobot();
                    agent->setSharedRobot(robot);
                    agentInstancesSegment->addEntity(agent);
                }
                else
                {
                    AgentInstancePtr agent = AgentInstancePtr::dynamicCast(agentInstance);
                    agent->setPosition(currentPosition);
                    agent->setOrientation(currentOrientation);
                    agentInstancesSegment->updateEntity(agent->getId(), agent);
                }
            }
        }
    }
}

void EgomotionTracker::setAutomaticExecution(bool automaticExecution, const Ice::Current&)
{
    ARMARX_VERBOSE << "Setting AutomaticExecution to " << automaticExecution << std::endl;

    m_automaticExecution = automaticExecution;
}

void EgomotionTracker::setCycleTrackingStatusEstimation(bool cycleTrackingStatusEstimation, const Ice::Current&)
{
    ARMARX_VERBOSE << "Setting CycleTrackingStatusEstimation to " << cycleTrackingStatusEstimation << std::endl;

    m_cycleTrackingStatusEstimation = cycleTrackingStatusEstimation;
    trackingDispatchingThread->UpdateCycleTrackingStatusEstimation();
}

void EgomotionTracker::setUseOrientationDifferenceTracking(bool useOrientationDifferenceTracking, const Ice::Current& c)
{
    ARMARX_INFO << "Setting useNormalOrientationDifferenceMeasureTracking to " << useOrientationDifferenceTracking << std::endl;
    useNormalOrientationDifferenceMeasureTracking = useOrientationDifferenceTracking;
    trackingDispatchingThread->UpdateUseNormalOrientationDifferenceMeasureTracking();
}

bool EgomotionTracker::getUseOrientationDifferenceTracking(const Ice::Current& c)
{
    return useNormalOrientationDifferenceMeasureTracking;
}

void EgomotionTracker::setUseOrientationDifferenceInitialisation(bool useOrientationDifferenceInitialisation, const Ice::Current& c)
{
    ARMARX_INFO << "Setting useNormalOrientationDifferenceMeasureInitialisation to " << useOrientationDifferenceInitialisation << std::endl;
    useNormalOrientationDifferenceMeasureInitialisation = useOrientationDifferenceInitialisation;
    trackingDispatchingThread->UpdateUseNormalOrientationDifferenceMeasureInitialisation();
}

bool EgomotionTracker::getUseOrientationDifferenceInitialisation(const Ice::Current& c)
{
    return useNormalOrientationDifferenceMeasureInitialisation;
}

void EgomotionTracker::setUsePSOptimizationTracking(bool usePSOptimizationTracking, const Ice::Current& c)
{
    ARMARX_INFO << "Setting swarmOptimizationEnabled to " << usePSOptimizationTracking << std::endl;
    swarmOptimizationEnabled = usePSOptimizationTracking;
    trackingDispatchingThread->UpdateUsePSOptimizationTracking();
}

bool EgomotionTracker::getUsePSOptimizationTracking(const Ice::Current& c)
{
    return swarmOptimizationEnabled;
}

void EgomotionTracker::setUseMahalanobisDistanceBasedScatteringReduction(bool useMahalanobisDistanceBasedScatteringReduction, const Ice::Current& c)
{
    ARMARX_INFO << "Setting useMahalanobisDistanceBasedScatteringReduction to " << useMahalanobisDistanceBasedScatteringReduction << std::endl;
    this->useMahalanobisDistanceBasedScatteringReduction = useMahalanobisDistanceBasedScatteringReduction;
    trackingDispatchingThread->updateUseMahalanobisDistanceBasedScatteringReduction();
}

bool EgomotionTracker::getUseMahalanobisDistanceBasedScatteringReduction(const Ice::Current& c)
{
    return useMahalanobisDistanceBasedScatteringReduction;
}

float EgomotionTracker::getNeckErrorRollMin(const Ice::Current& c)
{
    return trackingNeckErrorRollMin;
}

float EgomotionTracker::getNeckErrorRollMax(const Ice::Current& c)
{
    return trackingNeckErrorRollMax;
}

float EgomotionTracker::getNeckErrorPitchMin(const Ice::Current& c)
{
    return trackingNeckErrorPitchMin;
}

float EgomotionTracker::getNeckErrorPitchMax(const Ice::Current& c)
{
    return trackingNeckErrorPitchMax;
}

float EgomotionTracker::getNeckErrorYawMin(const Ice::Current& c)
{
    return trackingNeckErrorYawMin;
}

float EgomotionTracker::getNeckErrorYawMax(const Ice::Current& c)
{
    return trackingNeckErrorYawMax;
}

LocationEstimate EgomotionTracker::getLocationEstimate(const ::Ice::Current& c)
{
    LocationEstimate locationEstimate;
    CLocationState locationState = trackingDispatchingThread->GetTrackingControl()->GetLocationEstimate();
    locationEstimate.x = locationState.GetX();
    locationEstimate.y = locationState.GetY();
    locationEstimate.alpha = locationState.GetAlpha();
    return locationEstimate;
}

PlaneScatteringDirections EgomotionTracker::getPlaneScatteringDirectionsPS(const ::Ice::Current& c)
{
    PlaneScatteringDirections planeScatteringDirections;
    MeanScatteringDirections meanScatteringDirections;
    Direction direction;
    CPlaneScatteringDirections planeScatteringDirectionsPS = trackingDispatchingThread->GetTrackingControl()->GetParticleSwarmLocationEstimator()->GetPlaneScatteringDirections();
    meanScatteringDirections.x = planeScatteringDirectionsPS.GetMean().x;
    meanScatteringDirections.y = planeScatteringDirectionsPS.GetMean().y;
    direction.x = planeScatteringDirectionsPS.GetDirection1().x;
    direction.y = planeScatteringDirectionsPS.GetDirection1().y;
    planeScatteringDirections.meanScattDir = meanScatteringDirections;
    planeScatteringDirections.direction1 = direction;
    planeScatteringDirections.sigma1 = planeScatteringDirectionsPS.GetSigma1();
    planeScatteringDirections.sigma2 = planeScatteringDirectionsPS.GetSigma2();
    return planeScatteringDirections;
}

PlaneScatteringDirections EgomotionTracker::getPlaneScatteringDirectionsPF(const ::Ice::Current& c)
{
    PlaneScatteringDirections planeScatteringDirections;
    MeanScatteringDirections meanScatteringDirections;
    Direction direction;
    CPlaneScatteringDirections planeScatteringDirectionsPF = trackingDispatchingThread->GetTrackingControl()->GetTrackingPF()->GetPlaneScatteringDirections();
    meanScatteringDirections.x = planeScatteringDirectionsPF.GetMean().x;
    meanScatteringDirections.y = planeScatteringDirectionsPF.GetMean().y;
    direction.x = planeScatteringDirectionsPF.GetDirection1().x;
    direction.y = planeScatteringDirectionsPF.GetDirection1().y;
    planeScatteringDirections.meanScattDir = meanScatteringDirections;
    planeScatteringDirections.direction1 = direction;
    planeScatteringDirections.sigma1 = planeScatteringDirectionsPF.GetSigma1();
    planeScatteringDirections.sigma2 = planeScatteringDirectionsPF.GetSigma2();
    return planeScatteringDirections;
}

ParticleVector EgomotionTracker::getParticlesPS(const ::Ice::Current& c)
{
    std::vector<Particle> particleVector;
    const std::vector<CLocationSwarmParticle>& psParticles = trackingDispatchingThread->GetTrackingControl()->GetParticleSwarmLocationEstimator()->GetParticles();

    if (psParticles.size() > 0)
    {
        for (std::vector<CLocationSwarmParticle>::const_iterator particleIterator = psParticles.begin(); particleIterator != psParticles.end(); ++particleIterator)
        {
            Particle particle;
            LocationEstimate locationEstimate;
            locationEstimate.x = particleIterator->GetX();
            locationEstimate.y = particleIterator->GetY();
            locationEstimate.alpha = particleIterator->GetAlpha();
            particle.locEst = locationEstimate;
            particle.neckRollError = particleIterator->GetNeckRollError();
            particle.neckPitchError = particleIterator->GetNeckPitchError();
            particle.neckYawError = particleIterator->GetNeckYawError();
            particle.importanceWeight = particleIterator->GetImportanceWeight();
            particleVector.push_back(particle);
        }
    }

    return particleVector;
}

ParticleVector EgomotionTracker::getParticlesPF(const ::Ice::Current& c)
{
    std::vector<Particle> particleVector;
    std::vector<CLocationParticle> particlePFVector;
    particlePFVector.resize(m_maxTrackingParticles, CLocationParticle(CLocationState(1000, 1000, 0)));
    trackingDispatchingThread->GetTrackingControl()->GetParticles(particlePFVector);
    int numberOfActiveParticles = trackingDispatchingThread->GetTrackingControl()->GetNumberOfActiveParticles();

    if (particlePFVector.size() > 0)
    {
        for (int i = 0; i < numberOfActiveParticles; i++)
        {
            Particle particle;
            LocationEstimate locationEstimate;
            locationEstimate.x = particlePFVector[i].GetX();
            locationEstimate.y = particlePFVector[i].GetY();
            locationEstimate.alpha = particlePFVector[i].GetAlpha();
            particle.locEst = locationEstimate;
            particle.neckRollError = particlePFVector[i].GetNeckRollError();
            particle.neckPitchError = particlePFVector[i].GetNeckPitchError();
            particle.neckYawError = particlePFVector[i].GetNeckYawError();
            particle.importanceWeight = particlePFVector[i].GetImportanceWeight();
            particleVector.push_back(particle);
        }
    }

    return particleVector;
}

BoundingBox EgomotionTracker::getBoundingBox(const ::Ice::Current& c)
{
    BoundingBox boundingBox;
    SbVec3f min, max;
    trackingDispatchingThread->GetRenderingModel().CalculateAlignedModelBoundingBox(min, max);
    std::vector<float> minb;
    minb.push_back(min[0]);
    minb.push_back(min[1]);
    minb.push_back(min[2]);
    std::vector<float> maxb;
    maxb.push_back(max[0]);
    maxb.push_back(max[1]);
    maxb.push_back(max[2]);
    boundingBox.min = minb;
    boundingBox.max = maxb;
    return boundingBox;
}

int EgomotionTracker::getNumberOfActiveParticles(const ::Ice::Current& c)
{
    int numberOfActiveParticles = trackingDispatchingThread->GetTrackingControl()->GetNumberOfActiveParticles();
    return numberOfActiveParticles;
}

EgomotionTrackerStatus EgomotionTracker::getStatus(const ::Ice::Current& c)
{
    return status;
}

void EgomotionTracker::setNeckErrorLimits(Ice::Int rollMin, Ice::Int rollMax, Ice::Int pitchMin, Ice::Int pitchMax, Ice::Int yawMin, Ice::Int yawMax, const ::Ice::Current& c)
{
    trackingNeckErrorRollMin = rollMin;
    trackingNeckErrorRollMax = rollMax;
    trackingNeckErrorPitchMin = pitchMin;
    trackingNeckErrorPitchMax = pitchMax;
    trackingNeckErrorYawMin = yawMin;
    trackingNeckErrorYawMax = yawMax;
    trackingDispatchingThread->UpdateNeckErrorLimits();
}

void EgomotionTracker::loadConfiguration()
{
    headPlannerEnabled = getProperty<bool>("headPlannerEnabled").getValue();

    minScreenLineLength = getProperty<float>("minScreenLineLength").getValue();
    minSegmentsPerJunction = getProperty<float>("minSegmentsPerJunction").getValue();
    maxJunctionPointToSegmentDeviation = getProperty<float>("maxJunctionPointToSegmentDeviation").getValue();
    junctionSegmentLength  = getProperty<float>("junctionSegmentLength").getValue();
    maxJunctionLinesDotProduct = getProperty<float>("maxJunctionLinesDotProduct").getValue();

    tresholdCannyLow = getProperty<int>("tresholdCannyLow").getValue();
    tresholdCannyHigh = getProperty<int>("tresholdCannyHigh").getValue();

    samplingModeTracking = getProperty<CJunctionPointLineModelObservationModel::CSamplingMode>("samplingModeTracking").getValue();
    gaussianWeightingModeTracking = getProperty<CJunctionPointLineModelObservationModel::CGaussianWeightingMode>("gaussianWeightingModeTracking").getValue();
    numberOfNormalsModeTracking = getProperty<CJunctionPointLineModelObservationModel::CNumberOfNormalsMode>("numberOfNormalsModeTracking").getValue();
    numberOfNormalsTracking = getProperty<int>("numberOfNormalsTracking").getValue();
    minNumberOfVisibleModelElementsTracking = getProperty<int>("minNumberOfVisibleModelElementsTracking").getValue();
    deltaAlphaTracking = getProperty<float>("deltaAlphaTracking").getValue();
    desiredPMinTracking = getProperty<float>("desiredPMinTracking").getValue();
    useNormalOrientationDifferenceMeasureTracking = getProperty<bool>("useNormalOrientationDifferenceMeasureTracking").getValue();
    normalOrientationDifferenceScalarTracking = getProperty<float>("normalOrientationDifferenceScalarTracking").getValue();
    normalOrientationDifferenceFactorTracking = getProperty<float>("normalOrientationDifferenceFactorTracking").getValue();
    samplingModeInitialisation = getProperty<CJunctionPointLineModelObservationModel::CSamplingMode>("samplingModeInitialisation").getValue();
    gaussianWeightingModeInitialisation = getProperty<CJunctionPointLineModelObservationModel::CGaussianWeightingMode>("gaussianWeightingModeInitialisation").getValue();
    numberOfNormalsModeInitialisation = getProperty<CJunctionPointLineModelObservationModel::CNumberOfNormalsMode>("numberOfNormalsModeInitialisation").getValue();
    numberOfNormalsInitialisation = getProperty<int>("numberOfNormalsInitialisation").getValue();
    minNumberOfVisibleModelElementsInitialisation = getProperty<int>("minNumberOfVisibleModelElementsInitialisation").getValue();
    deltaAlphaInitialisation = getProperty<float>("deltaAlphaInitialisation").getValue();
    desiredPMinInitialisation = getProperty<float>("desiredPMinInitialisation").getValue();
    useNormalOrientationDifferenceMeasureInitialisation = getProperty<bool>("useNormalOrientationDifferenceMeasureInitialisation").getValue();
    normalOrientationDifferenceScalarInitialisation = getProperty<float>("normalOrientationDifferenceScalarInitialisation").getValue();
    normalOrientationDifferenceFactorInitialisation = getProperty<float>("normalOrientationDifferenceFactorInitialisation").getValue();

    agingCoefficientLocationState = getProperty<int>("agingCoefficientLocationState").getValue();
    minScatteringFactorLocationState = getProperty<float>("minScatteringFactorLocationState").getValue();
    maxScatteringFactorLocationState = getProperty<float>("maxScatteringFactorLocationState").getValue();
    agingCoefficientNeckError = getProperty<int>("agingCoefficientNeckError").getValue();
    minScatteringFactorNeckError = getProperty<float>("minScatteringFactorNeckError").getValue();
    maxScatteringFactorNeckError = getProperty<float>("maxScatteringFactorNeckError").getValue();
    swarmOptimizationEnabled = getProperty<bool>("swarmOptimizationEnabled").getValue();
    velocityFactorLocalBest = getProperty<float>("velocityFactorLocalBest").getValue();
    velocityFactorGlobalBest = getProperty<float>("velocityFactorGlobalBest").getValue();
    localBestAgingFactor = getProperty<float>("localBestAgingFactor").getValue();
    bestWeightTresholdSwarmOptimization = getProperty<float>("bestWeightTresholdSwarmOptimization").getValue();
    annealingEnabled = getProperty<bool>("annealingEnabled").getValue();
    numberOfAnnealingLevels = getProperty<int>("numberOfAnnealingLevels").getValue();
    annealingFactor = getProperty<float>("annealingFactor").getValue();
    bestWeightTresholdAnnealing = getProperty<float>("bestWeightTresholdAnnealing").getValue();
    conservativeUpdate = getProperty<bool>("conservativeUpdate").getValue();
    resamplingTresholdEnabled = getProperty<bool>("resamplingTresholdEnabled").getValue();
    relativeNEffTreshold = getProperty<float>("relativeNEffTreshold").getValue();
    neckErrorUncertaintyMode = getProperty<CNeckErrorUncertaintyModel::UncertaintyMode>("neckErrorUncertaintyMode").getValue();
    sigmaRoll = getProperty<float>("sigmaRoll").getValue();
    sigmaPitch = getProperty<float>("sigmaPitch").getValue();
    sigmaYaw = getProperty<float>("sigmaYaw").getValue();
    scatteringReductionEnabled = getProperty<bool>("scatteringReductionEnabled").getValue();
    lineRegistrationVisualizationMode = getProperty<CTrackingControl::LineRegistrationVisualizationMode>("lineRegistrationVisualizationMode").getValue();
    trackingNeckErrorRollMin = getProperty<float>("trackingNeckErrorRollMin").getValue();
    trackingNeckErrorRollMax = getProperty<float>("trackingNeckErrorRollMax").getValue();
    trackingNeckErrorPitchMin = getProperty<float>("trackingNeckErrorPitchMin").getValue();
    trackingNeckErrorPitchMax = getProperty<float>("trackingNeckErrorPitchMax").getValue();
    trackingNeckErrorYawMin = getProperty<float>("trackingNeckErrorYawMin").getValue();
    trackingNeckErrorYawMax = getProperty<float>("trackingNeckErrorYawMax").getValue();
    useMahalanobisDistanceBasedScatteringReduction = getProperty<bool>("useMahalanobisDistanceBasedScatteringReduction").getValue();
    counterTreshold = getProperty<int>("counterTreshold").getValue();
    mahalanobisDistanceBasedScatterinReductionResamplingMode = getProperty<CEgomotionTrackingPFExecutionUnit::MahalanobisDistanceBasedScatteringReductionResamplingMode>("mahalanobisDistanceBasedScatteringReductionResamplingMode").getValue();
    totalWeightPercentageTreshold = getProperty<float>("totalWeightPercentageTreshold").getValue();

    inertiaFactor = getProperty<float>("inertiaFactor").getValue();
    cognitionFactor = getProperty<float>("cognitionFactor").getValue();
    socialFactor = getProperty<float>("socialFactor").getValue();
    initialisationRegionMinX = getProperty<float>("initialisationRegionMinX").getValue();
    initialisationRegionMaxX = getProperty<float>("initialisationRegionMaxX").getValue();
    initialisationRegionMinY = getProperty<float>("initialisationRegionMinY").getValue();
    initialisationRegionMaxY = getProperty<float>("initialisationRegionMaxY").getValue();
    initialisationRegionMinAlpha = getProperty<float>("initialisationRegionMinAlpha").getValue();
    initialisationRegionMaxAlpha = getProperty<float>("initialisationRegionMaxAlpha").getValue();

    headPlannerRollMin = getProperty<float>("headPlannerRollMin").getValue();
    headPlannerRollMax = getProperty<float>("headPlannerRollMax").getValue();
    headPlannerPitchMin = getProperty<float>("headPlannerPitchMin").getValue();
    headPlannerPitchMax = getProperty<float>("headPlannerPitchMax").getValue();
    headPlannerYawMin = getProperty<float>("headPlannerYawMin").getValue();
    headPlannerYawMax = getProperty<float>("headPlannerYawMax").getValue();
    rollSamplesCount = getProperty<int>("rollSamplesCount").getValue();
    pitchSamplesCount = getProperty<int>("pitchSamplesCount").getValue();
    yawSamplesCount = getProperty<int>("yawSamplesCount").getValue();
    movingHeadAllowed = getProperty<bool>("movingHeadAllowed").getValue();
    configurationMass = getProperty<float>("configurationMass").getValue();
    dampingCoefficient = getProperty<float>("dampingCoefficient").getValue();
    targetFadingCoefficient = getProperty<float>("targetFadingCoefficient").getValue();
    springCoefficientMode = getProperty<CMassSpringHeadConfigurationPlanner::SpringCoefficientMode>("springCoefficientMode").getValue();
    moveToMaxRelevanceFactorTreshold = getProperty<float>("moveToMaxRelevanceFactorTreshold").getValue();
    useMoveToMaxHeuristic = getProperty<bool>("useMoveToMaxHeuristic").getValue();

    relativeSystematicErrorVelocityX = getProperty<float>("relativeSystematicErrorVelocityX").getValue();
    relativeSystematicErrorVelocityY = getProperty<float>("relativeSystematicErrorVelocityY").getValue();
    relativeSystematicErrorVelocityAlpha = getProperty<float>("relativeSystematicErrorVelocityAlpha").getValue();
    absoluteSystematicPlatformAxesTiltError = getProperty<float>("absoluteSystematicPlatformAxesTiltError").getValue();
    sigmaVelocityX = getProperty<float>("sigmaVelocityX").getValue();
    sigmaVelocityY = getProperty<float>("sigmaVelocityY").getValue();
    sigmaVelocityAlpha = getProperty<float>("sigmaVelocityAlpha").getValue();
    sigmaPlatformAxesTiltError = getProperty<float>("sigmaPlatformAxesTiltError").getValue();
    sigmaX = getProperty<float>("sigmaX").getValue();
    sigmaY = getProperty<float>("sigmaY").getValue();
    sigmaAlpha = getProperty<float>("sigmaAlpha").getValue();
    robotControlStateUncertaintyEnabled = getProperty<bool>("robotControlStateUncertaintyEnabled").getValue();
    locationStateUncertaintyEnabled = getProperty<bool>("locationStateUncertaintyEnabled").getValue();

    m_environmentModelFilePath = getProperty<std::string>("environmentModelFilePath").getValue();
    m_dissimilarityEnvironmentModelFilePath = getProperty<std::string>("dissimilarityEnvironmentModelFilePath").getValue();

    m_useDissimilarityLikelihood = getProperty<bool>("useDissimilarityLikelihood").getValue();

    m_dynamicElementParticleReinitializationImportanceWeightTreshold = getProperty<double>("dynamicElementParticleReinitializationImportanceWeightTreshold").getValue();
    m_dynamicElementParticleReinitializationIterationCountTreshold = getProperty<int>("dynamicElementParticleReinitializationIterationCountTreshold").getValue();

    m_dynamicElementStateEstimateAcceptabilityTreshold = getProperty<double>("dynamicElementStateEstimateAcceptabilityTreshold").getValue();

    /* DynamicElementStatusEstimation Configuration. */

    m_dynamicElementMinParticles = getProperty<int>("dynamicElementMinParticles").getValue();
    m_dynamicElementMaxParticles = getProperty<int>("dynamicElementMaxParticles").getValue();
    m_useDynamicElementParticlesFunctionOfTransformationRange = getProperty<bool>("useDynamicElementParticlesFunctionOfTransformationRange").getValue();
    m_dynamicElementParticlesRatio = getProperty<int>("dynamicElementParticlesRatio").getValue();
    m_dynamicElementNumParticles = getProperty<int>("dynamicElementNumParticles").getValue();

    m_numPSLocationEstimatorParticles = getProperty<int>("numPSLocationEstimatorParticles").getValue();
    m_maxTrackingParticles = getProperty<int>("maxTrackingParticles").getValue();

    m_dynamicElementswarmOptimizationEnabled = getProperty<bool>("dynamicElementswarmOptimizationEnabled").getValue();
    m_dynamicElementVelocityFactorLocalBest = getProperty<float>("dynamicElementVelocityFactorLocalBest").getValue();
    m_dynamicElementVelocityFactorGlobalBest = getProperty<float>("dynamicElementVelocityFactorGlobalBest").getValue();
    m_dynamicElementLocalBestAgingFactor = getProperty<float>("dynamicElementLocalBestAgingFactor").getValue();
    m_dynamicElementBestWeightTresholdSwarmOptimization = getProperty<float>("dynamicElementBestWeightTresholdSwarmOptimization").getValue();

    m_dynamicElementAnnealingEnabled = getProperty<bool>("dynamicElementAnnealingEnabled").getValue();
    m_dynamicElementNumberOfAnnealingLevels = getProperty<int>("dynamicElementNumberOfAnnealingLevels").getValue();
    m_dynamicElementAnnealingFactor = getProperty<float>("dynamicElementAnnealingFactor").getValue();
    m_dynamicElementBestWeightTresholdAnnealing = getProperty<float>("dynamicElementBestWeightTresholdAnnealing").getValue();
    m_dynamicElementConservativeUpdate = getProperty<bool>("dynamicElementConservativeUpdate").getValue();

    m_dynamicElementResamplingTresholdEnabled = getProperty<bool>("dynamicElementResamplingTresholdEnabled").getValue();
    m_dynamicElementRelativeNEffTreshold = getProperty<float>("dynamicElementRelativeNEffTreshold").getValue();

    m_dynamicElementUncertaintyMode = getProperty<DynamicElementStatusUncertaintyModel::UncertaintyMode>("dynamicElementUncertaintyMode").getValue();
    m_dynamicElementSigmaAlpha = getProperty<float>("dynamicElementSigmaAlpha").getValue();
}
