#pragma once

/* EgomotionTracking includes. */

#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotControlInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/Armar3ControlInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/CameraCalibration/StereoCalibrationGL.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Profiling.h>

/* ArmarXCore includes. */

#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/interface/observers/PlatformUnitObserverInterface.h>
#include <RobotAPI/interface/observers/KinematicUnitObserverInterface.h>

#include <VirtualRobot/VirtualRobot.h>

#include "EgomotionTracker.h"

namespace armarx
{
    class EgomotionTracker;

    class RobotControl: public CArmar3ControlInterface
    {
    public:

        /**
         * RobotControl constructor.
         *
         * @param egomotionTracker EgomotionTracker instance.
         * @param imageProviderInfo Information about the ImageProvider.
         * @param robotStateProxy RobotStateComponent proxy.
         * @param kinematicUnitProxy KinematicUnit proxy.
         * @param platformUnitObserverProxy PlatformUnitObserver proxy.
         * @param imagesAreUndistorted TRUE if images received from the ImageProvider are already undistorted.
         * @param kinematicUnitObserverProxy KinematicUnitObserver proxy.
         */
        RobotControl(EgomotionTracker* egomotionTracker, visionx::ImageProviderInfo imageProviderInfo, armarx::RobotStateComponentInterfacePrx robotStateProxy,
                     armarx::KinematicUnitInterfacePrx kinematicUnitProxy, armarx::PlatformUnitObserverInterfacePrx platformUnitObserverProxy, bool imagesAreUndistorted,
                     armarx::KinematicUnitObserverInterfacePrx kinematicUnitObserverProxy);
        /**
         * RobotControl destructor.
         */
        ~RobotControl() override;

        /* Inherited but not actually used. */
        bool LockRobotMovement() override;
        void ReleaseRobotMovementLock() override;
        bool SetTargetRobotControlState(const CRobotControlState& robotControlState) override;
        bool SetTargetRobotHeadConfiguration(const CHeadConfiguration& targetHeadConfiguration) override;
        void GetNeckRPYBounds(float& neckRollMin, float& neckRollMax, float& neckPitchMin, float& neckPitchMax, float& neckYawMin, float& neckYawMax) const override;

        /*!
         * Updates the robot's information (images/velocity).
         */
        bool DispatchRobot() override;
        /*!
         * Gets the current head configuration (RPY).
         * \return Current head configuration.
         */
        CHeadConfiguration GetHeadConfiguration() const override;

        bool CalculateCameraPosition(const CLocationState& location, SbVec3f& cameraTranslation, SbRotation& cameraRotation, const CHeadConfiguration& headConfiguration) const override;

    protected:

        CByteImage** m_ppUndistortedImages;
        CProfiling m_robotTimingProfiler;
        EgomotionTracker* egomotionTracker;
        RobotStateComponentInterfacePrx robotStateProxy;
        KinematicUnitInterfacePrx kinematicUnitProxy;
        PlatformUnitObserverInterfacePrx platformUnitObserverProxy;
        KinematicUnitObserverInterfacePrx kinematicUnitObserverProxy;
        bool imagesAreUndistorted;

        CHeadConfiguration m_currentHeadConfiguration;

        /* Used to set new values for the neck joints. */
        SharedRobotNodeInterfacePrx neck1NodeProxy;
        SharedRobotNodeInterfacePrx neck2NodeProxy;
        SharedRobotNodeInterfacePrx neck3NodeProxy;

        /* Robot and node pointers. */
        VirtualRobot::RobotPtr robotPtr;
        VirtualRobot::RobotNodePtr headBaseNodePtr;
        VirtualRobot::RobotNodePtr leftCameraNodePtr;
        VirtualRobot::RobotNodePtr rightCameraNodePtr;
        VirtualRobot::RobotNodePtr neck3NodePtr;

        /* Local transforms of the neck joints. Used to calculate the transformation matrix from the root to the camera node. */
        SbMatrix neck1LocalTransform;
        SbMatrix neck2LocalTransform;
        SbMatrix neck3LocalTransform;

        /* Rotation axis of the neck joints. Used to calculate the transformation matrix from the root to the camera node. */
        SbVec3f neck1RotationAxis;
        SbVec3f neck2RotationAxis;
        SbVec3f neck3RotationAxis;

        /* Additional transformation matrices. */
        SbMatrix m_rootToHeadBase;
        SbMatrix m_neck3ToCamera;

        /* Data field identifiers. Used to query the current velocity of the platform and the current head configuration. */
        DataFieldIdentifierBasePtr identifierVelocityX;
        DataFieldIdentifierBasePtr identifierVelocityY;
        DataFieldIdentifierBasePtr identifierVelocityRotation;
        DataFieldIdentifierBasePtr identifierNeckRoll;
        DataFieldIdentifierBasePtr identifierNeckPitch;
        DataFieldIdentifierBasePtr identifierNeckYaw;
    };
}

