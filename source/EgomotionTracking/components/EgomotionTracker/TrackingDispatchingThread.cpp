#include "TrackingDispatchingThread.h"

#include <EgomotionTracking/components/EgomotionTrackingConfiguration/TrackingConfiguration.h>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/XMLPrimitives.h>

using namespace std;
using namespace visionx;
using namespace armarx;

CTrackingDispatchingThread::CTrackingDispatchingThread(CControllableRobotControlInterface* pRobotControl, visionx::ImageProviderInfo imageProviderInfo,
        EgomotionTracker* egomotionTracker) :
    m_pRobotControl(pRobotControl)
{
    imageFormat = imageProviderInfo.imageFormat;
    m_pVisualizationImage = new CByteImage(imageFormat.dimension.width, imageFormat.dimension.height, CByteImage::eRGB24, false);

    this->egomotionTracker = egomotionTracker;

    m_headPlannerEnabled = egomotionTracker->headPlannerEnabled;

    m_pTrackingControl = new CTrackingControl(egomotionTracker->m_numPSLocationEstimatorParticles, egomotionTracker->m_maxTrackingParticles,
            egomotionTracker->m_dynamicElementMaxParticles);

    InitializeRenderer();
    InitializeRobotCalibration();
    InitializeCudaRenderer();
    InitializeObservationModels();
    InitializeTracking();
    InitializeParticleSwarmLocationEstimator();
    InitializeDynamicElementStatusEstimation();
    InitializeHeadConfigurationPlanner();
    ConfigureEdgeProcessor();
}

CTrackingDispatchingThread::~CTrackingDispatchingThread()
{
    if (m_pVisualizationImage)
    {
        delete m_pVisualizationImage;
    }

    if (m_pTrackingControl)
    {
        delete m_pTrackingControl;
    }
}

void CTrackingDispatchingThread::ReinitializeSwarmParticles()
{
    CParticleSwarmLocationEstimator* pPSLocationEstimator = m_pTrackingControl->GetParticleSwarmLocationEstimator();
    assert(pPSLocationEstimator);
    pPSLocationEstimator->InitializeParticlesInRegion(m_initialisationRegion);
}

void CTrackingDispatchingThread::SetHeadTarget(const CHeadConfiguration& targetConfiguration)
{
    m_massSpringHeadConfigurationPlanner.SetTargetHeadConfiguration(targetConfiguration);
}

CTrackingControl* CTrackingDispatchingThread::GetTrackingControl()
{
    return m_pTrackingControl;
}

const CByteImage* CTrackingDispatchingThread::GetOMVisualizationImage() const
{
    return m_pVisualizationImage;
}

const CRenderingModel& CTrackingDispatchingThread::GetRenderingModel() const
{
    return m_renderingModel;
}

void CTrackingDispatchingThread::StartTracking()
{
    if (m_pTrackingControl->GetTrackingMode() == CTrackingControl::eDispatchParticleSwarmLocationEstimator)
    {
        m_pTrackingControl->ReinitializeTrackingParticlesWithPSLocationEstimator();
    }

    m_pTrackingControl->SetTrackingMode(CTrackingControl::eEgomotionTracking);
}

void CTrackingDispatchingThread::InitializeLocalisation()
{
    CParticleSwarmLocationEstimator* pPSLocationEstimator = m_pTrackingControl->GetParticleSwarmLocationEstimator();
    assert(pPSLocationEstimator);
    pPSLocationEstimator->InitializeParticlesInRegion(m_initialisationRegion);
    m_pTrackingControl->SetTrackingMode(CTrackingControl::eDispatchParticleSwarmLocationEstimator);
    m_pTrackingControl->GetDESEControl()->resetDynamicElements();
}

void CTrackingDispatchingThread::StartStatusEstimation()
{
    m_pTrackingControl->SetTrackingMode(CTrackingControl::eDynamicElementStatusEstimation);
}

void CTrackingDispatchingThread::SetHeadPlannerEnabled(bool enabled)
{
    m_headPlannerEnabled = enabled;
}

bool CTrackingDispatchingThread::GetHeadPlannerEnabled()
{
    return m_headPlannerEnabled;
}

void CTrackingDispatchingThread::DispatchingFunction()
{
    m_pTrackingControl->DispatchLocalisation();

    m_massSpringHeadConfigurationPlanner.SetLocationEstimator(m_pTrackingControl);

    if (m_headPlannerEnabled)
    {
        float neckRollError = 0.0, neckPitchError = 0.0, neckYawError = 0.0;

        if (m_pTrackingControl->GetNeckRPYErrorEstimate(neckRollError, neckPitchError, neckYawError))
        {
            m_massSpringHeadConfigurationPlanner.SetNeckErrors(neckRollError, neckPitchError, neckYawError);
        }

        m_massSpringHeadConfigurationPlanner.DispatchHeadConfigurationPlanner();
    }
}

void CTrackingDispatchingThread::InitializeRenderer()
{
    m_parametricLineRenderer.SetVisibleScreenRegion(0, imageFormat.dimension.width, 0, imageFormat.dimension.height);
    m_parametricLineRenderer.SetRenderingModel(&m_renderingModel);
    m_parametricLineRenderer.SetImageSize(imageFormat.dimension.width, imageFormat.dimension.height);

    if (!m_renderingModel.loadRVLModelFromXMLFile(egomotionTracker->m_environmentModelFilePath))
    {
        std::cout << "Failed to load tracking RVL from XML file" << std::endl;
    }

    if (!m_renderingModel.loadMultipleVisualizationRVLModelsFromXMLFile(egomotionTracker->m_dissimilarityEnvironmentModelFilePath, m_pTrackingControl->getNumberOfThreads()))
    {
        std::cout << "Failed to load visualization model(s) from XML file" << std::endl;
        return;
    }

    ConfigureRenderer();
}

void CTrackingDispatchingThread::InitializeCudaRenderer()
{

}

void CTrackingDispatchingThread::InitializeRobotCalibration()
{
    if (m_pRobotControl != NULL)
    {
        m_parametricLineRenderer.SetProjectionMatrixGl(m_pRobotControl->GetCameraProjectionMatrixGL());
    }
}

void CTrackingDispatchingThread::InitializeObservationModels()
{
    m_trackingObservationModel.SetVisualizationImage(m_pVisualizationImage);
    m_initialisationObservationModel.SetVisualizationImage(m_pVisualizationImage);
    m_dynamicElementStatusEstimationObservationModel.SetVisualizationImage(m_pVisualizationImage);
    ConfigureObservationModels();
}

void CTrackingDispatchingThread::InitializeTracking()
{
    m_trackingLocationRegion = CSimpleLocationRegion(1000, 3000, 0, 2500, 0.0, M_PI);
    m_pTrackingControl->SetLocationRegion(&m_trackingLocationRegion);
    m_pTrackingControl->SetParametricLineRenderer(m_parametricLineRenderer);

    m_pTrackingControl->SetRobotControl(m_pRobotControl);
    m_pTrackingControl->SetMasterLocationEstimator(NULL);
    m_pTrackingControl->SetGroundTruthLocationEstimator(NULL);
    m_pTrackingControl->SetTrackingImageProcessor(&m_trackingIVTEdgeProcessor);

    m_pTrackingControl->SetTrackingMode(CTrackingControl::eEvaluateObservationModel);
    m_pTrackingControl->SetParametricLineRenderer(m_parametricLineRenderer);
    m_pTrackingControl->SetTrackingObservationModel(&m_trackingObservationModel);
    m_pTrackingControl->SetParticleSwarmLocationEstimatorObservationModel(&m_initialisationObservationModel);

    m_trackingLocationStatePredictor.SetLocationStateUncertaintyModel(&m_trackingLocationStateUncertaintyModel);
    m_trackingLocationStatePredictor.SetRobotControlStateUncertaintyModel(&m_trackingRobotControlStateUncertaintyModel);
    m_trackingLocationStatePredictor.SetRobotMotionModel(&m_trackingMotionModel);
    m_pTrackingControl->SetLocationStatePredictor(&m_trackingLocationStatePredictor);
    m_pTrackingControl->SetNeckErrorUncertaintyModel(&m_neckErrorUncertaintyModel);

    m_pTrackingControl->SetUncertaintyFactorCalculatorLocationState(&m_scatteringFactorCalculatorLocationState);
    m_pTrackingControl->SetUncertaintyFactorCalculatorNeckError(&m_scatteringFactorCalculatorNeckError);

    ConfigurePredictionModels();
    ConfigureTracking();
}

void CTrackingDispatchingThread::InitializeParticleSwarmLocationEstimator()
{
    ConfigureParticleSwarmLocationEstimator();
    ReinitializeSwarmParticles();
}

void CTrackingDispatchingThread::InitializeHeadConfigurationPlanner()
{
    m_massSpringHeadConfigurationPlanner.SetControllableRobot(m_pRobotControl);

    m_massSpringHeadConfigurationPlanner.SetRelevanceFactorProcessingUnit(&m_projectedLinesInViewRelevanceProcessingUnit);

    m_massSpringHeadConfigurationPlanner.SetLocationEstimator(m_pTrackingControl);
    m_projectedLinesInViewRelevanceProcessingUnit.SetParametricLineRenderer(&m_parametricLineRenderer);
    m_projectedLinesInViewRelevanceProcessingUnit.SetScreenLineRelevanceWeightingFunctionObject(&m_screenLineRelevanceWeightingFunctionObject);

    ConfigureHeadConfigurationPlaner();
}

void CTrackingDispatchingThread::InitializeDynamicElementStatusEstimation()
{
    m_pTrackingControl->SetDynamicElementStatusEstimationObservationModel(&m_dynamicElementStatusEstimationObservationModel);
    m_pTrackingControl->SetDynamicElementStatusUncertaintyModel(&m_dynamicElementStatusUncertaintyModel);
    m_pTrackingControl->InitializeDynamicElements();
    m_pTrackingControl->GetDESEControl()->setupModelVisualization();
    m_pTrackingControl->GetDESEControl()->setStereoCalibration(egomotionTracker->m_stereoCalibrationGL.getStereoCalibration());

    ConfigureDynamicElementStatusEstimation();
}

void CTrackingDispatchingThread::ConfigureRenderer()
{
    m_parametricLineRenderer.SetMinScreenLineLength(egomotionTracker->minScreenLineLength);
    m_parametricLineRenderer.SetMinSegmentsPerJunction(egomotionTracker->minSegmentsPerJunction);
    m_parametricLineRenderer.SetMaxJunctionPointToSegmentDeviation(egomotionTracker->maxJunctionPointToSegmentDeviation);
    m_parametricLineRenderer.SetJunctionSegmentLength(egomotionTracker->junctionSegmentLength);
    m_parametricLineRenderer.SetMaxJunctionLinesDotProduct(egomotionTracker->maxJunctionLinesDotProduct);
}

void CTrackingDispatchingThread::ConfigureEdgeProcessor()
{
    m_trackingIVTEdgeProcessor.SetThresholdCannyLow(egomotionTracker->tresholdCannyLow);
    m_trackingIVTEdgeProcessor.SetThresholdCannyHigh(egomotionTracker->tresholdCannyHigh);
}

void CTrackingDispatchingThread::ConfigureObservationModels()
{
    m_trackingObservationModel.SetSamplingMode(egomotionTracker->samplingModeTracking);
    m_trackingObservationModel.SetGaussianWeightingMode(egomotionTracker->gaussianWeightingModeTracking);
    m_trackingObservationModel.SetNumberOfNormalsMode(egomotionTracker->numberOfNormalsModeTracking);
    m_trackingObservationModel.SetNumberOfNormals(egomotionTracker->numberOfNormalsTracking);
    m_trackingObservationModel.SetMinNumberOfVisibleModelElements(egomotionTracker->minNumberOfVisibleModelElementsTracking);
    m_trackingObservationModel.SetDeltaAlpha(Conversions::DegreeToRadians(egomotionTracker->deltaAlphaTracking));
    m_trackingObservationModel.SetDesiredPMin(egomotionTracker->desiredPMinTracking);
    m_trackingObservationModel.SetUseNormalOrientationDifferenceMeasure(egomotionTracker->useNormalOrientationDifferenceMeasureTracking);
    m_trackingObservationModel.setNormalOrientationDifferenceScalar(egomotionTracker->normalOrientationDifferenceScalarTracking);
    m_trackingObservationModel.setNormalOrientationDifferenceFactor(egomotionTracker->normalOrientationDifferenceFactorTracking);
    m_initialisationObservationModel.SetSamplingMode(egomotionTracker->samplingModeInitialisation);
    m_initialisationObservationModel.SetGaussianWeightingMode(egomotionTracker->gaussianWeightingModeInitialisation);
    m_initialisationObservationModel.SetNumberOfNormalsMode(egomotionTracker->numberOfNormalsModeInitialisation);
    m_initialisationObservationModel.SetNumberOfNormals(egomotionTracker->numberOfNormalsInitialisation);
    m_initialisationObservationModel.SetMinNumberOfVisibleModelElements(egomotionTracker->minNumberOfVisibleModelElementsInitialisation);
    m_initialisationObservationModel.SetDeltaAlpha(Conversions::DegreeToRadians(egomotionTracker->deltaAlphaInitialisation));
    m_initialisationObservationModel.SetDesiredPMin(egomotionTracker->desiredPMinInitialisation);
    m_initialisationObservationModel.SetUseNormalOrientationDifferenceMeasure(egomotionTracker->useNormalOrientationDifferenceMeasureInitialisation);
    m_initialisationObservationModel.setNormalOrientationDifferenceScalar(egomotionTracker->normalOrientationDifferenceScalarInitialisation);
    m_initialisationObservationModel.setNormalOrientationDifferenceFactor(egomotionTracker->normalOrientationDifferenceFactorInitialisation);
    m_dynamicElementStatusEstimationObservationModel.SetSamplingMode(egomotionTracker->samplingModeTracking);
    m_dynamicElementStatusEstimationObservationModel.SetGaussianWeightingMode(egomotionTracker->gaussianWeightingModeTracking);
    m_dynamicElementStatusEstimationObservationModel.SetNumberOfNormalsMode(egomotionTracker->numberOfNormalsModeTracking);
    m_dynamicElementStatusEstimationObservationModel.SetNumberOfNormals(egomotionTracker->numberOfNormalsTracking);
    m_dynamicElementStatusEstimationObservationModel.SetMinNumberOfVisibleModelElements(0);
    m_dynamicElementStatusEstimationObservationModel.SetDeltaAlpha(Conversions::DegreeToRadians(egomotionTracker->deltaAlphaTracking));
    m_dynamicElementStatusEstimationObservationModel.SetDesiredPMin(egomotionTracker->desiredPMinTracking);
    m_dynamicElementStatusEstimationObservationModel.SetUseNormalOrientationDifferenceMeasure(egomotionTracker->useNormalOrientationDifferenceMeasureTracking);
    m_dynamicElementStatusEstimationObservationModel.setNormalOrientationDifferenceScalar(egomotionTracker->normalOrientationDifferenceScalarTracking);
    m_dynamicElementStatusEstimationObservationModel.setNormalOrientationDifferenceFactor(egomotionTracker->normalOrientationDifferenceFactorTracking);

}

void CTrackingDispatchingThread::ConfigureTracking()
{
    m_scatteringFactorCalculatorLocationState.SetAgingCoefficient(egomotionTracker->agingCoefficientLocationState);
    m_scatteringFactorCalculatorLocationState.SetScatteringRange(egomotionTracker->minScatteringFactorLocationState, egomotionTracker->maxScatteringFactorLocationState);
    m_scatteringFactorCalculatorNeckError.SetAgingCoefficient(egomotionTracker->agingCoefficientNeckError);
    m_scatteringFactorCalculatorNeckError.SetScatteringRange(egomotionTracker->minScatteringFactorNeckError, egomotionTracker->maxScatteringFactorNeckError);
    m_pTrackingControl->GetTrackingPF()->SetSwarmOptimization(egomotionTracker->swarmOptimizationEnabled, egomotionTracker->velocityFactorLocalBest, egomotionTracker->velocityFactorGlobalBest,
            egomotionTracker->localBestAgingFactor, egomotionTracker->bestWeightTresholdSwarmOptimization);
    m_pTrackingControl->GetTrackingPF()->SetAnnealing(egomotionTracker->annealingEnabled, egomotionTracker->numberOfAnnealingLevels, egomotionTracker->annealingFactor,
            egomotionTracker->bestWeightTresholdAnnealing, egomotionTracker->conservativeUpdate);
    m_pTrackingControl->GetTrackingPF()->SetResamplingThreshold(egomotionTracker->resamplingTresholdEnabled, egomotionTracker->relativeNEffTreshold);
    m_neckErrorUncertaintyModel.SetUncertaintyMode(egomotionTracker->neckErrorUncertaintyMode);
    m_neckErrorUncertaintyModel.SetErrorLimits(Conversions::DegreeToRadians(egomotionTracker->trackingNeckErrorRollMin), Conversions::DegreeToRadians(egomotionTracker->trackingNeckErrorRollMax),
            Conversions::DegreeToRadians(egomotionTracker->trackingNeckErrorPitchMin), Conversions::DegreeToRadians(egomotionTracker->trackingNeckErrorPitchMax),
            Conversions::DegreeToRadians(egomotionTracker->trackingNeckErrorYawMin), Conversions::DegreeToRadians(egomotionTracker->trackingNeckErrorYawMax));
    m_neckErrorUncertaintyModel.SetSigmaRoll(Conversions::DegreeToRadians(egomotionTracker->sigmaRoll));
    m_neckErrorUncertaintyModel.SetSigmaPitch(Conversions::DegreeToRadians(egomotionTracker->sigmaPitch));
    m_neckErrorUncertaintyModel.SetSigmaYaw(Conversions::DegreeToRadians(egomotionTracker->sigmaYaw));
    m_pTrackingControl->SetScatteringReduction(egomotionTracker->scatteringReductionEnabled);
    m_pTrackingControl->SetLineRegistrationVisualizationMode(egomotionTracker->lineRegistrationVisualizationMode);
    m_pTrackingControl->SetNumberOfActiveParticles(configuration_defaultActiveParticles);
    m_pTrackingControl->GetTrackingPF()->setUseMahalanobisDistanceBasedScatteringReduction(egomotionTracker->useMahalanobisDistanceBasedScatteringReduction);
    m_pTrackingControl->GetTrackingPF()->setCounterTreshold(egomotionTracker->counterTreshold);
    m_pTrackingControl->GetTrackingPF()->setMahalanobisDistanceBasedScatteringReductionResamplingMode(egomotionTracker->mahalanobisDistanceBasedScatterinReductionResamplingMode);
    m_pTrackingControl->GetTrackingPF()->setTotalWeightPercentageTreshold(egomotionTracker->totalWeightPercentageTreshold);
}

void CTrackingDispatchingThread::ConfigureParticleSwarmLocationEstimator()
{
    CParticleSwarmLocationEstimator* pPSLocationEstimator = m_pTrackingControl->GetParticleSwarmLocationEstimator();
    assert(pPSLocationEstimator);
    pPSLocationEstimator->SetParameters(egomotionTracker->inertiaFactor, egomotionTracker->cognitionFactor, egomotionTracker->socialFactor);
    m_initialisationRegion.SetMinX(egomotionTracker->initialisationRegionMinX);
    m_initialisationRegion.SetMinY(egomotionTracker->initialisationRegionMinY);
    m_initialisationRegion.SetMinAlpha(Conversions::DegreeToRadians(egomotionTracker->initialisationRegionMinAlpha));
    m_initialisationRegion.SetMaxX(egomotionTracker->initialisationRegionMaxX);
    m_initialisationRegion.SetMaxY(egomotionTracker->initialisationRegionMaxY);
    m_initialisationRegion.SetMaxAlpha(Conversions::DegreeToRadians(egomotionTracker->initialisationRegionMaxAlpha));
}

void CTrackingDispatchingThread::ConfigureHeadConfigurationPlaner()
{
    m_massSpringHeadConfigurationPlanner.SetPlanningLimits(Conversions::DegreeToRadians(egomotionTracker->headPlannerRollMin), Conversions::DegreeToRadians(egomotionTracker->headPlannerRollMax),
            Conversions::DegreeToRadians(egomotionTracker->headPlannerPitchMin), Conversions::DegreeToRadians(egomotionTracker->headPlannerPitchMax),
            Conversions::DegreeToRadians(egomotionTracker->headPlannerYawMin), Conversions::DegreeToRadians(egomotionTracker->headPlannerYawMax));
    m_massSpringHeadConfigurationPlanner.InitHeadConfigurationSamplesUniformDistributed(egomotionTracker->rollSamplesCount, egomotionTracker->pitchSamplesCount, egomotionTracker->yawSamplesCount);
    SetHeadTarget(CHeadConfiguration(SbVec3f(0.0, 0.0, 0.0)));
    m_massSpringHeadConfigurationPlanner.SetMovingHeadAllowed(egomotionTracker->movingHeadAllowed);
    m_massSpringHeadConfigurationPlanner.SetConfigurationMass(egomotionTracker->configurationMass);
    m_massSpringHeadConfigurationPlanner.SetDampingCoefficient(egomotionTracker->dampingCoefficient);
    m_massSpringHeadConfigurationPlanner.SetTargetFadingCoefficient(egomotionTracker->targetFadingCoefficient);
    m_massSpringHeadConfigurationPlanner.SetSpringCoefficientMode(egomotionTracker->springCoefficientMode);
    m_massSpringHeadConfigurationPlanner.SetMoveToMaxRelevanceFactorThreshold(egomotionTracker->moveToMaxRelevanceFactorTreshold);
    m_massSpringHeadConfigurationPlanner.SetUseMoveToMaxHeuristic(egomotionTracker->useMoveToMaxHeuristic);
}

void CTrackingDispatchingThread::ConfigureDynamicElementStatusEstimation()
{
    m_dynamicElementStatusUncertaintyModel.SetUncertaintyMode(egomotionTracker->m_dynamicElementUncertaintyMode);
    m_dynamicElementStatusUncertaintyModel.SetSigmaAlpha(egomotionTracker->m_dynamicElementSigmaAlpha);

    m_pTrackingControl->GetDESEControl()->getSingleDynamicElementStatusFilter()->setUseDissimilarityLikelihood(egomotionTracker->m_useDissimilarityLikelihood);

    m_pTrackingControl->GetDESEControl()->getSingleDynamicElementStatusFilter()->setSwarmOptimization(egomotionTracker->m_dynamicElementswarmOptimizationEnabled,
            egomotionTracker->m_dynamicElementVelocityFactorLocalBest,
            egomotionTracker->m_dynamicElementVelocityFactorGlobalBest,
            egomotionTracker->m_dynamicElementLocalBestAgingFactor,
            egomotionTracker->m_dynamicElementBestWeightTresholdSwarmOptimization);
    m_pTrackingControl->GetDESEControl()->getSingleDynamicElementStatusFilter()->setAnnealing(egomotionTracker->m_dynamicElementAnnealingEnabled,
            egomotionTracker->m_dynamicElementNumberOfAnnealingLevels,
            egomotionTracker->m_dynamicElementAnnealingFactor,
            egomotionTracker->m_dynamicElementBestWeightTresholdAnnealing,
            egomotionTracker->m_dynamicElementConservativeUpdate);
    m_pTrackingControl->GetDESEControl()->getSingleDynamicElementStatusFilter()->setResamplingThreshold(egomotionTracker->m_dynamicElementResamplingTresholdEnabled,
            egomotionTracker->m_dynamicElementRelativeNEffTreshold);

    m_pTrackingControl->GetDESEControl()->configureDynamicElementParticleReinitialization(egomotionTracker->m_dynamicElementParticleReinitializationImportanceWeightTreshold,
            egomotionTracker->m_dynamicElementParticleReinitializationIterationCountTreshold);
    m_pTrackingControl->GetDESEControl()->configureDynamicElementNumberOfParticles(egomotionTracker->m_dynamicElementMinParticles, egomotionTracker->m_dynamicElementMaxParticles,
            egomotionTracker->m_useDynamicElementParticlesFunctionOfTransformationRange,
            egomotionTracker->m_dynamicElementParticlesRatio, egomotionTracker->m_dynamicElementNumParticles);
    m_pTrackingControl->GetDESEControl()->setDynamicElementStateEstimateAcceptabilityTreshold(egomotionTracker->m_dynamicElementStateEstimateAcceptabilityTreshold);
}

void CTrackingDispatchingThread::ConfigurePredictionModels()
{
    m_trackingRobotControlStateUncertaintyModel.SetRelativeSystematicErrorVelocityX(egomotionTracker->relativeSystematicErrorVelocityX);
    m_trackingRobotControlStateUncertaintyModel.SetRelativeSystematicErrorVelocityY(egomotionTracker->relativeSystematicErrorVelocityY);
    m_trackingRobotControlStateUncertaintyModel.SetRelativeSystematicErrorVelocityAlpha(egomotionTracker->relativeSystematicErrorVelocityAlpha);
    m_trackingRobotControlStateUncertaintyModel.SetAbsoluteSystematicPlatformAxesTiltError(egomotionTracker->absoluteSystematicPlatformAxesTiltError);
    m_trackingRobotControlStateUncertaintyModel.SetSigmaVelocityX(Conversions::PercentageToProbability(egomotionTracker->sigmaVelocityX));
    m_trackingRobotControlStateUncertaintyModel.SetSigmaVelocityY(Conversions::PercentageToProbability(egomotionTracker->sigmaVelocityY));
    m_trackingRobotControlStateUncertaintyModel.SetSigmaVelocityAlpha(Conversions::PercentageToProbability(egomotionTracker->sigmaVelocityAlpha));
    m_trackingRobotControlStateUncertaintyModel.SetSigmaPlatformAxesTiltError(Conversions::DegreeToRadians(egomotionTracker->sigmaPlatformAxesTiltError));
    m_trackingLocationStateUncertaintyModel.SetSigmaX(egomotionTracker->sigmaX);
    m_trackingLocationStateUncertaintyModel.SetSigmaY(egomotionTracker->sigmaY);
    m_trackingLocationStateUncertaintyModel.SetSigmaAlpha(Conversions::DegreeToRadians(egomotionTracker->sigmaAlpha));
    m_trackingLocationStatePredictor.SetEnabledRobotControlStateUncertainty(egomotionTracker->robotControlStateUncertaintyEnabled);
    m_trackingLocationStatePredictor.SetEnabledLocationStateUncertainty(egomotionTracker->locationStateUncertaintyEnabled);
}

void CTrackingDispatchingThread::UpdateNeckErrorLimits()
{
    m_neckErrorUncertaintyModel.SetErrorLimits(Conversions::DegreeToRadians(egomotionTracker->trackingNeckErrorRollMin), Conversions::DegreeToRadians(egomotionTracker->trackingNeckErrorRollMax),
            Conversions::DegreeToRadians(egomotionTracker->trackingNeckErrorPitchMin), Conversions::DegreeToRadians(egomotionTracker->trackingNeckErrorPitchMax),
            Conversions::DegreeToRadians(egomotionTracker->trackingNeckErrorYawMin), Conversions::DegreeToRadians(egomotionTracker->trackingNeckErrorYawMax));
}

void CTrackingDispatchingThread::UpdateUseNormalOrientationDifferenceMeasureTracking()
{
    m_trackingObservationModel.SetUseNormalOrientationDifferenceMeasure(egomotionTracker->useNormalOrientationDifferenceMeasureTracking);
}

void CTrackingDispatchingThread::UpdateUseNormalOrientationDifferenceMeasureInitialisation()
{
    m_initialisationObservationModel.SetUseNormalOrientationDifferenceMeasure(egomotionTracker->useNormalOrientationDifferenceMeasureInitialisation);
}

void CTrackingDispatchingThread::UpdateUsePSOptimizationTracking()
{
    m_pTrackingControl->GetTrackingPF()->SetSwarmOptimization(egomotionTracker->swarmOptimizationEnabled, egomotionTracker->velocityFactorLocalBest, egomotionTracker->velocityFactorGlobalBest,
            egomotionTracker->localBestAgingFactor, egomotionTracker->bestWeightTresholdSwarmOptimization);
}

void CTrackingDispatchingThread::updateUseMahalanobisDistanceBasedScatteringReduction()
{
    m_pTrackingControl->GetTrackingPF()->setUseMahalanobisDistanceBasedScatteringReduction(egomotionTracker->useMahalanobisDistanceBasedScatteringReduction);
}

void CTrackingDispatchingThread::UpdateCycleTrackingStatusEstimation()
{
    m_pTrackingControl->setCycleTrackingStatusEstimation(egomotionTracker->m_cycleTrackingStatusEstimation);
}


