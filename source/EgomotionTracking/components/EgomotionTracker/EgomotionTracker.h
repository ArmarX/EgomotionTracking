/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    EgomotionTracker::components::EgomotionTracker
 * @author     Thomas von der Heyde ( tvh242 at hotmail dot com )
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

/* VisionX includes. */

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/interface/core/ImageProviderInterface.h>

/* ArmarXCore includes. */

#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/interface/observers/PlatformUnitObserverInterface.h>
#include <RobotAPI/interface/observers/KinematicUnitObserverInterface.h>

/* MemoryX includes. */

#include <MemoryX/interface/components/WorkingMemoryInterface.h>

/* STL includes. */

#include <string>
#include <vector>

/* EgomotionTracking includes. */

#include <EgomotionTracking/components/EgomotionTracking/Misc/RobotKinematic/SimpleArmar3RobotKinematic.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Profiling.h>

/* EVP includes. */

#include <Calibration/StereoCalibration.h>
#include <Image/ByteImage.h>

/* EgomotionTracker includes. */

#include <VisionX/interface/components/EgomotionTrackerInterface.h>

#include "TrackingDispatchingThread.h"
#include "RobotControl.h"

namespace armarx
{
    class RobotControl;
    class CTrackingDispatchingThread;

    class EgomotionTrackerProcessorPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        EgomotionTrackerProcessorPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<int>("numPSLocationEstimatorParticles", 1000, "Number of particles used to initialise the position of the robot.");
            defineOptionalProperty<int>("maxTrackingParticles", 200, "Maximum number of particles used to track the position of the robot.");

            defineOptionalProperty<int>("dynamicElementMinParticles", 50, "Minimum number of particles used to estimate the state of a dynamic element");
            defineOptionalProperty<int>("dynamicElementMaxParticles", 200, "Maximum number of particles used to estimate the state of a dynamic element");
            defineOptionalProperty<bool>("useDynamicElementParticlesFunctionOfTransformationRange", false, "If TRUE, the number of particles used to estimate the state of a dynamic element depends on the range of the element's transformation.");
            defineOptionalProperty<int>("dynamicElementParticlesRatio", 5, "Ratio of a dynamic element transformation's range and the number of particles used to estimate it's state. For example: the state of a translating element with a range of 1000mm and a ratio of 5 will be estimated using 200 particles.");
            defineOptionalProperty<int>("dynamicElementNumParticles", 100, "Number of particles used to estimate the state of a dynamic element. This number will be used if useDynamicElementParticlesFunctionOfTransformationRange is set to FALSE.");

            defineOptionalProperty<double>("dynamicElementParticleReinitializationImportanceWeightTreshold", 0.7, "");
            defineOptionalProperty<int>("dynamicElementParticleReinitializationIterationCountTreshold", 10, "Iteration count treshold for the reinitialization of DynamicElementParticles.");

            defineOptionalProperty<double>("dynamicElementStateEstimateAcceptabilityTreshold", 0.95, "Importance weight treshold for the state estimate of a DynamicElement to be accepted.");

            defineRequiredProperty<std::string>("environmentModelFilePath", "Path of the CAD model file of the environment.");
            defineRequiredProperty<std::string>("dissimilarityEnvironmentModelFilePath", "Path of the CAD model file used for the dissimilarity lihelihood.");

            defineOptionalProperty<bool>("useDissimilarityLikelihood", false, "If TRUE, additionally uses the dissimilarity likelihood to estimate the state of dynamic elements in the environment.");

            defineOptionalProperty<std::string>("imageProviderProxyName", "ImageSequenceProvider", "Name of the image provider.");
            defineOptionalProperty<std::string>("robotStateProxyName", "RobotStateComponent", "Name of the robot state component.");
            defineOptionalProperty<std::string>("kinematicUnitProxyName", "Armar3KinematicUnit", "Name of the kinematic unit.");
            defineOptionalProperty<std::string>("platformUnitObserverProxyName", "PlatformUnitObserver", "Name of the platform unit observer.");
            defineOptionalProperty<std::string>("workingMemoryProxyName", "WorkingMemory", "Name of the working memory.");
            defineOptionalProperty<std::string>("kinematicUnitObserverProxyName", "KinematicUnitObserver", "Name of the kinematic unit observer.");

            defineOptionalProperty<std::string>("jointAnglesChannelName", "jointangles", "Name of the data channel providing the joint angles.");
            defineOptionalProperty<std::string>("platformVelocityChannelName", "platformVelocity", "Name of the data channel providing the platform velocity");

            defineOptionalProperty<std::string>("leftCameraNodeName", "EyeLeftCamera", "Name of the node of the left camera");
            defineOptionalProperty<std::string>("rightCameraNodeName", "EyeRightCamera", "Name of the node of the right camera");
            defineOptionalProperty<std::string>("headBaseNodeName", "Head Base", "Name of the node of the head base");

            defineOptionalProperty<std::string>("platformVelocityXDataFieldName", "velocityX", "Name of the data field providing the platform X velocity");
            defineOptionalProperty<std::string>("platformVelocityYDataFieldName", "velocityY", "Name of the data field providing the platform Y velocity");
            defineOptionalProperty<std::string>("platformVelocityRotationDataFieldName", "velocityRotation", "Name of the data field providing the platform rotation velocity");

            defineOptionalProperty<std::string>("neck1NodeName", "Neck_1_Pitch", "Name of the first neck node. Used to calculate the transformation matrix from the root to the camera node.");
            defineOptionalProperty<std::string>("neck2NodeName", "Neck_2_Roll", "Name of the first neck node. Used to calculate the transformation matrix from the root to the camera node.");
            defineOptionalProperty<std::string>("neck3NodeName", "Neck_3_Yaw", "Name of the third neck node. Used to calculate the transformation matrix from the root to the camera node.");

            defineOptionalProperty<std::string>("neckRollNodeName", "Neck_2_Roll", "Name of the data field providing the neck roll. Also the name of the node corresponding to the neck roll.");
            defineOptionalProperty<std::string>("neckPitchNodeName", "Neck_1_Pitch", "Name of the data field providing the neck pitch. Also the name of the node corresponding to the neck pitch.");
            defineOptionalProperty<std::string>("neckYawNodeName", "Neck_3_Yaw", "Name of the data field providing the neck yaw. Also the name of the node corresponding to the neck yaw.");

            defineOptionalProperty<std::string>("agentInstanceName", "armar3b", "Name of the agent instance in the working memory. The instance with this name will be created if it does not yet exist, and it's position and orientation will be updated by the self-localization process.");

            defineOptionalProperty<float>("maximumTrackingExecutionTime", 120, "Maximum desired length of a tracking execution cycle in ms. The number of active particles will be adapted to respect this value.");

            /* Configuration */

            defineOptionalProperty<bool>("headPlannerEnabled", false, "Enable use of the head planner.")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);

            /* Renderer configuration */

            defineOptionalProperty<float>("minScreenLineLength", 30.0, "Minimum line length. If a lines length is inferior to this value, it will not be rendered.");
            defineOptionalProperty<float>("minSegmentsPerJunction", 2.0, "Minimum number of segments per junction. If a junctions number of segments is inferior to this value, it will not be rendered.");
            defineOptionalProperty<float>("maxJunctionPointToSegmentDeviation", 3.0, "Maximum segment-junction point deviation. If the distance between a segment and a junction point is greater than this value, the segment will not be considering as belonging to the junction point.");
            defineOptionalProperty<float>("junctionSegmentLength", 25.0, "Minim junction segment length. If a junction segments length is inferior to this value, it will not be rendered.");
            defineOptionalProperty<float>("maxJunctionLinesDotProduct", 0.95, "Maximum dot product between two segments of a same junction point for which they are still considered separate segments");

            /* Edge processor configuration */

            defineOptionalProperty<int>("tresholdCannyLow", 40, "Sets the lower Canny treshold");
            defineOptionalProperty<int>("tresholdCannyHigh", 60, "Sets the higher Canny treshold");

            /* Observation models configuration */

            defineOptionalProperty<CJunctionPointLineModelObservationModel::CSamplingMode>("samplingModeTracking", CJunctionPointLineModelObservationModel::eSampleAllLines, "Sets the sampling mode for the tracking observation model")
            .map("eSampleAllLines", CJunctionPointLineModelObservationModel::eSampleAllLines)
            .map("eSampleJunctionPoints", CJunctionPointLineModelObservationModel::eSampleJunctionPoints);
            defineOptionalProperty<CJunctionPointLineModelObservationModel::CGaussianWeightingMode>("gaussianWeightingModeTracking", CJunctionPointLineModelObservationModel::eWeightAll, "Sets the gaussian weighting mode for the tracking observation model")
            .map("eWeightAll", CJunctionPointLineModelObservationModel::eWeightAll)
            .map("eSmoothedHistogramWeighting", CJunctionPointLineModelObservationModel::eSmoothedHistogramWeighting);
            defineOptionalProperty<CJunctionPointLineModelObservationModel::CNumberOfNormalsMode>("numberOfNormalsModeTracking", CJunctionPointLineModelObservationModel::eLengthDependendNumberOfNormals, "Sets the number of normals mode for the tracking observaton model")
            .map("eLengthDependendNumberOfNormals", CJunctionPointLineModelObservationModel::eLengthDependendNumberOfNormals)
            .map("eFixedNumberOfNormals", CJunctionPointLineModelObservationModel::eFixedNumberOfNormals);
            defineOptionalProperty<int>("numberOfNormalsTracking", 5, "If number of normals mode = fixed, sets the number of normals - else, sets the number of normals per 100px");
            defineOptionalProperty<int>("minNumberOfVisibleModelElementsTracking", 5, "Sets the minimum required number of visible elements (junctions/lines)");
            defineOptionalProperty<float>("deltaAlphaTracking", 2.0, "Tracking delta alpha value used to determine the value of sigma used to calculate a particles weight.");
            defineOptionalProperty<float>("desiredPMinTracking", 0.001, "Tracking PMin value used to determine the value of sigma used to calculate a particles weight.");
            defineOptionalProperty<bool>("useNormalOrientationDifferenceMeasureTracking", false, "Sets whether to use the normal orientation difference as a measure in the observation likelihood function during the tracking phase");
            defineOptionalProperty<float>("normalOrientationDifferenceScalarTracking", 1.0, "Scalar multiplied with the normal orientation difference to controls its importance.");
            defineOptionalProperty<float>("normalOrientationDifferenceFactorTracking", 1.0, "Factor of the normal orientation difference measure used to control its importance.");
            defineOptionalProperty<CJunctionPointLineModelObservationModel::CSamplingMode>("samplingModeInitialisation", CJunctionPointLineModelObservationModel::eSampleAllLines, "Sets the sampling mode for the initialisation observation model")
            .map("eSampleAllLines", CJunctionPointLineModelObservationModel::eSampleAllLines)
            .map("eSampleJunctionPoints", CJunctionPointLineModelObservationModel::eSampleJunctionPoints);
            defineOptionalProperty<CJunctionPointLineModelObservationModel::CGaussianWeightingMode>("gaussianWeightingModeInitialisation", CJunctionPointLineModelObservationModel::eWeightAll, "Sets the gaussian weighting mode for the initialisation observation model")
            .map("eWeightAll", CJunctionPointLineModelObservationModel::eWeightAll)
            .map("eSmoothedHistogramWeighting", CJunctionPointLineModelObservationModel::eSmoothedHistogramWeighting);
            defineOptionalProperty<CJunctionPointLineModelObservationModel::CNumberOfNormalsMode>("numberOfNormalsModeInitialisation", CJunctionPointLineModelObservationModel::eLengthDependendNumberOfNormals, "Sets the number of normals mode for the initialisation observaton model")
            .map("eLengthDependendNumberOfNormals", CJunctionPointLineModelObservationModel::eLengthDependendNumberOfNormals)
            .map("eFixedNumberOfNormals", CJunctionPointLineModelObservationModel::eFixedNumberOfNormals);
            defineOptionalProperty<int>("numberOfNormalsInitialisation", 5, "If number of normals mode = fixed, sets the number of normals - else, sets the number of normals per 100px");
            defineOptionalProperty<int>("minNumberOfVisibleModelElementsInitialisation", 20, "Sets the minimum required number of visible elements (junctions/lines)");
            defineOptionalProperty<float>("deltaAlphaInitialisation", 4.0, "Initialisation PMin value used to determine the value of sigma used to calculate a particles weight.");
            defineOptionalProperty<float>("desiredPMinInitialisation", 0.001, "Initialisation PMin used to determine the value of sigma used to calculate a particles weight.");
            defineOptionalProperty<bool>("useNormalOrientationDifferenceMeasureInitialisation", false, "Sets whether to use the normal orientation difference as a measure in the observation likelihood function during the tracking phase");
            defineOptionalProperty<float>("normalOrientationDifferenceScalarInitialisation", 1.0, "Scalar multiplied with the normal orientation difference to controls its importance.");
            defineOptionalProperty<float>("normalOrientationDifferenceFactorInitialisation", 1.0, "Factor of the normal orientation difference measure used to control its importance.");

            /* Tracking configuration */

            defineOptionalProperty<int>("agingCoefficientLocationState", 30, "Sets the aging coefficient for the location state");
            defineOptionalProperty<float>("minScatteringFactorLocationState", 0.2, "Sets the minimum scattering factor for the location state");
            defineOptionalProperty<float>("maxScatteringFactorLocationState", 1.0, "Sets the maximum scattering factor for the location state");
            defineOptionalProperty<int>("agingCoefficientNeckError", 15, "Sets the aging coefficient for the neck error");
            defineOptionalProperty<float>("minScatteringFactorNeckError", 0.2, "Sets the minimum scattering factor for the neck error");
            defineOptionalProperty<float>("maxScatteringFactorNeckError", 1.0, "Sets the maximum scattering factor for the neck error");
            defineOptionalProperty<bool>("swarmOptimizationEnabled", false, "Enables swarm optimization for the tracking phase")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<float>("velocityFactorLocalBest", 0.2, "Sets the velocity factor for the local best");
            defineOptionalProperty<float>("velocityFactorGlobalBest", 0.05, "Sets the velocity factor for the global best");
            defineOptionalProperty<float>("localBestAgingFactor", 0.9, "Set the aging factor for the local best");
            defineOptionalProperty<float>("bestWeightTresholdSwarmOptimization", 0.0, "If swarm optimization is enabled, a swarm optimization cycle will only be computed if one of the particles has a weight superior to this value.");
            defineOptionalProperty<bool>("annealingEnabled", true, "Enables annealing for the tracking phase")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<int>("numberOfAnnealingLevels", 2, "Sets the number of annealing levels");
            defineOptionalProperty<float>("annealingFactor", 0.5, "Sets the annealing factor");
            defineOptionalProperty<float>("bestWeightTresholdAnnealing", 0.2, "If annealing is enabled, sets the annealing level to 0 if the weight of all particles is inferior to this value, which corresponds to disabling annealing.");
            defineOptionalProperty<bool>("conservativeUpdate", false, "If conservative update is set to TRUE and annealing is enabled, the location estimate will only be updated after going through all annealing levels. Else, the location estimate will be update after each annealing cycle regardless of wich annealing level is being computed.")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<bool>("resamplingTresholdEnabled", true, "If resampling treshold is enabled, the particles will only be resampled if the relative effective particle number falls under a certain treshold.")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<float>("relativeNEffTreshold", 0.9, "NEff treshold. If resampling treshold is enabled, the particles will only be resampled if the relative effective particle number falls below this value.");
            defineOptionalProperty<CNeckErrorUncertaintyModel::UncertaintyMode>("neckErrorUncertaintyMode", CNeckErrorUncertaintyModel::eRelativeGaussianNeckUncertainty, "Sets the neck error uncertainty mode")
            .map("eNoNeckErrorUncertainty", CNeckErrorUncertaintyModel::eNoNeckErrorUncertainty)
            .map("eRelativeGaussianNeckUncertainty", CNeckErrorUncertaintyModel::eRelativeGaussianNeckUncertainty)
            .map("eAbsoluteUniformNeckUncertainty", CNeckErrorUncertaintyModel::eAbsoluteUniformNeckUncertainty);
            defineOptionalProperty<float>("sigmaRoll", 1.0, "Sets sigma for the neck roll error uncertainty");
            defineOptionalProperty<float>("sigmaPitch", 1.0, "Sets sigma for the neck pitch error uncertainty");
            defineOptionalProperty<float>("sigmaYaw", 1.0, "Sets sigma for the neck yaw error uncertainty");
            defineOptionalProperty<bool>("scatteringReductionEnabled", true, "Enables scattering reduction when the robot is immobile")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<CTrackingControl::LineRegistrationVisualizationMode>("lineRegistrationVisualizationMode", CTrackingControl::eDrawLinesInInputImage, "Sets the line registration visualization mode")
            .map("eDrawLinesInEdgeImage", CTrackingControl::eDrawLinesInEdgeImage)
            .map("eDrawLinesInInputImage", CTrackingControl::eDrawLinesInInputImage);
            defineOptionalProperty<float>("trackingNeckErrorRollMin", -5.0, "Minimum roll neck error for the tracking phase neck error uncertainty model");
            defineOptionalProperty<float>("trackingNeckErrorRollMax", 5.0, "Maximum roll neck error for the tracking phase neck error uncertainty model");
            defineOptionalProperty<float>("trackingNeckErrorPitchMin", -10.0, "Minimum pitch neck error for the tracking phase neck error uncertainty model");
            defineOptionalProperty<float>("trackingNeckErrorPitchMax", 10.0, "Maximum pitch neck error for the tracking phase neck error uncertainty model");
            defineOptionalProperty<float>("trackingNeckErrorYawMin", -1.0, "Minimum yaw neck error for the tracking phase neck error uncertainty model");
            defineOptionalProperty<float>("trackingNeckErrorYawMax", 1.0, "Maximum yaw neck error for the tracking phase neck error uncertainty model");
            defineOptionalProperty<bool>("useMahalanobisDistanceBasedScatteringReduction", true, "True if particle scattering should be reduced by calculating the particles Mahalanobis distance to the estimated location");
            defineOptionalProperty<int>("counterTreshold", 10, "The number of iterations the determinant of the scattering matrix has to be below the treshold before Mahalanobis distance based scattering reduction activates if it's enabled.");
            defineOptionalProperty<CEgomotionTrackingPFExecutionUnit::MahalanobisDistanceBasedScatteringReductionResamplingMode>("mahalanobisDistanceBasedScatteringReductionResamplingMode", CEgomotionTrackingPFExecutionUnit::eStandardResampling, "Resampling mode used when Mahalanobis distance based scattering reduction is enabled.")
            .map("eStandardResampling", CEgomotionTrackingPFExecutionUnit::eStandardResampling)
            .map("eEigenvectorEigenvalueBasedResampling", CEgomotionTrackingPFExecutionUnit::eEigenvectorEigenvalueBasedResampling)
            .map("eLatticeSurfaceResampling", CEgomotionTrackingPFExecutionUnit::eLatticeSurfaceResampling);
            defineOptionalProperty<float>("totalWeightPercentageTreshold", 0.7, "Percentage of total particle weight used to calculate the resampable particles if Mahalanobis distance based scattering reduction is enabled.");

            /* Location estimator configuration */

            defineOptionalProperty<float>("inertiaFactor", 0.9, "Sets the inertia factor");
            defineOptionalProperty<float>("cognitionFactor", 0.6, "Sets the cognition factor");
            defineOptionalProperty<float>("socialFactor", 0.7, "Sets the social factor");
            defineOptionalProperty<float>("initialisationRegionMinX", 1000, "Min X of the location estimator initialisation region");
            defineOptionalProperty<float>("initialisationRegionMaxX", 3000, "Max X of the location estimator initialisation region");
            defineOptionalProperty<float>("initialisationRegionMinY", 0, "Min Y of the location estimator initialisation region");
            defineOptionalProperty<float>("initialisationRegionMaxY", 4500, "Max Y of the location estimator initialisation region");
            defineOptionalProperty<float>("initialisationRegionMinAlpha", 0, "Min Alpha of the location estimator initialisation region");
            defineOptionalProperty<float>("initialisationRegionMaxAlpha", 90, "Max Alpha of the location estimator initialisation region");

            /* Head planner configuration */

            defineOptionalProperty<float>("headPlannerRollMin", -19.0, "Sets the minimum neck roll angle for the head planner");
            defineOptionalProperty<float>("headPlannerRollMax", 19.0, "Sets the maximum nech roll angle for the head planner");
            defineOptionalProperty<float>("headPlannerPitchMin", -40.0, "Sets the minimum neck pitch angle for the head planner");
            defineOptionalProperty<float>("headPlannerPitchMax", 31.0, "Sets the maximum neck pitch angle for the head planner");
            defineOptionalProperty<float>("headPlannerYawMin", -90.0, "Sets the minimum neck yaw angle for the head planner");
            defineOptionalProperty<float>("headPlannerYawMax", 90.0, "Sets the maximum neck yaw angle for the head planner");
            defineOptionalProperty<int>("rollSamplesCount", 3, "Sets the number of neck roll samples for the head planner");
            defineOptionalProperty<int>("pitchSamplesCount", 5, "Sets the number of neck pitch samples for the head planner");
            defineOptionalProperty<int>("yawSamplesCount", 7, "Sets the number of neck yaw samples for the head planner");
            defineOptionalProperty<bool>("movingHeadAllowed", true, "Enables moving the head")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<float>("configurationMass", 0.2, "Sets the configuration mass");
            defineOptionalProperty<float>("dampingCoefficient", 0.8, "Sets the damping coefficient");
            defineOptionalProperty<float>("targetFadingCoefficient", 0.05, "Sets the target fading coefficient");
            defineOptionalProperty<CMassSpringHeadConfigurationPlanner::SpringCoefficientMode>("springCoefficientMode", CMassSpringHeadConfigurationPlanner::eTargetFadingMode, "Sets the spring coefficient mode")
            .map("eManualCoefficientSelection", CMassSpringHeadConfigurationPlanner::eManualCoefficientSelection)
            .map("eTargetFadingMode", CMassSpringHeadConfigurationPlanner::eTargetFadingMode);
            defineOptionalProperty<float>("moveToMaxRelevanceFactorTreshold", 0.2, "");
            defineOptionalProperty<bool>("useMoveToMaxHeuristic", true, "")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);

            /* Prediction models configuration */

            defineOptionalProperty<float>("relativeSystematicErrorVelocityX", 0.0, "Relative systematic error factor for the robots X velocity.");
            defineOptionalProperty<float>("relativeSystematicErrorVelocityY", 0.0, "Relative systematic error factor for the robots Y velocity.");
            defineOptionalProperty<float>("relativeSystematicErrorVelocityAlpha", 0.0, "Relative systematic error factor for the robots alpha velocity.");
            defineOptionalProperty<float>("absoluteSystematicPlatformAxesTiltError", 0.0, "Absolute systematic error for the robot platforms axes tilt error.");
            defineOptionalProperty<float>("sigmaVelocityX", 30.0, "Sigma value for the robots X velocity value uncertainty.");
            defineOptionalProperty<float>("sigmaVelocityY", 30.0, "Sigma value for the robots Y velocity value uncertainty.");
            defineOptionalProperty<float>("sigmaVelocityAlpha", 30.0, "Sigma value for the robots alpha velocity value uncertainty.");
            defineOptionalProperty<float>("sigmaPlatformAxesTiltError", 2.0, "Sigma value for the robot platforms tilt error value uncertainty.");
            defineOptionalProperty<float>("sigmaX", 10.0, "Sigma value for the robots X value uncertainty.");
            defineOptionalProperty<float>("sigmaY", 10.0, "Sigma value for the robots Y value uncertainty.");
            defineOptionalProperty<float>("sigmaAlpha", 2.0, "Sigma value for the robots alpha value uncertainty.");
            defineOptionalProperty<bool>("robotControlStateUncertaintyEnabled", true, "Enables adding an uncertainty to the robot control state values.")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<bool>("locationStateUncertaintyEnabled", true, "Enables adding an uncertainty to the location state values.")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);

            /* Dynamic element status estimation configuration. */

            defineOptionalProperty<bool>("dynamicElementswarmOptimizationEnabled", false, "Enables swarm optimization for the dynamic element status estimation phase")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<float>("dynamicElementVelocityFactorLocalBest", 0.2, "Sets the velocity factor for the local best");
            defineOptionalProperty<float>("dynamicElementVelocityFactorGlobalBest", 0.05, "Sets the velocity factor for the global best");
            defineOptionalProperty<float>("dynamicElementLocalBestAgingFactor", 0.9, "Set the aging factor for the local best");
            defineOptionalProperty<float>("dynamicElementBestWeightTresholdSwarmOptimization", 0.0, "If swarm optimization is enabled, a swarm optimization cycle will only be computed if one of the particles has a weight superior to this value.");
            defineOptionalProperty<bool>("dynamicElementAnnealingEnabled", false, "Enables annealing for the dynamic element status estimation phase")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<int>("dynamicElementNumberOfAnnealingLevels", 2, "Sets the number of annealing levels");
            defineOptionalProperty<float>("dynamicElementAnnealingFactor", 0.5, "Sets the annealing factor");
            defineOptionalProperty<float>("dynamicElementBestWeightTresholdAnnealing", 0.2, "If annealing is enabled, sets the annealing level to 0 if the weight of all particles is inferior to this value, which corresponds to disabling annealing.");
            defineOptionalProperty<bool>("dynamicElementConservativeUpdate", false, "If conservative update is set to TRUE and annealing is enabled, the location estimate will only be updated after going through all annealing levels. Else, the location estimate will be update after each annealing cycle regardless of wich annealing level is being computed.")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<bool>("dynamicElementResamplingTresholdEnabled", false, "If resampling treshold is enabled, the dynamic element particles will only be resampled if the relative effective particle number falls under a certain treshold.")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<float>("dynamicElementRelativeNEffTreshold", 0.9, "NEff treshold. If resampling treshold is enabled, the particles will only be resampled if the relative effective particle number falls below this value.");

            defineOptionalProperty<DynamicElementStatusUncertaintyModel::UncertaintyMode>("dynamicElementUncertaintyMode", DynamicElementStatusUncertaintyModel::relativeGaussianUncertainty, "Sets the dynamic element uncertainty mode")
            .map("eDynamicElementNoUncertainty", DynamicElementStatusUncertaintyModel::noUncertainty)
            .map("eDynamicElementRelativeGaussianUncertainty", DynamicElementStatusUncertaintyModel::relativeGaussianUncertainty)
            .map("eDynamicElementAbsoluteUniformUncertainty", DynamicElementStatusUncertaintyModel::absoluteUniformUncertainty);
            defineOptionalProperty<float>("dynamicElementSigmaAlpha", 0.01, "Sets sigma for the dynamic element uncertainty");
        }
    };

    class EgomotionTracker:
        virtual public visionx::ImageProcessor,
        virtual public EgomotionTrackerInterface
    {
        /* CTrackingDispatchingThread and CRobotControl are friends so that both have access to the configuration parameters. */
        friend class CTrackingDispatchingThread;
        friend class RobotControl;

    public:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new EgomotionTrackerProcessorPropertyDefinitions(getConfigIdentifier()));
        }

        /* Inherited from ImageProcessor. */

        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onDisconnectImageProcessor() override;
        void onExitImageProcessor() override;
        void process() override;

        std::string getDefaultName() const override
        {
            return "EgomotionTracker";
        }

        /* EgomotionTrackerInterface implementations. */

        /**
         * Starts the self-localization process. The switch from position initialization to tracking is done automatically, based on a treshold. Tracking and environmental object status estimation
         * is done sequentially.
         */
        void startSelfLocalisationProcess(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Stops the self-localization process.
         */
        void stopSelfLocalisationProcess(const Ice::Current& c = Ice::emptyCurrent) override; // Useless. Remove this from the final version.
        /**
         * Sets the neck error limits.
         *
         * @param rollMin The minimum neck roll error.
         * @param rollMax The maximum neck roll error.
         * @param pitchMin The minimum neck roll error.
         * @param pitchMax The maximum neck roll error.
         * @param yawMin The minimum neck yaw error.
         * @param yawMax The maximum neck yaw error.
         */
        void setNeckErrorLimits(Ice::Int rollMin, Ice::Int rollMax, Ice::Int pitchMin, Ice::Int pitchMax, Ice::Int yawMin, Ice::Int yawMax, const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns the current location estimate.
         *
         * @return The current location estimate.
         */
        LocationEstimate getLocationEstimate(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns the location estimator's plane scattering directions.
         *
         * @return The location estimator's plane scattering directions.
         */
        PlaneScatteringDirections getPlaneScatteringDirectionsPS(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns the tracking filter's plane scattering directions.
         *
         * @return The tracking filter's plane scattering directions.
         */
        PlaneScatteringDirections getPlaneScatteringDirectionsPF(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns the particles used by the location estimator.
         *
         * @return The particles used by the location estimator.
         */
        ParticleVector getParticlesPS(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns the particles used by the tracking filter.
         *
         * @return The particles used by the tracking filter.
         */
        ParticleVector getParticlesPF(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns the bounding box in the 2D plane of the geometric elements of the model of the environment.
         *
         * @return The bounding box in the 2D plane of the geometric elements of the model of the environment.
         */
        BoundingBox getBoundingBox(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns the number of active particles of the tracking filter.
         *
         * @return The number of active particles of the tracking filter.
         */
        int getNumberOfActiveParticles(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns the status of the EgomotionTracker component. This can be either: eStopped when the component is not doing anything, ePositionInitialisation, eTracking or eStatusEstimate. If
         * cycling between tracking and environmental object status estimate is active, the status returned corresponds to the status that would be active if cycling were de-activated.
         *
         * @return The status of the EgomotionTracker component.
         */
        EgomotionTrackerStatus getStatus(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Sets whether edge orientation difference should be used as a cue to calculate the importance weight of a particle during the tracking phase.
         *
         * @param useOrientationDifferenceTracking TRUE if edge orientation difference should be used as a cue to calculate the importance weight of a particle during the tracking phase.
         */
        void setUseOrientationDifferenceTracking(bool useOrientationDifferenceTracking, const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns whether edge orientation difference is used as a cue to calculate the importance weight of a particle during the tracking phase.
         *
         * @return TRUE if edge orientation difference is used as a cue to calculate the importance weight of a particle during the tracking phase.
         */
        bool getUseOrientationDifferenceTracking(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Sets whether edge orientation difference should be used as a cue to calculate the importance weight of a particle during the position initialisation phase.
         *
         * @param useOrientationDifferenceTracking TRUE if edge orientation difference should be used as a cue to calculate the importance weight of a particle during the position initialisation phase.
         */
        void setUseOrientationDifferenceInitialisation(bool useOrientationDifferenceInitialisation, const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns whether edge orientation difference is used as a cue to calculate the importance weight of a particle during the position initialisation phase.
         *
         * @return TRUE if edge orientation difference is used as a cue to calculate the importance weight of a particle during the position initialisation phase.
         */
        bool getUseOrientationDifferenceInitialisation(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Sets whether particle swarm optimization should be used during the tracking phase.
         *
         * @param usePSOptimizationTracking TRUE if particle swarm optimization should be used during the tracking phase.
         */
        void setUsePSOptimizationTracking(bool usePSOptimizationTracking, const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns whether particle swarm optimization is used during the tracking phase.
         *
         * @return TRUE if particle swarm optimization is used during the tracking phase.
         */
        bool getUsePSOptimizationTracking(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Sets whether particle scattering reduction based on the Mahalanobis distance of particles should be used during the tracking phase.
         *
         * @param useMahalanobisDistanceBasedScatteringReduction TRUE if particle scattering reduction based on the Mahalanobis distance of particles should be used during the tracking phase.
         */
        void setUseMahalanobisDistanceBasedScatteringReduction(bool useMahalanobisDistanceBasedScatteringReduction, const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns whether particle scattering reduction based on the Mahalanobis distance of particles is used during the tracking phase.
         *
         * @return TRUE if particle scattering reduction based on the Mahalanobis distance of particles is used during the tracking phase.
         */
        bool getUseMahalanobisDistanceBasedScatteringReduction(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns the minimum neck roll error.
         *
         * @return The minimum neck roll error.
         */
        float getNeckErrorRollMin(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns the maximum neck roll error.
         *
         * @return The maximum neck roll error.
         */
        float getNeckErrorRollMax(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns the minimum neck pitch error.
         *
         * @return The minimum neck pitch error.
         */
        float getNeckErrorPitchMin(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns the maximum neck pitch error.
         *
         * @return The maximum neck pitch error.
         */
        float getNeckErrorPitchMax(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns the minimum neck yaw error.
         *
         * @return The minimum neck yaw error.
         */
        float getNeckErrorYawMin(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Returns the maximum neck yaw error.
         *
         * @return The maximum neck yaw error.
         */
        float getNeckErrorYawMax(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Starts the position initialisation phase.
         */
        void beginLocalisation(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Starts the tracking phase.
         */
        void beginTracking(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Starts the environmental object state estimation phase.
         */
        void beginStatusEstimation(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Executes a single cycle of the self-localisation process.
         */
        void step(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Sets whether execution should be automatic, meaning: whenever a cycle is done, the next cycle is executed.
         *
         * @param automaticExecution TRUE if execution should be automatic.
         */
        void setAutomaticExecution(bool automaticExecution, const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * Sets whether tracking and environmental object state estimation iterations should be executed sequentially.
         *
         * @param cycleTrackingStatusEstimation TRUE if tracking and environmental object state estimation iterations should be executed sequentially.
         */
        void setCycleTrackingStatusEstimation(bool cycleTrackingStatusEstimation, const Ice::Current& c = Ice::emptyCurrent) override;

    protected:

        void loadConfiguration();

        std::string imageProviderProxyName;
        std::string robotStateProxyName;
        std::string kinematicUnitProxyName;
        std::string platformUnitObserverProxyName;
        std::string workingMemoryProxyName;
        std::string kinematicUnitObserverProxyName;

        std::string jointAnglesChannelName;
        std::string neckRollNodeName;
        std::string neckPitchNodeName;
        std::string neckYawNodeName;

        std::string platformVelocityChannelName;
        std::string platformVelocityXDataFieldName;
        std::string platformVelocityYDataFieldName;
        std::string platformVelocityRotationDataFieldName;

        std::string leftCameraNodeName;
        std::string rightCameraNodeName;
        std::string headBaseNodeName;

        std::string m_neck1NodeName;
        std::string m_neck2NodeName;
        std::string m_neck3NodeName;

        std::string agentInstanceName;

        visionx::ImageProviderInterfacePrx imageProviderProxy;
        RobotStateComponentInterfacePrx robotStateProxy;
        KinematicUnitInterfacePrx kinematicUnitProxy;
        PlatformUnitObserverInterfacePrx platformUnitObserverProxy;
        KinematicUnitObserverInterfacePrx kinematicUnitObserverProxy;
        memoryx::WorkingMemoryInterfacePrx workingMemoryProxy;
        memoryx::AgentInstancesSegmentBasePrx agentInstancesSegment;

        CStereoCalibration* stereoCalibration;
        visionx::ImageFormatInfo imageFormat;
        bool imagesAreUndistorted;

        CStereoCalibrationGL m_stereoCalibrationGL;

        CTrackingDispatchingThread* trackingDispatchingThread;
        RobotControl* robotControl;

        CByteImage** robotImages; /*!< Input images. */
        CByteImage** resultImages; /*!< Result images. EgomotionTracker provides 2 result images: an input image coupled with the observation model and an edge image. */

        CProfiling workloadProfiler;

        EgomotionTrackerStatus status; /*!< Status of the EgomotionTracker component. */

        float maximumTrackingExecutionTime; /*!< Maximum time (in ms) each processing cycle should take when tracking the robot's position. The number of the tracking
                                                     filter's active particles is adapted to respect this duration. */

        /* Configuration */

        bool headPlannerEnabled;

        /* Renderer coniguration */
        float minScreenLineLength;
        float minSegmentsPerJunction;
        float maxJunctionPointToSegmentDeviation;
        float junctionSegmentLength;
        float maxJunctionLinesDotProduct;

        /* Edge processor configuration */
        int tresholdCannyLow;
        int tresholdCannyHigh;

        /* Observation models configuration */
        CJunctionPointLineModelObservationModel::CSamplingMode samplingModeTracking;
        CJunctionPointLineModelObservationModel::CGaussianWeightingMode gaussianWeightingModeTracking;
        CJunctionPointLineModelObservationModel::CNumberOfNormalsMode numberOfNormalsModeTracking;
        int numberOfNormalsTracking;
        int minNumberOfVisibleModelElementsTracking;
        float deltaAlphaTracking;
        float desiredPMinTracking;
        bool useNormalOrientationDifferenceMeasureTracking;
        float normalOrientationDifferenceScalarTracking;
        float normalOrientationDifferenceFactorTracking;
        CJunctionPointLineModelObservationModel::CSamplingMode samplingModeInitialisation;
        CJunctionPointLineModelObservationModel::CGaussianWeightingMode gaussianWeightingModeInitialisation;
        CJunctionPointLineModelObservationModel::CNumberOfNormalsMode numberOfNormalsModeInitialisation;
        int numberOfNormalsInitialisation;
        int minNumberOfVisibleModelElementsInitialisation;
        float deltaAlphaInitialisation;
        float desiredPMinInitialisation;
        bool useNormalOrientationDifferenceMeasureInitialisation;
        float normalOrientationDifferenceScalarInitialisation;
        float normalOrientationDifferenceFactorInitialisation;

        /* Tracking configuration */
        int agingCoefficientLocationState;
        float minScatteringFactorLocationState;
        float maxScatteringFactorLocationState;
        int agingCoefficientNeckError;
        float minScatteringFactorNeckError;
        float maxScatteringFactorNeckError;
        bool swarmOptimizationEnabled;
        float velocityFactorLocalBest;
        float velocityFactorGlobalBest;
        float localBestAgingFactor;
        float bestWeightTresholdSwarmOptimization;
        bool annealingEnabled;
        int numberOfAnnealingLevels;
        float annealingFactor;
        float bestWeightTresholdAnnealing;
        bool conservativeUpdate;
        bool resamplingTresholdEnabled;
        float relativeNEffTreshold;
        CNeckErrorUncertaintyModel::UncertaintyMode neckErrorUncertaintyMode;
        float sigmaRoll;
        float sigmaPitch;
        float sigmaYaw;
        bool scatteringReductionEnabled;
        CTrackingControl::LineRegistrationVisualizationMode lineRegistrationVisualizationMode;
        float trackingNeckErrorRollMin;
        float trackingNeckErrorRollMax;
        float trackingNeckErrorPitchMin;
        float trackingNeckErrorPitchMax;
        float trackingNeckErrorYawMin;
        float trackingNeckErrorYawMax;
        bool useMahalanobisDistanceBasedScatteringReduction;
        int counterTreshold;
        CEgomotionTrackingPFExecutionUnit::MahalanobisDistanceBasedScatteringReductionResamplingMode mahalanobisDistanceBasedScatterinReductionResamplingMode;
        float totalWeightPercentageTreshold;

        /* Location estimator configuration */
        float inertiaFactor;
        float cognitionFactor;
        float socialFactor;
        float initialisationRegionMinX;
        float initialisationRegionMaxX;
        float initialisationRegionMinY;
        float initialisationRegionMaxY;
        float initialisationRegionMinAlpha;
        float initialisationRegionMaxAlpha;

        /* Head planner configuration */
        float headPlannerRollMin;
        float headPlannerRollMax;
        float headPlannerPitchMin;
        float headPlannerPitchMax;
        float headPlannerYawMin;
        float headPlannerYawMax;
        int rollSamplesCount;
        int pitchSamplesCount;
        int yawSamplesCount;
        bool movingHeadAllowed;
        float configurationMass;
        float dampingCoefficient;
        float targetFadingCoefficient;
        CMassSpringHeadConfigurationPlanner::SpringCoefficientMode springCoefficientMode;
        float moveToMaxRelevanceFactorTreshold;
        bool useMoveToMaxHeuristic;

        /* Prediction models configuration */
        float relativeSystematicErrorVelocityX;
        float relativeSystematicErrorVelocityY;
        float relativeSystematicErrorVelocityAlpha;
        float absoluteSystematicPlatformAxesTiltError;
        float sigmaVelocityX;
        float sigmaVelocityY;
        float sigmaVelocityAlpha;
        float sigmaPlatformAxesTiltError;
        float sigmaX;
        float sigmaY;
        float sigmaAlpha;
        bool robotControlStateUncertaintyEnabled;
        bool locationStateUncertaintyEnabled;

        /* MANUAL */

        bool automaticSwitchToTrackingEnabled; /* Switch from global localization is done automatically if this is set to TRUE. */
        bool m_automaticExecution; /* Self-localization cycle execution is linked to the process function of the ImageProcessor class. */
        bool m_cycleTrackingStatusEstimation; /* Both tracking and status estimation are done sequentially in the same self-localization cycle. */

        std::string m_environmentModelFilePath;
        std::string m_dissimilarityEnvironmentModelFilePath;

        bool m_useDissimilarityLikelihood;

        double m_dynamicElementParticleReinitializationImportanceWeightTreshold;
        int m_dynamicElementParticleReinitializationIterationCountTreshold;

        double m_dynamicElementStateEstimateAcceptabilityTreshold;

        /* DynamicElementStatusEstimation Configuration. */

        int m_dynamicElementMinParticles;
        int m_dynamicElementMaxParticles;
        bool m_useDynamicElementParticlesFunctionOfTransformationRange;
        int m_dynamicElementParticlesRatio;
        int m_dynamicElementNumParticles;

        int m_numPSLocationEstimatorParticles;
        int m_maxTrackingParticles;

        bool m_dynamicElementswarmOptimizationEnabled;
        float m_dynamicElementVelocityFactorLocalBest;
        float m_dynamicElementVelocityFactorGlobalBest;
        float m_dynamicElementLocalBestAgingFactor;
        float m_dynamicElementBestWeightTresholdSwarmOptimization;

        bool m_dynamicElementAnnealingEnabled;
        int m_dynamicElementNumberOfAnnealingLevels;
        float m_dynamicElementAnnealingFactor;
        float m_dynamicElementBestWeightTresholdAnnealing;
        bool m_dynamicElementConservativeUpdate;

        bool m_dynamicElementResamplingTresholdEnabled;
        float m_dynamicElementRelativeNEffTreshold;

        DynamicElementStatusUncertaintyModel::UncertaintyMode m_dynamicElementUncertaintyMode;
        float m_dynamicElementSigmaAlpha;
    };
}

