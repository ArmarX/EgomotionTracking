/*
 * TrackingControl.h
 *
 *  Created on: 13.03.2013
 *      Author: abyte
 */

#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationRegion.h>
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotControlInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/LocationStatePredictor/LocationStatePredictorInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/ObservationModel/ObservationModelInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/NeckErrorUncertaintyModel/NeckErrorUncertaintyModel.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ImageProcessor/TrackingImageProcessorInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/RenderingUnit/RenderingUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ImageProcessor/DeviationImageProcessor.h>
#include <EgomotionTracking/components/EgomotionTracking/LocationEstimator/LocationEstimator.h>
#include <EgomotionTracking/components/EgomotionTracking/LocationEstimator/TrackingFilter/EgomotionTrackingPF/EgomotionTrackingPF.h>
#include <EgomotionTracking/components/EgomotionTracking/LocationEstimator/ParticleSwarmLocationEstimator/ParticleSwarmLocationEstimator.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Profiling.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/Process/EgomotionTrackingThreadPool.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/UncertaintyScatteringFactorCalculator/UncertaintyModelScatteringFactorCalculator.h>
#include <EgomotionTracking/components/EgomotionTracking/DynamicElementStatusEstimator/DynamicElementStatusEstimationControl.h>

#include <EgomotionTracking/components/EgomotionTracking/Misc/SessionPlayer/SessionPlayer.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/RobotKinematic/SimpleArmar3RobotKinematic.h>

class CTrackingControl : public CLocationEstimator
{
public:
    CTrackingControl(int numPSLocationEstimatorParticles, int maxTrackingParticles, int maxDynamicElementParticles);
    ~CTrackingControl() override;

    bool DispatchLocalisation() override;

    const CLocationRegion* GetLocationRegion() const;
    void SetLocationRegion(const CLocationRegion* pLocationRegion);

    const CLocationEstimator* GetMasterLocationEstimator() const;
    void SetMasterLocationEstimator(CLocationEstimator* pMasterLocationEstimator);

    const CRobotControlInterface* GetRobotControl() const;
    void SetRobotControl(CRobotControlInterface* pRobotControl);

    const CLocationStatePredictorInterface* GetLocationStatePredictor() const;
    void SetLocationStatePredictor(CLocationStatePredictorInterface* pLocationStatePredictor);

    CLocationEstimator* GetGroundTruthLocationEstimator() const;
    void SetGroundTruthLocationEstimator(CLocationEstimator* pGroundTruthLocationEstimator);

    const CTrackingImageProcessorInterface* GetTrackingImageProcessor() const;
    void SetTrackingImageProcessor(CTrackingImageProcessorInterface* pTrackingImageProcessor);

    CObservationModelInterface* GetTrackingObservationModel() const;
    void SetTrackingObservationModel(CObservationModelInterface* pTrackingObservationModel);

    void SetDynamicElementStatusEstimationObservationModel(CObservationModelInterface* pDynamicElementStatusEstimationObservationModel);

    const CNeckErrorUncertaintyModel* GetNeckErrorUncertaintyModel() const;
    void SetNeckErrorUncertaintyModel(CNeckErrorUncertaintyModel* pNeckErrorUncertaintyModel);

    void SetDynamicElementStatusUncertaintyModel(DynamicElementStatusUncertaintyModel* pDynamicElementStatusUncertaintyModel);

    void SetParticleSwarmLocationEstimatorObservationModel(CObservationModelInterface* pPSLocationEstimatorObservationModel);

    void SetUncertaintyFactorCalculatorLocationState(CUncertaintyModelScatteringFactorCalculator* pUncertaintyFactorCalculator);
    void SetUncertaintyFactorCalculatorNeckError(CUncertaintyModelScatteringFactorCalculator* pUncertaintyFactorCalculator);

    bool ReinitializeTrackingParticlesWithMasterLocationEstimator();
    bool ReinitializeTrackingParticlesWithPSLocationEstimator();

    bool ReinitializeParticleSwarmLocationEstimatorParticlesInTrackingScatteringEllipse(float enlargementFactor = 1.0);

    void GetParticles(std::vector<CLocationParticle>& activeParticles);

    enum CTrackingMode
    {
        eEvaluateObservationModel,
        eEgomotionTracking,
        eDispatchParticleSwarmLocationEstimator,
        eDynamicElementStatusEstimation
    };
    CTrackingMode GetTrackingMode() const;
    void SetTrackingMode(CTrackingMode trackingMode);

    void SetParametricLineRenderer(CParametricLineRenderer& m_parametricLineRenderer);
    void SetNumberOfActiveParticles(unsigned int nActiveParticles);
    unsigned int GetNumberOfActiveParticles() const;

    CEgomotionTrackingPF* GetTrackingPF() const;
    CParticleSwarmLocationEstimator* GetParticleSwarmLocationEstimator();
    DynamicElementStatusEstimationControl* GetDESEControl();

    float GetEstimatedImportanceWeight() const;
    float GetNeff() const;
    float GetEstimationPlaneErrorLength() const;
    float GetScatteringEllipseArea() const;
    float GetEstimationToGTMahalanobisDistance() const;
    float GetScatteringDeterminant() const;

    EVP::Threading::CThreadPool* GetThreadPool() const;
    void SetThreadPool(CEgomotionTrackingThreadPool* pThreadPool);

    void SetAutoPSReinitialisation(bool enabled,
                                   float scatteringAreaThresholdInitialisation,
                                   float scatteringAreaThresholdLostLocation,
                                   float bestWeightThresholdInitialisation,
                                   int maxTries,
                                   int maxPSStepsPerTry,
                                   float enlargementFactorPerTry
                                  );

    enum CCameraSelectionMode
    {
        eUseLeftCamera,
        eUseRightCamera,
        eAlternateCameras
    };
    void SetCameraSelectionMode(CCameraSelectionMode mode, int alternatingInterval = 0);

    CRenderingUnit& GetBaseRenderingUnit();
    void SetScatteringReduction(bool enabled);

    enum LineRegistrationVisualizationMode
    {
        eDrawLinesInEdgeImage,
        eDrawLinesInInputImage
    };
    void SetLineRegistrationVisualizationMode(LineRegistrationVisualizationMode mode);

    bool GetNeckRPYErrorEstimate(float& neckRollError, float& neckPitchError, float& neckYawError) const;

    void InitializeDynamicElements();

    void setCycleTrackingStatusEstimation(bool cycleTrackingStatusEstimation);

    unsigned int getNumberOfThreads() const;

    const CByteImage* getVirtualRendererImage();

protected:
    void UpdateLinks();

    void SetCamera(CRobotControlInterface::CCameraID id);

    void CalculateOutputMahalanobisDistance();

    CTrackingMode m_trackingMode;

    const CLocationRegion* m_pLocationRegion;
    CLocationEstimator* m_pMasterLocationEstimator;
    CLocationEstimator* m_pGroundTruthLocationEstimator;

    CRobotControlInterface* m_pRobotControl;

    CLocationStatePredictorInterface* m_pLocationStatePredictor;
    CNeckErrorUncertaintyModel* m_pNeckErrorUncertaintyModel;
    DynamicElementStatusUncertaintyModel* m_pDynamicElementStatusUncertaintyModel;

    CUncertaintyModelScatteringFactorCalculator* m_pUncertaintyFactorCalculatorLocationState;
    CUncertaintyModelScatteringFactorCalculator* m_pUncertaintyFactorCalculatorNeckError;

    CTrackingImageProcessorInterface* m_pTrackingImageProcessor;

    CObservationModelInterface* m_pTrackingObservationModel;
    CObservationModelInterface* m_pPSLocationEstimatorObservationModel;
    CObservationModelInterface* m_pDynamicElementStatusEstimationObservationModel;

    CRenderingUnit m_baseRenderingUnit;
    CParametricLineRenderer* m_pBaseParametricLineRenderer;

    CEgomotionTrackingThreadPool* m_pThreadPool;
    CEgomotionTrackingPF* m_pEgomotionTrackingPF;

    /* PS-Localisator */
    CParticleSwarmLocationEstimator* m_pParticleSwarmLocationEstimator;

    /* DESEControl. */
    DynamicElementStatusEstimationControl* m_pDynamicElementStatusEstimationControl;

    struct
    {
        bool enabled;
        float scatteringAreaThresholdInitialisation;
        float scatteringAreaThresholdLostLocation;
        float bestWeightThresholdInitialisation;
        int maxTries;
        int maxPSStepsPerTry;
        float enlargementFactorPerTry;

        bool reinitialisationIsRequired;
        int currentTryCount;
        int currentPSStepCount;

    } m_autoPSReinitialisation;


    struct
    {
        CCameraSelectionMode mode;
        int alternatingInterval;

        int currentAlternatingCount;
        CRobotControlInterface::CCameraID currentAlternatingCamera;
    } m_cameraSelection;



    float m_outputEstimationToGTMahalanobisDistance;
    float m_outputScatteringDeterminant;
    PF_REAL m_outputEstimatedImportanceWeight;
    PF_REAL m_outputNeff;
    PF_REAL m_outputScatteringEllipseArea;

    unsigned int m_numberOfActiveParticles;
    bool m_numberOfParticlesChanged;
    LineRegistrationVisualizationMode m_lineRegistrationVisualizationMode;

    bool m_cycleTrackingStatusEstimation;

    int m_maxTrackingParticles;

    OfflineVirtualCameraView* m_pOfflineVirtualCameraView;

    const CByteImage* m_pVirtualRendererImage;

    bool m_DESECameraFrameUpdateRequired;
};

