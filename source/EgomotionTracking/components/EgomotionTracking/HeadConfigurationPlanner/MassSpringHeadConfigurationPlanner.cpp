#include "MassSpringHeadConfigurationPlanner.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>

CMassSpringHeadConfigurationPlanner::CMassSpringHeadConfigurationPlanner() :
    m_pRelevanceFactorProcessingUnit(NULL),
    m_targetFadingCoefficient(0.5),
    m_springCoefficientMode(eManualCoefficientSelection),
    m_maxAbsoluteAccelerationRoll(MASSPRINGHEADCONFIGURATIONPLANNER_MAXABSOLUTEACCELERATION),
    m_maxAbsoluteAccelerationPitch(MASSPRINGHEADCONFIGURATIONPLANNER_MAXABSOLUTEACCELERATION),
    m_maxAbsoluteAccelerationYaw(MASSPRINGHEADCONFIGURATIONPLANNER_MAXABSOLUTEACCELERATION),
    m_useMoveToMaxHeuristic(false), m_moveToMaxRelevanceFactorThreshold(0.5),
    m_neckErrorRoll(0.0), m_neckErrorPitch(0.0), m_neckErrorYaw(0.0)
{
    //  m_dispatchingProfiler.StartProfiling();
}

void CMassSpringHeadConfigurationPlanner::InitHeadConfigurationSamplesUniformDistributed(unsigned int rollSamplesCnt, unsigned int pitchSamplesCnt, unsigned int yawSamplesCnt)
{
    const float maxRoll = m_headSamplesInitialisationState.m_maxRoll;
    const float maxPitch = m_headSamplesInitialisationState.m_maxPitch;
    const float maxYaw = m_headSamplesInitialisationState.m_maxYaw;
    const float minRoll = m_headSamplesInitialisationState.m_minRoll;
    const float minPitch = m_headSamplesInitialisationState.m_minPitch;
    const float minYaw = m_headSamplesInitialisationState.m_minYaw;

    if (
        m_headSamplesInitialisationState.m_maxRoll != maxRoll ||
        m_headSamplesInitialisationState.m_maxPitch != maxPitch ||
        m_headSamplesInitialisationState.m_maxYaw != maxYaw ||
        m_headSamplesInitialisationState.m_minRoll != minRoll ||
        m_headSamplesInitialisationState.m_minPitch != minPitch ||
        m_headSamplesInitialisationState.m_minYaw != minYaw ||
        m_headSamplesInitialisationState.m_rollSamplesCnt != rollSamplesCnt ||
        m_headSamplesInitialisationState.m_pitchSamplesCnt != pitchSamplesCnt ||
        m_headSamplesInitialisationState.m_yawSamplesCnt != yawSamplesCnt
    )
    {
        m_headConfigurationSamples.clear();

        const float deltaRoll = rollSamplesCnt > 1 ? (maxRoll - minRoll) / (rollSamplesCnt - 1) : 0.0;
        const float deltaPitch = pitchSamplesCnt > 1 ? (maxPitch - minPitch) / (pitchSamplesCnt - 1) : 0.0;
        const float deltaYaw = yawSamplesCnt > 1 ? (maxYaw - minYaw) / (yawSamplesCnt - 1) : 0.0;

        float curRollValue = minRoll;
        for (unsigned int curRollSample = 0; curRollSample < rollSamplesCnt; ++curRollSample, curRollValue += deltaRoll)
        {
            float curPitchValue = minPitch;
            for (unsigned int curPitchSample = 0; curPitchSample < pitchSamplesCnt; ++curPitchSample, curPitchValue += deltaPitch)
            {
                float curYawValue = minYaw;
                for (unsigned int curYawSample = 0; curYawSample < yawSamplesCnt; ++curYawSample, curYawValue += deltaYaw)
                {
                    CHeadConfigurationSample newHeadConfigurationSample(m_targetHeadConfiguration);
                    newHeadConfigurationSample.SetNeckRoll(curRollValue);
                    newHeadConfigurationSample.SetNeckPitch(curPitchValue);
                    newHeadConfigurationSample.SetNeckYaw(curYawValue);
                    m_headConfigurationSamples.push_back(newHeadConfigurationSample);
                }
            }
        }

        ResetPlanner();
    }
}

void CMassSpringHeadConfigurationPlanner::ResetPlanner()
{
    m_currentConfiguration = SbVec3f(m_targetHeadConfiguration.GetNeckRoll(), m_targetHeadConfiguration.GetNeckPitch(), m_targetHeadConfiguration.GetNeckYaw());
    m_currentConfigurationAcceleration = SbVec3f(0.0, 0.0, 0.0);
    m_currentConfigurationVelocity = SbVec3f(0.0, 0.0, 0.0);
}

void CMassSpringHeadConfigurationPlanner::SetSpringCoefficientTarget(float springFactorTarget)
{
    m_springCoefficientTarget = springFactorTarget;
}

float CMassSpringHeadConfigurationPlanner::GetSpringCoefficientTarget() const
{
    return m_springCoefficientTarget;
}

void CMassSpringHeadConfigurationPlanner::SetSpringCoefficientSamplesGlobal(float springFactorSamples)
{
    m_springCoefficientSamplesGlobal = springFactorSamples;
}

float CMassSpringHeadConfigurationPlanner::GetSpringCoefficientSamplesGlobal() const
{
    return m_springCoefficientSamplesGlobal;
}

void CMassSpringHeadConfigurationPlanner::SetDampingCoefficient(float dampingCoefficient)
{
    m_dampingCoefficient = dampingCoefficient;
}

float CMassSpringHeadConfigurationPlanner::GetDampingCoefficient() const
{
    return m_dampingCoefficient;
}

void CMassSpringHeadConfigurationPlanner::SetConfigurationMass(float configurationMass)
{
    m_configurationMass = configurationMass;
}

float CMassSpringHeadConfigurationPlanner::GetConfigurationMass() const
{
    return m_configurationMass;
}

void CMassSpringHeadConfigurationPlanner::SetTargetFadingCoefficient(float targetFadingCoefficient)
{
    m_targetFadingCoefficient = Calculations::SaturizeToLimits(targetFadingCoefficient, 0.0, 1.0);
}

float CMassSpringHeadConfigurationPlanner::GetTargetFadingCoefficient() const
{
    return m_targetFadingCoefficient;
}

void CMassSpringHeadConfigurationPlanner::SetSpringCoefficientMode(CMassSpringHeadConfigurationPlanner::SpringCoefficientMode mode)
{
    m_springCoefficientMode = mode;
}

CMassSpringHeadConfigurationPlanner::SpringCoefficientMode CMassSpringHeadConfigurationPlanner::GetSpringCoefficientMode() const
{
    return m_springCoefficientMode;
}

void CMassSpringHeadConfigurationPlanner::SetUseMoveToMaxHeuristic(bool enabled)
{
    m_useMoveToMaxHeuristic = enabled;
}

void CMassSpringHeadConfigurationPlanner::SetMoveToMaxRelevanceFactorThreshold(float threshold)
{
    m_moveToMaxRelevanceFactorThreshold = threshold;
}

void CMassSpringHeadConfigurationPlanner::SetPlanningLimits(float rollMin, float rollMax, float pitchMin, float pitchMax, float yawMin, float yawMax)
{
    m_headSamplesInitialisationState.m_minRoll = rollMin;
    m_headSamplesInitialisationState.m_minPitch = pitchMin;
    m_headSamplesInitialisationState.m_minYaw = yawMin;
    m_headSamplesInitialisationState.m_maxRoll = rollMax;
    m_headSamplesInitialisationState.m_maxPitch = pitchMax;
    m_headSamplesInitialisationState.m_maxYaw = yawMax;
}

void CMassSpringHeadConfigurationPlanner::SetNeckErrors(float neckErrorRoll, float neckErrorPitch, float neckErrorYaw)
{
    m_neckErrorRoll = neckErrorRoll;
    m_neckErrorPitch = neckErrorPitch;
    m_neckErrorYaw = neckErrorYaw;
}

void CMassSpringHeadConfigurationPlanner::SetMaxAbsoluteAccelerations(float maxAccelerationRoll, float maxAccelerationPitch, float maxAccelerationYaw)
{
    m_maxAbsoluteAccelerationRoll = maxAccelerationRoll;
    m_maxAbsoluteAccelerationPitch = maxAccelerationPitch;
    m_maxAbsoluteAccelerationYaw = maxAccelerationYaw;
}

const CHeadConfiguration& CMassSpringHeadConfigurationPlanner::GetHeadConfigurationSamplesMean() const
{
    return m_headConfigurationSamplesMean;
}

const CHeadConfiguration& CMassSpringHeadConfigurationPlanner::GetHeadConfigurationSpringTarget() const
{
    return m_springTargetHeadConfiguration;
}

void CMassSpringHeadConfigurationPlanner::SetRelevanceFactorProcessingUnit(CRelevanceFactorProcessingUnitInterface* pRelevanceFactorProcessingUnit)
{
    m_pRelevanceFactorProcessingUnit = pRelevanceFactorProcessingUnit;
}

const CRelevanceFactorProcessingUnitInterface* CMassSpringHeadConfigurationPlanner::GetRelevanceFactorProcessingUnit() const
{
    return m_pRelevanceFactorProcessingUnit;
}

const std::vector<CHeadConfigurationSample>& CMassSpringHeadConfigurationPlanner::GetHeadConfigurationSamples() const
{
    return m_headConfigurationSamples;
}

bool CMassSpringHeadConfigurationPlanner::CalculateNextHeadConfiguration(CHeadConfiguration& nextHeadConfiguration)
{
    if (m_pRelevanceFactorProcessingUnit == NULL)
    {
        COUT_WARNING << "m_pRelevanceFactorProcessingUnit == NULL" << std::endl;
        return false;
    }
    if (m_pLocationEstimator == NULL)
    {
        COUT_WARNING << "m_pLocationEstimator == NULL" << std::endl;
        return false;
    }

    UpdateCurrentNeckErrorToSampleConfigurations();

    m_pRelevanceFactorProcessingUnit->SetRobotControl(m_pControllableRobot);
    if (!m_pRelevanceFactorProcessingUnit->CalculateRelevanceFactors(m_headConfigurationSamples, m_pLocationEstimator->GetLocationEstimate()))
    {
        COUT_WARNING << "m_pRelevanceFactorProcessingUnit->CalculateRelevanceFactors( ... ) failed" << std::endl;
        return false;
    }

    const float deltaT = m_pControllableRobot->GetRobotControlState().GetDeltaT();
    const float deltaTInSeconds = Conversions::MillisecondsToSeconds(deltaT);
    if (deltaT > MASSPRINGHEADCONFIGURATIONPLANNER_MAXDELTAT)
    {
        return false;
    }

    CalculateTotalAndMaxRelevanceAndSamplesMean();

    if (m_useMoveToMaxHeuristic)
    {
        const float meanRelevanceWeight = m_pRelevanceFactorProcessingUnit->CalculateRelevanceFactor(m_headConfigurationSamplesMean, m_pLocationEstimator->GetLocationEstimate());

        if (meanRelevanceWeight < m_moveToMaxRelevanceFactorThreshold * m_maxRelevanceFactor)
        {
            const float oldSpringTargetRelevanceWeight = m_pRelevanceFactorProcessingUnit->CalculateRelevanceFactor(m_springTargetHeadConfiguration, m_pLocationEstimator->GetLocationEstimate());
            if (oldSpringTargetRelevanceWeight < m_moveToMaxRelevanceFactorThreshold * m_maxRelevanceFactor)
            {
                m_springTargetHeadConfiguration = m_maxHeadConfigurationSample;
            }
        }
        else
        {
            m_springTargetHeadConfiguration = m_headConfigurationSamplesMean;
        }
    }
    else
    {
        m_springTargetHeadConfiguration = m_headConfigurationSamplesMean;
    }

    /* Calculate Forces */
    float springCoefficientSamples = 0.0;
    float springCoefficientTarget = 0.0;
    GetSpringCoefficients(springCoefficientSamples, springCoefficientTarget);

    const SbVec3f forceSamples = springCoefficientSamples * m_totalRelevanceFactor * (m_springTargetHeadConfiguration.GetSbVec3f() - m_currentConfiguration);
    const SbVec3f forceTarget = springCoefficientTarget * (m_targetHeadConfiguration.GetSbVec3f() - m_currentConfiguration);
    const SbVec3f forceDamping = -m_dampingCoefficient * m_currentConfigurationVelocity;

    /* Update Acceleration, Velocity and Position */
    m_currentConfigurationAcceleration = (1.0 / m_configurationMass) * (forceSamples + forceTarget + forceDamping);
    m_currentConfigurationVelocity = m_currentConfigurationVelocity + deltaTInSeconds * m_currentConfigurationAcceleration;
    m_currentConfiguration = m_currentConfiguration + deltaTInSeconds * m_currentConfigurationVelocity;

    m_currentConfigurationAcceleration[0] = Calculations::SaturizeAbsolute(m_currentConfigurationAcceleration[0], m_maxAbsoluteAccelerationRoll);
    m_currentConfigurationAcceleration[1] = Calculations::SaturizeAbsolute(m_currentConfigurationAcceleration[1], m_maxAbsoluteAccelerationRoll);
    m_currentConfigurationAcceleration[2] = Calculations::SaturizeAbsolute(m_currentConfigurationAcceleration[2], m_maxAbsoluteAccelerationRoll);
    nextHeadConfiguration = CHeadConfiguration(m_currentConfiguration);
    return true;
}

void CMassSpringHeadConfigurationPlanner::CalculateTotalAndMaxRelevanceAndSamplesMean()
{
    m_totalRelevanceFactor = 0.0;
    m_maxRelevanceFactor = -FLT_MAX;
    if (m_headConfigurationSamples.size() > 0)
    {
        float meanRoll = 0.0;
        float meanPitch = 0.0;
        float meanYaw = 0.0;

        for (std::vector<CHeadConfigurationSample>::const_iterator curSampleIterator = m_headConfigurationSamples.begin(); curSampleIterator != m_headConfigurationSamples.end(); ++curSampleIterator)
        {
            const CHeadConfigurationSample& curHeadConfigurationSample = *curSampleIterator;

            const float curRelevanceFactor = curHeadConfigurationSample.GetRelevanceFactor();
            m_totalRelevanceFactor += curRelevanceFactor;
            meanRoll += curRelevanceFactor * curHeadConfigurationSample.GetNeckRoll();
            meanPitch += curRelevanceFactor * curHeadConfigurationSample.GetNeckPitch();
            meanYaw += curRelevanceFactor * curHeadConfigurationSample.GetNeckYaw();

            if (curRelevanceFactor > m_maxRelevanceFactor)
            {
                m_maxRelevanceFactor = curRelevanceFactor;
                m_maxHeadConfigurationSample = curHeadConfigurationSample;
            }
        }

        if (m_totalRelevanceFactor > FLT_EPSILON)
        {
            meanRoll /= m_totalRelevanceFactor;
            meanPitch /= m_totalRelevanceFactor;
            meanYaw /= m_totalRelevanceFactor;
        }
        else
        {
            m_totalRelevanceFactor = 0.0;
            meanRoll = 0.0;
            meanPitch = 0.0;
            meanYaw = 0.0;
        }

        m_headConfigurationSamplesMean.SetNeckRoll(meanRoll);
        m_headConfigurationSamplesMean.SetNeckPitch(meanPitch);
        m_headConfigurationSamplesMean.SetNeckYaw(meanYaw);
    }
}

void CMassSpringHeadConfigurationPlanner::GetSpringCoefficients(float& springCoefficientSamples, float& springCoefficientTarget)
{
    if (m_springCoefficientMode == eManualCoefficientSelection)
    {
        springCoefficientSamples = m_springCoefficientSamplesGlobal;
        springCoefficientTarget = m_springCoefficientTarget;
    }
    else if (m_springCoefficientMode == eTargetFadingMode)
    {
        springCoefficientTarget = m_targetFadingCoefficient;
        if (m_totalRelevanceFactor > FLT_EPSILON)
        {
            springCoefficientSamples = ((1 - m_targetFadingCoefficient) * m_maxRelevanceFactor) / m_totalRelevanceFactor;
        }
        else
        {
            springCoefficientSamples = 0.0;
        }
    }
}

void CMassSpringHeadConfigurationPlanner::UpdateCurrentNeckErrorToSampleConfigurations()
{
    if (m_headConfigurationSamples.size() > 0)
    {
        for (std::vector<CHeadConfigurationSample>::iterator curSampleIterator = m_headConfigurationSamples.begin(); curSampleIterator != m_headConfigurationSamples.end(); ++curSampleIterator)
        {
            CHeadConfigurationSample& curHeadConfigurationSample = *curSampleIterator;

            curHeadConfigurationSample.SetNeckError(m_neckErrorRoll, m_neckErrorPitch, m_neckErrorYaw);
        }
    }
}
