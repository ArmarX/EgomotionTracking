#pragma once

#include <EgomotionTracking/components/EgomotionTracking/RobotControl/ControllableRobotControlInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/HeadConfiguration.h>
#include <EgomotionTracking/components/EgomotionTracking/LocationEstimator/LocationEstimator.h>

class CHeadConfigurationPlannerInterface
{
public:
    CHeadConfigurationPlannerInterface();

    bool DispatchHeadConfigurationPlanner();
    void SetControllableRobot(CControllableRobotControlInterface* pControllableRobot);
    const CControllableRobotControlInterface* GetControllableRobot() const;

    void SetTargetHeadConfiguration(const CHeadConfiguration& targetHeadConfiguration);
    const CHeadConfiguration& GetTargetHeadConfiguration() const;

    const CHeadConfiguration& GetCurrentPlannedHeadConfiguration() const;

    void SetHeadConfigurationLimits(float minNeckRoll, float maxNeckRoll,
                                    float minNeckPitch, float maxNeckPitch,
                                    float minNeckYaw, float maxNeckYaw);

    void SetLocationEstimator(const CLocationEstimator* pLocationEstimator);
    const CLocationEstimator* GetLocationEstimator() const;

    void SetMovingHeadAllowed(bool movingHeadAllowed);
    bool GetMovingHeadAllowed() const;
protected:
    virtual bool CalculateNextHeadConfiguration(CHeadConfiguration& nextHeadConfiguration) = 0;

    CControllableRobotControlInterface* m_pControllableRobot;
    const CLocationEstimator* m_pLocationEstimator;
    CHeadConfiguration m_targetHeadConfiguration;
    CHeadConfiguration m_currentPlannedHeadConfiguration;

    bool m_movingHeadAllowed;
};

