#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/HeadConfigurationSample.h>
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotControlInterface.h>
#include <vector>

#include "ScreenLineRelevanceWeightingFunctionInterface.h"

class CRelevanceFactorProcessingUnitInterface
{
public:
    CRelevanceFactorProcessingUnitInterface() : m_pRobotControl(NULL), m_pScreenLineRelevanceWeightingFunctionObject(NULL) {};
    virtual bool CalculateRelevanceFactors(std::vector<CHeadConfigurationSample>& headConfigurationSamples, const CLocationState& locationState) = 0;
    virtual float CalculateRelevanceFactor(const CHeadConfigurationSample& headConfiguration, const CLocationState& location) = 0;
    void SetRobotControl(const CRobotControlInterface* pRobotControl)
    {
        m_pRobotControl = pRobotControl;
    }

    void SetScreenLineRelevanceWeightingFunctionObject(const CScreenLineRelevanceWeightingFunctionInterface* pScreenLineRelevanceWeightingFunctionObject)
    {
        m_pScreenLineRelevanceWeightingFunctionObject = pScreenLineRelevanceWeightingFunctionObject;
    }

protected:
    const CRobotControlInterface* m_pRobotControl;
    const CScreenLineRelevanceWeightingFunctionInterface* m_pScreenLineRelevanceWeightingFunctionObject;
};

