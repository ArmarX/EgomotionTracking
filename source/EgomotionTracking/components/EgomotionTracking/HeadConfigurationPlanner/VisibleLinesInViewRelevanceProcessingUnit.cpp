#include "VisibleLinesInViewRelevanceProcessingUnit.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>

CVisibleLinesInViewRelevanceProcessingUnit::CVisibleLinesInViewRelevanceProcessingUnit() :
    m_pThreadPool(NULL), m_pRenderingUnit(NULL)
{
}

bool CVisibleLinesInViewRelevanceProcessingUnit::CalculateRelevanceFactors(std::vector<CHeadConfigurationSample>& headConfigurationSamples, const CLocationState& locationState)
{
    assert(m_pThreadPool);
    UpdateRenderingUnits();

    if (m_executionUnits.size() == 0)
    {
        COUT_ERROR << "m_executionUnits.size() == 0" << std::endl;
        return false;
    }
    if (m_pRobotControl == NULL)
    {
        COUT_ERROR << "m_pRobotControl == NULL" << std::endl;
        return false;
    }

    for (std::list<CVisibleLinesInViewRelevanceProcessingExecutionUnit>::iterator curExecutionUnitIterator = m_executionUnits.begin(); curExecutionUnitIterator != m_executionUnits.end(); ++curExecutionUnitIterator)
    {
        curExecutionUnitIterator->SetRobotControl(m_pRobotControl);
        curExecutionUnitIterator->SetLocationState(locationState);
        curExecutionUnitIterator->SetRelevanceWeightingFunctionObject(m_pScreenLineRelevanceWeightingFunctionObject);
        curExecutionUnitIterator->ClearHeadConfigurationSamples();
    }

    if (headConfigurationSamples.size() > 0)
    {
        unsigned int curHeadConfigurationCounter = 0;

        std::list<CVisibleLinesInViewRelevanceProcessingExecutionUnit>::iterator curExecutionUnitIterator = m_executionUnits.begin();
        for (std::vector<CHeadConfigurationSample>::iterator curHeadConfigurationSampleIterator = headConfigurationSamples.begin(); curHeadConfigurationSampleIterator != headConfigurationSamples.end(); ++curHeadConfigurationSampleIterator, ++curHeadConfigurationCounter)
        {
            CHeadConfigurationSample& curHeadConfigurationSample = *curHeadConfigurationSampleIterator;

            if (curHeadConfigurationCounter % m_sparsity == m_currentSparseIntervall)
            {
                curExecutionUnitIterator->AddSampleReference(&curHeadConfigurationSample);
                ++curExecutionUnitIterator;
                if (curExecutionUnitIterator == m_executionUnits.end())
                {
                    curExecutionUnitIterator = m_executionUnits.begin();
                }
            }
        }
    }
    for (std::list<CVisibleLinesInViewRelevanceProcessingExecutionUnit>::iterator curExecutionUnitIterator = m_executionUnits.begin(); curExecutionUnitIterator != m_executionUnits.end(); ++curExecutionUnitIterator)
    {
        m_pThreadPool->DispatchExecutionUnit(&(*curExecutionUnitIterator), true);
    }
    m_pThreadPool->Synchronize();

    m_currentSparseIntervall = (m_currentSparseIntervall + 1) % m_sparsity;
    return true;
}

float CVisibleLinesInViewRelevanceProcessingUnit::CalculateRelevanceFactor(const CHeadConfigurationSample& headConfiguration, const CLocationState& location)
{
    if (m_executionUnits.size() == 0)
    {
        COUT_ERROR << "m_executionUnits.size() == 0" << std::endl;
        return -1.0;
    }
    CVisibleLinesInViewRelevanceProcessingExecutionUnit& weightingEU = m_executionUnits.front();
    weightingEU.SetRobotControl(m_pRobotControl);
    weightingEU.SetLocationState(location);
    weightingEU.SetRelevanceWeightingFunctionObject(m_pScreenLineRelevanceWeightingFunctionObject);
    return weightingEU.CalculateRelevanceFactor(headConfiguration, location);
}

void CVisibleLinesInViewRelevanceProcessingUnit::SetThreadPool(EVP::Threading::CThreadPool* pThreadPool)
{
    if (pThreadPool->GetTotalThreads() > 0)
    {
        m_pThreadPool = pThreadPool;
        m_executionUnits.resize(pThreadPool->GetTotalThreads());

        for (std::list<CVisibleLinesInViewRelevanceProcessingExecutionUnit>::iterator currentExecutionUnit = m_executionUnits.begin(); currentExecutionUnit != m_executionUnits.end(); ++currentExecutionUnit)
        {
            if (m_pRenderingUnit != NULL)
            {
                currentExecutionUnit->SetRenderingUnit(*m_pRenderingUnit);
            }
        }
    }
}

void CVisibleLinesInViewRelevanceProcessingUnit::SetRenderingUnit(CRenderingUnit* pRenderingUnit)
{
    m_pRenderingUnit = pRenderingUnit;

    if (m_executionUnits.size() > 0 && pRenderingUnit != NULL)
    {
        for (std::list<CVisibleLinesInViewRelevanceProcessingExecutionUnit>::iterator curExecutionUnitIterator = m_executionUnits.begin(); curExecutionUnitIterator != m_executionUnits.end(); ++curExecutionUnitIterator)
        {
            curExecutionUnitIterator->SetRenderingUnit(*pRenderingUnit);
        }
    }
}

void CVisibleLinesInViewRelevanceProcessingUnit::SetSparsity(unsigned int sparsity)
{
    if (sparsity > 0)
    {
        m_sparsity = sparsity;
    }
    else
    {
        sparsity = 1;
    }
    m_currentSparseIntervall = 0;

}

void CVisibleLinesInViewRelevanceProcessingUnit::UpdateRenderingUnits()
{
    m_pRenderingUnit->UpdateRenderingConfigurationFromParent();
    if (m_executionUnits.size() > 0)
    {
        for (std::list<CVisibleLinesInViewRelevanceProcessingExecutionUnit>::iterator currentExecutionUnit = m_executionUnits.begin(); currentExecutionUnit != m_executionUnits.end(); ++currentExecutionUnit)
        {
            currentExecutionUnit->UpdateRenderingUnit();
        }
    }
}
