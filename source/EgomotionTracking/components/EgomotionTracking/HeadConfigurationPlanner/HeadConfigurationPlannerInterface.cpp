#include "HeadConfigurationPlannerInterface.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>

CHeadConfigurationPlannerInterface::CHeadConfigurationPlannerInterface() :
    m_pControllableRobot(NULL), m_pLocationEstimator(NULL), m_movingHeadAllowed(false)
{
}

bool CHeadConfigurationPlannerInterface::DispatchHeadConfigurationPlanner()
{
    if (m_pControllableRobot == NULL)
    {
        COUT_ERROR << "m_pControllableRobot == NULL" << std::endl;
        return false;
    }

    if (CalculateNextHeadConfiguration(m_currentPlannedHeadConfiguration))
    {
        if (m_movingHeadAllowed)
        {
            m_pControllableRobot->SetTargetRobotHeadConfiguration(m_currentPlannedHeadConfiguration);
        }
    }
    return true;
}

void CHeadConfigurationPlannerInterface::SetControllableRobot(CControllableRobotControlInterface* pControllableRobot)
{
    m_pControllableRobot = pControllableRobot;
}

const CControllableRobotControlInterface* CHeadConfigurationPlannerInterface::GetControllableRobot() const
{
    return m_pControllableRobot;
}

void CHeadConfigurationPlannerInterface::SetTargetHeadConfiguration(const CHeadConfiguration& targetHeadConfiguration)
{
    m_targetHeadConfiguration.SetValues(targetHeadConfiguration);
}

const CHeadConfiguration& CHeadConfigurationPlannerInterface::GetTargetHeadConfiguration() const
{
    return m_targetHeadConfiguration;
}

const CHeadConfiguration& CHeadConfigurationPlannerInterface::GetCurrentPlannedHeadConfiguration() const
{
    return m_currentPlannedHeadConfiguration;
}

void CHeadConfigurationPlannerInterface::SetHeadConfigurationLimits(float minNeckRoll, float maxNeckRoll, float minNeckPitch, float maxNeckPitch, float minNeckYaw, float maxNeckYaw)
{
    m_targetHeadConfiguration.SetLimits(minNeckRoll, maxNeckRoll, minNeckPitch, maxNeckPitch, minNeckYaw, maxNeckYaw);
}

void CHeadConfigurationPlannerInterface::SetLocationEstimator(const CLocationEstimator* pLocationEstimator)
{
    m_pLocationEstimator = pLocationEstimator;
}

const CLocationEstimator* CHeadConfigurationPlannerInterface::GetLocationEstimator() const
{
    return m_pLocationEstimator;
}

void CHeadConfigurationPlannerInterface::SetMovingHeadAllowed(bool movingHeadAllowed)
{
    m_movingHeadAllowed = movingHeadAllowed;
}

bool CHeadConfigurationPlannerInterface::GetMovingHeadAllowed() const
{
    return m_movingHeadAllowed;
}
