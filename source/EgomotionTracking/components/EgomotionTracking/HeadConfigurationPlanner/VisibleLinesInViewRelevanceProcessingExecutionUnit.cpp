#include "VisibleLinesInViewRelevanceProcessingExecutionUnit.h"

#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>

CVisibleLinesInViewRelevanceProcessingExecutionUnit::CVisibleLinesInViewRelevanceProcessingExecutionUnit() :
    m_pRelevanceWeightingFunctionObject(NULL)
{ }

void CVisibleLinesInViewRelevanceProcessingExecutionUnit::UpdateRenderingUnit()
{
    m_renderingUnit.UpdateRenderingConfigurationFromParent();
}

void CVisibleLinesInViewRelevanceProcessingExecutionUnit::SetRenderingUnit(const CRenderingUnit& renderingUnit)
{
    m_renderingUnit = renderingUnit;
}

void CVisibleLinesInViewRelevanceProcessingExecutionUnit::SetRobotControl(const CRobotControlInterface* pRobotControl)
{
    m_pRobotControl = pRobotControl;
}

void CVisibleLinesInViewRelevanceProcessingExecutionUnit::SetLocationState(const CLocationState& locationState)
{
    m_locationState = locationState;
}

void CVisibleLinesInViewRelevanceProcessingExecutionUnit::SetRelevanceWeightingFunctionObject(const CScreenLineRelevanceWeightingFunctionInterface* pRelevanceWeightingFunctionObject)
{
    m_pRelevanceWeightingFunctionObject = pRelevanceWeightingFunctionObject;
}

void CVisibleLinesInViewRelevanceProcessingExecutionUnit::ClearHeadConfigurationSamples()
{
    m_headConfigurationSamples.clear();
}

void CVisibleLinesInViewRelevanceProcessingExecutionUnit::AddSampleReference(CHeadConfigurationSample* pHeadConfigurationSample)
{
    m_headConfigurationSamples.push_back(pHeadConfigurationSample);
}

float CVisibleLinesInViewRelevanceProcessingExecutionUnit::CalculateRelevanceFactor(const CHeadConfigurationSample& headConfiguration, const CLocationState& location)
{
    SbRotation cameraOrientation;
    SbVec3f cameraPosition;

    SbMatrix transformationPlatformToCamera;
    m_pRobotControl->CalculateTransformationPlatformToCamera(headConfiguration, m_pRobotControl->GetCameraId(), transformationPlatformToCamera);
    Calculations::CalculateRobotCameraPosition(location, transformationPlatformToCamera, cameraPosition, cameraOrientation);

    std::vector<CScreenLine> renderedLines;
    m_renderingUnit.RenderLines(cameraPosition, cameraOrientation, renderedLines);

    return m_pRelevanceWeightingFunctionObject->CalculateRelevanceWeight(renderedLines);
}

bool CVisibleLinesInViewRelevanceProcessingExecutionUnit::Execute()
{
    if (m_pRelevanceWeightingFunctionObject == NULL)
    {
        COUT_ERROR << "m_pRelevanceWeightingFunctionObject == NULL" << std::endl;
        return false;
    }

    if (m_headConfigurationSamples.size() > 0)
    {
        for (std::vector<CHeadConfigurationSample*>::iterator curHeadConfigurationSampleIterator = m_headConfigurationSamples.begin(); curHeadConfigurationSampleIterator != m_headConfigurationSamples.end(); ++curHeadConfigurationSampleIterator)
        {
            CHeadConfigurationSample* pCurHeadConfiguration = *curHeadConfigurationSampleIterator;
            pCurHeadConfiguration->SetRelevanceFactor(CalculateRelevanceFactor(*pCurHeadConfiguration, m_locationState));
        }
    }
    return true;
}
