#pragma once
#include "RelevanceFactorProcessingUnitInterface.h"

#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/ParametricLineRenderer.h>
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotControlInterface.h>

class CProjectedLinesInViewRelevanceProcessingUnit : public CRelevanceFactorProcessingUnitInterface
{
public:
    CProjectedLinesInViewRelevanceProcessingUnit();
    bool CalculateRelevanceFactors(std::vector<CHeadConfigurationSample>& headConfigurationSamples, const CLocationState& locationState) override;
    float CalculateRelevanceFactor(const CHeadConfigurationSample& headConfiguration, const CLocationState& location) override;
    void SetParametricLineRenderer(CParametricLineRenderer* pParametricLineRenderer);
protected:
    CParametricLineRenderer* m_pParametricLineRenderer;
};

