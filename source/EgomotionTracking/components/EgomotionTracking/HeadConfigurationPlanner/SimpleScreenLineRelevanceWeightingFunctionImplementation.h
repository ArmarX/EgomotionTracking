#pragma once

#include "ScreenLineRelevanceWeightingFunctionInterface.h"

class CSimpleScreenLineRelevanceWeightingFunctionImplementation : public CScreenLineRelevanceWeightingFunctionInterface
{
public:
    CSimpleScreenLineRelevanceWeightingFunctionImplementation();
    float CalculateRelevanceWeight(const std::vector<CScreenLine>& screenLines) const override;

};

