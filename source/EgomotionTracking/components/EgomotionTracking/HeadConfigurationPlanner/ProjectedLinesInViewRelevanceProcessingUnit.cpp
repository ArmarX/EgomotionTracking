#include "ProjectedLinesInViewRelevanceProcessingUnit.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/OpenGLCalculations.h>

#include <Inventor/SbRotation.h>
#include <Inventor/SbVec3f.h>

CProjectedLinesInViewRelevanceProcessingUnit::CProjectedLinesInViewRelevanceProcessingUnit() :
    m_pParametricLineRenderer(NULL)
{
}

/*
#include <EgomotionTracking/Helpers/PrimitiveDrawer.h>
#include <Image/ByteImage.h>
#include <EgomotionTracking/Helpers/DebuggingPrimitives.h>
*/

bool CProjectedLinesInViewRelevanceProcessingUnit::CalculateRelevanceFactors(std::vector<CHeadConfigurationSample>& headConfigurationSamples, const CLocationState& locationState)
{
    if (m_pParametricLineRenderer == NULL)
    {
        COUT_ERROR << "m_pParametricLineRenderer == NULL" << std::endl;
        return false;
    }
    if (m_pRobotControl == NULL)
    {
        COUT_ERROR << "m_pRobotControl == NULL" << std::endl;
        return false;
    }

    if (m_pScreenLineRelevanceWeightingFunctionObject == NULL)
    {
        COUT_ERROR << "m_pScreenLineRelevanceWeightingFunctionObject == NULL" << std::endl;
        return false;
    }

    m_pParametricLineRenderer->SetProjectionMatrixGl(m_pRobotControl->GetCameraProjectionMatrixGL());

    if (headConfigurationSamples.size() > 0)
    {
        for (std::vector<CHeadConfigurationSample>::iterator curHeadConfigurationSampleIterator = headConfigurationSamples.begin(); curHeadConfigurationSampleIterator != headConfigurationSamples.end(); ++curHeadConfigurationSampleIterator)
        {
            CHeadConfigurationSample& curHeadConfiguration = *curHeadConfigurationSampleIterator;
            curHeadConfiguration.SetRelevanceFactor(CalculateRelevanceFactor(curHeadConfiguration, locationState));
            /*
            CByteImage tmpImage(640, 480, CByteImage::eRGB24);
            ImageProcessor::Zero(&tmpImage);
            PrimitiveDrawer::DrawLines(m_pParametricLineRenderer->GetProjectedScreenLineList(), 255, 0, 0, 1, &tmpImage);
            char curFn[256];
            sprintf(curFn, "testprojectedLines-%05d.bmp", i);
            ++i;
            tmpImage.SaveToFile(curFn);
            */
        }
    }
    return true;
}

float CProjectedLinesInViewRelevanceProcessingUnit::CalculateRelevanceFactor(const CHeadConfigurationSample& headConfiguration, const CLocationState& location)
{
    SbRotation cameraOrientation;
    SbVec3f cameraPosition;

    SbMatrix transformationPlatformToCamera;
    m_pRobotControl->CalculateTransformationPlatformToCamera(headConfiguration, m_pRobotControl->GetCameraId(), transformationPlatformToCamera);
    Calculations::CalculateRobotCameraPosition(location, transformationPlatformToCamera, cameraPosition, cameraOrientation);

    m_pParametricLineRenderer->SetCameraPosition(OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(cameraPosition, OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(cameraOrientation)));
    m_pParametricLineRenderer->ProjectModelLinesToScreen();

    const std::vector<CScreenLine>& projectedLines = m_pParametricLineRenderer->GetProjectedScreenLineList();
    return m_pScreenLineRelevanceWeightingFunctionObject->CalculateRelevanceWeight(projectedLines);
}

void CProjectedLinesInViewRelevanceProcessingUnit::SetParametricLineRenderer(CParametricLineRenderer* pParametricLineRenderer)
{
    m_pParametricLineRenderer = pParametricLineRenderer;
}
