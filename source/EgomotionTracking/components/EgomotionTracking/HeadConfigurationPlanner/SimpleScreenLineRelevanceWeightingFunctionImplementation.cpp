#include "SimpleScreenLineRelevanceWeightingFunctionImplementation.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>

CSimpleScreenLineRelevanceWeightingFunctionImplementation::CSimpleScreenLineRelevanceWeightingFunctionImplementation()
{
}

float CSimpleScreenLineRelevanceWeightingFunctionImplementation::CalculateRelevanceWeight(const std::vector<CScreenLine>& screenLines) const
{
    const float lineCntFactor = Calculations::SaturizeToLimits(static_cast<float>(screenLines.size()) / 80, 0.0, 1.0);
    float lengthSum = 0.0;
    if (screenLines.size() > 0)
    {
        for (std::vector<CScreenLine>::const_iterator lineIterator = screenLines.begin(); lineIterator != screenLines.end(); ++lineIterator)
        {
            const CScreenLine& curLine = *lineIterator;
            lengthSum += curLine.CalculateLength2d();
        }
    }
    const float lineLengthFactor = Calculations::SaturizeToLimits(lengthSum / 10000.0, 0.0, 1.0);
    return lineCntFactor * lineLengthFactor;
}
