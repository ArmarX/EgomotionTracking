#pragma once

#include "RelevanceFactorProcessingUnitInterface.h"
#include "VisibleLinesInViewRelevanceProcessingExecutionUnit.h"

#include <EgomotionTracking/components/EgomotionTracking/Processing/RenderingUnit/RenderingUnit.h>

#include <EgomotionTracking/components/EVP/Foundation/Process/ThreadPool.h>

#include <list>

class CVisibleLinesInViewRelevanceProcessingUnit : public CRelevanceFactorProcessingUnitInterface
{
public:
    CVisibleLinesInViewRelevanceProcessingUnit();
    bool CalculateRelevanceFactors(std::vector<CHeadConfigurationSample>& headConfigurationSamples, const CLocationState& locationState) override;
    float CalculateRelevanceFactor(const CHeadConfigurationSample& headConfiguration, const CLocationState& location) override;

    void SetThreadPool(EVP::Threading::CThreadPool* pThreadPool);
    void SetRenderingUnit(CRenderingUnit* pRenderingUnit);

    void SetSparsity(unsigned int sparsity);
protected:
    void UpdateRenderingUnits();
    EVP::Threading::CThreadPool* m_pThreadPool;
    CRenderingUnit* m_pRenderingUnit;

    std::list<CVisibleLinesInViewRelevanceProcessingExecutionUnit> m_executionUnits;

    unsigned int m_sparsity;
    unsigned int m_currentSparseIntervall;
};

