#pragma once

#include <vector>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/Line.h>

class CScreenLineRelevanceWeightingFunctionInterface
{
public:
    CScreenLineRelevanceWeightingFunctionInterface() {};
    virtual float CalculateRelevanceWeight(const std::vector<CScreenLine>& screenLines) const = 0;
};

