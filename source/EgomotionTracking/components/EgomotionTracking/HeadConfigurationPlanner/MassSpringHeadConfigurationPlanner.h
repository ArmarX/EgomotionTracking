#pragma once

#include "HeadConfigurationPlannerInterface.h"
#include "RelevanceFactorProcessingUnitInterface.h"
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/HeadConfigurationSample.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Profiling.h>

#include <Inventor/SbVec3f.h>
#include <vector>


const float MASSPRINGHEADCONFIGURATIONPLANNER_MAXDELTAT = 2000;

const float MASSPRINGHEADCONFIGURATIONPLANNER_MAXABSOLUTEACCELERATION = Conversions::DegreeToRadians(10);
class CMassSpringHeadConfigurationPlanner : public CHeadConfigurationPlannerInterface
{
public:
    CMassSpringHeadConfigurationPlanner();

    void InitHeadConfigurationSamplesUniformDistributed(unsigned int rollSamplesCnt, unsigned int pitchSamplesCnt, unsigned int yawSamplesCnt);

    void ResetPlanner();

    void SetSpringCoefficientTarget(float springFactorTarget);
    float GetSpringCoefficientTarget() const;
    void SetSpringCoefficientSamplesGlobal(float springFactorSamples);
    float GetSpringCoefficientSamplesGlobal() const;
    void SetDampingCoefficient(float dampingCoefficient);
    float GetDampingCoefficient() const;
    void SetConfigurationMass(float configurationMass);
    float GetConfigurationMass() const;
    void SetTargetFadingCoefficient(float targetFadingCoefficient);
    float GetTargetFadingCoefficient() const;

    void SetMaxAbsoluteAccelerations(float maxAccelerationRoll, float maxAccelerationPitch, float maxAccelerationYaw);

    const CHeadConfiguration& GetHeadConfigurationSamplesMean() const;
    const CHeadConfiguration& GetHeadConfigurationSpringTarget() const;

    void SetRelevanceFactorProcessingUnit(CRelevanceFactorProcessingUnitInterface* pRelevanceFactorProcessingUnit);
    const CRelevanceFactorProcessingUnitInterface* GetRelevanceFactorProcessingUnit() const;

    const std::vector<CHeadConfigurationSample>& GetHeadConfigurationSamples() const;

    enum SpringCoefficientMode
    {
        eManualCoefficientSelection,
        eTargetFadingMode
    };
    void SetSpringCoefficientMode(SpringCoefficientMode mode);
    SpringCoefficientMode GetSpringCoefficientMode() const;

    void SetUseMoveToMaxHeuristic(bool enabled);
    void SetMoveToMaxRelevanceFactorThreshold(float threshold);

    void SetPlanningLimits(float rollMin, float rollMax, float pitchMin, float pitchMax, float yawMin, float yawMax);
    void SetNeckErrors(float neckErrorRoll, float neckErrorPitch, float neckErrorYaw);
protected:
    bool CalculateNextHeadConfiguration(CHeadConfiguration& nextHeadConfiguration) override;
    void CalculateTotalAndMaxRelevanceAndSamplesMean();

    void GetSpringCoefficients(float& springCoefficientSamples, float& springCoefficientTarget);
    void UpdateCurrentNeckErrorToSampleConfigurations();

    struct CHeadSamplesInitialisationState
    {
        CHeadSamplesInitialisationState() : m_rollSamplesCnt(0), m_pitchSamplesCnt(0), m_yawSamplesCnt(0) {}
        float m_minRoll;
        float m_maxRoll;
        float m_minPitch;
        float m_maxPitch;
        float m_minYaw;
        float m_maxYaw;

        unsigned int m_rollSamplesCnt;
        unsigned int m_pitchSamplesCnt;
        unsigned int m_yawSamplesCnt;
    } m_headSamplesInitialisationState;

    CRelevanceFactorProcessingUnitInterface* m_pRelevanceFactorProcessingUnit;
    std::vector<CHeadConfigurationSample> m_headConfigurationSamples;

    CHeadConfigurationSample m_headConfigurationSamplesMean;
    float m_totalRelevanceFactor;
    float m_maxRelevanceFactor;
    CHeadConfigurationSample m_maxHeadConfigurationSample;

    SbVec3f m_currentConfiguration;
    SbVec3f m_currentConfigurationVelocity;
    SbVec3f m_currentConfigurationAcceleration;

    /* configuration gains */
    float m_springCoefficientTarget;
    float m_springCoefficientSamplesGlobal;
    float m_dampingCoefficient;
    float m_configurationMass;
    float m_targetFadingCoefficient;
    SpringCoefficientMode m_springCoefficientMode;

    float m_maxAbsoluteAccelerationRoll;
    float m_maxAbsoluteAccelerationPitch;
    float m_maxAbsoluteAccelerationYaw;

    bool m_useMoveToMaxHeuristic;
    float m_moveToMaxRelevanceFactorThreshold;
    CHeadConfigurationSample m_springTargetHeadConfiguration;

    float m_neckErrorRoll;
    float m_neckErrorPitch;
    float m_neckErrorYaw;
};

