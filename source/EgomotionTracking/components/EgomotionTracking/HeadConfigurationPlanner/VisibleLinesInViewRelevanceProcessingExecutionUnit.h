#pragma once

#include "ScreenLineRelevanceWeightingFunctionInterface.h"

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/HeadConfigurationSample.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/RenderingUnit/RenderingUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotControlInterface.h>

#include <EgomotionTracking/components/EVP/Foundation/Process/ExecutionUnit.h>
#include <vector>

class CVisibleLinesInViewRelevanceProcessingExecutionUnit : public EVP::Threading::CExecutionUnit
{
public:
    CVisibleLinesInViewRelevanceProcessingExecutionUnit();
    void UpdateRenderingUnit();

    void SetRenderingUnit(const CRenderingUnit& renderingUnit);
    void SetRobotControl(const CRobotControlInterface* pRobotControl);
    void SetLocationState(const CLocationState& locationState);
    void SetRelevanceWeightingFunctionObject(const CScreenLineRelevanceWeightingFunctionInterface* pRelevanceWeightingFunctionObject);
    void ClearHeadConfigurationSamples();
    void AddSampleReference(CHeadConfigurationSample* pHeadConfigurationSample);

    virtual float CalculateRelevanceFactor(const CHeadConfigurationSample& headConfiguration, const CLocationState& location);
protected:
    bool Execute() override;

    const CScreenLineRelevanceWeightingFunctionInterface* m_pRelevanceWeightingFunctionObject;
    CRenderingUnit m_renderingUnit;
    std::vector<CHeadConfigurationSample*> m_headConfigurationSamples;
    const CRobotControlInterface* m_pRobotControl;
    CLocationState m_locationState;
};

