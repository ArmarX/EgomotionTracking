#include "UncertaintyModelScatteringFactorCalculator.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>

CUncertaintyModelScatteringFactorCalculator::CUncertaintyModelScatteringFactorCalculator() :
    m_minScatteringFactor(0.0), m_maxScatteringFactor(1.0),
    m_scatteringFactor(0.0), m_agingCoefficient(10)
{
}

float CUncertaintyModelScatteringFactorCalculator::CalculateAgedScatteringFactor() const
{
    const float deltaScatteringFactor = (m_maxScatteringFactor - m_minScatteringFactor) / m_agingCoefficient;
    return Calculations::SaturizeToLimits(m_scatteringFactor - deltaScatteringFactor, m_minScatteringFactor, m_maxScatteringFactor);
}

void CUncertaintyModelScatteringFactorCalculator::Update(float scatteringFactor)
{
    const float agedScatteringFactor = CalculateAgedScatteringFactor();
    const float saturizedInputScatteringFactor = Calculations::SaturizeToLimits(scatteringFactor, m_minScatteringFactor, m_maxScatteringFactor);
    m_scatteringFactor = std::max(agedScatteringFactor, saturizedInputScatteringFactor);
}

float CUncertaintyModelScatteringFactorCalculator::GetScatteringFactor() const
{
    return m_scatteringFactor;
}

void CUncertaintyModelScatteringFactorCalculator::SetScatteringRange(float minScatteringFactor, float maxScatteringFactor)
{
    m_minScatteringFactor = minScatteringFactor;
    m_maxScatteringFactor = maxScatteringFactor;
    m_scatteringFactor = minScatteringFactor;
}

void CUncertaintyModelScatteringFactorCalculator::SetAgingCoefficient(float agingCoefficient)
{
    m_agingCoefficient = agingCoefficient;
}
