#pragma once

#include <EgomotionTracking/components/EgomotionTracking/Helpers/Profiling.h>

class CUncertaintyModelScatteringFactorCalculator
{
public:
    CUncertaintyModelScatteringFactorCalculator();
    void Update(float scatteringFactor);
    float GetScatteringFactor() const;

    void SetScatteringRange(float minScatteringFactor, float maxScatteringFactor);
    void SetAgingCoefficient(float agingCoefficient);
protected:
    float CalculateAgedScatteringFactor() const;

    float m_minScatteringFactor;
    float m_maxScatteringFactor;
    float m_scatteringFactor;

    float m_agingCoefficient;

};

