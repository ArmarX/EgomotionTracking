#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/RandomGenerator/RandomGenerator.h>
class CNeckErrorUncertaintyModel
{
public:
    CNeckErrorUncertaintyModel();
    enum UncertaintyMode
    {
        eNoNeckErrorUncertainty,
        eRelativeGaussianNeckUncertainty,
        eAbsoluteUniformNeckUncertainty
    };

    void AddNeckErrorUncertaintyToParticle(CLocationParticle& locationParticle, const CRandomGenerator* pRandomGenerator) const;
    void AddNeckErrorUncertaintyToParticle(CLocationParticle& locationParticle, const CRandomGenerator* pRandomGenerator, UncertaintyMode uncertaintyMode) const;

    void SetErrorLimits(float neckErrorRollMin,
                        float neckErrorRollMax,
                        float neckErrorPitchMin,
                        float neckErrorPitchMax,
                        float neckErrorYawMin,
                        float neckErrorYawMax);

    float GetMinRollError() const;
    float GetMaxRollError() const;
    float GetMinPitchError() const;
    float GetMaxPitchError() const;
    float GetMinYawError() const;
    float GetMaxYawError() const;

    void SetSigmaRoll(float sigmaRoll);
    void SetSigmaPitch(float sigmaPitch);
    void SetSigmaYaw(float sigmaYaw);

    float GetSigmaRoll() const;
    float GetSigmaPitch() const;
    float GetSigmaYaw() const;

    void SetUncertaintyMode(UncertaintyMode mode);
    void SetScatteringFactor(float scatteringFactor) const;
protected:
    float m_neckErrorRollMin;
    float m_neckErrorRollMax;
    float m_neckErrorPitchMin;
    float m_neckErrorPitchMax;
    float m_neckErrorYawMin;
    float m_neckErrorYawMax;

    float m_sigmaRoll;
    float m_sigmaPitch;
    float m_sigmaYaw;

    mutable float m_scatteringFactor;

    UncertaintyMode m_uncertaintyMode;
};

