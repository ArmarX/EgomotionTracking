#include "NeckErrorUncertaintyModel.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Conversions.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>
#define INITIALISATION_LIMIT Conversions::DegreeToRadians(10)

CNeckErrorUncertaintyModel::CNeckErrorUncertaintyModel() :
    m_neckErrorRollMin(-INITIALISATION_LIMIT), m_neckErrorRollMax(INITIALISATION_LIMIT),
    m_neckErrorPitchMin(-INITIALISATION_LIMIT), m_neckErrorPitchMax(INITIALISATION_LIMIT),
    m_neckErrorYawMin(-INITIALISATION_LIMIT), m_neckErrorYawMax(INITIALISATION_LIMIT),
    m_sigmaRoll(0.0), m_sigmaPitch(0.0), m_sigmaYaw(0.0), m_scatteringFactor(1.0),
    m_uncertaintyMode(eNoNeckErrorUncertainty)
{
}

void CNeckErrorUncertaintyModel::AddNeckErrorUncertaintyToParticle(CLocationParticle& locationParticle, const CRandomGenerator* pRandomGenerator) const
{
    AddNeckErrorUncertaintyToParticle(locationParticle, pRandomGenerator, m_uncertaintyMode);
}

void CNeckErrorUncertaintyModel::AddNeckErrorUncertaintyToParticle(CLocationParticle& locationParticle, const CRandomGenerator* pRandomGenerator, CNeckErrorUncertaintyModel::UncertaintyMode uncertaintyMode) const
{
    if (uncertaintyMode == eNoNeckErrorUncertainty)
    {
        locationParticle.SetNeckRollError(0.0);
        locationParticle.SetNeckPitchError(0.0);
        locationParticle.SetNeckYawError(0.0);
    }
    else if (uncertaintyMode == eAbsoluteUniformNeckUncertainty)
    {
        const float roll = pRandomGenerator->GetUniformRandomNumber(m_neckErrorRollMin, m_neckErrorRollMax);
        const float pitch = pRandomGenerator->GetUniformRandomNumber(m_neckErrorPitchMin, m_neckErrorPitchMax);
        const float yaw = pRandomGenerator->GetUniformRandomNumber(m_neckErrorYawMin, m_neckErrorYawMax);
        locationParticle.SetNeckRollError(roll);
        locationParticle.SetNeckPitchError(pitch);
        locationParticle.SetNeckYawError(yaw);
    }
    else if (uncertaintyMode == eRelativeGaussianNeckUncertainty)
    {
        const float roll = locationParticle.GetNeckRollError() + pRandomGenerator->GetGaussianRandomNumber(m_scatteringFactor * m_sigmaRoll);
        const float pitch = locationParticle.GetNeckPitchError() + pRandomGenerator->GetGaussianRandomNumber(m_scatteringFactor * m_sigmaPitch);
        const float yaw = locationParticle.GetNeckYawError() + pRandomGenerator->GetGaussianRandomNumber(m_scatteringFactor * m_sigmaYaw);
        locationParticle.SetNeckRollError(Calculations::SaturizeToLimits(roll, m_neckErrorRollMin, m_neckErrorRollMax));
        locationParticle.SetNeckPitchError(Calculations::SaturizeToLimits(pitch, m_neckErrorPitchMin, m_neckErrorPitchMax));
        locationParticle.SetNeckYawError(Calculations::SaturizeToLimits(yaw, m_neckErrorYawMin, m_neckErrorYawMax));
    }
}

void CNeckErrorUncertaintyModel::SetErrorLimits(float neckErrorRollMin, float neckErrorRollMax, float neckErrorPitchMin, float neckErrorPitchMax, float neckErrorYawMin, float neckErrorYawMax)
{
    m_neckErrorRollMin = neckErrorRollMin;
    m_neckErrorRollMax = neckErrorRollMax;
    m_neckErrorPitchMin = neckErrorPitchMin;
    m_neckErrorPitchMax = neckErrorPitchMax;
    m_neckErrorYawMin = neckErrorYawMin;
    m_neckErrorYawMax = neckErrorYawMax;
}

float CNeckErrorUncertaintyModel::GetMinRollError() const
{
    return m_neckErrorRollMin;
}

float CNeckErrorUncertaintyModel::GetMaxRollError() const
{
    return m_neckErrorRollMax;
}

float CNeckErrorUncertaintyModel::GetMinPitchError() const
{
    return m_neckErrorPitchMin;
}

float CNeckErrorUncertaintyModel::GetMaxPitchError() const
{
    return m_neckErrorPitchMax;
}

float CNeckErrorUncertaintyModel::GetMinYawError() const
{
    return m_neckErrorYawMin;
}

float CNeckErrorUncertaintyModel::GetMaxYawError() const
{
    return m_neckErrorYawMax;
}

void CNeckErrorUncertaintyModel::SetSigmaRoll(float sigmaRoll)
{
    m_sigmaRoll = sigmaRoll;
}

void CNeckErrorUncertaintyModel::SetSigmaPitch(float sigmaPitch)
{
    m_sigmaPitch = sigmaPitch;
}

void CNeckErrorUncertaintyModel::SetSigmaYaw(float sigmaYaw)
{
    m_sigmaYaw = sigmaYaw;
}

float CNeckErrorUncertaintyModel::GetSigmaRoll() const
{
    return m_sigmaRoll;
}

float CNeckErrorUncertaintyModel::GetSigmaPitch() const
{
    return m_sigmaPitch;
}

float CNeckErrorUncertaintyModel::GetSigmaYaw() const
{
    return m_sigmaYaw;
}

void CNeckErrorUncertaintyModel::SetUncertaintyMode(CNeckErrorUncertaintyModel::UncertaintyMode mode)
{
    m_uncertaintyMode = mode;
}

void CNeckErrorUncertaintyModel::SetScatteringFactor(float scatteringFactor) const
{
    m_scatteringFactor = scatteringFactor;
}
