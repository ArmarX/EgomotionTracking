/*
 * UncertaintyMotionModelLocationStatePredictor.cpp
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#include "UncertaintyMotionModelLocationStatePredictor.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>

CUncertaintyMotionModelLocationStatePredictor::CUncertaintyMotionModelLocationStatePredictor() :
    m_enabledRobotControlStateUncertainty(false), m_enabledLocationStateUncertainty(false),
    m_pRobotMotionModel(NULL),
    m_pRobotControlStateUncertaintyModel(NULL),
    m_pLocationStateUncertaintyModel(NULL)
{}

CUncertaintyMotionModelLocationStatePredictor::~CUncertaintyMotionModelLocationStatePredictor() {}

void CUncertaintyMotionModelLocationStatePredictor::SetScatteringFactor(float scatteringFactor) const
{
    if (m_pLocationStateUncertaintyModel)
    {
        m_pLocationStateUncertaintyModel->SetScatteringFactor(scatteringFactor);
    }
}

CLocationState CUncertaintyMotionModelLocationStatePredictor::PredictNewRobotLocation(const CLocationState& oldLocationState, const CRobotControlState& robotControlState, const CRandomGenerator* pRandomGenerator) const
{
    CRobotControlState controlState = robotControlState;

    if (m_enabledRobotControlStateUncertainty && m_pRobotControlStateUncertaintyModel != NULL)
    {
        m_pRobotControlStateUncertaintyModel->AddUncertaintyToRobotControlState(controlState, pRandomGenerator);
    }

    CLocationState resultLocation = oldLocationState;

    if (m_pRobotMotionModel != NULL)
    {
        resultLocation = m_pRobotMotionModel->CalculateNewRobotLocation(oldLocationState, controlState);
    }
    else
    {
        COUT_WARNING << "m_pRobotMotionModel == NULL" << std::endl;
    }

    if (m_enabledLocationStateUncertainty && m_pLocationStateUncertaintyModel != NULL)
    {
        m_pLocationStateUncertaintyModel->AddUncertaintyToLocationState(resultLocation, pRandomGenerator);
    }

    return resultLocation;
}
