/*
 * UncertaintyMotionModelLocationStatePredictor.h
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#pragma once

#include "LocationStatePredictorInterface.h"
#include "../MotionModel/RobotMotionModel.h"
#include "../LocationUncertaintyModel/RobotControlStateUncertaintyModel.h"
#include "../LocationUncertaintyModel/LocationStateUncertaintyModel.h"

class CUncertaintyMotionModelLocationStatePredictor : public CLocationStatePredictorInterface
{
public:
    CUncertaintyMotionModelLocationStatePredictor();
    ~CUncertaintyMotionModelLocationStatePredictor() override;

    void SetEnabledLocationStateUncertainty(bool enabledLocationStateUncertainty)
    {
        m_enabledLocationStateUncertainty = enabledLocationStateUncertainty;
    }

    void SetEnabledRobotControlStateUncertainty(bool enabledRobotControlStateUncertainty)
    {
        m_enabledRobotControlStateUncertainty = enabledRobotControlStateUncertainty;
    }

    void SetLocationStateUncertaintyModel(const CLocationStateUncertaintyModel* pLocationStateUncertaintyModel)
    {
        m_pLocationStateUncertaintyModel = pLocationStateUncertaintyModel;
    }

    void SetRobotControlStateUncertaintyModel(const CRobotControlStateUncertaintyModel* pRobotControlStateUncertaintyModel)
    {
        m_pRobotControlStateUncertaintyModel = pRobotControlStateUncertaintyModel;
    }

    void SetRobotMotionModel(const CRobotMotionModel* pRobotMotionModel)
    {
        m_pRobotMotionModel = pRobotMotionModel;
    }

    void SetScatteringFactor(float scatteringFactor) const override;

    CLocationState PredictNewRobotLocation(const CLocationState& oldLocationState, const CRobotControlState& robotControlState, const CRandomGenerator* pRandomGenerator) const override;
protected:
    bool m_enabledRobotControlStateUncertainty;
    bool m_enabledLocationStateUncertainty;

    const CRobotMotionModel* m_pRobotMotionModel;
    const CRobotControlStateUncertaintyModel* m_pRobotControlStateUncertaintyModel;
    const CLocationStateUncertaintyModel* m_pLocationStateUncertaintyModel;
};

