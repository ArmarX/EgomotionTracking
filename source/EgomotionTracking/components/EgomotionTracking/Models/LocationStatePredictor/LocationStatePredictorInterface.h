/*
 * LocationStatePredictorInterface.h
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/RobotControlState.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/RandomGenerator/RandomGenerator.h>

class CLocationStatePredictorInterface
{
public:
    CLocationStatePredictorInterface();
    virtual ~CLocationStatePredictorInterface();
    virtual CLocationState PredictNewRobotLocation(const CLocationState& oldLocationState, const CRobotControlState& robotControlState, const CRandomGenerator* pRandomGenerator) const = 0;

    virtual void SetScatteringFactor(float scatteringFactor) const {}
};

