#include "DynamicElementStatusUncertaintyModel.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Conversions.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>

DynamicElementStatusUncertaintyModel::DynamicElementStatusUncertaintyModel() :
    sigmaAlpha(0.0), scatteringFactor(1.0),
    uncertaintyMode(noUncertainty)
{

}

void DynamicElementStatusUncertaintyModel::AddUncertaintyToDynamicElementParticle(DynamicElementParticle& dynamicElementParticle, const CRandomGenerator* randomGenerator) const
{
    AddUncertaintyToDynamicElementParticle(dynamicElementParticle, randomGenerator, uncertaintyMode);
}

void DynamicElementStatusUncertaintyModel::AddUncertaintyToDynamicElementParticle(DynamicElementParticle& dynamicElementParticle, const CRandomGenerator* randomGenerator,
        UncertaintyMode uncertaintyMode) const
{
    if (uncertaintyMode == noUncertainty)
    {

    }
    else if (uncertaintyMode == absoluteUniformUncertainty)
    {
        const float alpha = randomGenerator->GetUniformRandomNumber();
        dynamicElementParticle.setAlpha(alpha);
    }
    else if (uncertaintyMode == relativeGaussianUncertainty)
    {
        const float alpha = dynamicElementParticle.getAlpha() + randomGenerator->GetGaussianRandomNumber(scatteringFactor * sigmaAlpha);
        dynamicElementParticle.setAlpha(Calculations::SaturizeToLimits(alpha, 0, 1));
    }
}

void DynamicElementStatusUncertaintyModel::SetSigmaAlpha(float sigmaAlpha)
{
    this->sigmaAlpha = sigmaAlpha;
}

float DynamicElementStatusUncertaintyModel::GetSigmaAlpha() const
{
    return sigmaAlpha;
}

void DynamicElementStatusUncertaintyModel::SetUncertaintyMode(UncertaintyMode uncertaintyMode)
{
    this->uncertaintyMode = uncertaintyMode;
}

void DynamicElementStatusUncertaintyModel::SetScatteringFactor(float scatteringFactor) const
{
    this->scatteringFactor = scatteringFactor;
}
