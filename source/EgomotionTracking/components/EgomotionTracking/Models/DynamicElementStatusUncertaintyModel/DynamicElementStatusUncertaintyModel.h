#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/DynamicElementParticle.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/RandomGenerator/RandomGenerator.h>

class DynamicElementStatusUncertaintyModel
{
public:
    DynamicElementStatusUncertaintyModel();
    enum UncertaintyMode
    {
        noUncertainty,
        relativeGaussianUncertainty,
        absoluteUniformUncertainty
    };
    void AddUncertaintyToDynamicElementParticle(DynamicElementParticle& dynamicElementParticle, const CRandomGenerator* randomGenerator) const;
    void AddUncertaintyToDynamicElementParticle(DynamicElementParticle& dynamicElementParticle, const CRandomGenerator* randomGenerator, UncertaintyMode uncertaintyMode) const;
    void SetSigmaAlpha(float sigmaAlpha);
    float GetSigmaAlpha() const;
    void SetUncertaintyMode(UncertaintyMode mode);
    void SetScatteringFactor(float scatteringFactor) const;

protected:
    float minAlpha;
    float maxAlpha;
    float sigmaAlpha;
    mutable float scatteringFactor;
    UncertaintyMode uncertaintyMode;
};

