/*
 * ObservationModelInterface.cpp
 *
 *  Created on: 19.03.2013
 *      Author: abyte
 */

#include "ObservationModelInterface.h"

CObservationModelInterface::CObservationModelInterface() :
    m_pVisualizationImage(NULL),
    m_pInputVisualizationImageBackground(NULL),
    m_pEdgeImage(NULL),
    m_pEdgeGradientOrientation(NULL),
    m_pEdgeGradient(NULL),
    m_needRenderedJunctionPoints(false),
    m_sensibility(1.0)
{
}

CObservationModelInterface::~CObservationModelInterface()
{
}

bool CObservationModelInterface::GetNeedRenderedJunctionPoints() const
{
    return m_needRenderedJunctionPoints;
}

void CObservationModelInterface::SetInputVisualizationImageBackground(const CByteImage* pVisualizationImageBackground)
{
    m_pInputVisualizationImageBackground = pVisualizationImageBackground;
}

