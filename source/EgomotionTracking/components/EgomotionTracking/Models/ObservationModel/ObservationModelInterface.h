/*
 * ObservationModelInterface.h
 *
 *  Created on: 19.03.2013
 *      Author: abyte
 */

#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/FilterParticleInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotControlInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ImageProcessor/TrackingImageProcessorInterface.h>

class CObservationModelInterface
{
public:
    CObservationModelInterface();
    virtual ~CObservationModelInterface();

    virtual float CalculateImportanceWeight(const FilterParticleInterface& filterParticle, bool drawVisualizationImage = false) const = 0;

    bool GetNeedRenderedJunctionPoints() const;

    float GetSensibility() const
    {
        return m_sensibility;
    }

    void SetSensibility(float sensibility)
    {
        if (sensibility <= 0.0)
        {
            sensibility = FLT_EPSILON;
        }
        m_sensibility = sensibility;
        UpdateSensibility();
    }

    const CByteImage* GetVisualizationImage() const
    {
        return m_pVisualizationImage;
    }

    void SetVisualizationImage(CByteImage* pVisualizationImage)
    {
        m_pVisualizationImage = pVisualizationImage;
    }

    void SetInputVisualizationImageBackground(const CByteImage* pVisualizationImageBackground);

    void SetMeasurement(const CByteImage* pEdgeImage, const float* pEdgeGradientOrientation, float** pEdgeGradient)
    {
        m_pEdgeImage = pEdgeImage;
        m_pEdgeGradientOrientation = pEdgeGradientOrientation;
        m_pEdgeGradient = pEdgeGradient;
    }

protected:
    virtual void UpdateSensibility() {};
    CByteImage* m_pVisualizationImage;
    const CByteImage* m_pInputVisualizationImageBackground;
    //const CTrackingImageProcessorInterface* m_pTrackingImageProcessor;

    const CByteImage* m_pEdgeImage;
    const float* m_pEdgeGradientOrientation;
    float** m_pEdgeGradient;

    bool m_needRenderedJunctionPoints;
private:
    float m_sensibility;
};

