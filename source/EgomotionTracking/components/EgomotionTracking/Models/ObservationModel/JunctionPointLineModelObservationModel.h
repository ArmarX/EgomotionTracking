/*
 * JunctionPointLineModelObservationModel.h
 *
 *  Created on: 19.03.2013
 *      Author: abyte
 */

#pragma once

#include "LineModelObservationModelInterface.h"

#include <EgomotionTracking/components/EgomotionTracking/Helpers/Conversions.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>

class CJunctionPointLineModelObservationModel : public CLineModelObservationModelInterface
{
public:
    CJunctionPointLineModelObservationModel();
    ~CJunctionPointLineModelObservationModel() override;

    float CalculateImportanceWeight(const FilterParticleInterface& filterParticle, bool drawVisualizationImage = false) const override;

    enum CLikelihoodMode
    {
        eGaussianLikelihood,
        eInlierOutlierLikelihoodPerElement,
        eInlierOutlierLikelihoodPerNormal
    };

    enum CSamplingMode
    {
        eSampleAllLines,
        eSampleJunctionPoints
    };

    enum CGaussianWeightingMode
    {
        eWeightAll,
        eSmoothedHistogramWeighting
    };

    enum CNumberOfNormalsMode
    {
        eFixedNumberOfNormals,
        eLengthDependendNumberOfNormals
    };

    CLikelihoodMode GetLikelihoodMode() const;
    void SetLikelihoodMode(const CLikelihoodMode& likelihoodMode);

    CSamplingMode GetSamplingMode() const;
    void SetSamplingMode(const CSamplingMode& samplingMode);

    CGaussianWeightingMode GetGaussianWeightingMode() const;
    void SetGaussianWeightingMode(CGaussianWeightingMode weightingMode);

    CNumberOfNormalsMode GetNumberOfNormalsMode() const;
    void SetNumberOfNormalsMode(CNumberOfNormalsMode numberOfNormalsMode);

    float GetDeltaAlpha() const;
    void SetDeltaAlpha(float deltaAlpha);

    float GetDesiredPDeltaAlpha() const;
    void SetDesiredPDeltaAlpha(float desiredPDeltaAlpha);

    float GetDesiredPMin() const;
    void SetDesiredPMin(float desiredPMin);

    float GetGaussianHistogrammSmoothingQuantileFactor() const;
    void SetGaussianHistogrammSmoothingQuantileFactor(float factor);

    float GetGaussianHistogrammSmoothingSigmaFactor() const;
    void SetGaussianHistogrammSmoothingSigmaFactor(float factor);

    /* Getter/Setter for useNormalOrientationDifferenceMeasure */
    bool GetUseNormalOrientationDifferenceMeasure() const;
    void SetUseNormalOrientationDifferenceMeasure(bool useOrientationDifferenceMeasure);

    /**
      Sets the scalar multiplied with the normal orientation difference to control its importance.
      \param normalOrientationDifferenceScalar The scalar multiplied with the normal orientation difference.
    */
    void setNormalOrientationDifferenceScalar(float normalOrientationDifferenceScalar);
    /**
      Sets the factor of the normal orientation difference used to control its importance.
      \param normalOrientationDifferenceFactor The factor of the normal orientation difference.
    */
    void setNormalOrientationDifferenceFactor(float normalOrientationDifferenceFactor);

    void DrawParticle(const FilterParticleInterface& particle, bool zeroVisualizationImage, int r, int g, int b);

    void DrawParticles(const std::vector<DynamicElementParticle>& particles, bool zeroVisualizationImage, int r, int g, int b);

    void DrawLines(const std::vector<CScreenLine>& screenLines, bool zeroVisualizationImage, int r, int g, int b);
    void DrawTriangles(const std::vector<CFaceTriangle>& screenFaceTriangles, bool zeroVisualizationImage);

protected:

    float CalculateWeightWithGaussianLikelihood(const FilterParticleInterface& filterParticle, bool drawVisualizationImage) const;
    float CalculateWeightWithInlierOutlierLikelihood(const FilterParticleInterface& filterParticle, bool drawVisualizationImage) const;

    inline unsigned int getNumberOfNormals(float lineLength) const
    {
        if (m_numberOfNormalsMode == eFixedNumberOfNormals)
        {
            return m_numberOfNormals;
        }
        /* return m_numberOfNormals per 100px */
        const unsigned int numberOfNormals =  Conversions::RoundFloatToNearestInt(m_numberOfNormals * (lineLength / 100.0));
        if (numberOfNormals == 0)
        {
            return 1;
        }
        return numberOfNormals;
    }

    void RecalculateParameters();
    void UpdateSensibility() override
    {
        RecalculateParameters();
    }

    CLikelihoodMode m_likelihoodMode;
    CSamplingMode m_samplingMode;
    CGaussianWeightingMode m_gaussianWeightingMode;
    CNumberOfNormalsMode m_numberOfNormalsMode;

    float m_sigmaGaussianLikelihood;
    float m_sigmaInlierOutlierLikelihood;

    float m_normalLengthGaussianLikelihood;
    float m_normalLengthInlierOutlierLikelihood;

    /* design Parameters */
    float m_deltaAlpha;
    float m_desiredPDeltaAlpha;
    float m_desiredPMin;

    float m_gaussianHistogrammSmoothingQuantileFactor;
    float m_gaussianHistogrammSmoothingSigmaFactor;

    bool useNormalOrientationDifferenceMeasure; /**< TRUE if the normal orientation difference should also be used as a measure to calculate the weight. */
    float m_normalOrientationDifferenceScalar; /**< Scalar multiplied with the orientation difference to control its importance. */
    float m_normalOrientationDifferenceFactor; /**< Factor of the orientation difference used to control its importance. */

    float m_sigmaNormalOrientationDifference;
};

