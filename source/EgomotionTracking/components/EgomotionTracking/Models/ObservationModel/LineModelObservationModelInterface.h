/*
 * LineModelObservationModelInterface.h
 *
 *  Created on: 19.03.2013
 *      Author: abyte
 */

#pragma once

#include "ObservationModelInterface.h"
#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/Line.h>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/PrimitiveDrawer.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Conversions.h>

#include <Inventor/SbVec3f.h>
#include <Inventor/SbMatrix.h>

#include <EgomotionTracking/components/RVL/Representation/ModelBased/GeometricGraph/Base/ModelEdge.h>

class CLineModelObservationModelInterface: public CObservationModelInterface
{
public:
    CLineModelObservationModelInterface();
    ~CLineModelObservationModelInterface() override;

    inline float CalculateSumOfSquaredNormalDistances(const CLine& line, unsigned int numberOfNormals, float maxNormalDistance, bool drawVisualizationImage,
            bool useNormalOrientationDifferenceMeasure, float& sumOfDotProducts) const
    {
        float saliencyWeight = ((CScreenLine*)&line)->m_pModelEdgeReference->GetSaliencyWeight();

        const float normalIntervalLength = line.CalculateLength2d() / (numberOfNormals + 1);

        const SbVec2f lineDirection = line.GetDirection2d();
        const SbVec2f normalDirection(-lineDirection[1], lineDirection[0]);

        float** pImageGradients = m_pEdgeGradient;
        const CByteImage* pEdgeImage = m_pEdgeImage;

        const float maxSquaredNormalDistance = maxNormalDistance * maxNormalDistance;

        float squaredNormalDistanceSum = 0.0;

        SbVec2f currentCheckPoint = line.GetP1SbVec2f();
        for (unsigned int i = 0; i < numberOfNormals; ++i)
        {
            currentCheckPoint += normalIntervalLength * lineDirection;
            const SbVec2f checkLineEndpoint1 = currentCheckPoint + maxNormalDistance * normalDirection;
            const SbVec2f checkLineEndpoint2 = currentCheckPoint - maxNormalDistance * normalDirection;

            SbVec2f nearestEdgePointDirection1(0.0, 0.0);
            SbVec2f nearestEdgePointDirection2(0.0, 0.0);
            SbVec2f nearestEdgePoint(0.0, 0.0);

            const bool foundEdgeInDirection1 = FindNearestMatchedEdgeOnLine(currentCheckPoint, checkLineEndpoint1, pEdgeImage, nearestEdgePointDirection1);
            const bool foundEdgeInDirection2 = FindNearestMatchedEdgeOnLine(currentCheckPoint, checkLineEndpoint2, pEdgeImage, nearestEdgePointDirection2);

            float currentSquaredNormalDistance = maxSquaredNormalDistance;
            if (foundEdgeInDirection1)
            {
                const SbVec2f displacement = nearestEdgePointDirection1 - currentCheckPoint;
                const float squaredDist = displacement.dot(displacement);

                currentSquaredNormalDistance = squaredDist;
                nearestEdgePoint = nearestEdgePointDirection1;
            }
            if (foundEdgeInDirection2)
            {
                const SbVec2f displacement = nearestEdgePointDirection2 - currentCheckPoint;
                const float squaredDist = displacement.dot(displacement);

                if (!foundEdgeInDirection1 || squaredDist < currentSquaredNormalDistance)
                {
                    currentSquaredNormalDistance = squaredDist;
                    nearestEdgePoint = nearestEdgePointDirection2;
                }
            }

            squaredNormalDistanceSum += currentSquaredNormalDistance * saliencyWeight;

            SbVec2f gradient;
            if (useNormalOrientationDifferenceMeasure)
            {
                if (foundEdgeInDirection1 || foundEdgeInDirection2)
                {
                    int imageGradientsBufferLocation = Calculations::CalculatePixelBufferLocation(nearestEdgePoint[0], nearestEdgePoint[1], pEdgeImage->width);
                    gradient.setValue(pImageGradients[imageGradientsBufferLocation][0], pImageGradients[imageGradientsBufferLocation][1]);
                    gradient.normalize();
                    float dotProduct = gradient.dot(normalDirection);
                    sumOfDotProducts += fabs(dotProduct);
                }
            }

            if (drawVisualizationImage)
            {
                if (!useNormalOrientationDifferenceMeasure)
                {
                    if (foundEdgeInDirection1 == true || foundEdgeInDirection2 == true)
                    {
                        PrimitiveDrawer::DrawLine(currentCheckPoint, nearestEdgePoint, 0, 255, 0, 1, m_pVisualizationImage);
                    }
                    else
                    {
                        PrimitiveDrawer::DrawLine(checkLineEndpoint1, checkLineEndpoint2, 255, 0, 0, 1, m_pVisualizationImage);
                    }
                }
                else
                {
                    /* Draw normal/gradient - VISUALIZATION. */
                    if (foundEdgeInDirection1 == true || foundEdgeInDirection2 == true)
                    {
                        /* Draw normal. */
                        PrimitiveDrawer::DrawLine(currentCheckPoint, currentCheckPoint + 10 * normalDirection, 0, 255, 0, 1, m_pVisualizationImage);
                        /* Draw gradient. */
                        PrimitiveDrawer::DrawLine(nearestEdgePoint, nearestEdgePoint + 10 * gradient, 255, 0, 0, 1, m_pVisualizationImage);
                    }
                }
            }
        }

        if (drawVisualizationImage)
        {

            PrimitiveDrawer::DrawLine(line.m_p1, line.m_p2, 0, 0, 255, 1, m_pVisualizationImage);
        }

        return squaredNormalDistanceSum;
    }

    inline unsigned int CalculateNumberOfMatchedNormals(const CLine& line, unsigned int numberOfNormals, float maxNormalLength, bool drawVisualizationImage) const
    {
        const float normalIntervalLength = line.CalculateLength2d() / (numberOfNormals + 1);

        const SbVec2f lineDirection = line.GetDirection2d();
        const SbVec2f normalDirection(-lineDirection[1], lineDirection[0]);

        const CByteImage* pEdgeImage = m_pEdgeImage;

        unsigned int numberOfMatchedNormals = 0;

        SbVec2f currentCheckPoint = line.GetP1SbVec2f();
        for (unsigned int i = 0; i < numberOfNormals; ++i)
        {
            currentCheckPoint += normalIntervalLength * lineDirection;
            const SbVec2f checkLineEndpoint1 = currentCheckPoint + maxNormalLength * normalDirection;
            const SbVec2f checkLineEndpoint2 = currentCheckPoint - maxNormalLength * normalDirection;

            SbVec2f nearestEdgePoint;
            bool foundEdgeInImage = FindNearestMatchedEdgeOnLine(currentCheckPoint, checkLineEndpoint1, pEdgeImage, nearestEdgePoint);

            if (!foundEdgeInImage)
            {
                foundEdgeInImage = FindNearestMatchedEdgeOnLine(currentCheckPoint, checkLineEndpoint2, pEdgeImage, nearestEdgePoint);
            }
            if (foundEdgeInImage)
            {
                ++numberOfMatchedNormals;
            }

            if (drawVisualizationImage)
            {
                if (foundEdgeInImage)
                {
                    PrimitiveDrawer::DrawLine(checkLineEndpoint1, checkLineEndpoint2, 0, 255, 0, 1, m_pVisualizationImage);
                    PrimitiveDrawer::DrawCircle(nearestEdgePoint, 1, 2, 255, 255, 0, m_pVisualizationImage);
                }
                else
                {
                    PrimitiveDrawer::DrawLine(checkLineEndpoint1, checkLineEndpoint2, 255, 0, 0, 1, m_pVisualizationImage);
                }
            }
        }
        if (drawVisualizationImage)
        {

            PrimitiveDrawer::DrawLine(line.m_p1, line.m_p2, 0, 0, 255, 1, m_pVisualizationImage);
        }
        return numberOfMatchedNormals;
    }
    inline bool FindNearestMatchedEdgeOnLine(const SbVec2f& linePoint1, const SbVec2f& linePoint2, const CByteImage* pEdgeImage, SbVec2f& foundEdgePoint) const
    {
        /* use Bresenham line algorithm to iterate over points on line */

        const int x1 = Conversions::RoundFloatToNearestInt(linePoint1[0]);
        const int x2 = Conversions::RoundFloatToNearestInt(linePoint2[0]);
        const int y1 = Conversions::RoundFloatToNearestInt(linePoint1[1]);
        const int y2 = Conversions::RoundFloatToNearestInt(linePoint2[1]);

        const int dx = x2 - x1;
        const int dy = y2 - y1;

        const int sx = dx >= 0 ? 1 : -1;
        const int sy = dy >= 0 ? 1 : -1;

        int x = x1;
        int y = y1;

        int err, pdx, pdy;
        int ddx, ddy;
        int pdErr, ddErr;

        const int absDx = abs(dx);
        const int absDy = abs(dy);

        if (absDx > absDy)
        {
            err = absDx / 2;
            pdx = sx;
            pdy = 0;
            ddx = 0;
            ddy = sy;
            pdErr = -absDy;
            ddErr = absDx;
        }
        else
        {
            err = absDy / 2;
            pdx = 0;
            pdy = sy;
            ddx = sx;
            ddy = 0;
            pdErr = -absDx;
            ddErr = absDy;
        }

        while (!(x == x2 && y == y2))
        {
            if (CheckPointIsMatchedEdge(x, y, pEdgeImage))
            {
                foundEdgePoint = SbVec2f(x, y);
                return true;
            }

            if (err < 0)
            {
                x += ddx;
                y += ddy;
                err += ddErr;
            }
            else
            {
                x += pdx;
                y += pdy;
                err += pdErr;
            }
        }
        if (CheckPointIsMatchedEdge(x, y, pEdgeImage))
        {
            foundEdgePoint = SbVec2f(x, y);
            return true;
        }

        return false;
    }

    inline bool CheckPointIsMatchedEdge(int pointX, int pointY, const CByteImage* pEdgeImage) const
    {
        if (pointX < 0 || pointY < 0 || pointX >= pEdgeImage->width || pointY >= pEdgeImage->height)
        {
            return false;
        }

        const int pixelBufferLocation = Calculations::CalculatePixelBufferLocation(pointX, pointY, pEdgeImage->width);
        const unsigned char imagePixelValue = pEdgeImage->pixels[pixelBufferLocation];

        if (imagePixelValue > 100)
        {
            return true;
        }
        return false;
    }

    float GetMaxNormalDistance() const;
    void SetMaxNormalDistance(float maxNormalDistance);
    unsigned int GetNumberOfNormals() const;
    void SetNumberOfNormals(unsigned int numberOfNormals);
    unsigned int GetMinNumberOfVisibleModelElements() const;
    void SetMinNumberOfVisibleModelElements(unsigned int minNumberOfVisibleModelElements);

    float GetSigma() const;
    void SetSigma(float sigma);

protected:

    float m_edgeOrientationTolerance;

    float m_maxNormalDistance;
    unsigned int m_numberOfNormals;
    unsigned int m_minNumberOfVisibleModelElements;
};

