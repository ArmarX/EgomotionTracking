/*
 * JunctionPointLineModelObservationModel.cpp
 *
 *  Created on: 19.03.2013
 *      Author: abyte
 */

#include "JunctionPointLineModelObservationModel.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Statistics.h>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/PrimitiveDrawer.h>
#include <numeric>

//XXX
const float focalLengthX = 525;

CJunctionPointLineModelObservationModel::CJunctionPointLineModelObservationModel() :
    m_likelihoodMode(eGaussianLikelihood), m_samplingMode(eSampleJunctionPoints), m_gaussianWeightingMode(eWeightAll), m_numberOfNormalsMode(eFixedNumberOfNormals),
    useNormalOrientationDifferenceMeasure(false), m_normalOrientationDifferenceScalar(0.0), m_normalOrientationDifferenceFactor(0.0)
{
    m_deltaAlpha = 1.0;
    m_desiredPDeltaAlpha = 0.1;
    m_desiredPMin = 0.00001;

    m_gaussianHistogrammSmoothingQuantileFactor = 0.0;
    m_gaussianHistogrammSmoothingSigmaFactor = 0.0;

    m_needRenderedJunctionPoints = true;
}

CJunctionPointLineModelObservationModel::~CJunctionPointLineModelObservationModel()
{
}

float CJunctionPointLineModelObservationModel::CalculateImportanceWeight(const FilterParticleInterface& filterParticle, bool drawVisualizationImage) const
{

    if (drawVisualizationImage && m_pVisualizationImage != NULL && m_pInputVisualizationImageBackground != NULL)
    {
        ImageProcessor::Zero(m_pVisualizationImage);
        ImageProcessor::ConvertImage(m_pInputVisualizationImageBackground, m_pVisualizationImage);
        //CPrimitiveDrawer::DrawLines(locationParticle.GetScreenLines(), 255, 255, 0, 1, m_pVisualizationImage);
    }
    else
    {
        drawVisualizationImage = false;
    }

    if (m_likelihoodMode == eGaussianLikelihood)
    {
        return CalculateWeightWithGaussianLikelihood(filterParticle, drawVisualizationImage);
    }
    else
    {
        return CalculateWeightWithInlierOutlierLikelihood(filterParticle, drawVisualizationImage);
    }
}

float CJunctionPointLineModelObservationModel::CalculateWeightWithGaussianLikelihood(const FilterParticleInterface& filterParticle, bool drawVisualizationImage) const
{
    /* Collect distances */
    std::vector<float> collectedDistances;
    collectedDistances.reserve(100);

    unsigned int numberOfNormalsChecked = 0;

    float sumOfDotProducts = 0.0;

    if (m_samplingMode == eSampleJunctionPoints)
    {
        if (filterParticle.GetScreenJunctionPoints().size() > m_minNumberOfVisibleModelElements)
        {
            for (std::vector<CJunctionPoint>::const_iterator jpIterator = filterParticle.GetScreenJunctionPoints().begin(); jpIterator != filterParticle.GetScreenJunctionPoints().end(); ++jpIterator)
            {
                if (jpIterator->m_junctionLines.size() > 0)
                {

                    for (std::list<CLine>::const_iterator lineIterator = jpIterator->m_junctionLines.begin(); lineIterator != jpIterator->m_junctionLines.end(); ++lineIterator)
                    {
                        collectedDistances.push_back(CalculateSumOfSquaredNormalDistances(*lineIterator, m_numberOfNormals, m_normalLengthGaussianLikelihood, drawVisualizationImage,
                                                     useNormalOrientationDifferenceMeasure, sumOfDotProducts));
                        numberOfNormalsChecked += m_numberOfNormals;
                    }
                }
            }
        }
    }
    else if (m_samplingMode == eSampleAllLines)
    {
        if (filterParticle.GetScreenLines().size() > m_minNumberOfVisibleModelElements)
        {
            for (std::vector<CScreenLine>::const_iterator lineIterator = filterParticle.GetScreenLines().begin(); lineIterator != filterParticle.GetScreenLines().end(); ++lineIterator)
            {
                const unsigned int numberOfNormals = getNumberOfNormals(lineIterator->CalculateLength2d());
                collectedDistances.push_back(CalculateSumOfSquaredNormalDistances(*lineIterator, numberOfNormals, m_normalLengthGaussianLikelihood, drawVisualizationImage,
                                             useNormalOrientationDifferenceMeasure, sumOfDotProducts));
                numberOfNormalsChecked += numberOfNormals;
            }
        }
    }

    if (m_gaussianWeightingMode == eWeightAll)
    {
        const float overallSum = std::accumulate(collectedDistances.begin(), collectedDistances.end(), 0.0);
        float squaredMean;
        float normalOrientationDifferenceMeasure = 0;

        if (collectedDistances.size() == 0)
        {
            squaredMean = m_normalLengthGaussianLikelihood * m_normalLengthGaussianLikelihood;
        }
        else
        {
            if (numberOfNormalsChecked > 0)
            {
                squaredMean = overallSum / (numberOfNormalsChecked);
            }
            else
            {
                squaredMean = 0;
            }
            if (useNormalOrientationDifferenceMeasure)
            {
                float meanSumOfDotProducts = sumOfDotProducts / numberOfNormalsChecked;
                //                normalOrientationDifferenceMeasure = m_normalOrientationDifferenceScalar * (1 - pow(meanSumOfDotProducts, m_normalOrientationDifferenceFactor));

                normalOrientationDifferenceMeasure = 1 - meanSumOfDotProducts;

                //                std::cout << "SquaredMean: " << squaredMean << " - NormalOrientationDifferenceMeasure: " << normalOrientationDifferenceMeasure << std::endl;
            }
        }

        float weight;
        if (!useNormalOrientationDifferenceMeasure)
        {
            weight = exp(-m_sigmaGaussianLikelihood * squaredMean);
        }
        else
        {
            // Option 01
            //            float weight2 = exp(-m_sigmaNormalOrientationDifference * normalOrientationDifferenceMeasure);

            //            float weight1 = exp(-m_sigmaGaussianLikelihood * squaredMean);

            //            weight = weight1 * weight2;

            weight = exp(-m_sigmaNormalOrientationDifference * normalOrientationDifferenceMeasure) * exp(-m_sigmaGaussianLikelihood * squaredMean);

            //            std::cout << "Weight: " << weight << " - Weight1: " << weight1 << " - Weight2: " << weight2 << std::endl;

            // Option 02
            //weight = exp(-m_sigmaGaussianLikelihood * squaredMean * normalOrientationDifferenceMeasure);
        }

        return weight;

    }
    else if (m_gaussianWeightingMode == eSmoothedHistogramWeighting)
    {
        float squaredMean;
        if (collectedDistances.size() == 0)
        {
            squaredMean = m_normalLengthGaussianLikelihood * m_normalLengthGaussianLikelihood;
        }
        else
        {
            std::sort(collectedDistances.begin(), collectedDistances.end());

            std::list<float> distancesWeights;
            const float sumOfDistancesWeights = Statistics::CalculateHistogrammQuantileSmoothingWeights(collectedDistances.size(), m_gaussianHistogrammSmoothingQuantileFactor, (1.0 - m_gaussianHistogrammSmoothingQuantileFactor), m_gaussianHistogrammSmoothingSigmaFactor, distancesWeights);

            // XXX
            /* FUER PLOTS zur erklärung!!!

            std::cout << "d: ";
            for(std::vector<float>::const_iterator distanceIterator = collectedDistances.begin(); distanceIterator != collectedDistances.end(); ++distanceIterator)
            {
                std::cout << *distanceIterator << ",";
            }
            std::cout << std::endl << "w: ";

            for(std::list<float>::const_iterator distanceWeightIterator = distancesWeights.begin(); distanceWeightIterator != distancesWeights.end(); ++distanceWeightIterator)
            {
                std::cout << *distanceWeightIterator << ",";
            }
            std::cout << std::endl;

            //XXX
            */

            float overallSum = 0.0;
            std::list<float>::const_iterator distanceWeightIterator = distancesWeights.begin();
            for (std::vector<float>::const_iterator distanceIterator = collectedDistances.begin(); distanceIterator != collectedDistances.end(); ++distanceIterator, ++ distanceWeightIterator)
            {
                overallSum += *distanceIterator** distanceWeightIterator;
            }

            squaredMean = overallSum / (sumOfDistancesWeights * (static_cast<float>(numberOfNormalsChecked) / collectedDistances.size()));
        }
        const float weight = exp(-m_sigmaGaussianLikelihood * squaredMean);
        return weight;
    }

    return 0.0;
}

float CJunctionPointLineModelObservationModel::CalculateWeightWithInlierOutlierLikelihood(const FilterParticleInterface& filterParticle, bool drawVisualizationImage) const
{
    unsigned int elementChecks = 0;
    unsigned int matchedElements = 0;
    const unsigned int branchMatchedNormalsThreshold = m_numberOfNormals - 1;
    if (m_samplingMode == eSampleJunctionPoints)
    {
        if (filterParticle.GetScreenJunctionPoints().size() > m_minNumberOfVisibleModelElements)
        {
            for (std::vector<CJunctionPoint>::const_iterator jpIterator = filterParticle.GetScreenJunctionPoints().begin(); jpIterator != filterParticle.GetScreenJunctionPoints().end(); ++jpIterator)
            {
                if (jpIterator->m_junctionLines.size() > 0)
                {
                    ++elementChecks;
                    bool allBranchesMatched = true;
                    for (std::list<CLine>::const_iterator lineIterator = jpIterator->m_junctionLines.begin(); lineIterator != jpIterator->m_junctionLines.end(); ++lineIterator)
                    {
                        const unsigned int matchedNormals = CalculateNumberOfMatchedNormals(*lineIterator, m_numberOfNormals, m_normalLengthInlierOutlierLikelihood, drawVisualizationImage);
                        if (matchedNormals < branchMatchedNormalsThreshold)
                        {
                            allBranchesMatched = false;
                            break;
                        }
                    }

                    if (allBranchesMatched)
                    {
                        ++matchedElements;
                    }
                }
            }
        }
    }
    else if (m_samplingMode == eSampleAllLines)
    {
        if (filterParticle.GetScreenLines().size() > m_minNumberOfVisibleModelElements)
        {
            for (std::vector<CScreenLine>::const_iterator lineIterator = filterParticle.GetScreenLines().begin(); lineIterator != filterParticle.GetScreenLines().end(); ++lineIterator)
            {
                ++elementChecks;
                const unsigned int numberOfNormals = getNumberOfNormals(lineIterator->CalculateLength2d());

                const unsigned int matchedNormals = CalculateNumberOfMatchedNormals(*lineIterator, numberOfNormals, m_normalLengthInlierOutlierLikelihood, drawVisualizationImage);
                if (matchedNormals >= numberOfNormals - 1)
                {
                    ++matchedElements;
                }

            }
        }
    }

    float outlierRatio;
    if (elementChecks == 0)
    {
        outlierRatio = 1.0;
    }
    else
    {
        const float inlierRatio = static_cast<float>(matchedElements) / elementChecks;
        outlierRatio = 1 - inlierRatio;
    }

    const float weight = exp(-m_sigmaInlierOutlierLikelihood * outlierRatio);
    return weight;
}

void CJunctionPointLineModelObservationModel::RecalculateParameters()
{
    const float dx = tan(m_deltaAlpha / GetSensibility()) * focalLengthX;
    m_sigmaGaussianLikelihood = -log(m_desiredPDeltaAlpha) / (dx * dx);
    m_normalLengthGaussianLikelihood = sqrt(-log(m_desiredPMin) / m_sigmaGaussianLikelihood);
    m_normalLengthInlierOutlierLikelihood = dx;
    m_sigmaInlierOutlierLikelihood = -2 * log(m_desiredPDeltaAlpha);

    //m_sigmaNormalOrientationDifference = -log(0.001);
    m_sigmaNormalOrientationDifference = -log(0.1) / (1 - (std::cos(Conversions::DegreeToRadians(15)) * std::cos(Conversions::DegreeToRadians(15))));
}



CJunctionPointLineModelObservationModel::CSamplingMode CJunctionPointLineModelObservationModel::GetSamplingMode() const
{
    return m_samplingMode;
}

void CJunctionPointLineModelObservationModel::SetSamplingMode(const CSamplingMode& samplingMode)
{
    if (samplingMode == eSampleAllLines)
    {
        m_needRenderedJunctionPoints = false;
    }
    else
    {
        m_needRenderedJunctionPoints = true;
    }
    m_samplingMode = samplingMode;
}

CJunctionPointLineModelObservationModel::CGaussianWeightingMode CJunctionPointLineModelObservationModel::GetGaussianWeightingMode() const
{
    return m_gaussianWeightingMode;
}

void CJunctionPointLineModelObservationModel::SetGaussianWeightingMode(CJunctionPointLineModelObservationModel::CGaussianWeightingMode weightingMode)
{
    m_gaussianWeightingMode = weightingMode;
}

CJunctionPointLineModelObservationModel::CNumberOfNormalsMode CJunctionPointLineModelObservationModel::GetNumberOfNormalsMode() const
{
    return m_numberOfNormalsMode;
}

void CJunctionPointLineModelObservationModel::SetNumberOfNormalsMode(CJunctionPointLineModelObservationModel::CNumberOfNormalsMode numberOfNormalsMode)
{
    m_numberOfNormalsMode = numberOfNormalsMode;
}

CJunctionPointLineModelObservationModel::CLikelihoodMode CJunctionPointLineModelObservationModel::GetLikelihoodMode() const
{
    return m_likelihoodMode;
}

void CJunctionPointLineModelObservationModel::SetLikelihoodMode(const CLikelihoodMode& likelihoodMode)
{
    m_likelihoodMode = likelihoodMode;
}

float CJunctionPointLineModelObservationModel::GetDesiredPMin() const
{
    return m_desiredPMin;
}

void CJunctionPointLineModelObservationModel::SetDesiredPMin(float desiredPMin)
{
    m_desiredPMin = desiredPMin;
    RecalculateParameters();
}

float CJunctionPointLineModelObservationModel::GetGaussianHistogrammSmoothingQuantileFactor() const
{
    return m_gaussianHistogrammSmoothingQuantileFactor;
}

void CJunctionPointLineModelObservationModel::SetGaussianHistogrammSmoothingQuantileFactor(float factor)
{
    m_gaussianHistogrammSmoothingQuantileFactor = factor;
}

float CJunctionPointLineModelObservationModel::GetGaussianHistogrammSmoothingSigmaFactor() const
{
    return m_gaussianHistogrammSmoothingSigmaFactor;
}

void CJunctionPointLineModelObservationModel::SetGaussianHistogrammSmoothingSigmaFactor(float factor)
{
    m_gaussianHistogrammSmoothingSigmaFactor = factor;
}

float CJunctionPointLineModelObservationModel::GetDesiredPDeltaAlpha() const
{
    return m_desiredPDeltaAlpha;
}

void CJunctionPointLineModelObservationModel::SetDesiredPDeltaAlpha(float desiredPDeltaAlpha)
{
    m_desiredPDeltaAlpha = desiredPDeltaAlpha;
    RecalculateParameters();
}

float CJunctionPointLineModelObservationModel::GetDeltaAlpha() const
{
    return m_deltaAlpha;
}

void CJunctionPointLineModelObservationModel::SetDeltaAlpha(float deltaAlpha)
{
    m_deltaAlpha = deltaAlpha;
    RecalculateParameters();
}

bool CJunctionPointLineModelObservationModel::GetUseNormalOrientationDifferenceMeasure() const
{
    return useNormalOrientationDifferenceMeasure;
}

void CJunctionPointLineModelObservationModel::SetUseNormalOrientationDifferenceMeasure(bool useOrientationDifferenceMeasure)
{
    this->useNormalOrientationDifferenceMeasure = useOrientationDifferenceMeasure;
}

void CJunctionPointLineModelObservationModel::setNormalOrientationDifferenceScalar(float normalOrientationDifferenceScalar)
{
    m_normalOrientationDifferenceScalar = normalOrientationDifferenceScalar;
}

void CJunctionPointLineModelObservationModel::setNormalOrientationDifferenceFactor(float normalOrientationDifferenceFactor)
{
    m_normalOrientationDifferenceFactor = normalOrientationDifferenceFactor;
}

void CJunctionPointLineModelObservationModel::DrawParticle(const FilterParticleInterface& particle, bool zeroVisualizationImage, int r, int g, int b)
{
    if (zeroVisualizationImage)
    {
        ImageProcessor::Zero(m_pVisualizationImage);
        ImageProcessor::ConvertImage(m_pInputVisualizationImageBackground, m_pVisualizationImage);
    }

    PrimitiveDrawer::DrawLines(particle.GetScreenLines(), r, g, b, 1, m_pVisualizationImage);
}

void CJunctionPointLineModelObservationModel::DrawParticles(const std::vector<DynamicElementParticle>& particles, bool zeroVisualizationImage, int r, int g, int b)
{
    if (zeroVisualizationImage)
    {
        ImageProcessor::Zero(m_pVisualizationImage);
        ImageProcessor::ConvertImage(m_pInputVisualizationImageBackground, m_pVisualizationImage);
    }

    for (std::vector<DynamicElementParticle>::const_iterator particleIterator = particles.begin(); particleIterator != particles.end(); ++particleIterator)
    {
        DrawParticle(*particleIterator, false, r, g, b);
    }
}

void CJunctionPointLineModelObservationModel::DrawLines(const std::vector<CScreenLine>& screenLines, bool zeroVisualizationImage, int r, int g, int b)
{
    if (zeroVisualizationImage)
    {
        ImageProcessor::Zero(m_pVisualizationImage);
        ImageProcessor::ConvertImage(m_pInputVisualizationImageBackground, m_pVisualizationImage);
    }

    //    PrimitiveDrawer::draw2dLines(screenLines, m_pVisualizationImage, false);
    PrimitiveDrawer::DrawLines(screenLines, r, g, b, 1, m_pVisualizationImage);
}

void CJunctionPointLineModelObservationModel::DrawTriangles(const std::vector<CFaceTriangle>& screenFaceTriangles, bool zeroVisualizationImage)
{
    if (zeroVisualizationImage)
    {
        ImageProcessor::Zero(m_pVisualizationImage);
        ImageProcessor::ConvertImage(m_pInputVisualizationImageBackground, m_pVisualizationImage);
    }

    PrimitiveDrawer::draw2dTriangles(screenFaceTriangles, m_pVisualizationImage);
}
