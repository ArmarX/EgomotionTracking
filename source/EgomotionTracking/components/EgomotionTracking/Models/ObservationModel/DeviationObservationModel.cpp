#include "DeviationObservationModel.h"

DeviationObservationModel::DeviationObservationModel()
{

}

DeviationObservationModel::~DeviationObservationModel()
{

}

float DeviationObservationModel::CalculateImportanceWeight(const CByteImage* pParticleDeviationImage, const CByteImage* pReferenceDeviationImage, const SbVec2f SurfacesPoints[4], const float Sigma)
{
    const int Radius = std::max(int(std::ceil(Sigma * 3.0f)), 1);

    CActiveZone Zone(SurfacesPoints, Radius, pParticleDeviationImage->width - 1, pParticleDeviationImage->height - 1);

    int X0, Y0, X1, Y1;

    Zone.GetOffsetBoundaries(X0, Y0, X1, Y1);

#ifdef _MAKE_MASK_IMAGE

    CByteImage Mask(pParticleDeviationImage->width, pParticleDeviationImage->height, CByteImage::eGrayScale);

    ImageProcessor::Zero(&Mask);

    for (int Y = Y0; Y <= Y1; ++Y)
    {
        unsigned char* pPixel = Mask.pixels + Mask.width * Y + X0;

        for (int X = X0; X <= X1; ++X)
        {
            *pPixel++ = Zone.IsInOffsetSubSpace(X, Y) ? 255 : (Zone.IsInSubSpace(X, Y) ? 128 : 0);
        }
    }

#endif

    CByteImage ParticleImage(pParticleDeviationImage->width, pParticleDeviationImage->height, CByteImage::eGrayScale);
    CByteImage ReferenceImage(pReferenceDeviationImage->width, pReferenceDeviationImage->height, CByteImage::eGrayScale);
    ImageProcessor::ConvertImage(pParticleDeviationImage, &ParticleImage);
    ImageProcessor::ConvertImage(pReferenceDeviationImage, &ReferenceImage);


    const int Diameter = Radius * 2 + 1;
    const int Area = Diameter * Diameter;
    float* const pKernelBase = new float[Area];
    float KernelIntegral = 0.0f;
    const float ExponentFactor = -1.0f / (2.0f * Sigma * Sigma);

    float* pKernel = pKernelBase;

    const float ScalingKernel = 1.0f / exp((Radius * Radius * 2) * ExponentFactor);

    for (int DY = -Radius; DY <= Radius; ++DY)
    {
        for (int DX = -Radius; DX <= Radius; ++DX)
        {
            const float Density = exp((DX * DX + DY * DY) * ExponentFactor) * ScalingKernel;
            *pKernel++ = Density;
            KernelIntegral += Density;
        }
    }

    float Similarity = 0.0f;

    int TotalCommonPixels = 0;

    float StdDevP, StdDevR;

    for (int Y = Y0; Y <= Y1; ++Y)
    {
        for (int X = X0; X <= X1; ++X)
        {
            if (Zone.IsInOffsetSubSpace(X, Y))
            {
                if (GetPixelLocalHomegenity(&ParticleImage, X, Y, Radius, pKernelBase, KernelIntegral, StdDevP))
                {
                    if (GetPixelLocalHomegenity(&ReferenceImage, X, Y, Radius, pKernelBase, KernelIntegral, StdDevR))
                    {
                        Similarity += std::abs(StdDevP - StdDevR);

                        ++TotalCommonPixels;
                    }
                }
            }
        }
    }

    delete[] pKernelBase;

    return TotalCommonPixels ? (Similarity / TotalCommonPixels) : __FLT_MAX__;
}

bool DeviationObservationModel::GetPixelLocalHomegenity(const CByteImage* pImage, const int X, const int Y, const int Radius, const float* pKernelBase, const float KernelIntegral, float& Homegenity)
{
    const int X0 = X - Radius;

    if (X0 < 0)
    {
        return false;
    }

    const int Y0 = Y - Radius;

    if (Y0 < 0)
    {
        return false;
    }

    const int X1 = X + Radius;

    if (X1 >= pImage->width)
    {
        return false;
    }

    const int Y1 = Y + Radius;

    if (Y1 >= pImage->height)
    {
        return false;
    }

    float Accumulator = 0.0f;

    const unsigned char* pPixelBase = pImage->pixels + (pImage->width * Y0) + X0;

    //const float* pKernel = pKernelBase; //unused variable

    for (int YS = Y0; YS <= Y1; ++YS, pPixelBase += pImage->width)
    {
        const unsigned char* pPixel = pPixelBase;

        for (int XS = X0; XS <= X1; ++XS)
        {
            Accumulator += float(*pPixel++);// * *pKernel++;
        }
    }

    const int Area = ((Radius * 2 + 1) * (Radius * 2 + 1));

    const float Mean = Accumulator / Area;//KernelIntegral;

    pPixelBase = pImage->pixels + (pImage->width * Y0) + X0;

    float SquareAccumulator = 0.0f;

    //pKernel = pKernelBase; //unused variable

    for (int YS = Y0; YS <= Y1; ++YS, pPixelBase += pImage->width)
    {
        const unsigned char* pPixel = pPixelBase;

        for (int XS = X0; XS <= X1; ++XS)
        {
            const float Deviation = (float(*pPixel++) - Mean); // * *pKernel++;

            SquareAccumulator += Deviation * Deviation;
        }
    }

    Homegenity = std::sqrt(SquareAccumulator / Area);


    return true;
}
