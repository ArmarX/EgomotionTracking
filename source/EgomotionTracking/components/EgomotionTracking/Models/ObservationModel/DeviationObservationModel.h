#pragma once

#include <cmath>
#include <string>
#include <Math/Math2d.h>
#include <Image/ByteImage.h>
#include <Image/ImageProcessor.h>
#include <Inventor/SbVec2f.h>

class DeviationObservationModel
{
public:

    DeviationObservationModel();
    ~DeviationObservationModel();

    //  pParticleDeviationImage comes from the virtual view using OpenGL
    //  pReferenceDeviationImage comes from the real camara
    float CalculateImportanceWeight(const CByteImage* pParticleDeviationImage, const CByteImage* pReferenceDeviationImage, const SbVec2f SurfacesPoints[4], const float Sigma);

protected:

    bool GetPixelLocalHomegenity(const CByteImage* pImage, const int X, const int Y, const int Radius, const float* pKernelBase, const float KernelIntegral, float& Homegenity);

    class CPartitionLine2D
    {
    public:

        CPartitionLine2D()
        {
        }

        void Set(const SbVec2f& PointA, const SbVec2f& PointB, const SbVec2f& PointC)
        {
            m_PointA = PointA;
            m_PointB = PointB;
            m_PointC = PointC;
            m_MidPoint = (PointB + PointA) * 0.5f;
            m_Direction = PointB - PointA;
            m_Direction.normalize();
            m_Normal.setValue(-m_Direction[1], m_Direction[0]);
            if (m_Normal.dot(PointC - m_MidPoint) < 0.0f)
            {
                m_Normal *= -1.0f;
            }
            m_CX = m_MidPoint[0];
            m_CY = m_MidPoint[1];
            m_NX = m_Normal[0];
            m_NY = m_Normal[1];
        }

        void SetOffset(const float Offset)
        {
            m_MidPoint += (m_Normal * Offset);
            m_CX = m_MidPoint[0];
            m_CY = m_MidPoint[1];
        }

        inline bool IsInHalfSpace(const float X, const float Y) const
        {
            return (((X - m_CX) * m_NX) + ((Y - m_CY) * m_NY)) >= 0.0f;
        }

        inline bool _IsInHalfSpace(const float X, const float Y) const
        {
            return ((X - m_CX) * m_NX)  >= ((m_CY - Y) * m_NY);
        }

    protected:

        SbVec2f m_PointA;
        SbVec2f m_PointB;
        SbVec2f m_PointC;
        SbVec2f m_MidPoint;
        SbVec2f m_Direction;
        SbVec2f m_Normal;
        float m_CX;
        float m_CY;
        float m_NX;
        float m_NY;
    };

    class CActiveZone
    {
    public:

        CActiveZone(const SbVec2f Points[4], const float Offset, const int SubWidth, const int SubHeight):
            m_P0(__FLT_MAX__, __FLT_MAX__),
            m_P1(-__FLT_MAX__, -__FLT_MAX__),
            m_OffsetP0(__FLT_MAX__, __FLT_MAX__),
            m_OffsetP1(-__FLT_MAX__, -__FLT_MAX__),
            m_Centroid(0.0f, 0.0f)
        {
            for (int i = 0; i < 4; ++i)
            {
                m_Points[i] = Points[i];
                m_Centroid += Points[i];
                for (int j = 0; j < 2; ++j)
                {
                    if (m_Points[i][j] < m_P0[j])
                    {
                        m_P0[j] = m_Points[i][j];
                    }
                    if (m_Points[i][j] > m_P1[j])
                    {
                        m_P1[j] = m_Points[i][j];
                    }
                }
            }
            m_Centroid *= 0.25f;
            m_OffsetP0.setValue(std::max(m_P0[0] - Offset, 0.0f), std::max(m_P0[1] - Offset, 0.0f));
            m_OffsetP1.setValue(std::min(m_P1[0] + Offset, float(SubWidth)), std::min(m_P1[1] + Offset, float(SubHeight)));
            for (int i = 0; i < 4; ++i)
            {
                m_Lines[i].Set(m_Points[(i + 1) % 4], m_Points[i], m_Centroid);
                m_OffsetLines[i] = m_Lines[i];
                m_OffsetLines[i].SetOffset(-Offset);
            }
        }

        void GetBoundaries(int& X0, int& Y0, int& X1, int& Y1) const
        {
            X0 = int(m_P0[0]);
            Y0 = int(m_P0[1]);
            X1 = int(std::ceil(m_P1[0]));
            Y1 = int(std::ceil(m_P1[1]));
        }

        void GetOffsetBoundaries(int& X0, int& Y0, int& X1, int& Y1) const
        {
            X0 = int(m_OffsetP0[0]);
            Y0 = int(m_OffsetP0[1]);
            X1 = int(std::ceil(m_OffsetP1[0]));
            Y1 = int(std::ceil(m_OffsetP1[1]));
        }

        inline bool IsInSubSpace(const float X, const float Y) const
        {
            return m_Lines[0].IsInHalfSpace(X, Y) && m_Lines[1].IsInHalfSpace(X, Y) && m_Lines[2].IsInHalfSpace(X, Y) && m_Lines[3].IsInHalfSpace(X, Y);
        }

        inline bool IsInOffsetSubSpace(const float X, const float Y) const
        {
            return m_OffsetLines[0].IsInHalfSpace(X, Y) && m_OffsetLines[1].IsInHalfSpace(X, Y) && m_OffsetLines[2].IsInHalfSpace(X, Y) && m_OffsetLines[3].IsInHalfSpace(X, Y);
        }

        inline bool _IsInSubSpace(const float X, const float Y) const
        {
            return m_Lines[0]._IsInHalfSpace(X, Y) && m_Lines[1]._IsInHalfSpace(X, Y) && m_Lines[2]._IsInHalfSpace(X, Y) && m_Lines[3]._IsInHalfSpace(X, Y);
        }

        inline bool _IsInOffsetSubSpace(const float X, const float Y) const
        {
            return m_OffsetLines[0]._IsInHalfSpace(X, Y) && m_OffsetLines[1]._IsInHalfSpace(X, Y) && m_OffsetLines[2]._IsInHalfSpace(X, Y) && m_OffsetLines[3]._IsInHalfSpace(X, Y);
        }

    protected:

        SbVec2f m_P0;
        SbVec2f m_P1;
        SbVec2f m_OffsetP0;
        SbVec2f m_OffsetP1;
        SbVec2f m_Centroid;
        SbVec2f m_Points[4];
        CPartitionLine2D m_Lines[4];
        CPartitionLine2D m_OffsetLines[4];
    };


};

