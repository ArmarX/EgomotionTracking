/*
 * LineModelObservationModelInterface.cpp
 *
 *  Created on: 19.03.2013
 *      Author: abyte
 */

#include "LineModelObservationModelInterface.h"
#include <EgomotionTracking/components/EgomotionTrackingConfiguration/TrackingConfiguration.h>



CLineModelObservationModelInterface::CLineModelObservationModelInterface()
{
}

CLineModelObservationModelInterface::~CLineModelObservationModelInterface()
{
}

float CLineModelObservationModelInterface::GetMaxNormalDistance() const
{
    return m_maxNormalDistance;
}

void CLineModelObservationModelInterface::SetMaxNormalDistance(float maxNormalDistance)
{
    m_maxNormalDistance = maxNormalDistance;
}

unsigned int CLineModelObservationModelInterface::GetNumberOfNormals() const
{
    return m_numberOfNormals;
}

void CLineModelObservationModelInterface::SetNumberOfNormals(unsigned int numberOfNormals)
{
    m_numberOfNormals = numberOfNormals;
}

unsigned int CLineModelObservationModelInterface::GetMinNumberOfVisibleModelElements() const
{
    return m_minNumberOfVisibleModelElements;
}

void CLineModelObservationModelInterface::SetMinNumberOfVisibleModelElements(unsigned int minNumberOfVisibleModelElements)
{
    m_minNumberOfVisibleModelElements = minNumberOfVisibleModelElements;
}

