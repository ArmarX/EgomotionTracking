/*
 * SimpleRobotMotionModel.cpp
 *
 *  Created on: 12.03.2013
 *      Author: abyte
 */

#include "SimpleRobotMotionModel.h"
#include <Inventor/SbVec3f.h>
#include <Inventor/SbRotation.h>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/Conversions.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/DebuggingPrimitives.h>

CSimpleRobotMotionModel::CSimpleRobotMotionModel()
{
}

CSimpleRobotMotionModel::~CSimpleRobotMotionModel()
{
}

CLocationState CSimpleRobotMotionModel::PredictNewRobotLocation(const CLocationState& oldLocationState, const CRobotControlState& robotControlState, const CRandomGenerator* pRandomGenerator)  const
{
    CLocationState newLocation;
    const float x = oldLocationState.GetX();
    const float y = oldLocationState.GetY();
    const float alpha = oldLocationState.GetAlpha();
    const float deltaT = robotControlState.GetDeltaT();
    const float deltaTinSeconds = Conversions::MillisecondsToSeconds(deltaT);

    SbVec3f motionVectorRobot(robotControlState.GetPlatformVelocityX() * deltaTinSeconds,
                              robotControlState.GetPlatformVelocityY() * deltaTinSeconds,
                              0.0);

    float newAlpha = Conversions::NormalizeRadiansTo2PiInterval(alpha + (robotControlState.GetPlatformVelocityAlpha() * deltaTinSeconds));

    float alphaMean = (newAlpha + alpha) / 2;

    SbRotation rot = SbRotation(SbVec3f(0.0, 0.0, 1.0), alphaMean + robotControlState.GetPlatformAxesTiltError());
    SbVec3f motionVectorWorld;
    rot.multVec(motionVectorRobot, motionVectorWorld);


    newLocation.SetX(x + motionVectorWorld[0]);
    newLocation.SetY(y + motionVectorWorld[1]);
    newLocation.SetAlpha(newAlpha);

    return newLocation;
}
