/*
 * SimpleRobotMotionModel.h
 *
 *  Created on: 12.03.2013
 *      Author: abyte
 */

#pragma once

#include "RobotMotionModel.h"
#include <Inventor/SbRotation.h>

class CSimpleRobotMotionModel: public CRobotMotionModel
{
public:
    CSimpleRobotMotionModel();
    ~CSimpleRobotMotionModel() override;
    CLocationState PredictNewRobotLocation(const CLocationState& oldLocationState, const CRobotControlState& robotControlState, const CRandomGenerator* pRandomGenerator)  const override;
};

