/*
 * RobotMotionModel.h
 *
 *  Created on: 12.03.2013
 *      Author: abyte
 */

#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/RobotControlState.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/LocationStatePredictor/LocationStatePredictorInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/RandomGenerator/RandomGenerator.h>

class CRobotMotionModel :  public CLocationStatePredictorInterface
{
public:
    CRobotMotionModel();
    ~CRobotMotionModel() override;
    CLocationState PredictNewRobotLocation(const CLocationState& oldLocationState, const CRobotControlState& robotControlState, const CRandomGenerator* pRandomGenerator) const override = 0;
    inline CLocationState CalculateNewRobotLocation(const CLocationState& oldLocationState, const CRobotControlState& robotControlState) const
    {
        return PredictNewRobotLocation(oldLocationState, robotControlState, NULL);
    }
protected:

};

