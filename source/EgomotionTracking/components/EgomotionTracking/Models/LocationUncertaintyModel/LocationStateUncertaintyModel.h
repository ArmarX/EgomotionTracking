/*
 * LocationStateUncertaintyModel.h
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/RandomGenerator/RandomGenerator.h>

class CLocationStateUncertaintyModel
{
public:
    CLocationStateUncertaintyModel();
    virtual ~CLocationStateUncertaintyModel();
    virtual bool AddUncertaintyToLocationState(CLocationState& locationState, const CRandomGenerator* pRandomGenerator) const = 0;

    void SetScatteringFactor(float scatteringFactor) const;
protected:
    mutable float m_scatteringFactor;
};

