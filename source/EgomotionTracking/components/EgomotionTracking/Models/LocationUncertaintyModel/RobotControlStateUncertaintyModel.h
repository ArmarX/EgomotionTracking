/*
 * RobotControlStateUncertaintyModel.h
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/RobotControlState.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/RandomGenerator/RandomGenerator.h>

class CRobotControlStateUncertaintyModel
{
public:
    CRobotControlStateUncertaintyModel();
    virtual ~CRobotControlStateUncertaintyModel();

    virtual bool AddUncertaintyToRobotControlState(CRobotControlState& robotControlState, const CRandomGenerator* pRandomGenerator) const = 0;

};

