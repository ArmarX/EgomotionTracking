/*
 * AbsoluteGaussianLocationStateUncertaintyModel.cpp
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#include "AbsoluteGaussianLocationStateUncertaintyModel.h"
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotPlatformTransformation.h>

CAbsoluteGaussianLocationStateUncertaintyModel::CAbsoluteGaussianLocationStateUncertaintyModel()
{

}

CAbsoluteGaussianLocationStateUncertaintyModel::~CAbsoluteGaussianLocationStateUncertaintyModel()
{
}

bool CAbsoluteGaussianLocationStateUncertaintyModel::AddUncertaintyToLocationState(CLocationState& locationState, const CRandomGenerator* pRandomGenerator) const
{

    float uncertaintyX = pRandomGenerator->GetGaussianRandomNumber(m_sigmaX * m_scatteringFactor);
    float uncertaintyY = pRandomGenerator->GetGaussianRandomNumber(m_sigmaY * m_scatteringFactor);
    float uncertaintyAlpha = pRandomGenerator->GetGaussianRandomNumber(m_sigmaAlpha * m_scatteringFactor);

    locationState =  CRobotPlatformTransformation::SetPlatformRelative(locationState, uncertaintyX, uncertaintyY, uncertaintyAlpha);

    return true;
}
