/*
 * GaussianLocationStateUncertaintyModel.cpp
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#include "GaussianLocationStateUncertaintyModel.h"

CGaussianLocationStateUncertaintyModel::CGaussianLocationStateUncertaintyModel() :
    m_sigmaX(0.0), m_sigmaY(0.0), m_sigmaAlpha(0.0)
{

}

CGaussianLocationStateUncertaintyModel::~CGaussianLocationStateUncertaintyModel()
{
}

