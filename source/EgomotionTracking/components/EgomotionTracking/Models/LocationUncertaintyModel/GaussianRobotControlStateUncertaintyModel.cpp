/*
 * GaussianRobotControlStateUncertaintyModel.cpp
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#include "GaussianRobotControlStateUncertaintyModel.h"

CGaussianRobotControlStateUncertaintyModel::CGaussianRobotControlStateUncertaintyModel() :
    m_sigmaVelocityX(0), m_sigmaVelocityY(0), m_sigmaVelocityAlpha(0), m_sigmaPlatformAxesTiltError(0.0)
{

}

CGaussianRobotControlStateUncertaintyModel::~CGaussianRobotControlStateUncertaintyModel()
{
}

