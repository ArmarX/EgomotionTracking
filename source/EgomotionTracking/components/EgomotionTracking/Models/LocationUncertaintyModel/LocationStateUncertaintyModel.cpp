/*
 * LocationStateUncertaintyModel.cpp
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#include "LocationStateUncertaintyModel.h"

CLocationStateUncertaintyModel::CLocationStateUncertaintyModel() :
    m_scatteringFactor(1.0)
{

}

CLocationStateUncertaintyModel::~CLocationStateUncertaintyModel()
{
}

void CLocationStateUncertaintyModel::SetScatteringFactor(float scatteringFactor) const
{
    m_scatteringFactor = scatteringFactor;
}

