/*
 * GaussianLocationStateUncertaintyModel.h
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#pragma once

#include "LocationStateUncertaintyModel.h"

class CGaussianLocationStateUncertaintyModel: public CLocationStateUncertaintyModel
{
public:
    CGaussianLocationStateUncertaintyModel();
    ~CGaussianLocationStateUncertaintyModel() override;

    float GetSigmaAlpha() const
    {
        return m_sigmaAlpha;
    }

    void SetSigmaAlpha(float sigmaAlpha)
    {
        m_sigmaAlpha = sigmaAlpha;
    }

    float GetSigmaX() const
    {
        return m_sigmaX;
    }

    void SetSigmaX(float sigmaX)
    {
        m_sigmaX = sigmaX;
    }

    float GetSigmaY() const
    {
        return m_sigmaY;
    }

    void SetSigmaY(float sigmaY)
    {
        m_sigmaY = sigmaY;
    }

protected:
    float m_sigmaX;
    float m_sigmaY;
    float m_sigmaAlpha;
};

