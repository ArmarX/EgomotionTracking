/*
 * RelativeGaussianRobotControlStateUncertaintyModel.h
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#pragma once

#include "GaussianRobotControlStateUncertaintyModel.h"

class CRelativeGaussianRobotControlStateUncertaintyModel: public CGaussianRobotControlStateUncertaintyModel
{
public:
    CRelativeGaussianRobotControlStateUncertaintyModel();
    ~CRelativeGaussianRobotControlStateUncertaintyModel() override;
    bool AddUncertaintyToRobotControlState(CRobotControlState& robotControlState, const CRandomGenerator* pRandomGenerator) const override;

    float GetRelativeSystematicErrorVelocityAlpha() const
    {
        return m_relativeSystematicErrorVelocityAlpha;
    }

    void SetRelativeSystematicErrorVelocityAlpha(float relativeSystematicErrorVelocityAlpha)
    {
        m_relativeSystematicErrorVelocityAlpha = relativeSystematicErrorVelocityAlpha;
    }

    float GetRelativeSystematicErrorVelocityX() const
    {
        return m_relativeSystematicErrorVelocityX;
    }

    void SetRelativeSystematicErrorVelocityX(float relativeSystematicErrorVelocityX)
    {
        m_relativeSystematicErrorVelocityX = relativeSystematicErrorVelocityX;
    }

    float GetRelativeSystematicErrorVelocityY() const
    {
        return m_relativeSystematicErrorVelocityY;
    }

    void SetRelativeSystematicErrorVelocityY(float relativeSystematicErrorVelocityY)
    {
        m_relativeSystematicErrorVelocityY = relativeSystematicErrorVelocityY;
    }

    float GetAbsoluteSystematicPlatformAxesTiltError() const
    {
        return m_absoluteSystematicPlatformTiltError;
    }

    void SetAbsoluteSystematicPlatformAxesTiltError(float absoluteSystematicPlatformTiltError)
    {
        m_absoluteSystematicPlatformTiltError = absoluteSystematicPlatformTiltError;
    }

protected:
    float m_relativeSystematicErrorVelocityX;
    float m_relativeSystematicErrorVelocityY;
    float m_relativeSystematicErrorVelocityAlpha;
    float m_absoluteSystematicPlatformTiltError;
};

