/*
 * GaussianRobotControlStateUncertaintyModel.h
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#pragma once

#include "RobotControlStateUncertaintyModel.h"

class CGaussianRobotControlStateUncertaintyModel: public CRobotControlStateUncertaintyModel
{
public:
    CGaussianRobotControlStateUncertaintyModel();
    ~CGaussianRobotControlStateUncertaintyModel() override;

    float GetSigmaVelocityAlpha() const
    {
        return m_sigmaVelocityAlpha;
    }

    void SetSigmaVelocityAlpha(float sigmaVelocityAlpha)
    {
        m_sigmaVelocityAlpha = sigmaVelocityAlpha;
    }

    float GetSigmaVelocityX() const
    {
        return m_sigmaVelocityX;
    }

    void SetSigmaVelocityX(float sigmaVelocityX)
    {
        m_sigmaVelocityX = sigmaVelocityX;
    }

    float GetSigmaVelocityY() const
    {
        return m_sigmaVelocityY;
    }

    void SetSigmaVelocityY(float sigmaVelocityY)
    {
        m_sigmaVelocityY = sigmaVelocityY;
    }

    float GetSigmaPlatformAxesTiltError() const
    {
        return m_sigmaPlatformAxesTiltError;
    }

    void SetSigmaPlatformAxesTiltError(float sigma)
    {
        m_sigmaPlatformAxesTiltError = sigma;
    }

protected:
    float m_sigmaVelocityX;
    float m_sigmaVelocityY;
    float m_sigmaVelocityAlpha;
    float m_sigmaPlatformAxesTiltError;
};

