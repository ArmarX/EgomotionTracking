/*
 * RelativeGaussianRobotControlStateUncertaintyModel.cpp
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#include "RelativeGaussianRobotControlStateUncertaintyModel.h"

CRelativeGaussianRobotControlStateUncertaintyModel::CRelativeGaussianRobotControlStateUncertaintyModel() :
    m_relativeSystematicErrorVelocityX(0.0),
    m_relativeSystematicErrorVelocityY(0.0),
    m_relativeSystematicErrorVelocityAlpha(0.0),
    m_absoluteSystematicPlatformTiltError(0.0)
{

}

CRelativeGaussianRobotControlStateUncertaintyModel::~CRelativeGaussianRobotControlStateUncertaintyModel()
{
}

bool CRelativeGaussianRobotControlStateUncertaintyModel::AddUncertaintyToRobotControlState(CRobotControlState& robotControlState, const CRandomGenerator* pRandomGenerator) const
{

    const float relativeUncertaintyFactorVelocityX = 1.0 + pRandomGenerator->GetGaussianRandomNumber(m_sigmaVelocityX);
    const float relativeUncertaintyFactorVelocityY = 1.0 + pRandomGenerator->GetGaussianRandomNumber(m_sigmaVelocityY);
    const float relativeUncertaintyFactorVelocityAlpha = 1.0 + pRandomGenerator->GetGaussianRandomNumber(m_sigmaVelocityAlpha);

    const float absoluteUncertaintyPlatformTiltError = pRandomGenerator->GetGaussianRandomNumber(m_sigmaPlatformAxesTiltError);

    const float velocityX = robotControlState.GetPlatformVelocityX();
    const float velocityY = robotControlState.GetPlatformVelocityY();
    const float velocityAlpha = robotControlState.GetPlatformVelocityAlpha();
    const float platformTiltError = robotControlState.GetPlatformAxesTiltError();

    const float relativeSystematicErrorFactorVelocityX = 1.0 + m_relativeSystematicErrorVelocityX;
    const float relativeSystematicErrorFactorVelocityY = 1.0 + m_relativeSystematicErrorVelocityY;
    const float relativeSystematicErrorFactorVelocityAlpha = 1.0 + m_relativeSystematicErrorVelocityAlpha;

    robotControlState.SetPlatformAxesTiltError(platformTiltError + absoluteUncertaintyPlatformTiltError + m_absoluteSystematicPlatformTiltError);
    bool ok = robotControlState.SetPlatformVelocityX(relativeUncertaintyFactorVelocityX * relativeSystematicErrorFactorVelocityX * velocityX);
    ok = ok && robotControlState.SetPlatformVelocityY(relativeUncertaintyFactorVelocityY * relativeSystematicErrorFactorVelocityY * velocityY);
    ok = ok && robotControlState.SetPlatformVelocityAlpha(relativeUncertaintyFactorVelocityAlpha * relativeSystematicErrorFactorVelocityAlpha * velocityAlpha);
    return ok;
}
