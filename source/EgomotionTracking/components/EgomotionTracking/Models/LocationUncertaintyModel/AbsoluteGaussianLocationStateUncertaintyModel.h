/*
 * AbsoluteGaussianLocationStateUncertaintyModel.h
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#pragma once

#include "GaussianLocationStateUncertaintyModel.h"

class CAbsoluteGaussianLocationStateUncertaintyModel: public CGaussianLocationStateUncertaintyModel
{
public:
    CAbsoluteGaussianLocationStateUncertaintyModel();
    ~CAbsoluteGaussianLocationStateUncertaintyModel() override;
    bool AddUncertaintyToLocationState(CLocationState& locationState, const CRandomGenerator* pRandomGenerator) const override;

};

