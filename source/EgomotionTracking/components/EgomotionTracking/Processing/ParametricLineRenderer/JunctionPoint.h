/*
 * JunctionPoint.h
 *
 *  Created on: 21.03.2013
 *      Author: abyte
 */

#pragma once

#include <Inventor/SbVec3f.h>
#include <list>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/XMLPrimitives.h>
#include "Line.h"

class CJunctionPoint
{
public:
    CJunctionPoint() : m_degree(0), m_isVisible(true) {}
    CJunctionPoint(SbVec3f junctionPoint) : m_junctionPoint(junctionPoint), m_degree(0), m_isVisible(true) {}

    SbVec3f m_junctionPoint;
    unsigned int m_degree;

    std::list<CLine> m_junctionLines;

    CJunctionPoint* m_pWorldJunctionPointReference;

    static bool CompareJunctionSbVec3fPoints(const SbVec3f& p1, const SbVec3f& p2)
    {
        if (p1[0] != p2[0])
        {
            return p1[0] < p2[0];
        }
        else if (p1[1] != p2[1])
        {
            return p1[1] < p2[1];
        }

        return p1[2] < p2[2];
    }

    class CompareJunctionSbVec3fPointsFunctor
    {
    public:
        bool operator()(const SbVec3f& p1, const SbVec3f& p2) const
        {
            return CompareJunctionSbVec3fPoints(p1, p2);
        }
    };

    class CompareJunctionJunctionPointsFunctor
    {
    public:
        bool operator()(const CJunctionPoint& p1, const CJunctionPoint& p2) const
        {
            return CompareJunctionSbVec3fPoints(p1.m_junctionPoint, p2.m_junctionPoint);
        }
    };
    bool operator< (const CJunctionPoint& jp)
    {
        return CompareJunctionSbVec3fPoints(m_junctionPoint, jp.m_junctionPoint);
    }

    bool m_isVisible;

    class isLowDegreeJunctionPoint
    {
    public:
        isLowDegreeJunctionPoint(unsigned int minJunctionDegree) : m_minJunctionDegree(minJunctionDegree) {}
        bool operator()(const CJunctionPoint& jp) const
        {
            if (jp.m_junctionLines.size() < m_minJunctionDegree)
            {
                return true;
            }
            return false;
        }
        unsigned int m_minJunctionDegree;
    };

    QDomElement SerializeToQDomElement() const
    {
        QDomDocument tmp;
        QDomElement junctionPointQDomElement = tmp.createElement("CJunctionPoint");

        QDomElement junctionPoint = XMLPrimitives::SerializeSbVec3fToQDomElement(m_junctionPoint, "junctionPoint");
        junctionPointQDomElement.appendChild(junctionPoint);
        junctionPointQDomElement.setAttribute("visibility", m_isVisible);
        return junctionPointQDomElement;
    }

    bool DeserializeFromQDomElement(QDomElement& lineDomElement)
    {
        if (lineDomElement.tagName() != "CJunctionPoint")
        {
            return false;
        }
        if (lineDomElement.childNodes().size() != 1)
        {
            return false;
        }

        QDomElement junctionPointQDomElement = lineDomElement.childNodes().item(0).toElement();

        if (junctionPointQDomElement.isNull() || junctionPointQDomElement.isNull())
        {
            return false;
        }

        QDomAttr attrVisibility = lineDomElement.attributeNode("visibility");

        if (attrVisibility.isNull())
        {
            return false;
        }

        bool valueValid;
        m_isVisible = static_cast<bool>(attrVisibility.value().toInt(&valueValid));
        if (!valueValid)
        {
            return false;
        }

        return XMLPrimitives::DeserializeQDomElementToSbVec(junctionPointQDomElement, "junctionPoint", m_junctionPoint);
    }

};

