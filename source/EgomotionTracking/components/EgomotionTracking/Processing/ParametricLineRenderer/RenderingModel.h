#pragma once
#include <list>
#include <Inventor/actions/SoActions.h>
#include <Inventor/nodes/SoNodes.h>
#include <Inventor/SoPrimitiveVertex.h>
#include <Inventor/SbViewportRegion.h>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/XMLPrimitives.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/DynamicElement.h>

#include <EgomotionTracking/components/RVL/Representation/ModelBased/GeometricGraph/Visualization/OpenInventor/OpenInventorModelMultipleMesh.h>

#include "Line.h"
#include "JunctionPoint.h"
#include "FaceTriangle.h"
#include "VisibleScreenRegion.h"

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    class CModelMultipleMesh;
                    class CModelTransformation;
                }
            }
        }
    }
}

class CRenderingModel
{
public:
    CRenderingModel();

    bool CalculateAlignedModelBoundingBox(SbVec3f& min, SbVec3f& max) const;
    void GenerateModelsFromSceneGraph(SoNode* pLineModel, SoNode* pFaceModel);
    void AddModelLine(const CLine& modelLine);

    bool WriteModelFaceTrianglesToFile(const QString& filename, bool onlyVisibleFaceTriangles = true);
    bool WriteModelLinesToFile(const QString& filename, bool onlyVisibleLines = true);

    bool LoadModelFaceTrianglesFromFile(const QString& filename, bool onlyVisibleFaceTriangles = true);
    bool LoadModelLinesFromFile(const QString& filename, bool onlyVisibleLines = true);

    std::list<CFaceTriangle>& GetModelFaceTriangleList();
    std::list<CLine>& GetModelLineList();

    void SetMinModelLineLength(float minModelLineLength);
    float GetMinModelLineLength();
    void SetMinModelFaceTriangleArea(float minModelFaceTriangleArea);
    float GetMinModelFaceTriangleArea();

    void SetGlobalModelElementsTransformation(const SbMatrix& globalTransformation);
    const SbMatrix& GetGlobalModelElementsTransformation();

    void SetShortModelLinesToInvisible();
    void SetSmallTriangleAreaToInvisible();
    void SetAllModelElementsVisibility(bool visibility);

    unsigned int GetNumberOfVisibleModelLines() const;
    unsigned int GetNumberOfVisibleModelFaceTriangles() const;

    bool loadRVLModelFromXMLFile(const std::string& filename);
    bool saveRVLModelToXMLFile(const std::string& filename);

    const RVL::Representation::ModelBased::GeometricGraph::Base::CModelMultipleMesh* getModelMultipleMesh() const;

    std::list<CLine>& getTransformationBoundingBoxLineList();
    std::list<CFaceTriangle>& getTransformationBoundingBoxFaceTriangleList();

    bool loadMultipleVisualizationRVLModelsFromXMLFile(const std::string& filename, unsigned int amountToLoad);

    const std::map<int, RVL::Representation::ModelBased::GeometricGraph::Base::CModelMultipleMesh*>& getVisualizationModelMultipleMeshMap() const;
    const std::map<int, RVL::Representation::ModelBased::GeometricGraph::Visualization::OpenInventor::COpenInventorModelMultipleMesh*>& getVisualizationOIModelMultipleMeshMap() const;

protected:

    void importRVLDataStructure();
    void extractTransformationBoundingBoxes();

    void GenerateModelLineList(SoNode* pLineModel);
    void GenerateModelFaceTriangleList(SoNode* pLineModel, SoNode* pFaceModel);
    void RemoveRedundantFaceTriangles();
    void RemoveRedundantLines();

    static void collect3dLines_cb(void* userdata, SoCallbackAction* action, const SoPrimitiveVertex* v1, const SoPrimitiveVertex* v2);
    static void collect3dTriangles_cb(void* userdata, SoCallbackAction* action, const SoPrimitiveVertex* v1, const SoPrimitiveVertex* v2, const SoPrimitiveVertex* v3);
    static inline float Calc2dLineLength(const CLine& line)
    {
        SbVec3f l1 = line.m_p1;
        SbVec3f l2 = line.m_p2;
        float dx = l1[0] - l2[0];
        float dy = l1[1] - l2[1];
        return sqrt(dx * dx + dy * dy);
    }
    static bool compare_faceArea2d(const CFaceTriangle& face1, const CFaceTriangle& face2)
    {
        // using herons formula (except square root)

        float a1 = Calc2dLineLength(CLine(face1.m_p1, face1.m_p2));
        float b1 = Calc2dLineLength(CLine(face1.m_p2, face1.m_p3));
        float c1 = Calc2dLineLength(CLine(face1.m_p3, face1.m_p1));
        float s1 = (a1 + b1 + c1) / 2;
        float squaredArea1 = s1 * (s1 - a1) * (s1 - b1) * (s1 - c1);

        float a2 = Calc2dLineLength(CLine(face2.m_p1, face2.m_p2));
        float b2 = Calc2dLineLength(CLine(face2.m_p2, face2.m_p3));
        float c2 = Calc2dLineLength(CLine(face2.m_p3, face2.m_p1));
        float s2 = (a2 + b2 + c2) / 2;
        float squaredArea2 = s2 * (s2 - a2) * (s2 - b2) * (s2 - c2);

        return squaredArea1 > squaredArea2;
    }

    float accumulatedTrianglePointDistance(const CFaceTriangle& t1, const CFaceTriangle& t2);
    float accumulatedTrianglePointDistanceFixedOrder(const CFaceTriangle& t1, SbVec3f t2p1, SbVec3f t2p2, SbVec3f t2p3);


    static QDomElement SerializeFaceTriangleList(
        const std::list<CFaceTriangle>& faceTriangleList,
        const QString& elementName, bool serializeOnlyVisibleFaceTriangles =
            false)
    {
        return XMLPrimitives::SerializeList(faceTriangleList, elementName, serializeOnlyVisibleFaceTriangles);
    }
    static QDomElement SerializeLineList(const std::list<CLine>& lineList, const QString& elementName, bool serializeOnlyVisibleLines = false)
    {
        return XMLPrimitives::SerializeList(lineList, elementName, serializeOnlyVisibleLines);
    }
    static QDomElement SerializeJunctionPointList(const std::list<CJunctionPoint>& junctionPointList, const QString& elementName, bool serializeOnlyVisibleLines = false)
    {
        return XMLPrimitives::SerializeList(junctionPointList, elementName, serializeOnlyVisibleLines);
    }

    static unsigned int DeserializeFaceTriangleList(QDomElement& faceTriangleListQDomElement, bool deserializeOnlyVisibleFaceTriangles, std::list<CFaceTriangle>& faceTriangleList)
    {
        return XMLPrimitives::DeserializeList(faceTriangleListQDomElement, "CFaceTriangle", deserializeOnlyVisibleFaceTriangles, faceTriangleList);
    }
    static unsigned int DeserializeLineList(QDomElement& lineListQDomElement, bool deserializeOnlyVisibleLines, std::list<CLine>& lineList)
    {
        return XMLPrimitives::DeserializeList(lineListQDomElement, "CLine", deserializeOnlyVisibleLines, lineList);
    }
    static unsigned int DeserializeJunctionPointList(QDomElement& junctionPointListQDomElement, bool deserializeOnlyVisibleJunctionPoints, std::list<CJunctionPoint>& junctionPointList)
    {
        return XMLPrimitives::DeserializeList(junctionPointListQDomElement, "CJunctionPoint", deserializeOnlyVisibleJunctionPoints, junctionPointList);
    }

    std::list<CFaceTriangle> m_modelFaceTriangleList;
    std::list<CLine> m_modelLineList;

    std::list<CFaceTriangle> m_transformationBoundingBoxFaceTriangleList;
    std::list<CLine> m_transformationBoundingBoxLineList;

    SbMatrix m_globalModelElementsTransformation;

    float m_minModelLineLength;
    float m_minModelFaceTriangleArea;

    RVL::Representation::ModelBased::GeometricGraph::Base::CModelMultipleMesh* m_pModelMultipleMesh;

    std::map<int, RVL::Representation::ModelBased::GeometricGraph::Base::CModelMultipleMesh*> m_pVisualizationModelMultipleMeshMap;
    std::map<int, RVL::Representation::ModelBased::GeometricGraph::Visualization::OpenInventor::COpenInventorModelMultipleMesh*> m_pVisualizationOIModelMultipleMeshMap;
};

