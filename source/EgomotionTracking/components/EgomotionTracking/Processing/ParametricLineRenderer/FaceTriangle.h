#pragma once
#include <Inventor/SbVec3f.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/XMLPrimitives.h>

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    class CModelFace;
                }
            }
        }
    }
}

class CFaceTriangle
{
public:
    CFaceTriangle(SbVec3f p1, SbVec3f p2, SbVec3f p3, CFaceTriangle* pWorldFaceTriangleReference) :
        m_p1(p1), m_p2(p2), m_p3(p3), m_isVisible(true), m_pWorldFaceTriangleReference(pWorldFaceTriangleReference), m_pModelFaceReference(NULL)
    {
        CalculateBounds();
    }

    CFaceTriangle(SbVec3f p1, SbVec3f p2, SbVec3f p3) :
        m_p1(p1), m_p2(p2), m_p3(p3), m_isVisible(true), m_pWorldFaceTriangleReference(NULL), m_pModelFaceReference(NULL)
    {
        CalculateBounds();
    }
    CFaceTriangle(): m_isVisible(true), m_pWorldFaceTriangleReference(NULL) {}
    SbVec3f m_p1;
    SbVec3f m_p2;
    SbVec3f m_p3;
    bool m_isVisible;
    CFaceTriangle* m_pWorldFaceTriangleReference;
    RVL::Representation::ModelBased::GeometricGraph::Base::CModelFace* m_pModelFaceReference;

    float m_boundMinX;
    float m_boundMaxX;
    float m_boundMinY;
    float m_boundMaxY;

    void CalculateBounds()
    {
        m_boundMinX = std::min(m_p1[0], std::min(m_p2[0], m_p3[0]));
        m_boundMaxX = std::max(m_p1[0], std::max(m_p2[0], m_p3[0]));
        m_boundMinY = std::min(m_p1[1], std::min(m_p2[1], m_p3[1]));
        m_boundMaxY = std::max(m_p1[1], std::max(m_p2[1], m_p3[1]));
    }

    inline float CalculateArea() const
    {
        float a = (m_p1 - m_p2).length();
        float b = (m_p2 - m_p3).length();
        float c = (m_p3 - m_p1).length();
        float s = (a + b + c) / 2;
        float squaredArea = s * (s - a) * (s - b) * (s - c);
        return sqrt(squaredArea);
    }

    QDomElement SerializeToQDomElement() const
    {
        QDomDocument tmp;
        QDomElement lineQDomElement = tmp.createElement("CFaceTriangle");

        QDomElement p1QDomElement = XMLPrimitives::SerializeSbVec3fToQDomElement(m_p1, "p1");
        QDomElement p2QDomElement = XMLPrimitives::SerializeSbVec3fToQDomElement(m_p2, "p2");
        QDomElement p3QDomElement = XMLPrimitives::SerializeSbVec3fToQDomElement(m_p3, "p3");
        lineQDomElement.appendChild(p1QDomElement);
        lineQDomElement.appendChild(p2QDomElement);
        lineQDomElement.appendChild(p3QDomElement);

        lineQDomElement.setAttribute("visibility", m_isVisible);
        return lineQDomElement;
    }
    bool DeserializeFromQDomElement(QDomElement& lineDomElement)
    {
        if (lineDomElement.tagName() != "CFaceTriangle")
        {
            return false;
        }
        if (lineDomElement.childNodes().size() != 3)
        {
            return false;
        }

        QDomElement p1QDomElement = lineDomElement.childNodes().item(0).toElement();
        QDomElement p2QDomElement = lineDomElement.childNodes().item(1).toElement();
        QDomElement p3QDomElement = lineDomElement.childNodes().item(2).toElement();

        if (p1QDomElement.isNull() || p2QDomElement.isNull() || p3QDomElement.isNull())
        {
            return false;
        }

        QDomAttr attrVisibility = lineDomElement.attributeNode("visibility");

        if (attrVisibility.isNull())
        {
            return false;
        }

        bool valueValid;
        m_isVisible = static_cast<bool>(attrVisibility.value().toInt(&valueValid));
        if (!valueValid)
        {
            return false;
        }
        return XMLPrimitives::DeserializeQDomElementToSbVec(p1QDomElement, "p1", m_p1)
               && XMLPrimitives::DeserializeQDomElementToSbVec(p2QDomElement, "p2", m_p2)
               && XMLPrimitives::DeserializeQDomElementToSbVec(p3QDomElement, "p3", m_p3);
    }
};

