/*
 * Line.h
 *
 *  Created on: 21.03.2013
 *      Author: abyte
 */

#pragma once

#include <Inventor/SbVec2f.h>
#include <Inventor/SbVec3f.h>
#include <list>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/XMLPrimitives.h>

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    class CModelEdge;
                }
            }
        }
    }
}

class CLine
{
public:
    CLine() : m_isVisible(false), m_pModelEdgeReference(NULL) {};
    CLine(SbVec3f p1, SbVec3f p2, bool isVisible = true) : m_p1(p1), m_p2(p2), m_isVisible(isVisible), m_pModelEdgeReference(NULL) {};
    SbVec3f m_p1;
    SbVec3f m_p2;
    bool m_isVisible;
    RVL::Representation::ModelBased::GeometricGraph::Base::CModelEdge* m_pModelEdgeReference;

    inline SbVec3f CalculateNearestPointOnLine2d(const SbVec3f& point) const
    {
        SbVec2f p2in2d(m_p2[0], m_p2[1]);
        SbVec2f p1in2d(m_p1[0], m_p1[1]);
        SbVec2f pointIn2d(point[0], point[1]);

        SbVec2f dLine = p2in2d - p1in2d;
        SbVec2f dP = pointIn2d - p1in2d;

        float lambda = dLine.dot(dP) / dLine.dot(dLine);
        if (lambda < 0.0)
        {
            return m_p1;
        }
        else if (lambda > 1.0)
        {
            return m_p2;
        }
        return m_p1 + lambda * (m_p2 - m_p1);
    }

    inline float CalculateLength() const
    {
        return (m_p2 - m_p1).length();
    }

    inline float CalculateLength2d() const
    {
        float dx = m_p1[0] - m_p2[0];
        float dy = m_p1[1] - m_p2[1];
        return sqrt(dx * dx + dy * dy);
    }

    inline float CalculateNormalizedDirectionScalarProduct2d(const CLine& line) const
    {
        SbVec2f dir1(m_p2[0] - m_p1[0], m_p2[1] - m_p1[1]);
        SbVec2f dir2(line.m_p2[0] - line.m_p1[0], line.m_p2[1] - line.m_p1[1]);
        dir1.normalize();
        dir2.normalize();
        return dir1.dot(dir2);
    }

    inline SbVec3f CalculateCenterPoint() const
    {
        return m_p1 + 0.5 * (m_p2 - m_p1);
    }

    inline void GetOrderedPointsX(SbVec3f& minX, SbVec3f& maxX) const
    {
        if (m_p1[0] < m_p2[0])
        {
            minX = m_p1;
            maxX = m_p2;
        }
        else
        {
            minX = m_p2;
            maxX = m_p1;
        }
    }


    inline SbVec2f GetP1SbVec2f() const
    {
        return SbVec2f(m_p1[0], m_p1[1]);
    }

    inline SbVec2f GetP2SbVec2f() const
    {
        return SbVec2f(m_p2[0], m_p2[1]);
    }

    inline SbVec2f GetDirection2d() const
    {
        SbVec2f dir = GetP2SbVec2f() - GetP1SbVec2f();
        dir.normalize();
        return dir;
    }

    inline SbVec2f GetNormal2d() const
    {
        SbVec2f dir = GetDirection2d();
        return SbVec2f(-dir[1], dir[0]);
    }

    QDomElement SerializeToQDomElement() const
    {
        QDomDocument tmp;
        QDomElement lineQDomElement = tmp.createElement("CLine");

        QDomElement p1QDomElement = XMLPrimitives::SerializeSbVec3fToQDomElement(m_p1, "p1");
        QDomElement p2QDomElement = XMLPrimitives::SerializeSbVec3fToQDomElement(m_p2, "p2");
        lineQDomElement.appendChild(p1QDomElement);
        lineQDomElement.appendChild(p2QDomElement);

        lineQDomElement.setAttribute("visibility", m_isVisible);
        return lineQDomElement;
    }

    bool DeserializeFromQDomElement(QDomElement& lineDomElement)
    {
        if (lineDomElement.tagName() != "CLine")
        {
            return false;
        }
        if (lineDomElement.childNodes().size() != 2)
        {
            return false;
        }

        QDomElement p1QDomElement = lineDomElement.childNodes().item(0).toElement();
        QDomElement p2QDomElement = lineDomElement.childNodes().item(1).toElement();

        if (p1QDomElement.isNull() || p2QDomElement.isNull())
        {
            return false;
        }

        QDomAttr attrVisibility = lineDomElement.attributeNode("visibility");

        if (attrVisibility.isNull())
        {
            return false;
        }

        bool valueValid;
        m_isVisible = static_cast<bool>(attrVisibility.value().toInt(&valueValid));
        if (!valueValid)
        {
            return false;
        }

        return XMLPrimitives::DeserializeQDomElementToSbVec(p1QDomElement, "p1", m_p1) && XMLPrimitives::DeserializeQDomElementToSbVec(p2QDomElement, "p2", m_p2);
    }
};

class CScreenLine : public CLine
{
    /* for collecting visible world lines */
public:
    CScreenLine() : m_pWorldLineReference(NULL) {}
    CScreenLine(SbVec3f p1, SbVec3f p2, CLine* worldLineReference = NULL) : CLine(p1, p2), m_pWorldLineReference(worldLineReference) {};
    CLine* m_pWorldLineReference;
};

