/*
 * ParametricLineRenderer.h
 *
 *  Created on: 12.02.2013
 *      Author: abyte
 */

#pragma once

#include <list>
#include <Inventor/actions/SoActions.h>
#include <Inventor/nodes/SoNodes.h>
#include <Inventor/SoPrimitiveVertex.h>
#include <Inventor/SbViewportRegion.h>

#include <EgomotionTracking/components/EVP/GlobalSettings.h>

#include <assert.h>

#include <queue>
#include <set>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/XMLPrimitives.h>
#include "Line.h"
#include "JunctionPoint.h"
#include "FaceTriangle.h"
#include "VisibleScreenRegion.h"
#include "RenderingModel.h"

class CParametricLineRenderer
{
public:
    class CSweepLinePoint
    {
    public:
        enum CSweepLinePointType
        {
            eFaceTriangleBoundMin,
            eFaceTriangleBoundMax,
            eSegmentCenterPoint,
            eLineEndMin,
            eLineEndMax,
            eJunctionPoint,
            eLeftLineEnd,
            eRightLineEnd
        };

        CSweepLinePoint(const CFaceTriangle* pFaceTriangle, float boundX, CSweepLinePointType type) : m_pFaceTriangle(pFaceTriangle), m_point(boundX, 0.0, 0.0), m_sweepLinePointType(type) {}
        CSweepLinePoint(CScreenLine* pLine, SbVec3f point, CSweepLinePointType type) : m_pLine(pLine), m_point(point), m_sweepLinePointType(type) {}
        CSweepLinePoint(const CScreenLine* pLine, SbVec3f point, CSweepLinePointType type) : m_pConstLine(pLine), m_point(point), m_sweepLinePointType(type) {}
        CSweepLinePoint(CJunctionPoint* pJunctionPoint, SbVec3f point) : m_pJunctionPoint(pJunctionPoint), m_point(point), m_sweepLinePointType(eJunctionPoint) {}
        bool operator< (const CSweepLinePoint& slp) const
        {
            return m_point[0] < slp.m_point[0];
        }

        union
        {
            CScreenLine* m_pLine;
            const CScreenLine* m_pConstLine;
            const CFaceTriangle* m_pFaceTriangle;
            CJunctionPoint* m_pJunctionPoint;
        };
        SbVec3f m_point;
        CSweepLinePointType m_sweepLinePointType;
    };

    struct  CParametricLineRendererConfiguration
    {
        CVisibleScreenRegion visibleScreenRegion;

        float minScreenLineLength;

        float maxJunctionPointToSegmentDeviation;
        unsigned int minSegmentsPerJunction;
        float junctionSegmentLength;
        float maxJunctionLinesDotProduct;
        SbMatrix projectionMatrixGL;
    };

    CParametricLineRenderer();
    virtual ~CParametricLineRenderer();

    bool RenderVisibleLines();
    bool RenderJunctionPoints();
    bool Render(std::vector<CScreenLine>& renderedScreenLines, std::vector<CJunctionPoint>& renderedScreenJunctionPoints, bool renderJunctionPoints = false);
    bool Render(std::vector<CScreenLine>& renderedScreenLines);

    bool CheckModelPartsAreInView(float relativeInViewRatio = 0.0);

    bool ProjectModelFaceTrianglesToScreen(bool projectOnlyVisibleModelFaceTriangles = true);
    bool ProjectModelLinesToScreen(bool projectOnlyVisibleModelLines = true);

    CParametricLineRendererConfiguration GetConfiguration() const;
    void SetConfiguration(const CParametricLineRendererConfiguration& conf);

    int SetCurrentlyApparentVisibleModelElementsToVisible();

    void SetCameraPosition(const SbMatrix& viewingMatrixGL);
    void SetVisibleScreenRegion(float xMin, float xMax, float yMin, float yMax);
    void SetImageSize(float width, float height);

    void SetMinScreenLineLength(float minScreenLineLength);

    void SetMaxJunctionPointToSegmentDeviation(float maxJunctionPointToSegmentDeviation);
    void SetMinSegmentsPerJunction(unsigned int minSegmentsPerJunction);
    void SetJunctionSegmentLength(float junctionSegmentLength);
    float GetMaxJunctionLinesDotProduct() const;

    void SetMaxJunctionLinesDotProduct(float maxJunctionLinesDotProduct);
    void GetImageSize(float& width, float& height);
    float GetMinScreenLineLength() const;
    float GetMaxJunctionPointToSegmentDeviation() const;
    unsigned int GetMinSegmentsPerJunction() const;
    float GetJunctionSegmentLength() const;

    const std::vector<CScreenLine>& GetProjectedScreenLineList() const;
    const std::vector<CScreenLine>& GetVisibleScreenLineList() const;
    const std::vector<CFaceTriangle>& GetProjectedScreenTriangleList() const;
    const std::vector<CJunctionPoint>& GetProjectedScreenJunctionPointList() const;
    const std::vector<CJunctionPoint>& GetVisibleScreenJunctionPointList() const;

    const std::vector<CScreenLine>& GetTransformationBoundingBoxScreenLineList() const;
    const std::vector<CFaceTriangle>& GetTransformationBoundingBoxScreenFaceTriangleList() const;

    const CVisibleScreenRegion& GetVisibleScreenRegion() const;

    const SbMatrix& GetProjectionMatrixGl() const;
    void SetProjectionMatrixGl(const SbMatrix& projectionMatrixGl);

    void SetRenderingModel(CRenderingModel* pRenderingModel);
    const CRenderingModel* GetRenderingModel() const;

    bool WriteConfigurationToFile(const QString& filename);
    bool LoadConfigurationFromFile(const QString& filename);

    static QDomElement SerializeParametricLineRendererConfiguration(const CParametricLineRenderer& renderer)
    {

        QDomDocument tmp;
        QDomElement qDomElement = tmp.createElement("CParametricRendererConfiguration");

        QDomElement visibleScreenRegionQDomElement = renderer.m_visibleScreenRegion.SerializeToQDomElement();

        qDomElement.appendChild(visibleScreenRegionQDomElement);

        qDomElement.setAttribute("imageWidth", renderer.m_imageWidth);
        qDomElement.setAttribute("imageHeight", renderer.m_imageHeight);
        qDomElement.setAttribute("nearClippingPlane", renderer.m_nearClippingPlane);
        qDomElement.setAttribute("minScreenLineLength", renderer.m_rendererConfiguration.minScreenLineLength);

        qDomElement.setAttribute("maxJunctionPointToSegmentDeviation", renderer.m_rendererConfiguration.maxJunctionPointToSegmentDeviation);
        qDomElement.setAttribute("minSegmentsPerJunction", renderer.m_rendererConfiguration.minSegmentsPerJunction);
        qDomElement.setAttribute("junctionSegmentLength", renderer.m_rendererConfiguration.junctionSegmentLength);


        return qDomElement;
    }
    static bool DeserializeParametricLineRendererConfiguration(QDomElement& qDomElement, CParametricLineRenderer& renderer)
    {
        if (qDomElement.tagName() != "CParametricRendererConfiguration")
        {
            return false;
        }
        if (qDomElement.childNodes().size() != 1)
        {
            return false;
        }

        QDomElement visibleScreenRegionQDomElement = qDomElement.childNodes().item(0).toElement();


        if (visibleScreenRegionQDomElement.isNull())
        {
            return false;
        }

        QDomAttr attrImageWidth = qDomElement.attributeNode("imageWidth");
        QDomAttr attrImageHeight = qDomElement.attributeNode("imageHeight");
        QDomAttr attrNearClippingPlane = qDomElement.attributeNode("nearClippingPlane");
        QDomAttr attrMinScreenLineLength = qDomElement.attributeNode("minScreenLineLength");
        QDomAttr attrMinModelLineLength = qDomElement.attributeNode("minModelLineLength");
        QDomAttr attrMinModelFaceTriangleLineLength = qDomElement.attributeNode("minModelFaceTriangleLineLength");
        QDomAttr attrMinModelFaceTriangleArea = qDomElement.attributeNode("minModelFaceTriangleArea");
        QDomAttr attrMaxJunctionPointToSegmentDeviation = qDomElement.attributeNode("maxJunctionPointToSegmentDeviation");
        QDomAttr attrMinSegmentsPerJunction = qDomElement.attributeNode("minSegmentsPerJunction");
        QDomAttr attrJunctionSegmentLength = qDomElement.attributeNode("junctionSegmentLength");

        if (attrImageWidth.isNull() || attrImageHeight.isNull() ||
            attrNearClippingPlane.isNull() || attrMinScreenLineLength.isNull() ||
            attrMinModelLineLength.isNull() || attrMinModelFaceTriangleLineLength.isNull() ||
            attrMinModelFaceTriangleArea.isNull() || attrMaxJunctionPointToSegmentDeviation.isNull() ||
            attrMinSegmentsPerJunction.isNull() || attrJunctionSegmentLength.isNull())
        {
            return false;
        }

        bool valueValid;
        renderer.m_imageWidth = attrImageWidth.value().toFloat(&valueValid);
        renderer.m_imageHeight = attrImageHeight.value().toFloat(&valueValid);
        renderer.m_nearClippingPlane = attrNearClippingPlane.value().toFloat(&valueValid);
        renderer.m_rendererConfiguration.minScreenLineLength = attrMinScreenLineLength.value().toFloat(&valueValid);
        renderer.m_rendererConfiguration.maxJunctionPointToSegmentDeviation = attrMaxJunctionPointToSegmentDeviation.value().toFloat(&valueValid);
        renderer.m_rendererConfiguration.minSegmentsPerJunction = attrMinSegmentsPerJunction.value().toUInt(&valueValid);
        renderer.m_rendererConfiguration.junctionSegmentLength = attrJunctionSegmentLength.value().toFloat(&valueValid);
        if (!valueValid)
        {
            return false;
        }

        return renderer.m_visibleScreenRegion.DeserializeFromQDomElement(visibleScreenRegionQDomElement);
    }

    const SbMatrix& GetViewingMatrixGL() const;
    void SetViewingMatrixGL(const SbMatrix& viewingMatrixGL);

    /* Dynamic Element State Estimation. */

    const std::vector<CScreenLine>& GetStaticAndEstimatedProjectedScreenLineList() const;
    void SetStaticAndEstimatedProjectedScreenLineList(const std::vector<CScreenLine>& staticAndEstimatedProjectedScreenLineList);
    void AddEstimatedScreenLineList(const std::vector<CScreenLine>& estimatedScreenLineList);

    const std::vector<CFaceTriangle>& GetStaticAndEstimatedProjectedScreenFaceTriangleList() const;
    void SetStaticAndEstimatedProjectedScreenFaceTriangleList(const std::vector<CFaceTriangle>& staticAndEstimatedProjectedScreenFaceTriangleList);
    void AddEstimatedScreenFaceTriangleList(const std::vector<CFaceTriangle>& estimatedScreenFaceTriangleList);

    bool RenderDynamicElementParticle(std::vector<CScreenLine>& renderedScreenLines, std::list<CLine>& transformedLineList,
                                      std::list<CFaceTriangle>& transformedFaceTriangleList, DynamicElementParticle::FrontalFacePoints2D& frontalFacePoints,
                                      const SbVec3f& pA, const SbVec3f& pB, const SbVec3f& pC, const SbVec3f& pD);
    bool ProjectDynamicElementLinesToScreen(std::list<CLine>& transformedLineList, bool projectOnlyVisibleLines = true);
    bool ProjectDynamicElementFaceTrianglesToScreen(std::list<CFaceTriangle>& transformedFaceTriangleList, bool projectOnlyVisibleFaceTriangles = true);
    void CalculateDynamicElementLineIntersectionPoints(std::vector<CScreenLine>& projectedScreenLineList,
            const std::vector<CScreenLine>& staticAndEstimatedProjectedScreenLineList,
            std::vector<std::list<SbVec3f> >& intersectionPoints);
    void CalculateVisibleDynamicElementLineSegments(std::vector<CScreenLine>& projectedScreenLineList, const std::vector<CFaceTriangle>& projectedTriangleList,
            const std::vector<CFaceTriangle>& staticAndEstimatedProjectedScreenFaceTriangleList,
            std::vector<std::list<SbVec3f> >& intersectionPoints, std::vector<CScreenLine>& resultVisibleLines);
    void SweepLineDynamicElementVisibleSegmentsCalculation(std::list<std::list<CScreenLine> >& segmentedLinesList, const std::vector<CFaceTriangle>& projectedTriangleList,
            const std::vector<CFaceTriangle>& staticAndEstimatedProjectedScreenFaceTriangleList);

    bool Check2dTriangleIsHidden(const CFaceTriangle& triangle, const std::vector<CFaceTriangle>& triangleList, unsigned int numberOfLineChecksBetween);

    bool ProjectLinesToScreen(std::list<CLine>& lineList, std::vector<CScreenLine>& projectedLineList, bool projectOnlyVisibleLines = true);
    bool ProjectFaceTrianglesToScreen(std::list<CFaceTriangle>& faceTriangleList, std::vector<CFaceTriangle>& projectedFaceTriangleList, bool projectOnlyVisibleFaceTriangles = true);

    /* Change back to protected. */
    void Fit2dLinesToVisibleScreenRegion(const std::vector<CScreenLine>& lineList, const CVisibleScreenRegion& visibleScreenRegion, std::vector<CScreenLine>& resultLineList);
    enum CIntersect2dLinesResult
    {
        eNoIntersectionPoint,
        eVirtualIntersectionPointFirstOrder,
        eVirtualIntersectionPointSecondOrder,
        eRealIntersectionPoint
    };
    CIntersect2dLinesResult Intersect2dLines(const CLine& line1, const CLine& line2, SbVec3f& intersectionPoint1, SbVec3f& intersectionPoint2);
    bool Check2dPointIsHidden(const SbVec3f& point, const std::vector<CFaceTriangle>& triangleList);

    bool ProjectModelPointToScreen(const SbVec3f& modelPoint, SbVec2f& screenPoint);

protected:
    inline CFaceTriangle ProjectEyeFaceTriangleToScreen(const SbMatrix& projectionMatrixGL, const SbVec4f& pointEye1, const SbVec4f& pointEye2, const SbVec4f& pointEye3, unsigned int imageWidth, unsigned int imageHeight, CFaceTriangle* pWorldFaceTriangleReference);

    inline bool ProjectFaceTriangleToScreen(const SbMatrix& projectionMatrixGL, const SbMatrix& viewingMatrixGL, CFaceTriangle& worldFaceTriangle,
                                            unsigned int imageWidth, unsigned int imageHeight, float nearClippingPlane,
                                            std::vector<CFaceTriangle>& resultScreenTriangleList);
    inline bool ProjectLineToScreen(const SbMatrix& projectionMatrixGL, const SbMatrix& viewingMatrixGL, const CLine& worldLine, unsigned int imageWidth,
                                    unsigned int imageHeight, float nearClippingPlane, CScreenLine& resultScreenLine);
    inline bool ProjectModelJunctionPointToScreen(const SbMatrix& projectionMatrixGL, const SbMatrix& viewingMatrixGL,
            const CJunctionPoint& worldJunctionPoint, unsigned int imageWidth, unsigned int imageHeight,
            float nearClippingPlane,  CJunctionPoint& resultScreenJunctionPoint);

    inline bool RenderVisibleLines(std::vector<CScreenLine>& visibleScreenLines);
    inline bool RenderJunctionPoints(std::vector<CScreenLine>& visibleScreenLines, std::vector<CJunctionPoint>& visibleScreenJunctionPoints);
    inline void CalculateLineIntersectionPoints(std::vector<CScreenLine>& projectedScreenLineList, std::vector<CScreenLine>& secondaryLineList,
            std::vector<std::list<SbVec3f> >& intersectionPoints);
    inline void CalculateVisibleLineSegments(std::vector<CScreenLine>& projectedScreenLineList, const std::vector<CFaceTriangle>& projectedTriangleList,
            std::vector<std::list<SbVec3f> >& intersectionPoints, std::vector<CScreenLine>& resultVisibleLines);
    inline void SweepLineVisibleSegmentsCalculation(std::list<std::list<CScreenLine> >& segmentedLinesList,
            const std::vector<CFaceTriangle>& projectedTriangleList);
    inline void SweepLineFitScreenLinesToJunctionPoint(std::vector<CJunctionPoint>& junctionPointsList, const std::vector<CScreenLine>& screenLineList,
            float junctionScreenSegmentLength, float minJunctionSegmentDeviation);

    static inline float Calc2dLineLength(const CLine& line)
    {
        SbVec3f l1 = line.m_p1;
        SbVec3f l2 = line.m_p2;
        float dx = l1[0] - l2[0];
        float dy = l1[1] - l2[1];
        return sqrt(dx * dx + dy * dy);
    }

    static inline float Calc2dPointDistance(const SbVec3f& p1, const SbVec3f& p2)
    {
        float dx = p1[0] - p2[0];
        float dy = p1[1] - p2[1];
        return sqrt(dx * dx + dy * dy);
    }

    static inline float Calc3dDistancePointLine(const SbVec3f& point, const CLine& line)
    {
        SbVec3f r = line.m_p2 - line.m_p1;

        float lambda = (point - line.m_p1).dot(r) / r.dot(r);

        if (lambda > 1.0)
        {
            lambda = 1.0;
        }
        else if (lambda < 0.0)
        {
            lambda = 0.0;
        }

        return (point - (r * lambda + line.m_p1)).length();
    }

    inline bool Intersect2dLineWithVerticalBorder(const SbVec3f& lineOffsetPoint, const SbVec3f& lineDirection, float borderX, SbVec3f& intersectionPoint);
    inline bool Intersect2dLineWithHorizontalBorder(const SbVec3f& lineOffsetPoint, const SbVec3f& lineDirection, float borderY, SbVec3f& intersectionPoint);
    inline bool CheckPointHiddenZDistDifference(const float& zDistDifference)
    {
        const float maxCoverDistance = 1;
        return zDistDifference < -maxCoverDistance;
    }
    inline bool ConditionalAddLineToList(const CScreenLine& line, std::vector<CScreenLine>& lineList)
    {
        if (line.CalculateLength2d() >= m_rendererConfiguration.minScreenLineLength)
        {
            assert(line.CalculateLength2d() < 2000);
            lineList.push_back(line);
            return true;
        }
        return false;
    }

    inline bool ConditionalAddProjectedLineToList(const CScreenLine& line, std::vector<CScreenLine>& lineList)
    {
        if (line.CalculateLength2d() >= 2)
        {
            lineList.push_back(line);
            return true;
        }
        return false;
    }

    inline bool ConditionalAddJunctionLineToList(const CLine& line, std::list<CLine>& junctionLines)
    {
        if (junctionLines.size() > 0)
        {
            for (std::list<CLine>::iterator junctionLineIterator = junctionLines.begin(); junctionLineIterator != junctionLines.end(); ++junctionLineIterator)
            {
                if (junctionLineIterator->CalculateNormalizedDirectionScalarProduct2d(line) > m_rendererConfiguration.maxJunctionLinesDotProduct)
                {
                    return false;
                }
            }
        }
        junctionLines.push_back(line);
        return true;
    }

    inline bool Check2dPointIsHiddenByTriangle(const SbVec3f& point, const CFaceTriangle& triangle);

    class CPoint2d
    {
    public:
        CPoint2d(SbVec3f pointVector, bool isVisible = true) : m_pointVector(pointVector), m_isVisible(isVisible) {}
        SbVec3f m_pointVector;
        bool m_isVisible;
    };
    unsigned int Check2dPointsAreHidden(std::list<CPoint2d>& pointList, const std::vector<CFaceTriangle>& triangleList);
    unsigned int Check2dLineIsHidden(const CLine& line, const std::vector<CFaceTriangle>& triangleList, unsigned int numberOfLineChecksBetween, std::list<CPoint2d>& checkedPointsResultList);

    enum CScreenRegionType
    {
        eRegionInScreen,
        eRegionOutTopLeft,
        eRegionOutTop,
        eRegionOutTopRight,
        eRegionOutLeft,
        eRegionOutRight,
        eRegionOutBottomLeft,
        eRegionOutBottom,
        eRegionOutBottomRight
    };

    inline bool CheckPointInVisibleScreenRegion(const SbVec3f& point, const CVisibleScreenRegion& visibleScreenRegion);
    inline CScreenRegionType GetPointScreenRegion(const SbVec3f& point, const CVisibleScreenRegion& visibleScreenRegion) const;
    inline bool Fit2dPointToVisibleScreenRegion(SbVec3f& pointOffset, const SbVec3f& lineDirection, const CVisibleScreenRegion& visibleScreenRegion);

    static bool compare_SbVec3fPosition(const SbVec3f& p1, const SbVec3f& p2)
    {
        if (p1[0] < p2[0])
        {
            return true;
        }
        else
        {
            if (p1[0] > p2[0])
            {
                return false;
            }
        }
        return p1[1] < p2[1];
    }

    CRenderingModel* m_pRenderingModel;

    std::vector<CScreenLine> m_projectedScreenLineList;
    std::vector<CScreenLine> m_visibleScreenLineList;
    std::vector<CFaceTriangle> m_projectedScreenFaceTriangleList;

    std::vector<CJunctionPoint> m_projectedScreenJunctionPointList;
    std::vector<CJunctionPoint> m_visibleScreenJunctionPointList;

    std::vector<std::list<SbVec3f> > m_intersectionPoints;

    SbMatrix m_viewingMatrixGL;

    float m_imageWidth;
    float m_imageHeight;

    CVisibleScreenRegion m_visibleScreenRegion;

    CParametricLineRendererConfiguration m_rendererConfiguration;

    float m_nearClippingPlane;

    std::vector<CScreenLine> m_staticAndEstimatedProjectedScreenLineList;
    std::vector<CFaceTriangle> m_staticAndEstimatedProjectedScreenFaceTriangleList;

    std::vector<CScreenLine> m_projectedTransformationBoundingBoxLineList;
    std::vector<CFaceTriangle> m_projectedTransformationBoundingBoxFaceTriangleList;
};

