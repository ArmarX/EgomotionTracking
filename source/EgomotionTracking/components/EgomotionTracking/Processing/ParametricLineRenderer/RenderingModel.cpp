#include "RenderingModel.h"

#include <EgomotionTracking/components/RVL/Representation/ModelBased/GeometricGraph/Loaders/OpenInventor/OpenInventorModelLoader.h>
#include <EgomotionTracking/components/RVL/Representation/ModelBased/GeometricGraph/Loaders/XmlModelLoader.h>
#include <EgomotionTracking/components/RVL/Representation/ModelBased/GeometricGraph/Base/ModelMultipleMesh.h>
#include <EgomotionTracking/components/RVL/Mathematics/_3D/Vector3D.h>

using namespace RVL::Representation::ModelBased::GeometricGraph::Base;
using namespace RVL::Representation::ModelBased::GeometricGraph::Visualization::OpenInventor;
using namespace RVL::Representation::ModelBased::GeometricGraph::Loaders;
using namespace RVL::Mathematics::_3D;
using namespace RVL::Containers::_3D;

CRenderingModel::CRenderingModel() :
    m_globalModelElementsTransformation(SbMatrix::identity())
{
    m_minModelFaceTriangleArea = 2000;
    m_minModelLineLength = 10;
}

void CRenderingModel::collect3dLines_cb(void* userdata,
                                        SoCallbackAction* /*action*/, const SoPrimitiveVertex* v1,
                                        const SoPrimitiveVertex* v2)
{

    const SbVec3f p1 = v1->getPoint();
    const SbVec3f p2 = v2->getPoint();
    std::list<CLine>* lineList = (std::list<CLine>*) userdata;

    CLine line3d(p1, p2, true);

    lineList->push_back(line3d);
}


void CRenderingModel::collect3dTriangles_cb(void* userdata,
        SoCallbackAction* /*action*/, const SoPrimitiveVertex* v1,
        const SoPrimitiveVertex* v2, const SoPrimitiveVertex* v3)
{
    const SbVec3f p1 = v1->getPoint();
    const SbVec3f p2 = v2->getPoint();
    const SbVec3f p3 = v3->getPoint();


    std::list<CFaceTriangle>* triangleList = static_cast<std::list<CFaceTriangle>*>(userdata);
    triangleList->push_back(CFaceTriangle(p1, p2, p3));
}

void CRenderingModel::GenerateModelLineList(SoNode* pLineModel)
{
    m_modelLineList.clear();

    std::list<CLine> lineList;
    SoCallbackAction ca;
    ca.addLineSegmentCallback(SoIndexedLineSet::getClassTypeId(), collect3dLines_cb, &lineList);

    double time1 = 0.0;
    double tstart = clock();

    ca.apply(pLineModel);

    time1 = clock() - tstart;
    time1 = time1 / CLOCKS_PER_SEC;
    std::cout << "Model Line List Generation took " << time1 << " seconds!" << std::endl;

    std::cout << "collected " << lineList.size() << " lines" << std::endl;

    RemoveRedundantLines();
}

void CRenderingModel::RemoveRedundantLines()
{
    const float minSeperatingDistance = FLT_EPSILON;
    std::list<CLine> lineList = m_modelLineList;
    m_modelLineList.clear();
    /* remove redundant lines */
    if (lineList.size() > 1)
    {
        std::list<CLine>::iterator leftLineIterator = lineList.begin();
        for (; leftLineIterator != lineList.end(); ++leftLineIterator)
        {
            bool redundant = false;
            std::list<CLine>::iterator rightLineIterator = lineList.end();
            --rightLineIterator;
            for (; leftLineIterator != rightLineIterator; --rightLineIterator)
            {
                if ((leftLineIterator->m_p1 - rightLineIterator->m_p1).length() < minSeperatingDistance
                    && (leftLineIterator->m_p2 - rightLineIterator->m_p2).length() < minSeperatingDistance)
                {
                    redundant = true;
                }
                else
                {
                    if ((leftLineIterator->m_p1 - rightLineIterator->m_p2).length() < minSeperatingDistance
                        && (leftLineIterator->m_p2 - rightLineIterator->m_p1).length() < minSeperatingDistance)
                    {
                        redundant = true;
                    }
                }
            }
            if (!redundant)
            {
                if ((leftLineIterator->m_p1 - leftLineIterator->m_p2).length() >= m_minModelLineLength)
                {
                    m_modelLineList.push_back(*leftLineIterator);
                }
                else
                {
                    //                    if (leftLineIterator->m_pModelEdgeReference != NULL)
                    //                    {
                    //                        leftLineIterator->m_pModelEdgeReference->SetVisibility(RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement::eStaticOccluded);

                    //                    }
                }
            }
            else
            {
                //                if (leftLineIterator->m_pModelEdgeReference != NULL)
                //                {
                //                    leftLineIterator->m_pModelEdgeReference->SetVisibility(RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement::eStaticOccluded);
                //                }
            }
        }
    }
    std::cout << "ModelLines after redundancy filtering: " << m_modelLineList.size() << std::endl;
}

void CRenderingModel::GenerateModelFaceTriangleList(SoNode* /*pLineModel*/,
        SoNode* pFaceModel)
{
    m_modelFaceTriangleList.clear();

    SoCallbackAction ca;
    ca.addTriangleCallback(SoFaceSet::getClassTypeId(), collect3dTriangles_cb, &m_modelFaceTriangleList);

    double time1 = 0.0;
    double tstart = clock();

    ca.apply(pFaceModel);

    time1 = clock() - tstart;
    time1 = time1 / CLOCKS_PER_SEC;
    std::cout << "Model Face Triangle List Generation took " << time1 << " seconds!" << std::endl;
    std::cout << "collected " << m_modelFaceTriangleList.size() << " triangles" << std::endl;

    RemoveRedundantFaceTriangles();

    m_modelFaceTriangleList.sort(compare_faceArea2d);
}

void CRenderingModel::RemoveRedundantFaceTriangles()
{

    const float minSeperatingDistance = FLT_EPSILON;
    std::list<CFaceTriangle> triangleList = m_modelFaceTriangleList;
    m_modelFaceTriangleList.clear();
    /* remove redundant triangles */
    if (triangleList.size() > 1)
    {
        std::list<CFaceTriangle>::iterator leftTriangleIterator = triangleList.begin();
        for (; leftTriangleIterator != triangleList.end(); ++leftTriangleIterator)
        {
            bool redundant = false;
            std::list<CFaceTriangle>::iterator rightTriangleIterator = triangleList.end();
            --rightTriangleIterator;
            for (; leftTriangleIterator != rightTriangleIterator; --rightTriangleIterator)
            {
                if (accumulatedTrianglePointDistance(*leftTriangleIterator, *rightTriangleIterator) < minSeperatingDistance)
                {
                    redundant = true;
                }
            }
            if (!redundant)
            {
                if (leftTriangleIterator->CalculateArea() >= m_minModelFaceTriangleArea)
                {
                    m_modelFaceTriangleList.push_back(*leftTriangleIterator);
                }
                else
                {
                    //                    if (leftTriangleIterator->m_pModelFaceReference != NULL)
                    //                    {
                    //                        leftTriangleIterator->m_pModelFaceReference->SetVisibility(RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement::eStaticOccluded);
                    //                    }
                }
            }
            else
            {
                //                if (leftTriangleIterator->m_pModelFaceReference != NULL)
                //                {
                //                    leftTriangleIterator->m_pModelFaceReference->SetVisibility(RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement::eStaticOccluded);
                //                }
            }
        }
    }
    std::cout << "ModelTriangles after redundancy filtering: " << m_modelFaceTriangleList.size() << std::endl;
}

void CRenderingModel::GenerateModelsFromSceneGraph(SoNode* pLineModel,
        SoNode* pFaceModel)
{
    GenerateModelLineList(pLineModel);
    GenerateModelFaceTriangleList(pLineModel, pFaceModel);
}

void CRenderingModel::AddModelLine(const CLine& modelLine)
{
    m_modelLineList.push_back(modelLine);
}

bool CRenderingModel::CalculateAlignedModelBoundingBox(SbVec3f& min, SbVec3f& max) const
{
    if (m_modelLineList.size() > 0)
    {
        min[0] = min[1] = min[2] = FLT_MAX;
        max[0] = max[1] = max[2] = -FLT_MAX;
        for (std::list<CLine>::const_iterator modelLineIterator = m_modelLineList.begin(); modelLineIterator != m_modelLineList.end(); ++ modelLineIterator)
        {
            SbVec3f p1Transformed, p2Transformed;
            m_globalModelElementsTransformation.multVecMatrix(modelLineIterator->m_p1, p1Transformed);
            m_globalModelElementsTransformation.multVecMatrix(modelLineIterator->m_p2, p2Transformed);

            for (int i = 0; i < 3; ++i)
            {
                if (p1Transformed[i] < min[i])
                {
                    min[i] = p1Transformed[i];
                }
                if (p2Transformed[i] < min[i])
                {
                    min[i] = p2Transformed[i];
                }

                if (p1Transformed[i] > max[i])
                {
                    max[i] = p1Transformed[i];
                }
                if (p2Transformed[i] > max[i])
                {
                    max[i] = p2Transformed[i];
                }
            }
        }
        return true;
    }

    return false;
}

float CRenderingModel::accumulatedTrianglePointDistanceFixedOrder(
    const CFaceTriangle& t1, SbVec3f t2p1, SbVec3f t2p2, SbVec3f t2p3)
{
    return (t1.m_p1 - t2p1).length() + (t1.m_p2 - t2p2).length() + (t1.m_p3 - t2p3).length();
}

float CRenderingModel::accumulatedTrianglePointDistance(const CFaceTriangle& t1,
        const CFaceTriangle& t2)
{
    float minATPD = accumulatedTrianglePointDistanceFixedOrder(t1, t2.m_p1, t2.m_p2, t2.m_p3);
    minATPD = std::min(minATPD, accumulatedTrianglePointDistanceFixedOrder(t1, t2.m_p1, t2.m_p3, t2.m_p2));

    minATPD = std::min(minATPD, accumulatedTrianglePointDistanceFixedOrder(t1, t2.m_p2, t2.m_p1, t2.m_p3));
    minATPD = std::min(minATPD, accumulatedTrianglePointDistanceFixedOrder(t1, t2.m_p2, t2.m_p3, t2.m_p1));

    minATPD = std::min(minATPD, accumulatedTrianglePointDistanceFixedOrder(t1, t2.m_p3, t2.m_p1, t2.m_p2));
    minATPD = std::min(minATPD, accumulatedTrianglePointDistanceFixedOrder(t1, t2.m_p3, t2.m_p2, t2.m_p1));

    return minATPD;
}

bool CRenderingModel::WriteModelFaceTrianglesToFile(
    const QString& filename, bool onlyVisibleFaceTriangles)
{
    QDomDocument modelFaceTrianglesDomDocument("ModelFaceTriangles");
    modelFaceTrianglesDomDocument.appendChild(SerializeFaceTriangleList(m_modelFaceTriangleList, "ModelFaceTrianglesList", onlyVisibleFaceTriangles));

    XMLPrimitives::WriteXMLDocumentToFile(modelFaceTrianglesDomDocument, filename);
    return true;
}

bool CRenderingModel::WriteModelLinesToFile(const QString& filename,
        bool onlyVisibleLines)
{
    QDomDocument modelLinesDomDocument("ModeLines");
    modelLinesDomDocument.appendChild(SerializeLineList(m_modelLineList, "ModelLineList", onlyVisibleLines));

    return XMLPrimitives::WriteXMLDocumentToFile(modelLinesDomDocument, filename);
}

bool CRenderingModel::LoadModelFaceTrianglesFromFile(
    const QString& filename, bool onlyVisibleFaceTriangles)
{
    QDomDocument doc;
    if (!XMLPrimitives::ReadXMLDocumentFromFile(doc, filename))
    {
        return false;
    }
    m_modelFaceTriangleList.clear();
    QDomElement rootElement = doc.documentElement();
    unsigned int loadedElements = DeserializeFaceTriangleList(rootElement, onlyVisibleFaceTriangles, m_modelFaceTriangleList);

    return loadedElements > 0;
}

bool CRenderingModel::LoadModelLinesFromFile(
    const QString& filename, bool onlyVisibleLines)
{
    QDomDocument doc;
    if (!XMLPrimitives::ReadXMLDocumentFromFile(doc, filename))
    {
        return false;
    }
    m_modelLineList.clear();
    QDomElement rootElement = doc.documentElement();
    unsigned int loadedElements = DeserializeLineList(rootElement, onlyVisibleLines, m_modelLineList);

    return loadedElements > 0;
}


std::list<CFaceTriangle>& CRenderingModel::GetModelFaceTriangleList()
{
    return m_modelFaceTriangleList;
}

std::list<CLine>& CRenderingModel::GetModelLineList()
{
    return m_modelLineList;
}

void CRenderingModel::SetMinModelLineLength(float minModelLineLength)
{
    m_minModelLineLength = minModelLineLength;
}

float CRenderingModel::GetMinModelLineLength()
{
    return m_minModelLineLength;
}

void CRenderingModel::SetMinModelFaceTriangleArea(float minModelFaceTriangleArea)
{
    m_minModelFaceTriangleArea = minModelFaceTriangleArea;
}

float CRenderingModel::GetMinModelFaceTriangleArea()
{
    return m_minModelFaceTriangleArea;
}

void CRenderingModel::SetGlobalModelElementsTransformation(const SbMatrix& globalTransformation)
{
    m_globalModelElementsTransformation = globalTransformation;
}

const SbMatrix& CRenderingModel::GetGlobalModelElementsTransformation()
{
    return m_globalModelElementsTransformation;
}

void CRenderingModel::SetShortModelLinesToInvisible()
{
    if (m_modelLineList.size() > 0)
    {
        for (std::list<CLine>::iterator lineIterator = m_modelLineList.begin();
             lineIterator != m_modelLineList.end(); ++lineIterator)
        {
            if ((lineIterator->m_p1 - lineIterator->m_p2).length() < m_minModelLineLength)
            {
                lineIterator->m_isVisible = false;
            }
        }
    }
}

void CRenderingModel::SetSmallTriangleAreaToInvisible()
{
    if (m_modelFaceTriangleList.size() > 0)
    {
        for (std::list<CFaceTriangle>::iterator faceTriangleIterator = m_modelFaceTriangleList.begin();
             faceTriangleIterator != m_modelFaceTriangleList.end(); ++faceTriangleIterator)
        {

            if (faceTriangleIterator->CalculateArea() < m_minModelFaceTriangleArea)
            {
                faceTriangleIterator->m_isVisible = false;
            }
        }
    }
}

void CRenderingModel::SetAllModelElementsVisibility(bool visibility)
{
    if (m_modelLineList.size() > 0)
    {
        for (std::list<CLine>::iterator lineIterator = m_modelLineList.begin();
             lineIterator != m_modelLineList.end(); ++lineIterator)
        {

            lineIterator->m_isVisible = visibility;
        }
    }

    if (m_modelFaceTriangleList.size() > 0)
    {
        for (std::list<CFaceTriangle>::iterator faceTriangleIterator = m_modelFaceTriangleList.begin();
             faceTriangleIterator != m_modelFaceTriangleList.end(); ++faceTriangleIterator)
        {

            faceTriangleIterator->m_isVisible = visibility;
        }
    }
}

unsigned int CRenderingModel::GetNumberOfVisibleModelLines() const
{
    unsigned int numberOfVisibleModelLines = 0;

    if (m_modelLineList.size() > 0)
    {
        for (std::list<CLine>::const_iterator lineIterator = m_modelLineList.begin(); lineIterator != m_modelLineList.end(); ++ lineIterator)
        {
            if (lineIterator->m_isVisible)
            {
                ++numberOfVisibleModelLines;
            }
        }
    }
    return numberOfVisibleModelLines;
}

unsigned int CRenderingModel::GetNumberOfVisibleModelFaceTriangles() const
{
    unsigned int numberOfVisibleModelFaceTriangles = 0;
    if (m_modelFaceTriangleList.size() > 0)
    {
        for (std::list<CFaceTriangle>::const_iterator faceTriangleIterator = m_modelFaceTriangleList.begin();
             faceTriangleIterator != m_modelFaceTriangleList.end(); ++faceTriangleIterator)
        {

            if (faceTriangleIterator->m_isVisible)
            {
                ++numberOfVisibleModelFaceTriangles;
            }
        }
    }
    return numberOfVisibleModelFaceTriangles;
}

bool CRenderingModel::loadRVLModelFromXMLFile(const std::string& filename)
{
    m_modelLineList.clear();
    m_modelFaceTriangleList.clear();

    RVL::Representation::ModelBased::GeometricGraph::Loaders::CXmlModelLoader xmlModelLoader;
    m_pModelMultipleMesh =  xmlModelLoader.LoadFromFile(filename);
    if (m_pModelMultipleMesh == NULL)
    {
        return false;
    }

    importRVLDataStructure();

    std::cout << "Loaded " << m_modelLineList.size() << " lines and " << m_modelFaceTriangleList.size() << " face triangles" << std::endl;

    RemoveRedundantLines();
    RemoveRedundantFaceTriangles();

    extractTransformationBoundingBoxes();

    std::cout << "Using " << m_modelLineList.size() << " lines and " << m_modelFaceTriangleList.size() << " face triangles" << std::endl;

    return true;
}

bool CRenderingModel::saveRVLModelToXMLFile(const std::string& filename)
{
    return m_pModelMultipleMesh->SaveToFile(filename, true);
}

void CRenderingModel::importRVLDataStructure()
{
    const std::map<int, CModelMesh*>& modelMeshes = m_pModelMultipleMesh->GetMeshes();

    for (std::map<int, CModelMesh*>::const_iterator modelMeshesIterator = modelMeshes.begin(); modelMeshesIterator != modelMeshes.end(); ++modelMeshesIterator)
    {
        if (!modelMeshesIterator->second->IsDynamic())
        {
            const std::map<int, CModelEdge*>& modelEdges = modelMeshesIterator->second->GetEdges();

            for (std::map<int, CModelEdge*>::const_iterator modelEdgesIterator = modelEdges.begin(); modelEdgesIterator != modelEdges.end(); ++modelEdgesIterator)
            {
                if (modelEdgesIterator->second->GetVisibility(true) == CModelElement::eStaticVisible &&
                    modelEdgesIterator->second->GetVisibility(false) == CModelElement::eStaticVisible)
                {
                    const CModelVertex* vertexA = modelEdgesIterator->second->GetReadOnlyVertexA();
                    const CModelVertex* vertexB = modelEdgesIterator->second->GetReadOnlyVertexB();
                    const CVector3D& pointA = vertexA->GetPoint(true);
                    const CVector3D& pointB = vertexB->GetPoint(true);
                    SbVec3f p1(pointA.GetX(), pointA.GetY(), pointA.GetZ());
                    SbVec3f p2(pointB.GetX(), pointB.GetY(), pointB.GetZ());
                    CLine line(p1, p2);
                    line.m_pModelEdgeReference = modelEdgesIterator->second;
                    m_modelLineList.push_back(line);
                }
            }

            const std::map<int, CModelFace*>& modelFaces = modelMeshesIterator->second->GetFaces();

            for (std::map<int, CModelFace*>::const_iterator modelFacesIterator = modelFaces.begin(); modelFacesIterator != modelFaces.end(); ++modelFacesIterator)
            {
                if (modelFacesIterator->second->GetVisibility(true) == CModelElement::eStaticVisible &&
                    modelFacesIterator->second->GetVisibility(false) == CModelElement::eStaticVisible)
                {
                    std::vector<CVector3D> modelPoints = modelFacesIterator->second->GetVertexPoints(true);
                    SbVec3f p1(modelPoints[0].GetX(), modelPoints[0].GetY(), modelPoints[0].GetZ());
                    SbVec3f p2(modelPoints[1].GetX(), modelPoints[1].GetY(), modelPoints[1].GetZ());
                    SbVec3f p3(modelPoints[2].GetX(), modelPoints[2].GetY(), modelPoints[2].GetZ());
                    CFaceTriangle faceTriangle(p1, p2, p3);
                    faceTriangle.m_pModelFaceReference = modelFacesIterator->second;
                    m_modelFaceTriangleList.push_back(faceTriangle);
                }
            }
        }
    }
}

const RVL::Representation::ModelBased::GeometricGraph::Base::CModelMultipleMesh* CRenderingModel::getModelMultipleMesh() const
{
    return m_pModelMultipleMesh;
}

void CRenderingModel::extractTransformationBoundingBoxes()
{
    std::map<int, CModelTransformation*> modelTransformations = m_pModelMultipleMesh->GetTransformations();

    for (std::map<int, CModelTransformation*>::iterator transformationIterator = modelTransformations.begin(); transformationIterator != modelTransformations.end(); ++transformationIterator)
    {
        CContinuousBoundingBox3D transformationBoundingBox;

        const std::map<int, CModelMesh*>& meshes = transformationIterator->second->GetMeshes();

        if (transformationIterator->second->GetTransformationType() == CModelTransformation::eTranslation)
        {
            static_cast<CModelTranslation*>(transformationIterator->second)->SetParametricNormalizedTranslation(0.0);
        }
        else if (transformationIterator->second->GetTransformationType() == CModelTransformation::eRotation)
        {
            static_cast<CModelRotation*>(transformationIterator->second)->SetParametricNormalizedRotation(0.0);
        }

        for (std::map<int, CModelMesh*>::const_iterator meshIterator = meshes.begin(); meshIterator != meshes.end(); ++meshIterator)
        {
            transformationBoundingBox.Extend(meshIterator->second->GetBoundingBox());
        }

        if (transformationIterator->second->GetTransformationType() == CModelTransformation::eTranslation)
        {
            static_cast<CModelTranslation*>(transformationIterator->second)->SetParametricNormalizedTranslation(1.0);
        }
        else if (transformationIterator->second->GetTransformationType() == CModelTransformation::eRotation)
        {
            static_cast<CModelRotation*>(transformationIterator->second)->SetParametricNormalizedRotation(1.0);
        }

        for (std::map<int, CModelMesh*>::const_iterator meshIterator = meshes.begin(); meshIterator != meshes.end(); ++meshIterator)
        {
            transformationBoundingBox.Extend(meshIterator->second->GetBoundingBox());
        }

        /* In the case of a rotation with a range greater than 90 degrees, the BB at 90 degrees must also be considered. */
        if (transformationIterator->second->GetTransformationType() == CModelTransformation::eRotation)
        {
            CModelRotation* modelRotation = static_cast<CModelRotation*>(transformationIterator->second);

            if (modelRotation->GetRotationRange() > M_PI_2)
            {
                modelRotation->SetParametricNormalizedRotation(1.0);

                for (std::map<int, CModelMesh*>::const_iterator meshIterator = meshes.begin(); meshIterator != meshes.end(); ++meshIterator)
                {
                    transformationBoundingBox.Extend(meshIterator->second->GetBoundingBox());
                }
            }
        }

        /* Vertices. */

        /* Max/Min points. */
        SbVec3f pointFrontRightUp(transformationBoundingBox.GetMaxPoint().GetX(), transformationBoundingBox.GetMaxPoint().GetY(), transformationBoundingBox.GetMaxPoint().GetZ());
        SbVec3f pointBackLeftBottom(transformationBoundingBox.GetMinPoint().GetX(), transformationBoundingBox.GetMinPoint().GetY(), transformationBoundingBox.GetMinPoint().GetZ());
        /* Front face. */
        SbVec3f pointFrontRightBottom(pointFrontRightUp[0], pointFrontRightUp[1], pointBackLeftBottom[2]);
        SbVec3f pointFrontLeftBottom(pointFrontRightUp[0], pointBackLeftBottom[1], pointBackLeftBottom[2]);
        SbVec3f pointFrontLeftUp(pointFrontRightUp[0], pointBackLeftBottom[1], pointFrontRightUp[2]);
        /* Back face. */
        SbVec3f pointBackLeftUp(pointBackLeftBottom[0], pointBackLeftBottom[1], pointFrontRightUp[2]);
        SbVec3f pointBackRightUp(pointBackLeftBottom[0], pointFrontRightUp[1], pointFrontRightUp[2]);
        SbVec3f pointBackRightBottom(pointBackLeftBottom[0], pointFrontRightUp[1], pointBackLeftBottom[2]);

        /* FaceTriangles. */

        /* Front. */
        m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointFrontLeftUp, pointFrontRightUp, pointFrontLeftBottom));
        m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointFrontRightBottom, pointFrontLeftBottom, pointFrontRightUp));
        /* Right. */
        m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointFrontRightUp, pointBackRightUp, pointFrontRightBottom));
        m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointBackRightBottom, pointFrontRightBottom, pointBackRightUp));
        /* Left. */
        m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointBackLeftUp, pointFrontLeftUp, pointBackLeftBottom));
        m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointFrontLeftBottom, pointBackLeftBottom, pointFrontLeftUp));
        /* Back. */
        m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointBackRightUp, pointBackLeftUp, pointBackRightBottom));
        m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointBackLeftBottom, pointBackRightBottom, pointBackLeftUp));
        /* Up. */
        m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointBackLeftUp, pointBackRightUp, pointFrontLeftUp));
        m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointFrontRightUp, pointFrontLeftUp, pointBackRightUp));
        /* Bottom. */
        m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointFrontLeftBottom, pointFrontRightBottom, pointBackLeftBottom));
        m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointBackRightBottom, pointBackLeftBottom, pointFrontRightBottom));

        /* Lines. */

        /* Front. */
        m_transformationBoundingBoxLineList.push_back(CLine(pointFrontRightUp, pointFrontRightBottom));
        m_transformationBoundingBoxLineList.push_back(CLine(pointFrontRightBottom, pointFrontLeftBottom));
        m_transformationBoundingBoxLineList.push_back(CLine(pointFrontLeftBottom, pointFrontLeftUp));
        m_transformationBoundingBoxLineList.push_back(CLine(pointFrontLeftUp, pointFrontRightUp));
        /* Back. */
        m_transformationBoundingBoxLineList.push_back(CLine(pointBackLeftUp, pointBackLeftBottom));
        m_transformationBoundingBoxLineList.push_back(CLine(pointBackLeftBottom, pointBackRightBottom));
        m_transformationBoundingBoxLineList.push_back(CLine(pointBackRightBottom, pointBackRightUp));
        m_transformationBoundingBoxLineList.push_back(CLine(pointBackRightUp, pointBackLeftUp));
        /* Sides. */
        m_transformationBoundingBoxLineList.push_back(CLine(pointFrontRightUp, pointBackRightUp));
        m_transformationBoundingBoxLineList.push_back(CLine(pointFrontRightBottom, pointBackRightBottom));
        m_transformationBoundingBoxLineList.push_back(CLine(pointFrontLeftBottom, pointBackLeftBottom));
        m_transformationBoundingBoxLineList.push_back(CLine(pointFrontLeftUp, pointBackLeftUp));
    }
}

std::list<CLine>& CRenderingModel::getTransformationBoundingBoxLineList()
{
    return m_transformationBoundingBoxLineList;
}

std::list<CFaceTriangle>& CRenderingModel::getTransformationBoundingBoxFaceTriangleList()
{
    return m_transformationBoundingBoxFaceTriangleList;
}

bool CRenderingModel::loadMultipleVisualizationRVLModelsFromXMLFile(const std::string& filename, unsigned int amountToLoad)
{
    if (!amountToLoad)
    {
        return false;
    }

    m_pVisualizationModelMultipleMeshMap.clear();
    m_pVisualizationOIModelMultipleMeshMap.clear();

    CXmlModelLoader xmlModelLoader;

    for (unsigned int i = 0; i < amountToLoad; ++i)
    {
        CModelMultipleMesh* pModelMultipleMesh = xmlModelLoader.LoadFromFile(filename);
        if (!pModelMultipleMesh)
        {
            return false;
        }
        m_pVisualizationModelMultipleMeshMap.insert(std::pair<int, CModelMultipleMesh*>(i, pModelMultipleMesh));

        COpenInventorModelMultipleMesh* pOIModelMultipleMesh = new COpenInventorModelMultipleMesh(pModelMultipleMesh, true);
        pOIModelMultipleMesh->GetBaseSwitch()->whichChild.setValue(SO_SWITCH_ALL);
        const std::map<int, CModelMesh*>& Meshes = pModelMultipleMesh->GetMeshes();
        std::map<int, CModelMesh*>::const_iterator EndMeshes = Meshes.end();
        for (std::map<int, CModelMesh*>::const_iterator pKeymesh = Meshes.begin(); pKeymesh != EndMeshes; ++pKeymesh)
        {
            COpenInventorModelMesh* pOpenInventorModelMesh = (COpenInventorModelMesh*)pKeymesh->second->GetElementData();
            for (int VisibilityType = CModelElement::eStaticOccluded; VisibilityType <= CModelElement::eDynamicVisible; ++VisibilityType)
            {
                pOpenInventorModelMesh->SetVerticesSetVisible(CModelElement::Visibility(VisibilityType), false);
                pOpenInventorModelMesh->SetEdgesSetVisible(CModelElement::Visibility(VisibilityType), false);
                pOpenInventorModelMesh->SetFaceSetVisible(CModelElement::Visibility(VisibilityType), false);
                pOpenInventorModelMesh->SetMultipleFacesSetVisible(CModelElement::Visibility(VisibilityType), false);
            }
            pOpenInventorModelMesh->SetFaceSetVisible(RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement::eStaticVisible, true);
            pOpenInventorModelMesh->SetEdgesSetVisible(RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement::eStaticVisible, true);
        }

        m_pVisualizationOIModelMultipleMeshMap.insert(std::pair<int, COpenInventorModelMultipleMesh*>(i, pOIModelMultipleMesh));
    }

    return true;
}

const std::map<int, CModelMultipleMesh*>& CRenderingModel::getVisualizationModelMultipleMeshMap() const
{
    return m_pVisualizationModelMultipleMeshMap;
}

const std::map<int, COpenInventorModelMultipleMesh*>& CRenderingModel::getVisualizationOIModelMultipleMeshMap() const
{
    return m_pVisualizationOIModelMultipleMeshMap;
}
