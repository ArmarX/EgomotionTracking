/*
 * ParametricLineRenderer.cpp
 *
 *  Created on: 12.02.2013
 *      Author: abyte
 */

#include "ParametricLineRenderer.h"

#include <queue>
#include <algorithm>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/OpenGLCalculations.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Profiling.h>

CParametricLineRenderer::CParametricLineRenderer() :
    m_pRenderingModel(NULL),
    m_viewingMatrixGL(SbMatrix::identity()),
    m_imageWidth(1024),
    m_imageHeight(768), m_visibleScreenRegion(0, 1024, 0, 768),
    m_nearClippingPlane(10)
{
    m_rendererConfiguration.projectionMatrixGL = SbMatrix::identity();
    m_rendererConfiguration.minScreenLineLength = 5;
    m_rendererConfiguration.minSegmentsPerJunction = 2;
    m_rendererConfiguration.junctionSegmentLength = 20;
    m_rendererConfiguration.maxJunctionLinesDotProduct = 0.95;
    m_rendererConfiguration.maxJunctionPointToSegmentDeviation = 3;
}

CParametricLineRenderer::~CParametricLineRenderer()
{

}

CFaceTriangle CParametricLineRenderer::ProjectEyeFaceTriangleToScreen(const SbMatrix& projectionMatrixGL, const SbVec4f& pointEye1, const SbVec4f& pointEye2, const SbVec4f& pointEye3,
        unsigned int imageWidth, unsigned int imageHeight, CFaceTriangle* pWorldFaceTriangleReference)
{
    return CFaceTriangle(OpenGLCalculations::ProjectEyePointToScreen(projectionMatrixGL, pointEye1, imageWidth, imageHeight),
                         OpenGLCalculations::ProjectEyePointToScreen(projectionMatrixGL, pointEye2, imageWidth, imageHeight),
                         OpenGLCalculations::ProjectEyePointToScreen(projectionMatrixGL, pointEye3, imageWidth, imageHeight), pWorldFaceTriangleReference);
}

bool CParametricLineRenderer::ProjectLineToScreen(const SbMatrix& projectionMatrixGL, const SbMatrix& viewingMatrixGL, const CLine& worldLine, unsigned int imageWidth,
        unsigned int imageHeight, float nearClippingPlane, CScreenLine& resultScreenLine)
{
    SbVec4f pointHom1(worldLine.m_p1[0], worldLine.m_p1[1], worldLine.m_p1[2], 1.0);
    SbVec4f pointHom2(worldLine.m_p2[0], worldLine.m_p2[1], worldLine.m_p2[2], 1.0);

    SbVec4f pointEye1, pointEye2;
    viewingMatrixGL.multVecMatrix(pointHom1, pointEye1);
    viewingMatrixGL.multVecMatrix(pointHom2, pointEye2);

    bool pointInClippingArea1 = OpenGLCalculations::EyePointIsInNearClippingArea(pointEye1, nearClippingPlane);
    bool pointInClippingArea2 = OpenGLCalculations::EyePointIsInNearClippingArea(pointEye2, nearClippingPlane);

    if (!pointInClippingArea1 || !pointInClippingArea2)
    {
        if (pointInClippingArea1)
        {
            if (!OpenGLCalculations::FitEyePointToClippingPlane(pointEye1, pointEye2, nearClippingPlane))
            {
                return false;
            }
        }
        else if (pointInClippingArea2)
        {
            if (!OpenGLCalculations::FitEyePointToClippingPlane(pointEye2, pointEye1, nearClippingPlane))
            {
                return false;
            }
        }

        resultScreenLine.m_p1 = OpenGLCalculations::ProjectEyePointToScreen(projectionMatrixGL, pointEye1, imageWidth, imageHeight);
        resultScreenLine.m_p2 = OpenGLCalculations::ProjectEyePointToScreen(projectionMatrixGL, pointEye2, imageWidth, imageHeight);

        return true;
    }

    return false;
}

bool CParametricLineRenderer::ProjectFaceTriangleToScreen(const SbMatrix& projectionMatrixGL, const SbMatrix& viewingMatrixGL, CFaceTriangle& worldFaceTriangle, unsigned int imageWidth,
        unsigned int imageHeight, float nearClippingPlane, std::vector<CFaceTriangle>& resultScreenTriangleList)
{
    SbVec4f pointHom[3];
    pointHom[0] = SbVec4f(worldFaceTriangle.m_p1[0], worldFaceTriangle.m_p1[1], worldFaceTriangle.m_p1[2], 1.0);
    pointHom[1] = SbVec4f(worldFaceTriangle.m_p2[0], worldFaceTriangle.m_p2[1], worldFaceTriangle.m_p2[2], 1.0);
    pointHom[2] = SbVec4f(worldFaceTriangle.m_p3[0], worldFaceTriangle.m_p3[1], worldFaceTriangle.m_p3[2], 1.0);

    SbVec4f pointEye[3];
    viewingMatrixGL.multVecMatrix(pointHom[0], pointEye[0]);
    viewingMatrixGL.multVecMatrix(pointHom[1], pointEye[1]);
    viewingMatrixGL.multVecMatrix(pointHom[2], pointEye[2]);

    SbVec4f clippingPoints[3], visiblePoints[3];
    unsigned int numberOfClippingPoints = 0, numberOfVisiblePoints = 0;
    for (int curEyePointCnt = 0; curEyePointCnt < 3; ++curEyePointCnt)
    {
        if (OpenGLCalculations::EyePointIsInNearClippingArea(pointEye[curEyePointCnt], nearClippingPlane))
        {
            clippingPoints[numberOfClippingPoints] = pointEye[curEyePointCnt];
            ++numberOfClippingPoints;
        }
        else
        {
            visiblePoints[numberOfVisiblePoints] = pointEye[curEyePointCnt];
            ++numberOfVisiblePoints;
        }
    }

    if (numberOfClippingPoints == 0)
    {
        resultScreenTriangleList.push_back(ProjectEyeFaceTriangleToScreen(projectionMatrixGL, visiblePoints[0], visiblePoints[1], visiblePoints[2], imageWidth, imageHeight,
                                           &worldFaceTriangle));
        return true;
    }
    else if (numberOfClippingPoints == 1)
    {
        SbVec4f clippingPoint0 = clippingPoints[0];
        SbVec4f clippingPoint1 = clippingPoints[0];

        if (OpenGLCalculations::FitEyePointToClippingPlane(clippingPoint0, visiblePoints[0], nearClippingPlane) &&
            OpenGLCalculations::FitEyePointToClippingPlane(clippingPoint1, visiblePoints[1], nearClippingPlane))
        {
            resultScreenTriangleList.push_back(ProjectEyeFaceTriangleToScreen(projectionMatrixGL, visiblePoints[0], visiblePoints[1], clippingPoint0, imageWidth, imageHeight,
                                               &worldFaceTriangle));
            resultScreenTriangleList.push_back(ProjectEyeFaceTriangleToScreen(projectionMatrixGL, visiblePoints[1], clippingPoint0, clippingPoint1, imageWidth, imageHeight,
                                               &worldFaceTriangle));
            return true;
        }

    }
    else if (numberOfClippingPoints == 2)
    {
        if (OpenGLCalculations::FitEyePointToClippingPlane(clippingPoints[0], visiblePoints[0], nearClippingPlane) &&
            OpenGLCalculations::FitEyePointToClippingPlane(clippingPoints[1], visiblePoints[0], nearClippingPlane))
        {
            resultScreenTriangleList.push_back(ProjectEyeFaceTriangleToScreen(projectionMatrixGL, visiblePoints[0], clippingPoints[0], clippingPoints[1], imageWidth, imageHeight,
                                               &worldFaceTriangle));
            return true;
        }
    }

    return false;
}

void CParametricLineRenderer::SetCameraPosition(const SbMatrix& viewingMatrixGL)
{
    m_viewingMatrixGL = viewingMatrixGL;
}

bool CParametricLineRenderer::ProjectModelLinesToScreen(bool projectOnlyVisibleModelLines)
{
    m_projectedScreenLineList.clear();

    if (m_pRenderingModel == NULL)
    {
        return false;
    }

    std::list<CLine>& modelLineList = m_pRenderingModel->GetModelLineList();

    std::vector<CScreenLine> tempProjectedScreenLineList;
    if (modelLineList.size() > 0)
    {
        std::list<CLine>::iterator lineIterator = modelLineList.begin();
        for (; lineIterator != modelLineList.end(); ++lineIterator)
        {
            if (!projectOnlyVisibleModelLines || lineIterator->m_isVisible)
            {
                CScreenLine screenLine;

                if (ProjectLineToScreen(m_rendererConfiguration.projectionMatrixGL, m_viewingMatrixGL, *lineIterator, m_imageWidth, m_imageHeight, m_nearClippingPlane, screenLine))
                {
                    screenLine.m_pWorldLineReference = &(*lineIterator);
                    screenLine.m_pModelEdgeReference = screenLine.m_pWorldLineReference->m_pModelEdgeReference;
                    ConditionalAddProjectedLineToList(screenLine, tempProjectedScreenLineList);
                }
            }
        }
    }

    Fit2dLinesToVisibleScreenRegion(tempProjectedScreenLineList, m_visibleScreenRegion, m_projectedScreenLineList);

    /* Project Lines corresponding to the BoundingBoxes of ModelTransformations. */

    m_projectedTransformationBoundingBoxLineList.clear();

    std::list<CLine>& transformationBoundingBoxLineList = m_pRenderingModel->getTransformationBoundingBoxLineList();

    std::vector<CScreenLine> tempProjectedTransformationBoundingBoxLineList;
    if (transformationBoundingBoxLineList.size() > 0)
    {
        for (std::list<CLine>::iterator lineIterator = transformationBoundingBoxLineList.begin(); lineIterator != transformationBoundingBoxLineList.end(); ++lineIterator)
        {
            CScreenLine screenLine;
            if (ProjectLineToScreen(m_rendererConfiguration.projectionMatrixGL, m_viewingMatrixGL, *lineIterator, m_imageWidth, m_imageHeight, m_nearClippingPlane, screenLine))
            {
                screenLine.m_pWorldLineReference = &(*lineIterator);
                ConditionalAddProjectedLineToList(screenLine, tempProjectedTransformationBoundingBoxLineList);
            }
        }
    }

    Fit2dLinesToVisibleScreenRegion(tempProjectedTransformationBoundingBoxLineList, m_visibleScreenRegion, m_projectedTransformationBoundingBoxLineList);

    return m_projectedScreenLineList.size() > 0;
}

inline bool CParametricLineRenderer::ProjectModelJunctionPointToScreen(const SbMatrix& projectionMatrixGL, const SbMatrix& viewingMatrixGL,
        const CJunctionPoint& worldJunctionPoint, unsigned int imageWidth, unsigned int imageHeight, float nearClippingPlane,
        CJunctionPoint& resultScreenJunctionPoint)
{

    SbVec4f junctionPointHom(worldJunctionPoint.m_junctionPoint[0], worldJunctionPoint.m_junctionPoint[1], worldJunctionPoint.m_junctionPoint[2], 1.0);
    SbVec4f junctionPointEye;
    viewingMatrixGL.multVecMatrix(junctionPointHom, junctionPointEye);

    if (OpenGLCalculations::EyePointIsInNearClippingArea(junctionPointEye, nearClippingPlane))
    {
        return false;
    }

    SbVec3f screenJunctionPoint = OpenGLCalculations::ProjectEyePointToScreen(projectionMatrixGL, junctionPointEye, imageWidth, imageHeight);
    if (!CheckPointInVisibleScreenRegion(screenJunctionPoint, m_visibleScreenRegion))
    {
        return false;
    }
    resultScreenJunctionPoint.m_junctionPoint = screenJunctionPoint;

    return true;
}


bool CParametricLineRenderer::Check2dPointIsHiddenByTriangle(const SbVec3f& point, const CFaceTriangle& triangle)
{
    /* see http://www.blackpawn.com/texts/pointinpoly/default.html for details */

    const SbVec2f A = SbVec2f(triangle.m_p1[0], triangle.m_p1[1]);
    const SbVec2f B = SbVec2f(triangle.m_p2[0], triangle.m_p2[1]);
    const SbVec2f C = SbVec2f(triangle.m_p3[0], triangle.m_p3[1]);
    const SbVec2f P = SbVec2f(point[0], point[1]);

    const SbVec2f V0 = C - A;
    const SbVec2f V1 = B - A;
    const SbVec2f V2 = P - A;

    float dot00 = V0.dot(V0);
    float dot01 = V0.dot(V1);
    float dot02 = V0.dot(V2);
    float dot11 = V1.dot(V1);
    float dot12 = V1.dot(V2);

    float invDenom = 1.0 / (dot00 * dot11 - dot01 * dot01);
    float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
    float v = (dot00 * dot12 - dot01 * dot02) * invDenom;

    // Check if point is in triangle
    if ((u >= 0) && (v >= 0) && (u + v <= 1))
    {

        const float zA = triangle.m_p1[2];
        const float zB = triangle.m_p2[2];
        const float zC = triangle.m_p3[2];

        const float z = zA + u * (zC - zA) + v * (zB - zA);

        if (CheckPointHiddenZDistDifference(z - point[2]))
        {
            SbVec3f l1 = triangle.m_p3 - triangle.m_p1;
            SbVec3f l2 = triangle.m_p2 - triangle.m_p1;

            SbVec3f n = l1.cross(l2);
            n.normalize();

            const float d = n.dot(triangle.m_p1);
            const float dist = fabs(n.dot(point) - d);

            return dist > 4;

            return true;
        }
    }

    return false;
}

unsigned int CParametricLineRenderer::Check2dPointsAreHidden(std::list<CPoint2d>& pointList, const std::vector<CFaceTriangle>& triangleList)
{
    unsigned int numberOfPointsHidden = 0;

    if (pointList.size() > 0)
    {
        for (std::list<CPoint2d>::iterator pointIterator = pointList.begin(); pointIterator != pointList.end(); ++pointIterator)
        {
            if (Check2dPointIsHidden(pointIterator->m_pointVector, triangleList))
            {
                pointIterator->m_isVisible = false;
                ++numberOfPointsHidden;
            }
        }
    }

    return numberOfPointsHidden;
}

unsigned int CParametricLineRenderer::Check2dLineIsHidden(const CLine& line, const std::vector<CFaceTriangle>& triangleList, unsigned int numberOfLineChecksBetween,
        std::list<CPoint2d>& checkedPointsResultList)
{
    checkedPointsResultList.clear();
    checkedPointsResultList.push_back(CPoint2d(line.m_p1, true));
    checkedPointsResultList.push_back(CPoint2d(line.m_p2, true));


    SbVec3f directionStep = (line.m_p2 - line.m_p1) * (1.0 / (numberOfLineChecksBetween + 1.0));
    SbVec3f curPoint = line.m_p1 + directionStep;
    for (unsigned int i = 0; i < numberOfLineChecksBetween; ++i)
    {
        checkedPointsResultList.push_back(CPoint2d(curPoint, true));
        curPoint = curPoint + directionStep;
    }

    unsigned int numberOfHiddenPoints = Check2dPointsAreHidden(checkedPointsResultList, triangleList);

    return numberOfHiddenPoints;
}

bool CParametricLineRenderer::Check2dTriangleIsHidden(const CFaceTriangle& triangle, const std::vector<CFaceTriangle>& triangleList, unsigned int numberOfLineChecksBetween)
{
    std::list<CLine> checkLinesList;

    /* margin lines */
    checkLinesList.push_back(CLine(triangle.m_p1, triangle.m_p2, true));
    checkLinesList.push_back(CLine(triangle.m_p2, triangle.m_p3, true));
    checkLinesList.push_back(CLine(triangle.m_p3, triangle.m_p1, true));
    /* lines through middle point */
    checkLinesList.push_back(CLine(triangle.m_p1, triangle.m_p2 + 0.5 * (triangle.m_p3 - triangle.m_p2), true));
    checkLinesList.push_back(CLine(triangle.m_p2, triangle.m_p3 + 0.5 * (triangle.m_p1 - triangle.m_p3), true));
    checkLinesList.push_back(CLine(triangle.m_p3, triangle.m_p1 + 0.5 * (triangle.m_p2 - triangle.m_p1), true));

    unsigned int numberOfLinesPartiallyVisible = 0;
    for (std::list<CLine>::iterator lineIterator = checkLinesList.begin(); lineIterator != checkLinesList.end(); ++lineIterator)
    {
        std::list<CPoint2d> checkedPointsResultList;
        if (Check2dLineIsHidden(*lineIterator, triangleList, numberOfLineChecksBetween, checkedPointsResultList) < numberOfLineChecksBetween)
        {
            /* at least three points of the line are visible */
            ++numberOfLinesPartiallyVisible;
        }
    }

    //    std::cout << "numberOfLinesPartiallyVisible=" << numberOfLinesPartiallyVisible << std::endl;
    return numberOfLinesPartiallyVisible < 3;
}

bool CParametricLineRenderer::Check2dPointIsHidden(const SbVec3f& point, const std::vector<CFaceTriangle>& triangleList)
{
    std::vector<CFaceTriangle>::const_iterator faceTriangleIterator;

    if (triangleList.size() > 0)
    {
        for (faceTriangleIterator = triangleList.begin(); faceTriangleIterator != triangleList.end(); ++faceTriangleIterator)
        {
            if (faceTriangleIterator->m_isVisible)
            {
                if (Check2dPointIsHiddenByTriangle(point, *faceTriangleIterator))
                {
                    return true;
                }
            }
        }
    }
    return false;
}

void CParametricLineRenderer::SetVisibleScreenRegion(float xMin, float xMax, float yMin, float yMax)
{
    m_visibleScreenRegion = CVisibleScreenRegion(xMin, xMax, yMin, yMax);
}

bool CParametricLineRenderer::Intersect2dLineWithVerticalBorder(const SbVec3f& lineOffsetPoint, const SbVec3f& lineDirection, float borderX,
        SbVec3f& intersectionPoint)
{
    if (fabs(lineDirection[0]) < FLT_EPSILON)
    {
        return false;
    }

    float lambda = (borderX - lineOffsetPoint[0]) / lineDirection[0];

    intersectionPoint[0] = borderX;
    intersectionPoint[1] = lineOffsetPoint[1] + lineDirection[1] * lambda;
    intersectionPoint[2] = lineOffsetPoint[2] + lineDirection[2] * lambda;

    return lambda > 0.0;
}

bool CParametricLineRenderer::Intersect2dLineWithHorizontalBorder(const SbVec3f& lineOffsetPoint, const SbVec3f& lineDirection, float borderY,
        SbVec3f& intersectionPoint)
{
    if (fabs(lineDirection[1]) < FLT_EPSILON)
    {
        return false;
    }

    float lambda = (borderY - lineOffsetPoint[1]) / lineDirection[1];

    intersectionPoint[1] = borderY;
    intersectionPoint[0] = lineOffsetPoint[0] + lineDirection[0] * lambda;
    intersectionPoint[2] = lineOffsetPoint[2] + lineDirection[2] * lambda;

    return lambda > 0.0;
}

CParametricLineRenderer::CIntersect2dLinesResult CParametricLineRenderer::Intersect2dLines(const CLine& line1, const CLine& line2, SbVec3f& intersectionPoint1, SbVec3f& intersectionPoint2)
{
    SbVec3f r1 = (line1.m_p2 - line1.m_p1);
    SbVec3f r2 = (line2.m_p2 - line2.m_p1);
    SbVec3f d = line2.m_p1 - line1.m_p1;

    float detR = -r1[0] * r2[1] + r1[1] * r2[0];

    if (fabs(detR) <= FLT_EPSILON)
    {
        return eNoIntersectionPoint;
    }

    float lambda1 = (1 / detR) * (-d[0] * r2[1] + d[1] * r2[0]);
    float lambda2 = (1 / detR) * (-d[0] * r1[1] + d[1] * r1[0]);

    intersectionPoint1 = line1.m_p1 + lambda1 * r1;
    intersectionPoint2 = line2.m_p1 + lambda2 * r2;

    unsigned int withinCount = 0;

    if (lambda1 >= 0.0 && lambda1 <= 1.0)
    {
        ++withinCount;
    }
    if (lambda2 >= 0.0 && lambda2 <= 1.0)
    {
        ++withinCount;
    }

    if (withinCount == 0)
    {
        return eVirtualIntersectionPointSecondOrder;
    }
    else if (withinCount == 1)
    {
        return eVirtualIntersectionPointFirstOrder;
    }

    return eRealIntersectionPoint;
}

bool CParametricLineRenderer::CheckPointInVisibleScreenRegion(const SbVec3f& point, const CVisibleScreenRegion& visibleScreenRegion)
{
    if (point[0] >= visibleScreenRegion.m_xMin && point[0] <= visibleScreenRegion.m_xMax &&
        point[1] >= visibleScreenRegion.m_yMin && point[1] <= visibleScreenRegion.m_yMax)
    {
        return true;
    }

    return false;
}

CParametricLineRenderer::CScreenRegionType CParametricLineRenderer::GetPointScreenRegion(const SbVec3f& point, const CVisibleScreenRegion& visibleScreenRegion) const
{
    if (point[0] < visibleScreenRegion.m_xMin)
    {
        // LEFT
        if (point[1] < visibleScreenRegion.m_yMin)
        {
            return eRegionOutTopLeft;
        }
        if (point[1] > visibleScreenRegion.m_yMax)
        {
            return eRegionOutBottomLeft;
        }
        return eRegionOutLeft;
    }
    else if (point[0] > visibleScreenRegion.m_xMax)
    {
        // RIGHT
        if (point[1] < visibleScreenRegion.m_yMin)
        {
            return eRegionOutTopRight;
        }
        if (point[1] > visibleScreenRegion.m_yMax)
        {
            return eRegionOutBottomRight;
        }
        return eRegionOutRight;

    }
    else
    {
        if (point[1] < visibleScreenRegion.m_yMin)
        {
            return eRegionOutTop;
        }
        if (point[1] > visibleScreenRegion.m_yMax)
        {
            return eRegionOutBottom;
        }
        return eRegionInScreen;
    }
}


void CParametricLineRenderer::CalculateLineIntersectionPoints(std::vector<CScreenLine>& projectedScreenLineList, std::vector<CScreenLine>& secondaryLineList,
        std::vector<std::list<SbVec3f> >& intersectionPoints)
{
    std::vector<CScreenLine>::iterator projectedScreenLineIterator;

    intersectionPoints.clear();
    intersectionPoints.resize(projectedScreenLineList.size());

    if (projectedScreenLineList.size() == 0)
    {
        return;
    }

    std::vector<std::list<SbVec3f> >::iterator intersectionSegmentIterator = intersectionPoints.begin();

    /* Intersect with Lines in the SecondaryLineList (TransformationBoundingBoxLines). */

    for (projectedScreenLineIterator = projectedScreenLineList.begin(); projectedScreenLineIterator != projectedScreenLineList.end(); ++projectedScreenLineIterator, ++intersectionSegmentIterator)
    {
        intersectionSegmentIterator->push_back(projectedScreenLineIterator->m_p1);
        intersectionSegmentIterator->push_back(projectedScreenLineIterator->m_p2);

        std::vector<CScreenLine>::const_iterator secondaryLineIterator;
        for (secondaryLineIterator = secondaryLineList.begin();
             secondaryLineIterator != secondaryLineList.end(); ++secondaryLineIterator)
        {
            SbVec3f intersectionPoint1, intersectionPoint2;

            if (Intersect2dLines(*projectedScreenLineIterator, *secondaryLineIterator, intersectionPoint1, intersectionPoint2) == eRealIntersectionPoint)
            {
                intersectionSegmentIterator->push_back(intersectionPoint1);
            }
        }
    }

    int count = 0;

    /* Intersect Lines amongst themselves. */

    intersectionSegmentIterator = intersectionPoints.begin();
    for (projectedScreenLineIterator = projectedScreenLineList.begin(); projectedScreenLineIterator != projectedScreenLineList.end(); ++projectedScreenLineIterator, ++intersectionSegmentIterator)
    {
        std::vector<CScreenLine>::iterator projectedScreenLineIterator2 = projectedScreenLineIterator + 1;
        std::vector<std::list<SbVec3f> >::iterator intersectionSegmentIterator2 = intersectionSegmentIterator + 1;

        for (; projectedScreenLineIterator2 != projectedScreenLineList.end(); ++projectedScreenLineIterator2, ++intersectionSegmentIterator2)
        {
            SbVec3f intersectionPoint1, intersectionPoint2;

            if (projectedScreenLineIterator != projectedScreenLineIterator2 &&
                Intersect2dLines(*projectedScreenLineIterator, *projectedScreenLineIterator2, intersectionPoint1, intersectionPoint2) == eRealIntersectionPoint)
            {
                intersectionSegmentIterator->push_back(intersectionPoint1);
                intersectionSegmentIterator2->push_back(intersectionPoint2);
                ++count;
            }
        }
    }

    //    std::cout << "Intersecting " << projectedScreenLineList.size() << " lines, " << count << " single intersections found" << std::endl;
}

void CParametricLineRenderer::SweepLineVisibleSegmentsCalculation(std::list<std::list<CScreenLine> >& segmentedLinesList,
        const std::vector<CFaceTriangle>& projectedTriangleList)
{
    std::vector<CSweepLinePoint> sweepLinePoints;
    sweepLinePoints.reserve(segmentedLinesList.size() * 10 + projectedTriangleList.size() * 2);

    for (std::list<std::list<CScreenLine> >::iterator lineSegmentsIterator = segmentedLinesList.begin(); lineSegmentsIterator != segmentedLinesList.end(); ++lineSegmentsIterator)
    {
        for (std::list<CScreenLine>::iterator segmentIterator = lineSegmentsIterator->begin(); segmentIterator != lineSegmentsIterator->end();  ++segmentIterator)
        {
            sweepLinePoints.push_back(CSweepLinePoint(&(*segmentIterator), segmentIterator->CalculateCenterPoint(), CSweepLinePoint::eSegmentCenterPoint));
        }
    }

    for (std::vector<CFaceTriangle>::const_iterator projectedFaceTriangleIterator = projectedTriangleList.begin(); projectedFaceTriangleIterator != projectedTriangleList.end(); ++projectedFaceTriangleIterator)
    {
        sweepLinePoints.push_back(CSweepLinePoint(&(*projectedFaceTriangleIterator), projectedFaceTriangleIterator->m_boundMinX, CSweepLinePoint::eFaceTriangleBoundMin));
        sweepLinePoints.push_back(CSweepLinePoint(&(*projectedFaceTriangleIterator), projectedFaceTriangleIterator->m_boundMaxX, CSweepLinePoint::eFaceTriangleBoundMax));
    }

    for (std::vector<CFaceTriangle>::const_iterator projectedFaceTriangleIterator = m_projectedTransformationBoundingBoxFaceTriangleList.begin();
         projectedFaceTriangleIterator != m_projectedTransformationBoundingBoxFaceTriangleList.end(); ++projectedFaceTriangleIterator)
    {
        sweepLinePoints.push_back(CSweepLinePoint(&(*projectedFaceTriangleIterator), projectedFaceTriangleIterator->m_boundMinX, CSweepLinePoint::eFaceTriangleBoundMin));
        sweepLinePoints.push_back(CSweepLinePoint(&(*projectedFaceTriangleIterator), projectedFaceTriangleIterator->m_boundMaxX, CSweepLinePoint::eFaceTriangleBoundMax));
    }

    // Sort 'sweepLinePoints' in order of ascending X coordinate.
    std::sort(sweepLinePoints.begin(), sweepLinePoints.end());

    std::set<const CFaceTriangle*> currentlyActiveTrianglesMap;
    // Iterate through sweep line points.
    for (std::vector<CSweepLinePoint>::iterator sweepLineIterator = sweepLinePoints.begin(); sweepLineIterator != sweepLinePoints.end(); ++sweepLineIterator)
    {
        CSweepLinePoint curSweepLinePoint = *sweepLineIterator;

        // Current sweep line point is the lower bound of a face triangle. Add the corresponding face triangle to the set of active face triangles.
        if (curSweepLinePoint.m_sweepLinePointType == CSweepLinePoint::eFaceTriangleBoundMin)
        {
            currentlyActiveTrianglesMap.insert(curSweepLinePoint.m_pFaceTriangle);
        }
        // Current sweep line point is the upper bound of a face triangle. Remove the corresponding face triangle from the set of active face triangles.
        else if (curSweepLinePoint.m_sweepLinePointType == CSweepLinePoint::eFaceTriangleBoundMax)
        {
            currentlyActiveTrianglesMap.erase(curSweepLinePoint.m_pFaceTriangle);
        }
        // Current sweep line point is the center of a line segment.
        else
        {
            bool curSweepPointVisible = true;

            // Iterate through active face triangles while current sweep line point is still marked as visible.
            for (std::set<const CFaceTriangle*>::iterator activeFaceTriangleIterator = currentlyActiveTrianglesMap.begin(); activeFaceTriangleIterator != currentlyActiveTrianglesMap.end() && curSweepPointVisible; ++activeFaceTriangleIterator)
            {
                const CFaceTriangle* curActiveTriangle = *activeFaceTriangleIterator;

                // Sweep point is inside current active face triangle.
                if (curSweepLinePoint.m_point[1] >= curActiveTriangle->m_boundMinY && curSweepLinePoint.m_point[1] < curActiveTriangle->m_boundMaxY)
                {
                    // If sweep point is hidden, mark it as not visible.
                    if (Check2dPointIsHiddenByTriangle(curSweepLinePoint.m_point, *curActiveTriangle))
                    {
                        curSweepPointVisible = false;
                        break;
                    }
                }
            }
            // Give the corresponding line segment the same visibility as its center point.
            curSweepLinePoint.m_pLine->m_isVisible = curSweepPointVisible;
        }
    }
}

void CParametricLineRenderer::CalculateVisibleLineSegments(std::vector<CScreenLine>& projectedScreenLineList, const std::vector<CFaceTriangle>& projectedTriangleList,
        std::vector<std::list<SbVec3f> >& intersectionPoints, std::vector<CScreenLine>& resultVisibleLines)
{
    std::list<std::list<CScreenLine> > segmentedLinesList;
    if (projectedScreenLineList.size() > 0)
    {
        for (std::vector<std::list<SbVec3f> >::iterator intersectionSegmentIterator = intersectionPoints.begin(); intersectionSegmentIterator != intersectionPoints.end();
             ++intersectionSegmentIterator)
        {
            intersectionSegmentIterator->sort(compare_SbVec3fPosition);
            intersectionSegmentIterator->unique();

            std::list<SbVec3f>::const_iterator intersectionPointIterator = intersectionSegmentIterator->begin();
            SbVec3f lastPoint = *intersectionPointIterator;
            ++intersectionPointIterator;

            int currentCorrespondentProjectedLineIndex = intersectionSegmentIterator - intersectionPoints.begin();

            std::list<CScreenLine> curLineSegments;
            for (; intersectionPointIterator != intersectionSegmentIterator->end(); ++intersectionPointIterator)
            {
                CScreenLine segment(lastPoint, *intersectionPointIterator, projectedScreenLineList.at(currentCorrespondentProjectedLineIndex).m_pWorldLineReference);
                segment.m_pModelEdgeReference = segment.m_pWorldLineReference->m_pModelEdgeReference;
                curLineSegments.push_back(segment);

                lastPoint = *intersectionPointIterator;
            }
            segmentedLinesList.push_back(curLineSegments);
        }
    }

    SweepLineVisibleSegmentsCalculation(segmentedLinesList, projectedTriangleList);

    for (std::list<std::list<CScreenLine> >::const_iterator lineSegmentsIterator = segmentedLinesList.begin(); lineSegmentsIterator != segmentedLinesList.end(); ++lineSegmentsIterator)
    {
        if (lineSegmentsIterator->size() > 0)
        {
            std::list<CScreenLine>::const_iterator segmentIterator = lineSegmentsIterator->begin();

            CScreenLine curVisibleSegment = *segmentIterator;
            segmentIterator++;

            for (; segmentIterator != lineSegmentsIterator->end();  ++segmentIterator)
            {
                CScreenLine curSegment = *segmentIterator;

                if (curSegment.m_isVisible)
                {
                    if (curVisibleSegment.m_isVisible)
                    {
                        curVisibleSegment.m_p2 = curSegment.m_p2;
                    }
                    else
                    {
                        curVisibleSegment = curSegment;
                    }
                }
                else
                {
                    if (curVisibleSegment.m_isVisible)
                    {
                        ConditionalAddLineToList(curVisibleSegment, resultVisibleLines);
                        curVisibleSegment.m_isVisible = false;
                    }
                }
            }
            if (curVisibleSegment.m_isVisible)
            {
                ConditionalAddLineToList(curVisibleSegment, resultVisibleLines);
                curVisibleSegment.m_isVisible = false;
            }
        }
    }
}

void CParametricLineRenderer::SetMinScreenLineLength(float minScreenLineLength)
{
    m_rendererConfiguration.minScreenLineLength = minScreenLineLength;
}

float CParametricLineRenderer::GetMinScreenLineLength() const
{
    return m_rendererConfiguration.minScreenLineLength;
}

bool CParametricLineRenderer::Render(std::vector<CScreenLine>& renderedScreenLines, std::vector<CJunctionPoint>& renderedScreenJunctionPoints, bool renderJunctionPoints)
{

    if (renderJunctionPoints)
    {
        return RenderJunctionPoints(renderedScreenLines, renderedScreenJunctionPoints);
    }
    else
    {
        return RenderVisibleLines(renderedScreenLines);
    }
    return true;
}

bool CParametricLineRenderer::Render(std::vector<CScreenLine>& renderedScreenLines)
{
    return RenderVisibleLines(renderedScreenLines);
}

bool CParametricLineRenderer::CheckModelPartsAreInView(float relativeInViewRatio)
{
    if (m_pRenderingModel == NULL)
    {
        return false;
    }

    if (ProjectModelLinesToScreen(true) && m_projectedScreenLineList.size() > relativeInViewRatio * m_pRenderingModel->GetModelLineList().size())
    {
        return true;
    }
    return false;
}


bool CParametricLineRenderer::RenderJunctionPoints()
{
    return RenderJunctionPoints(m_visibleScreenLineList, m_visibleScreenJunctionPointList);
}

bool CParametricLineRenderer::RenderJunctionPoints(std::vector<CScreenLine>& visibleScreenLines, std::vector<CJunctionPoint>& visibleScreenJunctionPoints)
{
    visibleScreenJunctionPoints.clear();

    if (!RenderVisibleLines(visibleScreenLines))
    {
        return false;
    }

    std::set<CJunctionPoint, CJunctionPoint::CompareJunctionJunctionPointsFunctor> collectedJunctionPoints;

    if (visibleScreenLines.size() > 0)
    {
        for (std::vector<CScreenLine>::const_iterator visibleScreenLineIterator = visibleScreenLines.begin(); visibleScreenLineIterator != visibleScreenLines.end();
             ++visibleScreenLineIterator)
        {
            collectedJunctionPoints.insert(CJunctionPoint(visibleScreenLineIterator->m_p1));
            collectedJunctionPoints.insert(CJunctionPoint(visibleScreenLineIterator->m_p2));
        }
    }

    m_projectedScreenJunctionPointList.clear();
    if (collectedJunctionPoints.size() > 0)
    {
        for (std::set<CJunctionPoint, CJunctionPoint::CompareJunctionJunctionPointsFunctor>::const_iterator jpIterator = collectedJunctionPoints.begin();
             jpIterator != collectedJunctionPoints.end(); ++ jpIterator)
        {

            bool redundant = false;
            if (m_projectedScreenJunctionPointList.size() > 0)
                for (std::vector<CJunctionPoint>::iterator finalJpIterator = m_projectedScreenJunctionPointList.begin();
                     finalJpIterator != m_projectedScreenJunctionPointList.end() && !redundant; ++finalJpIterator)
                {
                    if (Calc2dPointDistance(jpIterator->m_junctionPoint, finalJpIterator->m_junctionPoint) < m_rendererConfiguration.maxJunctionPointToSegmentDeviation / 2)
                    {
                        redundant = true;
                        finalJpIterator->m_junctionPoint = (1.0 / (finalJpIterator->m_degree + 1)) * (finalJpIterator->m_degree * finalJpIterator->m_junctionPoint + jpIterator->m_junctionPoint);
                        ++finalJpIterator->m_degree;
                    }
                }
            if (!redundant)
            {
                m_projectedScreenJunctionPointList.push_back(*jpIterator);
                m_projectedScreenJunctionPointList.back().m_degree = 1;
            }
            /*
            std::set<CJunctionPoint, CJunctionPoint::CompareJunctionJunctionPointsFunctor>::iterator jpIterator2 = jpIterator;
            jpIterator2++;
            bool redundant = false;
            for(; jpIterator2 != collectedJunctionPoints.end();++jpIterator2)
            {
                if(Calc2dPointDistance(jpIterator->m_junctionPoint, jpIterator2->m_junctionPoint) < m_rendererConfiguration.maxJunctionPointToSegmentDeviation/2)
                {
                    redundant = true;
                }
            }

            if(!redundant)
            {
                m_projectedScreenJunctionPointList.push_back(*jpIterator);
            }

            */
        }
    }

    if (m_projectedScreenJunctionPointList.size() > 0)
    {
        SweepLineFitScreenLinesToJunctionPoint(m_projectedScreenJunctionPointList, visibleScreenLines, m_rendererConfiguration.junctionSegmentLength, m_rendererConfiguration.maxJunctionPointToSegmentDeviation);

        for (std::vector<CJunctionPoint>::const_iterator junctionPointIterator = m_projectedScreenJunctionPointList.begin(); junctionPointIterator != m_projectedScreenJunctionPointList.end(); ++junctionPointIterator)
        {
            if (junctionPointIterator->m_junctionLines.size() >= m_rendererConfiguration.minSegmentsPerJunction)
            {
                visibleScreenJunctionPoints.push_back(*junctionPointIterator);
            }
        }
    }
    return true;
}

void test(std::vector<CScreenLine>& visibleScreenLines)
{
    if (visibleScreenLines.size() > 0)
    {
        for (std::vector<CScreenLine>::const_iterator lineIt = visibleScreenLines.begin(); lineIt != visibleScreenLines.end(); ++lineIt)
        {
            assert((*lineIt).CalculateLength2d() < 2000);
        }
    }
}

bool CParametricLineRenderer::RenderVisibleLines(std::vector<CScreenLine>& visibleScreenLines)
{
    visibleScreenLines.clear();

    if (ProjectModelLinesToScreen(true) && ProjectModelFaceTrianglesToScreen())
    {
        test(m_projectedScreenLineList);
        CalculateLineIntersectionPoints(m_projectedScreenLineList, m_projectedTransformationBoundingBoxLineList, m_intersectionPoints);
        CalculateVisibleLineSegments(m_projectedScreenLineList, m_projectedScreenFaceTriangleList, m_intersectionPoints, visibleScreenLines);
        test(visibleScreenLines);
        return true;
    }
    return false;
}

inline void CParametricLineRenderer::SweepLineFitScreenLinesToJunctionPoint(std::vector<CJunctionPoint>& junctionPointsList,
        const std::vector<CScreenLine>& screenLineList, float junctionSegmentLength, float maxJunctionPointToSegmentDeviation)
{
    std::vector<CSweepLinePoint> sweepLinePoints;

    for (std::vector<CScreenLine>::const_iterator lineIterator = screenLineList.begin(); lineIterator != screenLineList.end();  ++lineIterator)
    {
        if (lineIterator->CalculateLength2d() >= junctionSegmentLength)
        {
            SbVec3f offset(maxJunctionPointToSegmentDeviation, 0, 0);

            SbVec3f left, right;
            if (lineIterator->m_p1[0] < lineIterator->m_p2[0])
            {
                left = lineIterator->m_p1;
                right = lineIterator->m_p2;
            }
            else
            {
                left = lineIterator->m_p2;
                right = lineIterator->m_p1;
            }


            sweepLinePoints.push_back(CSweepLinePoint(&(*lineIterator), left - offset, CSweepLinePoint::eLeftLineEnd));
            sweepLinePoints.push_back(CSweepLinePoint(&(*lineIterator), right + offset, CSweepLinePoint::eRightLineEnd));
        }
    }

    for (std::vector<CJunctionPoint>::iterator junctionPointIterator = junctionPointsList.begin(); junctionPointIterator != junctionPointsList.end();  ++junctionPointIterator)
    {
        sweepLinePoints.push_back(CSweepLinePoint(&(*junctionPointIterator), junctionPointIterator->m_junctionPoint));
    }

    std::sort(sweepLinePoints.begin(), sweepLinePoints.end());

    std::set<const CScreenLine*> currentlyActiveLinesSet;
    for (std::vector<CSweepLinePoint>::iterator sweepLineIterator = sweepLinePoints.begin(); sweepLineIterator != sweepLinePoints.end(); ++sweepLineIterator)
    {
        const CSweepLinePoint curSweepLinePoint = *sweepLineIterator;

        if (curSweepLinePoint.m_sweepLinePointType == CSweepLinePoint::eLeftLineEnd)
        {
            currentlyActiveLinesSet.insert(curSweepLinePoint.m_pConstLine);
        }
        else if (curSweepLinePoint.m_sweepLinePointType == CSweepLinePoint::eRightLineEnd)
        {
            currentlyActiveLinesSet.erase(curSweepLinePoint.m_pConstLine);
        }
        else
        {
            CJunctionPoint* pCurrentJunctionPoint = sweepLineIterator->m_pJunctionPoint;
            const SbVec3f currentJunctionPoint = pCurrentJunctionPoint->m_junctionPoint;

            for (std::set<const CScreenLine*>::const_iterator activeLineIterator = currentlyActiveLinesSet.begin(); activeLineIterator != currentlyActiveLinesSet.end(); ++activeLineIterator)
            {
                const CScreenLine* currentLine = *activeLineIterator;

                const SbVec3f nearestPointOnLine = currentLine->CalculateNearestPointOnLine2d(currentJunctionPoint);

                if (Calc2dPointDistance(currentJunctionPoint, nearestPointOnLine) <= maxJunctionPointToSegmentDeviation)
                {
                    CLine jl1(nearestPointOnLine, currentLine->m_p1);
                    CLine jl2(nearestPointOnLine, currentLine->m_p2);

                    float length1 = jl1.CalculateLength2d();
                    float length2 = jl2.CalculateLength2d();

                    if (length1 >= junctionSegmentLength)
                    {
                        const SbVec3f d = (junctionSegmentLength / length1) * (jl1.m_p2 - jl1.m_p1);
                        jl1.m_p2 = jl1.m_p1 + d;
                        ConditionalAddJunctionLineToList(jl1, pCurrentJunctionPoint->m_junctionLines);
                    }
                    if (length2 >= junctionSegmentLength)
                    {
                        const SbVec3f d = (junctionSegmentLength / length2) * (jl2.m_p2 - jl2.m_p1);
                        jl2.m_p2 = jl2.m_p1 + d;
                        ConditionalAddJunctionLineToList(jl2, pCurrentJunctionPoint->m_junctionLines);
                    }
                }
            }
        }
    }
}


void CParametricLineRenderer::SetMaxJunctionPointToSegmentDeviation(float maxJunctionPointToSegmentDeviation)
{
    m_rendererConfiguration.maxJunctionPointToSegmentDeviation = maxJunctionPointToSegmentDeviation;
}

void CParametricLineRenderer::SetMinSegmentsPerJunction(unsigned int minSegmentsPerJunction)
{
    m_rendererConfiguration.minSegmentsPerJunction = minSegmentsPerJunction;
}

void CParametricLineRenderer::SetJunctionSegmentLength(float junctionSegmentLength)
{
    m_rendererConfiguration.junctionSegmentLength = junctionSegmentLength;
}

float CParametricLineRenderer::GetMaxJunctionPointToSegmentDeviation() const
{
    return m_rendererConfiguration.maxJunctionPointToSegmentDeviation;
}

unsigned int CParametricLineRenderer::GetMinSegmentsPerJunction() const
{
    return m_rendererConfiguration.minSegmentsPerJunction;
}

float CParametricLineRenderer::GetJunctionSegmentLength() const
{
    return m_rendererConfiguration.junctionSegmentLength;
}

bool CParametricLineRenderer::RenderVisibleLines()
{
    return RenderVisibleLines(m_visibleScreenLineList);
}

//XXX
int CParametricLineRenderer::SetCurrentlyApparentVisibleModelElementsToVisible()
{
    ProjectModelLinesToScreen(false);
    ProjectModelFaceTrianglesToScreen(false);

    std::cout << m_projectedScreenLineList.size() << std::endl;
    const unsigned int numberOfLineChecksBetween = 7;

    int numberOfVisibleScreenLines = 0;
    for (std::vector<CScreenLine>::const_iterator lineIterator = m_projectedScreenLineList.begin(); lineIterator != m_projectedScreenLineList.end(); ++lineIterator)
    {
        std::list<CPoint2d> resultPointList;
        unsigned int numberOfHiddenPoints = Check2dLineIsHidden(*lineIterator, m_projectedScreenFaceTriangleList, numberOfLineChecksBetween, resultPointList);
        if (numberOfHiddenPoints < numberOfLineChecksBetween)
        {
            // at least 3 points are visible

            assert(lineIterator->m_pWorldLineReference != NULL);

            lineIterator->m_pWorldLineReference->m_isVisible = true;
            ++numberOfVisibleScreenLines;

        }
    }

    int numberOfVisibleTriangles = 0;
    for (std::vector<CFaceTriangle>::const_iterator triangleIterator = m_projectedScreenFaceTriangleList.begin(); triangleIterator != m_projectedScreenFaceTriangleList.end(); ++ triangleIterator)
    {
        if (!Check2dTriangleIsHidden(*triangleIterator, m_projectedScreenFaceTriangleList, numberOfLineChecksBetween))
        {
            assert(triangleIterator->m_pWorldFaceTriangleReference != NULL);

            triangleIterator->m_pWorldFaceTriangleReference->m_isVisible = true;

            ++numberOfVisibleTriangles;
        }
    }

    std::cout << "Currently Visible: lines=" << numberOfVisibleScreenLines << "  triangles=" << numberOfVisibleTriangles << std::endl;

    return numberOfVisibleScreenLines;
}

void CParametricLineRenderer::Fit2dLinesToVisibleScreenRegion(
    const std::vector<CScreenLine>& lineList,
    const CVisibleScreenRegion& visibleScreenRegion,
    std::vector<CScreenLine>& resultLineList)
{

    if (lineList.size() > 0)
    {
        std::vector<CScreenLine>::const_iterator curLineIterator = lineList.begin();
        while (curLineIterator != lineList.end())
        {
            SbVec3f linePoint1 = curLineIterator->m_p1;
            SbVec3f linePoint2 = curLineIterator->m_p2;

            if (Fit2dPointToVisibleScreenRegion(linePoint1, (curLineIterator->m_p2 - curLineIterator->m_p1), visibleScreenRegion)
                && Fit2dPointToVisibleScreenRegion(linePoint2, (curLineIterator->m_p1 - curLineIterator->m_p2), visibleScreenRegion))
            {

                resultLineList.push_back(CScreenLine(linePoint1, linePoint2, curLineIterator->m_pWorldLineReference));
            }
            ++curLineIterator;


        }
    }
}

bool CParametricLineRenderer::Fit2dPointToVisibleScreenRegion(
    SbVec3f& pointOffset, const SbVec3f& lineDirection,
    const CVisibleScreenRegion& visibleScreenRegion)
{
    CScreenRegionType screenRegionType = GetPointScreenRegion(pointOffset, visibleScreenRegion);

    if (screenRegionType == eRegionInScreen)
    {
        return true;
    }

    SbVec3f resultPoint;
    if (screenRegionType == eRegionOutBottomLeft || screenRegionType == eRegionOutLeft || screenRegionType == eRegionOutTopLeft)
    {
        //LEFT
        if (Intersect2dLineWithVerticalBorder(pointOffset, lineDirection, visibleScreenRegion.m_xMin, resultPoint))
        {
            if (CheckPointInVisibleScreenRegion(resultPoint, visibleScreenRegion))
            {
                pointOffset = resultPoint;
                return true;
            }
        }

    }
    else if (screenRegionType == eRegionOutBottomRight || screenRegionType == eRegionOutRight || screenRegionType == eRegionOutTopRight)
    {
        //Right
        if (Intersect2dLineWithVerticalBorder(pointOffset, lineDirection, visibleScreenRegion.m_xMax, resultPoint))
        {
            if (CheckPointInVisibleScreenRegion(resultPoint, visibleScreenRegion))
            {
                pointOffset = resultPoint;
                return true;
            }
        }
    }

    if (screenRegionType == eRegionOutBottomLeft || screenRegionType == eRegionOutBottom || screenRegionType == eRegionOutBottomRight)
    {
        //Bottom
        if (Intersect2dLineWithHorizontalBorder(pointOffset, lineDirection, visibleScreenRegion.m_yMax, resultPoint))
        {
            if (CheckPointInVisibleScreenRegion(resultPoint, visibleScreenRegion))
            {
                pointOffset = resultPoint;
                return true;
            }
        }
    }
    else if (screenRegionType == eRegionOutTopLeft || screenRegionType == eRegionOutTop || screenRegionType == eRegionOutTopRight)
    {
        //Top
        if (Intersect2dLineWithHorizontalBorder(pointOffset, lineDirection, visibleScreenRegion.m_yMin, resultPoint))
        {
            if (CheckPointInVisibleScreenRegion(resultPoint, visibleScreenRegion))
            {
                pointOffset = resultPoint;
                return true;
            }
        }
    }

    return false;
}

const CVisibleScreenRegion& CParametricLineRenderer::GetVisibleScreenRegion() const
{
    return m_visibleScreenRegion;
}

const std::vector<CScreenLine>& CParametricLineRenderer::GetProjectedScreenLineList() const
{
    return m_projectedScreenLineList;
}

const std::vector<CScreenLine>& CParametricLineRenderer::GetVisibleScreenLineList() const
{
    return m_visibleScreenLineList;
}


const std::vector<CJunctionPoint>& CParametricLineRenderer::GetProjectedScreenJunctionPointList() const
{
    return m_projectedScreenJunctionPointList;
}

const std::vector<CJunctionPoint>& CParametricLineRenderer::GetVisibleScreenJunctionPointList() const
{
    return m_visibleScreenJunctionPointList;
}

const std::vector<CScreenLine>& CParametricLineRenderer::GetTransformationBoundingBoxScreenLineList() const
{
    return m_projectedTransformationBoundingBoxLineList;
}

const std::vector<CFaceTriangle>& CParametricLineRenderer::GetTransformationBoundingBoxScreenFaceTriangleList() const
{
    return m_projectedTransformationBoundingBoxFaceTriangleList;
}

bool CParametricLineRenderer::ProjectModelFaceTrianglesToScreen(bool projectOnlyVisibleModelFaceTriangles)
{
    m_projectedScreenFaceTriangleList.clear();

    if (m_pRenderingModel == NULL)
    {
        return false;
    }

    std::list<CFaceTriangle>& modelFaceTriangleList = m_pRenderingModel->GetModelFaceTriangleList();

    if (modelFaceTriangleList.size() > 0)
    {
        std::list<CFaceTriangle>::iterator triangleIterator = modelFaceTriangleList.begin();
        for (; triangleIterator != modelFaceTriangleList.end(); ++triangleIterator)
        {
            if (!projectOnlyVisibleModelFaceTriangles || triangleIterator->m_isVisible)
            {
                ProjectFaceTriangleToScreen(m_rendererConfiguration.projectionMatrixGL, m_viewingMatrixGL, *triangleIterator, m_imageWidth, m_imageHeight, m_nearClippingPlane, m_projectedScreenFaceTriangleList);
            }
        }
    }

    /* Project FaceTriangles of BoundingBoxes belonging to ModelTransformations. */

    m_projectedTransformationBoundingBoxFaceTriangleList.clear();

    std::list<CFaceTriangle>& transformationBoundingBoxFaceTriangeList = m_pRenderingModel->getTransformationBoundingBoxFaceTriangleList();

    if (transformationBoundingBoxFaceTriangeList.size() > 0)
    {
        for (std::list<CFaceTriangle>::iterator triangleIterator = transformationBoundingBoxFaceTriangeList.begin(); triangleIterator != transformationBoundingBoxFaceTriangeList.end();
             ++triangleIterator)
        {
            ProjectFaceTriangleToScreen(m_rendererConfiguration.projectionMatrixGL, m_viewingMatrixGL, *triangleIterator, m_imageWidth, m_imageHeight, m_nearClippingPlane,
                                        m_projectedTransformationBoundingBoxFaceTriangleList);
        }
    }

    return m_projectedScreenFaceTriangleList.size() > 0;
}

const std::vector<CFaceTriangle>& CParametricLineRenderer::GetProjectedScreenTriangleList() const
{
    return m_projectedScreenFaceTriangleList;
}

void CParametricLineRenderer::SetImageSize(float width, float height)
{
    m_imageWidth = width;
    m_imageHeight = height;
}

void CParametricLineRenderer::GetImageSize(float& width, float& height)
{
    width = m_imageWidth;
    height = m_imageHeight;
}

const SbMatrix& CParametricLineRenderer::GetProjectionMatrixGl() const
{
    return m_rendererConfiguration.projectionMatrixGL;
}

void CParametricLineRenderer::SetProjectionMatrixGl(const SbMatrix& projectionMatrixGl)
{
    m_rendererConfiguration.projectionMatrixGL = projectionMatrixGl;
}

void CParametricLineRenderer::SetRenderingModel(CRenderingModel* pRenderingModel)
{
    m_pRenderingModel = pRenderingModel;
}

const CRenderingModel* CParametricLineRenderer::GetRenderingModel() const
{
    return m_pRenderingModel;
}

CParametricLineRenderer::CParametricLineRendererConfiguration CParametricLineRenderer::GetConfiguration() const
{
    CParametricLineRendererConfiguration conf;

    conf.minScreenLineLength = GetMinScreenLineLength();
    conf.maxJunctionPointToSegmentDeviation = GetMaxJunctionPointToSegmentDeviation();
    conf.minSegmentsPerJunction = GetMinSegmentsPerJunction();
    conf.junctionSegmentLength = GetJunctionSegmentLength();
    conf.maxJunctionLinesDotProduct = GetMaxJunctionLinesDotProduct();
    conf.projectionMatrixGL = GetProjectionMatrixGl();
    return conf;
}

void CParametricLineRenderer::SetConfiguration(const CParametricLineRendererConfiguration& conf)
{
    SetMinScreenLineLength(conf.minScreenLineLength);
    SetMaxJunctionPointToSegmentDeviation(conf.maxJunctionPointToSegmentDeviation);
    SetMinSegmentsPerJunction(conf.minSegmentsPerJunction);
    SetJunctionSegmentLength(conf.junctionSegmentLength);
    SetMaxJunctionLinesDotProduct(conf.maxJunctionLinesDotProduct);
    SetProjectionMatrixGl(conf.projectionMatrixGL);
}

bool CParametricLineRenderer::WriteConfigurationToFile(const QString& filename)
{
    QDomDocument configurationDomDocument("ParametricLineRendererConfiguration");
    configurationDomDocument.appendChild(SerializeParametricLineRendererConfiguration(*this));
    return XMLPrimitives::WriteXMLDocumentToFile(configurationDomDocument, filename);
}

bool CParametricLineRenderer::LoadConfigurationFromFile(const QString& filename)
{
    QDomDocument doc;
    if (!XMLPrimitives::ReadXMLDocumentFromFile(doc, filename))
    {
        return false;
    }
    QDomElement rootElement = doc.documentElement();
    return DeserializeParametricLineRendererConfiguration(rootElement, *this);
}

float CParametricLineRenderer::GetMaxJunctionLinesDotProduct() const
{
    return m_rendererConfiguration.maxJunctionLinesDotProduct;
}

void CParametricLineRenderer::SetMaxJunctionLinesDotProduct(float maxJunctionLinesDotProduct)
{
    m_rendererConfiguration.maxJunctionLinesDotProduct = maxJunctionLinesDotProduct;
}

const SbMatrix& CParametricLineRenderer::GetViewingMatrixGL() const
{
    return m_viewingMatrixGL;
}

void CParametricLineRenderer::SetViewingMatrixGL(const SbMatrix& viewingMatrixGL)
{
    m_viewingMatrixGL = viewingMatrixGL;
}

const std::vector<CScreenLine>& CParametricLineRenderer::GetStaticAndEstimatedProjectedScreenLineList() const
{
    return m_staticAndEstimatedProjectedScreenLineList;
}

void CParametricLineRenderer::SetStaticAndEstimatedProjectedScreenLineList(const std::vector<CScreenLine>& staticAndEstimatedProjectedScreenLineList)
{
    m_staticAndEstimatedProjectedScreenLineList = staticAndEstimatedProjectedScreenLineList;
}

void CParametricLineRenderer::AddEstimatedScreenLineList(const std::vector<CScreenLine>& estimatedScreenLineList)
{
    m_staticAndEstimatedProjectedScreenLineList.insert(m_staticAndEstimatedProjectedScreenLineList.end(), estimatedScreenLineList.begin(), estimatedScreenLineList.end());
}

const std::vector<CFaceTriangle>& CParametricLineRenderer::GetStaticAndEstimatedProjectedScreenFaceTriangleList() const
{
    return m_staticAndEstimatedProjectedScreenFaceTriangleList;
}

void CParametricLineRenderer::SetStaticAndEstimatedProjectedScreenFaceTriangleList(const std::vector<CFaceTriangle>& staticAndEstimatedProjectedScreenFaceTriangleList)
{
    m_staticAndEstimatedProjectedScreenFaceTriangleList = staticAndEstimatedProjectedScreenFaceTriangleList;
}

void CParametricLineRenderer::AddEstimatedScreenFaceTriangleList(const std::vector<CFaceTriangle>& estimatedScreenFaceTriangleList)
{
    m_staticAndEstimatedProjectedScreenFaceTriangleList.insert(m_staticAndEstimatedProjectedScreenFaceTriangleList.end(), estimatedScreenFaceTriangleList.begin(),
            estimatedScreenFaceTriangleList.end());
}

bool CParametricLineRenderer::RenderDynamicElementParticle(std::vector<CScreenLine>& renderedScreenLines, std::list<CLine>& transformedLineList,
        std::list<CFaceTriangle>& transformedFaceTriangleList, DynamicElementParticle::FrontalFacePoints2D& frontalFacePoints,
        const SbVec3f& pA, const SbVec3f& pB, const SbVec3f& pC, const SbVec3f& pD)
{
    renderedScreenLines.clear();

    if (ProjectDynamicElementLinesToScreen(transformedLineList, false) && ProjectDynamicElementFaceTrianglesToScreen(transformedFaceTriangleList, false))
    {
        test(m_projectedScreenLineList);
        CalculateDynamicElementLineIntersectionPoints(m_projectedScreenLineList, m_staticAndEstimatedProjectedScreenLineList, m_intersectionPoints);
        CalculateVisibleDynamicElementLineSegments(m_projectedScreenLineList, m_projectedScreenFaceTriangleList, m_staticAndEstimatedProjectedScreenFaceTriangleList,
                m_intersectionPoints, renderedScreenLines);
        test(renderedScreenLines);

        /* Project frontal face points. */
        //unused variables, but keep possible sideeffects
        /*bool resultA =*/ ProjectModelPointToScreen(pA, frontalFacePoints.pointA);
        /*bool resultB =*/ ProjectModelPointToScreen(pB, frontalFacePoints.pointB);
        /*bool resultC =*/ ProjectModelPointToScreen(pC, frontalFacePoints.pointC);
        /*bool resultD =*/ ProjectModelPointToScreen(pD, frontalFacePoints.pointD);

        return true;
    }

    return false;
}

bool CParametricLineRenderer::ProjectDynamicElementLinesToScreen(std::list<CLine>& transformedLineList, bool projectOnlyVisibleLines)
{
    //    std::cout << "ProjectDynamicElementLinesToScreen" << std::endl;

    m_projectedScreenLineList.clear();

    if (m_pRenderingModel == NULL)
    {
        return false;
    }

    std::vector<CScreenLine> tempProjectedScreenLineList;

    if (transformedLineList.size() > 0)
    {
        //        std::cout << "Inside first IF" << std::endl;

        for (std::list<CLine>::iterator lineIterator = transformedLineList.begin(); lineIterator != transformedLineList.end(); ++lineIterator)
        {
            if (!projectOnlyVisibleLines || lineIterator->m_isVisible)
            {
                //                std::cout << "Inside second IF" << std::endl;

                CScreenLine screenLine;

                if (ProjectLineToScreen(m_rendererConfiguration.projectionMatrixGL, m_viewingMatrixGL, *lineIterator, m_imageWidth, m_imageHeight, m_nearClippingPlane,
                                        screenLine))
                {
                    //                    std::cout << "Inside third IF" << std::endl;

                    screenLine.m_pWorldLineReference = &(*lineIterator);
                    screenLine.m_pModelEdgeReference = screenLine.m_pWorldLineReference->m_pModelEdgeReference;
                    ConditionalAddProjectedLineToList(screenLine, tempProjectedScreenLineList);
                }
            }
        }
    }

    Fit2dLinesToVisibleScreenRegion(tempProjectedScreenLineList, m_visibleScreenRegion, m_projectedScreenLineList);

    return m_projectedScreenLineList.size() > 0;
}

bool CParametricLineRenderer::ProjectDynamicElementFaceTrianglesToScreen(std::list<CFaceTriangle>& transformedFaceTriangleList, bool projectOnlyVisibleFaceTriangles)
{
    m_projectedScreenFaceTriangleList.clear();

    if (m_pRenderingModel == NULL)
    {
        return false;
    }

    if (transformedFaceTriangleList.size() > 0)
    {
        for (std::list<CFaceTriangle>::iterator triangleIterator = transformedFaceTriangleList.begin(); triangleIterator != transformedFaceTriangleList.end();
             ++triangleIterator)
        {
            if (!projectOnlyVisibleFaceTriangles || triangleIterator->m_isVisible)
            {
                ProjectFaceTriangleToScreen(m_rendererConfiguration.projectionMatrixGL, m_viewingMatrixGL, *triangleIterator, m_imageWidth, m_imageHeight, m_nearClippingPlane,
                                            m_projectedScreenFaceTriangleList);
            }
        }
    }

    return m_projectedScreenFaceTriangleList.size() > 0;
}

void CParametricLineRenderer::CalculateDynamicElementLineIntersectionPoints(std::vector<CScreenLine>& projectedScreenLineList,
        const std::vector<CScreenLine>& staticAndEstimatedProjectedScreenLineList,
        std::vector<std::list<SbVec3f> >& intersectionPoints)
{
    std::vector<CScreenLine>::iterator projectedScreenLineIterator;

    intersectionPoints.clear();
    intersectionPoints.resize(projectedScreenLineList.size());

    if (projectedScreenLineList.size() == 0)
    {
        return;
    }

    /* Intersect projected screen lines with static and estimated model screen lines. */
    std::vector<std::list<SbVec3f> >::iterator intersectionSegmentIterator = intersectionPoints.begin();
    for (projectedScreenLineIterator = projectedScreenLineList.begin(); projectedScreenLineIterator != projectedScreenLineList.end();
         ++projectedScreenLineIterator, ++intersectionSegmentIterator)
    {
        intersectionSegmentIterator->push_back(projectedScreenLineIterator->m_p1);
        intersectionSegmentIterator->push_back(projectedScreenLineIterator->m_p2);

        std::vector<CScreenLine>::const_iterator staticAndEstimatedProjectedScreenLineIterator;

        for (staticAndEstimatedProjectedScreenLineIterator = staticAndEstimatedProjectedScreenLineList.begin();
             staticAndEstimatedProjectedScreenLineIterator != staticAndEstimatedProjectedScreenLineList.end(); ++staticAndEstimatedProjectedScreenLineIterator)
        {
            SbVec3f intersectionPoint1, intersectionPoint2;

            if (Intersect2dLines(*projectedScreenLineIterator, *staticAndEstimatedProjectedScreenLineIterator, intersectionPoint1, intersectionPoint2) == eRealIntersectionPoint)
            {
                intersectionSegmentIterator->push_back(intersectionPoint1);
            }
        }
    }

    /* Intersect projected screen lines amongst themselves. */
    intersectionSegmentIterator = intersectionPoints.begin();
    for (projectedScreenLineIterator = projectedScreenLineList.begin(); projectedScreenLineIterator != projectedScreenLineList.end();
         ++projectedScreenLineIterator, ++intersectionSegmentIterator)
    {
        std::vector<CScreenLine>::iterator projectedScreenLineIterator2 = projectedScreenLineIterator + 1;
        std::vector<std::list<SbVec3f> >::iterator intersectionSegmentIterator2 = intersectionSegmentIterator + 1;

        for (; projectedScreenLineIterator2 != projectedScreenLineList.end(); ++projectedScreenLineIterator2, ++intersectionSegmentIterator2)
        {
            SbVec3f intersectionPoint1, intersectionPoint2;

            if (projectedScreenLineIterator != projectedScreenLineIterator2 &&
                Intersect2dLines(*projectedScreenLineIterator, *projectedScreenLineIterator2, intersectionPoint1, intersectionPoint2) == eRealIntersectionPoint)
            {
                intersectionSegmentIterator->push_back(intersectionPoint1);
                intersectionSegmentIterator2->push_back(intersectionPoint2);
            }
        }
    }
}

void CParametricLineRenderer::CalculateVisibleDynamicElementLineSegments(std::vector<CScreenLine>& projectedScreenLineList,
        const std::vector<CFaceTriangle>& projectedTriangleList,
        const std::vector<CFaceTriangle>& staticAndEstimatedProjectedScreenFaceTriangleList,
        std::vector<std::list<SbVec3f> >& intersectionPoints, std::vector<CScreenLine>& resultVisibleLines)
{
    std::list<std::list<CScreenLine> > segmentedLinesList;

    if (projectedScreenLineList.size() > 0)
    {
        for (std::vector<std::list<SbVec3f> >::iterator intersectionSegmentIterator = intersectionPoints.begin(); intersectionSegmentIterator != intersectionPoints.end();
             ++ intersectionSegmentIterator)
        {
            intersectionSegmentIterator->sort(compare_SbVec3fPosition);
            intersectionSegmentIterator->unique();

            std::list<SbVec3f>::const_iterator intersectionPointIterator = intersectionSegmentIterator->begin();
            SbVec3f lastPoint = *intersectionPointIterator;
            ++intersectionPointIterator;

            int currentCorrespondentProjectedLineIndex = intersectionSegmentIterator - intersectionPoints.begin();

            std::list<CScreenLine> curLineSegments;
            for (; intersectionPointIterator != intersectionSegmentIterator->end(); ++intersectionPointIterator)
            {
                CScreenLine segment(lastPoint, *intersectionPointIterator, projectedScreenLineList.at(currentCorrespondentProjectedLineIndex).m_pWorldLineReference);
                segment.m_pModelEdgeReference = segment.m_pWorldLineReference->m_pModelEdgeReference;
                curLineSegments.push_back(segment);
                lastPoint = *intersectionPointIterator;
            }

            segmentedLinesList.push_back(curLineSegments);
        }
    }

    SweepLineDynamicElementVisibleSegmentsCalculation(segmentedLinesList, projectedTriangleList, staticAndEstimatedProjectedScreenFaceTriangleList);

    for (std::list<std::list<CScreenLine> >::const_iterator lineSegmentsIterator = segmentedLinesList.begin(); lineSegmentsIterator != segmentedLinesList.end();
         ++lineSegmentsIterator)
    {
        if (lineSegmentsIterator->size() > 0)
        {
            std::list<CScreenLine>::const_iterator segmentIterator = lineSegmentsIterator->begin();

            CScreenLine curVisibleSegment = *segmentIterator;
            segmentIterator++;

            for (; segmentIterator != lineSegmentsIterator->end();  ++segmentIterator)
            {
                CScreenLine curSegment = *segmentIterator;

                if (curSegment.m_isVisible)
                {
                    if (curVisibleSegment.m_isVisible)
                    {
                        curVisibleSegment.m_p2 = curSegment.m_p2;
                    }
                    else
                    {
                        curVisibleSegment = curSegment;
                    }
                }
                else
                {
                    if (curVisibleSegment.m_isVisible)
                    {
                        ConditionalAddLineToList(curVisibleSegment, resultVisibleLines);
                        curVisibleSegment.m_isVisible = false;
                    }
                }
            }
            if (curVisibleSegment.m_isVisible)
            {
                ConditionalAddLineToList(curVisibleSegment, resultVisibleLines);
                curVisibleSegment.m_isVisible = false;
            }
        }
    }
}

void CParametricLineRenderer::SweepLineDynamicElementVisibleSegmentsCalculation(std::list<std::list<CScreenLine> >& segmentedLinesList,
        const std::vector<CFaceTriangle>& projectedTriangleList,
        const std::vector<CFaceTriangle>& staticAndEstimatedProjectedScreenFaceTriangleList)
{
    std::vector<CSweepLinePoint> sweepLinePoints;
    sweepLinePoints.reserve(segmentedLinesList.size() * 10 + projectedTriangleList.size() * 2);

    for (std::list<std::list<CScreenLine> >::iterator lineSegmentsIterator = segmentedLinesList.begin(); lineSegmentsIterator != segmentedLinesList.end(); ++lineSegmentsIterator)
    {
        for (std::list<CScreenLine>::iterator segmentIterator = lineSegmentsIterator->begin(); segmentIterator != lineSegmentsIterator->end();  ++segmentIterator)
        {
            sweepLinePoints.push_back(CSweepLinePoint(&(*segmentIterator), segmentIterator->CalculateCenterPoint(), CSweepLinePoint::eSegmentCenterPoint));
        }
    }

    for (std::vector<CFaceTriangle>::const_iterator projectedFaceTriangleIterator = projectedTriangleList.begin(); projectedFaceTriangleIterator != projectedTriangleList.end();
         ++projectedFaceTriangleIterator)
    {
        sweepLinePoints.push_back(CSweepLinePoint(&(*projectedFaceTriangleIterator), projectedFaceTriangleIterator->m_boundMinX, CSweepLinePoint::eFaceTriangleBoundMin));
        sweepLinePoints.push_back(CSweepLinePoint(&(*projectedFaceTriangleIterator), projectedFaceTriangleIterator->m_boundMaxX, CSweepLinePoint::eFaceTriangleBoundMax));
    }

    for (std::vector<CFaceTriangle>::const_iterator staticAndEstimatedPojectedFaceTriangleIterator = staticAndEstimatedProjectedScreenFaceTriangleList.begin();
         staticAndEstimatedPojectedFaceTriangleIterator != staticAndEstimatedProjectedScreenFaceTriangleList.end(); ++staticAndEstimatedPojectedFaceTriangleIterator)
    {
        sweepLinePoints.push_back(CSweepLinePoint(&(*staticAndEstimatedPojectedFaceTriangleIterator), staticAndEstimatedPojectedFaceTriangleIterator->m_boundMinX,
                                  CSweepLinePoint::eFaceTriangleBoundMin));
        sweepLinePoints.push_back(CSweepLinePoint(&(*staticAndEstimatedPojectedFaceTriangleIterator), staticAndEstimatedPojectedFaceTriangleIterator->m_boundMaxX,
                                  CSweepLinePoint::eFaceTriangleBoundMax));
    }

    std::sort(sweepLinePoints.begin(), sweepLinePoints.end());

    std::set<const CFaceTriangle*> currentlyActiveTrianglesMap;
    for (std::vector<CSweepLinePoint>::iterator sweepLineIterator = sweepLinePoints.begin(); sweepLineIterator != sweepLinePoints.end(); ++sweepLineIterator)
    {
        CSweepLinePoint curSweepLinePoint = *sweepLineIterator;

        if (curSweepLinePoint.m_sweepLinePointType == CSweepLinePoint::eFaceTriangleBoundMin)
        {
            currentlyActiveTrianglesMap.insert(curSweepLinePoint.m_pFaceTriangle);
        }
        else if (curSweepLinePoint.m_sweepLinePointType == CSweepLinePoint::eFaceTriangleBoundMax)
        {
            currentlyActiveTrianglesMap.erase(curSweepLinePoint.m_pFaceTriangle);
        }
        else
        {
            bool curSweepPointVisible = true;

            for (std::set<const CFaceTriangle*>::iterator activeFaceTriangleIterator = currentlyActiveTrianglesMap.begin(); activeFaceTriangleIterator != currentlyActiveTrianglesMap.end() && curSweepPointVisible; ++activeFaceTriangleIterator)
            {
                const CFaceTriangle* curActiveTriangle = *activeFaceTriangleIterator;

                if (curSweepLinePoint.m_point[1] >= curActiveTriangle->m_boundMinY && curSweepLinePoint.m_point[1] < curActiveTriangle->m_boundMaxY)
                {
                    if (Check2dPointIsHiddenByTriangle(curSweepLinePoint.m_point, *curActiveTriangle))
                    {
                        curSweepPointVisible = false;
                        break;
                    }
                }
            }
            curSweepLinePoint.m_pLine->m_isVisible = curSweepPointVisible;
        }
    }
}

bool CParametricLineRenderer::ProjectLinesToScreen(std::list<CLine>& lineList, std::vector<CScreenLine>& projectedLineList, bool projectOnlyVisibleLines)
{
    projectedLineList.clear();

    std::vector<CScreenLine> tempProjectedScreenLineList;
    if (lineList.size() > 0)
    {
        for (std::list<CLine>::iterator lineIterator = lineList.begin(); lineIterator != lineList.end(); ++lineIterator)
        {
            if (!projectOnlyVisibleLines || lineIterator->m_isVisible)
            {
                CScreenLine screenLine;

                if (ProjectLineToScreen(m_rendererConfiguration.projectionMatrixGL, m_viewingMatrixGL, *lineIterator, m_imageWidth, m_imageHeight, m_nearClippingPlane, screenLine))
                {
                    screenLine.m_pWorldLineReference = &(*lineIterator);
                    ConditionalAddProjectedLineToList(screenLine, tempProjectedScreenLineList);
                }
            }
        }
    }

    Fit2dLinesToVisibleScreenRegion(tempProjectedScreenLineList, m_visibleScreenRegion, projectedLineList);

    return projectedLineList.size() > 0;
}

bool CParametricLineRenderer::ProjectFaceTrianglesToScreen(std::list<CFaceTriangle>& faceTriangleList, std::vector<CFaceTriangle>& projectedFaceTriangleList,
        bool projectOnlyVisibleFaceTriangles)
{
    projectedFaceTriangleList.clear();

    if (faceTriangleList.size() > 0)
    {
        for (std::list<CFaceTriangle>::iterator triangleIterator = faceTriangleList.begin(); triangleIterator != faceTriangleList.end(); ++triangleIterator)
        {
            if (!projectOnlyVisibleFaceTriangles || triangleIterator->m_isVisible)
            {
                ProjectFaceTriangleToScreen(m_rendererConfiguration.projectionMatrixGL, m_viewingMatrixGL, *triangleIterator, m_imageWidth, m_imageHeight, m_nearClippingPlane,
                                            projectedFaceTriangleList);
            }
        }
    }

    return projectedFaceTriangleList.size() > 0;
}

bool CParametricLineRenderer::ProjectModelPointToScreen(const SbVec3f& modelPoint, SbVec2f& screenPoint)
{
    SbVec4f pointHom(modelPoint[0], modelPoint[1], modelPoint[2], 1.0);

    SbVec4f pointEye;
    m_viewingMatrixGL.multVecMatrix(pointHom, pointEye);

    if (OpenGLCalculations::EyePointIsInNearClippingArea(pointEye, m_nearClippingPlane))
    {
        return false;
    }

    SbVec3f projection = OpenGLCalculations::ProjectEyePointToScreen(m_rendererConfiguration.projectionMatrixGL, pointEye, m_imageWidth, m_imageHeight);
    screenPoint = SbVec2f(projection[0], projection[1]);

    return true;
}
