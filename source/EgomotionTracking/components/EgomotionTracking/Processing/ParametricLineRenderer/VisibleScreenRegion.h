#pragma once
#include <EgomotionTracking/components/EgomotionTracking/Helpers/XMLPrimitives.h>

class CVisibleScreenRegion
{
public:
    CVisibleScreenRegion() :
        m_xMin(0.0),
        m_xMax(0.0),
        m_yMin(0.0),
        m_yMax(0.0) {}

    CVisibleScreenRegion(float xMin, float xMax, float yMin, float yMax) :
        m_xMin(xMin), m_xMax(xMax), m_yMin(yMin), m_yMax(yMax) {}
    float m_xMin;
    float m_xMax;
    float m_yMin;
    float m_yMax;
    QDomElement SerializeToQDomElement() const
    {
        QDomDocument tmp;
        QDomElement qDomElement = tmp.createElement("CVisibleScreenRegion");

        qDomElement.setAttribute("xMin", m_xMin);
        qDomElement.setAttribute("yMin", m_yMin);
        qDomElement.setAttribute("xMax", m_xMax);
        qDomElement.setAttribute("yMax", m_yMax);
        return qDomElement;
    }
    bool DeserializeFromQDomElement(QDomElement& lineDomElement)
    {
        if (lineDomElement.tagName() != "CVisibleScreenRegion")
        {
            return false;
        }

        QDomAttr attrXMin = lineDomElement.attributeNode("xMin");
        QDomAttr attrYMin = lineDomElement.attributeNode("yMin");
        QDomAttr attrXMax = lineDomElement.attributeNode("xMax");
        QDomAttr attrYMax = lineDomElement.attributeNode("yMax");

        if (attrXMin.isNull() || attrYMin.isNull() || attrXMax.isNull() || attrYMax.isNull())
        {
            return false;
        }

        bool valueValid;
        m_xMin = attrXMin.value().toFloat(&valueValid);
        m_yMin = attrYMin.value().toFloat(&valueValid);
        m_xMax = attrXMax.value().toFloat(&valueValid);
        m_yMax = attrYMax.value().toFloat(&valueValid);
        if (!valueValid)
        {
            return false;
        }
        return true;
    }
};

