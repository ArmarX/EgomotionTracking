/*
 * IVTEdgeProcessor.h
 *
 *  Created on: 19.03.2013
 *      Author: abyte
 */

#pragma once

#include "TrackingImageProcessorInterface.h"

#include <Image/ByteImage.h>
#include <Image/ShortImage.h>
#include <Image/FloatImage.h>
#include <Image/ImageProcessor.h>
#include <stdlib.h>

class CIVTEdgeProcessor : public CTrackingImageProcessorInterface
{
public:
    CIVTEdgeProcessor();
    ~CIVTEdgeProcessor() override;
    bool DispatchImageProcessing() override;

    int GetThresholdCannyHigh() const
    {
        return m_thresholdCannyHigh;
    }

    void SetThresholdCannyHigh(int thresholdCannyHigh)
    {
        m_thresholdCannyHigh = thresholdCannyHigh;
    }

    int GetThresholdCannyLow() const
    {
        return m_thresholdCannyLow;
    }

    void SetThresholdCannyLow(int thresholdCannyLow)
    {
        m_thresholdCannyLow = thresholdCannyLow;
    }

protected:
    void UpdateImageType() override;

    /* Functions taken from IVT: */
    static void track(unsigned char* magnitudes, int* stack, int offset, int width);
    void Canny(const CByteImage* pInputImage, CByteImage* pOutputImage, int nLowThreshold, int nHighThreshold);

    CByteImage m_inputGrayscaleImage;

    int m_thresholdCannyLow;
    int m_thresholdCannyHigh;

};

