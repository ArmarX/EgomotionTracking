#include "DeviationImageProcessor.h"

#include <Image/FloatImage.h>
#include <Image/ImageProcessor.h>

#include <iostream>

DeviationImageProcessor::DeviationImageProcessor() : m_pInputImage(NULL), m_radius(6), m_densityTreshold(0.95)
{

}

DeviationImageProcessor::~DeviationImageProcessor()
{

}

void DeviationImageProcessor::setInputImage(const CByteImage* pInputImage)
{
    bool typeUpdateNeeded = false;

    if (m_pInputImage == NULL || m_pInputImage->width != pInputImage->width || m_pInputImage->height != pInputImage->height || m_pInputImage->type != pInputImage->type ||
        m_outputImage.width != pInputImage->width || m_outputImage.height != pInputImage->height)
    {
        typeUpdateNeeded = true;
    }

    m_pInputImage = pInputImage;

    if (typeUpdateNeeded)
    {
        m_outputImage.Set(m_pInputImage->width, m_pInputImage->height, m_pInputImage->type, false);
    }
}

const CByteImage* DeviationImageProcessor::getInputImage() const
{
    return m_pInputImage;
}

const CByteImage& DeviationImageProcessor::getOutputImage() const
{
    return m_outputImage;
}

void DeviationImageProcessor::configureProcessor(int radius, float densityTreshold)
{
    if (radius >= 1)
    {
        m_radius = radius;
    }
    else
    {
        std::cout << "DeviationImageProcessor: Enforcing a minimal value of 1 for the radius" << std::endl;

        m_radius = 1;
    }

    m_densityTreshold = densityTreshold;
}

bool DeviationImageProcessor::dispatchImageProcessing(int x0, int x1, int y0, int y1)
{
    /* GENERAL CHECKS. */

    if (!m_pInputImage)
    {
        return false;
    }

    if (m_pInputImage->type != CByteImage::eRGB24)
    {
        return false;
    }

    if ((x0 < 0) || (y0 < 0) || (x1 >= m_pInputImage->width) || (y1 >= m_pInputImage->height) || (x0 > x1) || (y0 > y1))
    {
        return false;
    }

    /* GENERATE DEVIATION IMAGE. */

    const int RX0 = x0 - m_radius;

    const int RY0 = y0 - m_radius;

    const int RX1 = x1 + m_radius;

    const int RY1 = y1 + m_radius;

    if ((RX0 < 0) || (RY0 < 0) || (RX1 >= m_pInputImage->width) || (RY1 >= m_pInputImage->height))
    {
        return false;
    }

    CFloatImage deviationImage(m_pInputImage->width, m_pInputImage->height, 1);
    memset(deviationImage.pixels, 0, sizeof(float) * deviationImage.width * deviationImage.height);

    const int KernelArea = (m_radius * 2 + 1) * (m_radius * 2 + 1);

    float* const pKernelBase = new float[KernelArea];

    float* pKernel = pKernelBase;

    const float Sigma = -(m_radius * m_radius * 2.0f) / (2.0f * log(m_densityTreshold));

    const float ExponentFactor = -1.0f / (2.0f * Sigma);

    float AW = 0.0f;

    for (int DY = -m_radius; DY <= m_radius; ++DY)
    {
        for (int DX = -m_radius; DX <= m_radius; ++DX)
        {
            const float Density = exp((DX * DX + DY * DY) * ExponentFactor);

            *pKernel++ = Density;

            AW += Density;
        }
    }

    pKernel = pKernelBase;

    for (int i = 0; i < KernelArea; ++i, ++pKernel)
    {
        *pKernel = *pKernel / AW;
    }

    const RGBPixel* const pRGBPixelInputBase = (RGBPixel*) m_pInputImage->pixels;

    for (int Y = y0; Y <= y1; ++Y)
    {
        const RGBPixel* pCentralRGBPixel = pRGBPixelInputBase + Y * m_pInputImage->width + x0;

        float* pOutPixel = deviationImage.pixels + Y * deviationImage.width + x0;

        for (int X = x0; X <= x1; ++X, ++pCentralRGBPixel, ++pOutPixel)
        {
            float AR = 0.0f, AG = 0.0f, AB = 0.0f;

            const float* pKernel = pKernelBase;

            for (int DY = -m_radius; DY <= m_radius; ++DY)
            {
                const RGBPixel* pRegionRGBPixel = pRGBPixelInputBase + (Y + DY) * m_pInputImage->width + (X - m_radius);

                for (int DX = -m_radius; DX <= m_radius; ++DX, ++pRegionRGBPixel, ++pKernel)
                {
                    AR += float(pRegionRGBPixel->m_R) * *pKernel;

                    AG += float(pRegionRGBPixel->m_G) * *pKernel;

                    AB += float(pRegionRGBPixel->m_B) * *pKernel;
                }
            }

            pKernel = pKernelBase;

            float DeviationAccumulator = 0.0f;

            for (int DY = -m_radius; DY <= m_radius; ++DY)
            {
                const RGBPixel* pRegionRGBPixel = pRGBPixelInputBase + (Y + DY) * m_pInputImage->width + (X - m_radius);

                for (int DX = -m_radius; DX <= m_radius; ++DX, ++pRegionRGBPixel, ++pKernel)
                {
                    const float DR = float(pRegionRGBPixel->m_R) - AR;

                    const float DG = float(pRegionRGBPixel->m_G) - AG;

                    const float DB = float(pRegionRGBPixel->m_B) - AB;

                    DeviationAccumulator += sqrt(DR * DR + DG * DG + DB * DB) * *pKernel;
                }
            }

            *pOutPixel = DeviationAccumulator;
        }
    }

    delete[] pKernelBase;

    /* SETUP OUTPUT IMAGE. */

    RGBPixel* const pRGBPixelOutputBase = (RGBPixel*) m_outputImage.pixels;

    memset(pRGBPixelOutputBase, 128, sizeof(RGBPixel) * m_outputImage.width * m_outputImage.height);

    float MaximalValue = -__FLT_MAX__;

    float MinimalValue = __FLT_MAX__;

    for (int Y = y0; Y <= y1; ++Y)
    {
        const float* pInputPixel = deviationImage.pixels + deviationImage.width * Y + x0;

        for (int X = x0; X <= x1; ++X, ++pInputPixel)
        {
            if (*pInputPixel > MaximalValue)
            {
                MaximalValue = *pInputPixel;
            }

            if (*pInputPixel < MinimalValue)
            {
                MinimalValue = *pInputPixel;
            }
        }
    }

    const float Range = MaximalValue - MinimalValue;

    if (Range < __FLT_EPSILON__)
    {
        return false;
    }

    const float Normalization = 255.0f / Range;

    for (int Y = y0; Y <= y1; ++Y)
    {
        const float* pInputPixel = deviationImage.pixels + deviationImage.width * Y + x0;

        RGBPixel* pRGBPixel = pRGBPixelOutputBase + m_outputImage.width * Y + x0;

        for (int X = x0; X <= x1; ++X, ++pRGBPixel, ++pInputPixel)
        {
            pRGBPixel->m_R = pRGBPixel->m_G = pRGBPixel->m_B = int((*pInputPixel - MinimalValue) * Normalization + 0.5f);
        }
    }

    return true;
}



