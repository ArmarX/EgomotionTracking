/*
 * VirtualEdgeImageProcessor.h
 *
 *  Created on: 22.03.2013
 *      Author: abyte
 */

#pragma once

#include "TrackingImageProcessorInterface.h"
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/VirtualArmar3RobotControl.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/ParametricLineRenderer.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/RandomGenerator/ParkMillerRandomGenerator.h>

class CVirtualTrackingImageProcessor: public CTrackingImageProcessorInterface
{
public:
    CVirtualTrackingImageProcessor();
    ~CVirtualTrackingImageProcessor() override;

    bool DispatchImageProcessing() override;

    const CParametricLineRenderer* GetParametricLineRenderer() const
    {
        return m_pParametricLineRenderer;
    }

    void SetParametricLineRenderer(CParametricLineRenderer* pParametricLineRenderer)
    {
        m_pParametricLineRenderer = pParametricLineRenderer;
    }

    const CVirtualArmar3RobotControl* GetVirtualArmar3RobotControl() const
    {
        return m_pVirtualArmar3RobotControl;
    }

    void SetVirtualArmar3RobotControl(const CVirtualArmar3RobotControl* pVirtualArmar3RobotControl)
    {
        m_pVirtualArmar3RobotControl = pVirtualArmar3RobotControl;
    }
    void UpdateImageType() override;


    void SetSaltPepperNoise(bool enabled, float blackToWhiteNoiseFactor, float whiteToBlackNoiseFactor);
    void SetRandomEdges(bool enabled, unsigned int numberOfRandomEdges);

protected:
    void AddSaltPepperNoise();
    void AddRandomEdges();

    const CVirtualArmar3RobotControl* m_pVirtualArmar3RobotControl;
    CParametricLineRenderer* m_pParametricLineRenderer;

    float m_blackToWhiteNoiseFactor;
    float m_whiteToBlackNoiseFactor;
    bool m_useSaltPepperNoise;
    bool m_useRandomEdges;
    unsigned int m_numberOfRandomEdges;
    CParkMillerRandomGenerator m_randomGenerator;
};

