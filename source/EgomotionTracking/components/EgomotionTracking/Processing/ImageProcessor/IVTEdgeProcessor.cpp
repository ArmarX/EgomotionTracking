/*
 * IVTEdgeProcessor.cpp
 *
 *  Created on: 19.03.2013
 *      Author: abyte
 */

#include "IVTEdgeProcessor.h"
#include <math.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Conversions.h>
#include <deque>

CIVTEdgeProcessor::CIVTEdgeProcessor() :
    m_thresholdCannyLow(60),
    m_thresholdCannyHigh(80)
{
    EnableProcessingOptionSupport(eEdgeImageOption);
    EnableProcessingOptionSupport(eGradientOrientationOption);
    EnableProcessingOptionSupport(eEdgeListOption);
}

CIVTEdgeProcessor::~CIVTEdgeProcessor()
{
}

struct RGBPixel
{
    unsigned char m_R;

    unsigned char m_G;

    unsigned char m_B;
};

bool GenerateDeviationImage(const CByteImage* pSourceImage, CFloatImage* pDestinationImage, const int X0, const int Y0, const int X1, const int Y1, const int Radius, const float DensityThreshold)
{
    if (!pSourceImage)
    {
        return false;
    }

    if (!pDestinationImage)
    {
        return false;
    }

    if (pSourceImage->type != CByteImage::eRGB24)
    {
        return false;
    }

    if (pSourceImage->width != pDestinationImage->width)
    {
        return false;
    }

    if (pSourceImage->height != pDestinationImage->height)
    {
        return false;
    }

    if (Radius < 1)
    {
        return false;
    }

    const int RX0 = X0 - Radius;

    const int RY0 = Y0 - Radius;

    const int RX1 = X1 + Radius;

    const int RY1 = Y1 + Radius;

    if ((RX0 < 0) || (RY0 < 0) || (RX1 >= pSourceImage->width) || (RY1 >= pSourceImage->height) || (RX0 > RX1) || (RY0 > RY1))
    {
        return false;
    }

    memset(pDestinationImage->pixels, 0, sizeof(float)*pDestinationImage->width * pDestinationImage->height);

    const int KernelArea = (Radius * 2 + 1) * (Radius * 2 + 1);

    float* const pKernelBase = new float[KernelArea];

    float* pKernel = pKernelBase;

    const float Sigma = -(Radius * Radius * 2.0f) / (2.0f * log(DensityThreshold));

    const float ExponentFactor = -1.0f / (2.0f * Sigma);

    float AW = 0.0f;

    for (int DY = -Radius; DY <= Radius; ++DY)
    {
        for (int DX = -Radius; DX <= Radius; ++DX)
        {
            const float Density = exp((DX * DX + DY * DY) * ExponentFactor);

            *pKernel++ = Density;

            AW += Density;
        }
    }

    pKernel = pKernelBase;

    for (int i = 0; i < KernelArea; ++i, ++pKernel)
    {
        *pKernel = *pKernel / AW;
    }

    const RGBPixel* const pRGBPixelBase = (RGBPixel*) pSourceImage->pixels;

    for (int Y = Y0; Y <= Y1; ++Y)
    {
        const RGBPixel* pCentralRGBPixel = pRGBPixelBase + Y * pSourceImage->width + X0;

        float* pOutPixel = pDestinationImage->pixels + Y * pDestinationImage->width + X0;

        for (int X = X0; X <= X1; ++X, ++pCentralRGBPixel, ++pOutPixel)
        {
            float AR = 0.0f, AG = 0.0f, AB = 0.0f;

            const float* pKernel = pKernelBase;

            for (int DY = -Radius; DY <= Radius; ++DY)
            {
                const RGBPixel* pRegionRGBPixel = pRGBPixelBase + (Y + DY) * pSourceImage->width + (X - Radius);

                for (int DX = -Radius; DX <= Radius; ++DX, ++pRegionRGBPixel, ++pKernel)
                {
                    AR += float(pRegionRGBPixel->m_R)** pKernel;

                    AG += float(pRegionRGBPixel->m_G)** pKernel;

                    AB += float(pRegionRGBPixel->m_B)** pKernel;
                }
            }

            pKernel = pKernelBase;

            float DeviationAccumulator = 0.0f;

            for (int DY = -Radius; DY <= Radius; ++DY)
            {
                const RGBPixel* pRegionRGBPixel = pRGBPixelBase + (Y + DY) * pSourceImage->width + (X - Radius);

                for (int DX = -Radius; DX <= Radius; ++DX, ++pRegionRGBPixel, ++pKernel)
                {
                    const float DR = float(pRegionRGBPixel->m_R) - AR;

                    const float DG = float(pRegionRGBPixel->m_G) - AG;

                    const float DB = float(pRegionRGBPixel->m_B) - AB;

                    DeviationAccumulator += sqrt(DR * DR + DG * DG + DB * DB)** pKernel;
                }
            }

            *pOutPixel = DeviationAccumulator;
        }
    }

    delete[] pKernelBase;

    return true;
}

bool ExportDeviationImage(const CFloatImage* pDeviationImage, CByteImage* pDisplayImage, const int X0, const int Y0, const int X1, const int Y1)
{
    if (!pDeviationImage)
    {
        return false;
    }

    if (!pDisplayImage)
    {
        return false;
    }

    if (pDisplayImage->type != CByteImage::eRGB24)
    {
        return false;
    }

    if (pDeviationImage->width != pDisplayImage->width)
    {
        return false;
    }

    if (pDeviationImage->height != pDisplayImage->height)
    {
        return false;
    }

    if ((X0 < 0) || (Y0 < 0) || (X1 >= pDeviationImage->width) || (Y1 >= pDeviationImage->height) || (X0 > X1) || (Y0 > Y1))
    {
        return false;
    }

    RGBPixel* const pRGBPixelBase = (RGBPixel*) pDisplayImage->pixels;

    memset(pRGBPixelBase, 128, sizeof(RGBPixel)*pDisplayImage->width * pDisplayImage->height);

    float MaximalValue = -__FLT_MAX__;

    float MinimalValue = __FLT_MAX__;

    for (int Y = Y0; Y <= Y1; ++Y)
    {
        const float* pInputPixel = pDeviationImage->pixels + pDeviationImage->width * Y + X0;

        for (int X = X0; X <= X1; ++X, ++pInputPixel)
        {
            if (*pInputPixel > MaximalValue)
            {
                MaximalValue = *pInputPixel;
            }

            if (*pInputPixel < MinimalValue)
            {
                MinimalValue = *pInputPixel;
            }
        }
    }

    const float Range = MaximalValue - MinimalValue;

    if (Range < __FLT_EPSILON__)
    {
        return false;
    }

    const float Normalization = 255.0f / Range;

    for (int Y = Y0; Y <= Y1; ++Y)
    {
        const float* pInputPixel = pDeviationImage->pixels + pDeviationImage->width * Y + X0;

        RGBPixel* pRGBPixel = pRGBPixelBase + pDisplayImage->width * Y + X0;

        for (int X = X0; X <= X1; ++X, ++pRGBPixel, ++pInputPixel)
        {
            pRGBPixel->m_R = pRGBPixel->m_G = pRGBPixel->m_B = int((*pInputPixel - MinimalValue) * Normalization + 0.5f);
        }
    }

    return true;
}

bool Deviation(const CByteImage* pSourceImage, CByteImage* pDisplayImage, const int X0, const int Y0, const int X1, const int Y1, const int Radius = 6, const float DensityThreshold = 0.05f)
{
    if (!pSourceImage)
    {
        return false;
    }

    if (!pDisplayImage)
    {
        return false;
    }

    if (pSourceImage->type != CByteImage::eRGB24)
    {
        return false;
    }

    if (pSourceImage->width != pDisplayImage->width)
    {
        return false;
    }

    if (pSourceImage->height != pDisplayImage->height)
    {
        return false;
    }

    if (pSourceImage->type != pDisplayImage->type)
    {
        return false;
    }

    CFloatImage DeviationImage(pSourceImage->width, pSourceImage->height, 1);

    if (!GenerateDeviationImage(pSourceImage, &DeviationImage, X0, Y0, X1, Y1, Radius, DensityThreshold))
    {
        return false;
    }

    if (!ExportDeviationImage(&DeviationImage, pDisplayImage, X0, Y0, X1, Y1))
    {
        return false;
    }

    return true;
}

bool CIVTEdgeProcessor::DispatchImageProcessing()
{
    if (m_pInputImage == NULL)
    {
        return false;
    }

    if (EnabledImageProcessingOption(eEdgeImageOption) ||
        EnabledImageProcessingOption(eGradientOrientationOption) ||
        EnabledImageProcessingOption(eEdgeListOption))
    {
        ImageProcessor::ConvertImage(m_pInputImage, &m_inputGrayscaleImage);
        ImageProcessor::GaussianSmooth3x3(&m_inputGrayscaleImage, &m_inputGrayscaleImage);
        Canny(&m_inputGrayscaleImage, &m_edgeImage, m_thresholdCannyLow, m_thresholdCannyHigh);
    }

    if (EnabledImageProcessingOption(eEdgeListOption))
    {
        Vec2dList tmp;
        ImageProcessor::HoughTransformLines(&m_edgeImage, &m_edgeImage, tmp, 20, 10);
    }

    return true;
}

void CIVTEdgeProcessor::UpdateImageType()
{
    if (m_pInputImage != NULL)
    {
        m_inputGrayscaleImage.Set(m_pInputImage->width, m_pInputImage->height, CByteImage::eGrayScale, false);
    }
    CTrackingImageProcessorInterface::UpdateImageType();
}

struct DiscretePoint
{
    DiscretePoint(const int X, const int Y):
        m_X(X),
        m_Y(Y)
    {
    }

    int m_X;

    int m_Y;
};

struct Blob
{
    Blob(const int X, const int Y):
        m_X0(X),
        m_Y0(Y),
        m_X1(X),
        m_Y1(Y)
    {
        memset(m_ConnectivityHistogram, 0, sizeof(int) * 9);
    }

    int m_ConnectivityHistogram[9];

    int m_X0;

    int m_Y0;

    int m_X1;

    int m_Y1;

    std::deque<DiscretePoint> m_DiscretePoints;
};

float Bhattacharyya(const float* pDistributionA, const float* pDistributionB, int Length)
{
    float Accumulator = 0.0f;

    for (int i = 0; i < Length; ++i)
    {
        Accumulator += sqrt(pDistributionA[i] * pDistributionB[i]);
    }

    return -log(Accumulator);
}

//Magic!!!
const float g_ReferenceDistributionA[9] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};

const float g_ReferenceDistributionB[9] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};

bool ClassifyDistribution(const float* pDistribution, const int Length, const float DistanceThreshold)
{
    if (Bhattacharyya(pDistribution, g_ReferenceDistributionA, Length) < DistanceThreshold)
    {
        return true;
    }

    if (Bhattacharyya(pDistribution, g_ReferenceDistributionB, Length) < DistanceThreshold)
    {
        return true;
    }

    return false;
}

bool ClassifyBlob(const Blob* pBlob, const float DistanceThreshold)
{
    int TotalFrequency = 0;

    for (int i = 0; i < 9; ++i)
    {
        TotalFrequency += pBlob->m_ConnectivityHistogram[i];
    }

    float Distribution[9];

    for (int i = 0; i < 9; ++i)
    {
        Distribution[i] = float(pBlob->m_ConnectivityHistogram[i]) / float(TotalFrequency);
    }

    return ClassifyDistribution(Distribution, 8, DistanceThreshold);
}

void AdaptiveBlobBasedNoiseRemoval(const int Width, const int Height, unsigned char* const pPixels, const int MinimalBlobSize, const int MinimalDimension)
{
    unsigned char* pPixel = pPixels;

    const int SubWidth = Width - 1;

    const int SubHeight = Height - 1;

    std::deque<Blob*> SelectedBlobs;

    std::deque<Blob*> RejectedBlobs;

    std::deque<DiscretePoint> Expanding;

    for (int Y = 0; Y < Height; ++Y)
    {
        for (int X = 0; X < Width; ++X, ++pPixel)
        {
            if (*pPixel == 255)
            {
                *pPixel = 128;

                Blob* pBlob = new Blob(X, Y);

                const DiscretePoint RootPoint(X, Y);

                Expanding.push_back(RootPoint);

                pBlob->m_DiscretePoints.push_back(RootPoint);

                while (Expanding.size())
                {
                    const DiscretePoint ExpandingLocation = Expanding.front();
                    Expanding.pop_front();

                    const int SX0 = std::max(ExpandingLocation.m_X - 1, 0);
                    const int SY0 = std::max(ExpandingLocation.m_Y - 1, 0);
                    const int SX1 = std::min(ExpandingLocation.m_X + 1, SubWidth);
                    const int SY1 = std::min(ExpandingLocation.m_Y + 1, SubHeight);

                    for (int SY = SY0; SY <= SY1; ++SY)
                    {
                        unsigned char* pSearchPixel = pPixels + Width * SY + SX0;

                        for (int SX = SX0; SX <= SX1; ++SX, ++pSearchPixel)
                        {
                            if (*pSearchPixel == 255)
                            {
                                *pSearchPixel = 128;

                                const DiscretePoint ExpandedPoint(SX, SY);

                                Expanding.push_back(ExpandedPoint);

                                pBlob->m_DiscretePoints.push_back(ExpandedPoint);

                                if (SX < pBlob->m_X0)
                                {
                                    pBlob->m_X0 = SX;
                                }
                                else if (SX > pBlob->m_X1)
                                {
                                    pBlob->m_X1 = SX;
                                }
                                if (SY < pBlob->m_Y0)
                                {
                                    pBlob->m_Y0 = SY;
                                }
                                else if (SY > pBlob->m_Y1)
                                {
                                    pBlob->m_Y1 = SY;
                                }
                            }
                        }
                    }
                }

                const bool SizeCriterion = int(pBlob->m_DiscretePoints.size()) > MinimalBlobSize;

                if (SizeCriterion)
                {
                    const int BlobHeight = pBlob->m_Y1 - pBlob->m_Y0 + 1;
                    const int BlobWidth = pBlob->m_X1 - pBlob->m_X0 + 1;

                    const bool DimensionCriterion = std::max(BlobHeight, BlobWidth) > MinimalDimension;

                    if (DimensionCriterion)
                    {
                        std::deque<DiscretePoint>::iterator EndPoints = pBlob->m_DiscretePoints.end();

                        for (std::deque<DiscretePoint>::iterator pDiscretePoint = pBlob->m_DiscretePoints.begin(); pDiscretePoint != EndPoints; ++pDiscretePoint)
                        {
                            const int SX0 = std::max(pDiscretePoint->m_X - 1, 0);
                            const int SY0 = std::max(pDiscretePoint->m_Y - 1, 0);
                            const int SX1 = std::min(pDiscretePoint->m_X + 1, SubWidth);
                            const int SY1 = std::min(pDiscretePoint->m_Y + 1, SubHeight);

                            for (int SY = SY0; SY <= SY1 ; ++SY)
                            {
                                unsigned char* pSearchPixel = pPixels + Width * SY + SX0;

                                const int DY = pDiscretePoint->m_Y - SY;

                                for (int SX = SX0; SX <= SX1; ++SX, ++pSearchPixel)
                                {
                                    const int DX = pDiscretePoint->m_X - SX;

                                    if (DY || DX)
                                    {
                                        if (*pSearchPixel == 128)
                                        {
                                            ++pBlob->m_ConnectivityHistogram[((DY + 1) * 3) + (DX + 1)];
                                        }
                                    }
                                }
                            }
                        }

                        //Magic!!!
                        //const float DistanceThreshold = 0.0f;

                        //if(ClassifyBlob(pBlob,DistanceThreshold))
                        {
                            SelectedBlobs.push_back(pBlob);

                            pBlob = NULL;
                        }
                    }
                }

                if (pBlob)
                {
                    RejectedBlobs.push_back(pBlob);
                }
            }
        }
    }

    std::deque<Blob*>::iterator EndBlobs = SelectedBlobs.end();
    for (std::deque<Blob*>::iterator ppBlob = SelectedBlobs.begin(); ppBlob != EndBlobs; ++ppBlob)
    {
        std::deque<DiscretePoint>::iterator EndPoints = (*ppBlob)->m_DiscretePoints.end();

        for (std::deque<DiscretePoint>::iterator pDiscretePoint = (*ppBlob)->m_DiscretePoints.begin(); pDiscretePoint != EndPoints; ++pDiscretePoint)
        {
            unsigned char* pOutActivePixel = pPixels + (Width * pDiscretePoint->m_Y) + pDiscretePoint->m_X;

            *pOutActivePixel = 255;
        }

        delete *ppBlob;
    }

    EndBlobs = RejectedBlobs.end();
    for (std::deque<Blob*>::iterator ppBlob = RejectedBlobs.begin(); ppBlob != EndBlobs; ++ppBlob)
    {
        std::deque<DiscretePoint>::iterator EndPoints = (*ppBlob)->m_DiscretePoints.end();

        for (std::deque<DiscretePoint>::iterator pDiscretePoint = (*ppBlob)->m_DiscretePoints.begin(); pDiscretePoint != EndPoints; ++pDiscretePoint)
        {
            unsigned char* pOutActivePixel = pPixels + (Width * pDiscretePoint->m_Y) + pDiscretePoint->m_X;

            *pOutActivePixel = 64;
        }

        delete *ppBlob;
    }
}

void CIVTEdgeProcessor::Canny(const CByteImage* pInputImage, CByteImage* pOutputImage, int nLowThreshold, int nHighThreshold)
{

    if (pInputImage->width != pOutputImage->width || pInputImage->height != pOutputImage->height ||
        pInputImage->type != pOutputImage->type || pInputImage->type != CByteImage::eGrayScale)
    {
        printf("error: input and output image do not match for ImageProcessor::Canny\n");
        return;
    }

    if (nLowThreshold == 0)
    {
        nLowThreshold = 1;
    }
    if (nHighThreshold == 0)
    {
        nHighThreshold = 1;
    }

    const int width = pInputImage->width;
    const int height = pInputImage->height;
    const int nPixels = width * height;

    CShortImage gradientsX(width, height), gradientsY(width, height);
    ImageProcessor::SobelX(pInputImage, &gradientsX, false);
    ImageProcessor::SobelY(pInputImage, &gradientsY, false);

    ImageProcessor::Zero(pOutputImage);

    unsigned char* output = pOutputImage->pixels;
    short* magnitudes = gradientsX.pixels; // alias for the gradientX image

    int i;

    // calculate gradients and sectors
    for (i = 0; i < nPixels; i++)
    {
        const int gx = gradientsX.pixels[i];
        const int gy = gradientsY.pixels[i];
        const int agx = abs(gx);
        const int agy = abs(gy);
        const int g = agx + agy;
        //const int g = short(sqrtf(float(gx * gx + gy * gy)));



        magnitudes[i] = g;

        if (g >= nLowThreshold)
        {
            const float gradientDirection = atan2f(static_cast<float>(gy), static_cast<float>(gx));
            const float gradientOrientation = gradientDirection > 0 ? gradientDirection : gradientDirection + M_PI;
            if (m_pGradientOrientations != NULL)
            {
                m_pGradientOrientations[i] = gradientOrientation;
            }

            m_pGradients[i][0] = gx;
            m_pGradients[i][1] = gy;

            if (agx > (agy << 1))
            {
                output[i] = 10;
            }
            else if ((agx << 1) > agy)
            {
                output[i] = gx * gy >= 0 ? 12 : 13;
            }
            else
            {
                output[i] = 11;
            }
        }
    }

    // apply non maximal supression
    for (i = 0; i < nPixels; i++)
    {
        const int g = (int) magnitudes[i];

        if (g >= nLowThreshold)
        {
            switch (output[i])
            {
                case 10:
                    if (magnitudes[i + 1] <= g && magnitudes[i - 1] < g)
                    {
                        output[i] = g >= nHighThreshold ? 254 : 1;
                    }
                    break;

                case 11:
                    if (magnitudes[i + width] <= g && magnitudes[i - width] < g)
                    {
                        output[i] = g >= nHighThreshold ? 254 : 1;
                    }
                    break;

                case 12:
                    if (magnitudes[i + width + 1] <= g && magnitudes[i - width - 1] < g)
                    {
                        output[i] = g >= nHighThreshold ? 254 : 1;
                    }
                    break;

                case 13:
                    if (magnitudes[i + width - 1] <= g && magnitudes[i - width + 1] < g)
                    {
                        output[i] = g >= nHighThreshold ? 254 : 1;
                    }
                    break;
            }
        }
    }

    // track
    int* stack = new int[nPixels];

    for (i = 0; i < nPixels; i++)
        if (output[i] == 254)
        {
            track(output, stack, i, width);
            output[i] = 255;
        }

    for (i = 0; i < nPixels; i++)
    {
        if (output[i] != 255)
        {
            output[i] = 0;
        }
    }

    delete [] stack;

    AdaptiveBlobBasedNoiseRemoval(width, height, output, 32, 16);
}

void CIVTEdgeProcessor::track(unsigned char* magnitudes, int* stack, int offset, int width)
{
    int sp = 0;

    stack[sp++] = offset;

    while (sp--)
    {
        const int offset = stack[sp];

        if (magnitudes[offset - width - 1] == 1)
        {
            stack[sp++] = offset - width - 1;
            magnitudes[offset - width - 1] = 255;
        }

        if (magnitudes[offset + width + 1] == 1)
        {
            stack[sp++] = offset + width + 1;
            magnitudes[offset + width + 1] = 255;
        }

        if (magnitudes[offset - width + 1] == 1)
        {
            stack[sp++] = offset - width + 1;
            magnitudes[offset - width + 1] = 255;
        }

        if (magnitudes[offset + width - 1] == 1)
        {
            stack[sp++] = offset + width - 1;
            magnitudes[offset + width - 1] = 255;
        }

        if (magnitudes[offset - width] == 1)
        {
            stack[sp++] = offset - width;
            magnitudes[offset - width] = 255;
        }

        if (magnitudes[offset + width] == 1)
        {
            stack[sp++] = offset + width;
            magnitudes[offset + width] = 255;
        }

        if (magnitudes[offset - 1] == 1)
        {
            stack[sp++] = offset - 1;
            magnitudes[offset - 1] = 255;
        }

        if (magnitudes[offset + 1] == 1)
        {
            stack[sp++] = offset + 1;
            magnitudes[offset + 1] = 255;
        }
    }
}
