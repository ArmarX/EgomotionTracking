/*
 * TrackingImageProcessorInterface.h
 *
 *  Created on: 19.03.2013
 *      Author: abyte
 */

#pragma once

#include <Image/ByteImage.h>

#include <iostream>

class CTrackingImageProcessorInterface
{
public:
    CTrackingImageProcessorInterface();
    virtual ~CTrackingImageProcessorInterface();

    virtual bool DispatchImageProcessing() = 0;

    const CByteImage& GetEdgeImage() const
    {
        if (!SupportsProcessingOption(eEdgeImageOption))
        {
            std::cout << "ERROR! No Support for Edge Images!" << std::endl;
        }

        return m_edgeImage;
    }

    const float* GetGradientOrientations() const
    {
        if (!SupportsProcessingOption(eGradientOrientationOption))
        {
            std::cout << "ERROR! No Support for Gradient Orientation!" << std::endl;
        }

        return m_pGradientOrientations;
    }

    float** GetGradients()
    {
        return m_pGradients;
    }

    enum ImageProcessingOption
    {
        eEdgeImageOption,
        eGradientOrientationOption,
        eEdgeListOption,
        eEndOfImageProcessingEnum
    };
    bool SetProcessingOption(ImageProcessingOption option, bool enabled);
    bool SupportsProcessingOption(ImageProcessingOption option) const;
    bool EnabledImageProcessingOption(ImageProcessingOption option) const;

    const CByteImage* GetInputImage() const
    {
        return m_pInputImage;
    }

    void SetInputImage(const CByteImage* pInputImage)
    {
        bool typeUpdateNeeded = false;
        if (m_pInputImage == NULL ||
            m_pInputImage->width != pInputImage->width ||
            m_pInputImage->height != pInputImage->height ||
            m_pInputImage->type != pInputImage->type ||
            m_edgeImage.width != pInputImage->width ||
            m_edgeImage.height != pInputImage->height)
        {
            typeUpdateNeeded = true;

        }
        m_pInputImage = pInputImage;
        if (typeUpdateNeeded)
        {
            UpdateImageType();
        }
    }

protected:
    bool EnableProcessingOptionSupport(ImageProcessingOption option);

    virtual void UpdateImageType();

    CByteImage m_edgeImage;
    const CByteImage* m_pInputImage;
    float* m_pGradientOrientations;
    float** m_pGradients;

private:

    bool m_enabledProcessingOptions[eEndOfImageProcessingEnum];
    bool m_supportedProcessingOptions[eEndOfImageProcessingEnum];
};

