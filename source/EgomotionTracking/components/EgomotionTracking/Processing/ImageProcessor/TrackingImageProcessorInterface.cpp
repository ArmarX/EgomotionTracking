/*
 * TrackingImageProcessorInterface.cpp
 *
 *  Created on: 19.03.2013
 *      Author: abyte
 */

#include "TrackingImageProcessorInterface.h"

CTrackingImageProcessorInterface::CTrackingImageProcessorInterface() : m_pInputImage(NULL), m_pGradientOrientations(NULL), m_pGradients(NULL)
{

    for (unsigned int i = 0; i < eEndOfImageProcessingEnum; ++i)
    {
        m_supportedProcessingOptions[i] = 0;
        m_enabledProcessingOptions[i] = 0;
    }
}

CTrackingImageProcessorInterface::~CTrackingImageProcessorInterface()
{
    if (m_pGradientOrientations != NULL)
    {
        delete (m_pGradientOrientations);
    }
    if (m_pGradients != NULL)
    {
        for (int i = 0; i < (m_pInputImage->width * m_pInputImage->height); i++)
        {
            delete [] m_pGradients[i];
        }
        delete [] m_pGradients;
    }

}

bool CTrackingImageProcessorInterface::SupportsProcessingOption(ImageProcessingOption option) const
{
    if (option >= eEndOfImageProcessingEnum || option < 0)
    {
        return false;
    }
    return m_supportedProcessingOptions[option];
}

bool CTrackingImageProcessorInterface::EnabledImageProcessingOption(ImageProcessingOption option) const
{
    if (option >= eEndOfImageProcessingEnum || option < 0)
    {
        return false;
    }

    if (!SupportsProcessingOption(option))
    {
        return false;
    }

    return m_enabledProcessingOptions[option];
}

bool CTrackingImageProcessorInterface::SetProcessingOption(ImageProcessingOption option, bool enabled)
{
    if (option >= eEndOfImageProcessingEnum || option < 0)
    {
        return false;
    }

    if (!SupportsProcessingOption(option))
    {
        return false;
    }
    m_enabledProcessingOptions[option] = enabled;
    return true;
}

bool CTrackingImageProcessorInterface::EnableProcessingOptionSupport(ImageProcessingOption option)
{
    if (option >= eEndOfImageProcessingEnum || option < 0)
    {
        return false;
    }
    m_supportedProcessingOptions[option] = true;
    return true;
}

void CTrackingImageProcessorInterface::UpdateImageType()
{
    if (m_pInputImage != NULL)
    {
        m_edgeImage.Set(m_pInputImage->width, m_pInputImage->height, CByteImage::eGrayScale, false);
        if (m_pGradientOrientations != NULL)
        {
            delete[] m_pGradientOrientations;
        }
        m_pGradientOrientations = new float[m_pInputImage->width * m_pInputImage->height];
        if (m_pGradients != NULL)
        {
            for (int i = 0; i < (m_pInputImage->width * m_pInputImage->height); i++)
            {
                delete [] m_pGradients[i];
            }
            delete [] m_pGradients;
        }
        m_pGradients = new float*[m_pInputImage->width * m_pInputImage->height];
        for (int i = 0; i < (m_pInputImage->width * m_pInputImage->height); i++)
        {
            m_pGradients[i] = new float[2];
        }
    }
}
