/*
 * VirtualEdgeImageProcessor.cpp
 *
 *  Created on: 22.03.2013
 *      Author: abyte
 */

#include "VirtualEdgeImageProcessor.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/CameraConfiguration.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/PrimitiveDrawer.h>

#include <EgomotionTracking/components/EgomotionTrackingConfiguration/TrackingConfiguration.h>

#include <Inventor/SbVec3f.h>
#include <Inventor/SbRotation.h>


CVirtualTrackingImageProcessor::CVirtualTrackingImageProcessor() :
    m_pVirtualArmar3RobotControl(NULL), m_pParametricLineRenderer(NULL), m_useSaltPepperNoise(false)
{
    EnableProcessingOptionSupport(eEdgeImageOption);
    EnableProcessingOptionSupport(eGradientOrientationOption);
    EnableProcessingOptionSupport(eEdgeListOption);
    UpdateImageType();
}

CVirtualTrackingImageProcessor::~CVirtualTrackingImageProcessor()
{
}

bool CVirtualTrackingImageProcessor::DispatchImageProcessing()
{
    if (m_pVirtualArmar3RobotControl == NULL)
    {
        std::cout << "CVirtualTrackingImageProcessor::DispatchImageProcessing: Error m_pVirtualArmar3RobotControl == NULL" << std::endl;
        return false;
    }
    if (m_pParametricLineRenderer == NULL)
    {
        std::cout << "CVirtualTrackingImageProcessor::DispatchImageProcessing: Error m_pParametricLineRenderer == NULL" << std::endl;
        return false;
    }

    SbVec3f cameraTranslation;
    SbRotation cameraOrientation;
    m_pVirtualArmar3RobotControl->CalculateSelfCameraPosition(cameraTranslation, cameraOrientation);

    m_pParametricLineRenderer->SetCameraPosition(OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(cameraTranslation, OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(cameraOrientation)));
    m_pParametricLineRenderer->RenderVisibleLines();

    CByteImage tmpColorImage(configuration_imageWidth, configuration_imageHeight, CByteImage::eRGB24);
    ImageProcessor::Zero(&tmpColorImage);

    PrimitiveDrawer::draw2dLines(m_pParametricLineRenderer->GetVisibleScreenLineList(), &tmpColorImage, false);

    ImageProcessor::ConvertImage(&tmpColorImage, &m_edgeImage);

    //XXX calculate orientation image

    if (m_useSaltPepperNoise)
    {
        AddSaltPepperNoise();
    }
    if (m_useRandomEdges)
    {
        AddRandomEdges();
    }

    return true;
}

void CVirtualTrackingImageProcessor::UpdateImageType()
{
    m_edgeImage.Set(configuration_imageWidth, configuration_imageHeight, CByteImage::eGrayScale, false);
    if (m_pGradientOrientations != NULL)
    {
        delete[] m_pGradientOrientations;
    }
    m_pGradientOrientations = new float[configuration_imageWidth * configuration_imageHeight];
    if (m_pGradients != NULL)
    {
        for (int i = 0; i < (m_pInputImage->width * m_pInputImage->height); i++)
        {
            delete [] m_pGradients[i];
        }
        delete [] m_pGradients;
    }
    m_pGradients = new float*[m_pInputImage->width * m_pInputImage->height];
    for (int i = 0; i < (m_pInputImage->width * m_pInputImage->height); i++)
    {
        m_pGradients[i] = new float[2];
    }
}

void CVirtualTrackingImageProcessor::SetSaltPepperNoise(bool enabled, float blackToWhiteNoiseFactor, float whiteToBlackNoiseFactor)
{
    m_useSaltPepperNoise = enabled;
    m_blackToWhiteNoiseFactor = blackToWhiteNoiseFactor;
    m_whiteToBlackNoiseFactor = whiteToBlackNoiseFactor;
}

void CVirtualTrackingImageProcessor::SetRandomEdges(bool enabled, unsigned int numberOfRandomEdges)
{
    m_useRandomEdges = enabled;
    m_numberOfRandomEdges = numberOfRandomEdges;
}

void CVirtualTrackingImageProcessor::AddSaltPepperNoise()
{
    unsigned char* curPixel = m_edgeImage.pixels;
    for (int j = 0; j < m_edgeImage.height; ++j)
    {
        for (int i = 0; i < m_edgeImage.width; ++i)
        {
            double r = m_randomGenerator.GetUniformRandomNumber();
            if (*curPixel > 0)
            {
                if (r < m_whiteToBlackNoiseFactor)
                {
                    *curPixel = 0;
                }
            }
            else
            {
                if (r < m_blackToWhiteNoiseFactor)
                {
                    *curPixel = 255;
                }
            }
            curPixel++;
        }
    }
}

void CVirtualTrackingImageProcessor::AddRandomEdges()
{
    for (unsigned int i = 0; i < m_numberOfRandomEdges; ++i)
    {
        SbVec2f p1(m_randomGenerator.GetUniformRandomNumber(0.0, m_edgeImage.width),
                   m_randomGenerator.GetUniformRandomNumber(0.0, m_edgeImage.height));
        SbVec2f p2(m_randomGenerator.GetUniformRandomNumber(0.0, m_edgeImage.width),
                   m_randomGenerator.GetUniformRandomNumber(0.0, m_edgeImage.height));
        PrimitiveDrawer::DrawLine(p1, p2, 255, 255, 255, 1, &m_edgeImage);
    }
}
