#pragma once

#include <Image/ByteImage.h>

class DeviationImageProcessor
{

public:

    DeviationImageProcessor();
    virtual ~DeviationImageProcessor();

    void setInputImage(const CByteImage* pInputImage);
    const CByteImage* getInputImage() const;

    const CByteImage& getOutputImage() const;

    void configureProcessor(int radius, float densityTreshold);

    bool dispatchImageProcessing(int x0, int x1, int y0, int y1);

protected:

    struct RGBPixel
    {
        unsigned char m_R;

        unsigned char m_G;

        unsigned char m_B;
    };

    const CByteImage* m_pInputImage;
    CByteImage m_outputImage;

    int m_radius;
    float m_densityTreshold;
};

