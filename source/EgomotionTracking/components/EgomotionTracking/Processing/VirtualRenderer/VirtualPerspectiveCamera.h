#pragma once

#include <Inventor/fields/SoSFVec2f.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/SbLinear.h>
#include <Inventor/actions/SoGLRenderAction.h>
#include <Inventor/misc/SoState.h>
#include <Inventor/elements/SoProjectionMatrixElement.h>
#include <Inventor/elements/SoViewingMatrixElement.h>
#include <Inventor/elements/SoFocalDistanceElement.h>

#include "VirtualCameraParameters.h"

class VirtualPerspectiveCamera: public SoPerspectiveCamera
{
public:

    VirtualPerspectiveCamera();

    ~VirtualPerspectiveCamera() override;

    void SetCalibration(const CCalibration* pCalibration, float Near, float Far);

    void SetUsingCalibration(bool Enabled);

    bool IsUsingCalibration() const;

protected:

    void GLRender(SoGLRenderAction* pAction) override;

    SbViewVolume getViewVolume(float UseAspectRatio = 0.0f) const override;

    bool m_IsUsingCalibration;

    VirtualCameraParameters* m_pVirtualCameraParameters;
};

