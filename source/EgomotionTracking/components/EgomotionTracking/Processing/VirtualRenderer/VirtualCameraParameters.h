#pragma once

#include <iostream>
#include <Inventor/SbLinear.h>
#include <Calibration/Calibration.h>

class VirtualCameraParameters
{
public:

    VirtualCameraParameters(const CCalibration* pCalibration, const float Near, const float Far);

    const SbMatrix& GetProjectionMatrix() const;

    const SbVec2f& GetImageSize() const;

    const SbVec2f& GetFocalLenght() const;

    const SbVec2f& GetPrincipalPoint() const;

    const SbVec2f& GetFieldOfView() const;

    float GetAspectRatio() const;

    float GetNear() const;

    float GetFar() const;

protected:

    void Update(const CCalibration::CCameraParameters& Parameters, const Mat3d& IntrinsicParametersMatrix);

    SbMatrix m_ProjectionMatrix;

    SbVec2f m_ImageSize;

    SbVec2f m_FocalLenght;

    SbVec2f m_PrincipalPoint;

    SbVec2f m_FieldOfView;

    float m_AspectRatio;

    float m_Near;

    float m_Far;

    Mat3d m_IntrinsicParametersMatrix;
};

