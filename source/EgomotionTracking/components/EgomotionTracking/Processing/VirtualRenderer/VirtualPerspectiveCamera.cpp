#include "VirtualPerspectiveCamera.h"


VirtualPerspectiveCamera::VirtualPerspectiveCamera():
    SoPerspectiveCamera(),
    m_IsUsingCalibration(true),
    m_pVirtualCameraParameters(NULL)
{

}

VirtualPerspectiveCamera::~VirtualPerspectiveCamera()
{
    if (m_pVirtualCameraParameters)
    {
        delete m_pVirtualCameraParameters;

        m_pVirtualCameraParameters = NULL;
    }
}

void VirtualPerspectiveCamera::SetCalibration(const CCalibration* pCalibration, float Near, float Far)
{
    if (m_pVirtualCameraParameters)
    {
        delete m_pVirtualCameraParameters;

        m_pVirtualCameraParameters = NULL;
    }

    m_pVirtualCameraParameters = new VirtualCameraParameters(pCalibration, Near, Far);
}

void VirtualPerspectiveCamera::SetUsingCalibration(bool Enabled)
{
    m_IsUsingCalibration = Enabled;
}

bool VirtualPerspectiveCamera::IsUsingCalibration() const
{
    return m_IsUsingCalibration;
}

SbViewVolume VirtualPerspectiveCamera::getViewVolume(float UseAspectRatio) const
{
    if (m_IsUsingCalibration && m_pVirtualCameraParameters)
    {
        SbViewVolume ViewVolume;

        const SbVec2f& FieldOfView = m_pVirtualCameraParameters->GetFieldOfView();

        ViewVolume.perspective(FieldOfView[1], m_pVirtualCameraParameters->GetAspectRatio(), m_pVirtualCameraParameters->GetNear(), m_pVirtualCameraParameters->GetFar());

        ViewVolume.rotateCamera(orientation.getValue());

        ViewVolume.translateCamera(position.getValue());

        return ViewVolume;
    }
    else
    {
        return SoPerspectiveCamera::getViewVolume(UseAspectRatio);
    }
}

void VirtualPerspectiveCamera::GLRender(SoGLRenderAction* pAction)
{
    SoPerspectiveCamera::GLRender(pAction);

    if (m_IsUsingCalibration && m_pVirtualCameraParameters)
    {
        SoState* pState = pAction->getState();

        SoProjectionMatrixElement::set(pState, this, m_pVirtualCameraParameters->GetProjectionMatrix());

        SbMatrix RigidTransformation;

        RigidTransformation.setTransform(position.getValue(), orientation.getValue(), SbVec3f(1.0f, 1.0f, 1.0f));

        SoViewingMatrixElement::set(pState, this, RigidTransformation.inverse());

        SoFocalDistanceElement::set(pState, this, focalDistance.getValue());
    }
}
