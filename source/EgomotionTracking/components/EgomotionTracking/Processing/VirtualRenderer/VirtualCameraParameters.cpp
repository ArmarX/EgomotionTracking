#include "VirtualCameraParameters.h"

VirtualCameraParameters::VirtualCameraParameters(const CCalibration* pCalibration, const float Near, const float Far):
    m_Near(Near), m_Far(Far)
{
    Mat3d K;

    pCalibration->GetCalibrationMatrix(K);

    Update(pCalibration->GetCameraParameters(), K);

    /*printf("_______________________________\n");
    printf("Camera Parameters\n");
    printf("_______________________________\n");
    printf("width  nx = %i\n",Parameters.width);
    printf("height ny= %i\n",Parameters.height);
    printf("focalLength mx = %f\n",Parameters.focalLength.x);
    printf("focalLength my = %f\n",Parameters.focalLength.y);
    printf("Principal Point mx = %f\n",Parameters.principalPoint.x);
    printf("Principal Point my = %f\n",Parameters.principalPoint.y);
    printf("Distortion Factors [%f][%f][%f][%f]\n",Parameters.distortion[0],Parameters.distortion[1],Parameters.distortion[2],Parameters.distortion[4]);
    printf("_______________________________\n");
    printf("K=\n");
    printf("|[%f][%f][%f]|\n",K.r1,K.r2,K.r3);
    printf("|[%f][%f][%f]|\n",K.r4,K.r5,K.r6);
    printf("|[%f][%f][%f]|\n",K.r7,K.r8,K.r9);
    printf("_______________________________\n");
    printf("R=\n");
    printf("|[%f][%f][%f]|\n",Parameters.rotation.r1,Parameters.rotation.r2,Parameters.rotation.r3);
    printf("|[%f][%f][%f]|\n",Parameters.rotation.r4,Parameters.rotation.r5,Parameters.rotation.r6);
    printf("|[%f][%f][%f]|\n",Parameters.rotation.r7,Parameters.rotation.r8,Parameters.rotation.r9);
    printf("_______________________________\n");
    printf("T=\n");
    printf("|[%f]|\n",Parameters.translation.x);
    printf("|[%f]|\n",Parameters.translation.y);
    printf("|[%f]|\n",Parameters.translation.z);
    printf("_______________________________\n");*/
}

void VirtualCameraParameters::Update(const CCalibration::CCameraParameters& Parameters, const Mat3d& IntrinsicParametersMatrix)
{
    m_IntrinsicParametersMatrix = IntrinsicParametersMatrix;

    m_ImageSize.setValue(Parameters.width, Parameters.height);

    m_FocalLenght.setValue(Parameters.focalLength.x, Parameters.focalLength.y);

    m_PrincipalPoint.setValue(Parameters.principalPoint.x, Parameters.principalPoint.y);

    m_FieldOfView.setValue(atan((m_ImageSize[0] - m_PrincipalPoint[0]) / m_FocalLenght[0]) + atan(m_PrincipalPoint[0] / m_FocalLenght[0]),
                           atan((m_ImageSize[1] - m_PrincipalPoint[1]) / m_FocalLenght[1]) + atan(m_PrincipalPoint[1] / m_FocalLenght[1]));

    //    m_AspectRatio = m_FocalLenght[1] / m_FocalLenght[0];

    m_AspectRatio = (float)(Parameters.width) / (float)(Parameters.height);


    m_ProjectionMatrix[0][0] = (2.0f * m_IntrinsicParametersMatrix.r1 / Parameters.width);

    m_ProjectionMatrix[1][0] = 0.0f;

    m_ProjectionMatrix[2][0] = 0.0f;

    m_ProjectionMatrix[3][0] = 0.0f;

    m_ProjectionMatrix[0][1] = (2.0f * m_IntrinsicParametersMatrix.r2 / Parameters.width);

    m_ProjectionMatrix[1][1] = (2.0f * m_IntrinsicParametersMatrix.r5 / Parameters.height);

    m_ProjectionMatrix[2][1] = 0.0;

    m_ProjectionMatrix[3][1] = 0.0;

    m_ProjectionMatrix[0][2] = -((2.0f * m_IntrinsicParametersMatrix.r3 / Parameters.width) - 1.0f);

    m_ProjectionMatrix[1][2] = ((2.0f * m_IntrinsicParametersMatrix.r6 / Parameters.height) - 1.0f);

    m_ProjectionMatrix[2][2] = -(m_Far + m_Near) / (m_Far - m_Near);

    m_ProjectionMatrix[3][2] = -1.0f;

    m_ProjectionMatrix[0][3] = 0.0f;

    m_ProjectionMatrix[1][3] = 0.0f;

    m_ProjectionMatrix[2][3] = (-2.0f * m_Far * m_Near) / (m_Far - m_Near);

    m_ProjectionMatrix[3][3] = 0.0f;

    m_ProjectionMatrix = m_ProjectionMatrix.transpose();
}

const SbMatrix& VirtualCameraParameters::GetProjectionMatrix() const
{
    return m_ProjectionMatrix;
}

const SbVec2f& VirtualCameraParameters::GetImageSize() const
{
    return m_ImageSize;
}

const SbVec2f& VirtualCameraParameters::GetFocalLenght() const
{
    return m_FocalLenght;
}

const SbVec2f& VirtualCameraParameters::GetPrincipalPoint() const
{
    return m_PrincipalPoint;
}

const SbVec2f& VirtualCameraParameters::GetFieldOfView() const
{
    return m_FieldOfView;
}

float VirtualCameraParameters::GetAspectRatio() const
{
    return m_AspectRatio;
}

float VirtualCameraParameters::GetNear() const
{
    return m_Near;
}

float VirtualCameraParameters::GetFar() const
{
    return m_Far;
}
