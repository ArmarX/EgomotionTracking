#include "OfflineVirtualCameraView.h"

#include <Inventor/nodes/SoCube.h>

OfflineVirtualCameraView::OfflineVirtualCameraView(const int Width, const int Height, const CameraChannel Channel, const float NearClippingPlane, const float FarClippingPlane):
    m_CameraChannel(Channel),
    m_NearClippingPlane(NearClippingPlane),
    m_FarClippingPlane(FarClippingPlane),
    m_pRoot(NULL),
    m_pSceneNodes(NULL),
    m_pStereoCalibration(NULL),
    m_pMonocularCalibration(NULL),
    m_pVirtualPerspectiveCamera(NULL),
    m_pDirectionalLight(NULL),
    m_pOffscreenRenderer(NULL),
    m_pImage(NULL)
{
    m_pRoot = new SoExtSelection;

    m_pRoot->ref();

    m_pDirectionalLight = new SoDirectionalLight;

    m_pRoot->addChild(m_pDirectionalLight);

    m_pVirtualPerspectiveCamera = new VirtualPerspectiveCamera;

    m_pRoot->addChild(m_pVirtualPerspectiveCamera);

    m_pSceneNodes = new SoExtSelection;

    m_pRoot->addChild(m_pSceneNodes);

    m_pOffscreenRenderer = new SoOffscreenRenderer(SbViewportRegion(Width, Height));

    SoGLRenderAction* pGLRenderAction =  m_pOffscreenRenderer->getGLRenderAction();

    pGLRenderAction->setSmoothing(true);

    pGLRenderAction->setNumPasses(3);

    m_pImage = new CByteImage(Width, Height, CByteImage::eRGB24);
}

OfflineVirtualCameraView::~OfflineVirtualCameraView()
{
    ClearNodes();

    m_pRoot->unref();

    if (m_pOffscreenRenderer)
    {
        delete m_pOffscreenRenderer;

        m_pOffscreenRenderer = NULL;
    }

    if (m_pImage)
    {
        delete m_pImage;

        m_pImage = NULL;
    }
}

void OfflineVirtualCameraView::SetBackGroundColor(const unsigned char R, const unsigned char G, const unsigned char B)
{
    m_pOffscreenRenderer->setBackgroundColor(SbColor(float(R) / 255.0f, float(G) / 255.0f, float(B) / 255.0f));
}

bool OfflineVirtualCameraView::LoadStereoCalibration(const char* pCalibrationFile)
{
    if (!pCalibrationFile)
    {
        return false;
    }

    if (m_pStereoCalibration)
    {
        delete m_pStereoCalibration;

        m_pStereoCalibration = NULL;
    }

    m_pStereoCalibration = new CStereoCalibration;

    if (!m_pStereoCalibration->LoadCameraParameters(pCalibrationFile, true))
    {
        delete m_pStereoCalibration;

        m_pStereoCalibration = NULL;

        return false;
    }

    const CCalibration* pCalibration = NULL;

    switch (m_CameraChannel)
    {
        case eLeftCamera:

            pCalibration = m_pStereoCalibration->GetLeftCalibration();

            break;

        case eRightCamera:

            pCalibration = m_pStereoCalibration->GetRightCalibration();

            break;
    }

    m_pVirtualPerspectiveCamera->SetCalibration(pCalibration, m_NearClippingPlane, m_FarClippingPlane);

    const CCalibration::CCameraParameters& CameraParameters = pCalibration->GetCameraParameters();

    m_pOffscreenRenderer->setViewportRegion(SbViewportRegion(CameraParameters.width, CameraParameters.height));

    if (m_pImage)
    {
        delete m_pImage;

        m_pImage = NULL;
    }

    m_pImage = new CByteImage(CameraParameters.width, CameraParameters.height, CByteImage::eRGB24);

    return true;
}

bool OfflineVirtualCameraView::SetStereoCalibration(const CStereoCalibration* pStereoCalibration)
{
    const CCalibration* pCalibration = NULL;

    switch (m_CameraChannel)
    {
        case eLeftCamera:

            pCalibration = pStereoCalibration->GetLeftCalibration();

            break;

        case eRightCamera:

            pCalibration = pStereoCalibration->GetRightCalibration();

            break;
    }

    m_pVirtualPerspectiveCamera->SetCalibration(pCalibration, m_NearClippingPlane, m_FarClippingPlane);

    const CCalibration::CCameraParameters& CameraParameters = pCalibration->GetCameraParameters();

    m_pOffscreenRenderer->setViewportRegion(SbViewportRegion(CameraParameters.width, CameraParameters.height));

    if (m_pImage)
    {
        delete m_pImage;

        m_pImage = NULL;
    }

    m_pImage = new CByteImage(CameraParameters.width, CameraParameters.height, CByteImage::eRGB24);

    return true;
}

bool OfflineVirtualCameraView::AddNode(SoNode* pNode)
{
    if (!pNode)
    {
        return false;
    }

    if (m_pSceneNodes->findChild(pNode) >= 0)
    {
        return false;
    }

    m_pSceneNodes->addChild(pNode);

    return true;
}

void OfflineVirtualCameraView::ClearNodes()
{
    m_pSceneNodes->removeAllChildren();
}

void OfflineVirtualCameraView::SetCameraFrame(const SbMatrix& Frame)
{
    SbVec3f Translation, ScaleFactor;

    SbRotation Rotation, ScaleOrientation;

    Frame.getTransform(Translation, Rotation, ScaleFactor, ScaleOrientation);

    m_pVirtualPerspectiveCamera->position.setValue(Translation);

    m_pVirtualPerspectiveCamera->orientation.setValue(Rotation);
}

void OfflineVirtualCameraView::SetCameraFrame(const SbRotation& Rotation, const SbVec3f& Translation)
{
    m_pVirtualPerspectiveCamera->position.setValue(Translation);

    m_pVirtualPerspectiveCamera->orientation.setValue(Rotation);
}

bool OfflineVirtualCameraView::SetAntiAlias(const bool Smoothing, const int Passes)
{
    if (Smoothing && (Passes < 1))
    {
        return false;
    }

    SoGLRenderAction* pGLRenderAction =  m_pOffscreenRenderer->getGLRenderAction();

    pGLRenderAction->setSmoothing(Smoothing);

    if (Smoothing)
    {
        pGLRenderAction->setNumPasses(Passes);
    }
    else
    {
        pGLRenderAction->setNumPasses(1);
    }

    return true;
}

bool OfflineVirtualCameraView::Render()
{
    if (!m_pOffscreenRenderer->render(m_pRoot))
    {
        return false;
    }

    const SbVec2s& ViewportSizePixels = m_pOffscreenRenderer->getViewportRegion().getViewportSizePixels();

    const int TotalStrides = ViewportSizePixels[1];

    const int StrideSize = ViewportSizePixels[0] * 3;

    unsigned char* pDestination = m_pImage->pixels;

    const unsigned char* pSource = m_pOffscreenRenderer->getBuffer() + StrideSize * (TotalStrides - 1);

    for (int i = 0; i < TotalStrides; ++i, pDestination += StrideSize, pSource -= StrideSize)
    {
        memcpy(pDestination, pSource, StrideSize);
    }

    return true;
}

const CByteImage* OfflineVirtualCameraView::GetVirtualView()
{
    return m_pImage;
}

