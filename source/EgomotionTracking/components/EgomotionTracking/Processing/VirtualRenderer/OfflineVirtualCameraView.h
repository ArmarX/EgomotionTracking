#pragma once

#include "VirtualPerspectiveCamera.h"

#include <qwidget.h>
#include <qapplication.h>

#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/actions/SoBoxHighlightRenderAction.h>
#include <Inventor/nodes/SoExtSelection.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoCamera.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/SoOffscreenRenderer.h>
#include <Inventor/nodes/SoDirectionalLight.h>

#include "Calibration/StereoCalibration.h"
#include "Calibration/Calibration.h"
#include "Image/ByteImage.h"
#include "Image/ImageProcessor.h"

class OfflineVirtualCameraView
{
public:

    enum CameraChannel
    {
        eLeftCamera = 0,
        eRightCamera = 1
    };

    OfflineVirtualCameraView(const int Width, const int Height, const CameraChannel Channel, const float NearClippingPlane, const float FarClippingPlane);

    virtual ~OfflineVirtualCameraView();

    void SetBackGroundColor(const unsigned char R, const unsigned char G, const unsigned char B);

    bool LoadStereoCalibration(const char* pCalibrationFile);

    bool SetStereoCalibration(const CStereoCalibration* pStereoCalibration);

    bool AddNode(SoNode* pNode);

    void ClearNodes();

    void SetCameraFrame(const SbMatrix& Frame);

    void SetCameraFrame(const SbRotation& Rotation, const SbVec3f& Translation);

    bool SetAntiAlias(const bool Smoothing, const int Passes);

    bool Render();

    const CByteImage* GetVirtualView();

protected:

    CameraChannel m_CameraChannel;

    float m_NearClippingPlane;

    float m_FarClippingPlane;

    SoExtSelection* m_pRoot;

    SoExtSelection* m_pSceneNodes;

    CStereoCalibration* m_pStereoCalibration;

    CCalibration* m_pMonocularCalibration;

    VirtualPerspectiveCamera* m_pVirtualPerspectiveCamera;

    SoDirectionalLight* m_pDirectionalLight;

    SoOffscreenRenderer* m_pOffscreenRenderer;

    CByteImage* m_pImage;
};

