/*
 * RenderingUnit.h
 *
 *  Created on: 22.03.2013
 *      Author: abyte
 */

#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/ParametricLineRenderer.h>
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotControlInterface.h>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/CameraConfiguration.h>

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/DynamicElementParticle.h>

class CRenderingUnit
{
public:
    CRenderingUnit();
    virtual ~CRenderingUnit();

    const CRobotControlInterface* GetRobotControlInterface() const
    {
        return m_pRobotControlInterface;
    }

    void SetRobotControlInterface(const CRobotControlInterface* pRobotControlInterface)
    {
        m_pRobotControlInterface = pRobotControlInterface;
    }

    const CParametricLineRenderer& GetParametricLineRenderer() const
    {
        return m_parametricLineRenderer;
    }

    const CParametricLineRenderer* GetParentParametricLineRenderer() const
    {
        return m_pParentParametricLineRenderer;
    }

    void SetParametricLineRenderer(const CParametricLineRenderer& parametricLineRenderer)
    {
        // ahh
        m_parametricLineRenderer = parametricLineRenderer;
        m_pParentParametricLineRenderer = &parametricLineRenderer;
    }

    void UpdateRenderingConfigurationFromParent()
    {
        assert(m_pParentParametricLineRenderer);
        m_parametricLineRenderer.SetConfiguration(m_pParentParametricLineRenderer->GetConfiguration());
    }

    bool RenderParticle(CLocationParticle& locationParticle, bool renderJunctionPoints);

    bool CheckModelPartsAreInView(const CLocationState& locationState, float relativeInViewRatio);

    bool RenderLines(const SbVec3f& cameraPosition, const SbRotation& cameraOrientation, std::vector<CScreenLine>& renderedLines);

    bool CalculateParticleViewingMatrix(CLocationParticle& locationParticle, float* pParticleViewingMatrix);

    void UpdateHeadConfiguration();

    bool RenderDynamicElementParticle(DynamicElementParticle& dynamicElementParticle, DynamicElement* pDynamicElement, int threadId);

    void UpdateCameraPositionFromParent()
    {
        assert(m_pParentParametricLineRenderer);
        m_parametricLineRenderer.SetViewingMatrixGL(m_pParentParametricLineRenderer->GetViewingMatrixGL());
    }

    void UpdateStaticAndEstimatedProjectedPrimitivesFromParent()
    {
        assert(m_pParentParametricLineRenderer);
        m_parametricLineRenderer.SetStaticAndEstimatedProjectedScreenLineList(m_pParentParametricLineRenderer->GetStaticAndEstimatedProjectedScreenLineList());
        m_parametricLineRenderer.SetStaticAndEstimatedProjectedScreenFaceTriangleList(m_pParentParametricLineRenderer->GetStaticAndEstimatedProjectedScreenFaceTriangleList());
    }

protected:
    const CRobotControlInterface* m_pRobotControlInterface;
    CParametricLineRenderer m_parametricLineRenderer;
    const CParametricLineRenderer* m_pParentParametricLineRenderer;
    CHeadConfiguration m_headConfiguration;
};

