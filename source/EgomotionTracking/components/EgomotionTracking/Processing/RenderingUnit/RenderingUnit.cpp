/*
 * RenderingUnit.cpp
 *
 *  Created on: 22.03.2013
 *      Author: abyte
 */

#include "RenderingUnit.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>

#include <EgomotionTracking/components/RVL/Representation/ModelBased/GeometricGraph/Base/ModelMesh.h>

#include <time.h>

using namespace RVL::Representation::ModelBased::GeometricGraph::Base;
using namespace RVL::Mathematics::_3D;

CRenderingUnit::CRenderingUnit()
    : m_pParentParametricLineRenderer(NULL)
{

}

CRenderingUnit::~CRenderingUnit()
{

}

bool CRenderingUnit::RenderParticle(CLocationParticle& locationParticle, bool renderJunctionPoints)
{
    if (m_pRobotControlInterface == NULL)
    {
        COUT_ERROR << "m_pRobotControlInterface == NULL" << std::endl;
        return false;
    }

    locationParticle.GetScreenJunctionPointsWriteable().clear();
    locationParticle.GetScreenLinesWriteable().clear();

    m_headConfiguration.SetNeckError(locationParticle.GetNeckRollError(), locationParticle.GetNeckPitchError(), locationParticle.GetNeckYawError());
    //    m_headConfiguration = m_pRobotControlInterface->GetHeadConfiguration();

    SbVec3f cameraTranslation;
    SbRotation cameraOrientation;

    if (!m_pRobotControlInterface->CalculateCameraPosition(locationParticle, cameraTranslation, cameraOrientation, m_headConfiguration))
    {
        return false;
    }

    SbRotation openGLRot = OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(cameraOrientation);
    SbMatrix cameraFrame = OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(cameraTranslation, openGLRot);
    m_parametricLineRenderer.SetCameraPosition(cameraFrame);

    bool renderingResult = m_parametricLineRenderer.Render(locationParticle.GetScreenLinesWriteable(), locationParticle.GetScreenJunctionPointsWriteable(), renderJunctionPoints);

    if (renderingResult)
    {
        locationParticle.SetLinesRendered(true);
        if (renderJunctionPoints)
        {
            locationParticle.SetJunctionPointsRendered(true);
        }
        return true;
    }
    return false;
}

bool CRenderingUnit::CheckModelPartsAreInView(const CLocationState& locationState, float relativeInViewRatio)
{
    SbVec3f cameraTranslation;
    SbRotation cameraOrientation;
    if (!m_pRobotControlInterface->CalculateCameraPosition(locationState, cameraTranslation, cameraOrientation))
    {
        return false;
    }

    m_parametricLineRenderer.SetCameraPosition(OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(cameraTranslation, OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(cameraOrientation)));
    return m_parametricLineRenderer.CheckModelPartsAreInView(relativeInViewRatio);
}

bool CRenderingUnit::RenderLines(const SbVec3f& cameraPosition, const SbRotation& cameraOrientation, std::vector<CScreenLine>& renderedLines)
{
    m_parametricLineRenderer.SetCameraPosition(OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(cameraPosition, OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(cameraOrientation)));
    return m_parametricLineRenderer.Render(renderedLines);
}

bool CRenderingUnit::CalculateParticleViewingMatrix(CLocationParticle& locationParticle, float* pParticleViewingMatrix)
{
    if (m_pRobotControlInterface == NULL)
    {
        COUT_ERROR << "m_pRobotControlInterface == NULL" << std::endl;
        return false;
    }

    //CHeadConfiguration headConfiguration = m_pRobotControlInterface->GetHeadConfiguration();
    m_headConfiguration.SetNeckError(locationParticle.GetNeckRollError(), locationParticle.GetNeckPitchError(), locationParticle.GetNeckYawError());

    SbVec3f cameraTranslation;
    SbRotation cameraOrientation;

    if (!m_pRobotControlInterface->CalculateCameraPosition(locationParticle, cameraTranslation, cameraOrientation, m_headConfiguration))
    {
        return false;
    }

    m_parametricLineRenderer.SetCameraPosition(OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(cameraTranslation, OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(cameraOrientation)));

    const SbMatrix& particleViewingMatrix = m_parametricLineRenderer.GetViewingMatrixGL();

    for (int rowIndex = 0; rowIndex < 4; ++rowIndex)
    {
        for (int columnIndex = 0; columnIndex < 4; ++columnIndex)
        {
            pParticleViewingMatrix[rowIndex * 4 + columnIndex] = particleViewingMatrix[rowIndex][columnIndex];
        }
    }

    return true;
}

void CRenderingUnit::UpdateHeadConfiguration()
{
    m_headConfiguration = m_pRobotControlInterface->GetHeadConfiguration();
}

bool CRenderingUnit::RenderDynamicElementParticle(DynamicElementParticle& dynamicElementParticle, DynamicElement* pDynamicElement, int threadId)
{
    std::list<CLine> transformedDynamicElementLineList;
    std::list<CFaceTriangle> transformedDynamicElementFaceTriangleList;

    CModelTransformation* modelTransformation = pDynamicElement->getModelTransformation();
    CModelTransformation* visualizationModelTransformation = pDynamicElement->getModelTransformation(threadId);

    /* Update model used for filtering. */
    if (modelTransformation->GetTransformationType() == CModelTransformation::eTranslation)
    {
        static_cast<CModelTranslation*>(modelTransformation)->SetParametricNormalizedTranslation(dynamicElementParticle.getAlpha(), -1);
    }
    else if (modelTransformation->GetTransformationType() == CModelTransformation::eRotation)
    {
        static_cast<CModelRotation*>(modelTransformation)->SetParametricNormalizedRotation(dynamicElementParticle.getAlpha(), -1);
    }

    /* Update models used for rendering purposes. */
    if (visualizationModelTransformation->GetTransformationType() == CModelTransformation::eTranslation)
    {
        static_cast<CModelTranslation*>(visualizationModelTransformation)->SetParametricNormalizedTranslation(dynamicElementParticle.getAlpha(), -1);
    }
    else if (visualizationModelTransformation->GetTransformationType() == CModelTransformation::eRotation)
    {
        static_cast<CModelRotation*>(visualizationModelTransformation)->SetParametricNormalizedRotation(dynamicElementParticle.getAlpha(), -1);
    }

    const std::map<int, CModelMesh*>& modelMeshes = modelTransformation->GetMeshes();

    for (std::map<int, CModelMesh*>::const_iterator modelMeshesIterator = modelMeshes.begin(); modelMeshesIterator != modelMeshes.end(); ++modelMeshesIterator)
    {
        const std::map<int, CModelEdge*>& modelEdges = modelMeshesIterator->second->GetEdges();

        for (std::map<int, CModelEdge*>::const_iterator modelEdgesIterator = modelEdges.begin(); modelEdgesIterator != modelEdges.end(); ++modelEdgesIterator)
        {
            if (modelEdgesIterator->second->GetVisibility(false) == CModelElement::eStaticVisible)
            {
                CVector3D pointA, pointB;
                modelEdgesIterator->second->GetDynamicEndPoints(pointA, pointB);
                SbVec3f p1(pointA.GetX(), pointA.GetY(), pointA.GetZ());
                SbVec3f p2(pointB.GetX(), pointB.GetY(), pointB.GetZ());
                CLine line(p1, p2);
                line.m_pModelEdgeReference = modelEdgesIterator->second;
                transformedDynamicElementLineList.push_back(line);
            }
        }

        const std::map<int, CModelFace*>& modelFaces = modelMeshesIterator->second->GetFaces();

        for (std::map<int, CModelFace*>::const_iterator modelFacesIterator = modelFaces.begin(); modelFacesIterator != modelFaces.end(); ++modelFacesIterator)
        {
            if (modelFacesIterator->second->GetVisibility(false) == CModelElement::eStaticVisible)
            {
                RVL::Mathematics::_3D::CVector3D pointA, pointB, pointC;
                modelFacesIterator->second->GetDynamicEndPoints(pointA, pointB, pointC);
                SbVec3f p1(pointA.GetX(), pointA.GetY(), pointA.GetZ());
                SbVec3f p2(pointB.GetX(), pointB.GetY(), pointB.GetZ());
                SbVec3f p3(pointC.GetX(), pointC.GetY(), pointC.GetZ());
                CFaceTriangle faceTriangle(p1, p2, p3);
                faceTriangle.m_pModelFaceReference = modelFacesIterator->second;
                transformedDynamicElementFaceTriangleList.push_back(faceTriangle);
            }
        }
    }

    const DynamicElement::FrontalFaceVertices& frontalFaceVertices = pDynamicElement->getFrontalFaceVertices();
    CVector3D pointA = frontalFaceVertices.vertexA->GetDynamicPoint();
    CVector3D pointB = frontalFaceVertices.vertexB->GetDynamicPoint();
    CVector3D pointC = frontalFaceVertices.vertexC->GetDynamicPoint();
    CVector3D pointD = frontalFaceVertices.vertexD->GetDynamicPoint();
    SbVec3f pA(pointA.GetX(), pointA.GetY(), pointA.GetZ());
    SbVec3f pB(pointB.GetX(), pointB.GetY(), pointB.GetZ());
    SbVec3f pC(pointC.GetX(), pointC.GetY(), pointC.GetZ());
    SbVec3f pD(pointD.GetX(), pointD.GetY(), pointD.GetZ());

    return m_parametricLineRenderer.RenderDynamicElementParticle(dynamicElementParticle.GetScreenLinesWriteable(), transformedDynamicElementLineList,
            transformedDynamicElementFaceTriangleList, dynamicElementParticle.getFrontalFacePoints2DWriteable(),
            pA, pB, pC, pD);
}
