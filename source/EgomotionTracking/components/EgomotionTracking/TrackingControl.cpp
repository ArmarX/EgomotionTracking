/*
 * TrackingControl.cpp
 *
 *  Created on: 13.03.2013
 *      Author: abyte
 */

#include "TrackingControl.h"

#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotPlatformTransformation.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>

#include <EgomotionTracking/components/EgomotionTracking/Tests/TestCUDAParametricLineRenderer.h>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/DebuggingPrimitives.h>

#include <Image/ImageProcessor.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/PrimitiveDrawer.h>

#include <EgomotionTracking/components/EgomotionTracking/Models/ObservationModel/JunctionPointLineModelObservationModel.h>

#include <X11/Xlib.h>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/Profiling.h>

CTrackingControl::CTrackingControl(int numPSLocationEstimatorParticles, int maxTrackingParticles, int maxDynamicElementParticles) :
    m_trackingMode(eEvaluateObservationModel),
    m_pLocationRegion(NULL),
    m_pMasterLocationEstimator(NULL),
    m_pRobotControl(NULL),
    m_pLocationStatePredictor(NULL),
    m_pNeckErrorUncertaintyModel(NULL),
    m_pUncertaintyFactorCalculatorLocationState(NULL),
    m_pUncertaintyFactorCalculatorNeckError(NULL),
    m_pTrackingImageProcessor(NULL),
    m_pTrackingObservationModel(NULL),
    m_pBaseParametricLineRenderer(NULL),
    m_numberOfActiveParticles(0),
    m_cycleTrackingStatusEstimation(false),
    m_maxTrackingParticles(maxTrackingParticles),
    m_pVirtualRendererImage(NULL),
    m_DESECameraFrameUpdateRequired(false)
{
    /* Needed to use virtual rendering on multiple threads. */
    XInitThreads();

    /* Initialize SoQt to use virtual rendering. */
    SoQt::init("Test");

    int numberOfCPUCores = EVP::Threading::CThreadPool::GetAvailableLogicalCPUCores();

    m_pThreadPool = new CEgomotionTrackingThreadPool(numberOfCPUCores);

    COUT_INFO << "Started Threadpool with " << numberOfCPUCores << " Threads" << std::endl;

    m_pEgomotionTrackingPF = new CEgomotionTrackingPF(m_maxTrackingParticles, m_pThreadPool);
    COUT_INFO << "Started EgomotionTrackingPF" << std::endl;

    m_pParticleSwarmLocationEstimator = new CParticleSwarmLocationEstimator(numPSLocationEstimatorParticles, m_pThreadPool);
    COUT_INFO << "Started ParticleSwarmLocationEstimator" << std::endl;

    m_pDynamicElementStatusEstimationControl = new DynamicElementStatusEstimationControl(maxDynamicElementParticles, m_pThreadPool);
    COUT_INFO << "Started DynamicElementStatusEstimationControl" << std::endl;

    m_autoPSReinitialisation.enabled = false;
    SetCameraSelectionMode(eUseLeftCamera);
}

CTrackingControl::~CTrackingControl()
{
    if (m_pDynamicElementStatusEstimationControl)
    {
        delete m_pDynamicElementStatusEstimationControl;
    }
    if (m_pParticleSwarmLocationEstimator)
    {
        delete m_pParticleSwarmLocationEstimator;
    }
    if (m_pEgomotionTrackingPF)
    {
        delete m_pEgomotionTrackingPF;
    }
    if (m_pThreadPool)
    {
        delete m_pThreadPool;
    }
    if (m_pOfflineVirtualCameraView)
    {
        delete m_pOfflineVirtualCameraView;
    }
}

const CLocationRegion* CTrackingControl::GetLocationRegion() const
{
    return m_pLocationRegion;
}

void CTrackingControl::SetLocationRegion(const CLocationRegion* pLocationRegion)
{
    m_pLocationRegion = pLocationRegion;
    UpdateLinks();
}

const CLocationEstimator* CTrackingControl::GetMasterLocationEstimator() const
{
    return m_pMasterLocationEstimator;
}

void CTrackingControl::SetMasterLocationEstimator(CLocationEstimator* pMasterLocationEstimator)
{
    m_pMasterLocationEstimator = pMasterLocationEstimator;
    UpdateLinks();
}

const CRobotControlInterface* CTrackingControl::GetRobotControl() const
{
    return m_pRobotControl;
}

bool CTrackingControl::DispatchLocalisation()
{
    if (m_pRobotControl == NULL)
    {
        COUT_ERROR << "DispatchLocalisation() - ERROR: m_pRobotControl == NULL" << std::endl;
        m_estimationIsValid = false;
        return false;
    }

    if (m_pTrackingImageProcessor == NULL)
    {
        COUT_ERROR << "DispatchLocalisation() - ERROR: m_pTrackingImageProcessor == NULL" << std::endl;
        m_estimationIsValid = false;
        return false;
    }

    if (m_cameraSelection.mode == eAlternateCameras)
    {
        if (m_cameraSelection.currentAlternatingCount >= m_cameraSelection.alternatingInterval)
        {
            m_cameraSelection.currentAlternatingCount = 0;
            if (m_cameraSelection.currentAlternatingCamera == CRobotControlInterface::eLeftCamera)
            {
                m_cameraSelection.currentAlternatingCamera = CRobotControlInterface::eRightCamera;
            }
            else
            {
                m_cameraSelection.currentAlternatingCamera = CRobotControlInterface::eLeftCamera;
            }
            SetCamera(m_cameraSelection.currentAlternatingCamera);
        }
        m_cameraSelection.currentAlternatingCount++;
    }

    if (!m_pRobotControl->DispatchRobot())
    {
        COUT_ERROR << "m_pRobotControl->DispatchRobot() failed" << std::endl;
        m_estimationIsValid = false;
        return false;
    }

    m_pTrackingImageProcessor->SetProcessingOption(CTrackingImageProcessorInterface::eEdgeImageOption, true);
    m_pTrackingImageProcessor->SetProcessingOption(CTrackingImageProcessorInterface::eGradientOrientationOption, true);
    m_pTrackingImageProcessor->SetInputImage(&m_pRobotControl->GetCameraImage());

    m_pTrackingImageProcessor->DispatchImageProcessing();

    if (m_pGroundTruthLocationEstimator != NULL)
    {
        if (!m_pGroundTruthLocationEstimator->DispatchLocalisation())
        {
            COUT_ERROR << "m_pGroundTruthLocationEstimator->DispatchLocalisation() failed" << std::endl;
        }
    }

    if (m_pTrackingObservationModel == NULL)
    {
        COUT_ERROR << "DispatchLocalisation() - ERROR: m_pTrackingObservationModel == NULL" << std::endl;
        return false;
    }
    m_pTrackingObservationModel->SetMeasurement(&m_pTrackingImageProcessor->GetEdgeImage(), m_pTrackingImageProcessor->GetGradientOrientations(), m_pTrackingImageProcessor->GetGradients());

    if (m_lineRegistrationVisualizationMode == eDrawLinesInEdgeImage)
    {
        m_pTrackingObservationModel->SetInputVisualizationImageBackground(&m_pTrackingImageProcessor->GetEdgeImage());
        m_pPSLocationEstimatorObservationModel->SetInputVisualizationImageBackground(&m_pTrackingImageProcessor->GetEdgeImage());
        m_pDynamicElementStatusEstimationObservationModel->SetInputVisualizationImageBackground(&m_pTrackingImageProcessor->GetEdgeImage());
    }
    else if (m_lineRegistrationVisualizationMode == eDrawLinesInInputImage)
    {
        m_pTrackingObservationModel->SetInputVisualizationImageBackground(m_pTrackingImageProcessor->GetInputImage());
        m_pPSLocationEstimatorObservationModel->SetInputVisualizationImageBackground(m_pTrackingImageProcessor->GetInputImage());
        m_pDynamicElementStatusEstimationObservationModel->SetInputVisualizationImageBackground(m_pTrackingImageProcessor->GetInputImage());
    }

    if (m_trackingMode == eEvaluateObservationModel)
    {
        return true;
    }
    else if (m_trackingMode == eEgomotionTracking || (m_trackingMode == eDynamicElementStatusEstimation && m_cycleTrackingStatusEstimation == true))
    {
        CRobotControlState state;
        state = m_pRobotControl->GetRobotControlState();

        if (m_numberOfParticlesChanged)
        {
            m_pEgomotionTrackingPF->SetNumberOfActiveParticles(static_cast<int>(m_numberOfActiveParticles));
            m_numberOfParticlesChanged = false;
        }
        m_pRobotControl->ReleaseRobotMovementLock();

        m_pEgomotionTrackingPF->SetRobotControl(m_pRobotControl);
        m_outputEstimatedImportanceWeight = m_pEgomotionTrackingPF->Filter(1.0);
        m_outputNeff = m_pEgomotionTrackingPF->GetNEff();
        m_outputScatteringEllipseArea = m_pEgomotionTrackingPF->GetPlaneScatteringDirections().GetEllipseArea();
        m_locationEstimate = m_pEgomotionTrackingPF->GetLocationEstimate();
        m_outputScatteringDeterminant = Math3d::Det(m_pEgomotionTrackingPF->GetWeightedScatteringMatrix());
        CalculateOutputMahalanobisDistance();

        if (m_autoPSReinitialisation.enabled)
        {
            if (m_outputScatteringEllipseArea > m_autoPSReinitialisation.scatteringAreaThresholdLostLocation)
            {
                m_pEgomotionTrackingPF->SetMeasurementUpdateActive(false);
                m_autoPSReinitialisation.reinitialisationIsRequired = true;
            }
            if (m_autoPSReinitialisation.reinitialisationIsRequired)
            {
                /* Check at least 10% of model lines are visible in 90% of the particles */
                int inViewCount = 0;
                const std::vector<CLocationParticle>& particles = m_pEgomotionTrackingPF->GetParticles();
                for (std::vector<CLocationParticle>::const_iterator particleIterator = particles.begin(); particleIterator != particles.end(); ++particleIterator)
                {
                    if (particleIterator->GetActive())
                    {
                        /* check at least 10%  of Model lines are (estimated) in view */
                        if (m_baseRenderingUnit.CheckModelPartsAreInView(*particleIterator, 0.10))
                        {
                            ++inViewCount;
                        }
                    }
                }
                if (inViewCount > 0.9 * m_pEgomotionTrackingPF->GetNumberOfActiveParticles())
                {
                    if (m_pRobotControl->LockRobotMovement())
                    {
                        const float enlargementfactor = pow(m_autoPSReinitialisation.enlargementFactorPerTry, m_autoPSReinitialisation.currentTryCount);
                        ReinitializeParticleSwarmLocationEstimatorParticlesInTrackingScatteringEllipse(enlargementfactor);
                        ++m_autoPSReinitialisation.currentTryCount;
                        m_trackingMode = eDispatchParticleSwarmLocationEstimator;
                    }
                }
            }
        }

        m_estimationIsValid = true;
        m_DESECameraFrameUpdateRequired = true;
    }
    else if (m_trackingMode == eDispatchParticleSwarmLocationEstimator)
    {
        if (m_pPSLocationEstimatorObservationModel == NULL)
        {
            COUT_ERROR << "m_pParticleSwarmLocationEstimator == NULL" << std::endl;
            return false;
        }

        if (m_pRobotControl->LockRobotMovement())
        {
            m_pPSLocationEstimatorObservationModel->SetMeasurement(&m_pTrackingImageProcessor->GetEdgeImage(), m_pTrackingImageProcessor->GetGradientOrientations(), m_pTrackingImageProcessor->GetGradients());
            m_pParticleSwarmLocationEstimator->DispatchLocalisation();
            m_locationEstimate = m_pParticleSwarmLocationEstimator->GetLocationEstimate();
            m_outputEstimatedImportanceWeight = m_pParticleSwarmLocationEstimator->GetEstimationWeight();
            m_outputNeff = 0.0;
            m_outputScatteringEllipseArea = m_pParticleSwarmLocationEstimator->GetPlaneScatteringDirections().GetEllipseArea();
            m_outputEstimationToGTMahalanobisDistance = 0.0;
            m_outputScatteringDeterminant = 0.0;

            if (m_autoPSReinitialisation.enabled)
            {
                ++m_autoPSReinitialisation.currentPSStepCount;
                if (m_outputScatteringEllipseArea < m_autoPSReinitialisation.scatteringAreaThresholdInitialisation &&
                    m_outputEstimatedImportanceWeight >= m_autoPSReinitialisation.bestWeightThresholdInitialisation)
                {
                    ReinitializeTrackingParticlesWithPSLocationEstimator();
                    m_pRobotControl->ReleaseRobotMovementLock();
                    m_autoPSReinitialisation.reinitialisationIsRequired = false;
                    m_trackingMode = eEgomotionTracking;
                    m_pEgomotionTrackingPF->SetMeasurementUpdateActive(true);
                    m_autoPSReinitialisation.currentPSStepCount = 0;
                    m_autoPSReinitialisation.currentTryCount = 0;
                }
                if (m_autoPSReinitialisation.currentPSStepCount > m_autoPSReinitialisation.maxPSStepsPerTry)
                {
                    m_autoPSReinitialisation.currentPSStepCount = 0;
                    m_trackingMode = eEgomotionTracking;
                    if (m_autoPSReinitialisation.currentTryCount >= m_autoPSReinitialisation.maxTries)
                    {
                        COUT_INFO << "REINITIALISATION FAILED!!!" << std::endl;
                        m_autoPSReinitialisation.enabled = false;
                    }
                }
            }
        }
        m_estimationIsValid = true;
    }
    if (m_trackingMode == eDynamicElementStatusEstimation || (m_trackingMode == eEgomotionTracking && m_cycleTrackingStatusEstimation == true))
    {
        const CByteImage& cameraImage = m_pRobotControl->GetCameraImage();
        m_pDynamicElementStatusEstimationControl->setCameraImage(&cameraImage);

        m_pDynamicElementStatusEstimationObservationModel->SetMeasurement(&m_pTrackingImageProcessor->GetEdgeImage(), m_pTrackingImageProcessor->GetGradientOrientations(),
                m_pTrackingImageProcessor->GetGradients());

        CLocationParticle locationParticle(GetLocationEstimate());
        float neckErrorRoll, neckErrorPitch, neckErrorYaw;
        m_pEgomotionTrackingPF->GetNeckRPYErrorEstimate(neckErrorRoll, neckErrorPitch, neckErrorYaw);
        locationParticle.SetNeckRollError(neckErrorRoll);
        locationParticle.SetNeckPitchError(neckErrorPitch);
        locationParticle.SetNeckYawError(neckErrorYaw);

        m_pDynamicElementStatusEstimationControl->setLocationEstimate(locationParticle);
        m_pDynamicElementStatusEstimationControl->setLocationEstimateIsValid(true);

        m_pDynamicElementStatusEstimationControl->setCameraFrameUpdateRequired(true);

        m_pDynamicElementStatusEstimationControl->dispatch();

        m_DESECameraFrameUpdateRequired = false;
    }

    return true;
}

void CTrackingControl::SetRobotControl(CRobotControlInterface* pRobotControl)
{
    m_pRobotControl = pRobotControl;
    UpdateLinks();
}

const CLocationStatePredictorInterface* CTrackingControl::GetLocationStatePredictor() const
{
    return m_pLocationStatePredictor;
}

void CTrackingControl::SetLocationStatePredictor(CLocationStatePredictorInterface* pLocationStatePredictor)
{
    m_pLocationStatePredictor = pLocationStatePredictor;
    UpdateLinks();
}

bool CTrackingControl::ReinitializeTrackingParticlesWithMasterLocationEstimator()
{
    if (m_pMasterLocationEstimator == NULL)
    {
        std::cout << "CTrackingControl::DispatchLocalisation() - ERROR: m_pMasterLocationEstimator == NULL" << std::endl;
        return false;
    }

    m_pMasterLocationEstimator->DispatchLocalisation();
    m_locationEstimate = m_pMasterLocationEstimator->GetLocationEstimate();
    m_pEgomotionTrackingPF->InitializeFilter(m_locationEstimate);
    return true;
}

bool CTrackingControl::ReinitializeTrackingParticlesWithPSLocationEstimator()
{
    m_locationEstimate = m_pParticleSwarmLocationEstimator->GetLocationEstimate();
    m_pEgomotionTrackingPF->InitializeFilter(m_pParticleSwarmLocationEstimator->GetBestNParticles(m_maxTrackingParticles));
    return true;
}

bool CTrackingControl::ReinitializeParticleSwarmLocationEstimatorParticlesInTrackingScatteringEllipse(float enlargementFactor)
{
    CLocationState minState, maxState;
    m_pEgomotionTrackingPF->CalculateBoundingRect(minState, maxState);
    m_pParticleSwarmLocationEstimator->InitializeParticlesInRegion(m_pEgomotionTrackingPF->GetPlaneScatteringDirections(), minState.GetAlpha(), maxState.GetAlpha(), enlargementFactor);
    return true;
}


void CTrackingControl::GetParticles(std::vector<CLocationParticle>& activeParticles)
{
    m_pEgomotionTrackingPF->GetParticles(activeParticles);
}

CLocationEstimator* CTrackingControl::GetGroundTruthLocationEstimator() const
{
    return m_pGroundTruthLocationEstimator;
}

void CTrackingControl::SetGroundTruthLocationEstimator(CLocationEstimator* pGroundTruthLocationEstimator)
{
    m_pGroundTruthLocationEstimator = pGroundTruthLocationEstimator;
    UpdateLinks();
}

const CTrackingImageProcessorInterface* CTrackingControl::GetTrackingImageProcessor() const
{
    return m_pTrackingImageProcessor;
}

void CTrackingControl::SetTrackingImageProcessor(CTrackingImageProcessorInterface* pTrackingImageProcessor)
{
    m_pTrackingImageProcessor = pTrackingImageProcessor;
    UpdateLinks();
}

void CTrackingControl::SetParametricLineRenderer(CParametricLineRenderer& m_parametricLineRenderer)
{
    m_baseRenderingUnit.SetParametricLineRenderer(m_parametricLineRenderer);
    m_pBaseParametricLineRenderer = &m_parametricLineRenderer;
    UpdateLinks();
}

void CTrackingControl::SetNumberOfActiveParticles(unsigned int nActiveParticles)
{
    m_numberOfParticlesChanged = true;
    if (static_cast<int>(nActiveParticles) < m_maxTrackingParticles)
    {
        m_numberOfActiveParticles = nActiveParticles;
    }
    else
    {
        m_numberOfActiveParticles = m_maxTrackingParticles;
    }
}

unsigned int CTrackingControl::GetNumberOfActiveParticles() const
{
    return m_numberOfActiveParticles;
}

CEgomotionTrackingPF* CTrackingControl::GetTrackingPF() const
{
    return m_pEgomotionTrackingPF;
}

CParticleSwarmLocationEstimator* CTrackingControl::GetParticleSwarmLocationEstimator()
{
    return m_pParticleSwarmLocationEstimator;
}

DynamicElementStatusEstimationControl* CTrackingControl::GetDESEControl()
{
    return m_pDynamicElementStatusEstimationControl;
}

float CTrackingControl::GetEstimatedImportanceWeight() const
{
    return m_outputEstimatedImportanceWeight;
}

float CTrackingControl::GetNeff() const
{
    return m_outputNeff;
}

float CTrackingControl::GetEstimationPlaneErrorLength() const
{
    if (!m_pGroundTruthLocationEstimator)
    {
        return 0.0;
    }
    return m_locationEstimate.CalculateDistance2D(m_pGroundTruthLocationEstimator->GetLocationEstimate());
}

float CTrackingControl::GetScatteringEllipseArea() const
{
    return m_outputScatteringEllipseArea;
}

float CTrackingControl::GetEstimationToGTMahalanobisDistance() const
{
    return m_outputEstimationToGTMahalanobisDistance;
}

float CTrackingControl::GetScatteringDeterminant() const
{
    return m_outputScatteringDeterminant;
}

void CTrackingControl::UpdateLinks()
{

    if (m_pTrackingImageProcessor != NULL && m_pRobotControl != NULL)
    {
        m_pTrackingImageProcessor->SetInputImage(&m_pRobotControl->GetCameraImage());
    }
    if (m_pRobotControl)
    {
        m_baseRenderingUnit.SetRobotControlInterface(m_pRobotControl);
        if (m_cameraSelection.mode == eUseLeftCamera || m_cameraSelection.mode == eAlternateCameras)
        {
            SetCamera(CRobotControlInterface::eLeftCamera);
        }
        else
        {
            SetCamera(CRobotControlInterface::eRightCamera);
        }
    }
    if (m_pLocationStatePredictor)
    {
        m_pEgomotionTrackingPF->SetLocationStatePredictor(m_pLocationStatePredictor);
    }
    if (m_pNeckErrorUncertaintyModel)
    {
        if (m_pEgomotionTrackingPF)
        {
            m_pEgomotionTrackingPF->SetNeckErrorUncertaintyModel(m_pNeckErrorUncertaintyModel);
        }
        if (m_pParticleSwarmLocationEstimator)
        {
            m_pParticleSwarmLocationEstimator->SetNeckErrorUncertaintyModel(m_pNeckErrorUncertaintyModel);
        }
    }
    if (m_pTrackingObservationModel)
    {
        m_pEgomotionTrackingPF->SetObservationModel(m_pTrackingObservationModel);
    }
    if (m_pPSLocationEstimatorObservationModel)
    {
        m_pParticleSwarmLocationEstimator->SetObservationModel(m_pPSLocationEstimatorObservationModel);
    }
    if (m_pDynamicElementStatusUncertaintyModel)
    {
        m_pDynamicElementStatusEstimationControl->setDynamicElementStatusUncertaintyModel(m_pDynamicElementStatusUncertaintyModel);
    }
    if (m_pDynamicElementStatusEstimationObservationModel)
    {
        m_pDynamicElementStatusEstimationControl->setObservationModel(m_pDynamicElementStatusEstimationObservationModel);
    }

    m_pDynamicElementStatusEstimationControl->setParametricLineRenderer(m_pBaseParametricLineRenderer);

    m_pEgomotionTrackingPF->SetRenderingUnit(&m_baseRenderingUnit);
    m_pParticleSwarmLocationEstimator->SetRenderingUnit(&m_baseRenderingUnit);
    m_pDynamicElementStatusEstimationControl->setRenderingUnit(&m_baseRenderingUnit);

    m_pEgomotionTrackingPF->SetUncertaintyFactorCalculatorLocationState(m_pUncertaintyFactorCalculatorLocationState);
    m_pEgomotionTrackingPF->SetUncertaintyFactorCalculatorNeckError(m_pUncertaintyFactorCalculatorNeckError);

    m_pDynamicElementStatusEstimationControl->setRobotControl(m_pRobotControl);
}

void CTrackingControl::SetCamera(CRobotControlInterface::CCameraID id)
{
    if (m_pRobotControl)
    {
        m_pRobotControl->SetCamera(id);
        if (m_pBaseParametricLineRenderer)
        {
            m_pBaseParametricLineRenderer->SetProjectionMatrixGl(m_pRobotControl->GetCameraProjectionMatrixGL());
        }
    }
}

void CTrackingControl::CalculateOutputMahalanobisDistance()
{
    if (m_pEgomotionTrackingPF && m_pGroundTruthLocationEstimator)
    {
        Mat3d scatteringMat = m_pEgomotionTrackingPF->GetWeightedScatteringMatrix();
        Vec3d meanVec = m_pEgomotionTrackingPF->GetWeightedMean();
        const CLocationState& gtLocationEstimate = m_pGroundTruthLocationEstimator->GetLocationEstimate();
        Vec3d gtVec;
        gtVec.x = gtLocationEstimate.GetX();
        gtVec.y = gtLocationEstimate.GetY();
        gtVec.z = gtLocationEstimate.GetAlpha();
        m_outputEstimationToGTMahalanobisDistance = MathIVT::CalculateMahalanobisDistance(meanVec, gtVec, scatteringMat);
    }
    else
    {
        m_outputEstimationToGTMahalanobisDistance = 0.0;
    }
}



CTrackingControl::CTrackingMode CTrackingControl::GetTrackingMode() const
{
    return m_trackingMode;
}

void CTrackingControl::SetTrackingMode(CTrackingMode trackingMode)
{
    m_trackingMode = trackingMode;
}

CObservationModelInterface* CTrackingControl::GetTrackingObservationModel() const
{
    return m_pTrackingObservationModel;
}

void CTrackingControl::SetTrackingObservationModel(CObservationModelInterface* pTrackingObservationModel)
{
    m_pTrackingObservationModel = pTrackingObservationModel;
    UpdateLinks();
}

void CTrackingControl::SetDynamicElementStatusEstimationObservationModel(CObservationModelInterface* pDynamicElementStatusEstimationObservationModel)
{
    m_pDynamicElementStatusEstimationObservationModel = pDynamicElementStatusEstimationObservationModel;
    UpdateLinks();
}

const CNeckErrorUncertaintyModel* CTrackingControl::GetNeckErrorUncertaintyModel() const
{
    return m_pNeckErrorUncertaintyModel;
}

void CTrackingControl::SetNeckErrorUncertaintyModel(CNeckErrorUncertaintyModel* pNeckErrorUncertaintyModel)
{
    m_pNeckErrorUncertaintyModel = pNeckErrorUncertaintyModel;
    UpdateLinks();
}

void CTrackingControl::SetDynamicElementStatusUncertaintyModel(DynamicElementStatusUncertaintyModel* pDynamicElementStatusUncertaintyModel)
{
    m_pDynamicElementStatusUncertaintyModel = pDynamicElementStatusUncertaintyModel;
    UpdateLinks();
}

void CTrackingControl::SetParticleSwarmLocationEstimatorObservationModel(CObservationModelInterface* pPSLocationEstimatorObservationModel)
{
    m_pPSLocationEstimatorObservationModel = pPSLocationEstimatorObservationModel;
    UpdateLinks();
}

void CTrackingControl::SetUncertaintyFactorCalculatorLocationState(CUncertaintyModelScatteringFactorCalculator* pUncertaintyFactorCalculator)
{
    m_pUncertaintyFactorCalculatorLocationState = pUncertaintyFactorCalculator;
    UpdateLinks();
}

void CTrackingControl::SetUncertaintyFactorCalculatorNeckError(CUncertaintyModelScatteringFactorCalculator* pUncertaintyFactorCalculator)
{
    m_pUncertaintyFactorCalculatorNeckError = pUncertaintyFactorCalculator;
    UpdateLinks();
}

EVP::Threading::CThreadPool* CTrackingControl::GetThreadPool() const
{
    return m_pThreadPool;
}

void CTrackingControl::SetThreadPool(CEgomotionTrackingThreadPool* pThreadPool)
{
    m_pThreadPool = pThreadPool;
}

void CTrackingControl::SetAutoPSReinitialisation(bool enabled, float scatteringAreaThresholdInitialisation, float scatteringAreaThresholdLostLocation, float bestWeightThresholdInitialisation, int maxTries, int maxPSStepsPerTry, float enlargementFactorPerTry)
{
    m_autoPSReinitialisation.enabled = enabled;

    m_autoPSReinitialisation.scatteringAreaThresholdInitialisation = scatteringAreaThresholdInitialisation;
    m_autoPSReinitialisation.scatteringAreaThresholdLostLocation = scatteringAreaThresholdLostLocation;
    m_autoPSReinitialisation.bestWeightThresholdInitialisation = bestWeightThresholdInitialisation;
    m_autoPSReinitialisation.maxTries = maxTries;
    m_autoPSReinitialisation.maxPSStepsPerTry = maxPSStepsPerTry;
    m_autoPSReinitialisation.enlargementFactorPerTry = enlargementFactorPerTry;

    if (m_pRobotControl)
    {
        m_pRobotControl->ReleaseRobotMovementLock();
    }
    m_autoPSReinitialisation.reinitialisationIsRequired = false;
    m_autoPSReinitialisation.currentPSStepCount = 0;
    m_autoPSReinitialisation.currentTryCount = 0;
}

void CTrackingControl::SetCameraSelectionMode(CTrackingControl::CCameraSelectionMode mode, int alternatingInterval)
{
    m_cameraSelection.mode = mode;
    m_cameraSelection.alternatingInterval = alternatingInterval;
    m_cameraSelection.currentAlternatingCamera = CRobotControlInterface::eLeftCamera;
    m_cameraSelection.currentAlternatingCount = 0;

    UpdateLinks();
}

CRenderingUnit& CTrackingControl::GetBaseRenderingUnit()
{
    return m_baseRenderingUnit;
}

void CTrackingControl::SetScatteringReduction(bool enabled)
{
    m_pEgomotionTrackingPF->SetScatteringReduction(enabled);
}

void CTrackingControl::SetLineRegistrationVisualizationMode(CTrackingControl::LineRegistrationVisualizationMode mode)
{
    m_lineRegistrationVisualizationMode = mode;
}

bool CTrackingControl::GetNeckRPYErrorEstimate(float& neckRollError, float& neckPitchError, float& neckYawError) const
{
    switch (m_trackingMode)
    {
        case eEvaluateObservationModel:
            return false;
            break;
        case eEgomotionTracking:
            return m_pEgomotionTrackingPF->GetNeckRPYErrorEstimate(neckRollError, neckPitchError, neckYawError);
            break;
        case eDispatchParticleSwarmLocationEstimator:
            return m_pParticleSwarmLocationEstimator->GetNeckRPYErrorEstimate(neckRollError, neckPitchError, neckYawError);
            break;
        case eDynamicElementStatusEstimation:
            return false;
            break;
    }
    return false;
}

void CTrackingControl::InitializeDynamicElements()
{
    m_pDynamicElementStatusEstimationControl->initializeDynamicElements(m_pThreadPool->GetTotalThreads());
}

void CTrackingControl::setCycleTrackingStatusEstimation(bool cycleTrackingStatusEstimation)
{
    m_cycleTrackingStatusEstimation = cycleTrackingStatusEstimation;
}

unsigned int CTrackingControl::getNumberOfThreads() const
{
    return m_pThreadPool->GetTotalThreads();
}

const CByteImage* CTrackingControl::getVirtualRendererImage()
{
    return m_pVirtualRendererImage;
}
