//#ifndef TESTCUDAPARAMETRICLINERENDERER_H
//#define TESTCUDAPARAMETRICLINERENDERER_H

//#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>
//#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/ParametricLineRenderer.h>
//#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotControlInterface.h>
//#include <EgomotionTracking/components/EgomotionTracking/Helpers/CameraConfiguration.h>
//#include <EgomotionTracking/components/EgomotionTracking/Misc/RandomGenerator/ParkMillerRandomGenerator.h>

//#include <EgomotionTrackerCUDA/ParametricLineRenderer/ParametricLineRenderer.h>
//#include <EgomotionTrackerCUDA/ParametricLineRenderer/CommonData.h>

//class TestCUDAParametricLineRenderer
//{
//public:
//    TestCUDAParametricLineRenderer();
//    virtual ~TestCUDAParametricLineRenderer();
//    void SetCudaRenderer(CudaParametricLineRenderer* cudaRenderer);
//    void SetParametricLineRenderer(const CParametricLineRenderer& parametricLineRenderer)
//    {
//        m_parametricLineRenderer = parametricLineRenderer;
//    }
//    void SetRobotControlInterface(const CRobotControlInterface* pRobotControlInterface)
//    {
//        m_pRobotControlInterface = pRobotControlInterface;
//    }

//    bool TestSingleParticle(float x, float y, float alpha);
//    bool RandomTestResultCorrectness(float minX, float maxX, float minY, float maxY, float minAlpha, float maxAlpha, unsigned int numberOfTests);
//    bool RandomTestMultipleParticles(float minX, float maxX, float minY, float maxY, float minAlpha, float maxAlpha, unsigned int numberOfParticles);
//    bool DualTest(float x1, float y1, float alpha1, float x2, float y2, float alpha2);

//protected:
//    const CRobotControlInterface *m_pRobotControlInterface;
//    CParametricLineRenderer m_parametricLineRenderer;
//    CudaParametricLineRenderer* m_cudaRenderer;
//    CParkMillerRandomGenerator m_randomGenerator;

//    int m_numberOfModelLines;
//};

//#endif
