//#include "TestCUDAParametricLineRenderer.h"

//#include <EgomotionTracking/components/EgomotionTracking/Helpers/Profiling.h>

//#include <iostream>
//#include <fstream>

//TestCUDAParametricLineRenderer::TestCUDAParametricLineRenderer() : m_pRobotControlInterface(NULL), m_cudaRenderer(NULL)
//{

//}

//TestCUDAParametricLineRenderer::~TestCUDAParametricLineRenderer()
//{

//}

//void TestCUDAParametricLineRenderer::SetCudaRenderer(CudaParametricLineRenderer *cudaRenderer)
//{
//    m_cudaRenderer = cudaRenderer;
//}

//bool TestCUDAParametricLineRenderer::TestSingleParticle(float x, float y, float alpha)
//{
//    if(m_pRobotControlInterface == NULL)
//    {
//        std::cout << "ERROR: m_pRobotControlInterface is NULL" << std::endl;
//        return false;
//    }

//    int numberOfParticles = 2;

//    float* viewingMatricesGL = new float[numberOfParticles * 16];

//    CLocationState locationState(x, y, Conversions::DegreeToRadians(alpha));
//    CLocationParticle locationParticle(locationState);
//    locationParticle.GetScreenJunctionPointsWriteable().clear();
//    locationParticle.GetScreenLinesWriteable().clear();

//    CHeadConfiguration headConfiguration = m_pRobotControlInterface->GetHeadConfiguration();
//    headConfiguration.SetNeckError(locationParticle.GetNeckRollError(), locationParticle.GetNeckPitchError(), locationParticle.GetNeckYawError());
//    SbVec3f cameraTranslation;
//    SbRotation cameraOrientation;
//    if(!m_pRobotControlInterface->CalculateCameraPosition(locationParticle, cameraTranslation, cameraOrientation, headConfiguration))
//    {
//        return false;
//    }
//    m_parametricLineRenderer.SetCameraPosition(OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(cameraTranslation, OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(cameraOrientation)));
//    SbMatrix& particleViewingMatrix = m_parametricLineRenderer.GetViewingMatrixGL();
//    for (unsigned int index = 0; index < numberOfParticles; ++index)
//    {
//        float* particleViewingMatricesGL = viewingMatricesGL + 16 * index;
//        for (int rowIndex = 0; rowIndex < 4; ++rowIndex)
//        {
//            for (int columnIndex = 0; columnIndex < 4; ++columnIndex)
//            {
//                particleViewingMatricesGL[rowIndex * 4 + columnIndex] = particleViewingMatrix[rowIndex][columnIndex];
//            }
//        }
//    }

//    float* resultLineData = new float[2 * 6 * 130 * numberOfParticles];
//    int* particleResultLineStartIndex;
//    int* particleResultLineCount;
//    int* particleResultLineInfo = new int[2 * numberOfParticles];
//    particleResultLineStartIndex = particleResultLineInfo;
//    particleResultLineCount = particleResultLineStartIndex + numberOfParticles;
//    int resultLineCount = 0;

//    m_cudaRenderer->Render(viewingMatricesGL, numberOfParticles, resultLineData, particleResultLineInfo, &resultLineCount);
//    m_parametricLineRenderer.Render(locationParticle.GetScreenLinesWriteable());

//    LineSoA resultLines;
//    resultLines.startPointX = resultLineData;
//    resultLines.startPointY = resultLines.startPointX + resultLineCount;
//    resultLines.startPointZ = resultLines.startPointY + resultLineCount;
//    resultLines.endPointX = resultLines.startPointZ + resultLineCount;
//    resultLines.endPointY = resultLines.endPointX + resultLineCount;
//    resultLines.endPointZ = resultLines.endPointY + resultLineCount;
//    std::vector<CScreenLine>& egoResults = locationParticle.GetScreenLinesWriteable();

//    std::cout << "CUDA returned " << resultLineCount << " lines while EGO returned " << egoResults.size() << " lines" << std::endl;

//    std::cout << std::endl << "CUDA RESULTS" << std::endl << std::endl;
//    for (unsigned int index = 0; index < resultLineCount; ++index)
//    {
//        std::cout << "INDEX " << index << " - " << resultLines.startPointX[index] << "/" << resultLines.startPointY[index];
//        std::cout << "/" << resultLines.startPointZ[index] << "\t\t" << resultLines.endPointX[index] << "/" << resultLines.endPointY[index] << "/";
//        std::cout << resultLines.endPointZ[index] << std::endl;
//    }
//    for (unsigned int index = 0; index < numberOfParticles; ++index)
//    {
//        std::cout << "PARTICLE " << index << " starts at " << particleResultLineStartIndex[index] << " and has " << particleResultLineCount[index] << " lines" << std::endl;
//    }

//    std::cout << std::endl << "EGO RESULTS" << std::endl << std::endl;
//    for (unsigned int index = 0; index < egoResults.size(); ++index)
//    {
//        CScreenLine l = egoResults[index];
//        std::cout << "INDEX " << index << ": " << l.m_p1[0] << "/" << l.m_p1[1] << "/" << l.m_p1[2] << "\t\t";
//        std::cout << l.m_p2[0] << "/" << l.m_p2[1] << "/" << l.m_p2[2] << std::endl;
//    }

//    delete [] viewingMatricesGL;
//    delete [] resultLineData;
//    delete [] particleResultLineInfo;

//    return true;
//}

//bool TestCUDAParametricLineRenderer::RandomTestResultCorrectness(float minX, float maxX, float minY, float maxY, float minAlpha, float maxAlpha, unsigned int numberOfTests)
//{
//    if(m_pRobotControlInterface == NULL)
//    {
//        std::cout << "ERROR: m_pRobotControlInterface is NULL" << std::endl;
//        return false;
//    }

//    std::string outputFileName("/home/SMBAD/heyde/outputs/RandomTestResultCorrectnessOutput.txt");

//    std::ofstream outputFile;
//    outputFile.open(outputFileName.c_str());
//    if (!outputFile.is_open())
//    {
//        std::cout << "RandomTestResultCorrectness - Could not open output file" << std::endl;
//    }

//    std::cout << "RandomTestResultCorrectness - Running " << numberOfTests << " tests" << std::endl;

//    int numberOfParticles = 1;
//    bool allWentWell = true;

//    float* viewingMatrices = new float[numberOfParticles * 16];
//    CHeadConfiguration headConfiguration = m_pRobotControlInterface->GetHeadConfiguration();

//    float* resultLineData = new float[2 * 6 * 130 * numberOfParticles];
//    int* particleResultLineStartIndex;
//    int* particleResultLineCount;
//    int* particleResultLineInfo = new int[2 * numberOfParticles];
//    particleResultLineStartIndex = particleResultLineInfo;
//    particleResultLineCount = particleResultLineStartIndex + numberOfParticles;
//    int resultLineCount = 0;

//    CProfiling workloadProfiler;
//    workloadProfiler.StartProfiling();

//    for (unsigned int testIndex = 0; testIndex < numberOfTests; ++testIndex)
//    {
//        int x = m_randomGenerator.GetUniformRandomNumber(minX, maxX);
//        int y = m_randomGenerator.GetUniformRandomNumber(minY, maxY);
//        int alpha = m_randomGenerator.GetUniformRandomNumber(minAlpha, maxAlpha);

//        CLocationState locationState(x, y, Conversions::DegreeToRadians(alpha));
//        CLocationParticle locationParticle(locationState);
//        locationParticle.GetScreenJunctionPointsWriteable().clear();
//        locationParticle.GetScreenLinesWriteable().clear();
//        headConfiguration.SetNeckError(locationParticle.GetNeckRollError(), locationParticle.GetNeckPitchError(), locationParticle.GetNeckYawError());

//        SbVec3f cameraTranslation;
//        SbRotation cameraOrientation;
//        if(!m_pRobotControlInterface->CalculateCameraPosition(locationParticle, cameraTranslation, cameraOrientation, headConfiguration))
//        {
//            return false;
//        }
//        m_parametricLineRenderer.SetCameraPosition(OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(cameraTranslation, OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(cameraOrientation)));

//        SbMatrix& particleViewingMatrix = m_parametricLineRenderer.GetViewingMatrixGL();
//        for (unsigned int index = 0; index < numberOfParticles; ++index)
//        {
//            float* particleViewingMatrices = viewingMatrices + 16 * index;
//            for (int rowIndex = 0; rowIndex < 4; ++rowIndex)
//            {
//                for (int columnIndex = 0; columnIndex < 4; ++columnIndex)
//                {
//                    particleViewingMatrices[rowIndex * 4 + columnIndex] = particleViewingMatrix[rowIndex][columnIndex];
//                }
//            }
//        }

//        m_cudaRenderer->Render(viewingMatrices, numberOfParticles, resultLineData, particleResultLineInfo, &resultLineCount);
//        m_parametricLineRenderer.Render(locationParticle.GetScreenLinesWriteable());

//        LineSoA resultLines;
//        resultLines.startPointX = resultLineData;
//        resultLines.startPointY = resultLines.startPointX + resultLineCount;
//        resultLines.startPointZ = resultLines.startPointY + resultLineCount;
//        resultLines.endPointX = resultLines.startPointZ + resultLineCount;
//        resultLines.endPointY = resultLines.endPointX + resultLineCount;
//        resultLines.endPointZ = resultLines.endPointY + resultLineCount;

//        std::vector<CScreenLine>& egoResults = locationParticle.GetScreenLinesWriteable();

//        if (egoResults.size() * numberOfParticles != resultLineCount)
//        {
//            outputFile << "Particle X: " << x << " Y: " << y << " Alpha: " << alpha << " has incorrect results - ";
//            outputFile << "\tEGO returned " << egoResults.size() << " lines while CUDA returned" << resultLineCount << " lines" << std::endl;
//            allWentWell = false;
//        }
//        else
//        {
//            bool correct = true;
//            for (unsigned int lineIndex = 0; lineIndex < egoResults.size(); ++lineIndex)
//            {
//                CScreenLine egoLine = egoResults[lineIndex];
//                for (unsigned int particleIndex = 0; particleIndex < numberOfParticles; ++particleIndex)
//                {
//                    unsigned int cudaIndex = particleIndex * egoResults.size() + lineIndex;
//                    if (fabs(resultLines.startPointX[cudaIndex] - egoLine.m_p1[0]) > 0.01 ||
//                            fabs(resultLines.startPointY[cudaIndex] - egoLine.m_p1[1]) > 0.01 ||
//                            fabs(resultLines.startPointZ[cudaIndex] - egoLine.m_p1[2]) > 0.01 ||
//                            fabs(resultLines.endPointX[cudaIndex] - egoLine.m_p2[0]) > 0.01 ||
//                            fabs(resultLines.endPointY[cudaIndex] - egoLine.m_p2[1]) > 0.01 ||
//                            fabs(resultLines.endPointZ[cudaIndex] - egoLine.m_p2[2]) > 0.01)
//                    {
//                        correct = false;
//                        allWentWell = false;
//                    }
//                }
//            }
//            if (!correct)
//            {
//                outputFile << "Particle X: " << x << " Y: " << y << " Alpha: " << alpha << " has incorrect results" << std::endl;
//                outputFile << "\tBoth returned " << egoResults.size() << " lines" << std::endl;
//                for (unsigned int lineIndex = 0; lineIndex < egoResults.size(); ++lineIndex)
//                {
//                    CScreenLine egoLine = egoResults[lineIndex];
//                    for (unsigned int particleIndex = 0; particleIndex < numberOfParticles; ++particleIndex)
//                    {
//                        unsigned int cudaIndex = particleIndex * egoResults.size() + lineIndex;
//                        if (fabs(resultLines.startPointX[cudaIndex] - egoLine.m_p1[0]) > 0.001 ||
//                                fabs(resultLines.startPointY[cudaIndex] - egoLine.m_p1[1]) > 0.001 ||
//                                fabs(resultLines.startPointZ[cudaIndex] - egoLine.m_p1[2]) > 0.001 ||
//                                fabs(resultLines.endPointX[cudaIndex] - egoLine.m_p2[0]) > 0.001 ||
//                                fabs(resultLines.endPointY[cudaIndex] - egoLine.m_p2[1]) > 0.001 ||
//                                fabs(resultLines.endPointZ[cudaIndex] - egoLine.m_p2[2]) > 0.001)
//                        {
//                            outputFile << "\tCUDA INDEX " << cudaIndex << " - " << resultLines.startPointX[cudaIndex] << "/" << resultLines.startPointY[cudaIndex];
//                            outputFile << "/" << resultLines.startPointZ[cudaIndex] << "\t\t" << resultLines.endPointX[cudaIndex] << "/" << resultLines.endPointY[cudaIndex] << "/";
//                            outputFile << resultLines.endPointZ[cudaIndex]<< std::endl;
//                            outputFile << "\tEGO INDEX " << lineIndex << ": " << egoLine.m_p1[0] << "/" << egoLine.m_p1[1] << "/" << egoLine.m_p1[2] << "\t\t";
//                            outputFile << egoLine.m_p2[0] << "/" << egoLine.m_p2[1] << "/" << egoLine.m_p2[2] << std::endl;
//                        }
//                    }
//                }
//            }
//        }
//    }

//    float testExecutionTime = workloadProfiler.GetElapsedMS();

//    std::cout << "RandomTestResultCorrectness - Owarimasu" << std::endl;
//    if (!allWentWell)
//    {
//        std::cout << "RandomTestResultCorrectness - There were incorrect results, details written to " << outputFileName << std::endl;
//    }
//    std::cout << "RandomTestResultCorrectness - Testing took " << testExecutionTime << "ms" << std::endl;

//    outputFile.close();

//    delete [] viewingMatrices;
//    delete [] resultLineData;
//    delete [] particleResultLineInfo;

//    return true;
//}

//bool TestCUDAParametricLineRenderer::RandomTestMultipleParticles(float minX, float maxX, float minY, float maxY, float minAlpha, float maxAlpha, unsigned int numberOfParticles)
//{
//    if(m_pRobotControlInterface == NULL)
//    {
//        std::cout << "ERROR: m_pRobotControlInterface is NULL" << std::endl;
//        return false;
//    }

//    std::string outputFileName("/home/SMBAD/heyde/outputs/RandomTestMultipleParticlesOutput.txt");

//    std::ofstream outputFile;
//    outputFile.open(outputFileName.c_str());
//    if (!outputFile.is_open())
//    {
//        std::cout << "RandomTestMultipleParticles - Could not open output file" << std::endl;
//    }

//    std::cout << "RandomTestMultipleParticles - Running for " << numberOfParticles << " particles" << std::endl;

//    float* viewingMatrices = new float[numberOfParticles * 16];
//    CHeadConfiguration headConfiguration = m_pRobotControlInterface->GetHeadConfiguration();

//    float* resultLineData = new float[2 * 6 * 130 * numberOfParticles];
//    int* particleResultLineStartIndex;
//    int* particleResultLineCount;
//    int* particleResultLineInfo = new int[2 * numberOfParticles];
//    particleResultLineStartIndex = particleResultLineInfo;
//    particleResultLineCount = particleResultLineStartIndex + numberOfParticles;
//    int resultLineCount = 0;

//    outputFile << "EGO RENDERING RESULTS" << std::endl;

//    CProfiling workloadProfiler;
//    workloadProfiler.StartProfiling();

//    for (unsigned int particleIndex = 0; particleIndex < numberOfParticles; ++particleIndex)
//    {
//        int x = m_randomGenerator.GetUniformRandomNumber(minX, maxX);
//        int y = m_randomGenerator.GetUniformRandomNumber(minY, maxY);
//        int alpha = m_randomGenerator.GetUniformRandomNumber(minAlpha, maxAlpha);

//        outputFile << "\nEGO PARTICLE " << x << " " << y << " " << alpha << std::endl;

//        CLocationState locationState(x, y, Conversions::DegreeToRadians(alpha));
//        CLocationParticle locationParticle(locationState);
//        locationParticle.GetScreenJunctionPointsWriteable().clear();
//        locationParticle.GetScreenLinesWriteable().clear();
//        headConfiguration.SetNeckError(locationParticle.GetNeckRollError(), locationParticle.GetNeckPitchError(), locationParticle.GetNeckYawError());

//        SbVec3f cameraTranslation;
//        SbRotation cameraOrientation;
//        if(!m_pRobotControlInterface->CalculateCameraPosition(locationParticle, cameraTranslation, cameraOrientation, headConfiguration))
//        {
//            return false;
//        }
//        m_parametricLineRenderer.SetCameraPosition(OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(cameraTranslation, OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(cameraOrientation)));

//        SbMatrix& particleViewingMatrix = m_parametricLineRenderer.GetViewingMatrixGL();
//        float* particleViewingMatrices = viewingMatrices + 16 * particleIndex;
//        for (int rowIndex = 0; rowIndex < 4; ++rowIndex)
//        {
//            for (int columnIndex = 0; columnIndex < 4; ++columnIndex)
//            {
//                particleViewingMatrices[rowIndex * 4 + columnIndex] = particleViewingMatrix[rowIndex][columnIndex];
//            }
//        }

//        m_parametricLineRenderer.Render(locationParticle.GetScreenLinesWriteable());

//        std::vector<CScreenLine>& egoResults = locationParticle.GetScreenLinesWriteable();
//        outputFile << "PARTICLE has " << egoResults.size() << " lines" << std::endl;
//        for (int lineIndex = 0; lineIndex < egoResults.size(); ++lineIndex)
//        {
//            CScreenLine line = egoResults[lineIndex];
//            outputFile << "INDEX " << lineIndex << ": " << line.m_p1[0] << "/" << line.m_p1[1] << "/" << line.m_p1[2] << "\t\t";
//            outputFile << line.m_p2[0] << "/" << line.m_p2[1] << "/" << line.m_p2[2] << std::endl;
//        }
//    }

//    outputFile << "\nCUDA RENDERING RESULTS" << std::endl;

//    m_cudaRenderer->Render(viewingMatrices, numberOfParticles, resultLineData, particleResultLineInfo, &resultLineCount);

//    LineSoA resultLines;
//    resultLines.startPointX = resultLineData;
//    resultLines.startPointY = resultLines.startPointX + resultLineCount;
//    resultLines.startPointZ = resultLines.startPointY + resultLineCount;
//    resultLines.endPointX = resultLines.startPointZ + resultLineCount;
//    resultLines.endPointY = resultLines.endPointX + resultLineCount;
//    resultLines.endPointZ = resultLines.endPointY + resultLineCount;

//    outputFile << "\nPARTICLE RESULT LINE INFO" << std::endl;
//    for (int particleIndex = 0; particleIndex < numberOfParticles; ++particleIndex)
//    {
//        outputFile << "PARTICLE " << particleIndex << " has " << particleResultLineCount[particleIndex] << " lines starting at INDEX " << particleResultLineStartIndex[particleIndex] << std::endl;
//    }
//    outputFile << "\nRESULT LINES" << std::endl;
//    for (int lineIndex = 0; lineIndex < resultLineCount; ++lineIndex)
//    {
//        outputFile << "INDEX " << lineIndex << ": " << resultLines.startPointX[lineIndex] << "/" << resultLines.startPointY[lineIndex];
//        outputFile << "/" << resultLines.startPointZ[lineIndex] << "\t\t" << resultLines.endPointX[lineIndex] << "/" << resultLines.endPointY[lineIndex] << "/";
//        outputFile << resultLines.endPointZ[lineIndex]<< std::endl;
//    }

//    float testExecutionTime = workloadProfiler.GetElapsedMS();

//    std::cout << "RandomTestMultipleParticles - Owarimasu" << std::endl;
//    std::cout << "RandomTestMultipleParticles - Testing took " << testExecutionTime << "ms" << std::endl;

//    outputFile.close();

//    delete [] viewingMatrices;
//    delete [] resultLineData;
//    delete [] particleResultLineInfo;

//    return true;
//}

//bool TestCUDAParametricLineRenderer::DualTest(float x1, float y1, float alpha1, float x2, float y2, float alpha2)
//{
//    if(m_pRobotControlInterface == NULL)
//    {
//        std::cout << "ERROR: m_pRobotControlInterface is NULL" << std::endl;
//        return false;
//    }

//    std::string outputFileName("/home/SMBAD/heyde/outputs/DualTestOutput.txt");

//    std::ofstream outputFile;
//    outputFile.open(outputFileName.c_str());
//    if (!outputFile.is_open())
//    {
//        std::cout << "DualTest - Could not open output file" << std::endl;
//    }

//    float* viewingMatrices = new float[2 * 16];
//    CHeadConfiguration headConfiguration = m_pRobotControlInterface->GetHeadConfiguration();

//    float* resultLineData = new float[2 * 6 * 130 * 2];
//    int* particleResultLineStartIndex;
//    int* particleResultLineCount;
//    int* particleResultLineInfo = new int[2 * 2];
//    particleResultLineStartIndex = particleResultLineInfo;
//    particleResultLineCount = particleResultLineStartIndex + 2;
//    int resultLineCount = 0;

//    outputFile << "EGO RENDERING RESULTS" << std::endl;

//    CProfiling workloadProfiler;
//    workloadProfiler.StartProfiling();

//    outputFile << "\nEGO PARTICLE 01" << x1 << " " << y1 << " " << alpha1 << std::endl;

//    CLocationState locationState1(x1, y1, Conversions::DegreeToRadians(alpha1));
//    CLocationParticle locationParticle1(locationState1);
//    locationParticle1.GetScreenJunctionPointsWriteable().clear();
//    locationParticle1.GetScreenLinesWriteable().clear();
//    headConfiguration.SetNeckError(locationParticle1.GetNeckRollError(), locationParticle1.GetNeckPitchError(), locationParticle1.GetNeckYawError());

//    SbVec3f cameraTranslation1;
//    SbRotation cameraOrientation1;
//    if(!m_pRobotControlInterface->CalculateCameraPosition(locationParticle1, cameraTranslation1, cameraOrientation1, headConfiguration))
//    {
//        return false;
//    }
//    m_parametricLineRenderer.SetCameraPosition(OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(cameraTranslation1, OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(cameraOrientation1)));

//    m_parametricLineRenderer.Render(locationParticle1.GetScreenLinesWriteable());

//    SbMatrix& particleViewingMatrix1 = m_parametricLineRenderer.GetViewingMatrixGL();
//    float* particleViewingMatrices1 = viewingMatrices + 16 * 0;
//    for (int rowIndex = 0; rowIndex < 4; ++rowIndex)
//    {
//        for (int columnIndex = 0; columnIndex < 4; ++columnIndex)
//        {
//            particleViewingMatrices1[rowIndex * 4 + columnIndex] = particleViewingMatrix1[rowIndex][columnIndex];
//        }
//    }

//    std::vector<CScreenLine>& egoResults1 = locationParticle1.GetScreenLinesWriteable();
//    outputFile << "PARTICLE has " << egoResults1.size() << " lines" << std::endl;
//    for (int lineIndex = 0; lineIndex < egoResults1.size(); ++lineIndex)
//    {
//        CScreenLine line = egoResults1[lineIndex];
//        outputFile << "INDEX " << lineIndex << ": " << line.m_p1[0] << "/" << line.m_p1[1] << "/" << line.m_p1[2] << "\t\t";
//        outputFile << line.m_p2[0] << "/" << line.m_p2[1] << "/" << line.m_p2[2] << std::endl;
//    }

//    outputFile << "\nEGO PARTICLE 02" << x2 << " " << y2 << " " << alpha2 << std::endl;

//    CLocationState locationState2(x2, y2, Conversions::DegreeToRadians(alpha2));
//    CLocationParticle locationParticle2(locationState2);
//    locationParticle2.GetScreenJunctionPointsWriteable().clear();
//    locationParticle2.GetScreenLinesWriteable().clear();
//    headConfiguration.SetNeckError(locationParticle2.GetNeckRollError(), locationParticle2.GetNeckPitchError(), locationParticle2.GetNeckYawError());

//    SbVec3f cameraTranslation2;
//    SbRotation cameraOrientation2;
//    if(!m_pRobotControlInterface->CalculateCameraPosition(locationParticle2, cameraTranslation2, cameraOrientation2, headConfiguration))
//    {
//        return false;
//    }
//    m_parametricLineRenderer.SetCameraPosition(OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(cameraTranslation2, OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(cameraOrientation2)));

//    m_parametricLineRenderer.Render(locationParticle2.GetScreenLinesWriteable());

//    SbMatrix& particleViewingMatrix2 = m_parametricLineRenderer.GetViewingMatrixGL();
//    float* particleViewingMatrices2 = viewingMatrices + 16 * 1;
//    for (int rowIndex = 0; rowIndex < 4; ++rowIndex)
//    {
//        for (int columnIndex = 0; columnIndex < 4; ++columnIndex)
//        {
//            particleViewingMatrices2[rowIndex * 4 + columnIndex] = particleViewingMatrix2[rowIndex][columnIndex];
//        }
//    }

//    std::vector<CScreenLine>& egoResults2 = locationParticle2.GetScreenLinesWriteable();
//    outputFile << "PARTICLE has " << egoResults2.size() << " lines" << std::endl;
//    for (int lineIndex = 0; lineIndex < egoResults2.size(); ++lineIndex)
//    {
//        CScreenLine line = egoResults2[lineIndex];
//        outputFile << "INDEX " << lineIndex << ": " << line.m_p1[0] << "/" << line.m_p1[1] << "/" << line.m_p1[2] << "\t\t";
//        outputFile << line.m_p2[0] << "/" << line.m_p2[1] << "/" << line.m_p2[2] << std::endl;
//    }

//    outputFile << "\nCUDA RENDERING RESULTS" << std::endl;

//    m_cudaRenderer->Render(viewingMatrices, 2, resultLineData, particleResultLineInfo, &resultLineCount);

//    LineSoA resultLines;
//    resultLines.startPointX = resultLineData;
//    resultLines.startPointY = resultLines.startPointX + resultLineCount;
//    resultLines.startPointZ = resultLines.startPointY + resultLineCount;
//    resultLines.endPointX = resultLines.startPointZ + resultLineCount;
//    resultLines.endPointY = resultLines.endPointX + resultLineCount;
//    resultLines.endPointZ = resultLines.endPointY + resultLineCount;

//    outputFile << "\nPARTICLE RESULT LINE INFO" << std::endl;
//    for (int particleIndex = 0; particleIndex < 2; ++particleIndex)
//    {
//        outputFile << "PARTICLE " << particleIndex << " has " << particleResultLineCount[particleIndex] << " lines starting at INDEX " << particleResultLineStartIndex[particleIndex] << std::endl;
//    }
//    outputFile << "\nRESULT LINES" << std::endl;
//    for (int lineIndex = 0; lineIndex < resultLineCount; ++lineIndex)
//    {
//        outputFile << "INDEX " << lineIndex << ": " << resultLines.startPointX[lineIndex] << "/" << resultLines.startPointY[lineIndex];
//        outputFile << "/" << resultLines.startPointZ[lineIndex] << "\t\t" << resultLines.endPointX[lineIndex] << "/" << resultLines.endPointY[lineIndex] << "/";
//        outputFile << resultLines.endPointZ[lineIndex]<< std::endl;
//    }

//    float testExecutionTime = workloadProfiler.GetElapsedMS();

//    std::cout << "DualTest - Owarimasu" << std::endl;
//    std::cout << "DualTest - Testing took " << testExecutionTime << "ms" << std::endl;

//    outputFile.close();

//    delete [] viewingMatrices;
//    delete [] resultLineData;
//    delete [] particleResultLineInfo;

//    return true;
//}
