#include "WeightingExecutionUnit.h"

CWeightingExecutionUnit::CWeightingExecutionUnit() : m_pObservationModel(NULL)
{
}

void CWeightingExecutionUnit::SetObservationModel(const CObservationModelInterface* pObservationModel)
{
    m_pObservationModel = pObservationModel;
}


bool CWeightingExecutionUnit::Execute()
{
    assert(m_pObservationModel);
    std::vector<CLocationParticle>::iterator currentParticle = m_particlesBegin;

    for (; currentParticle < m_particlesEnd; ++currentParticle)
    {
        m_renderingUnit.RenderParticle(*currentParticle, m_pObservationModel->GetNeedRenderedJunctionPoints());
        const PF_REAL currentWeight = m_pObservationModel->CalculateImportanceWeight(*currentParticle, false);
        currentParticle->SetImportanceWeight(currentWeight);
    }
    return true;
}


void CWeightingExecutionUnit::UpdateRenderingUnit()
{
    m_renderingUnit.UpdateRenderingConfigurationFromParent();
}


void CWeightingExecutionUnit::SetParticles(const std::vector<CLocationParticle>::iterator& particlesBegin, const std::vector<CLocationParticle>::iterator& particlesEnd)
{
    m_particlesBegin = particlesBegin;
    m_particlesEnd = particlesEnd;
}


void CWeightingExecutionUnit::SetRenderingUnit(const CRenderingUnit& renderingUnit)
{
    m_renderingUnit = renderingUnit;
}
