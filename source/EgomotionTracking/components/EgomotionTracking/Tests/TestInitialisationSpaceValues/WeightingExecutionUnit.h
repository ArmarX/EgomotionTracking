#pragma once

#include <EgomotionTracking/components/EVP/Foundation/Process/ExecutionUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/ObservationModel/ObservationModelInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/RenderingUnit/RenderingUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>

#include <vector>

class CWeightingExecutionUnit : public EVP::Threading::CExecutionUnit
{
public:
    CWeightingExecutionUnit();

    void SetObservationModel(const CObservationModelInterface* pObservationModel);
    void SetRenderingUnit(const CRenderingUnit& renderingUnit);
    void SetParticles(const std::vector<CLocationParticle>::iterator& particlesBegin,
                      const std::vector<CLocationParticle>::iterator& particlesEnd);
    void UpdateRenderingUnit();

protected:
    bool Execute() override;

    const CObservationModelInterface* m_pObservationModel;
    CRenderingUnit m_renderingUnit;
    std::vector<CLocationParticle>::iterator m_particlesBegin;
    std::vector<CLocationParticle>::iterator m_particlesEnd;
};

