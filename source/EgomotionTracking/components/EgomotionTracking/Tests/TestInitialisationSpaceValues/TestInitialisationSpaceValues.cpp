#include "TestInitialisationSpaceValues.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/CSVPrimitives.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Profiling.h>

CTestInitialisationSpaceValues::CTestInitialisationSpaceValues() :
    m_pThreadPool(NULL),
    m_pObservationModel(NULL),
    m_pRobotControl(NULL),
    m_pRenderingUnit(NULL),
    m_pTrackingImageProcessor(NULL)
{
}

bool CTestInitialisationSpaceValues::RunTest()
{

    if (m_pRobotControl == NULL)
    {
        COUT_ERROR << "m_pRobotControl == NULL" << std::endl;
        return false;
    }

    if (m_pTrackingImageProcessor == NULL)
    {
        COUT_ERROR << "m_pTrackingImageProcessor == NULL" << std::endl;
        return false;
    }

    if (!m_pRobotControl->DispatchRobot())
    {
        COUT_ERROR << "m_pRobotControl->DispatchRobot() failed" << std::endl;
        return false;
    }

    //XXX not here, das zeugs komplett rauswerfen...
    m_pTrackingImageProcessor->SetProcessingOption(CTrackingImageProcessorInterface::eEdgeImageOption, true);
    m_pTrackingImageProcessor->SetProcessingOption(CTrackingImageProcessorInterface::eGradientOrientationOption, true);
    m_pTrackingImageProcessor->SetInputImage(&m_pRobotControl->GetCameraImage());

    m_pTrackingImageProcessor->DispatchImageProcessing();

    if (m_pObservationModel == NULL)
    {
        COUT_ERROR << " m_pObservationModel == NULL" << std::endl;
        return false;
    }
    m_pObservationModel->SetMeasurement(&m_pTrackingImageProcessor->GetEdgeImage(), m_pTrackingImageProcessor->GetGradientOrientations(), m_pTrackingImageProcessor->GetGradients());

    CSVDocument csvDocument;

    UpdateExecutionUnits();

    int numberOfParticlesToProcess = 0;
    int numberOfParticlesProcessed  = 0;
    for (float curX = m_locationRegion.GetMinX(); curX <= m_locationRegion.GetMaxX(); curX += m_resolutionX)
    {
        for (float curY = m_locationRegion.GetMinY(); curY <= m_locationRegion.GetMaxY(); curY += m_resolutionY)
        {
            ++numberOfParticlesToProcess;
        }
    }

    CProfiling etaProfiller;
    etaProfiller.StartProfiling();
    float lastProgressInPercentage = 0.0;

    for (float curX = m_locationRegion.GetMinX(); curX <= m_locationRegion.GetMaxX(); curX += m_resolutionX)
    {
        for (float curY = m_locationRegion.GetMinY(); curY <= m_locationRegion.GetMaxY(); curY += m_resolutionY)
        {
            m_locationParticlesPool.clear();

            for (float curAlpha = m_locationRegion.GetMinAlpha(); curAlpha <= m_locationRegion.GetMaxAlpha(); curAlpha += m_resolutionAlpha)
            {
                CLocationParticle curLocationParticle(CLocationState(curX, curY, curAlpha));

                if (m_neckSamplingMode == eFixed)
                {
                    curLocationParticle.SetNeckRollError(m_neckErrorRoll);
                    curLocationParticle.SetNeckPitchError(m_neckErrorPitch);
                    curLocationParticle.SetNeckYawError(m_neckErrorYaw);
                    m_locationParticlesPool.push_back(curLocationParticle);
                }
                else if (m_neckSamplingMode == eBruteForce)
                {
                    for (float curNeckRoll = m_neckErrorRollMin; curNeckRoll <= m_neckErrorRollMax; curNeckRoll += m_resolutionNeckRoll)
                    {
                        for (float curNeckPitch = m_neckErrorPitchMin; curNeckPitch <= m_neckErrorPitchMax; curNeckPitch += m_resolutionNeckPitch)
                        {
                            for (float curNeckYaw = m_neckErrorYawMin; curNeckYaw <= m_neckErrorYawMax; curNeckYaw += m_resolutionNeckYaw)
                            {
                                curLocationParticle.SetNeckRollError(curNeckRoll);
                                curLocationParticle.SetNeckPitchError(curNeckPitch);
                                curLocationParticle.SetNeckYawError(curNeckYaw);
                                m_locationParticlesPool.push_back(curLocationParticle);
                            }
                        }
                    }
                }
            }
            numberOfParticlesProcessed++;
            const float progressInPercentage = Conversions::ProportionsToPercentage(static_cast<float>(numberOfParticlesProcessed) / numberOfParticlesToProcess);
            std::cout << progressInPercentage;
            const float ellapsedSeconds = Conversions::MillisecondsToSeconds(etaProfiller.GetElapsedMS());
            if (ellapsedSeconds > 1.0)
            {
                const float etaInSeconds = (100.0 - progressInPercentage) / ((progressInPercentage - lastProgressInPercentage) / ellapsedSeconds);
                std::cout << "    ETA: " << etaInSeconds << "seconds (~ " << etaInSeconds / 60 << " min )";
                lastProgressInPercentage = progressInPercentage;
                etaProfiller.StartProfiling();
            }
            std::cout << std::endl;

            CLocationParticle curLocationParticle;
            CalculateParticlesPoolMaxWeight(curLocationParticle);
            /*
            char filename[200];
            sprintf(filename, "maxOMlocation-x%f-y%f.bmp", curX, curY);
            m_pObservationModel->GetVisualizationImage()->SaveToFile(filename);
            */
            csvDocument.AddEntry(CSVPrimitives::SerializeLocationParticleToCSV(curLocationParticle));
        }
    }

    CSVPrimitives::WriteCSVDocumentToFile(csvDocument, "foo.csv");
    return true;
}

void CTestInitialisationSpaceValues::SaveBesOMImageAtPos(float posX, float posY)
{
    if (m_pRobotControl == NULL)
    {
        COUT_ERROR << "m_pRobotControl == NULL" << std::endl;
        return;
    }

    if (m_pTrackingImageProcessor == NULL)
    {
        COUT_ERROR << "m_pTrackingImageProcessor == NULL" << std::endl;
        return;
    }

    if (!m_pRobotControl->DispatchRobot())
    {
        COUT_ERROR << "m_pRobotControl->DispatchRobot() failed" << std::endl;
        return;
    }

    //XXX not here, das zeugs komplett rauswerfen...
    m_pTrackingImageProcessor->SetProcessingOption(CTrackingImageProcessorInterface::eEdgeImageOption, true);
    m_pTrackingImageProcessor->SetProcessingOption(CTrackingImageProcessorInterface::eGradientOrientationOption, true);
    m_pTrackingImageProcessor->SetInputImage(&m_pRobotControl->GetCameraImage());

    m_pTrackingImageProcessor->DispatchImageProcessing();

    if (m_pObservationModel == NULL)
    {
        COUT_ERROR << " m_pObservationModel == NULL" << std::endl;
        return;
    }
    m_pObservationModel->SetMeasurement(&m_pTrackingImageProcessor->GetEdgeImage(), m_pTrackingImageProcessor->GetGradientOrientations(), m_pTrackingImageProcessor->GetGradients());
    UpdateExecutionUnits();


    m_locationParticlesPool.clear();

    std::cout << "roll: " << Conversions::RadiansToDegree(m_neckErrorRollMin) << " , " << Conversions::RadiansToDegree(m_neckErrorRollMax) << std::endl;
    std::cout << "pitch: " << Conversions::RadiansToDegree(m_neckErrorPitchMin) << " , " << Conversions::RadiansToDegree(m_neckErrorPitchMax) << std::endl;
    std::cout << "yaw: " << Conversions::RadiansToDegree(m_neckErrorYawMin) << " , " << Conversions::RadiansToDegree(m_neckErrorYawMax) << std::endl;

    std::cout << Conversions::RadiansToDegree(m_resolutionNeckRoll) << " , " << Conversions::RadiansToDegree(m_resolutionNeckPitch) << " , " << Conversions::RadiansToDegree(m_resolutionNeckYaw) << std::endl;
    for (float curAlpha = m_locationRegion.GetMinAlpha(); curAlpha <= m_locationRegion.GetMaxAlpha(); curAlpha += m_resolutionAlpha)
    {
        CLocationParticle curLocationParticle(CLocationState(posX, posY, curAlpha));

        if (m_neckSamplingMode == eFixed)
        {
            curLocationParticle.SetNeckRollError(m_neckErrorRoll);
            curLocationParticle.SetNeckPitchError(m_neckErrorPitch);
            curLocationParticle.SetNeckYawError(m_neckErrorYaw);
            m_locationParticlesPool.push_back(curLocationParticle);
        }
        else if (m_neckSamplingMode == eBruteForce)
        {
            for (float curNeckRoll = m_neckErrorRollMin; curNeckRoll <= m_neckErrorRollMax; curNeckRoll += m_resolutionNeckRoll)
            {
                for (float curNeckPitch = m_neckErrorPitchMin; curNeckPitch <= m_neckErrorPitchMax; curNeckPitch += m_resolutionNeckPitch)
                {
                    for (float curNeckYaw = m_neckErrorYawMin; curNeckYaw <= m_neckErrorYawMax; curNeckYaw += m_resolutionNeckYaw)
                    {
                        curLocationParticle.SetNeckRollError(curNeckRoll);
                        curLocationParticle.SetNeckPitchError(curNeckPitch);
                        curLocationParticle.SetNeckYawError(curNeckYaw);
                        m_locationParticlesPool.push_back(curLocationParticle);
                    }
                }
            }
        }
    }

    CLocationParticle curLocationParticle;
    CalculateParticlesPoolMaxWeight(curLocationParticle);
    m_pObservationModel->CalculateImportanceWeight(curLocationParticle, true);

    char filename[200];
    sprintf(filename, "maxOMlocation-x%f-y%f.bmp", posX, posY);
    m_pObservationModel->GetVisualizationImage()->SaveToFile(filename);


}

void CTestInitialisationSpaceValues::SetThreadPool(EVP::Threading::CThreadPool* pThreadPool)
{
    if (pThreadPool && pThreadPool->GetTotalThreads() > 0)
    {
        m_pThreadPool = pThreadPool;

        m_executionUnits.resize(pThreadPool->GetTotalThreads());
    }
}

void CTestInitialisationSpaceValues::SetObservationModel(CObservationModelInterface* pObservationModel)
{
    m_pObservationModel = pObservationModel;
}

void CTestInitialisationSpaceValues::SetRobotControl(CRobotControlInterface* pRobotControl)
{
    m_pRobotControl = pRobotControl;
}

void CTestInitialisationSpaceValues::SetRenderingUnit(CRenderingUnit* pRenderingUnit)
{
    m_pRenderingUnit = pRenderingUnit;
}

void CTestInitialisationSpaceValues::SetTrackingImageProcessor(CTrackingImageProcessorInterface* pTrackingImageProcessor)
{
    m_pTrackingImageProcessor = pTrackingImageProcessor;
}

void CTestInitialisationSpaceValues::SetLocationRegion(const CSimpleLocationRegion& locationRegion)
{
    m_locationRegion = locationRegion;
}

void CTestInitialisationSpaceValues::SetResolution(float resolutionX, float resolutionY, float resolutionAlpha)
{
    m_resolutionX = resolutionX;
    m_resolutionY = resolutionY;
    m_resolutionAlpha = resolutionAlpha;
}


void CTestInitialisationSpaceValues::SetNeckRPYError(float neckErrorRoll, float neckErrorPitch, float neckErrorYaw)
{
    m_neckErrorRoll = neckErrorRoll;
    m_neckErrorPitch = neckErrorPitch;
    m_neckErrorYaw = neckErrorYaw;
}

void CTestInitialisationSpaceValues::SetNeckResolution(float resolutionNeckRoll, float resolutionNeckPitch, float resolutionNeckYaw)
{
    m_resolutionNeckRoll = resolutionNeckRoll;
    m_resolutionNeckPitch = resolutionNeckPitch;
    m_resolutionNeckYaw = resolutionNeckYaw;
}

void CTestInitialisationSpaceValues::SetNeckLimits(float neckRollMin, float neckRollMax, float neckPitchMin, float neckPitchMax, float neckYawMin, float neckYawMax)
{
    m_neckErrorRollMin = neckRollMin;
    m_neckErrorRollMax = neckRollMax;
    m_neckErrorPitchMin = neckPitchMin;
    m_neckErrorPitchMax = neckPitchMax;
    m_neckErrorYawMin = neckYawMin;
    m_neckErrorYawMax = neckYawMax;
}

void CTestInitialisationSpaceValues::SetNeckSamplingMode(CTestInitialisationSpaceValues::NeckSamplingMode mode)
{
    m_neckSamplingMode = mode;
}

void CTestInitialisationSpaceValues::UpdateExecutionUnits()
{
    if (m_executionUnits.size() > 0)
        for (std::list<CWeightingExecutionUnit>::iterator currentExecutionUnit = m_executionUnits.begin(); currentExecutionUnit != m_executionUnits.end(); ++currentExecutionUnit)
        {
            if (m_pRenderingUnit != NULL)
            {
                currentExecutionUnit->SetRenderingUnit(*m_pRenderingUnit);
            }

            currentExecutionUnit->SetObservationModel(m_pObservationModel);
            currentExecutionUnit->UpdateRenderingUnit();
        }
}

float CTestInitialisationSpaceValues::CalculateParticlesPoolMaxWeight(CLocationParticle& maxLocationParticle)
{
    if (m_pThreadPool == NULL || m_executionUnits.size() <= 0)
    {
        return -1.0;
    }

    const int minParticlesPerUnit = m_locationParticlesPool.size() / m_executionUnits.size();
    const int smallUnits = m_executionUnits.size() * minParticlesPerUnit + m_executionUnits.size() - m_locationParticlesPool.size();


    int currentUnitIndex = 0;

    std::vector<CLocationParticle>::iterator particleIterator = m_locationParticlesPool.begin();

    for (std::list<CWeightingExecutionUnit>::iterator currentExecutionUnit = m_executionUnits.begin(); currentExecutionUnit != m_executionUnits.end(); ++currentExecutionUnit, ++currentUnitIndex)
    {
        int currentNumberOfParticles = minParticlesPerUnit;
        if (currentUnitIndex >= smallUnits)
        {
            ++currentNumberOfParticles;
        }
        currentExecutionUnit->SetParticles(particleIterator, particleIterator + currentNumberOfParticles);
        particleIterator += currentNumberOfParticles;

        m_pThreadPool->DispatchExecutionUnit(&(*currentExecutionUnit), true);
    }
    m_pThreadPool->Synchronize();


    float maxWeight = -FLT_MAX;
    for (std::vector<CLocationParticle>::iterator particleIterator = m_locationParticlesPool.begin(); particleIterator != m_locationParticlesPool.end(); ++particleIterator)
    {
        const float curWeight = particleIterator->GetImportanceWeight();
        if (curWeight > maxWeight)
        {
            maxWeight = curWeight;
            maxLocationParticle = *particleIterator;
        }
    }
    return maxWeight;
}
