#pragma once

#include <EgomotionTracking/components/EgomotionTracking/Models/ObservationModel/ObservationModelInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotControlInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/RenderingUnit/RenderingUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/RandomGenerator/ParkMillerRandomGenerator.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/SimpleLocationRegion.h>
#include "WeightingExecutionUnit.h"

#include <EgomotionTracking/components/EVP/Foundation/Process/ThreadPool.h>
#include <vector>


class CTestInitialisationSpaceValues
{
public:
    CTestInitialisationSpaceValues();

    bool RunTest();
    void SaveBesOMImageAtPos(float posX, float posY);

    void SetThreadPool(EVP::Threading::CThreadPool* pThreadPool);
    void SetObservationModel(CObservationModelInterface* pObservationModel);
    void SetRobotControl(CRobotControlInterface* pRobotControl);
    void SetRenderingUnit(CRenderingUnit* pRenderingUnit);
    void SetTrackingImageProcessor(CTrackingImageProcessorInterface* pTrackingImageProcessor);
    void SetLocationRegion(const CSimpleLocationRegion& locationRegion);

    void SetResolution(float resolutionX, float resolutionY, float resolutionAlpha);
    void SetNeckRPYError(float neckErrorRoll, float neckErrorPitch, float neckErrorYaw);

    void SetNeckResolution(float resolutionNeckRoll, float resolutionNeckPitch, float resolutionNeckYaw);
    void SetNeckLimits(float neckRollMin, float neckRollMax, float neckPitchMin, float neckPitchMax, float neckYawMin, float neckYawMax);
    enum NeckSamplingMode
    {
        eFixed,
        eBruteForce
    };
    void SetNeckSamplingMode(NeckSamplingMode mode);

protected:

    void UpdateExecutionUnits();
    float CalculateParticlesPoolMaxWeight(CLocationParticle& maxLocationParticle);
    EVP::Threading::CThreadPool* m_pThreadPool;
    std::list<CWeightingExecutionUnit> m_executionUnits;

    CObservationModelInterface* m_pObservationModel;
    CRobotControlInterface* m_pRobotControl;
    CRenderingUnit* m_pRenderingUnit;
    CTrackingImageProcessorInterface* m_pTrackingImageProcessor;

    CParkMillerRandomGenerator m_randomGenerator;
    CSimpleLocationRegion m_locationRegion;

    NeckSamplingMode m_neckSamplingMode;


    std::vector<CLocationParticle> m_locationParticlesPool;

    float m_resolutionX;
    float m_resolutionY;
    float m_resolutionAlpha;

    float m_neckErrorRoll;
    float m_neckErrorPitch;
    float m_neckErrorYaw;

    float m_neckErrorRollMin;
    float m_neckErrorRollMax;
    float m_neckErrorPitchMin;
    float m_neckErrorPitchMax;
    float m_neckErrorYawMin;
    float m_neckErrorYawMax;
    float m_resolutionNeckRoll;
    float m_resolutionNeckPitch;
    float m_resolutionNeckYaw;
};

