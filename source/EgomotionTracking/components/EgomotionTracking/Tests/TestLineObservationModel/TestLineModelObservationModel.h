
// * TestLineModelObservationModel.h
// *
// *  Created on: 22.03.2013
// *      Author: abyte
// */

//#ifndef TESTLINEMODELOBSERVATIONMODEL_H_
//#define TESTLINEMODELOBSERVATIONMODEL_H_

//#include <EgomotionTracking/components/EgomotionTracking/Models//ObservationModel/LineModelObservationModelInterface.h>
//#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>
//#include <EgomotionTracking/components/EgomotionTracking/Processing//RenderingUnit/RenderingUnit.h>
//#include <EgomotionTracking/components/EgomotionTracking/Processing//ImageProcessor/VirtualEdgeImageProcessor.h>
//#include <EgomotionTracking/components/EgomotionTracking/RobotControl/VirtualArmar3RobotControl.h>

////XXX
//#include <EgomotionTracking/components/EgomotionTracking/Visualization/2D/TrackingVisualization/Tracking2DVisualizationScene.h>

//#include <vector>
//#include <Image/ByteImage.h>


//#define OM_TEST_OUTPUT_DIRECTORY EGOMOTIONTRACKING_RESSOURCE_DIRECTORY "/output/omtest/"
//#define OM_TEST_VISUALIZATION_PREFIX "visualization"
//class CTestObservationModel {
//public:
//  CTestObservationModel();
//  virtual ~CTestObservationModel();

//    void PerformanceTestLineModelObservationModel(CObservationModelInterface* pObservationModel);

//  /* Evaluate each dimension seperatly */
//    void EvaluateLineModelObservationModelEachDimension(CObservationModelInterface* pObservationModel);
//    void EvaluateLineModelObservationModelEachDimensionWithSensibilities(CObservationModelInterface* pObservationModel);

//  /* Evaluate 2d-location */
//  void EvaluateLineModelObservationModel2DLocation(CObservationModelInterface* pObservationModel);

//  void AddLocationParticle(const CLocationParticle& locationParticle);
//  bool GetLocationParticle(int number, CLocationParticle& locationParticle);
//  void ClearLocationParticles();

//  int GetNumberOfLocations()
//  {
//      return m_testLocationParticles.size();
//  }

//  bool SaveTestLocationParticlesToFile(const QString& filename);
//  bool LoadTestLocationParticlesFromFile(const QString& filename);

//  void Set(CParametricLineRenderer* pRenderer, CVirtualArmar3RobotControl* pRobotControl);
//  float GetTestRangeAlphaDelta() const;
//  void SetTestRangeAlphaDelta(float testRangeAlphaDelta);
//  float GetTestRangeAlphaMax() const;
//  void SetTestRangeAlphaMax(float testRangeAlphaMax);
//  float GetTestRangeAlphaMin() const;
//  void SetTestRangeAlphaMin(float testRangeAlphaMin);
//  float GetTestRangeXDelta() const;
//  void SetTestRangeXDelta(float testRangeXDelta);
//  float GetTestRangeXMax() const;
//  void SetTestRangeXMax(float testRangeXMax);
//  float GetTestRangeXMin() const;
//  void SetTestRangeXMin(float testRangeXMin);
//  float GetTestRangeYDelta() const;
//  void SetTestRangeYDelta(float testRangeYDelta);
//  float GetTestRangeYMax() const;
//  void SetTestRangeYMax(float testRangeYMax);
//  float GetTestRangeYMin() const;
//  void SetTestRangeYMin(float testRangeYMin);
//  bool isTestStoreVisualization() const;
//  void SetTestStoreVisualization(bool testStoreVisualization);

//    CTracking2DVisualizationScene* GetTrackingVisualizationScene() const;
//    void SetTrackingVisualizationScene(CTracking2DVisualizationScene* pTrackingVisualizationScene);

//    protected:

//    struct CTestResult
//    {
//            CTestResult() {};
//            CTestResult(float deltaX, float deltaY, float deltaAlpha, float weight) :
//                m_deltaX(deltaX), m_deltaY(deltaY), m_deltaAlpha(deltaAlpha), m_weight(weight) {}
//      float m_deltaX;
//      float m_deltaY;
//      float m_deltaAlpha;
//      float m_weight;
//  };

//    void StoreEvaluationVisualizationImage(CObservationModelInterface* pObservationModel, const std::string& prefix, int frameIndex, int testLocationParticleIndex, float deltaX, float deltaY, float deltaAlpha, float weight);
//    void StoreLocationVisualizationImage(const CLocationState* estimatedLocation, const CLocationState* groundTruthLocation, const std::string& prefix, int frameIndex, int testLocationParticleIndex);
//    void StoreTestResultValues(const std::list<CTestResult>&resultValues, const std::string&, int testLocationParticleIndex, bool useStoreSensibilities = false, int sensibilityExponent= 0);

//  CVirtualTrackingImageProcessor m_virtualArmar3ImageProcessor;
//  CRenderingUnit m_renderingUnit;
//    CVirtualArmar3RobotControl* m_pRobotControl;
//  std::vector<CLocationParticle> m_testLocationParticles;

//    CTracking2DVisualizationScene* m_pTrackingVisualizationScene;
//  float m_testRangeXMin;
//  float m_testRangeXMax;
//  float m_testRangeXDelta;
//  float m_testRangeYMin;
//  float m_testRangeYMax;
//  float m_testRangeYDelta;
//  float m_testRangeAlphaMin;
//  float m_testRangeAlphaMax;
//  float m_testRangeAlphaDelta;

//  bool m_testStoreVisualization;
//};

//#endif /* TESTLINEMODELOBSERVATIONMODEL_H_
