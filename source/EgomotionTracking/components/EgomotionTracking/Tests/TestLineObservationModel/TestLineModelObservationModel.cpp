///*
// * TestLineModelObservationModel.cpp
// *
// *  Created on: 22.03.2013
// *      Author: abyte
// */

//#include "TestLineModelObservationModel.h"
//#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotPlatformTransformation.h>
//#include <EgomotionTracking/components/EgomotionTracking//Helpers/Conversions.h>

//#include <Image/PrimitivesDrawerCV.h>

//#include <iostream>
//#include <fstream>
//#include <time.h>

//CTestObservationModel::CTestObservationModel() :
//    m_pRobotControl(NULL), m_pTrackingVisualizationScene(NULL)
//{
//}

//CTestObservationModel::~CTestObservationModel() {
//}

//void CTestObservationModel::PerformanceTestLineModelObservationModel(CObservationModelInterface* pObservationModel) {
//    if(m_testLocationParticles.size() > 0)
//    {

//        for(std::vector<CLocationParticle>::iterator locationParticleIterator = m_testLocationParticles.begin(); locationParticleIterator != m_testLocationParticles.end(); ++locationParticleIterator)
//        {
//            std::list<CLocationParticle> particles;

//            m_pRobotControl->SetLocation(*locationParticleIterator);
//            m_virtualArmar3ImageProcessor.DispatchImageProcessing();
//            pObservationModel->SetMeasurement(&m_virtualArmar3ImageProcessor.GetEdgeImage(), m_virtualArmar3ImageProcessor.GetGradientOrientations(), m_virtualArmar3ImageProcessor.GetGradients());
//            pObservationModel->SetInputVisualizationImageBackground(&m_virtualArmar3ImageProcessor.GetEdgeImage());

//            const CLocationParticle currentLocationParticle = *locationParticleIterator;


//            for(float deltaY = m_testRangeYMin; deltaY <= m_testRangeYMax; deltaY += m_testRangeYDelta)
//            {
//                for(float deltaX = m_testRangeXMin; deltaX <= m_testRangeXMax; deltaX += m_testRangeXDelta)
//                {



//                    for(float deltaAlpha = m_testRangeAlphaMin; deltaAlpha <= m_testRangeAlphaMax; deltaAlpha += Conversions::DegreeToRadians(1.0))
//                    {
//                        CLocationParticle currentTestParticle = CRobotPlatformTransformation::SetPlatformRelative(currentLocationParticle, deltaX, deltaY, deltaAlpha);
//                        particles.push_back(currentTestParticle);
//                    }
//                }
//            }

//            double tStart = clock();
//            for(std::list<CLocationParticle>::iterator particleIterator = particles.begin(); particleIterator != particles.end(); ++particleIterator)
//            {
//                m_renderingUnit.RenderParticle(*particleIterator, true);
//            }

//            double tAfterRendering = clock();

//            for(std::list<CLocationParticle>::iterator particleIterator = particles.begin(); particleIterator != particles.end(); ++particleIterator)
//            {
//                float weight = pObservationModel->CalculateImportanceWeight(*particleIterator, m_testStoreVisualization);
//                (void)weight;
//            }

//            double tEnd = clock();

//            double secondsRendering = (tAfterRendering - tStart) / CLOCKS_PER_SEC;
//            double secondsImportanceWeighting = (tEnd - tAfterRendering) / CLOCKS_PER_SEC;
//            double secondsAll = (tEnd - tStart) / CLOCKS_PER_SEC;

//            std::cout << "Rendering " << particles.size() << " particles  took: " << secondsRendering << " seconds (" << Conversions::ProportionsToPercentage(secondsRendering/secondsAll) << "%)" << std::endl;
//            std::cout << "Weighting " << particles.size() << " particles  took: " << secondsImportanceWeighting << " seconds (" << Conversions::ProportionsToPercentage(secondsImportanceWeighting/secondsAll) << "%)" << std::endl;
//            std::cout << "All: " << secondsAll << " = " << particles.size()/secondsAll << " fps" << std::endl;

//        }
//    }


//}

//void CTestObservationModel::EvaluateLineModelObservationModel2DLocation(CObservationModelInterface* pObservationModel) {
//    int testLocationParticleIndex = 1;
//    if(m_testLocationParticles.size() > 0)
//    {

//        for(std::vector<CLocationParticle>::iterator locationParticleIterator = m_testLocationParticles.begin(); locationParticleIterator != m_testLocationParticles.end(); ++locationParticleIterator, ++testLocationParticleIndex)
//        {
//            m_pRobotControl->SetLocation(*locationParticleIterator);
//            m_virtualArmar3ImageProcessor.DispatchImageProcessing();
//            pObservationModel->SetMeasurement(&m_virtualArmar3ImageProcessor.GetEdgeImage(), m_virtualArmar3ImageProcessor.GetGradientOrientations(), m_virtualArmar3ImageProcessor.GetGradients());
//            pObservationModel->SetInputVisualizationImageBackground(&m_virtualArmar3ImageProcessor.GetEdgeImage());

//            const CLocationParticle currentLocationParticle = *locationParticleIterator;


//            int frameNumber = 1;
//            std::list<CTestResult> testResults;
//            for(float deltaY = m_testRangeYMin; deltaY <= m_testRangeYMax; deltaY += m_testRangeYDelta)
//            {
//                for(float deltaX = m_testRangeXMin; deltaX <= m_testRangeXMax; deltaX += m_testRangeXDelta)
//                {
//                    float maxWeight = -FLT_MAX;
//                    float maxDeltaAlpha = 0.0;

//                    for(float deltaAlpha = m_testRangeAlphaMin; deltaAlpha <= m_testRangeAlphaMax; deltaAlpha += Conversions::DegreeToRadians(1.0))
//                    {
//                        CLocationParticle currentTestParticle = CRobotPlatformTransformation::SetPlatformRelative(currentLocationParticle, deltaX, deltaY, deltaAlpha);
//                        m_renderingUnit.RenderParticle(currentTestParticle, true);
//                        float weight = pObservationModel->CalculateImportanceWeight(currentTestParticle, m_testStoreVisualization);
//                        if(weight > maxWeight)
//                        {
//                            maxWeight = weight;
//                            maxDeltaAlpha = deltaAlpha;
//                        }

//                    }

//                    testResults.push_back(CTestResult(deltaX, deltaY, maxDeltaAlpha, maxWeight));

//                    CLocationParticle tmp = CRobotPlatformTransformation::SetPlatformRelative(currentLocationParticle, deltaX, deltaY, maxDeltaAlpha);
//                    m_renderingUnit.RenderParticle(tmp, true);
//                    pObservationModel->CalculateImportanceWeight(tmp, m_testStoreVisualization);
//                    StoreEvaluationVisualizationImage(pObservationModel, "img2D", frameNumber, testLocationParticleIndex, deltaX, deltaY, maxDeltaAlpha, maxWeight);
//                    StoreLocationVisualizationImage(&tmp, &currentLocationParticle, "locationVisualization2D", frameNumber++, testLocationParticleIndex);
//                }
//            }
//            StoreTestResultValues(testResults, "results2D", testLocationParticleIndex);
//        }
//    }
//}

//void CTestObservationModel::EvaluateLineModelObservationModelEachDimension(CObservationModelInterface* pObservationModel) {

//    int testLocationParticleIndex = 1;
//    if(m_testLocationParticles.size() > 0)
//    {


//        for(std::vector<CLocationParticle>::iterator locationParticleIterator = m_testLocationParticles.begin(); locationParticleIterator != m_testLocationParticles.end(); ++locationParticleIterator, ++testLocationParticleIndex)
//        {
//            m_pRobotControl->SetLocation(*locationParticleIterator);
//            m_virtualArmar3ImageProcessor.DispatchImageProcessing();
//            pObservationModel->SetInputVisualizationImageBackground(&m_virtualArmar3ImageProcessor.GetEdgeImage());
//            pObservationModel->SetMeasurement(&m_virtualArmar3ImageProcessor.GetEdgeImage(), m_virtualArmar3ImageProcessor.GetGradientOrientations(), m_virtualArmar3ImageProcessor.GetGradients());

//            const CLocationParticle currentLocationParticle = *locationParticleIterator;

//            int i = 1;
//            std::list<CTestResult> testResults;
//            for(float deltaX = m_testRangeXMin; deltaX <= m_testRangeXMax; deltaX += m_testRangeXDelta)
//            {
//                CLocationParticle currentTestParticle = CRobotPlatformTransformation::SetPlatformRelative(currentLocationParticle, deltaX, 0.0, 0.0);
//                m_renderingUnit.RenderParticle(currentTestParticle, true);
//                float weight = pObservationModel->CalculateImportanceWeight(currentTestParticle, m_testStoreVisualization);

//                testResults.push_back(CTestResult(deltaX, 0.0, 0.0, weight));
//                StoreEvaluationVisualizationImage(pObservationModel, "imgDeltaX", i, testLocationParticleIndex, deltaX, 0.0, 0.0, weight);
//                StoreLocationVisualizationImage(&currentTestParticle, &currentLocationParticle, "locationVisualizationX", i++, testLocationParticleIndex);
//            }
//            StoreTestResultValues(testResults, "resultsDeltaX", testLocationParticleIndex);

//            testResults.clear();
//            i = 1;
//            for(float deltaY = m_testRangeYMin; deltaY <= m_testRangeYMax; deltaY += m_testRangeYDelta)
//            {
//                CLocationParticle currentTestParticle = CRobotPlatformTransformation::SetPlatformRelative(currentLocationParticle, 0.0, deltaY, 0.0);
//                m_renderingUnit.RenderParticle(currentTestParticle, true);
//                float weight = pObservationModel->CalculateImportanceWeight(currentTestParticle, m_testStoreVisualization);

//                testResults.push_back(CTestResult(0.0, deltaY, 0.0, weight));
//                StoreEvaluationVisualizationImage(pObservationModel, "imgDeltaY", i, testLocationParticleIndex, 0.0, deltaY, 0.0, weight);
//                StoreLocationVisualizationImage(&currentTestParticle, &currentLocationParticle, "locationVisualizationY", i++, testLocationParticleIndex);
//            }
//            StoreTestResultValues(testResults, "resultsDeltaY", testLocationParticleIndex);

//            testResults.clear();
//            i = 1;
//            for(float deltaAlpha = m_testRangeAlphaMin; deltaAlpha <= m_testRangeAlphaMax; deltaAlpha += m_testRangeAlphaDelta)
//            {
//                CLocationParticle currentTestParticle = CRobotPlatformTransformation::SetPlatformRelative(currentLocationParticle, 0.0, 0.0, deltaAlpha);
//                m_renderingUnit.RenderParticle(currentTestParticle, true);
//                float weight = pObservationModel->CalculateImportanceWeight(currentTestParticle, m_testStoreVisualization);

//                testResults.push_back(CTestResult(0.0, 0.0, deltaAlpha, weight));
//                StoreEvaluationVisualizationImage(pObservationModel, "imgDeltaAlpha", i, testLocationParticleIndex, 0.0, 0.0, deltaAlpha, weight);
//                StoreLocationVisualizationImage(&currentTestParticle, &currentLocationParticle, "locationVisualizationAlpha", i++, testLocationParticleIndex);
//            }
//            StoreTestResultValues(testResults, "resultsDeltaAlpha", testLocationParticleIndex);

//        }
//    }
//}

//void CTestObservationModel::EvaluateLineModelObservationModelEachDimensionWithSensibilities(CObservationModelInterface* pObservationModel)
//{
//    if(m_testLocationParticles.size() > 0)
//    {
//        float sensibilityBackup = pObservationModel->GetSensibility();

//        for(int sensibilityExp = -2; sensibilityExp <= 2; ++sensibilityExp)
//        {
//            int testLocationParticleIndex = 1;

//            float sensibility = pow(2.0f, sensibilityExp);
//            pObservationModel->SetSensibility(sensibility);

//            for(std::vector<CLocationParticle>::iterator locationParticleIterator = m_testLocationParticles.begin(); locationParticleIterator != m_testLocationParticles.end(); ++locationParticleIterator, ++testLocationParticleIndex)
//            {
//                m_pRobotControl->SetLocation(*locationParticleIterator);
//                m_virtualArmar3ImageProcessor.DispatchImageProcessing();
//                pObservationModel->SetMeasurement(&m_virtualArmar3ImageProcessor.GetEdgeImage(), m_virtualArmar3ImageProcessor.GetGradientOrientations(), m_virtualArmar3ImageProcessor.GetGradients());
//                pObservationModel->SetInputVisualizationImageBackground(&m_virtualArmar3ImageProcessor.GetEdgeImage());

//                const CLocationParticle currentLocationParticle = *locationParticleIterator;

//                std::list<CTestResult> testResults;
//                for(float deltaX = m_testRangeXMin; deltaX <= m_testRangeXMax; deltaX += m_testRangeXDelta)
//                {
//                    CLocationParticle currentTestParticle = CRobotPlatformTransformation::SetPlatformRelative(currentLocationParticle, deltaX, 0.0, 0.0);
//                    m_renderingUnit.RenderParticle(currentTestParticle, true);
//                    float weight = pObservationModel->CalculateImportanceWeight(currentTestParticle, m_testStoreVisualization);

//                    testResults.push_back(CTestResult(deltaX, 0.0, 0.0, weight));
//                }
//                StoreTestResultValues(testResults, "resultsDeltaX", testLocationParticleIndex, true, sensibilityExp);

//                testResults.clear();
//                for(float deltaY = m_testRangeYMin; deltaY <= m_testRangeYMax; deltaY += m_testRangeYDelta)
//                {
//                    CLocationParticle currentTestParticle = CRobotPlatformTransformation::SetPlatformRelative(currentLocationParticle, 0.0, deltaY, 0.0);
//                    m_renderingUnit.RenderParticle(currentTestParticle, true);
//                    float weight = pObservationModel->CalculateImportanceWeight(currentTestParticle, m_testStoreVisualization);

//                    testResults.push_back(CTestResult(0.0, deltaY, 0.0, weight));
//                }
//                StoreTestResultValues(testResults, "resultsDeltaY", testLocationParticleIndex, true, sensibilityExp);
//                testResults.clear();
//                for(float deltaAlpha = m_testRangeAlphaMin; deltaAlpha <= m_testRangeAlphaMax; deltaAlpha += m_testRangeAlphaDelta)
//                {
//                    CLocationParticle currentTestParticle = CRobotPlatformTransformation::SetPlatformRelative(currentLocationParticle, 0.0, 0.0, deltaAlpha);
//                    m_renderingUnit.RenderParticle(currentTestParticle, true);
//                    float weight = pObservationModel->CalculateImportanceWeight(currentTestParticle, m_testStoreVisualization);

//                    testResults.push_back(CTestResult(0.0, 0.0, deltaAlpha, weight));
//                }
//                StoreTestResultValues(testResults, "resultsDeltaAlpha", testLocationParticleIndex, true, sensibilityExp);

//            }
//        }
//        pObservationModel->SetSensibility(sensibilityBackup);
//    }
//}

//void CTestObservationModel::StoreTestResultValues(const std::list<CTestResult>& resultValues, const std::string& prefix, int testLocationParticleIndex, bool useStoreSensibilities, int sensibilityExponent) {
//    if(resultValues.size() > 0)
//    {
//        char filename[200];
//        if(!useStoreSensibilities)
//        {
//            sprintf(filename, "%s/%d-%s.csv", OM_TEST_OUTPUT_DIRECTORY, testLocationParticleIndex, prefix.c_str());
//        } else
//        {
//            sprintf(filename, "%s/%d-sensibility%d-%s.csv", OM_TEST_OUTPUT_DIRECTORY, testLocationParticleIndex,sensibilityExponent, prefix.c_str());
//        }
//        std::ofstream outputFile(filename);
//        if(outputFile.is_open())
//        {
//            for(std::list<CTestResult>::const_iterator testResultIterator = resultValues.begin(); testResultIterator != resultValues.end(); ++testResultIterator)
//            {
//                outputFile << testResultIterator->m_deltaX << "," <<
//                              testResultIterator->m_deltaY << "," <<
//                              testResultIterator->m_deltaAlpha << "," <<
//                              testResultIterator->m_weight << std::endl;
//            }
//        }
//        outputFile.close();
//    }
//}

//void CTestObservationModel::StoreEvaluationVisualizationImage(CObservationModelInterface* pObservationModel, const std::string& prefix, int frameIndex, int testLocationParticleIndex, float deltaX, float deltaY, float deltaAlpha, float weight)
//{
//    bool printValues = true;
//    if(m_testStoreVisualization && pObservationModel && pObservationModel->GetVisualizationImage())
//    {
//        CByteImage writeImage(pObservationModel->GetVisualizationImage()->width, pObservationModel->GetVisualizationImage()->height, pObservationModel->GetVisualizationImage()->type);
//        ImageProcessor::CopyImage(pObservationModel->GetVisualizationImage(), &writeImage);

//        char filename[200];
//        sprintf(filename, "%s/%s/%d-%s-%05d.bmp", OM_TEST_OUTPUT_DIRECTORY, OM_TEST_VISUALIZATION_PREFIX,
//                testLocationParticleIndex, prefix.c_str(), frameIndex);

//        if(printValues)
//        {
//            char row1[100];
//            char row2[100];

//            sprintf(row1, "Deviation: x=%+05.0f (mm), y=%+05.0f (mm), alpha=%+03.0f (degree)", deltaX, deltaY, Conversions::RadiansToDegree(deltaAlpha));
//            sprintf(row2, "Importance Weight: %f", weight);

//            PrimitivesDrawerCV::PutText(&writeImage, row1, 10, 15, 0.5, 0.5, 255, 255, 255, 1);
//            PrimitivesDrawerCV::PutText(&writeImage, row2, 10, 35, 0.5, 0.5, 255, 255, 255, 1);

//        }

//        writeImage.SaveToFile(filename);

//    }
//}

//void CTestObservationModel::StoreLocationVisualizationImage(const CLocationState* estimatedLocation, const CLocationState* groundTruthLocation, const std::string& prefix, int frameIndex, int testLocationParticleIndex)
//{
//    if(m_testStoreVisualization && m_pTrackingVisualizationScene)
//    {
//        char filenameSVG[200];
//        sprintf(filenameSVG, "%s/%s/%d-%s-%05d.svg", OM_TEST_OUTPUT_DIRECTORY, OM_TEST_VISUALIZATION_PREFIX,
//                testLocationParticleIndex, prefix.c_str(), frameIndex);
//        char filenameJPG[200];
//        sprintf(filenameJPG, "%s/%s/%d-%s-%05d.jpg", OM_TEST_OUTPUT_DIRECTORY, OM_TEST_VISUALIZATION_PREFIX,
//                testLocationParticleIndex, prefix.c_str(), frameIndex);

//        const CLocationState* groundTruthLocationBefore = m_pTrackingVisualizationScene->GetGroundTruthRobotLocation();
//        const CLocationState* estimatedLocationBefore = m_pTrackingVisualizationScene->GetEstimatedRobotLocation();

//        bool drawGridBefore = m_pTrackingVisualizationScene->GetDrawGrid();
//        m_pTrackingVisualizationScene->SetGroundTruthRobotLocation(groundTruthLocation);
//        m_pTrackingVisualizationScene->SetEstimatedRobotLocation(estimatedLocation);
//        m_pTrackingVisualizationScene->SetDrawGrid(true);
//        m_pTrackingVisualizationScene->Update();

//        //m_pTrackingVisualizationScene->SaveSceneToSVGFile(filenameSVG, "TestObservationModel Location Visualization");
//        m_pTrackingVisualizationScene->ExportSceneToImageFile(filenameJPG, 4, true, false);
//        m_pTrackingVisualizationScene->SetGroundTruthRobotLocation(groundTruthLocationBefore);
//        m_pTrackingVisualizationScene->SetEstimatedRobotLocation(estimatedLocationBefore);
//        m_pTrackingVisualizationScene->SetDrawGrid(drawGridBefore);
//    }
//}




//void CTestObservationModel::AddLocationParticle(const CLocationParticle& locationParticle) {
//    m_testLocationParticles.push_back(locationParticle);
//}

//void CTestObservationModel::ClearLocationParticles() {
//    m_testLocationParticles.clear();
//}

//bool CTestObservationModel::GetLocationParticle(int number, CLocationParticle& locationParticle) {
//    if(number >= 0 && number < (int) m_testLocationParticles.size())
//    {
//        locationParticle = m_testLocationParticles[number];
//        return true;
//    }
//    return false;
//}

//bool CTestObservationModel::SaveTestLocationParticlesToFile(const QString& filename) {

//    QDomDocument testLocationParticlesDomDocument("testLocationParticlesFile");

//    QDomElement rootElement = testLocationParticlesDomDocument.createElement("testLocationParticles");
//    testLocationParticlesDomDocument.appendChild(rootElement);

//    if(m_testLocationParticles.size() > 0)
//    {
//        for(std::vector<CLocationParticle>::iterator locationParticleIterator = m_testLocationParticles.begin(); locationParticleIterator != m_testLocationParticles.end(); ++locationParticleIterator)
//        {
//            rootElement.appendChild(XMLPrimitives::SerializeLocationStateToQDomElement(*locationParticleIterator, "TestLocationParticle"));
//        }
//    }
//    return XMLPrimitives::WriteXMLDocumentToFile(testLocationParticlesDomDocument, filename);
//}

//bool CTestObservationModel::LoadTestLocationParticlesFromFile(const QString& filename) {


//    QDomDocument doc;
//    if(!XMLPrimitives::ReadXMLDocumentFromFile(doc,filename))
//    {
//        return false;
//    }

//    QDomElement rootElement = doc.documentElement();

//    QDomNodeList testLocationsListListQDom = rootElement.elementsByTagName("TestLocationParticle");
//    if(testLocationsListListQDom.isEmpty())
//    {
//        return false;
//    }

//    m_testLocationParticles.clear();
//    for(int i = 0; i < testLocationsListListQDom.size(); ++i)
//    {
//        QDomElement curLocationElement = testLocationsListListQDom.item(i).toElement();
//        if(!curLocationElement.isNull())
//        {
//            CLocationParticle curLocationParticle;
//            if(XMLPrimitives::DeserializeQDomElementToLocationState(curLocationElement, curLocationParticle))
//            {
//                m_testLocationParticles.push_back(curLocationParticle);
//            }
//        }
//    }
//    return true;
//}

//void CTestObservationModel::Set(CParametricLineRenderer* pRenderer, CVirtualArmar3RobotControl* pRobotControl) {

//    m_pRobotControl = pRobotControl;

//    m_virtualArmar3ImageProcessor.SetParametricLineRenderer(pRenderer);
//    m_virtualArmar3ImageProcessor.SetVirtualArmar3RobotControl(pRobotControl);

//    m_renderingUnit.SetParametricLineRenderer(*pRenderer);
//    m_renderingUnit.SetRobotControlInterface(pRobotControl);
//}

//float CTestObservationModel::GetTestRangeAlphaDelta() const {
//    return m_testRangeAlphaDelta;
//}

//void CTestObservationModel::SetTestRangeAlphaDelta(float testRangeAlphaDelta) {
//    m_testRangeAlphaDelta = testRangeAlphaDelta;
//}

//float CTestObservationModel::GetTestRangeAlphaMax() const {
//    return m_testRangeAlphaMax;
//}

//void CTestObservationModel::SetTestRangeAlphaMax(float testRangeAlphaMax) {
//    m_testRangeAlphaMax = testRangeAlphaMax;
//}

//float CTestObservationModel::GetTestRangeAlphaMin() const {
//    return m_testRangeAlphaMin;
//}

//void CTestObservationModel::SetTestRangeAlphaMin(float testRangeAlphaMin) {
//    m_testRangeAlphaMin = testRangeAlphaMin;
//}

//float CTestObservationModel::GetTestRangeXDelta() const {
//    return m_testRangeXDelta;
//}

//void CTestObservationModel::SetTestRangeXDelta(float testRangeXDelta) {
//    m_testRangeXDelta = testRangeXDelta;
//}

//float CTestObservationModel::GetTestRangeXMax() const {
//    return m_testRangeXMax;
//}

//void CTestObservationModel::SetTestRangeXMax(float testRangeXMax) {
//    m_testRangeXMax = testRangeXMax;
//}

//float CTestObservationModel::GetTestRangeXMin() const {
//    return m_testRangeXMin;
//}

//void CTestObservationModel::SetTestRangeXMin(float testRangeXMin) {
//    m_testRangeXMin = testRangeXMin;
//}

//float CTestObservationModel::GetTestRangeYDelta() const {
//    return m_testRangeYDelta;
//}

//void CTestObservationModel::SetTestRangeYDelta(float testRangeYDelta) {
//    m_testRangeYDelta = testRangeYDelta;
//}

//float CTestObservationModel::GetTestRangeYMax() const {
//    return m_testRangeYMax;
//}

//void CTestObservationModel::SetTestRangeYMax(float testRangeYMax) {
//    m_testRangeYMax = testRangeYMax;
//}

//float CTestObservationModel::GetTestRangeYMin() const {
//    return m_testRangeYMin;
//}

//void CTestObservationModel::SetTestRangeYMin(float testRangeYMin) {
//    m_testRangeYMin = testRangeYMin;
//}

//bool CTestObservationModel::isTestStoreVisualization() const {
//    return m_testStoreVisualization;
//}

//void CTestObservationModel::SetTestStoreVisualization(bool testStoreVisualization) {
//    m_testStoreVisualization = testStoreVisualization;
//}


//CTracking2DVisualizationScene* CTestObservationModel::GetTrackingVisualizationScene() const
//{
//    return m_pTrackingVisualizationScene;
//}

//void CTestObservationModel::SetTrackingVisualizationScene(CTracking2DVisualizationScene* pTrackingVisualizationScene)
//{
//    m_pTrackingVisualizationScene = pTrackingVisualizationScene;
//}
