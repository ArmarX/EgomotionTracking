#include "SimpleLocationFeedbackControl.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>
#include <algorithm>

CSimpleLocationFeedbackControl::CSimpleLocationFeedbackControl()
{
}

bool CSimpleLocationFeedbackControl::DispatchFeedbackControlImplementation()
{
    if (m_pLocationEstimator == NULL || !m_pLocationEstimator->EstimationIsValid())
    {
        COUT_WARNING << "m_pLocationEstimator == NULL || !m_pLocationEstimator->EstimationIsValid()" << std::endl;
        Stop();
        return false;
    }
    const CLocationState& currentLocationState = m_pLocationEstimator->GetLocationEstimate();
    const float deltaT = GetDispatchingDeltaT();
    const float deltaTInSeconds = Conversions::MillisecondsToSeconds(deltaT);

    float deltaAlpha = m_targetLocation.GetAlpha() - currentLocationState.GetAlpha();
    if (deltaAlpha > M_PI)
    {
        deltaAlpha -= 2 * M_PI;
    }
    else if (deltaAlpha < -M_PI)
    {
        deltaAlpha += 2 * M_PI;
    }

    const float deltaXWorld = m_targetLocation.GetX() - currentLocationState.GetX();
    const float deltaYWorld = m_targetLocation.GetY() - currentLocationState.GetY();

    float currentVelocityX = 0.0, currentVelocityY = 0.0, currentVelocityAlpha = 0.0;
    GetCurrentRobotControlState(currentVelocityX, currentVelocityY, currentVelocityAlpha);

    float velocityX = 0.0;
    float velocityY = 0.0;
    float velocityAlpha = 0.0;

    if (fabs(deltaAlpha) > GetToleranceAlpha() || sqrt(deltaXWorld * deltaXWorld + deltaYWorld * deltaYWorld) > GetToleranceD() ||
        currentVelocityX > lfc_maxStopVelocity || currentVelocityY > lfc_maxStopVelocity)
    {
        const float curAvgTimeslotPlatformAngle = Conversions::NormalizeRadiansTo2PiInterval(currentLocationState.GetAlpha() + velocityAlpha * (Conversions::MillisecondsToSeconds(deltaT)) / 2);

        const float s = sin(curAvgTimeslotPlatformAngle);
        const float c = cos(curAvgTimeslotPlatformAngle);
        const float deltaXPlatform = c * deltaXWorld + s * deltaYWorld;
        const float deltaYPlatform = -s * deltaXWorld + c * deltaYWorld;

        velocityX = CalculateTargetVelocity(deltaXPlatform, deltaTInSeconds, currentVelocityX, GetMaxAccelerationX(), m_nextRobotControlState.GetMaxAbsVelocityX(), 50, 20);
        velocityY = CalculateTargetVelocity(deltaYPlatform, deltaTInSeconds, currentVelocityY, GetMaxAccelerationY(), m_nextRobotControlState.GetMaxAbsVelocityY(), 50, 20);
        velocityAlpha = CalculateTargetVelocity(deltaAlpha, deltaTInSeconds, currentVelocityAlpha, GetMaxAccelerationAlpha(), m_nextRobotControlState.GetMaxAbsVelocityAlpha(), Conversions::DegreeToRadians(10), Conversions::DegreeToRadians(5));

        SetRobotControlState(velocityX, velocityY, velocityAlpha);
    }
    else
    {
        Stop();
    }


    return true;
}

float CSimpleLocationFeedbackControl::CalculateTargetVelocity(float distance, float deltaTInSeconds, float currentVelocity, float maxAcceleration, float maxAbsVelocity, float linearControlDistanceThreshold, float linearControlVelocityThreshold)
{
    /* do we have to brake? */
    const float breakingDistance = 1000 * Calculations::Square(currentVelocity / 1000) / (maxAcceleration / 1000);

    const float breakingFactor = 1.5;
    const float accelerateFactor = 3;

    const float maxDeltaV = deltaTInSeconds * maxAcceleration;

    float velocity = 0.0;
    if (fabs(distance) < linearControlDistanceThreshold && currentVelocity < linearControlVelocityThreshold)
    {
        velocity = distance / 4;
    }
    else if (fabs(distance) < breakingFactor * breakingDistance)
    {
        const float deltaV = -Calculations::Sign(currentVelocity) * maxDeltaV;
        if (fabs(deltaV) > fabs(currentVelocity))
        {
            velocity = 0.0;
        }
        else
        {
            velocity = currentVelocity + deltaV;
        }
    }
    else if (fabs(distance) > accelerateFactor * breakingDistance)
    {
        velocity = currentVelocity + Calculations::Sign(distance) * maxDeltaV;
    }
    else
    {
        velocity = currentVelocity;
    }

    return Calculations::SaturizeAbsolute(velocity, maxAbsVelocity);
}
