#pragma once

#include <EgomotionTracking/components/EgomotionTracking/RobotControl/ControllableRobotControlInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/LocationEstimator/LocationEstimator.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/RobotControlState.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Profiling.h>


const float lfc_toleranceD = 25; //2,5cm
const float lfc_toleranceAlpha = Conversions::DegreeToRadians(3);
const float lfc_maxStopVelocity = 40.0; //mm/s

class CLocationFeedbackControlInterface
{
public:
    CLocationFeedbackControlInterface();

    bool DispatchFeedbackControl();

    void Stop();
    void Start();

    const CControllableRobotControlInterface* GetControllableRobot() const;
    void SetControllableRobot(CControllableRobotControlInterface* pControllableRobot);
    const CLocationEstimator* GetLocationEstimator() const;
    void SetLocationEstimator(const CLocationEstimator* pLocationEstimator);

    const CLocationState& GetTargetLocation() const;
    void SetTargetLocation(const CLocationState& targetLocation);
    void SetMaximumAbsoluteVelocities(float maxVelocityX, float maxVelocityY, float maxVelocityAlpha);
    void SetMaximumAbsoluteAccelerations(float maxAccelerationX, float maxAccelerationY, float maxAccelerationAlpha);
    void SetTolerance(float toleranceD, float toleranceAlpha);

    float GetMaxAccelerationX() const;
    float GetMaxAccelerationY() const;
    float GetMaxAccelerationAlpha() const;
protected:
    bool SetRobotControlState(float velocityX, float velocityY, float velocityAlpha);
    bool GetCurrentRobotControlState(float& velocityX, float& velocityY, float& velocityAlpha);

    virtual bool DispatchFeedbackControlImplementation() = 0;
    float GetDispatchingDeltaT() const;
    float GetToleranceD() const;
    float GetToleranceAlpha() const;

    const CLocationEstimator* m_pLocationEstimator;
    CLocationState m_targetLocation;
    CRobotControlState m_nextRobotControlState;
    CRobotControlState m_currentRobotControlState;

private:
    CControllableRobotControlInterface* m_pControllableRobot;
    CProfiling m_dispatchingProfiler;
    float m_dispatchingDeltaT;

    float m_toleranceD;
    float m_toleranceAlpha;

    float m_maxAccelerationX;
    float m_maxAccelerationY;
    float m_maxAccelerationAlpha;

    bool m_isMoving;
};

