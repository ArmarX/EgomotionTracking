#pragma once

#include "LocationFeedbackControlInterface.h"

class CSimpleLocationFeedbackControl : public CLocationFeedbackControlInterface
{
public:
    CSimpleLocationFeedbackControl();
    bool DispatchFeedbackControlImplementation() override;

protected:
    float CalculateTargetVelocity(float distance, float deltaTInSeconds, float currentVelocity, float maxAcceleration,  float maxAbsVelocity, float linearControlDistanceThreshold, float linearControlVelocityThreshold);
};

