#include "LocationFeedbackControlInterface.h"

CLocationFeedbackControlInterface::CLocationFeedbackControlInterface() :
    m_pLocationEstimator(NULL), m_pControllableRobot(NULL), m_dispatchingDeltaT(0.0),
    m_toleranceD(lfc_toleranceD), m_toleranceAlpha(lfc_toleranceAlpha),
    m_maxAccelerationX(FLT_MAX), m_maxAccelerationY(FLT_MAX), m_maxAccelerationAlpha(FLT_MAX),
    m_isMoving(false)
{
    m_dispatchingProfiler.StartProfiling();
}

bool CLocationFeedbackControlInterface::DispatchFeedbackControl()
{
    if (m_pControllableRobot == NULL)
    {
        return false;
    }

    if (m_isMoving)
    {
        m_currentRobotControlState = m_pControllableRobot->GetRobotControlState();
        m_dispatchingDeltaT = m_dispatchingProfiler.GetElapsedMS();
        m_dispatchingProfiler.StartProfiling();
        return DispatchFeedbackControlImplementation();
    }
    return true;
}

const CControllableRobotControlInterface* CLocationFeedbackControlInterface::GetControllableRobot() const
{
    return m_pControllableRobot;
}

void CLocationFeedbackControlInterface::SetControllableRobot(CControllableRobotControlInterface* pControllableRobot)
{
    m_pControllableRobot = pControllableRobot;
}

const CLocationEstimator* CLocationFeedbackControlInterface::GetLocationEstimator() const
{
    return m_pLocationEstimator;
}

void CLocationFeedbackControlInterface::SetLocationEstimator(const CLocationEstimator* pLocationEstimator)
{
    m_pLocationEstimator = pLocationEstimator;
}

const CLocationState& CLocationFeedbackControlInterface::GetTargetLocation() const
{
    return m_targetLocation;
}

void CLocationFeedbackControlInterface::SetTargetLocation(const CLocationState& targetLocation)
{
    m_targetLocation = targetLocation;
}


void CLocationFeedbackControlInterface::Stop()
{
    SetRobotControlState(0.0, 0.0, 0.0);
    m_isMoving = false;
}

void CLocationFeedbackControlInterface::Start()
{
    m_isMoving = true;
}

void CLocationFeedbackControlInterface::SetMaximumAbsoluteVelocities(float maxVelocityX, float maxVelocityY, float maxVelocityAlpha)
{
    m_nextRobotControlState.SetMaxAbsVelocityX(maxVelocityX);
    m_nextRobotControlState.SetMaxAbsVelocityY(maxVelocityY);
    m_nextRobotControlState.SetMaxAbsVelocityAlpha(maxVelocityAlpha);
}

void CLocationFeedbackControlInterface::SetMaximumAbsoluteAccelerations(float maxAccelerationX, float maxAccelerationY, float maxAccelerationAlpha)
{
    m_maxAccelerationX = maxAccelerationX;
    m_maxAccelerationY = maxAccelerationY;
    m_maxAccelerationAlpha = maxAccelerationAlpha;
}

bool CLocationFeedbackControlInterface::SetRobotControlState(float velocityX, float velocityY, float velocityAlpha)
{
    if (m_pControllableRobot == NULL)
    {
        return false;
    }
    m_nextRobotControlState.SetPlatformVelocitiesKeepXYRatio(velocityX, velocityY, velocityAlpha);
    m_pControllableRobot->SetTargetRobotControlState(m_nextRobotControlState);
    return true;
}

bool CLocationFeedbackControlInterface::GetCurrentRobotControlState(float& velocityX, float& velocityY, float& velocityAlpha)
{
    if (m_pControllableRobot == NULL)
    {
        return false;
    }
    velocityX = m_pControllableRobot->GetRobotControlState().GetPlatformVelocityX();
    velocityY = m_pControllableRobot->GetRobotControlState().GetPlatformVelocityY();
    velocityAlpha = m_pControllableRobot->GetRobotControlState().GetPlatformVelocityAlpha();
    return true;
}

float CLocationFeedbackControlInterface::GetDispatchingDeltaT() const
{
    return m_dispatchingDeltaT;
}

float CLocationFeedbackControlInterface::GetToleranceD() const
{
    return m_toleranceD;
}

float CLocationFeedbackControlInterface::GetToleranceAlpha() const
{
    return m_toleranceAlpha;
}

float CLocationFeedbackControlInterface::GetMaxAccelerationX() const
{
    return m_maxAccelerationX;
}

float CLocationFeedbackControlInterface::GetMaxAccelerationY() const
{
    return m_maxAccelerationY;
}

float CLocationFeedbackControlInterface::GetMaxAccelerationAlpha() const
{
    return m_maxAccelerationAlpha;
}
