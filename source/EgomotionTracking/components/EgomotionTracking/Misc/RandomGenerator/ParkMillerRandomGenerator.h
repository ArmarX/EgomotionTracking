#pragma once

#include "RandomGenerator.h"

class CParkMillerRandomGenerator : public CRandomGenerator
{
public:
    CParkMillerRandomGenerator();

    void Randomize() override;
protected:
    int randImplementation() const override;
    mutable unsigned int m_seed;
};

