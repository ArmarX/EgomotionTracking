#include "ParkMillerRandomGenerator.h"
#include <stdlib.h>

#include <iostream>
CParkMillerRandomGenerator::CParkMillerRandomGenerator()
{
    Randomize();
}

void CParkMillerRandomGenerator::Randomize()
{
    CRandomGenerator::Randomize();
    m_seed = CRandomGenerator::randImplementation();
}

int CParkMillerRandomGenerator::randImplementation() const
{
    /* taken from http://www.firstpr.com.au/dsp/rand31/ */
    long unsigned int hi, lo;
    lo = 16807 * (m_seed & 0xFFFF);
    hi = 16807 * (m_seed >> 16);

    lo += (hi & 0x7FFF) << 16;
    lo += hi >> 15;

    if (lo > 0x7FFFFFFF)
    {
        lo -= 0x7FFFFFFF;
    }

    return static_cast<int>(m_seed = (long)lo);
}
