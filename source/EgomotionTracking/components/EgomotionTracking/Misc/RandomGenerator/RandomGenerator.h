/*
 * RandomGenerator.h
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#pragma once
#include <iostream>

class CRandomGenerator
{
public:
    CRandomGenerator();
    virtual ~CRandomGenerator();

    double GetUniformRandomNumber() const;
    double GetGaussianRandomNumber() const;

    inline double GetGaussianRandomNumber(float sigma) const
    {
        return GetGaussianRandomNumber() * sigma;
    }

    inline double GetUniformRandomNumber(float min, float max) const
    {
        return min + (max - min) * GetUniformRandomNumber();
    }

    virtual void Randomize();
protected:
    virtual int randImplementation() const;

private:
    static bool globalSeedRandomized;

};

