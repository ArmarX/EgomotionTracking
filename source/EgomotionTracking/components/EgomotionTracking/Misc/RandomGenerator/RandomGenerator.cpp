/*
* RandomGenerator.cpp
*
*  Created on: 14.03.2013
*      Author: abyte
*/

#include "RandomGenerator.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

bool CRandomGenerator::globalSeedRandomized = false;

CRandomGenerator::CRandomGenerator()
{

}

CRandomGenerator::~CRandomGenerator()
{
}

double CRandomGenerator::GetUniformRandomNumber() const
{
    return randImplementation() / double(RAND_MAX);
}

/* taken from IVT */
double CRandomGenerator::GetGaussianRandomNumber() const
{
    static int next_gaussian = 0;
    static double saved_gaussian_value;

    double fac, rsq, v1, v2;

    if (next_gaussian == 0)
    {
        do
        {
            v1 = 2.0 * GetUniformRandomNumber() - 1.0;
            v2 = 2.0 * GetUniformRandomNumber() - 1.0;
            rsq = v1 * v1 + v2 * v2;
        }
        while (rsq >= 1.0 || rsq == 0.0);

        fac = sqrt(-2.0 * log(rsq) / rsq);
        saved_gaussian_value = v1 * fac;
        next_gaussian = 1;

        return v2 * fac;
    }
    else
    {
        next_gaussian = 0;
        return saved_gaussian_value;
    }
}

void CRandomGenerator::Randomize()
{
    if (!globalSeedRandomized)
    {
        srand(time(NULL));
        globalSeedRandomized = true;
    }
}

int CRandomGenerator::randImplementation() const
{
    return rand();
}
