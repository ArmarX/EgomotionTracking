#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/HeadConfiguration.h>
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotControlInterface.h>
#include <Inventor/SbMatrix.h>

class CRobotKinematicInterface
{
public:
    CRobotKinematicInterface();
    virtual SbMatrix CalculateTransformationPlatformToCamera(const CHeadConfiguration& headConfiguration, CRobotControlInterface::CCameraID cameraId) const = 0;
};

