#pragma once

#include "RobotKinematicInterface.h"
class CSimpleArmar3RobotKinematic : public CRobotKinematicInterface
{
public:
    CSimpleArmar3RobotKinematic();
    SbMatrix CalculateTransformationPlatformToCamera(const CHeadConfiguration& headConfiguration, CRobotControlInterface::CCameraID cameraId) const override;

protected:
    void InitTransformations();
    SbMatrix m_transformationPlatformToCenterOfArms;
    SbMatrix m_transformationCenterOfArmsToHeadBase;
    SbMatrix m_transformationHeadBaseToLeftCamera;
    SbMatrix m_transformationHeadBaseToRightCamera;
};

