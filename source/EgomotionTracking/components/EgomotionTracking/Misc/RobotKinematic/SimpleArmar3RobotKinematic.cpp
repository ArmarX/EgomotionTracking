#include "SimpleArmar3RobotKinematic.h"

CSimpleArmar3RobotKinematic::CSimpleArmar3RobotKinematic()
{
    InitTransformations();
}

SbMatrix CSimpleArmar3RobotKinematic::CalculateTransformationPlatformToCamera(const CHeadConfiguration& headConfiguration, CRobotControlInterface::CCameraID cameraId) const
{
    SbMatrix transformationNeckRotation = headConfiguration.CalculateNeckRotationTransformationMatrix();

    SbMatrix transformationPlatformToCenterOfArms = m_transformationPlatformToCenterOfArms;
    SbMatrix transformationCenterOfArmsToHeadBase = m_transformationCenterOfArmsToHeadBase;
    SbMatrix transformationHeadBaseToRightCamera = m_transformationHeadBaseToRightCamera;
    SbMatrix transformationHeadBaseToLeftCamera = m_transformationHeadBaseToLeftCamera;

    SbMatrix transformationToNeck = transformationPlatformToCenterOfArms.multLeft(transformationCenterOfArmsToHeadBase.multLeft(transformationNeckRotation));

    if (cameraId == CRobotControlInterface::eLeftCamera)
    {
        return transformationToNeck.multLeft(transformationHeadBaseToLeftCamera);
    }
    else if (cameraId == CRobotControlInterface::eRightCamera)
    {
        return transformationToNeck.multLeft(transformationHeadBaseToRightCamera);
    }
    return SbMatrix::identity();
}

void CSimpleArmar3RobotKinematic::InitTransformations()
{
    m_transformationPlatformToCenterOfArms.setTranslate(SbVec3f(0.0, 123.0, 1375.0));
    m_transformationCenterOfArmsToHeadBase.setTranslate(SbVec3f(0.0, 0.0, 118.0));
    m_transformationHeadBaseToLeftCamera.setTransform(SbVec3f(-46.5, 100.0, 174.5), SbRotation(SbVec3f(1.0, 0.0, 0.0), -M_PI / 2), SbVec3f(1.0, 1.0, 1.0));
    m_transformationHeadBaseToRightCamera.setTransform(SbVec3f(46.5, 100.0, 174.5), SbRotation(SbVec3f(1.0, 0.0, 0.0), -M_PI / 2), SbVec3f(1.0, 1.0, 1.0));
}
