#include "Thread.h"

CThread::CThread()
{
}

bool CThread::Start()
{
    return pthread_create(&m_thread, NULL, CThread::run, static_cast<void*>(this)) == 0;
}

bool CThread::Stop()
{
    return pthread_cancel(m_thread) == 0;
}

bool CThread::Join()
{
    return pthread_join(m_thread, NULL) == 0;
}

void* CThread::run(void* pThreadPtr)
{
    CThread* pThread = static_cast<CThread*>(pThreadPtr);
    pThread->Execute();
    return NULL;
}
