#pragma once

#include <pthread.h>

class CThread
{
public:
    CThread();

    bool Start();
    bool Stop();
    bool Join();
protected:
    virtual void Execute() = 0;
private:
    static void* run(void* pThreadPtr);
    pthread_t m_thread;
};

