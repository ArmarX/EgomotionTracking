#pragma once

#include <pthread.h>

class CMutex
{
public:
    CMutex();
    void LockBlocking();
    bool LockNonBlocking();
    bool Unlock();
protected:
    pthread_mutex_t m_mutex;
    pthread_mutexattr_t m_attributes;
};

