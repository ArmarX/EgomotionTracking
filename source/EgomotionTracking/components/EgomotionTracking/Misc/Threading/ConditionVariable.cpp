#include "ConditionVariable.h"
#include <assert.h>
#include <iostream>
CConditionVariable::CConditionVariable()
{
    pthread_cond_init(&m_conditionVariable, NULL);
}

void CConditionVariable::Wait()
{
    assert(pthread_cond_wait(&m_conditionVariable, &m_mutex) == 0);
}

void CConditionVariable::Signal()
{
    pthread_cond_signal(&m_conditionVariable);
}
