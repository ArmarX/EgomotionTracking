#include "DispatchingThread.h"

CDispatchingThread::CDispatchingThread()
{
    m_isRunning = true;
    m_dispatchingCondition = false;
    Start();
}

CDispatchingThread::~CDispatchingThread()
{
    m_isRunning = false;

    WaitUntilIdle();
}

bool CDispatchingThread::Dispatch()
{
    if (IsRunning())
    {
        WaitUntilIdle();

        m_dispatchingStartConditionVariable.LockBlocking();
        m_dispatchingCondition = true;
        m_dispatchingStartConditionVariable.Signal();
        m_dispatchingStartConditionVariable.Unlock();
        return true;
    }
    return false;
}

bool CDispatchingThread::IsRunning()
{
    return m_isRunning;
}

void CDispatchingThread::Execute()
{
    while (m_isRunning)
    {
        m_dispatchingStartConditionVariable.LockBlocking();

        while (m_dispatchingCondition == false)
        {
            m_dispatchingStartConditionVariable.Wait();
        }
        m_dispatchingStartConditionVariable.Unlock();

        DispatchingFunction();

        m_dispatchingFinishedConditionVariable.LockBlocking();
        m_dispatchingCondition = false;
        m_dispatchingFinishedConditionVariable.Signal();
        m_dispatchingFinishedConditionVariable.Unlock();
    }
}

void CDispatchingThread::WaitUntilIdle()
{
    m_dispatchingFinishedConditionVariable.LockBlocking();
    while (m_dispatchingCondition)
    {
        m_dispatchingFinishedConditionVariable.Wait();
    }
    m_dispatchingFinishedConditionVariable.Unlock();
}
