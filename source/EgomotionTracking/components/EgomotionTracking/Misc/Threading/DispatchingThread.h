#pragma once

#include "Thread.h"
#include "ConditionVariable.h"

class CDispatchingThread : public CThread
{
public:
    CDispatchingThread();
    virtual ~CDispatchingThread();
    bool Dispatch();
    bool IsRunning();

    void WaitUntilIdle();

protected:
    virtual void DispatchingFunction() = 0;
    void Execute() override;


    CConditionVariable m_dispatchingStartConditionVariable;
    CConditionVariable m_dispatchingFinishedConditionVariable;

    bool m_isRunning;
    bool m_dispatchingCondition;

};

