#include "Mutex.h"
#include <assert.h>

CMutex::CMutex()
{
    pthread_mutexattr_settype(&m_attributes, PTHREAD_MUTEX_ERRORCHECK);
    pthread_mutexattr_init(&m_attributes);
    pthread_mutex_init(&m_mutex, &m_attributes);
}

void CMutex::LockBlocking()
{
    assert(pthread_mutex_lock(&m_mutex) == 0);
}

bool CMutex::LockNonBlocking()
{
    return pthread_mutex_trylock(&m_mutex) == 0;
}

bool CMutex::Unlock()
{
    assert(pthread_mutex_unlock(&m_mutex) == 0);
    return true;
    return pthread_mutex_unlock(&m_mutex) == 0;
}
