#pragma once

#include "Mutex.h"

class CConditionVariable : public CMutex
{
public:
    CConditionVariable();
    void Wait();
    void Signal();
protected:
    pthread_cond_t m_conditionVariable;
};

