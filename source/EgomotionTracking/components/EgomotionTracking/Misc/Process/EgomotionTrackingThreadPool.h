#pragma once

#include <EgomotionTracking/components/EVP/Foundation/Process/ThreadPool.h>
#include <EgomotionTracking/components/EVP/Foundation/Process/PoolThread.h>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>

class CEgomotionTrackingThreadPool : public EVP::Threading::CThreadPool
{
public:
    CEgomotionTrackingThreadPool(unsigned int TotalThreads);

protected:
    void OnBeginFinalizePool() const override;
    void OnEndFinalizePool() const override;
    void OnBeginSynchronize() const override;
    void OnEndSynchronize() const override;
    void OnThreadOnIdle(const EVP::Threading::CPoolThread* pPoolThread) const override;
    void OnThreadOnWorking(const EVP::Threading::CPoolThread* pPoolThread, const EVP::Threading::CExecutionUnit* pExecutionUnit) const override;

};

