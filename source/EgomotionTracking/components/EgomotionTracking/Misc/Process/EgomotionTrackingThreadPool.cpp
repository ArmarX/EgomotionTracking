#include "EgomotionTrackingThreadPool.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>

CEgomotionTrackingThreadPool::CEgomotionTrackingThreadPool(unsigned int TotalThreads) :
    CThreadPool(TotalThreads)
{
    //SetDispatchingEvents(true);
}

void CEgomotionTrackingThreadPool::OnBeginFinalizePool() const
{
}

void CEgomotionTrackingThreadPool::OnEndFinalizePool() const
{
}

void CEgomotionTrackingThreadPool::OnBeginSynchronize() const
{
    COUT_DEBUG1 << "OnBeginSynchronize" << std::endl;
}

void CEgomotionTrackingThreadPool::OnEndSynchronize() const
{
    COUT_DEBUG2 << "OnEndSynchronize" << std::endl;
}

void CEgomotionTrackingThreadPool::OnThreadOnIdle(const EVP::Threading::CPoolThread* pPoolThread) const
{
}

void CEgomotionTrackingThreadPool::OnThreadOnWorking(const EVP::Threading::CPoolThread* pPoolThread, const EVP::Threading::CExecutionUnit* pExecutionUnit) const
{
}
