#include "SessionRecorder.h"
#include <QDateTime>

CSessionRecorder::CSessionRecorder() :
    m_pGroundTruthLocationEstimator(NULL),
    m_pRobotControl(NULL),
    m_pTrackinControl(NULL),
    m_recordingIndex(0)
{
    ResetRecording();
}


CSessionRecorder::~CSessionRecorder()
{

    WriteXMLDocument();

}


bool CSessionRecorder::DispatchRecording()
{
    if (m_pRobotControl == NULL)
    {
        COUT_ERROR << "m_pRobotControl == NULL" << std::endl;
        return false;
    }

    m_pRobotControl->DispatchRobot();

    if (m_pGroundTruthLocationEstimator == NULL)
    {
        COUT_ERROR << "m_pGroundTruthLocationEstimator == NULL" << std::endl;
        //return false;
    }
    else
    {
        m_pGroundTruthLocationEstimator->DispatchLocalisation();
    }

    if (!SaveXMLRecordElement())
    {
        COUT_ERROR << "Save XML Record Element failed!" << std::endl;
        return false;
    }

    if (!SaveViewImages())
    {
        COUT_ERROR << "Save View Images failed!" << std::endl;
        return false;
    }
    ++m_recordingIndex;
    return true;
}


void CSessionRecorder::SetGroundTruthLocationEstimator(CLocationEstimator* pGroundTruthLocationEstimator)
{
    m_pGroundTruthLocationEstimator = pGroundTruthLocationEstimator;
}

void CSessionRecorder::SetRobotControl(CRobotControlInterface* pRobotControl)
{
    m_pRobotControl = pRobotControl;
}

void CSessionRecorder::SetSessionDirectory(const std::string& directoryPath)
{
    m_sessionDirectoryPath = directoryPath;
}

void CSessionRecorder::SetTrackingControl(const CTrackingControl* pTrackinControl)
{
    m_pTrackinControl = pTrackinControl;
}

const CRobotControlInterface* CSessionRecorder::GetRobotControl()
{
    return m_pRobotControl;
}

void CSessionRecorder::ResetRecording()
{
    if (m_recordingIndex > 0)
    {
        WriteXMLDocument();
    }

    m_sessionXMLDocument = QDomDocument(SESSION_RECORDING_XML_FILE_DOCTYPE);
    QDomElement root = m_sessionXMLDocument.createElement(SESSION_RECORDING_XML_TAG_ROOT);
    m_sessionXMLDocument.appendChild(root);

    QDateTime dateTime = QDateTime::currentDateTime();
    QString dateTimeString = dateTime.toString();
    root.setAttribute("dateAndTime", dateTimeString);
    if (m_pTrackinControl != NULL)
    {
        float rollError = 0, pitchError = 0, yawError = 0;
        m_pTrackinControl->GetNeckRPYErrorEstimate(rollError, pitchError, yawError);
        root.setAttribute("initialNeckErrorRoll", rollError);
        root.setAttribute("initialNeckErrorPitch", pitchError);
        root.setAttribute("initialNeckErrorYaw", yawError);
    }

    if (m_pRobotControl != NULL)
    {
        QDomElement cameraProjectionsRoot = m_sessionXMLDocument.createElement(SESSION_RECORDING_XML_TAG_CAMERAPROJECTIONSROOT);
        root.appendChild(cameraProjectionsRoot);
        for (int currentCameraId = 0; currentCameraId < CRobotControlInterface::eCAMERAIDEND; ++currentCameraId)
        {
            QString cameraProjectionName = QString("cameraProjection%1").arg(currentCameraId);
            QDomElement cameraProjectionElement = XMLPrimitives::SerializeSbMatrixToQDomElement(m_pRobotControl->GetCameraProjectionMatrixGL(static_cast<CRobotControlInterface::CCameraID>(currentCameraId)), cameraProjectionName);
            cameraProjectionsRoot.appendChild(cameraProjectionElement);
        }
    }


    QDomElement recordingsRoot = m_sessionXMLDocument.createElement(SESSION_RECORDING_XML_TAG_RECORDINGSROOT);
    root.appendChild(recordingsRoot);

    m_recordingIndex = 0;
    m_recordingProfiler.StartProfiling();
}

int CSessionRecorder::GetRecordingIndex() const
{
    return m_recordingIndex;
}

bool CSessionRecorder::WriteXMLDocument()
{
    if (m_recordingIndex > 0)
    {
        char filename[300];
        snprintf(filename, 299, "%s/" SESSION_RECORDING_SESSION_FILENAME, m_sessionDirectoryPath.c_str());

        return XMLPrimitives::WriteXMLDocumentToFile(m_sessionXMLDocument, filename);
    }
    return false;
}

bool CSessionRecorder::SaveViewImages()
{
    char filename[300];
    for (int currentCameraId = 0; currentCameraId < CRobotControlInterface::eCAMERAIDEND; ++currentCameraId)
    {

        snprintf(filename, 299, "%s/" SESSION_RECORDING_VIEWIMAGE_FILENAME_FORMAT, m_sessionDirectoryPath.c_str(), currentCameraId, m_recordingIndex);
        if (!m_pRobotControl->GetCameraImage(static_cast<CRobotControlInterface::CCameraID>(currentCameraId)).SaveToFile(filename))
        {
            std::cout << "filename:" << filename << std::endl;
            return false;
        }
    }
    return true;
}

bool CSessionRecorder::SaveXMLRecordElement()
{
    QDomElement rootM = m_sessionXMLDocument.documentElement();
    if (rootM.isNull())
    {
        COUT_ERROR << "rootM.isNull()" << std::endl;
        return false;
    }

    QDomElement recordingsRoot = rootM.firstChildElement(SESSION_RECORDING_XML_TAG_RECORDINGSROOT);
    if (recordingsRoot.isNull())
    {
        COUT_ERROR << "recordingsRoot.isNull()" << std::endl;
        return false;
    }

    QDomElement recordElement = m_sessionXMLDocument.createElement(SESSION_RECORDING_XML_TAG_RECORDINGELEMENT);
    recordElement.setAttribute("recordingIndex", m_recordingIndex);
    recordElement.setAttribute("ellapsedMS", m_recordingProfiler.GetElapsedMS());

    QDomElement robotControlStateElement = XMLPrimitives::SerializeRobotControlStateToQDomElement(m_pRobotControl->GetRobotControlState(), "robotControlState");
    recordElement.appendChild(robotControlStateElement);

    QDomElement headConfigurationElement = XMLPrimitives::SerializeHeadConfigurationToQDomElement(m_pRobotControl->GetHeadConfiguration(), "headConfiguration");
    recordElement.appendChild(headConfigurationElement);

    if (m_pGroundTruthLocationEstimator != NULL)
    {
        QDomElement groundTruthLocationElement = XMLPrimitives::SerializeLocationStateToQDomElement(m_pGroundTruthLocationEstimator->GetLocationEstimate(), "groundTruthLocation");
        recordElement.appendChild(groundTruthLocationElement);
    }

    for (int currentCameraId = 0; currentCameraId < CRobotControlInterface::eCAMERAIDEND; ++currentCameraId)
    {
        QString platformToCameraTransformationElementName = QString("platformToCameraTransformation%1").arg(currentCameraId);
        QDomElement platformToCameraTransformationElement = XMLPrimitives::SerializeSbMatrixToQDomElement(m_pRobotControl->GetPlatformToCameraTransformation(static_cast<CRobotControlInterface::CCameraID>(currentCameraId)), platformToCameraTransformationElementName);
        recordElement.appendChild(platformToCameraTransformationElement);
    }

    recordingsRoot.appendChild(recordElement);
    recordingsRoot.setAttribute("numberOfRecords", m_recordingIndex + 1);

    return true;
}
