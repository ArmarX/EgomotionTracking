#pragma once

#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotControlInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/LocationEstimator/LocationEstimator.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/XMLPrimitives.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Profiling.h>
#include <EgomotionTracking/components/EgomotionTracking/TrackingControl.h>

#define SESSION_RECORDING_SESSION_FILENAME "session.xml"
#define SESSION_RECORDING_VIEWIMAGE_FILENAME_FORMAT "images/viewimage%02d-%06d.bmp"
#define SESSION_RECORDING_XML_FILE_DOCTYPE "ROBOTRECORDINGSESSIONXMLFILE"

#define SESSION_RECORDING_XML_TAG_ROOT "SessionRoot"
#define SESSION_RECORDING_XML_TAG_RECORDINGSROOT "RecordingElementsRoot"
#define SESSION_RECORDING_XML_TAG_CAMERAPROJECTIONSROOT "CameraProjectionsRoot"
#define SESSION_RECORDING_XML_TAG_RECORDINGELEMENT "RecordingElement"

class CSessionRecorder
{
public:
    CSessionRecorder();
    virtual ~CSessionRecorder();
    bool DispatchRecording();

    void SetGroundTruthLocationEstimator(CLocationEstimator* pGroundTruthLocationEstimator);
    void SetRobotControl(CRobotControlInterface* pRobotControl);
    void SetSessionDirectory(const std::string& directoryPath);
    void SetTrackingControl(const CTrackingControl* pTrackinControl);
    const CRobotControlInterface* GetRobotControl();

    void ResetRecording();
    int GetRecordingIndex() const;

    bool WriteXMLDocument();
protected:
    bool SaveViewImages();
    bool SaveXMLRecordElement();

    CLocationEstimator* m_pGroundTruthLocationEstimator;
    CRobotControlInterface* m_pRobotControl;
    const CTrackingControl* m_pTrackinControl;

    std::string m_sessionDirectoryPath;

    int m_recordingIndex;
    QDomDocument m_sessionXMLDocument;
    CProfiling m_recordingProfiler;

};

