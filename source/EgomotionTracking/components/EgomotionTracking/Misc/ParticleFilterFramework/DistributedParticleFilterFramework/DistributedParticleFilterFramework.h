/*
 * DistributedParticleFilterFramework.h
 *
 *  Created on: 02.01.2013
 *      Author: abyte
 */

#pragma once

#include <EgomotionTracking/components/EVP/Foundation/Process/ThreadPool.h>
#include <EgomotionTracking/components/EVP/Foundation/Process/ExecutionUnit.h>
#include <EgomotionTracking/components/EVP/GlobalSettings.h>

#include "DPFNode.h"

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>
#include <list>


class CDistributedParticleFilterFramework
{
public:
    CDistributedParticleFilterFramework(uint nParticles, uint nDimension, uint nNodes, uint nImbalanceThreshold, EVP::Threading::CThreadPool* pThreadPool);
    virtual ~CDistributedParticleFilterFramework();

    void GetGlobalMeanConfiguration(PF_REAL* pGlobalMeanConfiguration);
    PF_REAL ParticleFilter(PF_REAL* pResultMeanConfiguration, PF_REAL sigmaFactor = 1);

    /* Not possible in constructor, because of the virtual behaviour of GenerateNode */
    void InitializeNodes();

    PF_REAL CalculateEffectiveNumberOfParticles();

protected:

    void NormalizeLocalWeights();
    void CalculateNumberOfParticlesToBeResampled();
    void CalculateGlobalMean();

    virtual void RunLoadBalancer();

    virtual void InitializeConfiguration()
    {
        for (uint i = 0; i < m_nDimension; ++i)
        {
            m_pGlobalMeanConfiguration[i] = 0.0;
        }
    }
    virtual void GenerateNode(CDPFNode** pNewNode) = 0;

    class CBalanceNode
    {
    public:
        CBalanceNode(uint index, int localImbalance) : m_index(index), m_localImbalance(localImbalance) {}
        uint m_index;
        int m_localImbalance;

        bool operator< (const CBalanceNode& bn) const
        {
            return m_localImbalance < bn.m_localImbalance;
        }

    };

    uint m_nParticles;
    uint m_nDimension;
    uint m_nNodes;

    EVP::Threading::CThreadPool* m_pThreadPool;
    CDPFNode** m_ppDPFNodes;

    PF_REAL** m_ppLocalMeans;
    PF_REAL* m_pLocalWeights;

    PF_REAL m_totalWeight;

    PF_REAL* m_pGlobalMeanConfiguration;
    PF_REAL* m_pGlobalLastConfiguration;

    uint* m_pNParticlesToBeResampled;
    int* m_pNLocalImbalancePerNode;

    uint m_loadImbalanceFactor;
    uint m_nImbalanceThreshold;

    bool m_isInitialized;
};

