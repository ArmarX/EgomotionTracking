/*
 * DPFNode.cpp
 *
 *  Created on: 02.01.2013
 *      Author: abyte
 */

#include "DPFNode.h"

//XXX
#include <iostream>

CDPFNode::CDPFNode(uint nParticles, uint nDimension) :
    m_nParticles(nParticles), m_nDimension(nDimension)
{

    m_pLocalMeanConfiguration = new PF_REAL[nDimension];
    m_pGlobalLastConfiguration = new PF_REAL[nDimension];
    m_pGlobalMeanConfiguration = new PF_REAL[nDimension];

    m_pSigma = new PF_REAL[nDimension];
    m_pLowerLimit = new PF_REAL[nDimension];
    m_pUpperLimit = new PF_REAL[nDimension];

    uint i;

    // allocate memory
    m_ppS = new PF_REAL*[nParticles];
    for (i = 0; i < nParticles; i++)
    {
        m_ppS[i] = new PF_REAL[nDimension];
    }

    m_ppSTemp = new PF_REAL*[nParticles];
    for (i = 0; i < nParticles; i++)
    {
        m_ppSTemp[i] = new PF_REAL[nDimension];
    }

    m_pC = new PF_REAL[nParticles];
    m_pPi = new PF_REAL[nParticles];
    m_pWeights = new PF_REAL[nParticles];

    for (i = 0; i < nParticles; i++)
    {
        m_pC[i] = 0.0;
        m_pPi[i] = 0.0;
        m_pWeights[i] = 0.0;
    }

    m_randomGenerator.Randomize();
    m_nActiveParticles = 0;
}

CDPFNode::~CDPFNode()
{
    delete [] m_pLocalMeanConfiguration;
    delete [] m_pGlobalLastConfiguration;
    delete [] m_pGlobalMeanConfiguration;
    delete [] m_pSigma;
    delete [] m_pLowerLimit;
    delete [] m_pUpperLimit;

    uint i;

    for (i = 0; i < m_nParticles; i++)
    {
        delete [] m_ppS[i];
    }
    delete [] m_ppS;

    for (i = 0; i < m_nParticles; i++)
    {
        delete [] m_ppSTemp[i];
    }
    delete [] m_ppSTemp;

    delete [] m_pC;
    delete [] m_pPi;
    delete [] m_pWeights;

}

bool CDPFNode::Execute()
{
    Resample(m_sigmaFactor);
    m_nActiveParticles = m_nParticlesToBeResampled;

    // apply bayesian measurement weighting

    for (uint i = 0; i < m_nActiveParticles; ++i)
    {
        // update model (calculate forward kinematics)
        UpdateModel(m_ppS[i]);

        // evaluate likelihood function (compare edges,...)
        m_pWeights[i] = CalculateImportanceWeight();
    }

    CalculateProbabilities();
    CalculateLocalMean();

    return true;
}

void CDPFNode::SetGlobal(uint nParticlesToBeResampled, const PF_REAL* pGlobalMeanConfiguration, PF_REAL sigmaFactor)
{
    m_nParticlesToBeResampled = nParticlesToBeResampled;
    m_sigmaFactor = sigmaFactor;

    for (uint i = 0; i < m_nDimension; ++i)
    {
        m_pGlobalLastConfiguration[i] = m_pGlobalMeanConfiguration[i];
        m_pGlobalMeanConfiguration[i] = pGlobalMeanConfiguration[i];
    }
}


void CDPFNode::GetLocalMeanConfiguration(PF_REAL* pMean) const
{
    for (uint i = 0; i < m_nDimension; ++i)
    {
        pMean[i] = m_pLocalMeanConfiguration[i];
    }
}

PF_REAL CDPFNode::GetLocalTotalWeight() const
{
    return m_cTotal;
}

uint CDPFNode::GetNumberOfActiveParticles() const
{
    return m_nActiveParticles;
}

uint CDPFNode::PickBaseSample() const
{
    PF_REAL choice = m_randomGenerator.GetUniformRandomNumber() * m_cTotal;

    int low = 0;
    int high = m_nActiveParticles;

    while (high > (low + 1))
    {
        int middle = (high + low) / 2;

        if (choice > m_pC[middle])
        {
            low = middle;
        }
        else
        {
            high = middle;
        }
    }

    return low;
}

PF_REAL CDPFNode::CalculateProbabilityForConfiguration(
    const PF_REAL* pConfiguration)
{
    UpdateModel(pConfiguration);
    return CalculateImportanceWeight();
}

void CDPFNode::CalculateProbabilities()
{
    m_cTotal = 0.0;
    for (uint i = 0; i < m_nActiveParticles; ++i)
    {
        m_pC[i] = m_cTotal;
        m_cTotal += m_pWeights[i];
    }

    /* Normalize m_pPi*/
    const PF_REAL factor = 1 / m_cTotal;
    for (uint i = 0; i < m_nActiveParticles; ++i)
    {
        m_pPi[i] = m_pWeights[i] * factor;
    }
}

void CDPFNode::BalanceCopyIn(CDPFNode* pCopyOutDPFNode, uint nParticles)
{


    for (uint i = 0; i < nParticles; ++i)
    {
        uint currentCopyInID = m_nActiveParticles + i;
        uint currentCopyOutId = pCopyOutDPFNode->m_nActiveParticles - nParticles + i;

        memcpy(m_ppS[currentCopyInID], pCopyOutDPFNode->m_ppS[currentCopyOutId], sizeof(PF_REAL)*m_nDimension);
    }

    memcpy(m_pWeights + m_nActiveParticles, pCopyOutDPFNode->m_pWeights + pCopyOutDPFNode->m_nActiveParticles - nParticles, sizeof(PF_REAL)*nParticles);

    m_nActiveParticles += nParticles;
    pCopyOutDPFNode->m_nActiveParticles -= nParticles;
    CalculateProbabilities();
    pCopyOutDPFNode->CalculateProbabilities();

}

PF_REAL CDPFNode::CalculateSumOfSquaredNormalizedWeights(PF_REAL globalTotalWeight)
{
    PF_REAL sum = 0.0;
    for (uint currentWeightIndex = 0; currentWeightIndex < m_nActiveParticles; ++currentWeightIndex)
    {
        const PF_REAL currentNormalizedWeight = (m_pWeights[currentWeightIndex] / globalTotalWeight);
        sum += currentNormalizedWeight * currentNormalizedWeight;
    }
    return sum;
}

void CDPFNode::CalculateLocalMean()
{
    for (uint currentDimension = 0; currentDimension < m_nDimension; ++currentDimension)
    {
        m_pLocalMeanConfiguration[currentDimension] = 0.0;
    }

    for (uint currentParticle = 0; currentParticle < m_nActiveParticles; ++currentParticle)
    {
        for (uint currentDimension = 0; currentDimension < m_nDimension; ++currentDimension)
        {
            m_pLocalMeanConfiguration[currentDimension] += m_ppS[currentParticle][currentDimension] * m_pPi[currentParticle];
        }
    }
}
