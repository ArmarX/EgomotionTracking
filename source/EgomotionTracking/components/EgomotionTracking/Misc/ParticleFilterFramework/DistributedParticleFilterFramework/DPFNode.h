/*
 * DPFNode.h
 *
 *  Created on: 02.01.2013
 *      Author: abyte
 */

#pragma once

#include <EgomotionTracking/components/EVP/Foundation/Process/ExecutionUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/RandomGenerator/ParkMillerRandomGenerator.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>

class CDPFNode : public EVP::Threading::CExecutionUnit
{
public:
    CDPFNode(uint nParticles, uint nDimension);
    ~CDPFNode() override;

    bool Execute() override;

    void SetGlobal(uint nParticlesToBeResampled, const PF_REAL* pGlobalMeanConfiguration, PF_REAL sigmaFactor = 1.0);

    void GetLocalMeanConfiguration(PF_REAL* pMeanConfiguration) const;
    PF_REAL GetLocalTotalWeight() const;

    uint GetNumberOfActiveParticles() const;

    PF_REAL CalculateProbabilityForConfiguration(const PF_REAL* pConfiguration);

    void BalanceCopyIn(CDPFNode* pFatDPFNode, uint nParticles);

    PF_REAL CalculateSumOfSquaredNormalizedWeights(PF_REAL globalTotalWeight);

protected:
    virtual void UpdateModel(const PF_REAL* pConfiguration) = 0;
    /* Resample m_nParticlesToBeResampled particles */
    virtual void Resample(double dSigmaFactor) = 0;
    virtual PF_REAL CalculateImportanceWeight() = 0;

    uint PickBaseSample() const;
    void CalculateProbabilities();

    void CalculateLocalMean();

    CParkMillerRandomGenerator m_randomGenerator;

    uint m_nParticlesToBeResampled;
    PF_REAL m_sigmaFactor;

    uint m_nParticles;
    uint m_nActiveParticles;
    uint m_nDimension;

    PF_REAL* m_pLocalMeanConfiguration;
    PF_REAL* m_pGlobalMeanConfiguration;
    PF_REAL* m_pGlobalLastConfiguration;

    PF_REAL** m_ppS;
    PF_REAL** m_ppSTemp;

    PF_REAL* m_pC;
    PF_REAL m_cTotal;
    PF_REAL* m_pPi;
    PF_REAL* m_pWeights;

    PF_REAL* m_pSigma;
    PF_REAL* m_pLowerLimit;
    PF_REAL* m_pUpperLimit;
};

