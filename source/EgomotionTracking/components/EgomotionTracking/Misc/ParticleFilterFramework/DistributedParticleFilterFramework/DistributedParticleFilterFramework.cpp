/*
 * DistributedParticleFilterFramework.cpp
 *
 *  Created on: 02.01.2013
 *      Author: abyte
 */

#include "DistributedParticleFilterFramework.h"

//XXX
#include <iostream>
#include <algorithm>

CDistributedParticleFilterFramework::CDistributedParticleFilterFramework(uint nParticles, uint nDimension, uint nNodes, uint nImbalanceThreshold, EVP::Threading::CThreadPool* pThreadPool) :
    m_nParticles(nParticles), m_nDimension(nDimension), m_nNodes(nNodes), m_pThreadPool(pThreadPool), m_nImbalanceThreshold(nImbalanceThreshold), m_isInitialized(false)
{

    m_ppDPFNodes = new CDPFNode *[m_nNodes];
    for (uint i = 0; i < m_nNodes; ++i)
    {
        m_ppDPFNodes[i] = NULL;
    }

    m_ppLocalMeans = new PF_REAL *[m_nNodes];
    for (uint i = 0; i < m_nNodes; ++i)
    {
        m_ppLocalMeans[i] = new PF_REAL[m_nDimension];
    }

    m_pLocalWeights = new PF_REAL[m_nNodes];
    m_pNParticlesToBeResampled = new uint[m_nNodes];
    m_pNLocalImbalancePerNode = new int[m_nNodes];

    m_pGlobalMeanConfiguration = new PF_REAL[m_nDimension];
    m_pGlobalLastConfiguration = new PF_REAL[m_nDimension];

}

CDistributedParticleFilterFramework::~CDistributedParticleFilterFramework()
{
    /*

        RELEASE_ARRAY(m_pLocalWeights);
        RELEASE_ARRAY(m_pNParticlesToBeResampled);
        RELEASE_ARRAY(m_pNLocalImbalancePerNode);

        RELEASE_ARRAY(m_pGlobalMeanConfiguration);
        RELEASE_ARRAY(m_pGlobalLastConfiguration);

        for(uint i = 0; i < m_nNodes; ++i)
        {
            RELEASE_OBJECT(m_ppDPFNodes[i]);
        }
        RELEASE_ARRAY(m_ppDPFNodes);

        for(uint i = 0; i < m_nNodes; ++i)
        {
            RELEASE_ARRAY(m_ppLocalMeans[i]);
        }
        RELEASE_ARRAY(m_ppLocalMeans);
        */
}

PF_REAL CDistributedParticleFilterFramework::ParticleFilter(
    PF_REAL* pResultMeanConfiguration, PF_REAL sigmaFactor)
{

    if (m_isInitialized)
    {
        CalculateNumberOfParticlesToBeResampled();
        /* send global information to nodes */
        for (uint i = 0; i < m_nNodes; ++i)
        {
            m_ppDPFNodes[i]->SetGlobal(m_pNParticlesToBeResampled[i], m_pGlobalMeanConfiguration, sigmaFactor);
        }

        /* local Importance Sampling/Weighting */
        for (uint i = 0; i < m_nNodes; ++i)
        {
            m_pThreadPool->DispatchExecutionUnit(m_ppDPFNodes[i], true);
        }
        m_pThreadPool->Synchronize();

        /* calculate global values */
        m_totalWeight = 0.0;
        m_loadImbalanceFactor = 0;
        for (uint i = 0; i < m_nNodes; ++i)
        {
            m_ppDPFNodes[i]->GetLocalMeanConfiguration(m_ppLocalMeans[i]);
            PF_REAL tmpWeight = m_ppDPFNodes[i]->GetLocalTotalWeight();
            m_pLocalWeights[i] = tmpWeight;

            uint activeParticles = m_ppDPFNodes[i]->GetNumberOfActiveParticles();
            int localImbalance = activeParticles - m_nParticles / m_nNodes;
            m_loadImbalanceFactor += std::abs(localImbalance);
            m_pNLocalImbalancePerNode[i] = localImbalance;

            //  std::cout << i << ": ActiveParticles: " << m_ppDPFNodes[i]->GetNumberOfActiveParticles()  <<
            //                  "   LocalImbalance: " << localImbalance << std::endl;
            m_totalWeight += tmpWeight;
        }
        //std::cout << "ImbalanceFactor: " << m_loadImbalanceFactor << std::endl;
        NormalizeLocalWeights();

        CalculateGlobalMean();
        GetGlobalMeanConfiguration(pResultMeanConfiguration);

        if (m_loadImbalanceFactor > m_nImbalanceThreshold)
        {
            RunLoadBalancer();
        }

        return m_ppDPFNodes[0]->CalculateProbabilityForConfiguration(pResultMeanConfiguration);
    }

    return 0.0;
}

void CDistributedParticleFilterFramework::InitializeNodes()
{
    m_totalWeight = 0.0;
    for (uint i = 0; i < m_nNodes; ++i)
    {
        RELEASE_OBJECT(m_ppDPFNodes[i]);
        GenerateNode(&m_ppDPFNodes[i]);

        m_pLocalWeights[i] = 1.0;
        m_totalWeight += m_pLocalWeights[i];
    }
    NormalizeLocalWeights();

    CalculateNumberOfParticlesToBeResampled();
    InitializeConfiguration();

    /* set GlobalMean to initialization Value in the DPFNode Implementation for first SetGlobal */
    m_ppDPFNodes[0]->GetLocalMeanConfiguration(m_pGlobalMeanConfiguration);


    m_isInitialized = true;
}

void CDistributedParticleFilterFramework::GetGlobalMeanConfiguration(
    PF_REAL* pGlobalMeanConfiguration)
{
    for (uint i = 0; i < m_nDimension; ++i)
    {
        pGlobalMeanConfiguration[i] = m_pGlobalMeanConfiguration[i];
    }
}

void CDistributedParticleFilterFramework::CalculateNumberOfParticlesToBeResampled()
{
    uint m_nTotalResamples = 0;
    for (uint i = 0; i < m_nNodes; ++i)
    {
        uint curResamples = static_cast<uint>(m_pLocalWeights[i] * m_nParticles);
        m_nTotalResamples += curResamples;
        m_pNParticlesToBeResampled[i] = curResamples;
    }

    uint particlesPerThreadThreshold = static_cast<uint>(static_cast<PF_REAL>(m_nParticles / m_nNodes) + 0.5);
    uint residualResamples = m_nParticles - m_nTotalResamples;
    uint i = 0;
    while (residualResamples > 0)
    {
        uint curThread = i % m_nParticles;
        if (m_pNParticlesToBeResampled[curThread] <= particlesPerThreadThreshold)
        {
            m_pNParticlesToBeResampled[curThread]++;
            residualResamples--;
        }
        ++i;
    }
}



void CDistributedParticleFilterFramework::NormalizeLocalWeights()
{
    const PF_REAL factor = 1 / m_totalWeight;
    for (uint i = 0; i < m_nNodes; ++i)
    {
        m_pLocalWeights[i] *= factor;
    }
}

void CDistributedParticleFilterFramework::CalculateGlobalMean()
{
    for (uint i = 0; i < m_nDimension; ++i)
    {
        m_pGlobalLastConfiguration[i] = m_pGlobalMeanConfiguration[i];
        m_pGlobalMeanConfiguration[i] = 0.0;


    }

    for (uint i = 0; i < m_nNodes; ++i)
    {
        for (uint j = 0; j < m_nDimension; ++j)
        {
            m_pGlobalMeanConfiguration[j] += m_ppLocalMeans[i][j] * m_pLocalWeights[i];
        }
    }

}

PF_REAL CDistributedParticleFilterFramework::CalculateEffectiveNumberOfParticles()
{
    PF_REAL totalSumOfSquaredWeights = 0.0;
    for (uint currentNodeIndex = 0; currentNodeIndex < m_nNodes; ++currentNodeIndex)
    {
        totalSumOfSquaredWeights += m_ppDPFNodes[currentNodeIndex]->CalculateSumOfSquaredNormalizedWeights(m_totalWeight);
    }
    return 1 / totalSumOfSquaredWeights;
}

void CDistributedParticleFilterFramework::RunLoadBalancer()
{

    //std::cout << "####LOADBALANCE####" << std::endl;
    std::list<CBalanceNode> nodeBalanceList;

    for (uint i = 0; i < m_nNodes; ++i)
    {
        nodeBalanceList.push_back(CBalanceNode(i, m_pNLocalImbalancePerNode[i]));
    }
    nodeBalanceList.sort();

    std::list<CBalanceNode>::iterator frontBalanceNodeIterator = nodeBalanceList.begin();
    std::list<CBalanceNode>::iterator backBalanceNodeIterator = nodeBalanceList.end();
    --backBalanceNodeIterator;

    while (frontBalanceNodeIterator != backBalanceNodeIterator &&
           frontBalanceNodeIterator->m_localImbalance < 0 && backBalanceNodeIterator->m_localImbalance > 0)
    {
        uint nParticlesToCopy;

        if (-frontBalanceNodeIterator->m_localImbalance > backBalanceNodeIterator->m_localImbalance)
        {
            nParticlesToCopy = backBalanceNodeIterator->m_localImbalance;
        }
        else
        {

            nParticlesToCopy = -frontBalanceNodeIterator->m_localImbalance;
        }

        if (nParticlesToCopy > 0)
        {
            //std::cout << "ParticlesToCopy: " << nParticlesToCopy << std::endl;
            uint indexCopyIn = frontBalanceNodeIterator->m_index;
            uint indexCopyOut = backBalanceNodeIterator->m_index;

            m_ppDPFNodes[indexCopyIn]->BalanceCopyIn(m_ppDPFNodes[indexCopyOut], nParticlesToCopy);

        }

        frontBalanceNodeIterator->m_localImbalance += nParticlesToCopy;
        backBalanceNodeIterator->m_localImbalance -= nParticlesToCopy;

        if (frontBalanceNodeIterator->m_localImbalance >= 0)
        {
            ++frontBalanceNodeIterator;
        }
        if (backBalanceNodeIterator->m_localImbalance <= 0)
        {
            --backBalanceNodeIterator;
        }
    }

    PF_REAL totalWeightTMP = 0.0;
    for (uint i = 0; i < m_nNodes; ++i)
    {
        PF_REAL tmpWeight = m_ppDPFNodes[i]->GetLocalTotalWeight();
        m_pLocalWeights[i] = tmpWeight;
        totalWeightTMP += tmpWeight;
    }

    NormalizeLocalWeights();
}
