#pragma once


#include <EgomotionTracking/components/EgomotionTrackingConfiguration/TrackingConfiguration.h>

#include <EgomotionTracking/components/EgomotionTracking/Misc/RandomGenerator/ParkMillerRandomGenerator.h>
#include "SamplingAndWeightingExecutionUnit.h"

#include <EgomotionTracking/components/EVP/Foundation/Process/ThreadPool.h>

#include <vector>

class CParallelParticleFilterFramework
{
public:
    CParallelParticleFilterFramework(int nParticles, int nDimension, int nThreads, EVP::Threading::CThreadPool* pThreadPool);
    ~CParallelParticleFilterFramework();


    PF_REAL ParticleFilter(PF_REAL* pResultConfiguration, PF_REAL dSigmaFactor = 1);
    virtual void GetBestConfiguration(PF_REAL* pBestConfiguration);
    virtual void GetMeanConfiguration(PF_REAL* pMeanConfiguration);
    virtual void GetThresholdedMeanConfiguration(PF_REAL* pThresholdedMeanConfiguration);

    virtual void InitializeExecutionUnits() = 0;
    int GetNumberOfActiveParticles() const;
    void SetNumberOfActiveParticles(int nActiveParticles, bool resampleParticles = true);

    PF_REAL GetNEff()
    {
        return m_nEff;
    }

    void SetUseResamplingThreshold(bool useResamplingThreshold);
    void SetRelativeNEffResamplingThreshold(PF_REAL resamplingThreshold);

    void SetUseSwarmOptimization(bool useSwarmOptimization);
    void SetSwarmVelocityGlobalBestFactor(PF_REAL swarmVelocityGlobalBestFactor);
    void SetSwarmVelocityLocalBestFactor(PF_REAL swarmVelocityLocalBestFactor);
    void SetSwarmOptimizationBestWeightThreshold(PF_REAL weightThreshold);
    void SetSwarmLocalBestAgingFactor(PF_REAL swarmLocalBestAgingFactor);

    void SetUseAnnealing(bool useAnnealing);
    void SetNumberOfAnnealingLevels(unsigned int annealingLevels);
    void SetAnnealingBestWeightThreshold(PF_REAL weightThreshold);
    void ResetAnnealingLevel();
    unsigned int GetCurrentAnnealingLevel();


    void SetThresholdedMeanFactor(PF_REAL thresholdedMeanFactor);

    enum CEstimationMode
    {
        eMeanEstimate,
        eBestEstimate,
        eThresholdedMeanEstimate
    };

    void SetEstimationMode(const CEstimationMode& estimationMode);

    void CalculateBoundingRect(PF_REAL* pMin, PF_REAL* pMax) const;

protected:
    // protected methods
    int PickBaseSample();
    void CalculateBestAndMean();
    void CalculateThresholdedMean();
    void UpdateLocalBest();

private:
    inline void Resample(int numberOfNewParticles);
    inline void SwarmOptimization();

protected:

    // virtual methods (framework methods to be implemented: design pattern "framwork" with "template methods")
    virtual void CalculateFinalProbabilities() { }

    CParkMillerRandomGenerator m_randomGenerator;
    EVP::Threading::CThreadPool* m_pThreadPool;
    std::vector<CSamplingAndWeightingExecutionUnit*> m_executionUnits;

    // protected attributes
    PF_REAL* m_pMeanConfiguration;
    PF_REAL* m_pLastMeanConfiguration;
    PF_REAL* m_pBestConfiguration;
    PF_REAL* m_pThresholdedMean;

    PF_REAL m_maxNormalizedWeight;
    PF_REAL m_maxWeight;

    // Particle related attributes

    // Particle filter dimension
    const int m_nDimension;
    // Number of particles
    int m_nParticles;
    // Number of active particles
    int m_nActiveParticles;
    // Number of threads
    int m_nThreads;
    // Total particle weight
    PF_REAL m_cTotal;
    // Particle attributes/filter variables array
    PF_REAL** m_ppS;
    PF_REAL** m_ppSTemp;

    // Summed particle weight array
    PF_REAL* m_pC;
    // Particle weight array
    PF_REAL* m_pPi;
    // Normalized particle weight array
    PF_REAL* m_pPiNormalized;
    PF_REAL* m_pPiNormalizedTmp;
    PF_REAL* m_pTemp;
    PF_REAL m_nEff;

    bool m_useResamplingThreshold;
    PF_REAL m_relativeNEffResamplingThreshold;

    bool m_particlesAreResampled;

    bool m_useSwarmOptimization;
    PF_REAL** m_ppSwarmSBest;
    PF_REAL* m_pSwarmPiBest;
    PF_REAL m_swarmVelocityGlobalBestFactor;
    PF_REAL m_swarmVelocityLocalBestFactor;
    PF_REAL m_swarmLocalBestAgingFactor;
    PF_REAL m_swarmOptimizationBestWeightThreshold;

    bool m_useAnnealing;
    unsigned int m_numberOfAnnealingLevels;
    unsigned int m_currentAnnealingLevel;
    PF_REAL m_annealingBestWeightThreshold;

    CEstimationMode m_estimationMode;
    PF_REAL m_thresholdedMeanFactor;

    bool m_resampleBasedOnMahalanobisDistance;
    bool* m_particleCanBeResampled;

    /* CODE CHANGES: HOMOGENITY DISSIMILARITY. */

    float* m_pDissimilarityMax;
    float* m_pDissimilarityMin;
    int m_numberOfParticlesSetup;
    float* m_pAlphaSetup;
    bool m_useDissimilarity;
    float* m_pDissimilarity;
};



