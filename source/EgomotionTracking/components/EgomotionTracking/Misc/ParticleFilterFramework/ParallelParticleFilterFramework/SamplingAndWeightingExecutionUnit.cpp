#include "SamplingAndWeightingExecutionUnit.h"


CSamplingAndWeightingExecutionUnit::CSamplingAndWeightingExecutionUnit(int nDimension)
    : m_nDimension(nDimension),
      m_nParticles(0),
      m_annealingLevel(0),
      m_ppFirstParticle(NULL),
      m_pFirstParticleWeight(NULL)
{
    m_randomGenerator.Randomize();
}

bool CSamplingAndWeightingExecutionUnit::Execute()
{
    PF_REAL** ppCurrentParticle = m_ppFirstParticle;
    PF_REAL* pCurrentParticleWeight = m_pFirstParticleWeight;

    m_pCurrentAlphaSetup = m_pAlphaSetup;
    m_pCurrentDissimilarity = m_pDissimilarity;

    if (m_cameraFrameUpdateRequired)
    {
        *m_pMaxDissimilarity = FLT_MIN;
        *m_pMinDissimilarity = FLT_MAX;
    }

    for (int i = 0; i < m_nParticles; ++i)
    {
        Sample(ppCurrentParticle);
        *pCurrentParticleWeight = CalculateParticleWeight(ppCurrentParticle, false);
        ++ppCurrentParticle;
        ++pCurrentParticleWeight;

        ++m_pCurrentAlphaSetup;
        ++m_pCurrentDissimilarity;
    }

    return true;
}

void CSamplingAndWeightingExecutionUnit::SetParticles(PF_REAL** ppFirstParticle, PF_REAL* pFirstParticleWeight, int nNumberOfParticles,
        float* pAlphaSetup, float* pDissimilarity, bool useDissimilarity, float* pMinDissimilarity,
        float* pMaxDissimilarity)
{
    m_ppFirstParticle = ppFirstParticle;
    m_pFirstParticleWeight = pFirstParticleWeight;
    m_nParticles = nNumberOfParticles;

    m_pAlphaSetup = pAlphaSetup;
    m_pDissimilarity = pDissimilarity;
    m_useDissimilarity = useDissimilarity;

    m_pMinDissimilarity = pMinDissimilarity;
    m_pMaxDissimilarity = pMaxDissimilarity;
}

void CSamplingAndWeightingExecutionUnit::SetGlobalValues(const PF_REAL* pGlobalMeanConfiguration, const PF_REAL* pGlobalLastConfiguration, const PF_REAL* pGlobalBestConfiguration)
{
    m_pGlobalMeanConfiguration = pGlobalMeanConfiguration;
    m_pGlobalLastConfiguration = pGlobalLastConfiguration;
    m_pGlobalBestConfiguration = pGlobalBestConfiguration;
}

void CSamplingAndWeightingExecutionUnit::SetSigmaFactor(PF_REAL sigmaFactor)
{
    m_sigmaFactor = sigmaFactor;
}

void CSamplingAndWeightingExecutionUnit::SetAnnealingLevel(unsigned int annealingLevel)
{
    m_annealingLevel = annealingLevel;
}

void CSamplingAndWeightingExecutionUnit::setCameraFrameUpdateRequired(bool cameraFrameUpdateRequired)
{
    m_cameraFrameUpdateRequired = cameraFrameUpdateRequired;
}



