#pragma once

#include <EgomotionTracking/components/EVP/Foundation/Process/ExecutionUnit.h>

#include <EgomotionTracking/components/EgomotionTrackingConfiguration/TrackingConfiguration.h>
#include <EgomotionTracking/components/EgomotionTrackingConfiguration/ExternalDefinitions.h>

#include <EgomotionTracking/components/EgomotionTracking/Misc/RandomGenerator/ParkMillerRandomGenerator.h>

class CSamplingAndWeightingExecutionUnit : public EVP::Threading::CExecutionUnit
{

public:

    CSamplingAndWeightingExecutionUnit(int nDimension);

    bool Execute() override;

    void SetParticles(PF_REAL** ppFirstParticle, PF_REAL* pFirstParticleWeight,
                      int nNumberOfParticles, float* pAlphaSetup, float* pDissimilarity, bool useDissimilarity, float* pMinDissimilarity, float* pMaxDissimilarity);
    void SetGlobalValues(const PF_REAL* pGlobalMeanConfiguration, const PF_REAL* pGlobalLastConfiguration, const PF_REAL* pGlobalBestConfiguration);
    inline PF_REAL CalculateParticleWeight(PF_REAL** ppParticle)
    {
        return CalculateParticleWeight(ppParticle, true);
    }

    void SetSigmaFactor(PF_REAL sigmaFactor);
    void SetAnnealingLevel(unsigned int annealingLevel);

    void setCameraFrameUpdateRequired(bool cameraFrameUpdateRequired);

protected:
    virtual void Sample(PF_REAL** ppParticle) = 0;
    virtual PF_REAL CalculateParticleWeight(PF_REAL** ppParticle, bool externCall) = 0;

    int m_nDimension;
    int m_nParticles;
    unsigned int m_annealingLevel;

    PF_REAL** m_ppFirstParticle;
    PF_REAL* m_pFirstParticleWeight;

    const PF_REAL* m_pGlobalMeanConfiguration;
    const PF_REAL* m_pGlobalLastConfiguration;
    const PF_REAL* m_pGlobalBestConfiguration;

    PF_REAL m_sigmaFactor;

    CParkMillerRandomGenerator m_randomGenerator;

    float* m_pAlphaSetup;
    float* m_pDissimilarity;

    float* m_pCurrentAlphaSetup;
    float* m_pCurrentDissimilarity;

    bool m_useDissimilarity;

    float* m_pMinDissimilarity;
    float* m_pMaxDissimilarity;

    bool m_cameraFrameUpdateRequired;
};

