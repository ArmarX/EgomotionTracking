// ****************************************************************************
// This file is part of the Integrating Vision Toolkit (IVT).
//
// The IVT is maintained by the Karlsruhe Institute of Technology (KIT)
// (www.kit.edu) in cooperation with the company Keyetech (www.keyetech.de).
//
// Copyright (C) 2012 Karlsruhe Institute of Technology (KIT).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the KIT nor the names of its contributors may be
//    used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE KIT AND CONTRIBUTORS “AS IS” AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE KIT OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ****************************************************************************
// ****************************************************************************
// Filename:  ParticleFilterFramework.cpp
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************


#include "ParallelParticleFilterFramework.h"

#include <math.h>
#include <stdio.h>
#include <float.h>

#include <time.h>

// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CParallelParticleFilterFramework::CParallelParticleFilterFramework(int nParticles, int nDimension, int nThreads, EVP::Threading::CThreadPool* pThreadPool)
    : m_pThreadPool(pThreadPool), m_nDimension(nDimension),
      m_useResamplingThreshold(false), m_relativeNEffResamplingThreshold(1.0),
      m_useSwarmOptimization(false), m_swarmVelocityGlobalBestFactor(0.0), m_swarmVelocityLocalBestFactor(0.0), m_swarmLocalBestAgingFactor(0.9), m_swarmOptimizationBestWeightThreshold(0.0),
      m_useAnnealing(false), m_numberOfAnnealingLevels(1), m_currentAnnealingLevel(0), m_annealingBestWeightThreshold(0.0),
      m_estimationMode(eMeanEstimate), m_thresholdedMeanFactor(1.0), m_resampleBasedOnMahalanobisDistance(false),
      m_numberOfParticlesSetup(100), m_useDissimilarity(false)
{
    m_nParticles = nParticles;
    m_nActiveParticles = nParticles;
    m_nThreads = nThreads;

    m_pMeanConfiguration = new PF_REAL[nDimension];
    m_pLastMeanConfiguration = new PF_REAL[nDimension];
    m_pBestConfiguration = new PF_REAL[nDimension];
    m_pThresholdedMean = new PF_REAL[nDimension];

    int i;

    // allocate memory
    m_ppS = new PF_REAL*[nParticles];
    for (i = 0; i < nParticles; i++)
    {
        m_ppS[i] = new PF_REAL[nDimension];
    }

    m_ppSTemp = new PF_REAL*[nParticles];
    for (i = 0; i < nParticles; i++)
    {
        m_ppSTemp[i] = new PF_REAL[nDimension];
    }

    m_ppSwarmSBest = new PF_REAL*[nParticles];
    for (i = 0; i < nParticles; i++)
    {
        m_ppSwarmSBest[i] = new PF_REAL[nDimension];
    }
    m_pSwarmPiBest = new PF_REAL[nParticles];
    for (i = 0; i < nParticles; i++)
    {
        m_pSwarmPiBest[i] = 0.0;
    }

    m_pC = new PF_REAL[nParticles];
    m_pPi = new PF_REAL[nParticles];
    m_pPiNormalized = new PF_REAL[nParticles];
    m_pPiNormalizedTmp = new PF_REAL[nParticles];

    m_pTemp = new PF_REAL[nDimension];

    for (i = 0; i < nParticles; i++)
    {
        m_pC[i] = 0.0;
        m_pPi[i] = 0.0;
    }

    m_particleCanBeResampled = new bool[nParticles];

    m_particlesAreResampled = true;
    m_randomGenerator.Randomize();

    /* HOMOGENITY DISSIMILARITY. */

    m_pAlphaSetup = new float[nParticles];
    float alphaStep = 1.0 / (m_numberOfParticlesSetup - 1);
    for (i = 0; i < nParticles; ++i)
    {
        m_pAlphaSetup[i] = 0 + i * alphaStep;
    }
    m_pDissimilarity = new float[nParticles];
    for (i = 0; i < nParticles; ++i)
    {
        m_pDissimilarity[i] = 0.0;
    }
    m_pDissimilarityMax = new float[nThreads];
    m_pDissimilarityMin = new float[nThreads];
    for (i = 0; i < nThreads; ++i)
    {
        m_pDissimilarityMax[i] = FLT_MIN;
        m_pDissimilarityMin[i] = FLT_MAX;
    }
}

CParallelParticleFilterFramework::~CParallelParticleFilterFramework()
{
    delete [] m_pMeanConfiguration;
    delete [] m_pLastMeanConfiguration;
    delete [] m_pBestConfiguration;
    delete [] m_pThresholdedMean;

    int i;

    for (i = 0; i < m_nParticles; i++)
    {
        delete [] m_ppS[i];
    }
    delete [] m_ppS;

    for (i = 0; i < m_nParticles; i++)
    {
        delete [] m_ppSTemp[i];
    }
    delete [] m_ppSTemp;

    for (i = 0; i < m_nParticles; i++)
    {
        delete [] m_ppSwarmSBest[i];
    }
    delete [] m_ppSwarmSBest;
    delete [] m_pSwarmPiBest;

    delete [] m_pC;
    delete [] m_pPiNormalizedTmp;
    delete [] m_pPi;
    delete [] m_pPiNormalized;

    delete [] m_pTemp;

    delete [] m_particleCanBeResampled;

    delete [] m_pAlphaSetup;
    delete [] m_pDissimilarity;
    delete [] m_pDissimilarityMax;
    delete [] m_pDissimilarityMin;

    if (m_executionUnits.size() > 0)
        for (std::vector<CSamplingAndWeightingExecutionUnit*>::iterator currentExecutionUnit = m_executionUnits.begin(); currentExecutionUnit != m_executionUnits.end(); ++currentExecutionUnit)
        {
            delete *currentExecutionUnit;
        }
}


// ****************************************************************************
// Methods
// ****************************************************************************


void CParallelParticleFilterFramework::GetBestConfiguration(PF_REAL* pBestConfiguration)
{
    for (int i = 0; i < m_nDimension; ++i)
    {
        pBestConfiguration[i] = m_pBestConfiguration[i];
    }
}

void CParallelParticleFilterFramework::GetMeanConfiguration(PF_REAL* pMeanConfiguration)
{
    for (int i = 0; i < m_nDimension; ++i)
    {
        pMeanConfiguration[i] = m_pMeanConfiguration[i];
    }
}

void CParallelParticleFilterFramework::GetThresholdedMeanConfiguration(PF_REAL* pThresholdedMeanConfiguration)
{
    for (int i = 0; i < m_nDimension; ++i)
    {
        pThresholdedMeanConfiguration[i] = m_pThresholdedMean[i];
    }
}


int CParallelParticleFilterFramework::PickBaseSample()
{
    PF_REAL choice = m_randomGenerator.GetUniformRandomNumber() * m_cTotal;

    int low = 0;
    int high = m_nActiveParticles;

    while (high > (low + 1))
    {
        int middle = (high + low) / 2;

        if (choice > m_pC[middle])
        {
            low = middle;
        }
        else
        {
            high = middle;
        }
    }

    return low;
}

PF_REAL CParallelParticleFilterFramework::ParticleFilter(PF_REAL* pResultConfiguration, PF_REAL dSigmaFactor)
{
    if (m_executionUnits.size() == 0)
    {
        return -1.0;
    }

    if (!m_particlesAreResampled && (!m_useResamplingThreshold || ((m_nEff / m_nActiveParticles) < m_relativeNEffResamplingThreshold && m_nEff > 0.0)))
    {
        Resample(m_nActiveParticles);

        if (m_useSwarmOptimization)
        {
            SwarmOptimization();
        }
    }

    const int minParticlesPerUnit = m_nActiveParticles / m_executionUnits.size();
    const int smallUnits = m_executionUnits.size() * minParticlesPerUnit + m_executionUnits.size() - m_nActiveParticles;
    PF_REAL** currentFirstParticle = m_ppS;
    PF_REAL* currentFirstWeight = m_pPi;
    int currentUnitIndex = 0;
    float* currentFirstAlphaSetup = m_pAlphaSetup;
    float* currentFirstDissimilarity = m_pDissimilarity;
    float* currentFirstDissimilarityMax = m_pDissimilarityMax;
    float* currentFirstDissimilarityMin = m_pDissimilarityMin;

    for (std::vector<CSamplingAndWeightingExecutionUnit*>::iterator currentExecutionUnit = m_executionUnits.begin(); currentExecutionUnit != m_executionUnits.end(); ++currentExecutionUnit, ++currentUnitIndex)
    {
        int currentNumberOfParticles = minParticlesPerUnit;
        if (currentUnitIndex >= smallUnits)
        {
            ++currentNumberOfParticles;
        }
        (*currentExecutionUnit)->SetParticles(currentFirstParticle, currentFirstWeight,
                                              currentNumberOfParticles, currentFirstAlphaSetup, currentFirstDissimilarity, m_useDissimilarity,
                                              currentFirstDissimilarityMin, currentFirstDissimilarityMax);
        currentFirstParticle += currentNumberOfParticles;
        currentFirstWeight += currentNumberOfParticles;
        currentFirstAlphaSetup += currentNumberOfParticles;
        currentFirstDissimilarity += currentNumberOfParticles;
        currentFirstDissimilarityMin += 1;
        currentFirstDissimilarityMax += 1;

        (*currentExecutionUnit)->SetGlobalValues(m_pMeanConfiguration, m_pLastMeanConfiguration, m_pBestConfiguration);
        (*currentExecutionUnit)->SetSigmaFactor(dSigmaFactor);
        (*currentExecutionUnit)->SetAnnealingLevel(m_currentAnnealingLevel);
    }

    for (std::vector<CSamplingAndWeightingExecutionUnit*>::iterator currentExecutionUnit = m_executionUnits.begin(); currentExecutionUnit != m_executionUnits.end(); ++currentExecutionUnit)
    {
        m_pThreadPool->DispatchExecutionUnit(*currentExecutionUnit, true);
    }
    m_pThreadPool->Synchronize();

    // apply bayesian measurement weighting

    CalculateFinalProbabilities();

    /* CALCULATE THE FINAL WEIGHT VALUES CONSIDERING HOMOGENITY DISSIMILARITY. */
    if (m_useDissimilarity)
    {
        float minDissimilarity = FLT_MAX;
        float maxDissimilarity = FLT_MIN;

        for (int i = 0; i < m_nThreads; ++i)
        {
            if (m_pDissimilarityMin[i] < minDissimilarity)
            {
                minDissimilarity = m_pDissimilarityMin[i];
            }
            if (m_pDissimilarityMax[i] > maxDissimilarity)
            {
                maxDissimilarity = m_pDissimilarityMax[i];
            }
        }

        float dissimilarityRange = maxDissimilarity - minDissimilarity;

        if (dissimilarityRange > 0)
        {
            for (int i = 0; i < m_nActiveParticles; ++i)
            {
                /* Calculate the normalized dissimilarity values. */

                float dissimilarity = m_pDissimilarity[i];

                float normalizedDissimilarity = 1 - ((dissimilarity - minDissimilarity) / dissimilarityRange);

                /* Calculate the resulting particle weight. */

                //                float oldWeight = m_pPi[i];

                //                float newWeight = oldWeight * normalizedDissimilarity;

                m_pPi[i] *= normalizedDissimilarity;
            }
        }
    }

    m_particlesAreResampled = false;
    //calculate summed weights and total weight
    m_cTotal = 0;
    for (int i = 0; i < m_nActiveParticles; i++)
    {
        m_pC[i] = m_cTotal;
        m_cTotal += m_pPi[i];
    }

    // normalize and calculate neff
    if (m_cTotal < PF_REAL_EPSILON)
    {
        m_cTotal = 0.0;
        for (int i = 0; i < m_nActiveParticles; i++)
        {
            m_pC[i] = m_cTotal;
            m_cTotal += 1.0 / m_nActiveParticles;
            m_pPiNormalized[i] = 1.0 / m_nActiveParticles;
        }
        m_nEff = -1;
    }
    else
    {
        const PF_REAL factor = 1 / m_cTotal;
        PF_REAL sumOfSquaredNormalizedWeights = 0.0;
        for (int i = 0; i < m_nActiveParticles; i++)
        {
            const PF_REAL piNormalized = m_pPi[i] * factor;
            m_pPiNormalized[i] = piNormalized;
            sumOfSquaredNormalizedWeights += piNormalized * piNormalized;
        }
        m_nEff = 1.0 / sumOfSquaredNormalizedWeights;
        //std::cout << m_nEff << std::endl;
    }

    CalculateBestAndMean();
    UpdateLocalBest();

    if (m_estimationMode == eMeanEstimate)
    {
        GetMeanConfiguration(pResultConfiguration);
    }
    else if (m_estimationMode == eBestEstimate)
    {
        GetBestConfiguration(pResultConfiguration);
    }
    else
    {
        CalculateThresholdedMean();
        GetThresholdedMeanConfiguration(pResultConfiguration);
    }

    if (m_useAnnealing)
    {
        m_currentAnnealingLevel++;
        if (m_currentAnnealingLevel >= m_numberOfAnnealingLevels || m_maxWeight < m_annealingBestWeightThreshold)
        {
            m_currentAnnealingLevel = 0;
        }
    }

    return m_executionUnits.front()->CalculateParticleWeight(&pResultConfiguration);
}

void CParallelParticleFilterFramework::CalculateBestAndMean()
{
    for (int i = 0; i < m_nDimension; ++i)
    {
        m_pLastMeanConfiguration[i] = m_pMeanConfiguration[i];
        m_pMeanConfiguration[i] = 0;
    }

    m_maxNormalizedWeight = 0.0;
    int bestI = 0;
    for (int i = 0; i < m_nActiveParticles; ++i)
    {
        if (m_pPiNormalized[i] >= m_maxNormalizedWeight)
        {
            m_maxNormalizedWeight = m_pPiNormalized[i];
            m_maxWeight = m_pPi[i];
            bestI = i;
        }

        for (int j = 0; j < m_nDimension; ++j)
        {
            m_pMeanConfiguration[j] += m_pPiNormalized[i] * m_ppS[i][j];
        }
    }
    for (int j = 0; j < m_nDimension; ++j)
    {
        m_pBestConfiguration[j] = m_ppS[bestI][j];
    }
}

void CParallelParticleFilterFramework::CalculateThresholdedMean()
{
    for (int j = 0; j < m_nDimension; ++j)
    {
        m_pThresholdedMean[j] = 0.0;
    }

    PF_REAL meanThreshold = (1.0 * m_thresholdedMeanFactor) / (m_nActiveParticles);
    int nConfigurations = 0;
    for (int i = 0; i < m_nActiveParticles; ++i)
    {
        if (m_pPiNormalized[i] > meanThreshold)
        {
            for (int j = 0; j < m_nDimension; ++j)
            {
                m_pThresholdedMean[j] += m_ppS[i][j];
            }
            ++nConfigurations;
        }
    }

    if (nConfigurations > 0)
    {
        for (int i = 0; i < m_nDimension; ++i)
        {
            m_pThresholdedMean[i] /= nConfigurations;
        }
    }
}

void CParallelParticleFilterFramework::UpdateLocalBest()
{
    for (int i = 0; i < m_nActiveParticles; ++i)
    {
        if (m_pPiNormalized[i] >= m_pSwarmPiBest[i])
        {
            m_pSwarmPiBest[i] = m_pPiNormalized[i];
            for (int j = 0; j < m_nDimension; ++j)
            {
                m_ppSwarmSBest[i][j] = m_ppS[i][j];
            }
        }
        else
        {
            m_pSwarmPiBest[i] = m_pSwarmPiBest[i] * m_swarmLocalBestAgingFactor;
        }
    }
}

void CParallelParticleFilterFramework::Resample(int numberOfNewParticles)
{
    for (int newIndex = 0; newIndex < numberOfNewParticles; ++newIndex)
    {
        int oldIndex = PickBaseSample();

        if (m_resampleBasedOnMahalanobisDistance)
            while (!m_particleCanBeResampled[oldIndex])
            {
                oldIndex = PickBaseSample();
            }

        for (int dimIndex = 0; dimIndex < m_nDimension; ++dimIndex)
        {
            m_ppSTemp[newIndex][dimIndex] = m_ppS[oldIndex][dimIndex];
        }
        m_pPiNormalizedTmp[newIndex] = m_pPiNormalized[oldIndex];
    }
    PF_REAL** tmp = m_ppSTemp;
    m_ppSTemp = m_ppS;
    m_ppS = tmp;

    /* modification: store also particle weights for sampling step */
    PF_REAL* tmp2 = m_pPiNormalizedTmp;
    m_pPiNormalizedTmp = m_pPiNormalized;
    m_pPiNormalized = tmp2;

    m_particlesAreResampled = true;
}

void CParallelParticleFilterFramework::SwarmOptimization()
{
    if (m_maxWeight >= m_swarmOptimizationBestWeightThreshold)
    {
        for (int i = 0; i < m_nActiveParticles; ++i)
        {
            const PF_REAL factorGlobalBest = m_swarmVelocityGlobalBestFactor * m_randomGenerator.GetUniformRandomNumber();
            const PF_REAL factorLocalBest = m_swarmVelocityLocalBestFactor * m_randomGenerator.GetUniformRandomNumber();

            for (int j = 0; j < m_nDimension; ++j)
            {
                const PF_REAL vBest = (m_pBestConfiguration[j] - m_ppS[i][j]) * factorGlobalBest;

                const PF_REAL vMean = (m_ppSwarmSBest[i][j] - m_ppS[i][j]) * factorLocalBest;

                m_ppS[i][j] += (vBest + vMean);
            }
        }
    }
}

int CParallelParticleFilterFramework::GetNumberOfActiveParticles() const
{
    return m_nActiveParticles;
}

void CParallelParticleFilterFramework::SetNumberOfActiveParticles(int nActiveParticles, bool resampleParticles)
{
    if (nActiveParticles <= m_nParticles && nActiveParticles > 0)
    {
        if (resampleParticles)
        {
            Resample(nActiveParticles);
        }
        m_nActiveParticles = nActiveParticles;
    }
}

void CParallelParticleFilterFramework::SetUseResamplingThreshold(bool useResamplingThreshold)
{
    m_useResamplingThreshold = useResamplingThreshold;
}

void CParallelParticleFilterFramework::SetRelativeNEffResamplingThreshold(PF_REAL resamplingThreshold)
{
    m_relativeNEffResamplingThreshold = resamplingThreshold;
}

void CParallelParticleFilterFramework::SetUseSwarmOptimization(bool useSwarmOptimization)
{
    m_useSwarmOptimization = useSwarmOptimization;
}

void CParallelParticleFilterFramework::SetSwarmVelocityGlobalBestFactor(PF_REAL swarmVelocityGlobalBestFactor)
{
    m_swarmVelocityGlobalBestFactor = swarmVelocityGlobalBestFactor;
}

void CParallelParticleFilterFramework::SetSwarmVelocityLocalBestFactor(PF_REAL swarmVelocityLocalBestFactor)
{
    m_swarmVelocityLocalBestFactor = swarmVelocityLocalBestFactor;
}

void CParallelParticleFilterFramework::SetSwarmOptimizationBestWeightThreshold(PF_REAL weightThreshold)
{
    m_swarmOptimizationBestWeightThreshold = weightThreshold;
}

void CParallelParticleFilterFramework::SetSwarmLocalBestAgingFactor(PF_REAL swarmLocalBestAgingFactor)
{
    m_swarmLocalBestAgingFactor = swarmLocalBestAgingFactor;
}

void CParallelParticleFilterFramework::SetUseAnnealing(bool useAnnealing)
{
    m_useAnnealing = useAnnealing;
    ResetAnnealingLevel();
}

void CParallelParticleFilterFramework::SetNumberOfAnnealingLevels(unsigned int annealingLevels)
{
    m_numberOfAnnealingLevels = annealingLevels;
    ResetAnnealingLevel();
}

void CParallelParticleFilterFramework::SetAnnealingBestWeightThreshold(PF_REAL weightThreshold)
{
    m_annealingBestWeightThreshold = weightThreshold;
}

void CParallelParticleFilterFramework::ResetAnnealingLevel()
{
    m_currentAnnealingLevel = 0;
}

unsigned int CParallelParticleFilterFramework::GetCurrentAnnealingLevel()
{
    return m_currentAnnealingLevel;
}

void CParallelParticleFilterFramework::SetThresholdedMeanFactor(PF_REAL thresholdedMeanFactor)
{
    m_thresholdedMeanFactor = thresholdedMeanFactor;
}

void CParallelParticleFilterFramework::SetEstimationMode(const CEstimationMode& estimationMode)
{
    m_estimationMode = estimationMode;
}

void CParallelParticleFilterFramework::CalculateBoundingRect(PF_REAL* pMin, PF_REAL* pMax) const
{
    for (int j = 0; j < m_nDimension; ++j)
    {
        pMax[j] = -PF_REAL_MAX;
        pMin[j] = PF_REAL_MAX;
    }
    for (int i = 0; i < m_nActiveParticles; ++i)
    {
        for (int j = 0; j < m_nDimension; ++j)
        {
            if (m_ppS[i][j] < pMin[j])
            {
                pMin[j] = m_ppS[i][j];
            }
            if (m_ppS[i][j] > pMax[j])
            {
                pMax[j] = m_ppS[i][j];
            }
        }
    }
}
