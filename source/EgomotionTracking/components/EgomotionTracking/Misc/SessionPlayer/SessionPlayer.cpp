#include "SessionPlayer.h"

#include <EgomotionTracking/components/EgomotionTracking/Misc/SessionRecorder/SessionRecorder.h>

CSessionPlayer::CSessionPlayer()
{
}

bool CSessionPlayer::DispatchLocalisation()
{
    if (m_currentRecordIndex == 0)
    {
        QDomElement root = m_sessionXMLDocument.documentElement();
        if (root.isNull())
        {
            return false;
        }

        QDomElement recordingsRoot = root.firstChildElement(SESSION_RECORDING_XML_TAG_RECORDINGSROOT);
        if (recordingsRoot.isNull())
        {
            return false;
        }

        QDomNodeList records = recordingsRoot.elementsByTagName(SESSION_RECORDING_XML_TAG_RECORDINGELEMENT);
        if (records.isEmpty())
        {
            return false;
        }
        QDomElement currentRecordElement = records.item(m_currentRecordIndex).toElement();
        if (currentRecordElement.isNull())
        {
            return false;
        }

        QDomElement groundTruthLocationElement = currentRecordElement.firstChildElement("groundTruthLocation");
        if (groundTruthLocationElement.isNull() || !XMLPrimitives::DeserializeQDomElementToLocationState(groundTruthLocationElement, m_locationEstimate))
        {
            m_estimationIsValid = false;
        }
    }
    return true;
}

bool CSessionPlayer::DispatchRobot()
{
    if (m_stopped)
    {
        m_robotControlState.SetPlatformVelocityX(0.0);
        m_robotControlState.SetPlatformVelocityY(0.0);
        m_robotControlState.SetPlatformVelocityAlpha(0.0);
        return LoadCameraImages();

    }
    else
    {
        QDomElement root = m_sessionXMLDocument.documentElement();
        if (root.isNull())
        {
            return false;
        }

        QDomElement recordingsRoot = root.firstChildElement(SESSION_RECORDING_XML_TAG_RECORDINGSROOT);
        if (recordingsRoot.isNull())
        {
            return false;
        }

        QDomNodeList records = recordingsRoot.elementsByTagName(SESSION_RECORDING_XML_TAG_RECORDINGELEMENT);
        if (records.isEmpty())
        {
            return false;
        }
        QDomElement currentRecordElement = records.item(m_currentRecordIndex).toElement();
        if (currentRecordElement.isNull())
        {
            return false;
        }

        if (!LoadXMLRecordElement(currentRecordElement))
        {
            return false;
        }
        if (!LoadCameraImages())
        {
            return false;
        }

        ++m_currentRecordIndex;
        if (m_currentRecordIndex >= m_numberOfLoadedRecords)
        {
            m_currentRecordIndex = m_numberOfLoadedRecords - 1;
            m_stopped = true;
        }
    }
    return true;
}

const SbMatrix& CSessionPlayer::GetCameraProjectionMatrixGL(CCameraID cameraId) const
{
    return m_projectionMatrices[static_cast<int>(cameraId)];
}

const SbMatrix& CSessionPlayer::GetCameraProjectionMatrixGL() const
{
    return GetCameraProjectionMatrixGL(eLeftCamera);
}

bool CSessionPlayer::LockRobotMovement()
{
    m_stopped = true;
    return true;
}

void CSessionPlayer::ReleaseRobotMovementLock()
{
    m_stopped = false;
}

void CSessionPlayer::SetSessionDirectory(const std::string& directoryPath)
{
    m_sessionDirectoryPath = directoryPath;
    LoadSession();
}

int CSessionPlayer::GetCurrentRecordIndex()
{
    return m_currentRecordIndex;
}

int CSessionPlayer::GetNumberOfLoadedRecords()
{
    return m_numberOfLoadedRecords;
}

void CSessionPlayer::Reset()
{
    LoadSession();
}

CHeadConfiguration CSessionPlayer::GetHeadConfiguration() const
{
    return m_headConfiguration;
}

void CSessionPlayer::GetInitialNeckErrors(float& neckErrorRoll, float& neckErrorPitch, float& neckErrorYaw) const
{
    neckErrorRoll = m_initialNeckErrorRoll;
    neckErrorPitch = m_initialNeckErrorPitch;
    neckErrorYaw = m_initialNeckErrorYaw;
}

bool CSessionPlayer::LoadSession()
{
    m_currentRecordIndex = 0;
    m_stopped = false;

    m_sessionXMLDocument = QDomDocument(SESSION_RECORDING_XML_FILE_DOCTYPE);
    char filename[300];
    snprintf(filename, 299, "%s/" SESSION_RECORDING_SESSION_FILENAME, m_sessionDirectoryPath.c_str());
    if (!XMLPrimitives::ReadXMLDocumentFromFile(m_sessionXMLDocument, filename))
    {
        COUT_ERROR << "Couldn`t load session file: " << filename << std::endl;
        m_numberOfLoadedRecords = 0;
        return false;
    }

    QDomElement root = m_sessionXMLDocument.documentElement();

    QDomAttr initialNeckErrorRoll = root.attributeNode("initialNeckErrorRoll");
    QDomAttr initialNeckErrorPitch = root.attributeNode("initialNeckErrorPitch");
    QDomAttr initialNeckErrorYaw = root.attributeNode("initialNeckErrorYaw");

    if (!initialNeckErrorRoll.isNull() && !initialNeckErrorPitch.isNull() && !initialNeckErrorYaw.isNull())
    {
        m_initialNeckErrorRoll = initialNeckErrorRoll.value().toFloat();
        m_initialNeckErrorPitch = initialNeckErrorPitch.value().toFloat();
        m_initialNeckErrorYaw = initialNeckErrorYaw.value().toFloat();
    }
    else
    {
        m_initialNeckErrorRoll = 0.0;
        m_initialNeckErrorPitch = 0.0;
        m_initialNeckErrorYaw = 0.0;
    }

    QDomElement recordingsRoot = root.firstChildElement(SESSION_RECORDING_XML_TAG_RECORDINGSROOT);
    QDomAttr numberOfRecordsAttr = recordingsRoot.attributeNode("numberOfRecords");
    if (recordingsRoot.isNull() || numberOfRecordsAttr.isNull())
    {
        COUT_ERROR << "Couldn`t load session file (XML Error) " << std::endl;
        m_numberOfLoadedRecords = 0;
        return false;
    }
    m_numberOfLoadedRecords = numberOfRecordsAttr.value().toInt();
    m_currentRecordIndex = 0;

    return LoadProjectionMatrices();
}

bool CSessionPlayer::LoadProjectionMatrices()
{
    QDomElement root = m_sessionXMLDocument.documentElement();
    QDomElement cameraProjectionsRoot = root.firstChildElement(SESSION_RECORDING_XML_TAG_CAMERAPROJECTIONSROOT);
    if (cameraProjectionsRoot.isNull())
    {
        COUT_ERROR << "Couldn`t load projection matrices" << std::endl;
        return false;
    }

    for (int currentCameraId = 0; currentCameraId < CRobotControlInterface::eCAMERAIDEND; ++currentCameraId)
    {
        QString cameraProjectionName = QString("cameraProjection%1").arg(currentCameraId);
        QDomElement cameraProjectionElement = cameraProjectionsRoot.firstChildElement(cameraProjectionName);
        if (cameraProjectionElement.isNull())
        {
            COUT_ERROR << "Couldn`t load projection matrix:" << cameraProjectionName.toStdString() << std::endl;
            return false;
        }
        if (!XMLPrimitives::DeserializeQDomElementToSbMatrix(cameraProjectionElement, m_projectionMatrices[currentCameraId]))
        {
            COUT_ERROR << "Couldn`t load projection matrix (XML Reading Error)" << cameraProjectionName.toStdString() << std::endl;
            return false;
        }
    }

    LoadCameraImages();

    return true;
}

bool CSessionPlayer::LoadXMLRecordElement(const QDomElement& recordElement)
{
    QDomElement robotControlStateElement = recordElement.firstChildElement("robotControlState");
    if (robotControlStateElement.isNull())
    {
        return false;
    }
    if (!XMLPrimitives::DeserializeQDomElementToRobotControlState(robotControlStateElement, m_robotControlState))
    {
        return false;
    }

    QDomElement headConfigurationElement = recordElement.firstChildElement("headConfiguration");
    if (headConfigurationElement.isNull())
    {
        return false;
    }
    if (!XMLPrimitives::DeserializeQDomElementToHeadConfiguration(headConfigurationElement, m_headConfiguration))
    {
        return false;
    }

    QDomElement groundTruthLocationElement = recordElement.firstChildElement("groundTruthLocation");
    if (groundTruthLocationElement.isNull() || !XMLPrimitives::DeserializeQDomElementToLocationState(groundTruthLocationElement, m_locationEstimate))
    {
        m_estimationIsValid = false;
    }

    for (int currentCameraId = 0; currentCameraId < CRobotControlInterface::eCAMERAIDEND; ++currentCameraId)
    {
        QString platformToCameraTransformationElementName = QString("platformToCameraTransformation%1").arg(currentCameraId);
        QDomElement platformToCameraTransformationElement = recordElement.firstChildElement(platformToCameraTransformationElementName);
        if (platformToCameraTransformationElement.isNull())
        {
            return false;
        }

        if (!XMLPrimitives::DeserializeQDomElementToSbMatrix(platformToCameraTransformationElement, m_platformToCameraTransformation[currentCameraId]))
        {
            return false;
        }
    }

    m_estimationIsValid = true;
    return true;
}

bool CSessionPlayer::LoadCameraImages()
{
    char filename[300];
    for (int currentCameraId = 0; currentCameraId < CRobotControlInterface::eCAMERAIDEND; ++currentCameraId)
    {
        snprintf(filename, 299, "%s/" SESSION_RECORDING_VIEWIMAGE_FILENAME_FORMAT, m_sessionDirectoryPath.c_str(), currentCameraId, m_currentRecordIndex);
        if (!m_cameraImages[currentCameraId].LoadFromFile(filename))
        {
            return false;
        }
    }
    return true;
}
