#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/HeadConfiguration.h>
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotControlInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/Armar3ControlInterface.h>

#include <EgomotionTracking/components/EgomotionTracking/LocationEstimator/LocationEstimator.h>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>

#include <QDomDocument>

class CSessionPlayer : public CArmar3ControlInterface, public CLocationEstimator
{
public:
    CSessionPlayer();

    bool DispatchLocalisation() override;

    bool DispatchRobot() override;
    const SbMatrix& GetCameraProjectionMatrixGL(CCameraID cameraId) const override;
    const SbMatrix& GetCameraProjectionMatrixGL() const override;
    bool LockRobotMovement() override;
    void ReleaseRobotMovementLock() override;

    void SetSessionDirectory(const std::string& directoryPath);
    int GetCurrentRecordIndex();
    int GetNumberOfLoadedRecords();

    void Reset();

    CHeadConfiguration GetHeadConfiguration() const override;

    void GetInitialNeckErrors(float& neckErrorRoll, float& neckErrorPitch, float& neckErrorYaw) const;

    bool SetTargetRobotControlState(const CRobotControlState& robotControlState) override
    {
        return false;
    } ;
    bool SetTargetRobotHeadConfiguration(const CHeadConfiguration& targetHeadConfiguration) override
    {
        return false;
    } ;

    // virtual void GetNeckRPYBounds(float& neckRollMin, float& neckRollMax, float& neckPitchMin, float& neckPitchMax, float& neckYawMin, float& neckYawMax) const;

protected:

    bool LoadSession();
    bool LoadProjectionMatrices();

    bool LoadXMLRecordElement(const QDomElement& recordElement);
    bool LoadCameraImages();

    std::string m_sessionDirectoryPath;
    QDomDocument m_sessionXMLDocument;

    int m_currentRecordIndex;
    int m_numberOfLoadedRecords;

    SbMatrix m_projectionMatrices[CRobotControlInterface::eCAMERAIDEND];
    SbMatrix m_platformToCameraTransformation[CRobotControlInterface::eCAMERAIDEND];

    CHeadConfiguration m_headConfiguration;

    bool m_stopped;

    float m_initialNeckErrorRoll;
    float m_initialNeckErrorPitch;
    float m_initialNeckErrorYaw;
};

