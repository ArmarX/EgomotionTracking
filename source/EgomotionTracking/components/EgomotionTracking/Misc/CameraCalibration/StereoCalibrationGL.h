#pragma once

#include <Calibration/StereoCalibration.h>
#include <Calibration/Undistortion.h>

#include <Inventor/SbMatrix.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/OpenGLCalculations.h>

#include <string>
class CStereoCalibrationGL
{
public:
    CStereoCalibrationGL();
    CStereoCalibrationGL(float near, float far);
    bool LoadFromFile(const std::string& filename);
    const CStereoCalibration& GetStereoCalibration() const;

    void SetStereoCalibration(CStereoCalibration* stereoCalibration);

    void SetNearFarValues(float near, float far);

    const SbMatrix& GetLeftProjectionMatrixGL() const;
    const SbMatrix& GetRightProjectionMatrixGL() const;
    CUndistortion& GetUndistortion();

    // Needed by TestModelElementVisibility;
    CStereoCalibration const* getStereoCalibration() const;

protected:
    void CalculateProjectionMatrices();
    CStereoCalibration m_stereoCalibration;
    SbMatrix m_leftProjectionMatrixGL;
    SbMatrix m_rightProjectionMatrixGL;
    float m_near;
    float m_far;
    CUndistortion m_undistortion;

};

