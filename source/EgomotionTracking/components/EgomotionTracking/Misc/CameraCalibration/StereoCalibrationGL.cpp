#include "StereoCalibrationGL.h"

#include <iostream>

CStereoCalibrationGL::CStereoCalibrationGL() :
    m_near(-1.0), m_far(-1.0)
{
}

CStereoCalibrationGL::CStereoCalibrationGL(float near, float far) :
    m_near(near), m_far(far)
{

}

bool CStereoCalibrationGL::LoadFromFile(const std::string& filename)
{
    bool succeeded = m_stereoCalibration.LoadCameraParameters(filename.c_str());
    if (succeeded)
    {
        m_undistortion.Init(&m_stereoCalibration);
        CalculateProjectionMatrices();
    }
    return succeeded;
}


const CStereoCalibration& CStereoCalibrationGL::GetStereoCalibration() const
{
    return m_stereoCalibration;
}

void CStereoCalibrationGL::SetStereoCalibration(CStereoCalibration* stereoCalibration)
{
    m_stereoCalibration.Set(*stereoCalibration);
    m_undistortion.Init(&m_stereoCalibration);
    CalculateProjectionMatrices();
}

void CStereoCalibrationGL::SetNearFarValues(float near, float far)
{
    m_near = near;
    m_far = far;
}

const SbMatrix& CStereoCalibrationGL::GetLeftProjectionMatrixGL() const
{
    return m_leftProjectionMatrixGL;
}

const SbMatrix& CStereoCalibrationGL::GetRightProjectionMatrixGL() const
{
    return m_rightProjectionMatrixGL;
}

CUndistortion& CStereoCalibrationGL::GetUndistortion()
{
    return m_undistortion;
}

void CStereoCalibrationGL::CalculateProjectionMatrices()
{
    m_leftProjectionMatrixGL = OpenGLCalculations::CalculateGLProjectionMatrixFromIVTCameraCalibration(m_stereoCalibration.GetLeftCalibration(), m_near, m_far);
    m_rightProjectionMatrixGL = OpenGLCalculations::CalculateGLProjectionMatrixFromIVTCameraCalibration(m_stereoCalibration.GetRightCalibration(), m_near, m_far);
}

CStereoCalibration const* CStereoCalibrationGL::getStereoCalibration() const
{
    return &m_stereoCalibration;
}
