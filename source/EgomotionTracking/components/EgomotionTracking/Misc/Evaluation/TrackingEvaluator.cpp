#include "TrackingEvaluator.h"

#include <EgomotionTracking/components/EgomotionTracking/Helpers/CSVPrimitives.h>

CTrackingEvaluator::CTrackingEvaluator() :
    m_pTrackingControl(NULL)
{
    ResetEvaluation();
}

bool CTrackingEvaluator::DispatchEvaluation()
{
    CEvaluationMeasurement measurement;
    if (!GetCurrentMeasurement(measurement))
    {
        return false;
    }

    if (CheckIsFailureBoundingBox(measurement))
    {
        ++m_failuresBoundingBox;
    }
    if (CheckIsFailureMahalanobis(measurement))
    {
        ++m_failuresMahalanobisDistThreshold;
    }

    m_measurements.push_back(measurement);

    m_currentDeviationX = measurement.estimatedLocation.GetX() - measurement.groundTruthLocation.GetX();
    m_currentDeviationY = measurement.estimatedLocation.GetY() - measurement.groundTruthLocation.GetY();
    m_currentDeviationAlpha = measurement.estimatedLocation.GetAlpha() - measurement.groundTruthLocation.GetAlpha();
    m_currentDeviationD = sqrt(m_currentDeviationX * m_currentDeviationX + m_currentDeviationY * m_currentDeviationY);
    m_deviationAccumulatorX.AddDeviation(m_currentDeviationX);
    m_deviationAccumulatorY.AddDeviation(m_currentDeviationY);
    m_deviationAccumulatorAlpha.AddDeviation(m_currentDeviationAlpha);
    m_deviationAccumulatorD.AddDeviation(m_currentDeviationD);

    return true;
}


void CTrackingEvaluator::ResetEvaluation()
{
    m_deviationAccumulatorAlpha.Reset();
    m_deviationAccumulatorD.Reset();
    m_deviationAccumulatorX.Reset();
    m_deviationAccumulatorY.Reset();

    m_failuresBoundingBox = 0;
    m_failuresMahalanobisDistThreshold = 0;
    m_measurements.clear();
}

const CDeviationAccumulator& CTrackingEvaluator::GetDeviationAccumulatorX() const
{
    return m_deviationAccumulatorX;
}

const CDeviationAccumulator& CTrackingEvaluator::GetDeviationAccumulatorY() const
{
    return m_deviationAccumulatorY;
}

const CDeviationAccumulator& CTrackingEvaluator::GetDeviationAccumulatorAlpha() const
{
    return m_deviationAccumulatorAlpha;
}

const CDeviationAccumulator& CTrackingEvaluator::GetDeviationAccumulatorD() const
{
    return m_deviationAccumulatorD;
}

float CTrackingEvaluator::GetCurrentDeviationX() const
{
    return m_currentDeviationX;
}

float CTrackingEvaluator::GetCurrentDeviationY() const
{
    return m_currentDeviationY;
}

float CTrackingEvaluator::GetCurrentDeviationAlpha() const
{
    return m_currentDeviationAlpha;
}

float CTrackingEvaluator::GetCurrentDeviationD() const
{
    return m_currentDeviationD;
}

int CTrackingEvaluator::GetNumberOfEvaluationFrames() const
{
    return m_measurements.size();
}

int CTrackingEvaluator::GetNumberOfBoundingBoxFailures() const
{
    return m_failuresBoundingBox;
}

int CTrackingEvaluator::GetNumberOfMahalanobisThresholdFailures() const
{
    return m_failuresMahalanobisDistThreshold;
}

bool CTrackingEvaluator::WriteMeasurementsToCSV(const std::string& filename)
{
    if (m_measurements.size() > 0)
    {
        CSVDocument csvDoc;
        for (std::list<CEvaluationMeasurement>::const_iterator measurementIterator = m_measurements.begin(); measurementIterator != m_measurements.end(); ++measurementIterator)
        {
            const CEvaluationMeasurement& currentMeasurement = *measurementIterator;
            CSVEntry csvEntry;
            csvEntry << CSVPrimitives::SerializeLocationStateToCSV(currentMeasurement.estimatedLocation);
            csvEntry << CSVPrimitives::SerializeLocationStateToCSV(currentMeasurement.groundTruthLocation);
            csvEntry << currentMeasurement.nEff;
            csvEntry << currentMeasurement.scatteringPlaneEllipseArea;
            csvEntry << currentMeasurement.scatteringDeterminant;
            csvEntry << currentMeasurement.estimateToGTMahalanobisDist;
            csvEntry << CSVPrimitives::SerializeLocationStateToCSV(currentMeasurement.locationBoundingBox.minLocationState);
            csvEntry << CSVPrimitives::SerializeLocationStateToCSV(currentMeasurement.locationBoundingBox.maxLocationState);
            csvDoc << csvEntry;
        }
        return CSVPrimitives::WriteCSVDocumentToFile(csvDoc, filename);
    }
    return true;
}

bool CTrackingEvaluator::CheckIsFailureMahalanobis(const CTrackingEvaluator::CEvaluationMeasurement& measurement) const
{
    return measurement.estimateToGTMahalanobisDist > 3.0;
}

bool CTrackingEvaluator::CheckIsFailureBoundingBox(const CTrackingEvaluator::CEvaluationMeasurement& measurement) const
{
    if (measurement.groundTruthLocation.GetX() < measurement.locationBoundingBox.minLocationState.GetX() ||
        measurement.groundTruthLocation.GetX() > measurement.locationBoundingBox.maxLocationState.GetX() ||
        measurement.groundTruthLocation.GetY() < measurement.locationBoundingBox.minLocationState.GetY() ||
        measurement.groundTruthLocation.GetY() > measurement.locationBoundingBox.maxLocationState.GetY() ||
        measurement.groundTruthLocation.GetAlpha() < measurement.locationBoundingBox.minLocationState.GetAlpha() ||
        measurement.groundTruthLocation.GetAlpha() > measurement.locationBoundingBox.maxLocationState.GetAlpha())
    {
        return true;
    }
    return false;
}

bool CTrackingEvaluator::GetCurrentMeasurement(CEvaluationMeasurement& measurement) const
{
    if (!m_pTrackingControl || !m_pTrackingControl->GetGroundTruthLocationEstimator() || !m_pTrackingControl->GetTrackingPF())
    {
        return false;
    }

    measurement.estimatedLocation = m_pTrackingControl->GetLocationEstimate();
    measurement.groundTruthLocation = m_pTrackingControl->GetGroundTruthLocationEstimator()->GetLocationEstimate();

    measurement.estimateToGTMahalanobisDist = m_pTrackingControl->GetEstimationToGTMahalanobisDistance();
    measurement.nEff = m_pTrackingControl->GetNeff();
    measurement.scatteringDeterminant = m_pTrackingControl->GetScatteringDeterminant();
    measurement.scatteringPlaneEllipseArea = m_pTrackingControl->GetScatteringEllipseArea();

    m_pTrackingControl->GetTrackingPF()->CalculateBoundingRect(measurement.locationBoundingBox.minLocationState, measurement.locationBoundingBox.maxLocationState);
    return true;
}


void CTrackingEvaluator::SetTrackingControl(const CTrackingControl* pTrackingControl)
{
    m_pTrackingControl = pTrackingControl;
}
