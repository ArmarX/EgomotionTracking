#pragma once

#include <EgomotionTracking/components/EgomotionTracking/TrackingControl.h>
#include <EgomotionTracking/components/EgomotionTracking/LocationEstimator/TrackingFilter/EgomotionTrackingPF/PlaneScatteringDirections.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/DeviationAccumulator.h>

class CTrackingEvaluator
{
public:
    CTrackingEvaluator();
    bool DispatchEvaluation();

    void SetTrackingControl(const CTrackingControl* pTrackingControl);
    void ResetEvaluation();

    const CDeviationAccumulator& GetDeviationAccumulatorX() const;
    const CDeviationAccumulator& GetDeviationAccumulatorY() const;
    const CDeviationAccumulator& GetDeviationAccumulatorAlpha() const;
    const CDeviationAccumulator& GetDeviationAccumulatorD() const;

    float GetCurrentDeviationX() const;
    float GetCurrentDeviationY() const;
    float GetCurrentDeviationAlpha() const;
    float GetCurrentDeviationD() const;


    int GetNumberOfEvaluationFrames() const;
    int GetNumberOfBoundingBoxFailures() const;
    int GetNumberOfMahalanobisThresholdFailures() const;
    bool WriteMeasurementsToCSV(const std::string& filename);
protected:
    struct CEvaluationMeasurement
    {
        CLocationState estimatedLocation;
        CLocationState groundTruthLocation;
        float nEff;
        float scatteringPlaneEllipseArea;
        float scatteringDeterminant;
        float estimateToGTMahalanobisDist;
        struct CLocationBoundingBox
        {
            CLocationState minLocationState;
            CLocationState maxLocationState;
        } locationBoundingBox;
    };

    bool CheckIsFailureMahalanobis(const CEvaluationMeasurement& measurement) const;
    bool CheckIsFailureBoundingBox(const CEvaluationMeasurement& measurement) const;

    bool GetCurrentMeasurement(CEvaluationMeasurement& measurement) const;

    const CTrackingControl* m_pTrackingControl;
    std::list<CEvaluationMeasurement> m_measurements;

    CDeviationAccumulator m_deviationAccumulatorX;
    CDeviationAccumulator m_deviationAccumulatorY;
    CDeviationAccumulator m_deviationAccumulatorAlpha;
    CDeviationAccumulator m_deviationAccumulatorD;

    float m_currentDeviationX;
    float m_currentDeviationY;
    float m_currentDeviationAlpha;
    float m_currentDeviationD;

    int m_failuresBoundingBox;
    int m_failuresMahalanobisDistThreshold;
};

