#include "OpenGLCalculations.h"

OpenGLCalculations::OpenGLCalculations()
{
}

SbMatrix OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(const SbVec3f& position, const SbRotation& orientation)
{
    SbRotation viewingRotation = orientation.inverse();
    SbVec3f viewingTranslation;
    viewingRotation.multVec(position, viewingTranslation);

    SbMatrix viewingMatrix;
    viewingMatrix.setTransform(-viewingTranslation, viewingRotation, SbVec3f(1.0, 1.0, 1.0));

    return viewingMatrix;
}

SbMatrix OpenGLCalculations::CalculateGLViewingMatrixFromIVTCameraCoordinates(const Vec3d& translation, const Mat3d& rotation)
{
    SbMatrix rotationMatrix(rotation.r1, rotation.r2, rotation.r3, 0.0,
                            -rotation.r4, -rotation.r5, -rotation.r6, 0.0,
                            -rotation.r7, -rotation.r8, -rotation.r9, 0.0,
                            0.0, 0.0, 0.0, 1.0);
    SbVec3f cameraTranslation(translation.x, -translation.y, -translation.z);
    SbMatrix viewingMatrix;
    viewingMatrix.setTransform(cameraTranslation, rotationMatrix.transpose(), SbVec3f(1.0, 1.0, 1.0));
    return viewingMatrix;
}

SbMatrix OpenGLCalculations::CalculateGLProjectionMatrixFromIVTCameraCalibration(const CCalibration* cameraCalibration, float near, float far)
{
    SbMatrix projectionMatrix;

    const CCalibration::CCameraParameters& cameraParameters = cameraCalibration->GetCameraParameters();
    const float width = static_cast<float>(cameraParameters.width);
    const float height = static_cast<float>(cameraParameters.height);
    Mat3d k;
    cameraCalibration->GetCalibrationMatrix(k);

    const float K00 = k.r1;
    const float K01 = k.r2;
    const float K02 = k.r3;
    //const float K10 = k.r4;
    const float K11 = k.r5;
    const float K12 = k.r6;
    //const float K20 = k.r7;
    //const float K21 = k.r8;
    //const float K22 = k.r9;

    projectionMatrix[0][0] = (2.0f * K00) / width;
    projectionMatrix[0][1] = 0.0f;
    projectionMatrix[0][2] = 0.0f;
    projectionMatrix[0][3] = 0.0f;

    projectionMatrix[1][0] = (2.0f * K01) / width;
    projectionMatrix[1][1] = (2.0f * K11) / height;
    projectionMatrix[1][2] = 0.0f;
    projectionMatrix[1][3] = 0.0f;

    projectionMatrix[2][0] = (1.0f - (2.0f * K02) / width);
    projectionMatrix[2][1] = ((2.0f * K12) / height - 1.0f);
    projectionMatrix[2][2] = -(far + near) / (far - near);
    projectionMatrix[2][3] = -1.0f;

    projectionMatrix[3][0] = 0.0f;
    projectionMatrix[3][1] = 0.0f;
    projectionMatrix[3][2] = (-2.0 * far * near) / (far - near);
    projectionMatrix[3][3] = 0.0f;

    return projectionMatrix;
}
