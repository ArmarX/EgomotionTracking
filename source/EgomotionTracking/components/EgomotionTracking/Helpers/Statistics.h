#pragma once

#include "Conversions.h"
#include <assert.h>
#include <iostream>
class Statistics
{
public:
    static float CalculateHistogrammQuantileSmoothingWeights(int n, float relativeLeftQuantile, float relativeRightQuantile, float sigmaFactor, std::list<float>& resultWeights)
    {
        assert(relativeRightQuantile > relativeLeftQuantile);
        int a = ceil(relativeLeftQuantile * n);
        int b = floor(relativeRightQuantile * n);

        float sumOfWeights = 0.0;

        for (int i = 0; i < a; ++i)
        {
            const float relativeDist = static_cast<float>(i - a) / a;
            const float curWeight = exp(-sigmaFactor * relativeDist * relativeDist);
            resultWeights.push_back(curWeight);
            sumOfWeights += curWeight;
        }
        for (int i = a; i <= b; ++i)
        {
            resultWeights.push_back(1.0);
            sumOfWeights += 1.0;
        }
        for (int i = b + 1; i < n; ++i)
        {
            const float relativeDist = static_cast<float>(b - i - 1) / (n - b);
            const float curWeight = exp(-sigmaFactor * relativeDist * relativeDist);
            resultWeights.push_back(curWeight);
            sumOfWeights += curWeight;
        }
        return sumOfWeights;
    }
};

