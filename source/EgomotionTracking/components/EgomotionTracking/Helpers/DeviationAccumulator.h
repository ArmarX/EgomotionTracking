#pragma once

class CDeviationAccumulator
{
public:
    CDeviationAccumulator();

    void Reset();
    void AddDeviation(double deviation);

    double GetMax() const;
    double GetMin() const;
    double GetMean() const;
    double GetRMSD() const;

protected:
    double m_sum;
    double m_squaredSum;
    double m_max;
    double m_min;
    int m_cnt;
};

