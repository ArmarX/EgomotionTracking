/*
 * Conversions.h
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#pragma once

#include <math.h>
#include <list>
#include <cmath>

#define DEGREE_PER_RADIAN (180 / M_PI)
#define RADIANS_PER_DEGREE (1.0 / DEGREE_PER_RADIAN)

#define MILLISECONDS_PER_SECOND (1000.0)
#define SECONDS_PER_MILLISECOND (1.0 / MILLISECONDS_PER_SECOND)


class Conversions
{

public:
    virtual ~Conversions();

    static inline float RadiansToDegree(float radians)
    {
        return radians * DEGREE_PER_RADIAN;
    }

    static inline float DegreeToRadians(float degree)
    {
        return degree * RADIANS_PER_DEGREE;
    }

    static inline float NormalizeRadiansTo2PiInterval(float angle)
    {
        while (angle < 0)
        {
            angle += 2 * M_PI;
        }
        while (angle >= 2 * M_PI)
        {
            angle -= 2 * M_PI;
        }
        return angle;
    }

    static inline float CalculateMeanOfRadians(const std::list<float>& radians)
    {
        if (radians.size() == 0)
        {
            return 0;
        }

        double x = 0.0, y = 0.0;
        for (std::list<float>::const_iterator curRadianIterator = radians.begin(); curRadianIterator != radians.end(); ++curRadianIterator)
        {
            y += sin(*curRadianIterator);
            x += cos(*curRadianIterator);
        }
        x /= radians.size();
        y /= radians.size();
        return NormalizeRadiansTo2PiInterval(atan2(y, x));
    }

    static inline float CalculateWeightedMeanOfRadians(const std::list<float>& radians, const std::list<float>& weights)
    {
        if (radians.size() == 0 || radians.size() != weights.size())
        {
            return 0;
        }

        double x = 0.0, y = 0.0;
        std::list<float>::const_iterator curWeightIterator = weights.begin();
        for (std::list<float>::const_iterator curRadianIterator = radians.begin(); curRadianIterator != radians.end(); ++curRadianIterator, ++curWeightIterator)
        {
            y += sin(*curRadianIterator) * *curWeightIterator;
            x += cos(*curRadianIterator) * *curWeightIterator;
        }
        return NormalizeRadiansTo2PiInterval(atan2(y, x));
    }

    static inline float MillisecondsToSeconds(float ms)
    {
        return ms * SECONDS_PER_MILLISECOND;
    }

    static inline float SecondsToMilliseconds(float s)
    {
        return s * MILLISECONDS_PER_SECOND;
    }

    static inline float PercentageToProbability(float percentageValue)
    {
        return percentageValue * 0.01;
    }

    static inline float ProbabilityToPercentage(float probabilityValue)
    {
        return probabilityValue * 100.0;
    }

    static inline float PercentageToProportions(float percentageValue)
    {
        return percentageValue * 0.01;
    }

    static inline float ProportionsToPercentage(float probabilityValue)
    {
        return probabilityValue * 100.0;
    }

    static inline int RoundFloatToNearestInt(float v)
    {
        return static_cast<int>(v + 0.5);
    }

    static inline float MillimetreToMetre(float millimetre)
    {
        return millimetre / 1000.0;
    }

    static inline float MetreToMillimetre(float metre)
    {
        return metre * 1000.0;
    }

    static inline float SquareMillimetreToSqaureMetre(float squareMillimetre)
    {
        return squareMillimetre / (1000000.0);
    }

    static inline float SquareMetreToSquareMillimetre(float squareMetre)
    {
        return squareMetre * 1000000.0;
    }

    static double RoundToNearestDecimalNumber(double number, int decimals)
    {
        double decimalFactor = std::pow(10.0, static_cast<double>(decimals));
        return floor(number * decimalFactor + 0.5) / decimalFactor;
    }

    static float ByteColorValueToFloatColorValue(unsigned char byteColorValue)
    {
        return static_cast<float>(byteColorValue) / 255.0;
    }

    static unsigned int FloatColorValueToByteColorValue(float floatColorValue)
    {
        return static_cast<unsigned char>(floatColorValue * 255 + 0.5);
    }

private:
    Conversions();
};

