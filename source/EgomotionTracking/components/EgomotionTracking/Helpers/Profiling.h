#pragma once

#include <time.h>

class CProfiling
{
public:
    CProfiling() {}

    inline void StartProfiling()
    {
        clock_gettime(CLOCK_MONOTONIC, &m_start);
    }

    inline double GetElapsedMS() const
    {
        clock_gettime(CLOCK_MONOTONIC, &m_tmp);
        double elapsed = (m_tmp.tv_sec - m_start.tv_sec) * 1000.0;
        elapsed += (m_tmp.tv_nsec - m_start.tv_nsec) / 1000000;
        return elapsed;
    }

    inline double GetElapsedUS() const
    {
        clock_gettime(CLOCK_MONOTONIC, &m_tmp);
        double elapsed = (m_tmp.tv_sec - m_start.tv_sec) * 1000000.0;
        elapsed += (m_tmp.tv_nsec - m_start.tv_nsec) / 1000;
        return elapsed;
    }

protected:
    struct timespec m_start;
    mutable struct timespec m_tmp;

};

