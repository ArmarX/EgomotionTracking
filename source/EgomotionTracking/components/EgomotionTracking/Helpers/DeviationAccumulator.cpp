#include "DeviationAccumulator.h"
#include <float.h>
#include <cmath>

CDeviationAccumulator::CDeviationAccumulator()
{
    Reset();
}

void CDeviationAccumulator::Reset()
{
    m_sum = 0.0;
    m_squaredSum = 0.0;
    m_max = -DBL_MAX;
    m_min = DBL_MAX;
    m_cnt = 0;
}

void CDeviationAccumulator::AddDeviation(double deviation)
{
    double absDeviation = fabs(deviation);

    m_sum += absDeviation;
    m_squaredSum += deviation * deviation;
    if (absDeviation > m_max)
    {
        m_max = absDeviation;
    }
    if (absDeviation < m_min)
    {
        m_min = absDeviation;
    }

    ++m_cnt;
}

double CDeviationAccumulator::GetMax() const
{
    if (m_cnt == 0)
    {
        return 0.0;
    }
    return m_max;
}

double CDeviationAccumulator::GetMin() const
{
    if (m_cnt == 0)
    {
        return 0.0;
    }
    return m_min;
}

double CDeviationAccumulator::GetMean() const
{
    if (m_cnt == 0)
    {
        return 0.0;
    }
    return m_sum / m_cnt;
}

double CDeviationAccumulator::GetRMSD() const
{
    if (m_cnt == 0)
    {
        return 0.0;
    }
    return sqrt(m_squaredSum / m_cnt);
}
