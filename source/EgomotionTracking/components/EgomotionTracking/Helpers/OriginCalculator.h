/*
 * OriginCalculator.h
 *
 *  Created on: 15.03.2012
 *      Author: vollert
 */

#pragma once
#include <EgomotionTracking/components/EVP/Foundation/Mathematics/3D/Vector3D.h>
#include <EgomotionTracking/components/EVP/Foundation/Mathematics/3D/Matrix3D.h>
#include <EgomotionTracking/components/EVP/Foundation/Mathematics/4D/Matrix4D.h>

#define ORIGINCALCULATOR_MINEDGELENGTH  20.0
#define ORIGINCALCULATOR_MAXPOINTDISTANCE 3.0
#define ORIGINCALCULATOR_MAXSCALARPRODUCTTOLERANCE  0.1
class COriginCalculator
{
public:
    COriginCalculator();
    virtual ~COriginCalculator();

    enum AddEdgeResult
    {
        eAddedXAxis,
        eAddedYAxis,
        eAddedZAxis,
        eNoConjointAxes,
        eAllreadyFinished,
        eEdgeToShort,
        eNotOrthogonal
    };
    enum CalculatorState
    {
        eAddX,
        eAddY,
        eAddZ,
        eFinished
    };

    CalculatorState GetState();
    AddEdgeResult AddEdge(const EVP::Mathematics::_3D::CVector3D& pointA, const EVP::Mathematics::_3D::CVector3D& pointB);
    void Reset();
    bool GetTransformation(EVP::Mathematics::_4D::CMatrix4D& transformation);

protected:
    bool calculateTransformation();
    CalculatorState m_state;

    EVP::Mathematics::_3D::CVector3D m_base;
    EVP::Mathematics::_3D::CVector3D m_pointXA;
    EVP::Mathematics::_3D::CVector3D m_pointXB;

    float m_lengthX;
    float m_lengthY;
    float m_lengthZ;

    EVP::Mathematics::_3D::CVector3D m_dirX;
    EVP::Mathematics::_3D::CVector3D m_dirY;
    EVP::Mathematics::_3D::CVector3D m_dirZ;

    EVP::Mathematics::_4D::CMatrix4D m_transformation;
};

