#pragma once

#include <Inventor/SbVec3f.h>
#include <Inventor/SbVec4f.h>
#include <Inventor/SbMatrix.h>
#include <Inventor/SbRotation.h>

#include <Calibration/Calibration.h>

#include <cmath>
#include <cstdlib>
#include <cfloat>
#include <assert.h>

class OpenGLCalculations
{
public:
    static inline SbVec4f ProjectWorldPointToEyePoint(const SbMatrix& viewingMatrixGL, const SbVec3f& worldPoint)
    {
        const SbVec4f pointHom(worldPoint[0], worldPoint[1], worldPoint[2], 1.0);
        SbVec4f pointEye;
        viewingMatrixGL.multVecMatrix(pointHom, pointEye);
        return pointEye;
    }

    static inline SbVec3f ProjectEyePointToScreen(const SbMatrix& projectionMatrixGL, const SbVec4f& pointEye, unsigned int imageWidth, unsigned int imageHeight)
    {
        SbVec4f pointClip;
        projectionMatrixGL.multVecMatrix(pointEye, pointClip);

        SbVec3f pointScreen;
        pointScreen[0] = (pointClip[0] / pointClip[3] + 1.0) * (imageWidth / 2);
        pointScreen[1] = (-pointClip[1] / pointClip[3] + 1.0) * (imageHeight / 2);
        /* Store Depth in third Screen component */
        pointScreen[2] = -pointEye[2];

        return pointScreen;
    }

    static inline SbVec3f ProjectWorldPointToScreen(const SbMatrix& projectionMatrixGL, const SbMatrix& viewingMatrixGL, const SbVec3f& worldPoint, unsigned int imageWidth, unsigned int imageHeight)
    {
        /* For Projection See:
         *
         * http://www.songho.ca/opengl/gl_transform.html
         * http://www.songho.ca/opengl/gl_projectionmatrix.html
         */

        const SbVec4f pointEye = ProjectWorldPointToEyePoint(viewingMatrixGL, worldPoint);
        return ProjectEyePointToScreen(projectionMatrixGL, pointEye, imageWidth, imageHeight);
    }

    static inline bool EyePointIsInNearClippingArea(const SbVec4f point, float nearClippingPlane)
    {
        return -point[2] < nearClippingPlane;
    }

    static inline bool FitEyePointToClippingPlane(SbVec4f& clippedPoint, const SbVec4f& visiblePoint, float nearClippingPlane)
    {
        SbVec4f dir = visiblePoint - clippedPoint;
        if (abs(dir[2]) < FLT_EPSILON)
        {
            return false;
        }
        float lambda = -(nearClippingPlane + clippedPoint[2]) / dir[2];
        assert(lambda > 0.0);
        clippedPoint = clippedPoint + lambda * dir;

        return true;
    }

    static SbMatrix CalculateGLViewingMatrixFromGLCameraCoordinates(const SbVec3f& position, const SbRotation& orientation);
    static SbMatrix CalculateGLViewingMatrixFromIVTCameraCoordinates(const Vec3d& translation, const Mat3d& rotation);
    static SbMatrix CalculateGLProjectionMatrixFromIVTCameraCalibration(const CCalibration* cameraCalibration, float near, float far);

    /* Convert between IVT and GL camera orientation coordinate systems. */
    static inline SbRotation ConvertGLCameraOrientationCoordinateSystems(const SbRotation& orientation)
    {
        SbRotation convertion(SbVec3f(1.0, 0.0, 0.0), M_PI);
        convertion *= orientation;
        return convertion;
    }

private:
    OpenGLCalculations();

};

