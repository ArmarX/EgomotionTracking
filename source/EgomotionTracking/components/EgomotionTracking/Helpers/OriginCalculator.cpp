/*
 * OriginCalculator.cpp
 *
 *  Created on: 15.03.2012
 *      Author: vollert
 */

#include "OriginCalculator.h"

COriginCalculator::COriginCalculator()
{
    Reset();
}

COriginCalculator::~COriginCalculator()
{
}

COriginCalculator::CalculatorState COriginCalculator::GetState()
{
    return m_state;
}

COriginCalculator::AddEdgeResult COriginCalculator::AddEdge(const EVP::Mathematics::_3D::CVector3D& pointA, const EVP::Mathematics::_3D::CVector3D& pointB)
{
    if (pointA.GetDistance(pointB) < ORIGINCALCULATOR_MINEDGELENGTH)
    {
        return eEdgeToShort;
    }
    switch (m_state)
    {
        case eAddX:
            m_pointXA = pointA;
            m_pointXB = pointB;
            m_state = eAddY;
            return eAddedXAxis;
            break;
        case eAddY:
            if (pointA.GetDistance(m_pointXA) < ORIGINCALCULATOR_MAXPOINTDISTANCE)
            {
                m_base = pointA;
                m_dirX = m_pointXB - m_base;
                m_dirY = pointB - m_base;
            }
            else if (pointA.GetDistance(m_pointXB) < ORIGINCALCULATOR_MAXPOINTDISTANCE)
            {
                m_base = pointA;
                m_dirX = m_pointXA - m_base;
                m_dirY = pointB - m_base;
            }
            else if (pointB.GetDistance(m_pointXA) < ORIGINCALCULATOR_MAXPOINTDISTANCE)
            {
                m_base = pointB;
                m_dirX = m_pointXB - m_base;
                m_dirY = pointA - m_base;
            }
            else if (pointB.GetDistance(m_pointXB) < ORIGINCALCULATOR_MAXPOINTDISTANCE)
            {
                m_base = pointB;
                m_dirX = m_pointXA - m_base;
                m_dirY = pointA - m_base;
            }
            else
            {
                return eNoConjointAxes;
            }

            m_lengthX = m_dirX.GetLength();
            m_lengthY = m_dirY.GetLength();
            m_dirX.Normalize();
            m_dirY.Normalize();

            if (RealAbs(m_dirX.ScalarProduct(m_dirY)) > ORIGINCALCULATOR_MAXSCALARPRODUCTTOLERANCE)
            {
                return eNotOrthogonal;
            }

            m_state = eAddZ;
            return eAddedYAxis;
            break;
        case eAddZ:
            if (pointA.GetDistance(m_base) < ORIGINCALCULATOR_MAXPOINTDISTANCE)
            {
                m_dirZ = pointB - m_base;
            }
            else if (pointB.GetDistance(m_base) < ORIGINCALCULATOR_MAXPOINTDISTANCE)
            {
                m_dirZ = pointA - m_base;
            }
            else
            {
                return eNoConjointAxes;
            }

            m_lengthZ = m_dirZ.GetLength();
            m_dirZ.Normalize();
            if (RealAbs(m_dirZ.ScalarProduct(m_dirY)) > ORIGINCALCULATOR_MAXSCALARPRODUCTTOLERANCE ||
                RealAbs(m_dirZ.ScalarProduct(m_dirX)) > ORIGINCALCULATOR_MAXSCALARPRODUCTTOLERANCE)
            {
                return eNotOrthogonal;
            }

            calculateTransformation();
            m_state = eFinished;
            return eAddedZAxis;
            break;
        default:

            break;
    }
    return eAllreadyFinished;
}

#include <iostream>
bool COriginCalculator::calculateTransformation()
{
    EVP::Mathematics::_3D::CVector3D dirX = m_dirX;
    EVP::Mathematics::_3D::CVector3D dirY = m_dirY;
    EVP::Mathematics::_3D::CVector3D dirZ = m_dirZ;

    /* normalize Directions */
    if (m_lengthX < m_lengthY && m_lengthX < m_lengthZ)
    {
        /* X is shortest edge */
        if (m_lengthY > m_lengthZ)
        {
            dirZ = m_dirZ - m_dirY * m_dirZ.ScalarProduct(m_dirY);
        }
        else
        {
            m_dirY = m_dirY - m_dirZ * m_dirY.ScalarProduct(m_dirZ);
        }

        dirX = dirY.VectorProduct(dirZ);
    }
    else if (m_lengthY < m_lengthX && m_lengthY < m_lengthZ)
    {
        /* Y is shortest edge */
        if (m_lengthX > m_lengthZ)
        {
            dirZ = m_dirZ - m_dirX * m_dirZ.ScalarProduct(m_dirX);
        }
        else
        {
            m_dirX = m_dirX - m_dirZ * m_dirX.ScalarProduct(m_dirZ);
        }

        dirY = dirZ.VectorProduct(dirX);
    }
    else
    {
        /* Z is shortest edge */
        if (m_lengthY > m_lengthX)
        {
            dirX = m_dirX - m_dirY * m_dirX.ScalarProduct(m_dirY);
        }
        else
        {
            m_dirY = m_dirY - m_dirX * m_dirY.ScalarProduct(m_dirX);
        }
        dirZ = dirX.VectorProduct(dirY);
    }

    dirX.Normalize();
    dirY.Normalize();
    dirZ.Normalize();


    EVP::Mathematics::_3D::CMatrix3D rotation(dirX, dirY, dirZ);
    m_transformation.SetTransformation(rotation, m_base, 1.0);

    //std::cout << "Transformation calculated:" << std::endl << m_transformation.ToString(5);

    return false;

}

void COriginCalculator::Reset()
{
    m_state = eAddX;
}



bool COriginCalculator::GetTransformation(EVP::Mathematics::_4D::CMatrix4D& transformation)
{

    if (m_state == eFinished)
    {
        transformation = m_transformation;
        return true;
    }
    return false;
}



