/*
 * CameraConfiguration.h
 *
 *  Created on: 19.03.2013
 *      Author: abyte
 */

#pragma once

#include "OpenGLCalculations.h"
class CCameraConfiguration
{
public:
    CCameraConfiguration() {};
    CCameraConfiguration(SbVec3f position, SbRotation rotation) : m_position(position), m_rotation(rotation) {}
    SbVec3f m_position;
    SbRotation m_rotation;

    SbMatrix GetViewingMatrixGL() const
    {
        return OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(m_position, m_rotation);
    }


};

