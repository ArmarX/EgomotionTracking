#pragma once

#include <string>
#include <sstream>
#include <fstream>

#define CSVPRIMITIVES_DELIMITER (',')

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>

class CSVEntry
{
public:
    CSVEntry()
    {
        ClearEntry();
    }

    CSVEntry(const CSVEntry& entry)
    {
        m_entryStringStream << entry.GetEntryString();
    }

    template <class valueType>
    void AppendValue(valueType value)
    {
        if (m_entryStringStream.str().size() != 0)
        {
            m_entryStringStream << CSVPRIMITIVES_DELIMITER;
        }
        m_entryStringStream << value;
    }

    template <class valueType>
    void operator<< (valueType value)
    {
        AppendValue(value);
    }

    void AppendEntry(const CSVEntry& entryToAppend)
    {
        if (m_entryStringStream.str().size() != 0)
        {
            m_entryStringStream << CSVPRIMITIVES_DELIMITER;
        }
        m_entryStringStream << entryToAppend.GetEntryString();
    }

    void operator<< (const CSVEntry& entry)
    {
        AppendEntry(entry);
    }

    void ClearEntry()
    {
        m_entryStringStream.str("");
    }

    const std::string GetEntryString() const
    {
        return m_entryStringStream.str();
    }

protected:
    std::stringstream m_entryStringStream;
};

class CSVDocument
{
public:
    CSVDocument()
    {
        ClearDocument();
    }

    void AddEntry(const CSVEntry& entry)
    {
        m_documentStringStream << entry.GetEntryString() << std::endl;
    }

    void operator<<(const CSVEntry& entry)
    {
        AddEntry(entry);
    }

    void ClearDocument()
    {
        m_documentStringStream.str("");
    }

    const std::string GetDocumentString() const
    {
        return m_documentStringStream.str();
    }

protected:
    std::stringstream m_documentStringStream;
};

class CSVPrimitives
{
public:
    static CSVEntry SerializeLocationStateToCSV(const CLocationState& locationState)
    {
        CSVEntry entry;
        entry.AppendValue(locationState.GetX());
        entry.AppendValue(locationState.GetY());
        entry.AppendValue(locationState.GetAlpha());
        return entry;
    }

    static CSVEntry SerializeLocationParticleToCSV(const CLocationParticle& locationParticle)
    {
        CSVEntry entry;
        entry.AppendValue(locationParticle.GetX());
        entry.AppendValue(locationParticle.GetY());
        entry.AppendValue(locationParticle.GetAlpha());
        entry.AppendValue(locationParticle.GetImportanceWeight());
        return entry;
    }

    static bool WriteCSVDocumentToFile(const CSVDocument& doc, const std::string& filename)
    {
        std::ofstream filestream;
        filestream.open(filename.c_str());
        if (!filestream.is_open())
        {
            return false;
        }
        filestream << doc.GetDocumentString();
        filestream.close();
        return true;
    }

private:
    CSVPrimitives();
};

