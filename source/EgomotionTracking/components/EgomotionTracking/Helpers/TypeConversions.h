#pragma once


#include <Inventor/SbVec2d.h>
#include <Inventor/SbVec3d.h>
#include <Inventor/SbVec2f.h>
#include <Inventor/SbVec3f.h>
#include <Math/Math2d.h>
#include <Math/Math3d.h>
#include <Math/FloatMatrix.h>
#include <eigen3/Eigen/Dense>
#include <assert.h>

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>

class TypeConversions
{
public:
    static inline SbVec2d IVT2Coin(const Vec2d& vec2d)
    {
        return SbVec2d(vec2d.x, vec2d.y);
    }
    static inline Vec2d Coin2IVT(const SbVec2d& sbVec2d)
    {
        Vec2d result;
        result.x = sbVec2d[0];
        result.y = sbVec2d[1];
        return result;
    }
    static inline Vec2d Coin2IVT(const SbVec2f& sbVec2d)
    {
        return Coin2IVT(SbVec2d(sbVec2d));
    }
    static inline SbVec3d IVT2Coin(const Vec3d& vec3d)
    {
        return SbVec3d(vec3d.x, vec3d.y, vec3d.z);
    }
    static inline Vec3d Coin2IVT(const SbVec3d& sbVec3d)
    {
        Vec3d result;
        result.x = sbVec3d[0];
        result.y = sbVec3d[1];
        result.z = sbVec3d[2];
        return result;
    }
    static inline Vec3d Coin2IVT(const SbVec3f& sbVec3d)
    {
        return Coin2IVT(SbVec3d(sbVec3d));
    }

    static inline SbVec3f LocationState2Coin(const CLocationState& locationState)
    {
        return SbVec3f(locationState.GetX(), locationState.GetY(), locationState.GetAlpha());
    }

    static inline SbMatrix ConvertEigenToSbMatrix(const Eigen::Matrix4f& eigenMatrix)
    {
        return SbMatrix(eigenMatrix(0, 0), eigenMatrix(1, 0), eigenMatrix(2, 0), eigenMatrix(3, 0),
                        eigenMatrix(0, 1), eigenMatrix(1, 1), eigenMatrix(2, 1), eigenMatrix(3, 1),
                        eigenMatrix(0, 2), eigenMatrix(1, 2), eigenMatrix(2, 2), eigenMatrix(3, 2),
                        eigenMatrix(0, 3), eigenMatrix(1, 3), eigenMatrix(2, 3), eigenMatrix(3, 3));
    }

    static inline SbVec3f ConvertEigenToSbVector(const Eigen::Vector3f& eigenVector)
    {
        return SbVec3f(eigenVector(0), eigenVector(1), eigenVector(2));
    }

private:
    TypeConversions();
};

