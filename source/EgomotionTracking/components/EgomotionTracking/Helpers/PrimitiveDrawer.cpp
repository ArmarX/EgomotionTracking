/*
 * PrimitiveDrawer.cpp
 *
 *  Created on: 07.03.2013
 *      Author: abyte
 */

#include "PrimitiveDrawer.h"

#include "Conversions.h"

PrimitiveDrawer::PrimitiveDrawer()
{

}

PrimitiveDrawer::~PrimitiveDrawer()
{
}

void PrimitiveDrawer::drawVisibleRegion(const CVisibleScreenRegion& visibleScreenRegion, CByteImage* pDrawImage)
{

    PointPair2d lBottom;
    PointPair2d lTop;
    PointPair2d lLeft;
    PointPair2d lRight;

    lBottom.p1.x  = visibleScreenRegion.m_xMin;
    lBottom.p1.y = visibleScreenRegion.m_yMax;
    lBottom.p2.x  = visibleScreenRegion.m_xMax;
    lBottom.p2.y = visibleScreenRegion.m_yMax;

    lTop.p1.x  = visibleScreenRegion.m_xMin;
    lTop.p1.y = visibleScreenRegion.m_yMin;
    lTop.p2.x  = visibleScreenRegion.m_xMax;
    lTop.p2.y = visibleScreenRegion.m_yMin;

    lLeft.p1.x  = visibleScreenRegion.m_xMin;
    lLeft.p1.y = visibleScreenRegion.m_yMin;
    lLeft.p2.x  = visibleScreenRegion.m_xMin;
    lLeft.p2.y = visibleScreenRegion.m_yMax;

    lRight.p1.x  = visibleScreenRegion.m_xMax;
    lRight.p1.y = visibleScreenRegion.m_yMin;
    lRight.p2.x  = visibleScreenRegion.m_xMax;
    lRight.p2.y = visibleScreenRegion.m_yMax;

    PrimitivesDrawer::DrawLine(pDrawImage, lBottom, 0, 0, 255, 1);
    PrimitivesDrawer::DrawLine(pDrawImage, lTop, 0, 0, 255, 1);
    PrimitivesDrawer::DrawLine(pDrawImage, lLeft, 0, 0, 255, 1);
    PrimitivesDrawer::DrawLine(pDrawImage, lRight, 0, 0, 255, 1);

}


PointPair2d getIVTImageLine(const SbVec3f& p1, const SbVec3f& p2)
{
    PointPair2d line;

    line.p1.x = floorf(p1[0]);
    line.p1.y = floorf(p1[1]);
    line.p2.x = floorf(p2[0]);
    line.p2.y = floorf(p2[1]);
    return line;
}

bool inScreen(const SbVec3f p, CByteImage* pImg)
{
    if (p[0] >= 0.0 && p[0] < pImg->width && p[1] >= 0.0 && p[1] < pImg->height)
    {
        return true;
    }
    return false;
}

void PrimitiveDrawer::draw2dTriangles(const std::vector<CFaceTriangle>& screenTriangles, CByteImage* pDrawImage)
{
    if (screenTriangles.size() > 0)
    {
        for (std::vector<CFaceTriangle>::const_iterator it = screenTriangles.begin(); it != screenTriangles.end(); ++it)
        {

            if (inScreen(it->m_p1, pDrawImage) || inScreen(it->m_p2, pDrawImage) || inScreen(it->m_p3, pDrawImage))
            {
                /* floor needed to produce same image result as opengl rendering */
                PointPair2d curLine;
                curLine = getIVTImageLine(it->m_p1, it->m_p2);
                PrimitivesDrawer::DrawLine(pDrawImage, curLine, 255, 0, 0, 1);

                curLine = getIVTImageLine(it->m_p1, it->m_p3);
                PrimitivesDrawer::DrawLine(pDrawImage, curLine, 255, 0, 0, 1);


                curLine = getIVTImageLine(it->m_p2, it->m_p3);
                PrimitivesDrawer::DrawLine(pDrawImage, curLine, 255, 0, 0, 1);

#if 0
                SbVec3f m12 = it->m_p1 + 0.5 * (it->m_p2 - it->m_p1);
                SbVec3f m23 = it->m_p2 + 0.5 * (it->m_p3 - it->m_p2);
                SbVec3f m31 = it->m_p3 + 0.5 * (it->m_p1 - it->m_p3);

                curLine = getIVTImageLine(it->m_p1, m23);
                PrimitivesDrawer::DrawLine(pDrawImage, curLine, 255, 0, 0, 1);

                curLine = getIVTImageLine(it->m_p2, m31);
                PrimitivesDrawer::DrawLine(pDrawImage, curLine, 255, 0, 0, 1);

                curLine = getIVTImageLine(it->m_p3, m12);
                PrimitivesDrawer::DrawLine(pDrawImage, curLine, 255, 0, 0, 1);
#endif

            }
        }

#if 0
        for (std::list<CParametricLineRenderer::CFaceTriangle>::const_iterator it = screenTriangles.begin(); it != screenTriangles.end(); ++it)
        {
            /* floor needed to produce same image result as opengl rendering */
            Vec2d p1, p2, p3;
            p1.x = floorf(it->m_p1[0]);
            p1.y = floorf(it->m_p1[1]);
            p2.x = floorf(it->m_p2[0]);
            p2.y = floorf(it->m_p2[1]);
            p3.x = floorf(it->m_p3[0]);
            p3.y = floorf(it->m_p3[1]);


            //  std::cout << it->m_p1[2] << std::endl;

            int colval = std::min(255.0f, ((it->m_p1[2] + 1) / 5000) * 255);
            PrimitivesDrawer::DrawCircle(pDrawImage, p1, 2, 255 - colval, 0, colval, 2);
            colval = std::min(255.0f, ((it->m_p2[2] + 1) / 5000) * 255);
            PrimitivesDrawer::DrawCircle(pDrawImage, p2, 2, 255 - colval, 0, colval, 2);
            colval = std::min(255.0f, ((it->m_p3[2] + 1) / 5000) * 255);
            PrimitivesDrawer::DrawCircle(pDrawImage, p3, 2, 255 - colval, 0, colval, 2);
        }
#endif
    }
}

void PrimitiveDrawer::draw2dLines(const std::vector<CScreenLine>& screenLines, CByteImage* pDrawImage, bool drawEndpoints)
{
    if (screenLines.size() > 0)
    {
        for (std::vector<CScreenLine>::const_iterator it = screenLines.begin(); it != screenLines.end(); ++it)
        {
            /* floor needed to produce same image result as opengl rendering */
            PointPair2d curLine;
            curLine.p1.x = floorf(it->m_p1[0]);
            curLine.p1.y = floorf(it->m_p1[1]);
            curLine.p2.x = floorf(it->m_p2[0]);
            curLine.p2.y = floorf(it->m_p2[1]);

            PrimitivesDrawer::DrawLine(pDrawImage, curLine, 255, 255, 255, 1);

        }

        if (drawEndpoints)
        {
            for (std::vector<CScreenLine>::const_iterator it = screenLines.begin(); it != screenLines.end(); ++it)
            {
                /* floor needed to produce same image result as opengl rendering */
                PointPair2d curLine;
                curLine.p1.x = floorf(it->m_p1[0]);
                curLine.p1.y = floorf(it->m_p1[1]);
                curLine.p2.x = floorf(it->m_p2[0]);
                curLine.p2.y = floorf(it->m_p2[1]);

                PrimitivesDrawer::DrawCircle(pDrawImage, curLine.p1, 1, 0, 255, 0, 2);
                PrimitivesDrawer::DrawCircle(pDrawImage, curLine.p2, 1, 255, 0, 0, 2);
            }
        }

    }
}

void PrimitiveDrawer::draw2dJunctionPoints(const std::vector<CJunctionPoint>& screenJunctionPoints, CByteImage* pDrawImage)
{
    if (screenJunctionPoints.size() > 0)
    {
        std::vector<CJunctionPoint>::const_iterator junctionPointIterator = screenJunctionPoints.begin();

        for (; junctionPointIterator != screenJunctionPoints.end(); ++junctionPointIterator)
        {
            draw2dJunctionPoint(*junctionPointIterator, pDrawImage);
        }
    }
}

void PrimitiveDrawer::draw2dLineDirection(const Vec2d& startPoint, float direction, float length, int r, int g, int b, int thickness,
        CByteImage* pDrawImage)
{


    Vec2d endPoint = startPoint;
    endPoint.x += length * cos(direction);
    endPoint.y += length * sin(direction);

    PrimitivesDrawer::DrawLine(pDrawImage, startPoint, endPoint, r, g, b, thickness);
}

void PrimitiveDrawer::draw2dJunctionPoint(const CJunctionPoint& junctionPoint, CByteImage* pDrawImage)
{

    if (!junctionPoint.m_isVisible)
    {
        return;
    }

    int r = 0;
    int g = 0;
    int b = 255;


    for (std::list<CLine>::const_iterator junctionLineIterator = junctionPoint.m_junctionLines.begin(); junctionLineIterator != junctionPoint.m_junctionLines.end(); ++junctionLineIterator)
    {
        PointPair2d line;

        line.p1.x = junctionLineIterator->m_p1[0];
        line.p1.y = junctionLineIterator->m_p1[1];
        line.p2.x = junctionLineIterator->m_p2[0];
        line.p2.y = junctionLineIterator->m_p2[1];
        PrimitivesDrawer::DrawLine(pDrawImage, line, r, g, b, 3);

        //PrimitivesDrawer::DrawCircle(pDrawImage, line.p1 , 1, 209, 150, 19, 1);
        //PrimitivesDrawer::DrawCircle(pDrawImage, line.p2 , 1, 255, 20, 147, 1);

    }

    PrimitivesDrawer::DrawCircle(pDrawImage, junctionPoint.m_junctionPoint[0], junctionPoint.m_junctionPoint[1], 1, 255, 0, 0, 3);

}

void PrimitiveDrawer::DrawRandomizedNormalsInEdgeImage(const CByteImage* edgeImage, const float* gradientOrientationField, int r, int g, int b,
        int probabilityInPercent, CByteImage* pDrawImage)
{


    ImageProcessor::ConvertImage(edgeImage, pDrawImage);
    int i = 0;
    for (int y = 0; y < edgeImage->height; ++y)
    {
        for (int x = 0; x < edgeImage->width; ++x, ++i)
        {
            if (edgeImage->pixels[i] == 255)
            {
                if ((rand() % 100) < probabilityInPercent)
                {
                    Vec2d startpoint;
                    startpoint.x = x;
                    startpoint.y = y;
                    PrimitivesDrawer::DrawCircle(pDrawImage, startpoint, 2.0, 0, 255, 0, 2, false);



                    draw2dLineDirection(startpoint, gradientOrientationField[i], 20, 255, 0, 0, 1, pDrawImage);

                }

            }
        }
    }
}


void PrimitiveDrawer::DrawPoint(const SbVec2f& p, int r, int g, int b, CByteImage* pDrawImage)
{
    PrimitivesDrawer::DrawPoint(pDrawImage, p[0], p[1], r, g, b);
}

void PrimitiveDrawer::DrawLine(const SbVec2f& p1, const SbVec2f& p2, int r, int g, int b, int thickness, CByteImage* pDrawImage)
{
    Vec2d tmpP1;
    Vec2d tmpP2;

    tmpP1.x = p1[0];
    tmpP1.y = p1[1];
    tmpP2.x = p2[0];
    tmpP2.y = p2[1];
    PrimitivesDrawer::DrawLine(pDrawImage, tmpP1, tmpP2, r, g, b, thickness);
}

void PrimitiveDrawer::DrawLine(const SbVec3f& p1, const SbVec3f& p2, int r, int g, int b, int thickness, CByteImage* pDrawImage)
{
    Vec2d tmpP1;
    Vec2d tmpP2;

    tmpP1.x = p1[0];
    tmpP1.y = p1[1];
    tmpP2.x = p2[0];
    tmpP2.y = p2[1];
    PrimitivesDrawer::DrawLine(pDrawImage, tmpP1, tmpP2, r, g, b, thickness);
}

void PrimitiveDrawer::DrawLines(const std::vector<CScreenLine>& screenLines, int r, int g, int b, int thickness, CByteImage* pDrawImage)
{
    if (screenLines.size() > 0)
    {
        for (std::vector<CScreenLine>::const_iterator curLine = screenLines.begin(); curLine != screenLines.end(); ++ curLine)
        {
            DrawLine(curLine->m_p1, curLine->m_p2, r, g, b, thickness, pDrawImage);
        }
    }
}

void PrimitiveDrawer::DrawCircle(const SbVec2f& p, float radius, int thickness, int r, int g, int b, CByteImage* pDrawImage)
{
    PrimitivesDrawer::DrawCircle(pDrawImage, p[0], p[1], radius, r, g, b, thickness);
}

