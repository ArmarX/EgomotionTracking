/*
 * XMLPrimitives.h
 *
 *  Created on: 26.02.2013
 *      Author: abyte
 */

#pragma once

#include <QDomElement>
#include <QFile>
#include <QTextStream>
#include <Inventor/SbRotation.h>
#include <Inventor/SbVec3f.h>
#include <Inventor/SbMatrix.h>
#include <iostream>
#include <list>

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/RobotControlState.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/HeadConfiguration.h>

class XMLPrimitives
{
public:
    virtual ~XMLPrimitives();

    static QDomElement SerializeCameraConfigurationToQDomElement(const SbRotation& rotation, const SbVec3f& position, const QString& elementName)
    {
        QDomDocument tmp;
        QDomElement cameraConfigurationElement = tmp.createElement(elementName);

        cameraConfigurationElement.appendChild(SerializeSbRotationToQDomElement(rotation, "cameraRotation"));
        cameraConfigurationElement.appendChild(SerializeSbVec3fToQDomElement(position, "cameraPosition"));

        return cameraConfigurationElement;
    }

    static bool DeserializeQDomElementToCameraConfiguration(QDomElement& cameraConfigurationElement, const QString& elementName, SbRotation& resultRotation, SbVec3f& resultPosition)
    {
        if (cameraConfigurationElement.tagName() != elementName)
        {
            return false;
        }

        QDomElement rotationElement = cameraConfigurationElement.firstChildElement("cameraRotation");
        QDomElement positionElement = cameraConfigurationElement.firstChildElement("cameraPosition");

        if (!rotationElement.isNull() && !positionElement.isNull())
        {
            if (DeserializeQDomElementToSbRotation(rotationElement, "cameraRotation", resultRotation) &&
                DeserializeQDomElementToSbVec(positionElement, "cameraPosition", resultPosition))
            {
                return true;
            }
        }
        return false;
    }

    static QDomElement SerializeSbRotationToQDomElement(const SbRotation& rot, const QString& elementName)
    {
        QDomDocument tmp;
        QDomElement rotElement = tmp.createElement(elementName);

        SbVec3f axis;
        float radians;
        rot.getValue(axis, radians);
        rotElement.setAttribute("radians", radians);
        rotElement.appendChild(SerializeSbVec3fToQDomElement(axis, "axis"));
        return rotElement;
    }

    static bool DeserializeQDomElementToSbRotation(QDomElement& rotDomElement, const QString& elementName, SbRotation& resultRot)
    {
        if (rotDomElement.tagName() != elementName)
        {
            return false;
        }

        QDomAttr attrRadians;

        attrRadians = rotDomElement.attributeNode("radians");


        if (attrRadians.isNull())
        {
            return false;
        }

        bool valueValid;
        float radians = attrRadians.value().toFloat(&valueValid);
        if (!valueValid)
        {
            return false;
        }

        QDomElement axisElement = rotDomElement.firstChild().toElement();

        SbVec3f axis;
        if (axisElement.isNull() || !DeserializeQDomElementToSbVec(axisElement, "axis", axis))
        {
            return false;
        }
        resultRot = SbRotation(axis, radians);
        return true;
    }

    static QDomElement SerializeSbVec3fToQDomElement(const SbVec3f& vec, const QString& elementName)
    {
        QDomDocument tmp;
        QDomElement vecElem = tmp.createElement(elementName);
        vecElem.setAttribute("x", vec[0]);
        vecElem.setAttribute("y", vec[1]);
        vecElem.setAttribute("z", vec[2]);
        return vecElem;
    }

    static QDomElement SerializeSbMatrixToQDomElement(const SbMatrix& mat, const QString& elementName)
    {
        QDomDocument tmp;
        QDomElement matElem = tmp.createElement(elementName);

        matElem.setAttribute("m00", mat[0][0]);
        matElem.setAttribute("m01", mat[0][1]);
        matElem.setAttribute("m02", mat[0][2]);
        matElem.setAttribute("m03", mat[0][3]);

        matElem.setAttribute("m10", mat[1][0]);
        matElem.setAttribute("m11", mat[1][1]);
        matElem.setAttribute("m12", mat[1][2]);
        matElem.setAttribute("m13", mat[1][3]);

        matElem.setAttribute("m20", mat[2][0]);
        matElem.setAttribute("m21", mat[2][1]);
        matElem.setAttribute("m22", mat[2][2]);
        matElem.setAttribute("m23", mat[2][3]);

        matElem.setAttribute("m30", mat[3][0]);
        matElem.setAttribute("m31", mat[3][1]);
        matElem.setAttribute("m32", mat[3][2]);
        matElem.setAttribute("m33", mat[3][3]);

        return matElem;
    }

    static bool DeserializeQDomElementToSbMatrix(QDomElement& matDomElement, SbMatrix& resultMat)
    {
        for (int i = 0; i < 4; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                QString elementName = QString("m%1%2").arg(i).arg(j);
                QDomAttr curAttr = matDomElement.attributeNode(elementName);
                if (curAttr.isNull())
                {
                    return false;
                }
                bool valueValid;
                resultMat[i][j] = curAttr.value().toFloat(&valueValid);
                if (!valueValid)
                {
                    return false;
                }
            }
        }

        return true;
    }

    static bool DeserializeQDomElementToSbVec(QDomElement& vecDomElement, const QString& elementName, SbVec3f& resultVec)
    {
        if (vecDomElement.tagName() != elementName)
        {
            return false;
        }

        QDomAttr attrX, attrY, attrZ;

        attrX = vecDomElement.attributeNode("x");
        attrY = vecDomElement.attributeNode("y");
        attrZ = vecDomElement.attributeNode("z");

        if (attrX.isNull() || attrY.isNull() || attrZ.isNull())
        {
            return false;
        }

        bool valueValid;
        resultVec[0] = attrX.value().toFloat(&valueValid);
        if (!valueValid)
        {
            return false;
        }
        resultVec[1] = attrY.value().toFloat(&valueValid);
        if (!valueValid)
        {
            return false;
        }
        resultVec[2] = attrZ.value().toFloat(&valueValid);
        return valueValid;
    }


    static QDomElement SerializeRobotControlStateToQDomElement(const CRobotControlState& robotControlState, const QString& elementName)
    {
        QDomDocument tmp;
        QDomElement stateElement = tmp.createElement(elementName);
        stateElement.setAttribute("platformAxesTiltError", robotControlState.GetPlatformAxesTiltError());
        stateElement.setAttribute("velocityX", robotControlState.GetPlatformVelocityX());
        stateElement.setAttribute("velocityY", robotControlState.GetPlatformVelocityY());
        stateElement.setAttribute("velocityAlpha", robotControlState.GetPlatformVelocityAlpha());
        stateElement.setAttribute("deltaT", robotControlState.GetDeltaT());
        return stateElement;
    }

    static bool DeserializeQDomElementToRobotControlState(QDomElement& robotControlStateDomElement, CRobotControlState& resultRobotControlState)
    {

        QDomAttr attrPlatformAxesTiltError, attrVelocityX, attrVelocityY, attrVelocityAlpha, attrDeltaT;

        attrPlatformAxesTiltError = robotControlStateDomElement.attributeNode("platformAxesTiltError");
        attrVelocityX = robotControlStateDomElement.attributeNode("velocityX");
        attrVelocityY = robotControlStateDomElement.attributeNode("velocityY");
        attrVelocityAlpha = robotControlStateDomElement.attributeNode("velocityAlpha");
        attrDeltaT = robotControlStateDomElement.attributeNode("deltaT");

        if (attrPlatformAxesTiltError.isNull() || attrVelocityX.isNull() || attrVelocityY.isNull() || attrVelocityAlpha.isNull() || attrDeltaT.isNull())
        {
            return false;
        }

        bool valueValid;
        resultRobotControlState.SetPlatformAxesTiltError(attrPlatformAxesTiltError.value().toFloat(&valueValid));
        if (!valueValid)
        {
            return false;
        }
        resultRobotControlState.SetPlatformVelocityX(attrVelocityX.value().toFloat(&valueValid));
        if (!valueValid)
        {
            return false;
        }
        resultRobotControlState.SetPlatformVelocityY(attrVelocityY.value().toFloat(&valueValid));
        if (!valueValid)
        {
            return false;
        }
        resultRobotControlState.SetPlatformVelocityAlpha(attrVelocityAlpha.value().toFloat(&valueValid));
        if (!valueValid)
        {
            return false;
        }
        resultRobotControlState.SetDeltaT(attrDeltaT.value().toFloat(&valueValid));
        return valueValid;
    }

    static QDomElement SerializeHeadConfigurationToQDomElement(const CHeadConfiguration& headConfiguration, const QString& elementName)
    {
        QDomDocument tmp;
        QDomElement headElement = tmp.createElement(elementName);
        headElement.setAttribute("roll", headConfiguration.GetNeckRoll());
        headElement.setAttribute("pitch", headConfiguration.GetNeckPitch());
        headElement.setAttribute("yaw", headConfiguration.GetNeckYaw());
        return headElement;
    }

    static bool DeserializeQDomElementToHeadConfiguration(QDomElement& headConfigurationDomElement, CHeadConfiguration& resultHeadConfiguration)
    {
        QDomAttr attrRoll, attrPitch, attrYaw;

        attrRoll = headConfigurationDomElement.attributeNode("roll");
        attrPitch = headConfigurationDomElement.attributeNode("pitch");
        attrYaw = headConfigurationDomElement.attributeNode("yaw");

        if (attrRoll.isNull() || attrPitch.isNull() || attrYaw.isNull())
        {
            return false;
        }

        bool valueValid;
        resultHeadConfiguration.SetNeckRoll(attrRoll.value().toFloat(&valueValid));
        if (!valueValid)
        {
            return false;
        }
        resultHeadConfiguration.SetNeckPitch(attrPitch.value().toFloat(&valueValid));
        if (!valueValid)
        {
            return false;
        }
        resultHeadConfiguration.SetNeckYaw(attrYaw.value().toFloat(&valueValid));
        return valueValid;
    }

    static QDomElement SerializeLocationStateToQDomElement(const CLocationState& locationState, const QString& elementName)
    {
        QDomDocument tmp;
        QDomElement stateElement = tmp.createElement(elementName);
        stateElement.setAttribute("x", locationState.GetX());
        stateElement.setAttribute("y", locationState.GetY());
        stateElement.setAttribute("alpha", locationState.GetAlpha());
        return stateElement;
    }

    static bool DeserializeQDomElementToLocationState(QDomElement& locationDomElement, CLocationState& resultLocationState)
    {

        QDomAttr attrX, attrY, attrAlpha;

        attrX = locationDomElement.attributeNode("x");
        attrY = locationDomElement.attributeNode("y");
        attrAlpha = locationDomElement.attributeNode("alpha");

        if (attrX.isNull() || attrY.isNull() || attrAlpha.isNull())
        {
            return false;
        }

        bool valueValid;
        resultLocationState.SetX(attrX.value().toFloat(&valueValid));
        if (!valueValid)
        {
            return false;
        }
        resultLocationState.SetY(attrY.value().toFloat(&valueValid));
        if (!valueValid)
        {
            return false;
        }
        resultLocationState.SetAlpha(attrAlpha.value().toFloat(&valueValid));
        return valueValid;
    }

    template<class ListElementType>
    static QDomElement SerializeList(const std::list<ListElementType>& list, const QString& elementNameList, bool serializeOnlyVisibleElements = false)
    {
        QDomDocument tmp;
        QDomElement listQDomElement = tmp.createElement(elementNameList);

        if (list.size() > 0)
        {
            typename std::list<ListElementType>::const_iterator listElementIterator = list.begin();
            for (; listElementIterator != list.end(); ++listElementIterator)
            {
                if (!serializeOnlyVisibleElements || listElementIterator->m_isVisible)
                {
                    listQDomElement.appendChild(listElementIterator->SerializeToQDomElement());
                }
            }
        }

        return listQDomElement;
    }

    template<class ListElementType>
    static unsigned int DeserializeList(QDomElement& listQDomElement, QString elementsDomName, bool deserializeOnlyVisibleElements, std::list<ListElementType>& resultElementList)
    {
        QDomNodeList elementListQDom = listQDomElement.elementsByTagName(elementsDomName);
        if (elementListQDom.isEmpty())
        {
            return 0;
        }

        unsigned int numberOfLoadedElements = 0;
        for (int i = 0; i < elementListQDom.size(); ++i)
        {
            QDomElement curElement = elementListQDom.item(i).toElement();
            if (!curElement.isNull())
            {
                ListElementType curInstance;
                if (curInstance.DeserializeFromQDomElement(curElement))
                {
                    if (!deserializeOnlyVisibleElements || curInstance.m_isVisible)
                    {
                        resultElementList.push_back(curInstance);
                        ++numberOfLoadedElements;
                    }
                }
            }
        }
        return numberOfLoadedElements;
    }

    static bool ReadXMLDocumentFromFile(QDomDocument& doc, const QString& filename)
    {
        QFile file(filename);

        if (!file.open(QIODevice::ReadOnly))
        {
            return false;
        }

        QString errorMsg;
        int errorLine, errorColumn;
        if (!doc.setContent(&file, &errorMsg, &errorLine, &errorColumn))
        {
            std::cout << "XML Parsing failed: " << errorMsg.toStdString() << "  " << errorLine << ", " << errorColumn << std::endl;
            file.close();
            return false;
        }
        file.close();
        return true;
    }
    static bool WriteXMLDocumentToFile(const QDomDocument& doc, const QString& filename)
    {
        QFile file(filename);
        if (!file.open(QIODevice::WriteOnly))
        {
            return false;
        }

        QTextStream streamFile(&file);
        streamFile << doc.toString();
        file.close();
        return true;
    }
private:
    XMLPrimitives();
};

