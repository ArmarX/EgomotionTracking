/*
 * PrimitiveDrawer.h
 *
 *  Created on: 07.03.2013
 *      Author: abyte
 */

#pragma once

#include <list>
#include <Inventor/SbVec2f.h>
#include <Inventor/SbVec3f.h>

#include <Image/ByteImage.h>
#include <Image/PrimitivesDrawer.h>
#include <Image/ImageProcessor.h>
#include <Math/Math2d.h>

#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/ParametricLineRenderer.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/Line.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/JunctionPoint.h>

class PrimitiveDrawer
{
public:
    virtual ~PrimitiveDrawer();

    static void draw2dLineDirection(const Vec2d& startPoint, float direction, float length, int r, int g, int b, int thickness, CByteImage* pDrawImage);

    static void draw2dTriangles(const std::vector<CFaceTriangle>& screenTriangles, CByteImage* pDrawImage);
    static void draw2dLines(const std::vector<CScreenLine>& screenLines, CByteImage* pDrawImage, bool drawEndpoints = false);
    static void drawVisibleRegion(const CVisibleScreenRegion& visibleScreenRegion, CByteImage* pDrawImage);

    static void draw2dJunctionPoints(const std::vector<CJunctionPoint>& screenJunctionPoints, CByteImage* pDrawImage);

    static void draw2dJunctionPoint(const CJunctionPoint& screenJunctionPoint, CByteImage* pDrawImage);


    static void DrawRandomizedNormalsInEdgeImage(const CByteImage* edgeImage, const float* gradientOrientationField, int r, int g, int b, int probabilityInPercent, CByteImage* pDrawImage);

    static void DrawPoint(const SbVec2f& p, int r, int g, int b, CByteImage* pDrawImage);
    static void DrawLine(const SbVec2f& p1, const SbVec2f& p2, int r, int g, int b, int thickness, CByteImage* pDrawImage);
    static void DrawLine(const SbVec3f& p1, const SbVec3f& p2, int r, int g, int b, int thickness, CByteImage* pDrawImage);
    static void DrawLines(const std::vector<CScreenLine>& screenLines, int r, int g, int b, int thickness, CByteImage* pDrawImage);

    static void DrawCircle(const SbVec2f& p, float radius, int thickness, int r, int g, int b, CByteImage* pDrawImage);

private:
    PrimitiveDrawer();
};
