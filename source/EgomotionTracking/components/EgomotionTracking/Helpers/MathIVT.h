#pragma once

#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>

#include <Math/Math3d.h>
#include <Math/Math2d.h>
#include <Math/LinearAlgebra.h>
#include <Math/FloatMatrix.h>

// Eigen
#include <eigen3/Eigen/Eigen>

//#include <newmat/newmat.h>
//#include <newmat/newmatap.h>

#include "TypeConversions.h"
class MathIVT
{

public:

    static inline void DyadicTensorProcut(const Vec3d& v, const Vec3d& u, Mat3d& resultMat)
    {
        resultMat.r1 = v.x * u.x;
        resultMat.r2 = v.x * u.y;
        resultMat.r3 = v.x * u.z;
        resultMat.r4 = v.y * u.x;
        resultMat.r5 = v.y * u.y;
        resultMat.r6 = v.y * u.z;
        resultMat.r7 = v.z * u.x;
        resultMat.r8 = v.z * u.y;
        resultMat.r9 = v.z * u.z;
    }

    static inline void DyadicTensorProcut(const Vec3d& v, Mat3d& resultMat)
    {
        const double rxx = v.x * v.x;
        const double rxy = v.x * v.y;
        const double rxz = v.x * v.z;
        const double ryy = v.y * v.y;
        const double ryz = v.y * v.z;
        const double rzz = v.z * v.z;

        resultMat.r1 = rxx;
        resultMat.r2 = rxy;
        resultMat.r3 = rxz;
        resultMat.r4 = rxy;
        resultMat.r5 = ryy;
        resultMat.r6 = ryz;
        resultMat.r7 = rxz;
        resultMat.r8 = ryz;
        resultMat.r9 = rzz;
    }

    static inline bool CalculateSVD(const Mat3d& mat, Mat3d& W, Mat3d& U)
    {
        Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> A(3, 3);

        A(0, 0) = mat.r1;
        A(0, 1) = mat.r2;
        A(0, 2) = mat.r3;
        A(1, 0) = mat.r4;
        A(1, 1) = mat.r5;
        A(1, 2) = mat.r6;
        A(2, 0) = mat.r7;
        A(2, 1) = mat.r8;
        A(2, 2) = mat.r9;

        Eigen::JacobiSVD<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> > SVD(A, Eigen::ComputeFullU | Eigen::ComputeFullV);

        Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> D = SVD.singularValues();

        Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> RU = SVD.matrixU();

        W.r1 = D(0, 0);
        W.r2 = 0.0f;
        W.r3 = 0.0f;
        W.r4 = 0.0f;
        W.r5 = D(1, 0);
        W.r6 = 0.0f;
        W.r7 = 0.0f;
        W.r8 = 0.0f;
        W.r9 = D(2, 0);

        U.r1 = RU(0, 0);
        U.r2 = RU(0, 1);
        U.r3 = RU(0, 2);
        U.r4 = RU(1, 0);
        U.r5 = RU(1, 1);
        U.r6 = RU(1, 2);
        U.r7 = RU(2, 0);
        U.r8 = RU(2, 1);
        U.r9 = RU(2, 2);

        return true;

        //        Matrix A(3,3);
        //        IVT2Newmat(mat, A);
        //        DiagonalMatrix D(3);
        //        Matrix UNM(3,3);
        //        try
        //        {
        //            SVD(A, D, UNM);
        //        } catch (Exception)
        //        {
        //            COUT_STATIC_WARNING << "newmat SVD failed: " << Exception::what() << std::endl;
        //            W.r1 = 1.0;
        //            W.r2 = 0.0;
        //            W.r3 = 0.0;
        //            W.r4 = 1.0;
        //            U.r1 = 1.0;
        //            U.r2 = 0.0;
        //            U.r3 = 0.0;
        //            U.r4 = 1.0;
        //            return false;
        //        }
        //        Newmat2IVT(D, W);
        //        Newmat2IVT(UNM, U);
        //        return true;
    }

    static inline bool CalculateSVD(const Mat2d& mat, Mat2d& W, Mat2d& U)
    {
        Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> A(2, 2);

        A(0, 0) = mat.r1;
        A(0, 1) = mat.r2;
        A(1, 0) = mat.r3;
        A(1, 1) = mat.r4;

        Eigen::JacobiSVD<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> > SVD(A, Eigen::ComputeFullU | Eigen::ComputeFullV);

        Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> D = SVD.singularValues();

        Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> RU = SVD.matrixU();

        W.r1 = D(0, 0);
        W.r2 = 0.0f;
        W.r3 = D(1, 0);
        W.r4 = 0.0f;

        U.r1 = RU(0, 0);
        U.r2 = RU(0, 1);
        U.r3 = RU(1, 0);
        U.r4 = RU(1, 1);

        return true;

        //        Matrix A(2,2);
        //        IVT2Newmat(mat, A);
        //        DiagonalMatrix D(2);
        //        Matrix UNM(2,2);
        //        try
        //        {
        //            SVD(A, D, UNM);
        //        } catch (Exception)
        //        {
        //            COUT_STATIC_WARNING << "newmat SVD failed: " << Exception::what() << std::endl;
        //            W.r1 = 1.0;
        //            W.r2 = 0.0;
        //            W.r3 = 0.0;
        //            W.r4 = 1.0;
        //            U.r1 = 1.0;
        //            U.r2 = 0.0;
        //            U.r3 = 0.0;
        //            U.r4 = 1.0;
        //            return false;
        //        }
        //        Newmat2IVT(D, W);
        //        Newmat2IVT(UNM, U);
        //        return true;
    }

    static inline float CalculateMahalanobisDistance(const Vec3d& v1, const Vec3d& v2, const Mat3d& S)
    {
        Vec3d d, tmp;
        Mat3d invS;

        Math3d::SubtractVecVec(v1, v2, d);
        Math3d::Invert(S, invS);

        Math3d::MulMatVec(invS, d, tmp);
        return sqrt(Math3d::ScalarProduct(d, tmp));
    }

    static inline double CalculateMatrixDeterminant(const Mat3d& mat)
    {
        return Math3d::Det(mat);
        //        Matrix A(3, 3);
        //        IVT2Newmat(mat, A);

        //        return A.Determinant();
    }


protected:

    MathIVT();

    //    static void IVT2Newmat(const Mat3d & ivtMat, Matrix & newMat)
    //    {
    //        newMat << ivtMat.r1 << ivtMat.r2 << ivtMat.r3
    //               << ivtMat.r4 << ivtMat.r5 << ivtMat.r6
    //               << ivtMat.r7 << ivtMat.r8 << ivtMat.r9;
    //    }

    //    static void IVT2Newmat(const Mat2d & ivtMat, Matrix & newMat)
    //    {
    //        newMat << ivtMat.r1 << ivtMat.r2 << ivtMat.r3
    //               << ivtMat.r4;
    //    }

    //    static void Newmat2IVT(const Matrix & newMat, Mat2d & ivtMat)
    //    {
    //        ivtMat.r1 = newMat(1,1);
    //        ivtMat.r2 = newMat(1,2);
    //        ivtMat.r3 = newMat(2,1);
    //        ivtMat.r4 = newMat(2,2);
    //    }

    //    static void Newmat2IVT(const Matrix & newMat, Mat3d & ivtMat)
    //    {
    //        ivtMat.r1 = newMat(1,1);
    //        ivtMat.r2 = newMat(1,2);
    //        ivtMat.r3 = newMat(1,3);

    //        ivtMat.r4 = newMat(2,1);
    //        ivtMat.r5 = newMat(2,2);
    //        ivtMat.r6 = newMat(2,3);

    //        ivtMat.r7 = newMat(3,1);
    //        ivtMat.r8 = newMat(3,2);
    //        ivtMat.r9 = newMat(3,3);
    //    }
};

