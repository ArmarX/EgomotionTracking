/*
 * DebuggingPrimitives.h
 *
 *  Created on: 12.03.2013
 *      Author: abyte
 */

#pragma once

#include <Inventor/SbMatrix.h>
#include <Inventor/SbVec3f.h>
#include <Inventor/SbVec2f.h>
#include <Inventor/SbVec4f.h>
#include <Inventor/SbRotation.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/RobotControlState.h>

#include "MathIVT.h"
class DebuggingPrimitives
{
public:
    virtual ~DebuggingPrimitives();

    static void printSbMatrix(const SbMatrix& mat)
    {
        printf("|%f | %f | %f | %f |\n", mat[0][0], mat[0][1], mat[0][2], mat[0][3]);
        printf("|%f | %f | %f | %f |\n", mat[1][0], mat[1][1], mat[1][2], mat[1][3]);
        printf("|%f | %f | %f | %f |\n", mat[2][0], mat[2][1], mat[2][2], mat[2][3]);
        printf("|%f | %f | %f | %f |\n", mat[3][0], mat[3][1], mat[3][2], mat[3][3]);

    }

    static void printSbVec2f(const SbVec2f& vec)
    {
        std::cout << "( " << vec[0] << " | " << vec[1] << " )" << std::endl;
    }
    static void printSbVec3f(const SbVec3f& vec)
    {
        std::cout << "( " << vec[0] << " | " << vec[1] << " | " << vec[2] << " )" << std::endl;
    }
    static void printSbVec4f(const SbVec4f& vec)
    {
        std::cout << "( " << vec[0] << " | " << vec[1] << " | " << vec[2] << " | " << vec[3] << " )" << std::endl;
    }

    static void printSbRotation(const SbRotation& rot)
    {
        SbVec3f axis;
        float radians;
        rot.getValue(axis, radians);
        printf("rotAxis: ( %f | %f | %f ) radians: %f\n", axis[0], axis[1], axis[2], radians);
    }

    static void printCLocationState(const CLocationState& ls)
    {
        std::cout << "x = " << ls.GetX() << ",  y = " << ls.GetY() << ",  alpha = "  << ls.GetAlpha() << std::endl;
    }
    static void printCRobotControlState(const CRobotControlState& rcs)
    {
        std::cout << "velocityX = " << rcs.GetPlatformVelocityX() << ",  velocityY = " << rcs.GetPlatformVelocityY() << ",  velocityAlpha = " << rcs.GetPlatformVelocityAlpha() << std::endl;
    }

    static void printVec3d(const Vec3d& vec)
    {
        printf("( %f | %f | %f )\n", vec.x, vec.y, vec.z);
    }
    static void printVec2d(const Vec2d& vec)
    {
        printf("( %f | %f )\n", vec.x, vec.y);
    }
    static void printMat3d(const Mat3d& mat)
    {
        printf("|%f | %f | %f |\n", mat.r1, mat.r2, mat.r3);
        printf("|%f | %f | %f |\n", mat.r4, mat.r5, mat.r6);
        printf("|%f | %f | %f |\n", mat.r7, mat.r8, mat.r9);
    }
    static void printMat2d(const Mat2d& mat)
    {
        printf("|%f | %f |\n", mat.r1, mat.r2);
        printf("|%f | %f |\n", mat.r3, mat.r4);
    }

private:
    DebuggingPrimitives();
};

