/*
 * Calculations.h
 *
 *  Created on: 21.03.2013
 *      Author: abyte
 */

#pragma once

#include <math.h>

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
#include <Inventor/SbRotation.h>
#include <Inventor/SbVec3f.h>
#include <Inventor/SbMatrix.h>

#include <eigen3/Eigen/Dense>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/DebuggingPrimitives.h>

class Calculations
{
public:
    virtual ~Calculations();

    static inline int CalculatePixelBufferLocation(int x, int y, int imageWidth)
    {
        return y * imageWidth + x;
    }

    static inline float RoundUpToNextMultiple(float value, float multiple)
    {
        return multiple * ceil(value / multiple);
    }

    static inline float RoundDownToNextMultiple(float value, float multiple)
    {
        return multiple * floor(value / multiple);
    }

    static inline float SaturizeAbsolute(float value, float maxAbsoluteValue, float& saturationFactor)
    {
        if (value > maxAbsoluteValue)
        {
            saturationFactor = maxAbsoluteValue / value;
            return maxAbsoluteValue;
        }
        else if (value < -maxAbsoluteValue)
        {
            saturationFactor = -maxAbsoluteValue / value;
            return -maxAbsoluteValue;
        }
        saturationFactor = 1.0;
        return value;
    }

    static inline float SaturizeAbsolute(float value, float maxAbsoluteValue)
    {
        if (value > maxAbsoluteValue)
        {
            return maxAbsoluteValue;
        }
        else if (value < -maxAbsoluteValue)
        {
            return -maxAbsoluteValue;
        }
        return value;
    }


    static inline float SaturizeToLimits(float value, float minLimit, float maxLimit)
    {
        if (value < minLimit)
        {
            return minLimit;
        }
        if (value > maxLimit)
        {
            return maxLimit;
        }
        return value;
    }

    static inline float SaturizeToCircularInterval(float value, float minLimit, float maxLimit)
    {
        const float rangeLength = maxLimit - minLimit;
        while (value > maxLimit)
        {
            value -= rangeLength;
        }
        while (value < minLimit)
        {
            value += rangeLength;
        }
        return value;
    }

    static inline bool CalculateRobotCameraPosition(const CLocationState& location, const SbMatrix& transformationPlatformToCamera, SbVec3f& cameraTranslation, SbRotation& cameraRotation)
    {
        SbRotation platformRotation;
        platformRotation.setValue(SbVec3f(0.0, 0.0, 1.0), location.GetAlpha());

        SbMatrix platformPosition;
        platformPosition.setTransform(SbVec3f(location.GetX(), location.GetY(), 0.0), platformRotation, SbVec3f(1.0, 1.0, 1.0));

        SbMatrix cameraPosition = platformPosition.multLeft(transformationPlatformToCamera);

        SbVec3f tmp1;
        SbRotation tmp2;
        cameraPosition.getTransform(cameraTranslation, cameraRotation, tmp1, tmp2);

        return true;
    }

    static inline float Square(float value)
    {
        return value * value;
    }

    static inline int Sign(float value)
    {
        if (value > 0)
        {
            return 1;
        }
        return -1;
    }

    static inline double CalculateMahalanobisDistanceSixDimensions(const Eigen::Matrix<double, 6, 1>& vector1, const Eigen::Matrix<double, 6, 1>& vector2, const Eigen::Matrix<double, 6, 6>& matrix)
    {
        Eigen::Matrix<double, 6, 1>  deviation;
        Eigen::Matrix<double, 6, 1> tempVector;
        Eigen::Matrix<double, 6, 6> inverse;

        deviation = vector1 - vector2;
        inverse = matrix.inverse();

        tempVector = inverse * deviation;

        return sqrt(deviation.dot(tempVector));
    }

private:
    Calculations();
};

