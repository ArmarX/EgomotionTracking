#pragma once
#include <typeinfo>
#include <iostream>

#define CONSOLE_SIGNAL_GREEN "\033[32m"
#define CONSOLE_SIGNAL_RED "\033[31m"
#define CONSOLE_SIGNAL_BLUE "\033[34m"
#define CONSOLE_SIGNAL_YELLOW "\033[33m"
#define CONSOLE_SIGNAL_MAGENTA "\033[35m"
#define CONSOLE_SIGNAL_NORMAL "\033[0m"


#define COUT std::cout << CONSOLE_SIGNAL_NORMAL

#define COUT_INFO std::cout << CONSOLE_SIGNAL_GREEN << typeid(*this).name() << "::" << __FUNCTION__ << "(): "
#define COUT_ERROR std::cout << CONSOLE_SIGNAL_RED << typeid(*this).name() << "::" << __FUNCTION__ << "(): "
#define COUT_WARNING std::cout << CONSOLE_SIGNAL_YELLOW << typeid(*this).name() << "::" << __FUNCTION__ << "(): "
#define COUT_DEBUG1 std::cout << CONSOLE_SIGNAL_BLUE << "#DEBUG1# " << typeid(*this).name() << "::" << __FUNCTION__ << "(): "
#define COUT_DEBUG2 std::cout << CONSOLE_SIGNAL_MAGENTA << "#DEBUG2# " << typeid(*this).name() << "::" << __FUNCTION__ << "(): "

#define COUT_STATIC_INFO std::cout << CONSOLE_SIGNAL_GREEN << __FUNCTION__ << "(): "
#define COUT_STATIC_ERROR std::cout << CONSOLE_SIGNAL_RED << __FUNCTION__ << "(): "
#define COUT_STATIC_WARNING std::cout << CONSOLE_SIGNAL_YELLOW <<  __FUNCTION__ << "(): "
#define COUT_STATIC_DEBUG1 std::cout << CONSOLE_SIGNAL_BLUE << "#DEBUG1# " <<  __FUNCTION__ << "(): "
#define COUT_STATIC_DEBUG2 std::cout << CONSOLE_SIGNAL_MAGENTA << "#DEBUG2# " << __FUNCTION__ << "(): "

