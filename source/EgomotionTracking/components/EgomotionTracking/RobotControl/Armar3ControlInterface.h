/*
 * Armar3ControlInterface.h
 *
 *  Created on: 12.03.2013
 *      Author: abyte
 */

#pragma once

#include "RobotControlInterface.h"
#include "ControllableRobotControlInterface.h"

#include <Inventor/SbMatrix.h>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/DebuggingPrimitives.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/CameraCalibration/StereoCalibrationGL.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/HeadConfiguration.h>

class CArmar3ControlInterface: public CControllableRobotControlInterface
{
public:
    CArmar3ControlInterface();
    ~CArmar3ControlInterface() override;
    bool DispatchRobot() override = 0;

    const SbMatrix& GetCameraProjectionMatrixGL(CCameraID id) const override;

    void GetNeckRPYBounds(float& neckRollMin, float& neckRollMax,
                          float& neckPitchMin, float& neckPitchMax,
                          float& neckYawMin, float& neckYawMax) const override
    {
        m_currentHeadConfiguration.GetLimits(neckRollMin, neckRollMax,
                                             neckPitchMin, neckPitchMax,
                                             neckYawMin, neckYawMax);
    }
    virtual void SetStereoCalibrationGL(const CStereoCalibrationGL& stereoCalibrationGL);

    // Needed by TestModelElementVisibility.
    CStereoCalibrationGL const* getStereoCalibrationGL() const;

protected:
    bool SetInternNeckRPY(float roll, float pitch, float yaw);

    SbMatrix m_transformationPlatformToCenterOfArms;
    SbMatrix m_transformationCenterOfArmsToHeadBase;
    SbMatrix m_transformationHeadBaseToLeftCamera;
    SbMatrix m_transformationHeadBaseToRightCamera;


    CHeadConfiguration m_currentHeadConfiguration;

    CStereoCalibrationGL m_stereoCalibrationGL;
};

