#pragma once

#include "RobotControlInterface.h"
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/HeadConfiguration.h>

class CControllableRobotControlInterface : public CRobotControlInterface
{
public:
    CControllableRobotControlInterface();

    virtual bool SetTargetRobotControlState(const CRobotControlState& robotControlState) = 0;
    virtual bool SetTargetRobotHeadConfiguration(const CHeadConfiguration& targetHeadConfiguration) = 0;

    virtual void GetNeckRPYBounds(float& neckRollMin, float& neckRollMax, float& neckPitchMin, float& neckPitchMax, float& neckYawMin, float& neckYawMax) const = 0;
};

