/*
 * RobotPlatformTransformation.h
 *
 *  Created on: 25.03.2013
 *      Author: abyte
 */

#pragma once

#include <Inventor/SbRotation.h>
#include <Inventor/SbVec3f.h>

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
class CRobotPlatformTransformation
{
public:
    CRobotPlatformTransformation();
    virtual ~CRobotPlatformTransformation();

    static CLocationState SetPlatformRelative(const CLocationState& oldLocationState, float platformDeltaX, float platformDeltaY, float platformDeltaAlpha)
    {

        SbRotation rot = SbRotation(SbVec3f(0.0, 0.0, 1.0), oldLocationState.GetAlpha());

        SbVec3f motionVectorRobot(platformDeltaX, platformDeltaY, 0.0);
        SbVec3f motionVectorWorld;

        CLocationState newLocation;
        rot.multVec(motionVectorRobot, motionVectorWorld);


        newLocation.SetX(oldLocationState.GetX() + motionVectorWorld[0]);
        newLocation.SetY(oldLocationState.GetY() + motionVectorWorld[1]);
        newLocation.SetAlpha(Conversions::NormalizeRadiansTo2PiInterval(oldLocationState.GetAlpha() + platformDeltaAlpha));

        return newLocation;
    }
};

