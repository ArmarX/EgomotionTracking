/*
 * Armar3ControlInterface.cpp
 *
 *  Created on: 12.03.2013
 *      Author: abyte
 */

#include "Armar3ControlInterface.h"

CArmar3ControlInterface::CArmar3ControlInterface()
{
    m_transformationPlatformToCenterOfArms.setTranslate(SbVec3f(0.0, 123.0, 1375.0));
    m_transformationCenterOfArmsToHeadBase.setTranslate(SbVec3f(0.0, 0.0, 118.0));
    m_transformationHeadBaseToLeftCamera.setTransform(SbVec3f(-46.5, 100.0, 174.5), SbRotation(SbVec3f(1.0, 0.0, 0.0), -M_PI / 2), SbVec3f(1.0, 1.0, 1.0));
    m_transformationHeadBaseToRightCamera.setTransform(SbVec3f(46.5, 100.0, 174.5), SbRotation(SbVec3f(1.0, 0.0, 0.0), -M_PI / 2), SbVec3f(1.0, 1.0, 1.0));

    m_currentHeadConfiguration.SetLimits(Conversions::DegreeToRadians(-33),
                                         Conversions::DegreeToRadians(33),
                                         Conversions::DegreeToRadians(-40),
                                         Conversions::DegreeToRadians(40),
                                         Conversions::DegreeToRadians(-113),
                                         Conversions::DegreeToRadians(116));

}

CArmar3ControlInterface::~CArmar3ControlInterface()
{
}

const SbMatrix& CArmar3ControlInterface::GetCameraProjectionMatrixGL(CCameraID id) const
{
    if (id == eRightCamera)
    {
        return m_stereoCalibrationGL.GetRightProjectionMatrixGL();
    }
    return m_stereoCalibrationGL.GetLeftProjectionMatrixGL();
}

void CArmar3ControlInterface::SetStereoCalibrationGL(const CStereoCalibrationGL& stereoCalibrationGL)
{
    m_stereoCalibrationGL = stereoCalibrationGL;
}



bool CArmar3ControlInterface::SetInternNeckRPY(float roll, float pitch, float yaw)
{

    m_currentHeadConfiguration.SetNeckRoll(roll);
    m_currentHeadConfiguration.SetNeckPitch(pitch);
    m_currentHeadConfiguration.SetNeckYaw(yaw);
    return true;
}

CStereoCalibrationGL const* CArmar3ControlInterface::getStereoCalibrationGL() const
{
    return &m_stereoCalibrationGL;
}

