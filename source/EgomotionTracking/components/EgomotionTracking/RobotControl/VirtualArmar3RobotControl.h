/*
 * VirtualRobotControl.h
 *
 *  Created on: 11.03.2013
 *      Author: abyte
 */

#pragma once

#include "Armar3ControlInterface.h"
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/SimpleLocationRegion.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/RobotControlState.h>
#include <EgomotionTracking/components/EgomotionTracking/LocationEstimator/LocationEstimator.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/LocationStatePredictor/LocationStatePredictorInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/RandomGenerator/ParkMillerRandomGenerator.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Profiling.h>

#include <Inventor/nodes/SoNodes.h>
#include <Inventor/SoOffscreenRenderer.h>

#include <EgomotionTracking/components/EVP/Visualization/Geometry/3D/OpenInventor/Extensions/ExtendedPerspectiveCamera.h>
#include <EgomotionTracking/components/EVP/VisualSpace/Cameras/GeometricCalibration/StereoCameraGeometricCalibration.h>
#include <EgomotionTracking/components/EVP_ArmarX/VisualSpace/Cameras/GeometricCalibration/StereoCameraGeometricCalibrationIVT.h>

class CVirtualArmar3RobotControl: public CArmar3ControlInterface, public CLocationEstimator
{
public:
    CVirtualArmar3RobotControl(unsigned int imageWidth = 640, unsigned int imageHeight = 480);
    ~CVirtualArmar3RobotControl() override;
    bool DispatchRobot() override;
    bool DispatchLocalisation() override;
    virtual bool DispatchTimeUpdate(CRobotControlState newRobotControlState, float deltaT);

    virtual bool CalculateSelfCameraPosition(SbVec3f& cameraTranslation, SbRotation& cameraRotation) const;
    virtual bool CalculateSelfCameraPosition(SbVec3f& cameraTranslation, SbRotation& cameraRotation, CCameraID cameraId) const;


    void AddNodeToViewSceneGraph(SoNode* node);
    void ClearViewSceneGraphNodes();

    void SetImageSize(unsigned int imageWidth, unsigned int imageHeight);
    void SetCameraRendering(unsigned int shrinkedRenderingFactor = 1);
    void SetStereoCalibrationGL(const CStereoCalibrationGL& stereoCalibrationGL) override;

    void SetLocation(const CLocationState& location);

    bool SetNeckRPY(float roll, float pitch, float yaw);

    bool SetStereoCameraRenderingMode(EVP::Visualization::Geometry::_3D::OpenInventor::Extensions::CExtendedPerspectiveCamera::RenderingMode Mode);

    const CLocationStatePredictorInterface* GetLocationStatePredictor() const
    {
        return m_pLocationStatePredictor;
    }

    void SetLocationStatePredictor(CLocationStatePredictorInterface* locationStatePredictor)
    {
        m_pLocationStatePredictor = locationStatePredictor;
    }

    bool LockRobotMovement() override;
    void ReleaseRobotMovementLock() override;

    bool SetTargetRobotControlState(const CRobotControlState& robotControlState) override;
    bool SetTargetRobotHeadConfiguration(const CHeadConfiguration& targetHeadConfiguration) override;

    CHeadConfiguration GetHeadConfiguration() const override;
protected:
    bool LoadStereoCameraCalibration(const EVP::VisualSpace::Cameras::GeometricCalibration::CStereoCameraGeometricCalibration* pStereoCameraGeometricCalibration, float Near, float Far);

    void InitializeSceneGraph();

    bool RenderRobotViews();
    void RecalculateSbViewportRegion();


    CLocationStatePredictorInterface* m_pLocationStatePredictor;

    SoSeparator* m_pBaseSeparatorLeftCamera;
    SoSeparator* m_pBaseSeparatorRightCamera;
    SoSeparator* m_pBaseViewSceneGraphSeparator;
    SoDirectionalLight* m_pDirectionalLight;
    SoDirectionalLight* m_pHeadLight;

    SoRotation* m_pDirectionalLightRotationLeft;
    SoRotation* m_pDirectionalLightRotationRight;
    SoTransformSeparator* m_pDirectionalLightTransformSeparatorLeft;
    SoTransformSeparator* m_pDirectionalLightTransformSeparatorRight;

    EVP::Visualization::Geometry::_3D::OpenInventor::Extensions::CExtendedPerspectiveCamera* m_pPerspectiveLeftCamera;
    EVP::Visualization::Geometry::_3D::OpenInventor::Extensions::CExtendedPerspectiveCamera* m_pPerspectiveRightCamera;

    SoOffscreenRenderer m_offscreenRenderer;

    unsigned  int m_shrinkedRenderingFactor;
    unsigned int m_imageWidth;
    unsigned int m_imageHeight;

    CParkMillerRandomGenerator m_randomGenerator;
    CLocationState m_location;

    bool m_lockRobotMovement;

    CRobotControlState m_targetRobotControlState;
    CProfiling m_dispatchingProfiler;

    bool m_externTimeUpdateWasTriggered;
};


