/*
 * CRobotControlInterface.h
 *
 *  Created on: 11.03.2013
 *      Author: abyte
 */

#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/RobotControlState.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/HeadConfiguration.h>

#include <Inventor/SbVec3f.h>
#include <Inventor/SbRotation.h>
#include <Inventor/SbMatrix.h>

#include <Image/ByteImage.h>

class CRobotKinematicInterface;

class CRobotControlInterface
{
public:
    CRobotControlInterface();
    virtual ~CRobotControlInterface();

    enum CCameraID
    {
        eLeftCamera = 0,
        eRightCamera,
        eCAMERAIDEND
    };

    virtual bool DispatchRobot() = 0;

    virtual bool CalculateCameraPosition(const CLocationState& location, SbVec3f& cameraTranslation, SbRotation& cameraRotation) const;
    virtual bool CalculateCameraPosition(const CLocationState& location, SbVec3f& cameraTranslation, SbRotation& cameraRotation, CCameraID cameraId) const;

    virtual bool CalculateCameraPosition(const CLocationState& location, SbVec3f& cameraTranslation, SbRotation& cameraRotation, const CHeadConfiguration& headConfiguration) const;
    virtual bool CalculateCameraPosition(const CLocationState& location, SbVec3f& cameraTranslation, SbRotation& cameraRotation, CCameraID cameraId, const CHeadConfiguration& headConfiguration) const;

    const CRobotControlState& GetRobotControlState() const;

    const CByteImage& GetCameraImage() const;
    const CByteImage& GetCameraImage(CCameraID id) const;
    virtual const SbMatrix& GetCameraProjectionMatrixGL() const;
    virtual const SbMatrix& GetCameraProjectionMatrixGL(CCameraID id) const = 0;

    void SetCamera(CCameraID id);
    CCameraID GetCameraId() const;

    /* for automatic (re)initialisation */
    virtual bool LockRobotMovement() = 0;
    virtual void ReleaseRobotMovementLock() = 0;

    virtual SbMatrix GetPlatformToCameraTransformation(CCameraID cameraId) const;
    virtual bool CalculateTransformationPlatformToCamera(const CHeadConfiguration& headConfiguration, CCameraID cameraId, SbMatrix& resultTransformationPlatformToCamera) const;
    virtual CHeadConfiguration GetHeadConfiguration() const = 0;

    void SetRobotKinematic(const CRobotKinematicInterface* pRobotKinematic);
protected:
    CRobotControlState m_robotControlState;
    const CRobotKinematicInterface* m_pRobotKinematic;

    CByteImage m_cameraImages[eCAMERAIDEND];
    CCameraID m_cameraId;
};

