/*
 * VirtualArmar3RobotControl.cpp
 *
 *  Created on: 11.03.2013
 *      Author: abyte
 */

#include "VirtualArmar3RobotControl.h"

#include "../Helpers/DebuggingPrimitives.h"
#include <Image/ByteImage.h>
#include <Image/ImageProcessor.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/OpenGLCalculations.h>

CVirtualArmar3RobotControl::CVirtualArmar3RobotControl(unsigned int imageWidth, unsigned int imageHeight)
    : m_pLocationStatePredictor(NULL), m_offscreenRenderer(SbViewportRegion(imageWidth, imageHeight)),
      m_shrinkedRenderingFactor(1), m_lockRobotMovement(false)
{
    InitializeSceneGraph();

    SetImageSize(imageWidth, imageHeight);
    SetCameraRendering(1);

    m_offscreenRenderer.setBackgroundColor(SbColor(0.2, 0.2, 0.2));
    m_randomGenerator.Randomize();
    m_dispatchingProfiler.StartProfiling();
    m_externTimeUpdateWasTriggered = false;
}

CVirtualArmar3RobotControl::~CVirtualArmar3RobotControl()
{
    m_pBaseSeparatorLeftCamera->unref();
    m_pBaseSeparatorRightCamera->unref();
}

bool CVirtualArmar3RobotControl::DispatchRobot()
{
    RenderRobotViews();

    const float deltaT = m_dispatchingProfiler.GetElapsedMS();
    m_dispatchingProfiler.StartProfiling();

    if (!m_externTimeUpdateWasTriggered)
    {
        DispatchTimeUpdate(m_targetRobotControlState, deltaT);
    }
    m_externTimeUpdateWasTriggered = false;

    return true;
}

bool CVirtualArmar3RobotControl::DispatchLocalisation()
{
    m_estimationIsValid = true;
    return true;
}

bool CVirtualArmar3RobotControl::DispatchTimeUpdate(CRobotControlState newRobotControlState, float deltaT)
{
    m_externTimeUpdateWasTriggered = true;

    if (!m_lockRobotMovement)
    {
        m_robotControlState = newRobotControlState;
        m_robotControlState.SetDeltaT(deltaT);

        if (m_pLocationStatePredictor != NULL)
        {
            m_location = m_pLocationStatePredictor->PredictNewRobotLocation(m_location, m_robotControlState, &m_randomGenerator);
            m_locationEstimate = m_location;
        }
    }
    return true;
}

bool CVirtualArmar3RobotControl::CalculateSelfCameraPosition(SbVec3f& cameraTranslation, SbRotation& cameraRotation) const
{
    return CalculateCameraPosition(m_location, cameraTranslation, cameraRotation);
}

bool CVirtualArmar3RobotControl::CalculateSelfCameraPosition(SbVec3f& cameraTranslation, SbRotation& cameraRotation, CRobotControlInterface::CCameraID cameraId) const
{
    return CalculateCameraPosition(m_location, cameraTranslation, cameraRotation, cameraId);
}

void CVirtualArmar3RobotControl::AddNodeToViewSceneGraph(SoNode* node)
{
    m_pBaseViewSceneGraphSeparator->addChild(node);
}

void CVirtualArmar3RobotControl::ClearViewSceneGraphNodes()
{
    m_pBaseViewSceneGraphSeparator->removeAllChildren();
}

void CVirtualArmar3RobotControl::InitializeSceneGraph()
{
    m_pBaseSeparatorLeftCamera = new SoSeparator;
    m_pBaseSeparatorRightCamera = new SoSeparator;
    m_pBaseSeparatorLeftCamera->ref();
    m_pBaseSeparatorRightCamera->ref();

    m_pHeadLight = new SoDirectionalLight;
    m_pHeadLight->direction.setValue(0, 0, -1.0);
    m_pHeadLight->intensity.setValue(0.6);


    m_pDirectionalLight = new SoDirectionalLight;
    m_pDirectionalLight->direction.setValue(0, 0, -1.0);
    m_pDirectionalLight->intensity.setValue(0.6);

    m_pDirectionalLightTransformSeparatorLeft = new SoTransformSeparator;
    m_pDirectionalLightTransformSeparatorRight = new SoTransformSeparator;

    m_pDirectionalLightRotationLeft = new SoRotation;
    m_pDirectionalLightRotationRight = new SoRotation;

    m_pDirectionalLightTransformSeparatorLeft->addChild(m_pDirectionalLightRotationLeft);
    m_pDirectionalLightTransformSeparatorLeft->addChild(m_pDirectionalLight);
    m_pDirectionalLightTransformSeparatorRight->addChild(m_pDirectionalLightRotationRight);
    m_pDirectionalLightTransformSeparatorRight->addChild(m_pDirectionalLight);

    m_pBaseSeparatorLeftCamera->addChild(m_pDirectionalLightTransformSeparatorLeft);
    m_pBaseSeparatorRightCamera->addChild(m_pDirectionalLightTransformSeparatorRight);


    m_pPerspectiveLeftCamera = new EVP::Visualization::Geometry::_3D::OpenInventor::Extensions::CExtendedPerspectiveCamera;
    m_pPerspectiveRightCamera = new EVP::Visualization::Geometry::_3D::OpenInventor::Extensions::CExtendedPerspectiveCamera;


    m_pDirectionalLightRotationLeft->rotation.connectFrom(&m_pPerspectiveLeftCamera->orientation);
    m_pDirectionalLightRotationLeft->rotation.connectFrom(&m_pPerspectiveRightCamera->orientation);

    m_pBaseSeparatorLeftCamera->addChild(m_pPerspectiveLeftCamera);
    m_pBaseSeparatorRightCamera->addChild(m_pPerspectiveRightCamera);

    m_pBaseViewSceneGraphSeparator = new SoSeparator;
    m_pBaseViewSceneGraphSeparator->addChild(m_pHeadLight);

    m_pBaseSeparatorLeftCamera->addChild(m_pBaseViewSceneGraphSeparator);
    m_pBaseSeparatorRightCamera->addChild(m_pBaseViewSceneGraphSeparator);
}

void CVirtualArmar3RobotControl::SetLocation(const CLocationState& location)
{
    m_location = location;
    m_locationEstimate = location;
}

bool CVirtualArmar3RobotControl::LoadStereoCameraCalibration(
    const EVP::VisualSpace::Cameras::GeometricCalibration::CStereoCameraGeometricCalibration* pStereoCameraGeometricCalibration, float Near, float Far)
{

    return m_pPerspectiveLeftCamera->LoadCalibration(pStereoCameraGeometricCalibration->GetLeftCameraGeometricCalibration(), Near, Far) &&
           m_pPerspectiveRightCamera->LoadCalibration(pStereoCameraGeometricCalibration->GetRightCameraGeometricCalibration(), Near, Far);
}

bool CVirtualArmar3RobotControl::SetStereoCameraRenderingMode(EVP::Visualization::Geometry::_3D::OpenInventor::Extensions::CExtendedPerspectiveCamera::RenderingMode Mode)
{
    return m_pPerspectiveLeftCamera->SetRenderingMode(Mode) &&
           m_pPerspectiveRightCamera->SetRenderingMode(Mode);

}

bool CVirtualArmar3RobotControl::LockRobotMovement()
{
    if (m_robotControlState.IsMoving())
    {
        return false;
    }
    m_lockRobotMovement = true;
    return true;
}

void CVirtualArmar3RobotControl::ReleaseRobotMovementLock()
{
    m_lockRobotMovement = false;
}

bool CVirtualArmar3RobotControl::SetTargetRobotControlState(const CRobotControlState& robotControlState)
{
    m_targetRobotControlState = robotControlState;
    return true;
}

bool CVirtualArmar3RobotControl::SetTargetRobotHeadConfiguration(const CHeadConfiguration& targetHeadConfiguration)
{
    return SetNeckRPY(targetHeadConfiguration.GetNeckRoll(), targetHeadConfiguration.GetNeckPitch(), targetHeadConfiguration.GetNeckYaw());
}

CHeadConfiguration CVirtualArmar3RobotControl::GetHeadConfiguration() const
{
    return m_currentHeadConfiguration;
}

void CVirtualArmar3RobotControl::SetImageSize(unsigned int imageWidth, unsigned int imageHeight)
{
    m_imageHeight = imageHeight;
    m_imageWidth = imageWidth;

    for (int i = 0; i < eCAMERAIDEND; ++i)
    {
        m_cameraImages[i].Set(imageWidth, imageHeight, CByteImage::eRGB24, false);
    }

    RecalculateSbViewportRegion();
}

void CVirtualArmar3RobotControl::SetCameraRendering(unsigned int shrinkedRenderingFactor)
{
    m_shrinkedRenderingFactor = shrinkedRenderingFactor;
    if (m_shrinkedRenderingFactor == 0)
    {
        m_shrinkedRenderingFactor = 1;
    }
    RecalculateSbViewportRegion();
}

void CVirtualArmar3RobotControl::SetStereoCalibrationGL(const CStereoCalibrationGL& stereoCalibrationGL)
{
    CArmar3ControlInterface::SetStereoCalibrationGL(stereoCalibrationGL);
    CStereoCalibration tmpStereoCalibrationIVT = m_stereoCalibrationGL.GetStereoCalibration();
    EVP::VisualSpace::Cameras::GeometricCalibration::EVP_ArmarX::CStereoCameraGeometricCalibrationIVT tmpStereoCalibration(&tmpStereoCalibrationIVT);
    LoadStereoCameraCalibration(&tmpStereoCalibration, configuration_near, configuration_far);
}

bool CVirtualArmar3RobotControl::RenderRobotViews()
{

    SbVec3f cameraTranslation;
    SbRotation cameraRotation;

    for (int curCameraId = 0; curCameraId < eCAMERAIDEND; ++curCameraId)
    {
        CByteImage* dstImage = &m_cameraImages[curCameraId];
        if (!CalculateSelfCameraPosition(cameraTranslation, cameraRotation, static_cast<CCameraID>(curCameraId)))
        {
            return false;
        }

        m_pPerspectiveLeftCamera->position.setValue(cameraTranslation);
        m_pPerspectiveLeftCamera->orientation.setValue(OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(cameraRotation));

        SbBool ok = m_offscreenRenderer.render(m_pBaseSeparatorLeftCamera);
        if (ok)
        {
            if (m_shrinkedRenderingFactor > 1)
            {
                CByteImage tmp(m_imageWidth / m_shrinkedRenderingFactor, m_imageHeight / m_shrinkedRenderingFactor, CByteImage::eRGB24, true);
                CByteImage tmp2(m_imageWidth / m_shrinkedRenderingFactor, m_imageHeight / m_shrinkedRenderingFactor, CByteImage::eRGB24, false);

                tmp.pixels = m_offscreenRenderer.getBuffer();
                ImageProcessor::FlipY(&tmp, &tmp2);

                ImageProcessor::Resize(&tmp2, dstImage);
            }
            else
            {
                CByteImage tmp(m_imageWidth, m_imageHeight, CByteImage::eRGB24, true);
                tmp.pixels = m_offscreenRenderer.getBuffer();
                ImageProcessor::FlipY(&tmp, dstImage);
            }
        }
        else
        {
            return false;
        }
    }

    return true;
}

bool CVirtualArmar3RobotControl::SetNeckRPY(float roll, float pitch, float yaw)
{
    return SetInternNeckRPY(roll, pitch, yaw);
}

void CVirtualArmar3RobotControl::RecalculateSbViewportRegion()
{

    SbViewportRegion region(m_imageWidth / m_shrinkedRenderingFactor, m_imageHeight / m_shrinkedRenderingFactor);
    m_offscreenRenderer.setViewportRegion(region);
}
