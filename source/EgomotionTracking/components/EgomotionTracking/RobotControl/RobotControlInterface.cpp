/*
 * CRobotControlInterface.cpp
 *
 *  Created on: 11.03.2013
 *      Author: abyte
 */

#include "RobotControlInterface.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/RobotKinematic/RobotKinematicInterface.h>

#include <stdlib.h>

CRobotControlInterface::CRobotControlInterface() : m_pRobotKinematic(NULL), m_cameraId(eLeftCamera)
{

}

CRobotControlInterface::~CRobotControlInterface()
{
}

bool CRobotControlInterface::CalculateCameraPosition(const CLocationState& location, SbVec3f& cameraTranslation, SbRotation& cameraRotation) const
{
    return CalculateCameraPosition(location, cameraTranslation, cameraRotation, m_cameraId);
}

bool CRobotControlInterface::CalculateCameraPosition(const CLocationState& location, SbVec3f& cameraTranslation, SbRotation& cameraRotation, CRobotControlInterface::CCameraID cameraId) const
{
    return Calculations::CalculateRobotCameraPosition(location, GetPlatformToCameraTransformation(cameraId), cameraTranslation, cameraRotation);
}

bool CRobotControlInterface::CalculateCameraPosition(const CLocationState& location, SbVec3f& cameraTranslation, SbRotation& cameraRotation, const CHeadConfiguration& headConfiguration) const
{
    SbMatrix platformToCameraTransformation;
    CalculateTransformationPlatformToCamera(headConfiguration, GetCameraId(), platformToCameraTransformation);
    return Calculations::CalculateRobotCameraPosition(location, platformToCameraTransformation, cameraTranslation, cameraRotation);
}

bool CRobotControlInterface::CalculateCameraPosition(const CLocationState& location, SbVec3f& cameraTranslation, SbRotation& cameraRotation, CRobotControlInterface::CCameraID cameraId, const CHeadConfiguration& headConfiguration) const
{
    SbMatrix platformToCameraTransformation;
    CalculateTransformationPlatformToCamera(headConfiguration, cameraId, platformToCameraTransformation);
    return Calculations::CalculateRobotCameraPosition(location, platformToCameraTransformation, cameraTranslation, cameraRotation);
}

const CRobotControlState& CRobotControlInterface::GetRobotControlState() const
{
    return m_robotControlState;
}

const CByteImage& CRobotControlInterface::GetCameraImage() const
{
    return m_cameraImages[m_cameraId];
}

const CByteImage& CRobotControlInterface::GetCameraImage(CRobotControlInterface::CCameraID id) const
{
    if (id >= 0 && id < eCAMERAIDEND)
    {
        return m_cameraImages[id];
    }
    return m_cameraImages[0];
}

const SbMatrix& CRobotControlInterface::GetCameraProjectionMatrixGL() const
{
    return GetCameraProjectionMatrixGL(m_cameraId);
}

void CRobotControlInterface::SetCamera(CRobotControlInterface::CCameraID id)
{
    if (id >= 0 && id < eCAMERAIDEND)
    {
        m_cameraId = id;
    }
}

CRobotControlInterface::CCameraID CRobotControlInterface::GetCameraId() const
{
    return m_cameraId;
}

SbMatrix CRobotControlInterface::GetPlatformToCameraTransformation(CRobotControlInterface::CCameraID cameraId) const
{
    if (m_pRobotKinematic == NULL)
    {
        COUT_ERROR << "m_pRobotKinematic == NULL" << std::endl;
        exit(0);
    }
    return m_pRobotKinematic->CalculateTransformationPlatformToCamera(GetHeadConfiguration(), cameraId);
}

bool CRobotControlInterface::CalculateTransformationPlatformToCamera(const CHeadConfiguration& headConfiguration, CRobotControlInterface::CCameraID cameraId, SbMatrix& resultTransformationPlatformToCamera) const
{
    if (m_pRobotKinematic == NULL)
    {
        COUT_ERROR << "m_pRobotKinematic == NULL" << std::endl;
        exit(0);
    }
    resultTransformationPlatformToCamera = m_pRobotKinematic->CalculateTransformationPlatformToCamera(headConfiguration, cameraId);
    return true;
}

void CRobotControlInterface::SetRobotKinematic(const CRobotKinematicInterface* pRobotKinematic)
{
    m_pRobotKinematic = pRobotKinematic;
}
