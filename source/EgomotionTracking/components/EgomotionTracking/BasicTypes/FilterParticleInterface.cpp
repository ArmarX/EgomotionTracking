#include "FilterParticleInterface.h"

FilterParticleInterface::FilterParticleInterface() : m_importanceWeight(1.0), m_active(false)
{

}

void FilterParticleInterface::SetImportanceWeight(float importanceWeight)
{
    m_importanceWeight = importanceWeight;
}

float FilterParticleInterface::GetImportanceWeight() const
{
    return m_importanceWeight;
}

void FilterParticleInterface::SetActive(bool active)
{
    m_active = active;
}

bool FilterParticleInterface::GetActive() const
{
    return m_active;
}

const std::vector<CScreenLine>& FilterParticleInterface::GetScreenLines() const
{
    return m_screenLines;
}

std::vector<CScreenLine>& FilterParticleInterface::GetScreenLinesWriteable()
{
    return m_screenLines;
}

void FilterParticleInterface::SetScreenLines(const std::vector<CScreenLine>& screenLines)
{
    m_screenLines = screenLines;
}

const std::vector<CJunctionPoint>& FilterParticleInterface::GetScreenJunctionPoints() const
{
    return m_screenJunctionPoints;
}

std::vector<CJunctionPoint>& FilterParticleInterface::GetScreenJunctionPointsWriteable()
{
    return m_screenJunctionPoints;
}

void FilterParticleInterface::SetScreenJunctionPoints(const std::vector<CJunctionPoint>& screenJunctionPoints)
{
    m_screenJunctionPoints = screenJunctionPoints;
}
