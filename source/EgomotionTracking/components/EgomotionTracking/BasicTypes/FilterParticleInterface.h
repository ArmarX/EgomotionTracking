#pragma once

#include <vector>
#include <EgomotionTracking/components/EgomotionTrackingConfiguration/TrackingConfiguration.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/Line.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/JunctionPoint.h>

class FilterParticleInterface
{
public:

    FilterParticleInterface();

    void SetImportanceWeight(float importanceWeight);
    float GetImportanceWeight() const;

    void SetActive(bool active);
    bool GetActive() const;

    inline virtual void Set(PF_REAL* pParticleVector, PF_REAL* pImportanceWeight) = 0;
    inline virtual void Get(PF_REAL* pParticleVector, PF_REAL* pImportanceWeight) const = 0;

    const std::vector<CScreenLine>& GetScreenLines() const;
    std::vector<CScreenLine>& GetScreenLinesWriteable();
    void SetScreenLines(const std::vector<CScreenLine>& screenLines);

    const std::vector<CJunctionPoint>& GetScreenJunctionPoints() const;
    std::vector<CJunctionPoint>& GetScreenJunctionPointsWriteable();
    void SetScreenJunctionPoints(const std::vector<CJunctionPoint>& screenJunctionPoints);

protected:

    PF_REAL m_importanceWeight;
    bool m_active;

    std::vector<CScreenLine> m_screenLines;
    std::vector<CJunctionPoint> m_screenJunctionPoints;
};

