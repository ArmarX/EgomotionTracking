#pragma once

#include <map>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/Line.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/FaceTriangle.h>
#include <EgomotionTracking/components/EgomotionTrackingConfiguration/TrackingConfiguration.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/DynamicElementParticle.h>

#include <EgomotionTracking/components/RVL/Containers/_3D/ContinuousBoundingBox3D.h>

namespace RVL
{
    namespace Representation
    {
        namespace ModelBased
        {
            namespace GeometricGraph
            {
                namespace Base
                {
                    class CModelMesh;
                    class CModelVertex;
                    class CModelTransformation;
                }
            }
        }
    }
}

class DynamicElement
{
public:

    enum DynamicElementStatus
    {
        eIdle,
        eActive,
        eInactive
    };

    enum DynamicElementVisibility
    {
        eVisible,
        eNotVisible
    };

    struct FrontalFaceVertices
    {
        RVL::Representation::ModelBased::GeometricGraph::Base::CModelVertex* vertexA;
        RVL::Representation::ModelBased::GeometricGraph::Base::CModelVertex* vertexB;
        RVL::Representation::ModelBased::GeometricGraph::Base::CModelVertex* vertexC;
        RVL::Representation::ModelBased::GeometricGraph::Base::CModelVertex* vertexD;
    };

    DynamicElement(RVL::Representation::ModelBased::GeometricGraph::Base::CModelTransformation* pModelTransformation = NULL);

    DynamicElement(std::map<int, RVL::Representation::ModelBased::GeometricGraph::Base::CModelTransformation*> modelTransformationMap,
                   RVL::Representation::ModelBased::GeometricGraph::Base::CModelTransformation* pModelTransformation);
    explicit DynamicElement();

    void SetDynamicElementStatus(DynamicElementStatus dynamicElementStatus);
    DynamicElementStatus GetDynamicElementStatus() const;

    void SetDynamicElementVisibility(DynamicElementVisibility dynamicElementVisibility);
    DynamicElementVisibility GetDynamicElementVisibility() const;

    void SetCurrentStateEstimate(PF_REAL stateEstimate, PF_REAL importanceWeight);
    PF_REAL GetCurrentStateEstimate() const;
    PF_REAL GetCurrentStateEstimateImportanceWeight() const;

    RVL::Representation::ModelBased::GeometricGraph::Base::CModelTransformation* getModelTransformation();
    RVL::Representation::ModelBased::GeometricGraph::Base::CModelTransformation* getModelTransformation(int threadId);

    const std::vector<DynamicElementParticle>& getDynamicElementParticles() const;
    std::vector<DynamicElementParticle>& getDynamicElementParticlesWriteable();

    RVL::Containers::_3D::CContinuousBoundingBox3D getTransformationBoundingBox() const;
    std::list<CLine>& getTransformationBoundingBoxLineList();
    std::list<CFaceTriangle>& getTransformationBoundingBoxFaceTriangleList();

    void configureParticleReinitialization(double importanceWeightTreshold, int iterationCountTreshold);
    void configureNumberOfParticles(int minParticles, int maxParticles, bool useNumParticlesFunctionOfTransformationRange, int transformationRangeNumParticlesRatio, int numParticles);

    const FrontalFaceVertices& getFrontalFaceVertices() const;
    FrontalFaceVertices& getFrontalFaceVerticesWriteable();

protected:

    RVL::Representation::ModelBased::GeometricGraph::Base::CModelTransformation* m_pModelTransformation;

    DynamicElementStatus m_dynamicElementStatus;
    DynamicElementVisibility m_dynamicElementVisibility;

    PF_REAL m_currentStateEstimate;
    PF_REAL m_currentStateEstimateImportanceWeight;

    std::vector<DynamicElementParticle> m_dynamicElementParticles;
    RVL::Containers::_3D::CContinuousBoundingBox3D m_transformationBoundingBox;
    std::list<CLine> m_transformationBoundingBoxLineList;
    std::list<CFaceTriangle> m_transformationBoundingBoxFaceTriangleList;

    void CalculateTransformationBoundingBox();
    void CalculateFrontalFaceVertices();
    void InitializeDynamicElementParticles();

    double m_particleReinitializationImportanceWeightTreshold;
    int m_particleReinitializationCounterTreshold;

    int m_particleReinitializationCounter;

    /* Configuration - Number of DynamicElementParticles. */
    int m_minParticles;
    int m_maxParticles;
    bool m_useNumParticlesFunctionOfTransformationRange;
    int m_transformationRangeNumParticlesRatio;
    int m_numParticles;

    FrontalFaceVertices m_frontalFaceVertices;

    std::map<int, RVL::Representation::ModelBased::GeometricGraph::Base::CModelTransformation*> m_modelTransformationMap;
};

