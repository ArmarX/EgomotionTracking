#pragma once

#include "LocationParticle.h"
#include <EgomotionTracking/components/EgomotionTracking/Models/NeckErrorUncertaintyModel/NeckErrorUncertaintyModel.h>

class CLocationSwarmParticle : public CLocationParticle
{
public:
    CLocationSwarmParticle();

    void SetVelocityX(PF_REAL velocity);
    void SetVelocityY(PF_REAL velocity);
    void SetVelocityAlpha(PF_REAL velocity);
    void SetVelocityNeckRoll(PF_REAL velocity);
    void SetVelocityNeckPitch(PF_REAL velocity);
    void SetVelocityNeckYaw(PF_REAL velocity);
    PF_REAL GetVelocityX() const;
    PF_REAL GetVelocityY() const;
    PF_REAL GetVelocityAlpha() const;
    PF_REAL GetVelocityNeckRoll() const;
    PF_REAL GetVelocityNeckPitch() const;
    PF_REAL GetVelocityNeckYaw() const;

    void SetLocalBest(const CLocationParticle& localBest);
    const CLocationParticle& GetLocalBest() const;

    bool UpdateGlobalBest();
    bool UpdateLocalBest();
    void ResetLocalBest();

    void SetGlobalBest(CLocationParticle* pGlobalBest);

    void UpdateState(PF_REAL inertiaFactor, PF_REAL localBestFactor, PF_REAL globalBestFactor, const CNeckErrorUncertaintyModel* pNeckErrorUncertaintyModel);

protected:
    PF_REAL m_velocityX;
    PF_REAL m_velocityY;
    PF_REAL m_velocityAlpha;
    PF_REAL m_velocityNeckRoll;
    PF_REAL m_velocityNeckPitch;
    PF_REAL m_velocityNeckYaw;

    CLocationParticle m_localBest;
    CLocationParticle* m_pGlobalBest;
};

