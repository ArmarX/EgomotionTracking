/*
 * CLocationRegion.h
 *
 *  Created on: 11.03.2013
 *      Author: abyte
 */

#pragma once

#include "LocationState.h"

class CLocationState;
class CLocationRegion
{
public:
    CLocationRegion();
    virtual ~CLocationRegion();

    virtual bool LocationIsInRegion(const CLocationState& location) const = 0;
};

