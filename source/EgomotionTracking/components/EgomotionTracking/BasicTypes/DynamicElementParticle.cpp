#include "DynamicElementParticle.h"

DynamicElementParticle::DynamicElementParticle()
    : FilterParticleInterface(), m_alpha(0.0)
{

}

void DynamicElementParticle::setAlpha(float alpha)
{
    m_alpha = alpha;
}

float DynamicElementParticle::getAlpha() const
{
    return m_alpha;
}

bool DynamicElementParticle::operator<(const DynamicElementParticle& second) const
{
    return m_importanceWeight < second.GetImportanceWeight();
}

const DynamicElementParticle::FrontalFacePoints2D& DynamicElementParticle::getFrontalFacePoints2D() const
{
    return m_frontalFacePoints2D;
}

DynamicElementParticle::FrontalFacePoints2D& DynamicElementParticle::getFrontalFacePoints2DWriteable()
{
    return m_frontalFacePoints2D;
}
