#include "HeadConfigurationSample.h"

CHeadConfigurationSample::CHeadConfigurationSample() :
    CHeadConfiguration(), m_relevanceFactor(0.0)
{
}

CHeadConfigurationSample::CHeadConfigurationSample(const CHeadConfiguration& headConfiguration) :
    CHeadConfiguration(headConfiguration), m_relevanceFactor(0.0)
{
}

void CHeadConfigurationSample::SetRelevanceFactor(float relevanceFactor)
{
    m_relevanceFactor = relevanceFactor;
}


float CHeadConfigurationSample::GetRelevanceFactor() const
{
    return m_relevanceFactor;
}

