/*
 * CSimpleLocationRegion.h
 *
 *  Created on: 11.03.2013
 *      Author: abyte
 */

#pragma once

#include "LocationRegion.h"

class CSimpleLocationRegion : public CLocationRegion
{
public:
    CSimpleLocationRegion();
    CSimpleLocationRegion(float minX, float maxX, float minY, float maxY, float minAlpha, float maxAlpha);
    ~CSimpleLocationRegion() override;

    bool LocationIsInRegion(const CLocationState& location) const override;


    void Set(float minX, float maxX, float minY, float maxY, float minAlpha, float maxAlpha)
    {
        m_minX = minX;
        m_maxX = maxX;
        m_minY = minY;
        m_maxY = maxY;
        m_minAlpha = minAlpha;
        m_maxAlpha = maxAlpha;
    }

    float GetMaxAlpha() const
    {
        return m_maxAlpha;
    }

    void SetMaxAlpha(float maxAlpha)
    {
        m_maxAlpha = maxAlpha;
    }

    float GetMaxX() const
    {
        return m_maxX;
    }

    void SetMaxX(float maxX)
    {
        m_maxX = maxX;
    }

    float GetMaxY() const
    {
        return m_maxY;
    }

    void SetMaxY(float maxY)
    {
        m_maxY = maxY;
    }

    float GetMinAlpha() const
    {
        return m_minAlpha;
    }

    void SetMinAlpha(float minAlpha)
    {
        m_minAlpha = minAlpha;
    }

    float GetMinX() const
    {
        return m_minX;
    }

    void SetMinX(float minX)
    {
        m_minX = minX;
    }

    float GetMinY() const
    {
        return m_minY;
    }

    void SetMinY(float minY)
    {
        m_minY = minY;
    }

protected:
    float m_minX;
    float m_maxX;
    float m_minY;
    float m_maxY;
    float m_minAlpha;
    float m_maxAlpha;
};

