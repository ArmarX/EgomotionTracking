#pragma once

#include "HeadConfiguration.h"

#include <Inventor/SbMatrix.h>

class CHeadConfigurationSample : public CHeadConfiguration
{
public:
    CHeadConfigurationSample();
    CHeadConfigurationSample(const CHeadConfiguration& headConfiguration);

    void SetRelevanceFactor(float relevanceFactor);
    float GetRelevanceFactor() const;

protected:
    float m_relevanceFactor;
};

