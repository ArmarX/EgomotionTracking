/*
 * LocationParticle.cpp
 *
 *  Created on: 22.03.2013
 *      Author: abyte
 */

#include "LocationParticle.h"

CLocationParticle::CLocationParticle() :
    CLocationState(), FilterParticleInterface(),
    m_junctionPointsRendered(false), m_linesRendered(false),
    m_neckRollError(0.0), m_neckPitchError(0.0), m_neckYawError(0.0)
{
}

CLocationParticle::CLocationParticle(const CLocationState& locationState)
    : CLocationState(locationState), FilterParticleInterface(), m_junctionPointsRendered(false), m_linesRendered(false),
      m_neckRollError(0.0), m_neckPitchError(0.0), m_neckYawError(0.0)
{
}

CLocationParticle::~CLocationParticle()
{
}

bool CLocationParticle::operator <(const CLocationParticle& second) const
{
    return m_importanceWeight < second.GetImportanceWeight();
}

float CLocationParticle::GetNeckRollError() const
{
    return m_neckRollError;
}

float CLocationParticle::GetNeckPitchError() const
{
    return m_neckPitchError;
}

float CLocationParticle::GetNeckYawError() const
{
    return m_neckYawError;
}

void CLocationParticle::SetNeckRollError(float neckRollError)
{
    m_neckRollError = neckRollError;
}

void CLocationParticle::SetNeckPitchError(float neckPitchError)
{
    m_neckPitchError = neckPitchError;
}

void CLocationParticle::SetNeckYawError(float neckYawError)
{
    m_neckYawError = neckYawError;
}
