/*
 * CRobotControlState.h
 *
 *  Created on: 11.03.2013
 *      Author: abyte
 */

#pragma once

#include <float.h>
class CRobotControlState
{
public:
    CRobotControlState(float maxAbsVelocityX = FLT_MAX, float maxAbsVelocityY = FLT_MAX, float maxAbsVelocityAlpha = FLT_MAX);
    virtual ~CRobotControlState();
    float GetMaxAbsVelocityAlpha() const;
    void SetMaxAbsVelocityAlpha(float maxAbsVelocityAlpha);
    float GetMaxAbsVelocityX() const;
    void SetMaxAbsVelocityX(float maxAbsVelocityX);
    float GetMaxAbsVelocityY() const;
    void SetMaxAbsVelocityY(float maxAbsVelocityY);
    float GetPlatformVelocityAlpha() const;
    bool SetPlatformVelocityAlpha(float platformVelocityAlpha);
    float GetPlatformVelocityX() const;
    bool SetPlatformVelocityX(float platformVelocityX);
    float GetPlatformVelocityY() const;
    bool SetPlatformVelocityY(float platformVelocityY);
    float CalculateAbsolutePlatformVelocity() const;

    bool SetPlatformVelocitiesKeepXYRatio(float platformVelocityX, float platformVelocityY, float platformVelocityAlpha);

    float GetPlatformAxesTiltError() const;
    void SetPlatformAxesTiltError(float platformAxesTiltError);

    float GetDeltaT() const;
    void SetDeltaT(float deltaT);

    bool IsMoving() const;
private:
    /* m_platformVelocityX > 0 => platform moves right */
    float m_platformVelocityX;
    /* m_platformVelocityY > 0 => platform moves forward */
    float m_platformVelocityY;
    float m_platformVelocityAlpha;

    float m_maxAbsVelocityX;
    float m_maxAbsVelocityY;
    float m_maxAbsVelocityAlpha;

    float m_platformAxesTiltError;

    float m_deltaT;

};

