#include "HeadConfiguration.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>
#include <float.h>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/DebuggingPrimitives.h>

CHeadConfiguration::CHeadConfiguration() :
    m_neckRoll(0.0), m_neckPitch(0.0), m_neckYaw(0.0),
    m_neckErrorRoll(0.0), m_neckErrorPitch(0.0), m_neckErrorYaw(0.0),
    m_minNeckRoll(-FLT_MAX), m_minNeckPitch(-FLT_MAX), m_minNeckYaw(-FLT_MAX),
    m_maxNeckRoll(FLT_MAX), m_maxNeckPitch(FLT_MAX), m_maxNeckYaw(FLT_MAX)
{
}

CHeadConfiguration::CHeadConfiguration(const SbVec3f& headConfigurationVector)
    :    m_neckRoll(headConfigurationVector[0]), m_neckPitch(headConfigurationVector[1]), m_neckYaw(headConfigurationVector[2]),
         m_neckErrorRoll(0.0), m_neckErrorPitch(0.0), m_neckErrorYaw(0.0),
         m_minNeckRoll(-FLT_MAX), m_minNeckPitch(-FLT_MAX), m_minNeckYaw(-FLT_MAX),
         m_maxNeckRoll(FLT_MAX), m_maxNeckPitch(FLT_MAX), m_maxNeckYaw(FLT_MAX)
{}

void CHeadConfiguration::SetNeckRoll(float roll)
{
    m_neckRoll = Calculations::SaturizeToLimits(roll, m_minNeckRoll, m_maxNeckRoll);
}

void CHeadConfiguration::SetNeckPitch(float pitch)
{
    m_neckPitch = Calculations::SaturizeToLimits(pitch, m_minNeckPitch, m_maxNeckPitch);
}

void CHeadConfiguration::SetNeckYaw(float yaw)
{
    m_neckYaw = Calculations::SaturizeToLimits(yaw, m_minNeckYaw, m_maxNeckYaw);
}

float CHeadConfiguration::GetNeckRoll() const
{
    return m_neckRoll;
}

float CHeadConfiguration::GetNeckPitch() const
{
    return m_neckPitch;
}

float CHeadConfiguration::GetNeckYaw() const
{
    return m_neckYaw;
}

float CHeadConfiguration::GetNeckRollWithError() const
{
    return m_neckRoll + m_neckErrorRoll;
}

float CHeadConfiguration::GetNeckPitchWithError() const
{
    return m_neckPitch + m_neckErrorPitch;
}

float CHeadConfiguration::GetNeckYawWithError() const
{
    return m_neckYaw + m_neckErrorYaw;
}

void CHeadConfiguration::SetLimits(float minNeckRoll, float maxNeckRoll, float minNeckPitch, float maxNeckPitch, float minNeckYaw, float maxNeckYaw)
{
    m_minNeckRoll = minNeckRoll;
    m_maxNeckRoll = maxNeckRoll;
    m_minNeckPitch = minNeckPitch;
    m_maxNeckPitch = maxNeckPitch;
    m_minNeckYaw = minNeckYaw;
    m_maxNeckYaw = maxNeckYaw;
}

void CHeadConfiguration::GetLimits(float& minNeckRoll, float& maxNeckRoll, float& minNeckPitch, float& maxNeckPitch, float& minNeckYaw, float& maxNeckYaw) const
{
    minNeckRoll = m_minNeckRoll;
    maxNeckRoll = m_maxNeckRoll;
    minNeckPitch = m_minNeckPitch;
    maxNeckPitch = m_maxNeckPitch;
    minNeckYaw = m_minNeckYaw;
    maxNeckYaw = m_maxNeckYaw;
}

void CHeadConfiguration::SetValues(const CHeadConfiguration& headConfiguration)
{
    SetNeckRoll(headConfiguration.GetNeckRoll());
    SetNeckPitch(headConfiguration.GetNeckPitch());
    SetNeckYaw(headConfiguration.GetNeckYaw());
}

SbVec3f CHeadConfiguration::GetSbVec3f() const
{
    return SbVec3f(m_neckRoll, m_neckPitch, m_neckYaw);
}

SbMatrix CHeadConfiguration::CalculateNeckRotationTransformationMatrix() const
{
    SbMatrix transformationRoll;
    SbMatrix transformationPitch;
    SbMatrix transformationYaw;

    transformationRoll.setRotate(SbRotation(SbVec3f(0.0, 1.0, 0.0), GetNeckRollWithError()));
    transformationPitch.setRotate(SbRotation(SbVec3f(1.0, 0.0, 0.0), GetNeckPitchWithError()));
    transformationYaw.setRotate(SbRotation(SbVec3f(0.0, 0.0, 1.0), GetNeckYawWithError()));

    return transformationPitch.multLeft(transformationRoll.multLeft(transformationYaw));
}

void CHeadConfiguration::SetNeckError(float neckErrorRoll, float neckErrorPitch, float neckErrorYaw)
{
    m_neckErrorRoll = neckErrorRoll;
    m_neckErrorPitch = neckErrorPitch;
    m_neckErrorYaw = neckErrorYaw;
}
