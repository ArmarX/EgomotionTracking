/*
 * LocationParticle.h
 *
 *  Created on: 22.03.2013
 *      Author: abyte
 */

#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/FilterParticleInterface.h>
#include "LocationState.h"

class CLocationParticle : public CLocationState, public FilterParticleInterface
{
public:
    CLocationParticle();
    CLocationParticle(const CLocationState& locationState);
    ~CLocationParticle() override;

    bool isJunctionPointsRendered() const
    {
        return m_junctionPointsRendered;
    }

    void SetJunctionPointsRendered(bool junctionPointsRendered)
    {
        m_junctionPointsRendered = junctionPointsRendered;
    }

    bool isLinesRendered() const
    {
        return m_linesRendered;
    }

    void SetLinesRendered(bool linesRendered)
    {
        m_linesRendered = linesRendered;
    }

    inline void Set(PF_REAL* particleVector, PF_REAL* importanceWeight) override
    {
        m_x = particleVector[0];
        m_y = particleVector[1];
        m_alpha = particleVector[2];
        m_importanceWeight = *importanceWeight;
        m_neckRollError = particleVector[3];
        m_neckPitchError = particleVector[4];
        m_neckYawError = particleVector[5];
    }

    inline void Get(PF_REAL* particleVector, PF_REAL* importanceWeight) const override
    {
        particleVector[0] = m_x;
        particleVector[1] = m_y;
        particleVector[2] = m_alpha;
        particleVector[3] = m_neckRollError;
        particleVector[4] = m_neckPitchError;
        particleVector[5] = m_neckYawError;
        *importanceWeight = m_importanceWeight;
    }

    bool operator< (const CLocationParticle& second) const;

    float GetNeckRollError() const;
    float GetNeckPitchError() const;
    float GetNeckYawError() const;

    void SetNeckRollError(float neckRollError);
    void SetNeckPitchError(float neckPitchError);
    void SetNeckYawError(float neckYawError);

protected:

    bool m_junctionPointsRendered;
    bool m_linesRendered;

    float m_neckRollError;
    float m_neckPitchError;
    float m_neckYawError;
};

