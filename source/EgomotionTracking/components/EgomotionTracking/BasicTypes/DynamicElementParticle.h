#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/FilterParticleInterface.h>

class DynamicElement;

class DynamicElementParticle : public FilterParticleInterface
{
public:

    DynamicElementParticle();

    void setAlpha(float alpha);
    float getAlpha() const;

    inline void Set(PF_REAL* pParticleVector, PF_REAL* pImportanceWeight) override
    {
        m_alpha = pParticleVector[0];
        m_importanceWeight = *pImportanceWeight;
    }
    inline void Get(PF_REAL* pParticleVector, PF_REAL* pImportanceWeight) const override
    {
        pParticleVector[0] = m_alpha;
        *pImportanceWeight = m_importanceWeight;
    }

    bool operator<(const DynamicElementParticle& second) const;

    struct FrontalFacePoints2D
    {
        SbVec2f pointA;
        SbVec2f pointB;
        SbVec2f pointC;
        SbVec2f pointD;
    };

    const FrontalFacePoints2D& getFrontalFacePoints2D() const;
    FrontalFacePoints2D& getFrontalFacePoints2DWriteable();

protected:

    float m_alpha;
    FrontalFacePoints2D m_frontalFacePoints2D;
};

