#pragma once
#include <Inventor/SbVec3f.h>
#include <Inventor/SbMatrix.h>

class CHeadConfiguration
{
public:
    CHeadConfiguration();
    CHeadConfiguration(const SbVec3f& headConfigurationVector);

    void SetNeckRoll(float roll);
    void SetNeckPitch(float pitch);
    void SetNeckYaw(float yaw);

    float GetNeckRoll() const;
    float GetNeckPitch() const;
    float GetNeckYaw() const;

    float GetNeckRollWithError() const;
    float GetNeckPitchWithError() const;
    float GetNeckYawWithError() const;

    void SetLimits(float minNeckRoll, float maxNeckRoll,
                   float minNeckPitch, float maxNeckPitch,
                   float minNeckYaw, float maxNeckYaw);
    void GetLimits(float& minNeckRoll, float& maxNeckRoll,
                   float& minNeckPitch, float& maxNeckPitch,
                   float& minNeckYaw, float& maxNeckYaw) const;

    void SetValues(const CHeadConfiguration& headConfiguration);

    SbVec3f GetSbVec3f() const;

    SbMatrix CalculateNeckRotationTransformationMatrix() const;

    void SetNeckError(float neckErrorRoll, float neckErrorPitch, float neckErrorYaw);
protected:
    float m_neckRoll;
    float m_neckPitch;
    float m_neckYaw;

    float m_neckErrorRoll;
    float m_neckErrorPitch;
    float m_neckErrorYaw;

    float m_minNeckRoll;
    float m_minNeckPitch;
    float m_minNeckYaw;
    float m_maxNeckRoll;
    float m_maxNeckPitch;
    float m_maxNeckYaw;
};

