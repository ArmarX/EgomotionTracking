#include "LocationSwarmParticle.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>

CLocationSwarmParticle::CLocationSwarmParticle() :
    m_velocityX(0.0), m_velocityY(0.0), m_velocityAlpha(0.0),
    m_velocityNeckRoll(0.0), m_velocityNeckPitch(0.0), m_velocityNeckYaw(0.0),
    m_pGlobalBest(NULL)
{
    m_localBest.SetImportanceWeight(0.0);
}

void CLocationSwarmParticle::SetVelocityX(PF_REAL velocity)
{
    m_velocityX = velocity;
}

void CLocationSwarmParticle::SetVelocityY(PF_REAL velocity)
{
    m_velocityY = velocity;
}

void CLocationSwarmParticle::SetVelocityAlpha(PF_REAL velocity)
{
    m_velocityAlpha = velocity;
}

void CLocationSwarmParticle::SetVelocityNeckRoll(PF_REAL velocity)
{
    m_velocityNeckRoll = velocity;
}

void CLocationSwarmParticle::SetVelocityNeckPitch(PF_REAL velocity)
{
    m_velocityNeckPitch = velocity;
}

void CLocationSwarmParticle::SetVelocityNeckYaw(PF_REAL velocity)
{
    m_velocityNeckYaw = velocity;
}

PF_REAL CLocationSwarmParticle::GetVelocityX() const
{
    return m_velocityX;
}

PF_REAL CLocationSwarmParticle::GetVelocityY() const
{
    return m_velocityY;
}

PF_REAL CLocationSwarmParticle::GetVelocityAlpha() const
{
    return m_velocityAlpha;
}

PF_REAL CLocationSwarmParticle::GetVelocityNeckRoll() const
{
    return m_velocityNeckRoll;
}

PF_REAL CLocationSwarmParticle::GetVelocityNeckPitch() const
{
    return m_velocityNeckPitch;
}

PF_REAL CLocationSwarmParticle::GetVelocityNeckYaw() const
{
    return m_velocityNeckYaw;
}

void CLocationSwarmParticle::SetLocalBest(const CLocationParticle& localBest)
{
    m_localBest = localBest;
}

const CLocationParticle& CLocationSwarmParticle::GetLocalBest() const
{
    return m_localBest;
}

bool CLocationSwarmParticle::UpdateGlobalBest()
{
    if (m_pGlobalBest)
    {
        if (GetImportanceWeight() > m_pGlobalBest->GetImportanceWeight())
        {
            m_pGlobalBest->SetX(GetX());
            m_pGlobalBest->SetY(GetY());
            m_pGlobalBest->SetAlpha(GetAlpha());
            m_pGlobalBest->SetNeckRollError(GetNeckRollError());
            m_pGlobalBest->SetNeckPitchError(GetNeckPitchError());
            m_pGlobalBest->SetNeckYawError(GetNeckYawError());
            m_pGlobalBest->SetImportanceWeight(GetImportanceWeight());
            return true;
        }
    }
    return false;
}

bool CLocationSwarmParticle::UpdateLocalBest()
{
    if (GetImportanceWeight() > m_localBest.GetImportanceWeight())
    {
        m_localBest.SetX(GetX());
        m_localBest.SetY(GetY());
        m_localBest.SetAlpha(GetAlpha());
        m_localBest.SetNeckRollError(GetNeckRollError());
        m_localBest.SetNeckPitchError(GetNeckPitchError());
        m_localBest.SetNeckYawError(GetNeckYawError());
        m_localBest.SetImportanceWeight(GetImportanceWeight());
        return true;
    }
    return false;
}

void CLocationSwarmParticle::ResetLocalBest()
{
    m_localBest.SetX(GetX());
    m_localBest.SetY(GetY());
    m_localBest.SetAlpha(GetAlpha());
    m_localBest.SetNeckRollError(GetNeckRollError());
    m_localBest.SetNeckPitchError(GetNeckPitchError());
    m_localBest.SetNeckYawError(GetNeckYawError());
    m_localBest.SetImportanceWeight(0.0);
}

void CLocationSwarmParticle::SetGlobalBest(CLocationParticle* pGlobalBest)
{
    m_pGlobalBest = pGlobalBest;
}

void CLocationSwarmParticle::UpdateState(PF_REAL inertiaFactor, PF_REAL localBestFactor, PF_REAL globalBestFactor, const CNeckErrorUncertaintyModel* pNeckErrorUncertaintyModel)
{
    m_velocityX = inertiaFactor * m_velocityX + localBestFactor * (m_localBest.GetX() - GetX()) + globalBestFactor * (m_pGlobalBest->GetX() - GetX());
    m_velocityY = inertiaFactor * m_velocityY + localBestFactor * (m_localBest.GetY() - GetY()) + globalBestFactor * (m_pGlobalBest->GetY() - GetY());
    m_velocityAlpha = inertiaFactor * m_velocityAlpha + localBestFactor * (m_localBest.GetAlpha() - GetAlpha()) + globalBestFactor * (m_pGlobalBest->GetAlpha() - GetAlpha());

    m_velocityNeckRoll = inertiaFactor * m_velocityNeckRoll + localBestFactor * (m_localBest.GetNeckRollError() - GetNeckRollError()) + globalBestFactor * (m_pGlobalBest->GetNeckRollError() - GetNeckRollError());
    m_velocityNeckPitch = inertiaFactor * m_velocityNeckPitch + localBestFactor * (m_localBest.GetNeckPitchError() - GetNeckPitchError()) + globalBestFactor * (m_pGlobalBest->GetNeckPitchError() - GetNeckPitchError());
    m_velocityNeckYaw = inertiaFactor * m_velocityNeckYaw + localBestFactor * (m_localBest.GetNeckYawError() - GetNeckYawError()) + globalBestFactor * (m_pGlobalBest->GetNeckYawError() - GetNeckYawError());

    m_x += m_velocityX;
    m_y += m_velocityY;
    m_alpha += m_velocityAlpha;

    m_neckRollError += m_velocityNeckRoll;
    m_neckPitchError += m_velocityNeckPitch;
    m_neckYawError += m_velocityNeckYaw;

    m_neckRollError = Calculations::SaturizeToCircularInterval(m_neckRollError, pNeckErrorUncertaintyModel->GetMinRollError(), pNeckErrorUncertaintyModel->GetMaxRollError());
    m_neckPitchError = Calculations::SaturizeToCircularInterval(m_neckPitchError, pNeckErrorUncertaintyModel->GetMinPitchError(), pNeckErrorUncertaintyModel->GetMaxPitchError());
    m_neckYawError = Calculations::SaturizeToCircularInterval(m_neckYawError, pNeckErrorUncertaintyModel->GetMinYawError(), pNeckErrorUncertaintyModel->GetMaxYawError());
}
