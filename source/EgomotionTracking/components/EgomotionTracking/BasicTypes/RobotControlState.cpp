/*
 * CRobotControlState.cpp
 *
 *  Created on: 11.03.2013
 *      Author: abyte
 */

#include "RobotControlState.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>

CRobotControlState::CRobotControlState(float maxAbsVelocityX, float maxAbsVelocityY, float maxAbsVelocityAlpha) :
    m_platformVelocityX(0.0), m_platformVelocityY(0.0), m_platformVelocityAlpha(0.0),
    m_maxAbsVelocityX(maxAbsVelocityX), m_maxAbsVelocityY(maxAbsVelocityY), m_maxAbsVelocityAlpha(maxAbsVelocityAlpha),
    m_platformAxesTiltError(0.0)
{
}

CRobotControlState::~CRobotControlState()
{
}

float CRobotControlState::GetMaxAbsVelocityAlpha() const
{
    return m_maxAbsVelocityAlpha;
}

void CRobotControlState::SetMaxAbsVelocityAlpha(float maxAbsVelocityAlpha)
{
    m_maxAbsVelocityAlpha = maxAbsVelocityAlpha;
}

float CRobotControlState::GetMaxAbsVelocityX() const
{
    return m_maxAbsVelocityX;
}

void CRobotControlState::SetMaxAbsVelocityX(float maxAbsVelocityX)
{
    m_maxAbsVelocityX = maxAbsVelocityX;
}

float CRobotControlState::GetMaxAbsVelocityY() const
{
    return m_maxAbsVelocityY;
}

void CRobotControlState::SetMaxAbsVelocityY(float maxAbsVelocityY)
{
    m_maxAbsVelocityY = maxAbsVelocityY;
}

float CRobotControlState::GetPlatformVelocityAlpha() const
{
    return m_platformVelocityAlpha;
}

bool CRobotControlState::SetPlatformVelocityAlpha(float platformVelocityAlpha)
{
    if (platformVelocityAlpha > m_maxAbsVelocityAlpha)
    {
        m_platformVelocityAlpha = m_maxAbsVelocityAlpha;
        return false;
    }
    else if (platformVelocityAlpha < -m_maxAbsVelocityAlpha)
    {
        m_platformVelocityAlpha = -m_maxAbsVelocityAlpha;
        return false;
    }
    m_platformVelocityAlpha = platformVelocityAlpha;
    return true;
}

float CRobotControlState::GetPlatformVelocityX() const
{
    return m_platformVelocityX;
}

bool CRobotControlState::SetPlatformVelocityX(float platformVelocityX)
{
    if (platformVelocityX > m_maxAbsVelocityX)
    {
        m_platformVelocityX = m_maxAbsVelocityX;
        return false;
    }
    else if (platformVelocityX < -m_maxAbsVelocityX)
    {
        m_platformVelocityX = -m_maxAbsVelocityX;
        return false;
    }
    m_platformVelocityX = platformVelocityX;
    return true;
}

float CRobotControlState::GetPlatformVelocityY() const
{
    return m_platformVelocityY;
}

bool CRobotControlState::SetPlatformVelocityY(float platformVelocityY)
{
    if (platformVelocityY > m_maxAbsVelocityY)
    {
        m_platformVelocityY = m_maxAbsVelocityY;
        return false;
    }
    else if (platformVelocityY < -m_maxAbsVelocityY)
    {
        m_platformVelocityY = -m_maxAbsVelocityY;
        return false;
    }
    m_platformVelocityY = platformVelocityY;
    return true;
}

float CRobotControlState::CalculateAbsolutePlatformVelocity() const
{
    return sqrt(m_platformVelocityX * m_platformVelocityX + m_platformVelocityY * m_platformVelocityY);
}

bool CRobotControlState::SetPlatformVelocitiesKeepXYRatio(float platformVelocityX, float platformVelocityY, float platformVelocityAlpha)
{
    float saturationFactor;
    m_platformVelocityX = Calculations::SaturizeAbsolute(platformVelocityX, m_maxAbsVelocityX, saturationFactor);
    platformVelocityY *= saturationFactor;
    m_platformVelocityY = Calculations::SaturizeAbsolute(platformVelocityY, m_maxAbsVelocityY, saturationFactor);
    m_platformVelocityX *= saturationFactor;
    SetPlatformVelocityAlpha(platformVelocityAlpha);
    return true;
}

float CRobotControlState::GetPlatformAxesTiltError() const
{
    return m_platformAxesTiltError;
}

void CRobotControlState::SetPlatformAxesTiltError(float platformAxesTiltError)
{
    m_platformAxesTiltError = platformAxesTiltError;
}

float CRobotControlState::GetDeltaT() const
{
    return m_deltaT;
}

void CRobotControlState::SetDeltaT(float deltaT)
{
    m_deltaT = deltaT;
}

bool CRobotControlState::IsMoving() const
{
    //    return m_platformVelocityX != 0.0 || m_platformVelocityY != 0.0 || m_platformVelocityAlpha != 0.0;
    return fabs(m_platformVelocityX) >= 0.1 || fabs(m_platformVelocityY) >= 0.1 || fabs(m_platformVelocityAlpha) >= 0.1;
}
