/*
 * CSimpleLocationRegion.cpp
 *
 *  Created on: 11.03.2013
 *      Author: abyte
 */

#include "SimpleLocationRegion.h"
#include "math.h"
#include "float.h"

CSimpleLocationRegion::CSimpleLocationRegion() :
    m_minX(-FLT_MAX), m_maxX(FLT_MAX), m_minY(-FLT_MAX), m_maxY(FLT_MAX), m_minAlpha(0.0), m_maxAlpha(2 * M_PI)
{

}

CSimpleLocationRegion::CSimpleLocationRegion(float minX, float maxX, float minY, float maxY, float minAlpha, float maxAlpha) :
    m_minX(minX), m_maxX(maxX), m_minY(minY), m_maxY(maxY), m_minAlpha(minAlpha), m_maxAlpha(maxAlpha)
{
}

CSimpleLocationRegion::~CSimpleLocationRegion()
{
}

bool CSimpleLocationRegion::LocationIsInRegion(const CLocationState& location) const
{

    const float x = location.GetX();
    const float y = location.GetY();
    const float alpha = location.GetAlpha();

    return x >= m_minX && x <= m_maxX &&
           y >= m_minY && y <= m_maxY &&
           alpha >= m_minAlpha && alpha <= m_maxAlpha;
}
