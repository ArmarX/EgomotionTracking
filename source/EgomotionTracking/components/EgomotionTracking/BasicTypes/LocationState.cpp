/*
 * CLocationState.cpp
 *
 *  Created on: 11.03.2013
 *      Author: abyte
 */

#include "LocationState.h"
#include "math.h"
#include <Inventor/SbRotation.h>
#include <Inventor/SbVec3f.h>

CLocationState::CLocationState(CLocationRegion* pLocationRegion) :
    m_x(0.0), m_y(0.0), m_alpha(0.0), m_pLocationRegion(pLocationRegion)
{
}

CLocationState::CLocationState(float x, float y, float alpha, CLocationRegion* pLocationRegion) :
    m_x(x), m_y(y), m_alpha(alpha), m_pLocationRegion(pLocationRegion)
{
}

CLocationState::~CLocationState()
{
}

float CLocationState::GetAlpha() const
{
    return m_alpha;
}

bool CLocationState::SetAlpha(float alpha)
{
    m_alpha = alpha;
    if (m_pLocationRegion != NULL)
    {
        return m_pLocationRegion->LocationIsInRegion(*this);
    }
    return true;
}

const CLocationRegion* CLocationState::GetLocationRegion() const
{
    return m_pLocationRegion;
}

void CLocationState::SetLocationRegion(const CLocationRegion* pLocationRegion)
{
    m_pLocationRegion = pLocationRegion;
}

float CLocationState::GetX() const
{
    return m_x;
}

bool CLocationState::SetX(float x)
{
    m_x = x;
    if (m_pLocationRegion != NULL)
    {
        return m_pLocationRegion->LocationIsInRegion(*this);
    }
    return true;

}

float CLocationState::GetY() const
{
    return m_y;
}

bool CLocationState::SetY(float y)
{
    m_y = y;
    if (m_pLocationRegion != NULL)
    {
        return m_pLocationRegion->LocationIsInRegion(*this);
    }
    return true;
}

float CLocationState::CalculateDistance2D(const CLocationState& location) const
{
    float dx = m_x - location.GetX();
    float dy = m_y - location.GetY();
    return sqrt(dx * dx + dy * dy);
}

SbMatrix CLocationState::CalculateTransformationMatrix() const
{
    SbMatrix transformationMatrix;

    transformationMatrix.setTransform(SbVec3f(m_x, m_y, 0.0), SbRotation(SbVec3f(0.0, 0.0, 1.0), m_alpha), SbVec3f(1.0, 1.0, 1.0));
    return transformationMatrix;
}


