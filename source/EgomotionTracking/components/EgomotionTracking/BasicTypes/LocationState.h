/*
 * CLocationState.h
 *
 *  Created on: 11.03.2013
 *      Author: abyte
 */

#pragma once

#include "LocationRegion.h"
#include <EgomotionTracking/components/EgomotionTrackingConfiguration/TrackingConfiguration.h>

#include <iostream>

#include <Inventor/SbMatrix.h>

class CLocationRegion;

class CLocationState
{
public:
    CLocationState(CLocationRegion* pLocationRegion = NULL);
    CLocationState(float x, float y, float alpha, CLocationRegion* pLocationRegion = NULL);
    virtual ~CLocationState();

    float GetAlpha() const;
    bool SetAlpha(float alpha);
    const CLocationRegion* GetLocationRegion() const;
    void SetLocationRegion(const CLocationRegion* pLocationRegion);
    float GetX() const;
    bool SetX(float x);
    float GetY() const;
    bool SetY(float y);

    inline void Set(PF_REAL* locationValues)
    {
        m_x = locationValues[0];
        m_y = locationValues[1];
        m_alpha = locationValues[2];
    }

    inline void Get(PF_REAL* locationValues) const
    {
        locationValues[0] = m_x;
        locationValues[1] = m_y;
        locationValues[2] = m_alpha;
    }

    float CalculateDistance2D(const CLocationState& location) const;
    SbMatrix CalculateTransformationMatrix() const;
protected:
    float m_x;
    float m_y;
    float m_alpha;

    const CLocationRegion* m_pLocationRegion;
};

