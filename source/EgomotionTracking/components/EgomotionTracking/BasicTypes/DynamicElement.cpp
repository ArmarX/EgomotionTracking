#include "DynamicElement.h"

#include <EgomotionTracking/components/RVL/Representation/ModelBased/GeometricGraph/Base/ModelMesh.h>

using namespace RVL::Representation::ModelBased::GeometricGraph::Base;

DynamicElement::DynamicElement(RVL::Representation::ModelBased::GeometricGraph::Base::CModelTransformation* pModelTransformation)
    : m_pModelTransformation(pModelTransformation), m_dynamicElementStatus(eIdle), m_dynamicElementVisibility(eNotVisible), m_currentStateEstimate(0.0),
      m_particleReinitializationImportanceWeightTreshold(0.7), m_particleReinitializationCounterTreshold(10),
      m_particleReinitializationCounter(0),
      m_minParticles(50),
      m_maxParticles(200),
      m_useNumParticlesFunctionOfTransformationRange(false),
      m_transformationRangeNumParticlesRatio(5),
      m_numParticles(100)
{
    CalculateTransformationBoundingBox();
    CalculateFrontalFaceVertices();
}

DynamicElement::DynamicElement(std::map<int, CModelTransformation*> modelTransformationMap, CModelTransformation* pModelTransformation)
    : m_pModelTransformation(pModelTransformation), m_dynamicElementStatus(eIdle), m_dynamicElementVisibility(eNotVisible), m_currentStateEstimate(0.0),
      m_particleReinitializationImportanceWeightTreshold(0.7), m_particleReinitializationCounterTreshold(10),
      m_particleReinitializationCounter(0),
      m_minParticles(50),
      m_maxParticles(200),
      m_useNumParticlesFunctionOfTransformationRange(false),
      m_transformationRangeNumParticlesRatio(5),
      m_numParticles(100),
      m_modelTransformationMap(modelTransformationMap)
{
    CalculateTransformationBoundingBox();
    CalculateFrontalFaceVertices();
}

DynamicElement::DynamicElement()
    : m_dynamicElementStatus(eIdle), m_dynamicElementVisibility(eNotVisible), m_currentStateEstimate(0.0),
      m_particleReinitializationImportanceWeightTreshold(0.7), m_particleReinitializationCounterTreshold(10),
      m_particleReinitializationCounter(0),
      m_minParticles(50),
      m_maxParticles(200),
      m_useNumParticlesFunctionOfTransformationRange(false),
      m_transformationRangeNumParticlesRatio(5),
      m_numParticles(100)
{

}

void DynamicElement::SetDynamicElementStatus(DynamicElement::DynamicElementStatus dynamicElementStatus)
{
    if (m_dynamicElementStatus == eIdle)
    {
        if (dynamicElementStatus == eActive)
        {
            InitializeDynamicElementParticles();

            m_dynamicElementStatus = eActive;
        }
    }
    if (m_dynamicElementStatus == eInactive && dynamicElementStatus == eActive)
    {
        m_dynamicElementStatus = eActive;
    }
    if (m_dynamicElementStatus == eActive && dynamicElementStatus == eInactive)
    {
        m_dynamicElementStatus = eInactive;
    }
    if (dynamicElementStatus == eIdle)
    {
        m_dynamicElementStatus = eIdle;
    }
}

DynamicElement::DynamicElementStatus DynamicElement::GetDynamicElementStatus() const
{
    return m_dynamicElementStatus;
}

void DynamicElement::SetDynamicElementVisibility(DynamicElementVisibility dynamicElementVisibility)
{
    m_dynamicElementVisibility = dynamicElementVisibility;
}

DynamicElement::DynamicElementVisibility DynamicElement::GetDynamicElementVisibility() const
{
    return m_dynamicElementVisibility;
}

void DynamicElement::SetCurrentStateEstimate(PF_REAL stateEstimate, PF_REAL importanceWeight)
{
    m_currentStateEstimate = stateEstimate;
    m_currentStateEstimateImportanceWeight = importanceWeight;

    if (m_currentStateEstimateImportanceWeight < m_particleReinitializationImportanceWeightTreshold)
    {
        if (++m_particleReinitializationCounter == m_particleReinitializationCounterTreshold)
        {
            InitializeDynamicElementParticles();
        }
    }
    else
    {
        m_particleReinitializationCounter = 0;
    }
}

PF_REAL DynamicElement::GetCurrentStateEstimate() const
{
    return m_currentStateEstimate;
}

PF_REAL DynamicElement::GetCurrentStateEstimateImportanceWeight() const
{
    return m_currentStateEstimateImportanceWeight;
}

CModelTransformation* DynamicElement::getModelTransformation()
{
    return m_pModelTransformation;
}

CModelTransformation* DynamicElement::getModelTransformation(int threadId)
{
    return m_modelTransformationMap.at(threadId);
}

const std::vector<DynamicElementParticle>& DynamicElement::getDynamicElementParticles() const
{
    return m_dynamicElementParticles;
}

std::vector<DynamicElementParticle>& DynamicElement::getDynamicElementParticlesWriteable()
{
    return m_dynamicElementParticles;
}

void DynamicElement::CalculateTransformationBoundingBox()
{
    m_transformationBoundingBox.SetEmpty();

    CModelTransformation* pModelTransformation = m_pModelTransformation;

    const std::map<int, CModelMesh*>& meshes = pModelTransformation->GetMeshes();

    if (pModelTransformation->GetTransformationType() == CModelTransformation::eTranslation)
    {
        static_cast<CModelTranslation*>(pModelTransformation)->SetParametricNormalizedTranslation(0.0);
    }
    else if (pModelTransformation->GetTransformationType() == CModelTransformation::eRotation)
    {
        static_cast<CModelRotation*>(pModelTransformation)->SetParametricNormalizedRotation(0.0);
    }

    for (std::map<int, CModelMesh*>::const_iterator meshIterator = meshes.begin(); meshIterator != meshes.end(); ++meshIterator)
    {
        m_transformationBoundingBox.Extend(meshIterator->second->GetBoundingBox());
    }

    if (pModelTransformation->GetTransformationType() == CModelTransformation::eTranslation)
    {
        static_cast<CModelTranslation*>(pModelTransformation)->SetParametricNormalizedTranslation(1.0);
    }
    else if (pModelTransformation->GetTransformationType() == CModelTransformation::eRotation)
    {
        static_cast<CModelRotation*>(pModelTransformation)->SetParametricNormalizedRotation(1.0);
    }
    for (std::map<int, CModelMesh*>::const_iterator meshIterator = meshes.begin(); meshIterator != meshes.end(); ++meshIterator)
    {
        m_transformationBoundingBox.Extend(meshIterator->second->GetBoundingBox());
    }

    /* Vertices. */

    /* Max/Min points. */
    SbVec3f pointFrontRightUp(m_transformationBoundingBox.GetMaxPoint().GetX(), m_transformationBoundingBox.GetMaxPoint().GetY(), m_transformationBoundingBox.GetMaxPoint().GetZ());
    SbVec3f pointBackLeftBottom(m_transformationBoundingBox.GetMinPoint().GetX(), m_transformationBoundingBox.GetMinPoint().GetY(), m_transformationBoundingBox.GetMinPoint().GetZ());
    /* Front face. */
    SbVec3f pointFrontRightBottom(pointFrontRightUp[0], pointFrontRightUp[1], pointBackLeftBottom[2]);
    SbVec3f pointFrontLeftBottom(pointFrontRightUp[0], pointBackLeftBottom[1], pointBackLeftBottom[2]);
    SbVec3f pointFrontLeftUp(pointFrontRightUp[0], pointBackLeftBottom[1], pointFrontRightUp[2]);
    /* Back face. */
    SbVec3f pointBackLeftUp(pointBackLeftBottom[0], pointBackLeftBottom[1], pointFrontRightUp[2]);
    SbVec3f pointBackRightUp(pointBackLeftBottom[0], pointFrontRightUp[1], pointFrontRightUp[2]);
    SbVec3f pointBackRightBottom(pointBackLeftBottom[0], pointFrontRightUp[1], pointBackLeftBottom[2]);

    /* FaceTriangles. */

    /* Front. */
    m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointFrontLeftUp, pointFrontRightUp, pointFrontLeftBottom));
    m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointFrontRightBottom, pointFrontLeftBottom, pointFrontRightUp));
    /* Right. */
    m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointFrontRightUp, pointBackRightUp, pointFrontRightBottom));
    m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointBackRightBottom, pointFrontRightBottom, pointBackRightUp));
    /* Left. */
    m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointBackLeftUp, pointFrontLeftUp, pointBackLeftBottom));
    m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointFrontLeftBottom, pointBackLeftBottom, pointFrontLeftUp));
    /* Back. */
    m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointBackRightUp, pointBackLeftUp, pointBackRightBottom));
    m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointBackLeftBottom, pointBackRightBottom, pointBackLeftUp));
    /* Up. */
    m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointBackLeftUp, pointBackRightUp, pointFrontLeftUp));
    m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointFrontRightUp, pointFrontLeftUp, pointBackRightUp));
    /* Bottom. */
    m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointFrontLeftBottom, pointFrontRightBottom, pointBackLeftBottom));
    m_transformationBoundingBoxFaceTriangleList.push_back(CFaceTriangle(pointBackRightBottom, pointBackLeftBottom, pointFrontRightBottom));

    /* Lines. */

    /* Front. */
    m_transformationBoundingBoxLineList.push_back(CLine(pointFrontRightUp, pointFrontRightBottom));
    m_transformationBoundingBoxLineList.push_back(CLine(pointFrontRightBottom, pointFrontLeftBottom));
    m_transformationBoundingBoxLineList.push_back(CLine(pointFrontLeftBottom, pointFrontLeftUp));
    m_transformationBoundingBoxLineList.push_back(CLine(pointFrontLeftUp, pointFrontRightUp));
    /* Back. */
    m_transformationBoundingBoxLineList.push_back(CLine(pointBackLeftUp, pointBackLeftBottom));
    m_transformationBoundingBoxLineList.push_back(CLine(pointBackLeftBottom, pointBackRightBottom));
    m_transformationBoundingBoxLineList.push_back(CLine(pointBackRightBottom, pointBackRightUp));
    m_transformationBoundingBoxLineList.push_back(CLine(pointBackRightUp, pointBackLeftUp));
    /* Sides. */
    m_transformationBoundingBoxLineList.push_back(CLine(pointFrontRightUp, pointBackRightUp));
    m_transformationBoundingBoxLineList.push_back(CLine(pointFrontRightBottom, pointBackRightBottom));
    m_transformationBoundingBoxLineList.push_back(CLine(pointFrontLeftBottom, pointBackLeftBottom));
    m_transformationBoundingBoxLineList.push_back(CLine(pointFrontLeftUp, pointBackLeftUp));
}

void DynamicElement::CalculateFrontalFaceVertices()
{
    const std::map<int, CModelMesh*>& meshes = m_pModelTransformation->GetMeshes();

    for (std::map<int, CModelMesh*>::const_iterator meshIterator = meshes.begin(); meshIterator != meshes.end(); ++meshIterator)
    {
        const std::map<int, CModelVertex*>& vertices = meshIterator->second->GetVertices();

        for (std::map<int, CModelVertex*>::const_iterator vertexIterator = vertices.begin(); vertexIterator != vertices.end(); ++vertexIterator)
        {
            const std::string& label = vertexIterator->second->GetLabel();

            if (!label.empty())
            {
                if (label.find("_A") != std::string::npos)
                {
                    m_frontalFaceVertices.vertexA = vertexIterator->second;
                }
                else if (label.find("_B") != std::string::npos)
                {
                    m_frontalFaceVertices.vertexB = vertexIterator->second;
                }
                else if (label.find("_C") != std::string::npos)
                {
                    m_frontalFaceVertices.vertexC = vertexIterator->second;
                }
                else if (label.find("_D") != std::string::npos)
                {
                    m_frontalFaceVertices.vertexD = vertexIterator->second;
                }
            }
        }
    }
}

RVL::Containers::_3D::CContinuousBoundingBox3D DynamicElement::getTransformationBoundingBox() const
{
    return m_transformationBoundingBox;
}

std::list<CLine>& DynamicElement::getTransformationBoundingBoxLineList()
{
    return m_transformationBoundingBoxLineList;
}

std::list<CFaceTriangle>& DynamicElement::getTransformationBoundingBoxFaceTriangleList()
{
    return m_transformationBoundingBoxFaceTriangleList;
}

void DynamicElement::InitializeDynamicElementParticles()
{
    m_dynamicElementParticles.clear();

    CModelTransformation* pModelTransformation = m_pModelTransformation;

    int numberOfParticles;
    if (m_useNumParticlesFunctionOfTransformationRange)
    {
        double transformationRange = 0.0;
        if (pModelTransformation->GetTransformationType() == CModelTransformation::eTranslation)
        {
            transformationRange = static_cast<CModelTranslation*>(pModelTransformation)->GetDisplacementRange();

        }
        else if (pModelTransformation->GetTransformationType() == CModelTransformation::eRotation)
        {
            transformationRange = static_cast<CModelRotation*>(pModelTransformation)->GetRotationRange();
        }
        numberOfParticles = (int)(transformationRange / m_transformationRangeNumParticlesRatio);
    }
    else
    {
        numberOfParticles = m_numParticles;
    }

    numberOfParticles = std::min(numberOfParticles, m_maxParticles);
    numberOfParticles = std::max(numberOfParticles, m_minParticles);

    float step = (1.0 / (numberOfParticles - 1));
    for (int i = 0; i < numberOfParticles; ++i)
    {
        DynamicElementParticle newDynamicElementParticle;
        newDynamicElementParticle.setAlpha(0 + i * step);
        newDynamicElementParticle.SetActive(true);
        m_dynamicElementParticles.push_back(newDynamicElementParticle);
    }

    std::cout << "Initialized " << numberOfParticles << " particles" << std::endl;

    m_particleReinitializationCounter = 0;
}

void DynamicElement::configureParticleReinitialization(double importanceWeightTreshold, int iterationCountTreshold)
{
    m_particleReinitializationImportanceWeightTreshold = importanceWeightTreshold;
    m_particleReinitializationCounterTreshold = iterationCountTreshold;
}

void DynamicElement::configureNumberOfParticles(int minParticles, int maxParticles, bool useNumParticlesFunctionOfTransformationRange, int transformationRangeNumParticlesRatio,
        int numParticles)
{
    m_minParticles = minParticles;
    m_maxParticles = maxParticles;
    m_useNumParticlesFunctionOfTransformationRange = useNumParticlesFunctionOfTransformationRange;
    m_transformationRangeNumParticlesRatio = transformationRangeNumParticlesRatio;
    m_numParticles = numParticles;
}

const DynamicElement::FrontalFaceVertices& DynamicElement::getFrontalFaceVertices() const
{
    return m_frontalFaceVertices;
}

DynamicElement::FrontalFaceVertices& DynamicElement::getFrontalFaceVerticesWriteable()
{
    return m_frontalFaceVertices;
}
