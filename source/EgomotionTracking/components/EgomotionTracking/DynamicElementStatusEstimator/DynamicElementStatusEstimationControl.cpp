#include <EgomotionTracking/components/RVL/Representation/ModelBased/GeometricGraph/Base/ModelMesh.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/ObservationModel/JunctionPointLineModelObservationModel.h>

#include <boost/graph/topological_sort.hpp>

#include "DynamicElementStatusEstimationControl.h"

using namespace RVL::Representation::ModelBased::GeometricGraph::Base;

DynamicElementStatusEstimationControl::DynamicElementStatusEstimationControl(int maxNumberOfDynamicElementParticles, EVP::Threading::CThreadPool* pThreadPool)
    : m_dynamicElementStateEstimateAcceptabilityTreshold(0.95)
{
    m_pSingleDynamicElementStatusFilter = new SingleDynamicElementStatusFilter(maxNumberOfDynamicElementParticles, pThreadPool);
}

DynamicElementStatusEstimationControl::~DynamicElementStatusEstimationControl()
{
    delete m_pSingleDynamicElementStatusFilter;
}

void DynamicElementStatusEstimationControl::dispatch()
{
    if (m_dynamicElements.empty())
    {
        return;
    }

    CHeadConfiguration headConfiguration = m_pRobotControl->GetHeadConfiguration();
    headConfiguration.SetNeckError(m_locationEstimate.GetNeckRollError(), m_locationEstimate.GetNeckPitchError(), m_locationEstimate.GetNeckYawError());
    SbVec3f cameraTranslation;
    SbRotation cameraOrientation;
    m_pRobotControl->CalculateCameraPosition(m_locationEstimate, cameraTranslation, cameraOrientation, headConfiguration);
    m_pParametricLineRenderer->SetCameraPosition(OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(cameraTranslation, OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(cameraOrientation)));
    m_pParametricLineRenderer->Render(m_locationEstimate.GetScreenLinesWriteable(), m_locationEstimate.GetScreenJunctionPointsWriteable(), false);
    m_pParametricLineRenderer->SetStaticAndEstimatedProjectedScreenLineList(m_locationEstimate.GetScreenLines());
    m_pParametricLineRenderer->SetStaticAndEstimatedProjectedScreenFaceTriangleList(m_pParametricLineRenderer->GetProjectedScreenTriangleList());
    m_pRenderingUnit->UpdateCameraPositionFromParent();
    m_pRenderingUnit->UpdateStaticAndEstimatedProjectedPrimitivesFromParent();

    SbMatrix cameraFrame;
    cameraFrame.setTransform(cameraTranslation, OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(cameraOrientation), SbVec3f(1.0f, 1.0f, 1.0f));
    m_pSingleDynamicElementStatusFilter->setViewingMatrix(cameraFrame);

    /* Create the OcclusionGraph. Number of vertices is equal to the number of DynamicElements. Vertices correspond to the index of a DynamicElement in the list they are stored in. */

    typedef boost::adjacency_list<> OcclusionGraph;
    OcclusionGraph occlusionGraph(m_dynamicElements.size());

    /* Run a check on DynamicElements for occlusion. Add the necessary edges to the OcclusionGraph. */

    buildOcclusionGraph(occlusionGraph);

    /* Perform a topological sort on the OcclusionGraph. */

    typedef boost::graph_traits< OcclusionGraph >::vertex_descriptor Vertex;
    typedef std::vector< Vertex > VertexContainer;
    VertexContainer vertexContainer;
    boost::topological_sort(occlusionGraph, std::back_inserter(vertexContainer));

    /* Iterate over the vertices in the order given by the topological sort. */

    std::list<int> invalidIndexes;
    std::vector<DynamicElementParticle> results;
    std::vector<DynamicElementParticle> partialResults;
    for (VertexContainer::reverse_iterator reverseVertexIterator = vertexContainer.rbegin(); reverseVertexIterator != vertexContainer.rend(); ++reverseVertexIterator)
    {
        std::list<int>::iterator invalidIterator = std::find(invalidIndexes.begin(), invalidIndexes.end(), *reverseVertexIterator);

        if (invalidIterator == invalidIndexes.end())
        {
            /* Vertex is valid. */

            if (m_dynamicElements.at(*reverseVertexIterator).GetDynamicElementVisibility() == DynamicElement::eNotVisible)
            {
                /* DynamicElement is not visible. Set DynamicElementStatus to eInactive. */

                m_dynamicElements.at(*reverseVertexIterator).SetDynamicElementStatus(DynamicElement::eInactive);
            }
            else
            {
                /* DynamicElement is visible. Set DynamicElementStatus to eActive. */

                m_dynamicElements.at(*reverseVertexIterator).SetDynamicElementStatus(DynamicElement::eActive);

                /* Run a filter iteration. */

                DynamicElement& dynamicElement = m_dynamicElements.at(*reverseVertexIterator);
                m_pSingleDynamicElementStatusFilter->configure(dynamicElement.getDynamicElementParticles(), &dynamicElement);
                PF_REAL resultWeight = m_pSingleDynamicElementStatusFilter->filter(1.0);
                m_pSingleDynamicElementStatusFilter->getParticles(dynamicElement.getDynamicElementParticlesWriteable());
                dynamicElement.SetCurrentStateEstimate(m_pSingleDynamicElementStatusFilter->getStatusEstimate().GetImportanceWeight(), resultWeight);

                DynamicElementParticle& result = m_pSingleDynamicElementStatusFilter->getStatusEstimate();
                m_pRenderingUnit->RenderDynamicElementParticle(result, &dynamicElement, 0);

                if (resultWeight < m_dynamicElementStateEstimateAcceptabilityTreshold)
                {
                    /* State estimate is not acceptable. Mark occluded DynamicElements as not valid. */

                    typename boost::graph_traits<OcclusionGraph>::adjacency_iterator adjacencyIterator;
                    typename boost::graph_traits<OcclusionGraph>::adjacency_iterator adjacencyIteratorEnd;
                    for (boost::tie(adjacencyIterator, adjacencyIteratorEnd) = boost::adjacent_vertices(*reverseVertexIterator, occlusionGraph);
                         adjacencyIterator != adjacencyIteratorEnd; ++adjacencyIterator)
                    {
                        invalidIndexes.push_back(*adjacencyIterator);
                    }

                    partialResults.push_back(result);
                }
                else
                {
                    m_pParametricLineRenderer->AddEstimatedScreenLineList(result.GetScreenLines());
                    m_pParametricLineRenderer->AddEstimatedScreenFaceTriangleList(m_pRenderingUnit->GetParametricLineRenderer().GetProjectedScreenTriangleList());
                    m_pRenderingUnit->UpdateStaticAndEstimatedProjectedPrimitivesFromParent();

                    results.push_back(result);
                }
            }
        }
        else
        {
            /* Vertex is not valid. Set DynamicElementStatus to eInactive. */

            m_dynamicElements.at(*reverseVertexIterator).SetDynamicElementStatus(DynamicElement::eInactive);
        }
    }
    ((CJunctionPointLineModelObservationModel*)m_pObservationModel)->DrawParticles(results, true, 0, 255, 0);
    ((CJunctionPointLineModelObservationModel*)m_pObservationModel)->DrawParticles(partialResults, false, 255, 0, 0);
    ((CJunctionPointLineModelObservationModel*)m_pObservationModel)->DrawParticle(m_locationEstimate, false, 0, 0, 255);
}

void DynamicElementStatusEstimationControl::setLocationEstimate(const CLocationParticle& locationEstimate)
{
    m_locationEstimate = locationEstimate;
}

void DynamicElementStatusEstimationControl::setLocationEstimateIsValid(const bool& locationEstimateIsValid)
{
    m_locationEstimateIsValid = locationEstimateIsValid;
}

void DynamicElementStatusEstimationControl::setTrackingImageProcessor(CTrackingImageProcessorInterface* pTrackingImageProcessor)
{
    m_pTrackingImageProcessor = pTrackingImageProcessor;
}

void DynamicElementStatusEstimationControl::setObservationModel(CObservationModelInterface* pObservationModel)
{
    m_pObservationModel = pObservationModel;
    updateLinks();
}

void DynamicElementStatusEstimationControl::setRenderingUnit(CRenderingUnit* pRenderingUnit)
{
    m_pRenderingUnit = pRenderingUnit;
    updateLinks();
}

void DynamicElementStatusEstimationControl::setParametricLineRenderer(CParametricLineRenderer* pParametricLineRenderer)
{
    m_pParametricLineRenderer = pParametricLineRenderer;
}

void DynamicElementStatusEstimationControl::setDynamicElementStatusUncertaintyModel(DynamicElementStatusUncertaintyModel* pDynamicElementStatusUncertaintyModel)
{
    m_pDynamicElementStatusUncertaintyModel = pDynamicElementStatusUncertaintyModel;
    updateLinks();
}

void DynamicElementStatusEstimationControl::setRobotControl(CRobotControlInterface* pRobotControl)
{
    m_pRobotControl = pRobotControl;
}

void DynamicElementStatusEstimationControl::initializeDynamicElements(int numberOfThreads)
{
    m_dynamicElements.clear();

    /* Get filter multiple mesh and visualization multiple meshes. */
    const std::map<int, CModelMultipleMesh*>& visualizationModelMultipleMeshMap = m_pParametricLineRenderer->GetRenderingModel()->getVisualizationModelMultipleMeshMap();
    const CModelMultipleMesh* pModelMultipleMesh = m_pParametricLineRenderer->GetRenderingModel()->getModelMultipleMesh();

    /* Use the transformations from the filter multiple mesh for reference. */
    const std::map<int, CModelTransformation*>& referenceModelTransformationMap = pModelMultipleMesh->GetTransformations();

    for (std::map<int, CModelTransformation*>::const_iterator referenceModelTranformationIterator = referenceModelTransformationMap.begin();
         referenceModelTranformationIterator != referenceModelTransformationMap.end(); ++referenceModelTranformationIterator)
    {
        if (referenceModelTranformationIterator->second->HasMeshes())
        {
            if (referenceModelTranformationIterator->second->GetTransformationType() == CModelTransformation::eTranslation ||
                referenceModelTranformationIterator->second->GetTransformationType() == CModelTransformation::eRotation)
            {

                std::map<int, CModelTransformation*> tempTransformationMap;
                tempTransformationMap.clear();

                int transformationKey = referenceModelTranformationIterator->first;

                for (int i = 0; i < numberOfThreads; ++i)
                {
                    const std::map<int, CModelTransformation*>& currentModelTransformationMap = visualizationModelMultipleMeshMap.at(i)->GetTransformations();

                    tempTransformationMap.insert(std::pair<int, CModelTransformation*>(i, currentModelTransformationMap.at(transformationKey)));
                }

                DynamicElement newDynamicElement(tempTransformationMap, referenceModelTranformationIterator->second);
                m_dynamicElements.push_back(newDynamicElement);
            }
        }
    }
}

void DynamicElementStatusEstimationControl::setupModelVisualization()
{
    m_pSingleDynamicElementStatusFilter->setupModelVisualization();
}

const std::vector<DynamicElement>& DynamicElementStatusEstimationControl::getDynamicElements() const
{
    return m_dynamicElements;
}

SingleDynamicElementStatusFilter* DynamicElementStatusEstimationControl::getSingleDynamicElementStatusFilter()
{
    return m_pSingleDynamicElementStatusFilter;
}

void DynamicElementStatusEstimationControl::updateLinks()
{
    if (m_pObservationModel)
    {
        m_pSingleDynamicElementStatusFilter->setObservationModel(m_pObservationModel);
    }
    if (m_pRenderingUnit)
    {
        m_pSingleDynamicElementStatusFilter->setRenderingUnit(m_pRenderingUnit);
    }
    if (m_pDynamicElementStatusUncertaintyModel)
    {
        m_pSingleDynamicElementStatusFilter->setDynamicElementStatusUncertaintyModel(m_pDynamicElementStatusUncertaintyModel);
    }
}

bool DynamicElementStatusEstimationControl::buildOcclusionGraph(boost::adjacency_list<>& occlusionGraph)
{
    /* Project BoundingBoxes. */

    std::vector< std::vector<CScreenLine> > boundingBoxScreenLines(m_dynamicElements.size());
    std::vector< std::vector<CFaceTriangle> > boundingBoxScreenFaceTriangles(m_dynamicElements.size());

    for (std::vector<DynamicElement>::iterator dynamicElementIterator = m_dynamicElements.begin(); dynamicElementIterator != m_dynamicElements.end(); ++dynamicElementIterator)
    {
        bool linesProjected = m_pParametricLineRenderer->ProjectLinesToScreen(dynamicElementIterator->getTransformationBoundingBoxLineList(),
                              boundingBoxScreenLines.at(dynamicElementIterator - m_dynamicElements.begin()),
                              false);
        bool faceTrianglesProjected = m_pParametricLineRenderer->ProjectFaceTrianglesToScreen(dynamicElementIterator->getTransformationBoundingBoxFaceTriangleList(),
                                      boundingBoxScreenFaceTriangles.at(dynamicElementIterator - m_dynamicElements.begin()), false);

        /* Set DynamicElementVisibility. */

        if (linesProjected || faceTrianglesProjected)
        {
            dynamicElementIterator->SetDynamicElementVisibility(DynamicElement::eVisible);
        }
        else
        {
            dynamicElementIterator->SetDynamicElementVisibility(DynamicElement::eNotVisible);
        }
    }

    /* Calculate BoundingSquares. */

    std::vector<CVisibleScreenRegion> boundingSquares(m_dynamicElements.size());

    for (std::vector< std::vector<CScreenLine> >::iterator boundingBoxLinesIterator = boundingBoxScreenLines.begin(); boundingBoxLinesIterator != boundingBoxScreenLines.end();
         ++boundingBoxLinesIterator)
    {
        CVisibleScreenRegion boundingSquare(__FLT_MAX__, __FLT_MIN__, __FLT_MAX__, __FLT_MIN__);

        for (std::vector<CScreenLine>::iterator screenLineIterator = boundingBoxLinesIterator->begin(); screenLineIterator != boundingBoxLinesIterator->end(); ++screenLineIterator)
        {
            boundingSquare.m_xMin = std::min(boundingSquare.m_xMin, screenLineIterator->m_p1[0]);
            boundingSquare.m_yMin = std::min(boundingSquare.m_yMin, screenLineIterator->m_p1[1]);
            boundingSquare.m_xMax = std::max(boundingSquare.m_xMax, screenLineIterator->m_p1[0]);
            boundingSquare.m_yMax = std::max(boundingSquare.m_yMax, screenLineIterator->m_p1[1]);
        }

        boundingSquares.at(boundingBoxLinesIterator - boundingBoxScreenLines.begin()) = boundingSquare;
    }

    /* Check all pairs of BoundingBoxes for occlusion. */

    for (std::vector< std::vector<CScreenLine> >::iterator boundingBoxLinesIteratorA = boundingBoxScreenLines.begin(); boundingBoxLinesIteratorA != boundingBoxScreenLines.end() - 1;
         ++boundingBoxLinesIteratorA)
    {
        int indexA = boundingBoxLinesIteratorA - boundingBoxScreenLines.begin();

        for (std::vector< std::vector<CScreenLine> >::iterator boundingBoxLinesIteratorB = boundingBoxLinesIteratorA + 1; boundingBoxLinesIteratorB != boundingBoxScreenLines.end();
             ++boundingBoxLinesIteratorB)
        {
            int indexB = boundingBoxLinesIteratorB - boundingBoxScreenLines.begin();

            /* Check if BoundingSquares intersect. */

            CVisibleScreenRegion& boundingSquareA = boundingSquares.at(indexA);
            CVisibleScreenRegion& boundingSquareB = boundingSquares.at(indexB);

            if (boundingSquareA.m_xMin < boundingSquareB.m_xMax && boundingSquareA.m_xMax > boundingSquareB.m_xMin &&
                boundingSquareA.m_yMin < boundingSquareB.m_yMax && boundingSquareA.m_yMax > boundingSquareB.m_yMin)
            {
                /* Calculate BoundingSquareIntersection. */

                CVisibleScreenRegion boundingSquareIntersection;
                boundingSquareIntersection.m_xMin = std::max(boundingSquareA.m_xMin, boundingSquareB.m_xMin);
                boundingSquareIntersection.m_yMin = std::max(boundingSquareA.m_yMin, boundingSquareB.m_yMin);
                boundingSquareIntersection.m_xMax = std::min(boundingSquareA.m_xMax, boundingSquareB.m_xMax);
                boundingSquareIntersection.m_yMax = std::min(boundingSquareA.m_yMax, boundingSquareB.m_yMax);

                /* Fit ScreenLines to BoundingSquareIntersection. */

                std::vector<CScreenLine> croppedScreenLinesA;
                std::vector<CScreenLine> croppedScreenLinesB;

                m_pParametricLineRenderer->Fit2dLinesToVisibleScreenRegion(*boundingBoxLinesIteratorA, boundingSquareIntersection, croppedScreenLinesA);
                m_pParametricLineRenderer->Fit2dLinesToVisibleScreenRegion(*boundingBoxLinesIteratorB, boundingSquareIntersection, croppedScreenLinesB);

                /* Calculate intersections between both sets of ScreenLines. */

                std::vector<std::list<SbVec3f> > intersectionPointsA;
                std::vector<std::list<SbVec3f> > intersectionPointsB;

                if (croppedScreenLinesA.size() > 0 && croppedScreenLinesB.size() > 0)
                {
                    intersectionPointsA.resize(croppedScreenLinesA.size());
                    intersectionPointsB.resize(croppedScreenLinesB.size());

                    std::vector<std::list<SbVec3f> >::iterator intersectionPointListIteratorA = intersectionPointsA.begin();

                    for (std::vector<CScreenLine>::iterator screenLineIteratorA = croppedScreenLinesA.begin(); screenLineIteratorA != croppedScreenLinesA.end();
                         ++screenLineIteratorA, ++intersectionPointListIteratorA)
                    {
                        std::vector<std::list<SbVec3f> >::iterator intersectionPointListIteratorB = intersectionPointsB.begin();

                        for (std::vector<CScreenLine>::iterator screenLineIteratorB = croppedScreenLinesB.begin(); screenLineIteratorB != croppedScreenLinesB.end();
                             ++screenLineIteratorB, ++intersectionPointListIteratorB)
                        {
                            SbVec3f intersectionPointA, intersectionPointB;

                            if (m_pParametricLineRenderer->Intersect2dLines(*screenLineIteratorA, *screenLineIteratorB, intersectionPointA, intersectionPointB) == CParametricLineRenderer::eRealIntersectionPoint)
                            {
                                intersectionPointListIteratorA->push_back(intersectionPointA);
                                intersectionPointListIteratorB->push_back(intersectionPointB);
                            }
                        }
                    }
                }

                /* Segment both sets of ScreenLines. */

                std::vector<CScreenLine> segmentsA;
                std::vector<CScreenLine> segmentsB;

                std::vector<CScreenLine>::iterator screenLineIteratorA = croppedScreenLinesA.begin();
                for (std::vector<std::list<SbVec3f> >::iterator intersectionPointListIteratorA = intersectionPointsA.begin(); intersectionPointListIteratorA != intersectionPointsA.end();
                     ++intersectionPointListIteratorA, ++screenLineIteratorA)
                {
                    intersectionPointListIteratorA->push_back(screenLineIteratorA->m_p1);
                    intersectionPointListIteratorA->push_back(screenLineIteratorA->m_p2);

                    intersectionPointListIteratorA->sort(compare_SbVec3fPosition);
                    intersectionPointListIteratorA->unique();

                    std::list<SbVec3f>::const_iterator intersectionPointIteratorA = intersectionPointListIteratorA->begin();
                    SbVec3f lastPointA = *intersectionPointIteratorA;
                    ++intersectionPointIteratorA;

                    for (; intersectionPointIteratorA != intersectionPointListIteratorA->end(); ++intersectionPointIteratorA)
                    {
                        segmentsA.push_back(CScreenLine(lastPointA, *intersectionPointIteratorA));

                        lastPointA = *intersectionPointIteratorA;
                    }
                }

                std::vector<CScreenLine>::iterator screenLineIteratorB = croppedScreenLinesB.begin();
                for (std::vector<std::list<SbVec3f> >::iterator intersectionPointListIteratorB = intersectionPointsB.begin(); intersectionPointListIteratorB != intersectionPointsB.end();
                     ++intersectionPointListIteratorB, ++screenLineIteratorB)
                {
                    intersectionPointListIteratorB->push_back(screenLineIteratorB->m_p1);
                    intersectionPointListIteratorB->push_back(screenLineIteratorB->m_p2);

                    intersectionPointListIteratorB->sort(compare_SbVec3fPosition);
                    intersectionPointListIteratorB->unique();

                    std::list<SbVec3f>::const_iterator intersectionPointIteratorB = intersectionPointListIteratorB->begin();
                    SbVec3f lastPointB = *intersectionPointIteratorB;
                    ++intersectionPointIteratorB;

                    for (; intersectionPointIteratorB != intersectionPointListIteratorB->end(); ++intersectionPointIteratorB)
                    {
                        segmentsB.push_back(CScreenLine(lastPointB, *intersectionPointIteratorB));

                        lastPointB = *intersectionPointIteratorB;
                    }
                }

                /* Compare each set of ScreenLines to the opposing set of ScreenFaceTriangles. Check if there is an occlusion. */

                bool AOccludedByB = false;
                for (std::vector<CScreenLine>::iterator segmentIteratorA = segmentsA.begin(); segmentIteratorA != segmentsA.end(); ++segmentIteratorA)
                {
                    SbVec3f segmentCenterPointA = segmentIteratorA->CalculateCenterPoint();
                    if (m_pParametricLineRenderer->Check2dPointIsHidden(segmentCenterPointA, boundingBoxScreenFaceTriangles.at(indexB)))
                    {
                        AOccludedByB = true;
                        break;
                    }
                }

                bool BOccludedByA = false;
                for (std::vector<CScreenLine>::iterator segmentIteratorB = segmentsB.begin(); segmentIteratorB != segmentsB.end(); ++segmentIteratorB)
                {
                    SbVec3f segmentCenterPointB = segmentIteratorB->CalculateCenterPoint();
                    if (m_pParametricLineRenderer->Check2dPointIsHidden(segmentCenterPointB, boundingBoxScreenFaceTriangles.at(indexA)))
                    {
                        BOccludedByA = true;
                        break;
                    }
                }

                if (AOccludedByB && BOccludedByA)
                {
                    std::cout << "ERROR: DynamicElements " << boundingBoxLinesIteratorA - boundingBoxScreenLines.begin() << " and " << boundingBoxLinesIteratorB - boundingBoxScreenLines.begin() << " occlude each other" << std::endl;
                    std::cout << "SegmentsA: " << segmentsA.size() << " SegmentsB: " << segmentsB.size() << std::endl;

                    return false;
                }
                if (AOccludedByB)
                {
                    boost::add_edge(indexB, indexA, occlusionGraph);
                }
                if (BOccludedByA)
                {
                    boost::add_edge(indexA, indexB, occlusionGraph);
                }
            }
        }
    }

    return true;
}

void DynamicElementStatusEstimationControl::configureDynamicElementParticleReinitialization(double importanceWeightTreshold, int iterationCountTreshold)
{
    for (std::vector<DynamicElement>::iterator dynamicElementIterator = m_dynamicElements.begin(); dynamicElementIterator != m_dynamicElements.end();
         ++dynamicElementIterator)
    {
        dynamicElementIterator->configureParticleReinitialization(importanceWeightTreshold, iterationCountTreshold);
    }
}

void DynamicElementStatusEstimationControl::configureDynamicElementNumberOfParticles(int minParticles, int maxParticles, bool useNumParticlesFunctionOfTransformationRange,
        int transformationRangeNumParticlesRatio, int numParticles)
{
    for (std::vector<DynamicElement>::iterator dynamicElementIterator = m_dynamicElements.begin(); dynamicElementIterator != m_dynamicElements.end();
         ++dynamicElementIterator)
    {
        dynamicElementIterator->configureNumberOfParticles(minParticles, maxParticles, useNumParticlesFunctionOfTransformationRange, transformationRangeNumParticlesRatio, numParticles);
    }
}

void DynamicElementStatusEstimationControl::setDynamicElementStateEstimateAcceptabilityTreshold(double importanceWeightTreshold)
{
    m_dynamicElementStateEstimateAcceptabilityTreshold = importanceWeightTreshold;
}

void DynamicElementStatusEstimationControl::resetDynamicElements()
{
    for (std::vector<DynamicElement>::iterator dynamicElementIterator = m_dynamicElements.begin(); dynamicElementIterator != m_dynamicElements.end();
         ++dynamicElementIterator)
    {
        dynamicElementIterator->SetDynamicElementStatus(DynamicElement::eIdle);
    }
}

void DynamicElementStatusEstimationControl::setCameraImage(const CByteImage* pCameraImage)
{
    m_pSingleDynamicElementStatusFilter->setCameraImage(pCameraImage);
}

void DynamicElementStatusEstimationControl::setStereoCalibration(const CStereoCalibration* pStereoCalibration)
{
    m_pSingleDynamicElementStatusFilter->setStereoCalibration(pStereoCalibration);
}

void DynamicElementStatusEstimationControl::setCameraFrameUpdateRequired(bool cameraFrameUpdateRequired)
{
    m_pSingleDynamicElementStatusFilter->setCameraFrameUpdateRequired(cameraFrameUpdateRequired);
}

void DynamicElementStatusEstimationControl::setUseDissimilarityLikelihood(bool useDissimilarityLikelihood)
{
    m_pSingleDynamicElementStatusFilter->setUseDissimilarityLikelihood(useDissimilarityLikelihood);
}
