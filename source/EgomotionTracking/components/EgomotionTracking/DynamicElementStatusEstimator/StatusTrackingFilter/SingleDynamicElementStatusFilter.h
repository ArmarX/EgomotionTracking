#pragma once

#include "DynamicElementStatusFilterInterface.h"
#include "SingleDynamicElementStatusFilterExecutionUnit.h"
#include <EgomotionTracking/components/EgomotionTracking/Misc/ParticleFilterFramework/ParallelParticleFilterFramework/ParallelParticleFilterFramework.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/DynamicElementParticle.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/UncertaintyScatteringFactorCalculator/UncertaintyModelScatteringFactorCalculator.h>
#include <EgomotionTracking/components/EVP/Foundation/Process/ThreadPool.h>

class SingleDynamicElementStatusFilter : public DynamicElementStatusFilterInterface, protected CParallelParticleFilterFramework
{
public:

    SingleDynamicElementStatusFilter(int numberOfParticles, EVP::Threading::CThreadPool* pThreadPool);
    virtual ~SingleDynamicElementStatusFilter();

    void InitializeExecutionUnits() override;

    void configure(const std::vector<DynamicElementParticle>& dynamicElementParticles, DynamicElement* pDynamicElement) override;
    void getParticles(std::vector<DynamicElementParticle>& dynamicElementParticles) override;

    PF_REAL filter(PF_REAL sigmaFactor) override;

    int getNumberOfActiveParticles() const;
    void setNumberOfActiveParticles(int numberOfActiveParticles);

    void setResamplingThreshold(bool enabled, PF_REAL relativeNEffThreshold);
    void setSwarmOptimization(bool enabled, PF_REAL velocityFactorLocalBest, PF_REAL velocityFactorGlobalBest, PF_REAL localBestAgingFactor, PF_REAL bestWeightThreshold);
    void setAnnealing(bool enabled, unsigned int numberOfAnnealingLevels, PF_REAL annealingFactor, PF_REAL bestWeightThreshold, bool annealingConservativeUpdate);
    void setThresholdedMeanFactor(PF_REAL meanFactor);
    void setEstimationMode(const CEstimationMode& estimationMode);
    void setMeasurementUpdateActive(bool measurementUpdateActive);
    bool getMeasurementUpdateActive() const;
    unsigned int getCurrentAnnealingLevel();

    void setRenderingUnit(CRenderingUnit* pRenderingUnit) override;

    DynamicElementParticle& getStatusEstimate();

    void setupModelVisualization();

    void setViewingMatrix(const SbMatrix& viewingMatrix);

    void setCameraImage(const CByteImage* pCameraImage);

    void setStereoCalibration(const CStereoCalibration* pStereoCalibration);

    void setCameraFrameUpdateRequired(bool cameraFrameUpdateRequired);

    void setUseDissimilarityLikelihood(bool useDissimilarityLikelihood);

protected:

    PF_REAL m_annealingFactor;
    bool m_annealingConservativeUpdate;
    bool m_measurementUpdateActive;

    virtual PF_REAL calculateObservationModelSensitivity(unsigned int annealingLevel)
    {
        return pow(1 / m_annealingFactor, static_cast<int>(annealingLevel));
    }
    virtual PF_REAL calculateDynamicElementStatusUncertaintyModelDispersionCoefficient(unsigned int annealingLevel)
    {
        return pow(m_annealingFactor, static_cast<int>(annealingLevel));
    }

    void updateRenderingUnits();
    float calculateDynamicElementStatusScatteringFactor();

    DynamicElement* m_pDynamicElement;
    std::map<int, OfflineVirtualCameraView*> m_offlineVirtualCameraViewMap;
};

