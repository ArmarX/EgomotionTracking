#pragma once

#include <EgomotionTracking/components/EgomotionTracking/Misc/ParticleFilterFramework/ParallelParticleFilterFramework/SamplingAndWeightingExecutionUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/RobotControlState.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/RenderingUnit/RenderingUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/ObservationModel/ObservationModelInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/DynamicElementParticle.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/DynamicElementStatusUncertaintyModel/DynamicElementStatusUncertaintyModel.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ImageProcessor/DeviationImageProcessor.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/VirtualRenderer/OfflineVirtualCameraView.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/ObservationModel/DeviationObservationModel.h>

#include <EgomotionTracking/components/RVL/Representation/ModelBased/GeometricGraph/Visualization/OpenInventor/OpenInventorModelMultipleMesh.h>

class SingleDynamicElementStatusFilter;

class SingleDynamicElementStatusFilterExecutionUnit : public CSamplingAndWeightingExecutionUnit
{
public:

    SingleDynamicElementStatusFilterExecutionUnit(int id, OfflineVirtualCameraView* pOfflineVirtualCameraView, bool useVirtualRendering = false);
    ~SingleDynamicElementStatusFilterExecutionUnit() override;

    const CObservationModelInterface* GetObservationModel() const;
    void SetObservationModel(const CObservationModelInterface* pObservationModel);

    const DynamicElementStatusUncertaintyModel* GetDynamicElementStatusUncertaintyModel() const;
    void SetDynamicElementStatusUncertaintyModel(const DynamicElementStatusUncertaintyModel* pDynamicElementStatusUncertaintyModel);

    CRenderingUnit GetRenderingUnit() const;
    void SetRenderingUnit(const CRenderingUnit& renderingUnit);

    void SetMeasurementUpdateActive(bool measurementUpdateActive);

    void SetDynamicElement(DynamicElement* pDynamicElement);

    void UpdateRenderingUnit();

    void SetViewingMatrix(const SbMatrix& viewingMatrix);

    void SetCameraImage(const CByteImage* pCameraImage);

protected:

    void Sample(PF_REAL** ppParticle) override;

    PF_REAL CalculateParticleWeight(PF_REAL** ppParticle, bool externCall) override;

    const CObservationModelInterface* m_pObservationModel;
    const DynamicElementStatusUncertaintyModel* m_pDynamicElementStatusUncertaintyModel;
    CRenderingUnit m_renderingUnit;

    bool m_measurementUpdateActive;

    DynamicElementParticle m_tempDynamicElementParticle;
    DynamicElement* m_pDynamicElement;

    int m_id;

    bool m_useVirtualRendering;
    OfflineVirtualCameraView* m_pOfflineVirtualCameraView; /* Will probably make a member instance out of this. */
    SbMatrix m_viewingMatrix; /* Could switch this with a pointer to a member matrix of DESEControl. No need to constantly set it in that case. */

    DeviationObservationModel m_deviationObservationModel; /* May eventually switch this with a pointer to a shared instance. */

    const CByteImage* m_pVirtualRenderingImage; /* Could be either a pointer or a reference. Does not really make a difference. */
    const CByteImage* m_pCameraImage; /* Needs to be a pointer. */
};

