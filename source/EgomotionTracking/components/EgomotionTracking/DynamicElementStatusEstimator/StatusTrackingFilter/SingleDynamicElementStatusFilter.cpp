#include "SingleDynamicElementStatusFilter.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>

using namespace RVL::Representation::ModelBased::GeometricGraph::Visualization::OpenInventor;

SingleDynamicElementStatusFilter::SingleDynamicElementStatusFilter(int numberOfParticles, EVP::Threading::CThreadPool* pThreadPool)
    : DynamicElementStatusFilterInterface(),
      CParallelParticleFilterFramework(numberOfParticles, 1, pThreadPool->GetTotalThreads(), pThreadPool),
      m_annealingFactor(0.5), m_measurementUpdateActive(true)
{
    InitializeExecutionUnits();
}

SingleDynamicElementStatusFilter::~SingleDynamicElementStatusFilter()
{
    for (std::map<int, OfflineVirtualCameraView*>::iterator offlineVirtualCameraViewIterator = m_offlineVirtualCameraViewMap.begin();
         offlineVirtualCameraViewIterator != m_offlineVirtualCameraViewMap.end(); ++offlineVirtualCameraViewIterator)
    {
        if (offlineVirtualCameraViewIterator->second)
        {
            delete offlineVirtualCameraViewIterator->second;
        }
    }
}

void SingleDynamicElementStatusFilter::InitializeExecutionUnits()
{
    for (int i = 0; i < m_nThreads; ++i)
    {
        OfflineVirtualCameraView* pOfflineVirtualCameraView = new OfflineVirtualCameraView(640, 480, OfflineVirtualCameraView::eLeftCamera, 100, 10000);
        pOfflineVirtualCameraView->SetBackGroundColor(64, 64, 64);
        pOfflineVirtualCameraView->LoadStereoCalibration("/home/SMBAD/heyde/home/TrackingFiles/EgomotionTrackingRessources/stereo_calibration.txt");
        pOfflineVirtualCameraView->SetAntiAlias(true, 3);
        m_offlineVirtualCameraViewMap.insert(std::pair<int, OfflineVirtualCameraView*>(i, pOfflineVirtualCameraView));

        m_executionUnits.push_back(new SingleDynamicElementStatusFilterExecutionUnit(i, pOfflineVirtualCameraView, true));
    }
}

void SingleDynamicElementStatusFilter::configure(const std::vector<DynamicElementParticle>& dynamicElementParticles, DynamicElement* pDynamicElement)
{
    CParallelParticleFilterFramework::SetNumberOfActiveParticles(dynamicElementParticles.size(), false);

    int dynamicElementParticleIndex = 0;
    m_cTotal = 0.0;
    for (std::vector<DynamicElementParticle>::const_iterator dynamicElementParticleIterator = dynamicElementParticles.begin();
         dynamicElementParticleIterator != dynamicElementParticles.end(); ++dynamicElementParticleIterator, ++dynamicElementParticleIndex)
    {
        m_pC[dynamicElementParticleIndex] = m_cTotal;
        m_cTotal += dynamicElementParticleIterator->GetImportanceWeight();
        dynamicElementParticleIterator->Get(m_ppS[dynamicElementParticleIndex], &m_pPi[dynamicElementParticleIndex]);
    }

    m_pDynamicElement = pDynamicElement;
}

void SingleDynamicElementStatusFilter::getParticles(std::vector<DynamicElementParticle>& dynamicElementParticles)
{
    if (static_cast<int>(dynamicElementParticles.size()) != m_nActiveParticles)
    {
        COUT_ERROR << "dynamicElementParticles.size() != m_nActiveParticles" << std::endl;
        exit(0);
    }

    int dynamicElementParticleIndex = 0;
    for (std::vector<DynamicElementParticle>::iterator dynamicElementParticleIterator = dynamicElementParticles.begin();
         dynamicElementParticleIterator != dynamicElementParticles.end(); ++dynamicElementParticleIterator, ++dynamicElementParticleIndex)
    {
        dynamicElementParticleIterator->Set(m_ppS[dynamicElementParticleIndex], &m_pPi[dynamicElementParticleIndex]);
        if (dynamicElementParticleIndex < m_nActiveParticles)
        {
            dynamicElementParticleIterator->SetActive(true);
        }
        else
        {
            dynamicElementParticleIterator->SetActive(false);
        }
    }
}

PF_REAL SingleDynamicElementStatusFilter::filter(PF_REAL sigmaFactor)
{
    if (!(m_pObservationModel && m_pRenderingUnit && m_pDynamicElementStatusUncertaintyModel))
    {
        COUT_ERROR << "!(m_pObservationModel && m_pRobotControl && m_pRenderingUnit && m_pDynamicElementStatusUncertaintyModel)" << std::endl;
        m_estimateIsValid = false;
        return -1;
    }

    if (m_useAnnealing)
    {
        m_pObservationModel->SetSensibility(calculateObservationModelSensitivity(m_currentAnnealingLevel));
    }
    else
    {
        m_pObservationModel->SetSensibility(1.0);
    }

    m_pDynamicElementStatusUncertaintyModel->SetScatteringFactor(calculateDynamicElementStatusScatteringFactor());

    updateRenderingUnits();

    for (std::vector<CSamplingAndWeightingExecutionUnit*>::iterator euIterator = m_executionUnits.begin(); euIterator != m_executionUnits.end(); ++ euIterator)
    {
        SingleDynamicElementStatusFilterExecutionUnit* singleDynamicElementStatusFilterEU = static_cast<SingleDynamicElementStatusFilterExecutionUnit*>(*euIterator);
        singleDynamicElementStatusFilterEU->SetObservationModel(m_pObservationModel);
        singleDynamicElementStatusFilterEU->SetDynamicElementStatusUncertaintyModel(m_pDynamicElementStatusUncertaintyModel);
        singleDynamicElementStatusFilterEU->SetDynamicElement(m_pDynamicElement);
    }

    PF_REAL result[1];
    const PF_REAL resultWeight = ParticleFilter(result, sigmaFactor);

    if (!(m_useAnnealing && m_annealingConservativeUpdate) || m_currentAnnealingLevel == 0)
    {
        m_statusEstimate.setAlpha(result[0]);
    }
    m_estimateIsValid = true;
    return resultWeight;
}

void SingleDynamicElementStatusFilter::updateRenderingUnits()
{
    for (std::vector<CSamplingAndWeightingExecutionUnit*>::iterator euIterator = m_executionUnits.begin(); euIterator != m_executionUnits.end(); ++ euIterator)
    {
        SingleDynamicElementStatusFilterExecutionUnit* singleDynamicElementStatusFilterEU = static_cast<SingleDynamicElementStatusFilterExecutionUnit*>(*euIterator);
        singleDynamicElementStatusFilterEU->UpdateRenderingUnit();
    }
}

int SingleDynamicElementStatusFilter::getNumberOfActiveParticles() const
{
    return CParallelParticleFilterFramework::GetNumberOfActiveParticles();
}

void SingleDynamicElementStatusFilter::setNumberOfActiveParticles(int numberOfActiveParticles)
{
    CParallelParticleFilterFramework::SetNumberOfActiveParticles(numberOfActiveParticles);
}

void SingleDynamicElementStatusFilter::setResamplingThreshold(bool enabled, PF_REAL relativeNEffThreshold)
{
    SetUseResamplingThreshold(enabled);
    SetRelativeNEffResamplingThreshold(relativeNEffThreshold);
}

void SingleDynamicElementStatusFilter::setSwarmOptimization(bool enabled, PF_REAL velocityFactorLocalBest, PF_REAL velocityFactorGlobalBest, PF_REAL localBestAgingFactor,
        PF_REAL bestWeightThreshold)
{
    SetUseSwarmOptimization(enabled);
    SetSwarmVelocityLocalBestFactor(velocityFactorLocalBest);
    SetSwarmVelocityGlobalBestFactor(velocityFactorGlobalBest);
    SetSwarmLocalBestAgingFactor(localBestAgingFactor);
    SetSwarmOptimizationBestWeightThreshold(bestWeightThreshold);
}

void SingleDynamicElementStatusFilter::setAnnealing(bool enabled, unsigned int numberOfAnnealingLevels, PF_REAL annealingFactor, PF_REAL bestWeightThreshold,
        bool annealingConservativeUpdate)
{
    SetUseAnnealing(enabled);
    SetNumberOfAnnealingLevels(numberOfAnnealingLevels);
    SetAnnealingBestWeightThreshold(bestWeightThreshold);
    m_annealingFactor = annealingFactor;
    m_annealingConservativeUpdate = annealingConservativeUpdate;
}

void SingleDynamicElementStatusFilter::setThresholdedMeanFactor(PF_REAL meanFactor)
{
    CParallelParticleFilterFramework::SetThresholdedMeanFactor(meanFactor);
}

void SingleDynamicElementStatusFilter::setEstimationMode(const CParallelParticleFilterFramework::CEstimationMode& estimationMode)
{
    CParallelParticleFilterFramework::SetEstimationMode(estimationMode);
}

void SingleDynamicElementStatusFilter::setRenderingUnit(CRenderingUnit* pRenderingUnit)
{
    DynamicElementStatusFilterInterface::setRenderingUnit(pRenderingUnit);
    for (std::vector<CSamplingAndWeightingExecutionUnit*>::iterator euIterator = m_executionUnits.begin(); euIterator != m_executionUnits.end(); ++ euIterator)
    {
        SingleDynamicElementStatusFilterExecutionUnit* singleDynamicElementStatusFilterEU = dynamic_cast<SingleDynamicElementStatusFilterExecutionUnit*>(*euIterator);
        const CRenderingUnit& renderingUnit = *pRenderingUnit;
        singleDynamicElementStatusFilterEU->SetRenderingUnit(renderingUnit);
    }
}

void SingleDynamicElementStatusFilter::setMeasurementUpdateActive(bool measurementUpdateActive)
{
    m_measurementUpdateActive = measurementUpdateActive;
    for (std::vector<CSamplingAndWeightingExecutionUnit*>::iterator euIterator = m_executionUnits.begin(); euIterator != m_executionUnits.end(); ++ euIterator)
    {
        SingleDynamicElementStatusFilterExecutionUnit* singleDynamicElementStatusFilterEU = static_cast<SingleDynamicElementStatusFilterExecutionUnit*>(*euIterator);
        singleDynamicElementStatusFilterEU->SetMeasurementUpdateActive(m_measurementUpdateActive);
    }
}

bool SingleDynamicElementStatusFilter::getMeasurementUpdateActive() const
{
    return m_measurementUpdateActive;
}

unsigned int SingleDynamicElementStatusFilter::getCurrentAnnealingLevel()
{
    return CParallelParticleFilterFramework::GetCurrentAnnealingLevel();
}

float SingleDynamicElementStatusFilter::calculateDynamicElementStatusScatteringFactor()
{
    float scatteringFactor = 1.0;
    if (m_useAnnealing)
    {
        scatteringFactor *= calculateDynamicElementStatusUncertaintyModelDispersionCoefficient(m_currentAnnealingLevel);
    }
    return scatteringFactor;
}

DynamicElementParticle& SingleDynamicElementStatusFilter::getStatusEstimate()
{
    return m_statusEstimate;
}

void SingleDynamicElementStatusFilter::setupModelVisualization()
{
    std::map<int, COpenInventorModelMultipleMesh*> OIModelMultipleMeshMap = m_pRenderingUnit->GetParentParametricLineRenderer()->GetRenderingModel()->getVisualizationOIModelMultipleMeshMap();

    for (std::map<int, OfflineVirtualCameraView*>::iterator offlineVirtualCameraViewIterator = m_offlineVirtualCameraViewMap.begin();
         offlineVirtualCameraViewIterator != m_offlineVirtualCameraViewMap.end(); ++offlineVirtualCameraViewIterator)
    {
        offlineVirtualCameraViewIterator->second->AddNode(OIModelMultipleMeshMap.at(offlineVirtualCameraViewIterator->first)->GetBaseSwitch());
    }
}

void SingleDynamicElementStatusFilter::setViewingMatrix(const SbMatrix& viewingMatrix)
{
    for (std::vector<CSamplingAndWeightingExecutionUnit*>::iterator euIterator = m_executionUnits.begin(); euIterator != m_executionUnits.end(); ++ euIterator)
    {
        SingleDynamicElementStatusFilterExecutionUnit* singleDynamicElementStatusFilterEU = static_cast<SingleDynamicElementStatusFilterExecutionUnit*>(*euIterator);
        singleDynamicElementStatusFilterEU->SetViewingMatrix(viewingMatrix);
    }
}

void SingleDynamicElementStatusFilter::setCameraImage(const CByteImage* pCameraImage)
{
    for (std::vector<CSamplingAndWeightingExecutionUnit*>::iterator euIterator = m_executionUnits.begin(); euIterator != m_executionUnits.end(); ++ euIterator)
    {
        SingleDynamicElementStatusFilterExecutionUnit* singleDynamicElementStatusFilterEU = static_cast<SingleDynamicElementStatusFilterExecutionUnit*>(*euIterator);
        singleDynamicElementStatusFilterEU->SetCameraImage(pCameraImage);
    }
}

void SingleDynamicElementStatusFilter::setStereoCalibration(const CStereoCalibration* pStereoCalibration)
{
    for (std::map<int, OfflineVirtualCameraView*>::iterator offlineVirtualCameraViewIterator = m_offlineVirtualCameraViewMap.begin();
         offlineVirtualCameraViewIterator != m_offlineVirtualCameraViewMap.end(); ++offlineVirtualCameraViewIterator)
    {
        offlineVirtualCameraViewIterator->second->SetStereoCalibration(pStereoCalibration);
    }
}

void SingleDynamicElementStatusFilter::setCameraFrameUpdateRequired(bool cameraFrameUpdateRequired)
{
    for (std::vector<CSamplingAndWeightingExecutionUnit*>::iterator euIterator = m_executionUnits.begin(); euIterator != m_executionUnits.end(); ++ euIterator)
    {
        SingleDynamicElementStatusFilterExecutionUnit* singleDynamicElementStatusFilterEU = static_cast<SingleDynamicElementStatusFilterExecutionUnit*>(*euIterator);
        singleDynamicElementStatusFilterEU->setCameraFrameUpdateRequired(cameraFrameUpdateRequired);
    }
}

void SingleDynamicElementStatusFilter::setUseDissimilarityLikelihood(bool useDissimilarityLikelihood)
{
    m_useDissimilarity = useDissimilarityLikelihood;
}
