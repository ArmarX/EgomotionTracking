#pragma once

#include <EgomotionTracking/components/EgomotionTracking/Models/ObservationModel/ObservationModelInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/DynamicElementStatusUncertaintyModel/DynamicElementStatusUncertaintyModel.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/RenderingUnit/RenderingUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/RobotControlState.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/DynamicElementParticle.h>

class DynamicElementStatusFilterInterface
{
public:

    DynamicElementStatusFilterInterface();

    virtual void configure(const std::vector<DynamicElementParticle>& dynamicElementParticles, DynamicElement* pDynamicElement) = 0;
    virtual void getParticles(std::vector<DynamicElementParticle>& dynamicElementParticles) = 0;
    virtual PF_REAL filter(PF_REAL sigmaFactor) = 0;

    virtual void setObservationModel(CObservationModelInterface* pObservationModel);
    virtual void setDynamicElementStatusUncertaintyModel(DynamicElementStatusUncertaintyModel* pDynamicElementStatusUncertaintyModel);
    virtual void setRenderingUnit(CRenderingUnit* pRenderingUnit);

protected:

    DynamicElementParticle m_statusEstimate;
    bool m_estimateIsValid;

    CObservationModelInterface* m_pObservationModel;
    CRenderingUnit* m_pRenderingUnit;
    DynamicElementStatusUncertaintyModel* m_pDynamicElementStatusUncertaintyModel;
};

