#include "DynamicElementStatusFilterInterface.h"

DynamicElementStatusFilterInterface::DynamicElementStatusFilterInterface() :
    m_estimateIsValid(false),
    m_pObservationModel(NULL),
    m_pRenderingUnit(NULL),
    m_pDynamicElementStatusUncertaintyModel(NULL)
{

}

void DynamicElementStatusFilterInterface::setRenderingUnit(CRenderingUnit* pRenderingUnit)
{
    m_pRenderingUnit = pRenderingUnit;
}

void DynamicElementStatusFilterInterface::setObservationModel(CObservationModelInterface* pObservationModel)
{
    m_pObservationModel = pObservationModel;
}

void DynamicElementStatusFilterInterface::setDynamicElementStatusUncertaintyModel(DynamicElementStatusUncertaintyModel* pDynamicElementStatusUncertaintyModel)
{
    m_pDynamicElementStatusUncertaintyModel = pDynamicElementStatusUncertaintyModel;
}
