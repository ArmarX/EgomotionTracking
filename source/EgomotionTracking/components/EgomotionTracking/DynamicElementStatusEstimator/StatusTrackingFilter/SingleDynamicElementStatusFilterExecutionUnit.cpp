#include "SingleDynamicElementStatusFilterExecutionUnit.h"

#include <EgomotionTracking/components/RVL/Representation/ModelBased/GeometricGraph/Base/ModelMesh.h>

using namespace RVL::Representation::ModelBased::GeometricGraph::Visualization::OpenInventor;
using namespace RVL::Representation::ModelBased::GeometricGraph::Base;

SingleDynamicElementStatusFilterExecutionUnit::SingleDynamicElementStatusFilterExecutionUnit(int id, OfflineVirtualCameraView* pOfflineVirtualCameraView,
        bool useVirtualRendering)
    : CSamplingAndWeightingExecutionUnit(1), m_measurementUpdateActive(true), m_id(id),
      m_useVirtualRendering(useVirtualRendering), m_pOfflineVirtualCameraView(pOfflineVirtualCameraView),
      m_pVirtualRenderingImage(NULL)
{
    m_pVirtualRenderingImage = m_pOfflineVirtualCameraView->GetVirtualView();
}

SingleDynamicElementStatusFilterExecutionUnit::~SingleDynamicElementStatusFilterExecutionUnit()
{

}

void SingleDynamicElementStatusFilterExecutionUnit::Sample(PF_REAL** ppParticle)
{
    DynamicElementParticle oldDynamicElementParticle;
    PF_REAL weight = 0.0;
    oldDynamicElementParticle.Set(*ppParticle, &weight);

    DynamicElementParticle newDynamicElementParticle;
    newDynamicElementParticle.setAlpha(oldDynamicElementParticle.getAlpha());

    if (m_pDynamicElementStatusUncertaintyModel != NULL)
    {
        m_pDynamicElementStatusUncertaintyModel->AddUncertaintyToDynamicElementParticle(newDynamicElementParticle, &m_randomGenerator);
    }

    newDynamicElementParticle.Get(*ppParticle, &weight);
}

PF_REAL SingleDynamicElementStatusFilterExecutionUnit::CalculateParticleWeight(PF_REAL** ppParticle, bool externCall)
{
    if (m_measurementUpdateActive)
    {
        PF_REAL weight = 0.0;
        m_tempDynamicElementParticle.Set(*ppParticle, &weight);

        m_renderingUnit.RenderDynamicElementParticle(m_tempDynamicElementParticle, m_pDynamicElement, m_id);

        weight = m_pObservationModel->CalculateImportanceWeight(m_tempDynamicElementParticle, false);

        if (m_useDissimilarity)
        {
            /* Calculate the HomogenityDissimilarity value for the current dynamic element particle. */

            m_pOfflineVirtualCameraView->SetCameraFrame(m_viewingMatrix);
            m_pOfflineVirtualCameraView->Render();

            SbVec2f surfacePoints[4];
            const DynamicElementParticle::FrontalFacePoints2D& frontalFacePoints = m_tempDynamicElementParticle.getFrontalFacePoints2D();

            surfacePoints[0] = frontalFacePoints.pointA;
            surfacePoints[1] = frontalFacePoints.pointB;
            surfacePoints[2] = frontalFacePoints.pointC;
            surfacePoints[3] = frontalFacePoints.pointD;

            *m_pCurrentDissimilarity = m_deviationObservationModel.CalculateImportanceWeight(m_pVirtualRenderingImage, m_pCameraImage, surfacePoints, 5.0f / 3.0f);

            /* Calculate the HomogenityDissimilarity value for the current setup dynamic element particle.
             * Do this only if the value of alpha setup is between 0 and 1 inclusive.
             */

            if (*m_pCurrentAlphaSetup <= 1.0 && m_cameraFrameUpdateRequired)
            {
                m_tempDynamicElementParticle.setAlpha(*m_pCurrentAlphaSetup);

                m_renderingUnit.RenderDynamicElementParticle(m_tempDynamicElementParticle, m_pDynamicElement, m_id);

                m_pOfflineVirtualCameraView->Render();

                const DynamicElementParticle::FrontalFacePoints2D& setupFrontalFacePoints = m_tempDynamicElementParticle.getFrontalFacePoints2D();

                surfacePoints[0] = setupFrontalFacePoints.pointA;
                surfacePoints[1] = setupFrontalFacePoints.pointB;
                surfacePoints[2] = setupFrontalFacePoints.pointC;
                surfacePoints[3] = setupFrontalFacePoints.pointD;

                float setupDissimilarity = m_deviationObservationModel.CalculateImportanceWeight(m_pVirtualRenderingImage, m_pCameraImage, surfacePoints, 5.0f / 3.0f);

                if (setupDissimilarity < *m_pMinDissimilarity)
                {
                    *m_pMinDissimilarity = setupDissimilarity;
                }
                if (setupDissimilarity > *m_pMaxDissimilarity)
                {
                    *m_pMaxDissimilarity = setupDissimilarity;
                }
            }
        }

        return weight;
    }
    return 0.0;
}

void SingleDynamicElementStatusFilterExecutionUnit::SetMeasurementUpdateActive(bool measurementUpdateActive)
{
    m_measurementUpdateActive = measurementUpdateActive;
}

void SingleDynamicElementStatusFilterExecutionUnit::SetDynamicElement(DynamicElement* pDynamicElement)
{
    m_pDynamicElement = pDynamicElement;
}

void SingleDynamicElementStatusFilterExecutionUnit::UpdateRenderingUnit()
{
    m_renderingUnit.UpdateRenderingConfigurationFromParent();
    m_renderingUnit.UpdateCameraPositionFromParent();
    m_renderingUnit.UpdateStaticAndEstimatedProjectedPrimitivesFromParent();
}

void SingleDynamicElementStatusFilterExecutionUnit::SetViewingMatrix(const SbMatrix& viewingMatrix)
{
    m_viewingMatrix = viewingMatrix;
}

CRenderingUnit SingleDynamicElementStatusFilterExecutionUnit::GetRenderingUnit() const
{
    return m_renderingUnit;
}

void SingleDynamicElementStatusFilterExecutionUnit::SetRenderingUnit(const CRenderingUnit& renderingUnit)
{
    //m_renderingUnit = renderingUnit;
    m_renderingUnit.SetRobotControlInterface(renderingUnit.GetRobotControlInterface());
    m_renderingUnit.SetParametricLineRenderer(renderingUnit.GetParametricLineRenderer());
}

const CObservationModelInterface* SingleDynamicElementStatusFilterExecutionUnit::GetObservationModel() const
{
    return m_pObservationModel;
}

void SingleDynamicElementStatusFilterExecutionUnit::SetObservationModel(const CObservationModelInterface* pObservationModel)
{
    m_pObservationModel = pObservationModel;
}

const DynamicElementStatusUncertaintyModel* SingleDynamicElementStatusFilterExecutionUnit::GetDynamicElementStatusUncertaintyModel() const
{
    return m_pDynamicElementStatusUncertaintyModel;
}

void SingleDynamicElementStatusFilterExecutionUnit::SetDynamicElementStatusUncertaintyModel(const DynamicElementStatusUncertaintyModel* pDynamicElementStatusUncertaintyModel)
{
    m_pDynamicElementStatusUncertaintyModel = pDynamicElementStatusUncertaintyModel;
}

void SingleDynamicElementStatusFilterExecutionUnit::SetCameraImage(const CByteImage* pCameraImage)
{
    m_pCameraImage = pCameraImage;
}
