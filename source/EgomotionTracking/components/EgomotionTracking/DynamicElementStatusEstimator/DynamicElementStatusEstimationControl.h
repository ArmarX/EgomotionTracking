#pragma once

#include <EgomotionTracking/components/EVP/Foundation/Process/ThreadPool.h>

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/DynamicElement.h>
#include <EgomotionTracking/components/EgomotionTracking/DynamicElementStatusEstimator/StatusTrackingFilter/SingleDynamicElementStatusFilter.h>

#include <boost/graph/adjacency_list.hpp>

class DynamicElementStatusEstimationControl
{
public:

    DynamicElementStatusEstimationControl(int maxNumberOfDynamicElementParticles, EVP::Threading::CThreadPool* pThreadPool);
    ~DynamicElementStatusEstimationControl();

    void dispatch();

    void setLocationEstimate(const CLocationParticle& locationEstimate);
    void setLocationEstimateIsValid(const bool& locationEstimateIsValid);

    void setTrackingImageProcessor(CTrackingImageProcessorInterface* pTrackingImageProcessor);
    void setObservationModel(CObservationModelInterface* pObservationModel);
    void setRenderingUnit(CRenderingUnit* pRenderingUnit);
    void setParametricLineRenderer(CParametricLineRenderer* pParametricLineRenderer);
    void setDynamicElementStatusUncertaintyModel(DynamicElementStatusUncertaintyModel* pDynamicElementStatusUncertaintyModel);
    void setRobotControl(CRobotControlInterface* pRobotControl);

    void initializeDynamicElements(int numberOfThreads);
    void setupModelVisualization();

    void configureDynamicElementParticleReinitialization(double importanceWeightTreshold, int iterationCountTreshold);
    void configureDynamicElementNumberOfParticles(int minParticles, int maxParticles, bool useNumParticlesFunctionOfTransformationRange, int transformationRangeNumParticlesRatio, int numParticles);
    void setDynamicElementStateEstimateAcceptabilityTreshold(double importanceWeightTreshold);

    const std::vector<DynamicElement>& getDynamicElements() const;

    SingleDynamicElementStatusFilter* getSingleDynamicElementStatusFilter();

    void resetDynamicElements();

    void setCameraImage(const CByteImage* pCameraImage);

    void setStereoCalibration(const CStereoCalibration* pStereoCalibration);

    void setCameraFrameUpdateRequired(bool cameraFrameUpdateRequired);

    void setUseDissimilarityLikelihood(bool useDissimilarityLikelihood);

protected:

    CLocationParticle m_locationEstimate;
    bool m_locationEstimateIsValid;

    std::vector<DynamicElement> m_dynamicElements;

    SingleDynamicElementStatusFilter* m_pSingleDynamicElementStatusFilter;

    CTrackingImageProcessorInterface* m_pTrackingImageProcessor;
    CObservationModelInterface* m_pObservationModel;
    CRenderingUnit* m_pRenderingUnit;
    CParametricLineRenderer* m_pParametricLineRenderer;
    DynamicElementStatusUncertaintyModel* m_pDynamicElementStatusUncertaintyModel;
    CRobotControlInterface* m_pRobotControl;

    static bool compare_SbVec3fPosition(const SbVec3f& p1, const SbVec3f& p2)
    {
        if (p1[0] < p2[0])
        {
            return true;
        }
        else
        {
            if (p1[0] > p2[0])
            {
                return false;
            }
        }
        return p1[1] < p2[1];
    }

    void updateLinks();
    bool buildOcclusionGraph(boost::adjacency_list<>& occlusionGraph);

    PF_REAL m_dynamicElementStateEstimateAcceptabilityTreshold;
};

