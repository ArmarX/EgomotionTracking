/*
 * LocationEstimator.cpp
 *
 *  Created on: 11.03.2013
 *      Author: abyte
 */

#include "LocationEstimator.h"

CLocationEstimator::CLocationEstimator() :
    m_estimationIsValid(false),
    m_useBias(false),
    m_biasXR(0.0),
    m_biasYR(0.0),
    m_biasAlphaR(0.0)
{

}

CLocationEstimator::~CLocationEstimator()
{
}

bool CLocationEstimator::SetRelativeLocationBias(float biasXR, float biasYR, float biasAlphaR)
{
    m_biasXR = biasXR;
    m_biasYR = biasYR;
    m_biasAlphaR = biasAlphaR;
    return true;
}


bool CLocationEstimator::GetUseBias() const
{
    return m_useBias;
}

void CLocationEstimator::SetUseBias(bool useBias)
{
    m_useBias = useBias;
}

bool CLocationEstimator::EstimationIsValid() const
{
    return m_estimationIsValid;
}
