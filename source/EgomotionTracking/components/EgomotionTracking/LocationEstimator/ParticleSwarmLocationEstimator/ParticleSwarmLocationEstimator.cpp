#include "ParticleSwarmLocationEstimator.h"

CParticleSwarmLocationEstimator::CParticleSwarmLocationEstimator(int numberOfParticles, EVP::Threading::CThreadPool* pThreadPool) :
    m_pObservationModel(NULL), m_pNeckErrorUncertaintyModel(NULL), m_pRobotControlState(NULL), m_pRenderingUnit(NULL),
    m_inertiaFactor(1.0), m_cognitionFactor(1.0), m_socialFactor(1.0)
{
    m_particles.resize(numberOfParticles);
    m_globalBest.SetImportanceWeight(0.0);
    SetThreadPool(pThreadPool);
}

bool CParticleSwarmLocationEstimator::DispatchLocalisation()
{
    boost::mutex::scoped_lock scoped_lock(mutex);

    assert(m_pThreadPool);

    UpdateRenderingUnits();
    for (std::list<CPSWeightingExecutionUnit>::iterator currentExecutionUnit = m_executionUnits.begin(); currentExecutionUnit != m_executionUnits.end(); ++currentExecutionUnit)
    {
        currentExecutionUnit->SetObservationModel(m_pObservationModel);
        m_pThreadPool->DispatchExecutionUnit(&(*currentExecutionUnit), true);
    }
    m_pThreadPool->Synchronize();

    for (std::vector<CLocationSwarmParticle>::iterator particleIterator = m_particles.begin(); particleIterator != m_particles.end(); ++particleIterator)
    {
        particleIterator->UpdateGlobalBest();
    }

    for (std::vector<CLocationSwarmParticle>::iterator particleIterator = m_particles.begin(); particleIterator != m_particles.end(); ++particleIterator)
    {
        const float globalBestFactor = m_socialFactor * m_randomGenerator.GetUniformRandomNumber();
        const float localBestFactor = m_cognitionFactor * m_randomGenerator.GetUniformRandomNumber();
        particleIterator->UpdateState(m_inertiaFactor, localBestFactor, globalBestFactor, m_pNeckErrorUncertaintyModel);

    }
    CalculateScattering();

    m_pRenderingUnit->RenderParticle(m_globalBest, true);
    m_pObservationModel->CalculateImportanceWeight(m_globalBest, true);

    m_locationEstimate = m_globalBest;

    m_estimationIsValid = true;

    return true;
}

void CParticleSwarmLocationEstimator::InitializeParticlesInRegion(const CSimpleLocationRegion& searchRegion)
{
    for (std::vector<CLocationSwarmParticle>::iterator particleIterator = m_particles.begin(); particleIterator != m_particles.end(); ++particleIterator)
    {
        const float x = m_randomGenerator.GetUniformRandomNumber(searchRegion.GetMinX(), searchRegion.GetMaxX());
        const float y = m_randomGenerator.GetUniformRandomNumber(searchRegion.GetMinY(), searchRegion.GetMaxY());
        const float alpha = m_randomGenerator.GetUniformRandomNumber(searchRegion.GetMinAlpha(), searchRegion.GetMaxAlpha());
        particleIterator->SetX(x);
        particleIterator->SetY(y);
        particleIterator->SetAlpha(alpha);
        particleIterator->SetImportanceWeight(0.0);
        particleIterator->SetActive(true);
        particleIterator->ResetLocalBest();
        particleIterator->SetGlobalBest(&m_globalBest);

        const float estimatedParticleGapFactor = 1.0 / pow(configuration_psLocationEstimatorParticles, (1 / 3.0));
        const float vX = m_randomGenerator.GetGaussianRandomNumber(estimatedParticleGapFactor * (searchRegion.GetMaxX() - searchRegion.GetMinX()));
        const float vY = m_randomGenerator.GetGaussianRandomNumber(estimatedParticleGapFactor * (searchRegion.GetMaxY() - searchRegion.GetMinY()));
        const float vAlpha = m_randomGenerator.GetGaussianRandomNumber(estimatedParticleGapFactor * (searchRegion.GetMaxAlpha() - searchRegion.GetMinAlpha()));

        particleIterator->SetVelocityX(vX);
        particleIterator->SetVelocityY(vY);
        particleIterator->SetVelocityAlpha(vAlpha);

        if (m_pNeckErrorUncertaintyModel != NULL)
        {
            m_pNeckErrorUncertaintyModel->AddNeckErrorUncertaintyToParticle(*particleIterator, &m_randomGenerator, CNeckErrorUncertaintyModel::eAbsoluteUniformNeckUncertainty);

#if 0 //XXX
            const float vRoll = m_randomGenerator.GetGaussianRandomNumber(estimatedParticleGapFactor * (m_pNeckErrorUncertaintyModel->GetMaxRollError() - m_pNeckErrorUncertaintyModel->GetMinRollError()));
            const float vPitch = m_randomGenerator.GetGaussianRandomNumber(estimatedParticleGapFactor * (m_pNeckErrorUncertaintyModel->GetMaxPitchError() - m_pNeckErrorUncertaintyModel->GetMinPitchError()));
            const float vYaw = m_randomGenerator.GetGaussianRandomNumber(estimatedParticleGapFactor * (m_pNeckErrorUncertaintyModel->GetMaxYawError() - m_pNeckErrorUncertaintyModel->GetMinYawError()));
            particleIterator->SetVelocityNeckRoll(vRoll);
            particleIterator->SetVelocityNeckPitch(vPitch);
            particleIterator->SetVelocityNeckYaw(vYaw);
#else
            particleIterator->SetVelocityNeckRoll(0.0);
            particleIterator->SetVelocityNeckPitch(0.0);
            particleIterator->SetVelocityNeckYaw(0.0);
#endif
        }
        else
        {
            particleIterator->SetVelocityNeckRoll(0.0);
            particleIterator->SetVelocityNeckPitch(0.0);
            particleIterator->SetVelocityNeckYaw(0.0);
        }
    }
    m_globalBest.SetImportanceWeight(0.0);

}

void CParticleSwarmLocationEstimator::InitializeParticlesInRegion(const CPlaneScatteringDirections& searchEllipse, float alphaMin, float alphaMax, float enlargementFactor)
{
    const float majorRadius = searchEllipse.GetSigma1() * configuration_scatteringEllipseSigmaFactor * enlargementFactor;
    const float minorRadius = searchEllipse.GetSigma2() * configuration_scatteringEllipseSigmaFactor * enlargementFactor;

    for (std::vector<CLocationSwarmParticle>::iterator particleIterator = m_particles.begin(); particleIterator != m_particles.end(); ++particleIterator)
    {
        /* uniform distributed random points in ellipse (with polar coordinates, see http://www.mathworks.de/matlabcentral/newsreader/view_thread/83592 )*/
        const float r = sqrt(m_randomGenerator.GetUniformRandomNumber(0.0, 1.0));
        const float theta = m_randomGenerator.GetUniformRandomNumber(0.0, 2 * M_PI);


        const float xaligned = majorRadius * r * cos(theta);
        const float yaligned = minorRadius * r * sin(theta);

        const float orientationAngle = atan2(searchEllipse.GetDirection1().y, searchEllipse.GetDirection1().x);

        const float xrotated = xaligned * cos(orientationAngle) - yaligned * sin(orientationAngle);
        const float yrotated = xaligned * sin(orientationAngle) + yaligned * cos(orientationAngle);

        const float x = xrotated + searchEllipse.GetMean().x;
        const float y = yrotated + searchEllipse.GetMean().y;
        const float alpha = m_randomGenerator.GetUniformRandomNumber(alphaMin, alphaMax);

        particleIterator->SetX(x);
        particleIterator->SetY(y);
        particleIterator->SetAlpha(alpha);
        particleIterator->SetImportanceWeight(0.0);
        particleIterator->SetActive(true);
        particleIterator->ResetLocalBest();
        particleIterator->SetGlobalBest(&m_globalBest);

        const float estimatedParticleGapFactor = 1.0 / pow(configuration_psLocationEstimatorParticles, (1 / 3.0));
        const float vXAligned = m_randomGenerator.GetGaussianRandomNumber(estimatedParticleGapFactor * majorRadius);
        const float vYAligned = m_randomGenerator.GetGaussianRandomNumber(estimatedParticleGapFactor * minorRadius);

        const float vX = vXAligned * cos(orientationAngle) - vYAligned * sin(orientationAngle);
        const float vY = vXAligned * sin(orientationAngle) + vYAligned * cos(orientationAngle);

        const float vAlpha = m_randomGenerator.GetGaussianRandomNumber(estimatedParticleGapFactor * (alphaMax - alphaMin));

        particleIterator->SetVelocityX(vX);
        particleIterator->SetVelocityY(vY);
        particleIterator->SetVelocityAlpha(vAlpha);

        //XXX
        if (m_pNeckErrorUncertaintyModel != NULL)
        {
            m_pNeckErrorUncertaintyModel->AddNeckErrorUncertaintyToParticle(*particleIterator, &m_randomGenerator, CNeckErrorUncertaintyModel::eAbsoluteUniformNeckUncertainty);
#if 0 //XXX
            const float vRoll = m_randomGenerator.GetGaussianRandomNumber(estimatedParticleGapFactor * (m_pNeckErrorUncertaintyModel->GetMaxRollError() - m_pNeckErrorUncertaintyModel->GetMinRollError()));
            const float vPitch = m_randomGenerator.GetGaussianRandomNumber(estimatedParticleGapFactor * (m_pNeckErrorUncertaintyModel->GetMaxPitchError() - m_pNeckErrorUncertaintyModel->GetMinPitchError()));
            const float vYaw = m_randomGenerator.GetGaussianRandomNumber(estimatedParticleGapFactor * (m_pNeckErrorUncertaintyModel->GetMaxYawError() - m_pNeckErrorUncertaintyModel->GetMinYawError()));
            particleIterator->SetVelocityNeckRoll(vRoll);
            particleIterator->SetVelocityNeckPitch(vPitch);
            particleIterator->SetVelocityNeckYaw(vYaw);
#else
            particleIterator->SetVelocityNeckRoll(0.0);
            particleIterator->SetVelocityNeckPitch(0.0);
            particleIterator->SetVelocityNeckYaw(0.0);
#endif
        }
        else
        {
            particleIterator->SetVelocityNeckRoll(0.0);
            particleIterator->SetVelocityNeckPitch(0.0);
            particleIterator->SetVelocityNeckYaw(0.0);
        }
    }
    m_globalBest.SetImportanceWeight(0.0);
}

void CParticleSwarmLocationEstimator::SetObservationModel(const CObservationModelInterface* pObservationModel)
{
    m_pObservationModel = pObservationModel;
}

void CParticleSwarmLocationEstimator::SetNeckErrorUncertaintyModel(const CNeckErrorUncertaintyModel* pNeckErrorUncertaintyModel)
{
    m_pNeckErrorUncertaintyModel = pNeckErrorUncertaintyModel;
}

void CParticleSwarmLocationEstimator::SetRenderingUnit(CRenderingUnit* pRenderingUnit)
{
    m_pRenderingUnit = pRenderingUnit;
    for (std::list<CPSWeightingExecutionUnit>::iterator currentExecutionUnit = m_executionUnits.begin(); currentExecutionUnit != m_executionUnits.end(); ++currentExecutionUnit)
    {
        currentExecutionUnit->SetRenderingUnit(*m_pRenderingUnit);
    }
}

void CParticleSwarmLocationEstimator::SetRobotControlState(const CRobotControlState* pRobotControlState)
{
    m_pRobotControlState = pRobotControlState;
}

void CParticleSwarmLocationEstimator::SetThreadPool(EVP::Threading::CThreadPool* pThreadPool)
{
    if (pThreadPool && pThreadPool->GetTotalThreads() > 0)
    {
        m_pThreadPool = pThreadPool;

        m_executionUnits.resize(pThreadPool->GetTotalThreads());

        const int minParticlesPerUnit = m_particles.size() / m_executionUnits.size();
        const int smallUnits = m_executionUnits.size() * minParticlesPerUnit + m_executionUnits.size() - m_particles.size();

        int currentUnitIndex = 0;

        std::vector<CLocationSwarmParticle>::iterator swarmParticleIterator = m_particles.begin();

        for (std::list<CPSWeightingExecutionUnit>::iterator currentExecutionUnit = m_executionUnits.begin(); currentExecutionUnit != m_executionUnits.end(); ++currentExecutionUnit, ++currentUnitIndex)
        {
            int currentNumberOfParticles = minParticlesPerUnit;
            if (currentUnitIndex >= smallUnits)
            {
                ++currentNumberOfParticles;
            }
            currentExecutionUnit->SetParticles(swarmParticleIterator, swarmParticleIterator + currentNumberOfParticles);
            swarmParticleIterator += currentNumberOfParticles;

            if (m_pRenderingUnit != NULL)
            {
                currentExecutionUnit->SetRenderingUnit(*m_pRenderingUnit);
            }
        }
    }
}

const std::vector<CLocationSwarmParticle>& CParticleSwarmLocationEstimator::GetParticles()
{
    return m_particles;
}

std::vector<CLocationParticle> CParticleSwarmLocationEstimator::GetBestNParticles(unsigned int N)
{
    boost::mutex::scoped_lock scoped_lock(mutex);

    std::vector<CLocationParticle> bestParticles;
    std::copy(m_particles.begin(), m_particles.end(), std::back_inserter(bestParticles));
    std::sort(bestParticles.rbegin(), bestParticles.rend());
    if (N < bestParticles.size())
    {
        bestParticles.resize(N);
    }
    return bestParticles;
}

void CParticleSwarmLocationEstimator::SetParameters(float inertiaFactor, float cognitionFactor, float socialFactor)
{
    m_inertiaFactor = inertiaFactor;
    m_cognitionFactor = cognitionFactor;
    m_socialFactor = socialFactor;
}

const CPlaneScatteringDirections& CParticleSwarmLocationEstimator::GetPlaneScatteringDirections()
{
    return m_planeScatteringDirections;
}

PF_REAL CParticleSwarmLocationEstimator::GetEstimationWeight() const
{
    return m_globalBest.GetImportanceWeight();
}

bool CParticleSwarmLocationEstimator::GetNeckRPYErrorEstimate(float& neckRollError, float& neckPitchError, float& neckYawError) const
{
    neckRollError = m_globalBest.GetNeckRollError();
    neckPitchError = m_globalBest.GetNeckPitchError();
    neckYawError = m_globalBest.GetNeckYawError();
    return true;
}

void CParticleSwarmLocationEstimator::CalculateScattering()
{
    /* calculate mean */
    Vec3d mean;
    mean.x = 0.0;
    mean.y = 0.0;
    mean.z = 0.0;
    double totalWeight = 0.0;
    for (std::vector<CLocationSwarmParticle>::iterator particleIterator = m_particles.begin(); particleIterator != m_particles.end(); ++particleIterator)
    {
        const double w = particleIterator->GetImportanceWeight();
        mean.x += w * particleIterator->GetX();
        mean.y += w * particleIterator->GetY();
        mean.z += w * particleIterator->GetAlpha();
        totalWeight += w;
    }
    mean.x /= totalWeight;
    mean.y /= totalWeight;
    mean.z /= totalWeight;

    /* calculate covariance matrix */
    Mat3d covarianceMatrix = Math3d::zero_mat;
    for (std::vector<CLocationSwarmParticle>::iterator particleIterator = m_particles.begin(); particleIterator != m_particles.end(); ++particleIterator)
    {
        Vec3d deviation;
        deviation.x = particleIterator->GetX() - mean.x;
        deviation.y = particleIterator->GetY() - mean.y;
        deviation.z = particleIterator->GetAlpha() - mean.z;

        const double w = particleIterator->GetImportanceWeight();
        Mat3d tmp;
        MathIVT::DyadicTensorProcut(deviation, tmp);
        Math3d::MulMatScalar(tmp, w, tmp);
        Math3d::AddToMat(covarianceMatrix, tmp);
    }
    Math3d::MulMatScalar(covarianceMatrix, (1.0 / totalWeight), covarianceMatrix);

    m_planeScatteringDirections.SetMean(mean);
    m_planeScatteringDirections.CalculateDirections(covarianceMatrix);
}

void CParticleSwarmLocationEstimator::UpdateRenderingUnits()
{
    m_pRenderingUnit->UpdateRenderingConfigurationFromParent();
    m_pRenderingUnit->UpdateHeadConfiguration();
    if (m_executionUnits.size() > 0)
    {
        for (std::list<CPSWeightingExecutionUnit>::iterator currentExecutionUnit = m_executionUnits.begin(); currentExecutionUnit != m_executionUnits.end(); ++currentExecutionUnit)
        {
            currentExecutionUnit->UpdateRenderingUnit();
        }
    }
}
