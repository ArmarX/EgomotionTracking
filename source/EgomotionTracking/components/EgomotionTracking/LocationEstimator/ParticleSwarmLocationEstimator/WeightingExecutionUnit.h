#pragma once

#include <EgomotionTracking/components/EVP/Foundation/Process/ExecutionUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationSwarmParticle.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/ObservationModel/ObservationModelInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/NeckErrorUncertaintyModel/NeckErrorUncertaintyModel.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/RenderingUnit/RenderingUnit.h>

#include <vector>
class CPSWeightingExecutionUnit : public EVP::Threading::CExecutionUnit
{
public:
    CPSWeightingExecutionUnit();

    const CObservationModelInterface* GetObservationModel() const;
    void SetObservationModel(const CObservationModelInterface* pObservationModel);
    void SetRenderingUnit(const CRenderingUnit& renderingUnit);
    void SetParticles(const std::vector<CLocationSwarmParticle>::iterator& particlesBegin,
                      const std::vector<CLocationSwarmParticle>::iterator& particlesEnd);
    void UpdateRenderingUnit();

protected:
    bool Execute() override;

    const CObservationModelInterface* m_pObservationModel;
    CRenderingUnit m_renderingUnit;
    std::vector<CLocationSwarmParticle>::iterator m_particlesBegin;
    std::vector<CLocationSwarmParticle>::iterator m_particlesEnd;
};

