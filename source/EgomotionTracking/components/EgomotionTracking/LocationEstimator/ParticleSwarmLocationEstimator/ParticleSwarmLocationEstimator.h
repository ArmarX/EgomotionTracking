#pragma once

#include "../LocationEstimator.h"
#include <EgomotionTracking/components/EgomotionTracking/Misc/RandomGenerator/ParkMillerRandomGenerator.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/ObservationModel/ObservationModelInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/NeckErrorUncertaintyModel/NeckErrorUncertaintyModel.h>
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotControlInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/RenderingUnit/RenderingUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/SimpleLocationRegion.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationSwarmParticle.h>
#include "WeightingExecutionUnit.h"
#include "../TrackingFilter/EgomotionTrackingPF/PlaneScatteringDirections.h"
#include <boost/thread/mutex.hpp>

#include <EgomotionTracking/components/EVP/Foundation/Process/ThreadPool.h>

class CParticleSwarmLocationEstimator : public CLocationEstimator
{
public:
    CParticleSwarmLocationEstimator(int numberOfParticles, EVP::Threading::CThreadPool* pThreadPool = NULL);

    bool DispatchLocalisation() override;

    void InitializeParticlesInRegion(const CSimpleLocationRegion& searchRegion);
    void InitializeParticlesInRegion(const CPlaneScatteringDirections& searchEllipse, float alphaMin, float alphaMax, float enlargementFactor = 1.0);

    void SetObservationModel(const CObservationModelInterface* pObservationModel);
    void SetNeckErrorUncertaintyModel(const CNeckErrorUncertaintyModel* pNeckErrorUncertaintyModel);
    void SetRenderingUnit(CRenderingUnit* pRenderingUnit);
    void SetRobotControlState(const CRobotControlState* pRobotControlState);
    void SetThreadPool(EVP::Threading::CThreadPool* pThreadPool);

    const std::vector<CLocationSwarmParticle>& GetParticles();
    std::vector<CLocationParticle> GetBestNParticles(unsigned int N);

    void SetParameters(float inertiaFactor, float cognitionFactor, float socialFactor);

    const CPlaneScatteringDirections& GetPlaneScatteringDirections();
    PF_REAL GetEstimationWeight() const;

    bool GetNeckRPYErrorEstimate(float& neckRollError, float& neckPitchError, float& neckYawError) const;
protected:
    void CalculateScattering();
    void UpdateRenderingUnits();

    EVP::Threading::CThreadPool* m_pThreadPool;
    const CObservationModelInterface* m_pObservationModel;
    const CNeckErrorUncertaintyModel* m_pNeckErrorUncertaintyModel;
    const CRobotControlState* m_pRobotControlState;
    CRenderingUnit* m_pRenderingUnit;
    CParkMillerRandomGenerator m_randomGenerator;

    std::vector<CLocationSwarmParticle> m_particles;
    CLocationParticle m_globalBest;

    std::list<CPSWeightingExecutionUnit> m_executionUnits;

    float m_inertiaFactor;
    float m_cognitionFactor;
    float m_socialFactor;

    CPlaneScatteringDirections m_planeScatteringDirections;

    boost::mutex mutex;
};

