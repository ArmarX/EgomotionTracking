#include "WeightingExecutionUnit.h"

CPSWeightingExecutionUnit::CPSWeightingExecutionUnit() :
    m_pObservationModel(NULL)
{
}

void CPSWeightingExecutionUnit::SetParticles(const std::vector<CLocationSwarmParticle>::iterator& particlesBegin, const std::vector<CLocationSwarmParticle>::iterator& particlesEnd)
{
    m_particlesBegin = particlesBegin;
    m_particlesEnd = particlesEnd;
}

void CPSWeightingExecutionUnit::UpdateRenderingUnit()
{
    m_renderingUnit.UpdateRenderingConfigurationFromParent();
    m_renderingUnit.UpdateHeadConfiguration();
}

bool CPSWeightingExecutionUnit::Execute()
{
    assert(m_pObservationModel);

    std::vector<CLocationSwarmParticle>::iterator currentParticle = m_particlesBegin;

    for (; currentParticle < m_particlesEnd; ++currentParticle)
    {
        m_renderingUnit.RenderParticle(*currentParticle, m_pObservationModel->GetNeedRenderedJunctionPoints());
        const PF_REAL currentWeight = m_pObservationModel->CalculateImportanceWeight(*currentParticle, false);
        currentParticle->SetImportanceWeight(currentWeight);
        currentParticle->UpdateLocalBest();
    }
    return true;
}


const CObservationModelInterface* CPSWeightingExecutionUnit::GetObservationModel() const
{
    return m_pObservationModel;
}

void CPSWeightingExecutionUnit::SetObservationModel(const CObservationModelInterface* pObservationModel)
{
    m_pObservationModel = pObservationModel;
}

void CPSWeightingExecutionUnit::SetRenderingUnit(const CRenderingUnit& renderingUnit)
{
    m_renderingUnit = renderingUnit;
}
