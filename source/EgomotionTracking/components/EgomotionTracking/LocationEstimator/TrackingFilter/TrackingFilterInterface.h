#pragma once

#include <EgomotionTracking/components/EgomotionTracking/Models/LocationStatePredictor/LocationStatePredictorInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/ObservationModel/ObservationModelInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/NeckErrorUncertaintyModel/NeckErrorUncertaintyModel.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/RenderingUnit/RenderingUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/RobotControlState.h>
#include <EgomotionTracking/components/EgomotionTracking/LocationEstimator/LocationEstimator.h>

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/HeadConfiguration.h>

class CTrackingFilterInterface : public CLocationEstimator
{
public:
    CTrackingFilterInterface();

    virtual void InitializeFilter(const CLocationParticle& initializationState) = 0;
    virtual PF_REAL Filter(PF_REAL sigmaFactor) = 0;

    virtual void SetLocationStatePredictor(const CLocationStatePredictorInterface* pLocationStatePredictor);
    virtual void SetNeckErrorUncertaintyModel(const CNeckErrorUncertaintyModel* pNeckErrorUncertaintyModel);
    virtual void SetObservationModel(CObservationModelInterface* pObservationModel);
    virtual void SetRenderingUnit(CRenderingUnit* pRenderingUnit);
    virtual void SetRobotControl(const CRobotControlInterface* pRobotControl);
protected:

    const CLocationStatePredictorInterface* m_pLocationStatePredictor;
    const CNeckErrorUncertaintyModel* m_pNeckErrorUncertaintyModel;
    CObservationModelInterface* m_pObservationModel;
    const CRobotControlInterface* m_pRobotControl;
    CRenderingUnit* m_pRenderingUnit;
};

