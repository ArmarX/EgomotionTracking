#include "TrackingFilterInterface.h"

CTrackingFilterInterface::CTrackingFilterInterface() :
    m_pLocationStatePredictor(NULL),
    m_pNeckErrorUncertaintyModel(NULL),
    m_pObservationModel(NULL),
    m_pRobotControl(NULL),
    m_pRenderingUnit(NULL)
{
}

void CTrackingFilterInterface::SetRenderingUnit(CRenderingUnit* pRenderingUnit)
{
    m_pRenderingUnit = pRenderingUnit;
}

void CTrackingFilterInterface::SetObservationModel(CObservationModelInterface* pObservationModel)
{
    m_pObservationModel = pObservationModel;
}


void CTrackingFilterInterface::SetLocationStatePredictor(const CLocationStatePredictorInterface* pLocationStatePredictor)
{
    m_pLocationStatePredictor = pLocationStatePredictor;
}

void CTrackingFilterInterface::SetNeckErrorUncertaintyModel(const CNeckErrorUncertaintyModel* pNeckErrorUncertaintyModel)
{
    m_pNeckErrorUncertaintyModel = pNeckErrorUncertaintyModel;
}

void CTrackingFilterInterface::SetRobotControl(const CRobotControlInterface* pRobotControl)
{
    m_pRobotControl = pRobotControl;
}

