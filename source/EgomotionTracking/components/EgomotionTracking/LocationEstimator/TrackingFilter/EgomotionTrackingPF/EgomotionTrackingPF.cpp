
#include "EgomotionTrackingPF.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>

#include <EgomotionTracking/components/EgomotionTracking/Helpers/MathIVT.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>

#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/ParametricLineRenderer.h>

#include <EgomotionTracking/components/EgomotionTracking/Models/ObservationModel/JunctionPointLineModelObservationModel.h>

#include <numeric>

#include <boost/bind.hpp>

#include <eigen3/Eigen/Eigenvalues>
#include <eigen3/Eigen/SVD>

#include <Math/LinearAlgebra.h>
#include <Math/DoubleMatrix.h>

using namespace std;

CEgomotionTrackingPF::CEgomotionTrackingPF(int numberOfParticles, EVP::Threading::CThreadPool* pThreadPool)
    : CTrackingFilterInterface(),
      CParallelParticleFilterFramework(numberOfParticles, PARTICLE_FILTER_DIMESION, pThreadPool->GetTotalThreads(), pThreadPool),
      m_annealingFactor(0.5), m_measurementUpdateActive(true),
      m_scatteringReduction(false),
      m_pUncertaintyFactorCalculatorLocationState(NULL),
      m_pUncertaintyFactorCalculatorNeckError(NULL), m_useMahalanobisDistanceBasedScatteringReduction(false),
      m_counterTreshold(0), m_counter(0), m_totalWeightPercentageTreshold(0.0)
{
    InitializeExecutionUnits();
}

CEgomotionTrackingPF::~CEgomotionTrackingPF()
{

}

void CEgomotionTrackingPF::InitializeFilter(const CLocationParticle& initializationState)
{
    CRobotControlState restingRobot;
    m_particles.clear();
    if (!m_pLocationStatePredictor)
    {
        COUT_ERROR << "m_pLocationStatePredictor == NULL" << std::endl;
        exit(0);
    }
    for (int i = 0; i < m_nParticles; ++i)
    {
        CLocationParticle newParticle(m_pLocationStatePredictor->PredictNewRobotLocation(initializationState, restingRobot, &m_randomGenerator));

        if (i < m_nActiveParticles)
        {
            newParticle.SetActive(true);
        }
        else
        {
            newParticle.SetActive(false);
        }

        m_particles.push_back(newParticle);
    }
    SetParticlesToVector(m_particles);
    COUT_INFO << "Initialized " << m_nActiveParticles << " Particles" << std::endl;

    m_particlesAreResampled = true;

    m_resampleBasedOnMahalanobisDistance = false;
    m_counter = 0;
}

void CEgomotionTrackingPF::InitializeFilter(const CLocationParticle& initializationState, float neckRollError, float neckPitchError, float neckYawError)
{
    InitializeFilter(initializationState);
    if (m_particles.size() > 0)
    {
        for (vector<CLocationParticle>::iterator particleIt = m_particles.begin(); particleIt != m_particles.end(); ++particleIt)
        {
            CLocationParticle& curParticle = *particleIt;
            curParticle.SetNeckRollError(neckRollError);
            curParticle.SetNeckPitchError(neckPitchError);
            curParticle.SetNeckYawError(neckYawError);
        }
    }
    SetParticlesToVector(m_particles);
    m_particlesAreResampled = true;

    m_resampleBasedOnMahalanobisDistance = false;
    m_counter = 0;
}

void CEgomotionTrackingPF::InitializeFilter(const vector<CLocationParticle>& initialisationParticles)
{
    m_particles.clear();
    copy(initialisationParticles.begin(), initialisationParticles.end(), back_inserter(m_particles));
    SetParticlesToVector(m_particles);
    COUT_INFO << "Initialized " << m_nActiveParticles << " Particles" << std::endl;
    m_particlesAreResampled = true;

    m_resampleBasedOnMahalanobisDistance = false;
    m_counter = 0;
}

PF_REAL CEgomotionTrackingPF::Filter(PF_REAL sigmaFactor)
{
    /* update execution units */
    if (!(m_pLocationStatePredictor && m_pObservationModel &&
          m_pRobotControl && m_pRenderingUnit))
    {
        COUT_ERROR << "!(m_pLocationStatePredictor && m_pObservationModel && m_pRobotControl && m_pRenderingUnit)" << std::endl;
        m_estimationIsValid = false;
        return -1;
    }
    if (m_particles.size() == 0)
    {
        COUT_ERROR << "Position not initialized!" << std::endl;
        m_estimationIsValid = false;
        return -1;
    }

    if (m_useAnnealing)
    {
        m_pObservationModel->SetSensibility(CalculateObservationModelSensitivity(m_currentAnnealingLevel));
    }
    else
    {
        m_pObservationModel->SetSensibility(1.0);
    }
    m_pLocationStatePredictor->SetScatteringFactor(CalculateLocationScatteringFactor());
    m_pNeckErrorUncertaintyModel->SetScatteringFactor(CalculateNeckErrorScatteringFactor());
    UpdateRenderingUnits();

    for (vector<CSamplingAndWeightingExecutionUnit*>::iterator euIterator = m_executionUnits.begin(); euIterator != m_executionUnits.end(); ++ euIterator)
    {
        CEgomotionTrackingPFExecutionUnit* trackingEU = static_cast<CEgomotionTrackingPFExecutionUnit*>(*euIterator);
        trackingEU->SetPredictor(m_pLocationStatePredictor);
        trackingEU->SetObservationModel(m_pObservationModel);
        trackingEU->SetRobotControlState(&m_pRobotControl->GetRobotControlState());
        trackingEU->SetNeckErrorUncertaintyModel(m_pNeckErrorUncertaintyModel);

        /* MAHALANOBIS */

        trackingEU->setResampleBasedOnMahalanobisDistance(m_resampleBasedOnMahalanobisDistance);
        trackingEU->setMahalanobisDistanceBasedScatteringReductionResamplingMode(m_mahalanobisDistanceBasedScatteringReductionResamplingMode);
        if (m_resampleBasedOnMahalanobisDistance &&
            !(m_mahalanobisDistanceBasedScatteringReductionResamplingMode == CEgomotionTrackingPFExecutionUnit::eStandardResampling))
        {
            trackingEU->setEigenvaluesAndEigenvectors(m_resampableParticlesEigenvalues, m_resampableParticlesEigenvectors);
            trackingEU->setWeightedMean(m_weightedMeanSixDimensions);
            trackingEU->setResampableParticlesWeightedCovarianceMatrix(m_resampableParticlesWeightedCovarianceMatrix);
            trackingEU->setResampableParticlesMaximumMahalanobisDistance(m_resampableParticlesMaximumMahalanobisDistance);
        }
    }

    PF_REAL result[PARTICLE_FILTER_DIMESION];
    const PF_REAL resultWeight =  ParticleFilter(result, sigmaFactor);

    CalculateWeightedMeanAndScatteringMatrix();
    m_planeScatteringDirections.CalculateDirections(m_weightedScatteringMatrix);
    m_planeScatteringDirections.SetMean(m_weightedMean);

    if (!(m_useAnnealing && m_annealingConservativeUpdate) || m_currentAnnealingLevel == 0)
    {
        if (m_estimationMode == eBestEstimate)
        {
            m_locationEstimate.SetX(result[0]);
            m_locationEstimate.SetY(result[1]);
            m_locationEstimate.SetAlpha(result[2]);
        }
        else if (m_estimationMode == eMeanEstimate)
        {
            m_locationEstimate.SetX(result[0]);
            m_locationEstimate.SetY(result[1]);

            m_neckRollErrorEstimate = result[3];
            m_neckPitchErrorEstimate = result[4];
            m_neckYawErrorEstimate = result[5];

            /* the angle is a circular quantity! we have to take care of that... */
            list<float> radians;
            list<float> weights;
            for (int i = 0; i < m_nActiveParticles; ++i)
            {
                radians.push_back(m_ppS[i][2]);
                weights.push_back(m_pPiNormalized[i]);
            }
            m_locationEstimate.SetAlpha(Conversions::CalculateWeightedMeanOfRadians(radians, weights));
        }
        else if (m_estimationMode == eThresholdedMeanEstimate)
        {
            m_locationEstimate.SetX(result[0]);
            m_locationEstimate.SetY(result[1]);
            //XXX only for evaluation
            // wrong when robot turns 180°
            m_locationEstimate.SetAlpha(result[2]);
        }
    }

    if (m_useMahalanobisDistanceBasedScatteringReduction)
    {
        if (m_pRobotControl->GetRobotControlState().IsMoving())
        {
            m_resampleBasedOnMahalanobisDistance = false;
            m_counter = 0;
        }
        else
        {
            if (m_counter < m_counterTreshold)
            {
                m_counter++;
            }
            else
            {
                calculateSixDimensionsWeightedMeanAndCovarianceMatrix();
                if (m_weightedCovarianceMatrixSixDimensions.determinant() > PF_REAL_EPSILON)
                {
                    m_resampleBasedOnMahalanobisDistance = true;
                    calculateResampableParticles();
                    if (!(m_mahalanobisDistanceBasedScatteringReductionResamplingMode == CEgomotionTrackingPFExecutionUnit::eStandardResampling))
                    {
                        calculateResampableParticlesEigenvaluesAndEigenvectors();
                    }
                }
                else
                {
                    m_resampleBasedOnMahalanobisDistance = false;
                }
            }
        }
    }

    CLocationParticle location(m_locationEstimate);
    location.SetNeckPitchError(m_neckPitchErrorEstimate);
    location.SetNeckRollError(m_neckRollErrorEstimate);
    location.SetNeckYawError(m_neckYawErrorEstimate);

    m_pRenderingUnit->RenderParticle(location, false);

    /* Draw transformation bounding boxes. */
    //    const std::vector<CFaceTriangle>& transformationBoundingBoxesScreenFaceTriangles = m_pRenderingUnit->GetParametricLineRenderer().GetTransformationBoundingBoxScreenFaceTriangleList();
    //    ((CJunctionPointLineModelObservationModel*)m_pObservationModel)->DrawTriangles(transformationBoundingBoxesScreenFaceTriangles, false);

    m_estimationIsValid = true;
    return resultWeight;
}

void CEgomotionTrackingPF::GetParticles(vector<CLocationParticle>& particles)
{
    GetParticlesFromVector(particles);
}

const vector<CLocationParticle>& CEgomotionTrackingPF::GetParticles()
{
    GetParticles(m_particles);
    return m_particles;
}

void CEgomotionTrackingPF::InitializeExecutionUnits()
{
    for (int i = 0; i < m_nThreads; ++i)
    {
        m_executionUnits.push_back(new CEgomotionTrackingPFExecutionUnit());
    }
}

int CEgomotionTrackingPF::GetNumberOfActiveParticles()
{
    return CParallelParticleFilterFramework::GetNumberOfActiveParticles();
}

void CEgomotionTrackingPF::SetNumberOfActiveParticles(int nActiveParticles)
{
    CParallelParticleFilterFramework::SetNumberOfActiveParticles(nActiveParticles);
}

void CEgomotionTrackingPF::SetResamplingThreshold(bool enabled, PF_REAL relativeNEffThreshold)
{
    SetUseResamplingThreshold(enabled);
    SetRelativeNEffResamplingThreshold(relativeNEffThreshold);
}

void CEgomotionTrackingPF::SetSwarmOptimization(bool enabled, PF_REAL velocityFactorLocalBest, PF_REAL velocityFactorGlobalBest, PF_REAL localBestAgingFactor, PF_REAL bestWeightThreshold)
{
    SetUseSwarmOptimization(enabled);
    SetSwarmVelocityLocalBestFactor(velocityFactorLocalBest);
    SetSwarmVelocityGlobalBestFactor(velocityFactorGlobalBest);
    SetSwarmLocalBestAgingFactor(localBestAgingFactor);
    SetSwarmOptimizationBestWeightThreshold(bestWeightThreshold);
}

void CEgomotionTrackingPF::SetAnnealing(bool enabled, unsigned int numberOfAnnealingLevels, PF_REAL annealingFactor, PF_REAL bestWeightThreshold, bool conservativeUpdate)
{
    SetUseAnnealing(enabled);
    SetNumberOfAnnealingLevels(numberOfAnnealingLevels);
    SetAnnealingBestWeightThreshold(bestWeightThreshold);
    m_annealingFactor = annealingFactor;
    m_annealingConservativeUpdate = conservativeUpdate;
}

void CEgomotionTrackingPF::SetThresholdedMeanFactor(PF_REAL meanFactor)
{
    CParallelParticleFilterFramework::SetThresholdedMeanFactor(meanFactor);
}

void CEgomotionTrackingPF::SetEstimationMode(const CParallelParticleFilterFramework::CEstimationMode& mode)
{
    CParallelParticleFilterFramework::SetEstimationMode(mode);
}

void CEgomotionTrackingPF::SetRenderingUnit(CRenderingUnit* pRenderingUnit)
{
    CTrackingFilterInterface::SetRenderingUnit(pRenderingUnit);
    for (vector<CSamplingAndWeightingExecutionUnit*>::iterator euIterator = m_executionUnits.begin(); euIterator != m_executionUnits.end(); ++ euIterator)
    {
        CEgomotionTrackingPFExecutionUnit* trackingEU = static_cast<CEgomotionTrackingPFExecutionUnit*>(*euIterator);
        trackingEU->SetRenderingUnit(*m_pRenderingUnit);
    }
}

void CEgomotionTrackingPF::SetMeasurementUpdateActive(bool measurementUpdateActive)
{
    m_measurementUpdateActive = measurementUpdateActive;
    for (vector<CSamplingAndWeightingExecutionUnit*>::iterator euIterator = m_executionUnits.begin(); euIterator != m_executionUnits.end(); ++ euIterator)
    {
        CEgomotionTrackingPFExecutionUnit* trackingEU = static_cast<CEgomotionTrackingPFExecutionUnit*>(*euIterator);
        trackingEU->SetMeasurementUpdateActive(m_measurementUpdateActive);
    }
}

bool CEgomotionTrackingPF::GetMeasurementUpdateActive() const
{
    return m_measurementUpdateActive;
}

unsigned int CEgomotionTrackingPF::GetCurrentAnnealingLevel()
{
    return CParallelParticleFilterFramework::GetCurrentAnnealingLevel();
}

const Vec3d& CEgomotionTrackingPF::GetWeightedMean()
{
    return m_weightedMean;
}

const Mat3d& CEgomotionTrackingPF::GetWeightedScatteringMatrix()
{
    return m_weightedScatteringMatrix;
}

const CPlaneScatteringDirections& CEgomotionTrackingPF::GetPlaneScatteringDirections()
{
    return m_planeScatteringDirections;
}

void CEgomotionTrackingPF::CalculateBoundingRect(CLocationState& minState, CLocationState& maxState) const
{
    PF_REAL min[PARTICLE_FILTER_DIMESION];
    PF_REAL max[PARTICLE_FILTER_DIMESION];
    CParallelParticleFilterFramework::CalculateBoundingRect(min, max);

    minState.Set(min);
    maxState.Set(max);
}

void CEgomotionTrackingPF::SetUncertaintyFactorCalculatorLocationState(CUncertaintyModelScatteringFactorCalculator* pUncertaintyFactorCalculator)
{
    m_pUncertaintyFactorCalculatorLocationState = pUncertaintyFactorCalculator;
}

void CEgomotionTrackingPF::UpdateRenderingUnits()
{
    for (vector<CSamplingAndWeightingExecutionUnit*>::iterator euIterator = m_executionUnits.begin(); euIterator != m_executionUnits.end(); ++ euIterator)
    {
        CEgomotionTrackingPFExecutionUnit* trackingEU = static_cast<CEgomotionTrackingPFExecutionUnit*>(*euIterator);
        trackingEU->UpdateRenderingUnit();
    }
}

float CEgomotionTrackingPF::CalculateLocationScatteringFactor()
{
    float scatteringFactor = 1.0;
    if (m_pUncertaintyFactorCalculatorLocationState != NULL && m_scatteringReduction)
    {
        const CRobotControlState& currentRobotControlState = m_pRobotControl->GetRobotControlState();
        if (fabs(currentRobotControlState.GetPlatformVelocityX()) > 10.0 ||
            fabs(currentRobotControlState.GetPlatformVelocityY()) > 10.0)
        {
            m_pUncertaintyFactorCalculatorLocationState->Update(1.0);
        }
        else
        {
            m_pUncertaintyFactorCalculatorLocationState->Update(0.0);
        }
        scatteringFactor = m_pUncertaintyFactorCalculatorLocationState->GetScatteringFactor();
    }

    if (m_useAnnealing)
    {
        scatteringFactor *= CalculateLocationStatePredictorDispersionCoefficient(m_currentAnnealingLevel);
    }
    return scatteringFactor;
}

float CEgomotionTrackingPF::CalculateNeckErrorScatteringFactor()
{
    float scatteringFactor = 1.0;
    if (m_pUncertaintyFactorCalculatorNeckError != NULL && m_scatteringReduction)
    {
        const float headChangedThreshold = 2.0;
        const CHeadConfiguration& currentHeadConfiguration = m_pRobotControl->GetHeadConfiguration();
        if (fabs(Conversions::RadiansToDegree(currentHeadConfiguration.GetNeckRoll() - m_lastHeadConfiguration.GetNeckRoll())) > headChangedThreshold ||
            fabs(Conversions::RadiansToDegree(currentHeadConfiguration.GetNeckPitch() - m_lastHeadConfiguration.GetNeckPitch())) > headChangedThreshold ||
            fabs(Conversions::RadiansToDegree(currentHeadConfiguration.GetNeckYaw() - m_lastHeadConfiguration.GetNeckYaw())) > headChangedThreshold)
        {
            m_pUncertaintyFactorCalculatorNeckError->Update(1.0);
            m_lastHeadConfiguration = currentHeadConfiguration;
        }
        else
        {
            m_pUncertaintyFactorCalculatorNeckError->Update(0.0);
        }
        scatteringFactor = m_pUncertaintyFactorCalculatorNeckError->GetScatteringFactor();
    }

    if (m_useAnnealing)
    {
        scatteringFactor *= CalculateLocationStatePredictorDispersionCoefficient(m_currentAnnealingLevel);
    }
    return scatteringFactor;
}

void CEgomotionTrackingPF::SetParticlesToVector(const vector<CLocationParticle>& particles)
{
    if (static_cast<int>(particles.size()) != m_nParticles)
    {
        COUT_ERROR << "particles.size() != m_nParticles" << std::endl;
        exit(0);
    }

    int i = 0;
    m_cTotal = 0.0;
    for (vector<CLocationParticle>::const_iterator particleIterator = particles.begin(); particleIterator != particles.end(); ++particleIterator, ++i)
    {
        m_pC[i] = m_cTotal;
        m_cTotal += particleIterator->GetImportanceWeight();
        particleIterator->Get(m_ppS[i], &m_pPi[i]);
        //std::cout << "Initialized:" << m_ppS[i][0] << "," << m_ppS[i][1] << "," << m_ppS[i][2] << " : " << m_pPi[i] << std::endl;
    }
}

void CEgomotionTrackingPF::GetParticlesFromVector(vector<CLocationParticle>& particles)
{
    if (static_cast<int>(particles.size()) != m_nParticles)
    {
        COUT_ERROR << "particles.size() != m_nParticles" << std::endl;
        exit(0);
    }

    int i = 0;
    for (vector<CLocationParticle>::iterator particleIterator = particles.begin(); particleIterator != particles.end(); ++particleIterator, ++i)
    {
        particleIterator->Set(m_ppS[i], &m_pPi[i]);
        if (i < m_nActiveParticles)
        {
            particleIterator->SetActive(true);
        }
        else
        {
            particleIterator->SetActive(false);
        }
    }
}

void CEgomotionTrackingPF::CalculateWeightedMeanAndScatteringMatrix()
{
    PF_REAL wmeantmp[PARTICLE_FILTER_DIMESION];
    GetMeanConfiguration(wmeantmp);
    m_weightedMean.x = wmeantmp[0];
    m_weightedMean.y = wmeantmp[1];
    m_weightedMean.z = wmeantmp[2];

    m_weightedScatteringMatrix = Math3d::zero_mat;

    for (int i = 0; i < m_nActiveParticles; ++i)
    {
        Vec3d curParticle;
        curParticle.x = m_ppS[i][0];
        curParticle.y = m_ppS[i][1];
        curParticle.z = m_ppS[i][2];

        Vec3d deviation;
        Math3d::SubtractVecVec(curParticle, m_weightedMean, deviation);

        Mat3d tmp;
        MathIVT::DyadicTensorProcut(deviation, tmp);
        Math3d::MulMatScalar(tmp, m_pPiNormalized[i], tmp);
        Math3d::AddToMat(m_weightedScatteringMatrix, tmp);
    }
}

void CEgomotionTrackingPF::calculateSixDimensionsWeightedMeanAndCovarianceMatrix()
{
    PF_REAL tempWeightedMean[PARTICLE_FILTER_DIMESION];
    GetMeanConfiguration(tempWeightedMean);
    for (int i = 0; i < PARTICLE_FILTER_DIMESION; i++)
    {
        m_weightedMeanSixDimensions(i, 0) = tempWeightedMean[i];
    }

    m_weightedCovarianceMatrixSixDimensions = Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, PARTICLE_FILTER_DIMESION>::Zero();

    for (int i = 0; i < m_nActiveParticles; i++)
    {
        Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, 1> currentParticle;
        for (int j = 0; j < PARTICLE_FILTER_DIMESION; j++)
        {
            currentParticle(j, 0) = m_ppS[i][j];
        }

        Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, 1> deviation;
        deviation = currentParticle - m_weightedMeanSixDimensions;

        Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, PARTICLE_FILTER_DIMESION> tempMatrix;
        tempMatrix = deviation * deviation.transpose();

        tempMatrix = tempMatrix * m_pPiNormalized[i];
        m_weightedCovarianceMatrixSixDimensions += tempMatrix;
    }
}

void CEgomotionTrackingPF::SetScatteringReduction(bool enabled)
{
    m_scatteringReduction = enabled;
}

bool CEgomotionTrackingPF::GetNeckRPYErrorEstimate(float& neckRollError, float& neckPitchError, float& neckYawError) const
{
    neckRollError = m_neckRollErrorEstimate;
    neckPitchError = m_neckPitchErrorEstimate;
    neckYawError = m_neckYawErrorEstimate;
    return true;
}

void CEgomotionTrackingPF::SetUncertaintyFactorCalculatorNeckError(CUncertaintyModelScatteringFactorCalculator* pUncertaintyFactorCalculator)
{
    m_pUncertaintyFactorCalculatorNeckError = pUncertaintyFactorCalculator;
}

void CEgomotionTrackingPF::calculateResampableParticles()
{
    vector< std::pair<double, int> > particleMahalanobisDistance;
    particleMahalanobisDistance.resize(m_nActiveParticles);

    vector<CLocationParticle> particles;
    particles.resize(m_nParticles);
    GetParticles(particles);

    Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, 1> locationEstimateVector;
    locationEstimateVector(0, 0) = m_locationEstimate.GetX();
    locationEstimateVector(1, 0) = m_locationEstimate.GetY();
    locationEstimateVector(2, 0) = m_locationEstimate.GetAlpha();
    locationEstimateVector(3, 0) = m_neckRollErrorEstimate;
    locationEstimateVector(4, 0) = m_neckPitchErrorEstimate;
    locationEstimateVector(5, 0) = m_neckYawErrorEstimate;

    for (int i = 0; i < m_nActiveParticles; i++)
    {
        Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, 1> particleVector;
        particleVector(0, 0) = particles.at(i).GetX();
        particleVector(1, 0) = particles.at(i).GetY();
        particleVector(2, 0) = particles.at(i).GetAlpha();
        particleVector(3, 0) = particles.at(i).GetNeckRollError();
        particleVector(4, 0) = particles.at(i).GetNeckPitchError();
        particleVector(5, 0) = particles.at(i).GetNeckYawError();

        /* Compute the particles Mahalanobis distance and store it in the vector alongside the particles index. */
        double mahalanobisDistance = Calculations::CalculateMahalanobisDistanceSixDimensions(locationEstimateVector, particleVector, m_weightedCovarianceMatrixSixDimensions);

        particleMahalanobisDistance.at(i) = std::make_pair(mahalanobisDistance, i);
    }

    /* Sort by increasing Mahalanobis distance. */
    sort(particleMahalanobisDistance.begin(), particleMahalanobisDistance.end(), boost::bind(&std::pair<double, int>::first, _1) < boost::bind(&std::pair<double, int>::first, _2));

    /* Calculate the summed importance weight for each particle. */
    std::vector<PF_REAL> particleImportanceWeightSum(m_nActiveParticles);
    PF_REAL currentSumOfImportanceWeight = 0.0;
    for (int i = 0; i < m_nActiveParticles; i++)
    {
        int particleIndex = particleMahalanobisDistance.at(i).second;
        currentSumOfImportanceWeight += particles.at(particleIndex).GetImportanceWeight();
        particleImportanceWeightSum[i] = currentSumOfImportanceWeight;
    }

    /* Use binary search to find the last particle that falls under the summed importance weight treshold. */
    PF_REAL summedImportanceWeightTreshold = m_totalWeightPercentageTreshold * m_cTotal;
    int low = 0;
    int high = m_nActiveParticles;
    while (high > (low + 1))
    {
        int middle = (high + low) / 2;
        if (summedImportanceWeightTreshold > particleImportanceWeightSum[middle])
        {
            low = middle;
        }
        else
        {
            high = middle;
        }
    }

    /* This is the index of the last valid particle in the particleImportanceWeightSum/particleMahalanobisDistance vectors. */
    int lastParticleIndex = low;
    m_numberOfResampableParticles = lastParticleIndex + 1;

    /* Mark all particles under the summed importance weight treshold as resampable. */
    for (int i = 0; i < m_nParticles; i++)
    {
        m_particleCanBeResampled[i] = false;
    }
    for (int i = 0; i <= lastParticleIndex; i++)
    {
        int particleIndex = particleMahalanobisDistance.at(i).second;
        m_particleCanBeResampled[particleIndex] = true;
    }
}

void CEgomotionTrackingPF::setUseMahalanobisDistanceBasedScatteringReduction(bool useMahalanobisDistanceBasedScatteringReduction)
{
    m_useMahalanobisDistanceBasedScatteringReduction = useMahalanobisDistanceBasedScatteringReduction;
}

bool CEgomotionTrackingPF::getUseMahalanobisDistanceBasedScatteringReduction()
{
    return m_useMahalanobisDistanceBasedScatteringReduction;
}

void CEgomotionTrackingPF::setCounterTreshold(int counterTreshold)
{
    m_counterTreshold = counterTreshold;
}

int CEgomotionTrackingPF::getCounterTreshold()
{
    return m_counterTreshold;
}

void CEgomotionTrackingPF::setMahalanobisDistanceBasedScatteringReductionResamplingMode(
    const CEgomotionTrackingPFExecutionUnit::MahalanobisDistanceBasedScatteringReductionResamplingMode& mahalanobisDistanceBasedScatteringReductionResamplingMode)
{
    m_mahalanobisDistanceBasedScatteringReductionResamplingMode = mahalanobisDistanceBasedScatteringReductionResamplingMode;
}

CEgomotionTrackingPFExecutionUnit::MahalanobisDistanceBasedScatteringReductionResamplingMode CEgomotionTrackingPF::getMahalanobisDistanceBasedScatteringReductionResamplingMode()
{
    return m_mahalanobisDistanceBasedScatteringReductionResamplingMode;
}

void CEgomotionTrackingPF::calculateResampableParticlesEigenvaluesAndEigenvectors()
{
    /* Get resampable particles. */
    vector<CLocationParticle> resampableParticles;
    resampableParticles.resize(m_numberOfResampableParticles);
    int index = 0;
    double sumOfWeights = 0.0;
    for (int i = 0; i < m_nActiveParticles; i++)
    {
        if (m_particleCanBeResampled[i] == true)
        {
            resampableParticles.at(index).Set(m_ppS[i], &m_pPi[i]);
            sumOfWeights += m_pPi[i];
            index++;
        }
    }

    /* Calculate the weighted covariance matrix. */
    m_resampableParticlesWeightedCovarianceMatrix = Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, PARTICLE_FILTER_DIMESION>::Zero();
    for (int i = 0; i < m_numberOfResampableParticles; i++)
    {
        double particleImportanceWeight = resampableParticles.at(i).GetImportanceWeight();

        Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, 1> deviation;
        deviation(0, 0) = resampableParticles.at(i).GetX() - m_weightedMeanSixDimensions(0, 0);
        deviation(1, 0) = resampableParticles.at(i).GetY() - m_weightedMeanSixDimensions(1, 0);
        deviation(2, 0) = resampableParticles.at(i).GetAlpha() - m_weightedMeanSixDimensions(2, 0);
        deviation(3, 0) = resampableParticles.at(i).GetNeckRollError() - m_weightedMeanSixDimensions(3, 0);
        deviation(4, 0) = resampableParticles.at(i).GetNeckPitchError() - m_weightedMeanSixDimensions(4, 0);
        deviation(5, 0) = resampableParticles.at(i).GetNeckYawError() - m_weightedMeanSixDimensions(5, 0);

        Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, PARTICLE_FILTER_DIMESION> tempMatrix;
        tempMatrix = deviation * deviation.transpose();
        tempMatrix = tempMatrix * particleImportanceWeight;

        m_resampableParticlesWeightedCovarianceMatrix += tempMatrix;
    }
    m_resampableParticlesWeightedCovarianceMatrix = m_resampableParticlesWeightedCovarianceMatrix / sumOfWeights;

    /* Calculate the eigenvalues and eigenvectors. */
    Eigen::EigenSolver< Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, PARTICLE_FILTER_DIMESION> > eigensolver(m_resampableParticlesWeightedCovarianceMatrix);
    m_resampableParticlesEigenvalues =  eigensolver.eigenvalues().real();
    m_resampableParticlesEigenvectors = eigensolver.eigenvectors().real();

    /* Calculate the maximum Mahalanobis distance. This is equal to the distance of a particle at a distance of an eigenvalue along the corresponding eigenvector from the mean. */
    Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, 1> furthestParticle;
    furthestParticle = m_weightedMeanSixDimensions + m_resampableParticlesEigenvalues(0, 0) * m_resampableParticlesEigenvectors.col(0);
    m_resampableParticlesMaximumMahalanobisDistance = Calculations::CalculateMahalanobisDistanceSixDimensions(m_weightedMeanSixDimensions, furthestParticle, m_resampableParticlesWeightedCovarianceMatrix);
}

void CEgomotionTrackingPF::setTotalWeightPercentageTreshold(float totalWeightPercentageTreshold)
{
    m_totalWeightPercentageTreshold = totalWeightPercentageTreshold;
}
