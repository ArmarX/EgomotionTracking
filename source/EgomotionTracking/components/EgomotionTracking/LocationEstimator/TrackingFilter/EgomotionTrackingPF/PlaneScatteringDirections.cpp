#include "PlaneScatteringDirections.h"

CPlaneScatteringDirections::CPlaneScatteringDirections()
{
}

void CPlaneScatteringDirections::CalculateDirections(const Mat3d& scatteringMatrix)
{
    /* ignore alpha */
    Mat2d scatterMat2d;
    scatterMat2d.r1 = scatteringMatrix.r1;
    scatterMat2d.r2 = scatteringMatrix.r2;
    scatterMat2d.r3 = scatteringMatrix.r4;
    scatterMat2d.r4 = scatteringMatrix.r5;
    CalculateDirections(scatterMat2d);
}

void CPlaneScatteringDirections::CalculateDirections(const Mat2d& scatteringMatrix)
{
    Mat2d U, W;

    MathIVT::CalculateSVD(scatteringMatrix, W, U);
    m_direction1.x = U.r1;
    m_direction1.y = U.r3;
    m_direction2.x = U.r2;
    m_direction2.y = U.r4;
    m_sigma1 = sqrt(W.r1);
    m_sigma2 = sqrt(W.r4);
}


void CPlaneScatteringDirections::SetMean(const Vec2d& mean)
{
    m_mean = mean;
}

void CPlaneScatteringDirections::SetMean(const Vec3d& mean)
{
    m_mean.x = mean.x;
    m_mean.y = mean.y;
}

const Vec2d& CPlaneScatteringDirections::GetMean() const
{
    return m_mean;
}


const Vec2d& CPlaneScatteringDirections::GetDirection1() const
{
    return m_direction1;
}

const Vec2d& CPlaneScatteringDirections::GetDirection2() const
{
    return m_direction2;
}

float CPlaneScatteringDirections::GetSigma1() const
{
    return m_sigma1;
}

float CPlaneScatteringDirections::GetSigma2() const
{
    return m_sigma2;
}

float CPlaneScatteringDirections::GetEllipseArea(float ellipseSigmaFactor) const
{
    return M_PI * m_sigma1 * m_sigma2 * ellipseSigmaFactor * ellipseSigmaFactor;
}

void CPlaneScatteringDirections::setDirection1(Vec2d direction1)
{
    this->m_direction1 = direction1;
}

void CPlaneScatteringDirections::setSigma1(float sigma1)
{
    this->m_sigma1 = sigma1;
}

void CPlaneScatteringDirections::setSigma2(float sigma2)
{
    this->m_sigma2 = sigma2;
}
