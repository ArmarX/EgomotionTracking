#pragma once

#include "../TrackingFilterInterface.h"
#include "PlaneScatteringDirections.h"
#include "EgomotionTrackingPFExecutionUnit.h"

#include <EgomotionTracking/components/EgomotionTracking/Models/UncertaintyScatteringFactorCalculator/UncertaintyModelScatteringFactorCalculator.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/RobotControlState.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/ParticleFilterFramework/ParallelParticleFilterFramework/ParallelParticleFilterFramework.h>
#include <EgomotionTracking/components/EVP/Foundation/Process/ThreadPool.h>

#include <Inventor/SbMatrix.h>
#include <Inventor/SbVec3f.h>

#include <algorithm>

#include <eigen3/Eigen/Dense>

#define PARTICLE_FILTER_DIMESION 6

class CEgomotionTrackingPF : public CTrackingFilterInterface, protected CParallelParticleFilterFramework
{
public:
    CEgomotionTrackingPF(int numberOfParticles, EVP::Threading::CThreadPool* pThreadPool);
    ~CEgomotionTrackingPF() override;

    void InitializeFilter(const CLocationParticle& initializationState) override;
    virtual void InitializeFilter(const CLocationParticle& initializationState, float neckRollError, float neckPitchError, float neckYawError);
    virtual void InitializeFilter(const std::vector<CLocationParticle>& initialisationParticles);

    PF_REAL Filter(PF_REAL sigmaFactor) override;

    bool DispatchLocalisation() override
    {
        return false;
    }

    void GetParticles(std::vector<CLocationParticle>& particles);
    const std::vector<CLocationParticle>& GetParticles();
    void InitializeExecutionUnits() override;

    PF_REAL GetNEff()
    {
        return CParallelParticleFilterFramework::GetNEff();
    }

    int GetNumberOfActiveParticles();

    void SetNumberOfActiveParticles(int nActiveParticles);
    void SetResamplingThreshold(bool enabled, PF_REAL relativeNEffThreshold);
    void SetSwarmOptimization(bool enabled, PF_REAL velocityFactorLocalBest, PF_REAL velocityFactorGlobalBest, PF_REAL localBestAgingFactor, PF_REAL bestWeightThreshold);
    void SetAnnealing(bool enabled, unsigned int numberOfAnnealingLevels, PF_REAL annealingFactor, PF_REAL bestWeightThreshold, bool conservativeUpdate);
    void SetThresholdedMeanFactor(PF_REAL meanFactor);
    void SetEstimationMode(const CEstimationMode& mode);

    void SetRenderingUnit(CRenderingUnit* pRenderingUnit) override;

    void SetMeasurementUpdateActive(bool measurementUpdateActive);
    bool GetMeasurementUpdateActive() const;

    unsigned int GetCurrentAnnealingLevel();

    const Vec3d& GetWeightedMean();
    const Mat3d& GetWeightedScatteringMatrix();

    const CPlaneScatteringDirections& GetPlaneScatteringDirections();


    void CalculateBoundingRect(CLocationState& minState, CLocationState& maxState) const;

    void SetUncertaintyFactorCalculatorLocationState(CUncertaintyModelScatteringFactorCalculator* pUncertaintyFactorCalculator);
    void SetUncertaintyFactorCalculatorNeckError(CUncertaintyModelScatteringFactorCalculator* pUncertaintyFactorCalculator);
    void SetScatteringReduction(bool enabled);
    bool GetNeckRPYErrorEstimate(float& neckRollError, float& neckPitchError, float& neckYawError) const;

    void setUseMahalanobisDistanceBasedScatteringReduction(bool useMahalanobisDistanceBasedScatteringReduction);
    bool getUseMahalanobisDistanceBasedScatteringReduction();

    void setCounterTreshold(int counterTreshold);
    int getCounterTreshold();

    void setTotalWeightPercentageTreshold(float totalWeightPercentageTreshold);

    /* MAHALANOBIS */

    void setMahalanobisDistanceBasedScatteringReductionResamplingMode(
        const CEgomotionTrackingPFExecutionUnit::MahalanobisDistanceBasedScatteringReductionResamplingMode& mahalanobisDistanceBasedScatteringReductionResamplingMode);
    CEgomotionTrackingPFExecutionUnit::MahalanobisDistanceBasedScatteringReductionResamplingMode getMahalanobisDistanceBasedScatteringReductionResamplingMode();

protected:

    /* Protected methods. */

    void UpdateRenderingUnits();
    virtual PF_REAL CalculateObservationModelSensitivity(unsigned int annealingLevel)
    {
        return pow(1 / m_annealingFactor, static_cast<int>(annealingLevel));
    }
    virtual PF_REAL CalculateLocationStatePredictorDispersionCoefficient(unsigned int annealingLevel)
    {
        return pow(m_annealingFactor, static_cast<int>(annealingLevel));
    }

    float CalculateLocationScatteringFactor();
    float CalculateNeckErrorScatteringFactor();

    void SetParticlesToVector(const std::vector<CLocationParticle>& particles);
    void GetParticlesFromVector(std::vector<CLocationParticle>& particles);
    void CalculateWeightedMeanAndScatteringMatrix();

    /* Protected variables. */

    std::vector<CLocationParticle> m_particles;

    PF_REAL m_annealingFactor;
    bool m_annealingConservativeUpdate;

    Vec3d m_weightedMean;
    Mat3d m_weightedScatteringMatrix;
    CPlaneScatteringDirections m_planeScatteringDirections;
    bool m_measurementUpdateActive;
    bool m_scatteringReduction;
    CUncertaintyModelScatteringFactorCalculator* m_pUncertaintyFactorCalculatorLocationState;
    CUncertaintyModelScatteringFactorCalculator* m_pUncertaintyFactorCalculatorNeckError;
    CHeadConfiguration m_lastHeadConfiguration;

    float m_neckRollErrorEstimate;
    float m_neckPitchErrorEstimate;
    float m_neckYawErrorEstimate;

    /* MAHALANOBIS */

    bool m_useMahalanobisDistanceBasedScatteringReduction; /**< TRUE if particle scattering should be reduced based on their Mahalanobis distance. */
    int m_counterTreshold; /**< Counter value at which Mahalanobis distance based scattering reduction will kick in. */
    int m_counter; /**< Number of successive cycles in which the conditions for activating Mahalanobis distance based scattering reduction have been met. */

    int m_numberOfResampableParticles;

    Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, 1> m_weightedMeanSixDimensions; /**< Weighted mean considering all six particle filter dimensions. */
    Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, PARTICLE_FILTER_DIMESION> m_weightedCovarianceMatrixSixDimensions; /**< Weighted covariance matrix considering all six particle filter dimensions. */

    CEgomotionTrackingPFExecutionUnit::MahalanobisDistanceBasedScatteringReductionResamplingMode m_mahalanobisDistanceBasedScatteringReductionResamplingMode; /**< Resampling mode used when Mahalanobis distance based scattering reduction is enabled. */

    /**
      Calculate the Mahalanobis distance for each particle and, based on that, determine which particles should be resampled or not in the next cycle.
    */
    void calculateResampableParticles();

    /**
      Calculate the weighted mean and covariance matrix considering all six particle dimensions.
    */
    void calculateSixDimensionsWeightedMeanAndCovarianceMatrix();

    /**
      Calculate the eigenvalues and eigenvectors of the covariance matrix of the resampable particles.
    */
    void calculateResampableParticlesEigenvaluesAndEigenvectors();

    Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, 1> m_resampableParticlesEigenvalues; /**< Eigenvalues of the covariance matrix of the resampable particles. */
    Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, PARTICLE_FILTER_DIMESION> m_resampableParticlesEigenvectors; /**< Eigenvectors of the covariance matrix of the resampable particles. */

    float m_totalWeightPercentageTreshold;

    Eigen::Matrix<double, PARTICLE_FILTER_DIMESION, PARTICLE_FILTER_DIMESION> m_resampableParticlesWeightedCovarianceMatrix;
    double m_resampableParticlesMaximumMahalanobisDistance;
};

