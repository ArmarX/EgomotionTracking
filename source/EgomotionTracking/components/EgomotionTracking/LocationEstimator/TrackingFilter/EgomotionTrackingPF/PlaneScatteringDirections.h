#pragma once

#include <EgomotionTracking/components/EgomotionTracking/Helpers/MathIVT.h>

class CPlaneScatteringDirections
{
public:
    CPlaneScatteringDirections();
    void CalculateDirections(const Mat3d& scatteringMatrix);
    void CalculateDirections(const Mat2d& scatteringMatrix);

    void SetMean(const Vec2d& mean);
    void SetMean(const Vec3d& mean);
    const Vec2d& GetMean() const;
    const Vec2d& GetDirection1() const;
    const Vec2d& GetDirection2() const;
    float GetSigma1() const;
    float GetSigma2() const;

    float GetEllipseArea(float ellipseSigmaFactor = configuration_scatteringEllipseSigmaFactor) const;

    /* Functions to be used only to create a dummy instance for use in the ArmarXGui plugin */
    void setDirection1(Vec2d direction1);
    void setSigma1(float sigma1);
    void setSigma2(float sigma2);

protected:
    Vec2d m_mean;
    Vec2d m_direction1;
    Vec2d m_direction2;
    float m_sigma1;
    float m_sigma2;
};

