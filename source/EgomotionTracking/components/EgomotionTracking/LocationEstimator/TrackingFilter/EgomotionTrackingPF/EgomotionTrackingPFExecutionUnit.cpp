#include "EgomotionTrackingPFExecutionUnit.h"
#include "EgomotionTrackingPF.h"

#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>

CEgomotionTrackingPFExecutionUnit::CEgomotionTrackingPFExecutionUnit()
    : CSamplingAndWeightingExecutionUnit(PARTICLE_FILTER_DIMESION),
      m_measurementUpdateActive(true), m_resampleBasedOnMahalanobisDistance(false), m_mahalanobisDistanceBasedScatteringReductionResamplingMode(eStandardResampling)
{
    testRun = false;
}

void CEgomotionTrackingPFExecutionUnit::Sample(PF_REAL** ppParticle)
{
    if (!m_resampleBasedOnMahalanobisDistance || m_mahalanobisDistanceBasedScatteringReductionResamplingMode == eStandardResampling)
    {
        /* Resample particles the standard way. */

        CLocationParticle oldParticle;
        PF_REAL weight = 0.0;
        oldParticle.Set(*ppParticle, &weight);

        CLocationParticle newParticle(m_pPredictor->PredictNewRobotLocation(oldParticle, *m_pRobotControlState, &m_randomGenerator));
        newParticle.SetNeckRollError(oldParticle.GetNeckRollError());
        newParticle.SetNeckPitchError(oldParticle.GetNeckPitchError());
        newParticle.SetNeckYawError(oldParticle.GetNeckYawError());

        if (m_pNeckErrorUncertaintyModel != NULL)
        {
            m_pNeckErrorUncertaintyModel->AddNeckErrorUncertaintyToParticle(newParticle, &m_randomGenerator);
        }
        newParticle.Get(*ppParticle, &weight);
    }
    else if (m_mahalanobisDistanceBasedScatteringReductionResamplingMode == eEigenvectorEigenvalueBasedResampling ||
             m_mahalanobisDistanceBasedScatteringReductionResamplingMode == eLatticeSurfaceResampling)
    {
        /* Scatter particles inside the lattice. */

        CLocationParticle newParticle;
        PF_REAL weight = 0.0;

        Eigen::Matrix<double, 6, 1> newParticleVector = m_weightedMean;

        for (int i = 0; i < 6; i++)
        {
            Eigen::Matrix<double, 6, 1> componentVector = m_randomGenerator.GetUniformRandomNumber(-1.0, 1.0) * sqrt(m_eigenvalues(i, 0)) * m_eigenvectors.col(i);
            newParticleVector += componentVector;
        }

        /* Calculate the Mahalanobis distance of the generated vector. If it is greater than the maximum Mahalanobis distance, meaning that the new particle is outside
           the ellipsoid, transform the vector so that the particle lies inside the ellipsoid. */
        double newParticleVectorMahalanobisDistance = Calculations::CalculateMahalanobisDistanceSixDimensions(m_weightedMean, newParticleVector, m_resampableParticlesWeightedCovarianceMatrix);
        if (m_mahalanobisDistanceBasedScatteringReductionResamplingMode == eEigenvectorEigenvalueBasedResampling)
        {
            if (newParticleVectorMahalanobisDistance > m_resampableParticlesMaximumMahalanobisDistance)
            {
                newParticleVector = newParticleVector / newParticleVectorMahalanobisDistance * m_randomGenerator.GetUniformRandomNumber() * m_resampableParticlesMaximumMahalanobisDistance;
            }
        }
        else
        {
            newParticleVector = newParticleVector / newParticleVectorMahalanobisDistance * m_resampableParticlesMaximumMahalanobisDistance;
        }

        newParticle.SetX(newParticleVector(0, 0));
        newParticle.SetY(newParticleVector(1, 0));
        newParticle.SetAlpha(newParticleVector(2, 0));
        newParticle.SetNeckRollError(newParticleVector(3, 0));
        newParticle.SetNeckPitchError(newParticleVector(4, 0));
        newParticle.SetNeckYawError(newParticleVector(5, 0));

        newParticle.Get(*ppParticle, &weight);
    }
}

PF_REAL CEgomotionTrackingPFExecutionUnit::CalculateParticleWeight(PF_REAL** ppParticle, bool externCall)
{
    if (m_measurementUpdateActive)
    {
        PF_REAL weight = 0.0;
        m_tmpParticle.Set(*ppParticle, &weight);
        m_renderingUnit.RenderParticle(m_tmpParticle, m_pObservationModel->GetNeedRenderedJunctionPoints());
        weight = m_pObservationModel->CalculateImportanceWeight(m_tmpParticle, externCall);
        return weight;
    }
    return 0.0;
}

void CEgomotionTrackingPFExecutionUnit::SetMeasurementUpdateActive(bool measurementUpdateActive)
{
    m_measurementUpdateActive = measurementUpdateActive;
}

void CEgomotionTrackingPFExecutionUnit::UpdateRenderingUnit()
{
    m_renderingUnit.UpdateRenderingConfigurationFromParent();
    m_renderingUnit.UpdateHeadConfiguration();
}

CRenderingUnit CEgomotionTrackingPFExecutionUnit::GetRenderingUnit() const
{
    return m_renderingUnit;
}

void CEgomotionTrackingPFExecutionUnit::SetRenderingUnit(const CRenderingUnit& renderingUnit)
{
    m_renderingUnit = renderingUnit;
}

const CObservationModelInterface* CEgomotionTrackingPFExecutionUnit::GetObservationModel() const
{
    return m_pObservationModel;
}

void CEgomotionTrackingPFExecutionUnit::SetObservationModel(const CObservationModelInterface* pObservationModel)
{
    m_pObservationModel = pObservationModel;
}

const CRobotControlState* CEgomotionTrackingPFExecutionUnit::GetRobotControlState() const
{
    return m_pRobotControlState;
}

void CEgomotionTrackingPFExecutionUnit::SetRobotControlState(const CRobotControlState* pRobotControlState)
{
    m_pRobotControlState = pRobotControlState;
}

const CLocationStatePredictorInterface* CEgomotionTrackingPFExecutionUnit::GetPredictor() const
{
    return m_pPredictor;
}

void CEgomotionTrackingPFExecutionUnit::SetPredictor(const CLocationStatePredictorInterface* pPredictor)
{
    m_pPredictor = pPredictor;
}

const CNeckErrorUncertaintyModel* CEgomotionTrackingPFExecutionUnit::GetNeckErrorUncertaintyModel() const
{
    return m_pNeckErrorUncertaintyModel;
}

void CEgomotionTrackingPFExecutionUnit::SetNeckErrorUncertaintyModel(const CNeckErrorUncertaintyModel* pNeckErrorUncertaintyModel)
{
    m_pNeckErrorUncertaintyModel = pNeckErrorUncertaintyModel;
}

/* MAHALANOBIS */

void CEgomotionTrackingPFExecutionUnit::setMahalanobisDistanceBasedScatteringReductionResamplingMode(
    const MahalanobisDistanceBasedScatteringReductionResamplingMode& mahalanobisDistanceBasedScatteringReductionResamplingMode)
{
    m_mahalanobisDistanceBasedScatteringReductionResamplingMode = mahalanobisDistanceBasedScatteringReductionResamplingMode;
}

CEgomotionTrackingPFExecutionUnit::MahalanobisDistanceBasedScatteringReductionResamplingMode CEgomotionTrackingPFExecutionUnit::getMahalanobisDistanceBasedScatteringReductionResamplingMode()
{
    return m_mahalanobisDistanceBasedScatteringReductionResamplingMode;
}

void CEgomotionTrackingPFExecutionUnit::setResampleBasedOnMahalanobisDistance(bool resampleBasedOnMahalanobisDistance)
{
    m_resampleBasedOnMahalanobisDistance = resampleBasedOnMahalanobisDistance;
}

void CEgomotionTrackingPFExecutionUnit::setEigenvaluesAndEigenvectors(Eigen::Matrix<double, 6, 1> eigenvalues, Eigen::Matrix<double, 6, 6> eigenvectors)
{
    m_eigenvalues = eigenvalues;
    m_eigenvectors = eigenvectors;
}

void CEgomotionTrackingPFExecutionUnit::setWeightedMean(Eigen::Matrix<double, 6, 1> weightedMean)
{
    m_weightedMean = weightedMean;
}

void CEgomotionTrackingPFExecutionUnit::setResampableParticlesWeightedCovarianceMatrix(Eigen::Matrix<double, 6, 6> resampableParticlesWeightedCovarianceMatrix)
{
    m_resampableParticlesWeightedCovarianceMatrix = resampableParticlesWeightedCovarianceMatrix;
}

void CEgomotionTrackingPFExecutionUnit::setResampableParticlesMaximumMahalanobisDistance(double resampableParticlesMaximumMahalanobisDistance)
{
    m_resampableParticlesMaximumMahalanobisDistance = resampableParticlesMaximumMahalanobisDistance;
}

void CEgomotionTrackingPFExecutionUnit::test()
{
    if (testRun)
    {
        return;
    }

    testRun = true;
}
