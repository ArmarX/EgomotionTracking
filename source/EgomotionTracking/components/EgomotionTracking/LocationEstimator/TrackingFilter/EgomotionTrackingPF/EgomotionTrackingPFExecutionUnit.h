#pragma once

#include <EgomotionTracking/components/EgomotionTracking/Misc/ParticleFilterFramework/ParallelParticleFilterFramework/SamplingAndWeightingExecutionUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/RobotControlState.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/RenderingUnit/RenderingUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/ObservationModel/ObservationModelInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/LocationStatePredictor/LocationStatePredictorInterface.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/NeckErrorUncertaintyModel/NeckErrorUncertaintyModel.h>

#include <eigen3/Eigen/Dense>

class CEgomotionTrackingPF;

class CEgomotionTrackingPFExecutionUnit : public CSamplingAndWeightingExecutionUnit
{
public:
    CEgomotionTrackingPFExecutionUnit();

    const CLocationStatePredictorInterface* GetPredictor() const;
    void SetPredictor(const CLocationStatePredictorInterface* xpPredictor);

    const CNeckErrorUncertaintyModel* GetNeckErrorUncertaintyModel() const;
    void SetNeckErrorUncertaintyModel(const CNeckErrorUncertaintyModel* pNeckErrorUncertaintyModel);

    const CRobotControlState* GetRobotControlState() const;
    void SetRobotControlState(const CRobotControlState* pRobotControlState);

    const CObservationModelInterface* GetObservationModel() const;
    void SetObservationModel(const CObservationModelInterface* pObservationModel);

    CRenderingUnit GetRenderingUnit() const;
    void SetRenderingUnit(const CRenderingUnit& GetRenderingUnit);

    void SetMeasurementUpdateActive(bool measurementUpdateActive);

    void UpdateRenderingUnit();

    /* MAHALANOBIS */

    enum MahalanobisDistanceBasedScatteringReductionResamplingMode
    {
        eStandardResampling,
        eEigenvectorEigenvalueBasedResampling,
        eLatticeSurfaceResampling
    };

    void setMahalanobisDistanceBasedScatteringReductionResamplingMode(const MahalanobisDistanceBasedScatteringReductionResamplingMode& mahalanobisDistanceBasedScatteringReductionResamplingMode);
    MahalanobisDistanceBasedScatteringReductionResamplingMode getMahalanobisDistanceBasedScatteringReductionResamplingMode();

    void setResampleBasedOnMahalanobisDistance(bool resampleBasedOnMahalanobisDistance);

    void setEigenvaluesAndEigenvectors(Eigen::Matrix<double, 6, 1> eigenvalues, Eigen::Matrix<double, 6, 6> eigenvectors);

    void setWeightedMean(Eigen::Matrix<double, 6, 1> weightedMean);

    void setResampableParticlesWeightedCovarianceMatrix(Eigen::Matrix<double, 6, 6> resampableParticlesWeightedCovarianceMatrix);

    void setResampableParticlesMaximumMahalanobisDistance(double resampableParticlesMaximumMahalanobisDistance);

protected:
    void Sample(PF_REAL** ppParticle) override;
    PF_REAL CalculateParticleWeight(PF_REAL** ppParticle, bool externCall) override;

    const CLocationStatePredictorInterface* m_pPredictor;
    const CNeckErrorUncertaintyModel* m_pNeckErrorUncertaintyModel;
    const CRobotControlState* m_pRobotControlState;
    const CObservationModelInterface* m_pObservationModel;
    CRenderingUnit m_renderingUnit;
    bool m_measurementUpdateActive;

    CLocationParticle m_tmpParticle;

    /* MAHALANOBIS */

    bool m_resampleBasedOnMahalanobisDistance; /**< TRUE if particles are resampled based on their Mahalanobis distance. */
    MahalanobisDistanceBasedScatteringReductionResamplingMode m_mahalanobisDistanceBasedScatteringReductionResamplingMode; /**< Resampling mode used when Mahalanobis distance based scattering reduction is enabled. */

    Eigen::Matrix<double, 6, 1> m_eigenvalues;
    Eigen::Matrix<double, 6, 6> m_eigenvectors;

    Eigen::Matrix<double, 6, 1> m_weightedMean;

    Eigen::Matrix<double, 6, 6> m_resampableParticlesWeightedCovarianceMatrix;
    double m_resampableParticlesMaximumMahalanobisDistance;

    void test();
    bool testRun;
};

