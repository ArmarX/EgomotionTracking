/*
 * LocationEstimator.h
 *
 *  Created on: 11.03.2013
 *      Author: abyte
 */

#pragma once

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotPlatformTransformation.h>
class CLocationEstimator
{
public:
    CLocationEstimator();
    virtual ~CLocationEstimator();

    virtual bool DispatchLocalisation() = 0;

    const CLocationState& GetLocationEstimate() const
    {
        if (m_useBias)
        {
            m_biasedLocationEstimate =  CRobotPlatformTransformation::SetPlatformRelative(m_locationEstimate, m_biasXR, m_biasYR, m_biasAlphaR);
            return m_biasedLocationEstimate;
        }
        return m_locationEstimate;
    }

    void SetLocationRegion(const CLocationRegion* pLocationRegion)
    {
        m_locationEstimate.SetLocationRegion(pLocationRegion);
    }

    bool SetRelativeLocationBias(float biasXR, float biasYR, float biasAlphaR);
    bool GetUseBias() const;
    void SetUseBias(bool useBias);

    bool EstimationIsValid() const;

protected:
    CLocationState m_locationEstimate;
    bool m_estimationIsValid;
private:
    mutable CLocationState m_biasedLocationEstimate;
    bool m_useBias;
    float m_biasXR;
    float m_biasYR;
    float m_biasAlphaR;


};

