/*
 * TrackingConfiguration.h
 *
 *  Created on: 14.03.2013
 *      Author: abyte
 */

#pragma once
#include <float.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Conversions.h>

#define EGOMOTIONTRACKING_RESSOURCE_DIRECTORY "/home/SMBAD/heyde/armarx/VisionX/data/EgomotionTrackingData"

#define USE_BIG_KITCHEN

#ifdef USE_BIG_KITCHEN
#define KITCHEN_MODELS_DIRECTORY EGOMOTIONTRACKING_RESSOURCE_DIRECTORY "/bigKitchen/"
#else
#define KITCHEN_MODELS_DIRECTORY EGOMOTIONTRACKING_RESSOURCE_DIRECTORY "/smallKitchen/"
#endif
const float configuration_defaultDeltaT = 200;

const unsigned int configuration_imageWidth = 640;
const unsigned int configuration_imageHeight = 480;

const float configuration_near = 5;
const float configuration_far = 60000;

const bool configuration_spaceportAntialiasing = false;
const unsigned int configuration_spaceportTotalPasses = 8;

const float configuration_trackingVisualizationAreaMinX = -1000;
const float configuration_trackingVisualizationAreaMaxX = 4500;
const float configuration_trackingVisualizationAreaMinY = -1000;
const float configuration_trackingVisualizationAreaMaxY = 4000;

const unsigned int configuration_maxParticles = 300;
const unsigned int configuration_defaultActiveParticles = 200;
const unsigned int configuration_numberOfDynamicElementParticles = 100;
const int configuration_psLocationEstimatorParticles = 1000;

/* use/draw 3sigma interval scattering ellipse */
const int configuration_scatteringEllipseSigmaFactor = 3;

const float configuration_evaluation_success_threshold_d = 100; //10 cm
//const float configuration_evaluation_success_threshold_alpha = Conversions::DegreeToRadians(2); //2°; //unused variable
//const float configuration_evaluation_success_threshold_scatteringEllipseArea = Conversions::SquareMetreToSquareMillimetre(4 * 0.01); // 4dm //unused variable
typedef double PF_REAL;
#define PF_REAL_MIN     DBL_MIN
#define PF_REAL_MAX     DBL_MAX
#define PF_REAL_EPSILON  DBL_EPSILON


#define VISUALIZATION_RECORDING_FILENAMEPREFIX_VIEWIMAGE "viewImage-"
#define VISUALIZATION_RECORDING_FILENAMEPREFIX_EDGEIMAGE "edgeImage-"
#define VISUALIZATION_RECORDING_FILENAMEPREFIX_OMIMAGE "omImage-"
#define VISUALIZATION_RECORDING_FILENAMEPREFIX_PARTICLESVIEW1 "particlesView1-"
#define VISUALIZATION_RECORDING_FILENAMEPREFIX_PARTICLESVIEW2 "particlesView2-"
#define VISUALIZATION_RECORDING_FILENAMEPREFIX_PARTICLESSCENE "particlesScene-"
#define VISUALIZATION_RECORDING_FILENAMEPREFIX_TRACKINGVIEW "trackingView-"
#define VISUALIZATION_RECORDING_FILENAMEPREFIX_HEADCONFIGURATIONSVIEW "headConfigurationsView-"


const float configuration_visualization_recording_resolution_x = 1024;
const float configuration_visualization_recording_resolution_y = 768;

