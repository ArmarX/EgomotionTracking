#pragma once

/* #undef _EVP_USE_QT3 */
#define _EVP_USE_QT4
#define _EVP_USE_GUI_TOOLKIT

/* #undef _USE_FAST_CALLS_ */
#define _USE_OPEN_INVENTOR_
#define _USE_LOOKUP_TABLES_
#define _USE_ZLIB_

/* #undef _USE_SINGLE_PRECISION_ */
#define _USE_DOUBLE_PRECISION_
/* #undef _USE_LONG_DOUBLE_PRECISION_ */

