/*
 * TristimulusColorMap.h
 *
 *  Created on: 04.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "ColorMap.h"
#include "../../Foundation/DataTypes/DiscreteTristimulusPixel.h"

namespace EVP
{
    namespace Visualization
    {
        class CTristimulusColorMap: public TColorMap<CDiscreteTristimulusPixel>
        {
        public:

            enum PredefinedColorMap
            {
                eRGB_Intensity, eRGB_InverseIntensity, eRGB_Hot, eRGB_Red, eRGB_Green, eRGB_Blue
            };

            static  bool IsValidPredefinedColorMap(const PredefinedColorMap Map);
            static const CTristimulusColorMap* LoadPredefinedColorMap(const PredefinedColorMap Map);

            CTristimulusColorMap();
            ~CTristimulusColorMap() override;

            void SetPredefinedColorMap(PredefinedColorMap Map);
        };
    }
}

