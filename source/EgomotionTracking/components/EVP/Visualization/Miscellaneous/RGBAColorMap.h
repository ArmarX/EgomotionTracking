/*
 * RGBAColorMap.h
 *
 *  Created on: Apr 2, 2012
 *      Author: david
 */

#pragma once

#include "ColorMap.h"
#include "../../Foundation/DataTypes/RGBAColor.h"

namespace EVP
{
    namespace Visualization
    {
        class CRGBAColorMap: public TColorMap<CRGBAColor>
        {
        public:

            enum PredefinedColorMap
            {
                eRGBA_Intensity, eRGBA_InverseIntensity, eRGBA_Hot, eRGBA_HotAlphaSquare, eRGBA_HotAlphaCube, eRGBA_Red, eRGBA_Green, eRGBA_Blue
            };

            static CRGBAColorMap* LoadPredefinedColorMap(PredefinedColorMap Map);

            CRGBAColorMap();
            ~CRGBAColorMap() override;

            void SetPredefinedColorMap(PredefinedColorMap Map);
            bool SetAlphaProfileCurve(const real Gamma, const real Scale);
            bool SetAlphaGammaCurve(const real Gamma);
            bool SetAlphaScaleCurve(const real Scale);
        };
    }
}

