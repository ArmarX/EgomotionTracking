/*
 * RGBAColorMap.cpp
 *
 *  Created on: Apr 2, 2012
 *      Author: david
 */

#include "RGBAColorMap.h"

namespace EVP
{
    namespace Visualization
    {
        CRGBAColorMap* CRGBAColorMap::LoadPredefinedColorMap(PredefinedColorMap Map)
        {
            CRGBAColorMap* pRGBAColorMap = new CRGBAColorMap;
            pRGBAColorMap->SetPredefinedColorMap(Map);
            return pRGBAColorMap;
        }

        CRGBAColorMap::CRGBAColorMap() :
            TColorMap<CRGBAColor> ()
        {
        }

        CRGBAColorMap::~CRGBAColorMap()
            = default;

        void CRGBAColorMap::SetPredefinedColorMap(PredefinedColorMap Map)
        {
            Clear();
            switch (Map)
            {
                case eRGBA_Intensity:
                    AddColorKey(CRGBAColor(0), _REAL_ZERO_);
                    AddColorKey(CRGBAColor(255), _REAL_ONE_, true);
                    m_Name = "Intensity";
                    break;
                case eRGBA_InverseIntensity:
                    AddColorKey(CRGBAColor(255), _REAL_ZERO_);
                    AddColorKey(CRGBAColor(0), _REAL_ONE_, true);
                    m_Name = "Inverse Intensity";
                    break;
                case eRGBA_Hot:
                {
                    CRGBAColor DefinedHotColors[16];
                    const real Delta = _REAL_ONE_ / real(15.0);
                    real Alpha = _REAL_ZERO_;
                    DefinedHotColors[0].SetValue(0, 0, 189, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[1].SetValue(0, 0, 255, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[2].SetValue(0, 66, 255, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[3].SetValue(0, 132, 255, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[4].SetValue(0, 189, 255, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[5].SetValue(0, 255, 255, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[6].SetValue(66, 255, 189, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[7].SetValue(132, 255, 132, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[8].SetValue(189, 255, 66, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[9].SetValue(255, 255, 0, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[10].SetValue(255, 189, 0, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[11].SetValue(255, 132, 0, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[12].SetValue(255, 66, 0, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[13].SetValue(189, 0, 0, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[14].SetValue(132, 0, 0, RealRound(_REAL_255_ * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[15].SetValue(128, 0, 0, RealRound(_REAL_255_ * Alpha));
                    for (int i = 0; i < 16; ++i)
                    {
                        AddColorKey(DefinedHotColors[i], real(i) / real(15), (i == 15));
                    }
                    m_Name = "Hot";
                }
                break;
                case eRGBA_HotAlphaSquare:
                {
                    CRGBAColor DefinedHotColors[16];
                    const real Delta = _REAL_ONE_ / real(15.0);
                    real Alpha = _REAL_ZERO_;
                    DefinedHotColors[0].SetValue(0, 0, 189, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[1].SetValue(0, 0, 255, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[2].SetValue(0, 66, 255, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[3].SetValue(0, 132, 255, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[4].SetValue(0, 189, 255, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[5].SetValue(0, 255, 255, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[6].SetValue(66, 255, 189, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[7].SetValue(132, 255, 132, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[8].SetValue(189, 255, 66, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[9].SetValue(255, 255, 0, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[10].SetValue(255, 189, 0, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[11].SetValue(255, 132, 0, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[12].SetValue(255, 66, 0, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[13].SetValue(189, 0, 0, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[14].SetValue(132, 0, 0, RealRound(_REAL_255_ * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[15].SetValue(128, 0, 0, RealRound(_REAL_255_ * Alpha * Alpha));
                    for (int i = 0; i < 16; ++i)
                    {
                        AddColorKey(DefinedHotColors[i], real(i) / real(15), (i == 15));
                    }
                    m_Name = "Hot";
                }
                break;
                case eRGBA_HotAlphaCube:
                {
                    CRGBAColor DefinedHotColors[16];
                    const real Delta = _REAL_ONE_ / real(15.0);
                    real Alpha = _REAL_ZERO_;
                    DefinedHotColors[0].SetValue(0, 0, 189, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[1].SetValue(0, 0, 255, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[2].SetValue(0, 66, 255, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[3].SetValue(0, 132, 255, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[4].SetValue(0, 189, 255, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[5].SetValue(0, 255, 255, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[6].SetValue(66, 255, 189, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[7].SetValue(132, 255, 132, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[8].SetValue(189, 255, 66, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[9].SetValue(255, 255, 0, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[10].SetValue(255, 189, 0, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[11].SetValue(255, 132, 0, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[12].SetValue(255, 66, 0, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[13].SetValue(189, 0, 0, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[14].SetValue(132, 0, 0, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    Alpha += Delta;
                    DefinedHotColors[15].SetValue(128, 0, 0, RealRound(_REAL_255_ * Alpha * Alpha * Alpha));
                    for (int i = 0; i < 16; ++i)
                    {
                        AddColorKey(DefinedHotColors[i], real(i) / real(15), (i == 15));
                    }
                    m_Name = "Hot";
                }
                break;
                case eRGBA_Red:
                    AddColorKey(CRGBAColor(0), _REAL_ZERO_);
                    AddColorKey(CRGBAColor(255, 0, 0, 255), _REAL_ONE_, true);
                    m_Name = "Red";
                    break;
                case eRGBA_Green:
                    AddColorKey(CRGBAColor(0), _REAL_ZERO_);
                    AddColorKey(CRGBAColor(0, 255, 0, 255), _REAL_ONE_, true);
                    m_Name = "Green";
                    break;
                case eRGBA_Blue:
                    AddColorKey(CRGBAColor(0), _REAL_ZERO_);
                    AddColorKey(CRGBAColor(0, 0, 255, 255), _REAL_ONE_, true);
                    m_Name = "Blue";
                    break;
            }
        }

        bool CRGBAColorMap::SetAlphaProfileCurve(const real Gamma, const real Scale)
        {
            const uint TotalColors = TColorMap<CRGBAColor>::m_ColorKeys.size();
            if (TotalColors > 1)
            {
                const real Delta = _REAL_ONE_ / real(TotalColors - 1);
                real Alpha = _REAL_ZERO_;
                list<TColorMap<CRGBAColor>::CColorKey>::iterator End = TColorMap<CRGBAColor>::m_ColorKeys.end();
                for (list<TColorMap<CRGBAColor>::CColorKey>::iterator pColorKey = TColorMap<CRGBAColor>::m_ColorKeys.begin(); pColorKey != End; ++pColorKey, Alpha += Delta)
                {
                    pColorKey->m_Color.SetAlphaChannel(byte(RealRound(TMin(_REAL_255_, TMax(_REAL_ZERO_, _REAL_255_ * RealPow(Alpha, Gamma) * Scale)))));
                }
                TColorMap<CRGBAColor>::LoadLookupTable();
                return true;
            }
            return false;
        }

        bool CRGBAColorMap::SetAlphaGammaCurve(const real Gamma)
        {
            const uint TotalColors = TColorMap<CRGBAColor>::m_ColorKeys.size();
            if (TotalColors > 1)
            {
                const real Delta = _REAL_ONE_ / real(TotalColors - 1);
                real Alpha = _REAL_ZERO_;
                list<TColorMap<CRGBAColor>::CColorKey>::iterator End = TColorMap<CRGBAColor>::m_ColorKeys.end();
                for (list<TColorMap<CRGBAColor>::CColorKey>::iterator pColorKey = TColorMap<CRGBAColor>::m_ColorKeys.begin(); pColorKey != End; ++pColorKey, Alpha += Delta)
                {
                    pColorKey->m_Color.SetAlphaChannel(byte(RealRound(_REAL_255_ * RealPow(Alpha, Gamma))));
                }
                TColorMap<CRGBAColor>::LoadLookupTable();
                return true;
            }
            return false;
        }

        bool CRGBAColorMap::SetAlphaScaleCurve(const real Scale)
        {
            if (TColorMap<CRGBAColor>::m_ColorKeys.size() > 1)
            {
                list<TColorMap<CRGBAColor>::CColorKey>::iterator End = TColorMap<CRGBAColor>::m_ColorKeys.end();
                for (list<TColorMap<CRGBAColor>::CColorKey>::iterator pColorKey = TColorMap<CRGBAColor>::m_ColorKeys.begin(); pColorKey != End; ++pColorKey)
                {
                    pColorKey->m_Color.SetAlphaChannel(byte(RealRound(TMin(_REAL_255_, TMax(_REAL_ZERO_, real(pColorKey->m_Color.GetAlphaChannel()) * Scale)))));
                }
                TColorMap<CRGBAColor>::LoadLookupTable();
                return true;
            }
            return false;
        }
    }
}
