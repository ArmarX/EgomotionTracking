/*
 * TristimulusColorMap.cpp
 *
 *  Created on: 04.10.2011
 *      Author: gonzalez
 */

#include "TristimulusColorMap.h"

namespace EVP
{
    namespace Visualization
    {

        bool CTristimulusColorMap::IsValidPredefinedColorMap(const PredefinedColorMap Map)
        {
            return (Map == eRGB_Intensity) || (Map == eRGB_InverseIntensity) || (Map == eRGB_Hot) || (Map == eRGB_Red) || (Map == eRGB_Green) || (Map == eRGB_Blue);
        }

        const CTristimulusColorMap* CTristimulusColorMap::LoadPredefinedColorMap(const PredefinedColorMap Map)
        {
            CTristimulusColorMap* pTristimulusColorMap = new CTristimulusColorMap;
            pTristimulusColorMap->SetPredefinedColorMap(Map);
            return pTristimulusColorMap;
        }

        CTristimulusColorMap::CTristimulusColorMap() :
            TColorMap<CDiscreteTristimulusPixel> ()
        {
        }

        CTristimulusColorMap::~CTristimulusColorMap()
            = default;

        void CTristimulusColorMap::SetPredefinedColorMap(PredefinedColorMap Map)
        {
            Clear();
            switch (Map)
            {
                case eRGB_Intensity:
                    AddColorKey(CDiscreteTristimulusPixel::s_RGB_Black, _REAL_ZERO_);
                    AddColorKey(CDiscreteTristimulusPixel::s_RGB_White, _REAL_ONE_, true);
                    m_Name = "Intensity";
                    break;
                case eRGB_InverseIntensity:
                    AddColorKey(CDiscreteTristimulusPixel::s_RGB_White, _REAL_ZERO_);
                    AddColorKey(CDiscreteTristimulusPixel::s_RGB_Black, _REAL_ONE_, true);
                    m_Name = "Inverse Intensity";
                    break;
                case eRGB_Hot:
                {
                    CDiscreteTristimulusPixel DefinedHotColors[16];
                    DefinedHotColors[0].SetValue(0, 0, 189);
                    DefinedHotColors[1].SetValue(0, 0, 255);
                    DefinedHotColors[2].SetValue(0, 66, 255);
                    DefinedHotColors[3].SetValue(0, 132, 255);
                    DefinedHotColors[4].SetValue(0, 189, 255);
                    DefinedHotColors[5].SetValue(0, 255, 255);
                    DefinedHotColors[6].SetValue(66, 255, 189);
                    DefinedHotColors[7].SetValue(132, 255, 132);
                    DefinedHotColors[8].SetValue(189, 255, 66);
                    DefinedHotColors[9].SetValue(255, 255, 0);
                    DefinedHotColors[10].SetValue(255, 189, 0);
                    DefinedHotColors[11].SetValue(255, 132, 0);
                    DefinedHotColors[12].SetValue(255, 66, 0);
                    DefinedHotColors[13].SetValue(189, 0, 0);
                    DefinedHotColors[14].SetValue(132, 0, 0);
                    DefinedHotColors[15].SetValue(128, 0, 0);
                    for (int i = 0; i < 16; ++i)
                    {
                        AddColorKey(DefinedHotColors[i], real(i) / real(15), (i == 15));
                    }
                    m_Name = "Hot";
                }
                break;
                case eRGB_Red:
                    AddColorKey(CDiscreteTristimulusPixel::s_RGB_Black, _REAL_ZERO_);
                    AddColorKey(CDiscreteTristimulusPixel::s_RGB_Red, _REAL_ONE_, true);
                    m_Name = "Red";
                    break;
                case eRGB_Green:
                    AddColorKey(CDiscreteTristimulusPixel::s_RGB_Black, _REAL_ZERO_);
                    AddColorKey(CDiscreteTristimulusPixel::s_RGB_Green, _REAL_ONE_, true);
                    m_Name = "Green";
                    break;
                case eRGB_Blue:
                    AddColorKey(CDiscreteTristimulusPixel::s_RGB_Black, _REAL_ZERO_);
                    AddColorKey(CDiscreteTristimulusPixel::s_RGB_Blue, _REAL_ONE_, true);
                    m_Name = "Blue";
                    break;
            }
        }
    }
}
