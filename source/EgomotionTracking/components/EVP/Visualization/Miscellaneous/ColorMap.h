/*
 * TColorMap.h
 *
 *  Created on: 04.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"

namespace EVP
{
    namespace Visualization
    {
        template<typename ColorType> class TColorMap
        {
        public:

            TColorMap() :
                m_Name("Undefined")
            {
                m_pLUT = NULL;
                m_LUTScaleFactor = _REAL_ZERO_;
            }

            virtual ~TColorMap()
            {
                Clear();
            }

            bool AddColorKey(const ColorType& Color, const real Position, const bool Finalize = false)
            {
                if ((Position < _REAL_ZERO_) || (Position > _REAL_ONE_))
                {
                    return false;
                }
                m_ColorKeys.push_back(CColorKey(Position, Color));
                if (Finalize)
                {
                    m_Name = "Custom";
                    return LoadLookupTable();
                }
                return true;
            }

            void Clear()
            {
                m_ColorKeys.clear();
                RELEASE_ARRAY(m_pLUT)
                m_LUTScaleFactor = _REAL_ZERO_;
            }

            const std::string GetName() const
            {
                return m_Name;
            }

            const ColorType* GetColorLookupTable() const
            {
                return m_pLUT;
            }

            real GetColorLookupTableScaleFactor() const
            {
                return m_LUTScaleFactor;
            }

            void GetColor(const real Position, ColorType* pColor) const
            {
                if (m_pLUT && (Position >= _REAL_ZERO_) && (Position <= _REAL_ONE_))
                {
                    *pColor = m_pLUT[RoundToInteger(Position * m_LUTScaleFactor)];
                }
                else
                {
                    memset(pColor, 0, sizeof(ColorType));
                }
            }

            const ColorType GetColor(const real Position) const
            {
                if (m_pLUT && (Position >= _REAL_ZERO_) && (Position <= _REAL_ONE_))
                {
                    return m_pLUT[RoundToInteger(Position * m_LUTScaleFactor)];
                }
                else
                {
                    ColorType Color;
                    memset(&Color, 0, sizeof(ColorType));
                    return Color;
                }
            }

        protected:

            class CColorKey
            {
            public:

                CColorKey(const real& Position, const ColorType& Color) :
                    m_Position(Position), m_Color(Color)
                {
                }
                real m_Position;
                ColorType m_Color;
            };

            bool LoadLookupTable()
            {
                if (m_ColorKeys.size() > 1)
                {
                    m_ColorKeys.sort(Compare);
                    if ((m_ColorKeys.front().m_Position == _REAL_ZERO_) && (m_ColorKeys.back().m_Position == _REAL_ONE_))
                    {
                        RELEASE_ARRAY(m_pLUT)
                        const uint LUTQuantizationLevels = UpperToInteger(GetMaximalChromaticRatio()) + 1;
                        m_LUTScaleFactor = real(LUTQuantizationLevels - 1);
                        m_pLUT = new ColorType[LUTQuantizationLevels];
                        const real Delta = _REAL_ONE_ / real(LUTQuantizationLevels);
                        real Position = _REAL_ZERO_;
                        typename list<CColorKey>::const_iterator pColorKeyA = m_ColorKeys.begin();
                        typename list<CColorKey>::const_iterator pColorKeyB = pColorKeyA;
                        ++pColorKeyB;
                        ColorType* pColor = m_pLUT;
                        while (Position <= _REAL_ONE_)
                        {
                            if ((Position >= pColorKeyA->m_Position) && (Position <= pColorKeyB->m_Position))
                            {
                                const real IntervalLength = pColorKeyB->m_Position - pColorKeyA->m_Position;
                                const real ContributionA = (pColorKeyB->m_Position - Position) / IntervalLength;
                                const real ContributionB = (Position - pColorKeyA->m_Position) / IntervalLength;
                                const byte* pColorA = reinterpret_cast<const byte*>(&pColorKeyA->m_Color);
                                const byte* pColorB = reinterpret_cast<const byte*>(&pColorKeyB->m_Color);
                                byte* pColorC = reinterpret_cast<byte*>(pColor);
                                for (uint i = 0; i < sizeof(ColorType); ++i)
                                {
                                    pColorC[i] = SafeRoundToByte(real(pColorA[i]) * ContributionA + real(pColorB[i]) * ContributionB);
                                }
                                ++pColor;
                                Position += Delta;
                            }
                            else
                            {
                                ++pColorKeyA;
                                ++pColorKeyB;
                            }
                        }
                        return true;
                    }
                }
                return false;
            }

            real GetMaximalChromaticRatio()
            {
                typename list<CColorKey>::const_iterator EndColorKeys = m_ColorKeys.end();
                typename list<CColorKey>::const_iterator pColorKeyA = m_ColorKeys.begin();
                typename list<CColorKey>::const_iterator pColorKeyB = pColorKeyA;
                ++pColorKeyB;
                real MaximalChromaticRatio = _REAL_ZERO_;
                while (pColorKeyB != EndColorKeys)
                {
                    const real PositionDistance = pColorKeyB->m_Position - pColorKeyA->m_Position;
                    if (PositionDistance > _REAL_EPSILON_)
                    {
                        real ChromaticRatio = real(ComputeChebyshevChromaticDistance(pColorKeyA->m_Color, pColorKeyB->m_Color)) / PositionDistance;
                        if (ChromaticRatio > MaximalChromaticRatio)
                        {
                            MaximalChromaticRatio = ChromaticRatio;
                        }
                    }
                    ++pColorKeyA;
                    ++pColorKeyB;
                }
                return MaximalChromaticRatio;
            }

            int ComputeChebyshevChromaticDistance(const ColorType& ColorA, const ColorType& ColorB)
            {
                const byte* pColorA = reinterpret_cast<const byte*>(&ColorA);
                const byte* pColorB = reinterpret_cast<const byte*>(&ColorB);
                int ChebyshevChromaticDistance = 0;
                for (uint i = 0; i < sizeof(ColorType); ++i)
                {
                    ChebyshevChromaticDistance = TMax(abs((int(pColorA[i]) - int(pColorB[i]))), ChebyshevChromaticDistance);
                }
                return ChebyshevChromaticDistance;
            }

            static bool Compare(const CColorKey& lhs, const CColorKey& rhs)
            {
                return lhs.m_Position < rhs.m_Position;
            }

            std::string m_Name;
            ColorType* m_pLUT;
            real m_LUTScaleFactor;
            list<CColorKey> m_ColorKeys;
        };
    }
}

