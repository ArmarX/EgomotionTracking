/*
 * DensityVolume.h
 *
 *  Created on: Apr 2, 2012
 *      Author: david
 */

#pragma once

#include "../../../../GlobalSettings.h"

#ifdef _USE_OPEN_INVENTOR_SIMVOLEON_

//OPEN INVENTOR SIMVOLEON
#include "Simovoleon.h"

//EVP
#include "../../../../Foundation/Mathematics/3D/Vector3D.h"
#include "../../../../Foundation/Mathematics/3D/Density/DensityAggregation.h"
#include "../../../../VisualSpace/Common/ContinuousBoundingBox3D.h"
#include "../../../Geometry/3D/OpenInventor/VisualizationPrimitive.h"
#include "../../../Miscellaneous/RGBAColorMap.h"

namespace EVP
{
    namespace Visualization
    {
        namespace VolumeRendering
        {
            namespace Simvoleon
            {
                class CDensityVolume: public Geometry::_3D::OpenInventor::CVisualizationPrimitive
                {
                public:

                    static  real DetermineDiscretizationResolution(const CContinuousBoundingBox3D& BoundingBox, const uint TotalDiscretizationCells);
                    static  uint DetermineTotalDiscretizationCells(const CContinuousBoundingBox3D& BoundingBox, const real DiscretizationResolution);

                    CDensityVolume(const CRGBAColorMap* pColorMap);
                    virtual ~CDensityVolume();

                    bool SetDensityAggregation(const Mathematics::_3D::Density::CDensityPrimitive::ConditioningMode ConditioningMode, const Mathematics::_3D::Density::CDensityAggregation* pDensityAggregation, const real MinimalDensity, const real DiscretizationResolution);

                    bool SetColorMap(const CRGBAColorMap* pColorMap);
                    bool SetConfiguration(const CContinuousBoundingBox3D& BoundingBox, const real DiscretizationResolution);
                    bool ClearDensityCellValues();
                    virtual void Update(const bool FullUpdate);

                    uint GetDensityCellsWidth() const;
                    uint GetDensityCellsHeight() const;
                    uint GetDensityCellsDepth() const;
                    uint GetTotalSlideDensityCells() const;
                    uint GetTotalDensityCells() const;
                    real GetDiscretizationResolution() const;
                    const real* GetReadOnlyDensityCells() const;
                    real* GetWritableDensityCells();
                    real GetDensity(const uint X, const uint Y, const uint Z);
                    bool SetDensity(const uint X, const uint Y, const uint Z, const real Density);
                    const CContinuousBoundingBox3D& GetBoundingBox() const;
                    Mathematics::_3D::CVector3D GetBasePoint() const;
                    Mathematics::_3D::CVector3D GetCellDelta() const;

                    bool MapCellsToCellsAutoScaleLinear();
                    bool MapCellsToVoxelsClamppingLinear(const real MaximalDensity, const real MinimalDensity);
                    bool MapCellsToVoxelsDirectLinear(const real MaximalDensity, const real MinimalDensity);

                    bool HideSubspace(const CContinuousBoundingBox3D& Subspace);
                    void ShowFullSpace();

                    real GetMaximalDensity() const;
                    real GetMinimalDensity() const;
                    bool GetDescriptiveStatistics(real& MeanDensity, real& StandardDeviationDensity, real& MaximalDensity, real& MinimalDensity) const;

                protected:

                    bool LoadColorMap();
                    bool LoadDensity(const Mathematics::_3D::Density::CDensityPrimitive::ConditioningMode ConditioningMode, const Mathematics::_3D::Density::CDensityAggregation* pDensityAggregation);
                    bool MapCellsToCellsDirectLinear();
                    bool GetPartialDescriptiveStatistics(real& MaximalDensity, real& MinimalDensity) const;

                    const CRGBAColorMap* m_pColorMap;
                    real m_DiscretizationResolution;
                    uint8_t* m_pDiscreteDensityCells;
                    real* m_pContinousDensityCells;
                    SoVolumeData* m_pSoVolumeData;
                    SoTransferFunction* m_pSoTransferFunction;
                    SoVolumeRender* m_pSoVolumeRender;
                    CContinuousBoundingBox3D m_BoundingBox;
                    uint m_Width;
                    uint m_Height;
                    uint m_Depth;
                    uint m_SlideCells;
                    uint m_TotalCells;
                    real m_MaximalDensity;
                    real m_MinimalDensity;
                };
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_SIMVOLEON_*/

