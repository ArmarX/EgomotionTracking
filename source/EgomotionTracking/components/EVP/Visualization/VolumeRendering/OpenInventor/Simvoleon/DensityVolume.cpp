/*
 * DensityVolume.cpp
 *
 *  Created on: Apr 2, 2012
 *      Author: david
 */

#include "DensityVolume.h"

#ifdef _USE_OPEN_INVENTOR_SIMVOLEON_

namespace EVP
{
    namespace Visualization
    {
        namespace VolumeRendering
        {
            namespace Simvoleon
            {

                real CDensityVolume::DetermineDiscretizationResolution(const CContinuousBoundingBox3D& BoundingBox, const uint TotalDiscretizationCells)
                {
                    return RealPow(BoundingBox.GetVolume() / real(TotalDiscretizationCells), _REAL_THIRD_);
                }

                uint CDensityVolume::DetermineTotalDiscretizationCells(const CContinuousBoundingBox3D& BoundingBox, const real DiscretizationResolution)
                {
                    return uint(BoundingBox.GetWidth() / DiscretizationResolution) * uint(BoundingBox.GetHeight() / DiscretizationResolution) * uint(BoundingBox.GetDepth() / DiscretizationResolution);
                }

                CDensityVolume::CDensityVolume(const CRGBAColorMap* pColorMap) :
                    Geometry::_3D::OpenInventor::CVisualizationPrimitive(Geometry::_3D::OpenInventor::CVisualizationPrimitive::eVolumeRendering), m_pColorMap(pColorMap), m_DiscretizationResolution(_REAL_ZERO_), m_pDiscreteDensityCells(NULL), m_pContinousDensityCells(NULL), m_pSoVolumeData(NULL), m_pSoTransferFunction(NULL), m_pSoVolumeRender(NULL), m_Width(0), m_Height(0), m_Depth(0), m_SlideCells(0), m_TotalCells(0), m_MaximalDensity(_REAL_ZERO_), m_MinimalDensity(_REAL_ZERO_)
                {
                    m_pSoVolumeData = new SoVolumeData;
                    m_pBaseSeparator->addChild(m_pSoVolumeData);
                    m_pSoTransferFunction = new SoTransferFunction;
                    m_pBaseSeparator->addChild(m_pSoTransferFunction);
                    m_pSoVolumeRender = new SoVolumeRender;
                    m_pBaseSeparator->addChild(m_pSoVolumeRender);
                    LoadColorMap();
                    SwitchVisibility(false);
                }

                CDensityVolume::~CDensityVolume()
                {
                    RELEASE_ARRAY(m_pDiscreteDensityCells)
                    RELEASE_ARRAY(m_pContinousDensityCells)
                }

                bool CDensityVolume::SetDensityAggregation(const Mathematics::_3D::Density::CDensityPrimitive::ConditioningMode ConditioningMode, const Mathematics::_3D::Density::CDensityAggregation* pDensityAggregation, const real MinimalDensity, const real DiscretizationResolution)
                {
                    if (pDensityAggregation && pDensityAggregation->GetTotalDensityPrimitives())
                    {
                        const CContinuousBoundingBox3D BoundingBox = pDensityAggregation->GetDensityBoundingBox(ConditioningMode, MinimalDensity);
                        if (!BoundingBox.IsEmpty())
                            if (SetConfiguration(BoundingBox, DiscretizationResolution))
                                if (LoadDensity(ConditioningMode, pDensityAggregation))
                                {
                                    return MapCellsToCellsDirectLinear();
                                }
                    }
                    return false;
                }

                bool CDensityVolume::SetColorMap(const CRGBAColorMap* pColorMap)
                {
                    m_pColorMap = pColorMap;
                    return LoadColorMap();
                }

                bool CDensityVolume::SetConfiguration(const CContinuousBoundingBox3D& BoundingBox, const real DiscretizationResolution)
                {
                    if ((!BoundingBox.IsEmpty()) && (DiscretizationResolution > _REAL_EPSILON_))
                    {
                        RELEASE_ARRAY(m_pDiscreteDensityCells)
                        RELEASE_ARRAY(m_pContinousDensityCells)
                        m_BoundingBox = BoundingBox;
                        m_DiscretizationResolution = DiscretizationResolution;
                        m_Width = uint(m_BoundingBox.GetWidth() / m_DiscretizationResolution);
                        m_Height = uint(m_BoundingBox.GetHeight() / m_DiscretizationResolution);
                        m_Depth = uint(m_BoundingBox.GetDepth() / m_DiscretizationResolution);
                        m_SlideCells = m_Width * m_Height;
                        m_TotalCells = m_SlideCells * m_Depth;
                        if (m_TotalCells)
                        {
                            m_pDiscreteDensityCells = new uint8_t[m_TotalCells];
                            m_pContinousDensityCells = new real[m_TotalCells];
                            if (m_pDiscreteDensityCells && m_pContinousDensityCells)
                            {
                                memset(m_pDiscreteDensityCells, 0, m_TotalCells * sizeof(uint8_t));
                                memset(m_pContinousDensityCells, 0, m_TotalCells * sizeof(real));
                                m_pSoVolumeData->setVolumeData(SbVec3s(m_Width, m_Height, m_Depth), m_pDiscreteDensityCells, SoVolumeData::UNSIGNED_BYTE);
                                real X0 = _REAL_ZERO_, Y0 = _REAL_ZERO_, Z0 = _REAL_ZERO_, X1 = _REAL_ZERO_, Y1 = _REAL_ZERO_, Z1 = _REAL_ZERO_;
                                BoundingBox.GetBounds(X0, Y0, Z0, X1, Y1, Z1);
                                m_pSoVolumeData->setVolumeSize(SbBox3f(X0, Y0, Z0, X1, Y1, Z1));
                                SwitchVisibility(true);
                                return true;
                            }
                            else
                            {
                                RELEASE_ARRAY(m_pDiscreteDensityCells)
                                RELEASE_ARRAY(m_pContinousDensityCells)
                            }
                        }
                    }
                    return false;
                }

                bool CDensityVolume::ClearDensityCellValues()
                {
                    if (m_pDiscreteDensityCells && m_pContinousDensityCells)
                    {
                        memset(m_pDiscreteDensityCells, 0, m_TotalCells * sizeof(uint8_t));
                        memset(m_pContinousDensityCells, 0, m_TotalCells * sizeof(real));
                        m_MaximalDensity = _REAL_ZERO_;
                        m_MinimalDensity = _REAL_ZERO_;
                        return true;
                    }
                    return false;
                }

                void CDensityVolume::Update(const bool FullUpdate)
                {
                    if (FullUpdate)
                    {
                        LoadColorMap();
                    }
                    m_pSoVolumeData->touch();
                }

                bool CDensityVolume::LoadColorMap()
                {
                    if (m_pColorMap)
                    {
                        real Position = _REAL_ZERO_;
                        for (uint i = 0, j = 0; i < 256; ++i, Position += _REAL_1_255_)
                        {
                            const CRGBAColor Color = m_pColorMap->GetColor(Position);
                            m_pSoTransferFunction->colorMap.set1Value(j++, Color.GetNormalizedRedChannel());
                            m_pSoTransferFunction->colorMap.set1Value(j++, Color.GetNormalizedGreenChannel());
                            m_pSoTransferFunction->colorMap.set1Value(j++, Color.GetNormalizedBlueChannel());
                            m_pSoTransferFunction->colorMap.set1Value(j++, Color.GetNormalizedAlphaChannel());
                        }
                        m_pSoTransferFunction->predefColorMap = SoTransferFunction::NONE;
                        m_pSoTransferFunction->colorMapType = SoTransferFunction::RGBA;
                        return true;
                    }
                    else
                    {
                        m_pSoTransferFunction->predefColorMap = SoTransferFunction::TEMPERATURE;
                        m_pSoTransferFunction->colorMapType = SoTransferFunction::RGBA;
                        return false;
                    }
                }

                bool CDensityVolume::LoadDensity(const Mathematics::_3D::Density::CDensityPrimitive::ConditioningMode ConditioningMode, const Mathematics::_3D::Density::CDensityAggregation* pDensityAggregation)
                {
                    const Mathematics::_3D::CVector3D Basis = m_BoundingBox.GetMinPoint();
                    const Mathematics::_3D::CVector3D Delta = GetCellDelta();
                    const real Lx = Basis.GetX();
                    const real Ly = Basis.GetY();
                    const real Dx = Delta.GetX();
                    const real Dy = Delta.GetY();
                    const real Dz = Delta.GetZ();
                    real* pContinousDensityCell = m_pContinousDensityCells;
                    Mathematics::_3D::CVector3D Location = Basis;
                    m_MaximalDensity = pDensityAggregation->GetDensity(ConditioningMode, Location);
                    m_MinimalDensity = m_MaximalDensity;
                    for (uint Z = 0; Z < m_Depth; ++Z, Location.SetY(Ly), Location.AddOffsetZ(Dz))
                        for (uint Y = 0; Y < m_Height; ++Y, Location.SetX(Lx), Location.AddOffsetY(Dy))
                            for (uint X = 0; X < m_Width; ++X, Location.AddOffsetX(Dx))
                            {
                                const real Density = pDensityAggregation->GetDensity(ConditioningMode, Location);
                                *pContinousDensityCell++ = Density;
                                if (Density > m_MaximalDensity)
                                {
                                    m_MaximalDensity = Density;
                                }
                                else if (Density < m_MinimalDensity)
                                {
                                    m_MinimalDensity = Density;
                                }
                            }
                    return (m_MaximalDensity - m_MinimalDensity) > _REAL_EPSILON_;
                }

                bool CDensityVolume::GetDescriptiveStatistics(real& MeanDensity, real& StandardDeviationDensity, real& MaximalDensity, real& MinimalDensity) const
                {
                    if (m_pContinousDensityCells)
                    {
                        const real* pContinousDensityCell = m_pContinousDensityCells;
                        const real* const pContinousDensityCellFinal = m_pContinousDensityCells + m_TotalCells;
                        real Accumulator = _REAL_ZERO_, SquareAccumulator = _REAL_ZERO_;
                        MaximalDensity = *pContinousDensityCell;
                        MinimalDensity = MaximalDensity;
                        while (pContinousDensityCell < pContinousDensityCellFinal)
                        {
                            const real Density = *pContinousDensityCell++;
                            Accumulator += Density;
                            SquareAccumulator += Density * Density;
                            if (Density > MaximalDensity)
                            {
                                MaximalDensity = Density;
                            }
                            else if (Density < MinimalDensity)
                            {
                                MinimalDensity = Density;
                            }
                        }
                        MeanDensity = Accumulator / real(m_TotalCells);
                        StandardDeviationDensity = RealSqrt((SquareAccumulator / real(m_TotalCells)) - (MeanDensity * MeanDensity));
                        return (MaximalDensity - MinimalDensity) > _REAL_EPSILON_;
                    }
                    return false;
                }

                uint CDensityVolume::GetDensityCellsWidth() const
                {
                    return m_Width;
                }

                uint CDensityVolume::GetDensityCellsHeight() const
                {
                    return m_Height;
                }

                uint CDensityVolume::GetDensityCellsDepth() const
                {
                    return m_Depth;
                }

                uint CDensityVolume::GetTotalSlideDensityCells() const
                {
                    return m_SlideCells;
                }

                uint CDensityVolume::GetTotalDensityCells() const
                {
                    return m_TotalCells;
                }

                real CDensityVolume::GetDiscretizationResolution() const
                {
                    return m_DiscretizationResolution;
                }

                const real* CDensityVolume::GetReadOnlyDensityCells() const
                {
                    return m_pContinousDensityCells;
                }

                real* CDensityVolume::GetWritableDensityCells()
                {
                    return m_pContinousDensityCells;
                }

                real CDensityVolume::GetDensity(const uint X, const uint Y, const uint Z)
                {
                    if (m_pContinousDensityCells && (X < m_Width) && (Y < m_Height) && (Z < m_Depth))
                    {
                        return m_pContinousDensityCells[X + Y * m_Width + Z * m_SlideCells];
                    }
                    return _REAL_MIN_;
                }

                bool CDensityVolume::SetDensity(const uint X, const uint Y, const uint Z, const real Density)
                {
                    if (m_pContinousDensityCells && (X < m_Width) && (Y < m_Height) && (Z < m_Depth))
                    {
                        m_pContinousDensityCells[X + Y * m_Width + Z * m_SlideCells] = Density;
                        return true;
                    }
                    return false;
                }

                const CContinuousBoundingBox3D& CDensityVolume::GetBoundingBox() const
                {
                    return m_BoundingBox;
                }

                Mathematics::_3D::CVector3D CDensityVolume::GetBasePoint() const
                {
                    return m_BoundingBox.GetMinPoint();
                }

                Mathematics::_3D::CVector3D CDensityVolume::GetCellDelta() const
                {
                    if (m_BoundingBox.IsEmpty())
                    {
                        return Mathematics::_3D::CVector3D::s_Point_At_Origin_3D;
                    }
                    Mathematics::_3D::CVector3D Diagonal = m_BoundingBox.GetMaxPoint() - m_BoundingBox.GetMinPoint();
                    Diagonal.ScaleAnisotropic(m_Width, m_Height, m_Depth);
                    return Diagonal;
                }

                bool CDensityVolume::MapCellsToCellsAutoScaleLinear()
                {
                    if (m_pDiscreteDensityCells && m_pContinousDensityCells && GetPartialDescriptiveStatistics(m_MaximalDensity, m_MinimalDensity))
                    {
                        const real RangeDensity = m_MaximalDensity - m_MinimalDensity;
                        const real ScaleDensity = _REAL_255_ / RangeDensity;
                        const real* pContinousDensityCell = m_pContinousDensityCells;
                        const real* const pContinousDensityCellFinal = m_pContinousDensityCells + m_TotalCells;
                        uint8_t* pDiscreteDensityCell = m_pDiscreteDensityCells;
                        while (pContinousDensityCell < pContinousDensityCellFinal)
                        {
                            *pDiscreteDensityCell++ = uint8_t((*pContinousDensityCell++ - m_MinimalDensity) * ScaleDensity + _REAL_HALF_);
                        }
                        m_pSoVolumeData->touch();
                        return true;
                    }
                    return false;
                }

                bool CDensityVolume::MapCellsToVoxelsClamppingLinear(const real MaximalDensity, const real MinimalDensity)
                {
                    if (m_pDiscreteDensityCells && m_pContinousDensityCells && (MaximalDensity > MinimalDensity))
                    {
                        const real RangeDensity = MaximalDensity - MinimalDensity;
                        if (RangeDensity > _REAL_EPSILON_)
                        {
                            m_MaximalDensity = MaximalDensity;
                            m_MinimalDensity = MinimalDensity;
                            const real ScaleDensity = _REAL_255_ / RangeDensity;
                            const real* pContinousDensityCell = m_pContinousDensityCells;
                            const real* const pContinousDensityCellFinal = m_pContinousDensityCells + m_TotalCells;
                            uint8_t* pDiscreteDensityCell = m_pDiscreteDensityCells;
                            while (pContinousDensityCell < pContinousDensityCellFinal)
                            {
                                const real Density = *pContinousDensityCell++;
                                if (Density >= m_MaximalDensity)
                                {
                                    *pDiscreteDensityCell++ = 255;
                                }
                                else if (Density <= m_MinimalDensity)
                                {
                                    *pDiscreteDensityCell++ = 0;
                                }
                                else
                                {
                                    *pDiscreteDensityCell++ = uint8_t((Density - m_MinimalDensity) * ScaleDensity + _REAL_HALF_);
                                }
                            }
                            m_pSoVolumeData->touch();
                            return true;
                        }
                    }
                    return false;
                }

                bool CDensityVolume::MapCellsToVoxelsDirectLinear(const real MaximalDensity, const real MinimalDensity)
                {
                    if (m_pDiscreteDensityCells && m_pContinousDensityCells)
                    {
                        const real RangeDensity = MaximalDensity - MinimalDensity;
                        if (RangeDensity > _REAL_EPSILON_)
                        {
                            m_MaximalDensity = MaximalDensity;
                            m_MinimalDensity = MinimalDensity;
                            const real ScaleDensity = _REAL_255_ / RangeDensity;
                            const real* pContinousDensityCell = m_pContinousDensityCells;
                            uint8_t* pDiscreteDensityCell = m_pDiscreteDensityCells;
                            const real* const pContinousDensityCellFinal = m_pContinousDensityCells + m_TotalCells;
                            while (pContinousDensityCell < pContinousDensityCellFinal)
                            {
                                *pDiscreteDensityCell++ = uint8_t((*pContinousDensityCell++ - m_MinimalDensity) * ScaleDensity + _REAL_HALF_);
                            }
                            m_pSoVolumeData->touch();
                            return true;
                        }
                    }
                    return false;
                }

                bool CDensityVolume::HideSubspace(const CContinuousBoundingBox3D& Subspace)
                {
                    CContinuousBoundingBox3D IntersectionSubspace;
                    if (m_BoundingBox.GetIntersection(Subspace, IntersectionSubspace))
                    {
                        MapCellsToCellsDirectLinear();
                        const Mathematics::_3D::CVector3D A = IntersectionSubspace.GetMinPoint() - m_BoundingBox.GetMinPoint();
                        const Mathematics::_3D::CVector3D B = IntersectionSubspace.GetMaxPoint() - m_BoundingBox.GetMinPoint();
                        const uint X0 = uint(RealCeil(A.GetX() / m_DiscretizationResolution));
                        const uint Y0 = uint(RealCeil(A.GetY() / m_DiscretizationResolution));
                        const uint Z0 = uint(RealCeil(A.GetZ() / m_DiscretizationResolution));
                        const uint X1 = TMin(m_Width - 1, uint(RealFloor(B.GetX() / m_DiscretizationResolution)));
                        const uint Y1 = TMin(m_Height - 1, uint(RealFloor(B.GetY() / m_DiscretizationResolution)));
                        const uint Z1 = TMin(m_Depth - 1, uint(RealFloor(B.GetZ() / m_DiscretizationResolution)));
                        if ((X1 >= X0) && (Y1 >= Y0) && (Z1 >= Z0))
                        {
                            const uint BlockSize = sizeof(uint8_t) * (X1 - X0 + 1);
                            uint8_t* pDiscreteDensityCellBase = m_pDiscreteDensityCells + X0 + Y0 * m_Width;
                            for (uint Z = Z0; Z <= Z1; ++Z, pDiscreteDensityCellBase += m_SlideCells)
                            {
                                uint8_t* pDiscreteDensityCell = pDiscreteDensityCellBase;
                                for (uint Y = Y0; Y <= Y1; ++Y, pDiscreteDensityCell += m_Width)
                                {
                                    memset(pDiscreteDensityCell, 0, BlockSize);
                                }
                            }
                            m_pSoVolumeData->touch();
                            return true;
                        }
                    }
                    return false;
                }

                void CDensityVolume::ShowFullSpace()
                {
                    MapCellsToCellsDirectLinear();
                }

                real CDensityVolume::GetMaximalDensity() const
                {
                    return m_MaximalDensity;
                }

                real CDensityVolume::GetMinimalDensity() const
                {
                    return m_MinimalDensity;
                }

                bool CDensityVolume::MapCellsToCellsDirectLinear()
                {
                    const real RangeDensity = m_MaximalDensity - m_MinimalDensity;
                    if (RangeDensity > _REAL_EPSILON_)
                    {
                        const real ScaleDensity = _REAL_255_ / RangeDensity;
                        const real* pContinousDensity = m_pContinousDensityCells;
                        const real* const pContinousDensityCellFinal = m_pContinousDensityCells + m_TotalCells;
                        uint8_t* pDiscreteDensityCell = m_pDiscreteDensityCells;
                        while (pContinousDensity < pContinousDensityCellFinal)
                        {
                            *pDiscreteDensityCell++ = uint8_t((*pContinousDensity++ - m_MinimalDensity) * ScaleDensity + _REAL_HALF_);
                        }
                        m_pSoVolumeData->touch();
                        return true;
                    }
                    return false;
                }

                bool CDensityVolume::GetPartialDescriptiveStatistics(real& MaximalDensity, real& MinimalDensity) const
                {
                    if (m_pContinousDensityCells)
                    {
                        const real* pContinousDensityCell = m_pContinousDensityCells;
                        const real* const pContinousDensityCellFinal = m_pContinousDensityCells + m_TotalCells;
                        MaximalDensity = *pContinousDensityCell;
                        MinimalDensity = MaximalDensity;
                        while (pContinousDensityCell < pContinousDensityCellFinal)
                        {
                            const real Density = *pContinousDensityCell++;
                            if (Density > MaximalDensity)
                            {
                                MaximalDensity = Density;
                            }
                            else if (Density < MinimalDensity)
                            {
                                MinimalDensity = Density;
                            }
                        }
                        return (MaximalDensity - MinimalDensity) > _REAL_EPSILON_;
                    }
                    return false;
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_SIMVOLEON_*/
