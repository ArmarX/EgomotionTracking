/*
 * Simovoleon.h
 *
 *  Created on: Apr 2, 2012
 *      Author: david
 */

#pragma once

#include "../../../../GlobalSettings.h"

#ifdef _USE_OPEN_INVENTOR_SIMVOLEON_

#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>

#endif /* _USE_OPEN_INVENTOR_SIMVOLEON_*/

