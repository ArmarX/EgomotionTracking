/*
 * VisualizationPrimitive.h
 *
 *  Created on: Feb 28, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

//OPEN_INVENTOR
#include "OpenInventor.h"

//EVP
#include "../../../../Foundation/Miscellaneous/IdentifiableInstance.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    //TODO REMOVE IT FROM EVP::Visualization::Geometry::_3D::OpenInventor to just EVP::Visualization::OpenInventor
                    class CVisualizationPrimitive: public CIdentifiableInstance
                    {
                        IDENTIFIABLE

                    public:

                        enum VisualizationTypeId
                        {
                            ecoordinateSystem, eVisualizationVolume, eGeometricPrimitive, eVolumeRendering, eIsoSurface
                        };

                        enum VisualizationPredefinedColor
                        {
                            eBlack, eGray1_4, eGray1_2, eGray3_4, eWhite, eRed, eRed3_4, eRed1_2, eRed1_4, eGreen, eGreen3_4, eGreen1_2, eGreen1_4, eBlue, eBlue3_4, eBlue1_2, eBlue1_4, eYellow, eYellow3_4, eYellow1_2, eYellow1_4, eCyan, eCyan3_4, eCyan1_2, eCyan1_4, eMagenta, eMagenta3_4, eMagenta1_2, eMagenta1_4
                        };

                        static  SbColor GetPredefinedColor(CVisualizationPrimitive::VisualizationPredefinedColor Color);

                        CVisualizationPrimitive(const VisualizationTypeId TypeId);
                        ~CVisualizationPrimitive() override;

                        VisualizationTypeId GetVisualizationTypeId() const;

                        SoSwitch* GetBaseSwitch() const;

                        virtual void SetVisible(const bool Visible);
                        virtual  bool GetVisible() const;

                        virtual void SetColor(const CVisualizationPrimitive::VisualizationPredefinedColor Color);
                        virtual void SetColor(const byte R, const byte G, const byte B);
                        virtual void SetColor(const vreal R, const vreal G, const vreal B);
                        virtual void SetColor(const SbColor& Color);
                        virtual const SbColor& GetColor() const;

                        virtual void SetTransparency(const vreal Transparency);
                        virtual  vreal GetTransparency() const;

                        virtual void SetDrawStyle(const SoDrawStyle::Style Style);
                        virtual  SoDrawStyle::Style GetDrawStyle() const;

                        virtual void SetLineWidth(const vreal LineWidth);
                        virtual  vreal GetLineWidth() const;

                        virtual void SetPointSize(const vreal PointSize);
                        virtual  vreal GetPointSize() const;

                        virtual void SetLinePattern(const ushort Pattern);
                        virtual  ushort GetLinePattern() const;

                        virtual void SetComplexityType(const SoComplexity::Type Type);
                        virtual  SoComplexity::Type GetComplexityType() const;

                        virtual void SetComplexityValue(const vreal Complexity);
                        virtual  vreal GetComplexityValue() const;

                        virtual void Update(const bool FullUpdate) = 0;

                    protected:

                        void SwitchVisibility(const bool Visible);

                        bool m_IsVisible;
                        const VisualizationTypeId m_VisualizationTypeId;
                        SoSwitch* m_pBaseSwitch;
                        SoSeparator* m_pBaseSeparator;
                        SoDrawStyle* m_pBaseDrawStyle;
                        SoMaterial* m_pBaseMaterial;
                        SoComplexity* m_pBaseComplexity;
                    };
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
