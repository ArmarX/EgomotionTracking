/*
 * VisualizationPointPair.h
 *
 *  Created on: Feb 23, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

//OPEN_INVENTOR
#include "OpenInventor.h"

//GEOMETRY
#include "../../../../Foundation/Mathematics/3D/Geometry/PointPair3D.h"

//VISUALIZATION
#include "VisualizationGeometricPrimitive.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    class CVisualizationPointPair: public CVisualizationGeometricPrimitive
                    {
                    public:

                        CVisualizationPointPair(Mathematics::_3D::Geometry::CPointPair3D* pPointPair, CVisualizationVolume* pVisualizationVolume);
                        ~CVisualizationPointPair() override;

                        void SetDisplayRadius(const vreal DisplayRadius);
                        vreal GetDisplayaRadius() const;

                        void SetConnectingLineVisible(const bool Visible);
                        bool GetConnectingLineVisible() const;

                        void SetConnectingLineWidth(const vreal LineWidth);
                        vreal GetConnectingLineWidth() const;

                        void SetConnectingLinePattern(const ushort Pattern);
                        ushort GetConnectingLinePattern() const;

                        void Update(const bool FullUpdate) override;

                    protected:

                        bool IsComposedVisible() override;

                        bool m_ConnectingLineVisible;
                        SoSwitch* m_pConnectingLineSwitch;
                        SoDrawStyle* m_pConnectingLineDrawStyle;
                        SoCoordinate3* m_pConnectingLineCoordinate3;
                        SoLineSet* m_pConnectingLineSet;
                        SoTranslation* m_pSoTranslationA;
                        SoTranslation* m_pSoTranslationB;
                        SoSwitch* m_pSphereASwitch;
                        SoSwitch* m_pSphereBSwitch;
                        SoSphere* m_pSoSphereA;
                        SoSphere* m_pSoSphereB;
                    };
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
