/*
 * VisualizationPoint.cpp
 *
 *  Created on: Feb 23, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "VisualizationPoint.h"
#include "VisualizationVolume.h"

#ifdef _USE_OPEN_INVENTOR_

using namespace EVP::Mathematics::_3D::Geometry;

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    CVisualizationPoint::CVisualizationPoint(CPoint3D* pPoint, CVisualizationVolume* pVisualizationVolume) :
                        CVisualizationGeometricPrimitive(pPoint, pVisualizationVolume)
                    {
                        m_pSoTranslation = new SoTranslation;
                        m_pBaseSeparator->addChild(m_pSoTranslation);
                        m_pSoSphere = new SoSphere;
                        m_pBaseSeparator->addChild(m_pSoSphere);
                        m_pSoSphere->setUserData(this);
                        m_pSoSphere->radius = _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_;
                        Update(true);
                    }

                    CVisualizationPoint::~CVisualizationPoint()
                        = default;

                    void CVisualizationPoint::SetDisplayRadius(const vreal DisplayRadius)
                    {
                        const vreal InRangeDisplayRadius = TMin(TMax(DisplayRadius, _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_), _VISUALIZATION_POINT_MAXIMAL_DISPLAY_RADIUS_);
                        if (m_pSoSphere->radius.getValue() != InRangeDisplayRadius)
                        {
                            m_pSoSphere->radius = InRangeDisplayRadius;
                        }
                    }

                    vreal CVisualizationPoint::GetDisplayaRadius() const
                    {
                        return m_pSoSphere->radius.getValue();
                    }

                    bool CVisualizationPoint::IsComposedVisible()
                    {
                        if (m_IsVisible && m_pGeometricPrimitive)
                        {
                            if (m_pVisualizationVolume)
                            {
                                return m_pVisualizationVolume->MapPoint(T(((CPoint3D*) m_pGeometricPrimitive)->GetPoint()));
                            }
                            return true;
                        }
                        return false;
                    }

                    void CVisualizationPoint::Update(const bool FullUpdate)
                    {
                        if (FullUpdate && m_pGeometricPrimitive)
                        {
                            m_pSoTranslation->translation.setValue(T(((const CPoint3D*) m_pGeometricPrimitive)->GetPoint()));
                        }
                        SwitchVisibility(IsComposedVisible());
                    }
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
