/*
 * VisualizationCircle.cpp
 *
 *  Created on: Feb 23, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */
#include "VisualizationCircle.h"
#include "VisualizationVolume.h"

#ifdef _USE_OPEN_INVENTOR_

using namespace EVP::Mathematics::_3D::Geometry;

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    CVisualizationCircle::CVisualizationCircle(CCircle3D* pCircle, CVisualizationVolume* pVisualizationVolume) :
                        CVisualizationGeometricPrimitive(pCircle, pVisualizationVolume)
                    {
                        m_pSoCoordinate3 = new SoCoordinate3;
                        m_pBaseSeparator->addChild(m_pSoCoordinate3);
                        m_pSoLineSet = new SoLineSet;
                        m_pBaseSeparator->addChild(m_pSoLineSet);
                        m_pSoLineSet->setUserData(this);
                        m_pBaseDrawStyle->lineWidth = _VISUALIZATION_CIRCLE_DEFAULT_LINE_WIDTH_;
                        Update(true);
                    }

                    CVisualizationCircle::~CVisualizationCircle()
                        = default;

                    bool CVisualizationCircle::IsComposedVisible()
                    {
                        if (m_IsVisible && m_pGeometricPrimitive)
                        {
                            if (m_pVisualizationVolume)
                            {
                                CCircle3D* pCircle = (CCircle3D*) m_pGeometricPrimitive;
                                return m_pVisualizationVolume->MapCircle(T(pCircle->GetNormal()), T(pCircle->GetCenter()), T(pCircle->GetRadius()));
                            }
                            return true;
                        }
                        return false;
                    }

                    void CVisualizationCircle::Update(const bool FullUpdate)
                    {
                        if (FullUpdate)
                        {
                            GenerateCircle();
                        }
                        SwitchVisibility(IsComposedVisible());
                    }

                    void CVisualizationCircle::GenerateCircle()
                    {
                        if (m_pGeometricPrimitive)
                        {
                            const CCircle3D* pCircle = (const CCircle3D*) m_pGeometricPrimitive;
                            const SbVec3f& Normal = T(pCircle->GetNormal());
                            const SbVec3f& Center = T(pCircle->GetCenter());
                            const vreal Radius = T(pCircle->GetRadius());
                            uint TotalSegments = 0;
                            switch (SoComplexity::Type(m_pBaseComplexity->type.getValue()))
                            {
                                case SoComplexity::OBJECT_SPACE:
                                case SoComplexity::SCREEN_SPACE:
                                    TotalSegments = TMax(_VISUALIZATION_CIRCLE_MINIMAL_SEGMENT_LINES_, RoundToInteger(vreal(_VISUALIZATION_CIRCLE_MAXIMAL_SEGMENT_LINES_) * m_pBaseComplexity->value.getValue()));
                                    break;
                                case SoComplexity::BOUNDING_BOX:
                                    TotalSegments = _VISUALIZATION_CIRCLE_MINIMAL_SEGMENT_LINES_;
                                    break;
                            }
                            if (Radius < _VISUALIZATION_EPSILON_)
                            {
                                TotalSegments = 3;
                            }
                            const vreal Delta = _REAL_2PI_ / vreal(TotalSegments);
                            const SbRotation Rotation(SbVec3f(vreal(0.0), vreal(0.0), vreal(1.0)), Normal);
                            uint Index = 0;
                            SbVec3f Point;
                            m_pSoLineSet->numVertices = 0;
                            m_pSoCoordinate3->point.deleteValues(0);
                            for (vreal Alpha = _REAL_ZERO_; Alpha <= _REAL_2PI_; Alpha += Delta)
                            {
                                Point.setValue(RealCosinus(Alpha) * Radius, RealSinus(Alpha) * Radius, _REAL_ZERO_);
                                Rotation.multVec(Point, Point);
                                m_pSoCoordinate3->point.set1Value(Index++, Point + Center);
                            }
                            m_pSoCoordinate3->point.set1Value(Index++, m_pSoCoordinate3->point[0]);
                            m_pSoLineSet->numVertices.setValue(Index);
                        }
                    }
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
