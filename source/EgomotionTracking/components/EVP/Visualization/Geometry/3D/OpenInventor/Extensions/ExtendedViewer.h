/*
 * CExtendedViewer.h
 *
 *  Created on: Apr 15, 2012
 *      Author: david
 */

#pragma once

#include "../../../../../GlobalSettings.h"

#ifdef _USE_OPEN_INVENTOR_

//COIN
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

//EVP
#include "../OpenInventor.h"
#include "../../../../../VisualSpace/Common/Image.h"
#include "../../../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../../../Foundation/DataTypes/DiscreteRGBAPixel.h"

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    namespace Extensions
                    {
                        class CExtendedViewer: public SoQtExaminerViewer
                        {
                        public:

                            CExtendedViewer(QWidget* pParent);
                            ~CExtendedViewer() override;

                            void SetSourceImage(const TImage<byte>* pIntensitymage);
                            void SetSourceImage(const TImage<CDiscreteTristimulusPixel>* pRGBImage);
                            void SetSourceImage(const TImage<CDiscreteRGBAPixel>* pRGBAImage);

                            bool Update();

                        protected:


                            void actualRedraw() override;

                            struct BackGroundImage
                            {
                                enum Type
                                {
                                    eUnknown = 0, eIntensity = 1, eRGB = 3, eRGBA = 4
                                };
                                union SourcePointer
                                {
                                    const TImage<byte>* m_pIntensityImage;
                                    const TImage<CDiscreteTristimulusPixel>* m_pRGBImage;
                                    const TImage<CDiscreteRGBAPixel>* m_pRGBAImage;
                                };
                                union DestinyPointer
                                {
                                    TImage<byte>* m_pIntensityImage;
                                    TImage<CDiscreteTristimulusPixel>* m_pRGBImage;
                                    TImage<CDiscreteRGBAPixel>* m_pRGBAImage;
                                };
                                Type m_Type;
                                SourcePointer m_Source;
                                DestinyPointer m_Destiny;
                            };

                            SoSeparator* m_SeparatorBackGround;
                            SoImage* m_pImage;
                            BackGroundImage m_BackGroundImage;
                        };
                    }
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
