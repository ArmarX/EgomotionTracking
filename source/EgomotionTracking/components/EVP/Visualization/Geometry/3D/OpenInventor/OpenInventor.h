/*
 * OpenInventor.h
 *
 *  Created on: 22.09.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"

#ifdef _USE_OPEN_INVENTOR_
#ifndef _VISUALIZATION_EPSILON_

//OPEN INVENTOR COIN INCLUDES
#include <Inventor/SbString.h>
#include <Inventor/SbLinear.h>
#include <Inventor/SbBox3f.h>
#include <Inventor/SbColor.h>
#include <Inventor/nodes/SoNormal.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoIndexedLineSet.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoComplexity.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoExtSelection.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>

//EVP INCLUDES
#include "../../../../Foundation/Mathematics/3D/Vector3D.h"

//DEFINITIONS
#define _VISUALIZATION_EPSILON_                                     0.001f
#define _VISUALIZATION_PRIMITIVE_DEFAULT_COMPLEXITY_VALUE_          EVP::Visualization::Geometry::_3D::OpenInventor::vreal(1.0)
#define _VISUALIZATION_SPHERE_DEFAULT_TRANSPARENCY_                 EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.75)
#define _VISUALIZATION_CIRCLE_MINIMAL_SEGMENT_LINES_                6
#define _VISUALIZATION_CIRCLE_MAXIMAL_SEGMENT_LINES_                120
#define _VISUALIZATION_CIRCLE_DEFAULT_LINE_WIDTH_                   EVP::Visualization::Geometry::_3D::OpenInventor::vreal(1.5)
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y0Z0_               0
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y0Z0_               1
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y0Z1_               2
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y0Z1_               3
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y1Z0_               4
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y1Z0_               5
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y1Z1_               6
#define _VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y1Z1_               7
#define _VISUALIZATION_VOLUME_TOTAL_BOUNDING_VERTEX_X0Y1Z1_         8
#define _VISUALIZATION_VOLUME_BOUNDING_PLANE_XY0_                   0
#define _VISUALIZATION_VOLUME_BOUNDING_PLANE_XY1_                   1
#define _VISUALIZATION_VOLUME_BOUNDING_PLANE_XZ0_                   2
#define _VISUALIZATION_VOLUME_BOUNDING_PLANE_XZ1_                   3
#define _VISUALIZATION_VOLUME_BOUNDING_PLANE_YZ0_                   4
#define _VISUALIZATION_VOLUME_BOUNDING_PLANE_YZ1_                   5
#define _VISUALIZATION_VOLUME_TOTAL_BOUNDING_PLANES_                6
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X0Y0Z0_X1Y0Z0_          0
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X1Y0Z0_X1Y1Z0_          1
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X1Y1Z0_X0Y1Z0_          2
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X0Y1Z0_X0Y0Z0_          3
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X0Y0Z1_X1Y0Z1_          4
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X1Y0Z1_X1Y1Z1_          5
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X1Y1Z1_X0Y1Z1_          6
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X0Y1Z1_X0Y0Z1_          7
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X0Y0Z0_X0Y0Z1_          8
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X1Y0Z0_X1Y0Z1_          9
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X1Y1Z0_X1Y1Z1_          10
#define _VISUALIZATION_VOLUME_BOUNDING_LINE_X0Y1Z0_X0Y1Z1_          11
#define _VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_                 12
#define _VISUALIZATION_VOLUME_TOTAL_BOUNDING_VERTICES_              8
#define _VISUALIZATION_VOLUME_DEFAULT_LINE_COLOR_                   EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0),EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0),EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0)
#define _VISUALIZATION_VOLUME_DEFAULT_LINE_WIDTH_                   EVP::Visualization::Geometry::_3D::OpenInventor::vreal(1.0)
#define _VISUALIZATION_VOLUME_DEFAULT_LINE_PATTERN_                 0X3333
#define _VISUALIZATION_VOLUME_DEFAULT_TRANSPARENCY_                 EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.5)
#define _VISUALIZATION_POINT_PAIR_MINIMAL_DISPLAY_RADIUS_           EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.5)
#define _VISUALIZATION_POINT_PAIR_MAXIMAL_DISPLAY_RADIUS_           EVP::Visualization::Geometry::_3D::OpenInventor::vreal(10.0)
#define _VISUALIZATION_POINT_PAIR_DEFAULT_CONNECTING_LINE_PATTERN_  0X3333
#define _VISUALIZATION_POINT_PAIR_DEFAULT_CONNECTING_LINE_WIDTH_    EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.5)
#define _VISUALIZATION_POINT_PAIR_DEFAULT_TRANSPARENCY_             EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.5)
#define _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_                EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.5)
#define _VISUALIZATION_POINT_MAXIMAL_DISPLAY_RADIUS_                EVP::Visualization::Geometry::_3D::OpenInventor::vreal(10.0)
#define _VISUALIZATION_PLANE_DEFAULT_TRANSPARENCY_                  EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.75)
#define _VISUALIZATION_LINE_DEFAULT_LINE_WIDTH_                     EVP::Visualization::Geometry::_3D::OpenInventor::vreal(1.0)
#define _VISUALIZATION_LINE_DEFAULT_LINE_PATTERN_                   0XFFFF
#define _VISUALIZATION_LINE_DEFAULT_TRANSPARENCY_                   EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0)
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_AXIS_LENGTH_       EVP::Visualization::Geometry::_3D::OpenInventor::vreal(100.0)
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_CONE_LENGTH_       EVP::Visualization::Geometry::_3D::OpenInventor::vreal(5.0)
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_CONE_WIDTH_        EVP::Visualization::Geometry::_3D::OpenInventor::vreal(2.5)
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_TRANSPARENCY_      EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.75)
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_X_      "X"
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_Y_      "Y"
#define _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_Z_      "Z"

#define vAbs(x) fabsf(x)

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    typedef float vreal;
                    SbVec3f T(const Mathematics::_3D::CVector3D& X);
                    vreal T(const real X);
                }
            }
        }
    }
}

#endif
#endif
