/*
 * VisualizationVolume.cpp
 *
 *  Created on: Feb 23, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "VisualizationVolume.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {

                    CVisualizationVolume::CVisualizationVolume(const SbBox3f& BoundingBox) :
                        CVisualizationPrimitive(CVisualizationPrimitive::eVisualizationVolume)
                    {
                        m_BoundingBox = BoundingBox;
                        m_pSoCoordinate3 = new SoCoordinate3;
                        m_pBaseSeparator->addChild(m_pSoCoordinate3);
                        m_pSoIndexedLineSet = new SoIndexedLineSet;
                        m_pBaseSeparator->addChild(m_pSoIndexedLineSet);
                        m_pSoIndexedLineSet->setUserData(this);
                        m_pBaseDrawStyle->lineWidth = _VISUALIZATION_VOLUME_DEFAULT_LINE_WIDTH_;
                        m_pBaseDrawStyle->linePattern = _VISUALIZATION_VOLUME_DEFAULT_LINE_PATTERN_;
                        m_pBaseMaterial->transparency = _VISUALIZATION_VOLUME_DEFAULT_TRANSPARENCY_;
                        m_pBaseMaterial->diffuseColor.setValue(_VISUALIZATION_VOLUME_DEFAULT_LINE_COLOR_);
                        Update(true);
                    }

                    CVisualizationVolume::~CVisualizationVolume()
                        = default;

                    void CVisualizationVolume::SetBoundingBox(const SbBox3f& BoundingBox)
                    {
                        if (m_BoundingBox == BoundingBox)
                        {
                            return;
                        }
                        m_BoundingBox = BoundingBox;
                        Update(true);
                        UpdateRegisteredGeometricPrimitives(false);
                    }

                    const SbBox3f& CVisualizationVolume::GetBoundingBox() const
                    {
                        return m_BoundingBox;
                    }

                    bool CVisualizationVolume::IsEmpty() const
                    {
                        return m_BoundingBox.isEmpty();
                    }

                    bool CVisualizationVolume::MapPoint(const SbVec3f& Point) const
                    {
                        if (m_BoundingBox.isEmpty())
                        {
                            return false;
                        }
                        return IsInside(Point);
                    }

                    bool CVisualizationVolume::MapPointPair(const SbVec3f& PointA, const SbVec3f& PointB, bool& FlagA, bool& FlagB) const
                    {
                        if (m_BoundingBox.isEmpty())
                        {
                            FlagA = false;
                            FlagB = false;
                            return false;
                        }
                        FlagA = true;
                        for (int i = 0; i < _VISUALIZATION_VOLUME_TOTAL_BOUNDING_PLANES_; ++i)
                            if (m_Planes[i].getDistance(PointA) < -_VISUALIZATION_EPSILON_)
                            {
                                FlagA = false;
                                break;
                            }
                        FlagB = true;
                        for (int i = 0; i < _VISUALIZATION_VOLUME_TOTAL_BOUNDING_PLANES_; ++i)
                            if (m_Planes[i].getDistance(PointB) < -_VISUALIZATION_EPSILON_)
                            {
                                FlagB = false;
                                break;
                            }
                        return FlagA || FlagB;
                    }

                    bool CVisualizationVolume::MapLine(const SbLine& Line, SbVec3f& PointA, SbVec3f& PointB) const
                    {
                        if (m_BoundingBox.isEmpty())
                        {
                            return false;
                        }
                        SbVec3f IntersectionPoint;
                        vector<SbVec3f> IntersectionPoints;
                        for (int i = 0; i < _VISUALIZATION_VOLUME_TOTAL_BOUNDING_PLANES_; ++i)
                            if (m_Planes[i].intersect(Line, IntersectionPoint) && IsInside(IntersectionPoint))
                            {
                                IntersectionPoints.push_back(IntersectionPoint);
                            }

                        const uint TotalPoints = IntersectionPoints.size();
                        if (TotalPoints >= 2)
                        {
                            if (TotalPoints == 2)
                            {
                                PointA = IntersectionPoints[0];
                                PointB = IntersectionPoints[1];
                                return true;
                            }
                            else
                            {
                                PointA = IntersectionPoints[0];
                                int IndexMax = -1;
                                vreal MaximalDistance = _REAL_MIN_;
                                for (uint i = 1; i < TotalPoints; ++i)
                                {
                                    const vreal Distance = (IntersectionPoints[i] - PointA).length();
                                    if (Distance > MaximalDistance)
                                    {
                                        MaximalDistance = Distance;
                                        IndexMax = i;
                                    }
                                }
                                PointB = IntersectionPoints[IndexMax];
                                return true;
                            }
                        }
                        return false;
                    }

                    bool CVisualizationVolume::MapCircle(const SbVec3f& Normal, const SbVec3f& Center, const vreal Radius) const
                    {
                        if (m_BoundingBox.isEmpty())
                        {
                            return false;
                        }
                        if (IsInside(Center))
                        {
                            return true;
                        }
                        bool InsideImplicitSphere = false;
                        const vreal SquareRadius = Radius * Radius;
                        for (uint i = 0; i < _VISUALIZATION_VOLUME_TOTAL_BOUNDING_VERTICES_; ++i)
                            if ((m_Vertices[i] - Center).sqrLength() <= SquareRadius)
                            {
                                InsideImplicitSphere = true;
                                break;
                            }
                        if (!InsideImplicitSphere)
                            for (uint i = 0; i < _VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_; ++i)
                            {
                                const SbVec3f& SupportPoint = m_Lines[i].getPosition();
                                const SbVec3f& Direction = m_Lines[i].getDirection();
                                const SbVec3f ClosestPoint = SupportPoint + Direction * Direction.dot(Center - SupportPoint);
                                if (IsInside(ClosestPoint) && ((ClosestPoint - Center).sqrLength() <= SquareRadius))
                                {
                                    InsideImplicitSphere = true;
                                    break;
                                }
                            }
                        if (InsideImplicitSphere)
                        {
                            SbPlane ImplicitPlane(Normal, Center);
                            SbLine IntersectionLine;
                            for (uint i = 0; i < _VISUALIZATION_VOLUME_TOTAL_BOUNDING_PLANES_; ++i)
                                if (ImplicitPlane.intersect(m_Planes[i], IntersectionLine))
                                {
                                    const SbVec3f ProjectedCenter = IntersectionLine.getPosition() + IntersectionLine.getDirection() * IntersectionLine.getDirection().dot(Center - IntersectionLine.getPosition());
                                    const SbVec3f Displacement = ProjectedCenter - Center;
                                    const vreal DisplacementLength = Displacement.length();
                                    if (DisplacementLength > _VISUALIZATION_EPSILON_)
                                    {
                                        if (DisplacementLength <= Radius)
                                        {
                                            const SbVec3f ComplementaryDisplacementDirection = Displacement.cross(Normal);
                                            const SbVec3f ComplementaryDisplacement = ComplementaryDisplacementDirection * ((Radius * Radius - DisplacementLength * DisplacementLength) / ComplementaryDisplacementDirection.length());
                                            if (IsInside(Center + Displacement + ComplementaryDisplacement) || IsInside(Center + Displacement - ComplementaryDisplacement))
                                            {
                                                return true;
                                            }
                                        }
                                    }
                                    else if (IsInside(Center + IntersectionLine.getDirection() * Radius) || IsInside(Center - IntersectionLine.getDirection() * Radius))
                                    {
                                        return true;
                                    }

                                }
                            return false;
                        }
                        return false;
                    }

                    bool CVisualizationVolume::MapPlane(const SbPlane& Plane, vector<SbVec3f>& Points) const
                    {
                        if (m_BoundingBox.isEmpty())
                        {
                            return false;
                        }
                        SbVec3f IntersectionPointsAccumulator(vreal(0.0), vreal(0.0), vreal(0.0));
                        AxisIntersectionPoint CurrentAxisIntersectionPoint;
                        vector<AxisIntersectionPoint> AxisIntersectionPoints;
                        for (uint i = 0; i < _VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_; ++i)
                            if (Plane.intersect(m_Lines[i], CurrentAxisIntersectionPoint.m_Location) && IsInside(CurrentAxisIntersectionPoint.m_Location))
                            {
                                IntersectionPointsAccumulator += CurrentAxisIntersectionPoint.m_Location;
                                AxisIntersectionPoints.push_back(CurrentAxisIntersectionPoint);
                            }
                        const uint TotalPoints = AxisIntersectionPoints.size();
                        if (TotalPoints > 2)
                        {
                            const SbVec3f Centroid = IntersectionPointsAccumulator * (vreal(1.0) / vreal(AxisIntersectionPoints.size()));
                            const SbRotation Rotation(Plane.getNormal(), SbVec3f(vreal(0.0), vreal(0.0), vreal(1.0)));
                            for (vector<AxisIntersectionPoint>::iterator pAxisIntersectionPoint = AxisIntersectionPoints.begin(); pAxisIntersectionPoint != AxisIntersectionPoints.end(); ++pAxisIntersectionPoint)
                            {
                                const SbVec3f Displacement = pAxisIntersectionPoint->m_Location - Centroid;
                                SbVec3f DimensionReduced;
                                Rotation.multVec(Displacement, DimensionReduced);
                                const vreal Alpha = RealAtan2(DimensionReduced[1], DimensionReduced[0]);
                                if (Alpha < _REAL_ZERO_)
                                {
                                    pAxisIntersectionPoint->m_Alpha = Alpha + _REAL_2PI_;
                                }
                                else
                                {
                                    pAxisIntersectionPoint->m_Alpha = Alpha;
                                }
                            }
                            TSort(AxisIntersectionPoints.begin(), AxisIntersectionPoints.end(), SortAxisIntersectionPointByAlpha);
                            if ((AxisIntersectionPoints[0].m_Location - Centroid).cross((AxisIntersectionPoints[1].m_Location - Centroid)).dot(Plane.getNormal()) > vreal(0.0))
                                for (uint i = 0; i < TotalPoints; ++i)
                                {
                                    Points.push_back(AxisIntersectionPoints[i].m_Location);
                                }
                            else
                                for (int i = int(TotalPoints) - 1; i >= 0; --i)
                                {
                                    Points.push_back(AxisIntersectionPoints[i].m_Location);
                                }
                            return true;
                        }
                        return false;
                    }

                    bool CVisualizationVolume::MapSphere(const SbVec3f& Center, const vreal Radius) const
                    {
                        if (m_BoundingBox.isEmpty())
                        {
                            return false;
                        }
                        if (IsInside(Center))
                        {
                            return true;
                        }
                        else
                        {
                            const vreal SquareRadius = Radius * Radius;
                            for (uint i = 0; i < _VISUALIZATION_VOLUME_TOTAL_BOUNDING_VERTICES_; ++i)
                                if ((m_Vertices[i] - Center).sqrLength() <= SquareRadius)
                                {
                                    return true;
                                }
                            for (uint i = 0; i < _VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_; ++i)
                            {
                                const SbVec3f& SupportPoint = m_Lines[i].getPosition();
                                const SbVec3f& Direction = m_Lines[i].getDirection();
                                const SbVec3f ClosestPoint = SupportPoint + Direction * Direction.dot(Center - SupportPoint);
                                if (IsInside(ClosestPoint) && ((ClosestPoint - Center).sqrLength() <= SquareRadius))
                                {
                                    return true;
                                }
                            }
                        }
                        return false;
                    }

                    void CVisualizationVolume::UpdateRegisteredGeometricPrimitives(const bool FullUpdate)
                    {
                        if (m_GeometricPrimitives.size())
                        {
                            list<CVisualizationGeometricPrimitive*>::iterator End = m_GeometricPrimitives.end();
                            for (list<CVisualizationGeometricPrimitive*>::iterator ppGeometricPrimitive = m_GeometricPrimitives.begin(); ppGeometricPrimitive != End; ++ppGeometricPrimitive)
                            {
                                (*ppGeometricPrimitive)->Update(FullUpdate);
                            }
                        }
                    }

                    void CVisualizationVolume::Update(const bool FullUpdate)
                    {
                        if (FullUpdate)
                        {
                            vreal X0 = vreal(0.0), Y0 = vreal(0.0), Z0 = vreal(0.0), X1 = vreal(0.0), Y1 = vreal(0.0), Z1 = vreal(0.0);
                            m_BoundingBox.getBounds(X0, Y0, Z0, X1, Y1, Z1);
                            m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y0Z0_].setValue(X0, Y0, Z0);
                            m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y0Z0_].setValue(X1, Y0, Z0);
                            m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y0Z1_].setValue(X1, Y0, Z1);
                            m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y0Z1_].setValue(X0, Y0, Z1);
                            m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y1Z0_].setValue(X0, Y1, Z0);
                            m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y1Z0_].setValue(X1, Y1, Z0);
                            m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X1Y1Z1_].setValue(X1, Y1, Z1);
                            m_Vertices[_VISUALIZATION_VOLUME_BOUNDING_VERTEX_X0Y1Z1_].setValue(X0, Y1, Z1);
                            m_Planes[_VISUALIZATION_VOLUME_BOUNDING_PLANE_XY0_] = SbPlane(SbVec3f(vreal(0.0), vreal(0.0), vreal(1.0)), Z0);
                            m_Planes[_VISUALIZATION_VOLUME_BOUNDING_PLANE_XY1_] = SbPlane(SbVec3f(vreal(0.0), vreal(0.0), -vreal(1.0)), -Z1);
                            m_Planes[_VISUALIZATION_VOLUME_BOUNDING_PLANE_XZ0_] = SbPlane(SbVec3f(vreal(0.0), vreal(1.0), vreal(0.0)), Y0);
                            m_Planes[_VISUALIZATION_VOLUME_BOUNDING_PLANE_XZ1_] = SbPlane(SbVec3f(vreal(0.0), -vreal(1.0), vreal(0.0)), -Y1);
                            m_Planes[_VISUALIZATION_VOLUME_BOUNDING_PLANE_YZ0_] = SbPlane(SbVec3f(vreal(1.0), vreal(0.0), vreal(0.0)), X0);
                            m_Planes[_VISUALIZATION_VOLUME_BOUNDING_PLANE_YZ1_] = SbPlane(SbVec3f(-vreal(1.0), vreal(0.0), vreal(0.0)), -X1);
                            const uint IncidenceA[_VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_] = { 0, 1, 5, 4, 3, 2, 6, 7, 0, 1, 5, 4 };
                            const uint IncidenceB[_VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_] = { 1, 5, 4, 0, 2, 6, 7, 3, 3, 2, 6, 7 };
                            for (uint i = 0; i < _VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_; ++i)
                            {
                                m_Lines[i] = SbLine(m_Vertices[IncidenceA[i]], m_Vertices[IncidenceB[i]]);
                            }
                            for (uint i = 0; i < _VISUALIZATION_VOLUME_TOTAL_BOUNDING_VERTICES_; ++i)
                            {
                                m_pSoCoordinate3->point.set1Value(i, m_Vertices[i]);
                            }
                            const int LineSet[24] = { 0, 1, 5, 4, 0, -1, 3, 2, 6, 7, 3, -1, 0, 3, -1, 1, 2, -1, 5, 6, -1, 4, 7, -1 };
                            for (uint i = 0; i < 24; ++i)
                            {
                                m_pSoIndexedLineSet->coordIndex.set1Value(i, LineSet[i]);
                            }
                        }
                        SwitchVisibility(m_IsVisible && !m_BoundingBox.isEmpty());
                    }

                    bool CVisualizationVolume::IsInside(const SbVec3f& Point) const
                    {
                        vreal X0, Y0, Z0, X1, Y1, Z1;
                        m_BoundingBox.getBounds(X0, Y0, Z0, X1, Y1, Z1);
                        return (((Point[0] - X0) > -_VISUALIZATION_EPSILON_) && ((Point[1] - Y0) > -_VISUALIZATION_EPSILON_) && ((Point[2] - Z0) > -_VISUALIZATION_EPSILON_) && ((Point[0] - X1) < _VISUALIZATION_EPSILON_) && ((Point[1] - Y1) < _VISUALIZATION_EPSILON_) && ((Point[2] - Z1) < _VISUALIZATION_EPSILON_));
                    }

                    bool CVisualizationVolume::SortAxisIntersectionPointByAlpha(const AxisIntersectionPoint& lhs, const AxisIntersectionPoint& rhs)
                    {
                        return lhs.m_Alpha < rhs.m_Alpha;
                    }

                    bool CVisualizationVolume::RegisterGeometricPrimitive(CVisualizationGeometricPrimitive* pGeometricPrimitive)
                    {
                        if (pGeometricPrimitive && (pGeometricPrimitive->GetVisualizationVolume() != this))
                        {
                            pGeometricPrimitive->SetVisualizationVolume(this);
                            m_GeometricPrimitives.push_back(pGeometricPrimitive);
                            return true;
                        }
                        return false;
                    }

                    bool CVisualizationVolume::UnRegisterGeometricPrimitive(CVisualizationGeometricPrimitive* pGeometricPrimitive)
                    {
                        if (pGeometricPrimitive && (pGeometricPrimitive->GetVisualizationVolume() == this) && m_GeometricPrimitives.size())
                        {
                            list<CVisualizationGeometricPrimitive*>::iterator End = m_GeometricPrimitives.end();
                            for (list<CVisualizationGeometricPrimitive*>::iterator ppGeometricPrimitive = m_GeometricPrimitives.begin(); ppGeometricPrimitive != End; ++ppGeometricPrimitive)
                                if (*ppGeometricPrimitive == pGeometricPrimitive)
                                {
                                    m_GeometricPrimitives.erase(ppGeometricPrimitive);
                                    return true;
                                }
                        }
                        return false;
                    }
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
