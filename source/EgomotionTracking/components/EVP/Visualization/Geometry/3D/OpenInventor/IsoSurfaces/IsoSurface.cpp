/*
 * IsoSurface.cpp
 *
 *  Created on: Apr 6, 2012
 *      Author: david
 */

#include "IsoSurface.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    namespace IsoSurface
                    {
                        CIsoSurface::CIsoSurface() :
                            Geometry::_3D::OpenInventor::CVisualizationPrimitive(Geometry::_3D::OpenInventor::CVisualizationPrimitive::eIsoSurface), m_pCoordinates(nullptr), m_pIndexedFaceSet(nullptr)
                        {
                            m_pCoordinates = new SoCoordinate3;
                            m_pBaseSeparator->addChild(m_pCoordinates);
                            m_pNormal = new SoNormal;
                            m_pBaseSeparator->addChild(m_pNormal);
                            m_pIndexedFaceSet = new SoIndexedFaceSet;
                            m_pBaseSeparator->addChild(m_pIndexedFaceSet);
                        }

                        CIsoSurface::~CIsoSurface()
                            = default;

                        bool CIsoSurface::SetDensityCellGrid(Mathematics::_3D::Density::CDensityCellGrid* pDensityCellGrid)
                        {
                            if (pDensityCellGrid)
                            {
                                Mathematics::_3D::Density::CIsoDensitySurface IsoDensitySurface;
                                if (IsoDensitySurface.Generate(pDensityCellGrid))
                                {
                                    uint VertexIndex = 0;
                                    const list<Mathematics::_3D::Density::CIsoDensityVertex*>& IsoDensityVertices = IsoDensitySurface.GetIsoDensityVertices();
                                    list<Mathematics::_3D::Density::CIsoDensityVertex*>::const_iterator EndIsoDensityVertices = IsoDensityVertices.end();
                                    m_pCoordinates->point.setNum(IsoDensityVertices.size());
                                    for (list<Mathematics::_3D::Density::CIsoDensityVertex*>::const_iterator ppIsoDensityVertex = IsoDensityVertices.begin(); ppIsoDensityVertex != EndIsoDensityVertices; ++ppIsoDensityVertex)
                                    {
                                        Mathematics::_3D::Density::CIsoDensityVertex* pIsoDensityVertex = *ppIsoDensityVertex;
                                        const Mathematics::_3D::CVector3D& Location = pIsoDensityVertex->GetLocation();
                                        const Mathematics::_3D::CVector3D& Normal = pIsoDensityVertex->GetNormal();
                                        m_pCoordinates->point.set1Value(VertexIndex, Location.GetX(), Location.GetY(), Location.GetZ());
                                        m_pNormal->vector.set1Value(VertexIndex++, Normal.GetX(), Normal.GetY(), Normal.GetZ());
                                    }
                                    uint FaceIndex = 0;
                                    const list<Mathematics::_3D::Density::CIsoDensityFace*>& IsoDensityFaces = IsoDensitySurface.GetIsoDensityFaces();
                                    list<Mathematics::_3D::Density::CIsoDensityFace*>::const_iterator EndIsoDensityFaces = IsoDensityFaces.end();
                                    for (list<Mathematics::_3D::Density::CIsoDensityFace*>::const_iterator ppIsoDensityFace = IsoDensityFaces.begin(); ppIsoDensityFace != EndIsoDensityFaces; ++ppIsoDensityFace)
                                    {
                                        Mathematics::_3D::Density::CIsoDensityFace* pIsoDensityFace = *ppIsoDensityFace;
                                        m_pIndexedFaceSet->coordIndex.set1Value(FaceIndex++, pIsoDensityFace->GetIndexIsoDensityVertexA());
                                        m_pIndexedFaceSet->coordIndex.set1Value(FaceIndex++, pIsoDensityFace->GetIndexIsoDensityVertexB());
                                        m_pIndexedFaceSet->coordIndex.set1Value(FaceIndex++, pIsoDensityFace->GetIndexIsoDensityVertexC());
                                        m_pIndexedFaceSet->coordIndex.set1Value(FaceIndex++, -1);
                                    }
                                    return true;
                                }
                            }
                            return false;
                        }

                        void CIsoSurface::Update(const bool /*FullUpdate*/)
                        {
                        }
                    }
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
