/*
 * VisualizationGeometricPrimitive.cpp
 *
 *  Created on: Feb 22, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "VisualizationGeometricPrimitive.h"
#include "VisualizationVolume.h"

#ifdef _USE_OPEN_INVENTOR_

using namespace EVP::Mathematics::_3D::Geometry;

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {

                    CVisualizationGeometricPrimitive::CVisualizationGeometricPrimitive(CGeometricPrimitive3D* pGeometricPrimitive, CVisualizationVolume* pVisualizationVolume) :
                        CVisualizationPrimitive(CVisualizationPrimitive::eGeometricPrimitive)
                    {
                        if (pVisualizationVolume)
                        {
                            pVisualizationVolume->RegisterGeometricPrimitive(this);
                        }
                        m_pBaseComplexity->value = _VISUALIZATION_PRIMITIVE_DEFAULT_COMPLEXITY_VALUE_;
                        m_pGeometricPrimitive = pGeometricPrimitive;
                        m_AllowedGeometricPrimitiveTypeId = m_pGeometricPrimitive->GetTypeId();

#ifdef _USE_DEPENDECY_MECHANISM_

                        m_pGeometricPrimitive->AddDependency(this);

#endif

                    }

                    CVisualizationGeometricPrimitive::~CVisualizationGeometricPrimitive()
                    {
                        if (m_pVisualizationVolume)
                        {
                            m_pVisualizationVolume->UnRegisterGeometricPrimitive(this);
                        }

#ifdef _USE_DEPENDECY_MECHANISM_

                        if (m_pGeometricPrimitive)
                        {
                            m_pGeometricPrimitive->RemoveDependency(this);
                        }

#endif
                    }

                    void CVisualizationGeometricPrimitive::SetVisualizationVolume(CVisualizationVolume* pVisualizationVolume)
                    {
                        m_pVisualizationVolume = pVisualizationVolume;
                    }

                    const CVisualizationVolume* CVisualizationGeometricPrimitive::GetVisualizationVolume() const
                    {
                        return m_pVisualizationVolume;
                    }

                    bool CVisualizationGeometricPrimitive::SetGeometricPrimitive(CGeometricPrimitive3D* pGeometricPrimitive)
                    {
                        if (m_pGeometricPrimitive == pGeometricPrimitive)
                        {
                            return false;
                        }

                        if (pGeometricPrimitive && (pGeometricPrimitive->GetTypeId() != m_AllowedGeometricPrimitiveTypeId))
                        {
                            return false;
                        }

#ifdef _USE_DEPENDECY_MECHANISM_

                        if (m_pGeometricPrimitive)
                        {
                            m_pGeometricPrimitive->RemoveDependency(this);
                        }

#endif
                        m_pGeometricPrimitive = pGeometricPrimitive;

#ifdef _USE_DEPENDECY_MECHANISM_

                        if (m_pGeometricPrimitive)
                        {
                            m_pGeometricPrimitive->AddDependency(this);
                        }

#endif
                        Update(true);
                        return true;
                    }

                    CGeometricPrimitive3D* CVisualizationGeometricPrimitive::GetGeometricPrimitive() const
                    {
                        return m_pGeometricPrimitive;
                    }

#ifdef _USE_DEPENDECY_MECHANISM_

                    void CVisualizationGeometricPrimitive::OnDestructor(IDependencyManager* /*pDependencyManager*/)
                    {
                        m_pGeometricPrimitive = nullptr;
                        Update(true);
                    }

                    void CVisualizationGeometricPrimitive::OnChange(IDependencyManager* /*pDependencyManager*/)
                    {

                    }
#endif

                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
