/*
 * VisualizationPlane.h
 *
 *  Created on: Feb 23, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

//OPEN_INVENTOR
#include "OpenInventor.h"

//GEOMETRY
#include "../../../../Foundation/Mathematics/3D/Geometry/Plane3D.h"

//VISUALIZATION
#include "VisualizationGeometricPrimitive.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    class CVisualizationPlane: public CVisualizationGeometricPrimitive
                    {
                    public:

                        CVisualizationPlane(Mathematics::_3D::Geometry::CPlane3D* pPlane, CVisualizationVolume* pVisualizationVolume);
                        ~CVisualizationPlane() override;

                        bool HasSourcePoints() const;
                        void SetDisplayBySourcePoints(const bool Active);

                        void Update(const bool FullUpdate) override;

                    protected:

                        bool IsComposedVisible() override;

                        bool m_DisplayBySourcePoints;
                        SoCoordinate3* m_pSoCoordinate3;
                        SoFaceSet* m_SoFaceSet;
                    };
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
