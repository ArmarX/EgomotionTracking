/*
 * OpenInventor.cpp
 *
 *  Created on: 22.09.2011
 *      Author: gonzalez
 */

#include "OpenInventor.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    SbVec3f T(const Mathematics::_3D::CVector3D& X)
                    {
                        return SbVec3f(vreal(X.GetX()), vreal(X.GetY()), vreal(X.GetZ()));
                    }

                    vreal T(const real X)
                    {
                        return vreal(X);
                    }
                }
            }
        }
    }
}

#endif

