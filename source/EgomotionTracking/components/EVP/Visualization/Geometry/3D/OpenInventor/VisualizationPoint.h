/*
 * VisualizationPoint.h
 *
 *  Created on: Feb 23, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

//OPEN_INVENTOR
#include "OpenInventor.h"

//GEOMETRY
#include "../../../../Foundation/Mathematics/3D/Geometry/Point3D.h"

//VISUALIZATION
#include "VisualizationGeometricPrimitive.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    class CVisualizationPoint: public CVisualizationGeometricPrimitive
                    {
                    public:

                        CVisualizationPoint(Mathematics::_3D::Geometry::CPoint3D* pPoint, CVisualizationVolume* pVisualizationVolume);
                        ~CVisualizationPoint() override;

                        void SetDisplayRadius(const vreal DisplayRadius);
                        vreal GetDisplayaRadius() const;

                        void Update(const bool FullUpdate) override;

                    protected:

                        bool IsComposedVisible() override;

                        SoTranslation* m_pSoTranslation;
                        SoSphere* m_pSoSphere;
                    };
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
