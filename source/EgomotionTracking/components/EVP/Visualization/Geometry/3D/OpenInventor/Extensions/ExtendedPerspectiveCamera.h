/*
 * ExtendedPerspectiveCamera.h
 *
 *  Created on: Apr 15, 2012
 *      Author: david
 */

#pragma once

#include "../../../../../GlobalSettings.h"

#ifdef _USE_OPEN_INVENTOR_

//EVP
#include "../OpenInventor.h"
#include "../../../../../VisualSpace/Cameras/GeometricCalibration/CameraGeometricCalibration.h"

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    namespace Extensions
                    {
                        class CExtendedPerspectiveCamera: public SoPerspectiveCamera
                        {
                        public:

                            enum RenderingMode
                            {
                                eVirtualCameraParameteres, eCalibrationCameraParameters
                            };

                            CExtendedPerspectiveCamera();
                            ~CExtendedPerspectiveCamera() override;

                            bool LoadCalibration(const VisualSpace::Cameras::GeometricCalibration::CCameraGeometricCalibration* pCameraGeometricCalibration, const vreal Near, const vreal Far);
                            bool SetRenderingMode(const RenderingMode Mode);

                            void callback(SoCallbackAction* pAction) override;
                            void GLRender(SoGLRenderAction* pAction) override;
                            SbViewVolume getViewVolume(const vreal UseAspectRatio = 0.0f) const override;

                            const SbMatrix& GetGLProjectionMatrix();
                            void SetGLProjectionMatrix(const SbMatrix& projectionMatrix);
                        protected:

                            RenderingMode m_RenderingMode;
                            const VisualSpace::Cameras::GeometricCalibration::CCameraGeometricCalibration* m_pCameraGeometricCalibration;
                            SbMatrix m_ProjectionMatrix;
                            SbVec2f m_ImageSize;
                            SbVec2f m_FocalLenght;
                            SbVec2f m_PrincipalPoint;
                            SbVec2f m_FieldOfView;
                            vreal m_Near;
                            vreal m_Far;
                            vreal m_AspectRatio;
                        };
                    }
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
