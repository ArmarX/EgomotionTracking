/*
 * VisualizationCoordinateSystem.cpp
 *
 *  Created on: Mar 1, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "VisualizationCoordinateSystem.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    CVisualizationcoordinateSystem::CVisualizationcoordinateSystem() :
                        CVisualizationPrimitive(CVisualizationPrimitive::ecoordinateSystem)
                    {
                        SoMaterialBinding* pAxesMaterialBinding = new SoMaterialBinding;
                        m_pBaseSeparator->addChild(pAxesMaterialBinding);
                        pAxesMaterialBinding->value = SoMaterialBinding::PER_PART;
                        m_pAxesCoordinate3 = new SoCoordinate3;
                        m_pBaseSeparator->addChild(m_pAxesCoordinate3);
                        m_pAxesLineSet = new SoLineSet;
                        m_pBaseSeparator->addChild(m_pAxesLineSet);
                        SoMaterialBinding* pConesMaterialBinding = new SoMaterialBinding;
                        m_pBaseSeparator->addChild(pConesMaterialBinding);
                        for (int i = 0; i < 3; ++i)
                        {
                            SoSeparator* pAxisSeparator = new SoSeparator;
                            m_pBaseSeparator->addChild(pAxisSeparator);
                            m_Axes[i].m_pConeMaterial = new SoMaterial;
                            pAxisSeparator->addChild(m_Axes[i].m_pConeMaterial);
                            m_Axes[i].m_pConeTransform = new SoTransform;
                            pAxisSeparator->addChild(m_Axes[i].m_pConeTransform);
                            m_Axes[i].m_pCone = new SoCone;
                            pAxisSeparator->addChild(m_Axes[i].m_pCone);
                            m_Axes[i].m_pCone->parts = SoCone::SIDES;
                            m_Axes[i].m_pLabelTranslation = new SoTranslation;
                            pAxisSeparator->addChild(m_Axes[i].m_pLabelTranslation);
                            m_Axes[i].m_pLabelMaterial = new SoMaterial;
                            pAxisSeparator->addChild(m_Axes[i].m_pLabelMaterial);
                            m_Axes[i].m_pLabelText = new SoText2;
                            pAxisSeparator->addChild(m_Axes[i].m_pLabelText);
                        }
                        m_Axes[0].m_Label = _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_X_;
                        m_Axes[1].m_Label = _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_Y_;
                        m_Axes[2].m_Label = _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_LABEL_AXIS_Z_;
                        m_AxisLenght = _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_AXIS_LENGTH_;
                        m_ConeLenght = _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_CONE_LENGTH_;
                        m_ConeWidth = _VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_CONE_WIDTH_;
                        SetColors(CVisualizationPrimitive::eRed, CVisualizationPrimitive::eGreen, CVisualizationPrimitive::eBlue);
                        SetTransparency(_VISUALIZATION_COORDINATE_SYSTEM_DEFAULT_TRANSPARENCY_);
                        Update(true);
                    }

                    CVisualizationcoordinateSystem::~CVisualizationcoordinateSystem()
                        = default;

                    bool CVisualizationcoordinateSystem::SetColors(const CVisualizationPrimitive::VisualizationPredefinedColor AxisXColor, const CVisualizationPrimitive::VisualizationPredefinedColor AxisYColor, const CVisualizationPrimitive::VisualizationPredefinedColor AxisZColor)
                    {
                        if ((AxisXColor == AxisYColor) || (AxisXColor == AxisZColor) || (AxisYColor == AxisZColor))
                        {
                            return false;
                        }
                        else
                        {
                            const SbColor ColorX = CVisualizationPrimitive::GetPredefinedColor(AxisXColor);
                            const SbColor ColorY = CVisualizationPrimitive::GetPredefinedColor(AxisYColor);
                            const SbColor ColorZ = CVisualizationPrimitive::GetPredefinedColor(AxisZColor);
                            m_pBaseMaterial->diffuseColor.set1Value(0, ColorX);
                            m_pBaseMaterial->diffuseColor.set1Value(1, ColorY);
                            m_pBaseMaterial->diffuseColor.set1Value(2, ColorZ);
                            m_Axes[0].m_pConeMaterial->diffuseColor = ColorX;
                            m_Axes[1].m_pConeMaterial->diffuseColor = ColorY;
                            m_Axes[2].m_pConeMaterial->diffuseColor = ColorZ;
                            m_Axes[0].m_pLabelMaterial->diffuseColor = ColorX;
                            m_Axes[1].m_pLabelMaterial->diffuseColor = ColorY;
                            m_Axes[2].m_pLabelMaterial->diffuseColor = ColorZ;
                            return true;
                        }
                    }

                    void CVisualizationcoordinateSystem::GetColors(SbColor& AxisXColor, SbColor& AxisYColor, SbColor& AxisZColor)
                    {
                        const SbColor* pColors = m_pBaseMaterial->diffuseColor.getValues(0);
                        AxisXColor = pColors[0];
                        AxisYColor = pColors[1];
                        AxisZColor = pColors[2];
                    }

                    void CVisualizationcoordinateSystem::SetTransparency(const vreal Transparency)
                    {
                        m_Axes[0].m_pConeMaterial->transparency = Transparency;
                        m_Axes[1].m_pConeMaterial->transparency = Transparency;
                        m_Axes[2].m_pConeMaterial->transparency = Transparency;
                    }

                    void CVisualizationcoordinateSystem::SetAxesLength(const vreal Lenght)
                    {
                        if (m_AxisLenght != Lenght)
                        {
                            m_AxisLenght = Lenght;
                            Update(true);
                        }
                    }

                    vreal CVisualizationcoordinateSystem::GetAxesLength()
                    {
                        return m_AxisLenght;
                    }

                    void CVisualizationcoordinateSystem::SetConeLength(const vreal Lenght)
                    {
                        if (m_ConeLenght != Lenght)
                        {
                            m_ConeLenght = Lenght;
                            Update(true);
                        }
                    }

                    vreal CVisualizationcoordinateSystem::GetConeLength()
                    {
                        return m_ConeLenght;
                    }

                    void CVisualizationcoordinateSystem::SetConeWidth(const vreal Width)
                    {
                        if (m_ConeWidth != Width)
                        {
                            m_ConeWidth = Width;
                            Update(true);
                        }
                    }

                    vreal CVisualizationcoordinateSystem::GetConeWidth()
                    {
                        return m_ConeWidth;
                    }

                    void CVisualizationcoordinateSystem::Update(const bool FullUpdate)
                    {
                        if (FullUpdate)
                        {
                            const vreal LabelOffset = m_ConeLenght * vreal(0.5);
                            const vreal ConeTranslation = m_AxisLenght - LabelOffset;
                            m_pAxesCoordinate3->point.set1Value(0, vreal(0.0), vreal(0.0), vreal(0.0));
                            m_pAxesCoordinate3->point.set1Value(1, m_AxisLenght, vreal(0.0), vreal(0.0));
                            m_pAxesCoordinate3->point.set1Value(2, vreal(0.0), vreal(0.0), vreal(0.0));
                            m_pAxesCoordinate3->point.set1Value(3, vreal(0.0), m_AxisLenght, vreal(0.0));
                            m_pAxesCoordinate3->point.set1Value(4, vreal(0.0), vreal(0.0), vreal(0.0));
                            m_pAxesCoordinate3->point.set1Value(5, vreal(0.0), vreal(0.0), m_AxisLenght);
                            m_pAxesLineSet->numVertices.set1Value(0, 2);
                            m_pAxesLineSet->numVertices.set1Value(1, 2);
                            m_pAxesLineSet->numVertices.set1Value(2, 2);
                            const SbVec3f AxisY(vreal(0.0), vreal(1.0), vreal(0.0));
                            const SbVec3f LabelTranslation(vreal(0.0), LabelOffset, vreal(0.0));
                            for (int i = 0; i < 3; ++i)
                            {
                                SbVec3f Translation(vreal(0.0), vreal(0.0), vreal(0.0));
                                SbVec3f Axis(vreal(0.0), vreal(0.0), vreal(0.0));
                                Translation[i] = ConeTranslation;
                                Axis[i] = vreal(1.0);
                                m_Axes[i].m_pConeTransform->translation = Translation;
                                m_Axes[i].m_pConeTransform->rotation = SbRotation(AxisY, Axis);
                                m_Axes[i].m_pCone->height = m_ConeLenght;
                                m_Axes[i].m_pCone->bottomRadius = m_ConeWidth;
                                m_Axes[i].m_pLabelTranslation->translation = LabelTranslation;
                                m_Axes[i].m_pLabelText->string = m_Axes[i].m_Label;
                            }
                        }
                    }

                    bool CVisualizationcoordinateSystem::SetLabels(const_string pLabelX, const_string pLabelY, const_string pLabelZ)
                    {
                        if (pLabelX && pLabelY && pLabelZ)
                        {
                            m_Axes[0].m_pLabelText->string.setValue(pLabelX);
                            m_Axes[1].m_pLabelText->string.setValue(pLabelY);
                            m_Axes[2].m_pLabelText->string.setValue(pLabelZ);
                            return true;
                        }
                        return false;
                    }
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
