/*
 * VisualizationLine.cpp
 *
 *  Created on: Feb 23, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "VisualizationLine.h"
#include "VisualizationVolume.h"

#ifdef _USE_OPEN_INVENTOR_

using namespace EVP::Mathematics::_3D::Geometry;
using namespace EVP::Mathematics::_3D;

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {

                    CVisualizationLine::CVisualizationLine(CLine3D* pLine, CVisualizationVolume* pVisualizationVolume) :
                        CVisualizationGeometricPrimitive(pLine, pVisualizationVolume)
                    {
                        m_pSoCoordinate3 = new SoCoordinate3;
                        m_pBaseSeparator->addChild(m_pSoCoordinate3);
                        m_pSoLineSet = new SoLineSet;
                        m_pBaseSeparator->addChild(m_pSoLineSet);
                        m_pSoLineSet->setUserData(this);
                        m_pBaseDrawStyle->lineWidth = _VISUALIZATION_LINE_DEFAULT_LINE_WIDTH_;
                        m_pBaseDrawStyle->linePattern = _VISUALIZATION_LINE_DEFAULT_LINE_PATTERN_;
                        m_pBaseMaterial->transparency = _VISUALIZATION_LINE_DEFAULT_TRANSPARENCY_;
                        Update(true);
                    }

                    CVisualizationLine::~CVisualizationLine()
                        = default;

                    bool CVisualizationLine::IsComposedVisible()
                    {
                        if (m_IsVisible && m_pGeometricPrimitive)
                        {
                            const CLine3D* pLine = (const CLine3D*) m_pGeometricPrimitive;
                            if (m_pVisualizationVolume)
                            {
                                SbVec3f Ap, Bp;
                                if (m_pVisualizationVolume->MapLine(SbLine(T(pLine->GetSupportPoint()), T(pLine->GetPointAtUnitaryDisplacement())), Ap, Bp))
                                {
                                    const vreal DA = (Ap - SbVec3f(m_pSoCoordinate3->point[0])).length();
                                    const vreal DB = (Bp - SbVec3f(m_pSoCoordinate3->point[0])).length();
                                    if (DA < DB)
                                    {
                                        m_pSoCoordinate3->point.set1Value(0, Ap);
                                        m_pSoCoordinate3->point.set1Value(1, Bp);
                                    }
                                    else
                                    {
                                        m_pSoCoordinate3->point.set1Value(0, Bp);
                                        m_pSoCoordinate3->point.set1Value(1, Ap);
                                    }
                                    m_pSoLineSet->numVertices.setValue(2);
                                    return true;
                                }
                                return false;
                            }
                            CVector3D Aq, Bq;
                            if (pLine->GetSourcePoints(Aq, Bq))
                            {
                                m_pSoCoordinate3->point.set1Value(0, T(Aq));
                                m_pSoCoordinate3->point.set1Value(1, T(Bq));
                                m_pSoLineSet->numVertices.setValue(2);
                                return true;
                            }
                        }
                        return false;
                    }

                    void CVisualizationLine::Update(const bool /*FullUpdate*/)
                    {
                        SwitchVisibility(IsComposedVisible());
                    }
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
