/*
 * VisualizationGeometricPrimitive.h
 *
 *  Created on: Feb 22, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

//OPEN_INVENTOR
#include "OpenInventor.h"

//GEOMETRY
#include "../../../../Foundation/Mathematics/3D/Geometry/GeometricPrimitive3D.h"

//VISUALIZATION
#include "VisualizationPrimitive.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    class CVisualizationVolume;

#ifdef _USE_DEPENDECY_MECHANISM_

                    class CVisualizationGeometricPrimitive: public CVisualizationPrimitive, public IDependency

#else

                    class CVisualizationGeometricPrimitive: public CVisualizationPrimitive

#endif

                    {
                    public:

                        CVisualizationGeometricPrimitive(Mathematics::_3D::Geometry::CGeometricPrimitive3D* pGeometricPrimitive, CVisualizationVolume* pVisualizationVolume);
                        ~CVisualizationGeometricPrimitive() override;

                        bool SetGeometricPrimitive(Mathematics::_3D::Geometry::CGeometricPrimitive3D* pGeometricPrimitive);
                        Mathematics::_3D::Geometry::CGeometricPrimitive3D* GetGeometricPrimitive() const;

                        const CVisualizationVolume* GetVisualizationVolume() const;
                        void SetVisualizationVolume(CVisualizationVolume* pVisualizationVolume);

                    protected:

#ifdef _USE_DEPENDECY_MECHANISM_

                        void OnDestructor(IDependencyManager* pDependencyManager) override;
                        void OnChange(IDependencyManager* pDependencyManager) override;

#endif

                        virtual  bool IsComposedVisible() = 0;

                        CVisualizationVolume* m_pVisualizationVolume;
                        Mathematics::_3D::Geometry::CGeometricPrimitive3D* m_pGeometricPrimitive;
                        Mathematics::_3D::Geometry::CGeometricPrimitive3D::GeometricPrimitive3DTypeId m_AllowedGeometricPrimitiveTypeId;
                    };
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
