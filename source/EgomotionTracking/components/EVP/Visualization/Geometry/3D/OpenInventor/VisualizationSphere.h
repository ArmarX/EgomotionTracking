/*
 * VisualizationSphere.h
 *
 *  Created on: Feb 22, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

//OPEN_INVENTOR
#include "OpenInventor.h"

//GEOMETRY
#include "../../../../Foundation/Mathematics/3D/Geometry/Sphere3D.h"

//VISUALIZATION
#include "VisualizationGeometricPrimitive.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    class CVisualizationSphere: public CVisualizationGeometricPrimitive
                    {
                    public:

                        CVisualizationSphere(Mathematics::_3D::Geometry::CSphere3D* pSphere, CVisualizationVolume* pVisualizationVolume);
                        ~CVisualizationSphere() override;

                        void Update(const bool FullUpdate) override;

                    protected:

                        bool IsComposedVisible() override;

                        SoTranslation* m_pSoTranslation;
                        SoSphere* m_pSoSphere;
                    };
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
