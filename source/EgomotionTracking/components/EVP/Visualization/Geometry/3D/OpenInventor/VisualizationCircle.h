/*
 * VisualizationCircle.h
 *
 *  Created on: Feb 23, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

//OPEN_INVENTOR
#include "OpenInventor.h"

//GEOMETRY
#include "../../../../Foundation/Mathematics/3D/Geometry/Circle3D.h"

//VISUALIZATION
#include "VisualizationGeometricPrimitive.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    class CVisualizationCircle: public CVisualizationGeometricPrimitive
                    {
                    public:

                        CVisualizationCircle(Mathematics::_3D::Geometry::CCircle3D* pCircle, CVisualizationVolume* pVisualizationVolume);
                        ~CVisualizationCircle() override;

                        void Update(const bool FullUpdate) override;

                    protected:

                        bool  IsComposedVisible() override;
                        inline void GenerateCircle();

                        SoCoordinate3* m_pSoCoordinate3;
                        SoLineSet* m_pSoLineSet;
                    };
                }
            }
        }
    }
}

#endif /*_USE_OPEN_INVENTOR_*/
