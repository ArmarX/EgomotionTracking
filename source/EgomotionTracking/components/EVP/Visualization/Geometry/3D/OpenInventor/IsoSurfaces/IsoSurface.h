/*
 * IsoSurface.h
 *
 *  Created on: Apr 6, 2012
 *      Author: david
 */

#pragma once

#include "../../../../../GlobalSettings.h"

#ifdef _USE_OPEN_INVENTOR_

//EVP
#include "../../../../../Foundation/Mathematics/3D/Vector3D.h"
#include "../../../../../Foundation/Mathematics/3D/Density/DensityCellGrid.h"
#include "../../../../../Foundation/Mathematics/3D/Density/IsoDensitySurface.h"
#include "../../../../Miscellaneous/RGBAColorMap.h"
#include "../OpenInventor.h"
#include "../VisualizationPrimitive.h"

#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoNormal.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    namespace IsoSurface
                    {
                        class CIsoSurface: public Geometry::_3D::OpenInventor::CVisualizationPrimitive
                        {
                        public:

                            CIsoSurface();
                            ~CIsoSurface() override;

                            bool SetDensityCellGrid(Mathematics::_3D::Density::CDensityCellGrid* pDensityCellGrid);

                        protected:

                            void Update(const bool FullUpdate) override;

                            SoCoordinate3* m_pCoordinates;
                            SoNormal* m_pNormal;
                            SoIndexedFaceSet* m_pIndexedFaceSet;
                        };
                    }
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */

