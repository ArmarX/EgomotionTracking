/*
 * VisualizationVolume.h
 *
 *  Created on: Feb 23, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

//OPEN_INVENTOR
#include "OpenInventor.h"

//VISUALIZATION
#include "VisualizationGeometricPrimitive.h"
#include "VisualizationPrimitive.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    class CVisualizationVolume: public CVisualizationPrimitive
                    {
                    public:

                        CVisualizationVolume(const SbBox3f& BoundingBox);
                        ~CVisualizationVolume() override;

                        void SetBoundingBox(const SbBox3f& BoundingBox);
                        const SbBox3f& GetBoundingBox() const;

                        bool IsEmpty() const;
                        bool MapPoint(const SbVec3f& Point) const;
                        bool MapPointPair(const SbVec3f& PointA, const SbVec3f& PointB, bool& FlagA, bool& FlagB) const;
                        bool MapLine(const SbLine& Line, SbVec3f& PointA, SbVec3f& PointB) const;
                        bool MapCircle(const SbVec3f& Normal, const SbVec3f& Center, const vreal Radius) const;
                        bool MapPlane(const SbPlane& Plane, vector<SbVec3f>& Points) const;
                        bool MapSphere(const SbVec3f& Center, const vreal Radius) const;

                        bool RegisterGeometricPrimitive(CVisualizationGeometricPrimitive* pGeometricPrimitive);
                        bool UnRegisterGeometricPrimitive(CVisualizationGeometricPrimitive* pGeometricPrimitive);

                    protected:

                        struct AxisIntersectionPoint
                        {
                            SbVec3f m_Location;
                            vreal m_Alpha;
                        };

                        void UpdateRegisteredGeometricPrimitives(const bool FullUpdate);
                        void Update(const bool FullUpdate) override;
                        bool IsInside(const SbVec3f& Point) const;
                        static  bool SortAxisIntersectionPointByAlpha(const AxisIntersectionPoint& lhs, const AxisIntersectionPoint& rhs);

                        SbBox3f m_BoundingBox;
                        SbPlane m_Planes[_VISUALIZATION_VOLUME_TOTAL_BOUNDING_PLANES_];
                        SbLine m_Lines[_VISUALIZATION_VOLUME_TOTAL_BOUNDING_LINES_];
                        SbVec3f m_Vertices[_VISUALIZATION_VOLUME_TOTAL_BOUNDING_VERTICES_];
                        SoCoordinate3* m_pSoCoordinate3;
                        SoIndexedLineSet* m_pSoIndexedLineSet;
                        list<CVisualizationGeometricPrimitive*> m_GeometricPrimitives;
                    };
                }
            }
        }
    }
}

#endif /*_USE_OPEN_INVENTOR_ */
