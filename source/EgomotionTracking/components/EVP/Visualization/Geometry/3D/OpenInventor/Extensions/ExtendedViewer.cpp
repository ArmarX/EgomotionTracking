/*
 * CExtendedViewer.cpp
 *
 *  Created on: Apr 15, 2012
 *      Author: david
 */

#include "ExtendedViewer.h"

#ifdef _USE_OPEN_INVENTOR_

#include <GL/gl.h>

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    namespace Extensions
                    {
                        CExtendedViewer::CExtendedViewer(QWidget* pParent) :
                            SoQtExaminerViewer(pParent), m_SeparatorBackGround(nullptr), m_pImage(nullptr)
                        {
                            setClearBeforeRender(FALSE, TRUE);
                            m_SeparatorBackGround = new SoSeparator;
                            m_SeparatorBackGround->ref();
                            SoOrthographicCamera* pOrthographicCamera = new SoOrthographicCamera;
                            m_SeparatorBackGround->addChild(pOrthographicCamera);
                            m_pImage = new SoImage;
                            m_SeparatorBackGround->addChild(m_pImage);
                            pOrthographicCamera->position = SbVec3f(0, 0, 1);
                            pOrthographicCamera->height = 1;
                            pOrthographicCamera->nearDistance = 0.5;
                            pOrthographicCamera->farDistance = 1.5;
                            m_pImage->vertAlignment = SoImage::HALF;
                            m_pImage->horAlignment = SoImage::CENTER;
                            memset(&m_BackGroundImage, 0, sizeof(BackGroundImage));
                        }

                        CExtendedViewer::~CExtendedViewer()
                        {
                            switch (m_BackGroundImage.m_Type)
                            {
                                case BackGroundImage::eUnknown:
                                    break;
                                case BackGroundImage::eIntensity:
                                    RELEASE_OBJECT(m_BackGroundImage.m_Destiny.m_pIntensityImage)
                                    break;
                                case BackGroundImage::eRGB:
                                    RELEASE_OBJECT(m_BackGroundImage.m_Destiny.m_pRGBImage)
                                    break;
                                case BackGroundImage::eRGBA:
                                    RELEASE_OBJECT(m_BackGroundImage.m_Destiny.m_pRGBAImage)
                                    break;
                            }
                            m_SeparatorBackGround->unref();
                        }

                        void CExtendedViewer::SetSourceImage(const TImage<byte>* pIntensityImage)
                        {
                            if (pIntensityImage)
                            {
                                m_BackGroundImage.m_Type = BackGroundImage::eIntensity;
                                m_BackGroundImage.m_Source.m_pIntensityImage = pIntensityImage;
                                RELEASE_OBJECT(m_BackGroundImage.m_Destiny.m_pIntensityImage)
                                m_BackGroundImage.m_Destiny.m_pIntensityImage = m_BackGroundImage.m_Source.m_pIntensityImage->Clone(false);
                                if (Update())
                                {
                                    m_pImage->image.setValue(SbVec2s(m_BackGroundImage.m_Destiny.m_pIntensityImage->GetWidth(), m_BackGroundImage.m_Destiny.m_pIntensityImage->GetHeight()), m_BackGroundImage.m_Type, m_BackGroundImage.m_Destiny.m_pIntensityImage->GetBeginWritableBuffer(), SoSFImage::NO_COPY);
                                }
                            }
                        }

                        void CExtendedViewer::SetSourceImage(const TImage<CDiscreteTristimulusPixel>* pRGBImage)
                        {
                            if (pRGBImage)
                            {
                                m_BackGroundImage.m_Type = BackGroundImage::eRGB;
                                m_BackGroundImage.m_Source.m_pRGBImage = pRGBImage;
                                RELEASE_OBJECT(m_BackGroundImage.m_Destiny.m_pRGBImage)
                                m_BackGroundImage.m_Destiny.m_pRGBImage = m_BackGroundImage.m_Source.m_pRGBImage->Clone(false);
                                if (Update())
                                {
                                    m_pImage->image.setValue(SbVec2s(m_BackGroundImage.m_Destiny.m_pRGBImage->GetWidth(), m_BackGroundImage.m_Destiny.m_pRGBImage->GetHeight()), m_BackGroundImage.m_Type, reinterpret_cast<byte*>(m_BackGroundImage.m_Destiny.m_pRGBImage->GetBeginWritableBuffer()), SoSFImage::NO_COPY);
                                }
                            }
                        }

                        void CExtendedViewer::SetSourceImage(const TImage<CDiscreteRGBAPixel>* pRGBAImage)
                        {
                            if (pRGBAImage)
                            {
                                m_BackGroundImage.m_Type = BackGroundImage::eRGBA;
                                m_BackGroundImage.m_Source.m_pRGBAImage = pRGBAImage;
                                RELEASE_OBJECT(m_BackGroundImage.m_Destiny.m_pRGBAImage)
                                m_BackGroundImage.m_Destiny.m_pRGBAImage = m_BackGroundImage.m_Source.m_pRGBAImage->Clone(false);
                                if (Update())
                                {
                                    m_pImage->image.setValue(SbVec2s(m_BackGroundImage.m_Destiny.m_pRGBAImage->GetWidth(), m_BackGroundImage.m_Destiny.m_pRGBAImage->GetHeight()), m_BackGroundImage.m_Type, reinterpret_cast<byte*>(m_BackGroundImage.m_Destiny.m_pRGBAImage->GetBeginWritableBuffer()), SoSFImage::NO_COPY);
                                }
                            }
                        }

                        bool CExtendedViewer::Update()
                        {
                            switch (m_BackGroundImage.m_Type)
                            {
                                case BackGroundImage::eUnknown:
                                    return false;
                                    break;
                                case BackGroundImage::eIntensity:
                                    if (m_BackGroundImage.m_Source.m_pIntensityImage && m_BackGroundImage.m_Destiny.m_pIntensityImage)
                                    {
                                        const uint Width = m_BackGroundImage.m_Source.m_pIntensityImage->GetWidth();
                                        const uint Height = m_BackGroundImage.m_Source.m_pIntensityImage->GetHeight();
                                        const uint LineBufferSize = Width * sizeof(byte);
                                        const byte* pSourcePixel = m_BackGroundImage.m_Source.m_pIntensityImage->GetBeginReadOnlyBuffer();
                                        byte* pDestinyPixel = m_BackGroundImage.m_Destiny.m_pIntensityImage->GetBeginWritableBuffer() + (Width * (Height - 1));
                                        for (uint Y = 0; Y < Height; ++Y, pSourcePixel += Width, pDestinyPixel -= Width)
                                        {
                                            memcpy(pDestinyPixel, pSourcePixel, LineBufferSize);
                                        }
                                    }
                                    return true;
                                    break;
                                case BackGroundImage::eRGB:
                                    if (m_BackGroundImage.m_Source.m_pRGBImage && m_BackGroundImage.m_Destiny.m_pRGBImage)
                                    {
                                        const uint Width = m_BackGroundImage.m_Source.m_pRGBImage->GetWidth();
                                        const uint Height = m_BackGroundImage.m_Source.m_pRGBImage->GetHeight();
                                        const uint LineBufferSize = Width * sizeof(CDiscreteTristimulusPixel);
                                        const CDiscreteTristimulusPixel* pSourcePixel = m_BackGroundImage.m_Source.m_pRGBImage->GetBeginReadOnlyBuffer();
                                        CDiscreteTristimulusPixel* pDestinyPixel = m_BackGroundImage.m_Destiny.m_pRGBImage->GetBeginWritableBuffer() + (Width * (Height - 1));
                                        for (uint Y = 0; Y < Height; ++Y, pSourcePixel += Width, pDestinyPixel -= Width)
                                        {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
                                            memcpy(pDestinyPixel, pSourcePixel, LineBufferSize);
#pragma GCC diagnostic pop
                                        }
                                    }
                                    return true;
                                    break;
                                case BackGroundImage::eRGBA:
                                    if (m_BackGroundImage.m_Source.m_pRGBAImage && m_BackGroundImage.m_Destiny.m_pRGBAImage)
                                    {
                                        const uint Width = m_BackGroundImage.m_Source.m_pRGBAImage->GetWidth();
                                        const uint Height = m_BackGroundImage.m_Source.m_pRGBAImage->GetHeight();
                                        const uint LineBufferSize = Width * sizeof(CDiscreteRGBAPixel);
                                        const CDiscreteRGBAPixel* pSourcePixel = m_BackGroundImage.m_Source.m_pRGBAImage->GetBeginReadOnlyBuffer();
                                        CDiscreteRGBAPixel* pDestinyPixel = m_BackGroundImage.m_Destiny.m_pRGBAImage->GetBeginWritableBuffer() + (Width * (Height - 1));
                                        for (uint Y = 0; Y < Height; ++Y, pSourcePixel += Width, pDestinyPixel -= Width)
                                        {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
                                            memcpy(pDestinyPixel, pSourcePixel, LineBufferSize);
#pragma GCC diagnostic pop
                                        }
                                    }
                                    return true;
                                    break;
                            }
                            return false;
                        }

                        void CExtendedViewer::actualRedraw()
                        {
                            const SbViewportRegion& ViewportRegion = getViewportRegion();
                            const SbVec2s& ViewportOriginPixels = ViewportRegion.getViewportOriginPixels();
                            const SbVec2s& ViewportSizePixels = ViewportRegion.getViewportSizePixels();
                            glViewport(ViewportOriginPixels[0], ViewportOriginPixels[1], ViewportSizePixels[0], ViewportSizePixels[1]);
                            const SbColor BackgroundColor = getBackgroundColor();
                            glClearColor(BackgroundColor[0], BackgroundColor[1], BackgroundColor[2], 1.0f);
                            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
                            getGLRenderAction()->apply(m_SeparatorBackGround);
                            SoQtExaminerViewer::actualRedraw();
                        }
                    }
                }
            }
        }
    }
}

#endif
