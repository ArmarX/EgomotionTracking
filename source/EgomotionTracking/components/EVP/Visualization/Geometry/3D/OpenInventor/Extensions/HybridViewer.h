/*
 * HybridViewer.h
 *
 *  Created on: Apr 15, 2012
 *      Author: david
 */

#pragma once

#include "../../../../../GlobalSettings.h"

#ifdef _USE_OPEN_INVENTOR_
#ifdef _EVP_USE_GUI_TOOLKIT
//EVP
#include "../OpenInventor.h"
#include "ExtendedViewer.h"
#include "ExtendedPerspectiveCamera.h"

class QWidget;

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    namespace Extensions
                    {
                        class CHybridViewer
                        {
                        public:

                            CHybridViewer();
                            CHybridViewer(QWidget* pParent);
                            virtual ~CHybridViewer();

                            void AddContent(SoNode* pNode);
                            void ClearContent();

                            void SetGeometry(const uint X, const uint Y, const uint Width, const uint Height);

                            bool LoadCalibration(const VisualSpace::Cameras::GeometricCalibration::CCameraGeometricCalibration* pCameraGeometricCalibration, const vreal Near, const vreal Far);
                            bool SetRenderingMode(const CExtendedPerspectiveCamera::RenderingMode Mode);

                            void SetSourceImage(const TImage<byte>* pSourceImage);
                            void SetSourceImage(const TImage<CDiscreteTristimulusPixel>* pSourceImage);
                            void SetSourceImage(const TImage<CDiscreteRGBAPixel>* pSourceImage);

                            bool UpdateSourceImage();
                            void DoMainLoop();
                            void Render();
                            void SetAntialiasing(const uint Level);

                            void SetCameraPose(const SbVec3f& Position, const SbRotation& Orientation);
                            void SetCameraPosition(const SbVec3f& Position);
                            void SetCameraOrientation(const SbRotation& Orientation);

                            void GetCameraPose(SbVec3f& Position, SbRotation& Orientation) const;
                            SbVec3f GetCameraPosition() const;
                            SbRotation GetCameraOrientation() const;

                        protected:

                            QWidget* m_pParent;
                            CExtendedViewer* m_pExtendedViewer;
                            SoExtSelection* m_pRoot;
                            SoSeparator* m_pContent;
                            CExtendedPerspectiveCamera* m_pExtendedPerspectiveCamera;
                        };
                    }
                }
            }
        }
    }
}

#endif

#endif /* _USE_OPEN_INVENTOR_ */

