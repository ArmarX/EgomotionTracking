/*
 * ExtendedPerspectiveCamera.cpp
 *
 *  Created on: Apr 15, 2012
 *      Author: david
 */

#include "ExtendedPerspectiveCamera.h"

#ifdef _USE_OPEN_INVENTOR_

#include <Inventor/actions/SoGLRenderAction.h>
#include <Inventor/actions/SoCallbackAction.h>
#include <Inventor/misc/SoState.h>
#include <Inventor/elements/SoProjectionMatrixElement.h>
#include <Inventor/elements/SoViewingMatrixElement.h>
#include <Inventor/elements/SoFocalDistanceElement.h>
#include <Inventor/elements/SoViewVolumeElement.h>


namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    namespace Extensions
                    {
                        CExtendedPerspectiveCamera::CExtendedPerspectiveCamera() :
                            SoPerspectiveCamera(), m_RenderingMode(eVirtualCameraParameteres), m_pCameraGeometricCalibration(nullptr), m_Near(0.0f), m_Far(0.0f), m_AspectRatio(0.0f)
                        {
                        }

                        CExtendedPerspectiveCamera::~CExtendedPerspectiveCamera()
                            = default;

                        bool CExtendedPerspectiveCamera::LoadCalibration(const VisualSpace::Cameras::GeometricCalibration::CCameraGeometricCalibration* pCameraGeometricCalibration, const vreal Near, const vreal Far)
                        {
                            if (pCameraGeometricCalibration && (Far > 0.0) && (Near > 0.0) && (Far - Near > FLT_EPSILON))
                            {

                                //XXX XXX XXX!!!
                                /* Verify Values (signs don`t match with:
                                 * http://strawlab.org/2011/11/05/augmented-reality-with-OpenGL/
                                 *
                                 * see also:
                                 * http://sightations.wordpress.com/2010/08/03/simulating-calibrated-cameras-in-opengl/
                                 * http://www.songho.ca/opengl/gl_projectionmatrix.html
                                 * http://www.songho.ca/opengl/gl_transform.html
                                 */

                                m_pCameraGeometricCalibration = pCameraGeometricCalibration;
                                m_Near = Near;
                                m_Far = Far;
                                const Mathematics::_3D::CMatrix3D K = m_pCameraGeometricCalibration->GetCalibrationMatrix();
                                m_ImageSize.setValue(m_pCameraGeometricCalibration->GetImageWidth(), m_pCameraGeometricCalibration->GetImageHeight());
                                m_FocalLenght.setValue(m_pCameraGeometricCalibration->GetFocalLengthX(), m_pCameraGeometricCalibration->GetFocalLengthY());
                                m_PrincipalPoint.setValue(m_pCameraGeometricCalibration->GetImagePrincipalPoint().GetX(), m_pCameraGeometricCalibration->GetImagePrincipalPoint().GetY());
                                m_FieldOfView.setValue(atanf((m_ImageSize[0] - m_PrincipalPoint[0]) / m_FocalLenght[0]) + atanf(m_PrincipalPoint[0] / m_FocalLenght[0]), atanf((m_ImageSize[1] - m_PrincipalPoint[1]) / m_FocalLenght[1]) + atanf(m_PrincipalPoint[1] / m_FocalLenght[1]));
                                m_AspectRatio = m_FocalLenght[1] / m_FocalLenght[0];
                                m_ProjectionMatrix[0][0] = (2.0f * K[0][0] / m_pCameraGeometricCalibration->GetImageWidth());
                                m_ProjectionMatrix[0][1] = 0.0f;
                                m_ProjectionMatrix[0][2] = 0.0f;
                                m_ProjectionMatrix[0][3] = 0.0f;
                                m_ProjectionMatrix[1][0] = (2.0f * K[0][1] / m_pCameraGeometricCalibration->GetImageWidth());
                                m_ProjectionMatrix[1][1] = (2.0f * K[1][1] / m_pCameraGeometricCalibration->GetImageHeight());
                                m_ProjectionMatrix[1][2] = 0.0;
                                m_ProjectionMatrix[1][3] = 0.0;
                                m_ProjectionMatrix[2][0] = -((2.0f * K[0][2] / m_pCameraGeometricCalibration->GetImageWidth()) - 1.0f);
                                m_ProjectionMatrix[2][1] = ((2.0f * K[1][2] / m_pCameraGeometricCalibration->GetImageHeight()) - 1.0f);
                                m_ProjectionMatrix[2][2] = -(m_Far + m_Near) / (m_Far - m_Near);
                                m_ProjectionMatrix[2][3] = -1.0f;
                                m_ProjectionMatrix[3][0] = 0.0f;
                                m_ProjectionMatrix[3][1] = 0.0f;
                                m_ProjectionMatrix[3][2] = (-2.0f * m_Far * m_Near) / (m_Far - m_Near);
                                m_ProjectionMatrix[3][3] = 0.0f;
                                return true;
                            }
                            return false;
                        }

                        bool CExtendedPerspectiveCamera::SetRenderingMode(const RenderingMode Mode)
                        {
                            switch (Mode)
                            {
                                case eVirtualCameraParameteres:
                                    m_RenderingMode = eVirtualCameraParameteres;
                                    return true;
                                case eCalibrationCameraParameters:
                                    if (m_pCameraGeometricCalibration)
                                    {
                                        m_RenderingMode = eCalibrationCameraParameters;
                                        return true;
                                    }
                                    return false;
                            }
                            return false;
                        }

                        SbViewVolume CExtendedPerspectiveCamera::getViewVolume(const vreal UseAspectRatio) const
                        {
                            switch (m_RenderingMode)
                            {
                                case eVirtualCameraParameteres:
                                    return SoPerspectiveCamera::getViewVolume(UseAspectRatio);
                                case eCalibrationCameraParameters:
                                {
                                    SbViewVolume ViewVolume;
                                    ViewVolume.perspective(m_FieldOfView[1], m_AspectRatio, m_Near, m_Far);
                                    ViewVolume.rotateCamera(orientation.getValue());
                                    ViewVolume.translateCamera(position.getValue());
                                    return ViewVolume;
                                }
                            }
                            return SbViewVolume();
                        }

                        void CExtendedPerspectiveCamera::GLRender(SoGLRenderAction* pAction)
                        {
                            switch (m_RenderingMode)
                            {
                                case eVirtualCameraParameteres:
                                    SoPerspectiveCamera::GLRender(pAction);
                                    break;
                                case eCalibrationCameraParameters:
                                {
                                    SoState* pState = pAction->getState();
                                    SoProjectionMatrixElement::set(pState, this, m_ProjectionMatrix);
                                    SbMatrix TransformationMatrix;
                                    TransformationMatrix.setTransform(position.getValue(), orientation.getValue(), SbVec3f(1.0, 1.0, 1.0));
                                    SoViewingMatrixElement::set(pState, this, TransformationMatrix.inverse());
                                    SoFocalDistanceElement::set(pState, this, focalDistance.getValue());
                                }
                                break;
                            }
                        }
                        void CExtendedPerspectiveCamera::callback(SoCallbackAction* pAction)
                        {
                            switch (m_RenderingMode)
                            {
                                case eVirtualCameraParameteres:
                                    SoPerspectiveCamera::callback(pAction);
                                    break;
                                case eCalibrationCameraParameters:
                                {
                                    SoState* pState = pAction->getState();
                                    SoProjectionMatrixElement::set(pState, this, m_ProjectionMatrix);
                                    SbMatrix TransformationMatrix;
                                    TransformationMatrix.setTransform(position.getValue(), orientation.getValue(), SbVec3f(1.0, 1.0, 1.0));
                                    SoViewingMatrixElement::set(pState, this, TransformationMatrix.inverse());
                                    SoFocalDistanceElement::set(pState, this, focalDistance.getValue());

                                }
                                break;
                            }

                        }
                        const SbMatrix& CExtendedPerspectiveCamera::GetGLProjectionMatrix()
                        {
                            return m_ProjectionMatrix;
                        }

                        void CExtendedPerspectiveCamera::SetGLProjectionMatrix(const SbMatrix& projectionMatrix)
                        {
                            m_ProjectionMatrix = projectionMatrix;
                        }


                    }
                }
            }
        }
    }
}

#endif
