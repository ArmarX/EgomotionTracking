/*
 * VisualizationcoordinateSystem.h
 *
 *  Created on: Mar 1, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

//OPEN_INVENTOR
#include "OpenInventor.h"

//VISUALIZATION
#include "VisualizationPrimitive.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    class CVisualizationcoordinateSystem: public CVisualizationPrimitive
                    {
                    public:

                        CVisualizationcoordinateSystem();
                        ~CVisualizationcoordinateSystem() override;

                        bool SetColors(const CVisualizationPrimitive::VisualizationPredefinedColor AxisXColor, const CVisualizationPrimitive::VisualizationPredefinedColor AxisYColor, const CVisualizationPrimitive::VisualizationPredefinedColor AxisZColor);
                        void GetColors(SbColor& AxisXColor, SbColor& AxisYColor, SbColor& AxisZColor);

                        void SetTransparency(const vreal Transparency) override;

                        void SetAxesLength(const vreal Lenght);
                        vreal GetAxesLength();

                        void SetConeLength(const vreal Lenght);
                        vreal GetConeLength();

                        void SetConeWidth(const vreal Width);
                        vreal GetConeWidth();

                        void Update(const bool FullUpdate) override;

                        bool SetLabels(const_string pLabelX, const_string pLabelY, const_string pLabelZ);

                    protected:

                        vreal m_AxisLenght;
                        vreal m_ConeLenght;
                        vreal m_ConeWidth;
                        SoCoordinate3* m_pAxesCoordinate3;
                        SoLineSet* m_pAxesLineSet;
                        struct Axis
                        {
                            SbString m_Label;
                            SoTransform* m_pConeTransform;
                            SoMaterial* m_pConeMaterial;
                            SoCone* m_pCone;
                            SoTranslation* m_pLabelTranslation;
                            SoMaterial* m_pLabelMaterial;
                            SoText2* m_pLabelText;
                        };
                        Axis m_Axes[3];
                    };
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
