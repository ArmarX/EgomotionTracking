/*
 * VisualizationSphere.cpp
 *
 *  Created on: Feb 22, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "VisualizationSphere.h"
#include "VisualizationVolume.h"

#ifdef _USE_OPEN_INVENTOR_

using namespace EVP::Mathematics::_3D::Geometry;

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    CVisualizationSphere::CVisualizationSphere(CSphere3D* pSphere, CVisualizationVolume* pVisualizationVolume) :
                        CVisualizationGeometricPrimitive(pSphere, pVisualizationVolume)
                    {
                        m_pSoTranslation = new SoTranslation;
                        m_pBaseSeparator->addChild(m_pSoTranslation);
                        m_pSoSphere = new SoSphere;
                        m_pBaseSeparator->addChild(m_pSoSphere);
                        m_pSoSphere->setUserData(this);
                        m_pBaseMaterial->transparency = _VISUALIZATION_SPHERE_DEFAULT_TRANSPARENCY_;
                        Update(true);
                    }

                    CVisualizationSphere::~CVisualizationSphere()
                        = default;

                    bool CVisualizationSphere::IsComposedVisible()
                    {
                        if (m_IsVisible && m_pGeometricPrimitive)
                        {
                            if (m_pVisualizationVolume)
                            {
                                const CSphere3D* pSphere = (const CSphere3D*) m_pGeometricPrimitive;
                                return m_pVisualizationVolume->MapSphere(T(pSphere->GetCenter()), T(pSphere->GetRadius()));
                            }
                            return true;
                        }
                        return false;
                    }

                    void CVisualizationSphere::Update(const bool FullUpdate)
                    {
                        if (FullUpdate && m_pGeometricPrimitive)
                        {
                            const CSphere3D* pSphere = (const CSphere3D*) m_pGeometricPrimitive;
                            m_pSoSphere->radius = T(pSphere->GetRadius());
                            m_pSoTranslation->translation.setValue(T(pSphere->GetCenter()));
                        }
                        SwitchVisibility(IsComposedVisible());
                    }
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
