/*
 * VisualizationPointPair.cpp
 *
 *  Created on: Feb 23, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "VisualizationPointPair.h"
#include "VisualizationVolume.h"

#ifdef _USE_OPEN_INVENTOR_

using namespace EVP::Mathematics::_3D::Geometry;

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    CVisualizationPointPair::CVisualizationPointPair(CPointPair3D* pPointPair, CVisualizationVolume* pVisualizationVolume) :
                        CVisualizationGeometricPrimitive(pPointPair, pVisualizationVolume)
                    {
                        m_pSoTranslationA = new SoTranslation;
                        m_pBaseSeparator->addChild(m_pSoTranslationA);
                        m_pSphereASwitch = new SoSwitch;
                        m_pBaseSeparator->addChild(m_pSphereASwitch);
                        m_pSoSphereA = new SoSphere;
                        m_pSphereASwitch->addChild(m_pSoSphereA);
                        m_pSoSphereA->setUserData(this);
                        m_pSoTranslationB = new SoTranslation;
                        m_pBaseSeparator->addChild(m_pSoTranslationB);
                        m_pSphereBSwitch = new SoSwitch;
                        m_pBaseSeparator->addChild(m_pSphereBSwitch);
                        m_pSoSphereB = new SoSphere;
                        m_pSphereBSwitch->addChild(m_pSoSphereB);
                        m_pSoSphereB->setUserData(this);
                        m_pConnectingLineSwitch = new SoSwitch;
                        m_pBaseSeparator->addChild(m_pConnectingLineSwitch);
                        m_pConnectingLineDrawStyle = new SoDrawStyle;
                        m_pConnectingLineSwitch->addChild(m_pConnectingLineDrawStyle);
                        m_pConnectingLineCoordinate3 = new SoCoordinate3;
                        m_pConnectingLineSwitch->addChild(m_pConnectingLineCoordinate3);
                        m_pConnectingLineSet = new SoLineSet;
                        m_pConnectingLineSwitch->addChild(m_pConnectingLineSet);
                        m_pConnectingLineSet->setUserData(this);
                        m_pConnectingLineSwitch->whichChild = SO_SWITCH_ALL;
                        m_pSphereASwitch->whichChild = 0;
                        m_pSphereBSwitch->whichChild = 0;
                        m_ConnectingLineVisible = true;
                        m_pSoSphereA->radius = _VISUALIZATION_POINT_PAIR_MINIMAL_DISPLAY_RADIUS_;
                        m_pSoSphereB->radius = _VISUALIZATION_POINT_PAIR_MINIMAL_DISPLAY_RADIUS_;
                        m_pConnectingLineDrawStyle->linePattern = _VISUALIZATION_POINT_PAIR_DEFAULT_CONNECTING_LINE_PATTERN_;
                        m_pConnectingLineDrawStyle->lineWidth = _VISUALIZATION_POINT_PAIR_DEFAULT_CONNECTING_LINE_WIDTH_;
                        m_pBaseMaterial->transparency = _VISUALIZATION_POINT_PAIR_DEFAULT_TRANSPARENCY_;
                        Update(true);
                    }

                    CVisualizationPointPair::~CVisualizationPointPair()
                        = default;

                    void CVisualizationPointPair::SetDisplayRadius(const vreal DisplayRadius)
                    {
                        const vreal InRangeDisplayRadius = TMin(TMax(DisplayRadius, _VISUALIZATION_POINT_PAIR_MINIMAL_DISPLAY_RADIUS_), _VISUALIZATION_POINT_PAIR_MAXIMAL_DISPLAY_RADIUS_);
                        if (m_pSoSphereA->radius.getValue() != InRangeDisplayRadius)
                        {
                            m_pSoSphereA->radius = InRangeDisplayRadius;
                            m_pSoSphereB->radius = InRangeDisplayRadius;
                        }
                    }

                    vreal CVisualizationPointPair::GetDisplayaRadius() const
                    {
                        return m_pSoSphereA->radius.getValue();
                    }

                    void CVisualizationPointPair::SetConnectingLineVisible(const bool Visible)
                    {
                        if (m_ConnectingLineVisible != Visible)
                        {
                            m_ConnectingLineVisible = Visible;
                            if (m_ConnectingLineVisible)
                            {
                                if (m_pVisualizationVolume)
                                {
                                    bool FlagA = false, FlagB = false;
                                    const CPointPair3D* pPointPair = (const CPointPair3D*) m_pGeometricPrimitive;
                                    m_pVisualizationVolume->MapPointPair(T(pPointPair->GetPointA()), T(pPointPair->GetPointB()), FlagA, FlagB);
                                    m_pConnectingLineSwitch->whichChild = (FlagA && FlagB) ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                                }
                                else
                                {
                                    m_pConnectingLineSwitch->whichChild = SO_SWITCH_ALL;
                                }
                            }
                            else
                            {
                                m_pConnectingLineSwitch->whichChild = SO_SWITCH_NONE;
                            }
                        }
                    }

                    bool CVisualizationPointPair::GetConnectingLineVisible() const
                    {
                        return m_ConnectingLineVisible;
                    }

                    void CVisualizationPointPair::SetConnectingLineWidth(const vreal LineWidth)
                    {
                        m_pConnectingLineDrawStyle->lineWidth = LineWidth;
                    }

                    vreal CVisualizationPointPair::GetConnectingLineWidth() const
                    {
                        return m_pConnectingLineDrawStyle->lineWidth.getValue();
                    }

                    void CVisualizationPointPair::SetConnectingLinePattern(const ushort Pattern)
                    {
                        m_pConnectingLineDrawStyle->linePattern = Pattern;
                    }

                    ushort CVisualizationPointPair::GetConnectingLinePattern() const
                    {
                        return m_pConnectingLineDrawStyle->linePattern.getValue();
                    }

                    bool CVisualizationPointPair::IsComposedVisible()
                    {
                        if (m_IsVisible && m_pGeometricPrimitive)
                        {
                            if (m_pVisualizationVolume)
                            {
                                bool FlagA = false, FlagB = false;
                                const CPointPair3D* pPointPair = (const CPointPair3D*) m_pGeometricPrimitive;
                                m_pVisualizationVolume->MapPointPair(T(pPointPair->GetPointA()), T(pPointPair->GetPointB()), FlagA, FlagB);
                                m_pConnectingLineSwitch->whichChild = (m_ConnectingLineVisible && FlagA && FlagB) ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                                m_pSphereASwitch->whichChild = FlagA ? 0 : SO_SWITCH_NONE;
                                m_pSphereBSwitch->whichChild = FlagB ? 0 : SO_SWITCH_NONE;
                                return (FlagA || FlagB);
                            }
                            else
                            {
                                m_pConnectingLineSwitch->whichChild = m_ConnectingLineVisible ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                                m_pSphereASwitch->whichChild = 0;
                                m_pSphereBSwitch->whichChild = 0;
                                return true;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }

                    void CVisualizationPointPair::Update(const bool FullUpdate)
                    {
                        if (FullUpdate && m_pGeometricPrimitive)
                        {
                            CPointPair3D* pPointPair = (CPointPair3D*) m_pGeometricPrimitive;
                            const SbVec3f& A = T(pPointPair->GetPointA());
                            const SbVec3f& B = T(pPointPair->GetPointB());
                            const SbVec3f Delta = B - A;
                            m_pSoTranslationA->translation.setValue(A);
                            m_pSoTranslationB->translation.setValue(Delta);
                            m_pConnectingLineCoordinate3->point.set1Value(0, -Delta);
                            m_pConnectingLineCoordinate3->point.set1Value(1, SbVec3f(vreal(0.0), vreal(0.0), vreal(0.0)));
                            m_pConnectingLineSet->numVertices.setValue(2);
                        }
                        SwitchVisibility(IsComposedVisible());
                    }
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
