/*
 * Visualization.cpp
 *
 *  Created on: Feb 23, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "Visualization.h"

#ifdef _USE_OPEN_INVENTOR_

using namespace EVP::Mathematics::_3D::Geometry;

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    CVisualizationGeometricPrimitive* CVisualization::CreateVisualization(CGeometricPrimitive3D* pGeometricPrimitive, CVisualization* pVisualization, const bool SetVisualizationVolume)
                    {
                        if (pGeometricPrimitive)
                            switch (pGeometricPrimitive->GetTypeId())
                            {
                                case CGeometricPrimitive3D::eNone:
                                    return nullptr;
                                    break;
                                case CGeometricPrimitive3D::ePoint:
                                    return CreateVisualization(dynamic_cast<CPoint3D*>(pGeometricPrimitive), pVisualization, SetVisualizationVolume);
                                    break;
                                case CGeometricPrimitive3D::ePointPair:
                                    return CreateVisualization(dynamic_cast<CPointPair3D*>(pGeometricPrimitive), pVisualization, SetVisualizationVolume);
                                    break;
                                case CGeometricPrimitive3D::eLine:
                                    return CreateVisualization(dynamic_cast<CLine3D*>(pGeometricPrimitive), pVisualization, SetVisualizationVolume);
                                    break;
                                case CGeometricPrimitive3D::eCircle:
                                    return CreateVisualization(dynamic_cast<CCircle3D*>(pGeometricPrimitive), pVisualization, SetVisualizationVolume);
                                    break;
                                case CGeometricPrimitive3D::ePlane:
                                    return CreateVisualization(dynamic_cast<CPlane3D*>(pGeometricPrimitive), pVisualization, SetVisualizationVolume);
                                    break;
                                case CGeometricPrimitive3D::eSphere:
                                    return CreateVisualization(dynamic_cast<CSphere3D*>(pGeometricPrimitive), pVisualization, SetVisualizationVolume);
                                    break;
                            }
                        return nullptr;
                    }

                    CVisualizationPoint* CVisualization::CreateVisualization(CPoint3D* pPoint, CVisualization* pVisualization, const bool SetVisualizationVolume)
                    {
                        CVisualizationPoint* pVisualizationPoint = nullptr;
                        if (pPoint)
                        {
                            pVisualizationPoint = new CVisualizationPoint(pPoint, (pVisualization && SetVisualizationVolume) ? pVisualization->m_pVisualizationVolume : nullptr);
                            if (pVisualization && pVisualizationPoint)
                            {
                                pVisualization->AddVisualizationElement(pVisualizationPoint, false, false);
                            }
                        }
                        return pVisualizationPoint;
                    }

                    CVisualizationPointPair* CVisualization::CreateVisualization(CPointPair3D* pPointPair, CVisualization* pVisualization, const bool SetVisualizationVolume)
                    {
                        CVisualizationPointPair* pVisualizationPointPair = nullptr;
                        if (pPointPair)
                        {
                            pVisualizationPointPair = new CVisualizationPointPair(pPointPair, (pVisualization && SetVisualizationVolume) ? pVisualization->m_pVisualizationVolume : nullptr);
                            if (pVisualization && pVisualizationPointPair)
                            {
                                pVisualization->AddVisualizationElement(pVisualizationPointPair, false, false);
                            }
                        }
                        return pVisualizationPointPair;
                    }

                    CVisualizationLine* CVisualization::CreateVisualization(CLine3D* pLine, CVisualization* pVisualization, const bool SetVisualizationVolume)
                    {
                        CVisualizationLine* pVisualizationLine = nullptr;
                        if (pLine)
                        {
                            pVisualizationLine = new CVisualizationLine(pLine, (pVisualization && SetVisualizationVolume) ? pVisualization->m_pVisualizationVolume : nullptr);
                            if (pVisualization && pVisualizationLine)
                            {
                                pVisualization->AddVisualizationElement(pVisualizationLine, false, false);
                            }
                        }
                        return pVisualizationLine;
                    }

                    CVisualizationCircle* CVisualization::CreateVisualization(CCircle3D* CCircle, CVisualization* pVisualization, const bool SetVisualizationVolume)
                    {
                        CVisualizationCircle* pVisualizationCircle = nullptr;
                        if (CCircle)
                        {
                            pVisualizationCircle = new CVisualizationCircle(CCircle, (pVisualization && SetVisualizationVolume) ? pVisualization->m_pVisualizationVolume : nullptr);
                            if (pVisualization && pVisualizationCircle)
                            {
                                pVisualization->AddVisualizationElement(pVisualizationCircle, false, false);
                            }
                        }
                        return pVisualizationCircle;
                    }

                    CVisualizationPlane* CVisualization::CreateVisualization(CPlane3D* pPlane, CVisualization* pVisualization, const bool SetVisualizationVolume)
                    {
                        CVisualizationPlane* pVisualizationPlane = nullptr;
                        if (pPlane)
                        {
                            pVisualizationPlane = new CVisualizationPlane(pPlane, (pVisualization && SetVisualizationVolume) ? pVisualization->m_pVisualizationVolume : nullptr);
                            if (pVisualization && pVisualizationPlane)
                            {
                                pVisualization->AddVisualizationElement(pVisualizationPlane, false, false);
                            }
                        }
                        return pVisualizationPlane;
                    }

                    CVisualizationSphere* CVisualization::CreateVisualization(CSphere3D* pSphere, CVisualization* pVisualization, const bool SetVisualizationVolume)
                    {
                        CVisualizationSphere* pVisualizationSphere = nullptr;
                        if (pSphere)
                        {
                            pVisualizationSphere = new CVisualizationSphere(pSphere, (pVisualization && SetVisualizationVolume) ? pVisualization->m_pVisualizationVolume : nullptr);
                            if (pVisualization && pVisualizationSphere)
                            {
                                pVisualization->AddVisualizationElement(pVisualizationSphere, false, false);
                            }
                        }
                        return pVisualizationSphere;
                    }

                    CVisualization::CVisualization(const SbBox3f& BoundingBox, const bool OwnerShip)
                    {
                        m_pVisualizationSwitch = new SoSwitch;
                        m_pVisualizationSwitch->ref();
                        m_pVisualizationSwitch->whichChild = SO_SWITCH_ALL;
                        m_pVisualizationVolume = new CVisualizationVolume(BoundingBox);
                        m_pVisualizationSwitch->addChild(m_pVisualizationVolume->GetBaseSwitch());
                        m_OwnerShip = OwnerShip;
                    }

                    CVisualization::~CVisualization()
                    {
                        RemoveAllElements(m_OwnerShip);
                        m_pVisualizationSwitch->removeAllChildren();
                        m_pVisualizationSwitch->unref();
                        RELEASE_OBJECT(m_pVisualizationVolume)
                    }

                    SoSwitch* CVisualization::GetBaseSwitch() const
                    {
                        return m_pVisualizationSwitch;
                    }

                    CVisualizationVolume* CVisualization::GetVisualizationVolume() const
                    {
                        return m_pVisualizationVolume;
                    }

                    void CVisualization::SetVisible(const bool Visible)
                    {
                        m_pVisualizationSwitch->whichChild = Visible ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                    }

                    bool CVisualization::GetVisible() const
                    {
                        return (m_pVisualizationSwitch->whichChild.getValue() == SO_SWITCH_ALL);
                    }

                    bool CVisualization::AddVisualizationElement(CVisualizationGeometricPrimitive* pVisualizationPrimitive, const bool SetVisualizationVolume)
                    {
                        return AddVisualizationElement(pVisualizationPrimitive, SetVisualizationVolume);
                    }

                    CVisualizationGeometricPrimitive* CVisualization::GetVisualizationElementByInstanceId(const Identifier InstanceId)
                    {
                        return FindElement(InstanceId);
                    }

                    vector<CVisualizationGeometricPrimitive*> CVisualization::GetVisualizationElementsByTypeID(CGeometricPrimitive3D::GeometricPrimitive3DTypeId Id)
                    {
                        vector<CVisualizationGeometricPrimitive*> Elements;
                        for (list<CVisualizationGeometricPrimitive*>::iterator ppVisualizationPrimitive = m_VisualizationElement.begin(); ppVisualizationPrimitive != m_VisualizationElement.end(); ++ppVisualizationPrimitive)
                            if ((*ppVisualizationPrimitive)->GetGeometricPrimitive()->GetTypeId() == Id)
                            {
                                Elements.push_back(*ppVisualizationPrimitive);
                            }
                        return Elements;
                    }

                    bool CVisualization::RemoveVisualizationElement(CVisualizationGeometricPrimitive* pVisualizationPrimitive)
                    {
                        for (list<CVisualizationGeometricPrimitive*>::iterator ppVisualizationPrimitive = m_VisualizationElement.begin(); ppVisualizationPrimitive != m_VisualizationElement.end(); ++ppVisualizationPrimitive)
                            if ((*ppVisualizationPrimitive) == pVisualizationPrimitive)
                            {
                                m_pVisualizationSwitch->removeChild(pVisualizationPrimitive->GetBaseSwitch());
                                m_VisualizationElement.erase(ppVisualizationPrimitive);
                                return true;
                            }
                        return false;
                    }

                    bool CVisualization::RemoveVisualizationElementByInstanceId(const Identifier InstanceId, const bool DeleteElement)
                    {
                        for (list<CVisualizationGeometricPrimitive*>::iterator ppVisualizationPrimitive = m_VisualizationElement.begin(); ppVisualizationPrimitive != m_VisualizationElement.end(); ++ppVisualizationPrimitive)
                            if ((*ppVisualizationPrimitive)->GetGeometricPrimitive()->GetInstanceId() == InstanceId)
                            {
                                CVisualizationGeometricPrimitive* pVisualizationPrimitive = *ppVisualizationPrimitive;
                                m_pVisualizationSwitch->removeChild(pVisualizationPrimitive->GetBaseSwitch());
                                m_VisualizationElement.erase(ppVisualizationPrimitive);
                                if (DeleteElement)
                                    RELEASE_OBJECT(pVisualizationPrimitive)
                                    return true;
                            }
                        return false;
                    }

                    uint CVisualization::RemoveVisualizationElementsByTypeID(const CGeometricPrimitive3D::GeometricPrimitive3DTypeId Id, const bool DeleteElements)
                    {
                        uint TotalRemoved = 0;
                        for (list<CVisualizationGeometricPrimitive*>::iterator ppVisualizationPrimitive = m_VisualizationElement.begin(); ppVisualizationPrimitive != m_VisualizationElement.end(); ++ppVisualizationPrimitive)
                            if ((*ppVisualizationPrimitive)->GetGeometricPrimitive()->GetTypeId() == Id)
                            {
                                CVisualizationGeometricPrimitive* pVisualizationPrimitive = *ppVisualizationPrimitive;
                                m_pVisualizationSwitch->removeChild(pVisualizationPrimitive->GetBaseSwitch());
                                m_VisualizationElement.erase(ppVisualizationPrimitive);
                                if (DeleteElements)
                                    RELEASE_OBJECT(pVisualizationPrimitive)
                                    ++TotalRemoved;
                            }
                        return TotalRemoved;
                    }

                    void CVisualization::RemoveAllElements(const bool DeleteElements)
                    {
                        if (m_VisualizationElement.size())
                        {
                            if (m_pVisualizationSwitch->getNumChildren() > 1)
                            {
                                m_pVisualizationSwitch->removeAllChildren();
                                m_pVisualizationSwitch->addChild(m_pVisualizationVolume->GetBaseSwitch());
                            }
                            if (DeleteElements)
                                for (list<CVisualizationGeometricPrimitive*>::iterator ppVisualizationPrimitive = m_VisualizationElement.begin(); ppVisualizationPrimitive != m_VisualizationElement.end(); ++ppVisualizationPrimitive)
                                    RELEASE_OBJECT_DIRECT(*ppVisualizationPrimitive)
                                    m_VisualizationElement.clear();
                        }
                    }

                    CVisualizationGeometricPrimitive* CVisualization::FindElement(const Identifier InstanceId)
                    {
                        for (list<CVisualizationGeometricPrimitive*>::const_iterator ppVisualizationPrimitive = m_VisualizationElement.begin(); ppVisualizationPrimitive != m_VisualizationElement.end(); ++ppVisualizationPrimitive)
                            if ((*ppVisualizationPrimitive)->GetGeometricPrimitive()->GetInstanceId() == InstanceId)
                            {
                                return *ppVisualizationPrimitive;
                            }
                        return nullptr;
                    }

                    bool CVisualization::AddVisualizationElement(CVisualizationGeometricPrimitive* pVisualizationPrimitive, const bool AvoidRepetitions, const bool SetVisualizationVolume)
                    {
                        if (pVisualizationPrimitive)
                        {
                            if (AvoidRepetitions && FindElement(pVisualizationPrimitive->GetGeometricPrimitive()->GetInstanceId()))
                            {
                                return false;
                            }
                            m_pVisualizationSwitch->addChild(pVisualizationPrimitive->GetBaseSwitch());
                            if (SetVisualizationVolume)
                            {
                                pVisualizationPrimitive->SetVisualizationVolume(m_pVisualizationVolume);
                            }
                            m_VisualizationElement.push_back(pVisualizationPrimitive);
                            return true;
                        }
                        return false;
                    }
                }
            }
        }
    }
}

#endif /*_USE_OPEN_INVENTOR_*/
