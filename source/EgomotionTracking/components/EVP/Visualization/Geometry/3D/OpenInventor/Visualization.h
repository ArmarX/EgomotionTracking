/*
 * Visualization.h
 *
 *  Created on: Feb 23, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

//OPEN_INVENTOR
#include "OpenInventor.h"

//GEOMETRY
#include "../../../../Foundation/Mathematics/3D/Geometry/GeometricPrimitive3D.h"
#include "../../../../Foundation/Mathematics/3D/Geometry/Point3D.h"
#include "../../../../Foundation/Mathematics/3D/Geometry/PointPair3D.h"
#include "../../../../Foundation/Mathematics/3D/Geometry/Line3D.h"
#include "../../../../Foundation/Mathematics/3D/Geometry/Circle3D.h"
#include "../../../../Foundation/Mathematics/3D/Geometry/Plane3D.h"
#include "../../../../Foundation/Mathematics/3D/Geometry/Sphere3D.h"

//VISUALIZATION
#include "VisualizationGeometricPrimitive.h"
#include "VisualizationPoint.h"
#include "VisualizationPointPair.h"
#include "VisualizationLine.h"
#include "VisualizationCircle.h"
#include "VisualizationPlane.h"
#include "VisualizationSphere.h"
#include "VisualizationVolume.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    class CVisualization
                    {
                    public:

                        static CVisualizationGeometricPrimitive* CreateVisualization(Mathematics::_3D::Geometry::CGeometricPrimitive3D* pGeometricPrimitive, CVisualization* pVisualization, const bool SetVisualizationVolume);
                        static CVisualizationPoint* CreateVisualization(Mathematics::_3D::Geometry::CPoint3D* pPoint, CVisualization* pVisualization, const bool SetVisualizationVolume);
                        static CVisualizationPointPair* CreateVisualization(Mathematics::_3D::Geometry::CPointPair3D* pPointPair, CVisualization* pVisualization, const bool SetVisualizationVolume);
                        static CVisualizationLine* CreateVisualization(Mathematics::_3D::Geometry::CLine3D* pLine, CVisualization* pVisualization, const bool SetVisualizationVolume);
                        static CVisualizationCircle* CreateVisualization(Mathematics::_3D::Geometry::CCircle3D* pCircle, CVisualization* pVisualization, const bool SetVisualizationVolume);
                        static CVisualizationPlane* CreateVisualization(Mathematics::_3D::Geometry::CPlane3D* pPlane, CVisualization* pVisualization, const bool SetVisualizationVolume);
                        static CVisualizationSphere* CreateVisualization(Mathematics::_3D::Geometry::CSphere3D* pSphere, CVisualization* pVisualization, const bool SetVisualizationVolume);

                        CVisualization(const SbBox3f& BoundingBox, const bool OwnerShip = true);
                        virtual ~CVisualization();

                        SoSwitch* GetBaseSwitch() const;
                        CVisualizationVolume* GetVisualizationVolume() const;

                        void SetVisible(const bool Visible);
                        bool GetVisible() const;

                        bool AddVisualizationElement(CVisualizationGeometricPrimitive* pVisualizationPrimitive, const bool SetVisualizationVolume);
                        CVisualizationGeometricPrimitive* GetVisualizationElementByInstanceId(const Identifier InstanceId);
                        vector<CVisualizationGeometricPrimitive*> GetVisualizationElementsByTypeID(Mathematics::_3D::Geometry::CGeometricPrimitive3D::GeometricPrimitive3DTypeId Id);

                        bool RemoveVisualizationElement(CVisualizationGeometricPrimitive* pVisualizationPrimitive);
                        bool RemoveVisualizationElementByInstanceId(const Identifier InstanceId, const bool DeleteElements);
                        uint RemoveVisualizationElementsByTypeID(const Mathematics::_3D::Geometry::CGeometricPrimitive3D::GeometricPrimitive3DTypeId Id, const bool DeleteElement);
                        void RemoveAllElements(const bool DeleteElements);

                    protected:

                        inline CVisualizationGeometricPrimitive* FindElement(const Identifier InstanceId);
                        inline  bool AddVisualizationElement(CVisualizationGeometricPrimitive* pVisualizationPrimitive, const bool AvoidRepetitions, const bool SetVisualizationVolume);

                        bool m_OwnerShip;
                        SoSwitch* m_pVisualizationSwitch;
                        CVisualizationVolume* m_pVisualizationVolume;
                        list<CVisualizationGeometricPrimitive*> m_VisualizationElement;
                    };
                }
            }
        }
    }
}

#endif /*_USE_OPEN_INVENTOR_*/
