/*
 * VisualizationLine.h
 *
 *  Created on: Feb 23, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

//OPEN_INVENTOR
#include "OpenInventor.h"

//GEOMETRY
#include "../../../../Foundation/Mathematics/3D/Geometry/Line3D.h"

//VISUALIZATION
#include "VisualizationGeometricPrimitive.h"

#ifdef _USE_OPEN_INVENTOR_

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    class CVisualizationLine: public CVisualizationGeometricPrimitive
                    {
                    public:

                        CVisualizationLine(Mathematics::_3D::Geometry::CLine3D* pLine, CVisualizationVolume* pVisualizationVolume);
                        ~CVisualizationLine() override;

                        void Update(const bool FullUpdate = true) override;

                    protected:

                        bool IsComposedVisible() override;

                        SoCoordinate3* m_pSoCoordinate3;
                        SoLineSet* m_pSoLineSet;
                    };
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
