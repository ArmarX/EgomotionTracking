/*
 * VisualizationPlane.cpp
 *
 *  Created on: Feb 23, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "VisualizationPlane.h"
#include "VisualizationVolume.h"

#ifdef _USE_OPEN_INVENTOR_

using namespace EVP::Mathematics::_3D::Geometry;
using namespace EVP::Mathematics::_3D;

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    CVisualizationPlane::CVisualizationPlane(CPlane3D* pPlane, CVisualizationVolume* pVisualizationVolume) :
                        CVisualizationGeometricPrimitive(pPlane, pVisualizationVolume)
                    {
                        m_pSoCoordinate3 = new SoCoordinate3;
                        m_pBaseSeparator->addChild(m_pSoCoordinate3);
                        m_SoFaceSet = new SoFaceSet;
                        m_pBaseSeparator->addChild(m_SoFaceSet);
                        m_SoFaceSet->setUserData(this);
                        m_pBaseMaterial->transparency = _VISUALIZATION_PLANE_DEFAULT_TRANSPARENCY_;
                        m_DisplayBySourcePoints = pPlane->HasSourcePoints();
                        Update(true);
                    }

                    CVisualizationPlane::~CVisualizationPlane()
                        = default;

                    bool CVisualizationPlane::HasSourcePoints() const
                    {
                        return m_pGeometricPrimitive ? ((const CPlane3D*) m_pGeometricPrimitive)->HasSourcePoints() : false;
                    }

                    void CVisualizationPlane::SetDisplayBySourcePoints(const bool Active)
                    {
                        if (HasSourcePoints())
                        {
                            m_DisplayBySourcePoints = Active;
                            Update(true);
                        }
                    }

                    bool CVisualizationPlane::IsComposedVisible()
                    {
                        if (m_IsVisible && m_pGeometricPrimitive)
                        {
                            const CPlane3D* pPlane = (const CPlane3D*) m_pGeometricPrimitive;
                            if (m_pVisualizationVolume)
                            {
                                if (m_DisplayBySourcePoints)
                                {
                                    CVector3D Aq, Bq, Cq;
                                    if (pPlane->GetSourcePoints(Aq, Bq, Cq))
                                    {
                                        m_pSoCoordinate3->point.set1Value(0, T(Aq));
                                        m_pSoCoordinate3->point.set1Value(1, T(Bq));
                                        m_pSoCoordinate3->point.set1Value(2, T(Cq));
                                        m_SoFaceSet->numVertices.setValue(3);
                                        return true;
                                    }
                                    return false;
                                }
                                vector<SbVec3f> Points;
                                if (m_pVisualizationVolume->MapPlane(SbPlane(T(pPlane->GetNormal()), T(pPlane->GetHesseDistance())), Points))
                                {
                                    const uint TotalPoints = Points.size();
                                    for (uint i = 0; i < TotalPoints; ++i)
                                    {
                                        m_pSoCoordinate3->point.set1Value(i, Points[i]);
                                    }
                                    m_SoFaceSet->numVertices.setValue(TotalPoints);
                                    return true;
                                }
                            }
                            else
                            {
                                CVector3D Aq, Bq, Cq;
                                if (pPlane->GetSourcePoints(Aq, Bq, Cq))
                                {
                                    m_pSoCoordinate3->point.set1Value(0, T(Aq));
                                    m_pSoCoordinate3->point.set1Value(1, T(Bq));
                                    m_pSoCoordinate3->point.set1Value(2, T(Cq));
                                    m_SoFaceSet->numVertices.setValue(3);
                                    return true;
                                }
                            }
                        }
                        return false;
                    }

                    void CVisualizationPlane::Update(const bool /*FullUpdate*/)
                    {
                        SwitchVisibility(IsComposedVisible());
                    }
                }
            }
        }
    }
}

#endif /* _USE_OPEN_INVENTOR_ */
