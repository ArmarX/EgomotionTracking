/*
 * HybridViewer.cpp
 *
 *  Created on: Apr 15, 2012
 *      Author: david
 */

#include "HybridViewer.h"

#ifdef _USE_OPEN_INVENTOR_
#ifdef _EVP_USE_GUI_TOOLKIT

#include <Inventor/Qt/SoQt.h>
#include <qwidget.h>

namespace EVP
{
    namespace Visualization
    {
        namespace Geometry
        {
            namespace _3D
            {
                namespace OpenInventor
                {
                    namespace Extensions
                    {
                        CHybridViewer::CHybridViewer() :
                            m_pParent(nullptr), m_pExtendedViewer(nullptr), m_pRoot(nullptr), m_pContent(nullptr), m_pExtendedPerspectiveCamera(nullptr)
                        {
                            int argc = 0;
                            char** argv = nullptr;
                            m_pParent = SoQt::init(argc, argv, "HybridViewer");
                            m_pExtendedViewer = new CExtendedViewer(m_pParent);
                            m_pRoot = new SoExtSelection;
                            m_pExtendedPerspectiveCamera = new CExtendedPerspectiveCamera;
                            m_pRoot->addChild(m_pExtendedPerspectiveCamera);
                            m_pContent = new SoSeparator;
                            m_pRoot->addChild(m_pContent);
                            m_pExtendedViewer->setSceneGraph(m_pRoot);
                            m_pExtendedViewer->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_SORTED_TRIANGLE_BLEND);
                            m_pExtendedViewer->setAntialiasing(true, 2);
                            m_pExtendedViewer->setDecoration(false);
                            m_pExtendedViewer->setBackgroundColor(SbColor(0.5f, 0.5f, 0.5f));
                            m_pExtendedViewer->show();
                            SoQt::show(m_pParent);
                        }

                        CHybridViewer::CHybridViewer(QWidget* pParent) :
                            m_pParent(pParent), m_pExtendedViewer(nullptr), m_pRoot(nullptr), m_pContent(nullptr), m_pExtendedPerspectiveCamera(nullptr)
                        {
                            SoQt::init(m_pParent);
                            m_pExtendedViewer = new CExtendedViewer(m_pParent);
                            m_pRoot = new SoExtSelection;
                            m_pExtendedPerspectiveCamera = new CExtendedPerspectiveCamera;
                            m_pRoot->addChild(m_pExtendedPerspectiveCamera);
                            m_pContent = new SoSeparator;
                            m_pRoot->addChild(m_pContent);
                            m_pExtendedViewer->setSceneGraph(m_pRoot);
                            m_pExtendedViewer->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_SORTED_TRIANGLE_BLEND);
                            m_pExtendedViewer->setAntialiasing(true, 2);
                            m_pExtendedViewer->setDecoration(false);
                            m_pExtendedViewer->setBackgroundColor(SbColor(0.5f, 0.5f, 0.5f));
                            m_pExtendedViewer->show();
                            SoQt::show(m_pParent);
                        }

                        CHybridViewer::~CHybridViewer()
                        {
                            RELEASE_OBJECT(m_pExtendedViewer)
                        }

                        void CHybridViewer::AddContent(SoNode* pNode)
                        {
                            if (pNode)
                            {
                                m_pContent->addChild(pNode);
                            }
                        }

                        void CHybridViewer::ClearContent()
                        {
                            m_pContent->removeAllChildren();
                        }

                        void CHybridViewer::SetGeometry(const uint X, const uint Y, const uint Width, const uint Height)
                        {
                            m_pParent->setGeometry(X, Y, Width, Height);
                        }

                        bool CHybridViewer::LoadCalibration(const VisualSpace::Cameras::GeometricCalibration::CCameraGeometricCalibration* pCameraGeometricCalibration, const vreal Near, const vreal Far)
                        {
                            return m_pExtendedPerspectiveCamera->LoadCalibration(pCameraGeometricCalibration, Near, Far);
                        }

                        bool CHybridViewer::SetRenderingMode(const CExtendedPerspectiveCamera::RenderingMode Mode)
                        {
                            return m_pExtendedPerspectiveCamera->SetRenderingMode(Mode);
                        }

                        void CHybridViewer::SetSourceImage(const TImage<byte>* pSourceImage)
                        {
                            return m_pExtendedViewer->SetSourceImage(pSourceImage);
                        }

                        void CHybridViewer::SetSourceImage(const TImage<CDiscreteTristimulusPixel>* pSourceImage)
                        {
                            return m_pExtendedViewer->SetSourceImage(pSourceImage);
                        }

                        void CHybridViewer::SetSourceImage(const TImage<CDiscreteRGBAPixel>* pSourceImage)
                        {
                            return m_pExtendedViewer->SetSourceImage(pSourceImage);
                        }

                        bool CHybridViewer::UpdateSourceImage()
                        {
                            return m_pExtendedViewer->Update();
                        }

                        void CHybridViewer::DoMainLoop()
                        {
                            SoQt::mainLoop();
                        }

                        void CHybridViewer::Render()
                        {
                            m_pExtendedViewer->render();
                        }

                        void CHybridViewer::SetAntialiasing(const uint Level)
                        {
                            m_pExtendedViewer->setAntialiasing((Level > 1), Level);
                        }

                        void CHybridViewer::SetCameraPose(const SbVec3f& Position, const SbRotation& Orientation)
                        {
                            m_pExtendedPerspectiveCamera->position.setValue(Position);
                            m_pExtendedPerspectiveCamera->orientation.setValue(Orientation);
                        }

                        void CHybridViewer::SetCameraPosition(const SbVec3f& Position)
                        {
                            m_pExtendedPerspectiveCamera->position.setValue(Position);
                        }

                        void CHybridViewer::SetCameraOrientation(const SbRotation& Orientation)
                        {
                            m_pExtendedPerspectiveCamera->orientation.setValue(Orientation);
                        }

                        void CHybridViewer::GetCameraPose(SbVec3f& Position, SbRotation& Orientation) const
                        {
                            Position = m_pExtendedPerspectiveCamera->position.getValue();
                            Orientation = m_pExtendedPerspectiveCamera->orientation.getValue();
                        }

                        SbVec3f CHybridViewer::GetCameraPosition() const
                        {
                            return m_pExtendedPerspectiveCamera->position.getValue();
                        }

                        SbRotation CHybridViewer::GetCameraOrientation() const
                        {
                            return m_pExtendedPerspectiveCamera->orientation.getValue();
                        }
                    }
                }
            }
        }
    }
}
#endif
#endif
