/*
 * CSVGImage.h
 *
 *      Author: M. Sc. - Ing. David Israel Gonzalez-Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#pragma once

#include "SVGPrimitive.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                class CSVGImage: public CSVGPrimitive
                {
                public:

                    CSVGImage(const_string pFilePathName, const real LeftUpperX, const real LeftUpperY, const real Width, const real Height);
                    CSVGImage(const_string pFilePathName, const Mathematics::_2D::CVector2D& LeftUpper, const real Width, const real Height);
                    CSVGImage(const_string pFilePathName, const CPixelLocation& LeftUpper, const real Width, const real Height);
                    CSVGImage(const std::string& FilePathName, const real LeftUpperX, const real LeftUpperY, const real Width, real Height);
                    CSVGImage(const std::string& FilePathName, const Mathematics::_2D::CVector2D& LeftUpper, const real Width, const real Height);
                    CSVGImage(const std::string& FilePathName, const CPixelLocation& LeftUpper, const real Width, const real Height);

                    void Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const override;

                protected:

                    std::string ToSVGString(const uint Precision) const override;

                    Mathematics::_2D::CVector2D m_LeftUpper;
                    std::string m_FilePathName;
                    real m_Width;
                    real m_Height;
                };
            }
        }
    }
}

