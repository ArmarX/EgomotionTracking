/*
 * CSVGPath.cpp
 *
 *      Author: M. Sc. David Israel Gonzalez Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#include "SVGPath.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                CSVGPath::CSVGPath(const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                CSVGPath::~CSVGPath()
                    = default;

                std::string CSVGPath::ToSVGString(const uint Precision) const
                {
                    std::ostringstream OutputString;
                    OutputString.precision(Precision);
                    OutputString << "<path d=\"";
                    byte Type[] = { 'M', 'm', 'L', 'l' };
                    list<CPathElement>::const_iterator EndPathElements = m_PathElements.end();
                    for (list<CPathElement>::const_iterator pElement = m_PathElements.begin(); pElement != EndPathElements; ++pElement, OutputString << " ")
                    {
                        OutputString << Type[uint(pElement->GetType())] << " " << pElement->GetPoint().GetX() << " " << pElement->GetPoint().GetY();
                    }
                    OutputString << "\" style=\"stroke-linecap:round;stroke:rgb(" << m_StrokeColor.GetRedChannel() << "," << m_StrokeColor.GetGreenChannel() << "," << m_StrokeColor.GetBlueChannel() << ");stroke-width:" << m_StrokeWidth << ";stroke-opacity:" << m_StrokeColor.GetNormalizedAlphaChannel() << ";\"/>";
                    return OutputString.str();
                }

                void CSVGPath::Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const
                {
                    if (pOutputStream && m_IsActive && (m_PathElements.size() > 1))
                        switch (Mode)
                        {
                            case eSVG:
                                *pOutputStream << ToSVGString(Precision);
                                break;
                        }
                }
            }
        }
    }
}
