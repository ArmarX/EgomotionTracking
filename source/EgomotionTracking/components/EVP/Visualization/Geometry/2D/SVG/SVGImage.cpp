/*
 * CSVGImage.cpp
 *
 *      Author: M. Sc. David Israel Gonzalez Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#include "SVGImage.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                CSVGImage::CSVGImage(const_string pFilePathName, const real LeftUpperX, const real LeftUpperY, const real Width, real const Height) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_LeftUpper(LeftUpperX, LeftUpperY), m_FilePathName(pFilePathName), m_Width(Width), m_Height(Height)
                {
                }

                CSVGImage::CSVGImage(const_string pFilePathName, const CVector2D& LeftUpper, const real Width, const real Height) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_LeftUpper(LeftUpper), m_FilePathName(pFilePathName), m_Width(Width), m_Height(Height)
                {
                }

                CSVGImage::CSVGImage(const_string pFilePathName, const CPixelLocation& LeftUpper, const real Width, const real Height) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_LeftUpper(LeftUpper), m_FilePathName(pFilePathName), m_Width(Width), m_Height(Height)
                {
                }

                CSVGImage::CSVGImage(const std::string& FilePathName, const real LeftUpperX, const real LeftUpperY, const real Width, const real Height) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_LeftUpper(LeftUpperX, LeftUpperY), m_FilePathName(FilePathName), m_Width(Width), m_Height(Height)
                {
                }

                CSVGImage::CSVGImage(const std::string& FilePathName, const CVector2D& LeftUpper, const real Width, const real Height) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_LeftUpper(LeftUpper), m_FilePathName(FilePathName), m_Width(Width), m_Height(Height)
                {
                }

                CSVGImage::CSVGImage(const std::string& FilePathName, const CPixelLocation& LeftUpper, const real Width, const real Height) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_LeftUpper(LeftUpper), m_FilePathName(FilePathName), m_Width(Width), m_Height(Height)
                {
                }

                std::string CSVGImage::ToSVGString(const uint Precision) const
                {
                    std::ostringstream OutputString;
                    OutputString.precision(Precision);
                    OutputString << "<image x=\"" << m_LeftUpper.GetX() << "\" y=\"" << m_LeftUpper.GetY() << "\" width=\"" << m_Width << "\" height=\"" << m_Height << "\" xlink:href=\"" << m_FilePathName.c_str() << "\"/>";
                    return OutputString.str();
                }

                void CSVGImage::Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const
                {
                    if (pOutputStream && m_IsActive && m_FilePathName.length() && (m_Width > _REAL_EPSILON_) && (m_Height > _REAL_EPSILON_))
                        switch (Mode)
                        {
                            case eSVG:
                                *pOutputStream << ToSVGString(Precision);
                                break;
                        }
                }
            }
        }
    }
}
