/*
 * CSVGPolygon.h
 *
 *      Author: M. Sc. - Ing. David Israel Gonzalez-Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#pragma once

#include "SVGPrimitive.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                class CSVGPolygon: public CSVGPrimitive
                {
                public:

                    CSVGPolygon(const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);
                    CSVGPolygon(const list<Mathematics::_2D::CVector2D>& Points, real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);

                    inline void ClearPoints()
                    {
                        m_Points.clear();
                    }

                    inline void AddPoint(const real PointX, const real PointY)
                    {
                        m_Points.push_back(Mathematics::_2D::CVector2D(PointX, PointY));
                    }

                    inline void AddPoint(const Mathematics::_2D::CVector2D& Point)
                    {
                        m_Points.push_back(Point);
                    }

                    inline void AddPoint(const CPixelLocation& Location)
                    {
                        m_Points.push_back(Mathematics::_2D::CVector2D(Location));
                    }

                    inline  uint GetTotalPoints() const
                    {
                        return m_Points.size();
                    }

                    void Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const override;

                protected:

                    std::string ToSVGString(const uint Precision) const override;

                    real m_StrokeWidth;
                    CRGBAColor m_StrokeColor;
                    CRGBAColor m_FillColor;
                    list<Mathematics::_2D::CVector2D> m_Points;
                };
            }
        }
    }
}

