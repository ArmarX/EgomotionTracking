/*
 * CSVGLine.cpp
 *
 *      Author: M. Sc. David Israel Gonzalez Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#include "SVGLine.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                CSVGLine::CSVGLine(const real X1, const real Y1, const real X2, const real Y2, const real StrokeWidth, const CRGBAColor& StrokeColor) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_A(X1, Y1), m_B(X2, Y2), m_StrokeColor(StrokeColor), m_StrokeWidth(StrokeWidth)
                {
                }

                CSVGLine::CSVGLine(const CVector2D& A, const CVector2D& B, const real StrokeWidth, const CRGBAColor& StrokeColor) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_A(A), m_B(B), m_StrokeColor(StrokeColor), m_StrokeWidth(StrokeWidth)
                {
                }

                CSVGLine::CSVGLine(const CPixelLocation& A, const CPixelLocation& B, const real StrokeWidth, const CRGBAColor& StrokeColor) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_A(A), m_B(B), m_StrokeColor(StrokeColor), m_StrokeWidth(StrokeWidth)
                {
                }

                CSVGLine::CSVGLine(const CPixelLocation& A, const CVector2D& B, const real StrokeWidth, const CRGBAColor& StrokeColor) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_A(A), m_B(B), m_StrokeColor(StrokeColor), m_StrokeWidth(StrokeWidth)
                {
                }

                CSVGLine::CSVGLine(const CVector2D& A, const CPixelLocation& B, const real StrokeWidth, const CRGBAColor& StrokeColor) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_A(A), m_B(B), m_StrokeColor(StrokeColor), m_StrokeWidth(StrokeWidth)
                {
                }

                std::string CSVGLine::ToSVGString(const uint Precision) const
                {
                    std::ostringstream OutputString;
                    OutputString.precision(Precision);
                    OutputString << "<line x1=\"" << m_A.GetX() << "\" y1=\"" << m_A.GetY() << "\" x2=\"" << m_B.GetX() << "\" y2=\"" << m_B.GetY() << "\" style=\"stroke:rgb(" << m_StrokeColor.GetRedChannel() << "," << m_StrokeColor.GetGreenChannel() << "," << m_StrokeColor.GetBlueChannel() << ");stroke-width:" << m_StrokeWidth << ";stroke-linecap:round;stroke-opacity:" << m_StrokeColor.GetNormalizedAlphaChannel() << "\"/>";
                    return OutputString.str();
                }

                void CSVGLine::Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const
                {
                    if (pOutputStream && m_IsActive && (m_A.GetDistance(m_B) > _REAL_EPSILON_))
                        switch (Mode)
                        {
                            case eSVG:
                                *pOutputStream << ToSVGString(Precision);
                                break;
                        }
                }
            }
        }
    }
}
