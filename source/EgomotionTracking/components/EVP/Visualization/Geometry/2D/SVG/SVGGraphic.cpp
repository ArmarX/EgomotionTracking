/*
 * CSVGGraphic.cpp
 *
 *      Author: M. Sc. - Ing. David Israel Gonzalez-Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#include "../../../../Foundation/Files/OutFile.h"
#include "SVGGraphic.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                CSVGGraphic::CSVGGraphic(const real Width, const real Height) :
                    CSVGGroup(), m_Width(Width), m_Height(Height)
                {
                }

                CSVGGraphic::~CSVGGraphic()
                    = default;

                std::string CSVGGraphic::ToSVGString(const uint Precision) const
                {
                    std::ostringstream OutputString;
                    OutputString.precision(Precision);
                    OutputString << "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"" << m_Width << "\" height=\"" << m_Height << "\">";
                    OutputString << "<!-- Generated with EVP by M. Sc. - Ing. David Israel Gonzalez-Aguirre, gonzalez@ira.uka.de -->";
                    list<const CSVGPrimitive*>::const_iterator EndElements = m_Elements.end();
                    for (list<const CSVGPrimitive*>::const_iterator ppPrimitive = m_Elements.begin(); ppPrimitive != EndElements; ++ppPrimitive)
                    {
                        OutputString << (*ppPrimitive)->ToSVGString(Precision);
                    }
                    OutputString << "</svg>";
                    return OutputString.str();
                }

                void CSVGGraphic::Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const
                {
                    if (pOutputStream && m_IsActive)
                        switch (Mode)
                        {
                            case eSVG:
                                *pOutputStream << ToSVGString(Precision);
                                break;
                        }
                }

#ifdef _USE_ZLIB_
                bool CSVGGraphic::SaveFile(const CSVGPrimitive::SerializationMode Mode, const_string pPathFileName, const uint Precision, const bool Compressed) const
#else
                bool CSVGGraphic::SaveFile(const CSVGPrimitive::SerializationMode Mode, const_string pPathFileName, const uint Precision) const
#endif
                {
                    std::ostringstream OutputString;
                    OutputString.precision(Precision);
                    Serialize(&OutputString, Mode, Precision);
                    std::string Content = OutputString.str();
#ifdef _USE_ZLIB_

                    if (Compressed)
                    {
                        unsigned long Lenght = 0;
                        byte* pCompressedBuffer = CSVGGraphic::Compress(Content, Lenght);
                        if (pCompressedBuffer)
                        {
                            bool Result = false;
                            Files::COutFile OutputBinaryFile(pPathFileName, Files::CFile::eBinary);
                            if (OutputBinaryFile.IsReady())
                            {
                                OutputBinaryFile.WriteBuffer(&Lenght, sizeof(unsigned long));
                                OutputBinaryFile.WriteBuffer(pCompressedBuffer, Lenght);
                                Result = OutputBinaryFile.Close();
                            }
                            RELEASE_ARRAY(pCompressedBuffer)
                            return Result;
                        }
                        return false;
                    }
#endif
                    return Files::COutFile::WriteStringToFile(pPathFileName, Content);

                }

#ifdef _USE_ZLIB_

                byte* CSVGGraphic::Compress(std::string& DataString, unsigned long& Lenght)
                {
                    unsigned long LengthString = DataString.length();
                    unsigned long LengthZBuffer = (unsigned long)(RealCeil(real(LengthString) * real(1.125) + 12));
                    byte* pZBuffer = new byte[LengthZBuffer];
                    if (compress(pZBuffer, &LengthZBuffer, (byte*) DataString.c_str(), LengthString) == Z_OK)
                    {
                        Lenght = LengthZBuffer;
                        return pZBuffer;
                    }
                    else
                    {
                        Lenght = -1;
                        RELEASE_ARRAY(pZBuffer)
                        return nullptr;
                    }
                }

                bool CSVGGraphic::Decompress(std::string& DataString, byte* pCompressedBuffer, unsigned long CompressedLenght, unsigned long SourceLenght)
                {
                    byte* pUncompressedBuffer = new byte[SourceLenght];
                    if (uncompress(pUncompressedBuffer, &SourceLenght, pCompressedBuffer, CompressedLenght) == Z_OK)
                    {
                        DataString = std::string((char*) pUncompressedBuffer, SourceLenght);
                        RELEASE_ARRAY(pUncompressedBuffer)
                        return true;
                    }
                    else
                    {
                        DataString.clear();
                        RELEASE_ARRAY(pUncompressedBuffer)
                        return false;
                    }
                }
#endif

            }
        }
    }
}
