/*
 * CSVGSubGraphic.cpp
 *
 *  Created on: 19.10.2010
 *      Author: gonzalez
 */

#include "SVGSubGraphic.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {

                CSVGSubGraphic::CSVGSubGraphic() :
                    CSVGPrimitive(CSVGPrimitive::eNone)
                {
                }

                CSVGSubGraphic::~CSVGSubGraphic()
                {
                    Clear();
                }

                void CSVGSubGraphic::Clear()
                {
                    if (m_SVGPrimitives.size())
                    {
                        list<const CSVGPrimitive*>::iterator EndElements = m_SVGPrimitives.end();
                        for (list<const CSVGPrimitive*>::iterator ppPrimitive = m_SVGPrimitives.begin(); ppPrimitive != EndElements; ++ppPrimitive)
                            RELEASE_OBJECT_DIRECT(*ppPrimitive)
                            m_SVGPrimitives.clear();
                    }
                }

                std::string CSVGSubGraphic::ToSVGString(const uint Precision) const
                {
                    std::ostringstream OutputString;
                    OutputString.precision(Precision);
                    OutputString << "<symbol id =\"S" << m_InstanceId << "\" >";
                    list<const CSVGPrimitive*>::const_iterator EndElements = m_SVGPrimitives.end();
                    for (list<const CSVGPrimitive*>::const_iterator ppPrimitive = m_SVGPrimitives.begin(); ppPrimitive != EndElements; ++ppPrimitive)
                    {
                        OutputString << (*ppPrimitive)->ToSVGString(Precision);
                    }
                    OutputString << "</symbol>";
                    return OutputString.str();
                }

                void CSVGSubGraphic::Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const
                {
                    if (pOutputStream && m_IsActive && m_SVGPrimitives.size())
                        switch (Mode)
                        {
                            case eSVG:
                                *pOutputStream << ToSVGString(Precision);
                                break;
                        }
                }
            }
        }
    }
}
