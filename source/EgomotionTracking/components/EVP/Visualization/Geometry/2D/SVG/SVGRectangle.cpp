/*
 * CSVGRectangle.cpp
 *
 *      Author: M. Sc. David Israel Gonzalez Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#include "SVGRectangle.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                CSVGRectangle::CSVGRectangle(const real LeftUpperX, const real LeftUpperY, const real Width, const real Height, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_LeftUpper(LeftUpperX, LeftUpperY), m_Width(Width), m_Height(Height), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                CSVGRectangle::CSVGRectangle(const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const real X0, const real Y0, const real X1, const real Y1, const real StrokeWidth, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_LeftUpper(TMin(X0, X1), TMin(Y0, Y1)), m_Width(RealAbs(X1 - X0)), m_Height(RealAbs(Y1 - Y0)), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                CSVGRectangle::CSVGRectangle(const CVector2D& LeftUpper, const real Width, const real Height, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_LeftUpper(LeftUpper), m_Width(Width), m_Height(Height), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                CSVGRectangle::CSVGRectangle(const CVector2D& LeftUpper, const CVector2D& RightLower, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const real StrokeWidth, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_LeftUpper(TMin(LeftUpper.GetX(), RightLower.GetX()), TMin(LeftUpper.GetY(), RightLower.GetY())), m_Width(RealAbs(RightLower.GetX() - LeftUpper.GetX())), m_Height(RealAbs(RightLower.GetY() - LeftUpper.GetY())), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                CSVGRectangle::CSVGRectangle(const CPixelLocation& LeftUpper, const CPixelLocation& RightLower, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const real StrokeWidth, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_LeftUpper(LeftUpper), m_Width(abs(RightLower.m_x - LeftUpper.m_x)), m_Height(abs(RightLower.m_y - LeftUpper.m_y)), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                CSVGRectangle::CSVGRectangle(const Mathematics::_2D::CVector2D& Center, const real SideLength, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const real StrokeWidth, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_LeftUpper(Mathematics::_2D::CVector2D(Center.GetX() - (SideLength * _REAL_HALF_), Center.GetY() - (SideLength * _REAL_HALF_))), m_Width(SideLength), m_Height(SideLength), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                CSVGRectangle::CSVGRectangle(const CPixelLocation& Center, const real SideLength, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const real StrokeWidth, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_LeftUpper(Mathematics::_2D::CVector2D(real(Center.m_x) - (SideLength * _REAL_HALF_), real(Center.m_y) - (SideLength * _REAL_HALF_))), m_Width(SideLength), m_Height(SideLength), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                std::string CSVGRectangle::ToSVGString(const uint Precision) const
                {
                    std::ostringstream OutputString;
                    OutputString.precision(Precision);
                    switch (m_Style)
                    {
                        case CSVGPrimitive::eOnlyStroke:
                            OutputString << "<rect x=\"" << m_LeftUpper.GetX() << "\" y=\"" << m_LeftUpper.GetY() << "\" width=\"" << m_Width << "\" height=\"" << m_Height << "\" style=\"stroke-linecap:round;stroke:rgb(" << m_StrokeColor.GetRedChannel() << "," << m_StrokeColor.GetGreenChannel() << "," << m_StrokeColor.GetBlueChannel() << ");stroke-width:" << m_StrokeWidth << ";stroke-opacity:" << m_StrokeColor.GetNormalizedAlphaChannel() << ";fill:none;\"/>";
                            break;
                        case CSVGPrimitive::eOnlyFill:
                            OutputString << "<rect x=\"" << m_LeftUpper.GetX() << "\" y=\"" << m_LeftUpper.GetY() << "\" width=\"" << m_Width << "\" height=\"" << m_Height << "\" style=\"stroke:none;fill:rgb(" << m_FillColor.GetRedChannel() << "," << m_FillColor.GetGreenChannel() << "," << m_FillColor.GetBlueChannel() << ");fill-opacity:" << m_FillColor.GetNormalizedAlphaChannel() << "\"/>";
                            break;
                        case CSVGPrimitive::eNone:
                        case CSVGPrimitive::eStrokeAndFill:
                            OutputString << "<rect x=\"" << m_LeftUpper.GetX() << "\" y=\"" << m_LeftUpper.GetY() << "\" width=\"" << m_Width << "\" height=\"" << m_Height << "\" style=\"stroke-linecap:round;stroke:rgb(" << m_StrokeColor.GetRedChannel() << "," << m_StrokeColor.GetGreenChannel() << "," << m_StrokeColor.GetBlueChannel() << ");stroke-width:" << m_StrokeWidth << ";stroke-opacity:" << m_StrokeColor.GetNormalizedAlphaChannel() << ";fill:rgb(" << m_FillColor.GetRedChannel() << "," << m_FillColor.GetGreenChannel() << "," << m_FillColor.GetBlueChannel() << ");fill-opacity:" << m_FillColor.GetNormalizedAlphaChannel() << "\"/>";
                            break;
                    }
                    return OutputString.str();
                }

                void CSVGRectangle::Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const
                {
                    if (pOutputStream && m_IsActive && (m_Width > _REAL_EPSILON_) && (m_Height > _REAL_EPSILON_))
                        switch (Mode)
                        {
                            case eSVG:
                                *pOutputStream << ToSVGString(Precision);
                                break;
                        }
                }
            }
        }
    }
}
