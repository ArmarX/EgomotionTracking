/*
 * CSVGRectangle.h
 *
 *      Author: M. Sc. David Israel Gonzalez Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#pragma once

#include "SVGPrimitive.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                class CSVGRectangle: public CSVGPrimitive
                {
                public:

                    CSVGRectangle(const real LeftUpperX, const real LeftUpperY, const real Width, const real Height, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);
                    CSVGRectangle(const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const real X0, const real Y0, const real X1, const real Y1, const real StrokeWidth, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);
                    CSVGRectangle(const Mathematics::_2D::CVector2D& LeftUpper, const real width, const real Height, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);
                    CSVGRectangle(const Mathematics::_2D::CVector2D& LeftUpper, const Mathematics::_2D::CVector2D& RightLower, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const real StrokeWidth, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);
                    CSVGRectangle(const CPixelLocation& LeftUpper, const CPixelLocation& RightLower, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const real StrokeWidth, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);
                    CSVGRectangle(const Mathematics::_2D::CVector2D& Center, const real SideLength, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const real StrokeWidth, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);
                    CSVGRectangle(const CPixelLocation& Center, const real SideLength, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const real StrokeWidth, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);

                    void Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const override;

                protected:

                    std::string ToSVGString(const uint Precision) const override;

                    Mathematics::_2D::CVector2D m_LeftUpper;
                    real m_Width;
                    real m_Height;
                    real m_StrokeWidth;
                    CRGBAColor m_StrokeColor;
                    CRGBAColor m_FillColor;
                };
            }
        }
    }
}

