/*
 * CSVGCircle.h
 *
 *      Author: M. Sc. David Israel Gonzalez Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#pragma once

#include "SVGPrimitive.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                class CSVGCircle: public CSVGPrimitive
                {
                public:

                    CSVGCircle(const real CenterX, const real CenterY, const real Radius, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);
                    CSVGCircle(const Mathematics::_2D::CVector2D& Center, const real Radius, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);
                    CSVGCircle(const CPixelLocation& Center, const real Radius, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);

                    void Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const override;

                protected:

                    std::string ToSVGString(const uint Precision) const override;

                    Mathematics::_2D::CVector2D m_Center;
                    real m_Radius;
                    real m_StrokeWidth;
                    CRGBAColor m_StrokeColor;
                    CRGBAColor m_FillColor;
                };
            }
        }
    }
}

