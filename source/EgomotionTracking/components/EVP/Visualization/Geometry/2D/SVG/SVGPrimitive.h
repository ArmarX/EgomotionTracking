/*
 * CSVGPrimitive.h
 *
 *      Author: M. Sc. - Ing. David Israel Gonzalez-Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/Miscellaneous/IdentifiableInstance.h"
#include "../../../../Foundation/DataTypes/RGBAColor.h"
#include "../../../../Foundation/Mathematics/2D/Vector2D.h"

#define _DISPLAY_DEFAULT_PRECISION_ 10

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                class CSVGPrimitive: public CIdentifiableInstance
                {
                    IDENTIFIABLE

                    INSTANCE_COUNTING

                public:

                    enum Style
                    {
                        eNone, eOnlyStroke, eOnlyFill, eStrokeAndFill
                    };

                    enum SerializationMode
                    {
                        eSVG
                    };

                    CSVGPrimitive(const CSVGPrimitive::Style CurrentStyle);
                    ~CSVGPrimitive() override;

                    bool IsActive() const;
                    void SetActive(const bool Active);

                    virtual  std::string ToSVGString(const uint Precision = _DISPLAY_DEFAULT_PRECISION_) const = 0;
                    virtual void Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision = _DISPLAY_DEFAULT_PRECISION_) const = 0;

                protected:

                    Style m_Style;
                    bool m_IsActive;
                };
            }
        }
    }
}

