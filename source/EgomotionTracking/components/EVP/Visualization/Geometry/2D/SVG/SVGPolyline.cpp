/*
 * CSVGPolyline.cpp
 *
 *  Created on: Sep 14, 2009
 *      Author: david
 */

#include "SVGPolyline.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                CSVGPolyline::CSVGPolyline(const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                CSVGPolyline::CSVGPolyline(const list<Mathematics::_2D::CVector2D>& Points, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor), m_Points(Points)
                {
                }

                string CSVGPolyline::ToSVGString(const uint Precision) const
                {
                    ostringstream OutputString;
                    OutputString.precision(Precision);
                    OutputString << "<polyline points=\"";
                    list<CVector2D>::const_iterator EndPoints = m_Points.end();
                    for (list<CVector2D>::const_iterator pPoint = m_Points.begin(); pPoint != EndPoints; ++pPoint, OutputString << " ")
                    {
                        OutputString << pPoint->GetX() << "," << pPoint->GetY();
                    }
                    OutputString << "\" style=\"stroke-linecap:round;stroke:rgb(" << m_StrokeColor.GetRedChannel() << "," << m_StrokeColor.GetGreenChannel() << "," << m_StrokeColor.GetBlueChannel() << ");stroke-width:" << m_StrokeWidth << ";stroke-opacity:" << m_StrokeColor.GetNormalizedAlphaChannel() << ";\"/>";
                    return OutputString.str();
                }

                void CSVGPolyline::Serialize(ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const
                {
                    if (pOutputStream && m_IsActive && (m_Points.size() > 1))
                        switch (Mode)
                        {
                            case eSVG:
                                *pOutputStream << ToSVGString(Precision);
                                break;
                        }
                }
            }
        }
    }
}
