/*
 * SVGEllipticalArc.cpp
 *
 *  Created on: 04.01.2012
 *      Author: gonzalez
 */

#include "SVGEllipticalArc.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                CSVGEllipticalArc::CSVGEllipticalArc(const Mathematics::_2D::CVector2D& C, const real Rx, const real Ry, const real Alpha, const real Beta0, const real Beta1, const real StrokeWidth, const CRGBAColor& StrokeColor) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_A(C), m_B(C), m_Rx(Rx), m_Ry(Ry), m_Alpha(Alpha), m_LargeArc(RealAbs(Beta1 - Beta0) > _REAL_HALF_PI_), m_PositiveAngleSide((Beta1 - Beta0) > _REAL_ZERO_)
                {
                    const real CosinusAlpha = RealCosinus(Alpha);
                    const real SinusAlpha = RealSinus(Alpha);
                    const real Rotation[2][2] = { { CosinusAlpha, -SinusAlpha }, { SinusAlpha, CosinusAlpha } };
                    const real A[2] = { Rx * RealCosinus(Beta0), Ry * RealSinus(Beta0) };
                    const real B[2] = { Rx * RealCosinus(Beta1), Ry * RealSinus(Beta1) };
                    real Ar[2] = { _REAL_ZERO_, _REAL_ZERO_ };
                    real Br[2] = { _REAL_ZERO_, _REAL_ZERO_ };
                    for (uint r = 0; r < 2; ++r)
                        for (uint c = 0; c < 2; ++c)
                        {
                            Ar[r] += Rotation[r][c] * A[c];
                            Br[r] += Rotation[r][c] * B[c];
                        }
                    m_A.AddOffset(Ar[0], Ar[1]);
                    m_B.AddOffset(Br[0], Br[1]);
                }

                CSVGEllipticalArc::CSVGEllipticalArc(const Mathematics::_2D::CVector2D& A, const CPixelLocation& B, const real Rx, const real Ry, const real Alpha, const bool LargeArc, const bool PositiveAngleSide, const real StrokeWidth, const CRGBAColor& StrokeColor) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_A(A), m_B(B), m_Rx(Rx), m_Ry(Ry), m_Alpha(Alpha), m_LargeArc(LargeArc), m_PositiveAngleSide(PositiveAngleSide), m_StrokeColor(StrokeColor), m_StrokeWidth(StrokeWidth)
                {
                }

                CSVGEllipticalArc::~CSVGEllipticalArc()
                    = default;

                void CSVGEllipticalArc::Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const
                {
                    if (pOutputStream && m_IsActive && (m_A != m_B) && (m_Rx > _REAL_EPSILON_) && (m_Ry > _REAL_EPSILON_))
                        switch (Mode)
                        {
                            case eSVG:
                                *pOutputStream << ToSVGString(Precision);
                                break;
                        }
                }

                std::string CSVGEllipticalArc::ToSVGString(const uint Precision) const
                {
                    std::ostringstream OutputString;
                    OutputString.precision(Precision);
                    OutputString << "<path d=\"M " << m_A.GetX() << "," << m_A.GetY() << "a" << m_Rx << "," << m_Ry << " " << (_REAL_RADIANS_TO_DEGREES_ * m_Alpha) << " " << (m_LargeArc ? 1 : 0) << "," << (m_PositiveAngleSide ? 1 : 0) << " " << m_B.GetX() << "," << m_B.GetY() << "\" style=\"stroke:rgb(" << m_StrokeColor.GetRedChannel() << "," << m_StrokeColor.GetGreenChannel() << "," << m_StrokeColor.GetBlueChannel() << ");stroke-width:" << m_StrokeWidth << ";stroke-linecap:round;stroke-opacity:" << m_StrokeColor.GetNormalizedAlphaChannel() << ";fill:none;\"/>";
                    return OutputString.str();
                }
            }
        }
    }
}
