/*
 * CSVGPath.cpp
 *
 *      Author: M. Sc. - Ing. David Israel Gonzalez-Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#pragma once

#include "SVGPrimitive.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                class CSVGPath: public CSVGPrimitive
                {
                public:

                    CSVGPath(const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style);

                    ~CSVGPath() override;

                    inline void ClearElements()
                    {
                        m_PathElements.clear();
                    }

                    inline void MoveAbsolute(const real Ax, const real Ay)
                    {
                        m_PathElements.push_back(CPathElement(CPathElement::eMoveAbsolute, Ax, Ay));
                    }

                    inline void MoveAbsolute(const Mathematics::_2D::CVector2D& Point)
                    {
                        m_PathElements.push_back(CPathElement(CPathElement::eMoveAbsolute, Point));
                    }

                    inline void MoveAbsolute(const CPixelLocation& Location)
                    {
                        m_PathElements.push_back(CPathElement(CPathElement::eMoveAbsolute, Location));
                    }

                    inline void MoveRelative(const real Rx, const real Ry)
                    {
                        m_PathElements.push_back(CPathElement(CPathElement::eMoveRelative, Rx, Ry));
                    }

                    inline void MoveRelative(const Mathematics::_2D::CVector2D& Point)
                    {
                        m_PathElements.push_back(CPathElement(CPathElement::eMoveRelative, Point));
                    }

                    inline void MoveRelative(const CPixelLocation& Location)
                    {
                        m_PathElements.push_back(CPathElement(CPathElement::eMoveRelative, Location));
                    }

                    inline void LineToAbsolute(const real Ax, const real Ay)
                    {
                        m_PathElements.push_back(CPathElement(CPathElement::eLineToAbsolute, Ax, Ay));
                    }

                    inline void LineToAbsolute(const Mathematics::_2D::CVector2D& Point)
                    {
                        m_PathElements.push_back(CPathElement(CPathElement::eLineToAbsolute, Point));
                    }

                    inline void LineToAbsolute(const CPixelLocation& Location)
                    {
                        m_PathElements.push_back(CPathElement(CPathElement::eLineToAbsolute, Location));
                    }

                    inline void LineToRelative(const real Rx, const real Ry)
                    {
                        m_PathElements.push_back(CPathElement(CPathElement::eLineToRelative, Rx, Ry));
                    }

                    inline void LineToRelative(const Mathematics::_2D::CVector2D& Point)
                    {
                        m_PathElements.push_back(CPathElement(CPathElement::eLineToRelative, Point));
                    }

                    inline void LineToRelative(const CPixelLocation& Location)
                    {
                        m_PathElements.push_back(CPathElement(CPathElement::eLineToRelative, Location));
                    }

                    inline  uint GetTotalElements() const
                    {
                        return m_PathElements.size();
                    }

                    void Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const override;

                protected:

                    std::string ToSVGString(const uint Precision) const override;

                    class CPathElement
                    {
                    public:

                        enum OperationType
                        {
                            eMoveAbsolute = 0, eMoveRelative, eLineToAbsolute, eLineToRelative
                        };

                        CPathElement(const OperationType Operation, const Mathematics::_2D::CVector2D& Point) :
                            m_Point(Point), m_Type(Operation)
                        {
                        }

                        CPathElement(const OperationType Operation, const real PointX, const real PointY) :
                            m_Point(PointX, PointY), m_Type(Operation)
                        {
                        }

                        CPathElement(const OperationType Operation, const CPixelLocation& Location) :
                            m_Point(Location), m_Type(Operation)
                        {
                        }

                        inline  OperationType GetType() const
                        {
                            return m_Type;
                        }

                        inline const Mathematics::_2D::CVector2D& GetPoint() const
                        {
                            return m_Point;
                        }

                    protected:

                        Mathematics::_2D::CVector2D m_Point;
                        OperationType m_Type;
                    };

                    real m_StrokeWidth;
                    CRGBAColor m_StrokeColor;
                    CRGBAColor m_FillColor;
                    list<CPathElement> m_PathElements;
                };
            }
        }
    }
}

