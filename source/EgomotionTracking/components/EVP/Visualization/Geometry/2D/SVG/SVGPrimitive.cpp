/*
 * CSVGPrimitive.cpp
 *
 *      Author: M. Sc. David Israel Gonzalez Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#include "SVGPrimitive.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                IDENTIFIABLE_INITIALIZATION(CSVGPrimitive)
                INSTANCE_COUNTING_INITIALIZATION(CSVGPrimitive)

                CSVGPrimitive::CSVGPrimitive(const CSVGPrimitive::Style CurrentStyle) :
                    IDENTIFIABLE_CONTRUCTOR(CSVGPrimitive), m_Style(CurrentStyle), m_IsActive(true)
                {
                    INSTANCE_COUNTING_CONTRUCTOR(CSVGPrimitive);
                }

                CSVGPrimitive::~CSVGPrimitive()
                {
                    INSTANCE_COUNTING_DESTRUCTOR(CSVGPrimitive)
                }

                bool CSVGPrimitive::IsActive() const
                {
                    return m_IsActive;
                }

                void CSVGPrimitive::SetActive(const bool Active)
                {
                    m_IsActive = Active;
                }
            }
        }
    }
}
