/*
 * CSVGGroup.h
 *
 *      Author: M. Sc. - Ing. David Israel Gonzalez-Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#pragma once

#include "SVGPrimitive.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                class CSVGGroup: public CSVGPrimitive
                {
                public:

                    CSVGGroup();
                    ~CSVGGroup() override;

                    void Clear();
                    void Add(const CSVGPrimitive* pPrimitive);

                    CSVGGroup* CreateGroup();

                    void Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const override;

                    void SetTranslation(const real Tx, const real Ty);
                    void SetTranslation(const Mathematics::_2D::CVector2D& T);
                    void SetTranslation(const CPixelLocation& T);

                    void SetRotation(const real Alpha);
                    uint GetTotalElements() const;

                protected:

                    std::string ToSVGString(const uint Precision) const override;

                    real m_Alpha;
                    Mathematics::_2D::CVector2D m_Translation;
                    list<const CSVGPrimitive*> m_Elements;
                };
            }
        }
    }
}

