/*
 * CSVGGraphic.h
 *
 *      Author: M. Sc. - Ing. David Israel Gonzalez-Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#pragma once

#include "SVGCircle.h"
#include "SVGEllipse.h"
#include "SVGGroup.h"
#include "SVGImage.h"
#include "SVGLine.h"
#include "SVGPolygon.h"
#include "SVGRectangle.h"
#include "SVGText.h"
#include "SVGPath.h"
#include "SVGEllipticalArc.h"

#ifdef _USE_ZLIB_
#include <zlib.h>
#endif

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                class CSVGGraphic: public CSVGGroup
                {
                public:

                    CSVGGraphic(const real Width, const real Height);
                    ~CSVGGraphic() override;

                    void Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const override;

#ifdef _USE_ZLIB_
                    bool SaveFile(const CSVGPrimitive::SerializationMode Mode, const_string pPathFileName, const uint Precision = _DISPLAY_DEFAULT_PRECISION_, const bool Compressed = false) const ;
#else
                    bool SaveFile(const CSVGPrimitive::SerializationMode Mode, const_string pPathFileName, const uint Precision = _DISPLAY_DEFAULT_PRECISION_) const;
#endif
                protected:

                    std::string ToSVGString(const uint Precision) const override ;

#ifdef _USE_ZLIB_
                    static byte* Compress(std::string& DataString, unsigned long& Lenght);
                    static bool Decompress(std::string& DataString, byte* pCompressedBuffer, unsigned long CompressedLenght, unsigned long SourceLenght);
#endif
                    real m_Width;
                    real m_Height;
                };
            }
        }
    }
}

