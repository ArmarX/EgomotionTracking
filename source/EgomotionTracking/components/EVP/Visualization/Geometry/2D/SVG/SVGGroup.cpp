/*
 * CSVGGroup.cpp
 *
 *      Author: M. Sc. David Israel Gonzalez Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#include "SVGGroup.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                CSVGGroup::CSVGGroup() :
                    CSVGPrimitive(CSVGPrimitive::eNone), m_Alpha(_REAL_ZERO_)
                {

                }

                CSVGGroup::~CSVGGroup()
                {
                    Clear();
                }

                void CSVGGroup::SetTranslation(const real Tx, const real Ty)
                {
                    m_Translation.SetValue(Tx, Ty);
                }

                void CSVGGroup::SetTranslation(const CVector2D& T)
                {
                    m_Translation = T;
                }

                void CSVGGroup::SetTranslation(const CPixelLocation& T)
                {
                    m_Translation = T;
                }

                void CSVGGroup::SetRotation(const real Alpha)
                {
                    m_Alpha = Alpha;
                }

                void CSVGGroup::Clear()
                {
                    if (m_Elements.size())
                    {
                        list<const CSVGPrimitive*>::iterator EndElements = m_Elements.end();
                        for (list<const CSVGPrimitive*>::iterator ppPrimitive = m_Elements.begin(); ppPrimitive != EndElements; ++ppPrimitive)
                            RELEASE_OBJECT_DIRECT(*ppPrimitive)
                            m_Elements.clear();
                    }
                }

                void CSVGGroup::Add(const CSVGPrimitive* pPrimitive)
                {
                    if (pPrimitive)
                    {
                        m_Elements.push_back(pPrimitive);
                    }
                }

                CSVGGroup* CSVGGroup::CreateGroup()
                {
                    CSVGGroup* pGroup = new CSVGGroup();
                    Add(pGroup);
                    return pGroup;
                }

                std::string CSVGGroup::ToSVGString(const uint Precision) const
                {
                    std::ostringstream OutputString;
                    OutputString.precision(Precision);
                    bool TranslationFlag = m_Translation.IsNonNull();
                    bool RotationFlag = (RealAbs(m_Alpha) > _REAL_EPSILON_);
                    if (TranslationFlag || RotationFlag)
                    {
                        if (TranslationFlag && RotationFlag)
                        {
                            OutputString << "<g transform=\"translate(" << m_Translation.GetX() << "," << m_Translation.GetY() << "):rotate(" << (m_Alpha * _REAL_RADIANS_TO_DEGREES_) << ")\">";
                        }
                        else if (TranslationFlag)
                        {
                            OutputString << "<g transform=\"translate(" << m_Translation.GetX() << "," << m_Translation.GetY() << ")\">";
                        }
                        else
                        {
                            OutputString << "<g transform=\"rotate(" << (m_Alpha * _REAL_RADIANS_TO_DEGREES_) << ")\">";
                        }
                    }
                    else
                    {
                        OutputString << "<g>";
                    }
                    list<const CSVGPrimitive*>::const_iterator EndElements = m_Elements.end();
                    for (list<const CSVGPrimitive*>::const_iterator ppPrimitive = m_Elements.begin(); ppPrimitive != EndElements; ++ppPrimitive)
                    {
                        OutputString << (*ppPrimitive)->ToSVGString(Precision);
                    }
                    OutputString << "</g>";
                    return OutputString.str();
                }

                void CSVGGroup::Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const
                {
                    if (pOutputStream && m_IsActive && m_Elements.size())
                        switch (Mode)
                        {
                            case eSVG:
                                *pOutputStream << ToSVGString(Precision);
                                break;
                        }
                }

                uint CSVGGroup::GetTotalElements() const
                {
                    return m_Elements.size();
                }
            }
        }
    }
}
