/*
 * CSVGPolygon.cpp
 *
 *      Author: M. Sc. David Israel Gonzalez Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#include "SVGPolygon.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {

                CSVGPolygon::CSVGPolygon(const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                CSVGPolygon::CSVGPolygon(const list<CVector2D>& Points, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor), m_Points(Points)
                {
                }

                std::string CSVGPolygon::ToSVGString(const uint Precision) const
                {
                    std::ostringstream OutputString;
                    OutputString.precision(Precision);
                    OutputString << "<polygon points=\"";
                    uint TotalPoints = m_Points.size(), Index = 1;
                    list<CVector2D>::const_iterator EndPoints = m_Points.end();
                    for (list<CVector2D>::const_iterator pPoint = m_Points.begin(); pPoint != EndPoints; ++pPoint, ++Index)
                    {
                        OutputString << pPoint->GetX() << "," << pPoint->GetY();
                        if (Index < TotalPoints)
                        {
                            OutputString << " ";
                        }
                    }
                    switch (m_Style)
                    {
                        case CSVGPrimitive::eOnlyStroke:
                            OutputString << "\" style=\"stroke-linecap:round;stroke:rgb(" << m_StrokeColor.GetRedChannel() << "," << m_StrokeColor.GetGreenChannel() << "," << m_StrokeColor.GetBlueChannel() << ");stroke-width:" << m_StrokeWidth << ";stroke-opacity:" << m_StrokeColor.GetNormalizedAlphaChannel() << ";\"/>";
                            break;
                        case CSVGPrimitive::eOnlyFill:
                            OutputString << "\" style=\"stroke-linecap:round;stroke:none;fill:rgb(" << m_FillColor.GetRedChannel() << "," << m_FillColor.GetGreenChannel() << "," << m_FillColor.GetBlueChannel() << ");fill-opacity:" << m_FillColor.GetNormalizedAlphaChannel() << "\"/>";
                            break;
                        case CSVGPrimitive::eNone:
                        case CSVGPrimitive::eStrokeAndFill:
                            OutputString << "\" style=\"stroke-linecap:round;stroke:rgb(" << m_StrokeColor.GetRedChannel() << "," << m_StrokeColor.GetGreenChannel() << "," << m_StrokeColor.GetBlueChannel() << ");stroke-width:" << m_StrokeWidth << ";stroke-opacity:" << m_StrokeColor.GetNormalizedAlphaChannel() << ";fill:rgb(" << m_FillColor.GetRedChannel() << "," << m_FillColor.GetGreenChannel() << "," << m_FillColor.GetBlueChannel() << ");fill-opacity:" << m_FillColor.GetNormalizedAlphaChannel() << "\"/>";
                            break;
                    }
                    return OutputString.str();
                }

                void CSVGPolygon::Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const
                {
                    if (pOutputStream && m_IsActive && (m_Points.size() > 1))
                        switch (Mode)
                        {
                            case eSVG:
                                *pOutputStream << ToSVGString(Precision);
                                break;
                        }
                }
            }
        }
    }
}
