/*
 * CSVGSubGraphic.h
 *
 *      Author: M. Sc. - Ing. David Israel Gonzalez-Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#pragma once

#include "SVGPrimitive.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                class CSVGSubGraphic: public CSVGPrimitive
                {
                public:

                    CSVGSubGraphic();
                    ~CSVGSubGraphic() override;

                    void Clear();

                    inline void Add(const CSVGPrimitive* pPrimitive)
                    {
                        if (pPrimitive)
                        {
                            m_SVGPrimitives.push_back(pPrimitive);
                        }
                    }

                    inline  uint GetTotalElements() const
                    {
                        return m_SVGPrimitives.size();
                    }

                    inline const CSVGPrimitive* CreateInstance(const real PointX, const real PointY)
                    {
                        return new CSVGSubGraphicInstance(Mathematics::_2D::CVector2D(PointX, PointY), this);
                    }

                    inline const CSVGPrimitive* CreateInstance(const Mathematics::_2D::CVector2D& Point)
                    {
                        return new CSVGSubGraphicInstance(Point, this);
                    }

                    inline const CSVGPrimitive* CreateInstance(const CPixelLocation& Location)
                    {
                        return new CSVGSubGraphicInstance(Mathematics::_2D::CVector2D(Location), this);
                    }

                    void Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const override;

                protected:

                    std::string ToSVGString(const uint Precision) const override;

                    class CSVGSubGraphicInstance: public CSVGPrimitive
                    {
                    public:

                        CSVGSubGraphicInstance(const Mathematics::_2D::CVector2D& Point, const CSVGSubGraphic* pSubGraphic) :
                            CSVGPrimitive(CSVGPrimitive::eNone), m_pSubGraphic(pSubGraphic), m_Point(Point)
                        {
                        }

                        void Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const override
                        {
                            if (m_IsActive && pOutputStream && m_pSubGraphic && m_pSubGraphic->m_IsActive)
                                switch (Mode)
                                {
                                    case eSVG:
                                        *pOutputStream << ToSVGString(Precision);
                                        break;
                                }
                        }

                    protected:

                        std::string ToSVGString(const uint Precision) const override
                        {
                            if (m_IsActive && m_pSubGraphic && m_pSubGraphic->m_IsActive)
                            {
                                std::ostringstream OutputString;
                                OutputString.precision(Precision);
                                OutputString << "<use x=\"" << m_Point.GetX() << "\" y=\"" << m_Point.GetY() << "\" xlink:href=\"#S" << m_pSubGraphic->m_InstanceId << "\"/>";
                                return OutputString.str();
                            }
                            return g_EmptyString;
                        }

                        const CSVGSubGraphic* m_pSubGraphic;
                        Mathematics::_2D::CVector2D m_Point;
                    };

                    list<const CSVGPrimitive*> m_SVGPrimitives;
                };
            }
        }
    }
}

