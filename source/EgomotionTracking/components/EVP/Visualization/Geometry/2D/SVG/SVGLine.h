/*
 * CSVGLine.h
 *
 *      Author: M. Sc. - Ing. David Israel Gonzalez-Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#pragma once

#include "SVGPrimitive.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                class CSVGLine: public CSVGPrimitive
                {
                public:

                    CSVGLine(const real X1, const real Y1, const real X2, const real Y2, const real StrokeWidth, const CRGBAColor& StrokeColor);
                    CSVGLine(const Mathematics::_2D::CVector2D& A, const Mathematics::_2D::CVector2D& B, const real StrokeWidth, const CRGBAColor& StrokeColor);
                    CSVGLine(const CPixelLocation& A, const CPixelLocation& B, const real StrokeWidth, const CRGBAColor& StrokeColor);
                    CSVGLine(const CPixelLocation& A, const Mathematics::_2D::CVector2D& B, const real StrokeWidth, const CRGBAColor& StrokeColor);
                    CSVGLine(const Mathematics::_2D::CVector2D& A, const CPixelLocation& B, const real StrokeWidth, const CRGBAColor& StrokeColor);

                    void Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const override;

                protected:

                    std::string ToSVGString(const uint Precision) const override;

                    Mathematics::_2D::CVector2D m_A;
                    Mathematics::_2D::CVector2D m_B;
                    CRGBAColor m_StrokeColor;
                    real m_StrokeWidth;
                };
            }
        }
    }
}

