/*
 * CSVGEllipse.cpp
 *
 *      Author: M. Sc. David Israel Gonzalez Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#include "SVGEllipse.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                CSVGEllipse::CSVGEllipse(const real CenterX, const real CenterY, const real RadiusX, const real RadiusY, const real Alpha, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_Center(CenterX, CenterY), m_RadiusX(RadiusX), m_RadiusY(RadiusY), m_Alpha(Alpha), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                CSVGEllipse::CSVGEllipse(const CVector2D& Center, const real RadiusX, const real RadiusY, const real Alpha, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_Center(Center), m_RadiusX(RadiusX), m_RadiusY(RadiusY), m_Alpha(Alpha), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                CSVGEllipse::CSVGEllipse(const CPixelLocation& Center, const real RadiusX, const real RadiusY, const real Alpha, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_Center(Center), m_RadiusX(RadiusX), m_RadiusY(RadiusY), m_Alpha(Alpha), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                std::string CSVGEllipse::ToSVGString(const uint Precision) const
                {
                    std::ostringstream OutputString;
                    OutputString.precision(Precision);
                    switch (m_Style)
                    {
                        case CSVGPrimitive::eOnlyStroke:
                            OutputString << "<ellipse cx=\"" << m_Center.GetX() << "\" cy=\"" << m_Center.GetY() << "\" rx=\"" << m_RadiusX << "\" ry=\"" << m_RadiusY << "\" transform=\"rotate(" << _REAL_RADIANS_TO_DEGREES_ * m_Alpha << " " << m_Center.GetX() << " " << m_Center.GetY() << ")\" style=\"stroke:rgb(" << m_StrokeColor.GetRedChannel() << "," << m_StrokeColor.GetGreenChannel() << "," << m_StrokeColor.GetBlueChannel() << ");stroke-width:" << m_StrokeWidth << ";stroke-opacity:" << m_StrokeColor.GetNormalizedAlphaChannel() << ";fill:none;\"/>";
                            break;
                        case CSVGPrimitive::eOnlyFill:
                            OutputString << "<ellipse cx=\"" << m_Center.GetX() << "\" cy=\"" << m_Center.GetY() << "\" rx=\"" << m_RadiusX << "\" ry=\"" << m_RadiusY << "\" transform=\"rotate(" << _REAL_RADIANS_TO_DEGREES_ * m_Alpha << " " << m_Center.GetX() << " " << m_Center.GetY() << ")\" style=\"stroke:none;fill:rgb(" << m_FillColor.GetRedChannel() << "," << m_FillColor.GetGreenChannel() << "," << m_FillColor.GetBlueChannel() << ");fill-opacity:" << m_FillColor.GetNormalizedAlphaChannel() << "\"/>";
                            break;
                        case CSVGPrimitive::eNone:
                        case CSVGPrimitive::eStrokeAndFill:
                            OutputString << "<ellipse cx=\"" << m_Center.GetX() << "\" cy=\"" << m_Center.GetY() << "\" rx=\"" << m_RadiusX << "\" ry=\"" << m_RadiusY << "\" transform=\"rotate(" << _REAL_RADIANS_TO_DEGREES_ * m_Alpha << " " << m_Center.GetX() << " " << m_Center.GetY() << ")\" style=\"stroke:rgb(" << m_StrokeColor.GetRedChannel() << "," << m_StrokeColor.GetGreenChannel() << "," << m_StrokeColor.GetBlueChannel() << ");stroke-width:" << m_StrokeWidth << ";stroke-opacity:" << m_StrokeColor.GetNormalizedAlphaChannel() << ";fill:rgb(" << m_FillColor.GetRedChannel() << "," << m_FillColor.GetGreenChannel() << "," << m_FillColor.GetBlueChannel() << ");fill-opacity:" << m_FillColor.GetNormalizedAlphaChannel() << "\"/>";
                            break;
                    }
                    return OutputString.str();
                }

                void CSVGEllipse::Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const
                {
                    if (pOutputStream && m_IsActive && (m_RadiusX > _REAL_EPSILON_) && (m_RadiusY > _REAL_EPSILON_))
                        switch (Mode)
                        {
                            case eSVG:
                                *pOutputStream << ToSVGString(Precision);
                                break;
                        }
                }
            }
        }
    }
}
