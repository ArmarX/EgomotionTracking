/*
 * CSVGText.cpp
 *
 *      Author: M. Sc. David Israel Gonzalez Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#include "SVGText.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                CSVGText::CSVGText(const_string pText, const real LeftUpperX, const real LeftUpperY, const real FontSize, const CRGBAColor& StrokeColor) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_LeftUpper(LeftUpperX, LeftUpperY), m_Text(pText), m_FontSize(FontSize), m_StrokeColor(StrokeColor)
                {
                }

                CSVGText::CSVGText(const_string pText, const CVector2D& LeftUpper, const real FontSize, const CRGBAColor& StrokeColor) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_LeftUpper(LeftUpper), m_Text(pText), m_FontSize(FontSize), m_StrokeColor(StrokeColor)
                {
                }

                CSVGText::CSVGText(const_string pText, const CPixelLocation& LeftUpper, const real FontSize, const CRGBAColor& StrokeColor) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_LeftUpper(LeftUpper), m_Text(pText), m_FontSize(FontSize), m_StrokeColor(StrokeColor)
                {
                }

                CSVGText::CSVGText(const string& Text, const real LeftUpperX, const real LeftUpperY, const real FontSize, const CRGBAColor& StrokeColor) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_LeftUpper(LeftUpperX, LeftUpperY), m_Text(Text), m_FontSize(FontSize), m_StrokeColor(StrokeColor)
                {
                }

                CSVGText::CSVGText(const string& Text, const CVector2D& LeftUpper, const real FontSize, const CRGBAColor& StrokeColor) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_LeftUpper(LeftUpper), m_Text(Text), m_FontSize(FontSize), m_StrokeColor(StrokeColor)
                {
                }

                CSVGText::CSVGText(const string& Text, const CPixelLocation& LeftUpper, const real FontSize, const CRGBAColor& StrokeColor) :
                    CSVGPrimitive(CSVGPrimitive::eOnlyStroke), m_LeftUpper(LeftUpper), m_Text(Text), m_FontSize(FontSize), m_StrokeColor(StrokeColor)
                {
                }

                std::string CSVGText::ToSVGString(const uint Precision) const
                {
                    std::ostringstream OutputString;
                    OutputString.precision(Precision);
                    OutputString << "<text x=\"" << m_LeftUpper.GetX() << "\" y=\"" << m_LeftUpper.GetY() << "\" style=\"font-family:Arial;font-size:" << m_FontSize << ";fill:rgb(" << m_StrokeColor.GetRedChannel() << "," << m_StrokeColor.GetGreenChannel() << "," << m_StrokeColor.GetBlueChannel() << ")\">" << m_Text << "</text>";
                    return OutputString.str();
                }

                void CSVGText::Serialize(ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const
                {
                    if (pOutputStream && m_IsActive && m_Text.length())
                        switch (Mode)
                        {
                            case eSVG:
                                *pOutputStream << ToSVGString(Precision);
                                break;
                        }
                }
            }
        }
    }
}
