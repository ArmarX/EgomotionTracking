/*
 * CSVGEllipse.h
 *
 *      Author: M. Sc. - Ing. David Israel Gonzalez-Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#pragma once

#include "SVGPrimitive.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                class CSVGEllipse: public CSVGPrimitive
                {
                public:

                    CSVGEllipse(const real CenterX, const real CenterY, const real RadiusX, const real RadiusY, const real Alpha, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);
                    CSVGEllipse(const Mathematics::_2D::CVector2D& Center, const real RadiusX, const real RadiusY, const real Alpha, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);
                    CSVGEllipse(const CPixelLocation& Center, const real RadiusX, const real RadiusY, const real Alpha, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style = CSVGPrimitive::eStrokeAndFill);

                    void Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const override;

                protected:

                    std::string ToSVGString(const uint Precision) const override;

                    Mathematics::_2D::CVector2D m_Center;
                    real m_RadiusX;
                    real m_RadiusY;
                    real m_Alpha;
                    real m_StrokeWidth;
                    CRGBAColor m_StrokeColor;
                    CRGBAColor m_FillColor;
                };
            }
        }
    }
}

