/*
 * CSVGCircle.cpp
 *
 *      Author: M. Sc. David Israel Gonzalez Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#include "SVGCircle.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                CSVGCircle::CSVGCircle(const real CenterX, const real CenterY, const real Radius, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_Center(CenterX, CenterY), m_Radius(Radius), m_StrokeWidth(StrokeWidth), m_FillColor(FillColor)
                {
                }

                CSVGCircle::CSVGCircle(const CVector2D& Center, const real Radius, const real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_Center(Center), m_Radius(Radius), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                CSVGCircle::CSVGCircle(const CPixelLocation& Center, const real Radius, real StrokeWidth, const CRGBAColor& StrokeColor, const CRGBAColor& FillColor, const CSVGPrimitive::Style Style) :
                    CSVGPrimitive(Style), m_Center(Center), m_Radius(Radius), m_StrokeWidth(StrokeWidth), m_StrokeColor(StrokeColor), m_FillColor(FillColor)
                {
                }

                std::string CSVGCircle::ToSVGString(const uint Precision) const
                {
                    std::ostringstream OutputString;
                    OutputString.precision(Precision);
                    switch (m_Style)
                    {
                        case CSVGPrimitive::eOnlyStroke:
                            OutputString << "<circle cx=\"" << m_Center.GetX() << "\" cy=\"" << m_Center.GetY() << "\" r=\"" << m_Radius << "\" style=\"stroke:rgb(" << m_StrokeColor.GetRedChannel() << "," << m_StrokeColor.GetGreenChannel() << "," << m_StrokeColor.GetBlueChannel() << ");stroke-width:" << m_StrokeWidth << ";stroke-opacity:" << m_StrokeColor.GetNormalizedAlphaChannel() << ";fill:none;\"/>";
                            break;
                        case CSVGPrimitive::eOnlyFill:
                            OutputString << "<circle cx=\"" << m_Center.GetX() << "\" cy=\"" << m_Center.GetY() << "\" r=\"" << m_Radius << "\" style=\"stroke:none;fill:rgb(" << m_StrokeColor.GetRedChannel() << "," << m_StrokeColor.GetGreenChannel() << "," << m_StrokeColor.GetBlueChannel() << ");fill-opacity:" << m_StrokeColor.GetNormalizedAlphaChannel() << ";\"/>";
                            break;
                        case CSVGPrimitive::eNone:
                        case CSVGPrimitive::eStrokeAndFill:
                            OutputString << "<circle cx=\"" << m_Center.GetX() << "\" cy=\"" << m_Center.GetY() << "\" r=\"" << m_Radius << "\" style=\"stroke:rgb(" << m_StrokeColor.GetRedChannel() << "," << m_StrokeColor.GetGreenChannel() << "," << m_StrokeColor.GetBlueChannel() << ")stroke-width:" << m_StrokeWidth << ";stroke-opacity:" << m_StrokeColor.GetNormalizedAlphaChannel() << ";fill:rgb(" << m_FillColor.GetRedChannel() << "," << m_FillColor.GetGreenChannel() << "," << m_FillColor.GetBlueChannel() << ");fill-opacity:" << m_FillColor.GetNormalizedAlphaChannel() << "\"/>";
                            break;
                    }
                    return OutputString.str();
                }

                void CSVGCircle::Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const
                {
                    if (pOutputStream && m_IsActive && (m_Radius > _REAL_EPSILON_))
                        switch (Mode)
                        {
                            case eSVG:
                                *pOutputStream << ToSVGString(Precision);
                                break;
                        }
                }
            }
        }
    }
}
