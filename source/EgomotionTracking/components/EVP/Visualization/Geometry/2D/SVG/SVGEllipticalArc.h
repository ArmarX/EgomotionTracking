/*
 * SVGEllipticalArc.h
 *
 *  Created on: 04.01.2012
 *      Author: gonzalez
 */

#pragma once

#include "SVGPrimitive.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                class CSVGEllipticalArc: public CSVGPrimitive
                {
                public:

                    CSVGEllipticalArc(const Mathematics::_2D::CVector2D& C, const real Rx, const real Ry, const real Alpha, const real Beta0, const real Beta1, const real StrokeWidth, const CRGBAColor& StrokeColor);
                    CSVGEllipticalArc(const Mathematics::_2D::CVector2D& A, const CPixelLocation& B, const real Rx, const real Ry, const real Alpha, const bool LargeArc, const bool PositiveAngleSide, const real StrokeWidth, const CRGBAColor& StrokeColor);
                    ~CSVGEllipticalArc() override;

                    void Serialize(std::ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const override;

                protected:

                    std::string ToSVGString(const uint Precision) const override;

                    Mathematics::_2D::CVector2D m_A;
                    Mathematics::_2D::CVector2D m_B;
                    real m_Rx;
                    real m_Ry;
                    real m_Alpha;
                    bool m_LargeArc;
                    bool m_PositiveAngleSide;
                    CRGBAColor m_StrokeColor;
                    real m_StrokeWidth;
                };
            }
        }
    }
}

