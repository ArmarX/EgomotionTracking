/*
 * CSVGText.h
 *
 *      Author: M. Sc. - Ing. David Israel Gonzalez-Aguirre
 *      Email: gonzalez@ira.ika.de
 */

#pragma once

#include "SVGPrimitive.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Displaying
        {
            namespace SVG
            {
                class CSVGText: public CSVGPrimitive
                {
                public:

                    CSVGText(const_string pText, const real LeftUpperX, const real LeftUpperY, const real FontSize, const CRGBAColor& StrokeColor);
                    CSVGText(const_string pText, const Mathematics::_2D::CVector2D& LeftUpper, const real FontSize, const CRGBAColor& StrokeColor);
                    CSVGText(const_string pText, const CPixelLocation& LeftUpper, const real FontSize, const CRGBAColor& StrokeColor);
                    CSVGText(const string& Text, const real LeftUpperX, const real LeftUpperY, const real FontSize, const CRGBAColor& StrokeColor);
                    CSVGText(const string& Text, const Mathematics::_2D::CVector2D& LeftUpper, const real FontSize, const CRGBAColor& StrokeColor);
                    CSVGText(const string& Text, const CPixelLocation& LeftUpper, const real FontSize, const CRGBAColor& StrokeColor);

                    void Serialize(ostringstream* pOutputStream, const CSVGPrimitive::SerializationMode Mode, const uint Precision) const override;

                protected:

                    std::string ToSVGString(const uint Precision) const override;

                    Mathematics::_2D::CVector2D m_LeftUpper;
                    std::string m_Text;
                    real m_FontSize;
                    CRGBAColor m_StrokeColor;
                };
            }
        }
    }
}

