/*
 * GlobalSettings.cpp
 *
 *  Created on: 18.01.2012
 *      Author: gonzalez
 */
#include "GlobalSettings.h"

namespace EVP
{
    string IdentifierToString(const Identifier Id, const uint Lenght)
    {
        ostringstream Stream;
        Stream << std::uppercase << std::hex << Id;
        string Content = Stream.str();
        int TotalPadding = int(Lenght) - int(Content.length());
        string Padding;
        for (int i = 0; i < TotalPadding; ++i)
        {
            Padding += "0";
        }
        return Padding + Content;
    }

    string DeviceIdentifierToString(const DeviceIdentifier Id)
    {
        return IdentifierToString(Identifier(Id >> 32), 8) + IdentifierToString(Identifier(Id), 8);
    }

#ifdef _USE_SINGLE_PRECISION_

    string realValueToString(const real Value)
    {
        char cad[32];
        sprintf(cad, "%3.3f", Value);
        return string(cad);
    }

#endif

#ifdef _USE_DOUBLE_PRECISION_

    string realValueToString(const real Value)
    {
        char cad[32];
        sprintf(cad, "%3.3f", Value);
        return string(cad);
    }

#endif

#ifdef _USE_LONG_DOUBLE_PRECISION_

    string realValueToString(const real Value)
    {
        char cad[32];
        sprintf(cad, "%3.3Lf", Value);
        return string(cad);
    }

#endif

    byte SafeRoundToByte(const real X)
    {
        if (X >= _REAL_255_)
        {
            return _BYTE_MAX_;
        }
        else if (X <= _REAL_ZERO_)
        {
            return _BYTE_MIN_;
        }
        return byte(X + _REAL_HALF_);
    }
}
