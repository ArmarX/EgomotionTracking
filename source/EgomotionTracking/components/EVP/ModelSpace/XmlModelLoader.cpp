/*
 * XmlModelLoader.cpp
 *
 *  Created on: 23.02.2012
 *      Author: gonzalez
 */

#include "XmlModelLoader.h"

#ifdef _EVP_USE_MODEL_SPACE
#include <qdom.h>
#include <qfile.h>

namespace EVP
{
    namespace ModelSpace
    {
        CXmlModelLoader::CXmlModelLoader() :
            CModelLoader()
        {
        }

        CXmlModelLoader::~CXmlModelLoader()
            = default;

        CModelMultipleMesh* CXmlModelLoader::LoadFromFile(const_string pFileName)
        {
            if (Files::CFile::Exists(pFileName))
            {
                QFile InputFile(pFileName);
#ifdef _EVP_USE_QT3
                if (InputFile.open(IO_ReadOnly))
#else
                if (QIODevice::ReadOnly)
#endif
                {
                    QDomDocument XmlDocument;
                    const bool LoadingContentResult = XmlDocument.setContent(&InputFile);
                    InputFile.close();
                    if (LoadingContentResult)
                    {
                    }
                }
            }
            return nullptr;
        }
    }
}
#endif // _EVP_USE_MODEL_SPACE
