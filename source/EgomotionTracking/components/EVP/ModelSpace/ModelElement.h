/*
 * ModelElement.h
 *
 *  Created on: 23.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "../GlobalSettings.h"
#include "../Foundation/Miscellaneous/IdentifiableInstance.h"
#include "../Foundation/Mathematics/3D/Vector3D.h"
#include "../Foundation/Mathematics/3D/Geometry/Common3D.h"
#include "../Foundation/Files/File.h"
#include "../Foundation/Files/OutFile.h"

#define _EVP_MODELSPACE_APERTURE_TOLERANCE_ real(0.999847695)//1DEG
#define _EVP_MODELSPACE_POINT_DISTANCE_TOLERANCE_ real(0.001)//1MM
#define _EVP_MODELSPACE_SERIALIZING_DIGITS_ 6

#ifdef _EVP_USE_MODEL_SPACE

#include <qdom.h>
#include <qfile.h>

class QDomElement;
class QDomDocument;

namespace EVP
{
    namespace ModelSpace
    {
        namespace Visualization
        {
            class CModelSpaceElementVisualization;
        }

        class CModelElement: public CIdentifiableInstance
        {
            IDENTIFIABLE

        public:

            enum TypeId
            {
                eVertex = 0, eEdge, eFace, eMultiFace, eMesh, eMultiMesh
            };

            enum Visibility
            {
                eStaticOccluded = 0, eStaticVisible, eDynamicVisible
            };

            static  std::string TypeIdToString(const TypeId Id);
            static  TypeId StringToTypeId(const_string pTypeId);

            static  std::string VisibilityToString(const Visibility CurrentVisibility);
            static  Visibility StringToVisibility(const_string pCurrentVisibility);

            CModelElement(const TypeId Id);
            CModelElement(const TypeId Id, const Identifier IntanceId);

            ~CModelElement() override;

            bool HasLable() const;
            void SetLabel(const_string pLabel);
            std::string GetLable() const;

            std::string GetTypeName() const;
            TypeId GetTypeId() const;
            bool TypeEquals(const CModelElement* pModelPrimitive) const;

            bool IsEnabled() const;
            void SetEnabled(const bool Enabled);

            void SetVisibility(const Visibility Visible);
            Visibility GetVisibility() const;

            Visualization::CModelSpaceElementVisualization* GetModelSpaceElementVisualization();
            bool SetModelSpaceElementVisualization(Visualization::CModelSpaceElementVisualization* pModelSpaceElementVisualization);

            virtual  std::string ToString() const;
            virtual  bool SaveToFile(const_string pFileName, const bool OverrideFlag = true) const;
            virtual  bool LoadFromFile(const_string pFileName);

        protected:

            virtual  bool Deserialize(QDomElement& XmlElement) = 0;
            virtual QDomElement Serialize(QDomDocument& XmlDocument) const = 0;

            virtual  bool DeserializeAttributes(QDomElement& XmlElement);
            virtual void SerializeAttributes(QDomElement& XmlElement) const;

            TypeId m_TypeId;
            bool m_Enabled;
            Visibility m_Visibility;
            Visualization::CModelSpaceElementVisualization* m_pModelSpaceElementVisualization;
            std::string* m_pLabel;
        };
    }
}
#endif // _EVP_USE_MODEL_SPACE
