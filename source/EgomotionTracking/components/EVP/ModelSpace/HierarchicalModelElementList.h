/*
 * HierarchicalModelElementList.h
 *
 *  Created on: 24.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "ModelElement.h"
#include "ModelList.h"
#include "../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        template<typename HierarchicalModelElement> class THierarchicalModelElementList: public TModelList<HierarchicalModelElement>
        {
        public:

            THierarchicalModelElementList() :
                TModelList<HierarchicalModelElement> ()
            {
            }

            uint GetTotalElements() const
            {
                return TModelList<HierarchicalModelElement>::size();
            }

            uint GetTotalVisibleElements() const
            {
                uint TotalVisibleElements = 0;
                typename TModelList<HierarchicalModelElement>::const_iterator EndElements = TModelList<HierarchicalModelElement>::end();
                for (typename TModelList<HierarchicalModelElement>::const_iterator ppHierarchicalModelElement = TModelList<HierarchicalModelElement>::begin(); ppHierarchicalModelElement != EndElements; ++ppHierarchicalModelElement)
                    if ((*ppHierarchicalModelElement)->GetVisibility() != CModelElement::eStaticOccluded)
                    {
                        ++TotalVisibleElements;
                    }
                return TotalVisibleElements;
            }

            uint GetTotalEnabledElements() const
            {
                uint TotalEnabledElements = 0;
                typename TModelList<HierarchicalModelElement>::const_iterator EndElements = TModelList<HierarchicalModelElement>::end();
                for (typename TModelList<HierarchicalModelElement>::const_iterator ppHierarchicalModelElement = TModelList<HierarchicalModelElement>::begin(); ppHierarchicalModelElement != EndElements; ++ppHierarchicalModelElement)
                    if ((*ppHierarchicalModelElement)->IsEnabled())
                    {
                        ++TotalEnabledElements;
                    }
                return TotalEnabledElements;
            }

            uint GetTotalIndexableElements() const
            {
                uint TotalIndexableElements = 0;
                typename TModelList<HierarchicalModelElement>::const_iterator EndElements = TModelList<HierarchicalModelElement>::end();
                for (typename TModelList<HierarchicalModelElement>::const_iterator ppHierarchicalModelElement = TModelList<HierarchicalModelElement>::begin(); ppHierarchicalModelElement != EndElements; ++ppHierarchicalModelElement)
                    if ((*ppHierarchicalModelElement)->IsIndexable())
                    {
                        ++TotalIndexableElements;
                    }
                return TotalIndexableElements;
            }

            HierarchicalModelElement GetElementByInstanceId(const Identifier Id)
            {
                typename TModelList<HierarchicalModelElement>::const_iterator EndElements = TModelList<HierarchicalModelElement>::end();
                for (typename TModelList<HierarchicalModelElement>::const_iterator ppHierarchicalModelElement = TModelList<HierarchicalModelElement>::begin(); ppHierarchicalModelElement != EndElements; ++ppHierarchicalModelElement)
                    if ((*ppHierarchicalModelElement)->GetInstanceId() == Id)
                    {
                        return *ppHierarchicalModelElement;
                    }
                return ((HierarchicalModelElement)(NULL));
            }
        };
    }
}

#endif // _EVP_USE_MODEL_SPACE

