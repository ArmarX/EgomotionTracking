/*
 * ModelEdge.h
 *
 *  Created on: 23.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "HierarchicalModelPrimitive.h"
#include "../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelFace;
        class CModelVertex;

        class CModelEdge: public CHierarchicalModelPrimitive
        {
        public:

            CModelEdge(CModelVertex* pModelVertexA, CModelVertex* pModelVertexB, CModelMesh* pModelMesh);
            CModelEdge(CModelMesh* pModelMesh, const Identifier IntanceId);
            ~CModelEdge() override;

            CModelVertex* GetWritableVertexA();
            CModelVertex* GetWritableVertexB();
            CModelVertex* GetWritableVertexComplement(const CModelVertex* pModelVertex);

            const CModelVertex* GetReadOnlyVertexA() const ;
            const CModelVertex* GetReadOnlyVertexB() const ;
            const CModelVertex* GetReadOnlyVertexComplement(const CModelVertex* pModelVertex) const ;

            void SetFace(CModelFace* pModelFace);

            CModelFace* GetWritableFaceAlpha();
            CModelFace* GetWritableFaceBeta();

            bool HasBothFaces() const;
            bool IsFaceRedundant() const;

            bool IsLinking(const CModelVertex* pModelVertex) const;
            bool IsLinking(const CModelVertex* pModelVertexAlpha, const CModelVertex* pModelVertexBeta) const;

            Mathematics::_3D::CVector3D GetMidPoint() const;
            const Mathematics::_3D::CVector3D& GetDirection() const;
            real GetLength() const;
            real GetAperture() const;
            real GetNormalizedAperture() const;

            real GetMaximalAdjacentArea() const;
            bool HasAdjacentAreaInRange(const real MinimalArea, const real MaximalArea) const;
            real GetSelectionDensityAdjacentAreaInRange(const real MinimalArea, const real MaximalArea, const real MinimalScopeArea, const real MaximalScopeArea, const real ExponenFactor) const;

        protected:

            friend class CModelMesh;
            friend class CModelFace;

            void ClearElements();

            bool Deserialize(QDomElement& XmlElement) override;
            QDomElement Serialize(QDomDocument& XmlDocument) const override;

            bool DeserializeAttributes(QDomElement& XmlElement) override;
            void SerializeAttributes(QDomElement& XmlElement) const override;

            QDomElement SerializeReference(QDomDocument& XmlDocument) const override;

            CModelVertex* m_pModelVertexA;
            CModelVertex* m_pModelVertexB;
            CModelFace* m_pFaceAlpha;
            CModelFace* m_pFaceBeta;
            Mathematics::_3D::CVector3D m_Direction;
            real m_Length;
        };
    }
}

#endif // _EVP_USE_MODEL_SPACE
