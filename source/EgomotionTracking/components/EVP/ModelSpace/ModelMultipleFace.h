/*
 * ModelMultipleFace.h
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "HierarchicalModelComposition.h"
#include "ModelMesh.h"
#include "../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelFace;
        class CModelEdge;
        class CModelVertex;

        class CModelMultipleFace: public CHierarchicalModelComposition
        {
        public:

            CModelMultipleFace(CModelFace* pFace);
            CModelMultipleFace(CModelMesh* pModelMesh, const Identifier IntanceId);
            ~CModelMultipleFace() override;

            CModelMesh* GetWritableMesh();
            const CModelMesh* GetReadOnlyMesh() const;

            const THierarchicalModelElementList<CModelFace*>& GetFaces() const;
            const THierarchicalModelElementList<CModelEdge*>& GetEdges() const;
            const THierarchicalModelElementList<CModelVertex*>& GetVeritices() const;

            const Mathematics::_3D::CVector3D& GetNormal() const;
            const Mathematics::_3D::CVector3D& GetCenteroid() const;
            real GetArea() const;
            bool IsTrivial() const;

            uint GetTotalPrimitives() const override;

        protected:

            friend class CModelMesh;

            void ClearElements();

            bool Deserialize(QDomElement& XmlElement) override;
            QDomElement Serialize(QDomDocument& XmlDocument) const override;

            bool DeserializeAttributes(QDomElement& XmlElement) override;
            void SerializeAttributes(QDomElement& XmlElement) const override;

            uint Expansion(CModelFace* pFace, const real OrientationTolerance);
            void Extraction();
            void AddFace(CModelFace* pFace);

            THierarchicalModelElementList<CModelFace*> m_Faces;
            THierarchicalModelElementList<CModelEdge*> m_Edges;
            THierarchicalModelElementList<CModelVertex*> m_Veritices;
            Mathematics::_3D::CVector3D m_Normal;
            Mathematics::_3D::CVector3D m_Centeroid;
            real m_Area;
        };
    }
}

#endif // _EVP_USE_MODEL_SPACE
