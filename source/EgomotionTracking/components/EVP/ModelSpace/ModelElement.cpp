/*
 * ModelElement.cpp
 *
 *  Created on: 23.11.2011
 *      Author: gonzalez
 */

#include "ModelElement.h"
//#include "Visualization/ModelSpaceElementVisualization.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        IDENTIFIABLE_INITIALIZATION(CModelElement)

        std::string CModelElement::TypeIdToString(const TypeId Id)
        {
            switch (Id)
            {
                case eVertex:
                    return std::string("Vertex");
                case eEdge:
                    return std::string("Edge");
                case eFace:
                    return std::string("Face");
                case eMultiFace:
                    return std::string("MultiFace");
                case eMesh:
                    return std::string("Mesh");
                case eMultiMesh:
                    return std::string("MultiMesh");
            }
            return std::string("Unknown");
        }

        CModelElement::TypeId CModelElement::StringToTypeId(const_string pTypeId)
        {
            if (pTypeId)
            {
                const std::string CurrentTypeId(pTypeId);
                if (CurrentTypeId == std::string("Vertex"))
                {
                    return eVertex;
                }
                else if (CurrentTypeId == std::string("Edge"))
                {
                    return eEdge;
                }
                else if (CurrentTypeId == std::string("Face"))
                {
                    return eFace;
                }
                else if (CurrentTypeId == std::string("MultiFace"))
                {
                    return eMultiFace;
                }
                else if (CurrentTypeId == std::string("Mesh"))
                {
                    return eMesh;
                }
                else if (CurrentTypeId == std::string("eMultiMesh"))
                {
                    return eMultiMesh;
                }
            }
            return CModelElement::TypeId(eMultiMesh + 1);
        }

        std::string CModelElement::VisibilityToString(const Visibility CurrentVisibility)
        {
            switch (CurrentVisibility)
            {
                case eStaticOccluded:
                    return std::string("StaticOccluded");
                    break;
                case eStaticVisible:
                    return std::string("StaticVisible");
                    break;
                case eDynamicVisible:
                    return std::string("DynamicVisible");
                    break;
            }
            return std::string("Unknown");
        }

        CModelElement::Visibility CModelElement::StringToVisibility(const_string pCurrentVisibility)
        {
            if (pCurrentVisibility)
            {
                const std::string CurrentVisibility(pCurrentVisibility);
                if (CurrentVisibility == std::string("StaticOccluded"))
                {
                    return eStaticOccluded;
                }
                else if (CurrentVisibility == std::string("StaticVisible"))
                {
                    return eStaticVisible;
                }
                else if (CurrentVisibility == std::string("DynamicVisible"))
                {
                    return eDynamicVisible;
                }
            }
            return eStaticOccluded;
        }

        CModelElement::CModelElement(const TypeId Id) :
            IDENTIFIABLE_CONTRUCTOR(CModelElement), m_TypeId(Id), m_Enabled(true), m_Visibility(eStaticVisible), m_pModelSpaceElementVisualization(nullptr), m_pLabel(nullptr)
        {
        }

        CModelElement::CModelElement(const TypeId Id, const Identifier IntanceId) :
            CIdentifiableInstance(IntanceId), m_TypeId(Id), m_Enabled(false), m_Visibility(eStaticOccluded), m_pModelSpaceElementVisualization(nullptr), m_pLabel(nullptr)
        {
        }

        CModelElement::~CModelElement()
        {
            RELEASE_OBJECT(m_pLabel);
        }

        bool CModelElement::HasLable() const
        {
            return m_pLabel;
        }

        void CModelElement::SetLabel(const_string pLabel)
        {
            if (pLabel)
            {
                if (m_pLabel)
                {
                    *m_pLabel = std::string(pLabel);
                }
                else
                {
                    m_pLabel = new std::string(pLabel);
                }
            }
        }

        std::string CModelElement::GetLable() const
        {
            if (m_pLabel)
            {
                return *m_pLabel;
            }
            return std::string();
        }

        std::string CModelElement::GetTypeName() const
        {
            return TypeIdToString(m_TypeId);
        }

        CModelElement::TypeId CModelElement::GetTypeId() const
        {
            return m_TypeId;
        }

        bool CModelElement::TypeEquals(const CModelElement* pModelPrimitive) const
        {
            if (pModelPrimitive)
            {
                return (pModelPrimitive->m_TypeId == m_TypeId);
            }
            return false;
        }

        bool CModelElement::IsEnabled() const
        {
            return m_Enabled;
        }

        void CModelElement::SetEnabled(const bool Enabled)
        {
            m_Enabled = Enabled;
        }

        void CModelElement::SetVisibility(const Visibility Visible)
        {
            m_Visibility = Visible;
        }

        CModelElement::Visibility CModelElement::GetVisibility() const
        {
            return m_Visibility;
        }

        Visualization::CModelSpaceElementVisualization* CModelElement::GetModelSpaceElementVisualization()
        {
            return m_pModelSpaceElementVisualization;
        }

        bool CModelElement::SetModelSpaceElementVisualization(Visualization::CModelSpaceElementVisualization* pModelSpaceElementVisualization)
        {
            m_pModelSpaceElementVisualization = pModelSpaceElementVisualization;
            return pModelSpaceElementVisualization;
        }

        std::string CModelElement::ToString() const
        {
            QDomDocument XmlDocument;
            QDomElement RootElement = XmlDocument.createElement("EVP_MODEL");
            XmlDocument.appendChild(RootElement);
            RootElement.appendChild(Serialize(XmlDocument));
#ifdef _EVP_USE_QT3
            return std::string(XmlDocument.toString().latin1());
#else
            return std::string(XmlDocument.toString().toLatin1().data());
#endif
        }

        bool CModelElement::SaveToFile(const_string pFileName, const bool OverrideFlag) const
        {
            if (pFileName)
            {
                if (!OverrideFlag && Files::CFile::Exists(pFileName))
                {
                    return false;
                }
                Files::COutFile OutputTextFile(pFileName, Files::CFile::eText);
                if (OutputTextFile.IsReady())
                    if (OutputTextFile.WriteString(ToString()))
                    {
                        return OutputTextFile.Close();
                    }
            }
            return false;
        }

        bool CModelElement::LoadFromFile(const_string pFileName)
        {
            if (pFileName)
            {
                if (!Files::CFile::Exists(pFileName))
                {
                    return false;
                }
                QDomDocument XmlDocument("EVP_MODEL");
                QString FileName(pFileName);
                QFile InputFile(FileName);
#ifdef _EVP_USE_QT3
                if (!InputFile.open(IO_ReadOnly))
                {
                    return false;
                }
#else
                if (!InputFile.open(QIODevice::ReadOnly))
                {
                    return false;
                }
#endif

                const bool LoadingResult = XmlDocument.setContent(&InputFile);
                InputFile.close();
                if (LoadingResult)
                {
                    QDomElement XmlModelElementRoot = XmlDocument.firstChild().toElement();
                    if (XmlModelElementRoot.tagName() == "EVP_MODEL")
                    {
                        QDomElement XmlModelElement = XmlModelElementRoot.firstChild().toElement();
                        return Deserialize(XmlModelElement);
                    }
                }
            }
            return false;
        }

        bool CModelElement::DeserializeAttributes(QDomElement& XmlElement)
        {
            if (XmlElement.hasAttribute("Type") && XmlElement.hasAttribute("Id") && XmlElement.hasAttribute("Enabled") && XmlElement.hasAttribute("Visibility"))
            {
                m_TypeId = TypeId(XmlElement.attribute("Type").toUInt());
                m_InstanceId = Identifier(XmlElement.attribute("Id").toUInt());
                m_Enabled = bool(XmlElement.attribute("Enabled").toUInt());
                m_Visibility = Visibility(XmlElement.attribute("Visibility").toUInt());
                if (XmlElement.hasAttribute("Label"))
                {
#ifdef _EVP_USE_QT3
                    if (m_pLabel)
                    {
                        *m_pLabel = std::string(XmlElement.attribute("Label").latin1());
                    }
                    else
                    {
                        m_pLabel = new std::string(XmlElement.attribute("Label").latin1());
                    }
#else
                    if (m_pLabel)
                    {
                        *m_pLabel = std::string(XmlElement.attribute("Label").toLatin1().data());
                    }
                    else
                    {
                        m_pLabel = new std::string(XmlElement.attribute("Label").toLatin1().data());
                    }
#endif
                }
                else
                {
                    RELEASE_OBJECT(m_pLabel);
                }
                return true;
            }
            return false;
        }

        void CModelElement::SerializeAttributes(QDomElement& XmlElement) const
        {
            XmlElement.setAttribute("Type", m_TypeId);
            XmlElement.setAttribute("Id", m_InstanceId);
            XmlElement.setAttribute("Enabled", m_Enabled);
            XmlElement.setAttribute("Visibility", m_Visibility);
#ifdef _EVP_USE_QT3
            if (m_pLabel)
            {
                XmlElement.setAttribute("Label", *m_pLabel);
            }
#else
            if (m_pLabel)
            {
                XmlElement.setAttribute("Label", QString::fromStdString(*m_pLabel));
            }
#endif
        }
    }
}

#endif //_EVP_USE_MODEL_SPACE
