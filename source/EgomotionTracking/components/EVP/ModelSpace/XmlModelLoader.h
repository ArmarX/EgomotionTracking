/*
 * XmlModelLoader.h
 *
 *  Created on: 23.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "../Foundation/Files/File.h"
#include "ModelLoader.h"
#include "ModelArray.h"
#include "ModelMesh.h"
#include "ModelVertex.h"
#include "ModelEdge.h"
#include "ModelFace.h"
#include "ModelMultipleFace.h"
#include "../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CXmlModelLoader: public CModelLoader
        {
        public:

            CXmlModelLoader();
            ~CXmlModelLoader() override;

            CModelMultipleMesh* LoadFromFile(const_string pFileName) override;
        };
    }
}

#endif // _EVP_USE_MODEL_SPACE
