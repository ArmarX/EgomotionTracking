/*
 * HierarchicalModelComposition.h
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "HierarchicalModelElement.h"
#include "../GlobalSettings.h"


#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CHierarchicalModelComposition: public CHierarchicalModelElement
        {
        public:

            CHierarchicalModelComposition(const CModelElement::TypeId Id, CHierarchicalModelElement* pSuperior);
            CHierarchicalModelComposition(const CModelElement::TypeId Id, CHierarchicalModelElement* pSuperior, const Identifier IntanceId);
            ~CHierarchicalModelComposition() override;

            virtual  uint GetTotalPrimitives() const = 0;

        protected:

            bool DeserializeAttributes(QDomElement& XmlElement) override;
            void SerializeAttributes(QDomElement& XmlElement) const override;
        };
    }
}

#endif // _EVP_USE_MODEL_SPACE
