/*
 * ModelPrimitiveIndexer.h
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "ModelPrimitiveIndexingInterval.h"
#include "ModelArray.h"
#include "../GlobalSettings.h"

namespace EVP
{
    namespace ModelSpace
    {
        class CModelPrimitiveIndexer
        {
        public:

            CModelPrimitiveIndexer();
            virtual ~CModelPrimitiveIndexer();
        };
    }
}

