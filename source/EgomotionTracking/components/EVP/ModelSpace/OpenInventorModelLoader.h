/*
 * OpenInventorModelLoader.h
 *
 *  Created on: 25.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "ModelLoader.h"
#include "ModelArray.h"
#include "ModelMesh.h"
#include "ModelVertex.h"
#include "ModelEdge.h"
#include "ModelFace.h"
#include "ModelMultipleFace.h"
#include "../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE
class SoSeparator;
class SoNormal;
class SoNormalBinding;
class SoCoordinate3;
class SoFaceSet;

namespace EVP
{
    namespace ModelSpace
    {
        class COpenInventorModelLoader: public CModelLoader
        {
        public:

            COpenInventorModelLoader();
            ~COpenInventorModelLoader() override;

            CModelMultipleMesh* LoadFromFile(const_string pFileName) override;

        protected:

            TModelList<SoSeparator*> ExtractMeshesSeparators(SoSeparator* pFileRoot);
            bool GetMeshesStructuralElements(SoSeparator* pMeshesSeparator, SoNormal*& pNormal, SoNormalBinding*& pNormalBinding, SoCoordinate3*& pCoordinate3, SoFaceSet*& pFaceSet);
            TModelArray<CModelVertex*> LoadVertices(const real Tolerance, SoCoordinate3* pCoordinate3, CModelMesh* pModelMesh);
            TModelArray<CModelFace*> LoadFaces(const TModelArray<CModelVertex*>& Vertices, SoNormalBinding* pNormalBinding, SoNormal* pNormal, SoFaceSet* pFaceSet, CModelMesh* pModelMesh);
            void LoadMultiFaces(const TModelArray<CModelFace*>& Faces);
        };
    }
}
#endif // _EVP_USE_MODEL_SPACE

