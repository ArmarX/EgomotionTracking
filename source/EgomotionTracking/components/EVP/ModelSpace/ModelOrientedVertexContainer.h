/*
 * ModelOrientedVertexContainer.h
 *
 *  Created on: 24.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "ModelVertex.h"
#include "../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelOrientedVertexContainer
        {
        public:

            CModelOrientedVertexContainer(CModelVertex* pModelVertex, const Mathematics::_3D::CVector3D& Normal);
            CModelOrientedVertexContainer();

            void Clear();
            void SetModelVertex(CModelVertex* pModelVertex);
            CModelVertex* GetWritableModelVertex();
            const CModelVertex* GetReadOnlyModelVertex() const;
            bool AddFace(CModelFace* pModelFace);
            const Mathematics::_3D::CVector3D& GetPoint() const;
            void SetNormal(const Mathematics::_3D::CVector3D& Normal);
            const Mathematics::_3D::CVector3D& GetNormal() const;
            uint GetIndexInMesh() const;

        protected:

            friend class CModelFace;

            bool Deserialize(QDomElement& XmlElement, THierarchicalModelElementList<CModelVertex*>& MeshVeritices);
            QDomElement Serialize(QDomDocument& XmlDocument) const;

            CModelVertex* m_pModelVertex;
            Mathematics::_3D::CVector3D m_Normal;
        };
    }
}
#endif // _EVP_USE_MODEL_SPACE
