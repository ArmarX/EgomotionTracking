/*
 * ModelMesh.cpp
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#include "ModelVertex.h"
#include "ModelEdge.h"
#include "ModelFace.h"
#include "ModelMultipleFace.h"
#include "ModelMesh.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        CModelMesh::CModelMesh(CModelMultipleMesh* pMultipleMesh) :
            CHierarchicalModelComposition(CModelElement::eMesh, pMultipleMesh)
        {
            pMultipleMesh->AddMeshes(this, false);
        }

        CModelMesh::CModelMesh(CModelMultipleMesh* pMultipleMesh, const Identifier IntanceId) :
            CHierarchicalModelComposition(CModelElement::eMesh, pMultipleMesh, IntanceId)
        {
        }

        CModelMesh::~CModelMesh()
        {
            ClearElements();
        }

        void CModelMesh::ClearElements()
        {
            THierarchicalModelElementList<CModelVertex*>::const_iterator EndVeritices = m_Vertices.end();
            for (THierarchicalModelElementList<CModelVertex*>::const_iterator ppModelVertex = m_Vertices.begin(); ppModelVertex != EndVeritices; ++ppModelVertex)
            {
                delete *ppModelVertex;
            }
            m_Vertices.clear();
            THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = m_Edges.end();
            for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = m_Edges.begin(); ppModelEdge != EndEdges; ++ppModelEdge)
            {
                delete *ppModelEdge;
            }
            m_Edges.clear();
            THierarchicalModelElementList<CModelMultipleFace*>::const_iterator EndMultipleFaces = m_MultipleFaces.end();
            for (THierarchicalModelElementList<CModelMultipleFace*>::const_iterator ppModelMultipleFace = m_MultipleFaces.begin(); ppModelMultipleFace != EndMultipleFaces; ++ppModelMultipleFace)
            {
                delete *ppModelMultipleFace;
            }
            m_MultipleFaces.clear();
            THierarchicalModelElementList<CModelFace*>::const_iterator EndFaces = m_Faces.end();
            for (THierarchicalModelElementList<CModelFace*>::const_iterator ppModelFace = m_Faces.begin(); ppModelFace != EndFaces; ++ppModelFace)
            {
                delete *ppModelFace;
            }
            m_Faces.clear();
        }

        bool CModelMesh::AddVertex(CModelVertex* pModelVertex, const bool CheckUnique)
        {
            return m_Vertices.AddElement(pModelVertex, CheckUnique);
        }

        bool CModelMesh::AddEdge(CModelEdge* pModelEdge, const bool CheckUnique)
        {
            return m_Edges.AddElement(pModelEdge, CheckUnique);
        }

        bool CModelMesh::AddFace(CModelFace* pModelFace, const bool CheckUnique)
        {
            return m_Faces.AddElement(pModelFace, CheckUnique);
        }

        bool CModelMesh::AddMultipleFace(CModelMultipleFace* pModelMultipleFace, const bool CheckUnique)
        {
            return m_MultipleFaces.AddElement(pModelMultipleFace, CheckUnique);
        }

        bool CModelMesh::UpdateIndexability(const real MaximalLength, const real MinimalLength, const real MaximalAperture, const real MinimalAperture, const real MaximalArea, const real MinimalArea)
        {
            uint TotalIndexablePrimitives = 0;
            if (m_Edges.size())
            {
                THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = m_Edges.end();
                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = m_Edges.begin(); ppModelEdge != EndEdges; ++ppModelEdge)
                    if ((*ppModelEdge)->IsFaceRedundant() || ((*ppModelEdge)->GetLength() < MinimalLength) || ((*ppModelEdge)->GetLength() > MaximalLength) || ((*ppModelEdge)->GetAperture() < MinimalAperture) || ((*ppModelEdge)->GetAperture() > MaximalAperture))
                    {
                        (*ppModelEdge)->SetIndexable(false);
                    }
                    else
                    {
                        (*ppModelEdge)->SetIndexable(true);
                        ++TotalIndexablePrimitives;
                    }
            }
            if (m_Faces.size())
            {
                THierarchicalModelElementList<CModelFace*>::const_iterator EndFaces = m_Faces.end();
                for (THierarchicalModelElementList<CModelFace*>::const_iterator ppModelFace = m_Faces.begin(); ppModelFace != EndFaces; ++ppModelFace)
                    if ((*ppModelFace)->GetMultipleFace() || ((*ppModelFace)->GetArea() < MinimalArea) || ((*ppModelFace)->GetArea() > MaximalArea))
                    {
                        (*ppModelFace)->SetIndexable(false);
                    }
                    else
                    {
                        (*ppModelFace)->SetIndexable(true);
                        ++TotalIndexablePrimitives;
                    }
            }
            if (m_MultipleFaces.size())
            {
                THierarchicalModelElementList<CModelMultipleFace*>::const_iterator EndMultipleFaces = m_MultipleFaces.end();
                for (THierarchicalModelElementList<CModelMultipleFace*>::const_iterator ppModelMultipleFace = m_MultipleFaces.begin(); ppModelMultipleFace != EndMultipleFaces; ++ppModelMultipleFace)
                    if (((*ppModelMultipleFace)->GetArea() < MinimalArea) || ((*ppModelMultipleFace)->GetArea() > MaximalArea))
                    {
                        (*ppModelMultipleFace)->SetIndexable(false);
                    }
                    else
                    {
                        (*ppModelMultipleFace)->SetIndexable(true);
                        ++TotalIndexablePrimitives;
                    }
            }
            return TotalIndexablePrimitives;
        }

        bool CModelMesh::SetEnableByCriteria(const real MaximalLength, const real MinimalLength, const real MaximalAperture, const real MinimalAperture, const real MaximalArea, const real MinimalArea)
        {
            uint TotalEnabledPrimitives = 0;
            if (m_Edges.size())
            {
                THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = m_Edges.end();
                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = m_Edges.begin(); ppModelEdge != EndEdges; ++ppModelEdge)
                    if ((*ppModelEdge)->IsFaceRedundant() || ((*ppModelEdge)->GetLength() < MinimalLength) || ((*ppModelEdge)->GetLength() > MaximalLength) || ((*ppModelEdge)->GetAperture() < MinimalAperture) || ((*ppModelEdge)->GetAperture() > MaximalAperture))
                    {
                        (*ppModelEdge)->SetEnabled(false);
                    }
                    else
                    {
                        (*ppModelEdge)->SetEnabled(true);
                        ++TotalEnabledPrimitives;
                    }
            }
            if (m_Faces.size())
            {
                THierarchicalModelElementList<CModelFace*>::const_iterator EndFaces = m_Faces.end();
                for (THierarchicalModelElementList<CModelFace*>::const_iterator ppModelFace = m_Faces.begin(); ppModelFace != EndFaces; ++ppModelFace)
                    if ((*ppModelFace)->GetMultipleFace() || ((*ppModelFace)->GetArea() < MinimalArea) || ((*ppModelFace)->GetArea() > MaximalArea))
                    {
                        (*ppModelFace)->SetEnabled(false);
                    }
                    else
                    {
                        (*ppModelFace)->SetEnabled(true);
                        ++TotalEnabledPrimitives;
                    }
            }
            if (m_MultipleFaces.size())
            {
                THierarchicalModelElementList<CModelMultipleFace*>::const_iterator EndMultipleFaces = m_MultipleFaces.end();
                for (THierarchicalModelElementList<CModelMultipleFace*>::const_iterator ppModelMultipleFace = m_MultipleFaces.begin(); ppModelMultipleFace != EndMultipleFaces; ++ppModelMultipleFace)
                    if (((*ppModelMultipleFace)->GetArea() < MinimalArea) || ((*ppModelMultipleFace)->GetArea() > MaximalArea))
                    {
                        (*ppModelMultipleFace)->SetEnabled(false);
                    }
                    else
                    {
                        (*ppModelMultipleFace)->SetEnabled(true);
                        ++TotalEnabledPrimitives;
                    }
            }
            return TotalEnabledPrimitives;
        }

        void CModelMesh::ApplyTransformation(const EVP::Mathematics::_4D::CMatrix4D& Transformation)
        {
            THierarchicalModelElementList<CModelVertex*>::iterator EndVertices = m_Vertices.end();
            for (THierarchicalModelElementList<CModelVertex*>::iterator ppModelVertex = m_Vertices.begin(); ppModelVertex != EndVertices; ++ppModelVertex)
            {
                (*ppModelVertex)->ApplyTransformation(Transformation);
            }
        }

        const THierarchicalModelElementList<CModelVertex*>& CModelMesh::GetReadOnlyVeritices() const
        {
            return m_Vertices;
        }

        const THierarchicalModelElementList<CModelEdge*>& CModelMesh::GetReadOnlyEdges() const
        {
            return m_Edges;
        }

        const THierarchicalModelElementList<CModelFace*>& CModelMesh::GetReadOnlyFaces() const
        {
            return m_Faces;
        }

        const THierarchicalModelElementList<CModelMultipleFace*>& CModelMesh::GetReadOnlyMultipleFaces() const
        {
            return m_MultipleFaces;
        }

        THierarchicalModelElementList<CModelVertex*>& CModelMesh::GetWritableVeritices()
        {
            return m_Vertices;
        }

        THierarchicalModelElementList<CModelEdge*>& CModelMesh::GetWritableEdges()
        {
            return m_Edges;
        }

        THierarchicalModelElementList<CModelFace*>& CModelMesh::GetWritableFaces()
        {
            return m_Faces;
        }

        THierarchicalModelElementList<CModelMultipleFace*>& CModelMesh::GetWritableMultipleFaces()
        {
            return m_MultipleFaces;
        }

        uint CModelMesh::GetTotalPrimitives() const
        {
            return m_Vertices.size() + m_Edges.size() + m_Faces.size() + m_MultipleFaces.size();
        }

        bool CModelMesh::Deserialize(QDomElement& XmlElement)
        {
            if (XmlElement.tagName() == "Mesh")
            {
                ClearElements();
                DeserializeAttributes(XmlElement);

                QDomNodeList XmlElements = XmlElement.childNodes();

                QDomNode XmlVeritice = XmlElements.item(0).firstChild();
                while (!XmlVeritice.isNull())
                {
                    QDomElement XmlModelVertex = XmlVeritice.toElement();
                    CModelVertex* pModelVertex = new CModelVertex(this, 0);
                    if (pModelVertex->Deserialize(XmlModelVertex))
                    {
                        m_Vertices.push_back(pModelVertex);
                    }
                    else
                    {
                        delete pModelVertex;
                    }
                    XmlVeritice = XmlVeritice.nextSibling();
                }

                QDomNode XmlEdge = XmlElements.item(1).firstChild();
                while (!XmlEdge.isNull())
                {
                    QDomElement XmlModelEdge = XmlEdge.toElement();
                    CModelEdge* pModelEdge = new CModelEdge(this, 0);
                    if (pModelEdge->Deserialize(XmlModelEdge))
                    {
                        m_Edges.push_back(pModelEdge);
                    }
                    else
                    {
                        delete pModelEdge;
                    }
                    XmlEdge = XmlEdge.nextSibling();
                }

                QDomNode XmlFace = XmlElements.item(2).firstChild();
                while (!XmlFace.isNull())
                {
                    QDomElement XmlModelFace = XmlFace.toElement();
                    CModelFace* pModelFace = new CModelFace(this, 0);
                    if (pModelFace->Deserialize(XmlModelFace))
                    {
                        m_Faces.push_back(pModelFace);
                    }
                    else
                    {
                        delete pModelFace;
                    }
                    XmlFace = XmlFace.nextSibling();
                }

                QDomNode XmlMultipleFace = XmlElements.item(3).firstChild();
                while (!XmlMultipleFace.isNull())
                {
                    QDomElement XmlModelMultipleFace = XmlMultipleFace.toElement();
                    CModelMultipleFace* pModelMultipleFace = new CModelMultipleFace(this, 0);
                    if (pModelMultipleFace->Deserialize(XmlModelMultipleFace))
                    {
                        m_MultipleFaces.push_back(pModelMultipleFace);
                    }
                    else
                    {
                        delete pModelMultipleFace;
                    }
                    XmlMultipleFace = XmlMultipleFace.nextSibling();
                }

                return true;
            }
            return false;
        }

        QDomElement CModelMesh::Serialize(QDomDocument& XmlDocument) const
        {
            QDomElement XmlElement = XmlDocument.createElement("Mesh");
            SerializeAttributes(XmlElement);

            QDomElement XmlVeritices = XmlDocument.createElement("Vertices");
            XmlElement.appendChild(XmlVeritices);
            THierarchicalModelElementList<CModelVertex*>::const_iterator EndVertices = m_Vertices.end();
            for (THierarchicalModelElementList<CModelVertex*>::const_iterator ppModelVertex = m_Vertices.begin(); ppModelVertex != EndVertices; ++ppModelVertex)
            {
                XmlVeritices.appendChild((*ppModelVertex)->Serialize(XmlDocument));
            }

            QDomElement XmlEdges = XmlDocument.createElement("Edges");
            XmlElement.appendChild(XmlEdges);
            THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = m_Edges.end();
            for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = m_Edges.begin(); ppModelEdge != EndEdges; ++ppModelEdge)
            {
                XmlEdges.appendChild((*ppModelEdge)->Serialize(XmlDocument));
            }

            QDomElement XmlFaces = XmlDocument.createElement("Faces");
            XmlElement.appendChild(XmlFaces);
            THierarchicalModelElementList<CModelFace*>::const_iterator EndFaces = m_Faces.end();
            for (THierarchicalModelElementList<CModelFace*>::const_iterator ppModelFace = m_Faces.begin(); ppModelFace != EndFaces; ++ppModelFace)
            {
                XmlFaces.appendChild((*ppModelFace)->Serialize(XmlDocument));
            }

            QDomElement XmlMultipleFaces = XmlDocument.createElement("MultipleFaces");
            XmlElement.appendChild(XmlMultipleFaces);
            THierarchicalModelElementList<CModelMultipleFace*>::const_iterator EndMultipleFaces = m_MultipleFaces.end();
            for (THierarchicalModelElementList<CModelMultipleFace*>::const_iterator ppMultipleFace = m_MultipleFaces.begin(); ppMultipleFace != EndMultipleFaces; ++ppMultipleFace)
            {
                XmlMultipleFaces.appendChild((*ppMultipleFace)->Serialize(XmlDocument));
            }

            return XmlElement;
        }

        bool CModelMesh::DeserializeAttributes(QDomElement& XmlElement)
        {
            return CHierarchicalModelComposition::DeserializeAttributes(XmlElement);
        }

        void CModelMesh::SerializeAttributes(QDomElement& XmlElement) const
        {
            CHierarchicalModelComposition::SerializeAttributes(XmlElement);
        }
    }
}
#endif // _EVP_USE_MODEL_SPACE
