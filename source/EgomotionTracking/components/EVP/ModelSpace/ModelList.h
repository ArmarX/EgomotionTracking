/*
 * ModelList.h
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "../GlobalSettings.h"

namespace EVP
{
    namespace ModelSpace
    {
        template<typename ElementType> class TModelList: public list<ElementType>
        {
        public:

            TModelList() :
                list<ElementType>()
            {
            }

            TModelList(ElementType& Element) :
                list<ElementType>()
            {
                list<ElementType>::push_back(Element);
            }

            virtual ~TModelList()
            {
            }

            ElementType PopFront()
            {
                ElementType CurrentReference = list<ElementType>::front();
                list<ElementType>::pop_front();
                return CurrentReference;
            }

            bool AddElement(ElementType Element, const bool CheckUnique)
            {
                if (Element)
                {
                    if (CheckUnique)
                    {
                        typename list<ElementType>::const_iterator EndElements = list<ElementType>::end();
                        for (typename list<ElementType>::const_iterator pElementType = list<ElementType>::begin(); pElementType != EndElements; ++pElementType)
                            if (*pElementType == Element)
                            {
                                return false;
                            }
                    }
                    list<ElementType>::push_back(Element);
                    return true;
                }
                return false;
            }

            uint AddElements(TModelList<ElementType>& List, const bool CheckUnique)
            {
                if (List.size())
                {
                    if (CheckUnique)
                    {
                        uint TotalAddElements = 0;
                        typename list<ElementType>::const_iterator EndElements = List.end();
                        for (typename list<ElementType>::const_iterator pElementType = List.begin(); pElementType != EndElements; ++pElementType)
                            if (AddElement(*pElementType, true))
                            {
                                ++TotalAddElements;
                            }
                        return TotalAddElements;
                    }
                    else
                    {
                        list<ElementType>::insert(list<ElementType>::end(), List.begin(), List.end());
                        return List.size();
                    }
                }
                return 0;
            }

            uint SortedUnique()
            {
                list<ElementType>::sort();
                list<ElementType>::unique();
                return list<ElementType>::size();
            }

            uint Sorted()
            {
                list<ElementType>::sort();
                return list<ElementType>::size();
            }
        };
    }
}

