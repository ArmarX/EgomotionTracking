/*
 * ModelPrimitiveIndexingInterval.h
 *
 *  Created on: 24.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "../GlobalSettings.h"

namespace EVP
{
    namespace ModelSpace
    {
        class CModelPrimitiveIndexer;

        class CModelPrimitiveIndexingInterval
        {
        public:

            CModelPrimitiveIndexingInterval(CModelPrimitiveIndexer* pModelPrimitiveIndexer, const Identifier QueryId);
            CModelPrimitiveIndexingInterval(CModelPrimitiveIndexer* pModelPrimitiveIndexer, const Identifier QueryId, const uint IndexA, const uint IndexB, const uint Size);
            ~CModelPrimitiveIndexingInterval();

            const CModelPrimitiveIndexer* GetIndexer() const;
            Identifier GetQueryId() const;
            bool IsEmpty() const;
            bool HasElements() const;
            uint GetLenght() const;
            uint GetIndexA() const;
            uint GetIndexB() const;
            real GetAccumulatedProbability() const;

        protected:

            const CModelPrimitiveIndexer* m_pModelPrimitiveIndexer;
            const Identifier m_QueryId;
            const uint m_IndexA;
            const uint m_IndexB;
            const uint m_Length;
            const real m_AccumulatedProbability;
        };
    }
}

