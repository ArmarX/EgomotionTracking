/*
 * ModelArray.h
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "../GlobalSettings.h"
#include "ModelList.h"

namespace EVP
{
    namespace ModelSpace
    {
        template<typename ElementType> class TModelArray: public std::vector<ElementType>
        {
        public:

            TModelArray() :
                std::vector<ElementType> ()
            {
            }

            TModelArray(const uint Size) :
                std::vector<ElementType> ()
            {
                this->reserve(Size);
            }

            virtual ~TModelArray()
            {
            }

            void SetSize(const uint Size)
            {
                this->reserve(Size);
            }

            bool AddElement(ElementType Element, const bool CheckUnique)
            {
                if (Element)
                {
                    if (CheckUnique)
                    {
                        typename std::vector<ElementType>::const_iterator EndElements = std::vector<ElementType>::end();
                        for (typename std::vector<ElementType>::const_iterator pElementType = std::vector<ElementType>::begin(); pElementType != EndElements; ++pElementType)
                            if (*pElementType == Element)
                            {
                                return false;
                            }
                    }
                    this->push_back(Element);
                    return true;
                }
                return false;
            }

            uint AddElements(const TModelList<ElementType>& List, const bool CheckUnique)
            {
                if (List.size())
                {
                    uint TotalAddElements = 0;
                    typename TModelList<ElementType>::const_iterator EndElements = List.end();
                    for (typename TModelList<ElementType>::const_iterator pElementType = List.begin(); pElementType != EndElements; ++pElementType)
                        if (AddElement(*pElementType, CheckUnique))
                        {
                            ++TotalAddElements;
                        }
                    return TotalAddElements;
                }
                return 0;
            }
        };
    }
}

