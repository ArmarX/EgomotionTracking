/*
 * ModelLoader.h
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "ModelMultipleMesh.h"
#include "../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelLoader
        {
        public:

            CModelLoader();
            virtual ~CModelLoader();

            virtual CModelMultipleMesh* LoadFromFile(const_string pFileName) = 0;
        };
    }
}
#endif // _EVP_USE_MODEL_SPACE
