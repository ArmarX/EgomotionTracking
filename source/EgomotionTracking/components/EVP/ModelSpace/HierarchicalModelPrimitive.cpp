/*
 * HierarchicalModelPrimitive.cpp
 *
 *  Created on: 23.11.2011
 *      Author: gonzalez
 */

#include "HierarchicalModelPrimitive.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        CHierarchicalModelPrimitive::CHierarchicalModelPrimitive(const CModelElement::TypeId Id, CModelMesh* pMesh) :
            CHierarchicalModelElement(Id, pMesh)
        {
        }

        CHierarchicalModelPrimitive::CHierarchicalModelPrimitive(const CModelElement::TypeId Id, CModelMesh* pMesh, const Identifier IntanceId) :
            CHierarchicalModelElement(Id, pMesh, IntanceId)
        {
        }

        CHierarchicalModelPrimitive::~CHierarchicalModelPrimitive()
        {
            ClearDensitySelections();
        }

        CModelMesh* CHierarchicalModelPrimitive::GetWritableMesh()
        {
            return dynamic_cast<CModelMesh*>(GetWritableSuperior());
        }

        const CModelMesh* CHierarchicalModelPrimitive::GetReadOnlyMesh() const
        {
            return dynamic_cast<const CModelMesh*>(GetReadOnlySuperior());
        }

        void CHierarchicalModelPrimitive::AddDensitySelection(const Identifier QueryId, const real Density)
        {
            m_DensitySelections.AddElement(new CDensitySelection(QueryId, Density), false);
        }

        void CHierarchicalModelPrimitive::ClearDensitySelections()
        {
            if (m_DensitySelections.size())
            {
                TModelList<CDensitySelection*>::const_iterator EndDensitySelections = m_DensitySelections.end();
                for (TModelList<CDensitySelection*>::const_iterator ppDensitySelection = m_DensitySelections.begin(); ppDensitySelection != EndDensitySelections; ++ppDensitySelection)
                {
                    delete *ppDensitySelection;
                }
                m_DensitySelections.clear();
            }
        }

        bool CHierarchicalModelPrimitive::HasDensitySelections() const
        {
            return m_DensitySelections.size();
        }

        uint CHierarchicalModelPrimitive::GetTotalDensitySelections() const
        {
            return m_DensitySelections.size();
        }

        const TModelList<CDensitySelection*>& CHierarchicalModelPrimitive::GetDensitySelections(const bool Sorted)
        {
            if (Sorted)
            {
                m_DensitySelections.sort(SortDensitySelectionByDensityDownwards);
            }
            return m_DensitySelections;
        }

        const CDensitySelection* CHierarchicalModelPrimitive::GetMaximalDensitySelection() const
        {
            if (m_DensitySelections.size())
            {
                CDensitySelection* pDensitySelection = m_DensitySelections.front();
                real MaximalDensity = pDensitySelection->GetDensity();
                TModelList<CDensitySelection*>::const_iterator EndDensitySelections = m_DensitySelections.end();
                for (TModelList<CDensitySelection*>::const_iterator ppDensitySelection = ++m_DensitySelections.begin(); ppDensitySelection != EndDensitySelections; ++ppDensitySelection)
                    if ((*ppDensitySelection)->GetDensity() > MaximalDensity)
                    {
                        pDensitySelection = *ppDensitySelection;
                        MaximalDensity = pDensitySelection->GetDensity();
                    }
                return pDensitySelection;
            }
            return nullptr;
        }

        const CDensitySelection* CHierarchicalModelPrimitive::GetMinimalDensitySelection() const
        {
            if (m_DensitySelections.size())
            {
                CDensitySelection* pDensitySelection = m_DensitySelections.front();
                real MinimalDensity = pDensitySelection->GetDensity();
                TModelList<CDensitySelection*>::const_iterator EndDensitySelections = m_DensitySelections.end();
                for (TModelList<CDensitySelection*>::const_iterator ppDensitySelection = ++m_DensitySelections.begin(); ppDensitySelection != EndDensitySelections; ++ppDensitySelection)
                    if ((*ppDensitySelection)->GetDensity() < MinimalDensity)
                    {
                        pDensitySelection = *ppDensitySelection;
                        MinimalDensity = pDensitySelection->GetDensity();
                    }
                return pDensitySelection;
            }
            return nullptr;
        }

        real CHierarchicalModelPrimitive::GetDisjunctedDensity() const
        {
            if (m_DensitySelections.size())
            {
                real Density = m_DensitySelections.front()->GetDensity();
                TModelList<CDensitySelection*>::const_iterator EndDensitySelections = m_DensitySelections.end();
                for (TModelList<CDensitySelection*>::const_iterator ppDensitySelection = ++m_DensitySelections.begin(); ppDensitySelection != EndDensitySelections; ++ppDensitySelection)
                {
                    Density += (*ppDensitySelection)->GetDensity();
                }
                return Density;
            }
            return _REAL_ZERO_;
        }

        real CHierarchicalModelPrimitive::GetConjunctedDensity() const
        {
            if (m_DensitySelections.size())
            {
                real Density = m_DensitySelections.front()->GetDensity();
                TModelList<CDensitySelection*>::const_iterator EndDensitySelections = m_DensitySelections.end();
                for (TModelList<CDensitySelection*>::const_iterator ppDensitySelection = ++m_DensitySelections.begin(); ppDensitySelection != EndDensitySelections; ++ppDensitySelection)
                {
                    Density *= (*ppDensitySelection)->GetDensity();
                }
                return Density;
            }
            return _REAL_ZERO_;
        }

        bool CHierarchicalModelPrimitive::GetDensityByQueryId(const Identifier QueryId, real& Density) const
        {
            if (m_DensitySelections.size())
            {
                TModelList<CDensitySelection*>::const_iterator EndDensitySelections = m_DensitySelections.end();
                for (TModelList<CDensitySelection*>::const_iterator ppDensitySelection = m_DensitySelections.begin(); ppDensitySelection != EndDensitySelections; ++ppDensitySelection)
                    if ((*ppDensitySelection)->GetQueryId() == QueryId)
                    {
                        Density = (*ppDensitySelection)->GetDensity();
                        return true;
                    }
            }
            Density = _REAL_ZERO_;
            return false;
        }

        bool CHierarchicalModelPrimitive::DeserializeAttributes(QDomElement& XmlElement)
        {
            return CHierarchicalModelElement::DeserializeAttributes(XmlElement);
        }

        void CHierarchicalModelPrimitive::SerializeAttributes(QDomElement& XmlElement) const
        {
            CHierarchicalModelElement::SerializeAttributes(XmlElement);
        }

        bool CHierarchicalModelPrimitive::SortDensitySelectionByDensityDownwards(const CDensitySelection* plhs, const CDensitySelection* prhs)
        {
            return plhs->GetDensity() > prhs->GetDensity();
        }
    }
}
#endif// _EVP_USE_MODEL_SPACE
