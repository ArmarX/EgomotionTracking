/*
 * ModelMultipleMesh.cpp
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#include "ModelMultipleMesh.h"
#include "ModelMesh.h"
#include "ModelMultipleFace.h"
#include "ModelFace.h"
#include "ModelEdge.h"
#include "ModelVertex.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        CModelMultipleMesh::CModelMultipleMesh(CHierarchicalModelElement* pSuperior) :
            CHierarchicalModelComposition(CModelElement::eMultiMesh, pSuperior)
        {
        }

        CModelMultipleMesh::CModelMultipleMesh(CHierarchicalModelElement* pSuperior, const Identifier IntanceId) :
            CHierarchicalModelComposition(CModelElement::eMultiMesh, pSuperior, IntanceId)
        {
        }

        CModelMultipleMesh::~CModelMultipleMesh()
        {
            ClearElements();
        }

        void CModelMultipleMesh::ClearElements()
        {
            m_ModelEdgesLenghtIndexer.ClearEdges();
            m_ModelEdgesApertureIndexer.ClearEdges();
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                delete *ppModelMesh;
            }
            m_Meshes.clear();
        }

        bool CModelMultipleMesh::AddMeshes(CModelMesh* pModelMesh, const bool CheckUnique)
        {
            return m_Meshes.AddElement(pModelMesh, CheckUnique);
        }

        const THierarchicalModelElementList<CModelMesh*>& CModelMultipleMesh::GetMeshes() const
        {
            return m_Meshes;
        }

        uint CModelMultipleMesh::GetTotalPrimitives() const
        {
            uint TotalPrimitives = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalPrimitives += (*ppModelMesh)->GetTotalPrimitives();
            }
            return TotalPrimitives;
        }

        uint CModelMultipleMesh::GetTotalVertices() const
        {
            uint TotalVertices = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalVertices += (*ppModelMesh)->GetReadOnlyVeritices().size();
            }
            return TotalVertices;
        }

        uint CModelMultipleMesh::GetTotalVisibleVertices() const
        {
            uint TotalVisibleVertices = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalVisibleVertices += (*ppModelMesh)->GetReadOnlyVeritices().GetTotalVisibleElements();
            }
            return TotalVisibleVertices;
        }

        uint CModelMultipleMesh::GetTotalEnabledVertices() const
        {
            uint TotalEnabledVertices = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalEnabledVertices += (*ppModelMesh)->GetReadOnlyVeritices().GetTotalEnabledElements();
            }
            return TotalEnabledVertices;
        }

        uint CModelMultipleMesh::GetTotalIndexableVertices() const
        {
            uint TotalIndexableVertices = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalIndexableVertices += (*ppModelMesh)->GetReadOnlyVeritices().GetTotalIndexableElements();
            }
            return TotalIndexableVertices;
        }

        uint CModelMultipleMesh::GetTotalEdges() const
        {
            uint TotalEdges = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalEdges += (*ppModelMesh)->GetReadOnlyEdges().size();
            }
            return TotalEdges;
        }

        uint CModelMultipleMesh::GetTotalVisibleEdges() const
        {
            uint TotalVisibleEdges = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalVisibleEdges += (*ppModelMesh)->GetReadOnlyEdges().GetTotalVisibleElements();
            }
            return TotalVisibleEdges;
        }

        uint CModelMultipleMesh::GetTotalEnabledEdges() const
        {
            uint TotalEnabledEdges = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalEnabledEdges += (*ppModelMesh)->GetReadOnlyEdges().GetTotalEnabledElements();
            }
            return TotalEnabledEdges;
        }

        uint CModelMultipleMesh::GetTotalIndexableEdges() const
        {
            uint TotalIndexableEdges = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalIndexableEdges += (*ppModelMesh)->GetReadOnlyEdges().GetTotalIndexableElements();
            }
            return TotalIndexableEdges;
        }

        uint CModelMultipleMesh::GetTotalFaces() const
        {
            uint TotalFaces = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalFaces += (*ppModelMesh)->GetReadOnlyFaces().size();
            }
            return TotalFaces;
        }

        uint CModelMultipleMesh::GetTotalVisibleFaces() const
        {
            uint TotalVisibleFaces = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalVisibleFaces += (*ppModelMesh)->GetReadOnlyFaces().GetTotalVisibleElements();
            }
            return TotalVisibleFaces;
        }

        uint CModelMultipleMesh::GetTotalEnabledFaces() const
        {
            uint TotalEnabledeFaces = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalEnabledeFaces += (*ppModelMesh)->GetReadOnlyFaces().GetTotalEnabledElements();
            }
            return TotalEnabledeFaces;
        }

        uint CModelMultipleMesh::GetTotalIndexableFaces() const
        {
            uint TotalIndexableFaces = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalIndexableFaces += (*ppModelMesh)->GetReadOnlyFaces().GetTotalIndexableElements();
            }
            return TotalIndexableFaces;
        }

        uint CModelMultipleMesh::GetTotalMultiFaces() const
        {
            uint TotalMultiFaces = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalMultiFaces += (*ppModelMesh)->GetReadOnlyMultipleFaces().GetTotalElements();
            }
            return TotalMultiFaces;
        }

        uint CModelMultipleMesh::GetTotalVisibleMultiFaces() const
        {
            uint TotalVisibleMultiFaces = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalVisibleMultiFaces += (*ppModelMesh)->GetReadOnlyMultipleFaces().GetTotalVisibleElements();
            }
            return TotalVisibleMultiFaces;
        }

        uint CModelMultipleMesh::GetTotalEnabledMultiFaces() const
        {
            uint TotalEnabledeMultiFaces = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalEnabledeMultiFaces += (*ppModelMesh)->GetReadOnlyMultipleFaces().GetTotalEnabledElements();
            }
            return TotalEnabledeMultiFaces;
        }

        uint CModelMultipleMesh::GetTotalIndexableMultiFaces() const
        {
            uint TotalIndexableMultiFaces = 0;
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
            {
                TotalIndexableMultiFaces += (*ppModelMesh)->GetReadOnlyMultipleFaces().GetTotalIndexableElements();
            }
            return TotalIndexableMultiFaces;
        }

        void CModelMultipleMesh::ApplyTransformation(const EVP::Mathematics::_4D::CMatrix4D& Transformation)
        {
            THierarchicalModelElementList<CModelMesh*>::iterator EndMeshes = m_Meshes.end();
            if (m_Meshes.size() > 0)
            {
                for (THierarchicalModelElementList<CModelMesh*>::iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
                {
                    (*ppModelMesh)->ApplyTransformation(Transformation);
                }
            }
        }

        uint CModelMultipleMesh::GetTotalMeshes() const
        {
            return m_Meshes.size();
        }

        uint CModelMultipleMesh::GetTotalVisibleMeshes() const
        {
            return m_Meshes.GetTotalVisibleElements();
        }

        uint CModelMultipleMesh::GetTotalEnabledMeshes() const
        {
            return m_Meshes.GetTotalEnabledElements();
        }

        uint CModelMultipleMesh::GetTotalIndexableMeshes() const
        {
            return m_Meshes.GetTotalIndexableElements();
        }

        bool CModelMultipleMesh::LoadIndexers(const uint DiscretePDFSamples)
        {
            if (m_Meshes.size())
            {
                m_ModelEdgesLenghtIndexer.ClearEdges();
                m_ModelEdgesApertureIndexer.ClearEdges();
                THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
                {
                    m_ModelEdgesLenghtIndexer.AddEdgesFromMesh(*ppModelMesh, true, false);
                }
                m_ModelEdgesApertureIndexer.SetEdgeArray(m_ModelEdgesLenghtIndexer.GetEdgeArray());
                m_ModelEdgesLenghtIndexer.Indexing(CModelEdgesIndexer::eLenght, true, DiscretePDFSamples);
                m_ModelEdgesApertureIndexer.Indexing(CModelEdgesIndexer::eAperture, true, DiscretePDFSamples);
                return true;
            }
            return false;
        }

        bool CModelMultipleMesh::LoadIndexers(const real MaximalLength, const real MinimalLength, const real MaximalAperture, const real MinimalAperture, const real MaximalArea, const real MinimalArea, const uint DiscretePDFSamples)
        {
            if (m_Meshes.size())
            {
                m_ModelEdgesLenghtIndexer.ClearEdges();
                m_ModelEdgesApertureIndexer.ClearEdges();
                THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = m_Meshes.end();
                for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
                    if ((*ppModelMesh)->UpdateIndexability(MaximalLength, MinimalLength, MaximalAperture, MinimalAperture, MaximalArea, MinimalArea))
                    {
                        m_ModelEdgesLenghtIndexer.AddEdgesFromMesh(*ppModelMesh, true);
                    }
                m_ModelEdgesApertureIndexer.SetEdgeArray(m_ModelEdgesLenghtIndexer.GetEdgeArray());
                m_ModelEdgesLenghtIndexer.Indexing(CModelEdgesIndexer::eLenght, true, DiscretePDFSamples);
                m_ModelEdgesApertureIndexer.Indexing(CModelEdgesIndexer::eAperture, true, DiscretePDFSamples);
                return true;
            }
            return false;
        }

        CModelEdgesIndexer* CModelMultipleMesh::GetModelEdgesLenghtIndexer()
        {
            return &m_ModelEdgesLenghtIndexer;
        }

        CModelEdgesIndexer* CModelMultipleMesh::GetModelEdgesApertureIndexer()
        {
            return &m_ModelEdgesApertureIndexer;
        }

        bool CModelMultipleMesh::Deserialize(QDomElement& XmlElement)
        {
            if (XmlElement.tagName() == "MultipleMesh")
            {
                ClearElements();
                DeserializeAttributes(XmlElement);
                QDomNode XmlModelMesh = XmlElement.firstChild();
                while (!XmlModelMesh.isNull())
                {
                    QDomElement XmlModelMeshElement = XmlModelMesh.toElement();
                    CModelMesh* pModelMesh = new CModelMesh(this, 0);
                    if (pModelMesh->Deserialize(XmlModelMeshElement))
                    {
                        AddMeshes(pModelMesh, false);
                    }
                    XmlModelMesh = XmlModelMesh.nextSibling();
                }
                return true;
            }
            return false;
        }

        QDomElement CModelMultipleMesh::Serialize(QDomDocument& XmlDocument) const
        {
            QDomElement XmlElement = XmlDocument.createElement("MultipleMesh");
            SerializeAttributes(XmlElement);
            THierarchicalModelElementList<CModelMesh*>::const_iterator EndModelMeshes = m_Meshes.end();
            for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = m_Meshes.begin(); ppModelMesh != EndModelMeshes; ++ppModelMesh)
            {
                XmlElement.appendChild((*ppModelMesh)->Serialize(XmlDocument));
                //break;
            }
            return XmlElement;
        }

        bool CModelMultipleMesh::DeserializeAttributes(QDomElement& XmlElement)
        {
            return CHierarchicalModelElement::DeserializeAttributes(XmlElement);
        }

        void CModelMultipleMesh::SerializeAttributes(QDomElement& XmlElement) const
        {
            CHierarchicalModelElement::SerializeAttributes(XmlElement);
        }
    }
}
#endif // _EVP_USE_MODEL_SPACE
