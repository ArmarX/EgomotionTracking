/*
 * UnidimensionalPDF.h
 *
 *  Created on: 24.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "../GlobalSettings.h"

namespace EVP
{
    namespace ModelSpace
    {
        class CUnidimensionalPDF
        {
        public:

            CUnidimensionalPDF();
            ~CUnidimensionalPDF();

            void Clear();
            void SetTotalSamples(const uint TotalSamples, const real MaximalCriterion, const real MinimalCriterion);
            uint GetTotalSamples() const;
            real* GetWritablePDF();
            real* GetWritableAccumulatedPDF();
            real* GetWritableDensityDistribution();
            const real* GetReadOnlyPDF() const;
            const real* GetReadOnlyAccumulatedPDF() const;
            const real* GetReadOnlyDensityDistribution() const;
            void Normalize(const real TotalAccumulatorDensity, const real MaximalAccumulatorDensity);
            real GetDensity(const uint Index) const;
            real GetIntervalProbability(const uint IndexA, const uint IndexB) const;

        protected:

            uint m_TotalSamples;
            real* m_pPDF;
            real* m_pAccumulatedPDF;
            real* m_pDensityDistribution;
            real m_MaximalCriterion;
            real m_MinimalCriterion;
        };
    }
}

