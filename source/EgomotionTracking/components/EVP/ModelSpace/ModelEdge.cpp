/*
 * ModelEdge.cpp
 *
 *  Created on: 23.11.2011
 *      Author: gonzalez
 */

#include "ModelFace.h"
#include "ModelEdge.h"
#include "ModelVertex.h"
#include "ModelMultipleFace.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        CModelEdge::CModelEdge(CModelVertex* pModelVertexA, CModelVertex* pModelVertexB, CModelMesh* pModelMesh) :
            CHierarchicalModelPrimitive(CModelElement::eEdge, pModelMesh), m_pModelVertexA(pModelVertexA), m_pModelVertexB(pModelVertexB), m_pFaceAlpha(nullptr), m_pFaceBeta(nullptr), m_Direction(), m_Length(_REAL_ZERO_)
        {
            pModelMesh->AddEdge(this, false);
            m_pModelVertexA->AddEdge(this, false);
            m_pModelVertexB->AddEdge(this, false);

            m_Direction = m_pModelVertexB->GetPoint() - m_pModelVertexA->GetPoint();
            m_Length = m_Direction.Normalize();
        }

        CModelEdge::CModelEdge(CModelMesh* pModelMesh, const Identifier IntanceId) :
            CHierarchicalModelPrimitive(CModelElement::eEdge, pModelMesh, IntanceId), m_pModelVertexA(nullptr), m_pModelVertexB(nullptr), m_pFaceAlpha(nullptr), m_pFaceBeta(nullptr), m_Direction(), m_Length(_REAL_ZERO_)
        {
        }

        CModelEdge::~CModelEdge()
        {
            ClearElements();
        }

        void CModelEdge::ClearElements()
        {
            m_pModelVertexA = nullptr;
            m_pModelVertexB = nullptr;
            m_pFaceAlpha = nullptr;
            m_pFaceBeta = nullptr;
            m_Direction = Mathematics::_3D::CVector3D::s_Point_At_Origin_3D;
            m_Length = _REAL_ZERO_;
        }

        CModelVertex* CModelEdge::GetWritableVertexA()
        {
            return m_pModelVertexA;
        }

        CModelVertex* CModelEdge::GetWritableVertexB()
        {
            return m_pModelVertexB;
        }

        CModelVertex* CModelEdge::GetWritableVertexComplement(const CModelVertex* pModelVertex)
        {
            if (pModelVertex)
            {
                if (pModelVertex == m_pModelVertexA)
                {
                    return m_pModelVertexB;
                }
                if (pModelVertex == m_pModelVertexB)
                {
                    return m_pModelVertexA;
                }
            }
            return nullptr;
        }

        const CModelVertex* CModelEdge::GetReadOnlyVertexA() const
        {
            return m_pModelVertexA;
        }

        const CModelVertex* CModelEdge::GetReadOnlyVertexB() const
        {
            return m_pModelVertexB;
        }

        const CModelVertex* CModelEdge::GetReadOnlyVertexComplement(const CModelVertex* pModelVertex) const
        {
            //XXX REDUNDANT... besser: return GetWritableVertexComplement
            if (pModelVertex)
            {
                if (pModelVertex == m_pModelVertexA)
                {
                    return m_pModelVertexB;
                }
                if (pModelVertex == m_pModelVertexB)
                {
                    return m_pModelVertexA;
                }
            }
            return nullptr;
        }

        void CModelEdge::SetFace(CModelFace* pModelFace)
        {
            if (pModelFace && (!(m_pFaceAlpha && m_pFaceBeta)))
            {
                if (m_pFaceAlpha)
                {
                    m_pFaceBeta = pModelFace;
                }
                else
                {
                    m_pFaceAlpha = pModelFace;
                }
            }
        }

        CModelFace* CModelEdge::GetWritableFaceAlpha()
        {
            return m_pFaceAlpha;
        }

        CModelFace* CModelEdge::GetWritableFaceBeta()
        {
            return m_pFaceBeta;
        }

        bool CModelEdge::HasBothFaces() const
        {
            return (m_pFaceAlpha && m_pFaceBeta);
        }

        bool CModelEdge::IsFaceRedundant() const
        {
            return (m_pFaceAlpha && m_pFaceBeta && m_pFaceAlpha->GetMultipleFace() && (m_pFaceAlpha->GetMultipleFace() == m_pFaceBeta->GetMultipleFace()));
        }

        bool CModelEdge::IsLinking(const CModelVertex* pModelVertex) const
        {
            return pModelVertex && ((pModelVertex == m_pModelVertexA) || (pModelVertex == m_pModelVertexB));
        }

        bool CModelEdge::IsLinking(const CModelVertex* pModelVertexAlpha, const CModelVertex* pModelVertexBeta) const
        {
            return (pModelVertexAlpha && pModelVertexBeta && (pModelVertexAlpha != pModelVertexBeta) && (((pModelVertexAlpha == m_pModelVertexA) && (pModelVertexBeta == m_pModelVertexB)) || ((pModelVertexAlpha == m_pModelVertexB) && (pModelVertexBeta == m_pModelVertexA))));
        }

        Mathematics::_3D::CVector3D CModelEdge::GetMidPoint() const
        {
            return Mathematics::_3D::CVector3D::CreateMidPoint(m_pModelVertexA->GetPoint(), m_pModelVertexB->GetPoint());
        }

        const Mathematics::_3D::CVector3D& CModelEdge::GetDirection() const
        {
            return m_Direction;
        }

        real CModelEdge::GetLength() const
        {
            return m_Length;
        }

        real CModelEdge::GetAperture() const
        {
            return (m_pFaceAlpha && m_pFaceBeta) ? m_pFaceAlpha->GetNormal().ScalarProduct(m_pFaceBeta->GetNormal()) : _REAL_ZERO_;
        }

        real CModelEdge::GetNormalizedAperture() const
        {
            return (m_pFaceAlpha && m_pFaceBeta) ? RealArcCosinus(m_pFaceAlpha->GetNormal().ScalarProduct(m_pFaceBeta->GetNormal())) / _REAL_PI_ : _REAL_ZERO_;
        }

        real CModelEdge::GetMaximalAdjacentArea() const
        {
            if (m_pFaceAlpha || m_pFaceBeta)
            {
                if (m_pFaceBeta && m_pFaceAlpha)
                {
                    return TMax((m_pFaceAlpha->GetMultipleFace() ? m_pFaceAlpha->GetMultipleFace()->GetArea() : m_pFaceAlpha->GetArea()), (m_pFaceBeta->GetMultipleFace() ? m_pFaceBeta->GetMultipleFace()->GetArea() : m_pFaceBeta->GetArea()));
                }
                if (m_pFaceAlpha)
                {
                    return m_pFaceAlpha->GetMultipleFace() ? m_pFaceAlpha->GetMultipleFace()->GetArea() : m_pFaceAlpha->GetArea();
                }
                return m_pFaceBeta->GetMultipleFace() ? m_pFaceBeta->GetMultipleFace()->GetArea() : m_pFaceBeta->GetArea();
            }
            return _REAL_ZERO_;
        }

        bool CModelEdge::HasAdjacentAreaInRange(const real MinimalArea, const real MaximalArea) const
        {
            if (m_pFaceAlpha || m_pFaceBeta)
            {
                if (m_pFaceBeta)
                {
                    if (m_pFaceAlpha->GetMultipleFace())
                    {
                        const real Area = m_pFaceAlpha->GetMultipleFace()->GetArea();
                        if ((Area >= MinimalArea) && (Area <= MaximalArea))
                        {
                            return true;
                        }
                    }
                    const real Area = m_pFaceAlpha->GetArea();
                    if ((Area >= MinimalArea) && (Area <= MaximalArea))
                    {
                        return true;
                    }
                }
                if (m_pFaceBeta)
                {
                    if (m_pFaceBeta->GetMultipleFace())
                    {
                        const real Area = m_pFaceBeta->GetMultipleFace()->GetArea();
                        if ((Area >= MinimalArea) && (Area <= MaximalArea))
                        {
                            return true;
                        }
                    }
                    const real Area = m_pFaceBeta->GetArea();
                    if ((Area >= MinimalArea) && (Area <= MaximalArea))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        real CModelEdge::GetSelectionDensityAdjacentAreaInRange(const real MinimalArea, const real MaximalArea, const real MinimalScopeArea, const real MaximalScopeArea, const real ExponenFactor) const
        {
            if (m_pFaceAlpha || m_pFaceBeta)
            {
                real MinimalDeviation = _REAL_MAX_;
                if (m_pFaceBeta)
                {
                    if (m_pFaceAlpha->GetMultipleFace())
                    {
                        const real Area = m_pFaceAlpha->GetMultipleFace()->GetArea();
                        if ((Area >= MinimalArea) && (Area <= MaximalArea))
                        {
                            return _REAL_ONE_;
                        }
                        if ((Area >= MinimalScopeArea) && (Area <= MaximalScopeArea))
                        {
                            MinimalDeviation = TMin(Area - MinimalScopeArea, MaximalScopeArea - Area);
                        }
                    }
                    const real Area = m_pFaceAlpha->GetArea();
                    if ((Area >= MinimalArea) && (Area <= MaximalArea))
                    {
                        return _REAL_ONE_;
                    }
                    if ((Area >= MinimalScopeArea) && (Area <= MaximalScopeArea))
                    {
                        MinimalDeviation = TMin(TMin(Area - MinimalScopeArea, MaximalScopeArea - Area), MinimalDeviation);
                    }
                }
                if (m_pFaceBeta)
                {
                    if (m_pFaceBeta->GetMultipleFace())
                    {
                        const real Area = m_pFaceBeta->GetMultipleFace()->GetArea();
                        if ((Area >= MinimalArea) && (Area <= MaximalArea))
                        {
                            return _REAL_ONE_;
                        }
                        if ((Area >= MinimalScopeArea) && (Area <= MaximalScopeArea))
                        {
                            MinimalDeviation = TMin(TMin(Area - MinimalScopeArea, MaximalScopeArea - Area), MinimalDeviation);
                        }
                    }
                    const real Area = m_pFaceBeta->GetArea();
                    if ((Area >= MinimalArea) && (Area <= MaximalArea))
                    {
                        return _REAL_ONE_;
                    }
                    if ((Area >= MinimalScopeArea) && (Area <= MaximalScopeArea))
                    {
                        MinimalDeviation = TMin(TMin(Area - MinimalScopeArea, MaximalScopeArea - Area), MinimalDeviation);
                    }
                }
                return RealExp(MinimalDeviation * MinimalDeviation * ExponenFactor);
            }
            return _REAL_ZERO_;
        }

        bool CModelEdge::Deserialize(QDomElement& XmlElement)
        {
            if (XmlElement.tagName() == "Edge")
            {
                ClearElements();
                return DeserializeAttributes(XmlElement);
            }
            return false;
        }

        QDomElement CModelEdge::Serialize(QDomDocument& XmlDocument) const
        {
            QDomElement XmlElement = XmlDocument.createElement("Edge");
            SerializeAttributes(XmlElement);
            return XmlElement;
        }

        bool CModelEdge::DeserializeAttributes(QDomElement& XmlElement)
        {
            if (CHierarchicalModelPrimitive::DeserializeAttributes(XmlElement) && XmlElement.hasAttribute("VertexAId") && XmlElement.hasAttribute("VertexBId"))
            {
                THierarchicalModelElementList<CModelVertex*>& MeshVeritices = GetWritableMesh()->GetWritableVeritices();
                m_pModelVertexA = MeshVeritices.GetElementByInstanceId(Identifier(XmlElement.attribute("VertexA").toUInt()));
                m_pModelVertexB = MeshVeritices.GetElementByInstanceId(Identifier(XmlElement.attribute("VertexB").toUInt()));
                if (m_pModelVertexA && m_pModelVertexB)
                {
                    m_pModelVertexA->AddEdge(this, true);
                    m_pModelVertexB->AddEdge(this, true);
                    m_Direction = m_pModelVertexB->GetPoint() - m_pModelVertexA->GetPoint();
                    m_Length = m_Direction.Normalize();
                    return true;
                }
            }
            return false;
        }

        void CModelEdge::SerializeAttributes(QDomElement& XmlElement) const
        {
            CHierarchicalModelPrimitive::SerializeAttributes(XmlElement);
            XmlElement.setAttribute("VertexA", m_pModelVertexA->GetInstanceId());
            XmlElement.setAttribute("VertexB", m_pModelVertexB->GetInstanceId());
        }

        QDomElement CModelEdge::SerializeReference(QDomDocument& XmlDocument) const
        {
            QDomElement XmlElement = XmlDocument.createElement("Edge");
            XmlElement.setAttribute("Id", GetInstanceId());
            return XmlElement;
        }

    }
}
#endif //_EVP_USE_MODEL_SPACE
