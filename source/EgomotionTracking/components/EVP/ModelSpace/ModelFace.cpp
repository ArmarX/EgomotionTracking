/*
 * ModelFace.cpp
 *
 *  Created on: 23.11.2011
 *      Author: gonzalez
 */

#include "ModelFace.h"
#include "ModelEdge.h"
#include "ModelVertex.h"
#include "ModelMultipleFace.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        CModelFace::CModelFace(CModelMesh* pModelMesh, CModelVertex* pModelVertexA, CModelVertex* pModelVertexB, CModelVertex* pModelVertexC, const Mathematics::_3D::CVector3D& NormalA, const Mathematics::_3D::CVector3D& NormalB, const Mathematics::_3D::CVector3D& NormalC) :
            CHierarchicalModelPrimitive(CModelElement::eFace, pModelMesh), m_pModelMultipleFace(nullptr), m_OrientedVertexContainerA(pModelVertexA, NormalA), m_OrientedVertexContainerB(pModelVertexB, NormalB), m_OrientedVertexContainerC(pModelVertexC, NormalC), m_Normal(), m_Centeroid(), m_Area(_REAL_ZERO_)
        {
            pModelMesh->AddFace(this, false);

            CModelEdge* pModelEdgeAB = pModelVertexA->GetWritableLinkingEdge(pModelVertexB);
            CModelEdge* pModelEdgeBC = pModelVertexB->GetWritableLinkingEdge(pModelVertexC);
            CModelEdge* pModelEdgeCA = pModelVertexC->GetWritableLinkingEdge(pModelVertexA);

            m_Edges.AddElement(pModelEdgeAB, false);
            m_Edges.AddElement(pModelEdgeBC, false);
            m_Edges.AddElement(pModelEdgeCA, false);

            pModelEdgeAB->SetFace(this);
            pModelEdgeBC->SetFace(this);
            pModelEdgeCA->SetFace(this);

            m_OrientedVertexContainerA.AddFace(this);
            m_OrientedVertexContainerB.AddFace(this);
            m_OrientedVertexContainerC.AddFace(this);

            ComputeArea(pModelEdgeAB->GetLength(), pModelEdgeBC->GetLength(), pModelEdgeCA->GetLength());
            ComputeCentroid(pModelVertexA->GetPoint(), pModelVertexB->GetPoint(), pModelVertexC->GetPoint());
            ComputeNormal(pModelEdgeAB->GetDirection(), pModelEdgeBC->GetDirection(), NormalA, NormalB, NormalC);
        }

        CModelFace::CModelFace(CModelMesh* pModelMesh, const Identifier IntanceId) :
            CHierarchicalModelPrimitive(CModelElement::eFace, pModelMesh, IntanceId), m_pModelMultipleFace(nullptr), m_OrientedVertexContainerA(), m_OrientedVertexContainerB(), m_OrientedVertexContainerC(), m_Normal(), m_Centeroid(), m_Area(_REAL_ZERO_)
        {
        }

        CModelFace::~CModelFace()
        {
            ClearElements();
        }

        void CModelFace::ClearElements()
        {
            m_pModelMultipleFace = nullptr;
            CModelOrientedVertexContainer* OrientedVertexContainers[3] = { &m_OrientedVertexContainerA, &m_OrientedVertexContainerB, &m_OrientedVertexContainerC };
            for (uint i = 0; i < 3; ++i)
            {
                OrientedVertexContainers[i]->Clear();
            }
            m_Edges.clear();
            m_Centeroid = Mathematics::_3D::CVector3D::s_Point_At_Origin_3D;
            m_Area = _REAL_ZERO_;
        }

        void CModelFace::SetMultipleFace(const CModelMultipleFace* pModelMultipleFace)
        {
            m_pModelMultipleFace = pModelMultipleFace;
        }

        const CModelMultipleFace* CModelFace::GetMultipleFace() const
        {
            return m_pModelMultipleFace;
        }

        THierarchicalModelElementList<const CModelOrientedVertexContainer*> CModelFace::GetOrientedVertices() const
        {
            THierarchicalModelElementList<const CModelOrientedVertexContainer*> OrientedVertices;
            OrientedVertices.push_back(&m_OrientedVertexContainerA);
            OrientedVertices.push_back(&m_OrientedVertexContainerB);
            OrientedVertices.push_back(&m_OrientedVertexContainerC);
            return OrientedVertices;
        }

        const THierarchicalModelElementList<CModelEdge*>& CModelFace::GetEdges() const
        {
            return m_Edges;
        }

        const Mathematics::_3D::CVector3D& CModelFace::GetNormal() const
        {
            return m_Normal;
        }

        const Mathematics::_3D::CVector3D& CModelFace::GetCenteroid() const
        {
            return m_Centeroid;
        }

        real CModelFace::GetArea() const
        {
            return m_Area;
        }

        bool CModelFace::Deserialize(QDomElement& XmlElement)
        {
            if (XmlElement.tagName() == "Face")
            {
                ClearElements();
                if (DeserializeAttributes(XmlElement))
                {
#ifdef _EVP_USE_QT3
                    g_ConsoleStringOutput << XmlElement.text().latin1() << g_EndLine;
#else
                    g_ConsoleStringOutput << XmlElement.text().toLatin1().data() << g_EndLine;
#endif
                    QDomNodeList XmlOrientedVertices = XmlElement.firstChild().toElement().childNodes();
                    THierarchicalModelElementList<CModelVertex*>& MeshVeritices = GetWritableMesh()->GetWritableVeritices();
                    bool Result = true;
                    CModelOrientedVertexContainer* OrientedVertexContainers[3] = { &m_OrientedVertexContainerA, &m_OrientedVertexContainerB, &m_OrientedVertexContainerC };
                    for (uint i = 0; i < 3; ++i)
                    {
                        QDomElement XmlOrientedVertexContainer = XmlOrientedVertices.item(i).toElement();
                        Result &= OrientedVertexContainers[i]->Deserialize(XmlOrientedVertexContainer, MeshVeritices);
                    }
                    if (Result)
                    {
                        CModelVertex* pModelVertexA = m_OrientedVertexContainerA.GetWritableModelVertex();
                        CModelVertex* pModelVertexB = m_OrientedVertexContainerB.GetWritableModelVertex();
                        CModelVertex* pModelVertexC = m_OrientedVertexContainerC.GetWritableModelVertex();
                        CModelEdge* pModelEdgeAB = pModelVertexA->GetWritableLinkingEdge(pModelVertexB);
                        CModelEdge* pModelEdgeBC = pModelVertexB->GetWritableLinkingEdge(pModelVertexC);
                        CModelEdge* pModelEdgeCA = pModelVertexC->GetWritableLinkingEdge(pModelVertexA);
                        m_Edges.AddElement(pModelEdgeAB, false);
                        m_Edges.AddElement(pModelEdgeBC, false);
                        m_Edges.AddElement(pModelEdgeCA, false);
                        pModelEdgeAB->SetFace(this);
                        pModelEdgeBC->SetFace(this);
                        pModelEdgeCA->SetFace(this);
                        m_OrientedVertexContainerA.AddFace(this);
                        m_OrientedVertexContainerB.AddFace(this);
                        m_OrientedVertexContainerC.AddFace(this);
                        ComputeArea(pModelEdgeAB->GetLength(), pModelEdgeBC->GetLength(), pModelEdgeCA->GetLength());
                        ComputeCentroid(pModelVertexA->GetPoint(), pModelVertexB->GetPoint(), pModelVertexC->GetPoint());
                        return Result;
                    }
                }
            }
            return false;
        }

        QDomElement CModelFace::Serialize(QDomDocument& XmlDocument) const
        {
            QDomElement XmlElement = XmlDocument.createElement("Face");
            SerializeAttributes(XmlElement);
            QDomElement XmlOrientedVertices = XmlDocument.createElement("OrientedVertices");
            XmlElement.appendChild(XmlOrientedVertices);
            const CModelOrientedVertexContainer* OrientedVertexContainers[3] = { &m_OrientedVertexContainerA, &m_OrientedVertexContainerB, &m_OrientedVertexContainerC };
            for (uint i = 0; i < 3; ++i)
            {
                XmlOrientedVertices.appendChild(OrientedVertexContainers[i]->Serialize(XmlDocument));
            }
            return XmlElement;
        }

        bool CModelFace::DeserializeAttributes(QDomElement& XmlElement)
        {
            if (CHierarchicalModelPrimitive::DeserializeAttributes(XmlElement) && XmlElement.hasAttribute("NX") && XmlElement.hasAttribute("NY") && XmlElement.hasAttribute("NZ"))
            {
                m_Normal.SetValue(real(XmlElement.attribute("NX").toDouble()), real(XmlElement.attribute("NY").toDouble()), real(XmlElement.attribute("NZ").toDouble()));
                return true;
            }
            return false;
        }

        void CModelFace::SerializeAttributes(QDomElement& XmlElement) const
        {
            CHierarchicalModelPrimitive::SerializeAttributes(XmlElement);
            XmlElement.setAttribute("NX", m_Normal.GetX());
            XmlElement.setAttribute("NY", m_Normal.GetY());
            XmlElement.setAttribute("NZ", m_Normal.GetZ());
        }

        QDomElement CModelFace::SerializeReference(QDomDocument& XmlDocument) const
        {
            QDomElement XmlElement = XmlDocument.createElement("Face");
            XmlElement.setAttribute("Id", GetInstanceId());
            return XmlElement;
        }

        void CModelFace::ComputeArea(const real A, const real B, const real C)
        {
            const real Semiperimeter = (A + B + C) * _REAL_HALF_;
            m_Area = RealSqrt(Semiperimeter * (Semiperimeter - A) * (Semiperimeter - B) * (Semiperimeter - C));
        }

        void CModelFace::ComputeCentroid(const Mathematics::_3D::CVector3D& A, const Mathematics::_3D::CVector3D& B, const Mathematics::_3D::CVector3D& C)
        {
            const Mathematics::_3D::CVector3D MidPointAB = Mathematics::_3D::CVector3D::CreateMidPoint(A, B);
            const Mathematics::_3D::CVector3D MidPointBC = Mathematics::_3D::CVector3D::CreateMidPoint(B, C);
            Mathematics::_3D::CVector3D XA, XB;
            m_Centeroid = Mathematics::_3D::Geometry::ComputeLineToLineClosestPoints(MidPointAB, Mathematics::_3D::Geometry::ComputeUnitaryDirectionVector(MidPointAB, C), MidPointBC, Mathematics::_3D::Geometry::ComputeUnitaryDirectionVector(MidPointBC, A), XA, XB) ? Mathematics::_3D::Geometry::ComputeProjectionPointToPlane(m_Normal, A, Mathematics::_3D::CVector3D::CreateMidPoint(XA, XB)) : ((A + B + C) * _REAL_THIRD_);
        }

        void CModelFace::ComputeNormal(const Mathematics::_3D::CVector3D& DirectionA, const Mathematics::_3D::CVector3D& DirectionB, const Mathematics::_3D::CVector3D& NormalA, const Mathematics::_3D::CVector3D& NormalB, const Mathematics::_3D::CVector3D& NormalC)
        {
            m_Normal = DirectionA.VectorProduct(DirectionB);
            m_Normal.Normalize();
            if ((m_Normal.ScalarProduct(NormalA) < _REAL_ZERO_) || (m_Normal.ScalarProduct(NormalB) < _REAL_ZERO_) || (m_Normal.ScalarProduct(NormalC) < _REAL_ZERO_))
            {
                m_Normal.Negate();
            }
        }
    }
}
#endif // _EVP_USE_MODEL_SPACE
