/*
 * UnidimensionalPDF.cpp
 *
 *  Created on: 24.02.2012
 *      Author: gonzalez
 */

#include "UnidimensionalPDF.h"

namespace EVP
{

    namespace ModelSpace
    {

        CUnidimensionalPDF::CUnidimensionalPDF() :
            m_TotalSamples(0), m_pPDF(nullptr), m_pAccumulatedPDF(nullptr), m_pDensityDistribution(nullptr), m_MaximalCriterion(_REAL_MIN_), m_MinimalCriterion(_REAL_MAX_)
        {
        }

        CUnidimensionalPDF::~CUnidimensionalPDF()
        {
            Clear();
        }

        void CUnidimensionalPDF::Clear()
        {
            RELEASE_ARRAY(m_pPDF);
            RELEASE_ARRAY(m_pAccumulatedPDF);
            RELEASE_ARRAY(m_pDensityDistribution);
            m_TotalSamples = 0;
        }

        void CUnidimensionalPDF::SetTotalSamples(const uint TotalSamples, const real MaximalCriterion, const real MinimalCriterion)
        {
            m_MinimalCriterion = TMin(MaximalCriterion, MinimalCriterion);
            m_MaximalCriterion = TMax(MaximalCriterion, MinimalCriterion);
            if (m_TotalSamples != TotalSamples)
            {
                Clear();
                m_TotalSamples = TotalSamples;
                const uint BlockSize = sizeof(real) * m_TotalSamples;
                m_pAccumulatedPDF = new real[m_TotalSamples];
                m_pPDF = new real[m_TotalSamples];
                m_pDensityDistribution = new real[m_TotalSamples];
                memset(m_pAccumulatedPDF, 0, BlockSize);
                memset(m_pPDF, 0, BlockSize);
                memset(m_pDensityDistribution, 0, BlockSize);
            }
        }

        uint CUnidimensionalPDF::GetTotalSamples() const
        {
            return m_TotalSamples;
        }

        real* CUnidimensionalPDF::GetWritablePDF()
        {
            return m_pPDF;
        }

        real* CUnidimensionalPDF::GetWritableAccumulatedPDF()
        {
            return m_pAccumulatedPDF;
        }

        real* CUnidimensionalPDF::GetWritableDensityDistribution()
        {
            return m_pDensityDistribution;
        }

        const real* CUnidimensionalPDF::GetReadOnlyPDF() const
        {
            return m_pPDF;
        }

        const real* CUnidimensionalPDF::GetReadOnlyAccumulatedPDF() const
        {
            return m_pAccumulatedPDF;
        }

        const real* CUnidimensionalPDF::GetReadOnlyDensityDistribution() const
        {
            return m_pDensityDistribution;
        }

        void CUnidimensionalPDF::Normalize(const real TotalAccumulatorDensity, const real MaximalAccumulatorDensity)
        {
            m_pAccumulatedPDF[0] = m_pPDF[0] = m_pDensityDistribution[0] / TotalAccumulatorDensity;
            m_pDensityDistribution[0] /= MaximalAccumulatorDensity;
            for (uint i = 1; i < m_TotalSamples; ++i)
            {
                m_pPDF[i] = m_pDensityDistribution[i] / TotalAccumulatorDensity;
                m_pAccumulatedPDF[i] = m_pAccumulatedPDF[i - 1] + m_pPDF[i];
                m_pDensityDistribution[i] /= MaximalAccumulatorDensity;
            }
        }

        real CUnidimensionalPDF::GetDensity(const uint Index) const
        {
            if (Index < m_TotalSamples)
            {
                return m_pAccumulatedPDF[Index];
            }
            return _REAL_ZERO_;
        }

        real CUnidimensionalPDF::GetIntervalProbability(const uint IndexA, const uint IndexB) const
        {
            const uint SafeIndexA = TMin(IndexA, IndexB);
            const uint SafeIndexB = TMax(IndexA, IndexB);
            if ((SafeIndexB > SafeIndexA) && (SafeIndexB < m_TotalSamples) && (SafeIndexA < m_TotalSamples))
            {
                return m_pAccumulatedPDF[SafeIndexB] - m_pAccumulatedPDF[SafeIndexA];
            }
            return _REAL_ZERO_;
        }

    }

}
