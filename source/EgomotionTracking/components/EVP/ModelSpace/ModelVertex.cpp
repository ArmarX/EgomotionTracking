/*
 * ModelVertex.cpp
 *
 *  Created on: 23.11.2011
 *      Author: gonzalez
 */

#include "ModelVertex.h"
#include "ModelEdge.h"
#include "ModelFace.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        CModelVertex::CModelVertex(const Mathematics::_3D::CVector3D& Point, CModelMesh* pModelMesh) :
            CHierarchicalModelPrimitive(CModelElement::eVertex, pModelMesh), m_Point(Point), m_IndexInMesh(0)
        {
            pModelMesh->AddVertex(this, false);
        }

        CModelVertex::CModelVertex(CModelMesh* pModelMesh, const Identifier IntanceId) :
            CHierarchicalModelPrimitive(CModelElement::eVertex, pModelMesh, IntanceId), m_Point(), m_IndexInMesh(0)
        {
        }

        CModelVertex::~CModelVertex()
        {
            ClearElements();
        }

        void CModelVertex::ClearElements()
        {
            m_Point = Mathematics::_3D::CVector3D::s_Point_At_Origin_3D;
            m_IndexInMesh = 0;
            m_Edges.clear();
            m_Faces.clear();
        }

        bool CModelVertex::AddEdge(CModelEdge* pModelEdge, const bool CheckUnique)
        {
            return m_Edges.AddElement(pModelEdge, CheckUnique);
        }

        bool CModelVertex::AddFace(CModelFace* pModelFace, const bool CheckUnique)
        {
            return m_Faces.AddElement(pModelFace, CheckUnique);
        }

        const Mathematics::_3D::CVector3D& CModelVertex::GetPoint() const
        {
            return m_Point;
        }

        const THierarchicalModelElementList<CModelEdge*>& CModelVertex::GetEdges() const
        {
            return m_Edges;
        }

        const THierarchicalModelElementList<CModelFace*>& CModelVertex::GetFaces() const
        {
            return m_Faces;
        }

        CModelEdge* CModelVertex::GetWritableLinkingEdge(const CModelVertex* pVertex)
        {
            if (pVertex && (pVertex != this) && (pVertex->GetReadOnlySuperior() == GetReadOnlySuperior()))
            {
                THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = m_Edges.end();
                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = m_Edges.begin(); ppModelEdge != EndEdges; ++ppModelEdge)
                    if ((*ppModelEdge)->IsLinking(pVertex))
                    {
                        return *ppModelEdge;
                    }
            }
            return nullptr;
        }

        const CModelEdge* CModelVertex::GetReadOnlyLinkingEdge(const CModelVertex* pVertex) const
        {
            if (pVertex && (pVertex != this) && (pVertex->GetReadOnlySuperior() == GetReadOnlySuperior()))
            {
                THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = m_Edges.end();
                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = m_Edges.begin(); ppModelEdge != EndEdges; ++ppModelEdge)
                    if ((*ppModelEdge)->IsLinking(pVertex))
                    {
                        return *ppModelEdge;
                    }
            }
            return nullptr;
        }

        void CModelVertex::SetIndexInMesh(const uint IndexInMesh)
        {
            m_IndexInMesh = IndexInMesh;
        }

        uint CModelVertex::GetIndexInMesh() const
        {
            return m_IndexInMesh;
        }

        real CModelVertex::GetDistance(const Mathematics::_3D::CVector3D& Point) const
        {
            return m_Point.GetDistance(Point);
        }

        bool CModelVertex::Deserialize(QDomElement& XmlElement)
        {
            if (XmlElement.tagName() == "Vertex")
            {
                ClearElements();
                return DeserializeAttributes(XmlElement);
            }
            return false;
        }

        QDomElement CModelVertex::Serialize(QDomDocument& XmlDocument) const
        {
            QDomElement XmlElement = XmlDocument.createElement("Vertex");
            SerializeAttributes(XmlElement);
            return XmlElement;
        }

        bool CModelVertex::DeserializeAttributes(QDomElement& XmlElement)
        {
            if (CHierarchicalModelPrimitive::DeserializeAttributes(XmlElement) && XmlElement.hasAttribute("X") && XmlElement.hasAttribute("Y") && XmlElement.hasAttribute("Z"))
            {
                m_Point.SetValue(real(XmlElement.attribute("X").toDouble()), real(XmlElement.attribute("Y").toDouble()), real(XmlElement.attribute("Z").toDouble()));
                m_IndexInMesh = XmlElement.attribute("IndexInMesh").toUInt();
                return true;
            }
            return false;
        }

        void CModelVertex::SerializeAttributes(QDomElement& XmlElement) const
        {
            CHierarchicalModelPrimitive::SerializeAttributes(XmlElement);
            XmlElement.setAttribute("X", m_Point.GetX());
            XmlElement.setAttribute("Y", m_Point.GetY());
            XmlElement.setAttribute("IndexInMesh", m_IndexInMesh);
        }

        QDomElement CModelVertex::SerializeReference(QDomDocument& XmlDocument) const
        {
            QDomElement XmlElement = XmlDocument.createElement("Vertex");
            XmlElement.setAttribute("Id", GetInstanceId());
            return XmlElement;
        }

        void CModelVertex::ApplyTransformation(const EVP::Mathematics::_4D::CMatrix4D& Transformation)
        {
            Transformation.Transform(m_Point, m_Point);
        }
    }
}
#endif // _EVP_USE_MODEL_SPACE
