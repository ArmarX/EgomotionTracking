/*
 * ModelEdgesIndexer.cpp
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#include "../Foundation/Mathematics/1D/Common1D.h"
#include "ModelEdgesIndexer.h"
#include "ModelVertex.h"
#include "ModelEdge.h"
#include "ModelMesh.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        CModelEdgesIndexer::CModelEdgesIndexer() :
            CModelPrimitiveIndexer(), m_RangeCriterion(_REAL_ZERO_), m_MaximalCriterion(_REAL_ZERO_), m_MinimalCriterion(_REAL_ZERO_)
        {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
            memset(&m_PDF, 0, sizeof(CUnidimensionalPDF));
#pragma GCC diagnostic pop
        }

        CModelEdgesIndexer::~CModelEdgesIndexer()
        {
            ClearEdges();
        }

        void CModelEdgesIndexer::ClearEdges()
        {
            if (m_EdgeArray.size())
            {
                m_RangeCriterion = m_MinimalCriterion = m_MaximalCriterion = _REAL_ZERO_;
                m_EdgeArray.clear();
                m_SelectedEdges.clear();
            }
        }

        void CModelEdgesIndexer::AddEdgesFromMesh(CModelMesh* pModelMesh, const bool CheckUnique, const bool CheckIndexability)
        {
            if (pModelMesh)
            {
                const THierarchicalModelElementList<CModelEdge*>& Edges = pModelMesh->GetReadOnlyEdges();
                if (Edges.size())
                {
                    THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = Edges.end();
                    if (CheckIndexability)
                    {
                        for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = Edges.begin(); ppModelEdge != EndEdges; ++ppModelEdge)
                            if ((*ppModelEdge)->IsIndexable())
                            {
                                m_EdgeArray.AddElement(*ppModelEdge, CheckUnique);
                            }
                    }
                    else
                        for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = Edges.begin(); ppModelEdge != EndEdges; ++ppModelEdge)
                        {
                            m_EdgeArray.AddElement(*ppModelEdge, CheckUnique);
                        }
                }
            }
        }

        real CModelEdgesIndexer::GetRangeCriterion() const
        {
            return m_RangeCriterion;
        }

        real CModelEdgesIndexer::GetMaximalCriterion() const
        {
            return m_MaximalCriterion;
        }

        real CModelEdgesIndexer::GetMinimalCriterion() const
        {
            return m_MinimalCriterion;
        }

        CModelEdgesIndexer::IndexingCriterion CModelEdgesIndexer::GetIndexingCriterion() const
        {
            return m_IndexingCriterion;
        }

        void CModelEdgesIndexer::SetEdgeArray(const TModelArray<CModelEdge*>& EdgeArray)
        {
            m_EdgeArray = EdgeArray;
        }

        const TModelArray<CModelEdge*>& CModelEdgesIndexer::GetEdgeArray() const
        {
            return m_EdgeArray;
        }

        void CModelEdgesIndexer::Indexing(const IndexingCriterion SelectedIndexingCriterion, const bool EstimatePDF, const uint DiscretePDFSamples)
        {
            m_IndexingCriterion = SelectedIndexingCriterion;
            if (m_EdgeArray.size())
            {
                switch (m_IndexingCriterion)
                {
                    case eLenght:
                        TSort(m_EdgeArray.begin(), m_EdgeArray.end(), CModelEdgesIndexer::SortEdgesByLenght);
                        m_MinimalCriterion = m_EdgeArray.front()->GetLength();
                        m_MaximalCriterion = m_EdgeArray.back()->GetLength();
                        m_RangeCriterion = m_MaximalCriterion - m_MinimalCriterion;
                        break;
                    case eAperture:
                        TSort(m_EdgeArray.begin(), m_EdgeArray.end(), CModelEdgesIndexer::SortEdgesByAperture);
                        m_MinimalCriterion = m_EdgeArray.front()->GetAperture();
                        m_MaximalCriterion = m_EdgeArray.back()->GetAperture();
                        m_RangeCriterion = m_MaximalCriterion - m_MinimalCriterion;
                        break;
                }
                if (EstimatePDF)
                {
                    EstimateGaussianPDF(DiscretePDFSamples);
                }
            }
        }

        real CModelEdgesIndexer::GetIntervalProbability(const real ValueMaximal, const real ValueMinimal)
        {
            const uint TotalSamples = m_PDF.GetTotalSamples();
            if (TotalSamples && (RealAbs(ValueMaximal - ValueMinimal) > _REAL_EPSILON_))
            {
                const uint IndexA = uint(RealCeil(real(TotalSamples - 1) * ((TMax(TMin(ValueMaximal, ValueMinimal), m_MinimalCriterion) - m_MinimalCriterion) / m_RangeCriterion)));
                const uint IndexB = uint(RealFloor(real(TotalSamples - 1) * ((TMin(TMax(ValueMaximal, ValueMinimal), m_MaximalCriterion) - m_MinimalCriterion) / m_RangeCriterion)));
                return m_PDF.GetIntervalProbability(IndexA, IndexB);
            }
            return _REAL_ZERO_;
        }

        THierarchicalModelElementList<CModelEdge*> CModelEdgesIndexer::GetEdgesBySurfaceAdjacency(const Identifier QueryId, const real MinimalArea, const real MaximalArea, const real Sigma, const real MinimalDensity, const bool SetSelected, const bool AddToSelection)
        {
            THierarchicalModelElementList<CModelEdge*> SelectedEdges;
            const real DeviationAtMinimalDensity = EVP::Mathematics::_1D::NormalDistribution::DetermineDeviationAtDensity(MinimalDensity, Sigma);
            const real SafeMinimalArea = TMax(TMin(MinimalArea, MaximalArea), _REAL_ZERO_);
            const real SafeMaximalArea = TMax(MinimalArea, MaximalArea);
            const real SafeMinimalScopeArea = TMax(SafeMinimalArea - DeviationAtMinimalDensity, _REAL_ZERO_);
            const real SafeMaximalScopeArea = SafeMaximalArea + DeviationAtMinimalDensity;
            const uint TotalEdges = m_EdgeArray.size();
            for (uint Index = 0; Index < TotalEdges; ++Index)
                if (m_EdgeArray[Index]->HasAdjacentAreaInRange(SafeMinimalScopeArea, SafeMaximalScopeArea))
                {
                    SelectedEdges.AddElement(m_EdgeArray[Index], false);
                }
            if (SelectedEdges.Sorted() && SetSelected)
            {
                const real ExponenFactor = EVP::Mathematics::_1D::NormalDistribution::DetermineExponentFactor(Sigma);
                THierarchicalModelElementList<CModelEdge*>::iterator EndSelectedEdges = SelectedEdges.end();
                if (AddToSelection)
                    for (THierarchicalModelElementList<CModelEdge*>::iterator ppModelEdge = SelectedEdges.begin(); ppModelEdge != EndSelectedEdges; ++ppModelEdge)
                    {
                        (*ppModelEdge)->AddDensitySelection(QueryId, (*ppModelEdge)->GetSelectionDensityAdjacentAreaInRange(SafeMinimalArea, SafeMaximalArea, SafeMinimalScopeArea, SafeMaximalScopeArea, ExponenFactor));
                        m_SelectedEdges.AddElement(*ppModelEdge, true);
                    }
                else
                    for (THierarchicalModelElementList<CModelEdge*>::iterator ppModelEdge = SelectedEdges.begin(); ppModelEdge != EndSelectedEdges; ++ppModelEdge)
                    {
                        (*ppModelEdge)->AddDensitySelection(QueryId, (*ppModelEdge)->GetSelectionDensityAdjacentAreaInRange(SafeMinimalArea, SafeMaximalArea, SafeMinimalScopeArea, SafeMaximalScopeArea, ExponenFactor));
                    }
            }
            return SelectedEdges;
        }

        CModelPrimitiveIndexingInterval CModelEdgesIndexer::GetIndexingIntervalBySingleValue(const Identifier QueryId, const real Value, const real Sigma, const real MinimalDensity, const bool SetSelected, const bool AddToSelection)
        {
            const real ExponenFactor = EVP::Mathematics::_1D::NormalDistribution::DetermineExponentFactor(Sigma);
            const real DeviationAtMinimalDensity = EVP::Mathematics::_1D::NormalDistribution::DetermineDeviationAtDensity(MinimalDensity, Sigma);
            const CModelPrimitiveIndexingInterval IndexingInterval = GetIndexingInterval(QueryId, Value + DeviationAtMinimalDensity, Value - DeviationAtMinimalDensity);
            if (SetSelected && (!IndexingInterval.IsEmpty()))
            {
                const bool CheckUnique = !m_SelectedEdges.size();
                const uint IndexB = IndexingInterval.GetIndexB();
                switch (m_IndexingCriterion)
                {
                    case eLenght:
                        if (AddToSelection)
                            for (uint Index = IndexingInterval.GetIndexA(); Index <= IndexB; ++Index)
                            {
                                CModelEdge* pEdge = m_EdgeArray[Index];
                                const real Delta = pEdge->GetLength() - Value;
                                pEdge->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                                m_SelectedEdges.AddElement(pEdge, CheckUnique);
                            }
                        else
                            for (uint Index = IndexingInterval.GetIndexA(); Index <= IndexB; ++Index)
                            {
                                CModelEdge* pEdge = m_EdgeArray[Index];
                                const real Delta = pEdge->GetLength() - Value;
                                pEdge->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                            }
                        break;
                    case eAperture:
                        if (AddToSelection)
                            for (uint Index = IndexingInterval.GetIndexA(); Index <= IndexB; ++Index)
                            {
                                CModelEdge* pEdge = m_EdgeArray[Index];
                                const real Delta = pEdge->GetAperture() - Value;
                                pEdge->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                                m_SelectedEdges.AddElement(pEdge, CheckUnique);
                            }
                        else
                            for (uint Index = IndexingInterval.GetIndexA(); Index <= IndexB; ++Index)
                            {
                                CModelEdge* pEdge = m_EdgeArray[Index];
                                const real Delta = pEdge->GetAperture() - Value;
                                pEdge->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                            }
                        break;
                }
            }
            return IndexingInterval;
        }

        CModelPrimitiveIndexingInterval CModelEdgesIndexer::GetIndexingIntervalByRangeValue(const Identifier QueryId, const real ValueMaximal, const real ValueMinimal, const real Sigma, const real MinimalDensity, const bool SetSelected, const bool AddToSelection)
        {
            const real SafeValueMaximal = TMax(ValueMaximal, ValueMinimal);
            const real SafeValueMinimal = TMin(ValueMaximal, ValueMinimal);
            const bool HasDeviation = Mathematics::_1D::IsNonZero(Sigma);
            const real ExponenFactor = HasDeviation ? EVP::Mathematics::_1D::NormalDistribution::DetermineExponentFactor(Sigma) : _REAL_ZERO_;
            const real DeviationAtMinimalDensity = HasDeviation ? EVP::Mathematics::_1D::NormalDistribution::DetermineDeviationAtDensity(MinimalDensity, Sigma) : _REAL_ZERO_;
            const CModelPrimitiveIndexingInterval IndexingInterval = HasDeviation ? GetIndexingInterval(QueryId, SafeValueMaximal + DeviationAtMinimalDensity, SafeValueMinimal - DeviationAtMinimalDensity) : GetIndexingInterval(QueryId, SafeValueMaximal, SafeValueMinimal);
            if (SetSelected && IndexingInterval.HasElements())
            {
                const uint IndexB = IndexingInterval.GetIndexB();
                switch (m_IndexingCriterion)
                {
                    case eLenght:
                        if (AddToSelection)
                            for (uint Index = IndexingInterval.GetIndexA(); Index <= IndexB; ++Index)
                            {
                                CModelEdge* pEdge = m_EdgeArray[Index];
                                const real Value = pEdge->GetLength();
                                if ((Value >= ValueMinimal) && (Value <= SafeValueMaximal))
                                {
                                    pEdge->AddDensitySelection(QueryId, _REAL_ONE_);
                                }
                                else
                                {
                                    const real Delta = Value - SafeValueMaximal;
                                    pEdge->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                                }
                                m_SelectedEdges.AddElement(pEdge, true);
                            }
                        else
                            for (uint Index = IndexingInterval.GetIndexA(); Index <= IndexB; ++Index)
                            {
                                CModelEdge* pEdge = m_EdgeArray[Index];
                                const real Value = pEdge->GetLength();
                                if ((Value >= ValueMinimal) && (Value <= SafeValueMaximal))
                                {
                                    pEdge->AddDensitySelection(QueryId, _REAL_ONE_);
                                }
                                else
                                {
                                    const real Delta = Value - SafeValueMaximal;
                                    pEdge->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                                }
                            }
                        break;
                    case eAperture:
                        if (AddToSelection)
                            for (uint Index = IndexingInterval.GetIndexA(); Index <= IndexB; ++Index)
                            {
                                CModelEdge* pEdge = m_EdgeArray[Index];
                                const real Value = pEdge->GetAperture();
                                if ((Value >= ValueMinimal) && (Value <= SafeValueMaximal))
                                {
                                    pEdge->AddDensitySelection(QueryId, _REAL_ONE_);
                                }
                                else
                                {
                                    const real Delta = Value - SafeValueMaximal;
                                    pEdge->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                                }
                            }
                        else
                            for (uint Index = IndexingInterval.GetIndexA(); Index <= IndexB; ++Index)
                            {
                                CModelEdge* pEdge = m_EdgeArray[Index];
                                const real Value = pEdge->GetAperture();
                                if ((Value >= ValueMinimal) && (Value <= SafeValueMaximal))
                                {
                                    pEdge->AddDensitySelection(QueryId, _REAL_ONE_);
                                }
                                else
                                {
                                    const real Delta = (Value > SafeValueMaximal) ? Value - SafeValueMaximal : Value - SafeValueMinimal;
                                    pEdge->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                                }
                                m_SelectedEdges.AddElement(pEdge, true);
                            }
                        break;
                }
            }
            return IndexingInterval;
        }

        THierarchicalModelElementList<CModelEdge*> CModelEdgesIndexer::GetAdjacentEdgesBySingleValueFromInterval(const Identifier QueryId, const CModelPrimitiveIndexingInterval& IndexingInterval, const real Value, const real Sigma, const real MinimalDensity, const bool SetSelected, const bool AddToSelection)
        {
            THierarchicalModelElementList<CModelEdge*> SelectedEdges;
            if (IndexingInterval.HasElements() && (IndexingInterval.GetIndexer() == this))
            {
                const real DeviationAtMinimalDensity = EVP::Mathematics::_1D::NormalDistribution::DetermineDeviationAtDensity(MinimalDensity, Sigma);
                const uint IndexB = IndexingInterval.GetIndexB();
                switch (m_IndexingCriterion)
                {
                    case eLenght:
                        for (uint Index = IndexingInterval.GetIndexA(); Index <= IndexB; ++Index)
                        {
                            const CModelEdge* pEdge = m_EdgeArray[Index];
                            const THierarchicalModelElementList<CModelEdge*>* pEdgesSets[2] = { &pEdge->GetReadOnlyVertexA()->GetEdges(), &pEdge->GetReadOnlyVertexB()->GetEdges() };
                            for (uint i = 0; i < 2; ++i)
                            {
                                THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = pEdgesSets[i]->end();
                                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdgeAdjacent = pEdgesSets[i]->begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                                    if ((*ppModelEdgeAdjacent) != pEdge)
                                    {
                                        const real Delta = RealAbs((*ppModelEdgeAdjacent)->GetLength() - Value);
                                        if (Delta <= DeviationAtMinimalDensity)
                                        {
                                            SelectedEdges.AddElement((*ppModelEdgeAdjacent), false);
                                        }
                                    }
                            }
                        }
                        break;
                    case eAperture:
                        for (uint Index = IndexingInterval.GetIndexA(); Index <= IndexB; ++Index)
                        {
                            const CModelEdge* pEdge = m_EdgeArray[Index];
                            const THierarchicalModelElementList<CModelEdge*>* pEdgesSets[2] = { &pEdge->GetReadOnlyVertexA()->GetEdges(), &pEdge->GetReadOnlyVertexB()->GetEdges() };
                            for (uint i = 0; i < 2; ++i)
                            {
                                THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = pEdgesSets[i]->end();
                                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdgeAdjacent = pEdgesSets[i]->begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                                    if ((*ppModelEdgeAdjacent) != pEdge)
                                    {
                                        const real Delta = RealAbs((*ppModelEdgeAdjacent)->GetAperture() - Value);
                                        if (Delta <= DeviationAtMinimalDensity)
                                        {
                                            SelectedEdges.AddElement((*ppModelEdgeAdjacent), false);
                                        }
                                    }
                            }
                        }
                        break;
                }
                if (SelectedEdges.SortedUnique() && SetSelected)
                {
                    const real ExponenFactor = EVP::Mathematics::_1D::NormalDistribution::DetermineExponentFactor(Sigma);
                    THierarchicalModelElementList<CModelEdge*>::iterator EndEdges = SelectedEdges.end();
                    switch (m_IndexingCriterion)
                    {
                        case eLenght:
                            for (THierarchicalModelElementList<CModelEdge*>::iterator ppModelEdgeAdjacent = SelectedEdges.begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                            {
                                CModelEdge* pModelEdgeAdjacent = *ppModelEdgeAdjacent;
                                const real Delta = pModelEdgeAdjacent->GetLength() - Value;
                                pModelEdgeAdjacent->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                                if (AddToSelection)
                                {
                                    m_SelectedEdges.AddElement(pModelEdgeAdjacent, true);
                                }
                            }
                            break;
                        case eAperture:
                            for (THierarchicalModelElementList<CModelEdge*>::iterator ppModelEdgeAdjacent = SelectedEdges.begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                            {
                                CModelEdge* pModelEdgeAdjacent = *ppModelEdgeAdjacent;
                                const real Delta = pModelEdgeAdjacent->GetAperture() - Value;
                                pModelEdgeAdjacent->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                                if (AddToSelection)
                                {
                                    m_SelectedEdges.AddElement(pModelEdgeAdjacent, true);
                                }
                            }
                            break;
                    }
                }
            }
            return SelectedEdges;
        }

        THierarchicalModelElementList<CModelEdge*> CModelEdgesIndexer::GetAdjacentEdgesByRangeValueFromInterval(const Identifier QueryId, const CModelPrimitiveIndexingInterval& IndexingInterval, const real ValueMaximal, const real ValueMinimal, const real Sigma, const real MinimalDensity, const bool SetSelected, const bool AddToSelection)
        {
            THierarchicalModelElementList<CModelEdge*> SelectedEdges;
            if (IndexingInterval.HasElements() && (IndexingInterval.GetIndexer() == this))
            {
                const real DeviationAtMinimalDensity = EVP::Mathematics::_1D::NormalDistribution::DetermineDeviationAtDensity(MinimalDensity, Sigma);
                const real SafeMinimalValue = TMax(TMin(ValueMinimal, ValueMaximal), m_MinimalCriterion);
                const real SafeMaximalValue = TMin(TMax(ValueMinimal, ValueMaximal), m_MaximalCriterion);
                const real AllowedMinimalValue = TMax(SafeMinimalValue - DeviationAtMinimalDensity, m_MinimalCriterion);
                const real AllowedMaximalValue = TMin(SafeMaximalValue + DeviationAtMinimalDensity, m_MaximalCriterion);
                const uint IndexB = IndexingInterval.GetIndexB();
                switch (m_IndexingCriterion)
                {
                    case eLenght:
                        for (uint Index = IndexingInterval.GetIndexA(); Index <= IndexB; ++Index)
                        {
                            const CModelEdge* pEdge = m_EdgeArray[Index];
                            const THierarchicalModelElementList<CModelEdge*>* pEdgesSets[2] = { &pEdge->GetReadOnlyVertexA()->GetEdges(), &pEdge->GetReadOnlyVertexB()->GetEdges() };
                            for (uint i = 0; i < 2; ++i)
                            {
                                THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = pEdgesSets[i]->end();
                                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdgeAdjacent = pEdgesSets[i]->begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                                    if ((*ppModelEdgeAdjacent) != pEdge)
                                    {
                                        const real Value = (*ppModelEdgeAdjacent)->GetLength();
                                        if ((Value >= AllowedMinimalValue) && (Value <= AllowedMaximalValue))
                                        {
                                            SelectedEdges.AddElement((*ppModelEdgeAdjacent), false);
                                        }
                                    }
                            }
                        }
                        break;
                    case eAperture:
                        for (uint Index = IndexingInterval.GetIndexA(); Index <= IndexB; ++Index)
                        {
                            const CModelEdge* pEdge = m_EdgeArray[Index];
                            const THierarchicalModelElementList<CModelEdge*>* pEdgesSets[2] = { &pEdge->GetReadOnlyVertexA()->GetEdges(), &pEdge->GetReadOnlyVertexB()->GetEdges() };
                            for (uint i = 0; i < 2; ++i)
                            {
                                THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = pEdgesSets[i]->end();
                                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdgeAdjacent = pEdgesSets[i]->begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                                    if ((*ppModelEdgeAdjacent) != pEdge)
                                    {
                                        const real Value = (*ppModelEdgeAdjacent)->GetAperture();
                                        if ((Value >= AllowedMinimalValue) && (Value <= AllowedMaximalValue))
                                        {
                                            SelectedEdges.AddElement((*ppModelEdgeAdjacent), false);
                                        }
                                    }
                            }
                        }
                        break;
                }
                if (SelectedEdges.SortedUnique() && SetSelected)
                {
                    const real ExponenFactor = EVP::Mathematics::_1D::NormalDistribution::DetermineExponentFactor(Sigma);
                    THierarchicalModelElementList<CModelEdge*>::iterator EndEdges = SelectedEdges.end();
                    switch (m_IndexingCriterion)
                    {
                        case eLenght:
                            for (THierarchicalModelElementList<CModelEdge*>::iterator ppModelEdgeAdjacent = SelectedEdges.begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                            {
                                CModelEdge* pModelEdgeAdjacent = *ppModelEdgeAdjacent;
                                const real Value = (*ppModelEdgeAdjacent)->GetLength();
                                if ((Value < SafeMinimalValue) || (Value > SafeMaximalValue))
                                {
                                    const real Delta = Value - Value;
                                    pModelEdgeAdjacent->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                                }
                                else
                                {
                                    pModelEdgeAdjacent->AddDensitySelection(QueryId, _REAL_ONE_);
                                }
                                if (AddToSelection)
                                {
                                    m_SelectedEdges.AddElement(pModelEdgeAdjacent, true);
                                }
                            }
                            break;
                        case eAperture:
                            for (THierarchicalModelElementList<CModelEdge*>::iterator ppModelEdgeAdjacent = SelectedEdges.begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                            {
                                CModelEdge* pModelEdgeAdjacent = *ppModelEdgeAdjacent;
                                const real Value = (*ppModelEdgeAdjacent)->GetAperture();
                                if ((Value < SafeMinimalValue) || (Value > SafeMaximalValue))
                                {
                                    const real Delta = Value - Value;
                                    pModelEdgeAdjacent->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                                }
                                else
                                {
                                    pModelEdgeAdjacent->AddDensitySelection(QueryId, _REAL_ONE_);
                                }
                                if (AddToSelection)
                                {
                                    m_SelectedEdges.AddElement(pModelEdgeAdjacent, true);
                                }
                            }
                            break;
                    }
                }
            }
            return SelectedEdges;
        }

        THierarchicalModelElementList<CModelEdge*> CModelEdgesIndexer::GetAdjacentEdgesBySingleValueFromSelection(const Identifier QueryId, const THierarchicalModelElementList<CModelEdge*>& SourceSelection, const real Value, const real Sigma, const real MinimalDensity, const bool SetSelected, const bool AddToSelection)
        {
            THierarchicalModelElementList<CModelEdge*> SelectedEdges;
            if (SourceSelection.size())
            {
                const real DeviationAtMinimalDensity = EVP::Mathematics::_1D::NormalDistribution::DetermineDeviationAtDensity(MinimalDensity, Sigma);
                THierarchicalModelElementList<CModelEdge*>::const_iterator EndSourceSelection = SourceSelection.end();
                switch (m_IndexingCriterion)
                {
                    case eLenght:
                        for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = SourceSelection.begin(); ppModelEdge != EndSourceSelection; ++ppModelEdge)
                        {
                            const CModelEdge* pEdge = *ppModelEdge;
                            const THierarchicalModelElementList<CModelEdge*>* pEdgesSets[2] = { &pEdge->GetReadOnlyVertexA()->GetEdges(), &pEdge->GetReadOnlyVertexB()->GetEdges() };
                            for (uint i = 0; i < 2; ++i)
                            {
                                THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = pEdgesSets[i]->end();
                                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdgeAdjacent = pEdgesSets[i]->begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                                    if ((*ppModelEdgeAdjacent) != pEdge)
                                    {
                                        const real Delta = RealAbs((*ppModelEdgeAdjacent)->GetLength() - Value);
                                        if (Delta <= DeviationAtMinimalDensity)
                                        {
                                            SelectedEdges.AddElement((*ppModelEdgeAdjacent), false);
                                        }
                                    }
                            }
                        }
                        break;
                    case eAperture:
                        for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = SourceSelection.begin(); ppModelEdge != EndSourceSelection; ++ppModelEdge)
                        {
                            const CModelEdge* pEdge = *ppModelEdge;
                            const THierarchicalModelElementList<CModelEdge*>* pEdgesSets[2] = { &pEdge->GetReadOnlyVertexA()->GetEdges(), &pEdge->GetReadOnlyVertexB()->GetEdges() };
                            for (uint i = 0; i < 2; ++i)
                            {
                                THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = pEdgesSets[i]->end();
                                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdgeAdjacent = pEdgesSets[i]->begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                                    if ((*ppModelEdgeAdjacent) != pEdge)
                                    {
                                        const real Delta = RealAbs((*ppModelEdgeAdjacent)->GetAperture() - Value);
                                        if (Delta <= DeviationAtMinimalDensity)
                                        {
                                            SelectedEdges.AddElement((*ppModelEdgeAdjacent), false);
                                        }
                                    }
                            }
                        }
                        break;
                }
                if (SelectedEdges.SortedUnique() && SetSelected)
                {
                    const real ExponenFactor = EVP::Mathematics::_1D::NormalDistribution::DetermineExponentFactor(Sigma);
                    THierarchicalModelElementList<CModelEdge*>::iterator EndEdges = SelectedEdges.end();
                    switch (m_IndexingCriterion)
                    {
                        case eLenght:
                            for (THierarchicalModelElementList<CModelEdge*>::iterator ppModelEdgeAdjacent = SelectedEdges.begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                            {
                                CModelEdge* pModelEdgeAdjacent = *ppModelEdgeAdjacent;
                                const real Delta = pModelEdgeAdjacent->GetLength() - Value;
                                pModelEdgeAdjacent->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                                if (AddToSelection)
                                {
                                    m_SelectedEdges.AddElement(pModelEdgeAdjacent, true);
                                }
                            }
                            break;
                        case eAperture:
                            for (THierarchicalModelElementList<CModelEdge*>::iterator ppModelEdgeAdjacent = SelectedEdges.begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                            {
                                CModelEdge* pModelEdgeAdjacent = *ppModelEdgeAdjacent;
                                const real Delta = pModelEdgeAdjacent->GetAperture() - Value;
                                pModelEdgeAdjacent->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                                if (AddToSelection)
                                {
                                    m_SelectedEdges.AddElement(pModelEdgeAdjacent, true);
                                }
                            }
                            break;
                    }
                }
            }
            return SelectedEdges;
        }

        THierarchicalModelElementList<CModelEdge*> CModelEdgesIndexer::GetAdjacentEdgesByRangeValueFromSelection(const Identifier QueryId, const THierarchicalModelElementList<CModelEdge*>& SourceSelection, const real ValueMaximal, const real ValueMinimal, const real Sigma, const real MinimalDensity, const bool SetSelected, const bool AddToSelection)
        {
            THierarchicalModelElementList<CModelEdge*> SelectedEdges;
            if (SourceSelection.size())
            {
                const real DeviationAtMinimalDensity = EVP::Mathematics::_1D::NormalDistribution::DetermineDeviationAtDensity(MinimalDensity, Sigma);
                const real SafeMinimalValue = TMax(TMin(ValueMinimal, ValueMaximal), m_MinimalCriterion);
                const real SafeMaximalValue = TMin(TMax(ValueMinimal, ValueMaximal), m_MaximalCriterion);
                const real AllowedMinimalValue = TMax(SafeMinimalValue - DeviationAtMinimalDensity, m_MinimalCriterion);
                const real AllowedMaximalValue = TMin(SafeMaximalValue + DeviationAtMinimalDensity, m_MaximalCriterion);
                THierarchicalModelElementList<CModelEdge*>::const_iterator EndSourceSelection = SourceSelection.end();
                switch (m_IndexingCriterion)
                {
                    case eLenght:
                        for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = SourceSelection.begin(); ppModelEdge != EndSourceSelection; ++ppModelEdge)
                        {
                            const CModelEdge* pEdge = *ppModelEdge;
                            const THierarchicalModelElementList<CModelEdge*>* pEdgesSets[2] = { &pEdge->GetReadOnlyVertexA()->GetEdges(), &pEdge->GetReadOnlyVertexB()->GetEdges() };
                            for (uint i = 0; i < 2; ++i)
                            {
                                THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = pEdgesSets[i]->end();
                                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdgeAdjacent = pEdgesSets[i]->begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                                    if ((*ppModelEdgeAdjacent) != pEdge)
                                    {
                                        const real Value = (*ppModelEdgeAdjacent)->GetLength();
                                        if ((Value >= AllowedMinimalValue) && (Value <= AllowedMaximalValue))
                                        {
                                            SelectedEdges.AddElement((*ppModelEdgeAdjacent), false);
                                        }
                                    }
                            }
                        }
                        break;
                    case eAperture:
                        for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = SourceSelection.begin(); ppModelEdge != EndSourceSelection; ++ppModelEdge)
                        {
                            const CModelEdge* pEdge = *ppModelEdge;
                            const THierarchicalModelElementList<CModelEdge*>* pEdgesSets[2] = { &pEdge->GetReadOnlyVertexA()->GetEdges(), &pEdge->GetReadOnlyVertexB()->GetEdges() };
                            for (uint i = 0; i < 2; ++i)
                            {
                                THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = pEdgesSets[i]->end();
                                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdgeAdjacent = pEdgesSets[i]->begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                                    if ((*ppModelEdgeAdjacent) != pEdge)
                                    {
                                        const real Value = (*ppModelEdgeAdjacent)->GetAperture();
                                        if ((Value >= AllowedMinimalValue) && (Value <= AllowedMaximalValue))
                                        {
                                            SelectedEdges.AddElement((*ppModelEdgeAdjacent), false);
                                        }
                                    }
                            }
                        }
                        break;
                }
                if (SelectedEdges.SortedUnique() && SetSelected)
                {
                    const real ExponenFactor = EVP::Mathematics::_1D::NormalDistribution::DetermineExponentFactor(Sigma);
                    THierarchicalModelElementList<CModelEdge*>::iterator EndEdges = SelectedEdges.end();
                    switch (m_IndexingCriterion)
                    {
                        case eLenght:
                            for (THierarchicalModelElementList<CModelEdge*>::iterator ppModelEdgeAdjacent = SelectedEdges.begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                            {
                                CModelEdge* pModelEdgeAdjacent = *ppModelEdgeAdjacent;
                                const real Value = (*ppModelEdgeAdjacent)->GetLength();
                                if ((Value < SafeMinimalValue) || (Value > SafeMaximalValue))
                                {
                                    const real Delta = Value - Value;
                                    pModelEdgeAdjacent->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                                }
                                else
                                {
                                    pModelEdgeAdjacent->AddDensitySelection(QueryId, _REAL_ONE_);
                                }
                                if (AddToSelection)
                                {
                                    m_SelectedEdges.AddElement(pModelEdgeAdjacent, true);
                                }
                            }
                            break;
                        case eAperture:
                            for (THierarchicalModelElementList<CModelEdge*>::iterator ppModelEdgeAdjacent = SelectedEdges.begin(); ppModelEdgeAdjacent != EndEdges; ++ppModelEdgeAdjacent)
                            {
                                CModelEdge* pModelEdgeAdjacent = *ppModelEdgeAdjacent;
                                const real Value = (*ppModelEdgeAdjacent)->GetAperture();
                                if ((Value < SafeMinimalValue) || (Value > SafeMaximalValue))
                                {
                                    const real Delta = Value - Value;
                                    pModelEdgeAdjacent->AddDensitySelection(QueryId, RealExp(Delta * Delta * ExponenFactor));
                                }
                                else
                                {
                                    pModelEdgeAdjacent->AddDensitySelection(QueryId, _REAL_ONE_);
                                }
                                if (AddToSelection)
                                {
                                    m_SelectedEdges.AddElement(pModelEdgeAdjacent, true);
                                }
                            }
                            break;
                    }
                }
            }
            return SelectedEdges;
        }

        void CModelEdgesIndexer::ClearSelectedEdges()
        {
            if (m_SelectedEdges.size())
            {
                THierarchicalModelElementList<CModelEdge*>::const_iterator EndSelectedEdges = m_SelectedEdges.end();
                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = m_SelectedEdges.begin(); ppModelEdge != EndSelectedEdges; ++ppModelEdge)
                {
                    (*ppModelEdge)->ClearDensitySelections();
                }
                m_SelectedEdges.clear();
            }
        }

        bool CModelEdgesIndexer::HasSelectedEdges()
        {
            return m_SelectedEdges.size();
        }

        uint CModelEdgesIndexer::GetTotalSelectedEdges()
        {
            return m_SelectedEdges.size();
        }

        const THierarchicalModelElementList<CModelEdge*>& CModelEdgesIndexer::GetSelectedEdges() const
        {
            return m_SelectedEdges;
        }

        THierarchicalModelElementList<CModelMesh*> CModelEdgesIndexer::GetSelectedMeshes()
        {
            THierarchicalModelElementList<CModelMesh*> SelectedMeshes;
            if (m_SelectedEdges.size())
            {
                m_SelectedEdges.sort(SortEdgesByMesh);
                CModelMesh* pModelMesh = nullptr;
                THierarchicalModelElementList<CModelEdge*>::const_iterator EndSelectedEdges = m_SelectedEdges.end();
                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = m_SelectedEdges.begin(); ppModelEdge != EndSelectedEdges; ++ppModelEdge)
                    if ((*ppModelEdge)->GetWritableMesh() != pModelMesh)
                    {
                        pModelMesh = (*ppModelEdge)->GetWritableMesh();
                        SelectedMeshes.AddElement(pModelMesh, false);
                    }
            }
            return SelectedMeshes;
        }

        void CModelEdgesIndexer::EstimateGaussianPDF(const uint TotalDiscretePDFSamples)
        {
            const real Delta = m_RangeCriterion / real(TotalDiscretePDFSamples - 1);
            const uint TotalElements = m_EdgeArray.size();
            real Accumulator = _REAL_ZERO_, SquareAccumulator = _REAL_ZERO_;
            switch (m_IndexingCriterion)
            {
                case eLenght:
                    for (uint i = 0; i < TotalElements; ++i)
                    {
                        const real Value = m_EdgeArray[i]->GetLength();
                        Accumulator += Value;
                        SquareAccumulator += Value * Value;
                    }
                    break;
                case eAperture:
                    for (uint i = 0; i < TotalElements; ++i)
                    {
                        const real Value = m_EdgeArray[i]->GetAperture();
                        Accumulator += Value;
                        SquareAccumulator += Value * Value;
                    }
                    break;
            }
            const real Mean = Accumulator / real(TotalElements);
            const real StandarDeviation = RealSqrt((real(SquareAccumulator) / real(TotalElements)) - (Mean * Mean));
            const real BandWidth = RealPow((real(4.0) * RealPow(StandarDeviation, real(5.0))) / real(3 * TotalElements), real(1.0 / 5.0));
            const real ExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(BandWidth);
            real Value = m_MinimalCriterion, TotalAccumulatorDensity = _REAL_ZERO_, MaximalAccumulatorDensity = _REAL_ZERO_;
            m_PDF.SetTotalSamples(TotalDiscretePDFSamples, m_MaximalCriterion, m_MinimalCriterion);
            real* pDensityDistribution = m_PDF.GetWritableDensityDistribution();
            switch (m_IndexingCriterion)
            {
                case eLenght:
                    for (uint i = 0; i < TotalDiscretePDFSamples; ++i, Value += Delta)
                    {
                        real Density = _REAL_ZERO_;
                        for (uint j = 0; j < TotalElements; ++j)
                        {
                            const real Delta = m_EdgeArray[j]->GetLength() - Value;
                            Density += RealExp(Delta * Delta * ExponentFactor);
                        }
                        pDensityDistribution[i] = Density;
                        TotalAccumulatorDensity += Density;
                        if (Density > MaximalAccumulatorDensity)
                        {
                            MaximalAccumulatorDensity = Density;
                        }
                    }
                    m_PDF.Normalize(TotalAccumulatorDensity, MaximalAccumulatorDensity);
                    break;
                case eAperture:
                    for (uint i = 0; i < TotalDiscretePDFSamples; ++i, Value += Delta)
                    {
                        real Density = _REAL_ZERO_;
                        for (uint j = 0; j < TotalElements; ++j)
                        {
                            const real Delta = m_EdgeArray[j]->GetAperture() - Value;
                            Density += RealExp(Delta * Delta * ExponentFactor);
                        }
                        pDensityDistribution[i] = Density;
                        TotalAccumulatorDensity += Density;
                        if (Density > MaximalAccumulatorDensity)
                        {
                            MaximalAccumulatorDensity = Density;
                        }
                    }
                    m_PDF.Normalize(TotalAccumulatorDensity, MaximalAccumulatorDensity);
                    break;
            }

            /*EVP::ostringstream OutputString;
             OutputString.precision(6);
             const real* pNormalizedPDF = m_PDF.GetReadOnlyPDF();
             const real* pNormalizedAccumulatedPDF = m_PDF.GetReadOnlyAccumulatedPDF();
             const real* pNormalizedDensityDistribution = m_PDF.GetReadOnlyDensityDistribution();
             Value = m_MinimalCriterion;
             for (uint i = 0; i < TotalDiscretePDFSamples; ++i, Value += Delta)
             OutputString << Value << "\t" << pNormalizedPDF[i] << "\t" << pNormalizedAccumulatedPDF[i] << "\t" << pNormalizedDensityDistribution[i] << "\t" << g_EndLine;

             g_ConsoleStringOutput << "Mean = " << Mean << g_EndLine;
             g_ConsoleStringOutput << "StandarDeviation = " << StandarDeviation << g_EndLine;
             g_ConsoleStringOutput << "BandWidth = " << BandWidth << g_EndLine;
             EVP::Files::COutFile::WriteStringToFile("/home/staff/gonzalez/Thesis/PDF.txt", OutputString.str());*/

        }

        CModelPrimitiveIndexingInterval CModelEdgesIndexer::GetIndexingInterval(const Identifier QueryId, const real MaximalValue, const real m_MinimalValue)
        {
            const real SafeMaximal = TMax(MaximalValue, m_MinimalValue);
            const real SafeMinimal = TMin(MaximalValue, m_MinimalValue);
            if ((SafeMinimal >= m_MinimalCriterion) || (SafeMaximal <= m_MaximalCriterion))
            {
                const real LimitedMaximal = TMin(SafeMaximal, m_MaximalCriterion);
                const real LimitedMinimal = TMax(SafeMinimal, m_MinimalCriterion);
                switch (m_IndexingCriterion)
                {
                    case eLenght:
                    {
                        uint A = 0;
                        uint B = m_EdgeArray.size() - 1;
                        do
                        {
                            const uint Mid = uint(real(B - A) * _REAL_HALF_) + A;
                            if (m_EdgeArray[Mid]->GetLength() > LimitedMinimal)
                            {
                                B = Mid;
                            }
                            else
                            {
                                A = Mid;
                            }
                        }
                        while ((B - A) > 1);
                        const uint IndexA = A;
                        B = m_EdgeArray.size() - 1;
                        do
                        {
                            const uint Mid = uint(real(B - A) * _REAL_HALF_) + A;
                            if (m_EdgeArray[Mid]->GetLength() > LimitedMaximal)
                            {
                                B = Mid;
                            }
                            else
                            {
                                A = Mid;
                            }
                        }
                        while ((B - A) > 1);
                        return CModelPrimitiveIndexingInterval(this, QueryId, IndexA, B, m_EdgeArray.size());
                    }
                    break;
                    case eAperture:
                    {
                        uint A = 0;
                        uint B = m_EdgeArray.size() - 1;
                        do
                        {
                            const uint Mid = uint(real(B - A) * _REAL_HALF_) + A;
                            if (m_EdgeArray[Mid]->GetAperture() > LimitedMinimal)
                            {
                                B = Mid;
                            }
                            else
                            {
                                A = Mid;
                            }
                        }
                        while ((B - A) > 1);
                        const uint IndexA = A;
                        B = m_EdgeArray.size() - 1;
                        do
                        {
                            const uint Mid = uint(real(B - A) * _REAL_HALF_) + A;
                            if (m_EdgeArray[Mid]->GetAperture() > LimitedMaximal)
                            {
                                B = Mid;
                            }
                            else
                            {
                                A = Mid;
                            }
                        }
                        while ((B - A) > 1);
                        return CModelPrimitiveIndexingInterval(this, QueryId, IndexA, B, m_EdgeArray.size());
                    }
                    break;
                }
            }
            return CModelPrimitiveIndexingInterval(this, QueryId);
        }

        bool CModelEdgesIndexer::SortEdgesByLenght(const CModelEdge* pLhs, const CModelEdge* prhs)
        {
            return pLhs->GetLength() < prhs->GetLength();
        }

        bool CModelEdgesIndexer::SortEdgesByAperture(const CModelEdge* pLhs, const CModelEdge* prhs)
        {
            return pLhs->GetAperture() < prhs->GetAperture();
        }

        bool CModelEdgesIndexer::SortEdgesByMesh(const CModelEdge* pLhs, const CModelEdge* prhs)
        {
            return pLhs->GetSuperiorInstanceId() < prhs->GetSuperiorInstanceId();
        }
    }
}

#endif // _EVP_USE_MODEL_SPACE
