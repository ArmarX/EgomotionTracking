/*
 * ModelOrientedVertexContainer.cpp
 *
 *  Created on: 24.02.2012
 *      Author: gonzalez
 */

#include "ModelOrientedVertexContainer.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        CModelOrientedVertexContainer::CModelOrientedVertexContainer(CModelVertex* pModelVertex, const Mathematics::_3D::CVector3D& Normal) :
            m_pModelVertex(pModelVertex), m_Normal(Normal)
        {
        }

        CModelOrientedVertexContainer::CModelOrientedVertexContainer() :
            m_pModelVertex(nullptr), m_Normal()
        {
        }

        void CModelOrientedVertexContainer::Clear()
        {
            m_pModelVertex = nullptr;
            m_Normal = Mathematics::_3D::CVector3D::s_Point_At_Origin_3D;
        }

        void CModelOrientedVertexContainer::SetModelVertex(CModelVertex* pModelVertex)
        {
            m_pModelVertex = pModelVertex;
        }

        CModelVertex* CModelOrientedVertexContainer::GetWritableModelVertex()
        {
            return m_pModelVertex;
        }

        const CModelVertex* CModelOrientedVertexContainer::GetReadOnlyModelVertex() const
        {
            return m_pModelVertex;
        }

        bool CModelOrientedVertexContainer::AddFace(CModelFace* pModelFace)
        {
            return m_pModelVertex->AddFace(pModelFace, true);
        }

        const Mathematics::_3D::CVector3D& CModelOrientedVertexContainer::GetPoint() const
        {
            return m_pModelVertex->GetPoint();
        }

        void CModelOrientedVertexContainer::SetNormal(const Mathematics::_3D::CVector3D& Normal)
        {
            m_Normal = Normal;
        }

        const Mathematics::_3D::CVector3D& CModelOrientedVertexContainer::GetNormal() const
        {
            return m_Normal;
        }

        uint CModelOrientedVertexContainer::GetIndexInMesh() const
        {
            return m_pModelVertex->GetIndexInMesh();
        }

        bool CModelOrientedVertexContainer::Deserialize(QDomElement& XmlElement, THierarchicalModelElementList<CModelVertex*>& MeshVeritices)
        {
            if (XmlElement.tagName() == "OrientedVertex")
            {
                m_pModelVertex = MeshVeritices.GetElementByInstanceId(Identifier(XmlElement.attribute("VertexId").toUInt()));
                m_Normal.SetValue(real(XmlElement.attribute("NX").toDouble()), real(XmlElement.attribute("NY").toDouble()), real(XmlElement.attribute("NZ").toDouble()));
                m_Normal.Normalize();
                return true;
            }
            return false;
        }

        QDomElement CModelOrientedVertexContainer::Serialize(QDomDocument& XmlDocument) const
        {
            QDomElement XmlElement = XmlDocument.createElement("OrientedVertex");
            XmlElement.setAttribute("VertexId", m_pModelVertex->GetInstanceId());
            XmlElement.setAttribute("NX", m_Normal.GetX());
            XmlElement.setAttribute("NY", m_Normal.GetY());
            XmlElement.setAttribute("NZ", m_Normal.GetZ());
            return XmlElement;
        }
    }
}
#endif // _EVP_USE_MODEL_SPACE
