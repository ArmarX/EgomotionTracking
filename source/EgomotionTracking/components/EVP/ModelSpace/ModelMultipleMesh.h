/*
 * ModelMultipleMesh.h
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "HierarchicalModelElementList.h"
#include "HierarchicalModelComposition.h"
#include "ModelEdgesIndexer.h"
#include "../Foundation/Mathematics/4D/Matrix4D.h"
#include "../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelMesh;

        class CModelMultipleMesh: public CHierarchicalModelComposition
        {
        public:

            CModelMultipleMesh(CHierarchicalModelElement* pSuperior);
            CModelMultipleMesh(CHierarchicalModelElement* pSuperior, const Identifier IntanceId);
            ~CModelMultipleMesh() override;

            bool AddMeshes(CModelMesh* pModelMesh, const bool CheckUnique);
            const THierarchicalModelElementList<CModelMesh*>& GetMeshes() const;

            uint GetTotalPrimitives() const override;

            uint GetTotalVertices() const;
            uint GetTotalVisibleVertices() const;
            uint GetTotalEnabledVertices() const;
            uint GetTotalIndexableVertices() const;

            uint GetTotalEdges() const;
            uint GetTotalVisibleEdges() const;
            uint GetTotalEnabledEdges() const;
            uint GetTotalIndexableEdges() const;

            uint GetTotalFaces() const;
            uint GetTotalVisibleFaces() const;
            uint GetTotalEnabledFaces() const;
            uint GetTotalIndexableFaces() const;

            uint GetTotalMultiFaces() const;
            uint GetTotalVisibleMultiFaces() const;
            uint GetTotalEnabledMultiFaces() const;
            uint GetTotalIndexableMultiFaces() const;

            uint GetTotalMeshes() const;
            uint GetTotalVisibleMeshes() const;
            uint GetTotalEnabledMeshes() const;
            uint GetTotalIndexableMeshes() const;

            bool LoadIndexers(const uint DiscretePDFSamples);
            bool LoadIndexers(const real MaximalLength, const real MinimalLength, const real MaximalAperture, const real MinimalAperture, const real MaximalArea, const real MinimalArea, const uint DiscretePDFSamples);

            CModelEdgesIndexer* GetModelEdgesLenghtIndexer();
            CModelEdgesIndexer* GetModelEdgesApertureIndexer();

            void ApplyTransformation(const EVP::Mathematics::_4D::CMatrix4D& Transformation);

        protected:

            void ClearElements();

            bool Deserialize(QDomElement& XmlElement) override;
            QDomElement Serialize(QDomDocument& XmlDocument) const override;

            bool DeserializeAttributes(QDomElement& XmlElement) override;
            void SerializeAttributes(QDomElement& XmlElement) const override;

            THierarchicalModelElementList<CModelMesh*> m_Meshes;
            CModelEdgesIndexer m_ModelEdgesLenghtIndexer;
            CModelEdgesIndexer m_ModelEdgesApertureIndexer;
        };
    }
}
#endif // _EVP_USE_MODEL_SPACE
