/*
 * HierarchicalModelElement.h
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "ModelElement.h"
#include "../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CHierarchicalModelElement: public CModelElement
        {
        public:

            CHierarchicalModelElement(const CModelElement::TypeId Id, CHierarchicalModelElement* pSuperior);
            CHierarchicalModelElement(const CModelElement::TypeId Id, CHierarchicalModelElement* pSuperior, const Identifier IntanceId);
            ~CHierarchicalModelElement() override;

            CHierarchicalModelElement* GetWritableSuperior();
            const CHierarchicalModelElement* GetReadOnlySuperior() const;
            Identifier GetSuperiorInstanceId() const;
            bool IsIndexable() const;
            void SetIndexable(const bool Indexable);

        protected:

            bool DeserializeAttributes(QDomElement& XmlElement) override;
            void SerializeAttributes(QDomElement& XmlElement) const override;

            CHierarchicalModelElement* m_pSuperior;
            bool m_IsIndexable;
        };
    }
}

#endif //_EVP_USE_GUI_TOOLKIT
