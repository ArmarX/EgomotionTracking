/*
 * DensitySelection.cpp
 *
 *  Created on: 24.02.2012
 *      Author: gonzalez
 */

#include "DensitySelection.h"

namespace EVP
{
    namespace ModelSpace
    {
        CDensitySelection::CDensitySelection(const Identifier QueryId, const real Density) :
            m_QueryId(QueryId), m_Density(Density)
        {
        }

        CDensitySelection::~CDensitySelection()
            = default;

        Identifier CDensitySelection::GetQueryId() const
        {
            return m_QueryId;
        }

        real CDensitySelection::GetDensity() const
        {
            return m_Density;
        }
    }
}
