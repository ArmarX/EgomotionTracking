/*
 * HierarchicalModelPrimitive.h
 *
 *  Created on: 23.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "DensitySelection.h"
#include "HierarchicalModelElement.h"
#include "ModelMesh.h"
#include "../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CHierarchicalModelPrimitive: public CHierarchicalModelElement
        {
        public:

            CHierarchicalModelPrimitive(const CModelElement::TypeId Id, CModelMesh* pMesh);
            CHierarchicalModelPrimitive(const CModelElement::TypeId Id, CModelMesh* pMesh, const Identifier IntanceId);
            ~CHierarchicalModelPrimitive() override;

            CModelMesh* GetWritableMesh();
            const CModelMesh* GetReadOnlyMesh() const;

            void AddDensitySelection(const Identifier QueryId, const real Density);
            void ClearDensitySelections();
            bool HasDensitySelections() const;
            uint GetTotalDensitySelections() const;
            const TModelList<CDensitySelection*>& GetDensitySelections(const bool Sorted);
            const CDensitySelection* GetMaximalDensitySelection() const;
            const CDensitySelection* GetMinimalDensitySelection() const;
            real GetDisjunctedDensity() const;
            real GetConjunctedDensity() const;
            bool GetDensityByQueryId(const Identifier QueryId, real& Density) const;

        protected:

            bool DeserializeAttributes(QDomElement& XmlElement) override;
            void SerializeAttributes(QDomElement& XmlElement) const override;

            virtual QDomElement SerializeReference(QDomDocument& XmlDocument) const = 0;

            static  bool SortDensitySelectionByDensityDownwards(const CDensitySelection* plhs, const CDensitySelection* prhs);

            TModelList<CDensitySelection*> m_DensitySelections;
        };
    }
}

#endif // _EVP_USE_MODEL_SPACE
