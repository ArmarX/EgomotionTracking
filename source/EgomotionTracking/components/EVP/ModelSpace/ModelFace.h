/*
 * ModelFace.h
 *
 *  Created on: 23.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "HierarchicalModelPrimitive.h"
#include "ModelVertex.h"
#include "ModelOrientedVertexContainer.h"
#include "../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelMultipleFace;
        class CModelEdge;
        class CModelVertex;

        class CModelFace: public CHierarchicalModelPrimitive
        {
        public:

            CModelFace(CModelMesh* pModelMesh, CModelVertex* pModelVertexA, CModelVertex* pModelVertexB, CModelVertex* pModelVertexC, const Mathematics::_3D::CVector3D& NormalA, const Mathematics::_3D::CVector3D& NormalB, const Mathematics::_3D::CVector3D& NormalC);
            CModelFace(CModelMesh* pModelMesh, const Identifier IntanceId);
            ~CModelFace() override;

            void SetMultipleFace(const CModelMultipleFace* pModelMultipleFace);
            const CModelMultipleFace* GetMultipleFace() const;

            THierarchicalModelElementList<const CModelOrientedVertexContainer*> GetOrientedVertices() const;
            const THierarchicalModelElementList<CModelEdge*>& GetEdges() const;
            const Mathematics::_3D::CVector3D& GetNormal() const;
            const Mathematics::_3D::CVector3D& GetCenteroid() const;
            real GetArea() const;

        protected:

            friend class CModelMesh;
            friend class CModelMultipleFace;

            void ClearElements();

            bool Deserialize(QDomElement& XmlElement) override;
            QDomElement Serialize(QDomDocument& XmlDocument) const override;

            bool DeserializeAttributes(QDomElement& XmlElement) override;
            void SerializeAttributes(QDomElement& XmlElement) const override;

            QDomElement SerializeReference(QDomDocument& XmlDocument) const override;

            void ComputeArea(const real A, const real B, const real C);
            void ComputeCentroid(const Mathematics::_3D::CVector3D& A, const Mathematics::_3D::CVector3D& B, const Mathematics::_3D::CVector3D& C);
            void ComputeNormal(const Mathematics::_3D::CVector3D& DirectionA, const Mathematics::_3D::CVector3D& DirectionB, const Mathematics::_3D::CVector3D& NormalA, const Mathematics::_3D::CVector3D& NormalB, const Mathematics::_3D::CVector3D& NormalC);

            const CModelMultipleFace* m_pModelMultipleFace;
            CModelOrientedVertexContainer m_OrientedVertexContainerA;
            CModelOrientedVertexContainer m_OrientedVertexContainerB;
            CModelOrientedVertexContainer m_OrientedVertexContainerC;
            THierarchicalModelElementList<CModelEdge*> m_Edges;
            Mathematics::_3D::CVector3D m_Normal;
            Mathematics::_3D::CVector3D m_Centeroid;
            real m_Area;
        };
    }
}

#endif // _EVP_USE_MODEL_SPACE
