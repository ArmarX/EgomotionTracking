/*
 * ModelMultipleFace.cpp
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#include "ModelMultipleFace.h"
#include "ModelFace.h"
#include "ModelEdge.h"
#include "ModelVertex.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        CModelMultipleFace::CModelMultipleFace(CModelFace* pFace) :
            CHierarchicalModelComposition(CModelElement::eMultiFace, pFace->GetWritableMesh()), m_Area(_REAL_ZERO_)
        {
            if (Expansion(pFace, _EVP_MODELSPACE_APERTURE_TOLERANCE_) > 1)
            {
                Extraction();
                pFace->GetWritableMesh()->AddMultipleFace(this, false);
            }
        }

        CModelMultipleFace::CModelMultipleFace(CModelMesh* pModelMesh, const Identifier IntanceId) :
            CHierarchicalModelComposition(CModelElement::eMultiFace, pModelMesh), m_Area(_REAL_ZERO_)
        {
        }

        CModelMultipleFace::~CModelMultipleFace()
        {
            ClearElements();
        }

        void CModelMultipleFace::ClearElements()
        {
            THierarchicalModelElementList<CModelFace*>::const_iterator EndFaces = m_Faces.end();
            for (THierarchicalModelElementList<CModelFace*>::const_iterator ppModelFace = m_Faces.begin(); ppModelFace != EndFaces; ++ppModelFace)
            {
                (*ppModelFace)->SetMultipleFace(nullptr);
            }
            m_Faces.clear();
            m_Edges.clear();
            m_Veritices.clear();
            m_Normal = Mathematics::_3D::CVector3D::s_Point_At_Origin_3D;
            m_Centeroid = Mathematics::_3D::CVector3D::s_Point_At_Origin_3D;
            m_Area = _REAL_ZERO_;
        }

        CModelMesh* CModelMultipleFace::GetWritableMesh()
        {
            return dynamic_cast<CModelMesh*>(GetWritableSuperior());
        }

        const CModelMesh* CModelMultipleFace::GetReadOnlyMesh() const
        {
            return dynamic_cast<const CModelMesh*>(GetReadOnlySuperior());
        }

        const THierarchicalModelElementList<CModelFace*>& CModelMultipleFace::GetFaces() const
        {
            return m_Faces;
        }

        const THierarchicalModelElementList<CModelEdge*>& CModelMultipleFace::GetEdges() const
        {
            return m_Edges;
        }

        const THierarchicalModelElementList<CModelVertex*>& CModelMultipleFace::GetVeritices() const
        {
            return m_Veritices;
        }

        const Mathematics::_3D::CVector3D& CModelMultipleFace::GetNormal() const
        {
            return m_Normal;
        }

        const Mathematics::_3D::CVector3D& CModelMultipleFace::GetCenteroid() const
        {
            return m_Centeroid;
        }

        real CModelMultipleFace::GetArea() const
        {
            return m_Area;
        }

        bool CModelMultipleFace::IsTrivial() const
        {
            return (m_Faces.size() < 2);
        }

        uint CModelMultipleFace::GetTotalPrimitives() const
        {
            return m_Faces.size();
        }

        bool CModelMultipleFace::Deserialize(QDomElement& XmlElement)
        {
            if (XmlElement.tagName() == "MultipleFace")
            {
                ClearElements();
                DeserializeAttributes(XmlElement);
                THierarchicalModelElementList<CModelFace*>& MeshFaces = GetWritableMesh()->GetWritableFaces();
                QDomNode XmlFace = XmlElement.firstChild().toElement().firstChild();
                while (!XmlFace.isNull())
                {
                    AddFace(MeshFaces.GetElementByInstanceId(Identifier(XmlFace.toElement().attribute("Id").toUInt())));
                    XmlFace = XmlFace.nextSibling();
                }
                Extraction();
                return true;
            }
            return false;
        }

        QDomElement CModelMultipleFace::Serialize(QDomDocument& XmlDocument) const
        {
            QDomElement XmlElement = XmlDocument.createElement("MultipleFace");
            SerializeAttributes(XmlElement);
            QDomElement XmlFaces = XmlDocument.createElement("Faces");
            XmlElement.appendChild(XmlFaces);
            THierarchicalModelElementList<CModelFace*>::const_iterator EndFaces = m_Faces.end();
            for (THierarchicalModelElementList<CModelFace*>::const_iterator ppModelFace = m_Faces.begin(); ppModelFace != EndFaces; ++ppModelFace)
            {
                XmlFaces.appendChild((*ppModelFace)->SerializeReference(XmlDocument));
            }
            return XmlElement;
        }

        bool CModelMultipleFace::DeserializeAttributes(QDomElement& XmlElement)
        {
            if (CHierarchicalModelComposition::DeserializeAttributes(XmlElement) && XmlElement.hasAttribute("NX") && XmlElement.hasAttribute("NY") && XmlElement.hasAttribute("NZ"))
            {
                m_Normal.SetValue(real(XmlElement.attribute("NX").toDouble()), real(XmlElement.attribute("NY").toDouble()), real(XmlElement.attribute("NZ").toDouble()));
                return true;
            }
            return false;
        }

        void CModelMultipleFace::SerializeAttributes(QDomElement& XmlElement) const
        {
            CHierarchicalModelComposition::SerializeAttributes(XmlElement);
            XmlElement.setAttribute("NX", m_Normal.GetX());
            XmlElement.setAttribute("NY", m_Normal.GetY());
            XmlElement.setAttribute("NZ", m_Normal.GetZ());
        }

        uint CModelMultipleFace::Expansion(CModelFace* pFace, const real OrientationTolerance)
        {
            if (pFace && (!pFace->GetMultipleFace()))
            {
                m_Normal = pFace->GetNormal();
                AddFace(pFace);
                THierarchicalModelElementList<CModelFace*> ExpansionList;
                ExpansionList.push_back(pFace);
                while (ExpansionList.size())
                {
                    CModelFace* pExpandingModelFace = ExpansionList.PopFront();
                    THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = pExpandingModelFace->GetEdges().end();
                    for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = pExpandingModelFace->GetEdges().begin(); ppModelEdge != EndEdges; ++ppModelEdge)
                        if ((*ppModelEdge)->HasBothFaces())
                        {
                            CModelFace* pFaceAlpha = (*ppModelEdge)->GetWritableFaceAlpha();
                            if ((!pFaceAlpha->GetMultipleFace()) && (pFaceAlpha->GetNormal().ScalarProduct(m_Normal) > OrientationTolerance))
                            {
                                AddFace(pFaceAlpha);
                                ExpansionList.push_back(pFaceAlpha);
                            }
                            CModelFace* pFaceBeta = (*ppModelEdge)->GetWritableFaceBeta();
                            if ((!pFaceBeta->GetMultipleFace()) && (pFaceBeta->GetNormal().ScalarProduct(m_Normal) > OrientationTolerance))
                            {
                                AddFace(pFaceBeta);
                                ExpansionList.push_back(pFaceBeta);
                            }
                        }
                }
            }
            return m_Faces.size();
        }

        void CModelMultipleFace::Extraction()
        {
            m_Centeroid /= m_Area;
            THierarchicalModelElementList<CModelFace*>::const_iterator EndFaces = m_Faces.end();
            for (THierarchicalModelElementList<CModelFace*>::const_iterator ppModelFace = m_Faces.begin(); ppModelFace != EndFaces; ++ppModelFace)
            {
                THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = (*ppModelFace)->GetEdges().end();
                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = (*ppModelFace)->GetEdges().begin(); ppModelEdge != EndEdges; ++ppModelEdge)
                    if (!(*ppModelEdge)->IsFaceRedundant())
                    {
                        m_Edges.push_back(*ppModelEdge);
                    }
            }
            m_Edges.SortedUnique();
            THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = m_Edges.end();
            for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = m_Edges.begin(); ppModelEdge != EndEdges; ++ppModelEdge)
            {
                m_Veritices.push_back((*ppModelEdge)->GetWritableVertexA());
                m_Veritices.push_back((*ppModelEdge)->GetWritableVertexB());
            }
            m_Veritices.SortedUnique();
        }

        void CModelMultipleFace::AddFace(CModelFace* pFace)
        {
            pFace->SetMultipleFace(this);
            m_Area += pFace->GetArea();
            m_Centeroid += pFace->GetCenteroid() * pFace->GetArea();
            m_Faces.AddElement(pFace, false);
        }
    }
}
#endif //_EVP_USE_MODEL_SPACE
