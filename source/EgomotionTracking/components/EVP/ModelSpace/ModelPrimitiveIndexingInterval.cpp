/*
 * ModelPrimitiveIndexingInterval.cpp
 *
 *  Created on: 24.02.2012
 *      Author: gonzalez
 */

#include "ModelPrimitiveIndexingInterval.h"
#include "ModelPrimitiveIndexer.h"

namespace EVP
{
    namespace ModelSpace
    {
        CModelPrimitiveIndexingInterval::CModelPrimitiveIndexingInterval(CModelPrimitiveIndexer* pModelPrimitiveIndexer, const Identifier QueryId) :
            m_pModelPrimitiveIndexer(pModelPrimitiveIndexer), m_QueryId(QueryId), m_IndexA(0), m_IndexB(0), m_Length(0), m_AccumulatedProbability(_REAL_ZERO_)
        {
        }

        CModelPrimitiveIndexingInterval::CModelPrimitiveIndexingInterval(CModelPrimitiveIndexer* pModelPrimitiveIndexer, const Identifier QueryId, const uint IndexA, const uint IndexB, const uint Size) :
            m_pModelPrimitiveIndexer(pModelPrimitiveIndexer), m_QueryId(QueryId), m_IndexA(TMin(IndexA, IndexB)), m_IndexB(TMax(IndexA, IndexB)), m_Length((m_IndexB - m_IndexA) + 1), m_AccumulatedProbability(real(m_Length) / real(Size))
        {
        }

        CModelPrimitiveIndexingInterval::~CModelPrimitiveIndexingInterval()
            = default;

        const CModelPrimitiveIndexer* CModelPrimitiveIndexingInterval::GetIndexer() const
        {
            return m_pModelPrimitiveIndexer;
        }

        Identifier CModelPrimitiveIndexingInterval::GetQueryId() const
        {
            return m_QueryId;
        }

        bool CModelPrimitiveIndexingInterval::IsEmpty() const
        {
            return !m_Length;
        }

        bool CModelPrimitiveIndexingInterval::HasElements() const
        {
            return m_Length;
        }

        uint CModelPrimitiveIndexingInterval::GetLenght() const
        {
            return m_Length;
        }

        uint CModelPrimitiveIndexingInterval::GetIndexA() const
        {
            return m_IndexA;
        }

        uint CModelPrimitiveIndexingInterval::GetIndexB() const
        {
            return m_IndexB;
        }

        real CModelPrimitiveIndexingInterval::GetAccumulatedProbability() const
        {
            return m_AccumulatedProbability;
        }
    }
}
