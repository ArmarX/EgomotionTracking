/*
 * ModelSpaceContainedElementVisualization.cpp
 *
 *  Created on: 16.02.2012
 *      Author: gonzalez
 */

#include "ModelSpaceContainedElementVisualization.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        namespace Visualization
        {
            CModelSpaceContainedElementVisualization::CModelSpaceContainedElementVisualization(CHierarchicalModelElement* pHierarchicalModelElement, CModelSpaceContainerVisualization* pModelSpaceContainerVisualization) :
                CModelSpaceElementVisualization(pHierarchicalModelElement), m_pModelSpaceContainerVisualization(pModelSpaceContainerVisualization)
            {
                Create();
            }

            CModelSpaceContainedElementVisualization::~CModelSpaceContainedElementVisualization()
                = default;

            void CModelSpaceContainedElementVisualization::SetVisible(const VisibilityType Visibility)
            {
                if (m_Visibility != Visibility)
                {
                    if (m_Visibility == eDynamicContainedElement)
                    {
                        m_Visibility = Visibility;
                        if (m_pModelSpaceContainerVisualization)
                        {
                            UpdateVisibility();
                        }
                        else
                        {
                            m_pSwitch->whichChild = SO_SWITCH_ALL;
                        }
                    }
                    else
                    {
                        CModelSpaceElementVisualization::SetVisible(Visibility);
                    }
                }
            }

            const CModelSpaceContainerVisualization* CModelSpaceContainedElementVisualization::GetSpaceContainerVisualization() const
            {
                return m_pModelSpaceContainerVisualization;
            }

            void CModelSpaceContainedElementVisualization::Create()
            {
                if (m_pModelSpaceContainerVisualization)
                {
                    m_pModelSpaceContainerVisualization->AddElementAtConstructor(this);
                }
            }
        }
    }
}
#endif
