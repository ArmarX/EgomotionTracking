/*
 * ModelMeshVisualization.cpp
 *
 *  Created on: 08.02.2012
 *      Author: gonzalez
 */

#include "ModelMeshVisualization.h"
#include "../ModelMesh.h"
#include "../ModelVertex.h"
#include "../ModelEdge.h"
#include "../ModelFace.h"
#include "../ModelMultipleFace.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        namespace Visualization
        {
            CModelMeshVisualization::CModelMeshVisualization(CModelMesh* pModelMesh, CModelSpaceContainerVisualization* pModelSpaceContainerVisualization) :
                CModelSpaceContainedElementVisualization(pModelMesh, pModelSpaceContainerVisualization), m_pCoordinate3(nullptr), m_pSwitchVertices(nullptr), m_pSwitchEdges(nullptr), m_pSwitchFaces(nullptr), m_pSwitchMultiFaces(nullptr), m_VerticesVisibility(eNone), m_EdgesVisibility(eStaticContainedElementAndProps), m_FacesVisibility(eNone), m_MultiFacesVisibility(eNone)
            {

                Create();
            }

            CModelMeshVisualization::~CModelMeshVisualization()
            {
                Destroy();
            }

            void CModelMeshVisualization::SetVerticesVisible(const CModelSpaceElementVisualization::VisibilityType Visibility)
            {
                if (m_VerticesVisibility != Visibility)
                {
                    m_VerticesVisibility = Visibility;
                    switch (m_VerticesVisibility)
                    {
                        case eNone:
                            m_pSwitchVertices->whichChild = SO_SWITCH_NONE;
                            break;
                        case eDynamicContainedElement:
                            if (m_pModelSpaceContainerVisualization)
                            {
                                UpdateVerticesVisibility();
                            }
                            else
                            {
                                m_pSwitchVertices->whichChild = SO_SWITCH_ALL;
                            }
                            break;
                        case eStaticContainedElement:
                        case eStaticContainedElementAndProps:
                            m_pSwitchVertices->whichChild = SO_SWITCH_ALL;
                            break;
                    }
                }
            }

            void CModelMeshVisualization::SetEdgesVisible(const CModelSpaceElementVisualization::VisibilityType Visibility)
            {
                if (m_EdgesVisibility != Visibility)
                {
                    m_EdgesVisibility = Visibility;
                    switch (m_EdgesVisibility)
                    {
                        case eNone:
                            m_pSwitchEdges->whichChild = SO_SWITCH_NONE;
                            break;
                        case eDynamicContainedElement:
                            if (m_pModelSpaceContainerVisualization)
                            {
                                UpdateEdgesVisibility();
                            }
                            else
                            {
                                m_pSwitchEdges->whichChild = SO_SWITCH_ALL;
                            }
                            break;
                        case eStaticContainedElement:
                        case eStaticContainedElementAndProps:
                            m_pSwitchEdges->whichChild = SO_SWITCH_ALL;
                            break;
                    }
                }
            }

            void CModelMeshVisualization::SetFacesVisible(const CModelSpaceElementVisualization::VisibilityType Visibility)
            {
                if (m_FacesVisibility != Visibility)
                {
                    m_FacesVisibility = Visibility;
                    switch (m_FacesVisibility)
                    {
                        case eNone:
                            m_pSwitchFaces->whichChild = SO_SWITCH_NONE;
                            break;
                        case eDynamicContainedElement:
                            if (m_pModelSpaceContainerVisualization)
                            {
                                UpdateFacesVisibility();
                            }
                            else
                            {
                                m_pSwitchFaces->whichChild = SO_SWITCH_ALL;
                            }
                            break;
                        case eStaticContainedElement:
                        case eStaticContainedElementAndProps:
                            m_pSwitchFaces->whichChild = SO_SWITCH_ALL;
                            break;
                    }
                }
            }

            void CModelMeshVisualization::SetMultipleFacesVisible(const CModelSpaceElementVisualization::VisibilityType Visibility)
            {
                if (m_MultiFacesVisibility != Visibility)
                {
                    m_MultiFacesVisibility = Visibility;
                    switch (m_MultiFacesVisibility)
                    {
                        case eNone:
                            m_pSwitchMultiFaces->whichChild = SO_SWITCH_NONE;
                            break;
                        case eDynamicContainedElement:
                            if (m_pModelSpaceContainerVisualization)
                            {
                                UpdateMultiFacesVisibility();
                            }
                            else
                            {
                                m_pSwitchMultiFaces->whichChild = SO_SWITCH_ALL;
                            }
                            break;
                        case eStaticContainedElement:
                        case eStaticContainedElementAndProps:
                            m_pSwitchMultiFaces->whichChild = SO_SWITCH_ALL;
                            break;
                    }
                }
            }

            const SbBox3f& CModelMeshVisualization::GetBoundingBox() const
            {
                return m_Box;
            }

            uint CModelMeshVisualization::UpdateVerticesVisibility()
            {
                uint VerticesVisible = 0;
                if (m_VertexVisualizations.size())
                {
                    TModelList<CModelVertexVisualization*>::const_iterator EndVertices = m_VertexVisualizations.end();
                    for (TModelList<CModelVertexVisualization*>::const_iterator ppVertexVisualization = m_VertexVisualizations.begin(); ppVertexVisualization != EndVertices; ++ppVertexVisualization)
                        if ((*ppVertexVisualization)->UpdateVisibility())
                        {
                            ++VerticesVisible;
                        }
                }
                m_pSwitchVertices->whichChild = VerticesVisible ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                return VerticesVisible;
            }

            uint CModelMeshVisualization::UpdateEdgesVisibility()
            {
                uint EdgesVisible = 0;
                if (m_EdgeVisualizations.size())
                {
                    TModelList<CModelEdgeVisualization*>::const_iterator EndEdge = m_EdgeVisualizations.end();
                    for (TModelList<CModelEdgeVisualization*>::const_iterator ppEdgeVisualization = m_EdgeVisualizations.begin(); ppEdgeVisualization != EndEdge; ++ppEdgeVisualization)
                        if ((*ppEdgeVisualization)->UpdateVisibility())
                        {
                            ++EdgesVisible;
                        }
                }
                m_pSwitchEdges->whichChild = EdgesVisible ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                return EdgesVisible;
            }

            uint CModelMeshVisualization::UpdateFacesVisibility()
            {
                uint FacesVisible = 0;
                if (m_FaceVisualizations.size())
                {
                    TModelList<CModelFaceVisualization*>::const_iterator EndFaces = m_FaceVisualizations.end();
                    for (TModelList<CModelFaceVisualization*>::const_iterator ppFaceVisualization = m_FaceVisualizations.begin(); ppFaceVisualization != EndFaces; ++ppFaceVisualization)
                        if ((*ppFaceVisualization)->UpdateVisibility())
                        {
                            ++FacesVisible;
                        }
                }
                m_pSwitchFaces->whichChild = FacesVisible ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                return FacesVisible;
            }

            uint CModelMeshVisualization::UpdateMultiFacesVisibility()
            {
                uint MultiFacesVisible = 0;
                if (m_MultiFaceVisualizations.size())
                {
                    TModelList<CModelMultiFaceVisualization*>::const_iterator EndMultiFaces = m_MultiFaceVisualizations.end();
                    for (TModelList<CModelMultiFaceVisualization*>::const_iterator ppMultiFaceVisualization = m_MultiFaceVisualizations.begin(); ppMultiFaceVisualization != EndMultiFaces; ++ppMultiFaceVisualization)
                        if ((*ppMultiFaceVisualization)->UpdateVisibility())
                        {
                            ++MultiFacesVisible;
                        }
                }
                m_pSwitchMultiFaces->whichChild = MultiFacesVisible ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                return MultiFacesVisible;
            }

            bool CModelMeshVisualization::UpdateVisibility()
            {
                uint ElementsVisible = 0;
                if (m_pModelSpaceContainerVisualization->IsBoxContained(m_Box))
                {
                    ElementsVisible = UpdateVerticesVisibility() + UpdateEdgesVisibility() + UpdateEdgesVisibility() + UpdateFacesVisibility() + UpdateMultiFacesVisibility();
                }
                m_pSwitch->whichChild = ElementsVisible ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                return ElementsVisible;
            }

            void CModelMeshVisualization::Create()
            {
                m_pCoordinate3 = AddToSeparator<SoCoordinate3> ();
                m_pSwitchVertices = AddToSeparator<SoSwitch> ();
                m_pSwitchEdges = AddToSeparator<SoSwitch> ();
                m_pSwitchFaces = AddToSeparator<SoSwitch> ();
                m_pSwitchMultiFaces = AddToSeparator<SoSwitch> ();
                m_pSwitchVertices->whichChild = SO_SWITCH_NONE;
                m_pSwitchEdges->whichChild = SO_SWITCH_ALL;
                m_pSwitchFaces->whichChild = SO_SWITCH_NONE;
                m_pSwitchMultiFaces->whichChild = SO_SWITCH_NONE;
                const CModelMesh* pModelMesh = GetHierarchicalModelPrimitive<CModelMesh> ();
                CreateVertices(pModelMesh->GetReadOnlyVeritices());
                CreateEdges(pModelMesh->GetReadOnlyEdges());
                CreateFaces(pModelMesh->GetReadOnlyFaces());
                CreateMultipleFaces(pModelMesh->GetReadOnlyMultipleFaces());
            }

            void CModelMeshVisualization::CreateVertices(const THierarchicalModelElementList<CModelVertex*>& Vertices)
            {
                m_Box.makeEmpty();
                THierarchicalModelElementList<CModelVertex*>::const_iterator EndVertices = Vertices.end();
                for (THierarchicalModelElementList<CModelVertex*>::const_iterator ppModelVertex = Vertices.begin(); ppModelVertex != EndVertices; ++ppModelVertex)
                {
                    CModelVertex* pModelVertex = *ppModelVertex;
                    const SbVec3f Point = EVP::Visualization::Geometry::_3D::OpenInventor::T(pModelVertex->GetPoint());
                    m_pCoordinate3->point.set1Value(pModelVertex->GetIndexInMesh(), Point);
                    m_Box.extendBy(Point);
                    CModelVertexVisualization* pModelVertexVisualization = new CModelVertexVisualization(pModelVertex, m_pModelSpaceContainerVisualization);
                    m_pSwitchVertices->addChild(pModelVertexVisualization->GetSwitch());
                    m_VertexVisualizations.AddElement(pModelVertexVisualization, false);
                }
            }

            void CModelMeshVisualization::CreateEdges(const THierarchicalModelElementList<CModelEdge*>& Edges)
            {
                THierarchicalModelElementList<CModelEdge*>::const_iterator EndEdges = Edges.end();
                for (THierarchicalModelElementList<CModelEdge*>::const_iterator ppModelEdge = Edges.begin(); ppModelEdge != EndEdges; ++ppModelEdge)
                {
                    CModelEdgeVisualization* pModelEdgeVisualization = new CModelEdgeVisualization(*ppModelEdge, m_pModelSpaceContainerVisualization);
                    m_pSwitchEdges->addChild(pModelEdgeVisualization->GetSwitch());
                    m_EdgeVisualizations.AddElement(pModelEdgeVisualization, false);
                }
            }

            void CModelMeshVisualization::CreateFaces(const THierarchicalModelElementList<CModelFace*>& Faces)
            {
                THierarchicalModelElementList<CModelFace*>::const_iterator EndFaces = Faces.end();
                for (THierarchicalModelElementList<CModelFace*>::const_iterator ppModelFace = Faces.begin(); ppModelFace != EndFaces; ++ppModelFace)
                    if (!(*ppModelFace)->GetMultipleFace())
                    {
                        CModelFaceVisualization* pModelFaceVisualization = new CModelFaceVisualization(*ppModelFace, m_pModelSpaceContainerVisualization);
                        m_pSwitchFaces->addChild(pModelFaceVisualization->GetSwitch());
                        m_FaceVisualizations.AddElement(pModelFaceVisualization, false);
                    }
            }

            void CModelMeshVisualization::CreateMultipleFaces(const THierarchicalModelElementList<CModelMultipleFace*>& MultipleFaces)
            {
                THierarchicalModelElementList<CModelMultipleFace*>::const_iterator EndFaces = MultipleFaces.end();
                for (THierarchicalModelElementList<CModelMultipleFace*>::const_iterator ppModelMultipleFace = MultipleFaces.begin(); ppModelMultipleFace != EndFaces; ++ppModelMultipleFace)
                {
                    CModelMultiFaceVisualization* pModelMultiFaceVisualization = new CModelMultiFaceVisualization(*ppModelMultipleFace, m_pModelSpaceContainerVisualization);
                    m_pSwitchMultiFaces->addChild(pModelMultiFaceVisualization->GetSwitch());
                    m_MultiFaceVisualizations.AddElement(pModelMultiFaceVisualization, false);
                }
            }

            void CModelMeshVisualization::Destroy()
            {
                DestroyVertices();
                DestroyEdges();
                DestroyFaces();
                DestroyMultipleFaces();
            }

            void CModelMeshVisualization::DestroyVertices()
            {
                if (m_VertexVisualizations.size())
                {
                    TModelList<CModelVertexVisualization*>::const_iterator EndVertices = m_VertexVisualizations.end();
                    for (TModelList<CModelVertexVisualization*>::const_iterator ppVertexVisualization = m_VertexVisualizations.begin(); ppVertexVisualization != EndVertices; ++ppVertexVisualization)
                    {
                        delete *ppVertexVisualization;
                    }
                    m_VertexVisualizations.clear();
                    m_pSwitchVertices->removeAllChildren();
                }
            }

            void CModelMeshVisualization::DestroyEdges()
            {
                if (m_EdgeVisualizations.size())
                {
                    TModelList<CModelEdgeVisualization*>::const_iterator EndEdge = m_EdgeVisualizations.end();
                    for (TModelList<CModelEdgeVisualization*>::const_iterator ppEdgeVisualization = m_EdgeVisualizations.begin(); ppEdgeVisualization != EndEdge; ++ppEdgeVisualization)
                    {
                        delete *ppEdgeVisualization;
                    }
                    m_EdgeVisualizations.clear();
                    m_pSwitchEdges->removeAllChildren();
                }
            }

            void CModelMeshVisualization::DestroyFaces()
            {
                if (m_FaceVisualizations.size())
                {
                    TModelList<CModelFaceVisualization*>::const_iterator EndFaces = m_FaceVisualizations.end();
                    for (TModelList<CModelFaceVisualization*>::const_iterator ppFaceVisualization = m_FaceVisualizations.begin(); ppFaceVisualization != EndFaces; ++ppFaceVisualization)
                    {
                        delete *ppFaceVisualization;
                    }
                    m_FaceVisualizations.clear();
                    m_pSwitchFaces->removeAllChildren();
                }
            }

            void CModelMeshVisualization::DestroyMultipleFaces()
            {
                if (m_MultiFaceVisualizations.size())
                {
                    TModelList<CModelMultiFaceVisualization*>::const_iterator EndMultiFaces = m_MultiFaceVisualizations.end();
                    for (TModelList<CModelMultiFaceVisualization*>::const_iterator ppMultiFaceVisualization = m_MultiFaceVisualizations.begin(); ppMultiFaceVisualization != EndMultiFaces; ++ppMultiFaceVisualization)
                    {
                        delete *ppMultiFaceVisualization;
                    }
                    m_MultiFaceVisualizations.clear();
                    m_pSwitchMultiFaces->removeAllChildren();
                }
            }
        }
    }
}
#endif
