/*
 * ModelVertexVisualization.h
 *
 *  Created on: 08.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "ModelSpaceContainedElementVisualization.h"
#include "../../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelVertex;

        namespace Visualization
        {
            class CModelVertexVisualization: public CModelSpaceContainedElementVisualization
            {
            public:

                CModelVertexVisualization(CModelVertex* pModelVertex, CModelSpaceContainerVisualization* pModelSpaceContainerVisualization);
                ~CModelVertexVisualization() override;

                void SetDisplayRadius(const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DisplayRadius);
                EVP::Visualization::Geometry::_3D::OpenInventor::vreal GetDisplayaRadius() const;

                virtual void SetComplexityType(const SoComplexity::Type Type);
                virtual  SoComplexity::Type GetComplexityType() const;

                virtual void SetComplexityValue(const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Complexity);
                virtual  EVP::Visualization::Geometry::_3D::OpenInventor::vreal GetComplexityValue() const;

            protected:

                friend class CModelMeshVisualization;

                bool UpdateVisibility() override;

            private:

                void Create() override;

                SoComplexity* m_pComplexity;
                SoTranslation* m_pTranslation;
                SoSphere* m_pSphere;
            };
        }
    }
}

#endif
