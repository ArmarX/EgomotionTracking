/*
 * ModelMultiFaceVisualization.h
 *
 *  Created on: 08.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "ModelSpaceContainedElementVisualization.h"
#include "ModelFaceVisualization.h"
#include "../ModelList.h"
#include "../../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelMultipleFace;

        namespace Visualization
        {
            class CModelMultiFaceVisualization: public CModelSpaceContainedElementVisualization
            {
            public:

                CModelMultiFaceVisualization(CModelMultipleFace* pModelMultipleFace, CModelSpaceContainerVisualization* pModelSpaceContainerVisualization);
                ~CModelMultiFaceVisualization() override;

            protected:

                friend class CModelMeshVisualization;

                bool UpdateVisibility() override;

            private:

                void Create() override;
                void Destroy() override;

                TModelList<CModelFaceVisualization*> m_FaceVisualizations;
            };
        }
    }
}
#endif
