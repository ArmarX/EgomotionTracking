/*
 * ModelEdgeVisualization.cpp
 *
 *  Created on: 08.02.2012
 *      Author: gonzalez
 */

#include "ModelEdgeVisualization.h"
#include "../ModelEdge.h"
#include "../ModelVertex.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        namespace Visualization
        {
            CModelEdgeVisualization::CModelEdgeVisualization(CModelEdge* pModelEdge, CModelSpaceContainerVisualization* pModelSpaceContainerVisualization) :
                CModelSpaceContainedElementVisualization(pModelEdge, pModelSpaceContainerVisualization), m_DisplayingRedundant(false), m_pIndexedLineSet(nullptr)
            {
                Create();
            }

            CModelEdgeVisualization::~CModelEdgeVisualization()
                = default;

            void CModelEdgeVisualization::SetDisplayingRedundant(const bool DisplayRedundant)
            {
                if (m_DisplayingRedundant != DisplayRedundant)
                {
                    m_DisplayingRedundant = DisplayRedundant;
                    UpdateVisibility();
                }
            }

            bool CModelEdgeVisualization::IsDisplayingRedundant() const
            {
                return m_DisplayingRedundant;
            }

            bool CModelEdgeVisualization::UpdateVisibility()
            {
                const CModelEdge* pModelEdge = GetHierarchicalModelPrimitive<CModelEdge>();
                if ((!m_DisplayingRedundant) && pModelEdge->IsFaceRedundant())
                {
                    m_pSwitch->whichChild = SO_SWITCH_NONE;
                    return false;
                }
                if (m_pModelSpaceContainerVisualization->IsLineSegmentContained(EVP::Visualization::Geometry::_3D::OpenInventor::T(pModelEdge->GetReadOnlyVertexA()->GetPoint()), EVP::Visualization::Geometry::_3D::OpenInventor::T(pModelEdge->GetReadOnlyVertexB()->GetPoint())))
                {
                    m_pSwitch->whichChild = SO_SWITCH_ALL;
                    return true;
                }
                else
                {
                    m_pSwitch->whichChild = SO_SWITCH_NONE;
                    return false;
                }
            }

            void CModelEdgeVisualization::Create()
            {

                const CModelEdge* pModelEdge = GetHierarchicalModelPrimitive<CModelEdge>();
                m_pIndexedLineSet = AddToSeparator<SoIndexedLineSet>();

                m_pIndexedLineSet->coordIndex.set1Value(0, pModelEdge->GetReadOnlyVertexA()->GetIndexInMesh());
                m_pIndexedLineSet->coordIndex.set1Value(1, pModelEdge->GetReadOnlyVertexB()->GetIndexInMesh());

                if (m_DisplayingRedundant)
                {
                    m_pSwitch->whichChild = SO_SWITCH_ALL;
                }
                else
                {
                    m_pSwitch->whichChild = pModelEdge->IsFaceRedundant() ? SO_SWITCH_NONE : SO_SWITCH_ALL;
                }

            }

            SoNode* CModelEdgeVisualization::GetSelectionNode()
            {
                return m_pIndexedLineSet;
            }
        }
    }
}

#endif
