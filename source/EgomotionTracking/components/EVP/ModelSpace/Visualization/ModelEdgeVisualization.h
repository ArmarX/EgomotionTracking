/*
 * ModelEdgeVisualization.h
 *
 *  Created on: 08.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "ModelSpaceContainedElementVisualization.h"
#include "../../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelEdge;
        class CModelMeshVisualization;

        namespace Visualization
        {
            class CModelEdgeVisualization: public CModelSpaceContainedElementVisualization
            {
            public:

                CModelEdgeVisualization(CModelEdge* pModelEdge, CModelSpaceContainerVisualization* pModelSpaceContainerVisualization);
                ~CModelEdgeVisualization() override;

                void SetDisplayingRedundant(const bool DisplayRedundant);
                bool IsDisplayingRedundant() const;

                SoNode* GetSelectionNode() override;
            protected:

                friend class CModelMeshVisualization;

                bool UpdateVisibility() override;

            private:

                void Create() override;

                bool m_DisplayingRedundant;
                SoIndexedLineSet* m_pIndexedLineSet;
            };
        }
    }
}
#endif

