/*
 * ModelMultipleMeshVisualization.h
 *
 *  Created on: 08.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "ModelSpaceContainedElementVisualization.h"
#include "ModelMeshVisualization.h"
#include "../ModelList.h"
#include "../../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelMultipleMesh;
        class CModelMeshVisualization;

        namespace Visualization
        {
            class CModelMultipleMeshVisualization: public CModelSpaceContainedElementVisualization
            {
            public:

                CModelMultipleMeshVisualization(CModelMultipleMesh* pModelMultipleMesh, CModelSpaceContainerVisualization* pModelSpaceContainerVisualization);
                ~CModelMultipleMeshVisualization() override;

                const SbBox3f& GetBoundingBox() const;

            protected:

                bool UpdateVisibility() override;

            private:

                void Create() override;
                void Destroy() override;

                SbBox3f m_Box;
                TModelList<CModelMeshVisualization*> m_MeshVisualizations;
            };
        }
    }
}
#endif
