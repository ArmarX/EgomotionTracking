/*
 * ModelFaceVisualization.h
 *
 *  Created on: 08.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "ModelSpaceContainedElementVisualization.h"
#include "../../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelFace;

        namespace Visualization
        {
            class CModelFaceVisualization: public CModelSpaceContainedElementVisualization
            {
            public:

                enum DisplayMode
                {
                    eFaceNormal, eVertexNormal
                };

                CModelFaceVisualization(CModelFace* pModelFace, CModelSpaceContainerVisualization* pModelSpaceContainerVisualization);
                ~CModelFaceVisualization() override;

                virtual void SetDisplayMode(const DisplayMode Mode);

            protected:

                friend class CModelMeshVisualization;
                friend class CModelMultiFaceVisualization;

                bool UpdateVisibility() override;

            private:

                void Create() override;

                SoNormal* m_pNormal;
                SoNormalBinding* m_pNormalBinding;
                SoIndexedFaceSet* m_pIndexedFaceSet;
            };
        }
    }
}
#endif
