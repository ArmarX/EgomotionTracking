/*
 * ModelMultiFaceVisualization.cpp
 *
 *  Created on: 08.02.2012
 *      Author: gonzalez
 */

#include "ModelMultiFaceVisualization.h"
#include "../ModelMultipleFace.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        namespace Visualization
        {
            CModelMultiFaceVisualization::CModelMultiFaceVisualization(CModelMultipleFace* pModelMultipleFace, CModelSpaceContainerVisualization* pModelSpaceContainerVisualization) :
                CModelSpaceContainedElementVisualization(pModelMultipleFace, pModelSpaceContainerVisualization)
            {
                Create();
            }

            CModelMultiFaceVisualization::~CModelMultiFaceVisualization()
            {
                Destroy();
            }

            bool CModelMultiFaceVisualization::UpdateVisibility()
            {
                if (m_FaceVisualizations.size())
                {
                    uint VisibleFaces = 0;
                    TModelList<CModelFaceVisualization*>::const_iterator EndFaces = m_FaceVisualizations.end();
                    for (TModelList<CModelFaceVisualization*>::const_iterator ppFaceVisualization = m_FaceVisualizations.begin(); ppFaceVisualization != EndFaces; ++ppFaceVisualization)
                        if ((*ppFaceVisualization)->UpdateVisibility())
                        {
                            ++VisibleFaces;
                        }
                    if (VisibleFaces)
                    {
                        m_pSwitch->whichChild = SO_SWITCH_ALL;
                        return true;
                    }
                }
                m_pSwitch->whichChild = SO_SWITCH_NONE;
                return false;
            }

            void CModelMultiFaceVisualization::Create()
            {
                const CModelMultipleFace* pModelMultipleFace = GetHierarchicalModelPrimitive<CModelMultipleFace> ();
                THierarchicalModelElementList<CModelFace*>::const_iterator EndFaces = pModelMultipleFace->GetFaces().end();
                for (THierarchicalModelElementList<CModelFace*>::const_iterator ppModelFace = pModelMultipleFace->GetFaces().begin(); ppModelFace != EndFaces; ++ppModelFace)
                {
                    CModelFaceVisualization* pModelFaceVisualization = new CModelFaceVisualization(*ppModelFace, m_pModelSpaceContainerVisualization);
                    AddSubElement(pModelFaceVisualization);
                    m_FaceVisualizations.AddElement(pModelFaceVisualization, false);
                }
            }

            void CModelMultiFaceVisualization::Destroy()
            {
                if (m_FaceVisualizations.size())
                {
                    TModelList<CModelFaceVisualization*>::const_iterator EndFaces = m_FaceVisualizations.end();
                    for (TModelList<CModelFaceVisualization*>::const_iterator ppFaceVisualization = m_FaceVisualizations.begin(); ppFaceVisualization != EndFaces; ++ppFaceVisualization)
                    {
                        delete *ppFaceVisualization;
                    }
                    m_FaceVisualizations.clear();
                }
            }
        }
    }
}
#endif
