/*
 * ModelSpaceElementVisualization.h
 *
 *  Created on: 08.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "../../Visualization/Geometry/3D/OpenInventor/OpenInventor.h"
#include "../../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CHierarchicalModelElement;

        namespace Visualization
        {
            class CModelSpaceElementVisualization
            {
            public:

                enum VisibilityType
                {
                    eNone, eDynamicContainedElement, eStaticContainedElement, eStaticContainedElementAndProps
                };

                CModelSpaceElementVisualization(CHierarchicalModelElement* pHierarchicalModelElement);
                virtual ~CModelSpaceElementVisualization();

                SoSwitch* GetSwitch();
                virtual SoNode* GetSelectionNode();

                CHierarchicalModelElement* GetHierarchicalModelPrimitive();

                virtual void SetVisible(const VisibilityType Visibility);
                virtual  VisibilityType GetVisible() const;

                virtual void SetColor(const EVP::Visualization::Geometry::_3D::OpenInventor::vreal R, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal G, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal B);
                virtual void SetColor(const SbColor& Color);
                virtual const SbColor& GetColor() const;

                virtual void SetTransparency(const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Transparency);
                virtual  EVP::Visualization::Geometry::_3D::OpenInventor::vreal GetTransparency() const;

                virtual void SetDrawStyle(SoDrawStyle::Style Style);
                virtual  SoDrawStyle::Style GetDrawStyle() const;

                virtual void SetLineWidth(const EVP::Visualization::Geometry::_3D::OpenInventor::vreal LineWidth);
                virtual  EVP::Visualization::Geometry::_3D::OpenInventor::vreal GetLineWidth() const;

                virtual void SetPointSize(const EVP::Visualization::Geometry::_3D::OpenInventor::vreal PointSize);
                virtual  EVP::Visualization::Geometry::_3D::OpenInventor::vreal GetPointSize() const;

                virtual void SetLinePattern(const ushort Pattern);
                virtual  ushort GetLinePattern() const;

                virtual  bool AddProp(SoSeparator* pProSeparator);
                virtual void ClearProps();

            protected:

                friend class CModelSpaceContainerVisualization;

                virtual  bool UpdateVisibility() = 0;
                virtual  bool AddSubElement(CModelSpaceElementVisualization* pModelSpaceElementVisualization);
                virtual void Create();
                virtual void Destroy();

                template<class ModelPrimitive> const ModelPrimitive* GetHierarchicalModelPrimitive() const
                {
                    return dynamic_cast<const ModelPrimitive*>(m_pHierarchicalModelElement);
                }

                template<class NodeType> NodeType* AddToSeparator()
                {
                    NodeType* pInstance = new NodeType;
                    SoNode* pNode = dynamic_cast<SoNode*>(pInstance);
                    m_pSeparator->addChild(pNode);
                    pNode->setUserData(this);
                    return pInstance;
                }

                CHierarchicalModelElement* m_pHierarchicalModelElement;
                VisibilityType m_Visibility;
                SoSwitch* m_pSwitch;
                SoSeparator* m_pSeparator;
                SoDrawStyle* m_pDrawStyle;
                SoMaterial* m_pMaterial;
            };
        }
    }
}
#endif
