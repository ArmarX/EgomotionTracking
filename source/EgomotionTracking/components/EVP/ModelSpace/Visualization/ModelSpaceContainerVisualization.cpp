/*
 * ModelSpaceContainerVisualization.cpp
 *
 *  Created on: 14.02.2012
 *      Author: gonzalez
 */

#include "ModelSpaceContainerVisualization.h"
#include "ModelSpaceElementVisualization.h"
#include "../HierarchicalModelPrimitive.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        namespace Visualization
        {
            const SbVec3f CModelSpaceContainerVisualization::m_AxisX(EVP::Visualization::Geometry::_3D::OpenInventor::vreal(1.0), EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0), EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0));
            const SbVec3f CModelSpaceContainerVisualization::m_AxisY(EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0), EVP::Visualization::Geometry::_3D::OpenInventor::vreal(1.0), EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0));
            const SbVec3f CModelSpaceContainerVisualization::m_AxisZ(EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0), EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0), EVP::Visualization::Geometry::_3D::OpenInventor::vreal(1.0));

            CModelSpaceContainerVisualization::CModelSpaceContainerVisualization() :
                CModelSpaceElementVisualization(nullptr), m_BoundingSpaceShape(eFull), m_ContainmentPolicy(eFullyContained), m_Radius(EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0)), m_pDisplaySwitch(nullptr), m_pTranslation(nullptr), m_pSphere(nullptr), m_pCube(nullptr)
            {
                Create();
            }

            CModelSpaceContainerVisualization::~CModelSpaceContainerVisualization()
                = default;

            void CModelSpaceContainerVisualization::SetBoundingSpaceShapeEmpty(const uint UpdatingLevelsFlag)
            {
                if (m_BoundingSpaceShape != eEmpty)
                {
                    m_BoundingSpaceShape = eEmpty;
                    Update(UpdatingLevelsFlag);
                    UpDateDisplay();
                }
            }

            void CModelSpaceContainerVisualization::SetBoundingSpaceShapeBox(const uint UpdatingLevelsFlag, const SbBox3f& Box)
            {
                if ((m_BoundingSpaceShape != eBox) || (m_Box != Box))
                {
                    m_BoundingSpaceShape = eBox;
                    m_Box = Box;
                    SetPlanes(m_Box, m_Planes);
                    Update(UpdatingLevelsFlag);
                    UpDateDisplay();
                }
            }

            void CModelSpaceContainerVisualization::SetBoundingSpaceShapeBox(const uint UpdatingLevelsFlag, const SbVec3f& Point, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DXM, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DYM, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DZM, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DXP, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DYP, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DZP)
            {
                SetBoundingSpaceShapeBox(UpdatingLevelsFlag, SbBox3f(Point[0] - vAbs(DXM), Point[1] - vAbs(DYM), Point[2] - vAbs(DZM), Point[0] + vAbs(DXP), Point[1] + vAbs(DYP), Point[2] + vAbs(DZP)));
            }

            void CModelSpaceContainerVisualization::SetBoundingSpaceShapeBox(const uint UpdatingLevelsFlag, const SbVec3f& PointA, const SbVec3f& PointB)
            {
                SetBoundingSpaceShapeBox(UpdatingLevelsFlag, SbBox3f(TMin(PointA[0], PointB[0]), TMin(PointA[1], PointB[1]), TMin(PointA[2], PointB[2]), TMax(PointA[0], PointB[0]), TMax(PointA[1], PointB[1]), TMax(PointA[2], PointB[2])));
            }

            void CModelSpaceContainerVisualization::SetBoundingSpaceShapeBox(const uint UpdatingLevelsFlag, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal X0, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Y0, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Z0, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal X1, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Y1, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Z1)
            {
                SetBoundingSpaceShapeBox(UpdatingLevelsFlag, SbBox3f(X0, Y0, Z0, X1, Y1, Z1));
            }

            void CModelSpaceContainerVisualization::SetBoundingSpaceShapeSphere(const uint UpdatingLevelsFlag, const SbVec3f& Center, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Radius)
            {
                if ((m_BoundingSpaceShape != eSphere) || (m_Center != Center) || (m_Radius != Radius))
                {
                    m_BoundingSpaceShape = eSphere;
                    m_Center = Center;
                    m_Radius = Radius;
                    Update(UpdatingLevelsFlag);
                    UpDateDisplay();
                }
            }

            void CModelSpaceContainerVisualization::SetBoundingSpaceShapeSphere(const uint UpdatingLevelsFlag, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal CX, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal CY, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal CZ, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Radius)
            {
                SetBoundingSpaceShapeSphere(UpdatingLevelsFlag, SbVec3f(CX, CY, CZ), Radius);
            }

            void CModelSpaceContainerVisualization::SetBoundingSpaceShapeFull(const uint UpdatingLevelsFlag)
            {
                if (m_BoundingSpaceShape != eFull)
                {
                    m_BoundingSpaceShape = eFull;
                    Update(UpdatingLevelsFlag);
                    UpDateDisplay();
                }
            }

            void CModelSpaceContainerVisualization::SetContainmentPolicy(const ContainmentPolicy Policy, const uint UpdatingLevelsFlag)
            {
                if (m_ContainmentPolicy != Policy)
                {
                    m_ContainmentPolicy = Policy;
                    Update(UpdatingLevelsFlag);
                    UpDateDisplay();
                }
            }

            void CModelSpaceContainerVisualization::AddElementAtConstructor(CModelSpaceElementVisualization* pModelSpaceElementVisualization)
            {
                m_Elements[pModelSpaceElementVisualization->GetHierarchicalModelPrimitive()->GetTypeId()].AddElement(pModelSpaceElementVisualization, false);
            }

            bool CModelSpaceContainerVisualization::AddElement(CModelSpaceElementVisualization* pModelSpaceElementVisualization)
            {
                if (pModelSpaceElementVisualization)
                {
                    return m_Elements[pModelSpaceElementVisualization->GetHierarchicalModelPrimitive()->GetTypeId()].AddElement(pModelSpaceElementVisualization, true);
                }
                return false;
            }

            void CModelSpaceContainerVisualization::Update(const uint UpdatingLevelsFlag)
            {
                if (UpdatingLevelsFlag)
                {
                    const uint Levels = uint(CModelElement::eMultiMesh) + 1;
                    for (uint i = 0; i < Levels; ++i)
                        if (UpdatingLevelsFlag & (0X1 << i))
                        {
                            Update(m_Elements[i]);
                        }
                }
            }

            CModelSpaceContainerVisualization::BoundingSpaceShape CModelSpaceContainerVisualization::GetBoundingSpaceShape() const
            {
                return m_BoundingSpaceShape;
            }

            CModelSpaceContainerVisualization::ContainmentPolicy CModelSpaceContainerVisualization::GetContainmentPolicy() const
            {
                return m_ContainmentPolicy;
            }

            bool CModelSpaceContainerVisualization::IsBoxContained(const SbBox3f& Box) const
            {
                switch (m_BoundingSpaceShape)
                {
                    case eEmpty:
                        return false;
                    case eBox:
                        switch (m_ContainmentPolicy)
                        {
                            case eFullyContained:
                            {
                                const SbVec3f& LocalMin = m_Box.getMin();
                                const SbVec3f& RemoteMin = Box.getMin();
                                if (!(LocalMin[0] < RemoteMin[0]) && (LocalMin[1] < RemoteMin[1]) && (LocalMin[2] < RemoteMin[2]))
                                {
                                    return false;
                                }
                                const SbVec3f& LocalMax = m_Box.getMax();
                                const SbVec3f& RemoteMax = Box.getMax();
                                return (LocalMax[0] > RemoteMax[0]) && (LocalMax[1] > RemoteMax[1]) && (LocalMax[2] > RemoteMax[2]);
                            }
                            case ePartiallyContained:
                                return m_Box.intersect(Box);
                        }
                        break;
                    case eSphere:
                    {
                        const SbVec3f Delta(m_Radius, m_Radius, m_Radius);
                        const SbBox3f SphereBox(m_Center - Delta, m_Center + Delta);
                        if (SphereBox.intersect(Box))
                            switch (m_ContainmentPolicy)
                            {
                                case eFullyContained:
                                {
                                    const SbVec3f HalfScope = (Box.getMax() - Box.getMin()) / EVP::Visualization::Geometry::_3D::OpenInventor::vreal(2.0);
                                    const SbVec3f Center = (Box.getMax() + Box.getMin()) / EVP::Visualization::Geometry::_3D::OpenInventor::vreal(2.0);
                                    const SbVec3f Deltas[3] = { m_AxisX* HalfScope[0], m_AxisY* HalfScope[1], m_AxisZ* HalfScope[2] };
                                    const SbVec3f LowerPlaneBase = Center - Deltas[2];
                                    const SbVec3f A = Deltas[0] + Deltas[1];
                                    if (((LowerPlaneBase + A) - m_Center).length() > m_Radius)
                                    {
                                        return false;
                                    }
                                    const SbVec3f B = Deltas[1] - Deltas[0];
                                    if (((LowerPlaneBase + B) - m_Center).length() > m_Radius)
                                    {
                                        return false;
                                    }
                                    if (((LowerPlaneBase - A) - m_Center).length() > m_Radius)
                                    {
                                        return false;
                                    }
                                    if (((LowerPlaneBase - B) - m_Center).length() > m_Radius)
                                    {
                                        return false;
                                    }
                                    const SbVec3f UpperPlaneBase = Center + Deltas[2];
                                    if (((UpperPlaneBase + A) - m_Center).length() > m_Radius)
                                    {
                                        return false;
                                    }
                                    if (((UpperPlaneBase + B) - m_Center).length() > m_Radius)
                                    {
                                        return false;
                                    }
                                    if (((UpperPlaneBase - A) - m_Center).length() > m_Radius)
                                    {
                                        return false;
                                    }
                                    if (((UpperPlaneBase - B) - m_Center).length() > m_Radius)
                                    {
                                        return false;
                                    }
                                    return true;
                                }
                                case ePartiallyContained:
                                {
                                    SbPlane Planes[6];
                                    SetPlanes(Box, Planes);
                                    for (uint i = 0; i < 6; ++i)
                                        if (Planes[i].getDistance(m_Center) < -m_Radius)
                                        {
                                            return false;
                                        }
                                    return true;
                                }
                            }
                        return false;
                    }
                    case eFull:
                        return true;
                }
                return true;
            }

            bool CModelSpaceContainerVisualization::IsPointContained(const SbVec3f& Point) const
            {
                switch (m_BoundingSpaceShape)
                {
                    case eEmpty:
                        return false;
                    case eBox:
                        return m_Box.intersect(Point);
                    case eSphere:
                        return (m_Center - Point).length() <= m_Radius;
                    case eFull:
                        return true;
                }
                return true;
            }

            bool CModelSpaceContainerVisualization::IsLineSegmentContained(const SbVec3f& PointA, const SbVec3f& PointB) const
            {
                switch (m_BoundingSpaceShape)
                {
                    case eEmpty:
                        return false;
                    case eBox:
                        switch (m_ContainmentPolicy)
                        {
                            case eFullyContained:
                                return m_Box.intersect(PointA) && m_Box.intersect(PointB);
                            case ePartiallyContained:
                            {
                                if (m_Box.intersect(PointA) || m_Box.intersect(PointB))
                                {
                                    return true;
                                }
                                else
                                {
                                    const SbLine Line(PointA, PointB);
                                    SbVec3f IntersectionPoint;
                                    for (uint i = 0; i < 6; ++i)
                                        if (m_Planes[i].intersect(Line, IntersectionPoint) && m_Box.intersect(IntersectionPoint))
                                        {
                                            return true;
                                        }
                                    return false;
                                }
                            }
                        }
                        break;
                    case eSphere:
                        switch (m_ContainmentPolicy)
                        {
                            case eFullyContained:
                                return ((m_Center - PointA).length() <= m_Radius) && ((m_Center - PointB).length() <= m_Radius);
                            case ePartiallyContained:
                                if (((m_Center - PointA).length() <= m_Radius) || ((m_Center - PointB).length() <= m_Radius))
                                {
                                    return true;
                                }
                                else
                                {
                                    const SbLine Line(PointA, PointB);
                                    const SbVec3f CenterProjected = Line.getClosestPoint(m_Center);
                                    const EVP::Visualization::Geometry::_3D::OpenInventor::vreal ProjectionDistance = (CenterProjected - m_Center).length();
                                    if (ProjectionDistance <= m_Radius)
                                    {
                                        const SbVec3f Displacement = Line.getDirection() * sqrtf(m_Radius * m_Radius - ProjectionDistance * ProjectionDistance);
                                        const SbVec3f MidPoint = (PointA + PointB) / EVP::Visualization::Geometry::_3D::OpenInventor::vreal(2.0);
                                        const EVP::Visualization::Geometry::_3D::OpenInventor::vreal SegmentSemiLength = (PointA - PointB).length() / EVP::Visualization::Geometry::_3D::OpenInventor::vreal(2.0);
                                        const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DistanceA = (CenterProjected + Displacement - MidPoint).length();
                                        if ((DistanceA <= SegmentSemiLength) && (DistanceA >= -SegmentSemiLength))
                                        {
                                            return true;
                                        }
                                        const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DistanceB = (CenterProjected - Displacement - MidPoint).length();
                                        return (DistanceB <= SegmentSemiLength) && (DistanceB >= -SegmentSemiLength);
                                    }
                                    return false;
                                }
                        }
                        break;
                    case eFull:
                        return true;
                }
                return true;
            }

            bool CModelSpaceContainerVisualization::IsFaceContained(const SbVec3f& PointA, const SbVec3f& PointB, const SbVec3f& PointC) const
            {
                switch (m_BoundingSpaceShape)
                {
                    case eEmpty:
                        return false;
                    case eBox:
                        switch (m_ContainmentPolicy)
                        {
                            case eFullyContained:
                                return m_Box.intersect(PointA) && m_Box.intersect(PointB) && m_Box.intersect(PointC);
                            case ePartiallyContained:
                            {
                                if (m_Box.intersect(PointA) || m_Box.intersect(PointB) || m_Box.intersect(PointC))
                                {
                                    return true;
                                }
                                else
                                {
                                    const SbPlane FacePlane(PointA, PointB, PointC);
                                    SbPlane RestrictionPlanes[3];
                                    GenerateRestrictionPlanes(PointA, PointB, PointC, RestrictionPlanes);
                                    SbPlane BoundingBoxPlanes[6] = { m_Planes[0], m_Planes[1], m_Planes[2], m_Planes[3], m_Planes[4], m_Planes[5] };
                                    SbLine IntersectionLine;
                                    SbVec3f IntersectionPoint;
                                    for (uint i = 0; i < 6; ++i)
                                        if (BoundingBoxPlanes[i].intersect(FacePlane, IntersectionLine))
                                            for (uint j = 0; j < 6; ++j)
                                                if ((j != i) && BoundingBoxPlanes[j].intersect(IntersectionLine, IntersectionPoint) && m_Box.intersect(IntersectionPoint))
                                                {
                                                    for (uint k = 0; k < 3; ++k)
                                                        if (RestrictionPlanes[k].getDistance(IntersectionPoint) < EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0))
                                                        {
                                                            return false;
                                                        }
                                                    return true;
                                                }
                                    return false;
                                }
                            }
                        }
                        break;
                    case eSphere:
                        switch (m_ContainmentPolicy)
                        {
                            case eFullyContained:
                                return ((m_Center - PointA).length() <= m_Radius) && ((m_Center - PointB).length() <= m_Radius) && ((m_Center - PointC).length() <= m_Radius);
                            case ePartiallyContained:
                            {
                                if (((m_Center - PointA).length() <= m_Radius) || ((m_Center - PointB).length() <= m_Radius) || ((m_Center - PointC).length() <= m_Radius))
                                {
                                    return true;
                                }
                                else
                                {
                                    const SbPlane FacePlane(PointA, PointB, PointC);
                                    const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Distance = FacePlane.getDistance(m_Center);
                                    if (Distance <= m_Radius)
                                    {
                                        const EVP::Visualization::Geometry::_3D::OpenInventor::vreal CircelRadius = sqrtf(m_Radius * m_Radius - Distance * Distance);
                                        const SbVec3f CircleCenter = m_Center - FacePlane.getNormal() * Distance;
                                        SbPlane RestrictionPlanes[3];
                                        GenerateRestrictionPlanes(PointA, PointB, PointC, RestrictionPlanes);
                                        for (uint k = 0; k < 3; ++k)
                                            if (RestrictionPlanes[k].getDistance(CircleCenter) < -CircelRadius)
                                            {
                                                return false;
                                            }
                                        return true;
                                    }
                                    return false;
                                }
                            }
                        }
                        break;
                    case eFull:
                        return true;
                }
                return true;
            }

            void CModelSpaceContainerVisualization::SetComplexityType(const SoComplexity::Type Type)
            {
                m_pComplexity->type = Type;
            }

            SoComplexity::Type CModelSpaceContainerVisualization::GetComplexityType() const
            {
                return SoComplexity::Type(m_pComplexity->type.getValue());
            }

            void CModelSpaceContainerVisualization::Create()
            {
                m_pTranslation = AddToSeparator<SoTranslation>();
                m_pComplexity = AddToSeparator<SoComplexity>();
                m_pDisplaySwitch = AddToSeparator<SoSwitch>();
                m_pDisplaySwitch->whichChild = SO_SWITCH_NONE;
                m_pSphere = new SoSphere;
                m_pDisplaySwitch->addChild(m_pSphere);
                m_pCube = new SoCube;
                m_pDisplaySwitch->addChild(m_pCube);

                SetDrawStyle(SoDrawStyle::LINES);
                SetTransparency(0.95);
                m_pComplexity->type = SoComplexityTypeElement::OBJECT_SPACE;
                m_pComplexity->value = 1.0;
            }

            bool CModelSpaceContainerVisualization::UpdateVisibility()
            {
                return true;
            }

            void CModelSpaceContainerVisualization::UpDateDisplay()
            {
                switch (m_BoundingSpaceShape)
                {
                    case eEmpty:
                        if (m_pDisplaySwitch->whichChild.getValue() != SO_SWITCH_NONE)
                        {
                            m_pDisplaySwitch->whichChild = SO_SWITCH_NONE;
                        }
                        break;
                    case eBox:
                    {
                        const SbVec3f BoxCenter = (m_Box.getMax() + m_Box.getMin()) / EVP::Visualization::Geometry::_3D::OpenInventor::vreal(2.0);
                        if (m_pTranslation->translation.getValue() != BoxCenter)
                        {
                            m_pTranslation->translation.setValue(BoxCenter);
                        }
                        const SbVec3f BoxSize = m_Box.getMax() - m_Box.getMin();
                        if (m_pCube->width.getValue() != BoxSize[0])
                        {
                            m_pCube->width.setValue(BoxSize[0]);
                        }
                        if (m_pCube->height.getValue() != BoxSize[1])
                        {
                            m_pCube->height.setValue(BoxSize[1]);
                        }
                        if (m_pCube->depth.getValue() != BoxSize[2])
                        {
                            m_pCube->depth.setValue(BoxSize[2]);
                        }
                        if (m_pDisplaySwitch->whichChild.getValue() != 1)
                        {
                            m_pDisplaySwitch->whichChild = 1;
                        }
                    }
                    break;
                    case eSphere:
                        if (m_pTranslation->translation.getValue() != m_Center)
                        {
                            m_pTranslation->translation.setValue(m_Center);
                        }
                        if (m_pSphere->radius.getValue() != m_Radius)
                        {
                            m_pSphere->radius.setValue(m_Radius);
                        }
                        if (m_pDisplaySwitch->whichChild.getValue() != 0)
                        {
                            m_pDisplaySwitch->whichChild = 0;
                        }
                        break;
                    case eFull:
                        if (m_pDisplaySwitch->whichChild.getValue() != SO_SWITCH_NONE)
                        {
                            m_pDisplaySwitch->whichChild = SO_SWITCH_NONE;
                        }
                        break;
                }
            }

            void CModelSpaceContainerVisualization::GenerateRestrictionPlanes(const SbVec3f& PointA, const SbVec3f& PointB, const SbVec3f& PointC, SbPlane* pRestrictionPlanes) const
            {
                const SbVec3f Centroid = (PointA + PointB + PointC) / EVP::Visualization::Geometry::_3D::OpenInventor::vreal(3.0);
                const SbVec3f AB = (PointA + PointB) / EVP::Visualization::Geometry::_3D::OpenInventor::vreal(2.0);
                const SbVec3f BC = (PointB + PointC) / EVP::Visualization::Geometry::_3D::OpenInventor::vreal(2.0);
                const SbVec3f CA = (PointC + PointA) / EVP::Visualization::Geometry::_3D::OpenInventor::vreal(2.0);
                SbVec3f NormalFromABToCentroid = Centroid - AB;
                SbVec3f NormalFromBCToCentroid = Centroid - BC;
                SbVec3f NormalFromCAToCentroid = Centroid - CA;
                NormalFromABToCentroid.normalize();
                NormalFromBCToCentroid.normalize();
                NormalFromCAToCentroid.normalize();
                pRestrictionPlanes[0] = SbPlane(NormalFromABToCentroid, AB);
                pRestrictionPlanes[1] = SbPlane(NormalFromBCToCentroid, BC);
                pRestrictionPlanes[2] = SbPlane(NormalFromCAToCentroid, CA);
            }

            bool CModelSpaceContainerVisualization::SetPlanes(const SbBox3f& Box, SbPlane* pPlanes) const
            {
                if (Box.isEmpty())
                {
                    return false;
                }
                EVP::Visualization::Geometry::_3D::OpenInventor::vreal X0, Y0, Z0, X1, Y1, Z1;
                Box.getBounds(X0, Y0, Z0, X1, Y1, Z1);
                pPlanes[0] = SbPlane(m_AxisX, SbVec3f(X0, 0, 0));
                pPlanes[1] = SbPlane(-m_AxisX, SbVec3f(X1, 0, 0));
                pPlanes[2] = SbPlane(m_AxisY, SbVec3f(0, Y0, 0));
                pPlanes[3] = SbPlane(-m_AxisY, SbVec3f(0, Y1, 0));
                pPlanes[4] = SbPlane(m_AxisZ, SbVec3f(0, 0, Z0));
                pPlanes[5] = SbPlane(-m_AxisZ, SbVec3f(0, 0, Z1));
                return true;
            }

            void CModelSpaceContainerVisualization::Update(const TModelList<CModelSpaceElementVisualization*>& Elements)
            {
                TModelList<CModelSpaceElementVisualization*>::const_iterator End = Elements.end();
                for (TModelList<CModelSpaceElementVisualization*>::const_iterator ppModelSpaceElementVisualization = Elements.begin(); ppModelSpaceElementVisualization != End; ++ppModelSpaceElementVisualization)
                {
                    (*ppModelSpaceElementVisualization)->UpdateVisibility();
                }
            }
        }
    }
}
#endif
