/*
 * ModelMeshVisualization.h
 *
 *  Created on: 08.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "ModelSpaceContainedElementVisualization.h"
#include "ModelVertexVisualization.h"
#include "ModelEdgeVisualization.h"
#include "ModelFaceVisualization.h"
#include "ModelMultiFaceVisualization.h"
#include "../ModelList.h"
#include "../HierarchicalModelElementList.h"
#include "../../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelMesh;
        class CModelVertex;
        class CModelEdge;
        class CModelFace;
        class CModelMultipleFace;

        namespace Visualization
        {
            class CModelMeshVisualization: public CModelSpaceContainedElementVisualization
            {
            public:

                CModelMeshVisualization(CModelMesh* pModelMesh, CModelSpaceContainerVisualization* pModelSpaceContainerVisualization);
                ~CModelMeshVisualization() override;

                virtual void SetVerticesVisible(const CModelSpaceElementVisualization::VisibilityType Visibility);
                virtual void SetEdgesVisible(const CModelSpaceElementVisualization::VisibilityType Visibility);
                virtual void SetFacesVisible(const CModelSpaceElementVisualization::VisibilityType Visibility);
                virtual void SetMultipleFacesVisible(const CModelSpaceElementVisualization::VisibilityType Visibility);

                const SbBox3f& GetBoundingBox() const;

            protected:

                friend class CModelMultipleMeshVisualization;

                virtual  uint UpdateVerticesVisibility();
                virtual  uint UpdateEdgesVisibility();
                virtual  uint UpdateFacesVisibility();
                virtual  uint UpdateMultiFacesVisibility();
                bool UpdateVisibility() override;

            private:

                void Create() override;
                virtual void CreateVertices(const THierarchicalModelElementList<CModelVertex*>& Veritices);
                virtual void CreateEdges(const THierarchicalModelElementList<CModelEdge*>& Edges);
                virtual void CreateFaces(const THierarchicalModelElementList<CModelFace*>& Faces);
                virtual void CreateMultipleFaces(const THierarchicalModelElementList<CModelMultipleFace*>& MultipleFaces);

                void Destroy() override;
                void DestroyVertices();
                void DestroyEdges();
                void DestroyFaces();
                void DestroyMultipleFaces();

                TModelList<CModelVertexVisualization*> m_VertexVisualizations;
                TModelList<CModelEdgeVisualization*> m_EdgeVisualizations;
                TModelList<CModelFaceVisualization*> m_FaceVisualizations;
                TModelList<CModelMultiFaceVisualization*> m_MultiFaceVisualizations;

                SoCoordinate3* m_pCoordinate3;
                SoSwitch* m_pSwitchVertices;
                SoSwitch* m_pSwitchEdges;
                SoSwitch* m_pSwitchFaces;
                SoSwitch* m_pSwitchMultiFaces;
                SbBox3f m_Box;
                CModelSpaceElementVisualization::VisibilityType m_VerticesVisibility;
                CModelSpaceElementVisualization::VisibilityType m_EdgesVisibility;
                CModelSpaceElementVisualization::VisibilityType m_FacesVisibility;
                CModelSpaceElementVisualization::VisibilityType m_MultiFacesVisibility;
            };
        }
    }
}

#endif
