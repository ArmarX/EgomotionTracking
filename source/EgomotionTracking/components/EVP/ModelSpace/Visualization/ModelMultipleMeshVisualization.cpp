/*
 * ModelMultipleMeshVisualization.cpp
 *
 *  Created on: 08.02.2012
 *      Author: gonzalez
 */

#include "ModelMultipleMeshVisualization.h"
#include "../ModelMultipleMesh.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        namespace Visualization
        {
            CModelMultipleMeshVisualization::CModelMultipleMeshVisualization(CModelMultipleMesh* pModelMultipleMesh, CModelSpaceContainerVisualization* pModelSpaceContainerVisualization) :
                CModelSpaceContainedElementVisualization(pModelMultipleMesh, pModelSpaceContainerVisualization)
            {
                Create();
            }

            CModelMultipleMeshVisualization::~CModelMultipleMeshVisualization()
            {
                Destroy();
            }

            const SbBox3f& CModelMultipleMeshVisualization::GetBoundingBox() const
            {
                return m_Box;
            }

            bool CModelMultipleMeshVisualization::UpdateVisibility()
            {
                uint MeshesVisible = 0;
                if (m_pModelSpaceContainerVisualization->IsBoxContained(m_Box) && m_MeshVisualizations.size())
                {
                    TModelList<CModelMeshVisualization*>::const_iterator EndMeshVisualizations = m_MeshVisualizations.end();
                    for (TModelList<CModelMeshVisualization*>::const_iterator ppModelMeshVisualization = m_MeshVisualizations.begin(); ppModelMeshVisualization != EndMeshVisualizations; ++ppModelMeshVisualization)
                        if ((*ppModelMeshVisualization)->UpdateVisibility())
                        {
                            ++MeshesVisible;
                        }
                }
                m_pSwitch->whichChild = MeshesVisible ? SO_SWITCH_ALL : SO_SWITCH_NONE;
                return MeshesVisible;
            }

            void CModelMultipleMeshVisualization::Create()
            {
                m_Box.makeEmpty();
                const CModelMultipleMesh* pModelMultipleMesh = GetHierarchicalModelPrimitive<CModelMultipleMesh> ();
                THierarchicalModelElementList<CModelMesh*>::const_iterator EndMeshes = pModelMultipleMesh->GetMeshes().end();
                for (THierarchicalModelElementList<CModelMesh*>::const_iterator ppModelMesh = pModelMultipleMesh->GetMeshes().begin(); ppModelMesh != EndMeshes; ++ppModelMesh)
                {
                    CModelMeshVisualization* pModelMeshVisualization = new CModelMeshVisualization(*ppModelMesh, m_pModelSpaceContainerVisualization);
                    m_Box.extendBy(pModelMeshVisualization->GetBoundingBox());
                    AddSubElement(pModelMeshVisualization);
                    m_MeshVisualizations.AddElement(pModelMeshVisualization, false);
                }
            }

            void CModelMultipleMeshVisualization::Destroy()
            {
                if (m_MeshVisualizations.size())
                {
                    TModelList<CModelMeshVisualization*>::const_iterator EndMeshVisualizations = m_MeshVisualizations.end();
                    for (TModelList<CModelMeshVisualization*>::const_iterator ppModelMeshVisualization = m_MeshVisualizations.begin(); ppModelMeshVisualization != EndMeshVisualizations; ++ppModelMeshVisualization)
                    {
                        delete *ppModelMeshVisualization;
                    }
                    m_MeshVisualizations.clear();
                }
            }
        }
    }
}
#endif
