/*
 * ModelSpaceContainedElementVisualization.h
 *
 *  Created on: 16.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "ModelSpaceElementVisualization.h"
#include "ModelSpaceContainerVisualization.h"
#include "../../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        namespace Visualization
        {
            class CModelSpaceContainedElementVisualization: public CModelSpaceElementVisualization
            {
            public:

                CModelSpaceContainedElementVisualization(CHierarchicalModelElement* pHierarchicalModelElement, CModelSpaceContainerVisualization* pModelSpaceContainerVisualization);
                ~CModelSpaceContainedElementVisualization() override;

                void SetVisible(const VisibilityType Visibility) override;

                const CModelSpaceContainerVisualization* GetSpaceContainerVisualization() const;

            protected:

                void Create() override;

                CModelSpaceContainerVisualization* m_pModelSpaceContainerVisualization;
            };
        }
    }
}
#endif
