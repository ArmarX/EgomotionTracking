/*
 * ModelFaceVisualization.cpp
 *
 *  Created on: 08.02.2012
 *      Author: gonzalez
 */

#include "ModelFaceVisualization.h"
#include "../ModelFace.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        namespace Visualization
        {
            CModelFaceVisualization::CModelFaceVisualization(CModelFace* pModelFace, CModelSpaceContainerVisualization* pModelSpaceContainerVisualization) :
                CModelSpaceContainedElementVisualization(pModelFace, pModelSpaceContainerVisualization), m_pNormal(nullptr), m_pNormalBinding(nullptr), m_pIndexedFaceSet(nullptr)
            {
                Create();
            }

            CModelFaceVisualization::~CModelFaceVisualization()
                = default;

            void CModelFaceVisualization::SetDisplayMode(const DisplayMode Mode)
            {
                switch (Mode)
                {
                    case eFaceNormal:
                        m_pNormalBinding->value = SoNormalBindingElement::OVERALL;
                        break;
                    case eVertexNormal:
                        m_pNormalBinding->value = SoNormalBindingElement::PER_VERTEX_INDEXED;
                        break;
                }
            }

            bool CModelFaceVisualization::UpdateVisibility()
            {
                const CModelFace* pModelFace = GetHierarchicalModelPrimitive<CModelFace> ();
                const TModelList<const CModelOrientedVertexContainer*> OrientedVertices = pModelFace->GetOrientedVertices();
                TModelList<const CModelOrientedVertexContainer*>::const_iterator ppModelOrientedVertexContainer = OrientedVertices.begin();
                const CModelOrientedVertexContainer* pModelOrientedVertexContainerA = *ppModelOrientedVertexContainer++;
                const CModelOrientedVertexContainer* pModelOrientedVertexContainerB = *ppModelOrientedVertexContainer++;
                const CModelOrientedVertexContainer* pModelOrientedVertexContainerC = *ppModelOrientedVertexContainer;
                if (m_pModelSpaceContainerVisualization->IsFaceContained(EVP::Visualization::Geometry::_3D::OpenInventor::T(pModelOrientedVertexContainerA->GetPoint()), EVP::Visualization::Geometry::_3D::OpenInventor::T(pModelOrientedVertexContainerB->GetPoint()), EVP::Visualization::Geometry::_3D::OpenInventor::T(pModelOrientedVertexContainerC->GetPoint())))
                {
                    m_pSwitch->whichChild = SO_SWITCH_ALL;
                    return true;
                }
                else
                {
                    m_pSwitch->whichChild = SO_SWITCH_NONE;
                    return false;
                }
            }

            void CModelFaceVisualization::Create()
            {
                const CModelFace* pModelFace = GetHierarchicalModelPrimitive<CModelFace> ();
                m_pNormal = AddToSeparator<SoNormal> ();
                m_pNormalBinding = AddToSeparator<SoNormalBinding> ();
                m_pIndexedFaceSet = AddToSeparator<SoIndexedFaceSet> ();
                const TModelList<const CModelOrientedVertexContainer*> OrientedVertices = pModelFace->GetOrientedVertices();
                TModelList<const CModelOrientedVertexContainer*>::const_iterator ppModelOrientedVertexContainer = OrientedVertices.begin();
                const CModelOrientedVertexContainer* pModelOrientedVertexContainerA = *ppModelOrientedVertexContainer++;
                const CModelOrientedVertexContainer* pModelOrientedVertexContainerB = *ppModelOrientedVertexContainer++;
                const CModelOrientedVertexContainer* pModelOrientedVertexContainerC = *ppModelOrientedVertexContainer;
                m_pNormal->vector.set1Value(0, EVP::Visualization::Geometry::_3D::OpenInventor::T(pModelFace->GetNormal()));
                m_pNormal->vector.set1Value(1, EVP::Visualization::Geometry::_3D::OpenInventor::T(pModelOrientedVertexContainerA->GetNormal()));
                m_pNormal->vector.set1Value(2, EVP::Visualization::Geometry::_3D::OpenInventor::T(pModelOrientedVertexContainerB->GetNormal()));
                m_pNormal->vector.set1Value(3, EVP::Visualization::Geometry::_3D::OpenInventor::T(pModelOrientedVertexContainerC->GetNormal()));
                m_pNormalBinding->value = SoNormalBindingElement::OVERALL;
                m_pIndexedFaceSet->coordIndex.set1Value(0, pModelOrientedVertexContainerA->GetIndexInMesh());
                m_pIndexedFaceSet->coordIndex.set1Value(1, pModelOrientedVertexContainerB->GetIndexInMesh());
                m_pIndexedFaceSet->coordIndex.set1Value(2, pModelOrientedVertexContainerC->GetIndexInMesh());
                m_pIndexedFaceSet->coordIndex.set1Value(3, -1);
                m_pIndexedFaceSet->normalIndex.set1Value(0, 1);
                m_pIndexedFaceSet->normalIndex.set1Value(1, 2);
                m_pIndexedFaceSet->normalIndex.set1Value(2, 3);
            }
        }
    }
}
#endif
