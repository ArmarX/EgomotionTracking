/*
 * ModelSpaceContainerVisualization.h
 *
 *  Created on: 14.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "ModelSpaceElementVisualization.h"
#include "../ModelList.h"
#include "../../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        namespace Visualization
        {
            class CModelSpaceElementVisualization;

            class CModelSpaceContainerVisualization: public CModelSpaceElementVisualization
            {
            public:

                enum BoundingSpaceShape
                {
                    eEmpty, eBox, eSphere, eFull
                };

                enum ContainmentPolicy
                {
                    eFullyContained, ePartiallyContained
                };

                CModelSpaceContainerVisualization();
                ~CModelSpaceContainerVisualization() override;

                void SetBoundingSpaceShapeEmpty(const uint UpdatingLevelsFlag);
                void SetBoundingSpaceShapeBox(const uint UpdatingLevelsFlag, const SbBox3f& Box);
                void SetBoundingSpaceShapeBox(const uint UpdatingLevelsFlag, const SbVec3f& Point, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DXM, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DYM, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DZM, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DXP, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DYP, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DZP);
                void SetBoundingSpaceShapeBox(const uint UpdatingLevelsFlag, const SbVec3f& PointA, const SbVec3f& PointB);
                void SetBoundingSpaceShapeBox(const uint UpdatingLevelsFlag, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal X0, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Y0, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Z0, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal X1, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Y1, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Z1);
                void SetBoundingSpaceShapeSphere(const uint UpdatingLevelsFlag, const SbVec3f& Center, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Radius);
                void SetBoundingSpaceShapeSphere(const uint UpdatingLevelsFlag, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal CX, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal CY, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal CZ, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Radius);
                void SetBoundingSpaceShapeFull(const uint UpdatingLevelsFlag);

                void SetContainmentPolicy(const ContainmentPolicy Policy, const uint UpdatingLevelsFlag);

                virtual void AddElementAtConstructor(CModelSpaceElementVisualization* pModelSpaceElementVisualization);
                virtual  bool AddElement(CModelSpaceElementVisualization* pModelSpaceElementVisualization);
                virtual void Update(const uint UpdatingLevelsFlag);

                BoundingSpaceShape GetBoundingSpaceShape() const;
                ContainmentPolicy GetContainmentPolicy() const;

                bool IsBoxContained(const SbBox3f& Box) const;
                bool IsPointContained(const SbVec3f& Point) const;
                bool IsLineSegmentContained(const SbVec3f& PointA, const SbVec3f& PointB) const;
                bool IsFaceContained(const SbVec3f& PointA, const SbVec3f& PointB, const SbVec3f& PointC) const;

                virtual void SetComplexityType(const SoComplexity::Type Type);
                virtual  SoComplexity::Type GetComplexityType() const;

            protected:

                void Create() override;
                bool UpdateVisibility() override;
                virtual void UpDateDisplay();

                void GenerateRestrictionPlanes(const SbVec3f& PointA, const SbVec3f& PointB, const SbVec3f& PointC, SbPlane* pRestrictionPlanes) const;
                bool SetPlanes(const SbBox3f& Box, SbPlane* pPlanes) const;
                void Update(const TModelList<CModelSpaceElementVisualization*>& Elements);

                SbBox3f m_Box;
                SbPlane m_Planes[6];
                SbVec3f m_Center;
                TModelList<CModelSpaceElementVisualization*> m_Elements[6];
                BoundingSpaceShape m_BoundingSpaceShape;
                ContainmentPolicy m_ContainmentPolicy;
                EVP::Visualization::Geometry::_3D::OpenInventor::vreal m_Radius;

                SoSwitch* m_pDisplaySwitch;
                SoTranslation* m_pTranslation;
                SoComplexity* m_pComplexity;
                SoSphere* m_pSphere;
                SoCube* m_pCube;

                static const SbVec3f m_AxisX;
                static const SbVec3f m_AxisY;
                static const SbVec3f m_AxisZ;
            };
        }
    }
}
#endif
