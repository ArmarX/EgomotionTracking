/*
 * ModelVertexVisualization.cpp
 *
 *  Created on: 08.02.2012
 *      Author: gonzalez
 */

#include "ModelVertexVisualization.h"
#include "../ModelVertex.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        namespace Visualization
        {
            CModelVertexVisualization::CModelVertexVisualization(CModelVertex* pModelVertex, CModelSpaceContainerVisualization* pModelSpaceContainerVisualization) :
                CModelSpaceContainedElementVisualization(pModelVertex, pModelSpaceContainerVisualization), m_pComplexity(nullptr), m_pTranslation(nullptr), m_pSphere(nullptr)
            {
                Create();
            }

            CModelVertexVisualization::~CModelVertexVisualization()
                = default;

            void CModelVertexVisualization::SetDisplayRadius(const EVP::Visualization::Geometry::_3D::OpenInventor::vreal DisplayRadius)
            {
                const EVP::Visualization::Geometry::_3D::OpenInventor::vreal InRangeDisplayRadius = TMin(TMax(DisplayRadius, _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_), _VISUALIZATION_POINT_MAXIMAL_DISPLAY_RADIUS_);
                if (m_pSphere->radius.getValue() != InRangeDisplayRadius)
                {
                    m_pSphere->radius = InRangeDisplayRadius;
                }
            }

            EVP::Visualization::Geometry::_3D::OpenInventor::vreal CModelVertexVisualization::GetDisplayaRadius() const
            {
                return m_pSphere->radius.getValue();
            }

            void CModelVertexVisualization::SetComplexityType(const SoComplexity::Type Type)
            {
                m_pComplexity->type = Type;
            }

            SoComplexity::Type CModelVertexVisualization::GetComplexityType() const
            {
                return SoComplexity::Type(m_pComplexity->type.getValue());
            }

            void CModelVertexVisualization::SetComplexityValue(const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Complexity)
            {
                m_pComplexity->value = Complexity;
            }

            EVP::Visualization::Geometry::_3D::OpenInventor::vreal CModelVertexVisualization::GetComplexityValue() const
            {
                return m_pComplexity->value.getValue();
            }

            bool CModelVertexVisualization::UpdateVisibility()
            {
                if (m_pModelSpaceContainerVisualization->IsPointContained(EVP::Visualization::Geometry::_3D::OpenInventor::T(GetHierarchicalModelPrimitive<CModelVertex> ()->GetPoint())))
                {
                    m_pSwitch->whichChild = SO_SWITCH_ALL;
                    return true;
                }
                else
                {
                    m_pSwitch->whichChild = SO_SWITCH_NONE;
                    return false;
                }
            }

            void CModelVertexVisualization::Create()
            {
                const CModelVertex* pModelVertex = GetHierarchicalModelPrimitive<CModelVertex> ();
                m_pComplexity = AddToSeparator<SoComplexity> ();
                m_pTranslation = AddToSeparator<SoTranslation> ();
                m_pSphere = AddToSeparator<SoSphere> ();
                m_pComplexity->type = SoComplexityTypeElement::BOUNDING_BOX;
                m_pSphere->radius = _VISUALIZATION_POINT_MINIMAL_DISPLAY_RADIUS_;
                m_pTranslation->translation.setValue(EVP::Visualization::Geometry::_3D::OpenInventor::T(pModelVertex->GetPoint()));
            }
        }
    }
}
#endif
