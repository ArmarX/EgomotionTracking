/*
 * ModelSpaceElementVisualization.cpp
 *
 *  Created on: 08.02.2012
 *      Author: gonzalez
 */

#include "ModelSpaceElementVisualization.h"
#include "../HierarchicalModelPrimitive.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        namespace Visualization
        {
            CModelSpaceElementVisualization::CModelSpaceElementVisualization(CHierarchicalModelElement* pHierarchicalModelElement) :
                m_pHierarchicalModelElement(pHierarchicalModelElement), m_Visibility(eDynamicContainedElement), m_pSwitch(nullptr), m_pSeparator(nullptr), m_pDrawStyle(nullptr), m_pMaterial(nullptr)
            {
                Create();
            }

            CModelSpaceElementVisualization::~CModelSpaceElementVisualization()
            {
                Destroy();
            }

            SoSwitch* CModelSpaceElementVisualization::GetSwitch()
            {
                return m_pSwitch;
            }

            CHierarchicalModelElement* CModelSpaceElementVisualization::GetHierarchicalModelPrimitive()
            {
                return m_pHierarchicalModelElement;
            }

            void CModelSpaceElementVisualization::SetVisible(const VisibilityType Visibility)
            {
                if (m_Visibility != Visibility)
                {
                    m_Visibility = Visibility;
                    switch (m_Visibility)
                    {
                        case eNone:
                            m_pSwitch->whichChild = SO_SWITCH_NONE;
                            break;
                        case eDynamicContainedElement:
                            m_pSwitch->whichChild = SO_SWITCH_ALL;
                            break;
                        case eStaticContainedElement:
                            m_pSwitch->whichChild = 1;
                            break;
                        case eStaticContainedElementAndProps:
                            m_pSwitch->whichChild = SO_SWITCH_ALL;
                            break;
                    }
                }
            }

            CModelSpaceElementVisualization::VisibilityType CModelSpaceElementVisualization::GetVisible() const
            {
                return m_Visibility;
            }

            void CModelSpaceElementVisualization::SetColor(const EVP::Visualization::Geometry::_3D::OpenInventor::vreal R, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal G, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal B)
            {
                m_pMaterial->diffuseColor.setValue(TMin(EVP::Visualization::Geometry::_3D::OpenInventor::vreal(1.0), TMax(R, EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0))), TMin(EVP::Visualization::Geometry::_3D::OpenInventor::vreal(1.0), TMax(G, EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0))), TMin(EVP::Visualization::Geometry::_3D::OpenInventor::vreal(1.0), TMax(B, EVP::Visualization::Geometry::_3D::OpenInventor::vreal(0.0))));
            }

            void CModelSpaceElementVisualization::SetColor(const SbColor& Color)
            {
                m_pMaterial->diffuseColor = Color;
            }

            const SbColor& CModelSpaceElementVisualization::GetColor() const
            {
                return *m_pMaterial->diffuseColor.getValues(0);
            }

            void CModelSpaceElementVisualization::SetTransparency(const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Transparency)
            {
                m_pMaterial->transparency = Transparency;
            }

            EVP::Visualization::Geometry::_3D::OpenInventor::vreal CModelSpaceElementVisualization::GetTransparency() const
            {
                return *m_pMaterial->transparency.getValues(0);
            }

            void CModelSpaceElementVisualization::SetDrawStyle(SoDrawStyle::Style Style)
            {
                m_pDrawStyle->style = Style;
            }

            SoDrawStyle::Style CModelSpaceElementVisualization::GetDrawStyle() const
            {
                return SoDrawStyle::Style(m_pDrawStyle->style.getValue());
            }

            void CModelSpaceElementVisualization::SetLineWidth(const EVP::Visualization::Geometry::_3D::OpenInventor::vreal LineWidth)
            {
                m_pDrawStyle->lineWidth = LineWidth;
            }

            EVP::Visualization::Geometry::_3D::OpenInventor::vreal CModelSpaceElementVisualization::GetLineWidth() const
            {
                return m_pDrawStyle->lineWidth.getValue();
            }

            void CModelSpaceElementVisualization::SetPointSize(const EVP::Visualization::Geometry::_3D::OpenInventor::vreal PointSize)
            {
                m_pDrawStyle->pointSize = PointSize;
            }

            EVP::Visualization::Geometry::_3D::OpenInventor::vreal CModelSpaceElementVisualization::GetPointSize() const
            {
                return m_pDrawStyle->pointSize.getValue();
            }

            void CModelSpaceElementVisualization::SetLinePattern(const ushort Pattern)
            {
                m_pDrawStyle->linePattern = Pattern;
            }

            ushort CModelSpaceElementVisualization::GetLinePattern() const
            {
                return m_pDrawStyle->linePattern.getValue();
            }

            bool CModelSpaceElementVisualization::AddProp(SoSeparator* pProSeparator)
            {
                if (pProSeparator)
                {
                    m_pSwitch->addChild(pProSeparator);
                }
                return pProSeparator;
            }

            void CModelSpaceElementVisualization::ClearProps()
            {
                m_pSeparator->ref();
                m_pSwitch->removeAllChildren();
                m_pSwitch->addChild(m_pSeparator);
                m_pSeparator->unrefNoDelete();
            }

            bool CModelSpaceElementVisualization::AddSubElement(CModelSpaceElementVisualization* pModelSpaceElementVisualization)
            {
                if (pModelSpaceElementVisualization)
                {
                    m_pSeparator->addChild(pModelSpaceElementVisualization->GetSwitch());
                }
                return pModelSpaceElementVisualization;
            }

            void CModelSpaceElementVisualization::Create()
            {
                m_pSwitch = new SoSwitch;
                m_pSwitch->ref();

                m_pSeparator = new SoSeparator;
                m_pSwitch->addChild(m_pSeparator);
                m_pDrawStyle = AddToSeparator<SoDrawStyle>();
                m_pMaterial = AddToSeparator<SoMaterial>();
                m_pSwitch->whichChild = SO_SWITCH_ALL;
                m_Visibility = eStaticContainedElementAndProps;

                if (m_pHierarchicalModelElement)
                {
                    m_pHierarchicalModelElement->SetModelSpaceElementVisualization(this);
                }
            }

            void CModelSpaceElementVisualization::Destroy()
            {
                m_pSwitch->removeAllChildren();
                m_pSwitch->unref();
            }

            SoNode* CModelSpaceElementVisualization::GetSelectionNode()
            {
                return m_pSwitch;
            }
        }
    }
}
#endif
