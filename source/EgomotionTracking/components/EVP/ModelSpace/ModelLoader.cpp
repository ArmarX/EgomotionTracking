/*
 * ModelLoader.cpp
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#include "ModelLoader.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        CModelLoader::CModelLoader()
            = default;

        CModelLoader::~CModelLoader()
            = default;
    }
}
#endif //_EVP_USE_MODEL_SPACE
