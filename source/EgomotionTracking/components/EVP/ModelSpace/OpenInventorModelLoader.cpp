/*
 * OpenInventorModelLoader.cpp
 *
 *  Created on: 25.11.2011
 *      Author: gonzalez
 */

#include "OpenInventorModelLoader.h"
#ifdef _EVP_USE_MODEL_SPACE
#include <Inventor/SoInput.h>
#include <Inventor/SoDB.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoGroup.h>
#include <Inventor/nodes/SoNormal.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoNormalBinding.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/SoType.h>

namespace EVP
{
    namespace ModelSpace
    {
        COpenInventorModelLoader::COpenInventorModelLoader() :
            CModelLoader()
        {
        }

        COpenInventorModelLoader::~COpenInventorModelLoader()
            = default;

        CModelMultipleMesh* COpenInventorModelLoader::LoadFromFile(const_string pFileName)
        {
            if (pFileName)
            {
                SoInput InputFile;
                if (InputFile.openFile(pFileName))
                {
                    SoSeparator* pFileRoot = SoDB::readAll(&InputFile);
                    if (pFileRoot)
                    {
                        pFileRoot->ref();
                        TModelList<SoSeparator*> MeshesSeparators = ExtractMeshesSeparators(pFileRoot);
                        CModelMultipleMesh* pModelMultipleMesh = nullptr;
                        if (MeshesSeparators.size())
                        {
                            pModelMultipleMesh = new CModelMultipleMesh(nullptr);
                            TModelList<SoSeparator*>::iterator EndMeshesSeparators = MeshesSeparators.end();
                            for (TModelList<SoSeparator*>::iterator ppMeshesSeparator = MeshesSeparators.begin(); ppMeshesSeparator != EndMeshesSeparators; ++ppMeshesSeparator)
                            {
                                SoNormal* pNormal = nullptr;
                                SoNormalBinding* pNormalBinding = nullptr;
                                SoCoordinate3* pCoordinate3 = nullptr;
                                SoFaceSet* pFaceSet = nullptr;
                                if (GetMeshesStructuralElements(*ppMeshesSeparator, pNormal, pNormalBinding, pCoordinate3, pFaceSet))
                                {
                                    CModelMesh* pModelMesh = new CModelMesh(pModelMultipleMesh);
                                    const TModelArray<CModelVertex*> Vertices = LoadVertices(_EVP_MODELSPACE_POINT_DISTANCE_TOLERANCE_, pCoordinate3, pModelMesh);
                                    if (Vertices.size())
                                    {
                                        const TModelArray<CModelFace*> Faces = LoadFaces(Vertices, pNormalBinding, pNormal, pFaceSet, pModelMesh);
                                        if (Faces.size())
                                        {
                                            LoadMultiFaces(Faces);
                                        }
                                    }
                                }
                            }
                        }
                        pFileRoot->unref();
                        return pModelMultipleMesh;
                    }
                }
            }
            return nullptr;
        }

        /* Aqui eu simplesmente tenho que considerar o fato de que ele pode ter também um child do tipo Transform */
        TModelList<SoSeparator*> COpenInventorModelLoader::ExtractMeshesSeparators(SoSeparator* pFileRoot)
        {
            const SoType NormalClassTypeId = SoNormal::getClassTypeId();
            const SoType NormalBindingClassTypeId = SoNormalBinding::getClassTypeId();
            const SoType Coordinate3ClassTypeId = SoCoordinate3::getClassTypeId();
            const SoType FaceSetClassTypeId = SoFaceSet::getClassTypeId();
            TModelList<SoSeparator*> MeshesSeparators;
            TModelList<SoSeparator*> ExpansionMeshesSeparators;
            ExpansionMeshesSeparators.push_back(pFileRoot);
            while (ExpansionMeshesSeparators.size())
            {
                SoSeparator* pSeparator = ExpansionMeshesSeparators.front();
                ExpansionMeshesSeparators.pop_front();
                const uint TotalChildren = pSeparator->getNumChildren();
                if (TotalChildren == 4)
                {
                    uint Flags = 0;
                    for (uint i = 0; i < TotalChildren; ++i)
                    {
                        const SoType CurrentType = pSeparator->getChild(i)->getTypeId();
                        if (CurrentType == NormalClassTypeId)
                        {
                            Flags |= 0x1;
                        }
                        else if (CurrentType == NormalBindingClassTypeId)
                        {
                            Flags |= 0x2;
                        }
                        else if (CurrentType == Coordinate3ClassTypeId)
                        {
                            Flags |= 0x4;
                        }
                        else if (CurrentType == FaceSetClassTypeId)
                        {
                            Flags |= 0x8;
                        }
                    }
                    if (Flags == 0XF)
                    {
                        MeshesSeparators.push_back(pSeparator);
                        continue;
                    }
                }
                for (uint i = 0; i < TotalChildren; ++i)
                    if (pSeparator->getChild(i)->getTypeId() == SoSeparator::getClassTypeId())
                    {
                        ExpansionMeshesSeparators.push_back(dynamic_cast<SoSeparator*>(pSeparator->getChild(i)));
                    }
            }
            return MeshesSeparators;
        }

        bool COpenInventorModelLoader::GetMeshesStructuralElements(SoSeparator* pMeshesSeparator, SoNormal*& pNormal, SoNormalBinding*& pNormalBinding, SoCoordinate3*& pCoordinate3, SoFaceSet*& pFaceSet)
        {
            const SoType NormalClassTypeId = SoNormal::getClassTypeId();
            const SoType NormalBindingClassTypeId = SoNormalBinding::getClassTypeId();
            const SoType Coordinate3ClassTypeId = SoCoordinate3::getClassTypeId();
            const SoType FaceSetClassTypeId = SoFaceSet::getClassTypeId();
            const uint TotalChildren = pMeshesSeparator->getNumChildren();
            for (uint i = 0; i < TotalChildren; ++i)
            {
                const SoType CurrentType = pMeshesSeparator->getChild(i)->getTypeId();
                if (CurrentType == NormalClassTypeId)
                {
                    pNormal = dynamic_cast<SoNormal*>(pMeshesSeparator->getChild(i));
                }
                else if (CurrentType == NormalBindingClassTypeId)
                {
                    pNormalBinding = dynamic_cast<SoNormalBinding*>(pMeshesSeparator->getChild(i));
                }
                else if (CurrentType == Coordinate3ClassTypeId)
                {
                    pCoordinate3 = dynamic_cast<SoCoordinate3*>(pMeshesSeparator->getChild(i));
                }
                else if (CurrentType == FaceSetClassTypeId)
                {
                    pFaceSet = dynamic_cast<SoFaceSet*>(pMeshesSeparator->getChild(i));
                }
                else if (pNormal && pNormalBinding && pCoordinate3 && pFaceSet)
                {
                    break;
                }
            }
            return (pNormal && pNormalBinding && pCoordinate3 && pFaceSet);
        }

        TModelArray<CModelVertex*> COpenInventorModelLoader::LoadVertices(const real Tolerance, SoCoordinate3* pCoordinate3, CModelMesh* pModelMesh)
        {
            const uint TotalVertices = pCoordinate3->point.getNum();
            TModelArray<CModelVertex*> Vertices(TotalVertices);
            uint Index = 0;
            for (uint i = 0; i < TotalVertices; ++i)
            {
                const SbVec3f& X = pCoordinate3->point[i];
                Mathematics::_3D::CVector3D Point(X[0], X[1], X[2]);
                CModelVertex* pVertex = nullptr;
                for (uint j = 0; j < Vertices.size(); ++j)
                    if (Vertices[j]->GetDistance(Point) < Tolerance)
                    {
                        pVertex = Vertices[j];
                        break;
                    }
                if (!pVertex)
                {
                    pVertex = new CModelVertex(Point, pModelMesh);
                    pVertex->SetIndexInMesh(Index++);
                }
                Vertices.push_back(pVertex);
            }
            return Vertices;
        }

        TModelArray<CModelFace*> COpenInventorModelLoader::LoadFaces(const TModelArray<CModelVertex*>& Vertices, SoNormalBinding* pNormalBinding, SoNormal* pNormal, SoFaceSet* pFaceSet, CModelMesh* pModelMesh)
        {
            const uint TotalFaces = pFaceSet->numVertices.getNum();
            TModelArray<CModelFace*> Faces(TotalFaces);
            switch (pNormalBinding->value.getValue())
            {
                case SoNormalBinding::PER_VERTEX:
                    for (uint i = 0, j = 0; i < TotalFaces; ++i)
                        switch (pFaceSet->numVertices[i])
                        {
                            case 3:
                            {
                                const SbVec3f& NormalA = pNormal->vector[j];
                                CModelVertex* pModelVertexA = Vertices[j++];
                                const SbVec3f& NormalB = pNormal->vector[j];
                                CModelVertex* pModelVertexB = Vertices[j++];
                                const SbVec3f& NormalC = pNormal->vector[j];
                                CModelVertex* pModelVertexC = Vertices[j++];
                                if (!((pModelVertexA == pModelVertexB) || (pModelVertexA == pModelVertexC) || (pModelVertexB == pModelVertexC)))
                                {
                                    const CModelEdge* pModelEdgeAB = pModelVertexA->GetReadOnlyLinkingEdge(pModelVertexB);
                                    if (!pModelEdgeAB)
                                    {
                                        pModelEdgeAB = new CModelEdge(pModelVertexA, pModelVertexB, pModelMesh);
                                    }
                                    const CModelEdge* pModelEdgeBC = pModelVertexB->GetReadOnlyLinkingEdge(pModelVertexC);
                                    if (!pModelEdgeBC)
                                    {
                                        pModelEdgeBC = new CModelEdge(pModelVertexB, pModelVertexC, pModelMesh);
                                    }
                                    const CModelEdge* pModelEdgeCA = pModelVertexC->GetReadOnlyLinkingEdge(pModelVertexA);
                                    if (!pModelEdgeCA)
                                    {
                                        pModelEdgeCA = new CModelEdge(pModelVertexC, pModelVertexA, pModelMesh);
                                    }
                                    Faces.push_back(new CModelFace(pModelMesh, pModelVertexA, pModelVertexB, pModelVertexC, Mathematics::_3D::CVector3D(NormalA[0], NormalA[1], NormalA[2]), Mathematics::_3D::CVector3D(NormalB[0], NormalB[1], NormalB[2]), Mathematics::_3D::CVector3D(NormalC[0], NormalC[1], NormalC[2])));
                                }
                            }
                            break;
                            default:
                                break;
                        }
                    break;
                default:
                    break;
            }
            return Faces;
        }

        void COpenInventorModelLoader::LoadMultiFaces(const TModelArray<CModelFace*>& Faces)
        {
            const uint TotalFaces = Faces.size();
            for (uint i = 0; i < TotalFaces; ++i)
                if (!Faces[i]->GetMultipleFace())
                {
                    CModelMultipleFace* pModelMultipleFace = new CModelMultipleFace(Faces[i]);
                    if (pModelMultipleFace->IsTrivial())
                    {
                        delete pModelMultipleFace;
                    }
                }
        }
    }
}
#endif // _EVP_USE_MODEL_SPACE
