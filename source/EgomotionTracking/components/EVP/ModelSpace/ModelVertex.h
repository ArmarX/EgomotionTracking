/*
 * ModelVertex.h
 *
 *  Created on: 23.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "HierarchicalModelPrimitive.h"
#include "../Foundation/Mathematics/4D/Matrix4D.h"
#include "../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelFace;
        class CModelEdge;

        class CModelVertex: public CHierarchicalModelPrimitive
        {
        public:

            CModelVertex(const Mathematics::_3D::CVector3D& Point, CModelMesh* pModelMesh);
            CModelVertex(CModelMesh* pModelMesh, const Identifier IntanceId);
            ~CModelVertex() override;

            bool AddEdge(CModelEdge* pModelEdge, const bool CheckUnique);
            bool AddFace(CModelFace* pModelFace, const bool CheckUnique);

            const Mathematics::_3D::CVector3D& GetPoint() const;
            const THierarchicalModelElementList<CModelEdge*>& GetEdges() const;
            const THierarchicalModelElementList<CModelFace*>& GetFaces() const;

            CModelEdge* GetWritableLinkingEdge(const CModelVertex* pVertex);
            const CModelEdge* GetReadOnlyLinkingEdge(const CModelVertex* pVertex) const;

            void SetIndexInMesh(const uint IndexInMesh);
            uint GetIndexInMesh() const;

            real GetDistance(const Mathematics::_3D::CVector3D& Point) const;

            void ApplyTransformation(const EVP::Mathematics::_4D::CMatrix4D& Transformation);

        protected:

            friend class CModelMesh;

            void ClearElements();

            bool Deserialize(QDomElement& XmlElement) override;
            QDomElement Serialize(QDomDocument& XmlDocument) const override;

            bool DeserializeAttributes(QDomElement& XmlElement) override;
            void SerializeAttributes(QDomElement& XmlElement) const override;

            QDomElement SerializeReference(QDomDocument& XmlDocument) const override;

            Mathematics::_3D::CVector3D m_Point;
            uint m_IndexInMesh;
            THierarchicalModelElementList<CModelEdge*> m_Edges;
            THierarchicalModelElementList<CModelFace*> m_Faces;
        };
    }
}
#endif // _EVP_USE_MODEL_SPACE
