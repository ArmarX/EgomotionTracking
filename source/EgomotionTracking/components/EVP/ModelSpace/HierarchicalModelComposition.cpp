/*
 * HierarchicalModelComposition.cpp
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#include "HierarchicalModelComposition.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        CHierarchicalModelComposition::CHierarchicalModelComposition(const CModelElement::TypeId Id, CHierarchicalModelElement* pSuperior) :
            CHierarchicalModelElement(Id, pSuperior)
        {
        }

        CHierarchicalModelComposition::CHierarchicalModelComposition(const CModelElement::TypeId Id, CHierarchicalModelElement* pSuperior, const Identifier IntanceId) :
            CHierarchicalModelElement(Id, pSuperior, IntanceId)
        {
        }

        CHierarchicalModelComposition::~CHierarchicalModelComposition()
            = default;

        bool CHierarchicalModelComposition::DeserializeAttributes(QDomElement& XmlElement)
        {
            return CHierarchicalModelElement::DeserializeAttributes(XmlElement);
        }

        void CHierarchicalModelComposition::SerializeAttributes(QDomElement& XmlElement) const
        {
            CHierarchicalModelElement::SerializeAttributes(XmlElement);
        }
    }
}
#endif// _EVP_USE_MODEL_SPACE
