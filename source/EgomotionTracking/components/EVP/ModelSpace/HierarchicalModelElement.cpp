/*
 * HierarchicalModelElement.cpp
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#include "HierarchicalModelElement.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        CHierarchicalModelElement::CHierarchicalModelElement(const CModelElement::TypeId Id, CHierarchicalModelElement* pSuperior) :
            CModelElement(Id), m_pSuperior(pSuperior), m_IsIndexable(true)
        {
        }

        CHierarchicalModelElement::CHierarchicalModelElement(const CModelElement::TypeId Id, CHierarchicalModelElement* pSuperior, const Identifier IntanceId) :
            CModelElement(Id, IntanceId), m_pSuperior(pSuperior), m_IsIndexable(false)
        {

        }

        CHierarchicalModelElement::~CHierarchicalModelElement()
            = default;

        CHierarchicalModelElement* CHierarchicalModelElement::GetWritableSuperior()
        {
            return m_pSuperior;
        }

        const CHierarchicalModelElement* CHierarchicalModelElement::GetReadOnlySuperior() const
        {
            return m_pSuperior;
        }

        Identifier CHierarchicalModelElement::GetSuperiorInstanceId() const
        {
            if (m_pSuperior)
            {
                return m_pSuperior->GetInstanceId();
            }
            return 0;
        }

        bool CHierarchicalModelElement::IsIndexable() const
        {
            return m_IsIndexable;
        }

        void CHierarchicalModelElement::SetIndexable(const bool Indexable)
        {
            m_IsIndexable = Indexable;
        }

        bool CHierarchicalModelElement::DeserializeAttributes(QDomElement& XmlElement)
        {
            if (CModelElement::DeserializeAttributes(XmlElement) && XmlElement.hasAttribute("Indexable"))
            {
                m_IsIndexable = bool(XmlElement.attribute("Indexable").toUInt());
                return true;
            }
            return false;
        }

        void CHierarchicalModelElement::SerializeAttributes(QDomElement& XmlElement) const
        {
            CModelElement::SerializeAttributes(XmlElement);
            XmlElement.setAttribute("Indexable", m_IsIndexable);
        }
    }
}
#endif //_EVP_USE_MODEL_SPACE
