/*
 * ModelPrimitiveIndexer.cpp
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#include "ModelPrimitiveIndexer.h"

namespace EVP
{
    namespace ModelSpace
    {
        CModelPrimitiveIndexer::CModelPrimitiveIndexer()
            = default;

        CModelPrimitiveIndexer::~CModelPrimitiveIndexer()
            = default;
    }
}
