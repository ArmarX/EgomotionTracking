/*
 * ModelMesh.h
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "HierarchicalModelElementList.h"
#include "HierarchicalModelComposition.h"
#include "ModelMultipleMesh.h"
#include "../Foundation/Mathematics/4D/Matrix4D.h"
#include "../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelVertex;
        class CModelEdge;
        class CModelFace;
        class CModelMultipleFace;
        class CModelMultipleMesh;

        class CModelMesh: public CHierarchicalModelComposition
        {
        public:

            CModelMesh(CModelMultipleMesh* pMultipleMesh);
            CModelMesh(CModelMultipleMesh* pMultipleMesh, const Identifier IntanceId);
            ~CModelMesh() override;

            bool AddVertex(CModelVertex* pModelVertex, const bool CheckUnique);
            bool AddEdge(CModelEdge* pModelEdge, const bool CheckUnique);
            bool AddFace(CModelFace* pModelFace, const bool CheckUnique);
            bool AddMultipleFace(CModelMultipleFace* pModelMultipleFace, const bool CheckUnique);

            bool UpdateIndexability(const real MaximalLength, const real MinimalLength, const real MaximalAperture, const real MinimalAperture, const real MaximalArea, const real MinimalArea);
            bool SetEnableByCriteria(const real MaximalLength, const real MinimalLength, const real MaximalAperture, const real MinimalAperture, const real MaximalArea, const real MinimalArea);

            const THierarchicalModelElementList<CModelVertex*>& GetReadOnlyVeritices() const;
            const THierarchicalModelElementList<CModelEdge*>& GetReadOnlyEdges() const;
            const THierarchicalModelElementList<CModelFace*>& GetReadOnlyFaces() const;
            const THierarchicalModelElementList<CModelMultipleFace*>& GetReadOnlyMultipleFaces() const;

            THierarchicalModelElementList<CModelVertex*>& GetWritableVeritices();
            THierarchicalModelElementList<CModelEdge*>& GetWritableEdges();
            THierarchicalModelElementList<CModelFace*>& GetWritableFaces();
            THierarchicalModelElementList<CModelMultipleFace*>& GetWritableMultipleFaces();

            uint GetTotalPrimitives() const override;

            void ApplyTransformation(const EVP::Mathematics::_4D::CMatrix4D& Transformation);

        protected:

            friend class CModelMultipleMesh;

            void ClearElements();

            bool Deserialize(QDomElement& XmlElement) override;
            QDomElement Serialize(QDomDocument& XmlDocument) const override;

            bool DeserializeAttributes(QDomElement& XmlElement) override;
            void SerializeAttributes(QDomElement& XmlElement) const override;

            THierarchicalModelElementList<CModelVertex*> m_Vertices;
            THierarchicalModelElementList<CModelEdge*> m_Edges;
            THierarchicalModelElementList<CModelFace*> m_Faces;
            THierarchicalModelElementList<CModelMultipleFace*> m_MultipleFaces;
        };
    }
}

#endif // _EVP_USE_MODEL_SPACE
