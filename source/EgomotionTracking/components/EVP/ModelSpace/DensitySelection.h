/*
 * DensitySelection.h
 *
 *  Created on: 24.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "../GlobalSettings.h"

namespace EVP
{
    namespace ModelSpace
    {
        class CDensitySelection
        {
        public:

            CDensitySelection(const Identifier QueryId, const real Density);
            ~CDensitySelection();

            Identifier GetQueryId() const;
            real GetDensity() const;

        protected:

            const Identifier m_QueryId;
            const real m_Density;
        };
    }
}

