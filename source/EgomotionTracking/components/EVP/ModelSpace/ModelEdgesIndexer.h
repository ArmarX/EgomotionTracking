/*
 * ModelEdgesIndexer.h
 *
 *  Created on: 24.11.2011
 *      Author: gonzalez
 */

#pragma once

#include "UnidimensionalPDF.h"
#include "ModelPrimitiveIndexer.h"
#include "HierarchicalModelElementList.h"
#include "../GlobalSettings.h"

#ifdef _EVP_USE_MODEL_SPACE

namespace EVP
{
    namespace ModelSpace
    {
        class CModelMesh;
        class CModelEdge;

        class CModelEdgesIndexer: public CModelPrimitiveIndexer
        {
        public:

            enum IndexingCriterion
            {
                eLenght, eAperture
            };

            CModelEdgesIndexer();
            ~CModelEdgesIndexer() override;

            void ClearEdges();
            void AddEdgesFromMesh(CModelMesh* pModelMesh, const bool CheckUnique, const bool CheckIndexability = true);

            real GetRangeCriterion() const;
            real GetMaximalCriterion() const;
            real GetMinimalCriterion() const;
            IndexingCriterion GetIndexingCriterion() const;
            void SetEdgeArray(const TModelArray<CModelEdge*>& EdgeArray);
            const TModelArray<CModelEdge*>& GetEdgeArray() const;
            void Indexing(const IndexingCriterion SelectedIndexingCriterion, const bool EstimatePDF, const uint DiscretePDFSamples);
            real GetIntervalProbability(const real ValueMaximal, const real ValueMinimal);

            THierarchicalModelElementList<CModelEdge*> GetEdgesBySurfaceAdjacency(const Identifier QueryId, const real MinimalArea, const real MaximalArea, const real Sigma, const real MinimalDensity, const bool SetSelected, const bool AddToSelection);
            CModelPrimitiveIndexingInterval GetIndexingIntervalBySingleValue(const Identifier QueryId, const real Value, const real Sigma, const real MinimalDensity, const bool SetSelected, const bool AddToSelection);
            CModelPrimitiveIndexingInterval GetIndexingIntervalByRangeValue(const Identifier QueryId, const real ValueMaximal, const real ValueMinimal, const real Sigma, const real MinimalDensity, const bool SetSelected, const bool AddToSelection);
            THierarchicalModelElementList<CModelEdge*> GetAdjacentEdgesBySingleValueFromInterval(const Identifier QueryId, const CModelPrimitiveIndexingInterval& IndexingInterval, const real Value, const real Sigma, const real MinimalDensity, const bool SetSelected, const bool AddToSelection);
            THierarchicalModelElementList<CModelEdge*> GetAdjacentEdgesByRangeValueFromInterval(const Identifier QueryId, const CModelPrimitiveIndexingInterval& IndexingInterval, const real ValueMaximal, const real ValueMinimal, const real Sigma, const real MinimalDensity, const bool SetSelected, const bool AddToSelection);
            THierarchicalModelElementList<CModelEdge*> GetAdjacentEdgesBySingleValueFromSelection(const Identifier QueryId, const THierarchicalModelElementList<CModelEdge*>& SourceSelection, const real Value, const real Sigma, const real MinimalDensity, const bool SetSelected, const bool AddToSelection);
            THierarchicalModelElementList<CModelEdge*> GetAdjacentEdgesByRangeValueFromSelection(const Identifier QueryId, const THierarchicalModelElementList<CModelEdge*>& SourceSelection, const real ValueMaximal, const real ValueMinimal, const real Sigma, const real MinimalDensity, const bool SetSelected, const bool AddToSelection);

            void ClearSelectedEdges();
            bool HasSelectedEdges();
            uint GetTotalSelectedEdges();
            const THierarchicalModelElementList<CModelEdge*>& GetSelectedEdges() const;
            THierarchicalModelElementList<CModelMesh*> GetSelectedMeshes();

        protected:

            void EstimateGaussianPDF(const uint TotalDiscretePDFSamples);
            CModelPrimitiveIndexingInterval GetIndexingInterval(const Identifier QueryId, const real MaximalValue, const real m_MinimalValue);

            real m_RangeCriterion;
            real m_MaximalCriterion;
            real m_MinimalCriterion;
            IndexingCriterion m_IndexingCriterion;
            TModelArray<CModelEdge*> m_EdgeArray;
            THierarchicalModelElementList<CModelEdge*> m_SelectedEdges;
            CUnidimensionalPDF m_PDF;

            static  bool SortEdgesByLenght(const CModelEdge* pLhs, const CModelEdge* prhs);
            static  bool SortEdgesByAperture(const CModelEdge* pLhs, const CModelEdge* prhs);
            static  bool SortEdgesByMesh(const CModelEdge* pLhs, const CModelEdge* prhs);
        };
    }
}
#endif // _EVP_USE_MODEL_SPACE
