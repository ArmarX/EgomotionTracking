/*
 * MemoryManager.cpp
 *
 *  Created on: Apr 5, 2012
 *      Author: david
 */

#include "MemoryManager.h"

namespace EVP
{
    namespace Memory
    {
        uint CMemoryManager::GetTotalMemory()
        {

#ifdef _USE_WIN32_

            MEMORYSTATUSEX status;
            status.dwLength = sizeof(status);
            GlobalMemoryStatusEx(&status);
            return status.ullTotalPhys;

#endif

#ifdef _USE_WIN64_

#endif

#ifdef _USE_LINUX32_

            return sysconf(_SC_PHYS_PAGES) * sysconf(_SC_PAGE_SIZE);

#endif

#ifdef _USE_LINUX64_

#endif

#ifdef _USE_MACOS_

#endif

        }

        CMemoryManager::CMemoryManager()
            = default;

        CMemoryManager::~CMemoryManager()
            = default;
    }
}
