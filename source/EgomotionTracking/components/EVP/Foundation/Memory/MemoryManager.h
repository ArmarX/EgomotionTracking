/*
 * MemoryManager.h
 *
 *  Created on: Apr 5, 2012
 *      Author: david
 */

#pragma once

#include "../../GlobalSettings.h"

namespace EVP
{
    namespace Memory
    {
        class CMemoryManager
        {
        public:

            static  uint GetTotalMemory();

        protected:

            CMemoryManager();
            virtual ~CMemoryManager();
        };
    }
}

