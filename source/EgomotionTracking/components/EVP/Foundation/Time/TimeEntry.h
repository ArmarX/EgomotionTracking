/*
 * TimeEntry.h
 *
 *  Created on: Mar 30, 2012
 *      Author: david
 */

#pragma once

#include "../../GlobalSettings.h"

namespace EVP
{
    namespace Time
    {
        class CTimeEntry
        {
        public:

            struct ElapsedTimeInformation
            {
                real m_NaturalTime;
                real m_TicksTime;
                uint m_Ticks;
            };

            CTimeEntry(const Identifier OwnerId);
            CTimeEntry(const Identifier OwnerId, const string& Name);

            void Start();
            void Stop();
            void Stop(const timeval& T1, const clock_t& C1);
            void SetName(const string& Name);
            const string& GetName() const;
            Identifier GetOwnerId() const;
            real GetElapsedTime() const;
            real GetElapsedTicksTime() const;
            uint GetElapsedTicks() const;
            real GetElapsedDeviation() const;
            ElapsedTimeInformation GetElapsedTimeInformation() const;

        protected:

            Identifier m_OwnerId;
            string m_Name;
            timeval m_T0, m_T1;
            real m_ElapsedTime, m_ElapsedTicksTime;
            clock_t m_C0, m_C1;
            uint m_ElapsedTicks;
        };
    }
}

