/*
 * TimeLogger.cpp
 *
 *  Created on: 29.08.2011
 *      Author: gonzalez
 */

#include "TimeLogger.h"

namespace EVP
{
    namespace Time
    {
        Threading::CMutex CTimeLogger::s_TimeEntriesListMutex;
        list<CTimeEntry*> CTimeLogger::s_TimeEntriesList;

        bool CTimeLogger::StartTimeEntry(const Identifier OwnerId, const_string pName)
        {
            if (pName)
            {
                string Name(pName);
                s_TimeEntriesListMutex.BlockingLock();
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                list<CTimeEntry*>::iterator End = s_TimeEntriesList.end();
                for (list<CTimeEntry*>::iterator ppTimeEntry = s_TimeEntriesList.begin(); ppTimeEntry != End; ++ppTimeEntry)
                    if (((*ppTimeEntry)->GetOwnerId() == OwnerId) && ((*ppTimeEntry)->GetName() == Name))
                    {
                        g_ConsoleStringOutput << "TimeLogger[ OwnerId = \"" << OwnerId << "\" Name = \"" << (*ppTimeEntry)->GetName() << "\" ERROR Trying to restart existing time entry ]" << g_EndLine;
                        return false;
                    }
                CTimeEntry* pTimeEntry = new CTimeEntry(OwnerId, Name);
                s_TimeEntriesList.push_back(pTimeEntry);
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                s_TimeEntriesListMutex.Unlock();
                pTimeEntry->Start();
                return true;
            }
            return false;
        }

        real CTimeLogger::StopTimeEntry(const Identifier OwnerId, const_string pName, const bool Verbose)
        {
            if (pName)
            {
                timeval T1;
                gettimeofday(&T1, nullptr);
                clock_t C1 = clock();
                string Name(pName);
                s_TimeEntriesListMutex.BlockingLock();
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                list<CTimeEntry*>::iterator End = s_TimeEntriesList.end();
                for (list<CTimeEntry*>::iterator ppTimeEntry = s_TimeEntriesList.begin(); ppTimeEntry != End; ++ppTimeEntry)
                    if (((*ppTimeEntry)->GetOwnerId() == OwnerId) && ((*ppTimeEntry)->GetName() == Name))
                    {
                        CTimeEntry* pTimeEntry = *ppTimeEntry;
                        pTimeEntry->Stop(T1, C1);
                        s_TimeEntriesList.erase(ppTimeEntry);

                        s_TimeEntriesListMutex.Unlock();
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        const real ElapsedTime = pTimeEntry->GetElapsedTime();
                        if (Verbose)
                        {
                            g_ConsoleStringOutput << "\t\tTimeLogger[ OwnerId = \"" << OwnerId << "\" Name = \"" << pTimeEntry->GetName() << "\" Natural-Elapsed = " << ElapsedTime << " Ticks-Elapsed = " << pTimeEntry->GetElapsedTicksTime() << " Deviation = " << pTimeEntry->GetElapsedDeviation() << " Total-Ticks = " << pTimeEntry->GetElapsedTicks() << " ]" << g_EndLine;
                        }
                        RELEASE_OBJECT(pTimeEntry)
                        return ElapsedTime;
                    }
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                s_TimeEntriesListMutex.Unlock();
                g_ConsoleStringOutput << "\t\tTimeLogger[ OwnerId = \"" << OwnerId << "\" Name = \"" << Name << "\" ERROR Trying to stop non existing time entry ]" << g_EndLine;
            }
            return _REAL_ZERO_;
        }
    }
}
