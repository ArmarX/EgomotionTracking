/*
 * TimeLogger.h
 *
 *  Created on: 29.08.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "../Process/Mutex.h"
#include "TimeEntry.h"

#define TIME_LOGGER_BEGIN(X) Time::CTimeLogger::StartTimeEntry(m_InstanceId,X);
#define TIME_LOGGER_END(X) Time::CTimeLogger::StopTimeEntry(m_InstanceId,X);
#define TIME_LOGGER_VERBOSE_END(X,VERBOSE) Time::CTimeLogger::StopTimeEntry(m_InstanceId,X,VERBOSE)

namespace EVP
{
    namespace Time
    {
        class CTimeLogger
        {
        public:

            static  bool StartTimeEntry(const Identifier OwnerId, const_string pName);
            static  real StopTimeEntry(const Identifier OwnerId, const_string pName, const bool Verbose = true);

        protected:

            static Threading::CMutex s_TimeEntriesListMutex;
            static list<CTimeEntry*> s_TimeEntriesList;
        };
    }
}

