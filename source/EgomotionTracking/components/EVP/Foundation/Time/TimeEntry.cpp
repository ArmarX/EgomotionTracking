/*
 * TimeEntry.cpp
 *
 *  Created on: Mar 30, 2012
 *      Author: david
 */

#include "TimeEntry.h"

namespace EVP
{
    namespace Time
    {
        CTimeEntry::CTimeEntry(const Identifier OwnerId) :
            m_OwnerId(OwnerId), m_ElapsedTime(_REAL_ZERO_), m_ElapsedTicksTime(_REAL_ZERO_), m_C0(0), m_C1(0), m_ElapsedTicks(0)
        {
            memset(&m_T0, 0, sizeof(timeval));
            memset(&m_T1, 0, sizeof(timeval));
        }

        CTimeEntry::CTimeEntry(const Identifier OwnerId, const string& Name) :
            m_OwnerId(OwnerId), m_Name(Name), m_ElapsedTime(_REAL_ZERO_), m_ElapsedTicksTime(_REAL_ZERO_), m_C0(0), m_C1(0), m_ElapsedTicks(0)
        {
        }

        void CTimeEntry::Start()
        {
            gettimeofday(&m_T0, nullptr);
            m_C0 = clock();
        }

        void CTimeEntry::Stop()
        {
            gettimeofday(&m_T1, nullptr);
            m_C1 = clock();
            m_ElapsedTicks = m_C1 - m_C0;
            m_ElapsedTicksTime = real(m_ElapsedTicks * 1000) / real(CLOCKS_PER_SEC);
            m_ElapsedTime = (m_T1.tv_sec == m_T0.tv_sec) ? (real(m_T1.tv_usec - m_T0.tv_usec) / real(1000.0)) : (real(m_T1.tv_sec - m_T0.tv_sec - 1) * real(1000.0) + (m_T1.tv_usec + (real(1000000.0) - m_T0.tv_usec)) / real(1000.0));
        }

        void CTimeEntry::Stop(const timeval& T1, const clock_t& C1)
        {
            m_T1 = T1;
            m_C1 = C1;
            m_ElapsedTicks = m_C1 - m_C0;
            m_ElapsedTicksTime = real(m_ElapsedTicks * 1000) / real(CLOCKS_PER_SEC);
            m_ElapsedTime = (m_T1.tv_sec == m_T0.tv_sec) ? (real(m_T1.tv_usec - m_T0.tv_usec) / real(1000.0)) : (real(m_T1.tv_sec - m_T0.tv_sec - 1) * real(1000.0) + (m_T1.tv_usec + (real(1000000.0) - m_T0.tv_usec)) / real(1000.0));
        }

        void CTimeEntry::SetName(const string& Name)
        {
            m_Name = Name;
        }

        const string& CTimeEntry::GetName() const
        {
            return m_Name;
        }

        Identifier CTimeEntry::GetOwnerId() const
        {
            return m_OwnerId;
        }

        real CTimeEntry::GetElapsedTime() const
        {
            return m_ElapsedTime;
        }

        real CTimeEntry::GetElapsedTicksTime() const
        {
            return m_ElapsedTicksTime;
        }

        uint CTimeEntry::GetElapsedTicks() const
        {
            return m_ElapsedTicks;
        }

        real CTimeEntry::GetElapsedDeviation() const
        {
            return m_ElapsedTime - m_ElapsedTicksTime;
        }

        CTimeEntry::ElapsedTimeInformation CTimeEntry::GetElapsedTimeInformation() const
        {
            ElapsedTimeInformation ETI =
            { m_ElapsedTime, m_ElapsedTicksTime, m_ElapsedTicks };
            return ETI;
        }
    }
}
