/*
 * Mutex.cpp
 *
 *  Created on: 17.10.2011
 *      Author: gonzalez
 */

#include "Mutex.h"

namespace EVP
{
    namespace Threading
    {
        CMutex::CMutex()
        {
            pthread_mutexattr_settype(&m_Attributes, PTHREAD_MUTEX_ERRORCHECK);
            pthread_mutexattr_init(&m_Attributes);
            pthread_mutex_init(&m_Mutex, &m_Attributes);
        }

        CMutex::~CMutex()
        {
            pthread_mutex_destroy(&m_Mutex);
            pthread_mutexattr_destroy(&m_Attributes);
        }

        bool CMutex::IsLocked()
        {
            if (!pthread_mutex_trylock(&m_Mutex))
            {
                pthread_mutex_unlock(&m_Mutex);
                return false;
            }
            return true;
        }

        CMutex::OperationResult CMutex::BlockingLock()
        {
            return CMutex::OperationResult(pthread_mutex_lock(&m_Mutex));
        }

        CMutex::OperationResult CMutex::NonBlockingLock()
        {
            return CMutex::OperationResult(pthread_mutex_trylock(&m_Mutex));
        }

        CMutex::OperationResult CMutex::Unlock()
        {
            return CMutex::OperationResult(pthread_mutex_unlock(&m_Mutex));
        }
    }
}
