/*
 * ThreadPool.h
 *
 *  Created on: 27.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "PoolThread.h"
#include "Condition.h"

namespace EVP
{
    namespace Threading
    {
        class CExecutionUnit;

        class CThreadPool
        {
        public:

            static  string GetCPUBBrandName();
            static  uint GetAvailableLogicalCPUs();
            static  uint GetAvailableLogicalCPUCores();
            static  uint GetAvailablePhysicalCPUCores();
            static  bool IsHyperThreadingAvailable();

            enum StartingThreadSet
            {
                eEmpty, eAvailablePhysicalCPUCores, eAvailableLogicalCPUCores, eAvailableLogicalCPUs
            };

            CThreadPool(const StartingThreadSet Set, const uint MaximalTotalThreads, const CThreadBase::ContentionScope Scope = CThreadBase::eSystemContention);
            CThreadPool(const uint TotalThreads, const CThreadBase::ContentionScope Scope = CThreadBase::eSystemContention);
            virtual ~CThreadPool();

            bool AddThread(const CThreadBase::ContentionScope Scope);

            uint GetTotalThreads();
            uint GetTotalIdleThreads();

            bool DispatchExecutionUnit(CExecutionUnit* pExecutionUnit, const bool ThreadedMode, const uint PriorityOnlyWithRootPermisions = 0);
            void FinalizePool();
            void Synchronize();

            void SetDispatchingEvents(const bool Active);
            bool IsDispatchingEvents() const;

            uint GetConcurrencyLevel();
            bool SetConcurrencyLevel(const uint ConcurrencyLevel);

            bool FinalizeThreadById(const Identifier ThreadId);

        protected:

            friend class CPoolThread;

            virtual void OnBeginFinalizePool() const;
            virtual void OnEndFinalizePool() const;
            virtual void OnBeginSynchronize() const;
            virtual void OnEndSynchronize() const;
            virtual void OnThreadOnIdle(const CPoolThread* pPoolThread) const;
            virtual void OnThreadOnWorking(const CPoolThread* pPoolThread, const CExecutionUnit* pExecutionUnit) const;

            bool SetThreadOnIdle(CPoolThread* pPoolThread);
            uint GetTotalThreadsOnIdle();
            CPoolThread* GetThreadOnIdle();

        private:

            void StartUp(const uint TotalThreads, const CThreadBase::ContentionScope Scope);

            bool m_IsDispatchingEvents;
            std::vector<CPoolThread*> m_Threads;
            std::list<CPoolThread*> m_IdleThreads;
            CCondition m_IdleListCondition;

            static void QueryCPURegisters(unsigned Index, unsigned Registers[4]);
        };
    }
}

