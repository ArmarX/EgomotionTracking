/*
 * ExecutionUnit.h
 *
 *  Created on: 27.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "Mutex.h"

namespace EVP
{
    namespace Threading
    {
        class CPoolThread;

        class CExecutionUnit: public CMutex
        {
        public:

            CExecutionUnit();
            ~CExecutionUnit() override;

            virtual  bool Distpatch(volatile const bool* pKeepRunning);
            virtual void SetExecuterPoolThread(CPoolThread* pPoolThread);
            virtual void ClearExecuterPoolThread();
            CPoolThread* GetExecuterPoolThread();

        protected:

            bool KeepRunning() const;
            virtual  bool Execute() = 0;

            volatile const bool* m_pKeepRunning;
            CPoolThread* m_pPoolThread;
        };
    }
}

