/*
 * ThreadPool.cpp
 *
 *  Created on: 27.02.2012
 *      Author: gonzalez
 */

#include "ThreadPool.h"
#include "ExecutionUnit.h"

namespace EVP
{
    namespace Threading
    {
        void CThreadPool::QueryCPURegisters(unsigned Index, unsigned Registers[4])
        {

#ifdef _USE_WIN32_
            __cpuid((int*) Registers, (int) Index);
#endif
#ifdef _USE_LINUX32_
            asm volatile("cpuid" : "=a"(Registers[0]), "=b"(Registers[1]), "=c"(Registers[2]), "=d"(Registers[3]): "a"(Index), "c"(0));
#endif
        }

        string CThreadPool::GetCPUBBrandName()
        {
            uint Registers[4] =
            { 0 };
            QueryCPURegisters(0, Registers);
            char CPUBBrandName[16] =
            { 0 };
            memcpy(&CPUBBrandName[0], &Registers[1], sizeof(uint));
            memcpy(&CPUBBrandName[4], &Registers[3], sizeof(uint));
            memcpy(&CPUBBrandName[8], &Registers[2], sizeof(uint));
            return string(CPUBBrandName);
        }

        uint CThreadPool::GetAvailableLogicalCPUs()
        {
            uint Registers[4] =
            { 0 };
            QueryCPURegisters(1, Registers);
            return (Registers[1] >> 16) & 0xFF;
        }

        uint CThreadPool::GetAvailableLogicalCPUCores()
        {
            uint Registers[4] =
            { 0 };
            const string CPUBBrand = CThreadPool::GetCPUBBrandName();
            uint AvailableLogicalCPUCores = 0;
            if (CPUBBrand == "GenuineIntel")
            {
                QueryCPURegisters(4, Registers);
                AvailableLogicalCPUCores = ((Registers[0] >> 26) & 0x3F) + 1;
            }
            else if (CPUBBrand == "AuthenticAMD")
            {
                QueryCPURegisters(0x80000008, Registers);
                AvailableLogicalCPUCores = ((unsigned)(Registers[2] & 0xFF)) + 1;
            }
            return AvailableLogicalCPUCores;
        }

        uint CThreadPool::GetAvailablePhysicalCPUCores()
        {
            uint Registers[4] =
            { 0 };
            const string CPUBBrand = CThreadPool::GetCPUBBrandName();
            uint AvailablePhysicalCPUCores = 0;
            if (CPUBBrand == "GenuineIntel")
            {
                QueryCPURegisters(4, Registers);
                AvailablePhysicalCPUCores = ((Registers[0] >> 26) & 0x3F) + 1;
            }
            else if (CPUBBrand == "AuthenticAMD")
            {
                QueryCPURegisters(0x80000008, Registers);
                AvailablePhysicalCPUCores = ((unsigned)(Registers[2] & 0xFF)) + 1;
            }
            QueryCPURegisters(1, Registers);
            if ((Registers[3] & (1 << 28)) && (AvailablePhysicalCPUCores < GetAvailableLogicalCPUs()))
            {
                return AvailablePhysicalCPUCores / 2;
            }
            return AvailablePhysicalCPUCores;
        }

        bool CThreadPool::IsHyperThreadingAvailable()
        {
            const uint AvailableLogicalCPUs = GetAvailableLogicalCPUs();
            const uint AvailablePhysicalCPUCores = GetAvailablePhysicalCPUCores();
            uint Registers[4];
            QueryCPURegisters(1, Registers);
            return ((Registers[3] & (1 << 28)) && (AvailablePhysicalCPUCores < AvailableLogicalCPUs));
        }

        CThreadPool::CThreadPool(const StartingThreadSet Set, const uint MaximalTotalThreads, const CThreadBase::ContentionScope Scope) :
            m_IsDispatchingEvents(false)
        {
            uint TotalThreads = 0;
            switch (Set)
            {
                case eEmpty:
                    break;
                case eAvailablePhysicalCPUCores:
                    TotalThreads = CThreadPool::GetAvailablePhysicalCPUCores();
                    break;
                case eAvailableLogicalCPUCores:
                    TotalThreads = CThreadPool::GetAvailableLogicalCPUCores();
                    break;
                case eAvailableLogicalCPUs:
                    TotalThreads = CThreadPool::GetAvailableLogicalCPUs();
                    break;
            }
            StartUp(TMin(MaximalTotalThreads, TotalThreads), Scope);
        }

        CThreadPool::CThreadPool(const uint TotalThreads, const CThreadBase::ContentionScope Scope) :
            m_IsDispatchingEvents(false)
        {
            StartUp(TotalThreads, Scope);
        }

        CThreadPool::~CThreadPool()
        {
            FinalizePool();
        }

        void CThreadPool::StartUp(const uint TotalThreads, const CThreadBase::ContentionScope Scope)
        {
            for (uint i = 0; i < TotalThreads; ++i)
            {
                CPoolThread* pPoolThread = new CPoolThread(i, this);
                m_Threads.push_back(pPoolThread);
                pPoolThread->Start(Scope);
            }
        }

        bool CThreadPool::AddThread(const CThreadBase::ContentionScope Scope)
        {
            CPoolThread* pPoolThread = new CPoolThread(m_Threads.size(), this);
            m_Threads.push_back(pPoolThread);
            return pPoolThread->Start(Scope);
        }

        uint CThreadPool::GetTotalThreads()
        {
            return m_Threads.size();
        }

        uint CThreadPool::GetTotalIdleThreads()
        {
            m_IdleListCondition.BlockingLock();
            const uint TotalIdleThreads = m_IdleThreads.size();
            m_IdleListCondition.Unlock();
            return TotalIdleThreads;
        }

        bool CThreadPool::DispatchExecutionUnit(CExecutionUnit* pExecutionUnit, const bool ThreadedMode, const uint PriorityOnlyWithRootPermisions)
        {
            if (pExecutionUnit)
            {
                if (ThreadedMode)
                {
                    CPoolThread* pPoolThread = GetThreadOnIdle();
                    if (pPoolThread)
                    {
                        if (PriorityOnlyWithRootPermisions)
                        {
                            pPoolThread->SetPriority(PriorityOnlyWithRootPermisions);
                        }
                        if (m_IsDispatchingEvents)
                        {
                            OnThreadOnWorking(pPoolThread, pExecutionUnit);
                        }
                        return pPoolThread->SetExecutionUnit(pExecutionUnit);
                    }
                }
                else
                {
                    pExecutionUnit->BlockingLock();
                    const bool SequentiaExecutionResult = pExecutionUnit->Distpatch(nullptr);
                    pExecutionUnit->Unlock();
                    return SequentiaExecutionResult;
                }
            }
            return false;
        }

        void CThreadPool::FinalizePool()
        {
            if (m_Threads.size())
            {
                if (m_IsDispatchingEvents)
                {
                    OnBeginFinalizePool();
                }
                std::vector<CPoolThread*>::iterator EndThreads = m_Threads.end();
                for (std::vector<CPoolThread*>::iterator ppPoolThread = m_Threads.begin(); ppPoolThread != EndThreads; ++ppPoolThread)
                {
                    CPoolThread* pPoolThread = *ppPoolThread;
                    pPoolThread->Finalize();
                    pPoolThread->GetDeletionMutex()->BlockingLock();
                    delete pPoolThread;
                }
                m_Threads.clear();
                m_IdleThreads.clear();
                if (m_IsDispatchingEvents)
                {
                    OnEndFinalizePool();
                }
            }
        }

        void CThreadPool::Synchronize()
        {
            const uint TotalThreads = m_Threads.size();
            if (TotalThreads)
            {
                if (m_IsDispatchingEvents)
                {
                    OnBeginSynchronize();
                }

                m_IdleListCondition.BlockingLock();
                while (m_IdleThreads.size() != TotalThreads)
                {
                    m_IdleListCondition.Wait();
                }
                m_IdleListCondition.Unlock();

                if (m_IsDispatchingEvents)
                {
                    OnEndSynchronize();
                }
            }
        }

        void CThreadPool::SetDispatchingEvents(const bool Active)
        {
            m_IsDispatchingEvents = Active;
        }

        bool CThreadPool::IsDispatchingEvents() const
        {
            return m_IsDispatchingEvents;
        }

        uint CThreadPool::GetConcurrencyLevel()
        {
            return pthread_getconcurrency();
        }

        bool CThreadPool::SetConcurrencyLevel(const uint ConcurrencyLevel)
        {
            return !pthread_setconcurrency(ConcurrencyLevel);
        }

        bool CThreadPool::FinalizeThreadById(const Identifier ThreadId)
        {
            std::vector<CPoolThread*>::const_iterator EndThreads = m_Threads.end();
            for (std::vector<CPoolThread*>::const_iterator ppPoolThread = m_Threads.begin(); ppPoolThread != EndThreads; ++ppPoolThread)
                if ((*ppPoolThread)->GetThreadId() == ThreadId)
                {
                    (*ppPoolThread)->Finalize();
                    return true;
                }
            return false;
        }

        void CThreadPool::OnBeginFinalizePool() const
        {
        }

        void CThreadPool::OnEndFinalizePool() const
        {
        }

        void CThreadPool::OnBeginSynchronize() const
        {
        }

        void CThreadPool::OnEndSynchronize() const
        {
        }

        void CThreadPool::OnThreadOnIdle(const CPoolThread* /*pPoolThread*/) const
        {
        }

        void CThreadPool::OnThreadOnWorking(const CPoolThread* /*pPoolThread*/, const CExecutionUnit* /*pExecutionUnit*/) const
        {
        }

        bool CThreadPool::SetThreadOnIdle(CPoolThread* pPoolThread)
        {
            if (pPoolThread && (pPoolThread->GetThreadPool() == this))
            {
                m_IdleListCondition.BlockingLock();
                if (m_IdleThreads.size())
                {
                    list<CPoolThread*>::const_iterator EndThreads = m_IdleThreads.end();
                    for (list<CPoolThread*>::const_iterator ppPoolThread = m_IdleThreads.begin(); ppPoolThread != EndThreads; ++ppPoolThread)
                        if ((*ppPoolThread) == pPoolThread)
                        {
                            m_IdleListCondition.Unlock();
                            return false;
                        }
                }
                m_IdleThreads.push_back(pPoolThread);
                if (m_IsDispatchingEvents)
                {
                    OnThreadOnIdle(pPoolThread);
                }
                m_IdleListCondition.Broadcast();
                m_IdleListCondition.Unlock();
                return true;
            }
            return true;
        }

        uint CThreadPool::GetTotalThreadsOnIdle()
        {
            return m_IdleThreads.size();
        }

        CPoolThread* CThreadPool::GetThreadOnIdle()
        {
            if (m_Threads.size())
            {
                m_IdleListCondition.BlockingLock();
                while (m_IdleThreads.empty())
                {
                    m_IdleListCondition.Wait();
                }
                CPoolThread* pPoolThread = m_IdleThreads.front();
                m_IdleThreads.pop_front();
                m_IdleListCondition.Unlock();
                return pPoolThread;
            }
            return nullptr;
        }
    }
}
