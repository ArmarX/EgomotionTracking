/*
 * Process.cpp
 *
 *  Created on: 20.09.2011
 *      Author: gonzalez
 */

#include "../Files/OutFile.h"
#include "Process.h"

namespace EVP
{
    namespace Process
    {
        IDENTIFIABLE_INITIALIZATION(CProcess)

        CProcess::CProcess(const string& Name, const Identifier IntanceId) :
#ifdef _USE_PROCESS_PROFILING_
            CIdentifiableInstance(IntanceId), m_IsEnabled(true), m_Name(Name), m_TotalExecutions(0), m_ExecutionTrialId(0)/*, m_ExecutionMutex(this)*/, m_IsLogVerbose(true), m_IsLogEnabled(true), m_LogCurrentTrialId(0), m_LogCurrentSubprocessId(0), m_LogTotalNaturalExecutionTime(_REAL_ZERO_), m_LogTimeEntry(m_InstanceId)
#else
            CIdentifiableInstance(IntanceId), m_IsEnabled(true), m_Name(Name), m_TotalExecutions(0), m_ExecutionTrialId(0), m_ExecutionMutex(this)
#endif
        {
        }

        CProcess::CProcess(const_string pName, const Identifier IntanceId) :
#ifdef _USE_PROCESS_PROFILING_
            CIdentifiableInstance(IntanceId), m_IsEnabled(true), m_Name(pName), m_TotalExecutions(0), m_ExecutionTrialId(0)/*, m_ExecutionMutex(this)*/, m_IsLogVerbose(true), m_IsLogEnabled(true), m_LogCurrentTrialId(0), m_LogCurrentSubprocessId(0), m_LogTotalNaturalExecutionTime(_REAL_ZERO_), m_LogTimeEntry(m_InstanceId)
#else
            CIdentifiableInstance(IntanceId), m_IsEnabled(true), m_Name(pName), m_TotalExecutions(0), m_ExecutionTrialId(0), m_ExecutionMutex(this)
#endif
        {
        }

        CProcess::CProcess(const string& Name) :
#ifdef _USE_PROCESS_PROFILING_
            IDENTIFIABLE_CONTRUCTOR(CProcess), m_IsEnabled(true), m_Name(Name), m_TotalExecutions(0), m_ExecutionTrialId(0)/*, m_ExecutionMutex(this)*/, m_IsLogVerbose(true), m_IsLogEnabled(true), m_LogCurrentTrialId(0), m_LogCurrentSubprocessId(0), m_LogTotalNaturalExecutionTime(_REAL_ZERO_), m_LogTimeEntry(m_InstanceId)
#else
            IDENTIFIABLE_CONTRUCTOR(CProcess), m_IsEnabled(true), m_Name(Name), m_TotalExecutions(0), m_ExecutionTrialId(0), m_ExecutionMutex(this)
#endif
        {
        }

        CProcess::CProcess(const_string pName) :
#ifdef _USE_PROCESS_PROFILING_
            IDENTIFIABLE_CONTRUCTOR(CProcess), m_IsEnabled(true), m_Name(pName), m_TotalExecutions(0), m_ExecutionTrialId(0)/*, m_ExecutionMutex(this)*/, m_IsLogVerbose(true), m_IsLogEnabled(true), m_LogCurrentTrialId(0), m_LogCurrentSubprocessId(0), m_LogTotalNaturalExecutionTime(_REAL_ZERO_), m_LogTimeEntry(m_InstanceId)
#else
            IDENTIFIABLE_CONTRUCTOR(CProcess), m_IsEnabled(true), m_Name(pName), m_TotalExecutions(0), m_ExecutionTrialId(0), m_ExecutionMutex(this)
#endif
        {
        }

        CProcess::~CProcess()
            = default;

        bool CProcess::IsEnabled() const
        {
            return m_IsEnabled;
        }

        const string& CProcess::GetName() const
        {
            return m_Name;
        }

        Threading::CMutex* CProcess::GetExecutionMutex()
        {
            //return &m_ExecutionMutex;
            return nullptr;
        }

        uint CProcess::GetTotalExecutions()
        {
            return m_TotalExecutions;
        }

        bool CProcess::IsRunning()
        {
            /*switch (m_ExecutionMutex.TryLock())
             {
             case Threading::CMutex::eAlreadyLocked:
             return true;
             break;
             case Threading::CMutex::eSuccesfullyLocked:
             //m_ExecutionMutex.Unlock();
             return false;
             break;
             default:
             return false;
             }*/
            return false;
        }

        bool CProcess::BeginSettingsChangeBlockExecution(const bool Wait)
        {
            return true;
            /*if (Wait)
             return (m_ExecutionMutex.Lock() == Threading::CMutex::eSuccesfullyLocked);
             else
             return (m_ExecutionMutex.TryLock() == Threading::CMutex::eSuccesfullyLocked);*/
        }

        bool CProcess::EndSettingsChangeBlockExecution()
        {
            return true;
            /*return (m_ExecutionMutex.Unlock() == Threading::CMutex::eSuccessfullyUnlocked);*/
        }

        bool CProcess::StartExecuting(const Identifier TrialId)
        {
            //if (m_ExecutionMutex.Lock() == Threading::CMutex::eSuccesfullyLocked)
            {
                m_ExecutionTrialId = TrialId;
                return true;
            }
            return false;
        }

        bool CProcess::FinishExecution()
        {
            //if (m_ExecutionMutex.Unlock() == Threading::CMutex::eSuccessfullyUnlocked)
            {
                ++m_TotalExecutions;
                return true;
            }
            return false;
        }

        bool CProcess::StartDisplaying()
        {
            return true;
            //return (m_ExecutionMutex.Lock() == Threading::CMutex::eSuccesfullyLocked);
        }

        bool CProcess::FinishDisplaying()
        {
            return true;
            //return (m_ExecutionMutex.Unlock() == Threading::CMutex::eSuccessfullyUnlocked);
        }

#ifdef _USE_PROCESS_PROFILING_

        void CProcess::StartExecutionLog(const_string SubprocessName, const Identifier TrialId)
        {
            m_LogCurrentSubprocessName = string(SubprocessName);
            m_LogCurrentTrialId = TrialId;
            m_LogCurrentSubprocessId = GetRegisterSubprocessId(m_LogCurrentSubprocessName);
            m_LogTimeEntry.Start();
        }

        void CProcess::StopExecutionLog()
        {
            m_LogTimeEntry.Stop();
            LogExecutionTrial ExecutionTrial;
            ExecutionTrial.m_ElapsedTimeInformation = m_LogTimeEntry.GetElapsedTimeInformation();
            ExecutionTrial.m_TrialId = m_LogCurrentTrialId;
            ExecutionTrial.m_SubprocessId = m_LogCurrentSubprocessId;
            m_LogTotalNaturalExecutionTime += ExecutionTrial.m_ElapsedTimeInformation.m_NaturalTime;
            m_LogExecutionTrials.push_back(ExecutionTrial);
            if (m_IsLogVerbose)
            {
                g_ConsoleStringOutput << g_Tabulator << "[" << m_InstanceId << "] : " << m_Name << " [ Trial Id = " << ExecutionTrial.m_TrialId << ", Subprocess = \"" << m_LogCurrentSubprocessName << "\", Elapsed  =  " << ExecutionTrial.m_ElapsedTimeInformation.m_NaturalTime << " (ms) | " << ExecutionTrial.m_ElapsedTimeInformation.m_TicksTime << " (ms) | " << ExecutionTrial.m_ElapsedTimeInformation.m_Ticks << " (TKS) ]" << g_EndLine;
            }
        }

        void CProcess::SetLogEnabled(const bool Enabled)
        {
            m_IsLogEnabled = Enabled;
        }

        bool CProcess::IsLogEnabled() const
        {
            return m_IsLogEnabled;
        }

        void CProcess::SetVerbose(const bool Verbose)
        {
            m_IsLogVerbose = Verbose;
        }

        bool CProcess::IsVerbose() const
        {
            return m_IsLogVerbose;
        }

        void CProcess::ClearLog()
        {
            m_LogTotalNaturalExecutionTime = _REAL_ZERO_;
            m_LogExecutionTrials.clear();
        }

        uint CProcess::GetTotalExecutions() const
        {
            return m_LogExecutionTrials.size();
        }

        real CProcess::GetLastExecutionNaturalTime() const
        {
            return m_LogExecutionTrials.size() ? m_LogExecutionTrials.back().m_ElapsedTimeInformation.m_NaturalTime : _REAL_ZERO_;
        }

        real CProcess::GetLastExecutionTicksTime() const
        {
            return m_LogExecutionTrials.size() ? m_LogExecutionTrials.back().m_ElapsedTimeInformation.m_TicksTime : _REAL_ZERO_;
        }

        uint CProcess::GetLastExecutionTicks() const
        {
            return m_LogExecutionTrials.size() ? m_LogExecutionTrials.back().m_ElapsedTimeInformation.m_Ticks : _REAL_ZERO_;
        }

        real CProcess::GetGlobalMeanExecutionTime() const
        {
            return m_LogExecutionTrials.size() ? m_LogTotalNaturalExecutionTime / real(m_LogExecutionTrials.size()) : _REAL_ZERO_;
        }

        real CProcess::GetGlobalTotalExecutionTime() const
        {
            return m_LogTotalNaturalExecutionTime;
        }

        uint CProcess::GetRegisterSubprocessId(const string& SearchingSubprocessName)
        {
            uint Index = 0;
            for (list<string>::const_iterator pSubprocessName = m_RegisteredSubprocessNames.begin(); pSubprocessName != m_RegisteredSubprocessNames.end(); ++pSubprocessName, ++Index)
                if (*pSubprocessName == SearchingSubprocessName)
                {
                    return Index;
                }
            m_RegisteredSubprocessNames.push_back(SearchingSubprocessName);
            return m_RegisteredSubprocessNames.size() - 1;
        }

        real CProcess::GetSubprocessMeanExecutionNaturalTime(const Identifier SubprocessId) const
        {
            if (m_LogExecutionTrials.size())
            {
                real AccumulatorTime = _REAL_ZERO_;
                uint TotalCallsSubprocessId = 0;
                list<LogExecutionTrial>::const_iterator EndLogExecutionTrials = m_LogExecutionTrials.end();
                for (list<LogExecutionTrial>::const_iterator pLogExecutionTrial = m_LogExecutionTrials.begin(); pLogExecutionTrial != EndLogExecutionTrials; ++pLogExecutionTrial)
                    if (pLogExecutionTrial->m_SubprocessId == SubprocessId)
                    {
                        AccumulatorTime += pLogExecutionTrial->m_ElapsedTimeInformation.m_NaturalTime;
                        ++TotalCallsSubprocessId;
                    }
                if (TotalCallsSubprocessId)
                {
                    return AccumulatorTime / real(TotalCallsSubprocessId);
                }
            }
            return _REAL_ZERO_;
        }

        real CProcess::GetSubprocessMeanExecutionTicksTime(const Identifier SubprocessId) const
        {
            if (m_LogExecutionTrials.size())
            {
                real AccumulatorTime = _REAL_ZERO_;
                uint TotalCallsSubprocessId = 0;
                list<LogExecutionTrial>::const_iterator EndLogExecutionTrials = m_LogExecutionTrials.end();
                for (list<LogExecutionTrial>::const_iterator pLogExecutionTrial = m_LogExecutionTrials.begin(); pLogExecutionTrial != EndLogExecutionTrials; ++pLogExecutionTrial)
                    if (pLogExecutionTrial->m_SubprocessId == SubprocessId)
                    {
                        AccumulatorTime += pLogExecutionTrial->m_ElapsedTimeInformation.m_TicksTime;
                        ++TotalCallsSubprocessId;
                    }
                if (TotalCallsSubprocessId)
                {
                    return AccumulatorTime / real(TotalCallsSubprocessId);
                }
            }
            return _REAL_ZERO_;
        }

        real CProcess::GetSubprocessMeanExecutionTicks(const Identifier SubprocessId) const
        {
            if (m_LogExecutionTrials.size())
            {
                uint AccumulatorTicks = 0;
                uint TotalCallsSubprocessId = 0;
                list<LogExecutionTrial>::const_iterator EndLogExecutionTrials = m_LogExecutionTrials.end();
                for (list<LogExecutionTrial>::const_iterator pLogExecutionTrial = m_LogExecutionTrials.begin(); pLogExecutionTrial != EndLogExecutionTrials; ++pLogExecutionTrial)
                    if (pLogExecutionTrial->m_SubprocessId == SubprocessId)
                    {
                        AccumulatorTicks += pLogExecutionTrial->m_ElapsedTimeInformation.m_Ticks;
                        ++TotalCallsSubprocessId;
                    }
                if (TotalCallsSubprocessId)
                {
                    return real(AccumulatorTicks) / real(TotalCallsSubprocessId);
                }
            }
            return _REAL_ZERO_;
        }

        bool CProcess::ExportProcessExecutions(const_string pPathFileName, const ExportExecutionTrialsSortingMode Mode)
        {
            if (m_IsEnabled && m_LogExecutionTrials.size())
            {
                switch (Mode)
                {
                    case eByTrial:
                        m_LogExecutionTrials.sort(SortLogExecutionTrialByTrialId);
                        break;
                    case eBySubProcess:
                        m_LogExecutionTrials.sort(SortLogExecutionTrialBySubprocessId);
                        break;
                    case eByMaximaNaturalTime:
                        m_LogExecutionTrials.sort(SortLogExecutionTrialByMaximaNaturalTime);
                        break;
                    case ByMaximaTicksTime:
                        m_LogExecutionTrials.sort(SortLogExecutionTrialByMaximaTicksTime);
                        break;
                    case ByMaximaTicks:
                        m_LogExecutionTrials.sort(SortLogExecutionTrialByMaximaTicks);
                        break;
                    case ByMinimalNaturalTime:
                        m_LogExecutionTrials.sort(SortLogExecutionTrialByMinimalNaturalTime);
                        break;
                    case ByMinimalaTicksTime:
                        m_LogExecutionTrials.sort(SortLogExecutionTrialByMinimalTicksTime);
                        break;
                    case eByMinimalTicks:
                        m_LogExecutionTrials.sort(SortLogExecutionTrialByMinimalTicks);
                        break;
                }
                ostringstream OutputText;
                OutputText << "Process \"" << m_Name << "\"\nInstance Id [" << m_InstanceId << "]" << g_EndLine;
                OutputText << g_Tabulator << "Total Executions" << g_Tabulator << m_LogExecutionTrials.size() << g_EndLine;
                OutputText << g_Tabulator << "Total Execution Time (ms)" << g_Tabulator << m_LogTotalNaturalExecutionTime << g_EndLine;
                OutputText << g_DoubleTabulator << "Trial Id" << g_Tabulator << "Subprocess Id" << g_Tabulator << "Natural Time (ms)" << g_Tabulator << "Ticks Time (ms)" << g_Tabulator << "Ticks (TKS)" << g_EndLine;
                list<LogExecutionTrial>::iterator EndProcessExecutions = m_LogExecutionTrials.end();
                for (list<LogExecutionTrial>::iterator pLogExecutionTrial = m_LogExecutionTrials.begin(); pLogExecutionTrial != EndProcessExecutions; ++pLogExecutionTrial)
                {
                    OutputText << g_DoubleTabulator << pLogExecutionTrial->m_TrialId << g_Tabulator << pLogExecutionTrial->m_SubprocessId << g_Tabulator << pLogExecutionTrial->m_ElapsedTimeInformation.m_NaturalTime << g_Tabulator << pLogExecutionTrial->m_ElapsedTimeInformation.m_TicksTime << g_Tabulator << pLogExecutionTrial->m_ElapsedTimeInformation.m_Ticks << g_EndLine;
                }
                return Files::COutFile::WriteStringToFile(pPathFileName, OutputText.str());
            }
            return false;
        }

        bool CProcess::SortLogExecutionTrialByTrialId(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs)
        {
            if (lhs.m_TrialId == rhs.m_TrialId)
            {
                return lhs.m_SubprocessId < rhs.m_SubprocessId;
            }
            return lhs.m_TrialId < rhs.m_TrialId;
        }

        bool CProcess::SortLogExecutionTrialBySubprocessId(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs)
        {
            if (lhs.m_SubprocessId == rhs.m_SubprocessId)
            {
                return lhs.m_TrialId < rhs.m_TrialId;
            }
            return lhs.m_SubprocessId < rhs.m_SubprocessId;
        }

        bool CProcess::SortLogExecutionTrialByMaximaNaturalTime(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs)
        {
            return lhs.m_ElapsedTimeInformation.m_NaturalTime > rhs.m_ElapsedTimeInformation.m_NaturalTime;
        }

        bool CProcess::SortLogExecutionTrialByMaximaTicksTime(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs)
        {
            return lhs.m_ElapsedTimeInformation.m_TicksTime > rhs.m_ElapsedTimeInformation.m_TicksTime;
        }

        bool CProcess::SortLogExecutionTrialByMaximaTicks(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs)
        {
            return lhs.m_ElapsedTimeInformation.m_Ticks > rhs.m_ElapsedTimeInformation.m_Ticks;
        }

        bool CProcess::SortLogExecutionTrialByMinimalNaturalTime(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs)
        {
            return lhs.m_ElapsedTimeInformation.m_NaturalTime < rhs.m_ElapsedTimeInformation.m_NaturalTime;
        }

        bool CProcess::SortLogExecutionTrialByMinimalTicksTime(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs)
        {
            return lhs.m_ElapsedTimeInformation.m_TicksTime < rhs.m_ElapsedTimeInformation.m_TicksTime;
        }

        bool CProcess::SortLogExecutionTrialByMinimalTicks(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs)
        {
            return lhs.m_ElapsedTimeInformation.m_Ticks < rhs.m_ElapsedTimeInformation.m_Ticks;
        }
#endif

    }
}
