/*
 * Mutex.h
 *
 *  Created on: 17.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"

namespace EVP
{
    namespace Threading
    {
        class CMutex
        {
        public:

            enum OperationResult
            {
                eSuccess = 0, eMutexErrorStatusNotCallingThreadAlreadyLocked = EBUSY, eMutexErrorStatusUninitialized = EINVAL, eMutexErrorStatusCallingThreadAlreadyLocked = EDEADLK, eMutexErrorStatusNotOwner = EPERM
            };

            CMutex();
            virtual ~CMutex();

            bool IsLocked();
            OperationResult BlockingLock();
            OperationResult NonBlockingLock();
            OperationResult Unlock();

        protected:

            pthread_mutex_t m_Mutex;
            pthread_mutexattr_t m_Attributes;
        };
    }
}

