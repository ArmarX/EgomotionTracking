/*
 * ThreadBase.cpp
 *
 *  Created on: 17.10.2011
 *      Author: gonzalez
 */

#include "ThreadBase.h"

namespace EVP
{
    namespace Threading
    {
        IDENTIFIABLE_INITIALIZATION(CThreadBase)

        uint CThreadBase::GetMaximalPriorityAvailable()
        {
            return sched_get_priority_max(SCHED_RR);
        }

        uint CThreadBase::GetMinimalPriorityAvailable()
        {
            return sched_get_priority_min(SCHED_RR);
        }

        CThreadBase::CThreadBase(const Identifier ThreadId) :
            CIdentifiableInstance(ThreadId), m_Status(eStopped), m_AttachableMode(eJoinable), m_ContentionScope(eSystemContention), m_ThreadId(ThreadId), m_TrialId(0), m_Thread(0)
        {
        }

        CThreadBase::~CThreadBase()
        {
            Stop();
        }

        bool CThreadBase::SetPriority(const uint Priority)
        {
            if (m_Thread)
            {
                const uint MaximalPriorityAvailable = CThreadBase::GetMaximalPriorityAvailable();
                const uint MinimalPriorityAvailable = CThreadBase::GetMinimalPriorityAvailable();
                if ((Priority <= MaximalPriorityAvailable) && (Priority >= MinimalPriorityAvailable))
                {
                    sched_param PriorityStructure;
                    PriorityStructure.__sched_priority = Priority;
                    return !pthread_setschedparam(m_Thread, SCHED_RR, &PriorityStructure);
                }
            }
            return false;
        }

        uint CThreadBase::GetPriority()
        {
            if (m_Thread)
            {
                sched_param PriorityStructure;
                PriorityStructure.__sched_priority = -1;
                int Policy = -1;
                if (!pthread_getschedparam(m_Thread, &Policy, &PriorityStructure))
                {
                    return uint(PriorityStructure.__sched_priority);
                }
            }
            return 0;
        }

        bool CThreadBase::IsRunning() const
        {
            return (m_Status == eRunning);
        }

        bool CThreadBase::IsStopped() const
        {
            return (m_Status == eStopped);
        }

        CThreadBase::Status CThreadBase::GetStatus() const
        {
            return m_Status;
        }

        CThreadBase::AttachableMode CThreadBase::GetAttachableMode() const
        {
            return m_AttachableMode;
        }

        CThreadBase::ContentionScope CThreadBase::GetContentionScope() const
        {
            return m_ContentionScope;
        }

        Identifier CThreadBase::GetThreadId() const
        {
            return m_ThreadId;
        }

        void CThreadBase::IcrementTrialId()
        {
            ++m_TrialId;
        }

        void CThreadBase::SetTrialId(const Identifier TrialId)
        {
            m_TrialId = TrialId;
        }

        Identifier CThreadBase::GetTrialId() const
        {
            return m_TrialId;
        }

        bool CThreadBase::Start(const AttachableMode Mode, const ContentionScope Scope)
        {
            return Create(Mode, Scope);
        }

        bool CThreadBase::Stop()
        {
            if (Stopping())
            {
                if (pthread_cancel(m_Thread))
                {
                    return false;
                }
                m_Status = eStopped;
                return true;
            }
            return false;
        }

        bool CThreadBase::Detach()
        {
            if ((m_Status == eRunning) && (m_AttachableMode == eJoinable))
                if (!pthread_detach(m_Thread))
                {
                    m_AttachableMode = eDetached;
                    return true;
                }
            return false;
        }

        bool CThreadBase::Join(void** ppReturnedValue)
        {
            if ((m_Status == eRunning) && (m_AttachableMode == eJoinable))
            {
                return !pthread_join(m_Thread, ppReturnedValue);
            }
            return false;
        }

        uint CThreadBase::GetCPUAffinityMask()
        {
            uint CPUAffinity = 0;
            cpu_set_t CPUSetStructure;
            if (!pthread_getaffinity_np(m_Thread, sizeof(cpu_set_t), &CPUSetStructure))
                for (uint i = 0; i < CPU_SETSIZE; i++)
                    if (CPU_ISSET(i, &CPUSetStructure))
                    {
                        CPUAffinity |= (0x1 << i);
                    }
            return CPUAffinity;
        }

        bool CThreadBase::Create(const AttachableMode Mode, const ContentionScope Scope)
        {
            if (m_Status == eStopped)
            {
                m_AttachableMode = Mode;
                m_ContentionScope = Scope;
                if (!pthread_attr_init(&m_Attributes))
                {
                    switch (m_AttachableMode)
                    {
                        case eJoinable:
                            if (pthread_attr_setdetachstate(&m_Attributes, PTHREAD_CREATE_JOINABLE))
                            {
                                return false;
                            }
                            break;
                        case eDetached:
                            if (pthread_attr_setdetachstate(&m_Attributes, PTHREAD_CREATE_DETACHED))
                            {
                                return false;
                            }
                            break;
                    }
                    switch (m_ContentionScope)
                    {
                        case eProcessContention:
                            if (pthread_attr_setscope(&m_Attributes, PTHREAD_SCOPE_PROCESS))
                            {
                                return false;
                            }
                            break;
                        case eSystemContention:
                            if (pthread_attr_setscope(&m_Attributes, PTHREAD_SCOPE_SYSTEM))
                            {
                                return false;
                            }
                            break;
                    }
                    if (!pthread_attr_setschedpolicy(&m_Attributes, SCHED_RR))
                    {
                        return !pthread_create(&m_Thread, &m_Attributes, CThreadBase::Dispatch, this);
                    }
                }
            }
            return false;
        }

        bool CThreadBase::Yield()
        {
            if (m_Status == eRunning)
            {
                return !pthread_yield();
            }
            return false;
        }

        bool CThreadBase::Sleep(const real TimeIntervalInSeconds)
        {
            if (m_Status == eRunning)
            {
                if (TimeIntervalInSeconds > _REAL_ZERO_)
                {
                    timespec TimeIntervalSpecification;
                    TimeIntervalSpecification.tv_sec = time_t(RealFloor(TimeIntervalInSeconds));
                    TimeIntervalSpecification.tv_nsec = long((TimeIntervalInSeconds - TimeIntervalSpecification.tv_sec) * 1e6);
                    return !nanosleep(&TimeIntervalSpecification, nullptr);
                }
                return !pthread_yield();
            }
            return false;
        }

        void CThreadBase::Exit(void* pReturnValue)
        {
            if ((m_Status == eRunning) && (pthread_equal(pthread_self(), m_Thread)))
            {
                pthread_exit(pReturnValue);
                m_Status = eStopped;
            }
        }

        bool CThreadBase::Starting()
        {
            if ((m_Status != eRunning) && pthread_equal(pthread_self(), m_Thread))
            {
                m_Status = eRunning;
                return true;
            }
            return false;
        }

        bool CThreadBase::Stopping()
        {
            if (m_Status == eRunning)
            {
                return true;
            }
            return false;
        }

        bool CThreadBase::Finishing()
        {
            if ((m_Status == eRunning) && pthread_equal(pthread_self(), m_Thread))
            {
                m_Status = eStopped;
                return true;
            }
            return false;
        }

        void* CThreadBase::Dispatch(void* pData)
        {
            if (pData)
            {
                CThreadBase* pThreadBase = reinterpret_cast<CThreadBase*>(pData);
                if (pThreadBase)
                {
                    if (pThreadBase->Starting())
                    {
                        void* pReturnValue = pThreadBase->Run();
                        if (pThreadBase->Finishing())
                        {
                            return pReturnValue;
                        }
                    }
                }
            }
            return nullptr;
        }
    }
}

