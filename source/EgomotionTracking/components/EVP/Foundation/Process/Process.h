/*
 * Process.h
 *
 *  Created on: 20.09.2011
 *      Author: gonzalez
 */

#pragma once

#include "../Miscellaneous/IdentifiableInstance.h"
#include "../Time/TimeEntry.h"
#include "../Time/TimeLogger.h"
#include "Mutex.h"

#ifdef _USE_PROCESS_PROFILING_

#define START_PROCESS_EXECUTION_LOG(SubprocessName,TrialId) if(m_IsLogEnabled)StartExecutionLog(SubprocessName,TrialId);
#define STOP_PROCESS_EXECUTION_LOG if(m_IsLogEnabled)StopExecutionLog();
#define START_PROCESS_EXECUTION_TIMER(SubprocessName){ if(m_IsLogEnabled)Time::CTimeLogger::StartTimeEntry(m_InstanceId,SubprocessName);
#define STOP_PROCESS_EXECUTION_TIMER(SubprocessName)} if(m_IsLogEnabled)Time::CTimeLogger::StopTimeEntry(m_InstanceId,SubprocessName,m_IsLogVerbose);

#else

#define START_PROCESS_EXECUTION_LOG(SubProcessId,TrialId)
#define STOP_PROCESS_EXECUTION_LOG
#define START_PROCESS_EXECUTION_TIMER(SubprocessName)
#define STOP_PROCESS_EXECUTION_TIMER(SubprocessName)

#endif

namespace EVP
{
    namespace Process
    {
        class CProcess: public CIdentifiableInstance
        {
            IDENTIFIABLE

        public:

            CProcess(const string& Name, const Identifier IntanceId);
            CProcess(const_string pName, const Identifier IntanceId);
            CProcess(const string& Name);
            CProcess(const_string pName);
            ~CProcess() override;

            virtual  bool Execute(const Identifier TrialId) = 0;

            bool IsEnabled() const;
            const string& GetName() const;
            Threading::CMutex* GetExecutionMutex();
            uint GetTotalExecutions();
            bool IsRunning();

        protected:

            bool BeginSettingsChangeBlockExecution(const bool Wait);
            bool EndSettingsChangeBlockExecution();
            bool StartExecuting(const Identifier TrialId);
            bool FinishExecution();
            bool StartDisplaying();
            bool FinishDisplaying();

            bool m_IsEnabled;
            string m_Name;

        private:

            uint m_TotalExecutions;
            Identifier m_ExecutionTrialId;
            //Threading::CMutex m_ExecutionMutex;

#ifdef _USE_PROCESS_PROFILING_

        public:

            enum ExportExecutionTrialsSortingMode
            {
                eByTrial, eBySubProcess, eByMaximaNaturalTime, ByMaximaTicksTime, ByMaximaTicks, ByMinimalNaturalTime, ByMinimalaTicksTime, eByMinimalTicks
            };

            void StartExecutionLog(const_string SubprocessName, const Identifier TrialId);
            void StopExecutionLog();
            void SetLogEnabled(const bool Enabled);
            bool IsLogEnabled() const;
            void SetVerbose(const bool Verbose);
            bool IsVerbose() const;
            void ClearLog();
            uint GetTotalExecutions() const;
            real GetLastExecutionNaturalTime() const;
            real GetLastExecutionTicksTime() const;
            uint GetLastExecutionTicks() const;
            real GetGlobalMeanExecutionTime() const;
            real GetGlobalTotalExecutionTime() const;
            real GetSubprocessMeanExecutionNaturalTime(const Identifier SubprocessId) const;
            real GetSubprocessMeanExecutionTicksTime(const Identifier SubprocessId) const;
            real GetSubprocessMeanExecutionTicks(const Identifier SubprocessId) const;
            virtual  bool ExportProcessExecutions(const_string pPathFileName, const ExportExecutionTrialsSortingMode Mode);

        protected:

            struct LogExecutionTrial
            {
                Identifier m_TrialId;
                Identifier m_SubprocessId;
                Time::CTimeEntry::ElapsedTimeInformation m_ElapsedTimeInformation;
            };

            uint GetRegisterSubprocessId(const string& SearchingSubprocessName);
            static bool SortLogExecutionTrialByTrialId(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs);
            static bool SortLogExecutionTrialBySubprocessId(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs);
            static bool SortLogExecutionTrialByMaximaNaturalTime(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs);
            static bool SortLogExecutionTrialByMaximaTicksTime(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs);
            static bool SortLogExecutionTrialByMaximaTicks(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs);
            static bool SortLogExecutionTrialByMinimalNaturalTime(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs);
            static bool SortLogExecutionTrialByMinimalTicksTime(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs);
            static bool SortLogExecutionTrialByMinimalTicks(const LogExecutionTrial& lhs, const LogExecutionTrial& rhs);

            bool m_IsLogVerbose;
            bool m_IsLogEnabled;
            Identifier m_LogCurrentTrialId;
            Identifier m_LogCurrentSubprocessId;
            string m_LogCurrentSubprocessName;
            real m_LogTotalNaturalExecutionTime;
            Time::CTimeEntry m_LogTimeEntry;
            list<LogExecutionTrial> m_LogExecutionTrials;
            list<string> m_RegisteredSubprocessNames;

#endif

        };
    }
}

