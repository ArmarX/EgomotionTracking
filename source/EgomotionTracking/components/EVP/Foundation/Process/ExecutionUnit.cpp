/*
 * ExecutionUnit.cpp
 *
 *  Created on: 27.02.2012
 *      Author: gonzalez
 */

#include "ExecutionUnit.h"
#include "PoolThread.h"

namespace EVP
{
    namespace Threading
    {
        CExecutionUnit::CExecutionUnit() :
            CMutex(), m_pKeepRunning(nullptr), m_pPoolThread(nullptr)
        {
        }

        CExecutionUnit::~CExecutionUnit()
            = default;

        bool CExecutionUnit::Distpatch(volatile const bool* pKeepRunning)
        {
            if (pKeepRunning)
            {
                m_pKeepRunning = pKeepRunning;
                const bool Result = Execute();
                m_pKeepRunning = nullptr;
                return Result;
            }
            else
            {
                return Execute();
            }
        }

        void CExecutionUnit::SetExecuterPoolThread(CPoolThread* pPoolThread)
        {
            m_pPoolThread = pPoolThread;
        }

        void CExecutionUnit::ClearExecuterPoolThread()
        {
            m_pPoolThread = nullptr;
        }

        CPoolThread* CExecutionUnit::GetExecuterPoolThread()
        {
            return m_pPoolThread;
        }

        bool CExecutionUnit::KeepRunning() const
        {
            return m_pKeepRunning ? *m_pKeepRunning : true;
        }
    }
}
