/*
 * ThreadBase.h
 *
 *  Created on: 17.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "../Miscellaneous/IdentifiableInstance.h"
#include "Mutex.h"

namespace EVP
{
    namespace Threading
    {
        class CThreadBase: public CIdentifiableInstance
        {
            IDENTIFIABLE

        public:

            enum Status
            {
                eFree, eStopped, eRunning
            };

            enum AttachableMode
            {
                eJoinable, eDetached
            };

            enum ContentionScope
            {
                eProcessContention, eSystemContention
            };

            static  uint GetMaximalPriorityAvailable();
            static  uint GetMinimalPriorityAvailable();

            CThreadBase(const Identifier ThreadId);
            ~CThreadBase() override;

            //Notice: In Unix/linux To change priority it is necessary to have super user permission for the RR scheduling http://en.wikipedia.org/wiki/Setuid
            bool SetPriority(const uint Priority);
            uint GetPriority();

            bool IsRunning() const;
            bool IsStopped() const;
            Status GetStatus() const;
            AttachableMode GetAttachableMode() const;
            ContentionScope GetContentionScope() const;
            Identifier GetThreadId() const;

            void IcrementTrialId();
            void SetTrialId(const Identifier TrialId);
            Identifier GetTrialId() const;

            virtual  bool Start(const AttachableMode Mode = eJoinable, const ContentionScope Scope = eSystemContention);
            virtual  bool Stop();

            bool Detach();
            bool Join(void** ppReturnedValue = NULL);

            uint GetCPUAffinityMask();

        protected:

            bool Create(const AttachableMode Mode, const ContentionScope Scope);

            bool Yield();
            bool Sleep(const real TimeIntervalInSeconds);
            void Exit(void* pReturnValue);

            virtual  bool Starting();
            virtual  bool Stopping();
            virtual  bool Finishing();
            virtual void* Run() = 0;

            Status m_Status;
            AttachableMode m_AttachableMode;
            ContentionScope m_ContentionScope;
            Identifier m_ThreadId;
            Identifier m_TrialId;
            pthread_t m_Thread;
            pthread_attr_t m_Attributes;

        private:

            static void* Dispatch(void* pData);
        };
    }
}

