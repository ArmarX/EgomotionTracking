/*
 * RGBAColor.h
 *
 *  Created on: Mar 29, 2012
 *      Author: david
 */

#pragma once

#include "DiscreteRGBAPixel.h"

namespace EVP
{
    class CRGBAColor: public CDiscreteRGBAPixel
    {
    public:

        CRGBAColor();
        CRGBAColor(const byte InitializationValue);
        CRGBAColor(const byte R, const byte G, const byte B, const byte A);
        CRGBAColor(const CRGBAColor& RGBAColor);
        ~CRGBAColor();
    };
}

