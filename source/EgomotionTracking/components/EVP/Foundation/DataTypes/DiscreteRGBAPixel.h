/*
 * CDiscreteRGBAPixel.h
 *
 *  Created on: 07.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "Pixel.h"
#include "ColorSpaces.h"

namespace EVP
{
    class CDiscreteRGBAPixel: public TPixel<byte, 4>
    {
    public:

        static const CDiscreteRGBAPixel s_Minimal;
        static const CDiscreteRGBAPixel s_Zero;
        static const CDiscreteRGBAPixel s_Maximal;

        CDiscreteRGBAPixel();
        CDiscreteRGBAPixel(const byte InitializationValue);
        CDiscreteRGBAPixel(const byte R, const byte G, const byte B, const byte A);
        CDiscreteRGBAPixel(const CDiscreteRGBAPixel& CDiscreteRGBA);
        ~CDiscreteRGBAPixel();

        void SetValue(const TPixel<byte, 3>& Color, const byte A = 255);
        void SetValue(const byte R, const byte G, const byte B, const byte A);

        void SetRedChannel(const byte R);
        void SetGreenChannel(const byte G);
        void SetBlueChannel(const byte B);
        void SetAlphaChannel(const byte A);

        uint GetRedChannel() const;
        uint GetGreenChannel() const;
        uint GetBlueChannel() const;
        uint GetAlphaChannel() const;

        bool IsOpaque() const;
        bool IsPartialTranslucent() const;
        bool IsTranparent() const;

        real GetNormalizedRedChannel() const;
        real GetNormalizedGreenChannel() const;
        real GetNormalizedBlueChannel() const;
        real GetNormalizedAlphaChannel() const;
    };
}

