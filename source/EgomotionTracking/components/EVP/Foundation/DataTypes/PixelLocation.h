/*
 * PixelLocation.h
 *
 *  Created on: 10.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"

namespace EVP
{
    class CPixelLocation
    {
    public:

        static const CPixelLocation s_Minimal;
        static const CPixelLocation s_Zero;
        static const CPixelLocation s_Maximal;

        static const CPixelLocation GetDiscretizedDirection(const real Dx, const real Dy);

        CPixelLocation();
        CPixelLocation(const CPixelLocation& Point);
        CPixelLocation(const coordinate X, const coordinate Y);
        ~CPixelLocation();

        real GetLength() const;
        void Set(const coordinate X, const coordinate Y);
        void SetcoordinatesToMaximalValue(const CPixelLocation& Point);
        void SetTocoordinatesToMinimalValue(const CPixelLocation& Point);
        void SetcoordinatesToMaximalValue(const coordinate X, const coordinate Y);
        void SetTocoordinatesToMinimalValue(const coordinate X, const coordinate Y);
        void operator +=(const CPixelLocation& Point);
        void operator -=(const CPixelLocation& Point);
        coordinate GetManhattanDistance(const CPixelLocation& Point) const;
        real GetEuclideanDistance(const CPixelLocation& Point) const;

        coordinate m_x;
        coordinate m_y;
    };

    const CPixelLocation operator+(const CPixelLocation& A, const CPixelLocation& B);
    const CPixelLocation operator-(const CPixelLocation& A, const CPixelLocation& B);
}

