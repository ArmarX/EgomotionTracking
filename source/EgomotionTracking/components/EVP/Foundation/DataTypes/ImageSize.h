/*
 * ImageSize.h
 *
 *  Created on: 09.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "../Mathematics/2D/Vector2D.h"
#include "PixelLocation.h"

namespace EVP
{
    class CImageSize
    {
        template<typename PixelType> friend class TImage;

    public:

        CImageSize();
        CImageSize(const CImageSize& Size);
        CImageSize(const uint Width, const uint Height);
        ~CImageSize();

        bool Set(const uint Width, const uint Height);

        bool operator==(const CImageSize& ImageSize) const;
        bool operator!=(const CImageSize& ImageSize) const;
        bool Equals(const uint Width, const uint Height) const;

        uint GetWidth() const;
        uint GetHeight() const;
        uint GetArea() const;

        bool IsValid() const;
        bool IsValid(const uint Width, const uint Height) const;

        bool IsInside(const Mathematics::_2D::CVector2D& Point) const;
        bool IsInside(const CPixelLocation& PixelLocation) const;
        bool IsXInside(const coordinate X) const;
        bool IsYInside(const coordinate Y) const;
        bool IsInside(const coordinate X, const coordinate Y) const;
        bool IsRegionInside(const coordinate X0, const coordinate Y0, const coordinate X1, const coordinate Y1) const;

    protected:

        uint m_Width;
        uint m_Height;
    };
}

