/*
 * ContinuousTristimulusPixel.h
 *
 *  Created on: 10.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "Pixel.h"
#include "ColorSpaces.h"

namespace EVP
{
    class CContinuousTristimulusPixel: public TPixel<real, 3>
    {
    public:

        static const CContinuousTristimulusPixel s_Minimal;
        static const CContinuousTristimulusPixel s_Zero;
        static const CContinuousTristimulusPixel s_Maximal;

        CContinuousTristimulusPixel();
        CContinuousTristimulusPixel(const CContinuousTristimulusPixel& ContinuousTristimulusPixel);
        CContinuousTristimulusPixel(const CContinuousTristimulusPixel& Maximal, const CContinuousTristimulusPixel& Minimal);//TODO MAKE THIS STATIC FUNCTION NO A CONSTRUCTOR
        CContinuousTristimulusPixel(const real Alpha, const real Beta, const real Chi);

        void SetValue(const real* pChannelAccumulators, const real* pKernelAccumulators) _EVP_FAST_CALL_;
        void SetValue(const real Alpha, const real Beta, const real Chi);
        void SetShiftedScaledValue(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const CContinuousTristimulusPixel& Scale, const CContinuousTristimulusPixel& Minimal) _EVP_FAST_CALL_;
        void SetWeightedValue(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real Weight) _EVP_FAST_CALL_;
        void SetWeightedInverse(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real Weight) _EVP_FAST_CALL_;
        void AddWeightedValue(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real Weight) _EVP_FAST_CALL_;
        void AddWeightedValue(const CContinuousTristimulusPixel* pContinuousTristimulusPixel, const real Weight) _EVP_FAST_CALL_;
        void AddWeightedChannelValue(const uint ChannelIndex, const real Value, const real Weight) _EVP_FAST_CALL_;
        void AddWeightedValue(const TPixel<byte, 3>& DiscreteTristimulusPixel, const real Weight) _EVP_FAST_CALL_;
        void AddWeightedValue(const TPixel<byte, 3>* pDiscreteTristimulusPixel, const real Weight) _EVP_FAST_CALL_;
        void AddChannelValue(const uint ChannelIndex, const real Offset) _EVP_FAST_CALL_;

        void operator=(const real Intensity) _EVP_FAST_CALL_;
        void operator=(const TPixel<byte, 3>& DiscreteTristimulusPixel) _EVP_FAST_CALL_;
        void operator+=(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) _EVP_FAST_CALL_;
        void IsotropicScale(const real ScaleFactor);
        void IsotropicInverseScale(const real ScaleFactor);
        void NonIsotropicScale(const real ScaleFactorAlpha, const real ScaleFactorBeta, const real ScaleFactorChi);

        const CContinuousTristimulusPixel GetConditionalScaled(const real Scale) const;
        void SetRange(const CContinuousTristimulusPixel& ContinuousTristimulusPixelMaximal, const CContinuousTristimulusPixel& ContinuousTristimulusPixelMinimal) _EVP_FAST_CALL_;
        bool IsValidRange() const;

#ifdef _USE_COLOR_SPACE_RGB_

        real GetRGBManhattanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetRGBSquareEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetRGBFixedWeightedSquareEuclideanDistance(const CContinuousTristimulusPixel* pContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetRGBWeightedSquareEuclideanDistance(const CContinuousTristimulusPixel* pContinuousTristimulusPixel, const real WeightRed, const real WeightGreen, const real WeightBlue) const _EVP_FAST_CALL_;
        real GetRGBEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetRGBChebyshevDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetRGBMinkowskiDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real P, const real IP) const _EVP_FAST_CALL_;
        real GetLumaDesaturatedValue() const;
        real GetMeanDesaturatedValue() const;
        real GetLightnessDesaturatedValue() const;
#endif

#ifdef _USE_COLOR_SPACE_HSL_

        void SetFromContinuousRGBToHSL(const CContinuousTristimulusPixel* pContinuousRGB) _EVP_FAST_CALL_;
        real GetHSLManhattanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetHSLSquareEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetHSLEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetHSLChebyshevDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetHSLMinkowskiDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real P, const real IP) const _EVP_FAST_CALL_;
#endif

#ifdef _USE_COLOR_SPACE_HSI_

        void SetFromContinuousRGBToHSI(const CContinuousTristimulusPixel* pContinuousRGB) _EVP_FAST_CALL_;
        real GetHSIManhattanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetHSISquareEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetHSIEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetHSIChebyshevDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetHSIMinkowskiDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real P, const real IP) const _EVP_FAST_CALL_;

#endif

#ifdef _USE_COLOR_SPACE_HSV_

        void SetFromContinuousRGBToHSV(const CContinuousTristimulusPixel* pContinuousRGB) _EVP_FAST_CALL_;
        real GetHSVManhattanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetHSVSquareEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetHSVEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetHSVChebyshevDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetHSVMinkowskiDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real P, const real IP) const _EVP_FAST_CALL_;

#endif

#ifdef _USE_COLOR_SPACE_XYZ_

        void SetFromContinuousRGBToXYZ(const CContinuousTristimulusPixel* pContinuousRGB) _EVP_FAST_CALL_;
        real GetXYZManhattanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetXYZSquareEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetXYZEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const;
        real GetXYZChebyshevDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetXYZMinkowskiDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real P, const real IP) const _EVP_FAST_CALL_;

#endif

#ifdef _USE_COLOR_SPACE_LAB_

        void SetFromContinuousRGBToLAB_D50(const CContinuousTristimulusPixel* pContinuousRGB) _EVP_FAST_CALL_;
        void SetFromContinuousRGBToLAB_D55(const CContinuousTristimulusPixel* pContinuousRGB) _EVP_FAST_CALL_;
        void SetFromContinuousRGBToLAB_D65(const CContinuousTristimulusPixel* pContinuousRGB) _EVP_FAST_CALL_;
        void SetFromContinuousRGBToLAB_D75(const CContinuousTristimulusPixel* pContinuousRGB) _EVP_FAST_CALL_;
        real GetLABManhattanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetLABSquareEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetLABEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetLABChebyshevDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const _EVP_FAST_CALL_;
        real GetLABMinkowskiDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real P, const real IP) const _EVP_FAST_CALL_;
#endif

    protected:

#ifdef _USE_COLOR_SPACE_XYZ_

        static const real s_RGBToXYZTransformationMatrix[3][3];

#endif

#ifdef _USE_COLOR_SPACE_LAB_

        static const real s_RGBToLAB_D50_TransformationMatrix[/*3][3*/9];
        static const real s_RGBToLAB_D55_TransformationMatrix[/*3][3*/9];
        static const real s_RGBToLAB_D65_TransformationMatrix[/*3][3*/9];
        static const real s_RGBToLAB_D75_TransformationMatrix[/*3][3*/9];

#endif

    };

    CContinuousTristimulusPixel operator+(const CContinuousTristimulusPixel& A, const CContinuousTristimulusPixel& B);
    CContinuousTristimulusPixel operator-(const CContinuousTristimulusPixel& A, const CContinuousTristimulusPixel& B);
    CContinuousTristimulusPixel operator*(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real Scale);
    CContinuousTristimulusPixel operator/(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real Scale);
}

