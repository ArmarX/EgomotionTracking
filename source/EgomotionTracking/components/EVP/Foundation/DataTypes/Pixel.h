/*
 * Pixel.h
 *
 *  Created on: 09.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"

namespace EVP
{
    template<typename ChannelDataType, const uint TotalChannels> class TPixel
    {
    public:

        TPixel()
        {
        }

        TPixel(const ChannelDataType& Value)
        {
            for (uint i = 0; i < TotalChannels; ++i)
            {
                m_Channel[i] = Value;
            }
        }

        TPixel(const TPixel<ChannelDataType, TotalChannels>& Pixel)
        {
            memcpy(m_Channel, Pixel.m_Channel, TotalChannels * sizeof(ChannelDataType));
        }

        inline void SetValue(const TPixel& Pixel) _EVP_FAST_CALL_
        {
            memcpy(m_Channel, Pixel.m_Channel, TotalChannels * sizeof(ChannelDataType));
        }

        inline uint GetTotalChannels() const
        {
            return TotalChannels;
        }

        inline uint GetStorageSize() const
        {
            return sizeof(ChannelDataType) * TotalChannels;
        }

        inline const ChannelDataType GetReadOnlyChannelValueByIndex(const uint ChannelIndex) const _EVP_FAST_CALL_
        {
            return m_Channel[ChannelIndex];
        }

        inline const ChannelDataType& GetReadOnlyChannelReferenceByIndex(const uint ChannelIndex) const _EVP_FAST_CALL_
        {
            return m_Channel[ChannelIndex];
        }

        inline const ChannelDataType* GetReadOnlyChannelAddressByIndex(const uint ChannelIndex) const _EVP_FAST_CALL_
        {
            return m_Channel + ChannelIndex;
        }

        inline ChannelDataType& GetWritableChannelReferenceByIndex(const uint ChannelIndex) _EVP_FAST_CALL_
        {
            return m_Channel[ChannelIndex];
        }

        inline ChannelDataType* GetWritableChannelAddressByIndex(const uint ChannelIndex) _EVP_FAST_CALL_
        {
            return m_Channel + ChannelIndex;
        }

        inline const ChannelDataType GetMaximalChannelValue() const
        {
            ChannelDataType Maximal = m_Channel[0];
            for (uint i = 1; i < TotalChannels; ++i)
                if (m_Channel[i] > Maximal)
                {
                    Maximal = m_Channel[i];
                }
            return Maximal;
        }

        inline const ChannelDataType GetMinimalChannelValue() const
        {
            ChannelDataType Minimal = m_Channel[0];
            for (uint i = 1; i < TotalChannels; ++i)
                if (m_Channel[i] < Minimal)
                {
                    Minimal = m_Channel[i];
                }
            return Minimal;
        }

        inline void SetChannelValue(const uint ChannelIndex, const ChannelDataType Value) _EVP_FAST_CALL_
        {
            m_Channel[ChannelIndex] = Value;
        }

        inline void SetChannelToMaximalValue(const uint ChannelIndex, const ChannelDataType Value) _EVP_FAST_CALL_
        {
            if (Value > m_Channel[ChannelIndex])
            {
                m_Channel[ChannelIndex] = Value;
            }
        }

        inline void SetChannelToMinimalValue(const uint ChannelIndex, const ChannelDataType Value) _EVP_FAST_CALL_
        {
            if (Value < m_Channel[ChannelIndex])
            {
                m_Channel[ChannelIndex] = Value;
            }
        }

        inline void SetChannelsToMaximalValue(const TPixel* pPixel) _EVP_FAST_CALL_
        {
            for (uint i = 0; i < TotalChannels; i++)
                if (pPixel->m_Channel[i] > m_Channel[i])
                {
                    m_Channel[i] = pPixel->m_Channel[i];
                }
        }

        inline void SetChannelsToMinimalValue(const TPixel* pPixel) _EVP_FAST_CALL_
        {
            for (uint i = 0; i < TotalChannels; i++)
                if (pPixel->m_Channel[i] < m_Channel[i])
                {
                    m_Channel[i] = pPixel->m_Channel[i];
                }
        }

        inline void SetChannelsToMaximalValue(const TPixel& Pixel) _EVP_FAST_CALL_
        {
            for (uint i = 0; i < TotalChannels; i++)
                if (Pixel.m_Channel[i] > m_Channel[i])
                {
                    m_Channel[i] = Pixel.m_Channel[i];
                }
        }

        inline void SetChannelsToMinimalValue(const TPixel& Pixel) _EVP_FAST_CALL_
        {
            for (uint i = 0; i < TotalChannels; i++)
                if (Pixel.m_Channel[i] < m_Channel[i])
                {
                    m_Channel[i] = Pixel.m_Channel[i];
                }
        }

        static bool IsSafeAligned()
        {
            return sizeof(TPixel<ChannelDataType, TotalChannels>) == (sizeof(ChannelDataType) * TotalChannels);
        }

        ChannelDataType m_Channel[TotalChannels];
    };
}

