/*
 * ImageSize.cpp
 *
 *  Created on: 09.10.2011
 *      Author: gonzalez
 */

#include "ImageSize.h"

namespace EVP
{
    CImageSize::CImageSize() :
        m_Width(0), m_Height(0)
    {
    }

    CImageSize::CImageSize(const CImageSize& Size)
        = default;

    CImageSize::CImageSize(const uint Width, const uint Height) :
        m_Width(Width), m_Height(Height)
    {
    }

    CImageSize::~CImageSize()
        = default;

    bool CImageSize::Set(const uint Width, const uint Height)
    {
        m_Width = Width;
        m_Height = Height;
        return (m_Width > 0) && (m_Height > 0);
    }

    bool CImageSize::operator==(const CImageSize& ImageSize) const
    {
        return (m_Width == ImageSize.m_Width) && (m_Height == ImageSize.m_Height);
    }

    bool CImageSize::operator!=(const CImageSize& ImageSize) const
    {
        return (m_Width != ImageSize.m_Width) || (m_Height != ImageSize.m_Height);
    }

    bool CImageSize::Equals(const uint Width, const uint Height) const
    {
        return (m_Width == Width) && (m_Width == Height);
    }

    uint CImageSize::GetWidth() const
    {
        return m_Width;
    }

    uint CImageSize::GetHeight() const
    {
        return m_Height;
    }

    uint CImageSize::GetArea() const
    {
        return uint(m_Width * m_Height);
    }

    bool CImageSize::IsValid() const
    {
        return (m_Width > 0) && (m_Height > 0);
    }

    bool CImageSize::IsValid(const uint Width, const uint Height) const
    {
        return (Width > 0) && (Height > 0);
    }

    bool CImageSize::IsInside(const Mathematics::_2D::CVector2D& Point) const
    {
        return (Point.GetX() >= _REAL_ZERO_) && (Point.GetY() >= _REAL_ZERO_) && (RealCeil(Point.GetX()) < m_Width) && (RealCeil(Point.GetY()) < m_Height);
    }

    bool CImageSize::IsInside(const CPixelLocation& PixelLocation) const
    {
        return (PixelLocation.m_x >= 0) && (PixelLocation.m_y >= 0) && (PixelLocation.m_x < coordinate(m_Width)) && (PixelLocation.m_y < coordinate(m_Height));
    }

    bool CImageSize::IsXInside(const coordinate X) const
    {
        return (X >= 0) && (X < coordinate(m_Width));
    }

    bool CImageSize::IsYInside(const coordinate Y) const
    {
        return (Y >= 0) && (Y < coordinate(m_Height));
    }

    bool CImageSize::IsInside(const coordinate X, const coordinate Y) const
    {
        return (X >= 0) && (Y >= 0) && (X < coordinate(m_Width)) && (Y < coordinate(m_Height));
    }

    bool CImageSize::IsRegionInside(const coordinate X0, const coordinate Y0, const coordinate X1, const coordinate Y1) const
    {
        return (X0 >= 0) && (Y0 >= 0) && (X1 >= 0) && (Y1 >= 0) && (X0 < coordinate(m_Width)) && (Y0 < coordinate(m_Height)) && (X1 < coordinate(m_Width)) && (Y1 < coordinate(m_Height));
    }
}
