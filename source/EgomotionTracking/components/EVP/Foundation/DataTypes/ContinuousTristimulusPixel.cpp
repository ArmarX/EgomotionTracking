/*
 * ContinuousTristimulusPixel.cpp
 *
 *  Created on: 10.10.2011
 *      Author: gonzalez
 */

#include "ContinuousTristimulusPixel.h"

namespace EVP
{
    const CContinuousTristimulusPixel CContinuousTristimulusPixel::s_Minimal(_REAL_MIN_, _REAL_MIN_, _REAL_MIN_);
    const CContinuousTristimulusPixel CContinuousTristimulusPixel::s_Zero(_REAL_ZERO_, _REAL_ZERO_, _REAL_ZERO_);
    const CContinuousTristimulusPixel CContinuousTristimulusPixel::s_Maximal(_REAL_MAX_, _REAL_MAX_, _REAL_MAX_);

#ifdef _USE_COLOR_SPACE_XYZ_

    const real CContinuousTristimulusPixel::s_RGBToXYZTransformationMatrix[3][3] = { { 0.4124, 0.3576, 0.1805 }, { 0.2126, 0.7152, 0.0722 }, { 0.0193, 0.1192, 0.9505 } };

#endif

#ifdef _USE_COLOR_SPACE_LAB_

    const real CContinuousTristimulusPixel::s_RGBToLAB_D50_TransformationMatrix[/*3][3*/9] = { /*{*/0.0042770676212, 0.0037087278888, 0.0018719949215 /*}*/, /*{*/0.002126, 0.007152, 0.000722 /*}*/, /*{*/0.0002338861043, 0.0014445193592, 0.0115185876755 /*}*/};
    const real CContinuousTristimulusPixel::s_RGBToLAB_D55_TransformationMatrix[/*3][3*/9] = { /*{*/0.010451538, 0.0037374699888, 0.001886502609 /*}*/, /*{*/0.002126, 0.007152, 0.000722 /*}*/, /*{*/0.0002094454528, 0.0012935698432, 0.010314917248 /*}*/};
    const real CContinuousTristimulusPixel::s_RGBToLAB_D65_TransformationMatrix[/*3][3*/9] = { /*{*/0.0043390929936, 0.0037625112864, 0.001899142302 /*}*/, /*{*/0.002126, 0.007152, 0.000722 /*}*/, /*{*/0.000177243094, 0.001094682736, 0.00872899279 /*}*/};
    const real CContinuousTristimulusPixel::s_RGBToLAB_D75_TransformationMatrix[/*3][3*/9] = { /*{*/0.0042770676212, 0.0037653123672, 0.0019005561585 /*}*/, /*{*/0.002126, 0.007152, 0.000722 /*}*/, /*{*/0.0001573719298, 0.0009719551312, 0.007750363693 /*}*/};

#endif

    CContinuousTristimulusPixel::CContinuousTristimulusPixel() :
        TPixel<real, 3>()
    {
        memset(m_Channel, 0, sizeof(real) * 3);
    }

    CContinuousTristimulusPixel::CContinuousTristimulusPixel(const CContinuousTristimulusPixel& ContinuousTristimulusPixel)

        = default;

    CContinuousTristimulusPixel::CContinuousTristimulusPixel(const CContinuousTristimulusPixel& Maximal, const CContinuousTristimulusPixel& Minimal) :
        TPixel<real, 3>()
    {
        m_Channel[0] = Maximal.m_Channel[0] - Minimal.m_Channel[0];
        m_Channel[1] = Maximal.m_Channel[1] - Minimal.m_Channel[1];
        m_Channel[2] = Maximal.m_Channel[2] - Minimal.m_Channel[2];
    }

    CContinuousTristimulusPixel::CContinuousTristimulusPixel(const real Alpha, const real Beta, const real Chi) :
        TPixel<real, 3>()
    {
        m_Channel[0] = Alpha;
        m_Channel[1] = Beta;
        m_Channel[2] = Chi;
    }

    void CContinuousTristimulusPixel::SetValue(const real* pChannelAccumulators, const real* pKernelAccumulators)
    {
        m_Channel[0] = pChannelAccumulators[0] / pKernelAccumulators[0];
        m_Channel[1] = pChannelAccumulators[1] / pKernelAccumulators[1];
        m_Channel[2] = pChannelAccumulators[2] / pKernelAccumulators[2];
    }

    void CContinuousTristimulusPixel::SetValue(const real Alpha, const real Beta, const real Chi)
    {
        m_Channel[0] = Alpha;
        m_Channel[1] = Beta;
        m_Channel[2] = Chi;
    }

    void CContinuousTristimulusPixel::SetShiftedScaledValue(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const CContinuousTristimulusPixel& Scale, const CContinuousTristimulusPixel& Minimal)
    {
        m_Channel[0] = (ContinuousTristimulusPixel.m_Channel[0] - Minimal.m_Channel[0]) * Scale.m_Channel[0];
        m_Channel[1] = (ContinuousTristimulusPixel.m_Channel[1] - Minimal.m_Channel[1]) * Scale.m_Channel[1];
        m_Channel[2] = (ContinuousTristimulusPixel.m_Channel[2] - Minimal.m_Channel[2]) * Scale.m_Channel[2];
    }

    void CContinuousTristimulusPixel::SetWeightedValue(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real Weight)
    {
        m_Channel[0] = ContinuousTristimulusPixel.m_Channel[0] * Weight;
        m_Channel[1] = ContinuousTristimulusPixel.m_Channel[1] * Weight;
        m_Channel[2] = ContinuousTristimulusPixel.m_Channel[2] * Weight;
    }

    void CContinuousTristimulusPixel::SetWeightedInverse(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real Weight)
    {
        m_Channel[0] = ContinuousTristimulusPixel.m_Channel[0] / Weight;
        m_Channel[1] = ContinuousTristimulusPixel.m_Channel[1] / Weight;
        m_Channel[2] = ContinuousTristimulusPixel.m_Channel[2] / Weight;
    }

    void CContinuousTristimulusPixel::AddWeightedValue(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real Weight)
    {
        m_Channel[0] += ContinuousTristimulusPixel.m_Channel[0] * Weight;
        m_Channel[1] += ContinuousTristimulusPixel.m_Channel[1] * Weight;
        m_Channel[2] += ContinuousTristimulusPixel.m_Channel[2] * Weight;
    }

    void CContinuousTristimulusPixel::AddWeightedValue(const CContinuousTristimulusPixel* pContinuousTristimulusPixel, const real Weight)
    {
        m_Channel[0] += pContinuousTristimulusPixel->m_Channel[0] * Weight;
        m_Channel[1] += pContinuousTristimulusPixel->m_Channel[1] * Weight;
        m_Channel[2] += pContinuousTristimulusPixel->m_Channel[2] * Weight;
    }

    void CContinuousTristimulusPixel::AddWeightedChannelValue(const uint ChannelIndex, const real Value, const real Weight)
    {
        m_Channel[ChannelIndex] += Value * Weight;
    }

    void CContinuousTristimulusPixel::AddWeightedValue(const TPixel<byte, 3>& DiscreteTristimulusPixel, const real Weight)
    {
        m_Channel[0] += real(DiscreteTristimulusPixel.m_Channel[0]) * Weight;
        m_Channel[1] += real(DiscreteTristimulusPixel.m_Channel[1]) * Weight;
        m_Channel[2] += real(DiscreteTristimulusPixel.m_Channel[2]) * Weight;
    }

    void CContinuousTristimulusPixel::AddWeightedValue(const TPixel<byte, 3>* pDiscreteTristimulusPixel, const real Weight)
    {
        m_Channel[0] += real(pDiscreteTristimulusPixel->m_Channel[0]) * Weight;
        m_Channel[1] += real(pDiscreteTristimulusPixel->m_Channel[1]) * Weight;
        m_Channel[2] += real(pDiscreteTristimulusPixel->m_Channel[2]) * Weight;
    }

    void CContinuousTristimulusPixel::AddChannelValue(const uint ChannelIndex, const real Offset)
    {
        m_Channel[ChannelIndex] += Offset;
    }

    void CContinuousTristimulusPixel::operator=(const real Intensity)
    {
        m_Channel[0] = Intensity;
        m_Channel[1] = Intensity;
        m_Channel[2] = Intensity;
    }

    void CContinuousTristimulusPixel::operator=(const TPixel<byte, 3>& DiscreteTristimulusPixel)
    {
        m_Channel[0] += DiscreteTristimulusPixel.m_Channel[0];
        m_Channel[1] += DiscreteTristimulusPixel.m_Channel[1];
        m_Channel[2] += DiscreteTristimulusPixel.m_Channel[2];
    }

    void CContinuousTristimulusPixel::operator+=(const CContinuousTristimulusPixel& ContinuousTristimulusPixel)
    {
        m_Channel[0] += ContinuousTristimulusPixel.m_Channel[0];
        m_Channel[1] += ContinuousTristimulusPixel.m_Channel[1];
        m_Channel[2] += ContinuousTristimulusPixel.m_Channel[2];
    }

    void CContinuousTristimulusPixel::IsotropicScale(const real ScaleFactor)
    {
        m_Channel[0] *= ScaleFactor;
        m_Channel[1] *= ScaleFactor;
        m_Channel[2] *= ScaleFactor;
    }

    void CContinuousTristimulusPixel::IsotropicInverseScale(const real ScaleFactor)
    {
        m_Channel[0] /= ScaleFactor;
        m_Channel[1] /= ScaleFactor;
        m_Channel[2] /= ScaleFactor;
    }

    void CContinuousTristimulusPixel::NonIsotropicScale(const real ScaleFactorAlpha, const real ScaleFactorBeta, const real ScaleFactorChi)
    {
        m_Channel[0] *= ScaleFactorAlpha;
        m_Channel[1] *= ScaleFactorBeta;
        m_Channel[2] *= ScaleFactorChi;
    }

    const CContinuousTristimulusPixel CContinuousTristimulusPixel::GetConditionalScaled(const real Scale) const
    {
        return CContinuousTristimulusPixel(RealAbs(m_Channel[0]) > _REAL_EPSILON_ ? Scale / m_Channel[0] : _REAL_ZERO_, RealAbs(m_Channel[1]) > _REAL_EPSILON_ ? Scale / m_Channel[1] : _REAL_ZERO_, RealAbs(m_Channel[2]) > _REAL_EPSILON_ ? Scale / m_Channel[2] : _REAL_ZERO_);
    }

    void CContinuousTristimulusPixel::SetRange(const CContinuousTristimulusPixel& ContinuousTristimulusPixelMaximal, const CContinuousTristimulusPixel& ContinuousTristimulusPixelMinimal)
    {
        m_Channel[0] = ContinuousTristimulusPixelMaximal.m_Channel[0] - ContinuousTristimulusPixelMinimal.m_Channel[0];
        m_Channel[1] = ContinuousTristimulusPixelMaximal.m_Channel[1] - ContinuousTristimulusPixelMinimal.m_Channel[1];
        m_Channel[2] = ContinuousTristimulusPixelMaximal.m_Channel[2] - ContinuousTristimulusPixelMinimal.m_Channel[2];
    }

    bool CContinuousTristimulusPixel::IsValidRange() const
    {
        return !((m_Channel[0] < _REAL_EPSILON_) || (m_Channel[1] < _REAL_EPSILON_) || (m_Channel[2] < _REAL_EPSILON_));
    }

#ifdef _USE_COLOR_SPACE_RGB_

    real CContinuousTristimulusPixel::GetRGBManhattanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        return RealAbs(m_Channel[ChromaticSpaceRGB::eRed] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eRed]) + RealAbs(m_Channel[ChromaticSpaceRGB::eGreen] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eGreen]) + RealAbs(m_Channel[ChromaticSpaceRGB::eBlue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eBlue]);
    }

    real CContinuousTristimulusPixel::GetRGBSquareEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        const real DR = m_Channel[ChromaticSpaceRGB::eRed] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eRed];
        const real DG = m_Channel[ChromaticSpaceRGB::eGreen] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eGreen];
        const real DB = m_Channel[ChromaticSpaceRGB::eBlue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eBlue];
        return (DR * DR + DG * DG + DB * DB);
    }

    real CContinuousTristimulusPixel::GetRGBFixedWeightedSquareEuclideanDistance(const CContinuousTristimulusPixel* pContinuousTristimulusPixel) const
    {
        const real DR = (m_Channel[ChromaticSpaceRGB::eRed] - pContinuousTristimulusPixel->m_Channel[ChromaticSpaceRGB::eRed]) * _REAL_HALF_;
        const real DG = (m_Channel[ChromaticSpaceRGB::eGreen] - pContinuousTristimulusPixel->m_Channel[ChromaticSpaceRGB::eGreen]);
        const real DB = (m_Channel[ChromaticSpaceRGB::eBlue] - pContinuousTristimulusPixel->m_Channel[ChromaticSpaceRGB::eBlue]) * _REAL_HALF_;
        return (DR * DR + DG * DG + DB * DB);
    }

    real CContinuousTristimulusPixel::GetRGBWeightedSquareEuclideanDistance(const CContinuousTristimulusPixel* pContinuousTristimulusPixel, const real WeightRed, const real WeightGreen, const real WeightBlue) const
    {
        const real DR = (m_Channel[ChromaticSpaceRGB::eRed] - pContinuousTristimulusPixel->m_Channel[ChromaticSpaceRGB::eRed]) * WeightRed;
        const real DG = (m_Channel[ChromaticSpaceRGB::eGreen] - pContinuousTristimulusPixel->m_Channel[ChromaticSpaceRGB::eGreen]) * WeightGreen;
        const real DB = (m_Channel[ChromaticSpaceRGB::eBlue] - pContinuousTristimulusPixel->m_Channel[ChromaticSpaceRGB::eBlue]) * WeightBlue;
        return (DR * DR + DG * DG + DB * DB);
    }

    real CContinuousTristimulusPixel::GetRGBEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        const real DR = m_Channel[ChromaticSpaceRGB::eRed] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eRed];
        const real DG = m_Channel[ChromaticSpaceRGB::eGreen] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eGreen];
        const real DB = m_Channel[ChromaticSpaceRGB::eBlue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eBlue];
        return RealSqrt(DR * DR + DG * DG + DB * DB);
    }

    real CContinuousTristimulusPixel::GetRGBChebyshevDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        return TMax(TMax(RealAbs(m_Channel[ChromaticSpaceRGB::eRed] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eRed]), RealAbs(m_Channel[ChromaticSpaceRGB::eGreen] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eGreen])), RealAbs(m_Channel[ChromaticSpaceRGB::eBlue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eBlue]));
    }

    real CContinuousTristimulusPixel::GetRGBMinkowskiDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real P, const real IP) const
    {
        return RealPow(RealPow(RealAbs(m_Channel[ChromaticSpaceRGB::eRed] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eRed]), P) + RealPow(RealAbs(m_Channel[ChromaticSpaceRGB::eGreen] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eGreen]), P) + RealPow(RealAbs(m_Channel[ChromaticSpaceRGB::eBlue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceRGB::eBlue]), P), IP);
    }

    real CContinuousTristimulusPixel::GetLumaDesaturatedValue() const
    {
        return m_Channel[ChromaticSpaceRGB::eRed] * real(0.299) + m_Channel[ChromaticSpaceRGB::eGreen] * real(0.587) + m_Channel[ChromaticSpaceRGB::eBlue] * real(0.114);
    }

    real CContinuousTristimulusPixel::GetMeanDesaturatedValue() const
    {
        return (m_Channel[ChromaticSpaceRGB::eRed] + m_Channel[ChromaticSpaceRGB::eGreen] + m_Channel[ChromaticSpaceRGB::eBlue]) * _REAL_THIRD_;
    }

    real CContinuousTristimulusPixel::GetLightnessDesaturatedValue() const
    {
        real Max = m_Channel[0];
        real Min = Max;
        for (uint i = 1; i < 3; ++i)
            if (m_Channel[i] > Max)
            {
                Max = m_Channel[i];
            }
            else if (m_Channel[i] < Min)
            {
                Min = m_Channel[i];
            }
        return (Max + Min) * _REAL_HALF_;
    }

#endif

#ifdef _USE_COLOR_SPACE_HSL_

    void CContinuousTristimulusPixel::SetFromContinuousRGBToHSL(const CContinuousTristimulusPixel* pContinuousRGB)
    {
        const real NR = pContinuousRGB->m_Channel[ChromaticSpaceRGB::eRed] * _REAL_1_255_;
        const real NG = pContinuousRGB->m_Channel[ChromaticSpaceRGB::eGreen] * _REAL_1_255_;
        const real NB = pContinuousRGB->m_Channel[ChromaticSpaceRGB::eBlue] * _REAL_1_255_;
        const real M = TMax(TMax(NR, NG), NB);
        const real m = TMin(TMin(NR, NG), NB);
        const real C = M - m;
        m_Channel[ChromaticSpaceHSL::eLightness] = (M + m) * _REAL_HALF_;
        if (C > _REAL_EPSILON_)
        {
            if (NR == M)
            {
                m_Channel[ChromaticSpaceHSL::eHue] = (NG - NB) / C;
                m_Channel[ChromaticSpaceHSL::eHue] = m_Channel[ChromaticSpaceHSL::eHue] - RoundToInteger(m_Channel[ChromaticSpaceHSL::eHue] / real(6.0)) * 6;
            }
            else if (NG == M)
            {
                m_Channel[ChromaticSpaceHSL::eHue] = _REAL_TWO_ + (NB - NR) / C;
            }
            else
            {
                m_Channel[ChromaticSpaceHSL::eHue] = real(4.0) + (NR - NG) / C;
            }
            m_Channel[ChromaticSpaceHSL::eHue] *= _REAL_THIRD_PI_;
            m_Channel[ChromaticSpaceHSL::eSaturation] = C / (1 - RealAbs(_REAL_TWO_ * m_Channel[2] - _REAL_ONE_));
        }
        else
        {
            m_Channel[ChromaticSpaceHSL::eHue] = _UNDETERMINED_HUE_;
        }
    }

    real CContinuousTristimulusPixel::GetHSLManhattanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        if ((m_Channel[ChromaticSpaceHSL::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSL::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eLightness]);
        }
        return RealAbs(m_Channel[ChromaticSpaceHSL::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eHue]) + RealAbs(m_Channel[ChromaticSpaceHSL::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eSaturation]) + RealAbs(m_Channel[ChromaticSpaceHSL::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eLightness]);
    }

    real CContinuousTristimulusPixel::GetHSLSquareEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        if ((m_Channel[ChromaticSpaceHSL::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSL::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eLightness]);
        }
        const real DH = m_Channel[ChromaticSpaceHSL::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eHue];
        const real DS = m_Channel[ChromaticSpaceHSL::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eSaturation];
        const real DL = m_Channel[ChromaticSpaceHSL::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eLightness];
        return (DH * DH + DS * DS + DL * DL);
    }

    real CContinuousTristimulusPixel::GetHSLEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        if ((m_Channel[ChromaticSpaceHSL::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSL::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eLightness]);
        }
        const real DH = m_Channel[ChromaticSpaceHSL::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eHue];
        const real DS = m_Channel[ChromaticSpaceHSL::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eSaturation];
        const real DL = m_Channel[ChromaticSpaceHSL::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eLightness];
        return RealSqrt(DH * DH + DS * DS + DL * DL);
    }

    real CContinuousTristimulusPixel::GetHSLChebyshevDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        if ((m_Channel[ChromaticSpaceHSL::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSL::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eLightness]);
        }
        return TMax(TMax(RealAbs(m_Channel[ChromaticSpaceHSL::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eHue]), RealAbs(m_Channel[ChromaticSpaceHSL::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eSaturation])), RealAbs(m_Channel[ChromaticSpaceHSL::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eLightness]));
    }

    real CContinuousTristimulusPixel::GetHSLMinkowskiDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real P, const real IP) const
    {
        if ((m_Channel[ChromaticSpaceHSL::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSL::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eLightness]);
        }
        return RealPow(RealPow(RealAbs(m_Channel[ChromaticSpaceHSL::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eHue]), P) + RealPow(RealAbs(m_Channel[ChromaticSpaceHSL::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eSaturation]), P) + RealPow(RealAbs(m_Channel[ChromaticSpaceHSL::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSL::eLightness]), P), IP);
    }

#endif

#ifdef _USE_COLOR_SPACE_HSI_

    void CContinuousTristimulusPixel::SetFromContinuousRGBToHSI(const CContinuousTristimulusPixel* pContinuousRGB)
    {
        const real NR = pContinuousRGB->m_Channel[ChromaticSpaceRGB::eRed] * _REAL_1_255_;
        const real NG = pContinuousRGB->m_Channel[ChromaticSpaceRGB::eGreen] * _REAL_1_255_;
        const real NB = pContinuousRGB->m_Channel[ChromaticSpaceRGB::eBlue] * _REAL_1_255_;
        const real M = TMax(TMax(NR, NG), NB);
        const real m = TMin(TMin(NR, NG), NB);
        const real C = M - m;
        m_Channel[ChromaticSpaceHSI::eIntensity] = (NR + NG + NB) * _REAL_THIRD_;
        if (C > _REAL_EPSILON_)
        {
            if (NR == M)
            {
                m_Channel[ChromaticSpaceHSI::eHue] = (NG - NB) / C;
                m_Channel[ChromaticSpaceHSI::eHue] = m_Channel[ChromaticSpaceHSI::eHue] - RoundToInteger(m_Channel[ChromaticSpaceHSI::eHue] / real(6.0)) * 6;
            }
            else if (NG == M)
            {
                m_Channel[ChromaticSpaceHSI::eHue] = _REAL_TWO_ + (NB - NR) / C;
            }
            else
            {
                m_Channel[ChromaticSpaceHSI::eHue] = real(4.0) + (NR - NG) / C;
            }
            m_Channel[ChromaticSpaceHSI::eHue] *= _REAL_THIRD_PI_;
            m_Channel[ChromaticSpaceHSI::eSaturation] = C / (1 - RealAbs(_REAL_TWO_ * m_Channel[2] - _REAL_ONE_));
        }
        else
        {
            m_Channel[ChromaticSpaceHSI::eHue] = _UNDETERMINED_HUE_;
        }
    }

    real CContinuousTristimulusPixel::GetHSIManhattanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        if ((m_Channel[ChromaticSpaceHSI::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSI::eIntensity] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eIntensity]);
        }
        return RealAbs(m_Channel[ChromaticSpaceHSI::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eHue]) + RealAbs(m_Channel[ChromaticSpaceHSI::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eSaturation]) + RealAbs(m_Channel[ChromaticSpaceHSI::eIntensity] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eIntensity]);
    }

    real CContinuousTristimulusPixel::GetHSISquareEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        if ((m_Channel[ChromaticSpaceHSI::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSI::eIntensity] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eIntensity]);
        }
        const real DH = m_Channel[ChromaticSpaceHSI::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eHue];
        const real DS = m_Channel[ChromaticSpaceHSI::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eSaturation];
        const real DI = m_Channel[ChromaticSpaceHSI::eIntensity] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eIntensity];
        return (DH * DH + DS * DS + DI * DI);
    }

    real CContinuousTristimulusPixel::GetHSIEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        if ((m_Channel[ChromaticSpaceHSI::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSI::eIntensity] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eIntensity]);
        }
        const real DH = m_Channel[ChromaticSpaceHSI::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eHue];
        const real DS = m_Channel[ChromaticSpaceHSI::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eSaturation];
        const real DI = m_Channel[ChromaticSpaceHSI::eIntensity] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eIntensity];
        return RealSqrt(DH * DH + DS * DS + DI * DI);
    }

    real CContinuousTristimulusPixel::GetHSIChebyshevDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        if ((m_Channel[ChromaticSpaceHSI::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSI::eIntensity] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eIntensity]);
        }
        return TMax(TMax(RealAbs(m_Channel[ChromaticSpaceHSI::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eHue]), RealAbs(m_Channel[ChromaticSpaceHSI::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eSaturation])), RealAbs(m_Channel[ChromaticSpaceHSI::eIntensity] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eIntensity]));
    }

    real CContinuousTristimulusPixel::GetHSIMinkowskiDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real P, const real IP) const
    {
        if ((m_Channel[ChromaticSpaceHSI::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSI::eIntensity] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eIntensity]);
        }
        return RealPow(RealPow(RealAbs(m_Channel[ChromaticSpaceHSI::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eHue]), P) + RealPow(RealAbs(m_Channel[ChromaticSpaceHSI::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eSaturation]), P) + RealPow(RealAbs(m_Channel[ChromaticSpaceHSI::eIntensity] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSI::eIntensity]), P), IP);
    }

#endif

#ifdef _USE_COLOR_SPACE_HSV_

    void CContinuousTristimulusPixel::SetFromContinuousRGBToHSV(const CContinuousTristimulusPixel* pContinuousRGB)
    {
        const real NR = pContinuousRGB->m_Channel[ChromaticSpaceRGB::eRed] * _REAL_1_255_;
        const real NG = pContinuousRGB->m_Channel[ChromaticSpaceRGB::eGreen] * _REAL_1_255_;
        const real NB = pContinuousRGB->m_Channel[ChromaticSpaceRGB::eBlue] * _REAL_1_255_;
        const real M = TMax(TMax(NR, NG), NB);
        const real m = TMin(TMin(NR, NG), NB);
        const real C = M - m;
        m_Channel[ChromaticSpaceHSV::eValue] = M;
        if (C > _REAL_EPSILON_)
        {
            if (NR == M)
            {
                m_Channel[ChromaticSpaceHSV::eHue] = (NG - NB) / C;
                m_Channel[ChromaticSpaceHSV::eHue] = m_Channel[ChromaticSpaceHSV::eHue] - RoundToInteger(m_Channel[ChromaticSpaceHSV::eHue] / real(6.0)) * 6;
            }
            else if (NG == M)
            {
                m_Channel[ChromaticSpaceHSV::eHue] = _REAL_TWO_ + (NB - NR) / C;
            }
            else
            {
                m_Channel[ChromaticSpaceHSV::eHue] = real(4.0) + (NR - NG) / C;
            }
            m_Channel[ChromaticSpaceHSV::eHue] *= _REAL_THIRD_PI_;
            m_Channel[ChromaticSpaceHSV::eSaturation] = C / (1 - RealAbs(_REAL_TWO_ * m_Channel[2] - _REAL_ONE_));
        }
        else
        {
            m_Channel[ChromaticSpaceHSV::eHue] = _UNDETERMINED_HUE_;
        }
    }

    real CContinuousTristimulusPixel::GetHSVManhattanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        if ((m_Channel[ChromaticSpaceHSV::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSV::eValue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eValue]);
        }
        return RealAbs(m_Channel[ChromaticSpaceHSV::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eHue]) + RealAbs(m_Channel[ChromaticSpaceHSV::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eSaturation]) + RealAbs(m_Channel[ChromaticSpaceHSV::eValue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eValue]);
    }

    real CContinuousTristimulusPixel::GetHSVSquareEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        if ((m_Channel[ChromaticSpaceHSV::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSV::eValue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eValue]);
        }
        const real DH = m_Channel[ChromaticSpaceHSV::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eHue];
        const real DS = m_Channel[ChromaticSpaceHSV::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eSaturation];
        const real DV = m_Channel[ChromaticSpaceHSV::eValue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eValue];
        return (DH * DH + DS * DS + DV * DV);
    }

    real CContinuousTristimulusPixel::GetHSVEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        if ((m_Channel[ChromaticSpaceHSV::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSV::eValue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eValue]);
        }
        const real DH = m_Channel[ChromaticSpaceHSV::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eHue];
        const real DS = m_Channel[ChromaticSpaceHSV::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eSaturation];
        const real DV = m_Channel[ChromaticSpaceHSV::eValue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eValue];
        return RealSqrt(DH * DH + DS * DS + DV * DV);
    }

    real CContinuousTristimulusPixel::GetHSVChebyshevDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        if ((m_Channel[ChromaticSpaceHSV::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSV::eValue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eValue]);
        }
        return TMax(TMax(RealAbs(m_Channel[ChromaticSpaceHSV::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eHue]), RealAbs(m_Channel[ChromaticSpaceHSV::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eSaturation])), RealAbs(m_Channel[ChromaticSpaceHSV::eValue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eValue]));
    }

    real CContinuousTristimulusPixel::GetHSVMinkowskiDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real P, const real IP) const
    {
        if ((m_Channel[ChromaticSpaceHSV::eHue] == _UNDETERMINED_HUE_) || (ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eHue] == _UNDETERMINED_HUE_))
        {
            return RealAbs(m_Channel[ChromaticSpaceHSV::eValue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eValue]);
        }
        return RealPow(RealPow(RealAbs(m_Channel[ChromaticSpaceHSV::eHue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eHue]), P) + RealPow(RealAbs(m_Channel[ChromaticSpaceHSV::eSaturation] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eSaturation]), P) + RealPow(RealAbs(m_Channel[ChromaticSpaceHSV::eValue] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceHSV::eValue]), P), IP);
    }

#endif

#ifdef _USE_COLOR_SPACE_XYZ_

    void CContinuousTristimulusPixel::SetFromContinuousRGBToXYZ(const CContinuousTristimulusPixel* pContinuousRGB)
    {
        real Intermediate[3];
        for (uint i = 0; i < 3; ++i)
        {
            const real NC = pContinuousRGB->m_Channel[i] * _REAL_1_255_;
            Intermediate[i] = (NC <= real(0.04045) ? NC * real(0.077399381) : RealPow((NC + real(0.055)) * real(0.947867299), real(2.4))) * real(100.0);
        }
        for (uint i = 0; i < 3; ++i)
        {
            real Accumulator = _REAL_ZERO_;
            for (int j = 0; j < 3; ++j)
            {
                Accumulator += s_RGBToXYZTransformationMatrix[i][j] * Intermediate[j];
            }
            m_Channel[i] = Accumulator;
        }
    }

    real CContinuousTristimulusPixel::GetXYZManhattanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        return RealAbs(m_Channel[ChromaticSpaceXYZ::eX] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eX]) + RealAbs(m_Channel[ChromaticSpaceXYZ::eY] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eY]) + RealAbs(m_Channel[ChromaticSpaceXYZ::eZ] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eZ]);
    }

    real CContinuousTristimulusPixel::GetXYZSquareEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        const real DX = m_Channel[ChromaticSpaceXYZ::eX] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eX];
        const real DY = m_Channel[ChromaticSpaceXYZ::eY] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eY];
        const real DZ = m_Channel[ChromaticSpaceXYZ::eZ] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eZ];
        return (DX * DX + DY * DY + DZ * DZ);
    }

    real CContinuousTristimulusPixel::GetXYZEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        const real DX = m_Channel[ChromaticSpaceXYZ::eX] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eX];
        const real DY = m_Channel[ChromaticSpaceXYZ::eY] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eY];
        const real DZ = m_Channel[ChromaticSpaceXYZ::eZ] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eZ];
        return RealSqrt(DX * DX + DY * DY + DZ * DZ);
    }

    real CContinuousTristimulusPixel::GetXYZChebyshevDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        return TMax(TMax(RealAbs(m_Channel[ChromaticSpaceXYZ::eX] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eX]), RealAbs(m_Channel[ChromaticSpaceXYZ::eY] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eY])), RealAbs(m_Channel[ChromaticSpaceXYZ::eZ] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eZ]));
    }

    real CContinuousTristimulusPixel::GetXYZMinkowskiDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real P, const real IP) const
    {
        return RealPow(RealPow(RealAbs(m_Channel[ChromaticSpaceXYZ::eX] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eX]), P) + RealPow(RealAbs(m_Channel[ChromaticSpaceXYZ::eY] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eY]), P) + RealPow(RealAbs(m_Channel[ChromaticSpaceXYZ::eZ] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceXYZ::eZ]), P), IP);
    }

#endif

#ifdef _USE_COLOR_SPACE_LAB_

    void CContinuousTristimulusPixel::SetFromContinuousRGBToLAB_D50(const CContinuousTristimulusPixel* pContinuousRGB)
    {
        real Intermediate[3];
        for (uint i = 0; i < 3; ++i)
        {
            const real NC = pContinuousRGB->m_Channel[i] * _REAL_1_255_;
            Intermediate[i] = (NC <= real(0.04045) ? NC * real(0.077399381) : RealPow((NC + real(0.055)) * real(0.947867299), real(2.4))) * real(100.0);
        }
        real XYZ[3];
        const real* pTransformationMatrixLabD50 = s_RGBToLAB_D50_TransformationMatrix;
        for (uint i = 0; i < 3; ++i)
        {
            real Accumulator = _REAL_ZERO_;
            for (uint j = 0; j < 3; ++j)
            {
                Accumulator += *pTransformationMatrixLabD50++ * Intermediate[j];
            }
            XYZ[i] = (Accumulator > real(0.008856)) ? RealPow(Accumulator, _REAL_THIRD_) : Accumulator * real(7.787) + real(0.137931034);
        }
        m_Channel[ChromaticSpaceLAB::eLightness] = XYZ[1] * real(116.0) - real(16.0);
        m_Channel[ChromaticSpaceLAB::e_ChromaticA] = (XYZ[0] - XYZ[1]) * real(500.0);
        m_Channel[ChromaticSpaceLAB::e_ChromaticB] = (XYZ[1] - XYZ[2]) * real(200.0);
    }

    void CContinuousTristimulusPixel::SetFromContinuousRGBToLAB_D55(const CContinuousTristimulusPixel* pContinuousRGB)
    {
        real Intermediate[3];
        for (uint i = 0; i < 3; ++i)
        {
            const real NC = pContinuousRGB->m_Channel[i] * _REAL_1_255_;
            Intermediate[i] = (NC <= real(0.04045) ? NC * real(0.077399381) : RealPow((NC + real(0.055)) * real(0.947867299), real(2.4))) * real(100.0);
        }
        real XYZ[3];
        const real* pRGBToLAB_D55_TransformationMatrix = s_RGBToLAB_D55_TransformationMatrix;
        for (uint i = 0; i < 3; ++i)
        {
            real Accumulator = _REAL_ZERO_;
            for (uint j = 0; j < 3; ++j)
            {
                Accumulator += *pRGBToLAB_D55_TransformationMatrix++ * Intermediate[j];
            }
            XYZ[i] = (Accumulator > real(0.008856)) ? RealPow(Accumulator, _REAL_THIRD_) : Accumulator * real(7.787) + real(0.137931034);
        }
        m_Channel[ChromaticSpaceLAB::eLightness] = XYZ[1] * real(116.0) - real(16.0);
        m_Channel[ChromaticSpaceLAB::e_ChromaticA] = (XYZ[0] - XYZ[1]) * real(500.0);
        m_Channel[ChromaticSpaceLAB::e_ChromaticB] = (XYZ[1] - XYZ[2]) * real(200.0);
    }

    void CContinuousTristimulusPixel::SetFromContinuousRGBToLAB_D65(const CContinuousTristimulusPixel* pContinuousRGB)
    {
        real Intermediate[3];
        for (uint i = 0; i < 3; ++i)
        {
            const real NC = pContinuousRGB->m_Channel[i] * _REAL_1_255_;
            Intermediate[i] = (NC <= real(0.04045) ? NC * real(0.077399381) : RealPow((NC + real(0.055)) * real(0.947867299), real(2.4))) * real(100.0);
        }
        real XYZ[3];
        const real* pRGBToLAB_D65_TransformationMatrix = s_RGBToLAB_D65_TransformationMatrix;
        for (uint i = 0; i < 3; ++i)
        {
            real Accumulator = _REAL_ZERO_;
            for (uint j = 0; j < 3; ++j)
            {
                Accumulator += *pRGBToLAB_D65_TransformationMatrix++ * Intermediate[j];
            }
            XYZ[i] = (Accumulator > real(0.008856)) ? RealPow(Accumulator, _REAL_THIRD_) : Accumulator * real(7.787) + real(0.137931034);
        }
        m_Channel[ChromaticSpaceLAB::eLightness] = XYZ[1] * real(116.0) - real(16.0);
        m_Channel[ChromaticSpaceLAB::e_ChromaticA] = (XYZ[0] - XYZ[1]) * real(500.0);
        m_Channel[ChromaticSpaceLAB::e_ChromaticB] = (XYZ[1] - XYZ[2]) * real(200.0);
    }

    void CContinuousTristimulusPixel::SetFromContinuousRGBToLAB_D75(const CContinuousTristimulusPixel* pContinuousRGB)
    {
        real Intermediate[3];
        for (uint i = 0; i < 3; ++i)
        {
            const real NC = pContinuousRGB->m_Channel[i] * _REAL_1_255_;
            Intermediate[i] = (NC <= real(0.04045) ? NC * real(0.077399381) : RealPow((NC + real(0.055)) * real(0.947867299), real(2.4))) * real(100.0);
        }
        real XYZ[3];
        const real* pRGBToLAB_D75_TransformationMatrix = s_RGBToLAB_D75_TransformationMatrix;
        for (uint i = 0; i < 3; ++i)
        {
            real Accumulator = _REAL_ZERO_;
            for (uint j = 0; j < 3; ++j)
            {
                Accumulator += *pRGBToLAB_D75_TransformationMatrix++ * Intermediate[j];
            }
            XYZ[i] = (Accumulator > real(0.008856)) ? RealPow(Accumulator, _REAL_THIRD_) : Accumulator * real(7.787) + real(0.137931034);
        }
        m_Channel[ChromaticSpaceLAB::eLightness] = XYZ[1] * real(116.0) - real(16.0);
        m_Channel[ChromaticSpaceLAB::e_ChromaticA] = (XYZ[0] - XYZ[1]) * real(500.0);
        m_Channel[ChromaticSpaceLAB::e_ChromaticB] = (XYZ[1] - XYZ[2]) * real(200.0);
    }

    real CContinuousTristimulusPixel::GetLABManhattanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        return RealAbs(m_Channel[ChromaticSpaceLAB::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::eLightness]) + RealAbs(m_Channel[ChromaticSpaceLAB::e_ChromaticA] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::e_ChromaticA]) + RealAbs(m_Channel[ChromaticSpaceLAB::e_ChromaticB] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::e_ChromaticB]);
    }

    real CContinuousTristimulusPixel::GetLABSquareEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        const real DL = m_Channel[ChromaticSpaceLAB::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::eLightness];
        const real DA = m_Channel[ChromaticSpaceLAB::e_ChromaticA] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::e_ChromaticA];
        const real DB = m_Channel[ChromaticSpaceLAB::e_ChromaticB] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::e_ChromaticB];
        return (DL * DL + DA * DA + DB * DB);
    }

    real CContinuousTristimulusPixel::GetLABEuclideanDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        const real DL = m_Channel[ChromaticSpaceLAB::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::eLightness];
        const real DA = m_Channel[ChromaticSpaceLAB::e_ChromaticA] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::e_ChromaticA];
        const real DB = m_Channel[ChromaticSpaceLAB::e_ChromaticB] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::e_ChromaticB];
        return RealSqrt(DL * DL + DA * DA + DB * DB);
    }

    real CContinuousTristimulusPixel::GetLABChebyshevDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel) const
    {
        return TMax(TMax(RealAbs(m_Channel[ChromaticSpaceLAB::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::eLightness]), RealAbs(m_Channel[ChromaticSpaceLAB::e_ChromaticA] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::e_ChromaticA])), RealAbs(m_Channel[ChromaticSpaceLAB::e_ChromaticB] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::e_ChromaticB]));
    }

    real CContinuousTristimulusPixel::GetLABMinkowskiDistance(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real P, const real IP) const
    {
        return RealPow(RealPow(RealAbs(m_Channel[ChromaticSpaceLAB::eLightness] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::eLightness]), P) + RealPow(RealAbs(m_Channel[ChromaticSpaceLAB::e_ChromaticA] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::e_ChromaticA]), P) + RealPow(RealAbs(m_Channel[ChromaticSpaceLAB::e_ChromaticB] - ContinuousTristimulusPixel.m_Channel[ChromaticSpaceLAB::e_ChromaticB]), P), IP);
    }

    CContinuousTristimulusPixel operator+(const CContinuousTristimulusPixel& A, const CContinuousTristimulusPixel& B)
    {
        return CContinuousTristimulusPixel(A.m_Channel[0] + B.m_Channel[0], A.m_Channel[1] + B.m_Channel[1], A.m_Channel[2] + B.m_Channel[2]);
    }

    CContinuousTristimulusPixel operator-(const CContinuousTristimulusPixel& A, const CContinuousTristimulusPixel& B)
    {
        return CContinuousTristimulusPixel(A.m_Channel[0] - B.m_Channel[0], A.m_Channel[1] - B.m_Channel[1], A.m_Channel[2] - B.m_Channel[2]);
    }

    CContinuousTristimulusPixel operator*(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real Scale)
    {
        return CContinuousTristimulusPixel(ContinuousTristimulusPixel.m_Channel[0] * Scale, ContinuousTristimulusPixel.m_Channel[1] * Scale, ContinuousTristimulusPixel.m_Channel[2] * Scale);
    }

    CContinuousTristimulusPixel operator/(const CContinuousTristimulusPixel& ContinuousTristimulusPixel, const real Scale)
    {
        return CContinuousTristimulusPixel(ContinuousTristimulusPixel.m_Channel[0] / Scale, ContinuousTristimulusPixel.m_Channel[1] / Scale, ContinuousTristimulusPixel.m_Channel[2] / Scale);
    }

#endif

}
