/*
 * BayerPattern.h
 *
 *  Created on: 10.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "ColorSpaces.h"

namespace EVP
{
    //Encoding
    // 0 X  M N L P
    // R = 1
    // G = 2
    // B = 4
    // Bit "On" for "Index" of the color channel, example: for e_ _ _ >R<, R = 1 =>  0x_ _ _ 1
    enum BayerPatternType
    {
        eUnknownBayerPattern = 0X0000, eGRBG = 0x2412, eRGGB = 0x4221, eBGGR = 0x1224, eGBRG = 0x2142
    };

    bool LoadBayerPatternMap(const BayerPatternType BayerPattern, ChromaticSpaceRGB::ChannelContent BayerPatternMapYX[2][2]);
    bool LoadBayerPatternFlagMap(const BayerPatternType BayerPattern, const ChromaticSpaceRGB::ChannelContent ChannelContent, bool FlagMapYX[2][2]);
    std::string BayerPatternToString(const BayerPatternType BayerPattern);
}

