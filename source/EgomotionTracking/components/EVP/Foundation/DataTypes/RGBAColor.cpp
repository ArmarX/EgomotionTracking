/*
 * CRGBAColor.cpp
 *
 *  Created on: Mar 29, 2012
 *      Author: david
 */

#include "RGBAColor.h"

namespace EVP
{
    CRGBAColor::CRGBAColor() :
        CDiscreteRGBAPixel()
    {
    }

    CRGBAColor::CRGBAColor(const byte InitializationValue) :
        CDiscreteRGBAPixel(InitializationValue)
    {
    }

    CRGBAColor::CRGBAColor(const byte R, const byte G, const byte B, const byte A) :
        CDiscreteRGBAPixel(R, G, B, A)
    {
    }

    CRGBAColor::CRGBAColor(const CRGBAColor& RGBAColor)

        = default;

    CRGBAColor::~CRGBAColor()
        = default;
}
