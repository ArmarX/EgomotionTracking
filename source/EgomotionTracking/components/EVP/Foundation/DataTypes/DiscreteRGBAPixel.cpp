/*
 * CDiscreteRGBAPixel.cpp
 *
 *  Created on: 07.10.2011
 *      Author: gonzalez
 */

#include "DiscreteRGBAPixel.h"

#ifdef _USE_COLOR_SPACE_RGBA_
namespace EVP
{
    const CDiscreteRGBAPixel CDiscreteRGBAPixel::s_Minimal(0);
    const CDiscreteRGBAPixel CDiscreteRGBAPixel::s_Zero(0);
    const CDiscreteRGBAPixel CDiscreteRGBAPixel::s_Maximal(UCHAR_MAX);

    CDiscreteRGBAPixel::CDiscreteRGBAPixel() :
        TPixel<byte, 4> (byte(0))
    {
    }

    CDiscreteRGBAPixel::CDiscreteRGBAPixel(const byte InitializationValue) :
        TPixel<byte, 4> (InitializationValue)
    {
    }

    CDiscreteRGBAPixel::CDiscreteRGBAPixel(const byte R, const byte G, const byte B, const byte A) :
        TPixel<byte, 4> ()
    {
        m_Channel[ChromaticSpaceRGBA::eRed] = R;
        m_Channel[ChromaticSpaceRGBA::eGreen] = G;
        m_Channel[ChromaticSpaceRGBA::eBlue] = B;
        m_Channel[ChromaticSpaceRGBA::eAlpha] = A;
    }

    CDiscreteRGBAPixel::CDiscreteRGBAPixel(const CDiscreteRGBAPixel& CDiscreteRGBA)

        = default;

    CDiscreteRGBAPixel::~CDiscreteRGBAPixel()
        = default;

    void CDiscreteRGBAPixel::SetValue(const TPixel<byte, 3>& Color, const byte A)
    {
        m_Channel[ChromaticSpaceRGBA::eRed] = Color.m_Channel[ChromaticSpaceRGBA::eRed];
        m_Channel[ChromaticSpaceRGBA::eGreen] = Color.m_Channel[ChromaticSpaceRGBA::eGreen];
        m_Channel[ChromaticSpaceRGBA::eBlue] = Color.m_Channel[ChromaticSpaceRGBA::eBlue];
        m_Channel[ChromaticSpaceRGBA::eAlpha] = A;
    }

    void CDiscreteRGBAPixel::SetValue(const byte R, const byte G, const byte B, const byte A)
    {
        m_Channel[ChromaticSpaceRGBA::eRed] = R;
        m_Channel[ChromaticSpaceRGBA::eGreen] = G;
        m_Channel[ChromaticSpaceRGBA::eBlue] = B;
        m_Channel[ChromaticSpaceRGBA::eAlpha] = A;
    }

    uint CDiscreteRGBAPixel::GetRedChannel() const
    {
        return m_Channel[ChromaticSpaceRGBA::eRed];
    }

    uint CDiscreteRGBAPixel::GetGreenChannel() const
    {
        return m_Channel[ChromaticSpaceRGBA::eGreen];
    }

    uint CDiscreteRGBAPixel::GetBlueChannel() const
    {
        return m_Channel[ChromaticSpaceRGBA::eBlue];
    }

    uint CDiscreteRGBAPixel::GetAlphaChannel() const
    {
        return m_Channel[ChromaticSpaceRGBA::eAlpha];
    }

    void CDiscreteRGBAPixel::SetRedChannel(const byte R)
    {
        m_Channel[ChromaticSpaceRGBA::eRed] = R;
    }

    void CDiscreteRGBAPixel::SetGreenChannel(const byte G)
    {
        m_Channel[ChromaticSpaceRGBA::eGreen] = G;
    }

    void CDiscreteRGBAPixel::SetBlueChannel(const byte B)
    {
        m_Channel[ChromaticSpaceRGBA::eBlue] = B;
    }

    void CDiscreteRGBAPixel::SetAlphaChannel(const byte A)
    {
        m_Channel[ChromaticSpaceRGBA::eAlpha] = A;
    }

    bool CDiscreteRGBAPixel::IsOpaque() const
    {
        return (m_Channel[ChromaticSpaceRGBA::eAlpha] == 255);
    }

    bool CDiscreteRGBAPixel::IsPartialTranslucent() const
    {
        return (m_Channel[ChromaticSpaceRGBA::eAlpha] && (m_Channel[ChromaticSpaceRGBA::eAlpha] < 255));
    }

    bool CDiscreteRGBAPixel::IsTranparent() const
    {
        return (!m_Channel[ChromaticSpaceRGBA::eAlpha]);
    }

    real CDiscreteRGBAPixel::GetNormalizedRedChannel() const
    {
        return real(m_Channel[ChromaticSpaceRGBA::eRed]) * _REAL_1_255_;
    }

    real CDiscreteRGBAPixel::GetNormalizedGreenChannel() const
    {
        return real(m_Channel[ChromaticSpaceRGBA::eGreen]) * _REAL_1_255_;
    }

    real CDiscreteRGBAPixel::GetNormalizedBlueChannel() const
    {
        return real(m_Channel[ChromaticSpaceRGBA::eBlue]) * _REAL_1_255_;
    }

    real CDiscreteRGBAPixel::GetNormalizedAlphaChannel() const
    {
        return real(m_Channel[ChromaticSpaceRGBA::eAlpha]) * _REAL_1_255_;
    }
}
#endif /* _USE_COLOR_SPACE_RGBA_ */

