/*
 * DiscreteTristimulusPixel.h
 *
 *  Created on: 10.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../Mathematics/1D/RandomGenerator.h"
#include "Pixel.h"
#include "ColorSpaces.h"

namespace EVP
{
    class CDiscreteTristimulusPixel: public TPixel<byte, 3>
    {
    public:

        static const CDiscreteTristimulusPixel s_Minimal;
        static const CDiscreteTristimulusPixel s_Zero;
        static const CDiscreteTristimulusPixel s_Maximal;

#ifdef _USE_COLOR_SPACE_RGB_

        static const CDiscreteTristimulusPixel s_RGB_Black;
        static const CDiscreteTristimulusPixel s_RGB_Gray32;
        static const CDiscreteTristimulusPixel s_RGB_Gray64;
        static const CDiscreteTristimulusPixel s_RGB_Gray128;
        static const CDiscreteTristimulusPixel s_RGB_Gray192;
        static const CDiscreteTristimulusPixel s_RGB_White;
        static const CDiscreteTristimulusPixel s_RGB_Red;
        static const CDiscreteTristimulusPixel s_RGB_Green;
        static const CDiscreteTristimulusPixel s_RGB_Blue;
        static const CDiscreteTristimulusPixel s_RGB_Yellow;
        static const CDiscreteTristimulusPixel s_RGB_Magenta;
        static const CDiscreteTristimulusPixel s_RGB_Cyan;

#endif
        static  CDiscreteTristimulusPixel GenerateRandom(const uint MinimalAccumlatedMagnitud, const uint RandomSeed);

        CDiscreteTristimulusPixel();
        CDiscreteTristimulusPixel(const TPixel<real, 3>& ContinuousTristimulusPixel);
        CDiscreteTristimulusPixel(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel);
        CDiscreteTristimulusPixel(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel, const real Weight);
        CDiscreteTristimulusPixel(const byte Alpha, const byte Beta, const byte Chi);
        CDiscreteTristimulusPixel(const TPixel<byte, 4>& RGBA);

        void SetValue(const byte Alpha, const byte Beta, const byte Chi) _EVP_FAST_CALL_;
        void SetShiftedScaledValue(const TPixel<real, 3>& Value, const TPixel<real, 3>& Scale, const TPixel<real, 3>& Minimal);
        void SetWeightedValue(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel, const real Weight) _EVP_FAST_CALL_;
        void AddWeightedValue(const CDiscreteTristimulusPixel* pContinuousTristimulusPixel, const real Weight) _EVP_FAST_CALL_;

        void operator=(const TPixel<real, 3>& ContinuousTristimulusPixel) _EVP_FAST_CALL_;
        void operator=(const byte Value) _EVP_FAST_CALL_;
        void operator=(const real Value) _EVP_FAST_CALL_;

        void IsotropicScale(const real ScaleFactor) _EVP_FAST_CALL_;
        void NonIsotropicScale(const real ScaleFactorAlpha, const real ScaleFactorBeta, const real ScaleFactorChi) _EVP_FAST_CALL_;
        CDiscreteTristimulusPixel GetConditionalScaled(const real Scale) const _EVP_FAST_CALL_;
        void Clamping(const TPixel<real, 3>* pContinuousTristimulusPixel) _EVP_FAST_CALL_;
        const CDiscreteTristimulusPixel GetComplement() const;

#ifdef _USE_COLOR_SPACE_RGB_

#ifdef _USE_COLOR_SPACE_RGBA_

        void ComposeRGBA(const TPixel<byte, 4>& RGBA, const real Alpha);

#endif

        uint GetRGBManhattanDistance(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel) const _EVP_FAST_CALL_;
        uint GetRGBSquareEuclideanDistance(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel) const _EVP_FAST_CALL_;
        real GetRGBEuclideanDistance(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel) const _EVP_FAST_CALL_;
        uint GetRGBChebyshevDistance(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel) const _EVP_FAST_CALL_;
        real GetRGBMinkowskiDistance(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel, const real P, const real IP) const _EVP_FAST_CALL_;
        real GetLumaDesaturatedValue() const;
        real GetAverageDesaturatedValue() const;
        real GetLightnessDesaturatedValue() const;

#endif

    };

    CDiscreteTristimulusPixel operator+(const CDiscreteTristimulusPixel& A, const CDiscreteTristimulusPixel& B);
    CDiscreteTristimulusPixel operator-(const CDiscreteTristimulusPixel& A, const CDiscreteTristimulusPixel& B);
    CDiscreteTristimulusPixel operator*(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel, const real Scale);
    CDiscreteTristimulusPixel operator/(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel, const real Scale);
}

