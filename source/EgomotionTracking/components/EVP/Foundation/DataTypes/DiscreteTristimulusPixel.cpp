/*
 * DiscreteTristimulusPixel.cpp
 *
 *  Created on: 10.10.2011
 *      Author: gonzalez
 */

#include "DiscreteTristimulusPixel.h"

namespace EVP
{
    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_Minimal(0, 0, 0);
    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_Zero(0, 0, 0);
    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_Maximal(UCHAR_MAX, UCHAR_MAX, UCHAR_MAX);

#ifdef _USE_COLOR_SPACE_RGB_

    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_RGB_Black(0, 0, 0);
    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_RGB_Gray32(32, 32, 32);
    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_RGB_Gray64(64, 64, 4);
    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_RGB_Gray128(128, 128, 128);
    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_RGB_Gray192(192, 192, 192);
    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_RGB_White(255, 255, 255);
    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_RGB_Red(255, 0, 0);
    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_RGB_Green(0, 255, 0);
    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_RGB_Blue(0, 0, 255);
    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_RGB_Yellow(255, 255, 0);
    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_RGB_Magenta(255, 0, 255);
    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::s_RGB_Cyan(0, 255, 255);

#endif

    CDiscreteTristimulusPixel CDiscreteTristimulusPixel::GenerateRandom(const uint MinimalAccumlatedMagnitud, const uint RandomSeed)
    {
        Mathematics::_1D::CRandomGenerator::InitializeRandomSeed(RandomSeed);
        CDiscreteTristimulusPixel RandomDiscrete(Mathematics::_1D::CRandomGenerator::GenerateRandombyteValue(UCHAR_MAX, 0, RandomSeed), Mathematics::_1D::CRandomGenerator::GenerateRandombyteValue(UCHAR_MAX, 0, RandomSeed), Mathematics::_1D::CRandomGenerator::GenerateRandombyteValue(UCHAR_MAX, 0, RandomSeed));
        while (uint(RandomDiscrete.GetReadOnlyChannelValueByIndex(0) + RandomDiscrete.GetReadOnlyChannelValueByIndex(1) + RandomDiscrete.GetReadOnlyChannelValueByIndex(2)) < MinimalAccumlatedMagnitud)
        {
            RandomDiscrete.SetValue(Mathematics::_1D::CRandomGenerator::GenerateRandombyteValue(UCHAR_MAX, 0), Mathematics::_1D::CRandomGenerator::GenerateRandombyteValue(UCHAR_MAX, 0), Mathematics::_1D::CRandomGenerator::GenerateRandombyteValue(UCHAR_MAX, 0));
        }
        return RandomDiscrete;
    }

    CDiscreteTristimulusPixel::CDiscreteTristimulusPixel() :
        TPixel<byte, 3>(0)
    {
    }

    CDiscreteTristimulusPixel::CDiscreteTristimulusPixel(const TPixel<real, 3>& ContinuousTristimulusPixel) :
        TPixel<byte, 3>()
    {
        m_Channel[0] = SafeRoundToByte(ContinuousTristimulusPixel.m_Channel[0]);
        m_Channel[1] = SafeRoundToByte(ContinuousTristimulusPixel.m_Channel[1]);
        m_Channel[2] = SafeRoundToByte(ContinuousTristimulusPixel.m_Channel[2]);
    }

    CDiscreteTristimulusPixel::CDiscreteTristimulusPixel(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel)

        = default;

    CDiscreteTristimulusPixel::CDiscreteTristimulusPixel(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel, const real Weight) :
        TPixel<byte, 3>()
    {
        m_Channel[0] = SafeRoundToByte(real(DiscreteTristimulusPixel.m_Channel[0]) * Weight);
        m_Channel[1] = SafeRoundToByte(real(DiscreteTristimulusPixel.m_Channel[1]) * Weight);
        m_Channel[2] = SafeRoundToByte(real(DiscreteTristimulusPixel.m_Channel[2]) * Weight);
    }

    CDiscreteTristimulusPixel::CDiscreteTristimulusPixel(const byte Alpha, const byte Beta, const byte Chi) :
        TPixel<byte, 3>()
    {
        m_Channel[0] = Alpha;
        m_Channel[1] = Beta;
        m_Channel[2] = Chi;
    }

    CDiscreteTristimulusPixel::CDiscreteTristimulusPixel(const TPixel<byte, 4>& RGBA) :
        TPixel<byte, 3>()
    {
        memcpy(m_Channel, RGBA.m_Channel, 3);
    }

    void CDiscreteTristimulusPixel::SetValue(const byte Alpha, const byte Beta, const byte Chi)
    {
        m_Channel[0] = Alpha;
        m_Channel[1] = Beta;
        m_Channel[2] = Chi;
    }

    void CDiscreteTristimulusPixel::SetShiftedScaledValue(const TPixel<real, 3>& Value, const TPixel<real, 3>& Scale, const TPixel<real, 3>& Minimal)
    {
        m_Channel[0] = SafeRoundToByte((Value.m_Channel[0] - Minimal.m_Channel[0]) * Scale.m_Channel[0]);
        m_Channel[1] = SafeRoundToByte((Value.m_Channel[1] - Minimal.m_Channel[1]) * Scale.m_Channel[1]);
        m_Channel[2] = SafeRoundToByte((Value.m_Channel[2] - Minimal.m_Channel[2]) * Scale.m_Channel[2]);
    }

    void CDiscreteTristimulusPixel::SetWeightedValue(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel, const real Weight)
    {
        m_Channel[0] = SafeRoundToByte(real(DiscreteTristimulusPixel.m_Channel[0]) * Weight);
        m_Channel[1] = SafeRoundToByte(real(DiscreteTristimulusPixel.m_Channel[1]) * Weight);
        m_Channel[2] = SafeRoundToByte(real(DiscreteTristimulusPixel.m_Channel[2]) * Weight);
    }

    void CDiscreteTristimulusPixel::AddWeightedValue(const CDiscreteTristimulusPixel* pContinuousTristimulusPixel, const real Weight)
    {
        m_Channel[0] += SafeRoundToByte(real(pContinuousTristimulusPixel->m_Channel[0]) * Weight);
        m_Channel[1] += SafeRoundToByte(real(pContinuousTristimulusPixel->m_Channel[1]) * Weight);
        m_Channel[2] += SafeRoundToByte(real(pContinuousTristimulusPixel->m_Channel[2]) * Weight);
    }

    void CDiscreteTristimulusPixel::operator=(const TPixel<real, 3>& ContinuousTristimulusPixel)
    {
        m_Channel[0] = SafeRoundToByte(ContinuousTristimulusPixel.m_Channel[0]);
        m_Channel[1] = SafeRoundToByte(ContinuousTristimulusPixel.m_Channel[1]);
        m_Channel[2] = SafeRoundToByte(ContinuousTristimulusPixel.m_Channel[2]);
    }

    void CDiscreteTristimulusPixel::operator=(const byte Value)
    {
        memset(m_Channel, Value, sizeof(byte) * 3);
    }

    void CDiscreteTristimulusPixel::operator=(const real Value)
    {
        memset(m_Channel, SafeRoundToByte(Value), sizeof(byte) * 3);
    }

    void CDiscreteTristimulusPixel::IsotropicScale(const real ScaleFactor)
    {
        m_Channel[0] = SafeRoundToByte(real(m_Channel[0]) * ScaleFactor);
        m_Channel[1] = SafeRoundToByte(real(m_Channel[1]) * ScaleFactor);
        m_Channel[2] = SafeRoundToByte(real(m_Channel[2]) * ScaleFactor);
    }

    void CDiscreteTristimulusPixel::NonIsotropicScale(const real ScaleFactorAlpha, const real ScaleFactorBeta, const real ScaleFactorChi)
    {
        m_Channel[0] = SafeRoundToByte(real(m_Channel[0]) * ScaleFactorAlpha);
        m_Channel[1] = SafeRoundToByte(real(m_Channel[1]) * ScaleFactorBeta);
        m_Channel[2] = SafeRoundToByte(real(m_Channel[2]) * ScaleFactorChi);
    }

    CDiscreteTristimulusPixel CDiscreteTristimulusPixel::GetConditionalScaled(const real Scale) const
    {
        return CDiscreteTristimulusPixel(m_Channel[0] ? SafeRoundToByte(Scale / real(m_Channel[0])) : 0, m_Channel[1] ? SafeRoundToByte(Scale / real(m_Channel[1])) : 0, m_Channel[2] ? SafeRoundToByte(Scale / real(m_Channel[2])) : 0);
    }

    void CDiscreteTristimulusPixel::Clamping(const TPixel<real, 3>* pContinuousTristimulusPixel)
    {
        m_Channel[0] = SafeRoundToByte(pContinuousTristimulusPixel->m_Channel[0]);
        m_Channel[1] = SafeRoundToByte(pContinuousTristimulusPixel->m_Channel[1]);
        m_Channel[2] = SafeRoundToByte(pContinuousTristimulusPixel->m_Channel[2]);
    }

    const CDiscreteTristimulusPixel CDiscreteTristimulusPixel::GetComplement() const
    {
        return CDiscreteTristimulusPixel(UCHAR_MAX - m_Channel[0], UCHAR_MAX - m_Channel[1], UCHAR_MAX - m_Channel[2]);
    }

#ifdef _USE_COLOR_SPACE_RGB_

#ifdef _USE_COLOR_SPACE_RGBA_

    void CDiscreteTristimulusPixel::ComposeRGBA(const TPixel<byte, 4>& RGBA, const real Alpha)
    {
        const real Beta = _REAL_ONE_ - Alpha;
        m_Channel[ChromaticSpaceRGB::eRed] = SafeRoundToByte(real(RGBA.m_Channel[ChromaticSpaceRGBA::eRed]) * Alpha + real(m_Channel[ChromaticSpaceRGB::eRed]) * Beta);
        m_Channel[ChromaticSpaceRGB::eGreen] = SafeRoundToByte(real(RGBA.m_Channel[ChromaticSpaceRGBA::eGreen]) * Alpha + real(m_Channel[ChromaticSpaceRGB::eGreen]) * Beta);
        m_Channel[ChromaticSpaceRGB::eBlue] = SafeRoundToByte(real(RGBA.m_Channel[ChromaticSpaceRGBA::eBlue]) * Alpha + real(m_Channel[ChromaticSpaceRGB::eBlue]) * Beta);
    }

#endif

    uint CDiscreteTristimulusPixel::GetRGBManhattanDistance(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel) const
    {
        return abs(m_Channel[ChromaticSpaceRGB::eRed] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eRed]) + abs(m_Channel[ChromaticSpaceRGB::eGreen] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eGreen]) + abs(m_Channel[ChromaticSpaceRGB::eBlue] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eBlue]);
    }

    uint CDiscreteTristimulusPixel::GetRGBSquareEuclideanDistance(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel) const
    {
        const int DR = m_Channel[ChromaticSpaceRGB::eRed] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eRed];
        const int DG = m_Channel[ChromaticSpaceRGB::eGreen] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eGreen];
        const int DB = m_Channel[ChromaticSpaceRGB::eBlue] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eBlue];
        return (DR * DR + DG * DG + DB * DB);
    }

    real CDiscreteTristimulusPixel::GetRGBEuclideanDistance(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel) const
    {
        const int DR = m_Channel[ChromaticSpaceRGB::eRed] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eRed];
        const int DG = m_Channel[ChromaticSpaceRGB::eGreen] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eGreen];
        const int DB = m_Channel[ChromaticSpaceRGB::eBlue] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eBlue];
        return RealSqrt(real(DR * DR + DG * DG + DB * DB));
    }

    uint CDiscreteTristimulusPixel::GetRGBChebyshevDistance(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel) const
    {
        return TMax(TMax(abs(m_Channel[ChromaticSpaceRGB::eRed] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eRed]), abs(m_Channel[ChromaticSpaceRGB::eGreen] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eGreen])), abs(m_Channel[ChromaticSpaceRGB::eBlue] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eBlue]));
    }

    real CDiscreteTristimulusPixel::GetRGBMinkowskiDistance(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel, const real P, const real IP) const
    {
        return RealPow(RealPow(abs(m_Channel[ChromaticSpaceRGB::eRed] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eRed]), P) + RealPow(RealAbs(m_Channel[ChromaticSpaceRGB::eGreen] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eGreen]), P) + RealPow(RealAbs(m_Channel[ChromaticSpaceRGB::eBlue] - DiscreteTristimulusPixel.m_Channel[ChromaticSpaceRGB::eBlue]), P), IP);
    }

    real CDiscreteTristimulusPixel::GetLumaDesaturatedValue() const
    {
        return real(m_Channel[ChromaticSpaceRGB::eRed]) * real(0.299) + real(m_Channel[ChromaticSpaceRGB::eGreen]) * real(0.587) + real(m_Channel[ChromaticSpaceRGB::eBlue]) * real(0.114);
    }

    real CDiscreteTristimulusPixel::GetAverageDesaturatedValue() const
    {
        return real(m_Channel[ChromaticSpaceRGB::eRed] + m_Channel[ChromaticSpaceRGB::eGreen] + m_Channel[ChromaticSpaceRGB::eBlue]) * _REAL_THIRD_;
    }

    real CDiscreteTristimulusPixel::GetLightnessDesaturatedValue() const
    {
        return real(TMax(TMax(m_Channel[ChromaticSpaceRGB::eRed], m_Channel[ChromaticSpaceRGB::eGreen]), m_Channel[ChromaticSpaceRGB::eBlue]) + TMin(TMin(m_Channel[ChromaticSpaceRGB::eRed], m_Channel[ChromaticSpaceRGB::eGreen]), m_Channel[ChromaticSpaceRGB::eBlue])) * _REAL_HALF_;
    }

#endif

    CDiscreteTristimulusPixel operator+(const CDiscreteTristimulusPixel& A, const CDiscreteTristimulusPixel& B)
    {
        return CDiscreteTristimulusPixel(A.m_Channel[0] + B.m_Channel[0], A.m_Channel[1] + B.m_Channel[1], A.m_Channel[2] + B.m_Channel[2]);
    }

    CDiscreteTristimulusPixel operator-(const CDiscreteTristimulusPixel& A, const CDiscreteTristimulusPixel& B)
    {
        return CDiscreteTristimulusPixel(A.m_Channel[0] - B.m_Channel[0], A.m_Channel[1] - B.m_Channel[1], A.m_Channel[2] - B.m_Channel[2]);
    }

    CDiscreteTristimulusPixel operator*(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel, const real Scale)
    {
        return CDiscreteTristimulusPixel(SafeRoundToByte(real(DiscreteTristimulusPixel.m_Channel[0]) * Scale), SafeRoundToByte(real(DiscreteTristimulusPixel.m_Channel[1]) * Scale), SafeRoundToByte(real(DiscreteTristimulusPixel.m_Channel[2]) * Scale));
    }

    CDiscreteTristimulusPixel operator/(const CDiscreteTristimulusPixel& DiscreteTristimulusPixel, const real Scale)
    {
        return CDiscreteTristimulusPixel(SafeRoundToByte(real(DiscreteTristimulusPixel.m_Channel[0]) / Scale), SafeRoundToByte(real(DiscreteTristimulusPixel.m_Channel[1]) / Scale), SafeRoundToByte(real(DiscreteTristimulusPixel.m_Channel[2]) / Scale));
    }
}
