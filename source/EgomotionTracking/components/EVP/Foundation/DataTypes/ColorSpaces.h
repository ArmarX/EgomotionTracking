/*
 * ColorSpaces.h
 *
 *  Created on: 10.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"

namespace EVP
{
    enum ChromaticSpace //TODO VERIFY ALL ENUMS TO HAVE THE Unknown CASE AT THE END
    {

#ifdef _USE_COLOR_SPACE_RGB_
        eRGB = 0
#endif

#ifdef _USE_COLOR_SPACE_RGBA_
        , eRGBA = 1
#endif

#ifdef _USE_COLOR_SPACE_XYZ_
        , eXYZ = 2
#endif

#ifdef _USE_COLOR_SPACE_LAB_
        , eLab = 3
#endif

#ifdef _USE_COLOR_SPACE_HSL_
        , eHSL = 4
#endif

#ifdef _USE_COLOR_SPACE_HSI_
        , eHSI = 5
#endif

#ifdef _USE_COLOR_SPACE_HSV_
        , eHSV = 6
#endif
        , eUnknownColorSpace = 7
    };

#define _UNDETERMINED_HUE_ -1

#ifdef _USE_COLOR_SPACE_RGB_
    ////////////////////////////////////////////////////////////
    namespace ChromaticSpaceRGB
    {
        ////////////////////////////////////////////////////
        //Channel Content Indices
        ////////////////////////////////////////////////////
        enum ChannelContent
        {
            eRed = 0, eGreen = 1, eBlue = 2, eUnknownChannelContent = 3
        };
    }
    ////////////////////////////////////////////////////////////
#endif

#ifdef _USE_COLOR_SPACE_RGBA_
    ////////////////////////////////////////////////////////////
    namespace ChromaticSpaceRGBA
    {
        ////////////////////////////////////////////////////
        //Channel Content Indices
        ////////////////////////////////////////////////////
        enum ChannelContent
        {
            eRed = 0, eGreen = 1, eBlue = 2, eAlpha = 3, eUnknownChannelContent = 4
        };
    }
    ////////////////////////////////////////////////////////////
#endif

#ifdef _USE_COLOR_SPACE_HSL_
    ////////////////////////////////////////////////////////////
    namespace ChromaticSpaceHSL
    {
        ////////////////////////////////////////////////////
        //Channel Content Indices
        ////////////////////////////////////////////////////
        enum ChannelContent
        {
            eHue = 0, eSaturation = 1, eLightness = 2, eUnknownChannelContent = 3
        };
    }
    ////////////////////////////////////////////////////////////
#endif

#ifdef _USE_COLOR_SPACE_HSI_
    ////////////////////////////////////////////////////////////
    namespace ChromaticSpaceHSI
    {
        ////////////////////////////////////////////////////
        //Channel Content Indices
        ////////////////////////////////////////////////////
        enum ChannelContent
        {
            eHue = 0, eSaturation = 1, eIntensity = 2, eUnknownChannelContent = 3
        };
    }
    ////////////////////////////////////////////////////////////
#endif

#ifdef _USE_COLOR_SPACE_HSV_
    ////////////////////////////////////////////////////////////
    namespace ChromaticSpaceHSV
    {
        ////////////////////////////////////////////////////
        //Channel Content Indices
        ////////////////////////////////////////////////////
        enum ChannelContent
        {
            eHue = 0, eSaturation = 1, eValue = 2, eUnknownChannelContent = 3
        };
    }
    ////////////////////////////////////////////////////////////
#endif

#ifdef _USE_COLOR_SPACE_XYZ_
    ////////////////////////////////////////////////////////////
    namespace ChromaticSpaceXYZ
    {
        ////////////////////////////////////////////////////
        //Channel Content Indices
        ////////////////////////////////////////////////////
        enum ChannelContent
        {
            eX = 0, eY = 1, eZ = 2, eUnknownChannelContent = 3
        };
    }
    ////////////////////////////////////////////////////////////
#endif

#ifdef _USE_COLOR_SPACE_LAB_
    ////////////////////////////////////////////////////////////
    namespace ChromaticSpaceLAB
    {
        ////////////////////////////////////////////////////
        //Channel Content Indices
        ////////////////////////////////////////////////////
        enum ChannelContent
        {
            eLightness = 0, e_ChromaticA = 1, e_ChromaticB = 2, eUnknownChannelContent = 3
        };

        enum WhiteSpot
        {
            eD50 = 0, eD55 = 1, eD65 = 2, eD75 = 3, eUnknownWhiteSpot = 4
        };
    }
    ////////////////////////////////////////////////////////////
#endif

}

