/*
 * PixelLocation.cpp
 *
 *  Created on: 10.10.2011
 *      Author: gonzalez
 */

#include "PixelLocation.h"

namespace EVP
{
    const CPixelLocation CPixelLocation::s_Minimal(_COORDINATE_MIN_, _COORDINATE_MIN_);
    const CPixelLocation CPixelLocation::s_Zero(0, 0);
    const CPixelLocation CPixelLocation::s_Maximal(_COORDINATE_MAX_, _COORDINATE_MAX_);

    const CPixelLocation CPixelLocation::GetDiscretizedDirection(const real Dx, const real Dy)
    {
        return CPixelLocation((Dx >= _REAL_QUANTIZATION_THRESHOLD_) ? 1 : ((Dx < -_REAL_QUANTIZATION_THRESHOLD_) ? -1 : 0), (Dy >= _REAL_QUANTIZATION_THRESHOLD_) ? 1 : ((Dy < -_REAL_QUANTIZATION_THRESHOLD_) ? -1 : 0));
    }

    CPixelLocation::CPixelLocation() :
        m_x(0), m_y(0)
    {
    }

    CPixelLocation::CPixelLocation(const CPixelLocation& Point)
        = default;

    CPixelLocation::CPixelLocation(const coordinate X, const coordinate Y) :
        m_x(X), m_y(Y)
    {
    }

    CPixelLocation::~CPixelLocation()
        = default;

    real CPixelLocation::GetLength() const
    {
        return RealHypotenuse(real(m_x), real(m_y));
    }

    void CPixelLocation::Set(const coordinate X, const coordinate Y)
    {
        m_x = X;
        m_y = Y;
    }

    void CPixelLocation::SetcoordinatesToMaximalValue(const CPixelLocation& Point)
    {
        if (Point.m_x > m_x)
        {
            m_x = Point.m_x;
        }
        if (Point.m_y > m_y)
        {
            m_y = Point.m_y;
        }
    }

    void CPixelLocation::SetTocoordinatesToMinimalValue(const CPixelLocation& Point)
    {
        if (Point.m_x < m_x)
        {
            m_x = Point.m_x;
        }
        if (Point.m_y < m_y)
        {
            m_y = Point.m_y;
        }
    }

    void CPixelLocation::SetcoordinatesToMaximalValue(const coordinate X, const coordinate Y)
    {
        if (X > m_x)
        {
            m_x = X;
        }
        if (Y > m_y)
        {
            m_y = Y;
        }
    }

    void CPixelLocation::SetTocoordinatesToMinimalValue(const coordinate X, const coordinate Y)
    {
        if (X < m_x)
        {
            m_x = X;
        }
        if (Y < m_y)
        {
            m_y = Y;
        }
    }

    void CPixelLocation::operator +=(const CPixelLocation& Point)
    {
        m_x += Point.m_x;
        m_y += Point.m_y;
    }

    void CPixelLocation::operator -=(const CPixelLocation& Point)
    {
        m_x -= Point.m_x;
        m_y -= Point.m_y;
    }

    coordinate CPixelLocation::GetManhattanDistance(const CPixelLocation& Point) const
    {
        return abs(m_x - Point.m_x) + abs(m_y - Point.m_y);
    }

    real CPixelLocation::GetEuclideanDistance(const CPixelLocation& Point) const
    {
        return RealHypotenuse(real(m_x - Point.m_x), real(m_y - Point.m_y));
    }

    const CPixelLocation operator+(const CPixelLocation& A, const CPixelLocation& B)
    {
        return CPixelLocation(A.m_x + B.m_x, A.m_y + B.m_y);
    }

    const CPixelLocation operator-(const CPixelLocation& A, const CPixelLocation& B)
    {
        return CPixelLocation(A.m_x - B.m_x, A.m_y - B.m_y);
    }
}
