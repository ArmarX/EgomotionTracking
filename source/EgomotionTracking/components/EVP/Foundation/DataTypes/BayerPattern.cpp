/*
 * BayerPattern.cpp
 *
 *  Created on: 18.10.2011
 *      Author: gonzalez
 */

#include "BayerPattern.h"

namespace EVP
{
    bool LoadBayerPatternMap(const BayerPatternType BayerPattern, ChromaticSpaceRGB::ChannelContent BayerPatternMapYX[2][2])
    {
        if (BayerPattern == eUnknownBayerPattern)
        {
            for (uint Y = 0; Y < 2; ++Y)
                for (uint X = 0; X < 2; ++X)
                {
                    BayerPatternMapYX[Y][X] = ChromaticSpaceRGB::eUnknownChannelContent;
                }
            return false;
        }
        else
        {
            for (uint Y = 0, i = 0; Y < 2; ++Y)
                for (uint X = 0; X < 2; ++X, i += 4)
                {
                    BayerPatternMapYX[Y][X] = ChromaticSpaceRGB::ChannelContent((BayerPattern & (0xF << i)) >> (i + 1));
                }
            return true;
        }
    }

    bool LoadBayerPatternFlagMap(const BayerPatternType BayerPattern, const ChromaticSpaceRGB::ChannelContent ChannelContent, bool FlagMapYX[2][2])
    {
        if (BayerPattern == eUnknownBayerPattern)
        {
            for (uint Y = 0; Y < 2; ++Y)
                for (uint X = 0; X < 2; ++X)
                {
                    FlagMapYX[Y][X] = false;
                }
            return false;
        }
        else
        {
            for (uint Y = 0, i = 0; Y < 2; ++Y)
                for (uint X = 0; X < 2; ++X, i += 4)
                {
                    FlagMapYX[Y][X] = ChromaticSpaceRGB::ChannelContent((BayerPattern & (0xF << i)) >> (i + 1)) == ChannelContent;
                }
            return true;
        }
    }

    std::string BayerPatternToString(const BayerPatternType BayerPattern)
    {
        switch (BayerPattern)
        {
            case eUnknownBayerPattern:
                return std::string("Unknown Bayer Pattern");
            case eGRBG:
                return std::string("GRBG");
            case eRGGB:
                return std::string("RGGB");
            case eBGGR:
                return std::string("BGGR");
            case eGBRG:
                return std::string("GBRG");
        }
        return std::string("ERROR");
    }
}
