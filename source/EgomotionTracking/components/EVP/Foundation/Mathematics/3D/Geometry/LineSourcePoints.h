/*
 * LineSourcePoints.h
 *
 *  Created on: Apr 9, 2012
 *      Author: david
 */

#pragma once

#include "../Vector3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                class CLineSourcePoints
                {
                public:

                    CLineSourcePoints(const CVector3D& A, const CVector3D& B);
                    ~CLineSourcePoints();

                    void SetPoints(const CVector3D& A, const CVector3D& B);
                    void GetPoints(CVector3D& A, CVector3D& B) const;

                    bool ArePointsValid() const;
                    CVector3D GetMidPoint() const;
                    CVector3D GetDirection() const;
                    const CVector3D& GetPointA() const;
                    const CVector3D& GetPointB() const;

                protected:

                    CVector3D m_A;
                    CVector3D m_B;
                };
            }
        }
    }
}

