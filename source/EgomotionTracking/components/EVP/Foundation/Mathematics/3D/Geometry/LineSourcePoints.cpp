/*
 * LineSourcePoints.cpp
 *
 *  Created on: Apr 9, 2012
 *      Author: david
 */

#include "LineSourcePoints.h"
#include "Common3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CLineSourcePoints::CLineSourcePoints(const CVector3D& A, const CVector3D& B) :
                    m_A(A), m_B(B)
                {
                }

                CLineSourcePoints::~CLineSourcePoints()
                    = default;

                void CLineSourcePoints::SetPoints(const CVector3D& A, const CVector3D& B)
                {
                    m_A = A;
                    m_B = B;
                }

                void CLineSourcePoints::GetPoints(CVector3D& A, CVector3D& B) const
                {
                    A = m_A;
                    B = m_B;
                }

                bool CLineSourcePoints::ArePointsValid() const
                {
                    return ((m_A != m_B) && m_A.IsNotAtInfinity() && m_B.IsNotAtInfinity());
                }

                CVector3D CLineSourcePoints::GetMidPoint() const
                {
                    return CVector3D::CreateMidPoint(m_A, m_B);
                }

                CVector3D CLineSourcePoints::GetDirection() const
                {
                    return ComputeUnitaryDirectionVector(m_A, m_B);
                }

                const CVector3D& CLineSourcePoints::GetPointA() const
                {
                    return m_A;
                }

                const CVector3D& CLineSourcePoints::GetPointB() const
                {
                    return m_B;
                }
            }
        }
    }
}
