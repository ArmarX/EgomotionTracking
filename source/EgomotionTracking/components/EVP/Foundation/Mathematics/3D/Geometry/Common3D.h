/*
 * Common3D.h
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../Vector3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                bool AreUnitaryVectorsParallel(const CVector3D& A, const CVector3D& B);

                bool AreUnitaryVectorsOrthogonal(const CVector3D& A, const CVector3D& B);

                bool IsOnPlane(const CVector3D& Normal, const real HesseDistance, const CVector3D& X);

                bool IsOnPlane(const CVector3D& Normal, const CVector3D& SupportPoint, const CVector3D& X);

                bool IsOnPositiveSideOfPlane(const CVector3D& Normal, const real HesseDistance, const CVector3D& X);

                bool IsOnSphere(const CVector3D& Center, const real Radius, const CVector3D& X);

                bool IsInsideSphere(const CVector3D& Center, const real Radius, const CVector3D& X);

                CVector3D ComputeUnitaryDirectionVector(const CVector3D& A, const CVector3D& B);

                bool ComputeUnitaryOrthogonalVector(const CVector3D& A, const CVector3D& B, CVector3D& C);

                CVector3D ComputeProjectionPointToLine(const CVector3D& SupportPoint, const CVector3D& Direction, const CVector3D& X);

                CVector3D ComputeProjectionPointToPlane(const CVector3D& Normal, const CVector3D& SupportPoint, const CVector3D& X);

                CVector3D ComputeProjectionPointToPlane(const CVector3D& Normal, const real HesseDistance, const CVector3D& X);

                bool ComputeIntersectionLineAndPlane(const CVector3D& LineSupportPoint, const CVector3D& LineDirection, const CVector3D& PlaneNormal, const CVector3D& PlaneSupportPoint, CVector3D& X);

                bool ComputeIntersectionPlaneAndPlane(const CVector3D& PlaneNormal1, const real HesseDistance1, const CVector3D& PlaneNormal2, const real HesseDistance2, CVector3D& LineDirection, CVector3D& LineSupportPoint);

                bool IsIntersectingConfiguration(const real R1, const real R2, const real D12, bool& SinglePointFlag);

                bool CirclesIntersection(const real R, const real r, const real d, real& x, real& y, bool& SinglePointFlag);

                bool ComputeLineToLineClosestPoints(const CVector3D& LineSupportPoint1, const CVector3D& LineDirection1, const CVector3D& LineSupportPoint2, const CVector3D& LineDirection2, CVector3D& Point1, CVector3D& Point2);

                bool ComputeIntersectionCircleAndLine(const CVector3D& Normal, const CVector3D& Center, const real Radius, const CVector3D& CircleCenterProjectedOntoLine, CVector3D& A, CVector3D& B, bool& SinglePointFlag);
            }
        }
    }
}
