/*
 * Sphere3D.cpp
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "Point3D.h"
#include "PointPair3D.h"
#include "Line3D.h"
#include "Circle3D.h"
#include "Plane3D.h"
#include "Sphere3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CSphere3D::CSphere3D() :
                    CGeometricPrimitive3D(eSphere, nullptr, nullptr), m_Center(CVector3D::s_Point_At_Plus_Infinity_3D), m_Radius(_REAL_ZERO_)
                {
                }

                CSphere3D::CSphere3D(const real Cx, const real Cy, const real Cz, const real Radius, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(eSphere, pParentA, pParentB), m_Center(Cx, Cy, Cz), m_Radius(Radius)
                {
                    if (m_Center.IsNotAtInfinity() && _1D::IsNonZero(m_Radius) && _1D::IsNotAtInfinity(m_Radius))
                    {
                        SetStatus(eDefined);
                    }
                }

                CSphere3D::CSphere3D(const CVector3D& Center, const real Radius, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(eSphere, pParentA, pParentB), m_Center(Center), m_Radius(Radius)
                {
                    if (m_Center.IsNotAtInfinity() && _1D::IsNonZero(m_Radius) && _1D::IsNotAtInfinity(m_Radius))
                    {
                        SetStatus(eDefined);
                    }
                }

                CSphere3D::~CSphere3D()
                    = default;

                void CSphere3D::SetCenter(const CVector3D& Center)
                {
                    m_Center = Center;
                    if (m_Center.IsAtInfinity())
                    {
                        SetStatus(eUndefined);
                    }
                    else
                    {
                        SetStatus((_1D::IsNonZero(m_Radius) && _1D::IsNotAtInfinity(m_Radius)) ? eDefined : eUndefined);
                    }
                }

                const CVector3D& CSphere3D::GetCenter() const
                {
                    return m_Center;
                }

                void CSphere3D::SetRadius(const real Radius)
                {
                    m_Radius = Radius;
                    if (_1D::IsZero(m_Radius) || _1D::IsAtInfinity(m_Radius))
                    {
                        SetStatus(eUndefined);
                    }
                    else
                    {
                        SetStatus(m_Center.IsNotAtInfinity() ? eDefined : eUndefined);
                    }
                }

                real CSphere3D::GetRadius() const
                {
                    return m_Radius;
                }

                bool CSphere3D::IsOnSurface(const CVector3D& X) const
                {
                    return IsOnSphere(m_Center, m_Radius, X);
                }

                bool CSphere3D::IsInside(const CVector3D& X) const
                {
                    return IsInsideSphere(m_Center, m_Radius, X);
                }

                real CSphere3D::GetDistanceToSurface(const CVector3D& X) const
                {
                    return RealAbs(m_Center.GetDistance(X) - m_Radius);
                }

                real CSphere3D::GetDistanceToCenter(const CVector3D& X) const
                {
                    return m_Center.GetDistance(X);
                }

                CVector3D CSphere3D::GetClosestPointOnSurface(const CVector3D& X) const
                {
                    return m_Center + ComputeUnitaryDirectionVector(m_Center, X) * m_Radius;
                }

                string CSphere3D::ToString() const
                {
                    ostringstream Content;
                    Content.precision(_REAL_EPSILON_DISPLAY_DIGITS_);
                    Content << "<Sphere3D Id=\"" << m_InstanceId << "\" Status=\"" << GetStatusStringMode() << "\" Cx=\"" << m_Center.GetX() << "\" Cy=\"" << m_Center.GetY() << "\" Cz=\"" << m_Center.GetZ() << "\" Radius=\"" << m_Radius << "\"";
                    AppendParentalInformation(Content);
                    Content << "/>";
                    return Content.str();
                }

                bool CSphere3D::operator==(const CGeometricPrimitive3D& GeometricPrimitive) const
                {
                    if (IsUndefined() || GeometricPrimitive.IsUndefined())
                    {
                        return false;
                    }
                    if (&GeometricPrimitive == this)
                    {
                        return true;
                    }
                    if (GeometricPrimitive.GetTypeId() == GetTypeId())
                    {
                        const CSphere3D* pSphere = dynamic_cast<const CSphere3D*>(&GeometricPrimitive);
                        return (m_Radius == pSphere->m_Radius) && (m_Center == pSphere->m_Center);
                    }
                    return false;
                }

                IntersectionType CSphere3D::AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                if (pGeometricPrimitive == this)
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, eSphere);
                                    return eSelfInstanceIntersection;
                                }
                                return AnalyzeIntersection(dynamic_cast<const CSphere3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case eCircle:
                                return AnalyzeIntersection(dynamic_cast<const CCircle3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case ePointPair:
                                return AnalyzeIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case ePoint:
                                return AnalyzeIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case ePlane:
                                return AnalyzeIntersection(dynamic_cast<const CPlane3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case eLine:
                                return AnalyzeIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case eNone:
                                break;
                        }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive3D* CSphere3D::GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                if (pGeometricPrimitive == this)
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eSphere, eSelfInstanceIntersection);
                                    return nullptr;
                                }
                                return GenerateIntersection(dynamic_cast<const CSphere3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case eCircle:
                                return GenerateIntersection(dynamic_cast<const CCircle3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePointPair:
                                return GenerateIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePoint:
                                return GenerateIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePlane:
                                return GenerateIntersection(dynamic_cast<const CPlane3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case eLine:
                                return GenerateIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case eNone:
                                break;
                        }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                IntersectionType CSphere3D::AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (IsOnSphere(m_Center, m_Radius, pPoint->GetPoint()))
                    {
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSelfNumericalIntersection;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CSphere3D::AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const bool IntersectionA = IsOnSphere(m_Center, m_Radius, pPointPair->GetPointA());
                    const bool IntersectionB = IsOnSphere(m_Center, m_Radius, pPointPair->GetPointB());
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                            return eSelfNumericalIntersection;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSubspace;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CSphere3D::AnalyzeIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const CVector3D ClosestPoint = ComputeProjectionPointToLine(pLine->GetSupportPoint(), pLine->GetDirection(), m_Center);
                    if (ClosestPoint == m_Center)
                    {
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                        return eSubspace;
                    }
                    const CVector3D Displacement = ClosestPoint - m_Center;
                    CVector3D ImplicitPlaneNormal;
                    if (ComputeUnitaryOrthogonalVector(Displacement, pLine->GetDirection(), ImplicitPlaneNormal))
                    {
                        CVector3D A, B;
                        bool SinglePointFlag = false;
                        if (ComputeIntersectionCircleAndLine(ImplicitPlaneNormal, m_Center, m_Radius, ClosestPoint, A, B, SinglePointFlag))
                        {
                            if (SinglePointFlag)
                            {
                                SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                                return eSubspace;
                            }
                            SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                            return eSubspace;
                        }
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CSphere3D::AnalyzeIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const CVector3D& CircleCenter = pCircle->GetCenter();
                    const CVector3D& CircleNormal = pCircle->GetNormal();
                    const real CircleRadius = pCircle->GetRadius();
                    const CVector3D Displacement = CircleCenter - m_Center;
                    const real DisplacementLength = Displacement.GetLength();
                    bool SinglePointFlag = false;
                    if (IsIntersectingConfiguration(m_Radius, CircleRadius, DisplacementLength, SinglePointFlag))
                    {
                        if (SinglePointFlag)
                        {
                            const CVector3D X = m_Center + Displacement * m_Radius / DisplacementLength;
                            if (IsOnPlane(CircleNormal, CircleCenter, X))
                            {
                                SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                                return eSubspace;
                            }
                            SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                            return eEmpty;
                        }
                        if (m_Center == CircleCenter)
                        {
                            if (_1D::Equals(m_Radius, CircleRadius))
                            {
                                SetGeometricPrimitiveType(pIntersectionTypeId, eCircle);
                                return eSelfNumericalIntersection;
                            }
                            SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                            return eEmpty;
                        }
                        const CVector3D ImplicitCircleCenter = ComputeProjectionPointToPlane(CircleNormal, CircleCenter, m_Center);
                        const real DisplacementLength = m_Center.GetDistance(ImplicitCircleCenter);
                        const real DeviationDisplacementLength = DisplacementLength - m_Radius;
                        if (_1D::IsNonPositive(DeviationDisplacementLength))
                        {
                            if (_1D::IsZero(DeviationDisplacementLength))
                            {
                                if (_1D::Equals(CircleRadius, CircleCenter.GetDistance(ImplicitCircleCenter)))
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                                    return eSubspace;
                                }
                            }
                            else if (IsIntersectingConfiguration(RealSqrt(m_Radius * m_Radius - DisplacementLength * DisplacementLength), CircleRadius, CircleCenter.GetDistance(ImplicitCircleCenter), SinglePointFlag))
                            {
                                if (SinglePointFlag)
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                                    return eSubspace;
                                }
                                SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                                return eSubspace;
                            }
                        }
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CSphere3D::AnalyzeIntersection(const CPlane3D* pPlane, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const real DeviationDisplacementLength = m_Center.GetDistance(ComputeProjectionPointToPlane(pPlane->GetNormal(), pPlane->GetHesseDistance(), m_Center)) - m_Radius;
                    if (_1D::IsNonPositive(DeviationDisplacementLength))
                    {
                        if (_1D::IsZero(DeviationDisplacementLength))
                        {
                            SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                            return eSubspace;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, eCircle);
                        return eSubspace;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CSphere3D::AnalyzeIntersection(const CSphere3D* pSphere, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (m_Center == pSphere->m_Center)
                    {
                        if (_1D::Equals(m_Radius, pSphere->m_Radius))
                        {
                            SetGeometricPrimitiveType(pIntersectionTypeId, eSphere);
                            return eSelfNumericalIntersection;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                        return eEmpty;
                    }
                    bool SinglePointFlag = false;
                    if (IsIntersectingConfiguration(m_Radius, pSphere->m_Radius, m_Center.GetDistance(pSphere->m_Center), SinglePointFlag))
                    {
                        if (SinglePointFlag)
                        {
                            SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                            return eSubspace;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, eCircle);
                        return eSubspace;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive3D* CSphere3D::GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (IsOnSphere(m_Center, m_Radius, pPoint->GetPoint()))
                    {
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSelfNumericalIntersection);
                        return nullptr;
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CSphere3D::GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const bool IntersectionA = IsOnSphere(m_Center, m_Radius, pPointPair->GetPointA());
                    const bool IntersectionB = IsOnSphere(m_Center, m_Radius, pPointPair->GetPointB());
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePointPair, eSelfNumericalIntersection);
                            return nullptr;
                        }
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                        return new CPoint3D(IntersectionA ? pPointPair->GetPointA() : pPointPair->GetPointB(), this, pPointPair);
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CSphere3D::GenerateIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const CVector3D& LineDirection = pLine->GetDirection();
                    const CVector3D ClosestPoint = ComputeProjectionPointToLine(pLine->GetSupportPoint(), LineDirection, m_Center);
                    const CVector3D Displacement = ClosestPoint - m_Center;
                    if (Displacement.IsZeroLength())
                    {
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace);
                        return new CPointPair3D(m_Center + LineDirection * m_Radius, m_Center - LineDirection * m_Radius, this, pLine);
                    }
                    CVector3D ImplicitCircleNormal;
                    if (ComputeUnitaryOrthogonalVector(Displacement, LineDirection, ImplicitCircleNormal))
                    {
                        CVector3D A, B;
                        bool SinglePointFlag = false;
                        if (ComputeIntersectionCircleAndLine(ImplicitCircleNormal, m_Center, m_Radius, ClosestPoint, A, B, SinglePointFlag))
                        {
                            if (SinglePointFlag)
                            {
                                SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                                return new CPoint3D(A, this, pLine);
                            }
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace);
                            return new CPointPair3D(A, B, this, pLine);
                        }
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CSphere3D::GenerateIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const CVector3D& CircleCenter = pCircle->GetCenter();
                    const CVector3D& CircleNormal = pCircle->GetNormal();
                    const real CircleRadius = pCircle->GetRadius();
                    const CVector3D Displacement = CircleCenter - m_Center;
                    const real DisplacementLength = Displacement.GetLength();
                    bool SinglePointFlag = false;
                    if (IsIntersectingConfiguration(m_Radius, CircleRadius, DisplacementLength, SinglePointFlag))
                    {
                        if (SinglePointFlag)
                        {
                            const CVector3D X = m_Center + Displacement * m_Radius / DisplacementLength;
                            if (IsOnPlane(CircleNormal, CircleCenter, X))
                            {
                                SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                                return new CPoint3D(X, this, pCircle);
                            }
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                            return nullptr;
                        }
                        if (m_Center == CircleCenter)
                        {
                            if (_1D::Equals(m_Radius, CircleRadius))
                            {
                                SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eCircle, eSelfNumericalIntersection);
                                return nullptr;
                            }
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                            return nullptr;
                        }
                        const CVector3D ImplicitCircleCenter = ComputeProjectionPointToPlane(CircleNormal, CircleCenter, m_Center);
                        const CVector3D ImplicitCircleDisplacement = ImplicitCircleCenter - m_Center;
                        const real ImplicitDisplacementLength = ImplicitCircleDisplacement.GetLength();
                        const real DeviationDisplacementLength = ImplicitDisplacementLength - m_Radius;
                        if (_1D::IsNonPositive(DeviationDisplacementLength))
                        {
                            if (_1D::IsZero(DeviationDisplacementLength))
                            {
                                const CVector3D X = m_Center + ImplicitCircleDisplacement * m_Radius / ImplicitDisplacementLength;
                                if (IsOnSphere(CircleCenter, CircleRadius, X))
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                                    return new CPoint3D(X, this, pCircle);
                                }
                                SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                                return nullptr;
                            }
                            const real ImplicitCircleRadius = RealSqrt(m_Radius * m_Radius - ImplicitDisplacementLength * ImplicitDisplacementLength);
                            const real CircleToCircleDisplacementLenght = CircleCenter.GetDistance(ImplicitCircleCenter);
                            if (IsIntersectingConfiguration(ImplicitCircleRadius, CircleRadius, CircleToCircleDisplacementLenght, SinglePointFlag))
                            {
                                real Dx = _REAL_ZERO_, Dy = _REAL_ZERO_;
                                if (CirclesIntersection(TMax(ImplicitCircleRadius, CircleRadius), TMin(ImplicitCircleRadius, CircleRadius), CircleToCircleDisplacementLenght, Dx, Dy, SinglePointFlag))
                                {
                                    const CVector3D CenterBase = (ImplicitCircleRadius > CircleRadius) ? ImplicitCircleCenter : CircleCenter;
                                    const CVector3D CenterDirection = (ImplicitCircleRadius > CircleRadius) ? CircleCenter : ImplicitCircleCenter;
                                    const CVector3D Delta = ComputeUnitaryDirectionVector(CenterBase, CenterDirection) * Dx;
                                    const CVector3D MidPoint = CenterBase + Delta;
                                    if (SinglePointFlag)
                                    {
                                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                                        return new CPoint3D(MidPoint, this, pCircle);
                                    }
                                    CVector3D Beta;
                                    if (ComputeUnitaryOrthogonalVector(Delta, CircleNormal, Beta))
                                    {
                                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                                        Beta *= Dy;
                                        return new CPointPair3D(MidPoint + Beta, MidPoint - Beta, this, pCircle);
                                    }
                                }
                            }
                        }
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CSphere3D::GenerateIntersection(const CPlane3D* pPlane, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const CVector3D ImplicitCircleCenter = ComputeProjectionPointToPlane(pPlane->GetNormal(), pPlane->GetHesseDistance(), m_Center);
                    const CVector3D Displacement = ImplicitCircleCenter - m_Center;
                    const real DisplacementLength = Displacement.GetLength();
                    const real DeviationDisplacementLength = DisplacementLength - m_Radius;
                    if (_1D::IsNonPositive(DeviationDisplacementLength))
                    {
                        if (_1D::IsZero(DeviationDisplacementLength))
                        {
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                            return new CPoint3D(m_Center + Displacement * (m_Radius / Displacement.GetLength()), this, pPlane);
                        }
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eCircle, eSubspace);
                        return new CCircle3D(ImplicitCircleCenter, pPlane->GetNormal(), RealSqrt(m_Radius * m_Radius - DisplacementLength * DisplacementLength), this, pPlane);
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CSphere3D::GenerateIntersection(const CSphere3D* pSphere, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (m_Center == pSphere->m_Center)
                    {
                        if (_1D::Equals(m_Radius, pSphere->m_Radius))
                        {
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eSphere, eSelfNumericalIntersection);
                            return nullptr;
                        }
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                        return nullptr;
                    }
                    const CVector3D Displacement = pSphere->m_Center - m_Center;
                    const real DisplacementLength = Displacement.GetLength();
                    bool SinglePointFlag = false;
                    if (IsIntersectingConfiguration(m_Radius, pSphere->m_Radius, DisplacementLength, SinglePointFlag))
                    {
                        if (SinglePointFlag)
                        {
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                            return new CPoint3D(m_Center + Displacement * (m_Radius / DisplacementLength), this, pSphere);
                        }
                        else
                        {
                            real Dx = _REAL_ZERO_, Dy = _REAL_ZERO_;
                            if (CirclesIntersection(TMax(m_Radius, pSphere->m_Radius), TMin(m_Radius, pSphere->m_Radius), DisplacementLength, Dx, Dy, SinglePointFlag))
                            {
                                const CVector3D CenterBase = (m_Radius > pSphere->m_Radius) ? m_Center : pSphere->m_Center;
                                const CVector3D CenterDirection = (m_Radius > pSphere->m_Radius) ? pSphere->m_Center : m_Center;
                                const CVector3D Delta = ComputeUnitaryDirectionVector(CenterBase, CenterDirection);
                                SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eCircle, eSubspace);
                                return new CCircle3D(CenterBase + Delta * Dx, Delta, Dy, this, pSphere);
                            }
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                            return nullptr;
                        }
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }
            }
        }
    }
}
