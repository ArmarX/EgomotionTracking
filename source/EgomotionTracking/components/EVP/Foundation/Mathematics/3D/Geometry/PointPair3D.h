/*
 * PointPair3D.h
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

#include "GeometricPrimitive3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                class CPoint3D;

                class CPointPair3D: public CGeometricPrimitive3D
                {
                public:

                    CPointPair3D();
                    CPointPair3D(const real Xa, const real Ya, const real Za, const real Xb, const real Yb, const real Zb, const CGeometricPrimitive3D* pParentA = NULL, const CGeometricPrimitive3D* pParentB = NULL);
                    CPointPair3D(const CVector3D& PointA, const CVector3D& PointB, const CGeometricPrimitive3D* pParentA = NULL, const CGeometricPrimitive3D* pParentB = NULL);
                    ~CPointPair3D() override;

                    void SetPointA(const CVector3D& PointA);
                    const CVector3D& GetPointA() const;

                    void SetPointB(const CVector3D& PointB);
                    const CVector3D& GetPointB() const;

                    CVector3D GetMidPoint() const;
                    bool IsSinglePoint() const;
                    real GetABDistance() const;

                    string ToString() const override;
                    bool operator==(const CGeometricPrimitive3D& GeometricPrimitive) const override ;
                    IntersectionType AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId = NULL) const override ;
                    CGeometricPrimitive3D* GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId = NULL, IntersectionType* pIntersectionType = NULL) const override ;

                protected:

                    IntersectionType AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const ;
                    IntersectionType AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const ;
                    CGeometricPrimitive3D* GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const ;
                    CGeometricPrimitive3D* GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const ;

                    CVector3D m_PointA;
                    CVector3D m_PointB;
                };
            }
        }
    }
}

