/*
 * Line3D.cpp
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "Point3D.h"
#include "PointPair3D.h"
#include "Line3D.h"
#include "Circle3D.h"
#include "Plane3D.h"
#include "Sphere3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CLine3D::CLine3D() :
                    CGeometricPrimitive3D(eLine, nullptr, nullptr), m_Direction(CVector3D::s_Point_At_Origin_3D), m_SupportPoint(CVector3D::s_Point_At_Plus_Infinity_3D), m_pSourcePoints(nullptr)
                {
                }

                CLine3D::CLine3D(const real Xa, const real Ya, const real Za, const real Xb, const real Yb, const real Zb, const ArgumentMode Mode, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(eLine, pParentA, pParentB), m_Direction(CVector3D::s_Point_At_Origin_3D), m_SupportPoint(CVector3D::s_Point_At_Plus_Infinity_3D), m_pSourcePoints(nullptr)
                {
                    switch (Mode)
                    {
                        case eDirectionVectorAndSupportPoint:
                            m_Direction.SetValue(Xa, Ya, Za);
                            m_SupportPoint.SetValue(Xb, Yb, Zb);
                            if (m_Direction.IsNonZeroLength() && m_SupportPoint.IsNotAtInfinity())
                            {
                                m_Direction.Normalize();
                                SetStatus(eDefined);
                            }
                            break;
                        case ePointAPointB:
                            SetLineByPoints(CVector3D(Xa, Ya, Za), CVector3D(Xb, Yb, Zb));
                            break;
                    }
                }

                CLine3D::CLine3D(const CVector3D& A, const CVector3D& B, const ArgumentMode Mode, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(eLine, pParentA, pParentB), m_Direction(CVector3D::s_Point_At_Origin_3D), m_SupportPoint(CVector3D::s_Point_At_Plus_Infinity_3D), m_pSourcePoints(nullptr)
                {
                    switch (Mode)
                    {
                        case eDirectionVectorAndSupportPoint:
                            m_Direction = A;
                            m_SupportPoint = B;
                            if (m_Direction.IsNotAtInfinity() && m_SupportPoint.IsNotAtInfinity())
                            {
                                m_Direction.Normalize();
                                SetStatus(eDefined);
                            }
                            break;
                        case ePointAPointB:
                            SetLineByPoints(A, B);
                            break;
                    }
                }

                CLine3D::~CLine3D()
                {
                    RELEASE_OBJECT(m_pSourcePoints)
                }

                void CLine3D::SetLineByPoints(const CVector3D& A, const CVector3D& B)
                {
                    if (m_pSourcePoints)
                    {
                        m_pSourcePoints->SetPoints(A, B);
                    }
                    else
                    {
                        m_pSourcePoints = new CLineSourcePoints(A, B);
                    }
                    if (m_pSourcePoints->ArePointsValid())
                    {
                        m_SupportPoint = m_pSourcePoints->GetMidPoint();
                        m_Direction = m_pSourcePoints->GetDirection();
                        SetStatus(eDefined);
                    }
                    else
                    {
                        m_Direction = CVector3D::s_Point_At_Origin_3D;
                        m_SupportPoint = CVector3D::s_Point_At_Plus_Infinity_3D;
                        SetStatus(eUndefined);
                    }
                }

                bool CLine3D::HasSourcePoints() const
                {
                    return m_pSourcePoints;
                }

                bool CLine3D::GetSourcePoints(CVector3D& A, CVector3D& B) const
                {
                    if (m_pSourcePoints)
                    {
                        m_pSourcePoints->GetPoints(A, B);
                        return true;
                    }
                    return false;
                }

                void CLine3D::SetDirection(const CVector3D& Direction)
                {
                    m_Direction = Direction;
                    if (m_Direction.IsZeroLength())
                    {
                        SetStatus(eUndefined);
                    }
                    else
                    {
                        m_Direction.Normalize();
                        SetStatus(m_SupportPoint.IsAtInfinity() ? eUndefined : eDefined);
                    }
                }

                void CLine3D::SetSupportPoint(const CVector3D& SupportPoint)
                {
                    m_SupportPoint = SupportPoint;
                    if (m_SupportPoint.IsAtInfinity())
                    {
                        SetStatus(eUndefined);
                    }
                    else
                    {
                        SetStatus(m_Direction.IsUnitary() ? eDefined : eUndefined);
                    }
                }

                const CVector3D& CLine3D::GetDirection() const
                {
                    return m_Direction;
                }

                const CVector3D& CLine3D::GetSupportPoint() const
                {
                    return m_SupportPoint;
                }

                CVector3D CLine3D::GetPointAtUnitaryDisplacement() const
                {
                    return m_SupportPoint + m_Direction;
                }

                CVector3D CLine3D::GetPointAtDisplacement(const real Displacement) const
                {
                    return m_SupportPoint + (m_Direction * Displacement);
                }

                CVector3D CLine3D::GetClosestPoint(const CVector3D& X) const
                {
                    return ComputeProjectionPointToLine(m_SupportPoint, m_Direction, X);
                }

                bool CLine3D::GetClosestPoints(const CLine3D* pLine, CVector3D& A, CVector3D& B) const
                {
                    return ComputeLineToLineClosestPoints(m_SupportPoint, m_Direction, pLine->m_SupportPoint, pLine->m_Direction, A, B);
                }

                std::string CLine3D::ToString() const
                {
                    std::ostringstream Content;
                    Content.precision(_REAL_EPSILON_DISPLAY_DIGITS_);
                    Content << "<Line3D Id=\"" << m_InstanceId << "\" Status=\"" << GetStatusStringMode() << "\" Dx=\"" << m_Direction.GetX() << "\" Dy=\"" << m_Direction.GetY() << "\" Dz=\"" << m_Direction.GetZ() << "\" Sx=\"" << m_SupportPoint.GetX() << "\" Sy=\"" << m_SupportPoint.GetY() << "\" Sz=\"" << m_SupportPoint.GetZ() << "\"";
                    AppendParentalInformation(Content);
                    Content << "/>";
                    return Content.str();
                }

                bool CLine3D::operator==(const CGeometricPrimitive3D& GeometricPrimitive) const
                {
                    if (IsUndefined() || GeometricPrimitive.IsUndefined())
                    {
                        return false;
                    }
                    if (&GeometricPrimitive == this)
                    {
                        return true;
                    }
                    if (GeometricPrimitive.GetTypeId() == GetTypeId())
                    {
                        const CLine3D* pLine = dynamic_cast<const CLine3D*>(&GeometricPrimitive);
                        return (m_Direction == pLine->m_Direction) && (m_SupportPoint == pLine->m_SupportPoint);
                    }
                    return false;
                }

                IntersectionType CLine3D::AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case eCircle:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case ePointPair:
                                return AnalyzeIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case ePoint:
                                return AnalyzeIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case ePlane:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case eLine:
                                if (pGeometricPrimitive == this)
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, eLine);
                                    return eSelfInstanceIntersection;
                                }
                                return AnalyzeIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case eNone:
                                break;
                        }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive3D* CLine3D::GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case eCircle:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePointPair:
                                return GenerateIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePoint:
                                return GenerateIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePlane:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case eLine:
                                if (pGeometricPrimitive == this)
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eLine, eSelfInstanceIntersection);
                                    return nullptr;
                                }
                                return GenerateIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case eNone:
                                break;
                        }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                IntersectionType CLine3D::AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (pPoint->GetPoint() == ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pPoint->GetPoint()))
                    {
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSelfNumericalIntersection;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CLine3D::AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const bool IntersectionA = (pPointPair->GetPointA() == ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pPointPair->GetPointA()));
                    const bool IntersectionB = (pPointPair->GetPointB() == ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pPointPair->GetPointB()));
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                            return eSelfNumericalIntersection;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSubspace;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CLine3D::AnalyzeIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (AreUnitaryVectorsParallel(m_Direction, pLine->m_Direction))
                    {
                        const CVector3D ClosestPoint = ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pLine->m_SupportPoint);
                        if (ClosestPoint == pLine->m_SupportPoint)
                        {
                            SetGeometricPrimitiveType(pIntersectionTypeId, eLine);
                            return eSelfNumericalIntersection;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                        return eEmpty;
                    }
                    CVector3D A, B;
                    if (ComputeLineToLineClosestPoints(m_SupportPoint, m_Direction, pLine->m_SupportPoint, pLine->m_Direction, A, B) && (A == B))
                    {
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSubspace;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive3D* CLine3D::GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pPoint->GetPoint() == ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pPoint->GetPoint()))
                    {
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSelfNumericalIntersection);
                        return nullptr;
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CLine3D::GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const bool IntersectionA = (pPointPair->GetPointA() == ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pPointPair->GetPointA()));
                    const bool IntersectionB = (pPointPair->GetPointB() == ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pPointPair->GetPointB()));
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePointPair, eSelfNumericalIntersection);
                            return nullptr;
                        }
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                        return new CPoint3D(IntersectionA ? pPointPair->GetPointA() : pPointPair->GetPointB(), this, pPointPair);
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CLine3D::GenerateIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (AreUnitaryVectorsParallel(m_Direction, pLine->m_Direction))
                    {
                        const CVector3D ClosestPoint = ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pLine->m_SupportPoint);
                        if (ClosestPoint == pLine->m_SupportPoint)
                        {
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eLine, eSelfNumericalIntersection);
                            return nullptr;
                        }
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                        return nullptr;
                    }
                    CVector3D A, B;
                    if (ComputeLineToLineClosestPoints(m_SupportPoint, m_Direction, pLine->m_SupportPoint, pLine->m_Direction, A, B) && (A == B))
                    {
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                        return new CPoint3D(CVector3D::CreateMidPoint(A, B), this, pLine);
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }
            }
        }
    }
}
