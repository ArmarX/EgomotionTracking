/*
 * Common3D.cpp
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "Common3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                bool AreUnitaryVectorsParallel(const CVector3D& A, const CVector3D& B)
                {
                    return (RealAbs(RealAbs(A.ScalarProduct(B)) - _REAL_ONE_) < _REAL_EPSILON_);
                }

                bool AreUnitaryVectorsOrthogonal(const CVector3D& A, const CVector3D& B)
                {
                    return (RealAbs(A.ScalarProduct(B)) < _REAL_EPSILON_);
                }

                bool IsOnPlane(const CVector3D& Normal, const real HesseDistance, const CVector3D& X)
                {
                    return (RealAbs(Normal.ScalarProduct(X) - HesseDistance) < _REAL_EPSILON_);
                }

                bool IsOnPlane(const CVector3D& Normal, const CVector3D& SupportPoint, const CVector3D& X)
                {
                    return (RealAbs(Normal.ScalarProduct(X - SupportPoint)) < _REAL_EPSILON_);
                }

                bool IsOnPositiveSideOfPlane(const CVector3D& Normal, const real HesseDistance, const CVector3D& X)
                {
                    return ((Normal.ScalarProduct(X) - HesseDistance) > _REAL_ZERO_);
                }

                bool IsOnSphere(const CVector3D& Center, const real Radius, const CVector3D& X)
                {
                    return (RealAbs(Center.GetDistance(X) - Radius) < _REAL_EPSILON_);
                }

                bool IsInsideSphere(const CVector3D& Center, const real Radius, const CVector3D& X)
                {
                    return ((Radius - Center.GetDistance(X)) > _REAL_EPSILON_);
                }

                CVector3D ComputeUnitaryDirectionVector(const CVector3D& A, const CVector3D& B)
                {
                    CVector3D UnitaryDirectionVector = B - A;
                    UnitaryDirectionVector.Normalize();
                    return UnitaryDirectionVector;
                }

                bool ComputeUnitaryOrthogonalVector(const CVector3D& A, const CVector3D& B, CVector3D& C)
                {
                    C = A.VectorProduct(B);
                    return (C.Normalize() > _REAL_ZERO_);
                }

                CVector3D ComputeProjectionPointToLine(const CVector3D& SupportPoint, const CVector3D& Direction, const CVector3D& X)
                {
                    return SupportPoint + (Direction * Direction.ScalarProduct(X - SupportPoint));
                }

                CVector3D ComputeProjectionPointToPlane(const CVector3D& Normal, const CVector3D& SupportPoint, const CVector3D& X)
                {
                    return X - (Normal * Normal.ScalarProduct(X - SupportPoint));
                }

                CVector3D ComputeProjectionPointToPlane(const CVector3D& Normal, const real HesseDistance, const CVector3D& X)
                {
                    return X - Normal * (Normal.ScalarProduct(X) - HesseDistance);
                }

                bool ComputeIntersectionLineAndPlane(const CVector3D& LineSupportPoint, const CVector3D& LineDirection, const CVector3D& PlaneNormal, const CVector3D& PlaneSupportPoint, CVector3D& X)
                {
                    const real Discriminat = PlaneNormal.ScalarProduct(LineDirection);
                    if (RealAbs(Discriminat) <= _REAL_EPSILON_)
                    {
                        return false;
                    }
                    else
                    {
                        X = LineSupportPoint + LineDirection * (PlaneNormal.ScalarProduct(PlaneSupportPoint - LineSupportPoint) / Discriminat);
                        return true;
                    }
                }

                bool ComputeIntersectionPlaneAndPlane(const CVector3D& PlaneNormal1, const real HesseDistance1, const CVector3D& PlaneNormal2, const real HesseDistance2, CVector3D& LineDirection, CVector3D& LineSupportPoint)
                {
                    LineDirection = PlaneNormal1.VectorProduct(PlaneNormal2);
                    if (LineDirection.GetLength() > _REAL_EPSILON_)
                    {
                        LineDirection.Normalize();
                        real A[3][3];
                        A[0][0] = PlaneNormal1.GetX();
                        A[0][1] = PlaneNormal1.GetY();
                        A[0][2] = PlaneNormal1.GetZ();
                        A[1][0] = PlaneNormal2.GetX();
                        A[1][1] = PlaneNormal2.GetY();
                        A[1][2] = PlaneNormal2.GetZ();
                        A[2][0] = LineDirection.GetX();
                        A[2][1] = LineDirection.GetY();
                        A[2][2] = LineDirection.GetZ();
                        const real Determinant = -A[0][2] * A[1][1] * A[2][0] + A[0][1] * A[1][2] * A[2][0] + A[0][2] * A[1][0] * A[2][1] - A[0][0] * A[1][2] * A[2][1] - A[0][1] * A[1][0] * A[2][2] + A[0][0] * A[1][1] * A[2][2];
                        if (RealAbs(Determinant) > _REAL_EPSILON_)
                        {
                            const real Scale = _REAL_ONE_ / Determinant;
                            real PartialAI[3][2];
                            PartialAI[0][0] = (-A[1][2] * A[2][1] + A[1][1] * A[2][2]) * Scale;
                            PartialAI[0][1] = (A[0][2] * A[2][1] - A[0][1] * A[2][2]) * Scale;
                            PartialAI[1][0] = (A[1][2] * A[2][0] - A[1][0] * A[2][2]) * Scale;
                            PartialAI[1][1] = (-A[0][2] * A[2][0] + A[0][0] * A[2][2]) * Scale;
                            PartialAI[2][0] = (-A[1][1] * A[2][0] + A[1][0] * A[2][1]) * Scale;
                            PartialAI[2][1] = (A[0][1] * A[2][0] - A[0][0] * A[2][1]) * Scale;
                            LineSupportPoint.SetX(PartialAI[0][0] * HesseDistance1 + PartialAI[0][1] * HesseDistance2);
                            LineSupportPoint.SetY(PartialAI[1][0] * HesseDistance1 + PartialAI[1][1] * HesseDistance2);
                            LineSupportPoint.SetZ(PartialAI[2][0] * HesseDistance1 + PartialAI[2][1] * HesseDistance2);
                            return true;
                        }
                    }
                    return false;
                }

                bool IsIntersectingConfiguration(const real R1, const real R2, const real D12, bool& SinglePointFlag)
                {
                    SinglePointFlag = RealAbs((R1 + R2) - D12) <= _REAL_EPSILON_;
                    if (SinglePointFlag)
                    {
                        return true;
                    }
                    else
                    {
                        return (((R1 + R2) - D12) > -_REAL_EPSILON_) && (((R2 - R1) + D12) > -_REAL_EPSILON_) && (((R1 - R2) + D12) > -_REAL_EPSILON_);
                    }
                }

                bool CirclesIntersection(const real R, const real r, const real d, real& x, real& y, bool& SinglePointFlag)
                {
                    SinglePointFlag = RealAbs((R + r) - d) <= _REAL_EPSILON_;
                    if (SinglePointFlag)
                    {
                        x = R;
                        y = _REAL_ZERO_;
                        return true;
                    }
                    else
                    {
                        x = (d * d - r * r + R * R) / (_REAL_TWO_ * d);
                        const real Discriminat = R * R - x * x;
                        if (Discriminat < _REAL_ZERO_)
                        {
                            return false;
                        }
                        y = RealSqrt(Discriminat);
                        return true;
                    }
                }

                bool ComputeLineToLineClosestPoints(const CVector3D& LineSupportPoint1, const CVector3D& LineDirection1, const CVector3D& LineSupportPoint2, const CVector3D& LineDirection2, CVector3D& Point1, CVector3D& Point2)
                {
                    const real SwapArea = LineDirection1.VectorProduct(LineDirection2).GetLength();
                    if (SwapArea > _REAL_EPSILON_)
                    {
                        real A[3][2];
                        real ATA[2][2];
                        real Accumulator = _REAL_ZERO_;
                        A[0][0] = LineDirection1.GetX();
                        A[1][0] = LineDirection1.GetY();
                        A[2][0] = LineDirection1.GetZ();
                        A[0][1] = -LineDirection2.GetX();
                        A[1][1] = -LineDirection2.GetY();
                        A[2][1] = -LineDirection2.GetZ();
                        for (int r = 0; r < 2; ++r)
                            for (int c = 0; c < 2; ++c)
                            {
                                Accumulator = _REAL_ZERO_;
                                for (int i = 0; i < 3; ++i)
                                {
                                    Accumulator += A[i][r] * A[i][c];
                                }
                                ATA[r][c] = Accumulator;
                            }
                        const real Determinant = ATA[0][0] * ATA[1][1] - ATA[1][0] * ATA[0][1];
                        if (RealAbs(Determinant) > _REAL_EPSILON_)
                        {
                            real ATA_I[2][2];
                            real ATA_IAT[2][3];
                            ATA_I[0][0] = ATA[1][1] / Determinant;
                            ATA_I[0][1] = -ATA[0][1] / Determinant;
                            ATA_I[1][0] = -ATA[1][0] / Determinant;
                            ATA_I[1][1] = ATA[0][0] / Determinant;
                            for (int r = 0; r < 2; ++r)
                                for (int c = 0; c < 3; ++c)
                                {
                                    Accumulator = _REAL_ZERO_;
                                    for (uint i = 0; i < 2; ++i)
                                    {
                                        Accumulator += ATA_I[r][i] * A[c][i];
                                    }
                                    ATA_IAT[r][c] = Accumulator;
                                }
                            real X[2] = { _REAL_ZERO_ };
                            const real Deltas[] = { LineSupportPoint2.GetX() - LineSupportPoint1.GetX(), LineSupportPoint2.GetY() - LineSupportPoint1.GetY(), LineSupportPoint2.GetZ() - LineSupportPoint1.GetZ() };
                            for (int r = 0; r < 2; ++r)
                                for (int c = 0; c < 1; ++c)
                                {
                                    Accumulator = _REAL_ZERO_;
                                    for (int i = 0; i < 3; ++i)
                                    {
                                        Accumulator += ATA_IAT[r][i] * Deltas[i];
                                    }
                                    X[r] = Accumulator;
                                }
                            Point1 = LineSupportPoint1 + LineDirection1 * X[0];
                            Point2 = LineSupportPoint2 + LineDirection2 * X[1];
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                bool ComputeIntersectionCircleAndLine(const CVector3D& Normal, const CVector3D& Center, const real Radius, const CVector3D& CircleCenterProjectedOntoLine, CVector3D& A, CVector3D& B, bool& SinglePointFlag)
                {
                    const CVector3D Displacement = CircleCenterProjectedOntoLine - Center;
                    const real DisplacementLenght = Displacement.GetLength();
                    const real LenghtDeviation = DisplacementLenght - Radius;
                    if (LenghtDeviation <= _REAL_EPSILON_)
                    {
                        SinglePointFlag = RealAbs(LenghtDeviation) <= _REAL_EPSILON_;
                        if (SinglePointFlag)
                        {
                            A = CircleCenterProjectedOntoLine;
                            B = CircleCenterProjectedOntoLine;
                        }
                        else
                        {
                            CVector3D ComplementaryDisplacement = Displacement.VectorProduct(Normal);
                            ComplementaryDisplacement.Normalize();
                            ComplementaryDisplacement *= RealSqrt(Radius * Radius - DisplacementLenght * DisplacementLenght);
                            A = CircleCenterProjectedOntoLine + ComplementaryDisplacement;
                            B = CircleCenterProjectedOntoLine - ComplementaryDisplacement;
                        }
                        return true;
                    }
                    return false;
                }
            }
        }
    }
}
