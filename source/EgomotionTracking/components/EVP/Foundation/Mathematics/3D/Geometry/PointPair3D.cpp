/*
 * PointPair3D.cpp
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "Point3D.h"
#include "PointPair3D.h"
#include "Line3D.h"
#include "Circle3D.h"
#include "Plane3D.h"
#include "Sphere3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CPointPair3D::CPointPair3D() :
                    CGeometricPrimitive3D(ePointPair, nullptr, nullptr), m_PointA(CVector3D::s_Point_At_Plus_Infinity_3D), m_PointB(CVector3D::s_Point_At_Plus_Infinity_3D)
                {
                }

                CPointPair3D::CPointPair3D(const real Xa, const real Ya, const real Za, const real Xb, const real Yb, const real Zb, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(ePointPair, pParentA, pParentB), m_PointA(Xa, Ya, Za), m_PointB(Xb, Yb, Zb)
                {
                    if (m_PointA.IsNotAtInfinity() && m_PointB.IsNotAtInfinity())
                    {
                        SetStatus(eDefined);
                    }
                }

                CPointPair3D::CPointPair3D(const CVector3D& PointA, const CVector3D& PointB, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(ePointPair, pParentA, pParentB), m_PointA(PointA), m_PointB(PointB)
                {
                    if (m_PointA.IsNotAtInfinity() && m_PointB.IsNotAtInfinity())
                    {
                        SetStatus(eDefined);
                    }
                }

                CPointPair3D::~CPointPair3D()
                    = default;

                void CPointPair3D::SetPointA(const CVector3D& PointA)
                {
                    m_PointA = PointA;
                    SetStatus((m_PointA.IsAtInfinity() || m_PointB.IsAtInfinity()) ? eUndefined : eDefined);
                }

                const CVector3D& CPointPair3D::GetPointA() const
                {
                    return m_PointA;
                }

                void CPointPair3D::SetPointB(const CVector3D& PointB)
                {
                    m_PointB = PointB;
                    SetStatus((m_PointB.IsAtInfinity() || m_PointA.IsAtInfinity()) ? eUndefined : eDefined);
                }

                const CVector3D& CPointPair3D::GetPointB() const
                {
                    return m_PointB;
                }

                CVector3D CPointPair3D::GetMidPoint() const
                {
                    return CVector3D::CreateMidPoint(m_PointA, m_PointB);
                }

                bool CPointPair3D::IsSinglePoint() const
                {
                    return (m_PointA == m_PointB);
                }

                real CPointPair3D::GetABDistance() const
                {
                    return m_PointA.GetDistance(m_PointB);
                }

                std::string CPointPair3D::ToString() const
                {
                    std::ostringstream Content;
                    Content.precision(_REAL_EPSILON_DISPLAY_DIGITS_);
                    Content << "<PointPair3D Id=\"" << m_InstanceId << "\" Status=\"" << GetStatusStringMode() << "\" Xa=\"" << m_PointA.GetX() << "\" Ya=\"" << m_PointA.GetY() << "\" Za=\"" << m_PointA.GetZ() << "\" Xb=\"" << m_PointB.GetX() << "\" Yb=\"" << m_PointB.GetY() << "\" Zb=\"" << m_PointB.GetZ() << "\"";
                    AppendParentalInformation(Content);
                    Content << "/>";
                    return Content.str();
                }

                bool CPointPair3D::operator==(const CGeometricPrimitive3D& GeometricPrimitive) const
                {
                    if (IsUndefined() || GeometricPrimitive.IsUndefined())
                    {
                        return false;
                    }
                    if (&GeometricPrimitive == this)
                    {
                        return true;
                    }
                    if (GeometricPrimitive.GetTypeId() == GetTypeId())
                    {
                        const CPointPair3D* pPointPair = dynamic_cast<const CPointPair3D*>(&GeometricPrimitive);
                        return ((m_PointA == pPointPair->m_PointA) && (m_PointB == pPointPair->m_PointB)) || ((m_PointA == pPointPair->m_PointB) && (m_PointB == pPointPair->m_PointA));
                    }
                    return false;
                }

                IntersectionType CPointPair3D::AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case eCircle:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case ePointPair:
                                if (pGeometricPrimitive == this)
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                                    return eSelfInstanceIntersection;
                                }
                                return AnalyzeIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case ePoint:
                                return AnalyzeIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case ePlane:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case eLine:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case eNone:
                                break;
                        }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive3D* CPointPair3D::GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case eCircle:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePointPair:
                                if (pGeometricPrimitive == this)
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSelfInstanceIntersection);
                                    return nullptr;
                                }
                                return GenerateIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePoint:
                                return GenerateIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePlane:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case eLine:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case eNone:
                                break;
                        }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                IntersectionType CPointPair3D::AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const CVector3D& Point = pPoint->GetPoint();
                    if (m_PointA == m_PointB)
                    {
                        if ((CVector3D::CreateMidPoint(m_PointA, m_PointB) == Point) || (m_PointA == Point) || (m_PointB == Point))
                        {
                            SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                            return eSelfNumericalIntersection;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                        return eEmpty;
                    }
                    if ((m_PointA == Point) || (m_PointB == Point))
                    {
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSelfNumericalIntersection;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CPointPair3D::AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const bool IntersectionAA = (m_PointA == pPointPair->m_PointA);
                    const bool IntersectionAB = (m_PointA == pPointPair->m_PointB);
                    const bool IntersectionBA = (m_PointB == pPointPair->m_PointA);
                    const bool IntersectionBB = (m_PointB == pPointPair->m_PointB);
                    if (IntersectionAA || IntersectionAB || IntersectionBA || IntersectionBB)
                    {
                        const bool DirectIncidenceIntersection = IntersectionAA && IntersectionBB;
                        const bool CrossedIncidenceIntersection = IntersectionAB && IntersectionBA;
                        if (DirectIncidenceIntersection || CrossedIncidenceIntersection)
                        {
                            if (DirectIncidenceIntersection && CrossedIncidenceIntersection)
                            {
                                SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                                return eSelfNumericalIntersection;
                            }
                            SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                            return eSelfNumericalIntersection;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSubspace;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive3D* CPointPair3D::GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if ((m_PointA == pPoint->GetPoint()) || (m_PointB == pPoint->GetPoint()))
                    {
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSelfNumericalIntersection);
                        return nullptr;
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CPointPair3D::GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const bool IntersectionAA = (m_PointA == pPointPair->m_PointA);
                    const bool IntersectionAB = (m_PointA == pPointPair->m_PointB);
                    const bool IntersectionBA = (m_PointB == pPointPair->m_PointA);
                    const bool IntersectionBB = (m_PointB == pPointPair->m_PointB);
                    if (IntersectionAA || IntersectionAB || IntersectionBA || IntersectionBB)
                    {
                        if ((IntersectionAA && IntersectionBB) || (IntersectionAB && IntersectionBA))
                        {
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePointPair, eSelfNumericalIntersection);
                            return nullptr;
                        }
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                        return new CPoint3D((IntersectionAA || IntersectionAB) ? m_PointA : m_PointB, this, pPointPair);
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }
            }
        }
    }
}
