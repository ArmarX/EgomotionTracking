/*
 * Point3D.cpp
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "Point3D.h"
#include "PointPair3D.h"
#include "Line3D.h"
#include "Circle3D.h"
#include "Plane3D.h"
#include "Sphere3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CPoint3D::CPoint3D() :
                    CGeometricPrimitive3D(ePoint, nullptr, nullptr), m_Point(CVector3D::s_Point_At_Plus_Infinity_3D)
                {
                }

                CPoint3D::CPoint3D(const real X, const real Y, const real Z, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(ePoint, pParentA, pParentB), m_Point(X, Y, Z)
                {
                    if (m_Point.IsNotAtInfinity())
                    {
                        SetStatus(eDefined);
                    }
                }

                CPoint3D::CPoint3D(const CVector3D& Point, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(ePoint, pParentA, pParentB), m_Point(Point)
                {
                    if (m_Point.IsNotAtInfinity())
                    {
                        SetStatus(eDefined);
                    }
                }

                CPoint3D::~CPoint3D()
                    = default;

                void CPoint3D::SetPoint(const CVector3D& Point)
                {
                    m_Point = Point;
                    SetStatus(m_Point.IsAtInfinity() ? eUndefined : eDefined);
                }

                const CVector3D& CPoint3D::GetPoint() const
                {
                    return m_Point;
                }

                std::string CPoint3D::ToString() const
                {
                    std::ostringstream Content;
                    Content.precision(_REAL_EPSILON_DISPLAY_DIGITS_);
                    Content << "<Point3D Id=\"" << m_InstanceId << "\" Status=\"" << GetStatusStringMode() << "\" X=\"" << m_Point.GetX() << "\" Y=\"" << m_Point.GetY() << "\" Z=\"" << m_Point.GetZ() << "\"";
                    AppendParentalInformation(Content);
                    Content << "/>";
                    return Content.str();
                }

                bool CPoint3D::operator==(const CGeometricPrimitive3D& GeometricPrimitive) const
                {
                    if (IsUndefined() || GeometricPrimitive.IsUndefined())
                    {
                        return false;
                    }
                    if (&GeometricPrimitive == this)
                    {
                        return true;
                    }
                    if (GeometricPrimitive.GetTypeId() == GetTypeId())
                    {
                        const CPoint3D* pPoints = dynamic_cast<const CPoint3D*>(&GeometricPrimitive);
                        return (m_Point == pPoints->m_Point);
                    }
                    return false;
                }

                IntersectionType CPoint3D::AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case eCircle:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case ePointPair:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case ePoint:
                                if (pGeometricPrimitive == this)
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                                    return eSelfInstanceIntersection;
                                }
                                return AnalyzeIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case ePlane:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case eLine:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case eNone:
                                break;
                        }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive3D* CPoint3D::GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case eCircle:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePointPair:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePoint:
                                if (pGeometricPrimitive == this)
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSelfInstanceIntersection);
                                    return nullptr;
                                }
                                return GenerateIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePlane:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case eLine:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case eNone:
                                break;
                        }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                IntersectionType CPoint3D::AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (m_Point == pPoint->m_Point)
                    {
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSelfNumericalIntersection;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive3D* CPoint3D::GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (m_Point == pPoint->m_Point)
                    {
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSelfNumericalIntersection);
                        return nullptr;
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }
            }
        }
    }
}
