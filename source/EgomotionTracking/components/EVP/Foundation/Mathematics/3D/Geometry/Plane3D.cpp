/*
 * Plane3D.cpp
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "Point3D.h"
#include "PointPair3D.h"
#include "Line3D.h"
#include "Circle3D.h"
#include "Plane3D.h"
#include "Sphere3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CPlane3D::CPlane3D() :
                    CGeometricPrimitive3D(ePlane, nullptr, nullptr), m_Normal(CVector3D::s_Point_At_Origin_3D), m_HesseDistance(_REAL_MAX_), m_pSourcePoints(nullptr)
                {
                }

                CPlane3D::CPlane3D(const real Nx, const real Ny, const real Nz, const real HesseDistance, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(ePlane, pParentA, pParentB), m_Normal(Nx, Ny, Nz), m_HesseDistance(HesseDistance), m_pSourcePoints(nullptr)
                {
                    if (m_Normal.IsNonZeroLength() && m_Normal.IsNotAtInfinity() && _1D::IsNotAtInfinity(m_HesseDistance))
                    {
                        m_Normal.Normalize();
                        SetStatus(eDefined);
                    }
                }

                CPlane3D::CPlane3D(const CVector3D& Normal, const real HesseDistance, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(ePlane, pParentA, pParentB), m_Normal(Normal), m_HesseDistance(HesseDistance), m_pSourcePoints(nullptr)
                {
                    if (m_Normal.IsNonZeroLength() && m_Normal.IsNotAtInfinity() && _1D::IsNotAtInfinity(m_HesseDistance))
                    {
                        m_Normal.Normalize();
                        SetStatus(eDefined);
                    }
                }

                CPlane3D::CPlane3D(const CVector3D& A, const CVector3D& B, const CVector3D& C, const CVector3D* pPointOnPositiveSide) :
                    CGeometricPrimitive3D(ePlane, nullptr, nullptr), m_Normal(CVector3D::s_Point_At_Origin_3D), m_HesseDistance(_REAL_MAX_), m_pSourcePoints(nullptr)
                {
                    SetFromPoints(A, B, C, pPointOnPositiveSide);
                }

                CPlane3D::~CPlane3D()
                {
                    RELEASE_OBJECT(m_pSourcePoints)
                }

                bool CPlane3D::SetFromPoints(const CVector3D& A, const CVector3D& B, const CVector3D& C, const CVector3D* pPointOnSide)
                {
                    if (A.IsNotAtInfinity() && B.IsNotAtInfinity() && C.IsNotAtInfinity())
                    {
                        const CVector3D AB = B - A;
                        if (AB.IsNonZeroLength())
                        {
                            const CVector3D AC = C - A;
                            if (AC.IsNonZeroLength())
                            {
                                const CVector3D PerpendicularVector = AB.VectorProduct(AC);
                                if (PerpendicularVector.IsNonZeroLength())
                                {
                                    if (m_pSourcePoints)
                                    {
                                        m_pSourcePoints->SetPoints(A, B, C);
                                    }
                                    else
                                    {
                                        m_pSourcePoints = new CPlaneSourcePoints(A, B, C);
                                    }
                                    m_Normal = PerpendicularVector;
                                    m_Normal.Normalize();
                                    m_HesseDistance = m_Normal.ScalarProduct(A);
                                    if (pPointOnSide && _1D::IsNegative(m_Normal.ScalarProduct(*pPointOnSide) - m_HesseDistance))
                                    {
                                        m_Normal.Negate();
                                        m_HesseDistance = m_Normal.ScalarProduct(A);
                                        m_pSourcePoints->SetPoints(A, C, B);
                                    }
                                    SetStatus(eDefined);
                                    return true;
                                }
                            }
                        }
                    }
                    SetStatus(eUndefined);
                    return false;
                }

                void CPlane3D::SetNormal(const CVector3D& Normal)
                {
                    m_Normal = Normal;
                    if (m_Normal.IsZeroLength())
                    {
                        SetStatus(eUndefined);
                    }
                    else
                    {
                        m_Normal.Normalize();
                        SetStatus(_1D::IsNotAtInfinity(m_HesseDistance) ? eDefined : eUndefined);
                    }
                }

                const CVector3D& CPlane3D::GetNormal() const
                {
                    return m_Normal;
                }

                void CPlane3D::SetHesseDistance(const real HesseDistance)
                {
                    m_HesseDistance = HesseDistance;
                    if (_1D::IsAtInfinity(m_HesseDistance))
                    {
                        SetStatus(eUndefined);
                    }
                    else
                    {
                        SetStatus(m_Normal.IsNonZeroLength() ? eDefined : eUndefined);
                    }
                }

                real CPlane3D::GetHesseDistance() const
                {
                    return m_HesseDistance;
                }

                real CPlane3D::GetDistanceToPoint(const CVector3D& X) const
                {
                    return RealAbs(m_Normal.ScalarProduct(X) - m_HesseDistance);
                }

                real CPlane3D::GetOrientedDistanceToPoint(const CVector3D& X) const
                {
                    return m_Normal.ScalarProduct(X) - m_HesseDistance;
                }

                bool CPlane3D::InOnPositiveSide(const CVector3D& X) const
                {
                    return IsOnPositiveSideOfPlane(m_Normal, m_HesseDistance, X);
                }

                CVector3D CPlane3D::GetClosestPointOnPlane(const CVector3D& X) const
                {
                    return X - m_Normal * (m_Normal.ScalarProduct(X) - m_HesseDistance);
                }

                CVector3D CPlane3D::GetSupportPoint() const
                {
                    return m_Normal * m_HesseDistance;
                }

                bool CPlane3D::HasSourcePoints() const
                {
                    return m_pSourcePoints;
                }

                bool CPlane3D::GetSourcePoints(CVector3D& A, CVector3D& B, CVector3D& C) const
                {
                    if (m_pSourcePoints)
                    {
                        m_pSourcePoints->GetPoints(A, B, C);
                        return true;
                    }
                    return false;
                }

                std::string CPlane3D::ToString() const
                {
                    std::ostringstream Content;
                    Content.precision(_REAL_EPSILON_DISPLAY_DIGITS_);
                    Content << "<Plane3D Id=\"" << m_InstanceId << "\" Status=\"" << GetStatusStringMode() << "\" Nx=\"" << m_Normal.GetX() << "\" Ny=\"" << m_Normal.GetY() << "\" Nz=\"" << m_Normal.GetZ() << "\" HesseDistance=\"" << m_HesseDistance << "\"";
                    AppendParentalInformation(Content);
                    Content << "/>";
                    return Content.str();
                }

                bool CPlane3D::operator==(const CGeometricPrimitive3D& GeometricPrimitive) const
                {
                    if (IsUndefined() || GeometricPrimitive.IsUndefined())
                    {
                        return false;
                    }
                    if (&GeometricPrimitive == this)
                    {
                        return true;
                    }
                    if (GeometricPrimitive.GetTypeId() == GetTypeId())
                    {
                        const CPlane3D* pPlane = dynamic_cast<const CPlane3D*>(&GeometricPrimitive);
                        return (m_Normal == pPlane->m_Normal) && _1D::Equals(m_HesseDistance, pPlane->m_HesseDistance);
                    }
                    return false;
                }

                IntersectionType CPlane3D::AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case eCircle:
                                return AnalyzeIntersection(dynamic_cast<const CCircle3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case ePointPair:
                                return AnalyzeIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case ePoint:
                                return AnalyzeIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case ePlane:
                                if (pGeometricPrimitive == this)
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, ePlane);
                                    return eSelfInstanceIntersection;
                                }
                                return AnalyzeIntersection(dynamic_cast<const CPlane3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case eLine:
                                return AnalyzeIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case eNone:
                                break;
                        }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive3D* CPlane3D::GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case eCircle:
                                return GenerateIntersection(dynamic_cast<const CCircle3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePointPair:
                                return GenerateIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePoint:
                                return GenerateIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePlane:
                                if (pGeometricPrimitive == this)
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePlane, eSelfInstanceIntersection);
                                    return nullptr;
                                }
                                return GenerateIntersection(dynamic_cast<const CPlane3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case eLine:
                                return GenerateIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case eNone:
                                break;
                        }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                IntersectionType CPlane3D::AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (IsOnPlane(m_Normal, m_HesseDistance, pPoint->GetPoint()))
                    {
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSelfNumericalIntersection;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CPlane3D::AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const bool IntersectionA = IsOnPlane(m_Normal, m_HesseDistance, pPointPair->GetPointA());
                    const bool IntersectionB = IsOnPlane(m_Normal, m_HesseDistance, pPointPair->GetPointB());
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                            return eSelfNumericalIntersection;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSubspace;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CPlane3D::AnalyzeIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (AreUnitaryVectorsOrthogonal(m_Normal, pLine->GetDirection()))
                    {
                        if (IsOnPlane(m_Normal, m_HesseDistance, pLine->GetSupportPoint()))
                        {
                            SetGeometricPrimitiveType(pIntersectionTypeId, eLine);
                            return eSelfNumericalIntersection;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                        return eEmpty;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                    return eSubspace;
                }

                IntersectionType CPlane3D::AnalyzeIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const CVector3D& CircleCenter = pCircle->GetCenter();
                    const CVector3D& CircleNormal = pCircle->GetNormal();
                    if (AreUnitaryVectorsParallel(m_Normal, CircleNormal))
                    {
                        if (IsOnPlane(m_Normal, m_HesseDistance, CircleCenter))
                        {
                            SetGeometricPrimitiveType(pIntersectionTypeId, eCircle);
                            return eSelfNumericalIntersection;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                        return eEmpty;
                    }
                    CVector3D LineDirection, LineSupportPoint;
                    if (ComputeIntersectionPlaneAndPlane(m_Normal, m_HesseDistance, CircleNormal, pCircle->GetHesseDistance(), LineDirection, LineSupportPoint))
                    {
                        const CVector3D ClosestPoint = ComputeProjectionPointToLine(LineSupportPoint, LineDirection, CircleCenter);
                        if (ClosestPoint == CircleCenter)
                        {
                            SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                            return eSubspace;
                        }
                        CVector3D A, B;
                        bool SinglePointFlag = false;
                        if (ComputeIntersectionCircleAndLine(CircleNormal, CircleCenter, pCircle->GetRadius(), ClosestPoint, A, B, SinglePointFlag))
                        {
                            if (SinglePointFlag)
                            {
                                SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                                return eSubspace;
                            }
                            SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                            return eSubspace;
                        }
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CPlane3D::AnalyzeIntersection(const CPlane3D* pPlane, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (AreUnitaryVectorsParallel(m_Normal, pPlane->m_Normal))
                    {
                        if (IsOnPlane(m_Normal, m_HesseDistance, pPlane->m_Normal * pPlane->m_HesseDistance))
                        {
                            SetGeometricPrimitiveType(pIntersectionTypeId, ePlane);
                            return eSelfNumericalIntersection;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                        return eEmpty;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eLine);
                    return eSubspace;
                }

                CGeometricPrimitive3D* CPlane3D::GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (IsOnPlane(m_Normal, m_HesseDistance, pPoint->GetPoint()))
                    {
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eLine, eSelfNumericalIntersection);
                        return nullptr;
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CPlane3D::GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const bool IntersectionA = IsOnPlane(m_Normal, m_HesseDistance, pPointPair->GetPointA());
                    const bool IntersectionB = IsOnPlane(m_Normal, m_HesseDistance, pPointPair->GetPointB());
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePointPair, eSelfNumericalIntersection);
                            return nullptr;
                        }
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                        return new CPoint3D(IntersectionA ? pPointPair->GetPointA() : pPointPair->GetPointB(), this, pPointPair);
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CPlane3D::GenerateIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (AreUnitaryVectorsOrthogonal(m_Normal, pLine->GetDirection()))
                    {
                        if (IsOnPlane(m_Normal, m_HesseDistance, pLine->GetSupportPoint()))
                        {
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eLine, eSelfNumericalIntersection);
                            return nullptr;
                        }
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                        return nullptr;
                    }
                    CVector3D X;
                    if (ComputeIntersectionLineAndPlane(pLine->GetSupportPoint(), pLine->GetDirection(), m_Normal, m_Normal * m_HesseDistance, X))
                    {
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                        return new CPoint3D(X, this, pLine);
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CPlane3D::GenerateIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const CVector3D& CircleCenter = pCircle->GetCenter();
                    const CVector3D& CircleNormal = pCircle->GetNormal();
                    const real CircleRadius = pCircle->GetRadius();
                    if (AreUnitaryVectorsParallel(m_Normal, CircleNormal))
                    {
                        if (IsOnPlane(m_Normal, m_HesseDistance, CircleCenter))
                        {
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eCircle, eSelfNumericalIntersection);
                            return nullptr;
                        }
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                        return nullptr;
                    }
                    CVector3D LineDirection, LineSupportPoint;
                    if (ComputeIntersectionPlaneAndPlane(m_Normal, m_HesseDistance, CircleNormal, pCircle->GetHesseDistance(), LineDirection, LineSupportPoint))
                    {
                        const CVector3D ClosestPoint = ComputeProjectionPointToLine(LineSupportPoint, LineDirection, CircleCenter);
                        if (ClosestPoint == CircleCenter)
                        {
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace);
                            return new CPointPair3D(CircleCenter + LineDirection * CircleRadius, CircleCenter - LineDirection * CircleRadius, this, pCircle);
                        }
                        else
                        {
                            CVector3D A, B;
                            bool SinglePointFlag = false;
                            if (ComputeIntersectionCircleAndLine(CircleNormal, CircleCenter, pCircle->GetRadius(), ClosestPoint, A, B, SinglePointFlag))
                            {
                                if (SinglePointFlag)
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                                    return new CPoint3D(A, this, pCircle);
                                }
                                SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace);
                                return new CPointPair3D(A, B, this, pCircle);
                            }
                        }
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CPlane3D::GenerateIntersection(const CPlane3D* pPlane, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (AreUnitaryVectorsParallel(m_Normal, pPlane->m_Normal))
                    {
                        if (IsOnPlane(m_Normal, m_HesseDistance, pPlane->m_Normal * pPlane->m_HesseDistance))
                        {
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePlane, eSelfNumericalIntersection);
                            return nullptr;
                        }
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                        return nullptr;
                    }
                    CVector3D LineDirection, LineSupportPoint;
                    if (ComputeIntersectionPlaneAndPlane(m_Normal, m_HesseDistance, pPlane->m_Normal, pPlane->m_HesseDistance, LineDirection, LineSupportPoint))
                    {
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eLine, eSubspace);
                        return new CLine3D(LineDirection, LineSupportPoint, CLine3D::eDirectionVectorAndSupportPoint, this, pPlane);
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }
            }
        }
    }
}
