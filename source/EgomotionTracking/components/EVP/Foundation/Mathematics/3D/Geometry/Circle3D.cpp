/*
 * Circle3D.cpp
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "Point3D.h"
#include "PointPair3D.h"
#include "Line3D.h"
#include "Circle3D.h"
#include "Plane3D.h"
#include "Sphere3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CCircle3D::CCircle3D() :
                    CGeometricPrimitive3D(eCircle, nullptr, nullptr), m_Center(CVector3D::s_Point_At_Plus_Infinity_3D), m_Normal(CVector3D::s_Point_At_Origin_3D), m_Radius(_REAL_ZERO_)
                {
                }

                CCircle3D::CCircle3D(const real Cx, const real Cy, const real Cz, const real Nx, const real Ny, const real Nz, const real Radius, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(eCircle, pParentA, pParentB), m_Center(Cx, Cy, Cz), m_Normal(Nx, Ny, Nz), m_Radius(Radius)
                {
                    if (_1D::IsNonZero(m_Radius) && _1D::IsNotAtInfinity(m_Radius) && m_Normal.IsNonZeroLength() && m_Center.IsNotAtInfinity())
                    {
                        m_Normal.Normalize();
                        SetStatus(eDefined);
                    }
                }

                CCircle3D::CCircle3D(const CVector3D& Center, const CVector3D& Normal, const real Radius, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    CGeometricPrimitive3D(eCircle, pParentA, pParentB), m_Center(Center), m_Normal(Normal), m_Radius(Radius)
                {
                    if (_1D::IsNonZero(m_Radius) && _1D::IsNotAtInfinity(m_Radius) && m_Normal.IsNonZeroLength() && m_Center.IsNotAtInfinity())
                    {
                        m_Normal.Normalize();
                        SetStatus(eDefined);
                    }
                }

                CCircle3D::~CCircle3D()
                    = default;

                void CCircle3D::SetCenter(const CVector3D& Center)
                {
                    m_Center = Center;
                    SetStatus((m_Center.IsNotAtInfinity() && _1D::IsNonZero(m_Radius) && _1D::IsNotAtInfinity(m_Radius) && m_Normal.IsNonZeroLength()) ? eDefined : eUndefined);
                }

                const CVector3D& CCircle3D::GetCenter() const
                {
                    return m_Center;
                }

                void CCircle3D::SetNormal(const CVector3D& Normal)
                {
                    m_Normal = Normal;
                    if (m_Normal.IsNonZeroLength() && _1D::IsNonZero(m_Radius) && _1D::IsNotAtInfinity(m_Radius) && m_Center.IsNotAtInfinity())
                    {
                        m_Normal.Normalize();
                        SetStatus(eDefined);
                    }
                    else
                    {
                        SetStatus(eUndefined);
                    }
                }

                const CVector3D& CCircle3D::GetNormal() const
                {
                    return m_Normal;
                }

                void CCircle3D::SetRadius(const real Radius)
                {
                    m_Radius = Radius;
                    SetStatus((_1D::IsNonZero(m_Radius) && _1D::IsNotAtInfinity(m_Radius) && m_Normal.IsNonZeroLength() && m_Center.IsNotAtInfinity()) ? eDefined : eUndefined);
                }

                real CCircle3D::GetRadius() const
                {
                    return m_Radius;
                }

                real CCircle3D::GetHesseDistance() const
                {
                    return m_Normal.ScalarProduct(m_Center);
                }

                std::string CCircle3D::ToString() const
                {
                    std::ostringstream Content;
                    Content.precision(_REAL_EPSILON_DISPLAY_DIGITS_);
                    Content << "<Circle3D Id=\"" << m_InstanceId << "\" Status=\"" << GetStatusStringMode() << "\" Cx=\"" << m_Center.GetX() << "\" Cy=\"" << m_Center.GetY() << "\" Cz=\"" << m_Center.GetZ() << "\" Nx=\"" << m_Normal.GetX() << "\" Ny=\"" << m_Normal.GetY() << "\" Nz=\"" << m_Normal.GetZ() << "\" Radius=\"" << m_Radius << "\"";
                    AppendParentalInformation(Content);
                    Content << "/>";
                    return Content.str();
                }

                bool CCircle3D::operator==(const CGeometricPrimitive3D& GeometricPrimitive) const
                {
                    if (IsUndefined() || GeometricPrimitive.IsUndefined())
                    {
                        return false;
                    }
                    if (&GeometricPrimitive == this)
                    {
                        return true;
                    }
                    if (GeometricPrimitive.GetTypeId() == GetTypeId())
                    {
                        const CCircle3D* pCircle = dynamic_cast<const CCircle3D*>(&GeometricPrimitive);
                        return _1D::Equals(m_Radius, pCircle->m_Radius) && (m_Center == pCircle->m_Center) && (m_Normal == pCircle->m_Normal);
                    }
                    return false;
                }

                IntersectionType CCircle3D::AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case eCircle:
                                if (pGeometricPrimitive == this)
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                                    return eSelfInstanceIntersection;
                                }
                                return AnalyzeIntersection(dynamic_cast<const CCircle3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case ePointPair:
                                return AnalyzeIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case ePoint:
                                return AnalyzeIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case ePlane:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case eLine:
                                return AnalyzeIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case eNone:
                                break;
                        }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive3D* CCircle3D::GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eSphere:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case eCircle:
                                if (pGeometricPrimitive == this)
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eCircle, eSelfInstanceIntersection);
                                    return nullptr;
                                }
                                return GenerateIntersection(dynamic_cast<const CCircle3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePointPair:
                                return GenerateIntersection(dynamic_cast<const CPointPair3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePoint:
                                return GenerateIntersection(dynamic_cast<const CPoint3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePlane:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case eLine:
                                return GenerateIntersection(dynamic_cast<const CLine3D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case eNone:
                                break;
                        }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                IntersectionType CCircle3D::AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (IsOnSphere(m_Center, m_Radius, pPoint->GetPoint()) && IsOnPlane(m_Normal, m_Center, pPoint->GetPoint()))
                    {
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSelfNumericalIntersection;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CCircle3D::AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const bool IntersectionA = IsOnSphere(m_Center, m_Radius, pPointPair->GetPointA()) && IsOnPlane(m_Normal, m_Center, pPointPair->GetPointA());
                    const bool IntersectionB = IsOnSphere(m_Center, m_Radius, pPointPair->GetPointB()) && IsOnPlane(m_Normal, m_Center, pPointPair->GetPointB());
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                            return eSelfNumericalIntersection;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSubspace;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CCircle3D::AnalyzeIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    const CVector3D& LineSupportPoint = pLine->GetSupportPoint();
                    const CVector3D& LineDirection = pLine->GetDirection();
                    if (AreUnitaryVectorsOrthogonal(m_Normal, LineDirection))
                    {
                        if (IsOnPlane(m_Normal, m_Center, LineSupportPoint))
                        {
                            const CVector3D ClosestPoint = ComputeProjectionPointToLine(LineSupportPoint, LineDirection, m_Center);
                            if (ClosestPoint == m_Center)
                            {
                                SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                                return eSubspace;
                            }
                            CVector3D A, B;
                            bool SinglePointFlag = false;
                            if (ComputeIntersectionCircleAndLine(m_Normal, m_Center, m_Radius, ClosestPoint, A, B, SinglePointFlag))
                            {
                                if (SinglePointFlag)
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                                    return eSubspace;
                                }
                                SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                                return eSubspace;
                            }
                            SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                            return eEmpty;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                        return eEmpty;
                    }
                    CVector3D X;
                    if (ComputeIntersectionLineAndPlane(LineSupportPoint, LineDirection, m_Normal, m_Center, X) && IsOnSphere(m_Center, m_Radius, X))
                    {
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSubspace;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CCircle3D::AnalyzeIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId) const
                {
                    if (AreUnitaryVectorsParallel(m_Normal, pCircle->m_Normal))
                    {
                        if (IsOnPlane(m_Normal, m_Center, pCircle->m_Center))
                        {
                            if (m_Center == pCircle->m_Center)
                            {
                                if (_1D::Equals(m_Radius, pCircle->m_Radius))
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, eCircle);
                                    return eSelfNumericalIntersection;
                                }
                                SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                                return eEmpty;
                            }
                            const real CentersDistance = m_Center.GetDistance(pCircle->m_Center);
                            bool SinglePointFlag = false;
                            if (IsIntersectingConfiguration(TMax(m_Radius, pCircle->m_Radius), TMin(m_Radius, pCircle->m_Radius), CentersDistance, SinglePointFlag))
                            {
                                if (SinglePointFlag)
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                                    return eSubspace;
                                }
                                SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                                return eSubspace;
                            }
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                        return eEmpty;
                    }
                    CVector3D LineDirection, LineSupportPoint;
                    if (ComputeIntersectionPlaneAndPlane(m_Normal, GetHesseDistance(), pCircle->m_Normal, pCircle->GetHesseDistance(), LineDirection, LineSupportPoint))
                    {
                        CVector3D A, B;
                        bool SinglePointFlag = false;
                        const CVector3D ClosestPoint = ComputeProjectionPointToLine(LineSupportPoint, LineDirection, m_Center);
                        if (ComputeIntersectionCircleAndLine(m_Normal, m_Center, m_Radius, ClosestPoint, A, B, SinglePointFlag))
                        {
                            if (SinglePointFlag)
                            {
                                if (IsOnSphere(pCircle->m_Center, pCircle->m_Radius, A))
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                                    return eSubspace;
                                }
                                SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                                return eEmpty;
                            }
                            const bool IntersectionA = IsOnSphere(pCircle->m_Center, pCircle->m_Radius, A);
                            const bool IntersectionB = IsOnSphere(pCircle->m_Center, pCircle->m_Radius, B);
                            if (IntersectionA || IntersectionB)
                            {
                                if (IntersectionA && IntersectionB)
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, ePointPair);
                                    return eSubspace;
                                }
                                SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                                return eSubspace;
                            }
                        }
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive3D* CCircle3D::GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (IsOnSphere(m_Center, m_Radius, pPoint->GetPoint()) && IsOnPlane(m_Normal, m_Center, pPoint->GetPoint()))
                    {
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSelfNumericalIntersection);
                        return nullptr;
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CCircle3D::GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const bool IntersectionA = IsOnSphere(m_Center, m_Radius, pPointPair->GetPointA()) && IsOnPlane(m_Normal, m_Center, pPointPair->GetPointA());
                    const bool IntersectionB = IsOnSphere(m_Center, m_Radius, pPointPair->GetPointB()) && IsOnPlane(m_Normal, m_Center, pPointPair->GetPointB());
                    if (IntersectionA || IntersectionB)
                    {
                        if (IntersectionA && IntersectionB)
                        {
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePointPair, eSelfNumericalIntersection);
                            return nullptr;
                        }
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                        return new CPoint3D(IntersectionA ? pPointPair->GetPointA() : pPointPair->GetPointB(), this, pPointPair);
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CCircle3D::GenerateIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    const CVector3D& LineSupportPoint = pLine->GetSupportPoint();
                    const CVector3D& LineDirection = pLine->GetDirection();
                    if (AreUnitaryVectorsOrthogonal(m_Normal, LineDirection))
                    {
                        if (IsOnPlane(m_Normal, m_Center, LineSupportPoint))
                        {
                            const CVector3D ClosestPoint = ComputeProjectionPointToLine(LineSupportPoint, LineDirection, m_Center);
                            if (ClosestPoint == m_Center)
                            {
                                SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace);
                                return new CPointPair3D(m_Center + LineDirection * m_Radius, m_Center - LineDirection * m_Radius, this, pLine);
                            }
                            CVector3D A, B;
                            bool SinglePointFlag = false;
                            if (ComputeIntersectionCircleAndLine(m_Normal, m_Center, m_Radius, ClosestPoint, A, B, SinglePointFlag))
                            {
                                if (SinglePointFlag)
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                                    return new CPoint3D(A, this, pLine);
                                }
                                SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace);
                                return new CPointPair3D(A, B, this, pLine);
                            }
                        }
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                        return nullptr;
                    }
                    CVector3D X;
                    if (ComputeIntersectionLineAndPlane(LineSupportPoint, LineDirection, m_Normal, m_Center, X) && IsOnSphere(m_Center, m_Radius, X))
                    {
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                        return new CPoint3D(X, this, pLine);
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive3D* CCircle3D::GenerateIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (AreUnitaryVectorsParallel(m_Normal, pCircle->m_Normal))
                    {
                        if (IsOnPlane(m_Normal, m_Center, pCircle->m_Center))
                        {
                            if (m_Center == pCircle->m_Center)
                            {
                                if (_1D::Equals(m_Radius, pCircle->m_Radius))
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eCircle, eSelfNumericalIntersection);
                                    return nullptr;
                                }
                                SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                                return nullptr;
                            }
                            const real CentersDistance = m_Center.GetDistance(pCircle->m_Center);
                            bool SinglePointFlag = false;
                            if (IsIntersectingConfiguration(TMax(m_Radius, pCircle->m_Radius), TMin(m_Radius, pCircle->m_Radius), CentersDistance, SinglePointFlag))
                            {
                                if (SinglePointFlag)
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                                    return new CPoint3D(m_Center + ComputeUnitaryDirectionVector(m_Center, pCircle->m_Center) * m_Radius, this, pCircle);
                                }
                                real Dx = _REAL_ZERO_, Dy = _REAL_ZERO_;
                                if (CirclesIntersection(TMax(m_Radius, pCircle->m_Radius), TMin(m_Radius, pCircle->m_Radius), CentersDistance, Dx, Dy, SinglePointFlag))
                                {
                                    const CVector3D CenterBase = (m_Radius > pCircle->m_Radius) ? m_Center : pCircle->m_Center;
                                    const CVector3D CenterDirection = (m_Radius > pCircle->m_Radius) ? pCircle->m_Center : m_Center;
                                    const CVector3D Delta = ComputeUnitaryDirectionVector(CenterBase, CenterDirection) * Dx;
                                    CVector3D Beta;
                                    if (ComputeUnitaryOrthogonalVector(Delta, m_Normal, Beta))
                                    {
                                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace);
                                        Beta *= Dy;
                                        return new CPointPair3D(CenterBase + Delta + Beta, CenterBase + Delta - Beta, this, pCircle);
                                    }
                                }
                            }
                        }
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                        return nullptr;
                    }
                    CVector3D LineDirection, LineSupportPoint;
                    if (ComputeIntersectionPlaneAndPlane(m_Normal, GetHesseDistance(), pCircle->m_Normal, pCircle->GetHesseDistance(), LineDirection, LineSupportPoint))
                    {
                        const CVector3D ClosestPoint = ComputeProjectionPointToLine(LineSupportPoint, LineDirection, m_Center);
                        if (ClosestPoint == m_Center)
                        {
                            const CVector3D A = m_Center + LineDirection * m_Radius;
                            const CVector3D B = m_Center - LineDirection * m_Radius;
                            const bool IntersectionA = IsOnSphere(pCircle->m_Center, pCircle->m_Radius, A);
                            const bool IntersectionB = IsOnSphere(pCircle->m_Center, pCircle->m_Radius, B);
                            if (IntersectionA || IntersectionB)
                            {
                                if (IntersectionA && IntersectionB)
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace);
                                    return new CPointPair3D(A, B, this, pCircle);
                                }
                                SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                                return new CPoint3D(IntersectionA ? A : B, this, pCircle);
                            }
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                            return nullptr;
                        }
                        CVector3D A, B;
                        bool SinglePointFlag = false;
                        if (ComputeIntersectionCircleAndLine(m_Normal, m_Center, m_Radius, ClosestPoint, A, B, SinglePointFlag))
                        {
                            if (SinglePointFlag)
                            {
                                if (IsOnSphere(pCircle->m_Center, pCircle->m_Radius, A))
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                                    return new CPoint3D(A, this, pCircle);
                                }
                                SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                                return nullptr;
                            }
                            const bool IntersectionA = IsOnSphere(pCircle->m_Center, pCircle->m_Radius, A);
                            const bool IntersectionB = IsOnSphere(pCircle->m_Center, pCircle->m_Radius, B);
                            if (IntersectionA || IntersectionB)
                            {
                                if (IntersectionA && IntersectionB)
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePointPair, eSubspace);
                                    return new CPointPair3D(A, B, this, pCircle);
                                }
                                SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                                return new CPoint3D(IntersectionA ? A : B, this, pCircle);
                            }
                        }
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }
            }
        }
    }
}
