/*
 * GeometricPrimitive3D.cpp
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "GeometricPrimitive3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                INSTANCE_COUNTING_INITIALIZATION(CGeometricPrimitive3D)

                IDENTIFIABLE_INITIALIZATION(CGeometricPrimitive3D)

                CGeometricPrimitive3D::CGeometricPrimitive3D(const GeometricPrimitive3DTypeId TypeId, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB) :
                    IDENTIFIABLE_CONTRUCTOR(CGeometricPrimitive3D), m_TypeId(TypeId), m_Status(eUndefined), m_pParentA(nullptr), m_pParentB(nullptr), m_pDataPointer(nullptr)
                {
                    INSTANCE_COUNTING_CONTRUCTOR(CGeometricPrimitive3D)

                    if (pParentA && pParentB)
                    {
                        if (pParentA->GetTypeId() < pParentB->GetTypeId())
                        {
                            m_pParentA = pParentA;
                            m_pParentB = pParentB;
                        }
                        else if (pParentA->GetTypeId() == pParentB->GetTypeId())
                        {
                            if (pParentA->GetInstanceId() > pParentB->GetInstanceId())
                            {
                                m_pParentA = pParentA;
                                m_pParentB = pParentB;
                            }
                            else
                            {
                                m_pParentA = pParentB;
                                m_pParentB = pParentA;
                            }
                        }
                        else
                        {
                            m_pParentA = pParentB;
                            m_pParentB = pParentA;
                        }
                    }
                }

                CGeometricPrimitive3D::~CGeometricPrimitive3D()
                {
                    INSTANCE_COUNTING_DESTRUCTOR(CGeometricPrimitive3D)
                }

                CGeometricPrimitive3D::GeometricPrimitive3DTypeId CGeometricPrimitive3D::GetTypeId() const
                {
                    return m_TypeId;
                }

                void CGeometricPrimitive3D::SetStatus(const Status CurrentStatus)
                {
                    m_Status = CurrentStatus;
                }

                CGeometricPrimitive3D::Status CGeometricPrimitive3D::GetStatus() const
                {
                    return m_Status;
                }

                bool CGeometricPrimitive3D::IsDefined() const
                {
                    return (m_Status == eDefined);
                }

                bool CGeometricPrimitive3D::IsUndefined() const
                {
                    return (m_Status == eUndefined);
                }

                std::string CGeometricPrimitive3D::GetStatusStringMode() const
                {
                    return std::string((m_Status == eDefined) ? "Defined" : "Undefined");
                }

                void CGeometricPrimitive3D::SetDataPointer(const void* pDataPointer)
                {
                    m_pDataPointer = pDataPointer;
                }

                const void* CGeometricPrimitive3D::GetDataPointer() const
                {
                    return m_pDataPointer;
                }

                bool CGeometricPrimitive3D::HasDataPointer() const
                {
                    return m_pDataPointer;
                }

                bool CGeometricPrimitive3D::HasParents() const
                {
                    return (m_pParentA && m_pParentB);
                }

                const CGeometricPrimitive3D* CGeometricPrimitive3D::GetParentA() const
                {
                    return m_pParentA;
                }

                const CGeometricPrimitive3D* CGeometricPrimitive3D::GetParentB() const
                {
                    return m_pParentB;
                }

                bool CGeometricPrimitive3D::IsAncestor(const CGeometricPrimitive3D* pGeometricPrimitive, const bool FullSearch) const
                {
                    if (pGeometricPrimitive && m_pParentA && m_pParentB)
                    {
                        if ((m_pParentA == pGeometricPrimitive) || (m_pParentB == pGeometricPrimitive))
                        {
                            return true;
                        }
                        else if (FullSearch)
                        {
                            const CGeometricPrimitive3D* pAncestor = nullptr;
                            list<const CGeometricPrimitive3D*> AncestorsSearchList;
                            AncestorsSearchList.push_back(m_pParentA);
                            AncestorsSearchList.push_back(m_pParentB);
#ifdef _USE_INSTANCE_COUNTING_

                            while (AncestorsSearchList.size() && (AncestorsSearchList.size() < CGeometricPrimitive3D::s_TotalExistingInstances))

#else

                            while (AncestorsSearchList.size())

#endif
                            {
                                pAncestor = AncestorsSearchList.front();
                                AncestorsSearchList.pop_front();
                                if (pAncestor == pGeometricPrimitive)
                                {
                                    return true;
                                }
                                if (pAncestor->m_pParentA && pAncestor->m_pParentB)
                                {
                                    AncestorsSearchList.push_back(pAncestor->m_pParentA);
                                    AncestorsSearchList.push_back(pAncestor->m_pParentB);
                                }
                            }
                        }
                    }
                    return false;
                }

                void CGeometricPrimitive3D::AppendParentalInformation(ostream& Content) const
                {
                    if (m_pParentA && m_pParentB)
                    {
                        Content << " ParentA=\"" << m_pParentA->GetInstanceId() << "\" ParentB=\"" << m_pParentB->GetInstanceId() << "\"";
                    }
                }

                void CGeometricPrimitive3D::SetGeometricPrimitiveType(GeometricPrimitive3DTypeId* pGeometricPrimitiveTypeId, const GeometricPrimitive3DTypeId TypeId)
                {
                    if (pGeometricPrimitiveTypeId)
                    {
                        *pGeometricPrimitiveTypeId = TypeId;
                    }
                }

                void CGeometricPrimitive3D::SetIntersectionType(IntersectionType* pIntersectionType, const IntersectionType TypeId)
                {
                    if (pIntersectionType)
                    {
                        *pIntersectionType = TypeId;
                    }
                }

                void CGeometricPrimitive3D::SetIntersectionInformation(GeometricPrimitive3DTypeId* pGeometricPrimitiveTypeId, IntersectionType* pIntersectionType, const GeometricPrimitive3DTypeId TypeId, const IntersectionType IntersectionTypeId)
                {
                    if (pGeometricPrimitiveTypeId)
                    {
                        *pGeometricPrimitiveTypeId = TypeId;
                    }
                    if (pIntersectionType)
                    {
                        *pIntersectionType = IntersectionTypeId;
                    }
                }
            }
        }
    }
}
