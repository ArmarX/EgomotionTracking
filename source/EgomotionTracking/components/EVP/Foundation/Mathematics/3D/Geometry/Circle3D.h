/*
 * Circle3D.h
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

#include "GeometricPrimitive3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                class CPoint3D;
                class CPointPair3D;
                class CLine3D;

                class CCircle3D: public CGeometricPrimitive3D
                {
                public:

                    CCircle3D();
                    CCircle3D(const real Cx, const real Cy, const real Cz, const real Nx, const real Ny, const real Nz, const real Radius, const CGeometricPrimitive3D* pParentA = NULL, const CGeometricPrimitive3D* pParentB = NULL);
                    CCircle3D(const CVector3D& Center, const CVector3D& Normal, const real Radius, const CGeometricPrimitive3D* pParentA = NULL, const CGeometricPrimitive3D* pParentB = NULL);
                    ~CCircle3D() override;

                    void SetCenter(const CVector3D& Center);
                    const CVector3D& GetCenter() const;

                    void SetNormal(const CVector3D& Normal);
                    const CVector3D& GetNormal() const;

                    void SetRadius(const real Radius);
                    real GetRadius() const;
                    real GetHesseDistance() const;

                    std::string ToString() const override;
                    bool operator==(const CGeometricPrimitive3D& GeometricPrimitive) const override;
                    IntersectionType AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId = NULL) const override ;
                    CGeometricPrimitive3D* GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId = NULL, IntersectionType* pIntersectionType = NULL) const override;

                protected:

                    IntersectionType AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;

                    CVector3D m_Center;
                    CVector3D m_Normal;
                    real m_Radius;
                };
            }
        }
    }
}
