/*
 * Line3D.h
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

#include "GeometricPrimitive3D.h"
#include "LineSourcePoints.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                class CPoint3D;
                class CPointPair3D;

                class CLine3D: public CGeometricPrimitive3D
                {
                public:

                    enum ArgumentMode
                    {
                        eDirectionVectorAndSupportPoint, ePointAPointB
                    };

                    CLine3D();
                    CLine3D(const real Xa, const real Ya, const real Za, const real Xb, const real Yb, const real Zb, const ArgumentMode Mode, const CGeometricPrimitive3D* pParentA = NULL, const CGeometricPrimitive3D* pParentB = NULL);
                    CLine3D(const CVector3D& A, const CVector3D& B, const ArgumentMode Mode, const CGeometricPrimitive3D* pParentA = NULL, const CGeometricPrimitive3D* pParentB = NULL);
                    ~CLine3D() override;

                    void SetLineByPoints(const CVector3D& A, const CVector3D& B);
                    bool HasSourcePoints() const;
                    bool GetSourcePoints(CVector3D& A, CVector3D& B) const;

                    void SetDirection(const CVector3D& Direction);
                    const CVector3D& GetDirection() const;

                    void SetSupportPoint(const CVector3D& SupportPoint);
                    const CVector3D& GetSupportPoint() const;

                    CVector3D GetPointAtUnitaryDisplacement() const;
                    CVector3D GetPointAtDisplacement(const real Displacement) const;

                    CVector3D GetClosestPoint(const CVector3D& X) const;
                    bool GetClosestPoints(const CLine3D* pLine, CVector3D& A, CVector3D& B) const;

                    std::string ToString() const override;
                    bool operator==(const CGeometricPrimitive3D& GeometricPrimitive) const override;
                    IntersectionType AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId = NULL) const override;
                    CGeometricPrimitive3D* GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId = NULL, IntersectionType* pIntersectionType = NULL) const override;

                protected:

                    IntersectionType AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;

                    CVector3D m_Direction;
                    CVector3D m_SupportPoint;
                    CLineSourcePoints* m_pSourcePoints;
                };
            }
        }
    }
}

