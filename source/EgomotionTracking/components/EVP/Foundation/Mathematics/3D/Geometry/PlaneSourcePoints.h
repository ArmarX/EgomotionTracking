/*
 * PlaneSourcePoints.h
 *
 *  Created on: Apr 9, 2012
 *      Author: david
 */

#pragma once

#include "../Vector3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                class CPlaneSourcePoints
                {
                public:

                    CPlaneSourcePoints(const CVector3D& A, const CVector3D& B, const CVector3D& C);
                    ~CPlaneSourcePoints();

                    void SetPoints(const CVector3D& A, const CVector3D& B, const CVector3D& C);
                    void GetPoints(CVector3D& A, CVector3D& B, CVector3D& C) const;

                    const CVector3D& GetPointA() const;
                    const CVector3D& GetPointB() const;
                    const CVector3D& GetPointC() const;

                protected:

                    CVector3D m_A;
                    CVector3D m_B;
                    CVector3D m_C;
                };
            }
        }
    }
}

