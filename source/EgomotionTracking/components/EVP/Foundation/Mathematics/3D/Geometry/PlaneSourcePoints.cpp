/*
 * PlaneSourcePoints.cpp
 *
 *  Created on: Apr 9, 2012
 *      Author: david
 */

#include "PlaneSourcePoints.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                CPlaneSourcePoints::CPlaneSourcePoints(const CVector3D& A, const CVector3D& B, const CVector3D& C) :
                    m_A(A), m_B(B), m_C(C)
                {
                }

                CPlaneSourcePoints::~CPlaneSourcePoints()
                    = default;

                void CPlaneSourcePoints::SetPoints(const CVector3D& A, const CVector3D& B, const CVector3D& C)
                {
                    m_A = A;
                    m_B = B;
                    m_C = C;
                }

                void CPlaneSourcePoints::GetPoints(CVector3D& A, CVector3D& B, CVector3D& C) const
                {
                    A = m_A;
                    B = m_B;
                    C = m_C;
                }

                const CVector3D& CPlaneSourcePoints::GetPointA() const
                {
                    return m_A;
                }

                const CVector3D& CPlaneSourcePoints::GetPointB() const
                {
                    return m_B;
                }

                const CVector3D& CPlaneSourcePoints::GetPointC() const
                {
                    return m_C;
                }
            }
        }
    }
}
