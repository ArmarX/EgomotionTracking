/*
 * GeometricPrimitive3D.h
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

#include "../../../Miscellaneous/IdentifiableInstance.h"
#include "../../1D/Common1D.h"
#include "Common3D.h"

#ifdef _USE_DEPENDECY_MECHANISM_
#include "../../../Miscellaneous/DependencyManager.h"
#endif

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                enum IntersectionType
                {
                    eEmpty = 0, eSubspace = 1, eSelfNumericalIntersection = 2, eSelfInstanceIntersection = 4
                };

#ifdef _USE_DEPENDECY_MECHANISM_

                class CGeometricPrimitive3D: public CIdentifiableInstance, public IDependencyManager::IDependencyManager

#else

                class CGeometricPrimitive3D: public CIdentifiableInstance

#endif
                {
                    IDENTIFIABLE

                    INSTANCE_COUNTING

                public:

                    enum Status
                    {
                        eUndefined = 0, eDefined
                    };

                    enum GeometricPrimitive3DTypeId
                    {
                        eNone = 0, ePoint, ePointPair, eLine, eCircle, ePlane, eSphere
                    };

                    CGeometricPrimitive3D(const GeometricPrimitive3DTypeId TypeId, const CGeometricPrimitive3D* pParentA, const CGeometricPrimitive3D* pParentB);
                    ~CGeometricPrimitive3D() override;

                    GeometricPrimitive3DTypeId GetTypeId() const;

                    void SetStatus(const Status CurrentStatus);
                    Status GetStatus() const;
                    bool IsDefined() const;
                    bool IsUndefined() const;
                    std::string GetStatusStringMode() const;

                    void SetDataPointer(const void* pDataPointer);
                    const void* GetDataPointer() const;
                    bool HasDataPointer() const;

                    bool HasParents() const;
                    const CGeometricPrimitive3D* GetParentA() const;
                    const CGeometricPrimitive3D* GetParentB() const;
                    bool IsAncestor(const CGeometricPrimitive3D* pGeometricPrimitive, const bool FullSearch) const;

                    virtual  std::string ToString() const = 0;
                    virtual  bool operator==(const CGeometricPrimitive3D& GeometricPrimitive) const = 0;
                    virtual  IntersectionType AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId) const = 0;
                    virtual CGeometricPrimitive3D* GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const = 0;

                protected:

                    void AppendParentalInformation(ostream& Content) const;
                    static void SetGeometricPrimitiveType(GeometricPrimitive3DTypeId* pGeometricPrimitiveTypeId, const GeometricPrimitive3DTypeId TypeId);
                    static void SetIntersectionType(IntersectionType* pIntersectionType, const IntersectionType TypeId);
                    static void SetIntersectionInformation(GeometricPrimitive3DTypeId* pGeometricPrimitiveTypeId, IntersectionType* pIntersectionType, const GeometricPrimitive3DTypeId TypeId, const IntersectionType IntersectionTypeId);

                private:

                    const GeometricPrimitive3DTypeId m_TypeId;
                    Status m_Status;
                    const CGeometricPrimitive3D* m_pParentA;
                    const CGeometricPrimitive3D* m_pParentB;
                    const void* m_pDataPointer;
                };
            }

            inline Geometry::CGeometricPrimitive3D* operator^(const Geometry::CGeometricPrimitive3D& A, const Geometry::CGeometricPrimitive3D& B)
            {
                return A.GenerateIntersection(&B, NULL, NULL);
            }

            inline  Geometry::IntersectionType operator&(const Geometry::CGeometricPrimitive3D& A, const Geometry::CGeometricPrimitive3D& B)
            {
                return A.AnalyzeIntersection(&B, NULL);
            }
        }
    }
}

