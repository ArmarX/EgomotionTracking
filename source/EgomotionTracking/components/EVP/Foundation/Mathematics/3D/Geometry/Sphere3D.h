/*
 * Sphere3D.h
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

#include "GeometricPrimitive3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Geometry
            {
                class CPoint3D;
                class CPointPair3D;
                class CLine3D;
                class CCircle3D;
                class CPlane3D;

                class CSphere3D: public CGeometricPrimitive3D
                {
                public:

                    CSphere3D();
                    CSphere3D(const real Cx, const real Cy, const real Cz, const real Radius, const CGeometricPrimitive3D* pParentA = NULL, const CGeometricPrimitive3D* pParentB = NULL);
                    CSphere3D(const CVector3D& Center, const real Radius, const CGeometricPrimitive3D* pParentA = NULL, const CGeometricPrimitive3D* pParentB = NULL);
                    ~CSphere3D() override;

                    void SetCenter(const CVector3D& Center);
                    const CVector3D& GetCenter() const;

                    void SetRadius(const real Radius);
                    real GetRadius() const;

                    bool IsOnSurface(const CVector3D& X) const;
                    bool IsInside(const CVector3D& X) const;
                    real GetDistanceToSurface(const CVector3D& X) const;
                    real GetDistanceToCenter(const CVector3D& X) const;
                    CVector3D GetClosestPointOnSurface(const CVector3D& X) const;

                    string ToString() const override;
                    bool operator==(const CGeometricPrimitive3D& GeometricPrimitive) const override;
                    IntersectionType AnalyzeIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId = NULL) const override;
                    CGeometricPrimitive3D* GenerateIntersection(const CGeometricPrimitive3D* pGeometricPrimitive, GeometricPrimitive3DTypeId* pIntersectionTypeId = NULL, IntersectionType* pIntersectionType = NULL) const override;

                protected:

                    IntersectionType AnalyzeIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CPlane3D* pPlane, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CSphere3D* pSphere, GeometricPrimitive3DTypeId* pIntersectionTypeId) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPoint3D* pPoint, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPointPair3D* pPointPair, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CLine3D* pLine, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CCircle3D* pCircle, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CPlane3D* pPlane, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive3D* GenerateIntersection(const CSphere3D* pSphere, GeometricPrimitive3DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;

                    CVector3D m_Center;
                    real m_Radius;
                };
            }
        }
    }
}

