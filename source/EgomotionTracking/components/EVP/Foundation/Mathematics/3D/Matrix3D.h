/*
 * Matrix3D.h
 *
 *  Created on: 06.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"
#include "Vector3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            class CMatrix3D
            {
            public:

                CMatrix3D();
                CMatrix3D(const real E00, const real E01, const real E02, const real E10, const real E11, const real E12, const real E20, const real E21, const real E22);
                CMatrix3D(const CMatrix3D& Matrix);
                CMatrix3D(const CMatrix3D* pMatrix);
                CMatrix3D(const CVector3D& C0, const CVector3D& C1, const CVector3D& C2);
                CMatrix3D(const real Radians, const CVector3D& Axis);
                CMatrix3D(const real qw, const real qx, const real qy, const real qz);
                virtual ~CMatrix3D();

                void MakeIdentity();
                real GetDeterminat() const;
                CMatrix3D GetInverse(bool* pSingularity = NULL) const;
                CMatrix3D GetTranspose() const;
                void SetByElements(const real E00, const real E01, const real E02, const real E10, const real E11, const real E12, const real E20, const real E21, const real E22);
                void SetByRow(const real* pMatrix);
                void SetByQuaternion(const real Angle, const CVector3D& Axis);
                void SetByQuaternion(const real qw, const real qx, const real qy, const real qz);

                real* operator[](const uint Row);
                const real* operator[](const uint Row) const;
                CMatrix3D& operator=(const CMatrix3D& Matrix);
                CMatrix3D& operator*=(const CMatrix3D& Matrix);
                CMatrix3D operator*(const CMatrix3D& Matrix);
                CVector3D operator*(const CVector3D& Vector);
                CMatrix3D operator*(const real Scalar);
                bool operator==(const CMatrix3D& Matrix);
                bool operator!=(const CMatrix3D& Matrix);
                string ToString(const uint Precision) const;

            protected:

                union MatrixStructure
                {
                    real m_Array[9];
                    real m_Elements[3][3];
                };
                MatrixStructure m_InnerMatrixStructure;
            };
        }
    }
}

