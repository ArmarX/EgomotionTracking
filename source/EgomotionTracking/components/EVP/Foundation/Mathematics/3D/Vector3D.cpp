/*
 * Vector.cpp
 *
 *  Created on: 21.09.2011
 *      Author: gonzalez
 */
#include "Vector3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            const CVector3D CVector3D::s_Point_At_Origin_3D(_REAL_ZERO_, _REAL_ZERO_, _REAL_ZERO_);
            const CVector3D CVector3D::s_Point_At_Minus_Infinity_3D(-_REAL_MAX_, -_REAL_MAX_, -_REAL_MAX_);
            const CVector3D CVector3D::s_Point_At_Plus_Infinity_3D(_REAL_MAX_, _REAL_MAX_, _REAL_MAX_);
            const CVector3D CVector3D::s_Direction_Axis_X_3D(_REAL_ONE_, _REAL_ZERO_, _REAL_ZERO_);
            const CVector3D CVector3D::s_Direction_Axis_Y_3D(_REAL_ZERO_, _REAL_ONE_, _REAL_ZERO_);
            const CVector3D CVector3D::s_Direction_Axis_Z_3D(_REAL_ZERO_, _REAL_ZERO_, _REAL_ONE_);

            CVector3D CVector3D::CreateLinealCombination(const CVector3D& A, const CVector3D& B, const real SA, const real SB)
            {
                return CVector3D(A.m_Elements[0] * SA + B.m_Elements[0] * SB, A.m_Elements[1] * SA + B.m_Elements[1] * SB, A.m_Elements[2] * SA + B.m_Elements[2] * SB);
            }

            CVector3D CVector3D::CreateComplementaryLinealCombination(const CVector3D& A, const CVector3D& B, const real SA)
            {
                const real SB = _REAL_ONE_ - SA;
                return CVector3D(A.m_Elements[0] * SA + B.m_Elements[0] * SB, A.m_Elements[1] * SA + B.m_Elements[1] * SB, A.m_Elements[2] * SA + B.m_Elements[2] * SB);
            }

            CVector3D CVector3D::CreateMidPoint(const CVector3D& A, const CVector3D& B)
            {
                return CVector3D((A.m_Elements[0] + B.m_Elements[0]) * _REAL_HALF_, (A.m_Elements[1] + B.m_Elements[1]) * _REAL_HALF_, (A.m_Elements[2] + B.m_Elements[2]) * _REAL_HALF_);
            }

            CVector3D CVector3D::CreateCannonicalForm(const real X, const real Y, const real Z)
            {
                const real Elements[] = { X, Y, Z };
                real Maximal = _REAL_EPSILON_;
                int Index = -1;
                for (uint i = 0; i < 3; ++i)
                {
                    const real Magnitud = RealAbs(Elements[i]);
                    if (Magnitud > Maximal)
                    {
                        Index = i;
                        Maximal = Magnitud;
                    }
                }
                if (Index >= 0)
                {
                    if (Elements[Index] < _REAL_ZERO_)
                    {
                        return CVector3D(-X, -Y, -Z);
                    }
                    return CVector3D(X, Y, Z);
                }
                return CVector3D();
            }

            CVector3D::CVector3D()
            {
                memset(m_Elements, 0, sizeof(real) * 3);
            }

            CVector3D::CVector3D(const real X, const real Y, const real Z)
            {
                m_Elements[0] = X;
                m_Elements[1] = Y;
                m_Elements[2] = Z;
            }

            CVector3D::CVector3D(const CVector3D& Vector)
            {
                memcpy(m_Elements, Vector.m_Elements, sizeof(real) * 3);
            }

            bool CVector3D::SetCannonicalForm()
            {
                real Maximal = _REAL_EPSILON_;
                int Index = -1;
                for (uint i = 0; i < 3; ++i)
                {
                    const real Magnitud = RealAbs(m_Elements[i]);
                    if (Magnitud > Maximal)
                    {
                        Index = i;
                        Maximal = Magnitud;
                    }
                }
                if (Index >= 0)
                {
                    if (m_Elements[Index] < _REAL_ZERO_)
                    {
                        Negate();
                    }
                    return true;
                }
                return false;
            }

            void CVector3D::SetX(const real X)
            {
                m_Elements[0] = X;
            }

            void CVector3D::SetY(const real Y)
            {
                m_Elements[1] = Y;
            }

            void CVector3D::SetZ(const real Z)
            {
                m_Elements[2] = Z;
            }

            void CVector3D::SetValue(const real X, const real Y, const real Z)
            {
                m_Elements[0] = X;
                m_Elements[1] = Y;
                m_Elements[2] = Z;
            }

            void CVector3D::SetMaximalComponentValue(const real X, const real Y, const real Z)
            {
                m_Elements[0] = TMax(m_Elements[0], X);
                m_Elements[1] = TMax(m_Elements[1], Y);
                m_Elements[2] = TMax(m_Elements[2], Z);
            }

            void CVector3D::SetMinimalComponentValue(const real X, const real Y, const real Z)
            {
                m_Elements[0] = TMin(m_Elements[0], X);
                m_Elements[1] = TMin(m_Elements[1], Y);
                m_Elements[2] = TMin(m_Elements[2], Z);
            }

            void CVector3D::SetMaximalComponentValue(const CVector3D& Vector)
            {
                m_Elements[0] = TMax(m_Elements[0], Vector.m_Elements[0]);
                m_Elements[1] = TMax(m_Elements[1], Vector.m_Elements[1]);
                m_Elements[2] = TMax(m_Elements[2], Vector.m_Elements[2]);
            }

            void CVector3D::SetMinimalComponentValue(const CVector3D& Vector)
            {
                m_Elements[0] = TMin(m_Elements[0], Vector.m_Elements[0]);
                m_Elements[1] = TMin(m_Elements[1], Vector.m_Elements[1]);
                m_Elements[2] = TMin(m_Elements[2], Vector.m_Elements[2]);
            }

            void CVector3D::SetMidPoint(const CVector3D& A, const CVector3D& B)
            {
                m_Elements[0] = (A.m_Elements[0] + B.m_Elements[0]) * _REAL_HALF_;
                m_Elements[1] = (A.m_Elements[1] + B.m_Elements[1]) * _REAL_HALF_;
                m_Elements[2] = (A.m_Elements[2] + B.m_Elements[2]) * _REAL_HALF_;
            }

            void CVector3D::SetWeighted(const CVector3D& Vector, const real Weight)
            {
                m_Elements[0] = Vector.m_Elements[0] * Weight;
                m_Elements[1] = Vector.m_Elements[1] * Weight;
                m_Elements[2] = Vector.m_Elements[2] * Weight;
            }

            void CVector3D::SetWeightedInverse(const CVector3D& Vector, const real Weight)
            {
                m_Elements[0] = Vector.m_Elements[0] / Weight;
                m_Elements[1] = Vector.m_Elements[1] / Weight;
                m_Elements[2] = Vector.m_Elements[2] / Weight;
            }

            void CVector3D::SetLinealCombination(const CVector3D& A, const CVector3D& B, const real SA, const real SB)
            {
                m_Elements[0] = A.m_Elements[0] * SA + B.m_Elements[0] * SB;
                m_Elements[1] = A.m_Elements[1] * SA + B.m_Elements[1] * SB;
                m_Elements[2] = A.m_Elements[2] * SA + B.m_Elements[2] * SB;
            }

            void CVector3D::SetZero()
            {
                memset(m_Elements, 0, sizeof(real) * 3);
            }

            void CVector3D::Negate()
            {
                m_Elements[0] = -m_Elements[0];
                m_Elements[1] = -m_Elements[1];
                m_Elements[2] = -m_Elements[2];
            }

            void CVector3D::AddWeightedOffset(const CVector3D& Vector, const real Weight)
            {
                m_Elements[0] += Vector.m_Elements[0] * Weight;
                m_Elements[1] += Vector.m_Elements[1] * Weight;
                m_Elements[2] += Vector.m_Elements[2] * Weight;
            }

            void CVector3D::AddOffset(const real Dx, const real Dy, const real Dz)
            {
                m_Elements[0] += Dx;
                m_Elements[1] += Dy;
                m_Elements[2] += Dz;
            }

            void CVector3D::AddOffsetX(const real Dx)
            {
                m_Elements[0] += Dx;
            }

            void CVector3D::AddOffsetY(const real Dy)
            {
                m_Elements[1] += Dy;
            }

            void CVector3D::AddOffsetZ(const real Dz)
            {
                m_Elements[2] += Dz;
            }

            void CVector3D::AddCovariance(const CVector3D& Vector, real& Axx, real& Axy, real& Axz, real& Ayy, real& Ayz, real& Azz) const
            {
                const real Dx = Vector.m_Elements[0] - m_Elements[0];
                const real Dy = Vector.m_Elements[1] - m_Elements[1];
                const real Dz = Vector.m_Elements[2] - m_Elements[2];
                Axx += Dx * Dx;
                Axy += Dx * Dy;
                Axz += Dx * Dz;
                Ayy += Dy * Dy;
                Ayz += Dy * Dz;
                Azz += Dz * Dz;
            }

            void CVector3D::AddWeightedCovariance(const CVector3D& Vector, const real Weight, real& Axx, real& Axy, real& Axz, real& Ayy, real& Ayz, real& Azz) const
            {
                const real Dx = Vector.m_Elements[0] - m_Elements[0];
                const real Dy = Vector.m_Elements[1] - m_Elements[1];
                const real Dz = Vector.m_Elements[2] - m_Elements[2];
                Axx += Dx * Dx * Weight;
                Axy += Dx * Dy * Weight;
                Axz += Dx * Dz * Weight;
                Ayy += Dy * Dy * Weight;
                Ayz += Dy * Dz * Weight;
                Azz += Dz * Dz * Weight;
            }

            bool CVector3D::IsNull() const
            {
                return (RealAbs(m_Elements[0]) < _REAL_EPSILON_) && (RealAbs(m_Elements[1]) < _REAL_EPSILON_) && (RealAbs(m_Elements[2]) < _REAL_EPSILON_);
            }

            bool CVector3D::IsNonNull() const
            {
                return (RealAbs(m_Elements[0]) > _REAL_EPSILON_) || (RealAbs(m_Elements[1]) > _REAL_EPSILON_) || (RealAbs(m_Elements[2]) > _REAL_EPSILON_);
            }

            bool CVector3D::IsAtInfinity() const
            {
                return (RealAbs(m_Elements[0]) == _REAL_MAX_) || (RealAbs(m_Elements[1]) == _REAL_MAX_) || (RealAbs(m_Elements[2]) == _REAL_MAX_);
            }

            bool CVector3D::IsNotAtInfinity() const
            {
                return (RealAbs(m_Elements[0]) != _REAL_MAX_) && (RealAbs(m_Elements[1]) != _REAL_MAX_) && (RealAbs(m_Elements[2]) != _REAL_MAX_);
            }

            bool CVector3D::IsUnitary() const
            {
                return RealAbs(RealSqrt(m_Elements[0] * m_Elements[0] + m_Elements[1] * m_Elements[1] + m_Elements[2] * m_Elements[2]) - _REAL_ONE_) < _REAL_EPSILON_;
            }

            bool CVector3D::IsNonUnitary() const
            {
                return RealSqrt(m_Elements[0] * m_Elements[0] + m_Elements[1] * m_Elements[1] + m_Elements[2] * m_Elements[2]) >= _REAL_EPSILON_;
            }

            bool CVector3D::IsZeroLength() const
            {
                return (RealAbs(m_Elements[0]) < _REAL_EPSILON_) && (RealAbs(m_Elements[1]) < _REAL_EPSILON_) && (RealAbs(m_Elements[2]) < _REAL_EPSILON_);
            }

            bool CVector3D::IsNonZeroLength() const
            {
                return (RealAbs(m_Elements[0]) > _REAL_EPSILON_) || (RealAbs(m_Elements[1]) > _REAL_EPSILON_) || (RealAbs(m_Elements[2]) > _REAL_EPSILON_);
            }

            real CVector3D::GetX() const
            {
                return m_Elements[0];
            }

            real CVector3D::GetY() const
            {
                return m_Elements[1];
            }

            real CVector3D::GetZ() const
            {
                return m_Elements[2];
            }

            real CVector3D::GetAngle(const CVector3D& Vector) const
            {
                return RealArcCosinus((m_Elements[0] * Vector.m_Elements[0] + m_Elements[1] * Vector.m_Elements[1] + m_Elements[2] * Vector.m_Elements[2]) / (RealSqrt(m_Elements[0] * m_Elements[0] + m_Elements[1] * m_Elements[1] + m_Elements[2] * m_Elements[2]) * RealSqrt(Vector.m_Elements[0] * Vector.m_Elements[0] + Vector.m_Elements[1] * Vector.m_Elements[1] + Vector.m_Elements[2] * Vector.m_Elements[2])));
            }

            real CVector3D::GetAperture(const CVector3D& Vector) const
            {
                return (m_Elements[0] * Vector.m_Elements[0] + m_Elements[1] * Vector.m_Elements[1]) / (RealSqrt(m_Elements[0] * m_Elements[0] + m_Elements[1] * m_Elements[1] + m_Elements[2] * m_Elements[2]) * RealSqrt(Vector.m_Elements[0] * Vector.m_Elements[0] + Vector.m_Elements[1] * Vector.m_Elements[1] + Vector.m_Elements[2] * Vector.m_Elements[2]));
            }

            real CVector3D::GetAbsoluteAperture(const CVector3D& Vector) const
            {
                return RealAbs((m_Elements[0] * Vector.m_Elements[0] + m_Elements[1] * Vector.m_Elements[1]) / (RealSqrt(m_Elements[0] * m_Elements[0] + m_Elements[1] * m_Elements[1] + m_Elements[2] * m_Elements[2]) * RealSqrt(Vector.m_Elements[0] * Vector.m_Elements[0] + Vector.m_Elements[1] * Vector.m_Elements[1] + Vector.m_Elements[2] * Vector.m_Elements[2])));
            }

            real CVector3D::GetDistance(const CVector3D& Vector) const
            {
                const real Dx = m_Elements[0] - Vector.m_Elements[0];
                const real Dy = m_Elements[1] - Vector.m_Elements[1];
                const real Dz = m_Elements[2] - Vector.m_Elements[2];
                return RealSqrt(Dx * Dx + Dy * Dy + Dz * Dz);
            }

            real CVector3D::GetLength() const
            {
                return RealSqrt(m_Elements[0] * m_Elements[0] + m_Elements[1] * m_Elements[1] + m_Elements[2] * m_Elements[2]);
            }

            real CVector3D::GetSquareLength() const
            {
                return m_Elements[0] * m_Elements[0] + m_Elements[1] * m_Elements[1] + m_Elements[2] * m_Elements[2];
            }

            real CVector3D::Normalize()
            {
                const real Length = RealSqrt(m_Elements[0] * m_Elements[0] + m_Elements[1] * m_Elements[1] + m_Elements[2] * m_Elements[2]);
                m_Elements[0] /= Length;
                m_Elements[1] /= Length;
                m_Elements[2] /= Length;
                return Length;
            }

            void CVector3D::ScaleAnisotropic(const real Sx, const real Sy, const real Sz)
            {
                m_Elements[0] /= Sx;
                m_Elements[1] /= Sy;
                m_Elements[2] /= Sz;
            }

            string CVector3D::ToString(const uint Precision) const
            {
                ostringstream OutputString;
                OutputString.precision(Precision);
                OutputString << "<Vector3D X=\"" << m_Elements[0] << "\" Y=\"" << m_Elements[1] << "\" Z=\"" << m_Elements[2] << "/>";
                return OutputString.str();
            }

            const real* CVector3D::GetElements() const
            {
                return m_Elements;
            }

            CVector3D CVector3D::VectorProduct(const CVector3D& Vector) const
            {
                return CVector3D(m_Elements[1] * Vector.m_Elements[2] - m_Elements[2] * Vector.m_Elements[1], m_Elements[2] * Vector.m_Elements[0] - m_Elements[0] * Vector.m_Elements[2], m_Elements[0] * Vector.m_Elements[1] - m_Elements[1] * Vector.m_Elements[0]);
            }

            real CVector3D::ScalarProduct(const CVector3D& Vector) const
            {
                return m_Elements[0] * Vector.m_Elements[0] + m_Elements[1] * Vector.m_Elements[1] + m_Elements[2] * Vector.m_Elements[2];
            }

            real CVector3D::ScalarProduct(const real X, const real Y, const real Z) const
            {
                return m_Elements[0] * X + m_Elements[1] * Y + m_Elements[2] * Z;
            }

            real CVector3D::GetSquareDistance(const CVector3D& Vector) const
            {
                const real Dx = m_Elements[0] - Vector.m_Elements[0];
                const real Dy = m_Elements[1] - Vector.m_Elements[1];
                const real Dz = m_Elements[2] - Vector.m_Elements[2];
                return Dx * Dx + Dy * Dy + Dz * Dz;
            }

            void CVector3D::operator=(const CVector3D& Vector)
            {
                memcpy(m_Elements, Vector.m_Elements, sizeof(real) * 3);
            }

            void CVector3D::operator+=(const CVector3D& Vector)
            {
                m_Elements[0] += Vector.m_Elements[0];
                m_Elements[1] += Vector.m_Elements[1];
                m_Elements[2] += Vector.m_Elements[2];
            }

            void CVector3D::operator-=(const CVector3D& Vector)
            {
                m_Elements[0] -= Vector.m_Elements[0];
                m_Elements[1] -= Vector.m_Elements[1];
                m_Elements[2] -= Vector.m_Elements[2];
            }

            void CVector3D::operator*=(const real Scalar)
            {
                m_Elements[0] *= Scalar;
                m_Elements[1] *= Scalar;
                m_Elements[2] *= Scalar;
            }

            void CVector3D::operator/=(const real Scalar)
            {
                m_Elements[0] /= Scalar;
                m_Elements[1] /= Scalar;
                m_Elements[2] /= Scalar;
            }

            bool operator==(const CVector3D& A, const CVector3D& B)
            {
                return (RealAbs(A.GetX() - B.GetX()) < _REAL_EPSILON_) && (RealAbs(A.GetY() - B.GetY()) < _REAL_EPSILON_) && (RealAbs(A.GetZ() - B.GetZ()) < _REAL_EPSILON_);
            }

            bool operator!=(const CVector3D& A, const CVector3D& B)
            {
                return (RealAbs(A.GetX() - B.GetX()) > _REAL_EPSILON_) || (RealAbs(A.GetY() - B.GetY()) > _REAL_EPSILON_) || (RealAbs(A.GetZ() - B.GetZ()) > _REAL_EPSILON_);
            }

            CVector3D operator/(const CVector3D& Vector, const real Scalar)
            {
                return CVector3D(Vector.GetX() / Scalar, Vector.GetY() / Scalar, Vector.GetZ() / Scalar);
            }

            CVector3D operator*(const CVector3D& Vector, const real Scalar)
            {
                return CVector3D(Vector.GetX() * Scalar, Vector.GetY() * Scalar, Vector.GetZ() * Scalar);
            }

            CVector3D operator*(const real Scalar, const CVector3D& Vector)
            {
                return CVector3D(Vector.GetX() * Scalar, Vector.GetY() * Scalar, Vector.GetZ() * Scalar);
            }

            CVector3D operator+(const CVector3D& A, const CVector3D& B)
            {
                return CVector3D(A.GetX() + B.GetX(), A.GetY() + B.GetY(), A.GetZ() + B.GetZ());
            }

            CVector3D operator-(const CVector3D& A, const CVector3D& B)
            {
                return CVector3D(A.GetX() - B.GetX(), A.GetY() - B.GetY(), A.GetZ() - B.GetZ());
            }
        }
    }
}
