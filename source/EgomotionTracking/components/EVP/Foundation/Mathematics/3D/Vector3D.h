/*
 * Vector3D.h
 *
 *  Created on: 22.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"
#include "../../DataTypes/PixelLocation.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            class CVector3D
            {
            public:

                static const CVector3D s_Point_At_Origin_3D;
                static const CVector3D s_Point_At_Minus_Infinity_3D;
                static const CVector3D s_Point_At_Plus_Infinity_3D;
                static const CVector3D s_Direction_Axis_X_3D;
                static const CVector3D s_Direction_Axis_Y_3D;
                static const CVector3D s_Direction_Axis_Z_3D;

                static  CVector3D CreateLinealCombination(const CVector3D& A, const CVector3D& B, const real SA, const real SB);
                static  CVector3D CreateComplementaryLinealCombination(const CVector3D& A, const CVector3D& B, const real SA);
                static  CVector3D CreateMidPoint(const CVector3D& A, const CVector3D& B);
                static  CVector3D CreateCannonicalForm(const real X, const real Y, const real Z);

                CVector3D();
                CVector3D(const real X, const real Y, const real Z);
                CVector3D(const CVector3D& Vector);
                CVector3D(const CPixelLocation& Location);

                bool SetCannonicalForm();

                void SetX(const real X);
                void SetY(const real Y);
                void SetZ(const real Z);
                void SetValue(const real X, const real Y, const real Z);
                void SetMaximalComponentValue(const real X, const real Y, const real Z);
                void SetMinimalComponentValue(const real X, const real Y, const real Z);
                void SetMaximalComponentValue(const CVector3D& Vector);
                void SetMinimalComponentValue(const CVector3D& Vector);
                void SetMidPoint(const CVector3D& A, const CVector3D& B);
                void SetWeighted(const CVector3D& Vector, const real Weight);
                void SetWeightedInverse(const CVector3D& Vector, const real Weight);
                void SetLinealCombination(const CVector3D& A, const CVector3D& B, const real SA, const real SB);

                void SetZero();
                void Negate();
                real Normalize();
                void ScaleAnisotropic(const real Sx, const real Sy, const real Sz);

                void AddWeightedOffset(const CVector3D& Vector, const real Weight);
                void AddOffset(const real Dx, const real Dy, const real Dz);
                void AddOffsetX(const real Dx);
                void AddOffsetY(const real Dy);
                void AddOffsetZ(const real Dz);

                void AddCovariance(const CVector3D& Vector, real& Axx, real& Axy, real& Axz, real& Ayy, real& Ayz, real& Azz) const;
                void AddWeightedCovariance(const CVector3D& Vector, const real Weight, real& Axx, real& Axy, real& Axz, real& Ayy, real& Ayz, real& Azz) const;

                bool IsNull() const;
                bool IsNonNull() const;
                bool IsAtInfinity() const;
                bool IsNotAtInfinity() const;
                bool IsUnitary() const;
                bool IsNonUnitary() const;
                bool IsZeroLength() const;
                bool IsNonZeroLength() const;

                real GetX() const;
                real GetY() const;
                real GetZ() const;

                CVector3D VectorProduct(const CVector3D& Vector) const;
                real ScalarProduct(const CVector3D& Vector) const;
                real ScalarProduct(const real X, const real Y, const real Z) const;

                real GetAngle(const CVector3D& Vector) const;
                real GetAperture(const CVector3D& Vector) const;
                real GetAbsoluteAperture(const CVector3D& Vector) const;
                real GetDistance(const CVector3D& Vector) const;
                real GetSquareDistance(const CVector3D& Vector) const;
                real GetLength() const;
                real GetSquareLength() const;

                void operator=(const CVector3D& Vector);
                void operator+=(const CVector3D& Vector);
                void operator-=(const CVector3D& Vector);
                void operator*=(const real Scalar);
                void operator/=(const real Scalar);
                string ToString(const uint Precision) const;

                const real* GetElements() const;

            protected:

                real m_Elements[3];
            };

            bool operator==(const CVector3D& A, const CVector3D& B);
            bool operator!=(const CVector3D& A, const CVector3D& B);
            CVector3D operator/(const CVector3D& Vector, const real Scalar);
            CVector3D operator*(const CVector3D& Vector, const real Scalar);
            CVector3D operator*(const real Scalar, const CVector3D& Vector);
            CVector3D operator+(const CVector3D& A, const CVector3D& B);
            CVector3D operator-(const CVector3D& A, const CVector3D& B);
        }
    }
}

