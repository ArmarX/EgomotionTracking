/*
 * Matrix3D.cpp
 *
 *  Created on: 06.04.2011
 *      Author: gonzalez
 */

#include "Matrix3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            CMatrix3D::CMatrix3D()
            {
                memset(&m_InnerMatrixStructure, 0, sizeof(MatrixStructure));
            }

            CMatrix3D::CMatrix3D(const real E00, const real E01, const real E02, const real E10, const real E11, const real E12, const real E20, const real E21, const real E22)
            {
                m_InnerMatrixStructure.m_Elements[0][0] = E00;
                m_InnerMatrixStructure.m_Elements[0][1] = E01;
                m_InnerMatrixStructure.m_Elements[0][2] = E02;
                m_InnerMatrixStructure.m_Elements[1][0] = E10;
                m_InnerMatrixStructure.m_Elements[1][1] = E11;
                m_InnerMatrixStructure.m_Elements[1][2] = E12;
                m_InnerMatrixStructure.m_Elements[2][0] = E20;
                m_InnerMatrixStructure.m_Elements[2][1] = E21;
                m_InnerMatrixStructure.m_Elements[2][2] = E22;
            }

            CMatrix3D::CMatrix3D(const CMatrix3D& Matrix)
            {
                memcpy(&m_InnerMatrixStructure, &Matrix.m_InnerMatrixStructure, sizeof(MatrixStructure));
            }

            CMatrix3D::CMatrix3D(const CMatrix3D* pMatrix)
            {
                memcpy(&m_InnerMatrixStructure, &(pMatrix->m_InnerMatrixStructure), sizeof(MatrixStructure));
            }

            CMatrix3D::CMatrix3D(const CVector3D& C0, const CVector3D& C1, const CVector3D& C2)
            {
                m_InnerMatrixStructure.m_Elements[0][0] = C0.GetX();
                m_InnerMatrixStructure.m_Elements[0][1] = C1.GetX();
                m_InnerMatrixStructure.m_Elements[0][2] = C2.GetX();
                m_InnerMatrixStructure.m_Elements[1][0] = C0.GetY();
                m_InnerMatrixStructure.m_Elements[1][1] = C1.GetY();
                m_InnerMatrixStructure.m_Elements[1][2] = C2.GetY();
                m_InnerMatrixStructure.m_Elements[2][0] = C0.GetZ();
                m_InnerMatrixStructure.m_Elements[2][1] = C1.GetZ();
                m_InnerMatrixStructure.m_Elements[2][2] = C2.GetZ();
            }

            CMatrix3D::CMatrix3D(const real Radians, const CVector3D& Axis)
            {
                SetByQuaternion(Radians, Axis);
            }

            CMatrix3D::CMatrix3D(const real qw, const real qx, const real qy, const real qz)
            {
                SetByQuaternion(qw, qx, qy, qz);
            }

            CMatrix3D::~CMatrix3D()
                = default;

            void CMatrix3D::MakeIdentity()
            {
                memset(&m_InnerMatrixStructure, 0, sizeof(MatrixStructure));
                for (uint i = 0; i < 3; ++i)
                {
                    m_InnerMatrixStructure.m_Elements[i][i] = _REAL_ONE_;
                }
            }

            real CMatrix3D::GetDeterminat() const
            {
                return -m_InnerMatrixStructure.m_Elements[0][2] * m_InnerMatrixStructure.m_Elements[1][1] * m_InnerMatrixStructure.m_Elements[2][0] + m_InnerMatrixStructure.m_Elements[0][1] * m_InnerMatrixStructure.m_Elements[1][2] * m_InnerMatrixStructure.m_Elements[2][0] + m_InnerMatrixStructure.m_Elements[0][2] * m_InnerMatrixStructure.m_Elements[1][0] * m_InnerMatrixStructure.m_Elements[2][1] - m_InnerMatrixStructure.m_Elements[0][0] * m_InnerMatrixStructure.m_Elements[1][2] * m_InnerMatrixStructure.m_Elements[2][1] - m_InnerMatrixStructure.m_Elements[0][1] * m_InnerMatrixStructure.m_Elements[1][0] * m_InnerMatrixStructure.m_Elements[2][2] + m_InnerMatrixStructure.m_Elements[0][0] * m_InnerMatrixStructure.m_Elements[1][1] * m_InnerMatrixStructure.m_Elements[2][2];
            }

            CMatrix3D CMatrix3D::GetInverse(bool* pSingularity) const
            {
                CMatrix3D InverseMatrix;
                const real Determinant = -m_InnerMatrixStructure.m_Elements[0][2] * m_InnerMatrixStructure.m_Elements[1][1] * m_InnerMatrixStructure.m_Elements[2][0] + m_InnerMatrixStructure.m_Elements[0][1] * m_InnerMatrixStructure.m_Elements[1][2] * m_InnerMatrixStructure.m_Elements[2][0] + m_InnerMatrixStructure.m_Elements[0][2] * m_InnerMatrixStructure.m_Elements[1][0] * m_InnerMatrixStructure.m_Elements[2][1] - m_InnerMatrixStructure.m_Elements[0][0] * m_InnerMatrixStructure.m_Elements[1][2] * m_InnerMatrixStructure.m_Elements[2][1] - m_InnerMatrixStructure.m_Elements[0][1] * m_InnerMatrixStructure.m_Elements[1][0] * m_InnerMatrixStructure.m_Elements[2][2] + m_InnerMatrixStructure.m_Elements[0][0] * m_InnerMatrixStructure.m_Elements[1][1] * m_InnerMatrixStructure.m_Elements[2][2];
                if (RealAbs(Determinant) > _REAL_EPSILON_)
                {
                    if (pSingularity)
                    {
                        *pSingularity = false;
                    }
                    const real Normalization = _REAL_ONE_ / Determinant;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[0][0] = (-m_InnerMatrixStructure.m_Elements[1][2] * m_InnerMatrixStructure.m_Elements[2][1] + m_InnerMatrixStructure.m_Elements[1][1] * m_InnerMatrixStructure.m_Elements[2][2]) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[0][1] = (m_InnerMatrixStructure.m_Elements[0][2] * m_InnerMatrixStructure.m_Elements[2][1] - m_InnerMatrixStructure.m_Elements[0][1] * m_InnerMatrixStructure.m_Elements[2][2]) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[0][2] = (-m_InnerMatrixStructure.m_Elements[0][2] * m_InnerMatrixStructure.m_Elements[1][1] + m_InnerMatrixStructure.m_Elements[0][1] * m_InnerMatrixStructure.m_Elements[1][2]) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[1][0] = (m_InnerMatrixStructure.m_Elements[1][2] * m_InnerMatrixStructure.m_Elements[2][0] - m_InnerMatrixStructure.m_Elements[1][0] * m_InnerMatrixStructure.m_Elements[2][2]) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[1][1] = (-m_InnerMatrixStructure.m_Elements[0][2] * m_InnerMatrixStructure.m_Elements[2][0] + m_InnerMatrixStructure.m_Elements[0][0] * m_InnerMatrixStructure.m_Elements[2][2]) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[1][2] = (m_InnerMatrixStructure.m_Elements[0][2] * m_InnerMatrixStructure.m_Elements[1][0] - m_InnerMatrixStructure.m_Elements[0][0] * m_InnerMatrixStructure.m_Elements[1][2]) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[2][0] = (-m_InnerMatrixStructure.m_Elements[1][1] * m_InnerMatrixStructure.m_Elements[2][0] + m_InnerMatrixStructure.m_Elements[1][0] * m_InnerMatrixStructure.m_Elements[2][1]) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[2][1] = (m_InnerMatrixStructure.m_Elements[0][1] * m_InnerMatrixStructure.m_Elements[2][0] - m_InnerMatrixStructure.m_Elements[0][0] * m_InnerMatrixStructure.m_Elements[2][1]) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[2][2] = (-m_InnerMatrixStructure.m_Elements[0][1] * m_InnerMatrixStructure.m_Elements[1][0] + m_InnerMatrixStructure.m_Elements[0][0] * m_InnerMatrixStructure.m_Elements[1][1]) * Normalization;
                }
                else if (pSingularity)
                {
                    *pSingularity = true;
                }
                return InverseMatrix;
            }

            CMatrix3D CMatrix3D::GetTranspose() const
            {
                CMatrix3D TransposedMatrix;
                for (uint r = 0; r < 3; ++r)
                    for (uint c = 0; c < 3; ++c)
                    {
                        TransposedMatrix.m_InnerMatrixStructure.m_Elements[c][r] = m_InnerMatrixStructure.m_Elements[r][c];
                    }
                return TransposedMatrix;
            }

            void CMatrix3D::SetByElements(const real E00, const real E01, const real E02, const real E10, const real E11, const real E12, const real E20, const real E21, const real E22)
            {
                m_InnerMatrixStructure.m_Elements[0][0] = E00;
                m_InnerMatrixStructure.m_Elements[0][1] = E01;
                m_InnerMatrixStructure.m_Elements[0][2] = E02;
                m_InnerMatrixStructure.m_Elements[1][0] = E10;
                m_InnerMatrixStructure.m_Elements[1][1] = E11;
                m_InnerMatrixStructure.m_Elements[1][2] = E12;
                m_InnerMatrixStructure.m_Elements[2][0] = E20;
                m_InnerMatrixStructure.m_Elements[2][1] = E21;
                m_InnerMatrixStructure.m_Elements[2][2] = E22;
            }

            void CMatrix3D::SetByRow(const real* pMatrix)
            {
                for (uint r = 0; r < 3; ++r)
                    for (uint c = 0; c < 3; ++c)
                    {
                        m_InnerMatrixStructure.m_Elements[c][r] = *pMatrix++;
                    }
            }

            void CMatrix3D::SetByQuaternion(const real Angle, const CVector3D& Axis)
            {
                CVector3D NormalizedAxis = Axis;
                NormalizedAxis.Normalize();
                const real S = RealSinus(Angle * _REAL_HALF_);
                SetByQuaternion(RealCosinus(Angle * _REAL_HALF_), NormalizedAxis.GetX() * S, NormalizedAxis.GetY() * S, NormalizedAxis.GetZ() * S);
            }

            void CMatrix3D::SetByQuaternion(const real qw, const real qx, const real qy, const real qz)
            {
                const real Norm = RealSqrt(qw * qw + qx * qx + qy * qy + qz * qz);
                const real w = qw / Norm;
                const real x = qx / Norm;
                const real y = qy / Norm;
                const real z = qz / Norm;
                m_InnerMatrixStructure.m_Elements[0][0] = _REAL_ONE_ - _REAL_TWO_ * (y * y + z * z);
                m_InnerMatrixStructure.m_Elements[0][1] = -_REAL_TWO_ * w * z + _REAL_TWO_ * x * y;
                m_InnerMatrixStructure.m_Elements[0][2] = _REAL_TWO_ * w * y + _REAL_TWO_ * x * z;
                m_InnerMatrixStructure.m_Elements[1][0] = _REAL_TWO_ * w * z + _REAL_TWO_ * x * y;
                m_InnerMatrixStructure.m_Elements[1][1] = _REAL_ONE_ - _REAL_TWO_ * (x * x + z * z);
                m_InnerMatrixStructure.m_Elements[1][2] = -_REAL_TWO_ * w * x + _REAL_TWO_ * y * z;
                m_InnerMatrixStructure.m_Elements[2][0] = -_REAL_TWO_ * w * y + _REAL_TWO_ * x * z;
                m_InnerMatrixStructure.m_Elements[2][1] = _REAL_TWO_ * w * x + _REAL_TWO_ * y * z;
                m_InnerMatrixStructure.m_Elements[2][2] = _REAL_ONE_ - _REAL_TWO_ * (x * x + y * y);
            }

            real* CMatrix3D::operator[](const uint Row)
            {
                return m_InnerMatrixStructure.m_Elements[Row];
            }

            const real* CMatrix3D::operator[](const uint Row) const
            {
                return m_InnerMatrixStructure.m_Elements[Row];
            }

            CMatrix3D& CMatrix3D::operator=(const CMatrix3D& Matrix)
            {
                memcpy(&m_InnerMatrixStructure, &Matrix.m_InnerMatrixStructure, sizeof(MatrixStructure));
                return *this;
            }

            CMatrix3D& CMatrix3D::operator*=(const CMatrix3D& Matrix)
            {
                MatrixStructure TemporalMatrixStructure;
                for (uint r = 0; r < 3; ++r)
                    for (uint c = 0; c < 3; ++c)
                    {
                        TemporalMatrixStructure.m_Elements[r][c] = m_InnerMatrixStructure.m_Elements[r][0] * Matrix.m_InnerMatrixStructure.m_Elements[0][c] + m_InnerMatrixStructure.m_Elements[r][1] * Matrix.m_InnerMatrixStructure.m_Elements[1][c] + m_InnerMatrixStructure.m_Elements[r][2] * Matrix.m_InnerMatrixStructure.m_Elements[2][c];
                    }
                memcpy(&m_InnerMatrixStructure, &TemporalMatrixStructure, sizeof(MatrixStructure));
                return *this;
            }

            CMatrix3D CMatrix3D::operator*(const CMatrix3D& Matrix)
            {
                CMatrix3D ResultMatrix;
                for (uint r = 0; r < 3; ++r)
                    for (uint c = 0; c < 3; ++c)
                    {
                        ResultMatrix.m_InnerMatrixStructure.m_Elements[r][c] = m_InnerMatrixStructure.m_Elements[r][0] * Matrix.m_InnerMatrixStructure.m_Elements[0][c] + m_InnerMatrixStructure.m_Elements[r][1] * Matrix.m_InnerMatrixStructure.m_Elements[1][c] + m_InnerMatrixStructure.m_Elements[r][2] * Matrix.m_InnerMatrixStructure.m_Elements[2][c];
                    }
                return ResultMatrix;
            }

            CVector3D CMatrix3D::operator*(const CVector3D& Vector)
            {
                const real X = Vector.GetX();
                const real Y = Vector.GetY();
                const real Z = Vector.GetZ();
                return CVector3D(m_InnerMatrixStructure.m_Elements[0][0] * X + m_InnerMatrixStructure.m_Elements[0][1] * Y + m_InnerMatrixStructure.m_Elements[0][2] * Z, m_InnerMatrixStructure.m_Elements[1][0] * X + m_InnerMatrixStructure.m_Elements[1][1] * Y + m_InnerMatrixStructure.m_Elements[1][2] * Z, m_InnerMatrixStructure.m_Elements[2][0] * X + m_InnerMatrixStructure.m_Elements[2][1] * Y + m_InnerMatrixStructure.m_Elements[2][2] * Z);
            }

            CMatrix3D CMatrix3D::operator*(const real Scalar)
            {
                for (uint i = 0; i < 9; ++i)
                {
                    m_InnerMatrixStructure.m_Array[i] *= Scalar;
                }
                return *this;
            }

            bool CMatrix3D::operator==(const CMatrix3D& Matrix)
            {
                for (uint i = 0; i < 9; ++i)
                    if (RealAbs(m_InnerMatrixStructure.m_Array[i] - Matrix.m_InnerMatrixStructure.m_Array[i]) > _REAL_EPSILON_)
                    {
                        return false;
                    }
                return true;
            }

            bool CMatrix3D::operator!=(const CMatrix3D& Matrix)
            {
                for (uint i = 0; i < 9; ++i)
                    if (RealAbs(m_InnerMatrixStructure.m_Array[i] - Matrix.m_InnerMatrixStructure.m_Array[i]) > _REAL_EPSILON_)
                    {
                        return true;
                    }
                return false;
            }

            string CMatrix3D::ToString(const uint Precision) const
            {
                ostringstream OutputString;
                OutputString.precision(Precision);
                string Delim("; ");
                for (uint r = 0; r < 3; ++r)
                {
                    OutputString << m_InnerMatrixStructure.m_Elements[r][0] << Delim << m_InnerMatrixStructure.m_Elements[r][1] << Delim << m_InnerMatrixStructure.m_Elements[r][2] << g_EndLine;
                }
                return OutputString.str();
            }
        }
    }
}
