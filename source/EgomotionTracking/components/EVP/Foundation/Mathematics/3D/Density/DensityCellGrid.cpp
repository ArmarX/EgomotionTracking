/*
 * DensityCellGrid.cpp
 *
 *  Created on: Apr 3, 2012
 *      Author: david
 */

#include "../../../Memory/MemoryManager.h"
#include "DensityCellGrid.h"
#include "DensityCellContainerLink.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                CDensityCellGrid::CDensityCellGrid() :
                    m_ConditioningMode(CDensityPrimitive::eConjunctive), m_DiscretizationResolution(_REAL_ZERO_), m_Width(0), m_Height(0), m_Depth(0), m_TotalSliceContainers(0), m_TotalGridContainers(0), m_BasePoint(CVector3D::s_Point_At_Plus_Infinity_3D), m_CellDeltaVector(CVector3D::s_Point_At_Origin_3D), m_pDensityCellGrid(nullptr), m_TotalInnerCoreCells(0), m_TotalBoundaryCells(0)
                {
                }

                CDensityCellGrid::~CDensityCellGrid()
                {
                    ClearCellGrid();
                }

                bool CDensityCellGrid::SetConfiguration(const CContinuousBoundingBox3D& BoundingBox, const real DiscretizationResolution)
                {
                    if ((!BoundingBox.IsEmpty()) && (DiscretizationResolution > _REAL_EPSILON_))
                    {
                        ClearCellGrid();
                        const real ContinousWidth = BoundingBox.GetWidth();
                        const real ContinousHeight = BoundingBox.GetHeight();
                        const real ContinousDepth = BoundingBox.GetDepth();
                        m_DiscretizationResolution = DiscretizationResolution;
                        m_Width = uint(RealRound(ContinousWidth / m_DiscretizationResolution));
                        m_Height = uint(RealRound(ContinousHeight / m_DiscretizationResolution));
                        m_Depth = uint(RealRound(ContinousDepth / m_DiscretizationResolution));
                        m_TotalSliceContainers = m_Width * m_Height;
                        m_TotalGridContainers = m_TotalSliceContainers * m_Depth;
                        if (m_TotalGridContainers)
                        {
                            m_BasePoint = BoundingBox.GetMinPoint();
                            m_CellDeltaVector.SetValue(ContinousWidth / real(m_Width), ContinousHeight / real(m_Height), ContinousDepth / real(m_Depth));
                            m_pDensityCellGrid = new CDensityCellContainer[m_TotalGridContainers];
                            if (m_pDensityCellGrid)
                            {
                                return true;
                            }
                            else
                            {
                                RELEASE_ARRAY(m_pDensityCellGrid);
                                return false;
                            }
                        }
                        else
                        {
                            m_Width = 0;
                            m_Height = 0;
                            m_Depth = 0;
                        }
                    }
                    return false;
                }

                bool CDensityCellGrid::LoadDensity(const list<const CDensityPrimitive*>& DensityPrimitives, const CDensityPrimitive::ConditioningMode Mode)
                {
                    const uint TotalDensityPrimitives = DensityPrimitives.size();
                    if (TotalDensityPrimitives && m_pDensityCellGrid)
                    {
                        m_DensityPrimitives = DensityPrimitives;
                        m_ConditioningMode = Mode;
                        list<const CDensityPrimitive*>::const_iterator EndDensityPrimitives = m_DensityPrimitives.end();
                        list<const CDensityPrimitive*>::const_iterator BeginDensityPrimitives = m_DensityPrimitives.begin();
                        const real Lx = m_BasePoint.GetX();
                        const real Ly = m_BasePoint.GetY();
                        const real Dx = m_CellDeltaVector.GetX();
                        const real Dy = m_CellDeltaVector.GetY();
                        const real Dz = m_CellDeltaVector.GetZ();
                        CVector3D Location = m_BasePoint;
                        CDensityCellContainer* pDensityCellContainer = m_pDensityCellGrid;
                        switch (m_ConditioningMode)
                        {
                            case CDensityPrimitive::eConjunctive:
                                for (uint Z = 0; Z < m_Depth; ++Z, Location.AddOffsetZ(Dz), Location.SetY(Ly))
                                    for (uint Y = 0; Y < m_Height; ++Y, Location.AddOffsetY(Dy), Location.SetX(Lx))
                                        for (uint X = 0; X < m_Width; ++X, ++pDensityCellContainer, Location.AddOffsetX(Dx))
                                        {
                                            real Density = _REAL_ONE_;
                                            for (list<const CDensityPrimitive*>::const_iterator ppDensityPrimitive = BeginDensityPrimitives; ppDensityPrimitive != EndDensityPrimitives; ++ppDensityPrimitive)
                                            {
                                                Density *= (*ppDensityPrimitive)->GetDensity(Location);
                                            }
                                            pDensityCellContainer->SetValue(Location, Density);
                                        }
                                break;
                            case CDensityPrimitive::eDisjunctive:
                                for (uint Z = 0; Z < m_Depth; ++Z, Location.AddOffsetZ(Dz), Location.SetY(Ly))
                                    for (uint Y = 0; Y < m_Height; ++Y, Location.AddOffsetY(Dy), Location.SetX(Lx))
                                        for (uint X = 0; X < m_Width; ++X, ++pDensityCellContainer, Location.AddOffsetX(Dx))
                                        {
                                            real Density = _REAL_ZERO_;
                                            for (list<const CDensityPrimitive*>::const_iterator ppDensityPrimitive = BeginDensityPrimitives; ppDensityPrimitive != EndDensityPrimitives; ++ppDensityPrimitive)
                                            {
                                                Density += (*ppDensityPrimitive)->GetDensity(Location);
                                            }
                                            pDensityCellContainer->SetValue(Location, Density);
                                        }
                                break;
                        }
                        return true;
                    }
                    return false;
                }

                bool CDensityCellGrid::ExtractIsoDensityStructure(const real ThresholdDensity)
                {
                    ClearDensityCellContainerLinks();
                    const real SafeThresholdDensity = TMin(_REAL_ONE_, TMax(_REAL_ZERO_, ThresholdDensity));
                    const CDensityCellContainer* const pFinalDensityCellContainer = m_pDensityCellGrid + m_TotalGridContainers;
                    CDensityCellContainer* pDensityCellContainer = m_pDensityCellGrid;
                    list<CDensityCellContainer*> InnerDensityCellContainers;
                    while (pDensityCellContainer < pFinalDensityCellContainer)
                    {
                        if (pDensityCellContainer->Segment(SafeThresholdDensity))
                        {
                            InnerDensityCellContainers.push_back(pDensityCellContainer);
                        }
                        pDensityCellContainer++;
                    }
                    if (InnerDensityCellContainers.size())
                    {
                        const uint SubWidth = m_Width - 1;
                        const uint SubHeight = m_Height - 1;
                        const uint SubDepth = m_Depth - 1;
                        uint X = 0, Y = 0, Z = 0, Index = 0;
                        m_TotalBoundaryCells = 0;
                        list<CDensityCellContainer*>::iterator EndInnerDensityCellContainers = InnerDensityCellContainers.end();
                        switch (m_ConditioningMode)
                        {
                            case CDensityPrimitive::eConjunctive:
                                for (list<CDensityCellContainer*>::iterator ppDensityCellContainer = InnerDensityCellContainers.begin(); ppDensityCellContainer != EndInnerDensityCellContainers; ++ppDensityCellContainer)
                                {
                                    CDensityCellContainer* pInnerDensityCellContainer = *ppDensityCellContainer;
                                    pInnerDensityCellContainer->GetRelativeIndexingcoordinates(m_pDensityCellGrid, m_Width, m_TotalSliceContainers, X, Y, Z);
                                    if (X && (pInnerDensityCellContainer - 1)->IsOutter())
                                    {
                                        m_DensityCellContainerLinks.push_back(CreateConjunctiveLink(Index++, pInnerDensityCellContainer, pInnerDensityCellContainer - 1, SafeThresholdDensity));
                                    }
                                    if ((X < SubWidth) && (pInnerDensityCellContainer + 1)->IsOutter())
                                    {
                                        m_DensityCellContainerLinks.push_back(CreateConjunctiveLink(Index++, pInnerDensityCellContainer, pInnerDensityCellContainer + 1, SafeThresholdDensity));
                                    }
                                    if (Y && (pInnerDensityCellContainer - m_Width)->IsOutter())
                                    {
                                        m_DensityCellContainerLinks.push_back(CreateConjunctiveLink(Index++, pInnerDensityCellContainer, pInnerDensityCellContainer - m_Width, SafeThresholdDensity));
                                    }
                                    if ((Y < SubHeight) && (pInnerDensityCellContainer + m_Width)->IsOutter())
                                    {
                                        m_DensityCellContainerLinks.push_back(CreateConjunctiveLink(Index++, pInnerDensityCellContainer, pInnerDensityCellContainer + m_Width, SafeThresholdDensity));
                                    }
                                    if (Z && (pInnerDensityCellContainer - m_TotalSliceContainers)->IsOutter())
                                    {
                                        m_DensityCellContainerLinks.push_back(CreateConjunctiveLink(Index++, pInnerDensityCellContainer, pInnerDensityCellContainer - m_TotalSliceContainers, SafeThresholdDensity));
                                    }
                                    if ((Z < SubDepth) && (pInnerDensityCellContainer + m_TotalSliceContainers)->IsOutter())
                                    {
                                        m_DensityCellContainerLinks.push_back(CreateConjunctiveLink(Index++, pInnerDensityCellContainer, pInnerDensityCellContainer + m_TotalSliceContainers, SafeThresholdDensity));
                                    }
                                    if (pInnerDensityCellContainer->IsBoundary())
                                    {
                                        ++m_TotalBoundaryCells;
                                    }
                                }
                                break;
                            case CDensityPrimitive::eDisjunctive:
                                for (list<CDensityCellContainer*>::iterator ppDensityCellContainer = InnerDensityCellContainers.begin(); ppDensityCellContainer != EndInnerDensityCellContainers; ++ppDensityCellContainer)
                                {
                                    CDensityCellContainer* pInnerDensityCellContainer = *ppDensityCellContainer;
                                    pInnerDensityCellContainer->GetRelativeIndexingcoordinates(m_pDensityCellGrid, m_Width, m_TotalSliceContainers, X, Y, Z);
                                    if (X && (pInnerDensityCellContainer - 1)->IsOutter())
                                    {
                                        m_DensityCellContainerLinks.push_back(CreateDisjunctiveLink(Index++, pInnerDensityCellContainer, pInnerDensityCellContainer - 1, SafeThresholdDensity));
                                    }
                                    if ((X < SubWidth) && (pInnerDensityCellContainer + 1)->IsOutter())
                                    {
                                        m_DensityCellContainerLinks.push_back(CreateDisjunctiveLink(Index++, pInnerDensityCellContainer, pInnerDensityCellContainer + 1, SafeThresholdDensity));
                                    }
                                    if (Y && (pInnerDensityCellContainer - m_Width)->IsOutter())
                                    {
                                        m_DensityCellContainerLinks.push_back(CreateDisjunctiveLink(Index++, pInnerDensityCellContainer, pInnerDensityCellContainer - m_Width, SafeThresholdDensity));
                                    }
                                    if ((Y < SubHeight) && (pInnerDensityCellContainer + m_Width)->IsOutter())
                                    {
                                        m_DensityCellContainerLinks.push_back(CreateDisjunctiveLink(Index++, pInnerDensityCellContainer, pInnerDensityCellContainer + m_Width, SafeThresholdDensity));
                                    }
                                    if (Z && (pInnerDensityCellContainer - m_TotalSliceContainers)->IsOutter())
                                    {
                                        m_DensityCellContainerLinks.push_back(CreateDisjunctiveLink(Index++, pInnerDensityCellContainer, pInnerDensityCellContainer - m_TotalSliceContainers, SafeThresholdDensity));
                                    }
                                    if ((Z < SubDepth) && (pInnerDensityCellContainer + m_TotalSliceContainers)->IsOutter())
                                    {
                                        m_DensityCellContainerLinks.push_back(CreateDisjunctiveLink(Index++, pInnerDensityCellContainer, pInnerDensityCellContainer + m_TotalSliceContainers, SafeThresholdDensity));
                                    }
                                    if (pInnerDensityCellContainer->IsBoundary())
                                    {
                                        ++m_TotalBoundaryCells;
                                    }
                                }
                                break;
                        }
                        m_TotalInnerCoreCells = InnerDensityCellContainers.size() - m_TotalBoundaryCells;
                    }
                    return m_DensityCellContainerLinks.size();
                }

                CVector3D CDensityCellGrid::EstimateConjunctiveNormal(const CVector3D& X) const
                {
                    CVector3D Normal;
                    real Density = _REAL_ZERO_;
                    list<const CDensityPrimitive*>::const_iterator EndDensityPrimitives = m_DensityPrimitives.end();
                    for (list<const CDensityPrimitive*>::const_iterator ppDensityPrimitive = m_DensityPrimitives.begin(); ppDensityPrimitive != EndDensityPrimitives; ++ppDensityPrimitive)
                    {
                        const CVector3D& Gradient = (*ppDensityPrimitive)->GetDensityGradient(X, Density);
                        Normal -= Gradient / Density;
                    }
                    Normal.Normalize();
                    return Normal;
                }

                CVector3D CDensityCellGrid::EstimateDisjunctiveNormal(const CVector3D& X) const
                {
                    CVector3D Normal;
                    list<CVector3D> PartialDerivatives;
                    list<const CDensityPrimitive*>::const_iterator EndDensityPrimitives = m_DensityPrimitives.end();
                    for (list<const CDensityPrimitive*>::const_iterator ppDensityPrimitive = m_DensityPrimitives.begin(); ppDensityPrimitive != EndDensityPrimitives; ++ppDensityPrimitive)
                    {
                        Normal -= (*ppDensityPrimitive)->GetDensityGradient(X);
                    }
                    Normal.Normalize();
                    return Normal;
                }

                CDensityCellContainerLink* CDensityCellGrid::CreateConjunctiveLink(const uint Index, CDensityCellContainer* pInnerDensityCellContainer, CDensityCellContainer* pOutterDensityCellContainer, const real ThresholdDensity)
                {
                    const real DensityA = pInnerDensityCellContainer->GetDensity();
                    const real DensityB = pOutterDensityCellContainer->GetDensity();
                    const real DeltaA = RealAbs(DensityA - ThresholdDensity);
                    const real DeltaB = RealAbs(DensityB - ThresholdDensity);
                    const real TotalDeltas = DeltaA + DeltaB;
                    const real ContributionA = DeltaB / TotalDeltas;
                    const real ContributionB = DeltaA / TotalDeltas;
                    const CVector3D X = CVector3D::CreateLinealCombination(pInnerDensityCellContainer->GetLocation(), pOutterDensityCellContainer->GetLocation(), ContributionA, ContributionB);
                    return new CDensityCellContainerLink(Index, pInnerDensityCellContainer, pOutterDensityCellContainer, X, EstimateConjunctiveNormal(X), DensityA * ContributionA + DensityB * ContributionB);
                }

                CDensityCellContainerLink* CDensityCellGrid::CreateDisjunctiveLink(const uint Index, CDensityCellContainer* pInnerDensityCellContainer, CDensityCellContainer* pOutterDensityCellContainer, const real ThresholdDensity)
                {
                    const real DensityA = pInnerDensityCellContainer->GetDensity();
                    const real DensityB = pOutterDensityCellContainer->GetDensity();
                    const real DeltaA = RealAbs(DensityA - ThresholdDensity);
                    const real DeltaB = RealAbs(DensityB - ThresholdDensity);
                    const real TotalDeltas = DeltaA + DeltaB;
                    const real ContributionA = DeltaB / TotalDeltas;
                    const real ContributionB = DeltaA / TotalDeltas;
                    const CVector3D X = CVector3D::CreateLinealCombination(pInnerDensityCellContainer->GetLocation(), pOutterDensityCellContainer->GetLocation(), ContributionA, ContributionB);
                    return new CDensityCellContainerLink(Index, pInnerDensityCellContainer, pOutterDensityCellContainer, X, EstimateDisjunctiveNormal(X), DensityA * ContributionA + DensityB * ContributionB);
                }

                CDensityPrimitive::ConditioningMode CDensityCellGrid::GetConditioningMode() const
                {
                    return m_ConditioningMode;
                }

                real CDensityCellGrid::GetDiscretizationResolution() const
                {
                    return m_DiscretizationResolution;
                }

                bool CDensityCellGrid::IsContouringReady() const
                {
                    return m_DensityCellContainerLinks.size() && (m_Width > 1) && (m_Height > 1) && (m_Depth > 1);
                }

                uint CDensityCellGrid::GetWidth() const
                {
                    return m_Width;
                }

                uint CDensityCellGrid::GetHeight() const
                {
                    return m_Height;
                }

                uint CDensityCellGrid::GetDepth() const
                {
                    return m_Depth;
                }

                uint CDensityCellGrid::GetTotalSliceContainers() const
                {
                    return m_TotalSliceContainers;
                }

                uint CDensityCellGrid::GetTotalGridContainers() const
                {
                    return m_TotalGridContainers;
                }

                const CVector3D& CDensityCellGrid::GetBasePoint() const
                {
                    return m_BasePoint;
                }

                const CVector3D& CDensityCellGrid::GetCellDeltaVector() const
                {
                    return m_CellDeltaVector;
                }

                const CDensityCellContainer* CDensityCellGrid::GetReadOnlyDensityCellGrid() const
                {
                    return m_pDensityCellGrid;
                }

                CDensityCellContainer* CDensityCellGrid::GetWritableDensityCellGrid()
                {
                    return m_pDensityCellGrid;
                }

                const list<CDensityCellContainerLink*>& CDensityCellGrid::GetDensityCellContainerLinks() const
                {
                    return m_DensityCellContainerLinks;
                }

                real CDensityCellGrid::GetCellVolume() const
                {
                    return m_DiscretizationResolution * m_DiscretizationResolution * m_DiscretizationResolution;
                }

                real CDensityCellGrid::GetInnerCoreVolume() const
                {
                    return real(m_TotalInnerCoreCells) * (m_DiscretizationResolution * m_DiscretizationResolution * m_DiscretizationResolution);
                }

                real CDensityCellGrid::GetBoundaryVolume() const
                {
                    return real(m_TotalBoundaryCells) * (m_DiscretizationResolution * m_DiscretizationResolution * m_DiscretizationResolution);
                }

                void CDensityCellGrid::ClearCellGrid()
                {
                    ClearDensityCellContainerLinks();
                    RELEASE_ARRAY(m_pDensityCellGrid);
                }

                void CDensityCellGrid::ClearDensityCellContainerLinks()
                {
                    if (m_DensityCellContainerLinks.size())
                    {
                        list<CDensityCellContainerLink*>::const_iterator EndDensityCellContainerEdges = m_DensityCellContainerLinks.end();
                        for (list<CDensityCellContainerLink*>::const_iterator ppDensityCellContainerEdge = m_DensityCellContainerLinks.begin(); ppDensityCellContainerEdge != EndDensityCellContainerEdges; ++ppDensityCellContainerEdge)
                            RELEASE_OBJECT_DIRECT(*ppDensityCellContainerEdge)
                            m_DensityCellContainerLinks.clear();
                    }
                }
            }
        }
    }
}
