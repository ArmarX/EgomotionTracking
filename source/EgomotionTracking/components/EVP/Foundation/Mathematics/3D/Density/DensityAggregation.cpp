/*
 * DensityAggregation.cpp
 *
 *  Created on: Apr 2, 2012
 *      Author: david
 */

#include "DensityAggregation.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                CDensityAggregation::CDensityAggregation(const bool DensityPrimitiveOwnerShip) :
                    m_DensityPrimitiveOwnerShip(DensityPrimitiveOwnerShip)
                {
                }

                CDensityAggregation::~CDensityAggregation()
                {
                    ClearDensityPrimitives();
                }

                bool CDensityAggregation::AddDensityPrimitive(const CDensityPrimitive* pDensityPrimitive)
                {
                    if (pDensityPrimitive)
                    {
                        list<const CDensityPrimitive*>::const_iterator EndDensityPrimitives = m_DensityPrimitive.end();
                        for (list<const CDensityPrimitive*>::const_iterator ppDensityPrimitive = m_DensityPrimitive.begin(); ppDensityPrimitive != EndDensityPrimitives; ++ppDensityPrimitive)
                            if ((*ppDensityPrimitive) == pDensityPrimitive)
                            {
                                return false;
                            }
                        m_DensityPrimitive.push_back(pDensityPrimitive);
                        return true;
                    }
                    return false;
                }

                void CDensityAggregation::ClearDensityPrimitives()
                {
                    if (m_DensityPrimitive.size())
                    {
                        if (m_DensityPrimitiveOwnerShip)
                        {
                            list<const CDensityPrimitive*>::const_iterator EndDensityPrimitives = m_DensityPrimitive.end();
                            for (list<const CDensityPrimitive*>::const_iterator ppDensityPrimitive = m_DensityPrimitive.begin(); ppDensityPrimitive != EndDensityPrimitives; ++ppDensityPrimitive)
                            {
                                RELEASE_OBJECT_DIRECT(*ppDensityPrimitive);
                            }
                        }
                        m_DensityPrimitive.clear();
                    }
                }

                CContinuousBoundingBox3D CDensityAggregation::GetDensityBoundingBox(const CDensityPrimitive::ConditioningMode ConditioningMode, const real MinimalDensity) const
                {
                    if (m_DensityPrimitive.size())
                        switch (ConditioningMode)
                        {
                            case CDensityPrimitive::eConjunctive:
                            {
                                CContinuousBoundingBox3D DensityBoundingBox = m_DensityPrimitive.front()->GetDensityBoundingBox(MinimalDensity);
                                list<const CDensityPrimitive*>::const_iterator EndDensityPrimitives = m_DensityPrimitive.end();
                                for (list<const CDensityPrimitive*>::const_iterator ppDensityPrimitive = ++m_DensityPrimitive.begin(); ppDensityPrimitive != EndDensityPrimitives; ++ppDensityPrimitive)
                                    if (!DensityBoundingBox.GetIntersection((*ppDensityPrimitive)->GetDensityBoundingBox(MinimalDensity), DensityBoundingBox))
                                    {
                                        return CContinuousBoundingBox3D();
                                    }
                                return DensityBoundingBox;
                            }
                            break;
                            case CDensityPrimitive::eDisjunctive:
                            {
                                CContinuousBoundingBox3D DensityBoundingBox = m_DensityPrimitive.front()->GetDensityBoundingBox(MinimalDensity);
                                list<const CDensityPrimitive*>::const_iterator EndDensityPrimitives = m_DensityPrimitive.end();
                                for (list<const CDensityPrimitive*>::const_iterator ppDensityPrimitive = ++m_DensityPrimitive.begin(); ppDensityPrimitive != EndDensityPrimitives; ++ppDensityPrimitive)
                                {
                                    DensityBoundingBox.Extend((*ppDensityPrimitive)->GetDensityBoundingBox(MinimalDensity));
                                }
                                return DensityBoundingBox;

                            }
                            break;
                        }
                    return CContinuousBoundingBox3D();
                }

                real CDensityAggregation::GetDensity(const CDensityPrimitive::ConditioningMode ConditioningMode, const CVector3D& X) const
                {
                    if (m_DensityPrimitive.size())
                        switch (ConditioningMode)
                        {
                            case CDensityPrimitive::eConjunctive:
                            {
                                real Density = _REAL_ONE_;
                                list<const CDensityPrimitive*>::const_iterator EndDensityPrimitives = m_DensityPrimitive.end();
                                for (list<const CDensityPrimitive*>::const_iterator ppDensityPrimitive = m_DensityPrimitive.begin(); ppDensityPrimitive != EndDensityPrimitives; ++ppDensityPrimitive)
                                {
                                    Density *= (*ppDensityPrimitive)->GetDensity(X);
                                }
                                return Density;
                            }
                            break;
                            case CDensityPrimitive::eDisjunctive:
                            {
                                real Density = _REAL_ZERO_;
                                list<const CDensityPrimitive*>::const_iterator EndDensityPrimitives = m_DensityPrimitive.end();
                                for (list<const CDensityPrimitive*>::const_iterator ppDensityPrimitive = m_DensityPrimitive.begin(); ppDensityPrimitive != EndDensityPrimitives; ++ppDensityPrimitive)
                                {
                                    Density += (*ppDensityPrimitive)->GetDensity(X);
                                }
                                return Density;
                            }
                            break;
                        }
                    return _REAL_ZERO_;
                }

                CDensityCell CDensityAggregation::GetMaximalDensityCell(const CDensityPrimitive::ConditioningMode ConditioningMode, const CContinuousBoundingBox3D& BBox, const real DiscretizationInterval, const real MinimalDensity) const
                {
                    CDensityCell MaximalDensityCell;
                    if (m_DensityPrimitive.size())
                    {
                        list<const CDensityPrimitive*>::const_iterator EndDensityPrimitives = m_DensityPrimitive.end();
                        list<const CDensityPrimitive*>::const_iterator BeginDensityPrimitive = m_DensityPrimitive.begin();
                        CContinuousBoundingBox3D DensityConditionedBBox = GetDensityBoundingBox(ConditioningMode, MinimalDensity);
                        DensityConditionedBBox.GetIntersection(BBox, DensityConditionedBBox);
                        if (!DensityConditionedBBox.IsEmpty())
                        {
                            const CVector3D& Basis = DensityConditionedBBox.GetMinPoint();
                            const CVector3D Delta = DensityConditionedBBox.GetMaxPoint() - Basis;
                            const uint Width = Delta.GetX() / DiscretizationInterval;
                            const uint Height = Delta.GetY() / DiscretizationInterval;
                            const uint Depth = Delta.GetZ() / DiscretizationInterval;
                            CVector3D Location = Basis;
                            CVector3D MaximalDensityLocation = CVector3D::s_Point_At_Plus_Infinity_3D;
                            real MaximalDensity = _REAL_ZERO_;
                            switch (ConditioningMode)
                            {
                                case CDensityPrimitive::eConjunctive:
                                    for (uint Z = 0; Z < Depth; ++Z, Location.AddOffsetZ(DiscretizationInterval), Location.SetY(Basis.GetY()))
                                        for (uint Y = 0; Y < Height; ++Y, Location.AddOffsetY(DiscretizationInterval), Location.SetX(Basis.GetX()))
                                            for (uint X = 0; X < Width; ++X, Location.AddOffsetX(DiscretizationInterval))
                                            {
                                                real Density = _REAL_ONE_;
                                                for (list<const CDensityPrimitive*>::const_iterator ppDensityPrimitive = BeginDensityPrimitive; ppDensityPrimitive != EndDensityPrimitives; ++ppDensityPrimitive)
                                                {
                                                    Density *= (*ppDensityPrimitive)->GetDensity(Location);
                                                    if (Density <= MaximalDensity)
                                                    {
                                                        break;
                                                    }
                                                }
                                                if (Density > MaximalDensity)
                                                {
                                                    MaximalDensity = Density;
                                                    MaximalDensityLocation = Location;
                                                }
                                            }
                                    break;
                                case CDensityPrimitive::eDisjunctive:
                                    for (uint Z = 0; Z < Depth; ++Z, Location.AddOffsetZ(DiscretizationInterval), Location.SetY(Basis.GetY()))
                                        for (uint Y = 0; Y < Height; ++Y, Location.AddOffsetY(DiscretizationInterval), Location.SetX(Basis.GetX()))
                                            for (uint X = 0; X < Width; ++X, Location.AddOffsetX(DiscretizationInterval))
                                            {
                                                real Density = _REAL_ZERO_;
                                                for (list<const CDensityPrimitive*>::const_iterator ppDensityPrimitive = BeginDensityPrimitive; ppDensityPrimitive != EndDensityPrimitives; ++ppDensityPrimitive)
                                                {
                                                    Density += (*ppDensityPrimitive)->GetDensity(Location);
                                                }
                                                if (Density > MaximalDensity)
                                                {
                                                    MaximalDensity = Density;
                                                    MaximalDensityLocation = Location;
                                                }
                                            }
                                    break;
                            }
                            MaximalDensityCell.SetValue(MaximalDensityLocation, MaximalDensity);
                        }
                    }
                    return MaximalDensityCell;
                }

                uint CDensityAggregation::GetTotalDensityPrimitives() const
                {
                    return m_DensityPrimitive.size();
                }

                const list<const CDensityPrimitive*>& CDensityAggregation::GetDensityPrimitives() const
                {
                    return m_DensityPrimitive;
                }
            }
        }
    }
}
