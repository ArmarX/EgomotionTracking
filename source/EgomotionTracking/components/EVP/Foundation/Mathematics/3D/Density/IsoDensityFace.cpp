/*
 * IsoDensityFace.cpp
 *
 *  Created on: Apr 11, 2012
 *      Author: david
 */

#include "IsoDensityFace.h"
#include "IsoDensityVertex.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                CIsoDensityFace::CIsoDensityFace(CIsoDensityVertex* pIsoDensityVertexA, CIsoDensityVertex* pIsoDensityVertexB, CIsoDensityVertex* pIsoDensityVertexC) :
                    m_pIsoDensityVertexA(pIsoDensityVertexA), m_pIsoDensityVertexB(pIsoDensityVertexB), m_pIsoDensityVertexC(pIsoDensityVertexC), m_Normal(), m_Area(_REAL_ZERO_)
                {
                    const CVector3D AB = m_pIsoDensityVertexB->GetLocation() - m_pIsoDensityVertexA->GetLocation();
                    const CVector3D AC = m_pIsoDensityVertexC->GetLocation() - m_pIsoDensityVertexA->GetLocation();
                    m_Normal = AB.VectorProduct(AC);
                    m_Area = m_Normal.Normalize() * _REAL_HALF_;
                    m_pIsoDensityVertexA->AddIsoDensityFace(this);
                    m_pIsoDensityVertexB->AddIsoDensityFace(this);
                    m_pIsoDensityVertexC->AddIsoDensityFace(this);
                }

                CIsoDensityFace::~CIsoDensityFace()
                    = default;

                uint CIsoDensityFace::GetIndexIsoDensityVertexA() const
                {
                    return m_pIsoDensityVertexA->GetIndex();
                }

                uint CIsoDensityFace::GetIndexIsoDensityVertexB() const
                {
                    return m_pIsoDensityVertexB->GetIndex();
                }

                uint CIsoDensityFace::GetIndexIsoDensityVertexC() const
                {
                    return m_pIsoDensityVertexC->GetIndex();
                }

                const CIsoDensityVertex* CIsoDensityFace::GetIsoDensityVertexA() const
                {
                    return m_pIsoDensityVertexA;
                }

                const CIsoDensityVertex* CIsoDensityFace::GetIsoDensityVertexB() const
                {
                    return m_pIsoDensityVertexB;
                }

                const CIsoDensityVertex* CIsoDensityFace::GetIsoDensityVertexC() const
                {
                    return m_pIsoDensityVertexC;
                }

                const CVector3D& CIsoDensityFace::GetNorma() const
                {
                    return m_Normal;
                }

                real CIsoDensityFace::GetArea() const
                {
                    return m_Area;
                }

                CVector3D CIsoDensityFace::GetAreaWeightedNorma() const
                {
                    return m_Normal * m_Area;
                }
            }
        }
    }
}
