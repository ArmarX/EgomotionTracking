/*
 * DensityCell.cpp
 *
 *  Created on: Apr 3, 2012
 *      Author: david
 */

#include "DensityCell.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                CDensityCell::CDensityCell() :
                    m_Location(CVector3D::s_Point_At_Plus_Infinity_3D), m_Density(_REAL_ZERO_)
                {
                }

                CDensityCell::CDensityCell(const CDensityCell& DensityCell)
                    = default;

                CDensityCell::CDensityCell(const CVector3D& Location, const real Density) :
                    m_Location(Location), m_Density(Density)
                {
                }

                CDensityCell::~CDensityCell()
                    = default;

                void CDensityCell::SetValue(const CVector3D& Location, const real Density)
                {
                    m_Location = Location;
                    m_Density = Density;
                }

                void CDensityCell::ClearCell()
                {
                    m_Location = CVector3D::s_Point_At_Plus_Infinity_3D;
                    m_Density = _REAL_ZERO_;
                }

                const CVector3D& CDensityCell::GetLocation() const
                {
                    return m_Location;
                }

                real CDensityCell::GetDensity() const
                {
                    return m_Density;
                }
            }
        }
    }
}
