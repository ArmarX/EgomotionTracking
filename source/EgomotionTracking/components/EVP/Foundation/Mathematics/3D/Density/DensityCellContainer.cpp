/*
 * CDensityCellContainer.cpp
 *
 *  Created on: Apr 3, 2012
 *      Author: david
 */

#include "DensityCellContainer.h"
#include "DensityCellContainerLink.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                CDensityCellContainer::CDensityCellContainer() :
                    CDensityCell(), m_Status(eUndetermined)
                {
                }

                CDensityCellContainer::~CDensityCellContainer()
                    = default;

                CDensityCellContainer::Status CDensityCellContainer::GetStatus() const
                {
                    return m_Status;
                }

                void CDensityCellContainer::SetValue(const CVector3D& Location, const real Density)
                {
                    m_Location = Location;
                    m_Density = Density;
                    m_Status = eUnsegmented;
                }

                bool CDensityCellContainer::Segment(const real ThresholdDensity)
                {
                    if (m_Density >= ThresholdDensity)
                    {
                        m_Status = eInner;
                        return true;
                    }
                    else
                    {
                        m_Status = eOutter;
                        return false;
                    }
                }

                bool CDensityCellContainer::IsCore() const
                {
                    return !m_DensityCellContainerLinks.size();
                }

                bool CDensityCellContainer::IsBoundary() const
                {
                    return m_DensityCellContainerLinks.size();
                }

                bool CDensityCellContainer::IsInner() const
                {
                    return (m_Status == eInner);
                }

                bool CDensityCellContainer::IsInnerCore() const
                {
                    return (!m_DensityCellContainerLinks.size()) && (m_Status == eInner);
                }

                bool CDensityCellContainer::IsInnerBoundary() const
                {
                    return m_DensityCellContainerLinks.size() && (m_Status == eInner);
                }

                bool CDensityCellContainer::IsOutter() const
                {
                    return (m_Status == eOutter);
                }

                bool CDensityCellContainer::IsOutterCore() const
                {
                    return (!m_DensityCellContainerLinks.size()) && (m_Status == eOutter);
                }

                bool CDensityCellContainer::IsOutterBoundary() const
                {
                    return m_DensityCellContainerLinks.size() && (m_Status == eOutter);
                }

                bool CDensityCellContainer::GetRelativeIndexingcoordinates(const CDensityCellContainer* const pBaseBuffer, const uint Width, const uint PageSize, uint& X, uint& Y, uint& Z) const
                {
                    const int FullOffset = this - pBaseBuffer;
                    if (FullOffset >= 0)
                    {
                        Z = FullOffset / PageSize;
                        const int SubPageOffset = FullOffset - (Z * PageSize);
                        Y = SubPageOffset / Width;
                        X = SubPageOffset - (Y * Width);
                        return true;
                    }
                    return false;
                }

                void CDensityCellContainer::AddDensityCellContainerLink(CDensityCellContainerLink* pDensityCellContainerEdge)
                {
                    m_DensityCellContainerLinks.push_back(pDensityCellContainerEdge);
                }

                void CDensityCellContainer::ClearDensityCellContainerLinks()
                {
                    m_DensityCellContainerLinks.clear();
                }

                bool CDensityCellContainer::IsSingleLinked() const
                {
                    return m_DensityCellContainerLinks.size() == 1;
                }

                bool CDensityCellContainer::IsMulitpleLinked() const
                {
                    return m_DensityCellContainerLinks.size() > 1;
                }

                uint CDensityCellContainer::GetTotalDensityCellContainerLinks() const
                {
                    return m_DensityCellContainerLinks.size();
                }

                const list<CDensityCellContainerLink*>& CDensityCellContainer::GetDensityCellContainerLinks() const
                {
                    return m_DensityCellContainerLinks;
                }

                CDensityCellContainerLink* CDensityCellContainer::GetDensityCellContainerLink(const CDensityCellContainer* pDensityCellContainer)
                {
                    if (pDensityCellContainer && (pDensityCellContainer != this) && m_DensityCellContainerLinks.size() && (pDensityCellContainer->m_Status != m_Status))
                    {
                        list<CDensityCellContainerLink*>::const_iterator EndDensityCellContainerEdges = m_DensityCellContainerLinks.end();
                        for (list<CDensityCellContainerLink*>::const_iterator ppDensityCellContainerEdge = m_DensityCellContainerLinks.begin(); ppDensityCellContainerEdge != EndDensityCellContainerEdges; ++ppDensityCellContainerEdge)
                            if ((*ppDensityCellContainerEdge)->GetComplementDensityCellContainer(this) == pDensityCellContainer)
                            {
                                return *ppDensityCellContainerEdge;
                            }
                        return nullptr;
                    }
                    return nullptr;
                }

                bool CDensityCellContainer::GetConnectingLinkIsoDensityLocation(const CDensityCellContainer* pDensityCellContainer, CVector3D& Location) const
                {
                    if (pDensityCellContainer && (pDensityCellContainer != this) && m_DensityCellContainerLinks.size() && (pDensityCellContainer->m_Status != m_Status))
                    {
                        list<CDensityCellContainerLink*>::const_iterator EndDensityCellContainerEdges = m_DensityCellContainerLinks.end();
                        for (list<CDensityCellContainerLink*>::const_iterator ppDensityCellContainerEdge = m_DensityCellContainerLinks.begin(); ppDensityCellContainerEdge != EndDensityCellContainerEdges; ++ppDensityCellContainerEdge)
                            if ((*ppDensityCellContainerEdge)->GetComplementDensityCellContainer(this) == pDensityCellContainer)
                            {
                                Location = (*ppDensityCellContainerEdge)->GetLocation();
                                return true;
                            }
                    }
                    return false;
                }

                void CDensityCellContainer::ClearCell()
                {
                    m_Location = CVector3D::s_Point_At_Plus_Infinity_3D;
                    m_Density = _REAL_ZERO_;
                    m_Status = eUndetermined;
                }

                void CDensityCellContainer::ClearLinks()
                {
                    m_DensityCellContainerLinks.clear();
                }
            }
        }
    }
}
