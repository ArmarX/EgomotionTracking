/*
 * IsoDensityFace.h
 *
 *  Created on: Apr 11, 2012
 *      Author: david
 */

#pragma once

#include "../Vector3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                class CIsoDensityVertex;

                class CIsoDensityFace
                {
                public:

                    CIsoDensityFace(CIsoDensityVertex* pIsoDensityVertexA, CIsoDensityVertex* pIsoDensityVertexB, CIsoDensityVertex* pIsoDensityVertexC);
                    virtual ~CIsoDensityFace();

                    uint GetIndexIsoDensityVertexA() const;
                    uint GetIndexIsoDensityVertexB() const;
                    uint GetIndexIsoDensityVertexC() const;
                    const CIsoDensityVertex* GetIsoDensityVertexA() const;
                    const CIsoDensityVertex* GetIsoDensityVertexB() const;
                    const CIsoDensityVertex* GetIsoDensityVertexC() const;
                    const CVector3D& GetNorma() const;
                    real GetArea() const;
                    CVector3D GetAreaWeightedNorma() const;

                protected:

                    CIsoDensityVertex* m_pIsoDensityVertexA;
                    CIsoDensityVertex* m_pIsoDensityVertexB;
                    CIsoDensityVertex* m_pIsoDensityVertexC;
                    CVector3D m_Normal;
                    real m_Area;
                };
            }
        }
    }
}

