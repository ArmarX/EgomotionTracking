/*
 * DensityCellContainerLink.h
 *
 *  Created on: Apr 3, 2012
 *      Author: david
 */

#pragma once

#include "DensityCell.h"
#include "IsoDensityVertex.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                class CDensityCellContainer;

                class CDensityCellContainerLink: public CDensityCell
                {
                public:

                    CDensityCellContainerLink(const uint Index, CDensityCellContainer* pInnerDensityCellContainer, CDensityCellContainer* pOutterDensityCellContainer, const CVector3D& Location, const CVector3D& Normal, const real Density);
                    virtual ~CDensityCellContainerLink();

                    void SetIsoDensityVertex(CIsoDensityVertex* pIsoDensityVertex);
                    CIsoDensityVertex* GetIsoDensityVertex();

                    const CVector3D& GetNormal() const;
                    const CDensityCellContainer* GetInnerDensityCellContainer() const;
                    const CDensityCellContainer* GetOutterDensityCellContainer() const;
                    const CDensityCellContainer* GetComplementDensityCellContainer(const CDensityCellContainer* pDensityCellContainer) const;

                protected:

                    CVector3D m_Normal;
                    CIsoDensityVertex* m_pIsoDensityVertex;
                    CDensityCellContainer* m_pInnerDensityCellContainer;
                    CDensityCellContainer* m_pOutterDensityCellContainer;
                };
            }
        }
    }
}

