/*
 * DensityCellGrid.h
 *
 *  Created on: Apr 3, 2012
 *      Author: david
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "DensityPrimitive.h"
#include "DensityCellContainer.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                class CDensityCellContainerLink;

                class CDensityCellGrid
                {
                public:

                    CDensityCellGrid();
                    virtual ~CDensityCellGrid();

                    bool SetConfiguration(const CContinuousBoundingBox3D& BoundingBox, const real DiscretizationResolution);
                    bool LoadDensity(const list<const CDensityPrimitive*>& DensityPrimitives, const CDensityPrimitive::ConditioningMode Mode);
                    bool ExtractIsoDensityStructure(const real ThresholdDensity);

                    CDensityPrimitive::ConditioningMode GetConditioningMode() const;
                    real GetDiscretizationResolution() const;
                    bool IsContouringReady() const;
                    uint GetWidth() const;
                    uint GetHeight() const;
                    uint GetDepth() const;
                    uint GetTotalSliceContainers() const;
                    uint GetTotalGridContainers() const;
                    const CVector3D& GetBasePoint() const;
                    const CVector3D& GetCellDeltaVector() const;
                    const CDensityCellContainer* GetReadOnlyDensityCellGrid() const;
                    CDensityCellContainer* GetWritableDensityCellGrid();
                    const list<CDensityCellContainerLink*>& GetDensityCellContainerLinks() const;
                    real GetCellVolume() const;
                    real GetInnerCoreVolume() const;
                    real GetBoundaryVolume() const;

                    void ClearCellGrid();

                protected:

                    CVector3D EstimateConjunctiveNormal(const CVector3D& X) const;
                    CVector3D EstimateDisjunctiveNormal(const CVector3D& X) const;
                    CDensityCellContainerLink* CreateConjunctiveLink(const uint Index, CDensityCellContainer* pInnerDensityCellContainer, CDensityCellContainer* pOutterDensityCellContainer, const real ThresholdDensity);
                    CDensityCellContainerLink* CreateDisjunctiveLink(const uint Index, CDensityCellContainer* pInnerDensityCellContainer, CDensityCellContainer* pOutterDensityCellContainer, const real ThresholdDensity);
                    void ClearDensityCellContainerLinks();

                    list<const CDensityPrimitive*> m_DensityPrimitives;
                    CDensityPrimitive::ConditioningMode m_ConditioningMode;
                    real m_DiscretizationResolution;
                    uint m_Width;
                    uint m_Height;
                    uint m_Depth;
                    uint m_TotalSliceContainers;
                    uint m_TotalGridContainers;
                    CVector3D m_BasePoint;
                    CVector3D m_CellDeltaVector;
                    CDensityCellContainer* m_pDensityCellGrid;
                    list<CDensityCellContainerLink*> m_DensityCellContainerLinks;
                    uint m_TotalInnerCoreCells;
                    uint m_TotalBoundaryCells;
                };
            }
        }
    }
}

