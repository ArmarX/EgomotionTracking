/*
 * DensityCellContainerLink.cpp
 *
 *  Created on: Apr 3, 2012
 *      Author: david
 */

#include "DensityCellContainerLink.h"
#include "DensityCellContainer.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                CDensityCellContainerLink::CDensityCellContainerLink(const uint Index, CDensityCellContainer* pInnerDensityCellContainer, CDensityCellContainer* pOutterDensityCellContainer, const CVector3D& Location, const CVector3D& Normal, const real Density) :
                    CDensityCell(Location, Density), m_Normal(Normal), m_pIsoDensityVertex(nullptr), m_pInnerDensityCellContainer(pInnerDensityCellContainer), m_pOutterDensityCellContainer(pOutterDensityCellContainer)
                {
                    m_pInnerDensityCellContainer->AddDensityCellContainerLink(this);
                    m_pOutterDensityCellContainer->AddDensityCellContainerLink(this);
                }

                CDensityCellContainerLink::~CDensityCellContainerLink()
                {
                    m_pInnerDensityCellContainer->ClearDensityCellContainerLinks();
                    m_pOutterDensityCellContainer->ClearDensityCellContainerLinks();
                }

                void CDensityCellContainerLink::SetIsoDensityVertex(CIsoDensityVertex* pIsoDensityVertex)
                {
                    m_pIsoDensityVertex = pIsoDensityVertex;
                }

                CIsoDensityVertex* CDensityCellContainerLink::GetIsoDensityVertex()
                {
                    return m_pIsoDensityVertex;
                }

                const CVector3D& CDensityCellContainerLink::GetNormal() const
                {
                    return m_Normal;
                }

                const CDensityCellContainer* CDensityCellContainerLink::GetInnerDensityCellContainer() const
                {
                    return m_pInnerDensityCellContainer;
                }

                const CDensityCellContainer* CDensityCellContainerLink::GetOutterDensityCellContainer() const
                {
                    return m_pOutterDensityCellContainer;
                }

                const CDensityCellContainer* CDensityCellContainerLink::GetComplementDensityCellContainer(const CDensityCellContainer* pDensityCellContainer) const
                {
                    if (pDensityCellContainer == m_pInnerDensityCellContainer)
                    {
                        return m_pOutterDensityCellContainer;
                    }
                    if (pDensityCellContainer == m_pOutterDensityCellContainer)
                    {
                        return m_pInnerDensityCellContainer;
                    }
                    return nullptr;
                }
            }
        }
    }
}
