/*
 * IsoDensitySurface.h
 *
 *  Created on: Apr 11, 2012
 *      Author: david
 */

#pragma once

//EVP
#include "DensityCellGrid.h"
#include "DensityCellContainer.h"
#include "DensityCellContainerLink.h"
#include "IsoDensityVertex.h"
#include "IsoDensityFace.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                class CIsoDensitySurface
                {
                public:

                    CIsoDensitySurface();
                    virtual ~CIsoDensitySurface();

                    bool Generate(CDensityCellGrid* pDensityCellGrid);

                    const CDensityCellGrid* GetDensityCellGrid() const;
                    const list<CIsoDensityVertex*>& GetIsoDensityVertices() const;
                    const list<CIsoDensityFace*>& GetIsoDensityFaces() const;

                protected:

                    CDensityCellGrid* m_pDensityCellGrid;
                    list<CIsoDensityVertex*> m_IsoDensityVertices;
                    list<CIsoDensityFace*> m_IsoDensityFaces;

                    void CreateIsoDensityVertices();
                    void ClearIsoDensityVertices();

                    void CreateIsoDensityFaces();
                    void ClearIsoDensityFaces();

                    static bool s_IsMarchingCubesLUTLoaded;
                    struct MarchingCubesConfiguration
                    {
                        struct TringleVertexDeltas
                        {
                            uint m_A0;
                            uint m_A1;
                            uint m_B0;
                            uint m_B1;
                            uint m_C0;
                            uint m_C1;
                        };
                        list<TringleVertexDeltas> m_TringleFaces;
                    };
                    static void LoadMarchingCubesConfigurationsLUT();
                    static MarchingCubesConfiguration s_MarchingCubesLUT[256];
                };
            }
        }
    }
}

