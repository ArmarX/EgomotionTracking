/*
 * DensitySphere3D.h
 *
 *  Created on: Apr 2, 2012
 *      Author: david
 */

#pragma once

#include "../DensityPrimitive.h"
#include "../../Geometry/Sphere3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Geometry
                {
                    class CDensitySphere3D: public CDensityPrimitive, public _3D::Geometry::CSphere3D
                    {
                    public:

                        CDensitySphere3D(const CVector3D& Center, const real Radius, const CDensityPrimitive::DensityProfileTypeId ProfileTypeId);
                        ~CDensitySphere3D() override;
                    };
                }
            }
        }
    }
}

