/*
 * CDensityCellContainer.h
 *
 *  Created on: Apr 3, 2012
 *      Author: david
 */

#pragma once

#include "DensityCell.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                class CDensityCellContainerLink;

                class CDensityCellContainer: public CDensityCell
                {
                public:

                    enum Status
                    {
                        eUndetermined = -1, eUnsegmented = 0, eOutter = 1, eInner = 2
                    };

                    CDensityCellContainer();
                    ~CDensityCellContainer();

                    Status GetStatus() const;

                    void SetValue(const CVector3D& Location, const real Density) override;
                    bool Segment(const real ThresholdDensity);

                    bool IsCore() const;
                    bool IsBoundary() const;

                    bool IsInner() const;
                    bool IsInnerCore() const;
                    bool IsInnerBoundary() const;

                    bool IsOutter() const;
                    bool IsOutterCore() const;
                    bool IsOutterBoundary() const;

                    bool GetRelativeIndexingcoordinates(const CDensityCellContainer* const pBaseBuffer, const uint Width, const uint PageSize, uint& X, uint& Y, uint& Z) const;

                    void AddDensityCellContainerLink(CDensityCellContainerLink* pDensityCellContainerEdge);
                    void ClearDensityCellContainerLinks();

                    bool IsSingleLinked() const;
                    bool IsMulitpleLinked() const;
                    uint GetTotalDensityCellContainerLinks() const;
                    const list<CDensityCellContainerLink*>& GetDensityCellContainerLinks() const;
                    CDensityCellContainerLink* GetDensityCellContainerLink(const CDensityCellContainer* pDensityCellContainer);
                    bool GetConnectingLinkIsoDensityLocation(const CDensityCellContainer* pDensityCellContainer, CVector3D& Location) const;

                    void ClearCell() override;
                    virtual void ClearLinks();

                protected:

                    Status m_Status;
                    list<CDensityCellContainerLink*> m_DensityCellContainerLinks;
                };
            }
        }
    }
}

