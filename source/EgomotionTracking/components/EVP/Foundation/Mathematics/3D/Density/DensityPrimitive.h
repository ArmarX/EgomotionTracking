/*
 * DensityPrimitive.h
 *
 *  Created on: Apr 2, 2012
 *      Author: david
 */

#pragma once

//EVP
#include "../../../../VisualSpace/Common/ContinuousBoundingBox3D.h"
#include "../Vector3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                class CDensityPrimitive
                {
                public:

                    enum DensityTypeId
                    {
                        eDensitySphere
                    };

                    enum DensityProfileTypeId
                    {
                        eFreeForm, eGaussian
                    };

                    enum ConditioningMode
                    {
                        eConjunctive, eDisjunctive
                    };

                    CDensityPrimitive(const DensityTypeId TypeId, const DensityProfileTypeId ProfileTypeId);
                    virtual ~CDensityPrimitive();

                    DensityTypeId GetDensityTypeId() const;
                    DensityProfileTypeId GetDensityProfileTypeId() const;

                    virtual  real GetDensity(const CVector3D& X) const = 0;
                    virtual  real GetLogDensity(const CVector3D& X) const = 0;
                    virtual  CVector3D GetDensityGradient(const CVector3D& X) const = 0;
                    virtual  CVector3D GetDensityGradient(const CVector3D& X, real& Density) const = 0;
                    virtual  CContinuousBoundingBox3D GetDensityBoundingBox(const real MinimalDensity) const = 0;

                protected:

                    const DensityTypeId m_DensityTypeId;
                    const DensityProfileTypeId m_DensityProfileTypeId;
                };
            }
        }
    }
}

