/*
 * IsoDensityVertex.cpp
 *
 *  Created on: Apr 11, 2012
 *      Author: david
 */

#include "IsoDensityVertex.h"
#include "IsoDensityFace.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                CIsoDensityVertex::CIsoDensityVertex(const uint Index, const CVector3D& Location, const CVector3D& Normal, const real Density) :
                    CDensityCell(Location, Density), m_Index(Index), m_Normal(Normal)
                {
                }

                CIsoDensityVertex::~CIsoDensityVertex()
                    = default;

                void CIsoDensityVertex::AddIsoDensityFace(CIsoDensityFace* pIsoDensityFace)
                {
                    m_IsoDensityFaces.push_back(pIsoDensityFace);
                }

                uint CIsoDensityVertex::GetIndex() const
                {
                    return m_Index;
                }

                CVector3D CIsoDensityVertex::GetNormal() const
                {
                    return m_Normal;
                }

                const list<CIsoDensityFace*>& CIsoDensityVertex::GetIsoDensityFaces() const
                {
                    return m_IsoDensityFaces;
                }
            }
        }
    }
}
