/*
 * DensitySphere3D.cpp
 *
 *  Created on: Apr 2, 2012
 *      Author: david
 */

#include "DensitySphere3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Geometry
                {
                    CDensitySphere3D::CDensitySphere3D(const CVector3D& Center, const real Radius, const CDensityPrimitive::DensityProfileTypeId ProfileTypeId) :
                        CDensityPrimitive(CDensityPrimitive::eDensitySphere, ProfileTypeId), _3D::Geometry::CSphere3D(Center, Radius, nullptr, nullptr)
                    {
                    }

                    CDensitySphere3D::~CDensitySphere3D()
                        = default;
                }
            }
        }
    }
}
