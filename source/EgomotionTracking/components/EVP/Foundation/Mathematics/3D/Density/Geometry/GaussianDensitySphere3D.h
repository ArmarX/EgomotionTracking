/*
 * GaussianDensitySphere3D.h
 *
 *  Created on: Apr 2, 2012
 *      Author: david
 */

#pragma once

#include "../../../1D/Common1D.h"
#include "DensitySphere3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Geometry
                {
                    class CGaussianDensitySphere3D: public CDensitySphere3D
                    {
                    public:

                        CGaussianDensitySphere3D(const CVector3D& Center, const real Radius, const real StandarDeviation);
                        ~CGaussianDensitySphere3D() override;

                        bool SetStandarDeviation(const real StandarDeviation);
                        real GetStandarDeviation() const;

                        real GetDensity(const CVector3D& X) const override;
                        real GetLogDensity(const CVector3D& X) const override;
                        CVector3D GetDensityGradient(const CVector3D& X) const override;
                        CVector3D GetDensityGradient(const CVector3D& X, real& Density) const override;
                        CContinuousBoundingBox3D GetDensityBoundingBox(const real MinimalDensity) const override;

                    protected:

                        real m_StandarDeviation;
                        real m_ExponentFactor;
                        real m_DoubleExponentFactor;
                    };
                }
            }
        }
    }
}

