/*
 * IsoDensityVertex.h
 *
 *  Created on: Apr 11, 2012
 *      Author: david
 */

#pragma once

#include "DensityCell.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                class CIsoDensityFace;

                class CIsoDensityVertex: public CDensityCell
                {
                public:

                    CIsoDensityVertex(const uint Index, const CVector3D& Location, const CVector3D& Normal, const real Density);
                    virtual ~CIsoDensityVertex();

                    void AddIsoDensityFace(CIsoDensityFace* pIsoDensityFace);

                    uint GetIndex() const;
                    CVector3D GetNormal() const;
                    const list<CIsoDensityFace*>& GetIsoDensityFaces() const;

                protected:

                    uint m_Index;
                    CVector3D m_Normal;
                    list<CIsoDensityFace*> m_IsoDensityFaces;
                };
            }
        }
    }
}

