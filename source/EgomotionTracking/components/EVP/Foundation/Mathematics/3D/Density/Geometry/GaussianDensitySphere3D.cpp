/*
 * GaussianDensitySphere3D.cpp
 *
 *  Created on: Apr 2, 2012
 *      Author: david
 */

#include "GaussianDensitySphere3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                namespace Geometry
                {
                    CGaussianDensitySphere3D::CGaussianDensitySphere3D(const CVector3D& Center, const real Radius, const real StandarDeviation) :
                        CDensitySphere3D(Center, Radius, CDensityPrimitive::eGaussian), m_StandarDeviation(StandarDeviation), m_ExponentFactor(Mathematics::_1D::NormalDistribution::DetermineExponentFactor(m_StandarDeviation)), m_DoubleExponentFactor(_REAL_TWO_ * m_ExponentFactor)
                    {
                    }

                    CGaussianDensitySphere3D::~CGaussianDensitySphere3D()
                        = default;

                    bool CGaussianDensitySphere3D::SetStandarDeviation(const real StandarDeviation)
                    {
                        m_StandarDeviation = StandarDeviation;
                        m_ExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(m_StandarDeviation);
                        return m_StandarDeviation >= _REAL_ZERO_;
                    }

                    real CGaussianDensitySphere3D::GetStandarDeviation() const
                    {
                        return m_StandarDeviation;
                    }

                    real CGaussianDensitySphere3D::GetDensity(const CVector3D& X) const
                    {
                        const real OrientedDistanceToSurface = m_Center.GetDistance(X) - m_Radius;
                        return RealExp(m_ExponentFactor * OrientedDistanceToSurface * OrientedDistanceToSurface);
                    }

                    real CGaussianDensitySphere3D::GetLogDensity(const CVector3D& X) const
                    {
                        const real OrientedDistanceToSurface = m_Center.GetDistance(X) - m_Radius;
                        return OrientedDistanceToSurface * OrientedDistanceToSurface * m_ExponentFactor;
                    }

                    CVector3D CGaussianDensitySphere3D::GetDensityGradient(const CVector3D& X) const
                    {
                        const CVector3D Delta = X - m_Center;
                        const real DistanceToCenter = Delta.GetLength();
                        const real OrientedDistanceToSurface = DistanceToCenter - m_Radius;
                        return Delta * ((m_DoubleExponentFactor * OrientedDistanceToSurface * RealExp(m_ExponentFactor * OrientedDistanceToSurface * OrientedDistanceToSurface)) / DistanceToCenter);
                    }

                    CVector3D CGaussianDensitySphere3D::GetDensityGradient(const CVector3D& X, real& Density) const
                    {
                        const CVector3D Delta = X - m_Center;
                        const real DistanceToCenter = Delta.GetLength();
                        const real OrientedDistanceToSurface = DistanceToCenter - m_Radius;
                        Density = RealExp(m_ExponentFactor * OrientedDistanceToSurface * OrientedDistanceToSurface);
                        return Delta * ((m_DoubleExponentFactor * OrientedDistanceToSurface * Density) / DistanceToCenter);
                    }

                    CContinuousBoundingBox3D CGaussianDensitySphere3D::GetDensityBoundingBox(const real MinimalDensity) const
                    {
                        const real Deviation = Mathematics::_1D::NormalDistribution::DetermineDeviationAtDensity(TMax(MinimalDensity, _REAL_EPSILON_), m_StandarDeviation) + m_Radius;
                        CVector3D Delta(Deviation, Deviation, Deviation);
                        return CContinuousBoundingBox3D(m_Center - Delta, m_Center + Delta);
                    }
                }
            }
        }
    }
}
