/*
 * DensityCell.h
 *
 *  Created on: Apr 3, 2012
 *      Author: david
 */

#pragma once

//EVP
#include "../Vector3D.h"
#include "../../../../VisualSpace/Common/ContinuousBoundingBox3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                class CDensityCell
                {
                public:

                    CDensityCell();
                    CDensityCell(const CDensityCell& DensityCell);
                    CDensityCell(const CVector3D& Location, const real Density);
                    ~CDensityCell();

                    virtual void SetValue(const CVector3D& Location, const real Density);
                    virtual void ClearCell();

                    const CVector3D& GetLocation() const;
                    real GetDensity() const;

                protected:

                    CVector3D m_Location;
                    real m_Density;
                };
            }
        }
    }
}

