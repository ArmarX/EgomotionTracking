/*
 * DensityAggregation.h
 *
 *  Created on: Apr 2, 2012
 *      Author: david
 */

#pragma once

#include "DensityPrimitive.h"
#include "DensityCell.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                class CDensityAggregation
                {
                public:

                    CDensityAggregation(const bool DensityPrimitiveOwnerShip);
                    virtual ~CDensityAggregation();

                    bool AddDensityPrimitive(const CDensityPrimitive* pDensityPrimitive);
                    void ClearDensityPrimitives();

                    CContinuousBoundingBox3D GetDensityBoundingBox(const CDensityPrimitive::ConditioningMode ConditioningMode, const real MinimalDensity) const;
                    real GetDensity(const CDensityPrimitive::ConditioningMode ConditioningMode, const CVector3D& X) const;
                    CDensityCell GetMaximalDensityCell(const CDensityPrimitive::ConditioningMode ConditioningMode, const CContinuousBoundingBox3D& BBox, const real DiscretizationInterval, const real MinimalDensity) const;

                    uint GetTotalDensityPrimitives() const;
                    const list<const CDensityPrimitive*>& GetDensityPrimitives() const;

                protected:

                    const bool m_DensityPrimitiveOwnerShip;
                    list<const CDensityPrimitive*> m_DensityPrimitive;
                };
            }
        }
    }
}

