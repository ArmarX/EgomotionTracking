/*
 * DensityPrimitive.cpp
 *
 *  Created on: Apr 2, 2012
 *      Author: david
 */

#include "DensityPrimitive.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _3D
        {
            namespace Density
            {
                CDensityPrimitive::CDensityPrimitive(const DensityTypeId TypeId, const DensityProfileTypeId ProfileTypeId) :
                    m_DensityTypeId(TypeId), m_DensityProfileTypeId(ProfileTypeId)
                {
                }

                CDensityPrimitive::~CDensityPrimitive()
                    = default;

                CDensityPrimitive::DensityTypeId CDensityPrimitive::GetDensityTypeId() const
                {
                    return m_DensityTypeId;
                }

                CDensityPrimitive::DensityProfileTypeId CDensityPrimitive::GetDensityProfileTypeId() const
                {
                    return m_DensityProfileTypeId;
                }
            }
        }
    }
}
