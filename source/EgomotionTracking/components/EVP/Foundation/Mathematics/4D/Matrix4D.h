/*
 * Matrix4D.h
 *
 *  Created on: 14.03.2012
 *      Author: vollert
 */

#pragma once

#include "../../../GlobalSettings.h"
#include "../3D/Vector3D.h"
#include "../3D/Matrix3D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _4D
        {
            class CMatrix4D
            {
            public:

                CMatrix4D();
                CMatrix4D(const real E00, const real E01, const real E02, const real E03, const real E10, const real E11, const real E12, const real E13, const real E20, const real E21, const real E22, const real E23, const real E30, const real E31, const real E32, const real E33);
                CMatrix4D(const CMatrix4D& Matrix);
                CMatrix4D(const CMatrix4D* pMatrix);
                CMatrix4D(const _3D::CMatrix3D& Rotation, const _3D::CVector3D& Translation, const real Scale = _REAL_ONE_);
                virtual ~CMatrix4D();

                void MakeIdentity();
                real GetDeterminat() const;
                CMatrix4D GetInverse(bool* pSingularity = NULL) const;
                CMatrix4D GetTranspose() const;
                void SetByElements(const real E00, const real E01, const real E02, const real E03, const real E10, const real E11, const real E12, const real E13, const real E20, const real E21, const real E22, const real E23, const real E30, const real E31, const real E32, const real E33);
                void SetTransformation(const _3D::CMatrix3D& Rotation, const _3D::CVector3D& Translation, const real Scale = _REAL_ONE_);
                void Transform(const _3D::CVector3D& Point, _3D::CVector3D& TransformedPoint) const;

                real* operator[](const uint Row);
                const real* operator[](const uint Row) const;
                CMatrix4D& operator=(const CMatrix4D& Matrix);
                CMatrix4D& operator*=(const CMatrix4D& Matrix);
                CMatrix4D operator*(const CMatrix4D& Matrix) const;
                CMatrix4D operator*(const real Scalar);
                bool operator==(const CMatrix4D& Matrix) const;
                bool operator!=(const CMatrix4D& Matrix) const;
                string ToString(const uint Precision) const;

            protected:

                union MatrixStructure
                {
                    real m_Array[16];
                    real m_Elements[4][4];
                };
                MatrixStructure m_InnerMatrixStructure;
            };
        }
    }
}

