/*
 * Matrix4D.cpp
 *
 *  Created on: 14.03.2012
 *      Author: vollert
 */

#include "Matrix4D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _4D
        {
            CMatrix4D::CMatrix4D()
            {
                memset(&m_InnerMatrixStructure, 0, sizeof(MatrixStructure));
            }

            CMatrix4D::CMatrix4D(const real E00, const real E01, const real E02, const real E03, const real E10, const real E11, const real E12, const real E13, const real E20, const real E21, const real E22, const real E23, const real E30, const real E31, const real E32, const real E33)
            {
                m_InnerMatrixStructure.m_Elements[0][0] = E00;
                m_InnerMatrixStructure.m_Elements[0][1] = E01;
                m_InnerMatrixStructure.m_Elements[0][2] = E02;
                m_InnerMatrixStructure.m_Elements[0][3] = E03;
                m_InnerMatrixStructure.m_Elements[1][0] = E10;
                m_InnerMatrixStructure.m_Elements[1][1] = E11;
                m_InnerMatrixStructure.m_Elements[1][2] = E12;
                m_InnerMatrixStructure.m_Elements[1][3] = E13;
                m_InnerMatrixStructure.m_Elements[2][0] = E20;
                m_InnerMatrixStructure.m_Elements[2][1] = E21;
                m_InnerMatrixStructure.m_Elements[2][2] = E22;
                m_InnerMatrixStructure.m_Elements[2][3] = E23;
                m_InnerMatrixStructure.m_Elements[3][0] = E30;
                m_InnerMatrixStructure.m_Elements[3][1] = E31;
                m_InnerMatrixStructure.m_Elements[3][2] = E32;
                m_InnerMatrixStructure.m_Elements[3][3] = E33;
            }

            CMatrix4D::CMatrix4D(const CMatrix4D& Matrix)
            {
                memcpy(&m_InnerMatrixStructure, &Matrix.m_InnerMatrixStructure, sizeof(MatrixStructure));
            }

            CMatrix4D::CMatrix4D(const CMatrix4D* pMatrix)
            {
                memcpy(&m_InnerMatrixStructure, &(pMatrix->m_InnerMatrixStructure), sizeof(MatrixStructure));
            }

            CMatrix4D::CMatrix4D(const _3D::CMatrix3D& Rotation, const _3D::CVector3D& Translation, const real Scale)
            {
                SetTransformation(Rotation, Translation, Scale);
            }

            CMatrix4D::~CMatrix4D()
                = default;

            void CMatrix4D::MakeIdentity()
            {
                memset(&m_InnerMatrixStructure, 0, sizeof(MatrixStructure));
                for (uint i = 0; i < 4; ++i)
                {
                    m_InnerMatrixStructure.m_Elements[i][i] = _REAL_ONE_;
                }
            }

            real CMatrix4D::GetDeterminat() const
            {
                //Reference http://www.euclideanspace.com/maths/algebra/matrix/functions/inverse/fourD/index.htm
                const real m00 = m_InnerMatrixStructure.m_Elements[0][0];
                const real m01 = m_InnerMatrixStructure.m_Elements[0][1];
                const real m02 = m_InnerMatrixStructure.m_Elements[0][2];
                const real m03 = m_InnerMatrixStructure.m_Elements[0][3];
                const real m10 = m_InnerMatrixStructure.m_Elements[1][0];
                const real m11 = m_InnerMatrixStructure.m_Elements[1][1];
                const real m12 = m_InnerMatrixStructure.m_Elements[1][2];
                const real m13 = m_InnerMatrixStructure.m_Elements[1][3];
                const real m20 = m_InnerMatrixStructure.m_Elements[2][0];
                const real m21 = m_InnerMatrixStructure.m_Elements[2][1];
                const real m22 = m_InnerMatrixStructure.m_Elements[2][2];
                const real m23 = m_InnerMatrixStructure.m_Elements[2][3];
                const real m30 = m_InnerMatrixStructure.m_Elements[3][0];
                const real m31 = m_InnerMatrixStructure.m_Elements[3][1];
                const real m32 = m_InnerMatrixStructure.m_Elements[3][2];
                const real m33 = m_InnerMatrixStructure.m_Elements[3][3];
                return (m03 * m12 * m21 * m30 - m02 * m13 * m21 * m30 - m03 * m11 * m22 * m30 + m01 * m13 * m22 * m30 + m02 * m11 * m23 * m30 - m01 * m12 * m23 * m30 - m03 * m12 * m20 * m31 + m02 * m13 * m20 * m31 + m03 * m10 * m22 * m31 - m00 * m13 * m22 * m31 - m02 * m10 * m23 * m31 + m00 * m12 * m23 * m31 + m03 * m11 * m20 * m32 - m01 * m13 * m20 * m32 - m03 * m10 * m21 * m32 + m00 * m13 * m21 * m32 + m01 * m10 * m23 * m32 - m00 * m11 * m23 * m32 - m02 * m11 * m20 * m33 + m01 * m12 * m20 * m33 + m02 * m10 * m21 * m33 - m00 * m12 * m21 * m33 - m01 * m10 * m22 * m33 + m00 * m11 * m22 * m33);
            }

            CMatrix4D CMatrix4D::GetInverse(bool* pSingularity) const
            {
                CMatrix4D InverseMatrix;
                const real Determinant = GetDeterminat();
                if (RealAbs(Determinant) > _REAL_EPSILON_)
                {
                    if (pSingularity)
                    {
                        *pSingularity = false;
                    }
                    const real Normalization = _REAL_ONE_ / Determinant;
                    //Reference http://www.euclideanspace.com/maths/algebra/matrix/functions/inverse/fourD/index.htm
                    const real m00 = m_InnerMatrixStructure.m_Elements[0][0];
                    const real m01 = m_InnerMatrixStructure.m_Elements[0][1];
                    const real m02 = m_InnerMatrixStructure.m_Elements[0][2];
                    const real m03 = m_InnerMatrixStructure.m_Elements[0][3];
                    const real m10 = m_InnerMatrixStructure.m_Elements[1][0];
                    const real m11 = m_InnerMatrixStructure.m_Elements[1][1];
                    const real m12 = m_InnerMatrixStructure.m_Elements[1][2];
                    const real m13 = m_InnerMatrixStructure.m_Elements[1][3];
                    const real m20 = m_InnerMatrixStructure.m_Elements[2][0];
                    const real m21 = m_InnerMatrixStructure.m_Elements[2][1];
                    const real m22 = m_InnerMatrixStructure.m_Elements[2][2];
                    const real m23 = m_InnerMatrixStructure.m_Elements[2][3];
                    const real m30 = m_InnerMatrixStructure.m_Elements[3][0];
                    const real m31 = m_InnerMatrixStructure.m_Elements[3][1];
                    const real m32 = m_InnerMatrixStructure.m_Elements[3][2];
                    const real m33 = m_InnerMatrixStructure.m_Elements[3][3];
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[0][0] = (m12 * m23 * m31 - m13 * m22 * m31 + m13 * m21 * m32 - m11 * m23 * m32 - m12 * m21 * m33 + m11 * m22 * m33) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[0][1] = (m03 * m22 * m31 - m02 * m23 * m31 - m03 * m21 * m32 + m01 * m23 * m32 + m02 * m21 * m33 - m01 * m22 * m33) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[0][2] = (m02 * m13 * m31 - m03 * m12 * m31 + m03 * m11 * m32 - m01 * m13 * m32 - m02 * m11 * m33 + m01 * m12 * m33) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[0][3] = (m03 * m12 * m21 - m02 * m13 * m21 - m03 * m11 * m22 + m01 * m13 * m22 + m02 * m11 * m23 - m01 * m12 * m23) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[1][0] = (m13 * m22 * m30 - m12 * m23 * m30 - m13 * m20 * m32 + m10 * m23 * m32 + m12 * m20 * m33 - m10 * m22 * m33) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[1][1] = (m02 * m23 * m30 - m03 * m22 * m30 + m03 * m20 * m32 - m00 * m23 * m32 - m02 * m20 * m33 + m00 * m22 * m33) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[1][2] = (m03 * m12 * m30 - m02 * m13 * m30 - m03 * m10 * m32 + m00 * m13 * m32 + m02 * m10 * m33 - m00 * m12 * m33) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[1][3] = (m02 * m13 * m20 - m03 * m12 * m20 + m03 * m10 * m22 - m00 * m13 * m22 - m02 * m10 * m23 + m00 * m12 * m23) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[2][0] = (m11 * m23 * m30 - m13 * m21 * m30 + m13 * m20 * m31 - m10 * m23 * m31 - m11 * m20 * m33 + m10 * m21 * m33) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[2][1] = (m03 * m21 * m30 - m01 * m23 * m30 - m03 * m20 * m31 + m00 * m23 * m31 + m01 * m20 * m33 - m00 * m21 * m33) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[2][2] = (m01 * m13 * m30 - m03 * m11 * m30 + m03 * m10 * m31 - m00 * m13 * m31 - m01 * m10 * m33 + m00 * m11 * m33) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[2][3] = (m03 * m11 * m20 - m01 * m13 * m20 - m03 * m10 * m21 + m00 * m13 * m21 + m01 * m10 * m23 - m00 * m11 * m23) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[3][0] = (m12 * m21 * m30 - m11 * m22 * m30 - m12 * m20 * m31 + m10 * m22 * m31 + m11 * m20 * m32 - m10 * m21 * m32) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[3][1] = (m01 * m22 * m30 - m02 * m21 * m30 + m02 * m20 * m31 - m00 * m22 * m31 - m01 * m20 * m32 + m00 * m21 * m32) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[3][2] = (m02 * m11 * m30 - m01 * m12 * m30 - m02 * m10 * m31 + m00 * m12 * m31 + m01 * m10 * m32 - m00 * m11 * m32) * Normalization;
                    InverseMatrix.m_InnerMatrixStructure.m_Elements[3][3] = (m01 * m12 * m20 - m02 * m11 * m20 + m02 * m10 * m21 - m00 * m12 * m21 - m01 * m10 * m22 + m00 * m11 * m22) * Normalization;
                }
                else if (pSingularity)
                {
                    *pSingularity = true;
                }
                return InverseMatrix;
            }

            CMatrix4D CMatrix4D::GetTranspose() const
            {
                CMatrix4D TrasposedMatrix;
                for (uint r = 0; r < 4; ++r)
                    for (uint c = 0; c < 4; ++c)
                    {
                        TrasposedMatrix.m_InnerMatrixStructure.m_Elements[c][r] = m_InnerMatrixStructure.m_Elements[r][c];
                    }
                return TrasposedMatrix;
            }

            void CMatrix4D::SetByElements(const real E00, const real E01, const real E02, const real E03, const real E10, const real E11, const real E12, const real E13, const real E20, const real E21, const real E22, const real E23, const real E30, const real E31, const real E32, const real E33)
            {
                m_InnerMatrixStructure.m_Elements[0][0] = E00;
                m_InnerMatrixStructure.m_Elements[0][1] = E01;
                m_InnerMatrixStructure.m_Elements[0][2] = E02;
                m_InnerMatrixStructure.m_Elements[0][3] = E03;
                m_InnerMatrixStructure.m_Elements[1][0] = E10;
                m_InnerMatrixStructure.m_Elements[1][1] = E11;
                m_InnerMatrixStructure.m_Elements[1][2] = E12;
                m_InnerMatrixStructure.m_Elements[1][3] = E13;
                m_InnerMatrixStructure.m_Elements[2][0] = E20;
                m_InnerMatrixStructure.m_Elements[2][1] = E21;
                m_InnerMatrixStructure.m_Elements[2][2] = E22;
                m_InnerMatrixStructure.m_Elements[2][3] = E23;
                m_InnerMatrixStructure.m_Elements[3][0] = E30;
                m_InnerMatrixStructure.m_Elements[3][1] = E31;
                m_InnerMatrixStructure.m_Elements[3][2] = E32;
                m_InnerMatrixStructure.m_Elements[3][3] = E33;
            }

            void CMatrix4D::SetTransformation(const _3D::CMatrix3D& Rotation, const _3D::CVector3D& Translation, const real Scale)
            {
                memset(&m_InnerMatrixStructure, 0, sizeof(MatrixStructure));
                m_InnerMatrixStructure.m_Elements[0][0] = Rotation[0][0];
                m_InnerMatrixStructure.m_Elements[1][0] = Rotation[1][0];
                m_InnerMatrixStructure.m_Elements[2][0] = Rotation[2][0];
                m_InnerMatrixStructure.m_Elements[0][1] = Rotation[0][1];
                m_InnerMatrixStructure.m_Elements[1][1] = Rotation[1][1];
                m_InnerMatrixStructure.m_Elements[2][1] = Rotation[2][1];
                m_InnerMatrixStructure.m_Elements[0][2] = Rotation[0][2];
                m_InnerMatrixStructure.m_Elements[1][2] = Rotation[1][2];
                m_InnerMatrixStructure.m_Elements[2][2] = Rotation[2][2];
                m_InnerMatrixStructure.m_Elements[0][3] = Translation.GetX();
                m_InnerMatrixStructure.m_Elements[1][3] = Translation.GetY();
                m_InnerMatrixStructure.m_Elements[2][3] = Translation.GetZ();
                m_InnerMatrixStructure.m_Elements[3][3] = Scale;
            }

            real* CMatrix4D::operator[](const uint Row)
            {
                return m_InnerMatrixStructure.m_Elements[Row];
            }

            const real* CMatrix4D::operator[](const uint Row) const
            {
                return m_InnerMatrixStructure.m_Elements[Row];
            }

            CMatrix4D& CMatrix4D::operator=(const CMatrix4D& Matrix)
            {
                memcpy(&m_InnerMatrixStructure, &Matrix.m_InnerMatrixStructure, sizeof(MatrixStructure));
                return *this;
            }

            CMatrix4D& CMatrix4D::operator*=(const CMatrix4D& Matrix)
            {
                MatrixStructure TemporalMatrixStructure;
                for (uint r = 0; r < 4; ++r)
                    for (uint c = 0; c < 4; ++c)
                    {
                        TemporalMatrixStructure.m_Elements[r][c] = m_InnerMatrixStructure.m_Elements[r][0] * Matrix.m_InnerMatrixStructure.m_Elements[0][c] + m_InnerMatrixStructure.m_Elements[r][1] * Matrix.m_InnerMatrixStructure.m_Elements[1][c] + m_InnerMatrixStructure.m_Elements[r][2] * Matrix.m_InnerMatrixStructure.m_Elements[2][c] + m_InnerMatrixStructure.m_Elements[r][3] * Matrix.m_InnerMatrixStructure.m_Elements[3][c];
                    }
                memcpy(&m_InnerMatrixStructure, &TemporalMatrixStructure, sizeof(MatrixStructure));
                return *this;
            }

            CMatrix4D CMatrix4D::operator*(const CMatrix4D& Matrix) const
            {
                CMatrix4D ResultMatrix;
                for (uint r = 0; r < 4; ++r)
                    for (uint c = 0; c < 4; ++c)
                    {
                        ResultMatrix.m_InnerMatrixStructure.m_Elements[r][c] = m_InnerMatrixStructure.m_Elements[r][0] * Matrix.m_InnerMatrixStructure.m_Elements[0][c] + m_InnerMatrixStructure.m_Elements[r][1] * Matrix.m_InnerMatrixStructure.m_Elements[1][c] + m_InnerMatrixStructure.m_Elements[r][2] * Matrix.m_InnerMatrixStructure.m_Elements[2][c] + m_InnerMatrixStructure.m_Elements[r][3] * Matrix.m_InnerMatrixStructure.m_Elements[3][c];
                    }
                return ResultMatrix;
            }

            void CMatrix4D::Transform(const _3D::CVector3D& Point, _3D::CVector3D& TransformedPoint) const
            {
                const real* pElements = Point.GetElements();
                real TransformedHomogeneousElements[4];
                for (uint r = 0; r < 4; ++r)
                {
                    real Accumulator = m_InnerMatrixStructure.m_Elements[r][3];
                    for (uint c = 0; c < 3; ++c)
                    {
                        Accumulator += m_InnerMatrixStructure.m_Elements[r][c] * pElements[c];
                    }
                    TransformedHomogeneousElements[r] = Accumulator;
                }
                TransformedPoint.SetValue(TransformedHomogeneousElements[0] / TransformedHomogeneousElements[3], TransformedHomogeneousElements[1] / TransformedHomogeneousElements[3], TransformedHomogeneousElements[2] / TransformedHomogeneousElements[3]);
            }

            CMatrix4D CMatrix4D::operator*(const real Scalar)
            {
                for (uint i = 0; i < 16; ++i)
                {
                    m_InnerMatrixStructure.m_Array[i] *= Scalar;
                }
                return *this;
            }

            bool CMatrix4D::operator==(const CMatrix4D& Matrix) const
            {
                for (uint i = 0; i < 16; ++i)
                    if (RealAbs(m_InnerMatrixStructure.m_Array[i] - Matrix.m_InnerMatrixStructure.m_Array[i]) > _REAL_EPSILON_)
                    {
                        return false;
                    }
                return true;
            }

            bool CMatrix4D::operator!=(const CMatrix4D& Matrix) const
            {
                for (uint i = 0; i < 16; ++i)
                    if (RealAbs(m_InnerMatrixStructure.m_Array[i] - Matrix.m_InnerMatrixStructure.m_Array[i]) > _REAL_EPSILON_)
                    {
                        return true;
                    }
                return false;
            }

            string CMatrix4D::ToString(const uint Precision) const
            {
                ostringstream OutputString;
                OutputString.precision(Precision);
                string Delim("; ");
                for (uint r = 0; r < 4; ++r)
                {
                    OutputString << m_InnerMatrixStructure.m_Elements[r][0] << Delim << m_InnerMatrixStructure.m_Elements[r][1] << Delim << m_InnerMatrixStructure.m_Elements[r][2] << Delim << m_InnerMatrixStructure.m_Elements[r][3] << g_EndLine;
                }
                return OutputString.str();
            }
        }
    }
}
