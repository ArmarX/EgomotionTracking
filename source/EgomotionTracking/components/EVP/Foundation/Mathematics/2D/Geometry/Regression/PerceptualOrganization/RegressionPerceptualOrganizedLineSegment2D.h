/*
 * RegressionPerceptualOrganizedLineSegment2D.h
 *
 *  Created on: 03.01.2012
 *      Author: gonzalez
 */

#pragma once

#include "../RegressionLineSegment2D.h"
#include "PerceptualOrganizedGeometricElement.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                namespace Regression
                {
                    namespace PerceptualOrganization
                    {
                        class CRegressionPerceptualOrganizedLineSegment2D: public CPerceptualOrganizedGeometricElement, public CRegressionLineSegment2D
                        {
                        public:

                            CRegressionPerceptualOrganizedLineSegment2D(const CVector2D& EndPointA, const CVector2D& EndPointB, const CVector2D& RegressionCenter, const CVector2D& RegressionMainAxis, const CVector2D& RegressionSecondaryAxis, const CVector2D& RegressionMinAxesPoint, const CVector2D& RegressionMaxAxesPoint, const real MaximalDeviation, const real MeanDeviation, const CPerceptualOrganizedGeometricElement::PerceptualOrganizationType Type, const list<CRegressionLineSegment2D*>& RegressionLineSegments);
                            ~CRegressionPerceptualOrganizedLineSegment2D() override;

                            list<CRegressionLineSegment2D*> GetRegressionLineSegments() const;

                        protected:

                            const list<CRegressionLineSegment2D*> m_RegressionLineSegments;
                        };
                    }
                }
            }
        }
    }
}

