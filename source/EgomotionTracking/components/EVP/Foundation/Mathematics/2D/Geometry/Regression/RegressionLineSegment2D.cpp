/*
 * RegressionLineSegment2D.cpp
 *
 *  Created on: 30.12.2011
 *      Author: gonzalez
 */

#include "RegressionLineSegment2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                namespace Regression
                {
                    CRegressionLineSegment2D::CRegressionLineSegment2D(const CVector2D& EndPointA, const CVector2D& EndPointB, const CVector2D& RegressionCenter, const CVector2D& RegressionMainAxis, const CVector2D& RegressionSecondaryAxis, const CVector2D& RegressionMinAxesPoint, const CVector2D& RegressionMaxAxesPoint, const real MaximalDeviation, const real MeanDeviation, const void* pRegressionData) :
                        CRegressionGeometricPrimitive2D(RegressionCenter, RegressionMainAxis, RegressionSecondaryAxis, RegressionMinAxesPoint, RegressionMaxAxesPoint, MaximalDeviation, MeanDeviation), CLineSegment2D(EndPointA, EndPointB, pRegressionData)
                    {
                    }

                    CRegressionLineSegment2D::~CRegressionLineSegment2D()
                        = default;

                    bool CRegressionLineSegment2D::IsInsideCircularZone(const CVector2D& Center, const real Radius) const
                    {
                        const CVector2D MidPoint = m_pSourcePoints->GetMidPoint();
                        const real PerpendicularProjection = m_Normal.ScalarProduct(Center - MidPoint);
                        if (RealAbs(PerpendicularProjection) <= Radius)
                        {
                            const CVector2D ProjectedCenter = Center - m_Normal * PerpendicularProjection;
                            const CVector2D Displacement = RealSqrt(Radius * Radius - PerpendicularProjection * PerpendicularProjection) * m_Direction;
                            const real A = m_Direction.ScalarProduct(ProjectedCenter + Displacement - MidPoint);
                            const real B = m_Direction.ScalarProduct(ProjectedCenter - Displacement - MidPoint);
                            const real SemiLength = m_Length * _REAL_HALF_;
                            return (TMin(A, B) <= SemiLength) && (TMax(A, B) >= -SemiLength);
                        }
                        return false;
                    }

                    CGeometricPrimitive2D::GeometricPrimitive2DTypeId CRegressionLineSegment2D::GetGeometricPrimitiveTypeId() const
                    {
                        return GetTypeId();
                    }

                    void CRegressionLineSegment2D::GetEndPointPerpendicularDistance(const CLineSegment2D* pLineSegment, real& DistanceA, real& DistanceB) const
                    {
                        const CVector2D MidPoint = m_pSourcePoints->GetMidPoint();
                        DistanceA = RealAbs(m_Normal.ScalarProduct(pLineSegment->GetSourcePointA() - MidPoint));
                        DistanceB = RealAbs(m_Normal.ScalarProduct(pLineSegment->GetSourcePointB() - MidPoint));
                    }

                    bool CRegressionLineSegment2D::SortRegressionLineSegmentByLenghtDescent(const CRegressionLineSegment2D* plhs, const CRegressionLineSegment2D* prhs)
                    {
                        return plhs->GetLength() > prhs->GetLength();
                    }
                }
            }
        }
    }
}
