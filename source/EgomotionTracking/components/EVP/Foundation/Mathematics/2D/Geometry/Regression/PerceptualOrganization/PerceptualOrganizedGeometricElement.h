/*
 * PerceptualOrganizedGeometricElement.h
 *
 *  Created on: 03.01.2012
 *      Author: gonzalez
 */

#pragma once

#include "../../../../../../GlobalSettings.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                namespace Regression
                {
                    namespace PerceptualOrganization
                    {
                        class CPerceptualOrganizedGeometricElement
                        {
                        public:

                            enum PerceptualOrganizationType
                            {
                                eSingleSegmentCollinearityFusion, eMultipleSegmentCollinearityFusion
                            };

                            CPerceptualOrganizedGeometricElement(const PerceptualOrganizationType Type);
                            virtual ~CPerceptualOrganizedGeometricElement();

                            PerceptualOrganizationType GetPerceptualOrganizationType() const;

                            bool AddElements(CPerceptualOrganizedGeometricElement* pElement);
                            const list<CPerceptualOrganizedGeometricElement*>& GetElements() const;

                        protected:

                            const PerceptualOrganizationType m_Type;
                            list<CPerceptualOrganizedGeometricElement*> m_Elements;
                        };
                    }
                }
            }
        }
    }
}

