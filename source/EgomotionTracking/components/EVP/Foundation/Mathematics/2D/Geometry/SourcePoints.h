/*
 * SourcePoints.h
 *
 *  Created on: Mar 30, 2012
 *      Author: david
 */

#pragma once

#include "GeometricPrimitive2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                class CSourcePoints
                {
                public:

                    CSourcePoints(const CVector2D& A, const CVector2D& B);
                    ~CSourcePoints();

                    void SetPoints(const CVector2D& A, const CVector2D& B);
                    bool ArePointsValid() const;
                    CVector2D GetMidPoint() const;
                    CVector2D GetDirection() const;
                    const CVector2D& GetPointA() const;
                    const CVector2D& GetPointB() const;

                protected:

                    CVector2D m_A;
                    CVector2D m_B;
                };
            }
        }
    }
}

