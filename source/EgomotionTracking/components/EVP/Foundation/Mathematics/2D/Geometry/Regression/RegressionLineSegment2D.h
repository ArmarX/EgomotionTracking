/*
 * RegressionLineSegment2D.h
 *
 *  Created on: 30.12.2011
 *      Author: gonzalez
 */

#pragma once

#include "RegressionGeometricPrimitive2D.h"
#include "../LineSegment2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                namespace Regression
                {
                    class CRegressionLineSegment2D: public CRegressionGeometricPrimitive2D, public CLineSegment2D
                    {
                    public:

                        CRegressionLineSegment2D(const CVector2D& EndPointA, const CVector2D& EndPointB, const CVector2D& RegressionCenter, const CVector2D& RegressionMainAxis, const CVector2D& RegressionSecondaryAxis, const CVector2D& RegressionMinAxesPoint, const CVector2D& RegressionMaxAxesPoint, const real MaximalDeviation, const real MeanDeviation, const void* pRegressionData = NULL);
                        ~CRegressionLineSegment2D() override;

                        bool IsInsideCircularZone(const CVector2D& Center, const real Radius) const override;
                        CGeometricPrimitive2D::GeometricPrimitive2DTypeId GetGeometricPrimitiveTypeId() const override;
                        void GetEndPointPerpendicularDistance(const CLineSegment2D* pLineSegment, real& DistanceA, real& DistanceB) const;
                        static  bool SortRegressionLineSegmentByLenghtDescent(const CRegressionLineSegment2D* plhs, const CRegressionLineSegment2D* prhs);
                    };
                }
            }
        }
    }
}

