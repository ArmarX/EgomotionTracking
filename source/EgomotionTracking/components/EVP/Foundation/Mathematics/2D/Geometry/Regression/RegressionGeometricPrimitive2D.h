/*
 * RegressionGeometricPrimitive2D.h
 *
 *  Created on: 30.12.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../../VisualSpace/Common/ContinuousBoundingBox2D.h"
#include "../../Vector2D.h"
#include "../GeometricPrimitive2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                namespace Regression
                {
                    class CRegressionGeometricPrimitive2D
                    {
                    public:

                        CRegressionGeometricPrimitive2D(const CVector2D& Center, const CVector2D& MainAxis, const CVector2D& SecondaryAxis, const CVector2D& MinAxesPoint, const CVector2D& MaxAxesPoint, const real MaximalDeviation, const real MeanDeviation);
                        virtual ~CRegressionGeometricPrimitive2D();

                        void SetActive(const bool Active);
                        bool IsActive() const;

                        const CVector2D& GetCenter() const;
                        const CVector2D& GetMainAxis() const;
                        const CVector2D& GetSecondaryAxis() const;
                        const CContinuousBoundingBox2D& GetBoundingBox() const;
                        real GetMaximalDeviation() const;
                        real GetMeanDeviation() const;

                        virtual  bool IsInsideCircularZone(const CVector2D& Center, const real Radius) const = 0;
                        virtual  CGeometricPrimitive2D::GeometricPrimitive2DTypeId GetGeometricPrimitiveTypeId() const = 0;

                    protected:

                        bool m_Active;
                        const CVector2D m_Center;
                        const CVector2D m_MainAxis;
                        const CVector2D m_SecondaryAxis;
                        const CContinuousBoundingBox2D m_BoundingBox;
                        const real m_MaximalDeviation;
                        const real m_MeanDeviation;
                    };
                }
            }
        }
    }
}

