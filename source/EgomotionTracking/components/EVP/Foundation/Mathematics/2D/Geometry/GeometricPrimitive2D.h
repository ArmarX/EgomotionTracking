/*
 * GeometricPrimitive2D.h
 *
 *  Created on: 27.12.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../Miscellaneous/IdentifiableInstance.h"
#include "../../1D/Common1D.h"
#include "Common2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                enum IntersectionType
                {
                    eEmpty = 0, eSubspace = 1, eSelfNumericalIntersection = 2, eSelfInstanceIntersection = 4
                };

                class CGeometricPrimitive2D: public CIdentifiableInstance
                {
                    IDENTIFIABLE

                public:

                    enum Status
                    {
                        eUndefined = 0, eDefined
                    };

                    enum GeometricPrimitive2DTypeId
                    {
                        eNone = 0, ePoint, eLine, eCurve
                    };

                    CGeometricPrimitive2D(const GeometricPrimitive2DTypeId TypeId, const CGeometricPrimitive2D* pParentA, const CGeometricPrimitive2D* pParentB);
                    ~CGeometricPrimitive2D() override;

                    GeometricPrimitive2DTypeId GetTypeId() const;

                    void SetStatus(const Status CurrentStatus);
                    Status GetStatus() const;
                    bool IsDefined() const;
                    bool IsUndefined() const;
                    std::string GetStatusStringMode() const;

                    void SetDataPointer(const void* pDataPointer);
                    const void* GetDataPointer() const;
                    bool HasDataPointer() const;

                    bool HasParents() const;
                    const CGeometricPrimitive2D* GetParentA() const;
                    const CGeometricPrimitive2D* GetParentB() const;
                    bool IsAncestor(const CGeometricPrimitive2D* pGeometricPrimitive, const bool FullSearch) const;

                    virtual  std::string ToString() const = 0;
                    virtual  bool operator==(const CGeometricPrimitive2D& GeometricPrimitive) const = 0;
                    virtual  IntersectionType AnalyzeIntersection(const CGeometricPrimitive2D* pGeometricPrimitive, GeometricPrimitive2DTypeId* pIntersectionTypeId) const = 0;
                    virtual CGeometricPrimitive2D* GenerateIntersection(const CGeometricPrimitive2D* pGeometricPrimitive, GeometricPrimitive2DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const = 0;

                protected:

                    void AppendParentalInformation(ostream& Content) const;
                    static void SetGeometricPrimitiveType(GeometricPrimitive2DTypeId* pGeometricPrimitiveTypeId, const GeometricPrimitive2DTypeId TypeId);
                    static void SetIntersectionType(IntersectionType* pIntersectionType, const IntersectionType TypeId);
                    static void SetIntersectionInformation(GeometricPrimitive2DTypeId* pGeometricPrimitiveTypeId, IntersectionType* pIntersectionType, const GeometricPrimitive2DTypeId TypeId, const IntersectionType IntersectionTypeId);

                private:

                    const GeometricPrimitive2DTypeId m_TypeId;
                    Status m_Status;
                    const CGeometricPrimitive2D* m_pParentA;
                    const CGeometricPrimitive2D* m_pParentB;
                    const void* m_pDataPointer;
                };
            }
        }
    }
}

