/*
 * Common2D.cpp
 *
 *  Created on: 28.12.2011
 *      Author: gonzalez
 */

#include "../../1D/Common1D.h"
#include "Common2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                bool AreUnitaryVectorsParallel(const CVector2D& A, const CVector2D& B)
                {
                    return (RealAbs(RealAbs(A.ScalarProduct(B)) - _REAL_ONE_) < _REAL_EPSILON_);
                }

                real ComputeSquareDistance(const CVector2D& A, const CVector2D& B)
                {
                    const real Dx = A.GetX() - B.GetX();
                    const real Dy = A.GetY() - B.GetY();
                    return (Dx * Dx + Dy * Dy);
                }

                real ComputeDistance(const CVector2D& A, const CVector2D& B)
                {
                    return RealHypotenuse(A.GetX() - B.GetX(), A.GetY() - B.GetY());
                }

                CVector2D ComputeUnitaryDirectionVector(const CVector2D& A, const CVector2D& B)
                {
                    CVector2D UnitaryDirectionVector = B - A;
                    UnitaryDirectionVector.Normalize();
                    return UnitaryDirectionVector;
                }

                CVector2D ComputeProjectionPointToLine(const CVector2D& SupportPoint, const CVector2D& Direction, const CVector2D& X)
                {
                    return SupportPoint + (Direction * Direction.ScalarProduct(X - SupportPoint));
                }

                bool ComputeLineToLineIntersectionPoint(const CVector2D& SupportPointA, const CVector2D& DirectionA, const CVector2D& SupportPointB, const CVector2D& DirectionB, CVector2D& Point)
                {
                    if (AreUnitaryVectorsParallel(DirectionA, DirectionB))
                    {
                        return false;
                    }
                    const CVector2D AP = SupportPointA + DirectionA;
                    const CVector2D AN = SupportPointA - DirectionA;
                    const CVector2D BP = SupportPointB + DirectionB;
                    const CVector2D BN = SupportPointB - DirectionB;
                    const real T0 = ((AP.GetX() * AN.GetY()) - (AN.GetX() * AP.GetY()));
                    const real T1 = ((BP.GetX() * BN.GetY()) - (BN.GetX() * BP.GetY()));
                    const real T3 = (((AP.GetX() - AN.GetX()) * (BP.GetY() - BN.GetY())) - ((BP.GetX() - BN.GetX()) * (AP.GetY() - AN.GetY())));
                    Point.SetValue(((T0 * (BP.GetX() - BN.GetX())) - (T1 * (AP.GetX() - AN.GetX()))) / T3, ((T0 * (BP.GetY() - BN.GetY())) - (T1 * (AP.GetY() - AN.GetY()))) / T3);
                    return true;
                }

                bool ComputeLineSegmentToLineSegmentIntersectionPoint(const CVector2D& AP, const CVector2D& AN, const CVector2D& BP, const CVector2D& BN, CVector2D& Point)
                {
                    const real T3 = (((AP.GetX() - AN.GetX()) * (BP.GetY() - BN.GetY())) - ((BP.GetX() - BN.GetX()) * (AP.GetY() - AN.GetY())));
                    if (_1D::IsZero(T3))
                    {
                        return false;
                    }
                    const real T0 = ((AP.GetX() * AN.GetY()) - (AN.GetX() * AP.GetY()));
                    const real T1 = ((BP.GetX() * BN.GetY()) - (BN.GetX() * BP.GetY()));
                    Point.SetValue(((T0 * (BP.GetX() - BN.GetX())) - (T1 * (AP.GetX() - AN.GetX()))) / T3, ((T0 * (BP.GetY() - BN.GetY())) - (T1 * (AP.GetY() - AN.GetY()))) / T3);
                    return true;
                }
            }
        }
    }
}
