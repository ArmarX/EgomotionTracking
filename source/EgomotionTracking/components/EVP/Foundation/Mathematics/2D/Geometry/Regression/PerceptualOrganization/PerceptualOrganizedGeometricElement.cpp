/*
 * PerceptualOrganizedGeometricElement.cpp
 *
 *  Created on: 03.01.2012
 *      Author: gonzalez
 */

#include "PerceptualOrganizedGeometricElement.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                namespace Regression
                {
                    namespace PerceptualOrganization
                    {
                        CPerceptualOrganizedGeometricElement::CPerceptualOrganizedGeometricElement(const PerceptualOrganizationType Type) :
                            m_Type(Type)
                        {
                        }

                        CPerceptualOrganizedGeometricElement::~CPerceptualOrganizedGeometricElement()
                            = default;

                        CPerceptualOrganizedGeometricElement::PerceptualOrganizationType CPerceptualOrganizedGeometricElement::GetPerceptualOrganizationType() const
                        {
                            return m_Type;
                        }

                        bool CPerceptualOrganizedGeometricElement::AddElements(CPerceptualOrganizedGeometricElement* pElement)
                        {
                            if (pElement && (pElement != this))
                            {
                                list<CPerceptualOrganizedGeometricElement*>::const_iterator EndElements = m_Elements.end();
                                for (list<CPerceptualOrganizedGeometricElement*>::const_iterator ppElement = m_Elements.begin(); ppElement != EndElements; ++ppElement)
                                    if ((*ppElement) == pElement)
                                    {
                                        return false;
                                    }
                                m_Elements.push_back(pElement);
                                return true;
                            }
                            return false;
                        }

                        const list<CPerceptualOrganizedGeometricElement*>& CPerceptualOrganizedGeometricElement::GetElements() const
                        {
                            return m_Elements;
                        }
                    }
                }
            }
        }
    }
}
