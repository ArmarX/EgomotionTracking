/*
 * Line2D.cpp
 *
 *  Created on: 28.12.2011
 *      Author: gonzalez
 */

#include "Point2D.h"
#include "Line2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                CLine2D::CLine2D() :
                    CGeometricPrimitive2D(eLine, nullptr, nullptr), m_Direction(CVector2D::s_Point_At_Origin_2D), m_SupportPoint(CVector2D::s_Point_At_Plus_Infinity_2D), m_pSourcePoints(nullptr)
                {
                }

                CLine2D::CLine2D(const real Xa, const real Ya, const real Xb, const real Yb, const ArgumentMode Mode, const CGeometricPrimitive2D* pParentA, const CGeometricPrimitive2D* pParentB) :
                    CGeometricPrimitive2D(eLine, pParentA, pParentB), m_Direction(CVector2D::s_Point_At_Origin_2D), m_SupportPoint(CVector2D::s_Point_At_Plus_Infinity_2D), m_pSourcePoints(nullptr)
                {
                    switch (Mode)
                    {
                        case eDirectionVectorAndSupportPoint:
                            m_Direction = CVector2D::CreateCannonicalForm(Xa, Ya);
                            m_SupportPoint.SetValue(Xb, Yb);
                            if (m_Direction.IsNonZeroLength() && m_SupportPoint.IsNotAtInfinity())
                            {
                                m_Direction.Normalize();
                                SetStatus(eDefined);
                            }
                            break;
                        case ePointAPointB:
                            SetLineByPoints(CVector2D(Xa, Ya), CVector2D(Xb, Yb));
                            break;
                    }
                }

                CLine2D::CLine2D(const CVector2D& A, const CVector2D& B, const ArgumentMode Mode, const CGeometricPrimitive2D* pParentA, const CGeometricPrimitive2D* pParentB) :
                    CGeometricPrimitive2D(eLine, pParentA, pParentB), m_Direction(CVector2D::s_Point_At_Origin_2D), m_SupportPoint(CVector2D::s_Point_At_Plus_Infinity_2D), m_pSourcePoints(nullptr)
                {
                    switch (Mode)
                    {
                        case eDirectionVectorAndSupportPoint:
                            m_Direction = A;
                            m_SupportPoint = B;
                            if (m_Direction.IsNotAtInfinity() && m_SupportPoint.IsNotAtInfinity())
                            {
                                m_Direction.Normalize();
                                SetStatus(eDefined);
                            }
                            break;
                        case ePointAPointB:
                            SetLineByPoints(A, B);
                            break;
                    }
                }

                CLine2D::~CLine2D()
                {
                    RELEASE_OBJECT(m_pSourcePoints)
                }

                void CLine2D::SetLineByPoints(const CVector2D& A, const CVector2D& B)
                {
                    if (m_pSourcePoints)
                    {
                        m_pSourcePoints->SetPoints(A, B);
                    }
                    else
                    {
                        m_pSourcePoints = new CSourcePoints(A, B);
                    }
                    if (m_pSourcePoints->ArePointsValid())
                    {
                        m_SupportPoint = m_pSourcePoints->GetMidPoint();
                        m_Direction = m_pSourcePoints->GetDirection();
                        SetStatus(eDefined);
                    }
                    else
                    {
                        m_Direction = CVector2D::s_Point_At_Origin_2D;
                        m_SupportPoint = CVector2D::s_Point_At_Plus_Infinity_2D;
                        SetStatus(eUndefined);
                    }
                }

                bool CLine2D::HasSourcePoints() const
                {
                    return m_pSourcePoints;
                }

                bool CLine2D::GetSourcePoints(CVector2D& A, CVector2D& B) const
                {
                    if (m_pSourcePoints)
                    {
                        A = m_pSourcePoints->GetPointA();
                        B = m_pSourcePoints->GetPointB();
                        return true;
                    }
                    return false;
                }

                const CVector2D& CLine2D::GetSourcePointA() const
                {
                    if (m_pSourcePoints)
                    {
                        return m_pSourcePoints->GetPointA();
                    }
                    return CVector2D::s_Point_At_Plus_Infinity_2D;
                }

                const CVector2D& CLine2D::GetSourcePointB() const
                {
                    if (m_pSourcePoints)
                    {
                        return m_pSourcePoints->GetPointB();
                    }
                    return CVector2D::s_Point_At_Plus_Infinity_2D;
                }

                void CLine2D::SetDirection(const CVector2D& Direction)
                {
                    m_Direction = Direction;
                    if (m_Direction.IsZeroLength())
                    {
                        SetStatus(eUndefined);
                    }
                    else
                    {
                        m_Direction.Normalize();
                        SetStatus(m_SupportPoint.IsAtInfinity() ? eUndefined : eDefined);
                    }
                }

                void CLine2D::SetSupportPoint(const CVector2D& SupportPoint)
                {
                    m_SupportPoint = SupportPoint;
                    if (m_SupportPoint.IsAtInfinity())
                    {
                        SetStatus(eUndefined);
                    }
                    else
                    {
                        SetStatus(m_Direction.IsUnitary() ? eDefined : eUndefined);
                    }
                }

                const CVector2D& CLine2D::GetDirection() const
                {
                    return m_Direction;
                }

                const CVector2D& CLine2D::GetSupportPoint() const
                {
                    return m_SupportPoint;
                }

                CVector2D CLine2D::GetPointAtUnitaryDisplacement() const
                {
                    return m_SupportPoint + m_Direction;
                }

                CVector2D CLine2D::GetPointAtDisplacement(const real Displacement) const
                {
                    return m_SupportPoint + (m_Direction * Displacement);
                }

                CVector2D CLine2D::GetClosestPoint(const CVector2D& X) const
                {
                    return ComputeProjectionPointToLine(m_SupportPoint, m_Direction, X);
                }

                std::string CLine2D::ToString() const
                {
                    std::ostringstream Content;
                    Content.precision(_REAL_EPSILON_DISPLAY_DIGITS_);
                    Content << "<Line2D Id=\"" << m_InstanceId << "\" Status=\"" << GetStatusStringMode() << "\" Dx=\"" << m_Direction.GetX() << "\" Dy=\"" << m_Direction.GetY() << "\" Sx=\"" << m_SupportPoint.GetX() << "\" Sy=\"" << m_SupportPoint.GetY() << "\"";
                    AppendParentalInformation(Content);
                    Content << "/>";
                    return Content.str();
                }

                bool CLine2D::operator==(const CGeometricPrimitive2D& GeometricPrimitive) const
                {
                    if (IsUndefined() || GeometricPrimitive.IsUndefined())
                    {
                        return false;
                    }
                    if (&GeometricPrimitive == this)
                    {
                        return true;
                    }
                    if (GeometricPrimitive.GetTypeId() == GetTypeId())
                    {
                        const CLine2D* pLine = dynamic_cast<const CLine2D*>(&GeometricPrimitive);
                        return (m_Direction == pLine->m_Direction) && (m_SupportPoint == pLine->m_SupportPoint);
                    }
                    return false;
                }

                IntersectionType CLine2D::AnalyzeIntersection(const CGeometricPrimitive2D* pGeometricPrimitive, GeometricPrimitive2DTypeId* pIntersectionTypeId) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eCurve:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case ePoint:
                                return AnalyzeIntersection(dynamic_cast<const CPoint2D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case eLine:
                                if (pGeometricPrimitive == this)
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, eLine);
                                    return eSelfInstanceIntersection;
                                }
                                return AnalyzeIntersection(dynamic_cast<const CLine2D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case eNone:
                                break;
                        }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive2D* CLine2D::GenerateIntersection(const CGeometricPrimitive2D* pGeometricPrimitive, GeometricPrimitive2DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eCurve:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePoint:
                                return GenerateIntersection(dynamic_cast<const CPoint2D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case eLine:
                                if (pGeometricPrimitive == this)
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eLine, eSelfInstanceIntersection);
                                    return nullptr;
                                }
                                return GenerateIntersection(dynamic_cast<const CLine2D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case eNone:
                                break;
                        }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                bool CLine2D::CalculateIntersectionPoint(const CLine2D* pLine, CVector2D& X) const
                {
                    if (pLine && (pLine != this))
                    {
                        return ComputeLineToLineIntersectionPoint(m_SupportPoint, m_Direction, pLine->m_SupportPoint, pLine->m_Direction, X);
                    }
                    return false;
                }

                IntersectionType CLine2D::AnalyzeIntersection(const CPoint2D* pPoint, GeometricPrimitive2DTypeId* pIntersectionTypeId) const
                {
                    if (pPoint->GetPoint() == ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pPoint->GetPoint()))
                    {
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSelfNumericalIntersection;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                IntersectionType CLine2D::AnalyzeIntersection(const CLine2D* pLine, GeometricPrimitive2DTypeId* pIntersectionTypeId) const
                {
                    if (AreUnitaryVectorsParallel(m_Direction, pLine->m_Direction))
                    {
                        const CVector2D ClosestPoint = ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pLine->m_SupportPoint);
                        if (ClosestPoint == pLine->m_SupportPoint)
                        {
                            SetGeometricPrimitiveType(pIntersectionTypeId, eLine);
                            return eSelfNumericalIntersection;
                        }
                        SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                        return eEmpty;
                    }
                    CVector2D X;
                    if (ComputeLineToLineIntersectionPoint(m_SupportPoint, m_Direction, pLine->m_SupportPoint, pLine->m_Direction, X))
                    {
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSubspace;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive2D* CLine2D::GenerateIntersection(const CPoint2D* pPoint, GeometricPrimitive2DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pPoint->GetPoint() == ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pPoint->GetPoint()))
                    {
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSelfNumericalIntersection);
                        return nullptr;
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                CGeometricPrimitive2D* CLine2D::GenerateIntersection(const CLine2D* pLine, GeometricPrimitive2DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (AreUnitaryVectorsParallel(m_Direction, pLine->m_Direction))
                    {
                        const CVector2D ClosestPoint = ComputeProjectionPointToLine(m_SupportPoint, m_Direction, pLine->m_SupportPoint);
                        if (ClosestPoint == pLine->m_SupportPoint)
                        {
                            SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eLine, eSelfNumericalIntersection);
                            return nullptr;
                        }
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                        return nullptr;
                    }
                    CVector2D X;
                    if (ComputeLineToLineIntersectionPoint(m_SupportPoint, m_Direction, pLine->m_SupportPoint, pLine->m_Direction, X))
                    {
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSubspace);
                        return new CPoint2D(X, this, pLine);
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }
            }
        }
    }
}
