/*
 * RegressionGeometricPrimitive2D.cpp
 *
 *  Created on: 30.12.2011
 *      Author: gonzalez
 */

#include "RegressionGeometricPrimitive2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                namespace Regression
                {
                    CRegressionGeometricPrimitive2D::CRegressionGeometricPrimitive2D(const CVector2D& Center, const CVector2D& MainAxis, const CVector2D& SecondaryAxis, const CVector2D& MinAxesPoint, const CVector2D& MaxAxesPoint, const real MaximalDeviation, const real MeanDeviation) :
                        m_Active(true), m_Center(Center), m_MainAxis(MainAxis), m_SecondaryAxis(SecondaryAxis), m_BoundingBox(MinAxesPoint, MaxAxesPoint), m_MaximalDeviation(MaximalDeviation), m_MeanDeviation(MeanDeviation)
                    {
                    }

                    CRegressionGeometricPrimitive2D::~CRegressionGeometricPrimitive2D()
                        = default;

                    void CRegressionGeometricPrimitive2D::SetActive(const bool Active)
                    {
                        m_Active = Active;
                    }

                    bool CRegressionGeometricPrimitive2D::IsActive() const
                    {
                        return m_Active;
                    }

                    const CVector2D& CRegressionGeometricPrimitive2D::GetCenter() const
                    {
                        return m_Center;
                    }

                    const CVector2D& CRegressionGeometricPrimitive2D::GetMainAxis() const
                    {
                        return m_MainAxis;
                    }

                    const CVector2D& CRegressionGeometricPrimitive2D::GetSecondaryAxis() const
                    {
                        return m_SecondaryAxis;
                    }

                    const CContinuousBoundingBox2D& CRegressionGeometricPrimitive2D::GetBoundingBox() const
                    {
                        return m_BoundingBox;
                    }

                    real CRegressionGeometricPrimitive2D::GetMaximalDeviation() const
                    {
                        return m_MaximalDeviation;

                    }

                    real CRegressionGeometricPrimitive2D::GetMeanDeviation() const
                    {
                        return m_MeanDeviation;
                    }
                }
            }
        }
    }
}
