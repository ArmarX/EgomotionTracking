/*
 * Point2D.cpp
 *
 *  Created on: 28.12.2011
 *      Author: gonzalez
 */

#include "Point2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                CPoint2D::CPoint2D() :
                    CGeometricPrimitive2D(ePoint, nullptr, nullptr), m_Point(CVector2D::s_Point_At_Plus_Infinity_2D)
                {
                }

                CPoint2D::CPoint2D(const real X, const real Y, const CGeometricPrimitive2D* pParentA, const CGeometricPrimitive2D* pParentB) :
                    CGeometricPrimitive2D(ePoint, pParentA, pParentB), m_Point(X, Y)
                {
                    if (m_Point.IsNotAtInfinity())
                    {
                        SetStatus(eDefined);
                    }
                }

                CPoint2D::CPoint2D(const CVector2D& Point, const CGeometricPrimitive2D* pParentA, const CGeometricPrimitive2D* pParentB) :
                    CGeometricPrimitive2D(ePoint, pParentA, pParentB), m_Point(Point)
                {
                    if (m_Point.IsNotAtInfinity())
                    {
                        SetStatus(eDefined);
                    }
                }

                CPoint2D::~CPoint2D()
                    = default;

                void CPoint2D::SetPoint(const CVector2D& Point)
                {
                    m_Point = Point;
                    SetStatus(m_Point.IsAtInfinity() ? eUndefined : eDefined);
                }

                const CVector2D& CPoint2D::GetPoint() const
                {
                    return m_Point;
                }

                std::string CPoint2D::ToString() const
                {
                    std::ostringstream Content;
                    Content.precision(_REAL_EPSILON_DISPLAY_DIGITS_);
                    Content << "<Point2D Id=\"" << m_InstanceId << "\" Status=\"" << GetStatusStringMode() << "\" X=\"" << m_Point.GetX() << "\" Y=\"" << m_Point.GetY() << "\"";
                    AppendParentalInformation(Content);
                    Content << "/>";
                    return Content.str();
                }

                bool CPoint2D::operator==(const CGeometricPrimitive2D& GeometricPrimitive) const
                {
                    if (IsUndefined() || GeometricPrimitive.IsUndefined())
                    {
                        return false;
                    }
                    if (&GeometricPrimitive == this)
                    {
                        return true;
                    }
                    if (GeometricPrimitive.GetTypeId() == GetTypeId())
                    {
                        const CPoint2D* pPoints = dynamic_cast<const CPoint2D*>(&GeometricPrimitive);
                        return (m_Point == pPoints->m_Point);
                    }
                    return false;
                }

                IntersectionType CPoint2D::AnalyzeIntersection(const CGeometricPrimitive2D* pGeometricPrimitive, GeometricPrimitive2DTypeId* pIntersectionTypeId) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eCurve:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case ePoint:
                                if (pGeometricPrimitive == this)
                                {
                                    SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                                    return eSelfInstanceIntersection;
                                }
                                return AnalyzeIntersection(dynamic_cast<const CPoint2D*>(pGeometricPrimitive), pIntersectionTypeId);
                                break;
                            case eLine:
                                return pGeometricPrimitive->AnalyzeIntersection(this, pIntersectionTypeId);
                                break;
                            case eNone:
                                break;
                        }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive2D* CPoint2D::GenerateIntersection(const CGeometricPrimitive2D* pGeometricPrimitive, GeometricPrimitive2DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (pGeometricPrimitive && IsDefined() && pGeometricPrimitive->IsDefined())
                        switch (pGeometricPrimitive->GetTypeId())
                        {
                            case eCurve:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case ePoint:
                                if (pGeometricPrimitive == this)
                                {
                                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSelfInstanceIntersection);
                                    return nullptr;
                                }
                                return GenerateIntersection(dynamic_cast<const CPoint2D*>(pGeometricPrimitive), pIntersectionTypeId, pIntersectionType);
                                break;
                            case eLine:
                                return pGeometricPrimitive->GenerateIntersection(this, pIntersectionTypeId, pIntersectionType);
                                break;
                            case eNone:
                                break;
                        }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }

                IntersectionType CPoint2D::AnalyzeIntersection(const CPoint2D* pPoint, GeometricPrimitive2DTypeId* pIntersectionTypeId) const
                {
                    if (m_Point == pPoint->m_Point)
                    {
                        SetGeometricPrimitiveType(pIntersectionTypeId, ePoint);
                        return eSelfNumericalIntersection;
                    }
                    SetGeometricPrimitiveType(pIntersectionTypeId, eNone);
                    return eEmpty;
                }

                CGeometricPrimitive2D* CPoint2D::GenerateIntersection(const CPoint2D* pPoint, GeometricPrimitive2DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const
                {
                    if (m_Point == pPoint->m_Point)
                    {
                        SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, ePoint, eSelfNumericalIntersection);
                        return nullptr;
                    }
                    SetIntersectionInformation(pIntersectionTypeId, pIntersectionType, eNone, eEmpty);
                    return nullptr;
                }
            }
        }
    }
}
