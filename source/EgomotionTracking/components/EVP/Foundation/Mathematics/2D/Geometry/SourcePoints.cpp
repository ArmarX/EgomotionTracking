/*
 * SourcePoints.cpp
 *
 *  Created on: Mar 30, 2012
 *      Author: david
 */

#include "SourcePoints.h"

namespace EVP
{

    namespace Mathematics
    {

        namespace _2D
        {

            namespace Geometry
            {

                CSourcePoints::CSourcePoints(const CVector2D& A, const CVector2D& B) :
                    m_A(A), m_B(B)
                {
                }

                CSourcePoints::~CSourcePoints()
                    = default;

                void CSourcePoints::SetPoints(const CVector2D& A, const CVector2D& B)
                {
                    m_A = A;
                    m_B = B;
                }

                bool CSourcePoints::ArePointsValid() const
                {
                    return ((m_A != m_B) && m_A.IsNotAtInfinity() && m_B.IsNotAtInfinity());
                }

                CVector2D CSourcePoints::GetMidPoint() const
                {
                    return CVector2D::CreateMidPoint(m_A, m_B);
                }

                CVector2D CSourcePoints::GetDirection() const
                {
                    CVector2D UnitaryDirection = ComputeUnitaryDirectionVector(m_A, m_B);
                    UnitaryDirection.SetCannonicalForm();
                    return UnitaryDirection;
                }

                const CVector2D& CSourcePoints::GetPointA() const
                {
                    return m_A;
                }

                const CVector2D& CSourcePoints::GetPointB() const
                {
                    return m_B;
                }

            }

        }

    }

}
