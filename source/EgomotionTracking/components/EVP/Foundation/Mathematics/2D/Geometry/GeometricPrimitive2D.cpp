/*
 * GeometricPrimitive2D.cpp
 *
 *  Created on: 27.12.2011
 *      Author: gonzalez
 */

#include "GeometricPrimitive2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                IDENTIFIABLE_INITIALIZATION(CGeometricPrimitive2D)

                CGeometricPrimitive2D::CGeometricPrimitive2D(const GeometricPrimitive2DTypeId TypeId, const CGeometricPrimitive2D* pParentA, const CGeometricPrimitive2D* pParentB) :
                    IDENTIFIABLE_CONTRUCTOR(CGeometricPrimitive2D), m_TypeId(TypeId), m_Status(eUndefined), m_pParentA(nullptr), m_pParentB(nullptr), m_pDataPointer(nullptr)
                {
                    if (pParentA && pParentB)
                    {
                        if (pParentA->GetTypeId() < pParentB->GetTypeId())
                        {
                            m_pParentA = pParentA;
                            m_pParentB = pParentB;
                        }
                        else if (pParentA->GetTypeId() == pParentB->GetTypeId())
                        {
                            if (pParentA->GetInstanceId() > pParentB->GetInstanceId())
                            {
                                m_pParentA = pParentA;
                                m_pParentB = pParentB;
                            }
                            else
                            {
                                m_pParentA = pParentB;
                                m_pParentB = pParentA;
                            }
                        }
                        else
                        {
                            m_pParentA = pParentB;
                            m_pParentB = pParentA;
                        }
                    }
                }

                CGeometricPrimitive2D::~CGeometricPrimitive2D()
                    = default;

                CGeometricPrimitive2D::GeometricPrimitive2DTypeId CGeometricPrimitive2D::GetTypeId() const
                {
                    return m_TypeId;
                }

                void CGeometricPrimitive2D::SetStatus(const Status CurrentStatus)
                {
                    m_Status = CurrentStatus;
                }

                CGeometricPrimitive2D::Status CGeometricPrimitive2D::GetStatus() const
                {
                    return m_Status;
                }

                bool CGeometricPrimitive2D::IsDefined() const
                {
                    return (m_Status == eDefined);
                }

                bool CGeometricPrimitive2D::IsUndefined() const
                {
                    return (m_Status == eUndefined);
                }

                std::string CGeometricPrimitive2D::GetStatusStringMode() const
                {
                    return std::string((m_Status == eDefined) ? "Defined" : "Undefined");
                }

                void CGeometricPrimitive2D::SetDataPointer(const void* pDataPointer)
                {
                    m_pDataPointer = pDataPointer;
                }

                const void* CGeometricPrimitive2D::GetDataPointer() const
                {
                    return m_pDataPointer;
                }

                bool CGeometricPrimitive2D::HasDataPointer() const
                {
                    return m_pDataPointer;
                }

                bool CGeometricPrimitive2D::HasParents() const
                {
                    return (m_pParentA && m_pParentB);
                }

                const CGeometricPrimitive2D* CGeometricPrimitive2D::GetParentA() const
                {
                    return m_pParentA;
                }

                const CGeometricPrimitive2D* CGeometricPrimitive2D::GetParentB() const
                {
                    return m_pParentB;
                }

                bool CGeometricPrimitive2D::IsAncestor(const CGeometricPrimitive2D* pGeometricPrimitive, const bool FullSearch) const
                {
                    if (pGeometricPrimitive && m_pParentA && m_pParentB)
                    {
                        if ((m_pParentA == pGeometricPrimitive) || (m_pParentB == pGeometricPrimitive))
                        {
                            return true;
                        }
                        else if (FullSearch)
                        {
                            const CGeometricPrimitive2D* pAncestor = nullptr;
                            list<const CGeometricPrimitive2D*> AncestorsSearchList;
                            AncestorsSearchList.push_back(m_pParentA);
                            AncestorsSearchList.push_back(m_pParentB);
                            while (AncestorsSearchList.size())
                            {
                                pAncestor = AncestorsSearchList.front();
                                AncestorsSearchList.pop_front();
                                if (pAncestor == pGeometricPrimitive)
                                {
                                    return true;
                                }
                                if (pAncestor->m_pParentA && pAncestor->m_pParentB)
                                {
                                    AncestorsSearchList.push_back(pAncestor->m_pParentA);
                                    AncestorsSearchList.push_back(pAncestor->m_pParentB);
                                }
                            }
                        }
                    }
                    return false;
                }

                void CGeometricPrimitive2D::AppendParentalInformation(ostream& Content) const
                {
                    if (m_pParentA && m_pParentB)
                    {
                        Content << " ParentA=\"" << m_pParentA->GetInstanceId() << "\" ParentB=\"" << m_pParentB->GetInstanceId() << "\"";
                    }
                }

                void CGeometricPrimitive2D::SetGeometricPrimitiveType(GeometricPrimitive2DTypeId* pGeometricPrimitiveTypeId, const GeometricPrimitive2DTypeId TypeId)
                {
                    if (pGeometricPrimitiveTypeId)
                    {
                        *pGeometricPrimitiveTypeId = TypeId;
                    }
                }

                void CGeometricPrimitive2D::SetIntersectionType(IntersectionType* pIntersectionType, const IntersectionType TypeId)
                {
                    if (pIntersectionType)
                    {
                        *pIntersectionType = TypeId;
                    }
                }

                void CGeometricPrimitive2D::SetIntersectionInformation(GeometricPrimitive2DTypeId* pGeometricPrimitiveTypeId, IntersectionType* pIntersectionType, const GeometricPrimitive2DTypeId TypeId, const IntersectionType IntersectionTypeId)
                {
                    if (pGeometricPrimitiveTypeId)
                    {
                        *pGeometricPrimitiveTypeId = TypeId;
                    }
                    if (pIntersectionType)
                    {
                        *pIntersectionType = IntersectionTypeId;
                    }
                }
            }
        }
    }
}
