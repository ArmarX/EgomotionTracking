/*
 * LineSegment2D.cpp
 *
 *  Created on: 29.12.2011
 *      Author: gonzalez
 */

#include "LineSegment2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                CLineSegment2D::CLineSegment2D(const CVector2D& A, const CVector2D& B, const void* pData, const CGeometricPrimitive2D* pParentA, const CGeometricPrimitive2D* pParentB) :
                    CLine2D(A, B, CLine2D::ePointAPointB, pParentA, pParentB), m_Normal(m_Direction.GetRotated(_REAL_HALF_PI_)), m_Length(ComputeDistance(A, B))
                {
                    if (pData)
                    {
                        SetDataPointer(pData);
                    }
                }

                CLineSegment2D::~CLineSegment2D()
                    = default;

                const CVector2D& CLineSegment2D::GetNormal() const
                {
                    return m_Normal;
                }

                real CLineSegment2D::GetLength() const
                {
                    return m_Length;
                }

                const CVector2D& CLineSegment2D::GetEndPointA() const
                {
                    return m_pSourcePoints->GetPointA();
                }

                const CVector2D& CLineSegment2D::GetEndPointB() const
                {
                    return m_pSourcePoints->GetPointB();
                }

                bool CLineSegment2D::IsInsideSegment(const CVector2D& X, const real Tolerance) const
                {
                    const real InnerTolerance = TMax(_REAL_EPSILON_, RealAbs(Tolerance));
                    const CVector2D Delta = X - m_pSourcePoints->GetMidPoint();
                    return (RealAbs(m_Normal.ScalarProduct(Delta)) < InnerTolerance) && ((m_Direction.ScalarProduct(Delta) - (m_Length * _REAL_HALF_)) < InnerTolerance);
                }

                real CLineSegment2D::GetDistanceToSegment(const CVector2D& X) const
                {
                    const real ParalleProjectionDistance = m_Direction.ScalarProduct(X - m_pSourcePoints->GetMidPoint()) - (m_Length * _REAL_HALF_);
                    if (_1D::IsNegative(ParalleProjectionDistance))
                    {
                        return _REAL_ZERO_;
                    }
                    return ParalleProjectionDistance;
                }

                const CVector2D& CLineSegment2D::GetClosesEndPoint(const CVector2D& X) const
                {
                    return (ComputeSquareDistance(X, m_pSourcePoints->GetPointA()) < ComputeSquareDistance(X, m_pSourcePoints->GetPointB())) ? m_pSourcePoints->GetPointA() : m_pSourcePoints->GetPointB();
                }

                bool CLineSegment2D::GetOppositeEndPoint(const CVector2D& X, CVector2D& Y) const
                {
                    if (m_pSourcePoints->GetPointA() == X)
                    {
                        Y = m_pSourcePoints->GetPointB();
                        return true;
                    }
                    else if (m_pSourcePoints->GetPointB() == X)
                    {
                        Y = m_pSourcePoints->GetPointA();
                        return true;
                    }
                    return false;
                }

                real CLineSegment2D::GetClosestEndpointsDistance(const CLineSegment2D* pLineSegment, CVector2D& Local, CVector2D& Remote) const
                {
                    if (!pLineSegment)
                    {
                        return _REAL_MAX_;
                    }
                    if (pLineSegment == this)
                    {
                        return _REAL_ZERO_;
                    }
                    const CVector2D& LocalA = m_pSourcePoints->GetPointA();
                    const CVector2D& LocalB = m_pSourcePoints->GetPointB();
                    const CVector2D& RemoteA = pLineSegment->m_pSourcePoints->GetPointA();
                    const CVector2D& RemoteB = pLineSegment->m_pSourcePoints->GetPointB();
                    const real Distances[] = { ComputeSquareDistance(LocalA, RemoteA), ComputeSquareDistance(LocalA, RemoteB), ComputeSquareDistance(LocalB, RemoteA), ComputeSquareDistance(LocalB, RemoteB) };
                    real MinimalDistance = Distances[0];
                    uint Index = 0;
                    for (uint i = 1; i < 4; ++i)
                        if (Distances[i] < MinimalDistance)
                        {
                            MinimalDistance = Distances[i];
                            Index = i;
                        }
                    switch (Index)
                    {
                        case 0:
                            Local = LocalA;
                            Remote = RemoteA;
                            return -RealSqrt(MinimalDistance);
                        case 1:
                            Local = LocalA;
                            Remote = RemoteB;
                            return -RealSqrt(MinimalDistance);
                        case 2:
                            Local = LocalB;
                            Remote = RemoteA;
                            return RealSqrt(MinimalDistance);
                        case 3:
                            Local = LocalB;
                            Remote = RemoteB;
                            return RealSqrt(MinimalDistance);
                    }
                    return _REAL_ZERO_;
                }

                real CLineSegment2D::GetOrientationDeviation(const CLineSegment2D* pLineSegment) const
                {
                    const real Angle = RealArcCosinus(GetDirection().ScalarProduct(pLineSegment->GetDirection()));
                    return (Angle > _REAL_HALF_PI_) ? (_REAL_PI_ - Angle) : Angle;
                }

                bool CLineSegment2D::IsInsideExtensionZone(const CVector2D& EndPoint, const CVector2D& X) const
                {
                    const CVector2D MidPoint = m_pSourcePoints->GetMidPoint();
                    const CVector2D Delta = X - MidPoint;
                    if (_1D::IsPositive(m_Direction.ScalarProduct(Delta) - (m_Length * _REAL_HALF_)))
                    {
                        if (m_pSourcePoints->GetPointA() == EndPoint)
                        {
                            const CVector2D DirectionDelta = m_pSourcePoints->GetPointA() - MidPoint;
                            return _1D::IsPositive(DirectionDelta.ScalarProduct(Delta));
                        }
                        else if (m_pSourcePoints->GetPointB() == EndPoint)
                        {
                            const CVector2D DirectionDelta = m_pSourcePoints->GetPointB() - MidPoint;
                            return _1D::IsPositive(DirectionDelta.ScalarProduct(Delta));
                        }
                    }
                    return false;
                }
            }
        }
    }
}
