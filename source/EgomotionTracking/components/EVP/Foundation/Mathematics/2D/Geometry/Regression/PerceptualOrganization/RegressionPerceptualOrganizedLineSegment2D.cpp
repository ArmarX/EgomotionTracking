/*
 * RegressionPerceptualOrganizedLineSegment2D.cpp
 *
 *  Created on: 03.01.2012
 *      Author: gonzalez
 */

#include "RegressionPerceptualOrganizedLineSegment2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                namespace Regression
                {
                    namespace PerceptualOrganization
                    {

                        CRegressionPerceptualOrganizedLineSegment2D::CRegressionPerceptualOrganizedLineSegment2D(const CVector2D& EndPointA, const CVector2D& EndPointB, const CVector2D& RegressionCenter, const CVector2D& RegressionMainAxis, const CVector2D& RegressionSecondaryAxis, const CVector2D& RegressionMinAxesPoint, const CVector2D& RegressionMaxAxesPoint, const real MaximalDeviation, const real MeanDeviation, const CPerceptualOrganizedGeometricElement::PerceptualOrganizationType Type, const list<CRegressionLineSegment2D*>& RegressionLineSegments) :
                            CPerceptualOrganizedGeometricElement(Type), CRegressionLineSegment2D(EndPointA, EndPointB, RegressionCenter, RegressionMainAxis, RegressionSecondaryAxis, RegressionMinAxesPoint, RegressionMaxAxesPoint, MaximalDeviation, MeanDeviation), m_RegressionLineSegments(RegressionLineSegments)
                        {

                        }

                        CRegressionPerceptualOrganizedLineSegment2D::~CRegressionPerceptualOrganizedLineSegment2D()
                            = default;

                        list<CRegressionLineSegment2D*> CRegressionPerceptualOrganizedLineSegment2D::GetRegressionLineSegments() const
                        {
                            return m_RegressionLineSegments;
                        }
                    }
                }
            }
        }
    }
}
