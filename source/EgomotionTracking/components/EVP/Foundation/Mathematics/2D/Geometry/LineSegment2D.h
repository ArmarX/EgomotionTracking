/*
 * LineSegment2D.h
 *
 *  Created on: 29.12.2011
 *      Author: gonzalez
 */

#pragma once

#include "Line2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                class CLineSegment2D: public CLine2D
                {
                public:

                    CLineSegment2D(const CVector2D& A, const CVector2D& B, const void* pData = NULL, const CGeometricPrimitive2D* pParentA = NULL, const CGeometricPrimitive2D* pParentB = NULL);
                    ~CLineSegment2D() override;

                    const CVector2D& GetNormal() const;
                    real GetLength() const;
                    const CVector2D& GetEndPointA() const;
                    const CVector2D& GetEndPointB() const;

                    bool IsInsideSegment(const CVector2D& X, const real Tolerance) const;
                    real GetDistanceToSegment(const CVector2D& X) const;
                    const CVector2D& GetClosesEndPoint(const CVector2D& X) const;

                    bool GetOppositeEndPoint(const CVector2D& X, CVector2D& Y) const;
                    real GetClosestEndpointsDistance(const CLineSegment2D* pLineSegment, CVector2D& Local, CVector2D& Remote) const;
                    real GetOrientationDeviation(const CLineSegment2D* pLineSegment) const;

                    bool IsInsideExtensionZone(const CVector2D& EndPoint, const CVector2D& X) const;

                protected:

                    CVector2D m_Normal;
                    real m_Length;
                };
            }
        }
    }
}

