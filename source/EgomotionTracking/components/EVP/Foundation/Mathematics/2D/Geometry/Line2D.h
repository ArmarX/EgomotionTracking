/*
 * Line2D.h
 *
 *  Created on: 28.12.2011
 *      Author: gonzalez
 */

#pragma once

#include "GeometricPrimitive2D.h"
#include "SourcePoints.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                class CPoint2D;

                class CLine2D: public CGeometricPrimitive2D
                {
                public:

                    enum ArgumentMode
                    {
                        eDirectionVectorAndSupportPoint, ePointAPointB
                    };

                    CLine2D();
                    CLine2D(const real Xa, const real Ya, const real Xb, const real Yb, const ArgumentMode Mode, const CGeometricPrimitive2D* pParentA = NULL, const CGeometricPrimitive2D* pParentB = NULL);
                    CLine2D(const CVector2D& A, const CVector2D& B, const ArgumentMode Mode, const CGeometricPrimitive2D* pParentA = NULL, const CGeometricPrimitive2D* pParentB = NULL);
                    ~CLine2D() override;

                    void SetLineByPoints(const CVector2D& A, const CVector2D& B);
                    bool HasSourcePoints() const;
                    bool GetSourcePoints(CVector2D& A, CVector2D& B) const;
                    const CVector2D& GetSourcePointA() const;
                    const CVector2D& GetSourcePointB() const;

                    void SetDirection(const CVector2D& Direction);
                    const CVector2D& GetDirection() const;

                    void SetSupportPoint(const CVector2D& SupportPoint);
                    const CVector2D& GetSupportPoint() const;

                    CVector2D GetPointAtUnitaryDisplacement() const;
                    CVector2D GetPointAtDisplacement(const real Displacement) const;
                    CVector2D GetClosestPoint(const CVector2D& X) const;

                    std::string ToString() const override;
                    bool operator==(const CGeometricPrimitive2D& GeometricPrimitive) const override;
                    IntersectionType AnalyzeIntersection(const CGeometricPrimitive2D* pGeometricPrimitive, GeometricPrimitive2DTypeId* pIntersectionTypeId = NULL) const override;
                    CGeometricPrimitive2D* GenerateIntersection(const CGeometricPrimitive2D* pGeometricPrimitive, GeometricPrimitive2DTypeId* pIntersectionTypeId = NULL, IntersectionType* pIntersectionType = NULL) const override;

                    bool CalculateIntersectionPoint(const CLine2D* pLine, CVector2D& X) const;

                protected:

                    IntersectionType AnalyzeIntersection(const CPoint2D* pPoint, GeometricPrimitive2DTypeId* pIntersectionTypeId) const;
                    IntersectionType AnalyzeIntersection(const CLine2D* pLine, GeometricPrimitive2DTypeId* pIntersectionTypeId) const;
                    CGeometricPrimitive2D* GenerateIntersection(const CPoint2D* pPoint, GeometricPrimitive2DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;
                    CGeometricPrimitive2D* GenerateIntersection(const CLine2D* pLine, GeometricPrimitive2DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const;

                    CVector2D m_Direction;
                    CVector2D m_SupportPoint;
                    CSourcePoints* m_pSourcePoints;
                };
            }
        }
    }
}

