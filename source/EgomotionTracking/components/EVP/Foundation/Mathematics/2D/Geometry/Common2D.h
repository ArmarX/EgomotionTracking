/*
 * Common2D.h
 *
 *  Created on: 28.12.2011
 *      Author: gonzalez
 */

#pragma once

#include "../Vector2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                bool AreUnitaryVectorsParallel(const CVector2D& A, const CVector2D& B);
                real ComputeSquareDistance(const CVector2D& A, const CVector2D& B);
                real ComputeDistance(const CVector2D& A, const CVector2D& B);
                CVector2D ComputeUnitaryDirectionVector(const CVector2D& A, const CVector2D& B);
                CVector2D ComputeProjectionPointToLine(const CVector2D& SupportPoint, const CVector2D& Direction, const CVector2D& X);
                bool ComputeLineToLineIntersectionPoint(const CVector2D& SupportPointA, const CVector2D& DirectionA, const CVector2D& SupportPointB, const CVector2D& DirectionB, CVector2D& Point);
                bool ComputeLineSegmentToLineSegmentIntersectionPoint(const CVector2D& AP, const CVector2D& AN, const CVector2D& BP, const CVector2D& BN, CVector2D& Point);
            }
        }
    }
}

