/*
 * Point2D.h
 *
 *  Created on: 28.12.2011
 *      Author: gonzalez
 */

#pragma once

#include "GeometricPrimitive2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            namespace Geometry
            {
                class CPoint2D: public CGeometricPrimitive2D
                {
                public:

                    CPoint2D();
                    CPoint2D(const real X, const real Y, const CGeometricPrimitive2D* pParentA = NULL, const CGeometricPrimitive2D* pParentB = NULL);
                    CPoint2D(const CVector2D& Point, const CGeometricPrimitive2D* pParentA = NULL, const CGeometricPrimitive2D* pParentB = NULL);
                    ~CPoint2D() override;

                    void SetPoint(const CVector2D& Point);
                    const CVector2D& GetPoint() const;

                    std::string ToString() const override;
                    bool operator==(const CGeometricPrimitive2D& GeometricPrimitive) const override ;
                    IntersectionType AnalyzeIntersection(const CGeometricPrimitive2D* pGeometricPrimitive, GeometricPrimitive2DTypeId* pIntersectionTypeId = NULL) const override ;
                    CGeometricPrimitive2D* GenerateIntersection(const CGeometricPrimitive2D* pGeometricPrimitive, GeometricPrimitive2DTypeId* pIntersectionTypeId = NULL, IntersectionType* pIntersectionType = NULL) const override ;

                protected:

                    IntersectionType AnalyzeIntersection(const CPoint2D* pPoint, GeometricPrimitive2DTypeId* pIntersectionTypeId) const ;
                    CGeometricPrimitive2D* GenerateIntersection(const CPoint2D* pPoint, GeometricPrimitive2DTypeId* pIntersectionTypeId, IntersectionType* pIntersectionType) const ;

                private:

                    CVector2D m_Point;
                };
            }
        }
    }
}

