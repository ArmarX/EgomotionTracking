/*
 * StructuralTensor2D.cpp
 *
 *  Created on: 21.09.2011
 *      Author: gonzalez
 */

#include "StructuralTensor2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
            CStructuralTensor2D::CStructuralTensor2D() :
                m_Covariance(2, 2), m_EigenVectors(2, 2), m_EigenValues(2, 2)
            {
                m_IsSingular = true;
            }

            //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
            CStructuralTensor2D::CStructuralTensor2D(const real Cxx, const real Cxy, const real Cyy) :
                m_Covariance(2, 2), m_EigenVectors(2, 2), m_EigenValues(2, 2)
            {
                m_IsSingular = RealAbs(Cxx * Cyy - Cxy * Cxy) < _REAL_EPSILON_;
                if (!m_IsSingular)
                {
                    m_Covariance(0, 0) = ND::LinearAlgebra::MatrixReal(Cxx);
                    m_Covariance(0, 1) = ND::LinearAlgebra::MatrixReal(Cxy);
                    m_Covariance(1, 0) = ND::LinearAlgebra::MatrixReal(Cxy);
                    m_Covariance(1, 1) = ND::LinearAlgebra::MatrixReal(Cyy);
                }
            }

            CStructuralTensor2D::~CStructuralTensor2D()
                = default;

            //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
            bool CStructuralTensor2D::Decomposition(const real Cxx, const real Cxy, const real Cyy)
            {
                m_IsSingular = (RealAbs(Cxx * Cyy - Cxy * Cxy) < _REAL_EPSILON_);
                if (m_IsSingular)
                {
                    return false;
                }
                m_Covariance(0, 0) = ND::LinearAlgebra::MatrixReal(Cxx);
                m_Covariance(0, 1) = ND::LinearAlgebra::MatrixReal(Cxy);
                m_Covariance(1, 0) = ND::LinearAlgebra::MatrixReal(Cxy);
                m_Covariance(1, 1) = ND::LinearAlgebra::MatrixReal(Cyy);
                ND::LinearAlgebra::SVD(m_Covariance, m_EigenValues, m_EigenVectors);
                return true;
            }

            bool CStructuralTensor2D::IsSingular() const
            {
                return m_IsSingular;
            }

            //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
            bool CStructuralTensor2D::Decomposition()
            {
                if (m_IsSingular)
                {
                    return false;
                }
                ND::LinearAlgebra::SVD(m_Covariance, m_EigenValues, m_EigenVectors);
                return true;
            }

            //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
            real CStructuralTensor2D::GetMainAxisLength() const
            {
                return RealSqrt(real(m_EigenValues(0, 0)));
            }

            //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
            real CStructuralTensor2D::GetSecondaryAxisLength() const
            {
                return RealSqrt(real(m_EigenValues(1, 1)));
            }

            //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
            CVector2D CStructuralTensor2D::GetUnitaryMainAxis() const
            {
                return CVector2D::CreateCannonicalForm(real(m_EigenVectors(0, 0)), real(m_EigenVectors(1, 0)));
            }

            //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
            CVector2D CStructuralTensor2D::GetUnitarySecondaryAxis() const
            {
                return CVector2D::CreateCannonicalForm(real(m_EigenVectors(0, 1)), real(m_EigenVectors(1, 1)));
            }
        }
    }
}
