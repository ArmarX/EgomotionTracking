/*
 * StructuralTensor2D.h
 *
 *  Created on: 21.09.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"
#include "../../../Foundation/Mathematics/2D/Vector2D.h"
#include "../ND/LinearAlgebra.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            class CStructuralTensor2D
            {
            public:

                CStructuralTensor2D();
                CStructuralTensor2D(const real Cxx, const real Cxy, const real Cyy);
                ~CStructuralTensor2D();

                bool Decomposition(const real Cxx, const real Cxy, const real Cyy);
                bool IsSingular() const;
                bool Decomposition();
                real GetMainAxisLength() const;
                real GetSecondaryAxisLength() const;
                CVector2D GetUnitaryMainAxis() const;
                CVector2D GetUnitarySecondaryAxis() const;

            protected:

                bool m_IsSingular;
                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
                ND::LinearAlgebra::CMatrixND m_Covariance;
                ND::LinearAlgebra::CMatrixND m_EigenVectors;
                ND::LinearAlgebra::CMatrixND m_EigenValues;
            };
        }
    }
}

