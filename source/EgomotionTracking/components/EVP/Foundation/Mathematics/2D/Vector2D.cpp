/*
 * Vector.cpp
 *
 *  Created on: 21.09.2011
 *      Author: gonzalez
 */
#include "Vector2D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            const CVector2D CVector2D::s_Point_At_Origin_2D(_REAL_ZERO_, _REAL_ZERO_);
            const CVector2D CVector2D::s_Point_At_Minus_Infinity_2D(-_REAL_MAX_, -_REAL_MAX_);
            const CVector2D CVector2D::s_Point_At_Plus_Infinity_2D(_REAL_MAX_, _REAL_MAX_);
            const CVector2D CVector2D::s_Direction_Axis_X_2D(_REAL_ONE_, _REAL_ZERO_);
            const CVector2D CVector2D::s_Direction_Axis_Y_2D(_REAL_ZERO_, _REAL_ONE_);

            CVector2D CVector2D::CreateLinealCombination(const CVector2D& A, const CVector2D& B, const real SA, const real SB)
            {
                return CVector2D(A.m_Elements[0] * SA + B.m_Elements[0] * SB, A.m_Elements[1] * SA + B.m_Elements[1] * SB);
            }

            CVector2D CVector2D::CreateComplementaryLinealCombination(const CVector2D& A, const CVector2D& B, const real SA)
            {
                const real SB = _REAL_ONE_ - SA;
                return CVector2D(A.m_Elements[0] * SA + B.m_Elements[0] * SB, A.m_Elements[1] * SA + B.m_Elements[1] * SB);
            }

            CVector2D CVector2D::CreateMidPoint(const CVector2D& A, const CVector2D& B)
            {
                return CVector2D((A.m_Elements[0] + B.m_Elements[0]) * _REAL_HALF_, (A.m_Elements[1] + B.m_Elements[1]) * _REAL_HALF_);
            }

            CVector2D CVector2D::CreateCannonicalForm(const real X, const real Y)
            {
                const real AX = RealAbs(X);
                const real AY = RealAbs(Y);
                if (TMax(AX, AY) > _REAL_EPSILON_)
                {
                    if (AX > AY)
                    {
                        if (X < _REAL_ZERO_)
                        {
                            return CVector2D(-X, -Y);
                        }
                        return CVector2D(X, Y);
                    }
                    else if (AX < AY)
                    {
                        if (Y < _REAL_ZERO_)
                        {
                            return CVector2D(-X, -Y);
                        }
                        return CVector2D(X, Y);
                    }
                    else
                    {
                        if (X < _REAL_ZERO_)
                        {
                            return CVector2D(-X, -Y);
                        }
                        return CVector2D(X, Y);
                    }
                }
                return CVector2D();
            }

            CVector2D::CVector2D()
            {
                memset(m_Elements, 0, sizeof(real) * 2);
            }

            CVector2D::CVector2D(const real X, const real Y)
            {
                m_Elements[0] = X;
                m_Elements[1] = Y;
            }

            CVector2D::CVector2D(const CVector2D& Vector)
            {
                memcpy(m_Elements, Vector.m_Elements, sizeof(real) * 2);
            }

            CVector2D::CVector2D(const CPixelLocation& Location)
            {
                m_Elements[0] = Location.m_x;
                m_Elements[1] = Location.m_y;
            }

            bool CVector2D::SetCannonicalForm()
            {
                const real Ax = RealAbs(m_Elements[0]);
                const real Ay = RealAbs(m_Elements[1]);
                if (TMax(Ax, Ay) > _REAL_EPSILON_)
                {
                    if (Ax > Ay)
                    {
                        if (m_Elements[0] < _REAL_ZERO_)
                        {
                            Negate();
                        }
                    }
                    else if (Ax < Ay)
                    {
                        if (m_Elements[1] < _REAL_ZERO_)
                        {
                            Negate();
                        }
                    }
                    else if (m_Elements[0] < _REAL_ZERO_)
                    {
                        Negate();
                    }
                    return true;
                }
                return false;
            }

            void CVector2D::SetX(const real X)
            {
                m_Elements[0] = X;
            }

            void CVector2D::SetY(const real Y)
            {
                m_Elements[1] = Y;
            }

            void CVector2D::SetValue(const real X, const real Y)
            {
                m_Elements[0] = X;
                m_Elements[1] = Y;
            }

            void CVector2D::SetMaximalComponentValue(const real X, const real Y)
            {
                m_Elements[0] = TMax(m_Elements[0], X);
                m_Elements[1] = TMax(m_Elements[1], Y);
            }

            void CVector2D::SetMinimalComponentValue(const real X, const real Y)
            {
                m_Elements[0] = TMin(m_Elements[0], X);
                m_Elements[1] = TMin(m_Elements[1], Y);
            }

            void CVector2D::SetMaximalComponentValue(const CVector2D& Vector)
            {
                m_Elements[0] = TMax(m_Elements[0], Vector.m_Elements[0]);
                m_Elements[1] = TMax(m_Elements[1], Vector.m_Elements[1]);
            }

            void CVector2D::SetMinimalComponentValue(const CVector2D& Vector)
            {
                m_Elements[0] = TMin(m_Elements[0], Vector.m_Elements[0]);
                m_Elements[1] = TMin(m_Elements[1], Vector.m_Elements[1]);
            }

            void CVector2D::SetMidPoint(const CVector2D& A, const CVector2D& B)
            {
                m_Elements[0] = (A.m_Elements[0] + B.m_Elements[0]) * _REAL_HALF_;
                m_Elements[1] = (A.m_Elements[1] + B.m_Elements[1]) * _REAL_HALF_;
            }

            void CVector2D::SetWeighted(const CVector2D& Vector, const real Weight)
            {
                m_Elements[0] = Vector.m_Elements[0] * Weight;
                m_Elements[1] = Vector.m_Elements[1] * Weight;
            }

            void CVector2D::SetWeightedInverse(const CVector2D& Vector, const real Weight)
            {
                m_Elements[0] = Vector.m_Elements[0] / Weight;
                m_Elements[1] = Vector.m_Elements[1] / Weight;
            }

            void CVector2D::SetLinealCombination(const CVector2D& A, const CVector2D& B, const real SA, const real SB)
            {
                m_Elements[0] = A.m_Elements[0] * SA + B.m_Elements[0] * SB;
                m_Elements[1] = A.m_Elements[1] * SA + B.m_Elements[1] * SB;
            }

            void CVector2D::SetZero()
            {
                memset(m_Elements, 0, sizeof(real) * 2);
            }

            void CVector2D::Negate()
            {
                m_Elements[0] = -m_Elements[0];
                m_Elements[1] = -m_Elements[1];
            }

            void CVector2D::Rotate(const real Alpha)
            {
                const real CosinusAlpha = RealCosinus(Alpha);
                const real SinusAlpha = RealSinus(Alpha);
                m_Elements[0] = m_Elements[0] * CosinusAlpha - m_Elements[1] * SinusAlpha;
                m_Elements[1] = m_Elements[0] * SinusAlpha + m_Elements[1] * CosinusAlpha;
            }

            void CVector2D::AddWeightedOffset(const CVector2D& Vector, const real Weight)
            {
                m_Elements[0] += Vector.m_Elements[0] * Weight;
                m_Elements[1] += Vector.m_Elements[1] * Weight;
            }

            void CVector2D::AddOffset(const real Dx, const real Dy)
            {
                m_Elements[0] += Dx;
                m_Elements[1] += Dy;
            }

            void CVector2D::AddOffsetX(const real Dx)
            {
                m_Elements[0] += Dx;
            }

            void CVector2D::AddOffsetY(const real Dy)
            {
                m_Elements[1] += Dy;
            }

            void CVector2D::AddPartialCovariance(const CVector2D& Vector, real& Axx, real& Ayy) const
            {
                const real Dx = m_Elements[0] - Vector.m_Elements[0];
                const real Dy = m_Elements[1] - Vector.m_Elements[1];
                Axx += Dx * Dx;
                Ayy += Dy * Dy;
            }

            void CVector2D::AddCovariance(const CVector2D& Vector, real& Axx, real& Axy, real& Ayy) const
            {
                const real Dx = m_Elements[0] - Vector.m_Elements[0];
                const real Dy = m_Elements[1] - Vector.m_Elements[1];
                Axx += Dx * Dx;
                Axy += Dx * Dy;
                Ayy += Dy * Dy;
            }

            void CVector2D::AddWeightedCovariance(const CVector2D& Vector, const real Weight, real& Axx, real& Axy, real& Ayy, real& Aw) const
            {
                const real Dx = m_Elements[0] - Vector.m_Elements[0];
                const real Dy = m_Elements[1] - Vector.m_Elements[1];
                Axx += Dx * Dx * Weight;
                Axy += Dx * Dy * Weight;
                Ayy += Dy * Dy * Weight;
                Aw += Weight;
            }

            bool CVector2D::IsNull() const
            {
                return (RealAbs(m_Elements[0]) < _REAL_EPSILON_) && (RealAbs(m_Elements[1]) < _REAL_EPSILON_);
            }

            bool CVector2D::IsNonNull() const
            {
                return (RealAbs(m_Elements[0]) > _REAL_EPSILON_) || (RealAbs(m_Elements[1]) > _REAL_EPSILON_);
            }

            bool CVector2D::IsAtInfinity() const
            {
                return (RealAbs(m_Elements[0]) == _REAL_MAX_) || (RealAbs(m_Elements[1]) == _REAL_MAX_);
            }

            bool CVector2D::IsNotAtInfinity() const
            {
                return (RealAbs(m_Elements[0]) != _REAL_MAX_) && (RealAbs(m_Elements[1]) != _REAL_MAX_);
            }

            bool CVector2D::IsUnitary() const
            {
                return RealAbs(RealHypotenuse(m_Elements[0], m_Elements[1]) - _REAL_ONE_) < _REAL_EPSILON_;
            }

            bool CVector2D::IsNonUnitary() const
            {
                return RealHypotenuse(m_Elements[0], m_Elements[1]) >= _REAL_EPSILON_;
            }

            bool CVector2D::IsZeroLength() const
            {
                return (RealAbs(m_Elements[0]) < _REAL_EPSILON_) && (RealAbs(m_Elements[1]) < _REAL_EPSILON_);
            }

            bool CVector2D::IsNonZeroLength() const
            {
                return (RealAbs(m_Elements[0]) > _REAL_EPSILON_) || (RealAbs(m_Elements[1]) > _REAL_EPSILON_);
            }

            real CVector2D::GetX() const
            {
                return m_Elements[0];
            }

            real CVector2D::GetY() const
            {
                return m_Elements[1];
            }

            coordinate CVector2D::GetTruncatedDiscreteX() const
            {
                return coordinate(m_Elements[0]);
            }

            coordinate CVector2D::GetTruncatedDiscreteY() const
            {
                return coordinate(m_Elements[1]);
            }

            CPixelLocation CVector2D::GetRoundedLocation() const
            {
                return CPixelLocation(RealRound(m_Elements[0]), RealRound(m_Elements[1]));
            }

            real CVector2D::GetAngle() const
            {
                return RealAtan2(m_Elements[1], m_Elements[0]);
            }

            real CVector2D::GetHalfResolutionAngle() const
            {
                if (RealAbs(m_Elements[0]) > _REAL_EPSILON_)
                {
                    return RealAtan(m_Elements[1] / m_Elements[0]);
                }
                if (RealAbs(m_Elements[1]) < _REAL_EPSILON_)
                {
                    return _REAL_ZERO_;
                }
                return (m_Elements[1] > 0) ? _REAL_HALF_PI_ : -_REAL_HALF_PI_;
            }

            real CVector2D::GetNormalizedHalfResolutionAngle() const
            {
                return (m_Elements[1] >= _REAL_ZERO_) ? RealAtan2(m_Elements[1], m_Elements[0]) * _REAL_INVERSE_PI_ : RealAtan2(-m_Elements[1], -m_Elements[0]) * _REAL_INVERSE_PI_;
            }

            real CVector2D::GetAbsoluteAngle() const
            {
                const real Alpha = RealAtan2(m_Elements[1], m_Elements[0]);
                return (Alpha < _REAL_ZERO_) ? _REAL_2PI_ + Alpha : Alpha;
            }

            real CVector2D::GetAbsoluteRotation() const
            {
                return RealAbs(RealAtan2(m_Elements[1], m_Elements[0]));
            }

            real CVector2D::GetAngle(const CVector2D& Vector) const
            {
                return RealArcCosinus((m_Elements[0] * Vector.m_Elements[0] + m_Elements[1] * Vector.m_Elements[1]) / (RealHypotenuse(m_Elements[0], m_Elements[1]) * RealHypotenuse(Vector.m_Elements[0], Vector.m_Elements[1])));
            }

            real CVector2D::GetAperture(const CVector2D& Vector) const
            {
                return (m_Elements[0] * Vector.m_Elements[0] + m_Elements[1] * Vector.m_Elements[1]) / (RealHypotenuse(m_Elements[0], m_Elements[1]) * RealHypotenuse(Vector.m_Elements[0], Vector.m_Elements[1]));
            }

            real CVector2D::GetAbsoluteAperture(const CVector2D& Vector) const
            {
                return RealAbs((m_Elements[0] * Vector.m_Elements[0] + m_Elements[1] * Vector.m_Elements[1]) / (RealHypotenuse(m_Elements[0], m_Elements[1]) * RealHypotenuse(Vector.m_Elements[0], Vector.m_Elements[1])));
            }

            real CVector2D::GetDistance(const CVector2D& Vector) const
            {
                return RealHypotenuse(m_Elements[0] - Vector.m_Elements[0], m_Elements[1] - Vector.m_Elements[1]);
            }

            real CVector2D::GetLength() const
            {
                return RealHypotenuse(m_Elements[0], m_Elements[1]);
            }

            real CVector2D::GetSquareLength() const
            {
                return m_Elements[0] * m_Elements[0] + m_Elements[1] * m_Elements[1];
            }

            CVector2D CVector2D::GetRotated(const real Alpha) const
            {
                const real CosinusAlpha = RealCosinus(Alpha);
                const real SinusAlpha = RealSinus(Alpha);
                return CVector2D(CosinusAlpha * m_Elements[0] - SinusAlpha * m_Elements[1], SinusAlpha * m_Elements[0] + CosinusAlpha * m_Elements[1]);
            }

            CVector2D CVector2D::GetPerpendicular() const
            {
                return CVector2D(-m_Elements[1], m_Elements[0]);
            }

            void CVector2D::GetCovariance(const CVector2D& Vector, real& Cxx, real& Cxy, real& Cyy) const
            {
                const real Dx = m_Elements[0] - Vector.m_Elements[0];
                const real Dy = m_Elements[1] - Vector.m_Elements[1];
                Cxx = Dx * Dx;
                Cxy = Dx * Dy;
                Cyy = Dy * Dy;
            }

            real CVector2D::Normalize()
            {
                const real Length = RealSqrt(m_Elements[0] * m_Elements[0] + m_Elements[1] * m_Elements[1]);
                m_Elements[0] /= Length;
                m_Elements[1] /= Length;
                return Length;
            }

            void CVector2D::ScaleAnisotropic(const real Sx, const real Sy)
            {
                m_Elements[0] /= Sx;
                m_Elements[1] /= Sy;
            }

            std::string CVector2D::ToString(const uint Precision) const
            {
                std::ostringstream OutputString;
                OutputString.precision(Precision);
                OutputString << "<Vector2D X=\"" << m_Elements[0] << "\" Y=\"" << m_Elements[1] << "\"/>";
                return OutputString.str();
            }

            const real* CVector2D::GetElements() const
            {
                return m_Elements;
            }

            CPixelLocation CVector2D::GetQuantizedOrientation() const
            {
                return CPixelLocation((m_Elements[0] >= _REAL_QUANTIZATION_THRESHOLD_) ? 1 : ((m_Elements[0] < -_REAL_QUANTIZATION_THRESHOLD_) ? -1 : 0), (m_Elements[1] >= _REAL_QUANTIZATION_THRESHOLD_) ? 1 : ((m_Elements[1] < -_REAL_QUANTIZATION_THRESHOLD_) ? -1 : 0));
            }

            CPixelLocation CVector2D::GetForwardsDiscreteLocation(const CPixelLocation& CurrentLocation) const
            {
                return CPixelLocation((m_Elements[0] >= _REAL_QUANTIZATION_THRESHOLD_) ? (CurrentLocation.m_x + 1) : ((m_Elements[0] < -_REAL_QUANTIZATION_THRESHOLD_) ? (CurrentLocation.m_x - 1) : CurrentLocation.m_x), (m_Elements[1] >= _REAL_QUANTIZATION_THRESHOLD_) ? (CurrentLocation.m_y + 1) : ((m_Elements[1] < -_REAL_QUANTIZATION_THRESHOLD_) ? (CurrentLocation.m_y - 1) : CurrentLocation.m_y));
            }

            CPixelLocation CVector2D::GetBackwardsDiscreteLocation(const CPixelLocation& CurrentLocation) const
            {
                return CPixelLocation((m_Elements[0] >= _REAL_QUANTIZATION_THRESHOLD_) ? (CurrentLocation.m_x - 1) : ((m_Elements[0] < -_REAL_QUANTIZATION_THRESHOLD_) ? (CurrentLocation.m_x + 1) : CurrentLocation.m_x), (m_Elements[1] >= _REAL_QUANTIZATION_THRESHOLD_) ? (CurrentLocation.m_y - 1) : ((m_Elements[1] < -_REAL_QUANTIZATION_THRESHOLD_) ? (CurrentLocation.m_y + 1) : CurrentLocation.m_y));
            }

            real CVector2D::VectorProduct(const CVector2D& Vector) const
            {
                return (m_Elements[0] * Vector.m_Elements[1]) - (m_Elements[1] * Vector.m_Elements[0]);
            }

            real CVector2D::ScalarProduct(const CVector2D& Vector) const
            {
                return m_Elements[0] * Vector.m_Elements[0] + m_Elements[1] * Vector.m_Elements[1];
            }

            real CVector2D::ScalarProduct(const CPixelLocation& Location) const
            {
                return m_Elements[0] * real(Location.m_x) + m_Elements[1] * real(Location.m_y);
            }

            real CVector2D::ScalarProduct(const real X, const real Y) const
            {
                return m_Elements[0] * X + m_Elements[1] * Y;
            }

            real CVector2D::ScalarProductSquare(const real X, const real Y) const
            {
                const real ScalarProduct = m_Elements[0] * X + m_Elements[1] * Y;
                return ScalarProduct * ScalarProduct;
            }

            real CVector2D::GetDistance(const CPixelLocation& Location) const
            {
                return RealHypotenuse(m_Elements[0] - real(Location.m_x), m_Elements[1] - real(Location.m_y));
            }

            real CVector2D::GetSquareDistance(const CVector2D& Vector) const
            {
                const real Dx = m_Elements[0] - Vector.m_Elements[0];
                const real Dy = m_Elements[1] - Vector.m_Elements[1];
                return Dx * Dx + Dy * Dy;
            }

            real CVector2D::GetSquareDistance(const CPixelLocation& Location) const
            {
                const real Dx = m_Elements[0] - real(Location.m_x);
                const real Dy = m_Elements[1] - real(Location.m_y);
                return Dx * Dx + Dy * Dy;
            }

            void CVector2D::operator=(const CVector2D& Vector)
            {
                memcpy(m_Elements, Vector.m_Elements, sizeof(real) * 2);
            }

            void CVector2D::operator=(const CPixelLocation& Location)
            {
                m_Elements[0] = Location.m_x;
                m_Elements[1] = Location.m_y;
            }

            void CVector2D::operator+=(const CVector2D& Vector)
            {
                m_Elements[0] += Vector.m_Elements[0];
                m_Elements[1] += Vector.m_Elements[1];
            }

            void CVector2D::operator-=(const CVector2D& Vector)
            {
                m_Elements[0] -= Vector.m_Elements[0];
                m_Elements[1] -= Vector.m_Elements[1];
            }

            void CVector2D::operator*=(const real Scalar)
            {
                m_Elements[0] *= Scalar;
                m_Elements[1] *= Scalar;
            }

            void CVector2D::operator/=(const real Scalar)
            {
                m_Elements[0] /= Scalar;
                m_Elements[1] /= Scalar;
            }

            bool operator==(const CVector2D& A, const CVector2D& B)
            {
                return (RealAbs(A.GetX() - B.GetX()) < _REAL_EPSILON_) && (RealAbs(A.GetY() - B.GetY()) < _REAL_EPSILON_);
            }

            bool operator!=(const CVector2D& A, const CVector2D& B)
            {
                return (RealAbs(A.GetX() - B.GetX()) > _REAL_EPSILON_) || (RealAbs(A.GetY() - B.GetY()) > _REAL_EPSILON_);
            }

            const CVector2D operator/(const CVector2D& Vector, const real Scalar)
            {
                return CVector2D(Vector.GetX() / Scalar, Vector.GetY() / Scalar);
            }

            const CVector2D operator*(const CVector2D& Vector, const real Scalar)
            {
                return CVector2D(Vector.GetX() * Scalar, Vector.GetY() * Scalar);
            }

            const CVector2D operator*(const real Scalar, const CVector2D& Vector)
            {
                return CVector2D(Vector.GetX() * Scalar, Vector.GetY() * Scalar);
            }

            const CVector2D operator+(const CVector2D& A, const CVector2D& B)
            {
                return CVector2D(A.GetX() + B.GetX(), A.GetY() + B.GetY());
            }

            const CVector2D operator-(const CVector2D& A, const CVector2D& B)
            {
                return CVector2D(A.GetX() - B.GetX(), A.GetY() - B.GetY());
            }

        }
    }
}
