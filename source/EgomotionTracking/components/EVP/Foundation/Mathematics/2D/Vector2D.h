/*
 * Vector2D.h
 *
 *  Created on: 22.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"
#include "../../DataTypes/PixelLocation.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _2D
        {
            class CVector2D
            {
            public:

                static const CVector2D s_Point_At_Origin_2D;
                static const CVector2D s_Point_At_Minus_Infinity_2D;
                static const CVector2D s_Point_At_Plus_Infinity_2D;
                static const CVector2D s_Direction_Axis_X_2D;
                static const CVector2D s_Direction_Axis_Y_2D;

                static  CVector2D CreateLinealCombination(const CVector2D& A, const CVector2D& B, const real SA, const real SB);
                static  CVector2D CreateComplementaryLinealCombination(const CVector2D& A, const CVector2D& B, const real SA);
                static  CVector2D CreateMidPoint(const CVector2D& A, const CVector2D& B);
                static  CVector2D CreateCannonicalForm(const real X, const real Y);

                CVector2D();
                CVector2D(const real X, const real Y);
                CVector2D(const CVector2D& Vector);
                CVector2D(const CPixelLocation& Location);

                bool SetCannonicalForm();

                void SetX(const real X);
                void SetY(const real Y);
                void SetValue(const real X, const real Y);
                void SetMaximalComponentValue(const real X, const real Y);
                void SetMinimalComponentValue(const real X, const real Y);
                void SetMaximalComponentValue(const CVector2D& Vector);
                void SetMinimalComponentValue(const CVector2D& Vector);
                void SetMidPoint(const CVector2D& A, const CVector2D& B);
                void SetWeighted(const CVector2D& Vector, const real Weight);
                void SetWeightedInverse(const CVector2D& Vector, const real Weight);
                void SetLinealCombination(const CVector2D& A, const CVector2D& B, const real SA, const real SB);

                void SetZero();
                void Negate();
                void Rotate(const real Alpha);
                real Normalize();
                void ScaleAnisotropic(const real Sx, const real Sy);

                void AddWeightedOffset(const CVector2D& Vector, const real Weight);
                void AddOffset(const real Dx, const real Dy);
                void AddOffsetX(const real Dx);
                void AddOffsetY(const real Dy);

                void AddPartialCovariance(const CVector2D& Vector, real& Axx, real& Ayy) const;
                void AddCovariance(const CVector2D& Vector, real& Axx, real& Axy, real& Ayy) const;
                void AddWeightedCovariance(const CVector2D& Vector, const real Weight, real& Axx, real& Axy, real& Ayy, real& Aw) const;

                bool IsNull() const;
                bool IsNonNull() const;
                bool IsAtInfinity() const;
                bool IsNotAtInfinity() const;
                bool IsUnitary() const;
                bool IsNonUnitary() const;
                bool IsZeroLength() const;
                bool IsNonZeroLength() const;

                real GetX() const;
                real GetY() const;
                coordinate GetTruncatedDiscreteX() const;
                coordinate GetTruncatedDiscreteY() const;
                CPixelLocation GetRoundedLocation() const;
                CPixelLocation GetQuantizedOrientation() const;
                CPixelLocation GetForwardsDiscreteLocation(const CPixelLocation& CurrentLocation) const;
                CPixelLocation GetBackwardsDiscreteLocation(const CPixelLocation& CurrentLocation) const;

                real VectorProduct(const CVector2D& Vector) const;
                real ScalarProduct(const CVector2D& Vector) const;
                real ScalarProduct(const CPixelLocation& Location) const;
                real ScalarProduct(const real X, const real Y) const;
                real ScalarProductSquare(const real X, const real Y) const;

                real GetAngle() const;
                real GetHalfResolutionAngle() const;
                real GetNormalizedHalfResolutionAngle() const;
                real GetAbsoluteAngle() const;
                real GetAbsoluteRotation() const;
                real GetAngle(const CVector2D& Vector) const;
                real GetAperture(const CVector2D& Vector) const;
                real GetAbsoluteAperture(const CVector2D& Vector) const;
                real GetDistance(const CVector2D& Vector) const;
                real GetDistance(const CPixelLocation& Location) const;
                real GetSquareDistance(const CVector2D& Vector) const;
                real GetSquareDistance(const CPixelLocation& Location) const;
                real GetLength() const;
                real GetSquareLength() const;
                CVector2D GetRotated(const real Alpha) const;
                CVector2D GetPerpendicular() const;
                void GetCovariance(const CVector2D& Vector, real& Cxx, real& Cxy, real& Cyy) const;

                void operator=(const CVector2D& Vector);
                void operator=(const CPixelLocation& Location);
                void operator+=(const CVector2D& Vector);
                void operator-=(const CVector2D& Vector);
                void operator*=(const real Scalar);
                void operator/=(const real Scalar);
                std::string ToString(const uint Precision) const;

                const real* GetElements() const;

            protected:

                real m_Elements[2];
            };

            bool operator==(const CVector2D& A, const CVector2D& B);
            bool operator!=(const CVector2D& A, const CVector2D& B);
            const CVector2D operator/(const CVector2D& Vector, const real Scalar);
            const CVector2D operator*(const CVector2D& Vector, const real Scalar);
            const CVector2D operator*(const real Scalar, const CVector2D& Vector);
            const CVector2D operator+(const CVector2D& A, const CVector2D& B);
            const CVector2D operator-(const CVector2D& A, const CVector2D& B);
        }
    }
}

