/*
 * LinearAlgebra.h
 *
 *  Created on: 21.09.2011
 *      Author: gonzalez
 */

#include "LinearAlgebra.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace ND
        {
            namespace LinearAlgebra
            {
                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
                CMatrixND CreateZeroMatrix(const int Rows, const int Cols)
                {
                    CMatrixND ZeroMatrix(Rows, Cols);
                    ZeroMatrix.setZero();
                    return ZeroMatrix;
                }

                /*
                 CDiagonalMatrixND S;
                 CMatrixND U , V;
                 SVD(A,S,U,V);
                 const int TotalRows = S.Nrows();
                 CMatrixND DPI = CreateZeroMatrix(TotalRows,TotalRows);
                 for(int i = 1 ; i <= TotalRows ; ++i)
                 if (std::abs(S(i,i)) > std::numeric_limits<MatrixReal>::epsilon())
                 DPI(i,i) = MatrixReal(1) / S(i,i);
                 return V * DPI * U.t();
                */

                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
                const CMatrixND PseudoInverseSVD(const CMatrixND& A)
                {
                    const int TotalRows = A.rows();

                    Eigen::JacobiSVD<Eigen::Matrix<MatrixReal, Eigen::Dynamic, Eigen::Dynamic> > SVD(A, Eigen::ComputeFullU | Eigen::ComputeFullV);

                    Eigen::Matrix<MatrixReal, Eigen::Dynamic, Eigen::Dynamic> S = SVD.singularValues();

                    CMatrixND DPI(TotalRows, TotalRows);

                    DPI.setZero();

                    for (int i = 0 ; i < TotalRows ; ++i)
                    {
                        const double Value = (double) std::abs((long double)S(i, i));
                        if (Value > std::numeric_limits<MatrixReal>::epsilon())
                        {
                            DPI(i, i) = MatrixReal(1.0) / Value;
                        }
                    }

                    return SVD.matrixV() * DPI * SVD.matrixU().transpose();
                }

                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
                std::string DisplayMatrix(const std::string& Name, const CMatrixND& A)
                {
                    const int Rows = A.rows();
                    const int Cols = A.cols();
                    std::ostringstream OutputText;
                    OutputText.precision(std::numeric_limits<MatrixReal>::digits10);
                    OutputText << "\t\n" << Name << "\n";
                    for (int r = 0 ; r < Rows ; ++r)
                    {
                        OutputText << "|\t";
                        for (int c = 0 ; c < Cols ; ++c)
                        {
                            OutputText << A(r, c) << "\t";
                        }
                        OutputText << "|\n";
                    }
                    OutputText << "\n";
                    return OutputText.str();
                }

                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
                bool SVD(CMatrixND& Covariance, CMatrixND& EigenValues, CMatrixND& EigenVectors)
                {
                    const int Rows = Covariance.rows();

                    const int Cols = Covariance.cols();

                    if (Rows <= 1)
                    {
                        return false;
                    }

                    if (Rows != Cols)
                    {
                        return false;
                    }

                    if (EigenValues.rows() != Rows)
                    {
                        return false;
                    }

                    if (EigenValues.cols() != Cols)
                    {
                        return false;
                    }

                    if (EigenVectors.rows() != Rows)
                    {
                        return false;
                    }

                    if (EigenVectors.cols() != Cols)
                    {
                        return false;
                    }

                    Eigen::JacobiSVD<Eigen::Matrix<MatrixReal, Eigen::Dynamic, Eigen::Dynamic> > SVD(Covariance, Eigen::ComputeFullU | Eigen::ComputeFullV);

                    EigenValues = SVD.singularValues();

                    EigenVectors = SVD.matrixU();

                    return true;
                }

                /*
                 //---------------------------------------------------------------
                 CMatrixND CreateZeroMatrix(const int Rows, const int Cols)
                 {
                 CMatrixND ZeroMatrix(Rows,Cols);
                 for(int r = 1 ; r <= Rows ; ++r)
                 for(int c = 1 ; c <= Cols ; ++c)
                 ZeroMatrix(r,c) = MatrixReal(0);
                 return ZeroMatrix;
                 }

                 const CMatrixND PseudoInverseSVD(const CMatrixND& A)
                 {
                 CDiagonalMatrixND S;
                 CMatrixND U , V;
                 SVD(A,S,U,V);
                 const int TotalRows = S.Nrows();
                 CMatrixND DPI = CreateZeroMatrix(TotalRows,TotalRows);
                 for(int i = 1 ; i <= TotalRows ; ++i)
                 if (std::abs(S(i,i)) > std::numeric_limits<MatrixReal>::epsilon())
                 DPI(i,i) = MatrixReal(1) / S(i,i);
                 return V * DPI * U.t();
                 }

                 std::string DisplayMatrix(const std::string& Name, const CMatrixND& A)
                 {
                 const int Rows = A.Nrows();
                 const int Cols = A.Ncols();
                 std::ostringstream OutputText;
                 OutputText.precision(std::numeric_limits<MatrixReal>::digits10);
                 OutputText << "\t\n" << Name << "\n";
                 for(int r = 1 ; r <= Rows ; ++r)
                 {
                 OutputText << "|\t";
                 for(int c = 1 ; c <= Cols ; ++c)
                 OutputText << A(r,c) << "\t";
                 OutputText << "|\n";
                 }
                 OutputText << "\n";
                 return OutputText.str();
                 }
                 //---------------------------------------------------------------
                 */

                //              CMatrixND CreateZeroMatrix(const uint Rows, const uint Cols)
                //              {
                //                  CMatrixND ZeroMatrix(Rows, Cols);
                //                  for (uint r = 1; r <= Rows; ++r)
                //                      for (uint c = 1; c <= Cols; ++c)
                //                          ZeroMatrix(r, c) = MatrixReal(_REAL_ZERO_);
                //                  return ZeroMatrix;
                //              }

                //                 CMatrixND PseudoInverseSVD(const CMatrixND& A)
                //              {
                //                  CDiagonalMatrixND S;
                //                  CMatrixND U, V;
                //                  SVD(A, S, U, V);
                //                  const uint TotalRows = S.Nrows();
                //                  CMatrixND DPI = CreateZeroMatrix(TotalRows, TotalRows);
                //                  for (uint i = 1; i <= TotalRows; ++i)
                //                      if (fabs(S(i, i)) > DBL_EPSILON)
                //                          DPI(i, i) = MatrixReal(1.0) / S(i, i);
                //                  return V * DPI * U.t();
                //              }

                //              void DisplayMatrix(const_string pName, const CMatrixND& A)
                //              {
                //                  const uint Rows = A.Nrows();
                //                  const uint Cols = A.Ncols();
                //                  g_ConsoleStringOutput << g_EndLine << g_Tabulator << pName << g_EndLine;
                //                  for (uint r = 1; r <= Rows; ++r)
                //                  {
                //                      g_ConsoleStringOutput << "|" << g_Tabulator;
                //                      for (uint c = 1; c <= Cols; ++c)
                //                          g_ConsoleStringOutput << A(r, c) << g_Tabulator;
                //                      g_ConsoleStringOutput << "|" << g_EndLine;
                //                  }
                //                  g_ConsoleStringOutput << g_EndLine;
                //              }
            }

        }
    }
}
