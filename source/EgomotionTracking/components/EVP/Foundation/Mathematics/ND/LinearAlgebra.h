/*
 * LinearAlgebra.h
 *
 *  Created on: 21.09.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"

// Eigen
#include <eigen3/Eigen/Eigen>

//#include <newmat/newmatap.h>

namespace EVP
{
    namespace Mathematics
    {
        namespace ND
        {

            namespace LinearAlgebra
            {

                typedef double MatrixReal;
                typedef Eigen::Matrix<MatrixReal, Eigen::Dynamic, Eigen::Dynamic> CMatrixND;
                //typedef NEWMAT::DiagonalMatrix CDiagonalMatrixND;
                //typedef NEWMAT::BaseException CMatrixBaseException;

                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
                CMatrixND CreateZeroMatrix(const int Rows, const int Cols);
                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
                const CMatrixND PseudoInverseSVD(const CMatrixND& A);
                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
                std::string DisplayMatrix(const std::string& Name, const CMatrixND& A);
                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN
                bool SVD(CMatrixND& Covariance, CMatrixND& EigenValues, CMatrixND& EigenVectors);

                //#ifdef use_namespace

                //              typedef NEWMAT::Real MatrixReal;
                //              typedef NEWMAT::Matrix CMatrixND;
                //              typedef NEWMAT::DiagonalMatrix CDiagonalMatrixND;
                //              typedef NEWMAT::BaseException CMatrixBaseException;

                //#else

                //              typedef DoubleReal MatrixReal;
                //              typedef Matrix CMatrixND;
                //              typedef DiagonalMatrix CDiagonalMatrixND;
                //              typedef BaseException CMatrixBaseException;

                //#endif

                //              CMatrixND CreateZeroMatrix(const uint Rows, const uint Cols);
                //                CMatrixND PseudoInverseSVD(const CMatrixND& A);
                //              void DisplayMatrix(const_string pName, const CMatrixND& A);

            }
        }
    }
}

