/*
 * BicubicLookUpTable.h
 *
 *  Created on: 01.09.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"

//PROPEERTY MACROS
#ifdef _USE_LOOKUP_TABLES_

#define DECLARE_BICUBIC_LOOKUP_TABLE static Mathematics::_1D::CBicubicLookUpTable::BicubicLookUpTable s_BLT;
#define INITIALIZE_BICUBIC_LOOKUP_TABLE(CLASS) Mathematics::_1D::CBicubicLookUpTable::BicubicLookUpTable CLASS::s_BLT = { _REAL_ZERO_, _REAL_ZERO_, _REAL_ZERO_, NULL };
#define LOAD_BICUBIC_LOOKUP_TABLE(CLASS) if (!CLASS::s_BLT.m_pBicubic)Mathematics::_1D::CBicubicLookUpTable::GetLookupTable(&CLASS::s_BLT);

#else

#define DECLARE_BICUBIC_LOOKUP_TABLE
#define INITIALIZE_BICUBIC_LOOKUP_TABLE(CLASS)
#define LOAD_BICUBIC_LOOKUP_TABLE(CLASS)

#endif

namespace EVP
{
    namespace Mathematics
    {
        namespace _1D
        {
            class CBicubicLookUpTable
            {
            public:

                static real BicubicKernel(const real X);

#ifdef _USE_LOOKUP_TABLES_

                struct BicubicLookUpTable
                {
                    real m_MaximalArgumentValue;
                    real m_MinimalArgumentValue;
                    real m_SamplesPerUnit;
                    real* m_pBicubic;
                };

                static bool Initialize(const real SamplesPerUnit = real(8192.0), const real MaximalArgumentValue = real(2.0), const real MinimalArgumentValue = real(-2.0));
                static bool GetLookupTable(BicubicLookUpTable* pBLT);
                static bool Finalize();

            protected:

                static BicubicLookUpTable s_LookupTable;
                static real* s_BicubicBuffer;
#endif
            };
        }
    }
}

