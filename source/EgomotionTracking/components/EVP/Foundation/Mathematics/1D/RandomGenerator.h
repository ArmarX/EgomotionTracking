/*
 * RandomGenerator.h
 *
 *  Created on: 10.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _1D
        {
            class CRandomGenerator
            {
            public:

                static void InitializeRandomSeed(const uint RandomSeed);
                static  byte GenerateRandombyteValue(const byte UpperLimit = UCHAR_MAX, const byte LowerLimit = 0, const uint RandomSeed = 0);
                static  int GenerateRandomSignedIntegerValue(const int UpperLimit = INT_MAX, const int LowerLimit = INT_MIN, const uint RandomSeed = 0);
                static  uint GenerateRandomUnsignedIntegerValue(const uint UpperLimit = UINT_MAX, const uint LowerLimit = 0, const uint RandomSeed = 0);
                static  real GenerateRandomrealValue(const real UpperLimit = _REAL_MAX_, const real LowerLimit = _REAL_MIN_, const uint RandomSeed = 0);

            protected:

                static  real s_Normalization;
            };
        }
    }
}

