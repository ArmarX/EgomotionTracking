/*
 * ExponentialLookUpTable.cpp
 *
 *  Created on: 01.09.2011
 *      Author: gonzalez
 */

#include "ExponentialLookUpTable.h"

#ifdef _USE_LOOKUP_TABLES_

namespace EVP
{
    namespace Mathematics
    {
        namespace _1D
        {
            CExponentialLookupTable::ExponentialLookupTable CExponentialLookupTable::s_LookupTable = {  _REAL_ZERO_, _REAL_ZERO_, 0, 0, 0, nullptr  };
            real* CExponentialLookupTable::s_pExpBuffer = nullptr;

            bool CExponentialLookupTable::Initialize(const uint SamplesPerUnit, const real MaximalArgumentValue)
            {
                if (s_pExpBuffer)
                {
                    return false;
                }
                s_LookupTable.m_SamplesPerUnit = TMin(TMax(SamplesPerUnit, 128u), 131072u);
                s_LookupTable.m_MaximalArgumentValue = TMax(TMin(MaximalArgumentValue, RealLog(_REAL_MAX_)), -RealLog(_REAL_EPSILON_));
                s_LookupTable.m_MinimalArgumentValue = -s_LookupTable.m_MaximalArgumentValue;
                const uint SubTotalElements = UpperToInteger(s_LookupTable.m_MaximalArgumentValue * real(s_LookupTable.m_SamplesPerUnit));
                s_LookupTable.m_MaximalIndex = SubTotalElements;
                s_LookupTable.m_MinimalIndex = -int(SubTotalElements);
                s_pExpBuffer = new real[SubTotalElements * 2 + 1];
                s_LookupTable.m_pExp = s_pExpBuffer + SubTotalElements;
                const LongDoubleReal Delta = LongDoubleReal(1.0) / LongDoubleReal(s_LookupTable.m_SamplesPerUnit);
                LongDoubleReal X = LongDoubleReal(0.0);
                real* pLUTForwards = s_LookupTable.m_pExp;
                real* pLUTBackWards = s_LookupTable.m_pExp;
                for (uint i = 0; i < SubTotalElements; ++i, X += Delta, ++pLUTForwards, --pLUTBackWards)
                {
                    *pLUTForwards = real(expl(X));
                    *pLUTBackWards = real(expl(-X));
                }
                return true;
            }

            bool CExponentialLookupTable::GetLookupTable(ExponentialLookupTable* pELT)
            {
                if (!s_pExpBuffer)
                {
                    memset(pELT, 0, sizeof(ExponentialLookupTable));
                    g_ErrorStringOutput << "FATAL ERROR EVP::ExponentialLookupTable[ Trying lookup table usage without initialization ][" << __FILE__ << ":" << __LINE__ << "]" << g_EndLine;
                    exit(-1);
                    return false;
                }
                memcpy(pELT, &s_LookupTable, sizeof(ExponentialLookupTable));
                return true;
            }

            bool CExponentialLookupTable::Finalize()
            {
                if (!s_pExpBuffer)
                {
                    return false;
                }
                RELEASE_ARRAY(s_pExpBuffer)
                memset(&s_LookupTable, 0, sizeof(ExponentialLookupTable));
                return true;
            }
        }
    }
}
#endif

