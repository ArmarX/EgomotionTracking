/*
 * PowerLookUpTable.h
 *
 *  Created on: 26.09.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"

#ifdef _USE_LOOKUP_TABLES_

namespace EVP
{
    namespace Mathematics
    {
        namespace _1D
        {
            class CPowerLookUpTable
            {
            public:

                struct PowerLookupTable
                {
                    real m_MaximalArgumentValue;
                    real m_MinimalArgumentValue;
                    real m_SamplesPerUnit;
                    real m_Power;
                    real* m_pPowBase;
                    real* m_pPow;
                };

                static void GetLookupTable(PowerLookupTable* pPLT, const real Power, const real SamplesPerUnit = real(8192.0), const real MaximalArgumentValue = real(1.0), const real MinimalArgumentValue = real(-1.0));
                static  bool Finalize();

            protected:

                static list<PowerLookupTable> m_sTables;
            };
        }
    }
}

#endif /* _USE_LOOKUP_TABLES_ */
