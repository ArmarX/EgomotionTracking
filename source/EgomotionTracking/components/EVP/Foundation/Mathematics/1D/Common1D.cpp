/*
 * Common1D.cpp
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "Common1D.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _1D
        {
            bool IsAtInfinity(const real a)
            {
                return (RealAbs(a) == _REAL_MAX_);
            }

            bool IsNotAtInfinity(const real a)
            {
                return (RealAbs(a) != _REAL_MAX_);
            }

            bool IsZero(const real a)
            {
                return (RealAbs(a) <= _REAL_EPSILON_);
            }

            bool IsNonZero(const real a)
            {
                return (RealAbs(a) > _REAL_EPSILON_);
            }

            bool IsPositive(const real a)
            {
                return (a > _REAL_EPSILON_);
            }

            bool IsNonPositive(const real a)
            {
                return (a <= _REAL_EPSILON_);
            }

            bool IsNegative(const real a)
            {
                return (a < -_REAL_EPSILON_);
            }

            bool IsNonNegative(const real a)
            {
                return (a >= -_REAL_EPSILON_);
            }

            bool Equals(const real a, const real b)
            {
                return (RealAbs(a - b) < _REAL_EPSILON_);
            }

            namespace NormalDistribution
            {
                real DetermineNonNormalizedStandardDeviation(const real fx, const real x)
                {
                    return RealSqrt(-(x * x) / (_REAL_TWO_ * RealLog(fx)));
                }

                real DetermineDeviationAtDensity(const real Density, const real StandardDeviation)
                {
                    return RealSqrt(RealLog(Density) * (-_REAL_TWO_) * StandardDeviation * StandardDeviation);
                }

                real DetermineBandWidthByVariance(const real Variance, const uint TotalSamples)
                {
                    return RealPow(real(4.0 / 3.0f) * RealPow(Variance, real(2.5)) / real(TotalSamples), real(1.0 / 5.0));
                }

                real DetermineBandWidthByStandardDeviation(const real StandardDeviation, const uint TotalSamples)
                {
                    return RealPow(real(4.0 / 3.0f) * RealPow(StandardDeviation, real(5.0)) / real(TotalSamples), real(1.0 / 5.0));
                }

                real DetermineExponentFactor(const real StandardDeviation)
                {
                    return -_REAL_ONE_ / (_REAL_TWO_ * StandardDeviation * StandardDeviation);
                }

                real DetermineCoefficient(const real StandardDeviation)
                {
                    return _REAL_ONE_ / (StandardDeviation * RealSqrt(_REAL_2PI_));
                }

                real DetermineNormalizedDensity(const real x, const real Mean, const real StandardDeviation)
                {
                    const real Deviation = x - Mean;
                    const real Variance = StandardDeviation * StandardDeviation;
                    return RealExp((Deviation * Deviation) / (-_REAL_TWO_ * Variance)) / RealSqrt(_REAL_2PI_ * Variance);
                }

                real DetermineNonNormalizedDensity(const real x, const real Mean, const real StandardDeviation)
                {
                    const real Deviation = x - Mean;
                    return RealExp((Deviation * Deviation) / (-_REAL_TWO_ * StandardDeviation * StandardDeviation));
                }
            }
        }
    }
}
