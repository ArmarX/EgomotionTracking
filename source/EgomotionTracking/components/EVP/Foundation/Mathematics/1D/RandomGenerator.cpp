/*
 * RandomGenerator.cpp
 *
 *  Created on: 10.10.2011
 *      Author: gonzalez
 */

#include "RandomGenerator.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _1D
        {
            real CRandomGenerator::s_Normalization = _REAL_ONE_ / real(RAND_MAX);

            void CRandomGenerator::InitializeRandomSeed(const uint RandomSeed)
            {
                srand(RandomSeed);
            }

            byte CRandomGenerator::GenerateRandombyteValue(const byte UpperLimit, const byte LowerLimit, const uint RandomSeed)
            {
                if (RandomSeed)
                {
                    srand(RandomSeed);
                }
                return SafeRoundToByte(real(abs(UpperLimit - LowerLimit)) * real(rand()) * s_Normalization) + TMin(UpperLimit, LowerLimit);
            }

            int CRandomGenerator::GenerateRandomSignedIntegerValue(const int UpperLimit, const int LowerLimit, const uint RandomSeed)
            {
                if (RandomSeed)
                {
                    srand(RandomSeed);
                }
                return RoundToInteger(real(abs(UpperLimit - LowerLimit)) * real(rand()) * s_Normalization) + TMin(UpperLimit, LowerLimit);
            }

            uint CRandomGenerator::GenerateRandomUnsignedIntegerValue(const uint UpperLimit, const uint LowerLimit, const uint RandomSeed)
            {
                if (RandomSeed)
                {
                    srand(RandomSeed);
                }
                return RoundToInteger(real(TMax(UpperLimit, LowerLimit) - TMin(UpperLimit, LowerLimit)) * real(rand()) * s_Normalization) + TMin(UpperLimit, LowerLimit);
            }

            real CRandomGenerator::GenerateRandomrealValue(const real UpperLimit, const real LowerLimit, const uint RandomSeed)
            {
                if (RandomSeed)
                {
                    srand(RandomSeed);
                }
                return (RealAbs(TMax(UpperLimit, LowerLimit) - TMin(UpperLimit, LowerLimit)) * real(rand()) * s_Normalization) + TMin(UpperLimit, LowerLimit);
            }
        }
    }
}
