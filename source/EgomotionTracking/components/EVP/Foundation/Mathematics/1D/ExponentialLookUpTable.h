/*
 * ExponentialLookUpTable.h
 *
 *  Created on: 01.09.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"

//PROPEERTY MACROS
#ifdef _USE_LOOKUP_TABLES_

#define DECLARE_EXPONENTIAL_LOOKUP_TABLE static Mathematics::_1D::CExponentialLookupTable::ExponentialLookupTable s_ELT;
#define INITIALIZE_EXPONENTIAL_LOOKUP_TABLE(CLASS) Mathematics::_1D::CExponentialLookupTable::ExponentialLookupTable CLASS::s_ELT = { _REAL_ZERO_, _REAL_ZERO_, 0, 0, 0, NULL };
#define LOAD_EXPONENTIAL_LOOKUP_TABLE(CLASS) if(!CLASS::s_ELT.m_pExp)Mathematics::_1D::CExponentialLookupTable::GetLookupTable(&CLASS::s_ELT);

#else

#define DECLARE_EXPONENTIAL_LOOKUP_TABLE
#define INITIALIZE_EXPONENTIAL_LOOKUP_TABLE(CLASS)
#define LOAD_EXPONENTIAL_LOOKUP_TABLE(CLASS)

#endif

#ifdef _USE_LOOKUP_TABLES_

namespace EVP
{
    namespace Mathematics
    {
        namespace _1D
        {
            class CExponentialLookupTable
            {
            public:

                struct ExponentialLookupTable
                {
                    real m_MaximalArgumentValue;
                    real m_MinimalArgumentValue;
                    uint m_SamplesPerUnit;
                    int m_MaximalIndex;
                    int m_MinimalIndex;
                    real* m_pExp;
                };

                static  bool Initialize(const uint SamplesPerUnit = 32768, const real MaximalArgumentValue = real(24.0));
                static  bool GetLookupTable(ExponentialLookupTable* pELT);
                static  bool Finalize();

            protected:

                static ExponentialLookupTable s_LookupTable;
                static real* s_pExpBuffer;
            };
        }
    }
}

#endif
