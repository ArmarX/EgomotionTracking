/*
 * Common1D.h
 *
 *  Created on: Feb 3, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

#include "../../../GlobalSettings.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _1D
        {
            bool IsAtInfinity(const real a);

            bool IsNotAtInfinity(const real a);

            bool IsZero(const real a);

            bool IsNonZero(const real a);

            bool IsPositive(const real a);

            bool IsNonPositive(const real a);

            bool IsNegative(const real a);

            bool IsNonNegative(const real a);

            bool Equals(const real a, const real b);

            namespace NormalDistribution
            {
                real DetermineNonNormalizedStandardDeviation(const real fx, const real x);

                real DetermineDeviationAtDensity(const real Density, const real StandardDeviation);

                real DetermineBandWidthByVariance(const real Variance, const uint TotalSamples);

                real DetermineBandWidthByStandardDeviation(const real StandardDeviation, const uint TotalSamples);

                real DetermineExponentFactor(const real StandardDeviation);

                real DetermineCoefficient(const real StandardDeviation);

                real DetermineNormalizedDensity(const real x, const real Mean, const real StandardDeviation);

                real DetermineNonNormalizedDensity(const real x, const real Mean, const real StandardDeviation);
            }
        }
    }
}
