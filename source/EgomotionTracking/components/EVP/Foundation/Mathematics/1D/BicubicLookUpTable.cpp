/*
 * BicubicLookUpTable.cpp
 *
 *  Created on: 01.09.2011
 *      Author: gonzalez
 */

#include "BicubicLookUpTable.h"

namespace EVP
{
    namespace Mathematics
    {
        namespace _1D
        {

#ifdef _USE_LOOKUP_TABLES_

            CBicubicLookUpTable::BicubicLookUpTable CBicubicLookUpTable::s_LookupTable = { _REAL_ZERO_, _REAL_ZERO_, _REAL_ZERO_, nullptr };
            real* CBicubicLookUpTable::s_BicubicBuffer = nullptr;

            bool CBicubicLookUpTable::Initialize(const real SamplesPerUnit, const real MaximalArgumentValue, const real MinimalArgumentValue)
            {
                if (s_BicubicBuffer)
                {
                    return false;
                }
                s_LookupTable.m_SamplesPerUnit = TMin(TMax(SamplesPerUnit, real(128.0)), real(131072.0));
                s_LookupTable.m_MaximalArgumentValue = TMin(MaximalArgumentValue, real(3.0));
                s_LookupTable.m_MinimalArgumentValue = TMax(MinimalArgumentValue, real(-3.0));
                const uint TotalPositiveElements = UpperToInteger(s_LookupTable.m_MaximalArgumentValue * s_LookupTable.m_SamplesPerUnit);
                const uint TotalNegativeElements = UpperToInteger(RealAbs(s_LookupTable.m_MinimalArgumentValue * s_LookupTable.m_SamplesPerUnit));
                s_BicubicBuffer = new real[TotalPositiveElements + TotalNegativeElements + 1];
                s_LookupTable.m_pBicubic = s_BicubicBuffer + TotalNegativeElements;
                const real Delta = _REAL_ONE_ / s_LookupTable.m_SamplesPerUnit;
                real X = _REAL_ZERO_;
                real* pLUT = s_LookupTable.m_pBicubic;
                for (uint i = 0; i < TotalPositiveElements; ++i, X += Delta, ++pLUT)
                {
                    *pLUT = BicubicKernel(X);
                }
                X = -Delta;
                pLUT = s_LookupTable.m_pBicubic - 1;
                for (uint i = 1; i < TotalPositiveElements; ++i, X -= Delta, --pLUT)
                {
                    *pLUT = BicubicKernel(X);
                }
                return true;
            }

            bool CBicubicLookUpTable::GetLookupTable(BicubicLookUpTable* pBLT)
            {
                if (!s_BicubicBuffer)
                {
                    memset(pBLT, 0, sizeof(BicubicLookUpTable));
                    g_ErrorStringOutput << "FATAL ERROR EVP::BicubicLookupTable[ Trying lookup table usage without initialization ][" << __FILE__ << ":" << __LINE__ << "]" << g_EndLine;
                    exit(-1);
                    return false;
                }
                memcpy(pBLT, &s_LookupTable, sizeof(BicubicLookUpTable));
                return true;
            }

            bool CBicubicLookUpTable::Finalize()
            {
                if (!s_BicubicBuffer)
                {
                    return false;
                }
                RELEASE_ARRAY(s_BicubicBuffer)
                memset(&s_LookupTable, 0, sizeof(BicubicLookUpTable));
                return true;
            }

#endif
            real CBicubicLookUpTable::BicubicKernel(const real X)
            {
                real Result = (X < _REAL_ZERO_) ? _REAL_ZERO_ : real(6.0) * X * X * X;
                real Xmm = X + _REAL_TWO_;
                Result += (Xmm < _REAL_ZERO_) ? _REAL_ZERO_ : Xmm * Xmm * Xmm;
                Xmm = X + _REAL_ONE_;
                Result += (Xmm < _REAL_ZERO_) ? _REAL_ZERO_ : real(-4.0) * Xmm * Xmm * Xmm;
                Xmm = X - _REAL_ONE_;
                Result += (Xmm < _REAL_ZERO_) ? _REAL_ZERO_ : real(-4.0) * Xmm * Xmm * Xmm;
                return Result * real(0.1666666666666666666);
            }
        }
    }
}
