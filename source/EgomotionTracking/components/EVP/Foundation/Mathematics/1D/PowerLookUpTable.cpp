/*
 * PowerLookUpTable.cpp
 *
 *  Created on: 26.09.2011
 *      Author: gonzalez
 */

#include "PowerLookUpTable.h"

#ifdef _USE_LOOKUP_TABLES_

namespace EVP
{
    namespace Mathematics
    {
        namespace _1D
        {
            list<CPowerLookUpTable::PowerLookupTable> CPowerLookUpTable::m_sTables;

            void CPowerLookUpTable::GetLookupTable(PowerLookupTable* pPLT, const real Power, const real SamplesPerUnit, const real MaximalArgumentValue, const real MinimalArgumentValue)
            {
                memset(pPLT, 0, sizeof(PowerLookupTable));
                if (m_sTables.size())
                    for (list<PowerLookupTable>::iterator pPowerLookupTable = m_sTables.begin(); pPowerLookupTable != m_sTables.end(); ++pPowerLookupTable)
                        if ((pPowerLookupTable->m_Power >= Power) && (pPowerLookupTable->m_SamplesPerUnit >= SamplesPerUnit) && (pPowerLookupTable->m_MaximalArgumentValue >= MaximalArgumentValue) && (pPowerLookupTable->m_MinimalArgumentValue <= MinimalArgumentValue))
                        {
                            memcpy(pPLT, &(*pPowerLookupTable), sizeof(PowerLookupTable));
                            return;
                        }
                PowerLookupTable PLT =
                { MaximalArgumentValue, MinimalArgumentValue, SamplesPerUnit, Power, nullptr, nullptr };
                const uint TotalNegative = uint(RealAbs(MinimalArgumentValue) * SamplesPerUnit);
                const uint TotalPositive = uint(MaximalArgumentValue * SamplesPerUnit);
                const uint TotalSamples = TotalNegative + 1 + TotalPositive;
                PLT.m_pPowBase = new real[TotalSamples];
                PLT.m_pPow = PLT.m_pPowBase + TotalNegative;
                PLT.m_pPow[0] = _REAL_ZERO_;
                const real Delta = _REAL_ONE_ / SamplesPerUnit;
                real X = Delta;
                for (uint i = 1; i < TotalPositive; ++i, X += Delta)
                {
                    PLT.m_pPow[i] = RealPow(X, Power);
                }
                X = -Delta;
                for (uint i = 1; i < TotalNegative; ++i, X -= Delta)
                {
                    PLT.m_pPow[-i] = RealPow(X, Power);
                }
                memcpy(pPLT, &PLT, sizeof(PowerLookupTable));
                m_sTables.push_back(PLT);
            }

            bool CPowerLookUpTable::Finalize()
            {
                if (!m_sTables.size())
                {
                    return false;
                }
                for (list<PowerLookupTable>::iterator pPowerLookupTable = m_sTables.begin(); pPowerLookupTable != m_sTables.end(); ++pPowerLookupTable)
                    RELEASE_ARRAY(pPowerLookupTable->m_pPowBase)
                    m_sTables.clear();
                return true;
            }
        }
    }
}
#endif

