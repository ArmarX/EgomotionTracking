/*
 * DependencyManager.cpp
 *
 *  Created on: Aug 26, 2011
 *      Author: david
 */

#include "DependencyManager.h"

namespace EVP
{
    IDependencyManager::IDependencyManager()
    {
        m_DispatchDestructorEvent = true;
        m_DispatchChangeEvent = true;
    }

    IDependencyManager::~IDependencyManager()
    {
        if (m_DispatchDestructorEvent && m_Dependencies.size())
            for (list<IDependency*>::iterator ppDependency = m_Dependencies.begin(); ppDependency != m_Dependencies.end(); ++ppDependency)
            {
                (*ppDependency)->OnDestructor(this);
            }
    }

    void IDependencyManager::SetDestructorEventEnabled(const bool Enabled)
    {
        m_DispatchDestructorEvent = Enabled;
    }

    bool IDependencyManager::IsDestructorEventEnabled()
    {
        return m_DispatchDestructorEvent;
    }

    void IDependencyManager::SetChangeEventEnabled(const bool Enabled)
    {
        m_DispatchChangeEvent = Enabled;
    }

    bool IDependencyManager::IsChangeEventEnabled()
    {
        return m_DispatchChangeEvent;
    }

    bool IDependencyManager::AddDependency(IDependency* pDependency)
    {
        if (pDependency)
        {
            for (list<IDependency*>::iterator ppDependency = m_Dependencies.begin(); ppDependency != m_Dependencies.end(); ++ppDependency)
                if ((*ppDependency) == pDependency)
                {
                    return false;
                }
            m_Dependencies.push_back(pDependency);
            return true;
        }
        return false;
    }

    bool IDependencyManager::RemoveDependency(const IDependency* pDependency)
    {
        if (pDependency)
            for (list<IDependency*>::iterator ppDependency = m_Dependencies.begin(); ppDependency != m_Dependencies.end(); ++ppDependency)
                if ((*ppDependency) == pDependency)
                {
                    m_Dependencies.remove(*ppDependency);
                    return true;
                }
        return false;
    }

    void IDependencyManager::OnChange()
    {
        if (m_DispatchChangeEvent && m_Dependencies.size())
            for (list<IDependency*>::iterator ppDependency = m_Dependencies.begin(); ppDependency != m_Dependencies.end(); ++ppDependency)
            {
                (*ppDependency)->OnChange(this);
            }
    }
}
