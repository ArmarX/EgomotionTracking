/*
 * Dependency.cpp
 *
 *  Created on: Mar 30, 2012
 *      Author: david
 */

#include "Dependency.h"

namespace EVP
{
    IDependency::IDependency()
        = default;

    IDependency::~IDependency()
        = default;
}
