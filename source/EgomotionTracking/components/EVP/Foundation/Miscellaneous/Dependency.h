/*
 * Dependency.h
 *
 *  Created on: Mar 30, 2012
 *      Author: david
 */

#pragma once

namespace EVP
{
    class IDependencyManager;

    class IDependency
    {
    public:

        IDependency();
        virtual ~IDependency();

        virtual void OnDestructor(IDependencyManager* pDependencyManager) = 0;
        virtual void OnChange(IDependencyManager* pDependencyManager) = 0;
    };
}

