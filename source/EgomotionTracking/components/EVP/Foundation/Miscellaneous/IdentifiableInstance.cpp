/*
 * IdentifiableInstance.cpp
 *
 *  Created on: 20.09.2011
 *      Author: gonzalez
 */

#include "IdentifiableInstance.h"

namespace EVP
{
    CIdentifiableInstance::CIdentifiableInstance(const Identifier IntanceId)
    {
        m_InstanceId = IntanceId;
    }

    CIdentifiableInstance::~CIdentifiableInstance()
        = default;

    Identifier CIdentifiableInstance::GetInstanceId() const
    {
        return m_InstanceId;
    }
}
