/*
 * DependencyManager.h
 *
 *  Created on: Aug 26, 2011
 *      Author: david
 */

#pragma once

#include "../../GlobalSettings.h"
#include "Dependency.h"

namespace EVP
{
    class IDependencyManager
    {
    public:

        IDependencyManager();
        virtual ~IDependencyManager();

        void SetDestructorEventEnabled(const bool Enabled);
        bool IsDestructorEventEnabled();

        void SetChangeEventEnabled(const bool Enabled);
        bool IsChangeEventEnabled();

        bool AddDependency(IDependency* pDependency);
        bool RemoveDependency(const IDependency* pDependency);

    protected:

        void OnChange();

        bool m_DispatchDestructorEvent;
        bool m_DispatchChangeEvent;
        list<IDependency*> m_Dependencies;
    };
}

