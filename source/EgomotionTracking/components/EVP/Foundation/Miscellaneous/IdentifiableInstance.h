/*
 * IdentifiableInstance.h
 *
 *  Created on: 20.09.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"

//PROPERTY MACROS
#define IDENTIFIABLE static Identifier s_InstanceIdCounter;
#define IDENTIFIABLE_INITIALIZATION(CLASS) Identifier CLASS::s_InstanceIdCounter = 1;
#define IDENTIFIABLE_CONTRUCTOR(CLASS) CIdentifiableInstance(CLASS::s_InstanceIdCounter++)
#define IDENTIFIABLE_DERIVED_CONTRUCTOR s_InstanceIdCounter++

namespace EVP
{
    class CIdentifiableInstance
    {
    public:

        CIdentifiableInstance(const Identifier IntanceId);
        virtual ~CIdentifiableInstance();

        Identifier GetInstanceId() const;

    protected:

        Identifier m_InstanceId;
    };
}

