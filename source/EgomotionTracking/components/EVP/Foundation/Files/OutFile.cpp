/*
 * OutFile.cpp
 *
 *  Created on: Mar 28, 2012
 *      Author: david
 */

#include "OutFile.h"

namespace EVP
{
    namespace Files
    {
        bool COutFile::WriteBufferToFile(const_string pPathFileName, const void* pBuffer, const uint BufferSize, const CFile::FileMode Mode)
        {
            if (pPathFileName && pBuffer && BufferSize)
            {
                std::ofstream OutFile;
                switch (Mode)
                {
                    case CFile::eText:
                        OutFile.open(pPathFileName, std::ios::out);
                        break;
                    case CFile::eBinary:
                        OutFile.open(pPathFileName, std::ios::out | std::ios::binary);
                        break;
                }
                if ((!OutFile.fail()) && OutFile.is_open())
                {
                    OutFile.write((char*) pBuffer, BufferSize);
                    if (!OutFile.fail())
                    {
                        OutFile.close();
                        return (!OutFile.fail());
                    }
                }
            }
            return false;
        }

        bool COutFile::WriteStringToFile(const_string pPathFileName, const std::string& Content)
        {
            return WriteBufferToFile(pPathFileName, Content.c_str(), Content.length(), CFile::eText);
        }

        COutFile::COutFile(const_string pPathFileName, const CFile::FileMode Mode) :
            CFile(pPathFileName, Mode, CFile::eOutput)
        {
            m_FileStatus = Open() ? CFile::eReady : CFile::eErrorWhileOpenning;
        }

        COutFile::~COutFile()
        {
            Close();
        }

        bool COutFile::Open()
        {
            switch (m_Mode)
            {
                case CFile::eText:
                    m_OutStream.open(m_PathFileName.c_str(), std::ios::out);
                    break;
                case CFile::eBinary:
                    m_OutStream.open(m_PathFileName.c_str(), std::ios::out | std::ios::binary);
                    break;
            }
            return (!m_OutStream.fail()) && m_OutStream.is_open();
        }

        bool COutFile::WriteBuffer(const void* pBuffer, const uint BufferSize)
        {
            if (IsReady() && pBuffer && BufferSize)
            {
                m_OutStream.write((char*) pBuffer, BufferSize);
                if (m_OutStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileWrinting;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool COutFile::WriteString(const std::string& Content)
        {
            if (IsReady() && Content.length())
            {
                m_OutStream.write((char*) Content.c_str(), Content.length());
                if (m_OutStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileWrinting;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool COutFile::WriteValue(const uint Value)
        {
            if (IsReady())
            {
                m_OutStream.write((char*) &Value, sizeof(uint));
                if (m_OutStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileWrinting;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool COutFile::WriteValue(const real Value)
        {
            if (IsReady())
            {
                m_OutStream.write((char*) &Value, sizeof(real));
                if (m_OutStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileWrinting;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool COutFile::Close()
        {
            if (m_FileStatus != eClosed)
            {
                m_OutStream.close();
                if (!m_OutStream.fail())
                {
                    m_FileStatus = CFile::eClosed;
                    return true;
                }
            }
            return false;
        }
    }
}
