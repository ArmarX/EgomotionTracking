/*
 * OutFile.h
 *
 *  Created on: Mar 28, 2012
 *      Author: david
 */

#pragma once

#include "File.h"

namespace EVP
{
    namespace Files
    {
        class COutFile: public CFile
        {
        public:

            static bool WriteBufferToFile(const_string pPathFileName, const void* pBuffer, const uint BufferSize, const CFile::FileMode Mode);
            static bool WriteStringToFile(const_string pPathFileName, const std::string& Content);

            COutFile(const_string pPathFileName, const CFile::FileMode Mode);
            ~COutFile() override;

            bool WriteBuffer(const void* pBuffer, const uint BufferSize);
            bool WriteString(const std::string& Content);
            bool WriteValue(const uint Value);
            bool WriteValue(const real Value);

            bool Close() override;

        protected:

            bool Open() override;
            std::ofstream m_OutStream;
        };
    }
}

