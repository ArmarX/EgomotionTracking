/*
 * InFile.cpp
 *
 *  Created on: Mar 28, 2012
 *      Author: david
 */

#include "InFile.h"

namespace EVP
{
    namespace Files
    {
        bool CInFile::ReadBufferFromFile(const_string pPathFileName, void* pBuffer, const uint BufferSize, const CFile::FileMode Mode)
        {
            if (BufferSize && (CFile::Exists(pPathFileName) >= BufferSize))
            {
                std::ifstream InStream;
                switch (Mode)
                {
                    case CFile::eText:
                        InStream.open(pPathFileName, std::ios::in);
                        break;
                    case CFile::eBinary:
                        InStream.open(pPathFileName, std::ios::in | std::ios::binary);
                        break;
                }
                if ((!InStream.fail()) && InStream.is_open())
                {
                    InStream.read((char*) pBuffer, BufferSize);
                    if (!InStream.fail())
                    {
                        InStream.close();
                        return !InStream.fail();
                    }
                }
            }
            return false;
        }

        bool CInFile::ReadStringFromFile(const_string pPathFileName, std::string& Content)
        {
            const uint FileSize = CFile::Exists(pPathFileName);
            if (FileSize)
            {
                std::ifstream InStream(pPathFileName, std::ios::in);
                if ((!InStream.fail()) && InStream.is_open())
                {
                    const uint BufferSize = FileSize + 1;
                    char* pBuffer = new char[BufferSize];
                    InStream.read(pBuffer, BufferSize);
                    if (InStream.gcount())
                    {
                        Content = std::string(pBuffer);
                    }
                    else
                    {
                        Content.clear();
                    }
                    delete[] pBuffer;
                    if (!InStream.fail())
                    {
                        InStream.close();
                        return !InStream.fail();
                    }
                }
            }
            return false;
        }

        CInFile::CInFile(const_string pPathFileName, const CFile::FileMode Mode) :
            CFile(pPathFileName, Mode, CFile::eInput)
        {
            m_FileStatus = Open() ? CFile::eReady : CFile::eErrorWhileOpenning;
        }

        CInFile::~CInFile()
        {
            Close();
        }

        bool CInFile::ReadBuffer(void* pBuffer, const uint BufferSize)
        {
            if (IsReady() && pBuffer && BufferSize)
            {
                m_InStream.read((char*) pBuffer, BufferSize);
                if (m_InStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileReading;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool CInFile::ReadLine(std::string& Content)
        {
            if (IsReady())
            {
                char ContentBuffer[1024];
                m_InStream.read(ContentBuffer, 1024);
                if (m_InStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileReading;
                    return false;
                }
                if (!m_InStream.gcount())
                {
                    return false;
                }
                Content = std::string(ContentBuffer);
                return true;
            }
            return false;
        }

        bool CInFile::ReadValue(uint& Value)
        {
            if (IsReady())
            {
                m_InStream.read((char*) &Value, sizeof(uint));
                if (m_InStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileReading;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool CInFile::ReadValue(real& Value)
        {
            if (IsReady())
            {
                m_InStream.read((char*) &Value, sizeof(real));
                if (m_InStream.fail())
                {
                    m_FileStatus = CFile::eErrorWhileReading;
                    return false;
                }
                return true;
            }
            return false;
        }

        bool CInFile::Close()
        {
            if (m_FileStatus != eClosed)
            {
                m_InStream.close();
                if (!m_InStream.fail())
                {
                    m_FileStatus = CFile::eClosed;
                    return true;
                }
            }
            return false;
        }

        bool CInFile::Open()
        {
            switch (m_Mode)
            {
                case CFile::eText:
                    m_InStream.open(m_PathFileName.c_str(), std::ios::in);
                    break;
                case CFile::eBinary:
                    m_InStream.open(m_PathFileName.c_str(), std::ios::in | std::ios::binary);
                    break;
            }
            return (!m_InStream.fail()) && m_InStream.is_open();
        }
    }
}
