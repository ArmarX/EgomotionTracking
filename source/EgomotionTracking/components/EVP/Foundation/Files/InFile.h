/*
 * InFile.h
 *
 *  Created on: Mar 28, 2012
 *      Author: david
 */

#pragma once

#include "File.h"

namespace EVP
{
    namespace Files
    {
        class CInFile: public CFile
        {
        public:

            static bool ReadBufferFromFile(const_string pPathFileName, void* pBuffer, const uint BufferSize, const CFile::FileMode Mode);
            static bool ReadStringFromFile(const_string pPathFileName, std::string& Content);

            CInFile(const_string pPathFileName, const CFile::FileMode Mode);
            ~CInFile() override;

            bool ReadBuffer(void* pBuffer, const uint BufferSize);
            bool ReadLine(std::string& Content);
            bool ReadValue(uint& Value);
            bool ReadValue(real& Value);
            bool Close() override;

        protected:

            bool Open() override;
            std::ifstream m_InStream;
        };
    }
}

