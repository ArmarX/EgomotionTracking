/*
 * File.cpp
 *
 *  Created on: 17.10.2011
 *      Author: gonzalez
 */

#include "File.h"

namespace EVP
{
    namespace Files
    {
        uint CFile::Exists(const_string pPathFileName, const uint ConditionalFixedbyteSize)
        {
            if (pPathFileName)
            {
                struct stat QueryResult;
                if (stat(pPathFileName, &QueryResult) == 0)
                {
                    if (ConditionalFixedbyteSize)
                    {
                        return (ConditionalFixedbyteSize == uint(QueryResult.st_size)) ? ConditionalFixedbyteSize : 0;
                    }
                    return uint(QueryResult.st_size);
                }
            }
            return 0;
        }

        CFile::CFile(const_string pPathFileName, const FileMode Mode, const FileType Type) :
            m_PathFileName(pPathFileName), m_Mode(Mode), m_Type(Type), m_FileStatus(eClosed)
        {
        }

        CFile::~CFile()
            = default;

        const std::string& CFile::GetPathFileName() const
        {
            return m_PathFileName;
        }

        CFile::FileMode CFile::GetMode() const
        {
            return m_Mode;
        }

        CFile::FileType CFile::GetType() const
        {
            return m_Type;
        }

        CFile::FileStatus CFile::GetFileStatus() const
        {
            return m_FileStatus;
        }

        bool CFile::IsReady() const
        {
            return (m_FileStatus == CFile::eReady);
        }
    }
}
