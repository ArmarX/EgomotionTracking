/*
 * File.h
 *
 *  Created on: 17.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"

namespace EVP
{
    namespace Files
    {
        class CFile
        {
        public:

            static uint Exists(const_string pPathFileName, const uint ConditionalFixedbyteSize = 0);

            enum FileMode
            {
                eText, eBinary
            };

            enum FileType
            {
                eInput, eOutput
            };

            enum FileStatus
            {
                eReady, eErrorWhileOpenning, eErrorWhileClosing, eErrorWhileWrinting, eErrorWhileReading, eClosed
            };

            CFile(const_string pPathFileName, const FileMode Mode, const FileType Type);
            virtual ~CFile();

            const string& GetPathFileName() const;
            FileMode GetMode() const;
            FileType GetType() const;
            FileStatus GetFileStatus() const;
            bool IsReady() const;

        protected:

            virtual  bool Open() = 0;
            virtual  bool Close() = 0;

            const string m_PathFileName;
            const FileMode m_Mode;
            const FileType m_Type;
            FileStatus m_FileStatus;
        };
    }
}

