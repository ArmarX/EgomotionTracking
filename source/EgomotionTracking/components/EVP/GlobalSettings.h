/*
 * GlobalSettings.h
 *
 *  Created on: 09.09.2011
 *      Author: gonzalez
 */

#pragma once
//GENERAL INFORMATION
/////////////////////////////////////////////////////////////////////////////
/*#ifndef EVP_INFO
#define EVP_INFO
#define _LIBRARY_ACRONYM_ "EVP"
#define _LIBRARY_NAME_ "ENVIRONMENTAL VISUAL PERCEPTION"
#define _MAJOR_VERSION_ 0
#define _MINOR_VERSION_ 0
#define _REVISION_NUMBER_ 0
#define _BUILD_NUMBER_ 0
#define _AUTHOR_ "M. Sc. - Ing. David Israel González Aguirre"
#define _AUTHOR_MAIL_ "gonzalez@ira.uka.de"
#define _CONTRIBUTOR0_ "Dipl-Inf. Julian Hoch"
#define _CONTRIBUTOR1_ "Michael Vollert"
#define _THANKS0_ "Prof. Dr. R. Dillmann" //20.04.2012 12:26 Rootik3 "Redundancy is Beautiful" + "Keine Angst vor Komplexität"
#define _THANKS1_ "Prof. Dr. T. Asfour"
#define _THANKS2_ "Prof. Dr. E. Bayro"
#define _THANKS3_ "Dr. P. Azad"
#define _SUPPORTER0_ "DAAD-CONACYT"
#define _SUPPORTER1_ "KIT-Karlsruhe Institute for Technology"
#define _SUPPORTER2_ "DFG-Project SFB 588"
#define _SUPPORTER3_ "EU-Project GRASP"
#define _SUPPORTER4_ "EU-Project Xperience"

#endif*/

/////////////////////////////////////////////////////////////////////////////
#include <EgomotionTracking/components/EgomotionTrackingConfiguration/EVPBuildSettings.h>
/////////////////////////////////////////////////////////////////////////////

//GENERAL SWITCHES
/////////////////////////////////////////////////////////////////////////////

//OPERATIVE SYSTEM
//#define _USE_WIN32_
//#define _USE_WIN64_
//#define _USE_MACOS_
#define _USE_LINUX32_
//#define _USE_LINUX64_

#define _USE_DEPENDECY_MECHANISM_
#define _USE_INSTANCE_COUNTING_

//#define _USE_OPEN_INVENTOR_SIMVOLEON_
#define _USE_PROCESS_PROFILING_
#define _USE_COLOR_SPACE_RGB_
#define _USE_COLOR_SPACE_RGBA_
#define _USE_COLOR_SPACE_HSL_
#define _USE_COLOR_SPACE_HSI_
#define _USE_COLOR_SPACE_HSV_
#define _USE_COLOR_SPACE_XYZ_
#define _USE_COLOR_SPACE_LAB_
/////////////////////////////////////////////////////////////////////////////

//STANDAR LIBRARIES
/////////////////////////////////////////////////////////////////////////////
#include <list>
#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath>

#ifdef _USE_LINUX32_

#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>

#endif

#ifdef _USE_WIN32_

#include <windows.h>

#endif

#include <time.h>
#include <limits.h>
#include <float.h>
#include <string>
#include <string.h>
#include <stdarg.h>
#include <pthread.h>
#include <sched.h>
#include <errno.h>
#include <stdint.h>
/////////////////////////////////////////////////////////////////////////////

namespace EVP
{
    //STANDAR NAMES SPACES
    /////////////////////////////////////////////////////////////////////////////
    using std::vector;
    using std::list;
    using std::string;
    using std::ostringstream;
    using std::ostream;

#define TMax std::max
#define TMin std::min
#define TSort std::stable_sort
#define g_ErrorStringOutput std::cerr  //TODO VERIFY THAT ERRORs OUTPUT GO HERE
#define g_ConsoleStringOutput std::cout
#define g_EndLine std::endl
#define g_Tabulator "\t"
#define g_DoubleTabulator "\t\t"
#define g_EndLineTabulator "\n\t"
    const string g_EmptyString;
    /////////////////////////////////////////////////////////////////////////////

    //GLOBAL DATA TYPES
    /////////////////////////////////////////////////////////////////////////////

    typedef const char* const_string; //TODO Rename to ptr_string and have "const ptr_string"

#define _BYTE_MIN_  0
#define _BYTE_MAX_  UCHAR_MAX
    typedef uint8_t byte;

#define _COORDINATE_MIN_    INT_MIN
#define _COORDINATE_MAX_    INT_MAX
    typedef int32_t coordinate;

#define _IDENTIFIER_MIN_    0
#define _IDENTIFIER_MAX_    UINT_MAX
    typedef uint32_t Identifier;

#define _DEVICEIDENTIFIER_MIN_  0
#define _DEVICEIDENTIFIER_MAX_  LONG_LONG_MAX
    typedef uint64_t DeviceIdentifier;

    string IdentifierToString(const Identifier Id, const uint Lenght);
    string DeviceIdentifierToString(const DeviceIdentifier Id);

    typedef float SingleReal;
    typedef double DoubleReal;
    typedef long double LongDoubleReal;

    /////////////////////////////////////////////////////////////////////////////

    //LOOKUP TABLES SWITCHES
#ifdef _USE_LOOKUP_TABLES_
    /////////////////////////////////////////////////////////////////////////////
#define _DEMOSAICING_USE_LOOKUP_TABLE_
#define _FUSION_USE_LOOKUP_TABLE_
#define _SMOOTHING_USE_LOOKUP_TABLE_
#define _SALIENCYBLOCK_USE_LOOKUP_TABLE_
#define _HISTOGRAMFUSIONPIXEL_USE_LOOKUP_TABLE_
#define _STEREO_IMAGE_BAYER_HDR_SYNTHESIZER_USE_LOOKUP_TABLE_
#define _STEREOIMAGE_CONTINUOUS_ADAPTIVE_BILATERAL_SMOOTHER_USE_LOOKUP_TABLE_
#define _STEREOIMAGE_CONTINUOUS_RGB_DESATURATOR_USE_LOOKUP_TABLE_
#define _SALIENCY_PIXEL_USE_LOOKUP_TABLE_
#define _EDGE_SALIENCY_BLOCK_USE_LOOKUP_TABLE_
#define _STEREOIMAGE_GABOR_PHASE_RIM_SALIENCY_EXTRACTOR_USE_LOOKUP_TABLE_
    /////////////////////////////////////////////////////////////////////////////
#endif

    //INSTANCE COUNTING SWITCHES
#ifdef _USE_INSTANCE_COUNTING_
    /////////////////////////////////////////////////////////////////////////////
#define INSTANCE_COUNTING static Identifier s_TotalExistingInstances;public:static Identifier GetTotalExistingInstances(){return s_TotalExistingInstances;}protected:
#define INSTANCE_COUNTING_INITIALIZATION(CLASS) Identifier CLASS::s_TotalExistingInstances = 0;
#define INSTANCE_COUNTING_CONTRUCTOR(CLASS) ++CLASS::s_TotalExistingInstances;
#define INSTANCE_COUNTING_DESTRUCTOR(CLASS) --CLASS::s_TotalExistingInstances;
#else
#define INSTANCE_COUNTING
#define INSTANCE_COUNTING_INITIALIZATION(CLASS)
#define INSTANCE_COUNTING_CONTRUCTOR(CLASS)
#define INSTANCE_COUNTING_DESTRUCTOR(CLASS)
    /////////////////////////////////////////////////////////////////////////////
#endif

    //INSTANCE FAST CALLS
#ifdef _USE_FAST_CALLS_
    /////////////////////////////////////////////////////////////////////////////
#define _EVP_FAST_CALL_ __attribute__((fastcall))
#else
#define _EVP_FAST_CALL_
    /////////////////////////////////////////////////////////////////////////////
#endif

#ifdef _USE_SINGLE_PRECISION_
    /////////////////////////////////////////////////////////////////////////////
    typedef float real;
    const string realValueToString(const real Value);
#define _REAL_ZERO_ 0.0f
#define _REAL_HALF_ (1.0f/2.0f)
#define _REAL_THIRD_ (1.0f/3.0f)
#define _REAL_TWO_THIRDS_ (2.0f/3.0f)
#define _REAL_FOUR_THIRDS_ (4.0f/3.0f)
#define _REAL_FOURTH_ (1.0f/4.0f)
#define _REAL_ONE_ 1.0f
#define _REAL_TWO_ 2.0f
#define _REAL_THREE_ 3.0f
#define _REAL_255_ 255.0f
#define _REAL_1_255_ (1.0f/255.0f)
#define _REAL_127_5_ 127.5f
#define _REAL_1_127_5_ (1.0f/127.5f)
#define _REAL_QUANTIZATION_THRESHOLD_ 0.382683432f
#define _REAL_DEGREES_TO_RADIANS_   0.017453293f
#define _REAL_RADIANS_TO_DEGREES_   57.295779513f
#define _REAL_HALF_PI_ 1.57079632679489661923f
#define _REAL_THIRD_PI_ 1.04719755119659774615f
#define _REAL_INVERSE_PI_ 0.3183098862f
#define _REAL_PI_ 3.14159265358979323846f
#define _REAL_2PI_ 6.283185307179586477f
#define _REAL_SQRT2_ 1.41421356237309504880f
#define _REAL_SQRT_1_2_ 0.70710678118654752440f
#define _REAL_INVERSE_HALF_PI_ 0.636619772
#define _REAL_MAX_ FLT_MAX
#define _REAL_MIN_ -FLT_MAX
#define _REAL_EPSILON_ FLT_EPSILON
#define _REAL_EPSILON_DISPLAY_DIGITS_ 8
#define _REAL_MILI_ 0.0001f
#define _REAL_MICRO_ 0.0000001f
#define RealSqrt(x) sqrtf(x)
#define RealAbs(x) fabsf(x)
#define RealSinus(x) sinf(x)
#define RealCosinus(x) cosf(x)
#define RealArcCosinus(x) acosf(x)
#define RealAtan(x) atanf(x)
#define RealAtan2(y,x) atan2f(y,x)
#define RealLog(x) logf(x)
#define RealLog10(x) log10f(x)
#define RealLog2(x) logf(x)/logf(2.0f)
#define RealExp(x) expf(x)
#define RealPow(x,y) powf(x,y)
#define RealRound(x) roundf(x)
#define RealCeil(x) ceilf(x)
#define RealFloor(x) floorf(x)
#define RealErrorFunction(x) erff(x)
#define RealHypotenuse(x,y) hypotf(x,y)
    /////////////////////////////////////////////////////////////////////////////
#endif

#ifdef _USE_DOUBLE_PRECISION_
    /////////////////////////////////////////////////////////////////////////////
    typedef double real;
    string realValueToString(const real Value);
#define _REAL_ZERO_ 0.0
#define _REAL_HALF_ (1.0/2.0)
#define _REAL_THIRD_ (1.0/3.0)
#define _REAL_TWO_THIRDS_ (2.0/3.0)
#define _REAL_FOUR_THIRDS_ (4.0/3.0)
#define _REAL_FOURTH_ (1.0/4.0)
#define _REAL_ONE_ 1.0
#define _REAL_TWO_ 2.0
#define _REAL_THREE_ 3.0
#define _REAL_255_ 255.0
#define _REAL_1_255_ 0.003921568627450980392156862745098
#define _REAL_127_5_ 127.5
#define _REAL_1_127_5_ 0.0078431372549019607843137254901961
#define _REAL_QUANTIZATION_THRESHOLD_ 0.382683432
#define _REAL_DEGREES_TO_RADIANS_   0.017453293
#define _REAL_RADIANS_TO_DEGREES_   57.295779513
#define _REAL_HALF_PI_ 1.57079632679489661923
#define _REAL_THIRD_PI_ 1.04719755119659774615
#define _REAL_INVERSE_PI_ 0.3183098862
#define _REAL_PI_ 3.14159265358979323846
#define _REAL_2PI_ 6.283185307179586477
#define _REAL_SQRT2_ 1.41421356237309504880
#define _REAL_SQRT_1_2_ 0.70710678118654752440
#define _REAL_INVERSE_HALF_PI_ 0.63661977
#define _REAL_MAX_ DBL_MAX
#define _REAL_MIN_ -DBL_MAX
#define _REAL_EPSILON_ DBL_EPSILON
#define _REAL_EPSILON_DISPLAY_DIGITS_ 16
#define _REAL_MILI_ 0.0001
#define _REAL_MICRO_ 0.0000001
#define RealSqrt(x) sqrt(x)
#define RealAbs(x) fabs(x)
#define RealSinus(x) sin(x)
#define RealCosinus(x) cos(x)
#define RealArcCosinus(x) acos(x)
#define RealAtan(x) atan(x)
#define RealAtan2(y,x) atan2(y,x)
#define RealLog(x) log(x)
#define RealLog10(x) log10(x)
#define RealLog2(x) log(x)/log(2.0)
#define RealExp(x) exp(x)
#define RealPow(x,y) pow(x,y)
#define RealRound(x) round(x)
#define RealCeil(x) ceil(x)
#define RealFloor(x) floor(x)
#define RealErrorFunction(x) erf(x)
#define RealHypotenuse(x,y) hypot(x,y)
    /////////////////////////////////////////////////////////////////////////////
#endif

#ifdef _USE_LONG_DOUBLE_PRECISION_
    /////////////////////////////////////////////////////////////////////////////
    typedef long double real;
    string realValueToString(const real Value);
#define _REAL_ZERO_ (long double)(0.0)
#define _REAL_HALF_ (((long double)(1.0))/((long double)(2.0)))
#define _REAL_THIRD_ (long double)(1.0/3.0)
#define _REAL_TWO_THIRDS_ (long double)(2.0/3.0)
#define _REAL_FOUR_THIRDS_ (long double)(4.0/3.0)
#define _REAL_FOURTH_ (long double)(1.0/4.0)
#define _REAL_ONE_ (long double)(1.0)
#define _REAL_TWO_ (long double)(2.0)
#define _REAL_THREE_ (long double)(3.0)
#define _REAL_255_ (long double)(255.0)
#define _REAL_1_255_ (long double)(0.003921568627450980392156862745098)
#define _REAL_127_5_ (long double)(127.5)
#define _REAL_1_127_5_ (long double)(0.0078431372549019607843137254901961)
#define _REAL_QUANTIZATION_THRESHOLD_  (long double)(0.382683432)
#define _REAL_DEGREES_TO_RADIANS_   (long double)(0.017453293)
#define _REAL_RADIANS_TO_DEGREES_   (long double)(57.295779513)
#define _REAL_HALF_PI_ (long double)(1.57079632679489661923)
#define _REAL_THIRD_PI_ (long double)(1.04719755119659774615)
#define _REAL_INVERSE_PI_ (long double)(0.3183098862)
#define _REAL_PI_ (long double)(3.14159265358979323846)
#define _REAL_2PI_ (long double)(6.283185307179586477)
#define _REAL_SQRT2_ (long double)(1.41421356237309504880)
#define _REAL_SQRT_1_2_ (long double)(0.70710678118654752440)
#define _REAL_INVERSE_HALF_PI_ (long double)(0.63661977)
#define _REAL_MAX_ LDBL_MAX
#define _REAL_MIN_ -LDBL_MAX
#define _REAL_EPSILON_ LDBL_EPSILON
#define _REAL_EPSILON_DISPLAY_DIGITS_ 24
#define _REAL_MILI_ (long double)(0.0001)
#define _REAL_MICRO_ (long double)(0.0000001)
#define RealSqrt(x) sqrtl(x)
#define RealAbs(x) fabsl(x)
#define RealSinus(x) sinl(x)
#define RealCosinus(x) cosl(x)
#define RealArcCosinus(x) acosl(x)
#define RealAtan(x) atanl(x)
#define RealAtan2(y,x) atan2l(y,x)
#define RealLog(x) logl(x)
#define RealLog10(x) log10l(x)
#define RealLog2(x) logl(x)/log(2.0)
#define RealExp(x) expl(x)
#define RealPow(x,y) powl(x,y)
#define RealRound(x) roundl(x)
#define RealCeil(x) ceill(x)
#define RealFloor(x) floorl(x)
#define RealErrorFunction(x) erfl(x)
#define RealHypotenuse(x,y) hypotl(x,y)
    /////////////////////////////////////////////////////////////////////////////
#endif

#define RoundNegativeRealToInteger(x) int(x-_REAL_HALF_) //TODO USE THIS WHR POSSIBLE MUCH FASTER THAN RoundToInteger
#define RoundPositiveRealToInteger(x) int(x+_REAL_HALF_) //TODO USE THIS WHR POSSIBLE MUCH FASTER THAN RoundToInteger
#define RoundToInteger(x) int(RealRound(x)) //TODO ADD RoundtoPixelcoordinate VErify where to use a faster approach, in particular the lookup tables
#define UpperToInteger(x) int(RealCeil(x))
#define DownToInteger(x) int(x)

#define RELEASE_OBJECT_DIRECT(pObject) {delete pObject;}
#define RELEASE_OBJECT(pObject) if(pObject){delete pObject;pObject=NULL;}
#define RELEASE_OBJECT_BY_OWNERSHIP(pObject,OwnerShip) if(pObject){if(OwnerShip)delete pObject;pObject=NULL;}
#define RELEASE_ARRAY(pBase) if(pBase){delete[]pBase;pBase=NULL;}
#define RELEASE_ARRAY_BY_OWNERSHIP(pBase,OwnerShip) if(pBase){if(OwnerShip)delete[]pBase;pBase=NULL;}

#define RoundTobyte(x) byte(RealRound(x))
    byte SafeRoundToByte(const real X);

}

#ifdef _EVP_USE_GUI_TOOLKIT
#ifdef _USE_OPEN_INVENTOR_
#define _EVP_USE_MODEL_SPACE
#endif //_USE_OPEN_INVENTOR_
#endif //_EVP_USE_GUI_TOOLKIT

