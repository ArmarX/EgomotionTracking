/*
 * StereoImageContinuousSubsampler.cpp
 *
 *  Created on: 25.03.2011
 *      Author: gonzalez
 */

#ifdef ON___

#include "StereoImageContinuousSubsampler.h"

using namespace EVP::VisualSpace::Kernels;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            CStereoImageContinuousSubsampler::CStereoImageContinuousSubsampler(const real SubsamplingFactor, const CImageActiveZone* pActiveZone, const TImage<real>** ppInputImages) :
                TStereoImageProcess<real, real, CDiscreteTristimulusPixel> (eStereoImageContinuousSubsampler, pActiveZone, ppInputImages), m_SubsamplingFactor(SubsamplingFactor), m_SubsamplingOffset(realInverse(m_SubsamplingFactor)), m_pSmoothingKernel(NULL)
            {
                m_IsEnabled &= (m_SubsamplingFactor > _REAL_EPSILON_);
                if (m_IsEnabled)
                {
                    m_pSmoothingKernel = new CConvolutionGaussianKernel2D(RealLog2(m_SubsamplingOffset));
                    m_OutputActiveZone.AddMarginOffset(m_pSmoothingKernel->GetRadius());
                    m_OutputActiveZone = m_OutputActiveZone.CreateScaledZone(m_SubsamplingFactor);
                    m_ppOutputImages = new TImage<real>* [_STEREO_];
                    m_ppDisplayImages = new TImage<CDiscreteTristimulusPixel>* [_STEREO_];
                    const CImageSize& OutputImageSize = m_OutputActiveZone.GetSize();
                    for (uint i = 0; i < _STEREO_; ++i)
                    {
                        m_ppOutputImages[i] = new TImage<real> (OutputImageSize);
                        m_ppDisplayImages[i] = new TImage<CDiscreteTristimulusPixel> (OutputImageSize);
                    }
                }
            }

            CStereoImageContinuousSubsampler::~CStereoImageContinuousSubsampler()
            {
                if (m_IsEnabled)
                {
                    for (uint i = 0; i < _STEREO_; ++i)
                    {
                        delete m_ppOutputImages[i];
                        delete m_ppDisplayImages[i];
                    }
                    delete[] m_ppOutputImages;
                    m_ppOutputImages = NULL;
                    delete[] m_ppDisplayImages;
                    m_ppDisplayImages = NULL;
                    delete m_pSmoothingKernel;
                    m_pSmoothingKernel = NULL;
                }
            }

            real CStereoImageContinuousSubsampler::GetSubsamplingFactor() const
            {
                return m_SubsamplingFactor;
            }

            bool CStereoImageContinuousSubsampler::Subsampling(const Identifier TrialId)
            {
                if (m_IsEnabled)
                {
                    START_PROCESS_EXECUTION_LOG("Subsampling", TrialId)

                    const coordinate SX0 = m_InputActiveZone.GetX0();
                    const coordinate SY0 = m_InputActiveZone.GetY0();
                    const coordinate DX0 = m_OutputActiveZone.GetX0();
                    const coordinate DY0 = m_OutputActiveZone.GetY0();
                    const coordinate DX1 = m_OutputActiveZone.GetX1();
                    const coordinate DY1 = m_OutputActiveZone.GetY1();
                    const coordinate InputImageWidth = m_InputActiveZone.GetWidth();
                    const coordinate OutputImageWidth = m_OutputActiveZone.GetWidth();
                    const uint KernelDiameter = m_pSmoothingKernel->GetWidth();
                    const real* pBaseKernel = m_pSmoothingKernel->GetKernelReadOnlyBuffer();
                    real* pLeftBaseOutputPixel = m_ppOutputImages[_LEFT_]->GetWritableBufferAt(DX0, DY0);
                    real* pRightBaseOutputPixel = m_ppOutputImages[_RIGHT_]->GetWritableBufferAt(DX0, DY0);
                    real ys0 = SY0;
                    for (coordinate y = DY0; y < DY1; ++y, ys0 += m_SubsamplingOffset, pLeftBaseOutputPixel += OutputImageWidth, pRightBaseOutputPixel += OutputImageWidth)
                    {
                        real* pLeftOutputPixel = pLeftBaseOutputPixel;
                        real* pRightOutputPixel = pRightBaseOutputPixel;
                        const coordinate yds = RoundToInteger(ys0);
                        real xs0 = SX0;
                        for (coordinate x = DX0; x < DX1; ++x, xs0 += m_SubsamplingOffset, ++pLeftOutputPixel, ++pRightOutputPixel)
                        {
                            real LeftAcumulator = _REAL_ZERO_, RightAcumulator = _REAL_ZERO_;
                            const real* pKernel = pBaseKernel;
                            const real* pLeftBaseInputPixel = m_ppInputImages[_LEFT_]->GetReadOnlyBufferAt(RoundToInteger(xs0), yds);
                            const real* pRightBaseInputPixel = m_ppInputImages[_RIGHT_]->GetReadOnlyBufferAt(RoundToInteger(xs0), yds);
                            for (uint i = 0; i < KernelDiameter; ++i, pLeftBaseInputPixel += InputImageWidth, pRightBaseInputPixel += InputImageWidth)
                            {
                                const real* pLeftInputPixel = pLeftBaseInputPixel;
                                const real* pRightInputPixel = pRightBaseInputPixel;
                                for (uint j = 0; j < KernelDiameter; ++j, ++pLeftInputPixel, ++pRightInputPixel, ++pKernel)
                                {
                                    LeftAcumulator += *pLeftInputPixel * *pKernel;
                                    RightAcumulator += *pRightInputPixel * *pKernel;
                                }
                            }
                            *pLeftOutputPixel = LeftAcumulator;
                            *pRightOutputPixel = RightAcumulator;
                        }
                    }

                    STOP_PROCESS_EXECUTION_LOG

                    return true;
                }
                return false;
            }
        }
    }
}
#endif
