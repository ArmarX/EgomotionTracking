/*
 * StereoImageContinuousTristimulusSubsampler.h
 *
 *  Created on: 25.03.2011
 *      Author: gonzalez
 */
#ifdef ON___

#ifndef STEREOIMAGECONTINUOUSTRISTIMULUSSUBSAMPLER_H_
#define STEREOIMAGECONTINUOUSTRISTIMULUSSUBSAMPLER_H_

#include "../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../Common/StereoImageExporter.h"
#include "../../Kernels/ConvolutionGaussianKernel2D.h"
#include "../Base/StereoImageProcess.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            class CStereoImageContinuousTristimulusSubsampler: public TStereoImageProcess<CContinuousTristimulusPixel, CContinuousTristimulusPixel, CDiscreteTristimulusPixel>
            {
            public:

                CStereoImageContinuousTristimulusSubsampler(const real SubsamplingFactor, const CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>** pInputImages);
                virtual ~CStereoImageContinuousTristimulusSubsampler();

                real GetSubsamplingFactor() const;
                virtual  bool Subsampling(const Identifier TrialId);


            protected:

                const real m_SubsamplingFactor;
                const real m_SubsamplingOffset;
                Kernels::CConvolutionGaussianKernel2D* m_pSmoothingKernel;
            };
        }
    }
}

#endif /* STEREOIMAGECONTINUOUSTRISTIMULUSSUBSAMPLER_H_ */
#endif
