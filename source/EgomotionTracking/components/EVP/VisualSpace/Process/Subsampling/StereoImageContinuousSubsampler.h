/*
 * StereoImageContinuousRGBSubsampler.h
 *
 *  Created on: 25.03.2011
 *      Author: gonzalez
 */

#ifdef ON___

#ifndef STEREOIMAGECONTINUOUSSUBSAMPLER_H_
#define STEREOIMAGECONTINUOUSSUBSAMPLER_H_

#include "../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../Common/StereoImageExporter.h"
#include "../../Kernels/ConvolutionGaussianKernel2D.h"
#include "../Base/StereoImageProcess.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            class CStereoImageContinuousSubsampler: public TStereoImageProcess<real, real, CDiscreteTristimulusPixel>
            {
            public:

                CStereoImageContinuousSubsampler(const real SubsamplingFactor, const CImageActiveZone* pActiveZone, const TImage<real>** ppInputImages);
                virtual ~CStereoImageContinuousSubsampler();

                real GetSubsamplingFactor() const;
                virtual  bool Subsampling(const Identifier TrialId);

            protected:

                const real m_SubsamplingFactor;
                const real m_SubsamplingOffset;
                Kernels::CConvolutionGaussianKernel2D* m_pSmoothingKernel;
            };
        }
    }
}

#endif /* STEREOIMAGECONTINUOUSSUBSAMPLER_H_ */

#endif
