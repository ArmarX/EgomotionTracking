/*
 * StereoImageMultiChannelHDRSynthesizer.h
 *
 *  Created on: 11.04.2011
 *      Author: gonzalez
 */
/*
#ifndef STEREOIMAGEMULTICHANNELHDRSYNTHESIZER_H_
#define STEREOIMAGEMULTICHANNELHDRSYNTHESIZER_H_

//SUBPROCESS IDS
#define _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_ADD_EXPOSURE_                                    0
#define _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_ADD_EXPOSURE_FAST_MODE_                          1
#define _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_ADD_EXPOSURE_FASTEST_MODE_                       2
#define _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_SYNTHESIS_                                       3

//DEFAULT VALUES
#define _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SYNTHESIS_MODE_                    Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eHomomorphic
#define _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_WEIGHTING_MODE_                    Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eGaussianKernel
#define _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SIGMA_GAUSSIAN_KERNEL_             real(45.0)
#define _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_INTENSITY_A_GAUSSIAN_KERNEL_       real(8.0)
#define _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_INTENSITY_B_GAUSSIAN_KERNEL_       real(248.0)
#define _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SIGMA_A_GAUSSIAN_KERNEL_           real(2.636040916)
#define _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SIGMA_B_GAUSSIAN_KERNEL_           real(2.636040916)
#define _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SAMPLES_PER_DISCRETE_INCREMENT_    25
#define _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_MINIMAL_INTEGRATION_VALUE_         1
#define _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_MAXIMAL_INTEGRATION_VALUE_         254

#include "../../../Visualization/Miscellaneous/TristimulusColorMap.h"
#include "../../Cameras/RadiometricCalibration/MultiChannelRadiometricResponseFunction.h"
#include "../Base/StereoImageProcess.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            class CStereoImageMultiChannelHDRSynthesizer: public TStereoImageProcess<CContinuousTristimulusPixel, CContinuousTristimulusPixel, CDiscreteTristimulusPixel>
            {
                public:

                    struct Paramters
                    {
                            Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::SynthesisMode m_SynthesisMode;
                            Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::WeightingMode m_WeightingMode;
                            real m_SigmaGaussianKernel;
                            real m_GaussianBoxIntensityA;
                            real m_GaussianBoxIntensityB;
                            real m_SigmaGaussianKernelA;
                            real m_SigmaGaussianKernelB;
                            int m_SamplesPerDiscreteIncrement;
                            real m_MinimalIntegrationValue;
                            real m_MaximalIntegrationValue;
                    };

                    enum DisplayType
                    {
                        eAccumulatorRadianceMaps, eAccumulatorWeightingMaps, eRadianceMaps
                    };

                    struct ChannelDescriptiveStatistics
                    {
                            CContinuousTristimulusPixel m_MaximalIntensity;
                            CContinuousTristimulusPixel m_MinimalIntensity;
                            CContinuousTristimulusPixel m_Range;
                            CContinuousTristimulusPixel m_Mean;
                            int m_EncondingBits;
                    };

                    CStereoImageMultiChannelHDRSynthesizer(Cameras::RadiometricCalibration::CMultiChannelRadiometricResponseFunction** ppMultiChannelRadiometricResponseFunction, const CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>** ppInputImages);
                    virtual ~CStereoImageMultiChannelHDRSynthesizer();

                    const bool AddExposureContinousMode(const Identifier TrialId, const real ShuttingTime);
                    const bool AddExposureLinealInterpolatedMode(const Identifier TrialId, const real ShuttingTime);
                    const bool AddExposureSampledMode(const Identifier TrialId, const real ShuttingTime);

                    const bool Synthesis(const Identifier TrialId);
                    void ClearExposures();
                    uint GetTotalExposures();

                    TImage<CContinuousTristimulusPixel>** GetAccumulatorRadianceMaps();
                    TImage<CContinuousTristimulusPixel>** GetAccumulatorWeightingMaps();
                    Paramters* GetParamters();

                    bool Display(DisplayType Type);

                    bool GetChannelsDescriptiveStatistics(ChannelDescriptiveStatistics& LeftChannelDescriptiveStatistics, ChannelDescriptiveStatistics& RightChannelDescriptiveStatistics);

                    bool LoadSampledResponseIdealFunction();

                    TImage<CContinuousTristimulusPixel>** GetChromaticMaps();

                protected:

                    inline real ContinuousTriangularWeigthingKernel(real Intensity);
                    inline real ContinuousEpanechnikovWeigthingKernel(real Intensity);
                    inline real ContinuousGaussianWeigthingKernel(real Intensity, real ExponentFactor);
                    inline real ContinuousGaussianBoxWeigthingKernel(real Intensity, real IntensityA, real IntensityB, real ExponentFactorA, real ExponentFactorB);
                    inline void AddChromaticSample(real LeftWeight, real RightWeight);
                    inline void NormalizeChromaticSamples();

                    Paramters m_Paramters;
                    uint m_TotalExposures;
                    bool m_IsSampledResponseIdealFunctionLoaded;
                    real m_LeftWeightAccumulator;
                    real m_RightWeightAccumulator;
                    TImage<CContinuousTristimulusPixel>* m_ppAccumulatorChromaticMaps[2];
                    TImage<CContinuousTristimulusPixel>* m_ppAccumulatorRadianceMaps[2];
                    TImage<CContinuousTristimulusPixel>* m_ppAccumulatorWeightingMaps[2];
                    Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction* m_pLeftRedChannelRadiometricResponseFunction;
                    Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction* m_pLeftGreenChannelRadiometricResponseFunction;
                    Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction* m_pLeftBlueChannelRadiometricResponseFunction;
                    Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction* m_pRightRedChannelRadiometricResponseFunction;
                    Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction* m_pRightGreenChannelRadiometricResponseFunction;
                    Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction* m_pRightBlueChannelRadiometricResponseFunction;

                    real m_SamplesPerDiscreteIncrement;
                    CContinuousTristimulusPixel m_LeftMinimalIntegrationValue;
                    real* m_pLeftRedWeigthingKernel;
                    real* m_pLeftGreenWeigthingKernel;
                    real* m_pLeftBlueWeigthingKernel;
                    real* m_pLeftRedSampledResponseIdealFunction;
                    real* m_pLeftGreenSampledResponseIdealFunction;
                    real* m_pLeftBlueSampledResponseIdealFunction;
                    CContinuousTristimulusPixel m_RightMinimalIntegrationValue;
                    real* m_pRightRedWeigthingKernel;
                    real* m_pRightGreenWeigthingKernel;
                    real* m_pRightBlueWeigthingKernel;
                    real* m_pRightRedSampledResponseIdealFunction;
                    real* m_pRightGreenSampledResponseIdealFunction;
                    real* m_pRightBlueSampledResponseIdealFunction;
            };
        }
    }
}

#endif *//* STEREOIMAGEMULTICHANNELHDRSYNTHESIZER_H_ */
