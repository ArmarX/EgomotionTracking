/*
 * StereoImageSingleChannelPixelBasedHDRSynthesizer.h
 *
 *  Created on: 27.09.2011
 *      Author: gonzalez
 */
/*
#ifndef STEREOIMAGESINGLECHANNELPIXELBASEDHDRSYNTHESIZER_H_
#define STEREOIMAGESINGLECHANNELPIXELBASEDHDRSYNTHESIZER_H_

#include "../Base/StereoImageProcess.h"
#include "../../../Foundation/Files/File.h"
#include "../../../Visualization/Miscellaneous/TristimulusColorMap.h"
#include "../../../VisualSpace/Cameras/RadiometricCalibration/PixelBasedCameraRadiometricCalibration.h"

#define TSISPHS TStereoImageProcess<IntensityPixelDataType, real, CDiscreteTristimulusPixel>

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            template<typename IntensityPixelDataType> class TStereoImageSingleChannelPixelBasedHDRSynthesizer: public TStereoImageProcess<IntensityPixelDataType, real, CDiscreteTristimulusPixel>
            {
                public:

                    TStereoImageSingleChannelPixelBasedHDRSynthesizer(DeviceIdentifier LeftCameraId, DeviceIdentifier RightCameraId, const CImageActiveZone* pActiveZone, TImage<IntensityPixelDataType>* pInputImages[2]) :
                        TStereoImageProcess<IntensityPixelDataType, real, CDiscreteTristimulusPixel> (CStereoImageProcessBase::eStereoImageSingleChannelPixelBasedHDRSynthesizer, pActiveZone, pInputImages)
                    {
                        m_CalibrationReady = false;
                        m_LeftCameraId = LeftCameraId;
                        m_RightCameraId = RightCameraId;
                        TSISPHS::m_Enabled &= m_LeftCameraId && m_RightCameraId && (m_LeftCameraId != m_RightCameraId);
                        if (TSISPHS::m_Enabled)
                            for (uint i = 0; i < _STEREO_; ++i)
                            {
                                const CImageSize& ImageSize = TSISPHS::m_pInputImages[i]->GetSize();
                                TSISPHS::m_pOutputImages[i] = TImage<real> ();
                                TSISPHS::m_pDisplayImages[i] = TImage<CDiscreteTristimulusPixel> (ImageSize);
                                m_pRadiometricCalibrationImages[i] = TImage<Cameras::RadiometricCalibration::CPixelBasedCameraRadiometricCalibration::RadiometricCalibrationPixel> (ImageSize);
                                m_pAccumulatorChromaticMaps[i] = new TImage<real> (ImageSize);
                                m_pAccumulatorRadianceMaps[i] = new TImage<real> (ImageSize);
                                m_pAccumulatorWeightingMaps[i] = new TImage<real> (ImageSize);
                            }
                    }

                    virtual ~TStereoImageSingleChannelPixelBasedHDRSynthesizer()
                    {
                        if (TSISPHS::m_Enabled)
                            for (uint i = 0; i < _STEREO_; ++i)
                            {
                                delete TSISPHS::m_pOutputImages[i];
                                delete TSISPHS::m_pDisplayImages[i];
                                delete m_pRadiometricCalibrationImages[i];
                                delete m_pAccumulatorChromaticMaps[i];
                                delete m_pAccumulatorRadianceMaps[i];
                                delete m_pAccumulatorWeightingMaps[i];
                            }
                    }

                    bool LoadResponceFunctionFromBinaryFile(const_string pLeftFileName, const_string pRightFileName)
                    {
                        if (TSISPHS::m_Enabled)
                        {
                            m_CalibrationReady = LoadResponceFunctionFromBinaryFile(pLeftFileName, m_LeftCameraId, m_pRadiometricCalibrationImages[0]);
                            m_CalibrationReady &= LoadResponceFunctionFromBinaryFile(pRightFileName, m_RightCameraId, m_pRadiometricCalibrationImages[1]);
                            return m_CalibrationReady;
                        }
                        return false;
                    }

                    inline TImage<real>** GetAccumulatorRadianceMaps()
                    {
                        return m_pAccumulatorRadianceMaps;
                    }

                    inline TImage<real>** GetAccumulatorWeightingMaps()
                    {
                        return m_pAccumulatorWeightingMaps;
                    }

                    inline TImage<real>** GetChromaticMaps()
                    {
                        return m_pAccumulatorChromaticMaps;
                    }

                protected:

                    bool LoadResponceFunctionFromBinaryFile(const_string pPathFileName, DeviceIdentifier CameraId, TImage<Cameras::RadiometricCalibration::CPixelBasedCameraRadiometricCalibration::RadiometricCalibrationPixel>* pRadiometricCalibrationImages)
                    {
                        if (CFile::Exists(pPathFileName))
                        {
                            CInputFile InputBinaryFile(pPathFileName, _INPUT_BINARYFILE_FLAGS_);
                            if ((!InputBinaryFile.fail()) && InputBinaryFile.is_open())
                            {
                                DeviceIdentifier StoredCameraId = 0;
                                InputBinaryFile.read((char*) &StoredCameraId, sizeof(DeviceIdentifier));
                                if (CameraId == StoredCameraId)
                                {
                                    CImageSize ImageSize;
                                    InputBinaryFile.read((char*) &ImageSize, sizeof(CImageSize));
                                    if (ImageSize == pRadiometricCalibrationImages->GetSize())
                                    {
                                        InputBinaryFile.read((char*) pRadiometricCalibrationImages->GetBeginWritableBuffer(), pRadiometricCalibrationImages->GetBufferSize());
                                        InputBinaryFile.close();
                                        return !InputBinaryFile.fail();
                                    }
                                }
                            }
                        }
                        return false;
                    }

                    bool m_CalibrationReady;
                    DeviceIdentifier m_LeftCameraId;
                    DeviceIdentifier m_RightCameraId;
                    TImage<Cameras::RadiometricCalibration::CPixelBasedCameraRadiometricCalibration::RadiometricCalibrationPixel>* m_pRadiometricCalibrationImages[2];
                    TImage<real>* m_pAccumulatorChromaticMaps[2];
                    TImage<real>* m_pAccumulatorRadianceMaps[2];
                    TImage<real>* m_pAccumulatorWeightingMaps[2];
            };
        }
    }
}

#endif*/ /* STEREOIMAGESINGLECHANNELPIXELBASEDHDRSYNTHESIZER_H_ */
