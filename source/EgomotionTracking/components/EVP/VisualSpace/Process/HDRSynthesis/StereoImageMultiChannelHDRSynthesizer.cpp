/*
 * StereoImageMultiChannelHDRSynthesizer.cpp
 *
 *  Created on: 11.04.2011
 *      Author: gonzalez
 */

#include "StereoImageMultiChannelHDRSynthesizer.h"

/*
namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            CStereoImageMultiChannelHDRSynthesizer::CStereoImageMultiChannelHDRSynthesizer(Cameras::RadiometricCalibration::CMultiChannelRadiometricResponseFunction** ppMultiChannelRadiometricResponseFunction, const CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>** ppInputImages) :
                TStereoImageProcess<CContinuousTristimulusPixel, CContinuousTristimulusPixel, CDiscreteTristimulusPixel> (eStereoImageMultiChannelHDRSynthesizer, pActiveZone, ppInputImages)
            {
                m_IsSampledResponseIdealFunctionLoaded = false;
                m_pRightRedSampledResponseIdealFunction = m_pRightGreenSampledResponseIdealFunction = m_pRightBlueSampledResponseIdealFunction = m_pLeftRedSampledResponseIdealFunction = m_pLeftGreenSampledResponseIdealFunction = m_pLeftBlueSampledResponseIdealFunction = m_pRightRedWeigthingKernel = m_pRightGreenWeigthingKernel = m_pRightBlueWeigthingKernel = m_pLeftRedWeigthingKernel = m_pLeftGreenWeigthingKernel = m_pLeftBlueWeigthingKernel = NULL;
                m_pRightRedChannelRadiometricResponseFunction = m_pRightGreenChannelRadiometricResponseFunction = m_pRightBlueChannelRadiometricResponseFunction = m_pLeftRedChannelRadiometricResponseFunction = m_pLeftGreenChannelRadiometricResponseFunction = m_pLeftBlueChannelRadiometricResponseFunction = NULL;
                m_SamplesPerDiscreteIncrement = m_RightWeightAccumulator = m_LeftWeightAccumulator = _REAL_ZERO_;
                memset(&m_Paramters, 0, sizeof(Paramters));
                if (m_IsEnabled)
                {
                    for (uint i = 0; i < _STEREO_; ++i)
                    {
                        const CImageSize& ImageSize = m_ppInputImages[i]->GetSize();
                        m_ppOutputImages[i] = new TImage<CContinuousTristimulusPixel> (ImageSize);
                        m_ppDisplayImages[i] = new TImage<CDiscreteTristimulusPixel> (ImageSize);
                        m_ppAccumulatorChromaticMaps[i] = new TImage<CContinuousTristimulusPixel> (ImageSize);
                        m_ppAccumulatorRadianceMaps[i] = new TImage<CContinuousTristimulusPixel> (ImageSize);
                        m_ppAccumulatorWeightingMaps[i] = new TImage<CContinuousTristimulusPixel> (ImageSize);
                    }
                    Cameras::RadiometricCalibration::CMultiChannelRadiometricResponseFunction* pLeftMultiChannelRadiometricResponseFunction = ppMultiChannelRadiometricResponseFunction[0];
                    Cameras::RadiometricCalibration::CMultiChannelRadiometricResponseFunction* pRightMultiChannelRadiometricResponseFunction = ppMultiChannelRadiometricResponseFunction[1];
                    m_pLeftRedChannelRadiometricResponseFunction = pLeftMultiChannelRadiometricResponseFunction->GetRedChannelRadiometricResponseFunction();
                    m_pLeftGreenChannelRadiometricResponseFunction = pLeftMultiChannelRadiometricResponseFunction->GetGreenChannelRadiometricResponseFunction();
                    m_pLeftBlueChannelRadiometricResponseFunction = pLeftMultiChannelRadiometricResponseFunction->GetBlueChannelRadiometricResponseFunction();
                    m_pRightRedChannelRadiometricResponseFunction = pRightMultiChannelRadiometricResponseFunction->GetRedChannelRadiometricResponseFunction();
                    m_pRightGreenChannelRadiometricResponseFunction = pRightMultiChannelRadiometricResponseFunction->GetGreenChannelRadiometricResponseFunction();
                    m_pRightBlueChannelRadiometricResponseFunction = pRightMultiChannelRadiometricResponseFunction->GetBlueChannelRadiometricResponseFunction();
                    m_Paramters.m_SynthesisMode = _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SYNTHESIS_MODE_;
                    m_Paramters.m_WeightingMode = _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_WEIGHTING_MODE_;
                    m_Paramters.m_SigmaGaussianKernel = _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SIGMA_GAUSSIAN_KERNEL_;
                    m_Paramters.m_GaussianBoxIntensityA = _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_INTENSITY_A_GAUSSIAN_KERNEL_;
                    m_Paramters.m_GaussianBoxIntensityB = _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_INTENSITY_B_GAUSSIAN_KERNEL_;
                    m_Paramters.m_SigmaGaussianKernelA = _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SIGMA_A_GAUSSIAN_KERNEL_;
                    m_Paramters.m_SigmaGaussianKernelB = _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SIGMA_B_GAUSSIAN_KERNEL_;
                    m_Paramters.m_SamplesPerDiscreteIncrement = _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SAMPLES_PER_DISCRETE_INCREMENT_;
                    m_Paramters.m_MinimalIntegrationValue = _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_MINIMAL_INTEGRATION_VALUE_;
                    m_Paramters.m_MaximalIntegrationValue = _STEREOIMAGE_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_MAXIMAL_INTEGRATION_VALUE_;
                }
            }

            CStereoImageMultiChannelHDRSynthesizer::~CStereoImageMultiChannelHDRSynthesizer()
            {
                if (m_IsEnabled)
                    for (uint i = 0; i < _STEREO_; ++i)
                    {
                        delete m_ppOutputImages[i];
                        delete m_ppDisplayImages[i];
                        delete m_ppAccumulatorChromaticMaps[i];
                        delete m_ppAccumulatorRadianceMaps[i];
                        delete m_ppAccumulatorWeightingMaps[i];
                    }
            }

            void CStereoImageMultiChannelHDRSynthesizer::AddChromaticSample(real LeftWeight, real RightWeight)
            {
                const coordinate X0 = m_OutputActiveZone.GetX0();
                const coordinate Y0 = m_OutputActiveZone.GetY0();
                const coordinate X1 = m_OutputActiveZone.GetX1();
                const coordinate Y1 = m_OutputActiveZone.GetY1();
                const coordinate ImageWidth = m_OutputActiveZone.GetWidth();
                const CContinuousTristimulusPixel* pLeftBaseInputPixel = m_ppInputImages[0]->GetReadOnlyBufferAt(Y0, X0);
                const CContinuousTristimulusPixel* pRighBasetInputPixel = m_ppInputImages[1]->GetReadOnlyBufferAt(Y0, X0);
                CContinuousTristimulusPixel* pLeftBaseChromaticPixel = m_ppAccumulatorChromaticMaps[0]->GetWritableBufferAt(Y0, X0);
                CContinuousTristimulusPixel* pRightBaseChromaticPixel = m_ppAccumulatorChromaticMaps[1]->GetWritableBufferAt(Y0, X0);
                for (int y = Y0; y < Y1; ++y, pLeftBaseInputPixel += ImageWidth, pRighBasetInputPixel += ImageWidth, pLeftBaseChromaticPixel += ImageWidth, pRightBaseChromaticPixel += ImageWidth)
                {
                    const CContinuousTristimulusPixel* pLeftInputPixel = pLeftBaseInputPixel;
                    const CContinuousTristimulusPixel* pRightInputPixel = pRighBasetInputPixel;
                    CContinuousTristimulusPixel* pLeftChromaticPixel = pLeftBaseChromaticPixel;
                    CContinuousTristimulusPixel* pRightChromaticPixel = pRightBaseChromaticPixel;
                    for (int x = X0; x < X1; ++x, ++pLeftInputPixel, ++pRightInputPixel, ++pLeftChromaticPixel, ++pRightChromaticPixel)
                    {
                        pLeftChromaticPixel->AddWeightedValue(pLeftInputPixel, LeftWeight);
                        pRightChromaticPixel->AddWeightedValue(pRightInputPixel, RightWeight);
                    }
                }
                m_LeftWeightAccumulator += LeftWeight;
                m_RightWeightAccumulator += RightWeight;
            }

            void CStereoImageMultiChannelHDRSynthesizer::NormalizeChromaticSamples()
            {
                const coordinate X0 = m_OutputActiveZone.GetX0();
                const coordinate Y0 = m_OutputActiveZone.GetY0();
                const coordinate X1 = m_OutputActiveZone.GetX1();
                const coordinate Y1 = m_OutputActiveZone.GetY1();
                const coordinate ImageWidth = m_OutputActiveZone.GetWidth();
                const real LeftWeight = realInverse(m_LeftWeightAccumulator);
                const real RightWeight = realInverse(m_RightWeightAccumulator);
                CContinuousTristimulusPixel* pLeftBaseChromaticPixel = m_ppAccumulatorChromaticMaps[0]->GetWritableBufferAt(Y0, X0);
                CContinuousTristimulusPixel* pRightBaseChromaticPixel = m_ppAccumulatorChromaticMaps[1]->GetWritableBufferAt(Y0, X0);
                for (coordinate y = Y0; y < Y1; ++y, pLeftBaseChromaticPixel += ImageWidth, pRightBaseChromaticPixel += ImageWidth)
                {
                    CContinuousTristimulusPixel* pLeftChromaticPixel = pLeftBaseChromaticPixel;
                    CContinuousTristimulusPixel* pRightChromaticPixel = pRightBaseChromaticPixel;
                    for (coordinate x = X0; x < X1; ++x, ++pLeftChromaticPixel, ++pRightChromaticPixel)
                    {
                        pLeftChromaticPixel->Scale(LeftWeight);
                        pRightChromaticPixel->Scale(RightWeight);
                    }
                }
            }

            const bool CStereoImageMultiChannelHDRSynthesizer::AddExposureContinousMode(const Identifier TrialId, const real ShuttingTime)
            {
                if (m_IsEnabled)
                {
                    START_PROCESS_EXECUTION_LOG("AddExposureContinousMode", TrialId)

                        const int X0 = m_OutputActiveZone.GetX0();
                        const int Y0 = m_OutputActiveZone.GetY0();
                        const int X1 = m_OutputActiveZone.GetX1();
                        const int Y1 = m_OutputActiveZone.GetY1();
                        const int ImageWidth = m_OutputActiveZone.GetWidth();
                        const CContinuousTristimulusPixel* pLeftBaseInputPixel = m_ppInputImages[0]->GetReadOnlyBufferAt(Y0, X0);
                        const CContinuousTristimulusPixel* pRightBaseInputPixel = m_ppInputImages[1]->GetReadOnlyBufferAt(Y0, X0);
                        CContinuousTristimulusPixel* pLeftBaseAccumulatorRadiancePixel = m_ppAccumulatorRadianceMaps[0]->GetWritableBufferAt(Y0, X0);
                        CContinuousTristimulusPixel* pRightBaseAccumulatorRadiancePixel = m_ppAccumulatorRadianceMaps[1]->GetWritableBufferAt(Y0, X0);
                        CContinuousTristimulusPixel* pLeftBaseAccumulatorWeightingPixel = m_ppAccumulatorWeightingMaps[0]->GetWritableBufferAt(Y0, X0);
                        CContinuousTristimulusPixel* pRightBaseAccumulatorWeightingPixel = m_ppAccumulatorWeightingMaps[1]->GetWritableBufferAt(Y0, X0);
                        real LeftWeight = _REAL_ZERO_, RightWeight = _REAL_ZERO_, LogShuttingTime = RealLog(ShuttingTime);
                        switch (m_Paramters.m_WeightingMode)
                        {
                            case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eTriangularKernel:
                                for (int y = Y0; y < Y1; ++y, pLeftBaseInputPixel += ImageWidth, pRightBaseInputPixel += ImageWidth, pLeftBaseAccumulatorRadiancePixel += ImageWidth, pRightBaseAccumulatorRadiancePixel += ImageWidth, pLeftBaseAccumulatorWeightingPixel += ImageWidth, pRightBaseAccumulatorWeightingPixel += ImageWidth)
                                {
                                    const CContinuousTristimulusPixel* pLeftInputPixel = pLeftBaseInputPixel;
                                    const CContinuousTristimulusPixel* pRightInputPixel = pRightBaseInputPixel;
                                    CContinuousTristimulusPixel* pLeftAccumulatorRadiancePixel = pLeftBaseAccumulatorRadiancePixel;
                                    CContinuousTristimulusPixel* pRightAccumulatorRadiancePixel = pRightBaseAccumulatorRadiancePixel;
                                    CContinuousTristimulusPixel* pLeftAccumulatorWeightingPixel = pLeftBaseAccumulatorWeightingPixel;
                                    CContinuousTristimulusPixel* pRightAccumulatorWeightingPixel = pRightBaseAccumulatorWeightingPixel;
                                    for (int x = X0; x < X1; ++x, ++pLeftInputPixel, ++pRightInputPixel, ++pLeftAccumulatorWeightingPixel, ++pLeftAccumulatorRadiancePixel, ++pRightAccumulatorWeightingPixel, ++pRightAccumulatorRadiancePixel)
                                        for (int i = 0; i < 3; ++i)
                                        {
                                            if ((pLeftInputPixel->m_Element[i] < m_Paramters.m_MaximalIntegrationValue) && (pLeftInputPixel->m_Element[i] > m_LeftMinimalIntegrationValue.m_Element[i]))
                                            {
                                                real Weight = ContinuousTriangularWeigthingKernel(pLeftInputPixel->m_Element[i]);
                                                LeftWeight += Weight;
                                                pLeftAccumulatorWeightingPixel->m_Element[i] += Weight;
                                                pLeftAccumulatorRadiancePixel->m_Element[i] += Weight * (m_pLeftRedChannelRadiometricResponseFunction->GetResponseIdealFunction(pLeftInputPixel->m_Element[i]) - LogShuttingTime);
                                            }
                                            if ((pRightInputPixel->m_Element[i] < m_Paramters.m_MaximalIntegrationValue) && (pRightInputPixel->m_Element[i] > m_RightMinimalIntegrationValue.m_Element[i]))
                                            {
                                                real Weight = ContinuousTriangularWeigthingKernel(pRightInputPixel->m_Element[i]);
                                                RightWeight += Weight;
                                                pRightAccumulatorWeightingPixel->m_Element[i] += Weight;
                                                pRightAccumulatorRadiancePixel->m_Element[i] += Weight * (m_pRightRedChannelRadiometricResponseFunction->GetResponseIdealFunction(pRightInputPixel->m_Element[i]) - LogShuttingTime);
                                            }
                                        }

                                }
                                break;
                            case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eEpanechnikovKernel:
                                for (int y = Y0; y < Y1; ++y, pLeftBaseInputPixel += ImageWidth, pRightBaseInputPixel += ImageWidth, pLeftBaseAccumulatorRadiancePixel += ImageWidth, pRightBaseAccumulatorRadiancePixel += ImageWidth, pLeftBaseAccumulatorWeightingPixel += ImageWidth, pRightBaseAccumulatorWeightingPixel += ImageWidth)
                                {
                                    const CContinuousTristimulusPixel* pLeftInputPixel = pLeftBaseInputPixel;
                                    const CContinuousTristimulusPixel* pRightInputPixel = pRightBaseInputPixel;
                                    CContinuousTristimulusPixel* pLeftAccumulatorRadiancePixel = pLeftBaseAccumulatorRadiancePixel;
                                    CContinuousTristimulusPixel* pRightAccumulatorRadiancePixel = pRightBaseAccumulatorRadiancePixel;
                                    CContinuousTristimulusPixel* pLeftAccumulatorWeightingPixel = pLeftBaseAccumulatorWeightingPixel;
                                    CContinuousTristimulusPixel* pRightAccumulatorWeightingPixel = pRightBaseAccumulatorWeightingPixel;
                                    for (int x = X0; x < X1; ++x, ++pLeftInputPixel, ++pRightInputPixel, ++pLeftAccumulatorWeightingPixel, ++pLeftAccumulatorRadiancePixel, ++pRightAccumulatorWeightingPixel, ++pRightAccumulatorRadiancePixel)
                                        for (int i = 0; i < 3; ++i)
                                        {
                                            if ((pLeftInputPixel->m_Element[i] < m_Paramters.m_MaximalIntegrationValue) && (pLeftInputPixel->m_Element[i] > m_LeftMinimalIntegrationValue.m_Element[i]))
                                            {
                                                real Weight = ContinuousEpanechnikovWeigthingKernel(pLeftInputPixel->m_Element[i]);
                                                LeftWeight += Weight;
                                                pLeftAccumulatorWeightingPixel->m_Element[i] += Weight;
                                                pLeftAccumulatorRadiancePixel->m_Element[i] += Weight * (m_pLeftRedChannelRadiometricResponseFunction->GetResponseIdealFunction(pLeftInputPixel->m_Element[i]) - LogShuttingTime);
                                            }
                                            if ((pRightInputPixel->m_Element[i] < m_Paramters.m_MaximalIntegrationValue) && (pRightInputPixel->m_Element[i] > m_RightMinimalIntegrationValue.m_Element[i]))
                                            {
                                                real Weight = ContinuousEpanechnikovWeigthingKernel(pRightInputPixel->m_Element[i]);
                                                RightWeight += Weight;
                                                pRightAccumulatorWeightingPixel->m_Element[i] += Weight;
                                                pRightAccumulatorRadiancePixel->m_Element[i] += Weight * (m_pRightRedChannelRadiometricResponseFunction->GetResponseIdealFunction(pRightInputPixel->m_Element[i]) - LogShuttingTime);
                                            }
                                        }
                                }
                                break;
                            case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eGaussianKernel:
                            {
                                const real ExponentFactor = GaussianExponentFactor(m_Paramters.m_SigmaGaussianKernel);
                                for (int y = Y0; y < Y1; ++y, pLeftBaseInputPixel += ImageWidth, pRightBaseInputPixel += ImageWidth, pLeftBaseAccumulatorRadiancePixel += ImageWidth, pRightBaseAccumulatorRadiancePixel += ImageWidth, pLeftBaseAccumulatorWeightingPixel += ImageWidth, pRightBaseAccumulatorWeightingPixel += ImageWidth)
                                {
                                    const CContinuousTristimulusPixel* pLeftInputPixel = pLeftBaseInputPixel;
                                    const CContinuousTristimulusPixel* pRightInputPixel = pRightBaseInputPixel;
                                    CContinuousTristimulusPixel* pLeftAccumulatorRadiancePixel = pLeftBaseAccumulatorRadiancePixel;
                                    CContinuousTristimulusPixel* pRightAccumulatorRadiancePixel = pRightBaseAccumulatorRadiancePixel;
                                    CContinuousTristimulusPixel* pLeftAccumulatorWeightingPixel = pLeftBaseAccumulatorWeightingPixel;
                                    CContinuousTristimulusPixel* pRightAccumulatorWeightingPixel = pRightBaseAccumulatorWeightingPixel;
                                    for (int x = X0; x < X1; ++x, ++pLeftInputPixel, ++pRightInputPixel, ++pLeftAccumulatorWeightingPixel, ++pLeftAccumulatorRadiancePixel, ++pRightAccumulatorWeightingPixel, ++pRightAccumulatorRadiancePixel)
                                        for (int i = 0; i < 3; ++i)
                                        {
                                            if ((pLeftInputPixel->m_Element[i] < m_Paramters.m_MaximalIntegrationValue) && (pLeftInputPixel->m_Element[i] > m_LeftMinimalIntegrationValue.m_Element[i]))
                                            {
                                                real Weight = ContinuousGaussianWeigthingKernel(pLeftInputPixel->m_Element[i], ExponentFactor);
                                                LeftWeight += Weight;
                                                pLeftAccumulatorWeightingPixel->m_Element[i] += Weight;
                                                pLeftAccumulatorRadiancePixel->m_Element[i] += Weight * (m_pLeftRedChannelRadiometricResponseFunction->GetResponseIdealFunction(pLeftInputPixel->m_Element[i]) - LogShuttingTime);
                                            }
                                            if ((pRightInputPixel->m_Element[i] < m_Paramters.m_MaximalIntegrationValue) && (pRightInputPixel->m_Element[i] > m_RightMinimalIntegrationValue.m_Element[i]))
                                            {
                                                real Weight = ContinuousGaussianWeigthingKernel(pRightInputPixel->m_Element[i], ExponentFactor);
                                                RightWeight += Weight;
                                                pRightAccumulatorWeightingPixel->m_Element[i] += Weight;
                                                pRightAccumulatorRadiancePixel->m_Element[i] += Weight * (m_pRightRedChannelRadiometricResponseFunction->GetResponseIdealFunction(pRightInputPixel->m_Element[i]) - LogShuttingTime);
                                            }
                                        }
                                }
                            }
                                break;
                            case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eGaussianBoxKernel:
                            {
                                const real ExponentFactorA = GaussianExponentFactor(m_Paramters.m_SigmaGaussianKernelA);
                                const real ExponentFactorB = GaussianExponentFactor(m_Paramters.m_SigmaGaussianKernelB);
                                for (int y = Y0; y < Y1; ++y, pLeftBaseInputPixel += ImageWidth, pRightBaseInputPixel += ImageWidth, pLeftBaseAccumulatorRadiancePixel += ImageWidth, pRightBaseAccumulatorRadiancePixel += ImageWidth, pLeftBaseAccumulatorWeightingPixel += ImageWidth, pRightBaseAccumulatorWeightingPixel += ImageWidth)
                                {
                                    const CContinuousTristimulusPixel* pLeftInputPixel = pLeftBaseInputPixel;
                                    const CContinuousTristimulusPixel* pRightInputPixel = pRightBaseInputPixel;
                                    CContinuousTristimulusPixel* pLeftAccumulatorRadiancePixel = pLeftBaseAccumulatorRadiancePixel;
                                    CContinuousTristimulusPixel* pRightAccumulatorRadiancePixel = pRightBaseAccumulatorRadiancePixel;
                                    CContinuousTristimulusPixel* pLeftAccumulatorWeightingPixel = pLeftBaseAccumulatorWeightingPixel;
                                    CContinuousTristimulusPixel* pRightAccumulatorWeightingPixel = pRightBaseAccumulatorWeightingPixel;

                                    for (int x = X0; x < X1; ++x, ++pLeftInputPixel, ++pRightInputPixel, ++pLeftAccumulatorWeightingPixel, ++pLeftAccumulatorRadiancePixel, ++pRightAccumulatorWeightingPixel, ++pRightAccumulatorRadiancePixel)
                                        for (int i = 0; i < 3; ++i)
                                        {
                                            if ((pLeftInputPixel->m_Element[i] < m_Paramters.m_MaximalIntegrationValue) && (pLeftInputPixel->m_Element[i] > m_LeftMinimalIntegrationValue.m_Element[i]))
                                            {
                                                real Weight = ContinuousGaussianBoxWeigthingKernel(pLeftInputPixel->m_Element[i], m_Paramters.m_GaussianBoxIntensityA, m_Paramters.m_GaussianBoxIntensityB, ExponentFactorA, ExponentFactorB);
                                                LeftWeight += Weight;
                                                pLeftAccumulatorWeightingPixel->m_Element[i] += Weight;
                                                pLeftAccumulatorRadiancePixel->m_Element[i] += Weight * (m_pLeftRedChannelRadiometricResponseFunction->GetResponseIdealFunction(pLeftInputPixel->m_Element[i]) - LogShuttingTime);
                                            }
                                            if ((pRightInputPixel->m_Element[i] < m_Paramters.m_MaximalIntegrationValue) && (pRightInputPixel->m_Element[i] > m_RightMinimalIntegrationValue.m_Element[i]))
                                            {
                                                real Weight = ContinuousGaussianBoxWeigthingKernel(pRightInputPixel->m_Element[i], m_Paramters.m_GaussianBoxIntensityA, m_Paramters.m_GaussianBoxIntensityB, ExponentFactorA, ExponentFactorB);
                                                RightWeight += Weight;
                                                pRightAccumulatorWeightingPixel->m_Element[i] += Weight;
                                                pRightAccumulatorRadiancePixel->m_Element[i] += Weight * (m_pRightRedChannelRadiometricResponseFunction->GetResponseIdealFunction(pRightInputPixel->m_Element[i]) - LogShuttingTime);
                                            }
                                        }
                                }
                            }
                                break;
                        }
                        AddChromaticSample(LeftWeight, RightWeight);
                        ++m_TotalExposures;

                    STOP_PROCESS_EXECUTION_LOG
                    return true;
                }
                return false;
            }

            const bool CStereoImageMultiChannelHDRSynthesizer::AddExposureLinealInterpolatedMode(const Identifier TrialId, const real ShuttingTime)
            {
                if (m_IsEnabled && m_IsSampledResponseIdealFunctionLoaded)
                {
                    START_PROCESS_EXECUTION_LOG("AddExposureLinealInterpolatedMode", TrialId)

                        const int X0 = m_OutputActiveZone.GetX0();
                        const int Y0 = m_OutputActiveZone.GetY0();
                        const int X1 = m_OutputActiveZone.GetX1();
                        const int Y1 = m_OutputActiveZone.GetY1();
                        const int ImageWidth = m_OutputActiveZone.GetWidth();
                        const CContinuousTristimulusPixel* pLeftBaseInputPixel = m_ppInputImages[0]->GetReadOnlyBufferAt(Y0, X0);
                        const CContinuousTristimulusPixel* pRightBaseInputPixel = m_ppInputImages[1]->GetReadOnlyBufferAt(Y0, X0);
                        CContinuousTristimulusPixel* pLeftBaseAccumulatorRadiancePixel = m_ppAccumulatorRadianceMaps[0]->GetWritableBufferAt(Y0, X0);
                        CContinuousTristimulusPixel* pRightBaseAccumulatorRadiancePixel = m_ppAccumulatorRadianceMaps[1]->GetWritableBufferAt(Y0, X0);
                        CContinuousTristimulusPixel* pLeftBaseAccumulatorWeightingPixel = m_ppAccumulatorWeightingMaps[0]->GetWritableBufferAt(Y0, X0);
                        CContinuousTristimulusPixel* pRightBaseAccumulatorWeightingPixel = m_ppAccumulatorWeightingMaps[1]->GetWritableBufferAt(Y0, X0);
                        real LeftWeight = _REAL_ZERO_, RightWeight = _REAL_ZERO_, LogShuttingTime = RealLog(ShuttingTime);
                        for (int y = Y0; y < Y1; ++y, pLeftBaseInputPixel += ImageWidth, pRightBaseInputPixel += ImageWidth, pLeftBaseAccumulatorRadiancePixel += ImageWidth, pRightBaseAccumulatorRadiancePixel += ImageWidth, pLeftBaseAccumulatorWeightingPixel += ImageWidth, pRightBaseAccumulatorWeightingPixel += ImageWidth)
                        {
                            const CContinuousTristimulusPixel* pLeftInputPixel = pLeftBaseInputPixel;
                            const CContinuousTristimulusPixel* pRightInputPixel = pRightBaseInputPixel;
                            CContinuousTristimulusPixel* pLeftAccumulatorRadiancePixel = pLeftBaseAccumulatorRadiancePixel;
                            CContinuousTristimulusPixel* pRightAccumulatorRadiancePixel = pRightBaseAccumulatorRadiancePixel;
                            CContinuousTristimulusPixel* pLeftAccumulatorWeightingPixel = pLeftBaseAccumulatorWeightingPixel;
                            CContinuousTristimulusPixel* pRightAccumulatorWeightingPixel = pRightBaseAccumulatorWeightingPixel;

                            for (int x = X0; x < X1; ++x, ++pLeftInputPixel, ++pRightInputPixel, ++pLeftAccumulatorWeightingPixel, ++pLeftAccumulatorRadiancePixel, ++pRightAccumulatorWeightingPixel, ++pRightAccumulatorRadiancePixel)
                                for (int i = 0; i < 3; ++i)
                                {
                                    if ((pLeftInputPixel->m_Element[i] < m_Paramters.m_MaximalIntegrationValue) && (pLeftInputPixel->m_Element[i] > m_LeftMinimalIntegrationValue.m_Element[i]))
                                    {
                                        real Weight = m_pLeftRedChannelRadiometricResponseFunction->GetLinealInterpolatedWeigthingKernel(pLeftInputPixel->m_Element[i]);
                                        LeftWeight += Weight;
                                        pLeftAccumulatorWeightingPixel->m_Element[i] += Weight;
                                        pLeftAccumulatorRadiancePixel->m_Element[i] += Weight * (m_pLeftRedChannelRadiometricResponseFunction->GetLinealInterpolatedResponseIdealFunction(pLeftInputPixel->m_Element[i]) - LogShuttingTime);
                                    }
                                    if ((pRightInputPixel->m_Element[i] < m_Paramters.m_MaximalIntegrationValue) && (pRightInputPixel->m_Element[i] > m_RightMinimalIntegrationValue.m_Element[i]))
                                    {
                                        real Weight = m_pRightRedChannelRadiometricResponseFunction->GetLinealInterpolatedWeigthingKernel(pRightInputPixel->m_Element[i]);
                                        RightWeight += Weight;
                                        pRightAccumulatorWeightingPixel->m_Element[i] += Weight;
                                        pRightAccumulatorRadiancePixel->m_Element[i] += Weight * (m_pRightRedChannelRadiometricResponseFunction->GetLinealInterpolatedResponseIdealFunction(pRightInputPixel->m_Element[i]) - LogShuttingTime);
                                    }
                                }

                        }
                        AddChromaticSample(LeftWeight, RightWeight);
                        ++m_TotalExposures;

                    STOP_PROCESS_EXECUTION_LOG

                    return true;
                }
                return false;
            }

            const bool CStereoImageMultiChannelHDRSynthesizer::AddExposureSampledMode(const Identifier TrialId, const real ShuttingTime)
            {
                if (m_IsEnabled && m_IsSampledResponseIdealFunctionLoaded)
                {
                    START_PROCESS_EXECUTION_LOG("AddExposureSampledMode", TrialId)

                        const int X0 = m_OutputActiveZone.GetX0();
                        const int Y0 = m_OutputActiveZone.GetY0();
                        const int X1 = m_OutputActiveZone.GetX1();
                        const int Y1 = m_OutputActiveZone.GetY1();
                        const int ImageWidth = m_OutputActiveZone.GetWidth();
                        const CContinuousTristimulusPixel* pLeftBaseInputPixel = m_ppInputImages[0]->GetReadOnlyBufferAt(Y0, X0);
                        const CContinuousTristimulusPixel* pRightBaseInputPixel = m_ppInputImages[1]->GetReadOnlyBufferAt(Y0, X0);
                        CContinuousTristimulusPixel* pLeftBaseAccumulatorRadiancePixel = m_ppAccumulatorRadianceMaps[0]->GetWritableBufferAt(Y0, X0);
                        CContinuousTristimulusPixel* pRightBaseAccumulatorRadiancePixel = m_ppAccumulatorRadianceMaps[1]->GetWritableBufferAt(Y0, X0);
                        CContinuousTristimulusPixel* pLeftBaseAccumulatorWeightingPixel = m_ppAccumulatorWeightingMaps[0]->GetWritableBufferAt(Y0, X0);
                        CContinuousTristimulusPixel* pRightBaseAccumulatorWeightingPixel = m_ppAccumulatorWeightingMaps[1]->GetWritableBufferAt(Y0, X0);
                        real LeftWeight = _REAL_ZERO_, RightWeight = _REAL_ZERO_, LogShuttingTime = RealLog(ShuttingTime);
                        for (int y = Y0; y < Y1; ++y, pLeftBaseInputPixel += ImageWidth, pRightBaseInputPixel += ImageWidth, pLeftBaseAccumulatorRadiancePixel += ImageWidth, pRightBaseAccumulatorRadiancePixel += ImageWidth, pLeftBaseAccumulatorWeightingPixel += ImageWidth, pRightBaseAccumulatorWeightingPixel += ImageWidth)
                        {
                            const CContinuousTristimulusPixel* pLeftInputPixel = pLeftBaseInputPixel;
                            const CContinuousTristimulusPixel* pRightInputPixel = pRightBaseInputPixel;
                            CContinuousTristimulusPixel* pLeftAccumulatorRadiancePixel = pLeftBaseAccumulatorRadiancePixel;
                            CContinuousTristimulusPixel* pRightAccumulatorRadiancePixel = pRightBaseAccumulatorRadiancePixel;
                            CContinuousTristimulusPixel* pLeftAccumulatorWeightingPixel = pLeftBaseAccumulatorWeightingPixel;
                            CContinuousTristimulusPixel* pRightAccumulatorWeightingPixel = pRightBaseAccumulatorWeightingPixel;
                            for (int x = X0; x < X1; ++x, ++pLeftInputPixel, ++pRightInputPixel, ++pLeftAccumulatorWeightingPixel, ++pLeftAccumulatorRadiancePixel, ++pRightAccumulatorWeightingPixel, ++pRightAccumulatorRadiancePixel)
                                for (int i = 0; i < 3; ++i)
                                {
                                    if ((pLeftInputPixel->m_Element[i] < m_Paramters.m_MaximalIntegrationValue) && (pLeftInputPixel->m_Element[i] > m_LeftMinimalIntegrationValue.m_Element[i]))
                                    {
                                        int Index = RoundToInteger(m_SamplesPerDiscreteIncrement * pLeftInputPixel->m_Element[i]);
                                        real Weight = m_pLeftRedWeigthingKernel[Index];
                                        LeftWeight += Weight;
                                        pLeftAccumulatorWeightingPixel->m_Element[i] += Weight;
                                        pLeftAccumulatorRadiancePixel->m_Element[i] += Weight * (m_pLeftRedSampledResponseIdealFunction[Index] - LogShuttingTime);
                                    }
                                    if ((pRightInputPixel->m_Element[i] < m_Paramters.m_MaximalIntegrationValue) && (pRightInputPixel->m_Element[i] > m_RightMinimalIntegrationValue.m_Element[i]))
                                    {
                                        int Index = RoundToInteger(m_SamplesPerDiscreteIncrement * pRightInputPixel->m_Element[i]);
                                        real Weight = m_pRightRedWeigthingKernel[Index];
                                        RightWeight += Weight;
                                        pRightAccumulatorWeightingPixel->m_Element[i] += Weight;
                                        pRightAccumulatorRadiancePixel->m_Element[i] += Weight * (m_pRightRedSampledResponseIdealFunction[Index] - LogShuttingTime);
                                    }
                                }
                        }
                        AddChromaticSample(LeftWeight, RightWeight);
                        ++m_TotalExposures;

                    STOP_PROCESS_EXECUTION_LOG

                    return true;
                }
                return false;
            }

            const bool CStereoImageMultiChannelHDRSynthesizer::Synthesis(const Identifier TrialId)
            {
                if (m_IsEnabled && (m_TotalExposures > 1))
                {
                    START_PROCESS_EXECUTION_LOG("Synthesis", TrialId)

                        m_ppOutputImages[0]->ClearDensityPrimitives();
                        m_ppOutputImages[1]->ClearDensityPrimitives();
                        const int X0 = m_OutputActiveZone.GetX0();
                        const int Y0 = m_OutputActiveZone.GetY0();
                        const int X1 = m_OutputActiveZone.GetX1();
                        const int Y1 = m_OutputActiveZone.GetY1();
                        const int ImageWidth = m_OutputActiveZone.GetWidth();
                        const CContinuousTristimulusPixel* pLeftBaseAccumulatorRadiancePixel = m_ppAccumulatorRadianceMaps[0]->GetReadOnlyBufferAt(Y0, X0);
                        const CContinuousTristimulusPixel* pRightBaseAccumulatorRadiancePixel = m_ppAccumulatorRadianceMaps[1]->GetReadOnlyBufferAt(Y0, X0);
                        const CContinuousTristimulusPixel* pLeftBaseAccumulatorWeightingPixel = m_ppAccumulatorWeightingMaps[0]->GetReadOnlyBufferAt(Y0, X0);
                        const CContinuousTristimulusPixel* pRightBaseAccumulatorWeightingPixel = m_ppAccumulatorWeightingMaps[1]->GetReadOnlyBufferAt(Y0, X0);
                        CContinuousTristimulusPixel* pLeftBaseOutputPixel = m_ppOutputImages[0]->GetWritableBufferAt(Y0, X0);
                        CContinuousTristimulusPixel* pRightBaseOutputPixel = m_ppOutputImages[1]->GetWritableBufferAt(Y0, X0);
                        switch (m_Paramters.m_SynthesisMode)
                        {
                            case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eHomomorphic:
                                for (int y = Y0; y < Y1; ++y, pLeftBaseOutputPixel += ImageWidth, pRightBaseOutputPixel += ImageWidth, pLeftBaseAccumulatorRadiancePixel += ImageWidth, pRightBaseAccumulatorRadiancePixel += ImageWidth, pLeftBaseAccumulatorWeightingPixel += ImageWidth, pRightBaseAccumulatorWeightingPixel += ImageWidth)
                                {
                                    const CContinuousTristimulusPixel* pLeftAccumulatorRadiancePixel = pLeftBaseAccumulatorRadiancePixel;
                                    const CContinuousTristimulusPixel* pRightAccumulatorRadiancePixel = pRightBaseAccumulatorRadiancePixel;
                                    const CContinuousTristimulusPixel* pLeftAccumulatorWeightingPixel = pLeftBaseAccumulatorWeightingPixel;
                                    const CContinuousTristimulusPixel* pRightAccumulatorWeightingPixel = pRightBaseAccumulatorWeightingPixel;
                                    CContinuousTristimulusPixel* pLeftOutputPixel = pLeftBaseOutputPixel;
                                    CContinuousTristimulusPixel* pRightOutputPixel = pRightBaseOutputPixel;
                                    for (int x = X0; x < X1; ++x, ++pLeftOutputPixel, ++pLeftAccumulatorRadiancePixel, ++pLeftAccumulatorWeightingPixel, ++pRightOutputPixel, ++pRightAccumulatorRadiancePixel, ++pRightAccumulatorWeightingPixel)
                                        for (int i = 0; i < 3; ++i)
                                        {
                                            if (pLeftAccumulatorWeightingPixel->m_Element[i] > _REAL_EPSILON_)
                                                pLeftOutputPixel->m_Element[i] = pLeftAccumulatorRadiancePixel->m_Element[i] / pLeftAccumulatorWeightingPixel->m_Element[i];
                                            if (pRightAccumulatorWeightingPixel->m_Element[i] > _REAL_EPSILON_)
                                                pRightOutputPixel->m_Element[i] = pRightAccumulatorRadiancePixel->m_Element[i] / pRightAccumulatorWeightingPixel->m_Element[i];
                                        }

                                }
                                break;
                            case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::elinear:
                                for (int y = Y0; y < Y1; ++y, pLeftBaseOutputPixel += ImageWidth, pRightBaseOutputPixel += ImageWidth, pLeftBaseAccumulatorRadiancePixel += ImageWidth, pRightBaseAccumulatorRadiancePixel += ImageWidth, pLeftBaseAccumulatorWeightingPixel += ImageWidth, pRightBaseAccumulatorWeightingPixel += ImageWidth)
                                {
                                    const CContinuousTristimulusPixel* pLeftAccumulatorRadiancePixel = pLeftBaseAccumulatorRadiancePixel;
                                    const CContinuousTristimulusPixel* pRightAccumulatorRadiancePixel = pRightBaseAccumulatorRadiancePixel;
                                    const CContinuousTristimulusPixel* pLeftAccumulatorWeightingPixel = pLeftBaseAccumulatorWeightingPixel;
                                    const CContinuousTristimulusPixel* pRightAccumulatorWeightingPixel = pRightBaseAccumulatorWeightingPixel;
                                    CContinuousTristimulusPixel* pLeftOutputPixel = pLeftBaseOutputPixel;
                                    CContinuousTristimulusPixel* pRightOutputPixel = pRightBaseOutputPixel;
                                    for (int x = X0; x < X1; ++x, ++pLeftOutputPixel, ++pLeftAccumulatorRadiancePixel, ++pLeftAccumulatorWeightingPixel, ++pRightOutputPixel, ++pRightAccumulatorRadiancePixel, ++pRightAccumulatorWeightingPixel)
                                        for (int i = 0; i < 3; ++i)
                                        {
                                            if (pLeftAccumulatorWeightingPixel->m_Element[i] > _REAL_EPSILON_)
                                                pLeftOutputPixel->m_Element[i] = RealExp(pLeftAccumulatorRadiancePixel->m_Element[i] / pLeftAccumulatorWeightingPixel->m_Element[i]);
                                            if (pRightAccumulatorWeightingPixel->m_Element[i] > _REAL_EPSILON_)
                                                pRightOutputPixel->m_Element[i] = RealExp(pRightAccumulatorRadiancePixel->m_Element[i] / pRightAccumulatorWeightingPixel->m_Element[i]);
                                        }

                                }
                                break;
                        }
                        NormalizeChromaticSamples();

                    STOP_PROCESS_EXECUTION_LOG

                    return true;
                }
                return false;
            }

            void CStereoImageMultiChannelHDRSynthesizer::ClearExposures()
            {
                m_TotalExposures = 0;
                m_RightWeightAccumulator = m_LeftWeightAccumulator = _REAL_ZERO_;
                for (uint i = 0; i < _STEREO_; ++i)
                {
                    m_ppAccumulatorChromaticMaps[i]->ClearDensityPrimitives();
                    m_ppAccumulatorRadianceMaps[i]->ClearDensityPrimitives();
                    m_ppAccumulatorWeightingMaps[i]->ClearDensityPrimitives();
                }
            }

            uint CStereoImageMultiChannelHDRSynthesizer::GetTotalExposures()
            {
                return m_TotalExposures;
            }

            TImage<CContinuousTristimulusPixel>** CStereoImageMultiChannelHDRSynthesizer::GetAccumulatorRadianceMaps()
            {
                return m_ppAccumulatorRadianceMaps;
            }

            TImage<CContinuousTristimulusPixel>** CStereoImageMultiChannelHDRSynthesizer::GetAccumulatorWeightingMaps()
            {
                return m_ppAccumulatorWeightingMaps;
            }

            TImage<CContinuousTristimulusPixel>** CStereoImageMultiChannelHDRSynthesizer::GetChromaticMaps()
            {
                return m_ppAccumulatorChromaticMaps;
            }

            CStereoImageMultiChannelHDRSynthesizer::Paramters* CStereoImageMultiChannelHDRSynthesizer::GetParamters()
            {
                return &m_Paramters;
            }

            bool CStereoImageMultiChannelHDRSynthesizer::Display(DisplayType Type)
            {
                if (m_IsEnabled)
                    switch (Type)
                    {
                        case eAccumulatorRadianceMaps:
                            return CStereoImageExporter::ExportNormalizing(&m_OutputActiveZone, const_cast<const TImage<CContinuousTristimulusPixel>**> (m_ppAccumulatorRadianceMaps), m_ppDisplayImages);
                            break;
                        case eAccumulatorWeightingMaps:
                            return CStereoImageExporter::ExportNormalizing(&m_OutputActiveZone, const_cast<const TImage<CContinuousTristimulusPixel>**> (m_ppAccumulatorWeightingMaps), m_ppDisplayImages);
                            break;
                        case eRadianceMaps:
                            return CStereoImageExporter::ExportNormalizing(&m_OutputActiveZone, GetOutputImagesReadOnly(), m_ppDisplayImages);
                            break;
                    }
                return false;
            }

            bool CStereoImageMultiChannelHDRSynthesizer::GetChannelsDescriptiveStatistics(ChannelDescriptiveStatistics& LeftChannelDescriptiveStatistics, ChannelDescriptiveStatistics& RightChannelDescriptiveStatistics)
            {
                if (m_IsEnabled)
                {
                    const int X0 = m_OutputActiveZone.GetX0();
                    const int Y0 = m_OutputActiveZone.GetY0();
                    const int X1 = m_OutputActiveZone.GetX1();
                    const int Y1 = m_OutputActiveZone.GetY1();
                    const int ImageWidth = m_OutputActiveZone.GetWidth();
                    const CContinuousTristimulusPixel* pLeftBaseOutputPixel = m_ppOutputImages[0]->GetWritableBufferAt(Y0, X0);
                    const CContinuousTristimulusPixel* pRightBaseOutputPixel = m_ppOutputImages[1]->GetWritableBufferAt(Y0, X0);
                    LeftChannelDescriptiveStatistics.m_MaximalIntensity = LeftChannelDescriptiveStatistics.m_MinimalIntensity = *pLeftBaseOutputPixel;
                    RightChannelDescriptiveStatistics.m_MaximalIntensity = RightChannelDescriptiveStatistics.m_MinimalIntensity = *pRightBaseOutputPixel;
                    for (int y = Y0; y < Y1; ++y, pLeftBaseOutputPixel += ImageWidth, pRightBaseOutputPixel += ImageWidth)
                    {
                        const CContinuousTristimulusPixel* pLeftOutputPixel = pLeftBaseOutputPixel;
                        const CContinuousTristimulusPixel* pRightOutputPixel = pRightBaseOutputPixel;
                        for (int x = X0; x < X1; ++x, ++pLeftOutputPixel, ++pRightOutputPixel)
                            for (int i = 0; i < 3; ++i)
                            {
                                LeftChannelDescriptiveStatistics.m_Mean.m_Element[i] += pLeftOutputPixel->m_Element[i];
                                if (pLeftOutputPixel->m_Element[i] > LeftChannelDescriptiveStatistics.m_MaximalIntensity.m_Element[i])
                                    LeftChannelDescriptiveStatistics.m_MaximalIntensity.m_Element[i] = pLeftOutputPixel->m_Element[i];
                                else if (pLeftOutputPixel->m_Element[i] < LeftChannelDescriptiveStatistics.m_MinimalIntensity.m_Element[i])
                                    LeftChannelDescriptiveStatistics.m_MinimalIntensity.m_Element[i] = pLeftOutputPixel->m_Element[i];
                                RightChannelDescriptiveStatistics.m_Mean.m_Element[i] += pRightOutputPixel->m_Element[i];
                                if (pRightOutputPixel->m_Element[i] > RightChannelDescriptiveStatistics.m_MaximalIntensity.m_Element[i])
                                    RightChannelDescriptiveStatistics.m_MaximalIntensity.m_Element[i] = pRightOutputPixel->m_Element[i];
                                else if (pRightOutputPixel->m_Element[i] < RightChannelDescriptiveStatistics.m_MinimalIntensity.m_Element[i])
                                    RightChannelDescriptiveStatistics.m_MinimalIntensity.m_Element[i] = pRightOutputPixel->m_Element[i];
                            }
                    }
                    const real ScaleFactor = realInverse(real(m_OutputActiveZone.GetActiveArea()));

                    for (int i = 0; i < 3; ++i)
                    {
                        LeftChannelDescriptiveStatistics.m_Mean.m_Element[i] *= ScaleFactor;
                        RightChannelDescriptiveStatistics.m_Mean.m_Element[i] *= ScaleFactor;
                        LeftChannelDescriptiveStatistics.m_Range.m_Element[i] = LeftChannelDescriptiveStatistics.m_MaximalIntensity.m_Element[i] - LeftChannelDescriptiveStatistics.m_MinimalIntensity.m_Element[i];
                        RightChannelDescriptiveStatistics.m_Range.m_Element[i] = RightChannelDescriptiveStatistics.m_MaximalIntensity.m_Element[i] - RightChannelDescriptiveStatistics.m_MinimalIntensity.m_Element[i];
                    }
                    real LeftMaximalChannelLinealRange = _REAL_ZERO_, RightMaximalChannelLinealRange = _REAL_ZERO_;
                    if (m_Paramters.m_SynthesisMode == Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eHomomorphic)
                        for (int i = 0; i < 3; ++i)
                        {
                            real ChannelLinealRange = RealExp(LeftChannelDescriptiveStatistics.m_MaximalIntensity.m_Element[i]) - RealExp(LeftChannelDescriptiveStatistics.m_MaximalIntensity.m_Element[i]);
                            if (ChannelLinealRange > LeftMaximalChannelLinealRange)
                                LeftMaximalChannelLinealRange = ChannelLinealRange;
                            ChannelLinealRange = RealExp(LeftChannelDescriptiveStatistics.m_MaximalIntensity.m_Element[i]) - RealExp(LeftChannelDescriptiveStatistics.m_MaximalIntensity.m_Element[i]);
                            if (ChannelLinealRange > RightMaximalChannelLinealRange)
                                RightMaximalChannelLinealRange = ChannelLinealRange;
                        }
                    else
                        for (int i = 0; i < 3; ++i)
                        {
                            real ChannelLinealRange = LeftChannelDescriptiveStatistics.m_MaximalIntensity.m_Element[i] - LeftChannelDescriptiveStatistics.m_MaximalIntensity.m_Element[i];
                            if (ChannelLinealRange > LeftMaximalChannelLinealRange)
                                LeftMaximalChannelLinealRange = ChannelLinealRange;
                            ChannelLinealRange = RightChannelDescriptiveStatistics.m_MaximalIntensity.m_Element[i] - RightChannelDescriptiveStatistics.m_MaximalIntensity.m_Element[i];
                            if (ChannelLinealRange > RightMaximalChannelLinealRange)
                                RightMaximalChannelLinealRange = ChannelLinealRange;
                        }
                    LeftChannelDescriptiveStatistics.m_EncondingBits = UpperToInteger(RealLog2(RealCeil(LeftMaximalChannelLinealRange)));
                    RightChannelDescriptiveStatistics.m_EncondingBits = UpperToInteger(RealLog2(RealCeil(RightMaximalChannelLinealRange)));

                    return true;
                }
                return false;
            }

            bool CStereoImageMultiChannelHDRSynthesizer::LoadIdealSampledResponseFunction()
            {
                if (m_IsEnabled)
                {
                    bool Result = false;
                    switch (m_Paramters.m_WeightingMode)
                    {
                        case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eTriangularKernel:
                        {
                            Result = m_pLeftRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode);
                            Result &= m_pLeftGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode);
                            Result &= m_pLeftBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode);
                            Result &= m_pRightRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode);
                            Result &= m_pRightGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode);
                            Result &= m_pRightBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode);
                        }
                            break;
                        case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eEpanechnikovKernel:
                        {
                            Result = m_pLeftRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode);
                            Result &= m_pLeftGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode);
                            Result &= m_pLeftBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode);
                            Result &= m_pRightRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode);
                            Result &= m_pRightGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode);
                            Result &= m_pRightBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode);
                        }
                            break;
                        case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eGaussianKernel:
                        {
                            Result = m_pLeftRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode, m_Paramters.m_SigmaGaussianKernel);
                            Result &= m_pLeftGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode, m_Paramters.m_SigmaGaussianKernel);
                            Result &= m_pLeftBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode, m_Paramters.m_SigmaGaussianKernel);
                            Result &= m_pRightRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode, m_Paramters.m_SigmaGaussianKernel);
                            Result &= m_pRightGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode, m_Paramters.m_SigmaGaussianKernel);
                            Result &= m_pRightBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode, m_Paramters.m_SigmaGaussianKernel);
                        }
                            break;
                        case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eGaussianBoxKernel:
                        {
                            Result = m_pLeftRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode, m_Paramters.m_GaussianBoxIntensityA, m_Paramters.m_GaussianBoxIntensityB, m_Paramters.m_SigmaGaussianKernelA, m_Paramters.m_SigmaGaussianKernelB);
                            Result &= m_pLeftGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode, m_Paramters.m_GaussianBoxIntensityA, m_Paramters.m_GaussianBoxIntensityB, m_Paramters.m_SigmaGaussianKernelA, m_Paramters.m_SigmaGaussianKernelB);
                            Result &= m_pLeftBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode, m_Paramters.m_GaussianBoxIntensityA, m_Paramters.m_GaussianBoxIntensityB, m_Paramters.m_SigmaGaussianKernelA, m_Paramters.m_SigmaGaussianKernelB);
                            Result &= m_pRightRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode, m_Paramters.m_GaussianBoxIntensityA, m_Paramters.m_GaussianBoxIntensityB, m_Paramters.m_SigmaGaussianKernelA, m_Paramters.m_SigmaGaussianKernelB);
                            Result &= m_pRightGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode, m_Paramters.m_GaussianBoxIntensityA, m_Paramters.m_GaussianBoxIntensityB, m_Paramters.m_SigmaGaussianKernelA, m_Paramters.m_SigmaGaussianKernelB);
                            Result &= m_pRightBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(m_Paramters.m_SamplesPerDiscreteIncrement, m_Paramters.m_WeightingMode, m_Paramters.m_GaussianBoxIntensityA, m_Paramters.m_GaussianBoxIntensityB, m_Paramters.m_SigmaGaussianKernelA, m_Paramters.m_SigmaGaussianKernelB);
                        }
                            break;
                    }
                    if (Result)
                    {

                        m_LeftMinimalIntegrationValue.m_Channel.m_R = TMax(m_Paramters.m_MinimalIntegrationValue, m_pLeftRedChannelRadiometricResponseFunction->GetMinimalIntegrationValue());
                        m_LeftMinimalIntegrationValue.m_Channel.m_G = TMax(m_Paramters.m_MinimalIntegrationValue, m_pLeftGreenChannelRadiometricResponseFunction->GetMinimalIntegrationValue());
                        m_LeftMinimalIntegrationValue.m_Channel.m_B = TMax(m_Paramters.m_MinimalIntegrationValue, m_pLeftBlueChannelRadiometricResponseFunction->GetMinimalIntegrationValue());
                        m_RightMinimalIntegrationValue.m_Channel.m_R = TMax(m_Paramters.m_MinimalIntegrationValue, m_pRightRedChannelRadiometricResponseFunction->GetMinimalIntegrationValue());
                        m_RightMinimalIntegrationValue.m_Channel.m_G = TMax(m_Paramters.m_MinimalIntegrationValue, m_pRightGreenChannelRadiometricResponseFunction->GetMinimalIntegrationValue());
                        m_RightMinimalIntegrationValue.m_Channel.m_B = TMax(m_Paramters.m_MinimalIntegrationValue, m_pRightBlueChannelRadiometricResponseFunction->GetMinimalIntegrationValue());

                        m_SamplesPerDiscreteIncrement = m_pLeftRedChannelRadiometricResponseFunction->GetSamplesPerDiscreteIncrement();
                        m_pLeftRedWeigthingKernel = m_pLeftRedChannelRadiometricResponseFunction->GetWeigthingKernel();
                        m_pLeftGreenWeigthingKernel = m_pLeftGreenChannelRadiometricResponseFunction->GetWeigthingKernel();
                        m_pLeftBlueWeigthingKernel = m_pLeftBlueChannelRadiometricResponseFunction->GetWeigthingKernel();
                        m_pLeftRedSampledResponseIdealFunction = m_pLeftRedChannelRadiometricResponseFunction->GetSampledResponseIdealFunction();
                        m_pLeftGreenSampledResponseIdealFunction = m_pLeftGreenChannelRadiometricResponseFunction->GetSampledResponseIdealFunction();
                        m_pLeftBlueSampledResponseIdealFunction = m_pLeftBlueChannelRadiometricResponseFunction->GetSampledResponseIdealFunction();
                        m_pRightRedWeigthingKernel = m_pRightRedChannelRadiometricResponseFunction->GetWeigthingKernel();
                        m_pRightGreenWeigthingKernel = m_pRightGreenChannelRadiometricResponseFunction->GetWeigthingKernel();
                        m_pRightBlueWeigthingKernel = m_pRightBlueChannelRadiometricResponseFunction->GetWeigthingKernel();
                        m_pRightRedSampledResponseIdealFunction = m_pRightRedChannelRadiometricResponseFunction->GetSampledResponseIdealFunction();
                        m_pRightGreenSampledResponseIdealFunction = m_pRightGreenChannelRadiometricResponseFunction->GetSampledResponseIdealFunction();
                        m_pRightBlueSampledResponseIdealFunction = m_pRightBlueChannelRadiometricResponseFunction->GetSampledResponseIdealFunction();
                    }
                    m_IsSampledResponseIdealFunctionLoaded = Result;
                    return Result;
                }
                return false;
            }

            real CStereoImageMultiChannelHDRSynthesizer::ContinuousTriangularWeigthingKernel(real Intensity)
            {
                return _REAL_ONE_ - RealAbs(Intensity - _REAL_127_5_) * _REAL_1_127_5_;
            }

            real CStereoImageMultiChannelHDRSynthesizer::ContinuousEpanechnikovWeigthingKernel(real Intensity)
            {
                real NormalizedDeviation = RealAbs(Intensity - _REAL_127_5_) * _REAL_1_127_5_;
                return _REAL_ONE_ - NormalizedDeviation * NormalizedDeviation;
            }

            real CStereoImageMultiChannelHDRSynthesizer::ContinuousGaussianWeigthingKernel(real Intensity, real ExponentFactor)
            {
                real Deviation = Intensity - _REAL_127_5_;
                return RealExp(Deviation * Deviation * ExponentFactor);
            }

            real CStereoImageMultiChannelHDRSynthesizer::ContinuousGaussianBoxWeigthingKernel(real Intensity, real IntensityA, real IntensityB, real ExponentFactorA, real ExponentFactorB)
            {
                if ((Intensity >= IntensityA) && (Intensity <= IntensityB))
                    return _REAL_ONE_;
                else if (Intensity < IntensityA)
                {
                    real Deviation = Intensity - IntensityA;
                    return RealExp(Deviation * Deviation * ExponentFactorA);
                }
                else
                {
                    real Deviation = Intensity - IntensityB;
                    return RealExp(Deviation * Deviation * ExponentFactorB);
                }
            }
        }
    }
}
*/
