/*
 * StereoImageBayerHDRSynthesizer.cpp
 *
 *  Created on: 11.04.2011
 *      Author: gonzalez
 */
/*
#include "StereoImageBayerHDRSynthesizer.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
#ifdef _STEREO_IMAGE_BAYER_HDR_SYNTHESIZER_USE_LOOKUP_TABLE_
            INITIALIZE_EXPONENTIAL_LOOKUP_TABLE(CStereoImageBayerHDRSynthesizer)
#endif

            CStereoImageBayerHDRSynthesizer::CStereoImageBayerHDRSynthesizer(const BayerPatternType Type, Cameras::RadiometricCalibration::CMultiChannelRadiometricResponseFunction** ppRadiometricResponseFunctions, const CImageActiveZone* pActiveZone, const TImage<byte>** ppInputImages) :
                TStereoImageProcess<byte, real, CDiscreteTristimulusPixel> (eStereoImageBayerHDRSynthesizer, pActiveZone, ppInputImages), m_BayerPattern(Type), m_TotalExposures(0), m_IsSampledResponseIdealFunctionLoaded(false), m_ActiveAreaNormalization(_REAL_ZERO_), m_LeftWeightAccumulator(_REAL_ZERO_), m_RightWeightAccumulator(_REAL_ZERO_), m_LeftMinimalValue(CDiscreteTristimulusPixel::s_Maximal), m_RightMinimalValue(CDiscreteTristimulusPixel::s_Maximal), m_LeftMaximalValue(
                        CDiscreteTristimulusPixel::s_Minimal), m_RightMaximalValue(CDiscreteTristimulusPixel::s_Minimal), m_pLimitedLeftWeigthingKernel(NULL), m_pLimitedRightWeigthingKernel(NULL), m_ppAccumulatorChromaticMaps(NULL), m_ppAccumulatorRadianceMaps(NULL), m_ppAccumulatorWeightingMaps(NULL), m_pLeftRedChannelRadiometricResponseFunction(NULL), m_pLeftGreenChannelRadiometricResponseFunction(NULL), m_pLeftBlueChannelRadiometricResponseFunction(NULL), m_pRightRedChannelRadiometricResponseFunction(
                        NULL), m_pRightGreenChannelRadiometricResponseFunction(NULL), m_pRightBlueChannelRadiometricResponseFunction(NULL)
            {
                memset(&m_Paramters, 0, sizeof(Paramters));
                memset(&m_LeftChannelDescriptiveStatistics, 0, sizeof(ChannelDescriptiveStatistics));
                memset(&m_RightChannelDescriptiveStatistics, 0, sizeof(ChannelDescriptiveStatistics));
                if (m_IsEnabled)
                {

#ifdef _STEREO_IMAGE_BAYER_HDR_SYNTHESIZER_USE_LOOKUP_TABLE_
                    LOAD_EXPONENTIAL_LOOKUP_TABLE(CStereoImageBayerHDRSynthesizer)
#endif

                    m_ppOutputImages = new TImage<real>*[_STEREO_];
                    m_ppAccumulatorChromaticMaps = new TImage<real>*[_STEREO_];
                    m_ppAccumulatorRadianceMaps = new TImage<real>*[_STEREO_];
                    m_ppAccumulatorWeightingMaps = new TImage<real>*[_STEREO_];
                    for (uint i = 0; i < _STEREO_; ++i)
                    {
                        const CImageSize& ImageSize = m_ppInputImages[i]->GetSize();
                        m_ppOutputImages[i] = new TImage<real> (ImageSize);
                        m_ppAccumulatorChromaticMaps[i] = new TImage<real> (ImageSize);
                        m_ppAccumulatorRadianceMaps[i] = new TImage<real> (ImageSize);
                        m_ppAccumulatorWeightingMaps[i] = new TImage<real> (ImageSize);
                    }
                    m_pLeftRedChannelRadiometricResponseFunction = ppRadiometricResponseFunctions[_LEFT_]->GetRedChannelRadiometricResponseFunction();
                    m_pLeftGreenChannelRadiometricResponseFunction = ppRadiometricResponseFunctions[_LEFT_]->GetGreenChannelRadiometricResponseFunction();
                    m_pLeftBlueChannelRadiometricResponseFunction = ppRadiometricResponseFunctions[_LEFT_]->GetBlueChannelRadiometricResponseFunction();
                    m_pRightRedChannelRadiometricResponseFunction = ppRadiometricResponseFunctions[_RIGHT_]->GetRedChannelRadiometricResponseFunction();
                    m_pRightGreenChannelRadiometricResponseFunction = ppRadiometricResponseFunctions[_RIGHT_]->GetGreenChannelRadiometricResponseFunction();
                    m_pRightBlueChannelRadiometricResponseFunction = ppRadiometricResponseFunctions[_RIGHT_]->GetBlueChannelRadiometricResponseFunction();
                    m_ActiveAreaNormalization = _REAL_ONE_ / (real(m_OutputActiveZone.GetActiveArea()));
                    m_Paramters.m_SynthesisMode = _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SYNTHESIS_MODE_;
                    m_Paramters.m_WeightingMode = _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_WEIGHTING_MODE_;
                    m_Paramters.m_SigmaGaussianKernel = _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SIGMA_GAUSSIAN_KERNEL_;
                    m_Paramters.m_GaussianBoxIntensityA = _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_INTENSITY_A_GAUSSIAN_KERNEL_;
                    m_Paramters.m_GaussianBoxIntensityB = _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_INTENSITY_B_GAUSSIAN_KERNEL_;
                    m_Paramters.m_SigmaGaussianKernelA = _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SIGMA_A_GAUSSIAN_KERNEL_;
                    m_Paramters.m_SigmaGaussianKernelB = _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SIGMA_B_GAUSSIAN_KERNEL_;
                    m_Paramters.m_MinimalIntegrationValue = _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_MINIMAL_INTEGRATION_VALUE_;
                    m_Paramters.m_MaximalIntegrationValue = _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_MAXIMAL_INTEGRATION_VALUE_;
                    m_LeftMaximalValue.SetValue(m_Paramters.m_MaximalIntegrationValue, m_Paramters.m_MaximalIntegrationValue, m_Paramters.m_MaximalIntegrationValue);
                    m_RightMaximalValue.SetValue(m_Paramters.m_MaximalIntegrationValue, m_Paramters.m_MaximalIntegrationValue, m_Paramters.m_MaximalIntegrationValue);
                    m_LeftMinimalValue.SetValue(m_Paramters.m_MinimalIntegrationValue, m_Paramters.m_MinimalIntegrationValue, m_Paramters.m_MinimalIntegrationValue);
                    m_RightMinimalValue.SetValue(m_Paramters.m_MinimalIntegrationValue, m_Paramters.m_MinimalIntegrationValue, m_Paramters.m_MinimalIntegrationValue);
                }
            }

            CStereoImageBayerHDRSynthesizer::~CStereoImageBayerHDRSynthesizer()
            {
                if (m_IsEnabled)
                {
                    for (uint i = 0; i < _STEREO_; ++i)
                    {
                        delete m_ppOutputImages[i];
                        delete m_ppAccumulatorChromaticMaps[i];
                        delete m_ppAccumulatorRadianceMaps[i];
                        delete m_ppAccumulatorWeightingMaps[i];
                    }
                    delete[] m_ppOutputImages;
                    delete[] m_ppAccumulatorChromaticMaps;
                    delete[] m_ppAccumulatorRadianceMaps;
                    delete[] m_ppAccumulatorWeightingMaps;
                    m_ppOutputImages = NULL;
                    m_ppAccumulatorChromaticMaps = NULL;
                    m_ppAccumulatorRadianceMaps = NULL;
                    m_ppAccumulatorWeightingMaps = NULL;
                }
            }

            void CStereoImageBayerHDRSynthesizer::AddChromaticSample(const real LeftWeight, const real RightWeight)
            {
                if ((LeftWeight > _REAL_EPSILON_) || (RightWeight > _REAL_EPSILON_))
                {
                    const CPixelLocation& OutputUpperLeftPixelLocation = m_OutputActiveZone.GetUpperLeftLocation();
                    const CPixelLocation& OutputLowerRightPixelLocation = m_OutputActiveZone.GetLowerRightLocation();
                    const coordinate ImageWidth = m_OutputActiveZone.GetWidth();
                    const byte* pLeftBaseInputPixel = m_ppInputImages[_LEFT_]->GetReadOnlyBufferAt(OutputUpperLeftPixelLocation);
                    const byte* pRightBaseInputPixel = m_ppInputImages[_RIGHT_]->GetReadOnlyBufferAt(OutputUpperLeftPixelLocation);
                    real* pLeftBaseChromaticPixel = m_ppAccumulatorChromaticMaps[_LEFT_]->GetWritableBufferAt(OutputUpperLeftPixelLocation);
                    real* pRightBaseChromaticPixel = m_ppAccumulatorChromaticMaps[_RIGHT_]->GetWritableBufferAt(OutputUpperLeftPixelLocation);
                    for (CPixelLocation Location = OutputUpperLeftPixelLocation; Location.m_y < OutputLowerRightPixelLocation.m_y; ++Location.m_y, pLeftBaseInputPixel += ImageWidth, pRightBaseInputPixel += ImageWidth, pLeftBaseChromaticPixel += ImageWidth, pRightBaseChromaticPixel += ImageWidth)
                    {
                        const byte* pLeftInputPixel = pLeftBaseInputPixel;
                        const byte* pRightInputPixel = pRightBaseInputPixel;
                        real* pLeftChromaticPixel = pLeftBaseChromaticPixel;
                        real* pRightChromaticPixel = pRightBaseChromaticPixel;
                        for (Location.m_x = OutputUpperLeftPixelLocation.m_x; Location.m_x < OutputLowerRightPixelLocation.m_x; ++Location.m_x, ++pLeftChromaticPixel, ++pRightChromaticPixel, ++pLeftInputPixel, ++pRightInputPixel)
                        {
                            *pLeftChromaticPixel += LeftWeight * *pLeftInputPixel;
                            *pRightChromaticPixel += RightWeight * *pRightInputPixel;
                        }
                    }
                    m_LeftWeightAccumulator += LeftWeight;
                    m_RightWeightAccumulator += RightWeight;
                }
            }

            void CStereoImageBayerHDRSynthesizer::AddExposureIntegration(const real LeftWeight, const real RightWeight, const uint ShuttingTimeIndex, const ExposureIntegrationSampleType SampleType)
            {
                if ((LeftWeight > _REAL_EPSILON_) || (RightWeight > _REAL_EPSILON_))
                {
                    ExposureIntegration EI =
                    { 0 };
                    switch (SampleType)
                    {
                        case eCombined:
                            EI.m_LeftEnergy = EI.m_RightEnergy = (LeftWeight + RightWeight) * m_ActiveAreaNormalization;
                            break;
                        case ePerChannel:
                            EI.m_LeftEnergy = LeftWeight * m_ActiveAreaNormalization;
                            EI.m_RightEnergy = RightWeight * m_ActiveAreaNormalization;
                            break;
                    }
                    EI.m_ShuttingTimeIndex = ShuttingTimeIndex;
                    m_ExposureIntegrations.push_back(EI);
                }
            }

            void CStereoImageBayerHDRSynthesizer::NormalizeChromaticSamples()
            {
                const CPixelLocation& OutputUpperLeftPixelLocation = m_OutputActiveZone.GetUpperLeftLocation();
                const CPixelLocation& OutputLowerRightPixelLocation = m_OutputActiveZone.GetLowerRightLocation();
                const coordinate ImageWidth = m_OutputActiveZone.GetWidth();
                const real LeftNormalization = _REAL_ONE_ / (m_LeftWeightAccumulator);
                const real RightNormalization = _REAL_ONE_ / (m_RightWeightAccumulator);
                real* pLeftBaseChromaticPixel = m_ppAccumulatorChromaticMaps[_LEFT_]->GetWritableBufferAt(OutputUpperLeftPixelLocation);
                real* pRightBaseChromaticPixel = m_ppAccumulatorChromaticMaps[_RIGHT_]->GetWritableBufferAt(OutputUpperLeftPixelLocation);
                for (CPixelLocation Location = OutputUpperLeftPixelLocation; Location.m_y < OutputLowerRightPixelLocation.m_y; ++Location.m_y, pLeftBaseChromaticPixel += ImageWidth, pRightBaseChromaticPixel += ImageWidth)
                {
                    real* pLeftChromaticPixel = pLeftBaseChromaticPixel;
                    real* pRightChromaticPixel = pRightBaseChromaticPixel;
                    for (Location.m_x = OutputUpperLeftPixelLocation.m_x; Location.m_x < OutputLowerRightPixelLocation.m_x; ++Location.m_x, ++pLeftChromaticPixel, ++pRightChromaticPixel)
                    {
                        *pLeftChromaticPixel *= LeftNormalization;
                        *pRightChromaticPixel *= RightNormalization;
                    }
                }
            }

            const bool CStereoImageBayerHDRSynthesizer::AddSceneAnalysisSample(const Identifier TrialId, const uint ShuttingTimeIndex, const ExposureIntegrationSampleType SampleType)
            {
                if (m_IsEnabled && m_IsSampledResponseIdealFunctionLoaded)
                {
                    START_PROCESS_EXECUTION_LOG("AddSceneAnalysisSample", TrialId)

                    const CPixelLocation& OutputUpperLeftPixelLocation = m_OutputActiveZone.GetUpperLeftLocation();
                    const CPixelLocation& OutputLowerRightPixelLocation = m_OutputActiveZone.GetLowerRightLocation();
                    const coordinate ImageWidth = m_OutputActiveZone.GetWidth();
                    const byte* pLeftBaseInputPixel = m_ppInputImages[_LEFT_]->GetReadOnlyBufferAt(OutputUpperLeftPixelLocation);
                    const byte* pRighBasetInputPixel = m_ppInputImages[_RIGHT_]->GetReadOnlyBufferAt(OutputUpperLeftPixelLocation);
                    real LeftWeight = _REAL_ZERO_, RightWeight = _REAL_ZERO_;
                    for (CPixelLocation Location = OutputUpperLeftPixelLocation; Location.m_y < OutputLowerRightPixelLocation.m_y; ++Location.m_y, pLeftBaseInputPixel += ImageWidth, pRighBasetInputPixel += ImageWidth)
                    {
                        const byte* pLeftInputPixel = pLeftBaseInputPixel;
                        const byte* pRightInputPixel = pRighBasetInputPixel;
                        for (Location.m_x = OutputUpperLeftPixelLocation.m_x; Location.m_x < OutputLowerRightPixelLocation.m_x; ++Location.m_x, ++pLeftInputPixel, ++pRightInputPixel)
                        {
                            LeftWeight += m_pLimitedLeftWeigthingKernel[*pLeftInputPixel];
                            RightWeight += m_pLimitedRightWeigthingKernel[*pRightInputPixel];
                        }
                    }
                    AddExposureIntegration(LeftWeight, RightWeight, ShuttingTimeIndex, SampleType);

                    STOP_PROCESS_EXECUTION_LOG

                    return true;
                }
                return false;
            }

            const bool CStereoImageBayerHDRSynthesizer::AddExposureAndChromatic(const Identifier TrialId, const uint ShuttingTimeIndex, const bool EnableChromaticIntegration)
            {
                if (m_IsEnabled && m_IsSampledResponseIdealFunctionLoaded)
                {
                    START_PROCESS_EXECUTION_LOG("AddExposureAndChromaticTableMode", TrialId)

                    const CPixelLocation& OutputUpperLeftPixelLocation = m_OutputActiveZone.GetUpperLeftLocation();
                    const CPixelLocation& OutputLowerRightPixelLocation = m_OutputActiveZone.GetLowerRightLocation();
                    const coordinate ImageWidth = m_OutputActiveZone.GetWidth();
                    const byte* pLeftBaseInputPixel = m_ppInputImages[_LEFT_]->GetReadOnlyBufferAt(OutputUpperLeftPixelLocation);
                    const byte* pRightBaseInputPixel = m_ppInputImages[_RIGHT_]->GetReadOnlyBufferAt(OutputUpperLeftPixelLocation);
                    const real* pLeftGreenTableResponseIdealFunction = m_pLeftGreenChannelRadiometricResponseFunction->GetIntegrationTable()->GetReadOnlyBufferLineAt(ShuttingTimeIndex);
                    const real* pRightGreenTableResponseIdealFunction = m_pRightGreenChannelRadiometricResponseFunction->GetIntegrationTable()->GetReadOnlyBufferLineAt(ShuttingTimeIndex);
                    const real* ppLeftChannelTableResponseIdealFunction[] =
                    { m_pLeftRedChannelRadiometricResponseFunction->GetIntegrationTable()->GetReadOnlyBufferLineAt(ShuttingTimeIndex), m_pLeftBlueChannelRadiometricResponseFunction->GetIntegrationTable()->GetReadOnlyBufferLineAt(ShuttingTimeIndex) };
                    const real* ppRightChannelTableResponseIdealFunction[] =
                    { m_pRightRedChannelRadiometricResponseFunction->GetIntegrationTable()->GetReadOnlyBufferLineAt(ShuttingTimeIndex), m_pRightBlueChannelRadiometricResponseFunction->GetIntegrationTable()->GetReadOnlyBufferLineAt(ShuttingTimeIndex) };
                    real* pLeftBaseAccumulatorRadiancePixel = m_ppAccumulatorRadianceMaps[_LEFT_]->GetWritableBufferAt(OutputUpperLeftPixelLocation);
                    real* pRightBaseAccumulatorRadiancePixel = m_ppAccumulatorRadianceMaps[_RIGHT_]->GetWritableBufferAt(OutputUpperLeftPixelLocation);
                    real* pLeftBaseAccumulatorWeightingPixel = m_ppAccumulatorWeightingMaps[_LEFT_]->GetWritableBufferAt(OutputUpperLeftPixelLocation);
                    real* pRightBaseAccumulatorWeightingPixel = m_ppAccumulatorWeightingMaps[_RIGHT_]->GetWritableBufferAt(OutputUpperLeftPixelLocation);
                    real LeftWeight = _REAL_ZERO_, RightWeight = _REAL_ZERO_;
                    const coordinate SelectingChannelFlag = (m_BayerPattern & 0x4) >> 2;
                    const coordinate SelectingShiftingFlag = (OutputUpperLeftPixelLocation.m_x & 0x01) ^ ((m_BayerPattern & 0x02) >> 1);
                    for (CPixelLocation Location = OutputUpperLeftPixelLocation; Location.m_y < OutputLowerRightPixelLocation.m_y; ++Location.m_y, pLeftBaseInputPixel += ImageWidth, pRightBaseInputPixel += ImageWidth, pLeftBaseAccumulatorRadiancePixel += ImageWidth, pRightBaseAccumulatorRadiancePixel += ImageWidth, pLeftBaseAccumulatorWeightingPixel += ImageWidth, pRightBaseAccumulatorWeightingPixel += ImageWidth)
                    {
                        const real* pLeftSelectedChannelTableResponseIdealFunction = ppLeftChannelTableResponseIdealFunction[(Location.m_y & 0x1) ^ SelectingChannelFlag];
                        const real* pRightSelectedChannelTableResponseIdealFunction = ppRightChannelTableResponseIdealFunction[(Location.m_y & 0x1) ^ SelectingChannelFlag];
                        const coordinate TogglerSwitch = (Location.m_y & 0x1) ^ SelectingShiftingFlag;
                        const byte* pLeftInputPixel = pLeftBaseInputPixel;
                        const byte* pRightInputPixel = pRightBaseInputPixel;
                        real* pLeftAccumulatorRadiancePixel = pLeftBaseAccumulatorRadiancePixel;
                        real* pRightAccumulatorRadiancePixel = pRightBaseAccumulatorRadiancePixel;
                        real* pLeftAccumulatorWeightingPixel = pLeftBaseAccumulatorWeightingPixel;
                        real* pRightAccumulatorWeightingPixel = pRightBaseAccumulatorWeightingPixel;
                        for (Location.m_x = OutputUpperLeftPixelLocation.m_x; Location.m_x < OutputLowerRightPixelLocation.m_x; ++Location.m_x, ++pLeftInputPixel, ++pRightInputPixel, ++pLeftAccumulatorWeightingPixel, ++pLeftAccumulatorRadiancePixel, ++pRightAccumulatorWeightingPixel, ++pRightAccumulatorRadiancePixel)
                            if ((Location.m_x & 0x1) ^ TogglerSwitch)//Channel Green
                            {
                                const real LeftValue = m_pLimitedLeftWeigthingKernel[*pLeftInputPixel];
                                if (LeftValue)
                                {
                                    LeftWeight += LeftValue;
                                    *pLeftAccumulatorWeightingPixel += LeftValue;
                                    *pLeftAccumulatorRadiancePixel += pLeftGreenTableResponseIdealFunction[*pLeftInputPixel];
                                }
                                const real RightValue = m_pLimitedRightWeigthingKernel[*pRightInputPixel];
                                if (RightValue)
                                {
                                    RightWeight += RightValue;
                                    *pRightAccumulatorWeightingPixel += RightValue;
                                    *pRightAccumulatorRadiancePixel += pRightGreenTableResponseIdealFunction[*pRightInputPixel];
                                }
                            }
                            else//Selected Channel
                            {
                                const real LeftValue = m_pLimitedLeftWeigthingKernel[*pLeftInputPixel];
                                if (LeftValue)
                                {
                                    LeftWeight += LeftValue;
                                    *pLeftAccumulatorWeightingPixel += LeftValue;
                                    *pLeftAccumulatorRadiancePixel += pLeftSelectedChannelTableResponseIdealFunction[*pLeftInputPixel];
                                }
                                const real RightValue = m_pLimitedRightWeigthingKernel[*pRightInputPixel];
                                if (RightValue)
                                {
                                    RightWeight += RightValue;
                                    *pRightAccumulatorWeightingPixel += RightValue;
                                    *pRightAccumulatorRadiancePixel += pRightSelectedChannelTableResponseIdealFunction[*pRightInputPixel];
                                }
                            }
                    }
                    if (EnableChromaticIntegration)
                        AddChromaticSample(LeftWeight, RightWeight);
                    AddExposureIntegration(LeftWeight, RightWeight, ShuttingTimeIndex, ePerChannel);
                    ++m_TotalExposures;

                    STOP_PROCESS_EXECUTION_LOG

                    return true;
                }
                return false;
            }

            const bool CStereoImageBayerHDRSynthesizer::Synthesis(const Identifier TrialId, const bool EnableChromaticSynthesis)
            {
                if (m_IsEnabled && (m_TotalExposures > 1))
                {
                    START_PROCESS_EXECUTION_LOG("Synthesis", TrialId)

                    m_ppOutputImages[_LEFT_]->Clear();
                    m_ppOutputImages[_RIGHT_]->Clear();
                    const CPixelLocation& OutputUpperLeftPixelLocation = m_OutputActiveZone.GetUpperLeftLocation();
                    const CPixelLocation& OutputLowerRightPixelLocation = m_OutputActiveZone.GetLowerRightLocation();
                    const coordinate ImageWidth = m_OutputActiveZone.GetWidth();
                    const real* pLeftBaseAccumulatorRadiancePixel = m_ppAccumulatorRadianceMaps[_LEFT_]->GetReadOnlyBufferAt(OutputUpperLeftPixelLocation);
                    const real* pRightBaseAccumulatorRadiancePixel = m_ppAccumulatorRadianceMaps[_RIGHT_]->GetReadOnlyBufferAt(OutputUpperLeftPixelLocation);
                    const real* pLeftBaseAccumulatorWeightingPixel = m_ppAccumulatorWeightingMaps[_LEFT_]->GetReadOnlyBufferAt(OutputUpperLeftPixelLocation);
                    const real* pRightBaseAccumulatorWeightingPixel = m_ppAccumulatorWeightingMaps[_RIGHT_]->GetReadOnlyBufferAt(OutputUpperLeftPixelLocation);
                    real* pLeftBaseOutputPixel = m_ppOutputImages[_LEFT_]->GetWritableBufferAt(OutputUpperLeftPixelLocation);
                    real* pRightBaseOutputPixel = m_ppOutputImages[_RIGHT_]->GetWritableBufferAt(OutputUpperLeftPixelLocation);
                    switch (m_Paramters.m_SynthesisMode)
                    {
                        case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eHomomorphic:
                            for (CPixelLocation Location = OutputUpperLeftPixelLocation; Location.m_y < OutputLowerRightPixelLocation.m_y; ++Location.m_y, pLeftBaseOutputPixel += ImageWidth, pRightBaseOutputPixel += ImageWidth, pLeftBaseAccumulatorRadiancePixel += ImageWidth, pRightBaseAccumulatorRadiancePixel += ImageWidth, pLeftBaseAccumulatorWeightingPixel += ImageWidth, pRightBaseAccumulatorWeightingPixel += ImageWidth)
                            {
                                const real* pLeftAccumulatorRadiancePixel = pLeftBaseAccumulatorRadiancePixel;
                                const real* pRightAccumulatorRadiancePixel = pRightBaseAccumulatorRadiancePixel;
                                const real* pLeftAccumulatorWeightingPixel = pLeftBaseAccumulatorWeightingPixel;
                                const real* pRightAccumulatorWeightingPixel = pRightBaseAccumulatorWeightingPixel;
                                real* pLeftOutputPixel = pLeftBaseOutputPixel;
                                real* pRightOutputPixel = pRightBaseOutputPixel;
                                for (Location.m_x = OutputUpperLeftPixelLocation.m_x; Location.m_x < OutputLowerRightPixelLocation.m_x; ++Location.m_x, ++pLeftOutputPixel, ++pLeftAccumulatorRadiancePixel, ++pLeftAccumulatorWeightingPixel, ++pRightOutputPixel, ++pRightAccumulatorRadiancePixel, ++pRightAccumulatorWeightingPixel)
                                {
                                    if (*pLeftAccumulatorWeightingPixel > _REAL_EPSILON_)
                                        *pLeftOutputPixel = *pLeftAccumulatorRadiancePixel / *pLeftAccumulatorWeightingPixel;
                                    if (*pRightAccumulatorWeightingPixel > _REAL_EPSILON_)
                                        *pRightOutputPixel = *pRightAccumulatorRadiancePixel / *pRightAccumulatorWeightingPixel;
                                }
                            }
                            break;
                        case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::elinear:
                            for (CPixelLocation Location = OutputUpperLeftPixelLocation; Location.m_y < OutputLowerRightPixelLocation.m_y; ++Location.m_y, pLeftBaseOutputPixel += ImageWidth, pRightBaseOutputPixel += ImageWidth, pLeftBaseAccumulatorRadiancePixel += ImageWidth, pRightBaseAccumulatorRadiancePixel += ImageWidth, pLeftBaseAccumulatorWeightingPixel += ImageWidth, pRightBaseAccumulatorWeightingPixel += ImageWidth)
                            {
                                const real* pLeftAccumulatorRadiancePixel = pLeftBaseAccumulatorRadiancePixel;
                                const real* pRightAccumulatorRadiancePixel = pRightBaseAccumulatorRadiancePixel;
                                const real* pLeftAccumulatorWeightingPixel = pLeftBaseAccumulatorWeightingPixel;
                                const real* pRightAccumulatorWeightingPixel = pRightBaseAccumulatorWeightingPixel;
                                real* pLeftOutputPixel = pLeftBaseOutputPixel;
                                real* pRightOutputPixel = pRightBaseOutputPixel;
                                for (Location.m_x = OutputUpperLeftPixelLocation.m_x; Location.m_x < OutputLowerRightPixelLocation.m_x; ++Location.m_x, ++pLeftOutputPixel, ++pLeftAccumulatorRadiancePixel, ++pLeftAccumulatorWeightingPixel, ++pRightOutputPixel, ++pRightAccumulatorRadiancePixel, ++pRightAccumulatorWeightingPixel)
                                {
                                    *pLeftOutputPixel = (*pLeftAccumulatorWeightingPixel > _REAL_EPSILON_) ? Linearization(*pLeftAccumulatorRadiancePixel / *pLeftAccumulatorWeightingPixel) : _REAL_ONE_;
                                    *pRightOutputPixel = (*pRightAccumulatorWeightingPixel > _REAL_EPSILON_) ? Linearization(*pRightAccumulatorRadiancePixel / *pRightAccumulatorWeightingPixel) : _REAL_ONE_;
                                }
                            }
                            break;
                    }
                    if (EnableChromaticSynthesis)
                        NormalizeChromaticSamples();

                    STOP_PROCESS_EXECUTION_LOG

                    return true;
                }
                return false;
            }

            void CStereoImageBayerHDRSynthesizer::ClearExposures()
            {
                if (m_TotalExposures)
                {
                    m_TotalExposures = 0;
                    m_RightWeightAccumulator = m_LeftWeightAccumulator = _REAL_ZERO_;
                    for (uint i = 0; i < _STEREO_; ++i)
                    {
                        m_ppAccumulatorChromaticMaps[i]->Clear();
                        m_ppAccumulatorRadianceMaps[i]->Clear();
                        m_ppAccumulatorWeightingMaps[i]->Clear();
                    }
                    m_ExposureIntegrations.clear();
                }
            }

            const bool CStereoImageBayerHDRSynthesizer::CreateExposureIntegrationPlan(const uint TotalPlannedExposures, const ExposureIntegrationPlanType PlanType, const bool Verbose)
            {
                uint TotalExposureIntegrations = m_ExposureIntegrations.size();
                if (TotalExposureIntegrations)
                {
                    m_PlannExposureIntegrations.clear();
                    ExposureIntegration* pPreviousEI = &m_ExposureIntegrations[0];
                    ExposureIntegration* pCurrentEI = NULL;
                    switch (PlanType)
                    {
                        case eGradient:
                            for (uint i = 1; i < TotalExposureIntegrations; ++i)
                            {
                                pCurrentEI = &m_ExposureIntegrations[i];
                                pCurrentEI->m_LeftDelta = RealAbs(pCurrentEI->m_LeftEnergy - pPreviousEI->m_LeftEnergy);
                                pCurrentEI->m_LeftAccumulator = pCurrentEI->m_LeftDelta + pPreviousEI->m_LeftAccumulator;
                                pCurrentEI->m_RightDelta = RealAbs(pCurrentEI->m_RightEnergy - pPreviousEI->m_RightEnergy);
                                pCurrentEI->m_RightAccumulator = pCurrentEI->m_RightDelta + pPreviousEI->m_RightAccumulator;
                                pPreviousEI = pCurrentEI;
                            }
                            break;
                        case eEnergy:
                            for (uint i = 1; i < TotalExposureIntegrations; ++i)
                            {
                                pCurrentEI = &m_ExposureIntegrations[i];
                                pCurrentEI->m_LeftDelta = pCurrentEI->m_LeftEnergy;
                                pCurrentEI->m_LeftAccumulator = pCurrentEI->m_LeftDelta + pPreviousEI->m_LeftAccumulator;
                                pCurrentEI->m_RightDelta = pCurrentEI->m_RightEnergy;
                                pCurrentEI->m_RightAccumulator = pCurrentEI->m_RightDelta + pPreviousEI->m_RightAccumulator;
                                pPreviousEI = pCurrentEI;
                            }
                            break;
                        case eIsoArcLength:
                        {
                            real MaximalShuttingTimeIndex = pPreviousEI->m_ShuttingTimeIndex;
                            real MinimalShuttingTimeIndex = pPreviousEI->m_ShuttingTimeIndex;
                            real MaximalEnergyLeft = pPreviousEI->m_LeftEnergy;
                            real MinimalEnergyLeft = pPreviousEI->m_LeftEnergy;
                            real MaximalEnergyRight = pPreviousEI->m_RightEnergy;
                            real MinimalEnergyRight = pPreviousEI->m_RightEnergy;
                            for (uint i = 0; i < TotalExposureIntegrations; ++i)
                            {
                                pCurrentEI = &m_ExposureIntegrations[i];
                                if (pCurrentEI->m_ShuttingTimeIndex > MaximalShuttingTimeIndex)
                                    MaximalShuttingTimeIndex = pCurrentEI->m_ShuttingTimeIndex;
                                else if (pCurrentEI->m_ShuttingTimeIndex < MinimalShuttingTimeIndex)
                                    MinimalShuttingTimeIndex = pCurrentEI->m_ShuttingTimeIndex;
                                if (pCurrentEI->m_LeftEnergy > MaximalEnergyLeft)
                                    MaximalEnergyLeft = pCurrentEI->m_LeftEnergy;
                                else if (pCurrentEI->m_LeftEnergy < MinimalEnergyLeft)
                                    MinimalEnergyLeft = pCurrentEI->m_LeftEnergy;
                                if (pCurrentEI->m_RightEnergy > MaximalEnergyRight)
                                    MaximalEnergyRight = pCurrentEI->m_RightEnergy;
                                else if (pCurrentEI->m_RightEnergy < MinimalEnergyRight)
                                    MinimalEnergyRight = pCurrentEI->m_RightEnergy;
                            }
                            real NormalizationEnergyLeft = _REAL_ONE_ / (MaximalEnergyLeft - MinimalEnergyLeft);
                            real NormalizationEnergyRight = _REAL_ONE_ / (MaximalEnergyRight - MinimalEnergyRight);
                            real NormalizationShuttingTimeIndex = MaximalShuttingTimeIndex - MinimalShuttingTimeIndex;
                            for (uint i = 0; i < TotalExposureIntegrations; ++i)
                            {
                                pCurrentEI = &m_ExposureIntegrations[i];
                                real DeltaExposure = (pCurrentEI->m_ShuttingTimeIndex - pPreviousEI->m_ShuttingTimeIndex) * NormalizationShuttingTimeIndex;
                                DeltaExposure *= DeltaExposure;
                                real DeltaEnergy = (pCurrentEI->m_LeftEnergy - pPreviousEI->m_LeftEnergy) * NormalizationEnergyLeft;
                                pCurrentEI->m_LeftDelta = RealSqrt(DeltaEnergy * DeltaEnergy + DeltaExposure);
                                pCurrentEI->m_LeftAccumulator = pCurrentEI->m_LeftDelta + pPreviousEI->m_LeftAccumulator;
                                DeltaEnergy = (pCurrentEI->m_RightEnergy - pPreviousEI->m_RightEnergy) * NormalizationEnergyRight;
                                pCurrentEI->m_RightDelta = RealSqrt(DeltaEnergy * DeltaEnergy + DeltaExposure);
                                pCurrentEI->m_RightAccumulator = pCurrentEI->m_RightDelta + pPreviousEI->m_RightAccumulator;
                                pPreviousEI = pCurrentEI;
                            }
                        }
                            break;
                    }
                    real LeftRegularDelta = pCurrentEI->m_LeftAccumulator / real(TotalPlannedExposures);
                    real RightRegularDelta = pCurrentEI->m_RightAccumulator / real(TotalPlannedExposures);
                    real LeftDisplacement = _REAL_ZERO_, RightDisplacement = _REAL_ZERO_, PreContribution = _REAL_ZERO_, PostContribution = _REAL_ZERO_;
                    ExposureIntegration PlanEI =
                    { 0 };
                    int LeftPreIndex = 0, RightPreIndex = 0, SubExposures = DownToInteger(TotalExposureIntegrations) - 1;
                    for (uint i = 0; i <= TotalPlannedExposures; ++i, LeftDisplacement += LeftRegularDelta, RightDisplacement += RightRegularDelta)
                    {
                        if (LeftDisplacement > m_ExposureIntegrations[LeftPreIndex].m_LeftAccumulator)
                        {
                            while (LeftDisplacement > m_ExposureIntegrations[TMin(LeftPreIndex + 1, SubExposures)].m_LeftAccumulator)
                                if (++LeftPreIndex >= SubExposures)
                                    break;
                        }
                        else
                            while (LeftDisplacement < m_ExposureIntegrations[TMax(LeftPreIndex - 1, 0)].m_LeftAccumulator)
                                LeftPreIndex = TMax(LeftPreIndex - 1, 0);
                        pPreviousEI = &m_ExposureIntegrations[LeftPreIndex];
                        pCurrentEI = &m_ExposureIntegrations[TMin(LeftPreIndex + 1, SubExposures)];
                        real InterSampleDelta = pCurrentEI->m_LeftAccumulator - pPreviousEI->m_LeftAccumulator;
                        if (InterSampleDelta > _REAL_EPSILON_)
                        {
                            PostContribution = (LeftDisplacement - pPreviousEI->m_LeftAccumulator) / InterSampleDelta;
                            PreContribution = _REAL_ONE_ - PostContribution;
                        }
                        else
                            PostContribution = PreContribution = _REAL_HALF_;
                        PlanEI.m_ShuttingTimeIndex = pCurrentEI->m_ShuttingTimeIndex * PostContribution + pPreviousEI->m_ShuttingTimeIndex * PreContribution;
                        PlanEI.m_LeftEnergy = pCurrentEI->m_LeftEnergy * PostContribution + pPreviousEI->m_LeftEnergy * PreContribution;
                        if (RightDisplacement > m_ExposureIntegrations[RightPreIndex].m_RightAccumulator)
                        {
                            while (RightDisplacement > m_ExposureIntegrations[TMin(RightPreIndex + 1, SubExposures)].m_RightAccumulator)
                                if (++RightPreIndex >= SubExposures)
                                    break;
                        }
                        else
                            while (RightDisplacement < m_ExposureIntegrations[TMax(RightPreIndex - 1, 0)].m_RightAccumulator)
                                RightPreIndex = TMax(RightPreIndex - 1, 0);
                        pPreviousEI = &m_ExposureIntegrations[RightPreIndex];
                        pCurrentEI = &m_ExposureIntegrations[TMin(RightPreIndex + 1, SubExposures)];
                        InterSampleDelta = pCurrentEI->m_RightAccumulator - pPreviousEI->m_RightAccumulator;
                        if (InterSampleDelta > _REAL_EPSILON_)
                        {
                            PostContribution = (RightDisplacement - pPreviousEI->m_RightAccumulator) / InterSampleDelta;
                            PreContribution = _REAL_ONE_ - PostContribution;
                        }
                        else
                            PostContribution = PreContribution = _REAL_HALF_;
                        PlanEI.m_RightEnergy = pCurrentEI->m_RightEnergy * PostContribution + pPreviousEI->m_RightEnergy * PreContribution;
                        m_PlannExposureIntegrations.push_back(PlanEI);
                    }
                    if (Verbose)
                    {
                        DisplayExposureIntegrations();
                        DisplayPlannedExposureIntegrations();
                    }
                    m_ExposureIntegrations.clear();
                    return true;
                }
                return false;
            }

            void CStereoImageBayerHDRSynthesizer::DisplayExposureIntegrations() const
            {
                g_ConsoleStringOutput << "Exposure Integrations" << g_EndLine;
                vector<ExposureIntegration>::const_iterator EndExposureIntegrations = m_ExposureIntegrations.end();
                for (vector<ExposureIntegration>::const_iterator pExposureIntegration = m_ExposureIntegrations.begin(); pExposureIntegration != EndExposureIntegrations; ++pExposureIntegration)
                {
                    g_ConsoleStringOutput << pExposureIntegration->m_ShuttingTimeIndex << g_Tabulator;
                    g_ConsoleStringOutput << pExposureIntegration->m_LeftEnergy << g_Tabulator;
                    g_ConsoleStringOutput << pExposureIntegration->m_LeftDelta << g_Tabulator;
                    g_ConsoleStringOutput << pExposureIntegration->m_LeftAccumulator << g_DoubleTabulator;
                    g_ConsoleStringOutput << pExposureIntegration->m_RightEnergy << g_Tabulator;
                    g_ConsoleStringOutput << pExposureIntegration->m_RightDelta << g_Tabulator;
                    g_ConsoleStringOutput << pExposureIntegration->m_RightAccumulator << g_EndLine;
                }
            }

            void CStereoImageBayerHDRSynthesizer::DisplayPlannedExposureIntegrations() const
            {
                g_ConsoleStringOutput << "Planned Exposure Integrations" << g_EndLine;
                list<ExposureIntegration>::const_iterator EndExposureIntegrations = m_PlannExposureIntegrations.end();
                for (list<ExposureIntegration>::const_iterator pExposureIntegration = m_PlannExposureIntegrations.begin(); pExposureIntegration != EndExposureIntegrations; ++pExposureIntegration)
                {
                    g_ConsoleStringOutput << pExposureIntegration->m_ShuttingTimeIndex << g_Tabulator;
                    g_ConsoleStringOutput << pExposureIntegration->m_LeftEnergy << g_Tabulator;
                    g_ConsoleStringOutput << pExposureIntegration->m_RightEnergy << g_EndLine;
                }
            }

            const bool CStereoImageBayerHDRSynthesizer::ComputeDescriptiveStatistics(const Identifier TrialId)
            {
                if (m_IsEnabled && m_TotalExposures)
                {
                    START_PROCESS_EXECUTION_LOG("ComputeDescriptiveStatistics", TrialId)

                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate ImageWidth = m_OutputActiveZone.GetWidth();
                    m_LeftChannelDescriptiveStatistics.m_MaximalIntensity = CContinuousTristimulusPixel::s_Minimal;
                    m_RightChannelDescriptiveStatistics.m_MaximalIntensity = CContinuousTristimulusPixel::s_Minimal;
                    m_LeftChannelDescriptiveStatistics.m_MinimalIntensity = CContinuousTristimulusPixel::s_Maximal;
                    m_RightChannelDescriptiveStatistics.m_MinimalIntensity = CContinuousTristimulusPixel::s_Minimal;
                    m_LeftChannelDescriptiveStatistics.m_Mean = CContinuousTristimulusPixel::s_Zero;
                    m_RightChannelDescriptiveStatistics.m_Mean = CContinuousTristimulusPixel::s_Zero;
                    real* pBaseLeftOutputPixel = m_ppOutputImages[_LEFT_]->GetWritableBufferAt(X0, Y0);
                    real* pBaseRightOutputPixel = m_ppOutputImages[_RIGHT_]->GetWritableBufferAt(X0, Y0);

                    uint Counter[3] =
                    { 0 };
                    int SelectingChannelFlag = m_BayerPattern & 0x1;
                    int SelectingShiftingFlag = (X0 & 0x01) ^ ((m_BayerPattern & 0x02) >> 1);
                    for (int y = Y0; y < Y1; ++y, pBaseLeftOutputPixel += ImageWidth, pBaseRightOutputPixel += ImageWidth)
                    {
                        real* pLeftOutputPixel = pBaseLeftOutputPixel;
                        real* pRightOutputPixel = pBaseRightOutputPixel;
                        int BlueOn = (y & 0x1) ^ SelectingChannelFlag;
                        for (int x = X0; x < X1; ++x, ++pLeftOutputPixel, ++pRightOutputPixel)
                        {
                            int ChannelSelector = ((x & 0x1) ^ SelectingShiftingFlag) | BlueOn;
                            m_LeftChannelDescriptiveStatistics.m_Mean.AddChannelValue(ChannelSelector, *pLeftOutputPixel);
                            m_LeftChannelDescriptiveStatistics.m_MaximalIntensity.SetChannelToMaximalValue(ChannelSelector, *pLeftOutputPixel);
                            m_LeftChannelDescriptiveStatistics.m_MinimalIntensity.SetChannelToMaximalValue(ChannelSelector, *pLeftOutputPixel);
                            m_RightChannelDescriptiveStatistics.m_Mean.AddChannelValue(ChannelSelector, *pRightOutputPixel);
                            m_RightChannelDescriptiveStatistics.m_MaximalIntensity.SetChannelToMaximalValue(ChannelSelector, *pRightOutputPixel);
                            m_RightChannelDescriptiveStatistics.m_MinimalIntensity.SetChannelToMaximalValue(ChannelSelector, *pRightOutputPixel);
                            ++Counter[ChannelSelector];
                        }
                    }
                    const real Normalization[3] =
                    { _REAL_ONE_ / real(Counter[0]), _REAL_ONE_ / real(Counter[1]), _REAL_ONE_ / real(Counter[2]) };
                    m_LeftChannelDescriptiveStatistics.m_Mean.NonIsotropicScale(Normalization[0], Normalization[1], Normalization[2]);
                    m_RightChannelDescriptiveStatistics.m_Mean.NonIsotropicScale(Normalization[0], Normalization[1], Normalization[2]);
                    m_LeftChannelDescriptiveStatistics.m_Range.SetRange(m_LeftChannelDescriptiveStatistics.m_MaximalIntensity, m_LeftChannelDescriptiveStatistics.m_MinimalIntensity);
                    m_RightChannelDescriptiveStatistics.m_Range.SetRange(m_RightChannelDescriptiveStatistics.m_MaximalIntensity, m_RightChannelDescriptiveStatistics.m_MinimalIntensity);

                    if (m_Paramters.m_SynthesisMode == Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eHomomorphic)
                    {
                        real LeftMaximalChannelLinealRange = _REAL_ZERO_, RightMaximalChannelLinealRange = _REAL_ZERO_;
                        for (int i = 0; i < 3; ++i)
                        {
                            const real LeftChannelLinealRange = Linearization(m_LeftChannelDescriptiveStatistics.m_MaximalIntensity.GetReadOnlyChannelReferenceByIndex(i)) - Linearization(m_LeftChannelDescriptiveStatistics.m_MinimalIntensity.GetReadOnlyChannelReferenceByIndex(i));
                            if (LeftChannelLinealRange > LeftMaximalChannelLinealRange)
                                LeftMaximalChannelLinealRange = LeftChannelLinealRange;
                            const real RightChannelLinealRange = Linearization(m_RightChannelDescriptiveStatistics.m_MaximalIntensity.GetReadOnlyChannelReferenceByIndex(i)) - Linearization(m_RightChannelDescriptiveStatistics.m_MinimalIntensity.GetReadOnlyChannelReferenceByIndex(i));
                            if (RightChannelLinealRange > RightMaximalChannelLinealRange)
                                RightMaximalChannelLinealRange = RightChannelLinealRange;
                        }
                        m_LeftChannelDescriptiveStatistics.m_EncondingBits = UpperToInteger(RealLog2(RealCeil(LeftMaximalChannelLinealRange)));
                        m_RightChannelDescriptiveStatistics.m_EncondingBits = UpperToInteger(RealLog2(RealCeil(RightMaximalChannelLinealRange)));
                    }
                    else
                    {
                        m_LeftChannelDescriptiveStatistics.m_EncondingBits = UpperToInteger(RealLog2(RealCeil(m_LeftChannelDescriptiveStatistics.m_Range.GetMaximalChannelValue())));
                        m_RightChannelDescriptiveStatistics.m_EncondingBits = UpperToInteger(RealLog2(RealCeil(m_RightChannelDescriptiveStatistics.m_Range.GetMaximalChannelValue())));
                    }

                    STOP_PROCESS_EXECUTION_LOG

                    return true;
                }
                return false;
            }

            const bool CStereoImageBayerHDRSynthesizer::LoadSampledResponseIdealFunction()
            {
                if (m_IsEnabled)
                {
                    bool Result = false;
                    switch (m_Paramters.m_WeightingMode)
                    {
                        case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eTriangularKernel:
                            Result = m_pLeftRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode);
                            Result &= m_pLeftGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode);
                            Result &= m_pLeftBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode);
                            Result &= m_pRightRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode);
                            Result &= m_pRightGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode);
                            Result &= m_pRightBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode);
                            break;
                        case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eEpanechnikovKernel:
                            Result = m_pLeftRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode);
                            Result &= m_pLeftGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode);
                            Result &= m_pLeftBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode);
                            Result &= m_pRightRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode);
                            Result &= m_pRightGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode);
                            Result &= m_pRightBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode);
                            break;
                        case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eGaussianKernel:
                            Result = m_pLeftRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode, m_Paramters.m_SigmaGaussianKernel);
                            Result &= m_pLeftGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode, m_Paramters.m_SigmaGaussianKernel);
                            Result &= m_pLeftBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode, m_Paramters.m_SigmaGaussianKernel);
                            Result &= m_pRightRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode, m_Paramters.m_SigmaGaussianKernel);
                            Result &= m_pRightGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode, m_Paramters.m_SigmaGaussianKernel);
                            Result &= m_pRightBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode, m_Paramters.m_SigmaGaussianKernel);
                            break;
                        case Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eGaussianBoxKernel:
                            Result = m_pLeftRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode, m_Paramters.m_GaussianBoxIntensityA, m_Paramters.m_GaussianBoxIntensityB, m_Paramters.m_SigmaGaussianKernelA, m_Paramters.m_SigmaGaussianKernelB);
                            Result &= m_pLeftGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode, m_Paramters.m_GaussianBoxIntensityA, m_Paramters.m_GaussianBoxIntensityB, m_Paramters.m_SigmaGaussianKernelA, m_Paramters.m_SigmaGaussianKernelB);
                            Result &= m_pLeftBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode, m_Paramters.m_GaussianBoxIntensityA, m_Paramters.m_GaussianBoxIntensityB, m_Paramters.m_SigmaGaussianKernelA, m_Paramters.m_SigmaGaussianKernelB);
                            Result &= m_pRightRedChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode, m_Paramters.m_GaussianBoxIntensityA, m_Paramters.m_GaussianBoxIntensityB, m_Paramters.m_SigmaGaussianKernelA, m_Paramters.m_SigmaGaussianKernelB);
                            Result &= m_pRightGreenChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode, m_Paramters.m_GaussianBoxIntensityA, m_Paramters.m_GaussianBoxIntensityB, m_Paramters.m_SigmaGaussianKernelA, m_Paramters.m_SigmaGaussianKernelB);
                            Result &= m_pRightBlueChannelRadiometricResponseFunction->LoadIdealSampledResponseFunction(1, m_Paramters.m_WeightingMode, m_Paramters.m_GaussianBoxIntensityA, m_Paramters.m_GaussianBoxIntensityB, m_Paramters.m_SigmaGaussianKernelA, m_Paramters.m_SigmaGaussianKernelB);
                            break;
                    }
                    if (Result)
                    {
                        byte LeftRedMin = RealCeil(m_pLeftRedChannelRadiometricResponseFunction->GetValueAtMinimalWeigthingKernelFromDarkside(real(0.001)));
                        byte LeftRedMax = byte(m_pLeftRedChannelRadiometricResponseFunction->GetValueAtMinimalWeigthingKernelFromLightSide(real(0.001)));
                        byte LeftGreenMin = RealCeil(m_pLeftGreenChannelRadiometricResponseFunction->GetValueAtMinimalWeigthingKernelFromDarkside(real(0.001)));
                        byte LeftGreenMax = byte(m_pLeftGreenChannelRadiometricResponseFunction->GetValueAtMinimalWeigthingKernelFromLightSide(real(0.001)));
                        byte LeftBlueMin = RealCeil(m_pLeftBlueChannelRadiometricResponseFunction->GetValueAtMinimalWeigthingKernelFromDarkside(real(0.001)));
                        byte LeftBlueMax = byte(m_pLeftBlueChannelRadiometricResponseFunction->GetValueAtMinimalWeigthingKernelFromLightSide(real(0.001)));
                        m_LeftMinimalValue.SetValue(TMax(LeftRedMin, TMax(m_Paramters.m_MinimalIntegrationValue, (byte)RealCeil(m_pLeftRedChannelRadiometricResponseFunction->GetMinimalIntegrationValue()))), TMax(LeftGreenMin, TMax(m_Paramters.m_MinimalIntegrationValue, (byte)RealCeil(m_pLeftGreenChannelRadiometricResponseFunction->GetMinimalIntegrationValue()))), TMax(LeftBlueMin, TMax(m_Paramters.m_MinimalIntegrationValue, (byte)RealCeil(m_pLeftBlueChannelRadiometricResponseFunction->GetMinimalIntegrationValue()))));
                        m_LeftMaximalValue.SetValue(TMin(m_Paramters.m_MaximalIntegrationValue, LeftRedMax), TMin(m_Paramters.m_MaximalIntegrationValue, LeftGreenMax), TMin(m_Paramters.m_MaximalIntegrationValue, LeftBlueMax));
                        m_pLimitedLeftWeigthingKernel = m_pLeftGreenChannelRadiometricResponseFunction->GetWeigthingKernel(m_LeftMinimalValue.GetMaximalChannelValue(), m_LeftMaximalValue.GetMinimalChannelValue());

                        byte RightRedMin = RealCeil(m_pRightRedChannelRadiometricResponseFunction->GetValueAtMinimalWeigthingKernelFromDarkside(real(0.001)));
                        byte RightRedMax = byte(m_pRightRedChannelRadiometricResponseFunction->GetValueAtMinimalWeigthingKernelFromLightSide(real(0.001)));
                        byte RightGreenMin = RealCeil(m_pRightGreenChannelRadiometricResponseFunction->GetValueAtMinimalWeigthingKernelFromDarkside(real(0.001)));
                        byte RightGreenMax = byte(m_pRightGreenChannelRadiometricResponseFunction->GetValueAtMinimalWeigthingKernelFromLightSide(real(0.001)));
                        byte RightBlueMin = RealCeil(m_pRightBlueChannelRadiometricResponseFunction->GetValueAtMinimalWeigthingKernelFromDarkside(real(0.001)));
                        byte RightBlueMax = byte(m_pRightBlueChannelRadiometricResponseFunction->GetValueAtMinimalWeigthingKernelFromLightSide(real(0.001)));
                        m_RightMinimalValue.SetValue(TMax(RightRedMin, TMax(m_Paramters.m_MinimalIntegrationValue, (byte)RealCeil(m_pRightRedChannelRadiometricResponseFunction->GetMinimalIntegrationValue()))), TMax(RightGreenMin, TMax(m_Paramters.m_MinimalIntegrationValue, (byte)RealCeil(m_pRightGreenChannelRadiometricResponseFunction->GetMinimalIntegrationValue()))), TMax(RightBlueMin,
                                TMax(m_Paramters.m_MinimalIntegrationValue, (byte)RealCeil(m_pRightBlueChannelRadiometricResponseFunction->GetMinimalIntegrationValue()))));
                        m_RightMaximalValue.SetValue(TMin(m_Paramters.m_MaximalIntegrationValue, RightRedMax), TMin(m_Paramters.m_MaximalIntegrationValue, RightGreenMax), TMin(m_Paramters.m_MaximalIntegrationValue, RightBlueMax));
                        m_pLimitedRightWeigthingKernel = m_pRightGreenChannelRadiometricResponseFunction->GetWeigthingKernel(m_RightMinimalValue.GetMaximalChannelValue(), m_RightMaximalValue.GetMinimalChannelValue());
                    }
                    m_IsSampledResponseIdealFunctionLoaded = Result;
                    return Result;
                }
                return false;
            }

            const bool CStereoImageBayerHDRSynthesizer::LoadIntegrationTables(const list<real>& IntegrationPeriods)
            {
                bool Result = m_pLeftRedChannelRadiometricResponseFunction->LoadIntegrationTable(IntegrationPeriods);
                Result &= m_pLeftGreenChannelRadiometricResponseFunction->LoadIntegrationTable(IntegrationPeriods);
                Result &= m_pLeftBlueChannelRadiometricResponseFunction->LoadIntegrationTable(IntegrationPeriods);
                Result &= m_pRightRedChannelRadiometricResponseFunction->LoadIntegrationTable(IntegrationPeriods);
                Result &= m_pRightGreenChannelRadiometricResponseFunction->LoadIntegrationTable(IntegrationPeriods);
                Result &= m_pRightBlueChannelRadiometricResponseFunction->LoadIntegrationTable(IntegrationPeriods);
                return Result;
            }

            const real CStereoImageBayerHDRSynthesizer::ContinuousTriangularWeigthingKernel(const real Intensity) const
            {
                return _REAL_ONE_ - RealAbs(Intensity - _REAL_127_5_) * _REAL_1_127_5_;
            }

            const real CStereoImageBayerHDRSynthesizer::ContinuousEpanechnikovWeigthingKernel(const real Intensity) const
            {
                const real NormalizedDeviation = RealAbs(Intensity - _REAL_127_5_) * _REAL_1_127_5_;
                return _REAL_ONE_ - NormalizedDeviation * NormalizedDeviation;
            }

            const real CStereoImageBayerHDRSynthesizer::ContinuousGaussianWeigthingKernel(const real Intensity, const real ExponentFactor) const
            {
                const real Deviation = Intensity - _REAL_127_5_;
                return RealExp(Deviation * Deviation * ExponentFactor);
            }

            const real CStereoImageBayerHDRSynthesizer::ContinuousGaussianBoxWeigthingKernel(const real Intensity, const real IntensityA, const real IntensityB, const real ExponentFactorA, const real ExponentFactorB) const
            {
                if ((Intensity >= IntensityA) && (Intensity <= IntensityB))
                    return _REAL_ONE_;
                else if (Intensity < IntensityA)
                {
                    const real Deviation = Intensity - IntensityA;
                    return RealExp(Deviation * Deviation * ExponentFactorA);
                }
                else
                {
                    const real Deviation = Intensity - IntensityB;
                    return RealExp(Deviation * Deviation * ExponentFactorB);
                }
            }

            const real CStereoImageBayerHDRSynthesizer::Linearization(const real X) const
            {
#ifdef _STEREO_IMAGE_BAYER_HDR_SYNTHESIZER_USE_LOOKUP_TABLE_
                return (X >= s_ELT.m_MaximalArgumentValue) ? RealExp(X) : s_ELT.m_pExp[RoundToInteger(s_ELT.m_SamplesPerUnit * X)];
#else
                return RealExp(X);
#endif
            }
        }
    }
}
*/
