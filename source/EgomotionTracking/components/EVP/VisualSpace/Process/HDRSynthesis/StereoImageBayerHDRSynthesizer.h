/*
 * StereoImageBayerHDRSynthesizer.h
 *
 *  Created on: 11.04.2011
 *      Author: gonzalez
 */
/*
#ifndef STEREOIMAGEBAYERHDRSYNTHESIZER_H_
#define STEREOIMAGEBAYERHDRSYNTHESIZER_H_

#include "../../../Foundation/Mathematics/1D/ExponentialLookUpTable.h"
#include "../../../Foundation/DataTypes/BayerPattern.h"
#include "../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../../Visualization/Miscellaneous/TristimulusColorMap.h"
#include "../../Cameras/RadiometricCalibration/MultiChannelRadiometricResponseFunction.h"
#include "../Base/StereoImageProcess.h"

//DEFAULT PARAMETER VALUES
#define _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SYNTHESIS_MODE_                  Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eHomomorphic
#define _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_WEIGHTING_MODE_                  Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::eGaussianKernel
#define _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SIGMA_GAUSSIAN_KERNEL_           real(45.0)
#define _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_INTENSITY_A_GAUSSIAN_KERNEL_     real(16.0)
#define _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_INTENSITY_B_GAUSSIAN_KERNEL_     real(240.0)
#define _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SIGMA_A_GAUSSIAN_KERNEL_         real(4.0)
#define _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_SIGMA_B_GAUSSIAN_KERNEL_         real(4.0)
#define _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_MINIMAL_INTEGRATION_VALUE_       byte(1)
#define _STEREOIMAGE_BAYER_MULTI_CHANNEL_HDR_SYNTHESIZER_DEFAULT_VALUE_MAXIMAL_INTEGRATION_VALUE_       byte(254)

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            class CStereoImageBayerHDRSynthesizer: public TStereoImageProcess<byte, real, CDiscreteTristimulusPixel>
            {
                public:

                    struct Paramters
                    {
                            Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::SynthesisMode m_SynthesisMode;
                            Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction::WeightingMode m_WeightingMode;
                            real m_SigmaGaussianKernel;
                            real m_GaussianBoxIntensityA;
                            real m_GaussianBoxIntensityB;
                            real m_SigmaGaussianKernelA;
                            real m_SigmaGaussianKernelB;
                            byte m_MinimalIntegrationValue;
                            byte m_MaximalIntegrationValue;
                    };

                    struct ChannelDescriptiveStatistics
                    {
                            CContinuousTristimulusPixel m_MaximalIntensity;
                            CContinuousTristimulusPixel m_MinimalIntensity;
                            CContinuousTristimulusPixel m_Range;
                            CContinuousTristimulusPixel m_Mean;
                            uint m_EncondingBits;
                    };

                    struct ExposureIntegration
                    {
                            real m_LeftEnergy;
                            real m_LeftDelta;
                            real m_LeftAccumulator;
                            real m_RightEnergy;
                            real m_RightDelta;
                            real m_RightAccumulator;
                            uint m_ShuttingTimeIndex;
                    };

                    enum ExposureIntegrationSampleType
                    {
                        eCombined, ePerChannel
                    };

                    enum ExposureIntegrationPlanType
                    {
                        eGradient, eEnergy, eIsoArcLength
                    };

                    CStereoImageBayerHDRSynthesizer(const BayerPatternType Type, Cameras::RadiometricCalibration::CMultiChannelRadiometricResponseFunction** ppRadiometricResponseFunctions, const CImageActiveZone* pActiveZone, const TImage<byte>** ppInputImages);
                    virtual ~CStereoImageBayerHDRSynthesizer();

                    const bool AddSceneAnalysisSample(const Identifier TrialId, const uint ShuttingTimeIndex, const ExposureIntegrationSampleType SampleType);
                    const bool AddExposureAndChromatic(const Identifier TrialId, const uint ShuttingTimeIndex, const bool EnableChromaticIntegration);
                    const bool Synthesis(const Identifier TrialId, const bool EnableChromaticSynthesis);
                    const bool ComputeDescriptiveStatistics(const Identifier TrialId);

                    void ClearExposures();

                    inline const uint GetTotalExposures() const
                    {
                        return m_TotalExposures;
                    }

                    inline const TImage<real>** GetAccumulatorRadianceMaps() const
                    {
                        return const_cast<const TImage<real>**> (m_ppAccumulatorRadianceMaps);
                    }

                    inline const TImage<real>** GetAccumulatorWeightingMaps() const
                    {
                        return const_cast<const TImage<real>**> (m_ppAccumulatorWeightingMaps);
                    }

                    inline const TImage<real>** GetChromaticMaps() const
                    {
                        return const_cast<const TImage<real>**> (m_ppAccumulatorChromaticMaps);
                    }

                    inline Paramters* GetParamters()
                    {
                        return &m_Paramters;
                    }

                    const bool LoadSampledResponseIdealFunction();
                    const bool LoadIntegrationTables(const list<real>& IntegrationPeriods);

                    const bool CreateExposureIntegrationPlan(const uint TotalPlannedExposures, const ExposureIntegrationPlanType PlanType, const bool Verbose);

                    inline const list<ExposureIntegration>& GetPlannExposureIntegrations() const
                    {
                        return m_PlannExposureIntegrations;
                    }

                    inline const BayerPatternType GetBayerPatternType() const
                    {
                        return m_BayerPattern;
                    }

                    inline const byte GetMinimalIntegrableValue() const
                    {
                        return m_IsSampledResponseIdealFunctionLoaded ? TMax(m_LeftMinimalValue.GetMaximalChannelValue(), m_RightMinimalValue.GetMaximalChannelValue()) : 0;
                    }

                    inline const ChannelDescriptiveStatistics& GetLeftChannelDescriptiveStatistics() const
                    {
                        return m_LeftChannelDescriptiveStatistics;
                    }

                    inline const ChannelDescriptiveStatistics& GetRightChannelDescriptiveStatistics() const
                    {
                        return m_RightChannelDescriptiveStatistics;
                    }

                protected:

                    inline void DisplayExposureIntegrations() const ;
                    inline void DisplayPlannedExposureIntegrations() const ;
                    inline const real ContinuousTriangularWeigthingKernel(const real Intensity) const ;
                    inline const real ContinuousEpanechnikovWeigthingKernel(const real Intensity) const ;
                    inline const real ContinuousGaussianWeigthingKernel(const real Intensity, const real ExponentFactor) const ;
                    inline const real ContinuousGaussianBoxWeigthingKernel(const real Intensity, const real IntensityA, const real IntensityB, const real ExponentFactorA, const real ExponentFactorB) const;
                    inline void AddChromaticSample(const real LeftWeight, const real RightWeight);
                    inline void AddExposureIntegration(const real LeftWeight, const real RightWeight, const uint ShuttingTimeIndex, const ExposureIntegrationSampleType SampleType);
                    inline void NormalizeChromaticSamples();
                    inline const real Linearization(const real X) const ;

                    Paramters m_Paramters;

                    const BayerPatternType m_BayerPattern;
                    uint m_TotalExposures;
                    bool m_IsSampledResponseIdealFunctionLoaded;
                    real m_ActiveAreaNormalization;
                    real m_LeftWeightAccumulator;
                    real m_RightWeightAccumulator;
                    CDiscreteTristimulusPixel m_LeftMinimalValue;
                    CDiscreteTristimulusPixel m_RightMinimalValue;
                    CDiscreteTristimulusPixel m_LeftMaximalValue;
                    CDiscreteTristimulusPixel m_RightMaximalValue;
                    const real* m_pLimitedLeftWeigthingKernel;
                    const real* m_pLimitedRightWeigthingKernel;
                    TImage<real>** m_ppAccumulatorChromaticMaps;
                    TImage<real>** m_ppAccumulatorRadianceMaps;
                    TImage<real>** m_ppAccumulatorWeightingMaps;
                    Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction* m_pLeftRedChannelRadiometricResponseFunction;
                    Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction* m_pLeftGreenChannelRadiometricResponseFunction;
                    Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction* m_pLeftBlueChannelRadiometricResponseFunction;
                    Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction* m_pRightRedChannelRadiometricResponseFunction;
                    Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction* m_pRightGreenChannelRadiometricResponseFunction;
                    Cameras::RadiometricCalibration::CSingleChannelRadiometricResponseFunction* m_pRightBlueChannelRadiometricResponseFunction;
                    ChannelDescriptiveStatistics m_LeftChannelDescriptiveStatistics;
                    ChannelDescriptiveStatistics m_RightChannelDescriptiveStatistics;
                    vector<ExposureIntegration> m_ExposureIntegrations;
                    list<ExposureIntegration> m_PlannExposureIntegrations;

#ifdef _STEREO_IMAGE_BAYER_HDR_SYNTHESIZER_USE_LOOKUP_TABLE_
                    DECLARE_EXPONENTIAL_LOOKUP_TABLE
#endif

            };
        }
    }
}

#endif *//* STEREOIMAGEBAYERHDRSYNTHESIZER_H_ */
