/*
 * MonocularLensUndistortion.h
 *
 *  Created on: 29.02.2012
 *      Author: gonzalez
 */

#pragma once

#include "../../Base/ImageProcessBase.h"
#include "../../../Cameras/GeometricCalibration/CameraGeometricCalibration.h"
#include "../../../Cameras/GeometricCalibration/CameraLensUndistorter.h"

#define MLU TImageProcessBase<PixelDataType, PixelDataType, CDiscreteTristimulusPixel>

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                template<typename PixelDataType> class TMonocularLensUndistortion: public TImageProcessBase<PixelDataType, PixelDataType, CDiscreteTristimulusPixel>
                {
                public:

                    class CParameters: public CParametersBase
                    {
                    public:

                        enum UndistortionMode
                        {
                            eFullImage = Cameras::GeometricCalibration::CCameraLensUndistorter::eFullImage, eMaximalRectangularImageRegion = Cameras::GeometricCalibration::CCameraLensUndistorter::eMaximalRectangularImageRegion, eClippingImageRegion = Cameras::GeometricCalibration::CCameraLensUndistorter::eClippingImageRegion
                        };

                        CParameters(TMonocularLensUndistortion<PixelDataType>* pHost) :
                            CParametersBase(), m_pHost(pHost), m_UndistortionMode(eFullImage)
                        {
                        }

                        ~CParameters()
                        {
                        }

                        const CParametersBase::ParameterChangeResult SetUndistortionMode(const UndistortionMode Mode, const bool Wait = true)
                        {
                            if (!((Mode == eFullImage) || (Mode == eMaximalRectangularImageRegion) || (Mode == eClippingImageRegion)))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (Mode == m_UndistortionMode)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_UndistortionMode = Mode;
                                m_pHost->UpdateUndistortionMode();
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        const UndistortionMode GetUndistortionMode() const
                        {
                            return m_UndistortionMode;
                        }

                    protected:

                        TMonocularLensUndistortion<PixelDataType>* m_pHost;
                        UndistortionMode m_UndistortionMode;
                    };

                    TMonocularLensUndistortion(const Cameras::GeometricCalibration::CCameraGeometricCalibration* pCameraGeometricCalibration, CImageActiveZone* pActiveZone, const TImage<PixelDataType>* pInputImage, Threading::CMutex* pInputImageMutex) :
                        TImageProcessBase<PixelDataType, PixelDataType, CDiscreteTristimulusPixel> (CImageProcessBase::eLensUndistortion, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this), m_pCameraGeometricCalibration(pCameraGeometricCalibration), m_pCameraLensUndistorter(NULL)
                    {
                        if (MLU::m_IsEnabled)
                        {
                            m_pCameraLensUndistorter = new Cameras::GeometricCalibration::CCameraLensUndistorter();
                            if (m_pCameraLensUndistorter->LoadFromCalibration(m_pCameraGeometricCalibration, MLU::m_pInputActiveZone))
                            {
                                MLU::m_pInternalInputImage = new TImage<PixelDataType> (MLU::m_pInputActiveZone->GetSize());
                                UpdateUndistortionMode();
                            }
                        }
                    }

                    virtual ~TMonocularLensUndistortion()
                    {
                        MLU::m_OutputActiveZone.Disconnect(true, true);
                        RELEASE_OBJECT(MLU::m_pOutputImage)
                        RELEASE_OBJECT(MLU::m_pDisplayImage)
                        RELEASE_OBJECT(MLU::m_pInternalInputImage)
                        RELEASE_OBJECT(m_pCameraLensUndistorter)
                    }

                    virtual const bool Execute(const Identifier TrialId)
                    {
                        if (MLU::m_IsEnabled)
                        {
                            if (MLU::StartExecuting(TrialId))
                            {
                                if (MLU::m_IsLogEnabled)
                                {
                                    MLU::StartExecutionLog("Execute", TrialId);
                                }

                                MLU::LoadInputImage();

                                bool Undistortion = false;
                                switch (m_Parameters.GetUndistortionMode())
                                {
                                    case CParameters::eFullImage:
                                        Undistortion = m_pCameraLensUndistorter->FullImageUndistort(MLU::m_pInternalInputImage, MLU::m_pOutputImage);
                                        break;
                                    case CParameters::eMaximalRectangularImageRegion:
                                        Undistortion = m_pCameraLensUndistorter->MaximalRectangularImageRegionUndistort(MLU::m_pInternalInputImage, MLU::m_pOutputImage);
                                        break;
                                    case CParameters::eClippingImageRegion:
                                        Undistortion = m_pCameraLensUndistorter->ClippingImageRegionUndistort(MLU::m_pInternalInputImage, MLU::m_pOutputImage);
                                        break;
                                }

                                if (MLU::m_IsLogEnabled)
                                {
                                    MLU::StopExecutionLog();
                                }

                                return MLU::FinishExecution() && Undistortion;
                            }
                        }
                        return false;
                    }

                    virtual const bool Display()
                    {
                        if (MLU::m_IsEnabled && MLU::m_OutputActiveZone.IsEnabled() && MLU::GetTotalExecutions())
                        {
                            if (MLU::StartDisplaying())
                            {
                                MLU::m_pDisplayImage->Clear();

                                const PixelDataType* pOutPixel = MLU::m_pOutputImage->GetBeginReadOnlyBuffer();
                                CDiscreteTristimulusPixel* pDisplayPixel = MLU::m_pDisplayImage->GetBeginWritableBuffer();
                                const CDiscreteTristimulusPixel* const pDisplayPixelEnd = MLU::m_pDisplayImage->GetEndReadOnlyBuffer();
                                while (pDisplayPixel < pDisplayPixelEnd)
                                {
                                    *pDisplayPixel++ = *pOutPixel++;
                                }

                                return MLU::FinishDisplaying();
                            }
                        }
                        return false;
                    }

                    virtual CParameters* GetParameters()
                    {
                        return MLU::m_IsEnabled ? &m_Parameters : NULL;
                    }

                    const TImage<bool>* GetFullUndistortedImageMask(const uint Margin) const
                    {
                        return m_pCameraLensUndistorter->GetFullUndistortedImageMask(Margin);
                    }

                    const Mathematics::_2D::CVector2D& GetReferenceUpperLeftCorner() const
                    {
                        return m_pCameraLensUndistorter->GetReferenceUpperLeftCorner();
                    }

                protected:

                    friend class CParameters;

                    void UpdateUndistortionMode()
                    {
                        Cameras::GeometricCalibration::CCameraLensUndistorter::UndistortionMode Mode = Cameras::GeometricCalibration::CCameraLensUndistorter::UndistortionMode(m_Parameters.GetUndistortionMode());
                        CImageActiveZone* pActiveZone = m_pCameraLensUndistorter->GetWritableActiveZoneByMode(Mode);
                        const CImageSize ActiveSize = pActiveZone->GetActiveSize();
                        MLU::m_OutputActiveZone.Set(ActiveSize);
                        if (MLU::m_pOutputImage)
                        {
                            MLU::m_pOutputImage->Resize(ActiveSize);
                        }
                        else
                        {
                            MLU::m_pOutputImage = new TImage<PixelDataType> (ActiveSize);
                        }
                        if (MLU::m_pDisplayImage)
                        {
                            MLU::m_pDisplayImage->Resize(ActiveSize);
                        }
                        else
                        {
                            MLU::m_pDisplayImage = new TImage<CDiscreteTristimulusPixel> (ActiveSize);
                        }
                    }

                    CParameters m_Parameters;
                    const Cameras::GeometricCalibration::CCameraGeometricCalibration* m_pCameraGeometricCalibration;
                    Cameras::GeometricCalibration::CCameraLensUndistorter* m_pCameraLensUndistorter;
                    const TImage<bool>* m_pFullUndistortedImageMask;
                };
            }
        }
    }
}

