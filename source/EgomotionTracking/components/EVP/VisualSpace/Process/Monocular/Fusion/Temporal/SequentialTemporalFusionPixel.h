/*
 * TemporalFusionPixel.h
 *
 *  Created on: Apr 20, 2012
 *      Author: david
 */

#pragma once

#include "../../../../../GlobalSettings.h"
#include "../../../../../Foundation/Mathematics/1D/Common1D.h"
#include "../../../../../Foundation/DataTypes/PixelLocation.h"
#include "../../../../../Foundation/Files/OutFile.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                namespace Fusion
                {
                    namespace Temporal
                    {
                        class CSequentialTemporalFusionPixel
                        {
                        public:

                            CSequentialTemporalFusionPixel();
                            ~CSequentialTemporalFusionPixel();

                            void SetMaximalSamplingScope(const uint MaximalSamplingScope);

                            void AddIntialSample(const byte Intensity);
                            void AddSubsequentSample(const byte Intensity);
                            void ClearSamples();

                            uint GetTotalSamples() const;
                            const vector<byte>& GetSamples() const;
                            byte GetSampleByIndex(const uint Index) const;

                            uint GetMinimal() const;
                            uint GetMaximal() const;
                            uint GetRange() const;
                            real GetMean() const;
                            real GetMedian() const;
                            real GetStandardDeviation() const;
                            real GetDispersion() const;
                            real GetFusion() const;
                            real GetSignalToNoiseRatio() const;
                            real MeanFusion();
                            real EpanechnikovFusion(const real SamplingPrecision, const real OptimizationPrecision);
                            real GaussianFusion(const real SamplingPrecision, const real OptimizationPrecision);
                            bool ExportToFile(const_string pPathFileName, const real SamplingPrecision, const CPixelLocation* pLocation) const;

                        protected:

                            uint m_Accumulator;
                            uint m_SquareAccumulator;
                            uint m_Minimal;
                            uint m_Maximal;
                            real m_Fusion;
                            vector<byte> m_IntensitySamples;

                        };
                    }
                }
            }
        }
    }
}

