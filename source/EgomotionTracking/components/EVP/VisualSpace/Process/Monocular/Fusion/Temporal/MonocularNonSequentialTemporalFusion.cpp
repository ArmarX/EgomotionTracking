/*
 * MonocularNonSequentialTemporalFusion.cpp
 *
 *  Created on: Apr 24, 2012
 *      Author: david
 */

#include "MonocularNonSequentialTemporalFusion.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                namespace Fusion
                {
                    namespace Temporal
                    {

                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //CMonocularNonSequentialTemporalFusion::CParameters
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        CMonocularNonSequentialTemporalFusion::CParameters::~CParameters()
                        {
                            RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                        }

                        CParametersBase::ParameterChangeResult CMonocularNonSequentialTemporalFusion::CParameters::SetSamplesPerFusion(const uint SamplesPerFusion, const bool Wait)
                        {
                            if (SamplesPerFusion < 2)
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (SamplesPerFusion == m_SamplesPerFusion)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_SamplesPerFusion = SamplesPerFusion;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        uint CMonocularNonSequentialTemporalFusion::CParameters::GetSamplesPerFusion() const
                        {
                            return m_SamplesPerFusion;
                        }

                        CParametersBase::ParameterChangeResult CMonocularNonSequentialTemporalFusion::CParameters::SetSamplingMode(const SamplingMode Mode, const bool Wait)
                        {
                            if (!((Mode == CParameters::eDirect) || (Mode == CParameters::eIndirect)))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (Mode == m_SamplingMode)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_SamplingMode = Mode;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        CMonocularNonSequentialTemporalFusion::CParameters::SamplingMode CMonocularNonSequentialTemporalFusion::CParameters::GetSamplingMode() const
                        {
                            return m_SamplingMode;
                        }

                        CParametersBase::ParameterChangeResult CMonocularNonSequentialTemporalFusion::CParameters::SetFusionMethod(const FusionMethod Method, const bool Wait)
                        {
                            if (!((Method == CParameters::eMean) || (Method == CParameters::eDiscreteEpanechnikov) || (Method == CParameters::eDiscreteGaussian) || (Method == CParameters::eContinuousEpanechnikov) || (Method == CParameters::eContinuousGaussian)))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (Method == m_FusionMethod)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_FusionMethod = Method;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        CMonocularNonSequentialTemporalFusion::CParameters::FusionMethod CMonocularNonSequentialTemporalFusion::CParameters::GetFusionMethod() const
                        {
                            return m_FusionMethod;
                        }

                        CParametersBase::ParameterChangeResult CMonocularNonSequentialTemporalFusion::CParameters::SetDisplayChannel(const DisplayChannel Channel, const bool Wait)
                        {
                            if (!((Channel == CParameters::eRange) || (Channel == CParameters::eStandardDeviation) || (Channel == CParameters::eFusion) || (Channel == CParameters::eDispersion) || (Channel == CParameters::eSignalToNoiseRatio) || (Channel == CParameters::eBandWidth) || (Channel == CParameters::eGlobalMaxima)))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (Channel == m_DisplayChannel)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_DisplayChannel = Channel;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        CMonocularNonSequentialTemporalFusion::CParameters::DisplayChannel CMonocularNonSequentialTemporalFusion::CParameters::GetDisplayChannel() const
                        {
                            return m_DisplayChannel;
                        }

                        CParametersBase::ParameterChangeResult CMonocularNonSequentialTemporalFusion::CParameters::SetDisplayMode(const DisplayMode Mode, const bool Wait)
                        {
                            if (!((Mode == CParameters::eRoundedIntensity) || (Mode == CParameters::eScaledIntensity) || (Mode == CParameters::eColorMapped)))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (Mode == m_DisplayMode)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_DisplayMode = Mode;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        CMonocularNonSequentialTemporalFusion::CParameters::DisplayMode CMonocularNonSequentialTemporalFusion::CParameters::GetDisplayMode() const
                        {
                            return m_DisplayMode;
                        }

                        CParametersBase::ParameterChangeResult CMonocularNonSequentialTemporalFusion::CParameters::SetAutoCleanEnabled(const bool AutoCleanEnabled, const bool Wait)
                        {
                            if (AutoCleanEnabled == m_AutoCleanEnabled)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_AutoCleanEnabled = AutoCleanEnabled;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        bool CMonocularNonSequentialTemporalFusion::CParameters::GetAutoCleanEnabled() const
                        {
                            return m_AutoCleanEnabled;
                        }

                        CParametersBase::ParameterChangeResult CMonocularNonSequentialTemporalFusion::CParameters::SetOptimizationPrecision(const real OptimizationPrecision, const bool Wait)
                        {
                            if ((OptimizationPrecision <= _REAL_EPSILON_) || (OptimizationPrecision > _REAL_FOURTH_))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (OptimizationPrecision == m_OptimizationPrecision)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_OptimizationPrecision = OptimizationPrecision;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        real CMonocularNonSequentialTemporalFusion::CParameters::GetOptimizationPrecision() const
                        {
                            return m_OptimizationPrecision;
                        }

                        CParametersBase::ParameterChangeResult CMonocularNonSequentialTemporalFusion::CParameters::SetScopeFactor(const real ScopeFactor, const bool Wait)
                        {
                            if ((ScopeFactor <= _REAL_ONE_) || (ScopeFactor > real(4.0)))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (ScopeFactor == m_ScopeFactor)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_ScopeFactor = ScopeFactor;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        real CMonocularNonSequentialTemporalFusion::CParameters::GetScopeFactor() const
                        {
                            return m_ScopeFactor;
                        }

                        CParametersBase::ParameterChangeResult CMonocularNonSequentialTemporalFusion::CParameters::SetColorMap(const Visualization::CTristimulusColorMap* pColorMap, const bool OwnerShip, const bool Wait)
                        {
                            if (!pColorMap)
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (pColorMap == m_pColorMap)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                                m_ColorMapOwnerShip = OwnerShip;
                                m_pColorMap = pColorMap;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        CParametersBase::ParameterChangeResult CMonocularNonSequentialTemporalFusion::CParameters::SetColorMap(const Visualization::CTristimulusColorMap::PredefinedColorMap ColorMap, const bool Wait)
                        {
                            if (!Visualization::CTristimulusColorMap::IsValidPredefinedColorMap(ColorMap))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                                m_ColorMapOwnerShip = true;
                                m_pColorMap = Visualization::CTristimulusColorMap::LoadPredefinedColorMap(ColorMap);
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        const Visualization::CTristimulusColorMap* CMonocularNonSequentialTemporalFusion::CParameters::GetColorMap() const
                        {
                            return m_pColorMap;
                        }

                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //CMonocularNonSequentialTemporalFusion
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        CMonocularNonSequentialTemporalFusion::CMonocularNonSequentialTemporalFusion(CImageActiveZone* pActiveZone, const TImage<byte>* pInputImage, Threading::CMutex* pInputImageMutex) :
                            TImageProcessBase<byte, real, CDiscreteTristimulusPixel>(CImageProcessBase::eDensityFusion, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this), m_pTemporalFusionImage(nullptr), m_TotalLoadedSamples(0), m_pGlobalMaximaIntensityLocations(nullptr)
                        {
                            if (m_IsEnabled)
                            {
                                const CImageSize& Size = m_pInputActiveZone->GetSize();
                                m_pOutputImage = new TImage<real>(Size);
                                m_pDisplayImage = new TImage<CDiscreteTristimulusPixel>(Size);
                                m_pInternalInputImage = new TImage<byte>(Size);
                                m_pTemporalFusionImage = new TImage<CNonSequentialTemporalFusionPixel>(Size);
                                m_pGlobalMaximaIntensityLocations = new uint[256];
                                m_OutputActiveZone.SetConnection(m_pInputActiveZone);
                            }
                        }

                        CMonocularNonSequentialTemporalFusion::~CMonocularNonSequentialTemporalFusion()
                        {
                            ClearPendingImages();
                            m_OutputActiveZone.Disconnect(true, true);
                            RELEASE_OBJECT(m_pOutputImage)
                            RELEASE_OBJECT(m_pDisplayImage)
                            RELEASE_OBJECT(m_pInternalInputImage)
                            RELEASE_OBJECT(m_pTemporalFusionImage)
                            RELEASE_ARRAY(m_pGlobalMaximaIntensityLocations)
                        }

                        bool CMonocularNonSequentialTemporalFusion::Execute(const Identifier TrialId)
                        {
                            if (m_IsEnabled && m_OutputActiveZone.IsEnabled())
                            {
                                if (StartExecuting(TrialId))
                                {
                                    START_PROCESS_EXECUTION_LOG("Execute", TrialId)

                                    LoadInputImage();

                                    switch (m_Parameters.GetSamplingMode())
                                    {
                                        case CParameters::eDirect:
                                            LoadImage(m_pInternalInputImage);
                                            break;
                                        case CParameters::eIndirect:
                                            m_PendingImages.push_back(m_pInternalInputImage->Clone(true));
                                            break;
                                    }

                                    if ((m_TotalLoadedSamples + m_PendingImages.size()) == m_Parameters.GetSamplesPerFusion())
                                    {
                                        if (m_PendingImages.size())
                                        {
                                            LoadPendingImages();
                                        }
                                        switch (m_Parameters.GetFusionMethod())
                                        {
                                            case CParameters::eMean:
                                                MeanFusion();
                                                break;
                                            case CParameters::eDiscreteEpanechnikov:
                                                DiscreteEpanechnikovFusion(m_Parameters.GetScopeFactor());
                                                break;
                                            case CParameters::eDiscreteGaussian:
                                                DiscreteGaussianFusion(m_Parameters.GetScopeFactor());
                                                break;
                                            case CParameters::eContinuousEpanechnikov:
                                                ContinuousEpanechnikovFusion(m_Parameters.GetOptimizationPrecision(), m_Parameters.GetScopeFactor());
                                                break;
                                            case CParameters::eContinuousGaussian:
                                                ContinuousGaussianFusion(m_Parameters.GetOptimizationPrecision(), m_Parameters.GetScopeFactor());
                                                break;
                                        }
                                        //if (m_Parameters.GetAutoCleanEnabled())
                                        //  ClearSamples();
                                        // TODO@ Remove this from similar clases and clean in Pixel AddIntialSample
                                    }

                                    STOP_PROCESS_EXECUTION_LOG

                                    return FinishExecution();
                                }
                            }
                            return false;
                        }

                        bool CMonocularNonSequentialTemporalFusion::Display()
                        {
                            if (m_IsEnabled && m_OutputActiveZone.IsEnabled() && GetTotalExecutions())
                            {
                                if (StartDisplaying())
                                {
                                    bool Result = false;
                                    //m_pDisplayImage->Clear();
                                    switch (m_Parameters.GetDisplayChannel())
                                    {
                                        case CParameters::eRange:
                                        {
                                            const TImage<real> RangeImage = GetRangeImage();
                                            Result = DisplayByMode(&RangeImage, m_Parameters.GetDisplayMode());
                                        }
                                        break;
                                        case CParameters::eStandardDeviation:
                                        {
                                            const TImage<real> RangeImage = GetStandardDeviationImage();
                                            Result = DisplayByMode(&RangeImage, m_Parameters.GetDisplayMode());
                                        }
                                        break;
                                        case CParameters::eFusion:
                                        {
                                            Result = DisplayByMode(m_pOutputImage, m_Parameters.GetDisplayMode());
                                        }
                                        break;
                                        case CParameters::eDispersion:
                                        {
                                            const TImage<real> DispersionImage = GetDispersionImage();
                                            Result = DisplayByMode(&DispersionImage, m_Parameters.GetDisplayMode());
                                        }
                                        break;
                                        case CParameters::eSignalToNoiseRatio:
                                        {
                                            const TImage<real> SignalToNoiseRatioImage = GetSignalToNoiseRatioImage();
                                            Result = DisplayByMode(&SignalToNoiseRatioImage, m_Parameters.GetDisplayMode());
                                        }
                                        break;
                                        case CParameters::eBandWidth:
                                        {
                                            const TImage<real> SignalToNoiseRatioImage = GetSignalToNoiseRatioImage();
                                            Result = DisplayByMode(&SignalToNoiseRatioImage, m_Parameters.GetDisplayMode());
                                        }
                                        break;
                                        case CParameters::eGlobalMaxima:
                                        {
                                            const TImage<real> SignalToNoiseRatioImage = GetGlobalMaximaImage();
                                            Result = DisplayByMode(&SignalToNoiseRatioImage, m_Parameters.GetDisplayMode());
                                        }
                                        break;
                                    }
                                    Result &= FinishDisplaying();
                                    return Result;
                                }
                            }
                            return false;
                        }

                        CMonocularNonSequentialTemporalFusion::CParameters* CMonocularNonSequentialTemporalFusion::GetParameters()
                        {
                            return m_IsEnabled ? &m_Parameters : nullptr;
                        }

                        void CMonocularNonSequentialTemporalFusion::LoadPendingImages()
                        {
                            list<const TImage<byte>*>::const_iterator EndPendingSampleImages = m_PendingImages.end();
                            for (list<const TImage<byte>*>::const_iterator ppPendingImage = m_PendingImages.begin(); ppPendingImage != EndPendingSampleImages; ++ppPendingImage)
                            {
                                const TImage<byte>* pPendingImage = *ppPendingImage;
                                LoadImage(pPendingImage);
                                RELEASE_OBJECT(pPendingImage)
                            }
                            m_PendingImages.clear();
                        }

                        void CMonocularNonSequentialTemporalFusion::ClearPendingImages()
                        {
                            if (m_PendingImages.size())
                            {
                                list<const TImage<byte>*>::const_iterator EndPendingSampleImages = m_PendingImages.end();
                                for (list<const TImage<byte>*>::const_iterator ppPendingImage = m_PendingImages.begin(); ppPendingImage != EndPendingSampleImages; ++ppPendingImage)
                                {
                                    const TImage<byte>* pPendingImage = *ppPendingImage;
                                    RELEASE_OBJECT(pPendingImage)
                                }
                                m_PendingImages.clear();
                            }
                        }

                        void CMonocularNonSequentialTemporalFusion::LoadImage(const TImage<byte>* pSampleImage)
                        {
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CNonSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                const byte* pInputPixel = pSampleImage->GetBeginReadOnlyBuffer();
                                if (m_TotalLoadedSamples++)
                                    for (CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                    {
                                        pTemporalFusionPixel->AddSubsequentSample(*pInputPixel++);
                                    }
                                else
                                    for (CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                    {
                                        pTemporalFusionPixel->AddIntialSample(*pInputPixel++);
                                    }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                const byte* pInputPixel = pSampleImage->GetReadOnlyBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                if (m_TotalLoadedSamples++)
                                    for (uint y = 0; y < ActiveHeight; ++y, pInputPixel += Offset, pTemporalFusionPixel += Offset)
                                        for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                        {
                                            pTemporalFusionPixel->AddSubsequentSample(*pInputPixel++);
                                        }
                                else
                                    for (uint y = 0; y < ActiveHeight; ++y, pInputPixel += Offset, pTemporalFusionPixel += Offset)
                                        for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                        {
                                            pTemporalFusionPixel->AddIntialSample(*pInputPixel++);
                                        }
                            }
                        }

                        void CMonocularNonSequentialTemporalFusion::ClearSamples()
                        {
                            m_TotalLoadedSamples = 0;
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CNonSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                for (CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    pTemporalFusionPixel->ClearSamples();
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        pTemporalFusionPixel->ClearSamples();
                                    }
                            }
                        }

                        void CMonocularNonSequentialTemporalFusion::MeanFusion()
                        {
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CNonSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = m_pOutputImage->GetBeginWritableBuffer();
                                for (CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->MeanFusion();
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->MeanFusion();
                                    }
                            }
                        }

                        void CMonocularNonSequentialTemporalFusion::DiscreteEpanechnikovFusion(const real ScopeFactor)
                        {
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CNonSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = m_pOutputImage->GetBeginWritableBuffer();
                                for (CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->DiscreteEpanechnikovFusion(m_pGlobalMaximaIntensityLocations, ScopeFactor);
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->DiscreteEpanechnikovFusion(m_pGlobalMaximaIntensityLocations, ScopeFactor);
                                    }
                            }
                        }

                        void CMonocularNonSequentialTemporalFusion::DiscreteGaussianFusion(const real ScopeFactor)
                        {
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CNonSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = m_pOutputImage->GetBeginWritableBuffer();
                                for (CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->DiscreteGaussianFusion(m_pGlobalMaximaIntensityLocations, ScopeFactor);
                                }
                            }
                            else
                            {
                                CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->DiscreteGaussianFusion(m_pGlobalMaximaIntensityLocations, ScopeFactor);
                                    }
                            }
                        }

                        void CMonocularNonSequentialTemporalFusion::ContinuousEpanechnikovFusion(const real OptimizationPrecision, const real ScopeFactor)
                        {
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CNonSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = m_pOutputImage->GetBeginWritableBuffer();
                                for (CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->ContinuousEpanechnikovFusion(m_pGlobalMaximaIntensityLocations, OptimizationPrecision, ScopeFactor);
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->ContinuousEpanechnikovFusion(m_pGlobalMaximaIntensityLocations, OptimizationPrecision, ScopeFactor);
                                    }
                            }
                        }

                        void CMonocularNonSequentialTemporalFusion::ContinuousGaussianFusion(const real OptimizationPrecision, const real ScopeFactor)
                        {
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CNonSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = m_pOutputImage->GetBeginWritableBuffer();
                                for (CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->ContinuousGaussianFusion(m_pGlobalMaximaIntensityLocations, OptimizationPrecision, ScopeFactor);
                                }
                            }
                            else
                            {
                                CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->ContinuousGaussianFusion(m_pGlobalMaximaIntensityLocations, OptimizationPrecision, ScopeFactor);
                                    }
                            }
                        }

                        TImage<real> CMonocularNonSequentialTemporalFusion::GetRangeImage() const
                        {
                            TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CNonSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();
                                for (CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->GetRange();
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->GetRange();
                                    }
                            }
                            return SelectedChannelImage;
                        }

                        TImage<real> CMonocularNonSequentialTemporalFusion::GetStandardDeviationImage() const
                        {
                            TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CNonSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();
                                for (CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->GetStandardDeviation();
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->GetStandardDeviation();
                                    }
                            }
                            return SelectedChannelImage;
                        }

                        TImage<real> CMonocularNonSequentialTemporalFusion::GetDispersionImage() const
                        {
                            TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CNonSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();
                                for (CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->GetDispersion();
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->GetDispersion();
                                    }
                            }
                            return SelectedChannelImage;
                        }

                        TImage<real> CMonocularNonSequentialTemporalFusion::GetSignalToNoiseRatioImage() const
                        {
                            TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CNonSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();

                                for (CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->GetSignalToNoiseRatio();
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());

                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->GetSignalToNoiseRatio();
                                    }
                            }
                            return SelectedChannelImage;
                        }

                        TImage<real> CMonocularNonSequentialTemporalFusion::GetBandWidthImage() const
                        {
                            TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CNonSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();
                                for (CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->GetBandWidth();
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->GetBandWidth();
                                    }
                            }
                            return SelectedChannelImage;
                        }

                        TImage<real> CMonocularNonSequentialTemporalFusion::GetGlobalMaximaImage() const
                        {
                            TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
                            uint* pGlobalMaximaIntensityLocations = new uint[256];
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CNonSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();
                                for (CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->GetGlobalMaxima(pGlobalMaximaIntensityLocations);
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CNonSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->GetGlobalMaxima(pGlobalMaximaIntensityLocations);
                                    }
                            }
                            RELEASE_ARRAY(pGlobalMaximaIntensityLocations)
                            return SelectedChannelImage;
                        }

                        bool CMonocularNonSequentialTemporalFusion::DisplayByMode(const TImage<real>* pImage, const CParameters::DisplayMode Mode)
                        {
                            switch (Mode)
                            {
                                case CParameters::eRoundedIntensity:
                                    return CImageExporter::DisplayRoundedIntensity(&m_OutputActiveZone, pImage, m_pDisplayImage);
                                case CParameters::eScaledIntensity:
                                    return CImageExporter::DisplayScaledIntensity(&m_OutputActiveZone, pImage, m_pDisplayImage);
                                case CParameters::eColorMapped:
                                    return CImageExporter::DisplayColorMapped(&m_OutputActiveZone, pImage, m_pDisplayImage, m_Parameters.GetColorMap());
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}
