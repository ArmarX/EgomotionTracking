/*
 * MonocularDensityFusion.cpp
 *
 *  Created on: 20.10.2011
 *      Author: gonzalez
 */

#include "MonocularDensityFusion.h"

namespace EVP::VisualSpace::Process::Monocular
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //CMonocularDensityFusion::CParameters
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    CMonocularDensityFusion::CParameters::~CParameters()
    {
        RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
    }

    CParametersBase::ParameterChangeResult CMonocularDensityFusion::CParameters::SetSamplesPerFusion(const uint SamplesPerFusion, const bool Wait)
    {
        const uint MaximalPossibleSamples = (0x1 << (sizeof(EVP::VisualSpace::Fusion::CDensityFusionPixel::FusionBin) * 8)) - 1;
        if ((SamplesPerFusion < 2) || (SamplesPerFusion > MaximalPossibleSamples))
        {
            return CParametersBase::eFailure_ParametersOutofScope;
        }
        if (SamplesPerFusion == m_SamplesPerFusion)
        {
            return CParametersBase::eNoChange;
        }
        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
        {
            m_SamplesPerFusion = SamplesPerFusion;
            m_pHost->UpdateSamplesPerFusion();
            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
        }
        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
    }

    uint CMonocularDensityFusion::CParameters::GetSamplesPerFusion() const
    {
        return m_SamplesPerFusion;
    }

    CParametersBase::ParameterChangeResult CMonocularDensityFusion::CParameters::SetSamplingMode(const SamplingMode Mode, const bool Wait)
    {
        if (!((Mode == CParameters::eDirect) || (Mode == CParameters::eIndirect)))
        {
            return CParametersBase::eFailure_ParametersOutofScope;
        }
        if (Mode == m_SamplingMode)
        {
            return CParametersBase::eNoChange;
        }
        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
        {
            m_SamplingMode = Mode;
            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
        }
        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
    }

    CMonocularDensityFusion::CParameters::SamplingMode CMonocularDensityFusion::CParameters::GetSamplingMode() const
    {
        return m_SamplingMode;
    }

    CParametersBase::ParameterChangeResult CMonocularDensityFusion::CParameters::SetFusionMethod(const FusionMethod Method, const bool Wait)
    {
        if (!((Method == CParameters::eMean) || (Method == CParameters::eEpanechnikov) || (Method == CParameters::eGaussian) || (Method == CParameters::eDiscreteEpanechnikov) || (Method == CParameters::eDiscreteGaussian)))
        {
            return CParametersBase::eFailure_ParametersOutofScope;
        }
        if (Method == m_FusionMethod)
        {
            return CParametersBase::eNoChange;
        }
        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
        {
            m_FusionMethod = Method;
            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
        }
        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
    }

    CMonocularDensityFusion::CParameters::FusionMethod CMonocularDensityFusion::CParameters::GetFusionMethod() const
    {
        return m_FusionMethod;
    }

    CParametersBase::ParameterChangeResult CMonocularDensityFusion::CParameters::SetDisplayChannel(const DisplayChannel Channel, const bool Wait)
    {
        if (!((Channel == CParameters::eRange) || (Channel == CParameters::eStandardDeviation) || (Channel == CParameters::eFusion) || (Channel == CParameters::eDispersion) || (Channel == CParameters::eSignalToNoiseRatio) || (Channel == CParameters::eGlobalMaxima)))
        {
            return CParametersBase::eFailure_ParametersOutofScope;
        }
        if (Channel == m_DisplayChannel)
        {
            return CParametersBase::eNoChange;
        }
        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
        {
            m_DisplayChannel = Channel;
            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
        }
        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
    }

    CMonocularDensityFusion::CParameters::DisplayChannel CMonocularDensityFusion::CParameters::GetDisplayChannel() const
    {
        return m_DisplayChannel;
    }

    CParametersBase::ParameterChangeResult CMonocularDensityFusion::CParameters::SetDisplayMode(const DisplayMode Mode, const bool Wait)
    {
        if (!((Mode == CParameters::eRoundedIntensity) || (Mode == CParameters::eScaledIntensity) || (Mode == CParameters::eColorMapped)))
        {
            return CParametersBase::eFailure_ParametersOutofScope;
        }
        if (Mode == m_DisplayMode)
        {
            return CParametersBase::eNoChange;
        }
        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
        {
            m_DisplayMode = Mode;
            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
        }
        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
    }

    CMonocularDensityFusion::CParameters::DisplayMode CMonocularDensityFusion::CParameters::GetDisplayMode() const
    {
        return m_DisplayMode;
    }

    CParametersBase::ParameterChangeResult CMonocularDensityFusion::CParameters::SetAutoCleanEnabled(const bool AutoCleanEnabled, const bool Wait)
    {
        if (AutoCleanEnabled == m_AutoCleanEnabled)
        {
            return CParametersBase::eNoChange;
        }
        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
        {
            m_AutoCleanEnabled = AutoCleanEnabled;
            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
        }
        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
    }

    bool CMonocularDensityFusion::CParameters::GetAutoCleanEnabled() const
    {
        return m_AutoCleanEnabled;
    }

    CParametersBase::ParameterChangeResult CMonocularDensityFusion::CParameters::SetAdaptiveBandwidthFactor(const real AdaptiveBandwidthFactor, const bool Wait)
    {
        if ((AdaptiveBandwidthFactor < _REAL_1_255_) || (AdaptiveBandwidthFactor > _REAL_THREE_))
        {
            return CParametersBase::eFailure_ParametersOutofScope;
        }
        if (AdaptiveBandwidthFactor == m_AdaptiveBandwidthFactor)
        {
            return CParametersBase::eNoChange;
        }
        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
        {
            m_AdaptiveBandwidthFactor = AdaptiveBandwidthFactor;
            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
        }
        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
    }

    real CMonocularDensityFusion::CParameters::GetAdaptiveBandwidthFactor() const
    {
        return m_AdaptiveBandwidthFactor;
    }

    CParametersBase::ParameterChangeResult CMonocularDensityFusion::CParameters::SetRangePrecision(const real RangePrecision, const bool Wait)
    {
        if ((RangePrecision <= _REAL_EPSILON_) || (RangePrecision > _REAL_FOURTH_))
        {
            return CParametersBase::eFailure_ParametersOutofScope;
        }
        if (RangePrecision == m_RangePrecision)
        {
            return CParametersBase::eNoChange;
        }
        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
        {
            m_RangePrecision = RangePrecision;
            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
        }
        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
    }

    real CMonocularDensityFusion::CParameters::GetRangePrecision() const
    {
        return m_RangePrecision;
    }

    CParametersBase::ParameterChangeResult CMonocularDensityFusion::CParameters::SetColorMap(const Visualization::CTristimulusColorMap* pColorMap, const bool OwnerShip, const bool Wait)
    {
        if (!pColorMap)
        {
            return CParametersBase::eFailure_ParametersOutofScope;
        }
        if (pColorMap == m_pColorMap)
        {
            return CParametersBase::eNoChange;
        }
        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
        {
            RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
            m_ColorMapOwnerShip = OwnerShip;
            m_pColorMap = pColorMap;
            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
        }
        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
    }

    CParametersBase::ParameterChangeResult CMonocularDensityFusion::CParameters::SetColorMap(const Visualization::CTristimulusColorMap::PredefinedColorMap ColorMap, const bool Wait)
    {
        if (!Visualization::CTristimulusColorMap::IsValidPredefinedColorMap(ColorMap))
        {
            return CParametersBase::eFailure_ParametersOutofScope;
        }
        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
        {
            RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
            m_ColorMapOwnerShip = true;
            m_pColorMap = Visualization::CTristimulusColorMap::LoadPredefinedColorMap(ColorMap);
            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
        }
        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
    }

    const Visualization::CTristimulusColorMap* CMonocularDensityFusion::CParameters::GetColorMap() const
    {
        return m_pColorMap;
    }

    CParametersBase::ParameterChangeResult CMonocularDensityFusion::CParameters::SetEnableMasking(const bool EnableMasking, const bool Wait)
    {
        if (EnableMasking == m_EnableMasking)
        {
            return CParametersBase::eNoChange;
        }
        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
        {
            m_EnableMasking = EnableMasking;
            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
        }
        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
    }

    bool CMonocularDensityFusion::CParameters::GetEnableMasking() const
    {
        return m_EnableMasking;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //CMonocularDensityFusion
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    CMonocularDensityFusion::CMonocularDensityFusion(CImageActiveZone* pActiveZone, const TImage<byte>* pInputImage, Threading::CMutex* pInputImageMutex) :
        TImageProcessBase<byte, real, CDiscreteTristimulusPixel>(CImageProcessBase::eDensityFusion, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this), m_pDensityFusionImage(nullptr), m_TotalLoadedSamples(0), m_pGlobalMaxima(nullptr), m_IsMaskingEnabled(false)
    {
        if (m_IsEnabled)
        {
            const CImageSize& Size = m_pInputActiveZone->GetSize();
            m_pOutputImage = new TImage<real>(Size);
            m_pDisplayImage = new TImage<CDiscreteTristimulusPixel>(Size);
            m_pInternalInputImage = new TImage<byte>(Size);
            m_IsMaskingEnabled = InitializeFusionImage(nullptr, Size);
            m_pGlobalMaxima = new uint[256];
            memset(m_pGlobalMaxima, 0, sizeof(uint) * 256);
            m_OutputActiveZone.SetConnection(m_pInputActiveZone);
        }
    }

    CMonocularDensityFusion::CMonocularDensityFusion(CImageActiveZone* pActiveZone, const TImage<bool>* pMaskImage, const TImage<byte>* pInputImage, Threading::CMutex* pInputImageMutex) :
        TImageProcessBase<byte, real, CDiscreteTristimulusPixel>(CImageProcessBase::eDensityFusion, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this), m_pDensityFusionImage(nullptr), m_TotalLoadedSamples(0), m_pGlobalMaxima(nullptr), m_IsMaskingEnabled(false)
    {
        if (m_IsEnabled)
        {
            const CImageSize& Size = m_pInputActiveZone->GetSize();
            m_pOutputImage = new TImage<real>(Size);
            m_pDisplayImage = new TImage<CDiscreteTristimulusPixel>(Size);
            m_pInternalInputImage = new TImage<byte>(Size);
            m_IsMaskingEnabled = InitializeFusionImage(pMaskImage, Size);
            m_pGlobalMaxima = new uint[256];
            memset(m_pGlobalMaxima, 0, sizeof(uint) * 256);
            m_OutputActiveZone.SetConnection(m_pInputActiveZone);
        }
    }
    CMonocularDensityFusion::~CMonocularDensityFusion()
    {
        ClearPendingImages();
        m_OutputActiveZone.Disconnect(true, true);
        RELEASE_ARRAY(m_pGlobalMaxima)
        RELEASE_OBJECT(m_pOutputImage)
        RELEASE_OBJECT(m_pDisplayImage)
        RELEASE_OBJECT(m_pInternalInputImage)
        RELEASE_OBJECT(m_pDensityFusionImage)
    }

    bool CMonocularDensityFusion::Execute(const Identifier TrialId)
    {
        if (m_IsEnabled && m_OutputActiveZone.IsEnabled())
        {
            if (StartExecuting(TrialId))
            {

                START_PROCESS_EXECUTION_LOG("Execute", TrialId)

                LoadInputImage();

                switch (m_Parameters.GetSamplingMode())
                {
                    case CParameters::eDirect:
                        LoadImage(m_pInternalInputImage);
                        break;
                    case CParameters::eIndirect:
                        m_PendingImages.push_back(m_pInternalInputImage->Clone());
                        break;
                }

                if ((m_TotalLoadedSamples + m_PendingImages.size()) == m_Parameters.GetSamplesPerFusion())
                {
                    if (m_PendingImages.size())
                    {
                        LoadPendingImages();
                    }
                    switch (m_Parameters.GetFusionMethod())
                    {
                        case CParameters::eMean:
                            MeanFusion();
                            break;
                        case CParameters::eEpanechnikov:
                            EpanechnikovFusion(m_Parameters.GetAdaptiveBandwidthFactor(), m_Parameters.GetRangePrecision());
                            break;
                        case CParameters::eGaussian:
                            GaussianFusion(m_Parameters.GetAdaptiveBandwidthFactor(), m_Parameters.GetRangePrecision());
                            break;
                        case CParameters::eDiscreteEpanechnikov:
                            DiscreteEpanechnikovFusion(m_Parameters.GetAdaptiveBandwidthFactor(), m_Parameters.GetRangePrecision());
                            break;
                        case CParameters::eDiscreteGaussian:
                            DiscreteGaussianFusion(m_Parameters.GetAdaptiveBandwidthFactor(), m_Parameters.GetRangePrecision());
                            break;
                    }
                    if (m_Parameters.GetAutoCleanEnabled())
                    {
                        ClearSamples();
                    }
                }

                STOP_PROCESS_EXECUTION_LOG

                return FinishExecution();
            }
        }
        return false;
    }

    bool CMonocularDensityFusion::Display()
    {
        if (m_IsEnabled && m_OutputActiveZone.IsEnabled() && GetTotalExecutions())
        {
            if (StartDisplaying())
            {
                bool Result = false;
                m_pDisplayImage->Clear();
                switch (m_Parameters.GetDisplayChannel())
                {
                    case CParameters::eRange:
                    {
                        const TImage<real> RangeImage = GetRangeImage();
                        Result = DisplayByMode(&RangeImage, m_Parameters.GetDisplayMode());
                        break;
                    }
                    case CParameters::eStandardDeviation:
                    {
                        const TImage<real> RangeImage = GetStandardDeviationImage();
                        Result = DisplayByMode(&RangeImage, m_Parameters.GetDisplayMode());
                        break;
                    }
                    case CParameters::eFusion:
                    {
                        Result = DisplayByMode(m_pOutputImage, m_Parameters.GetDisplayMode());
                    }
                    break;
                    case CParameters::eDispersion:
                    {
                        const TImage<real> DispersionImage = GetDispersionImage();
                        Result = DisplayByMode(&DispersionImage, m_Parameters.GetDisplayMode());
                    }
                    break;
                    case CParameters::eSignalToNoiseRatio:
                    {
                        const TImage<real> SignalToNoiseRatioImage = GetSignalToNoiseRatioImage();
                        Result = DisplayByMode(&SignalToNoiseRatioImage, m_Parameters.GetDisplayMode());
                    }
                    break;
                    case CParameters::eGlobalMaxima:
                    {
                        const TImage<real> SignalToNoiseRatioImage = GetGlobalMaximaImage();
                        Result = DisplayByMode(&SignalToNoiseRatioImage, m_Parameters.GetDisplayMode());
                    }
                    break;
                }
                Result &= FinishDisplaying();
                return Result;
            }
        }
        return false;
    }

    CMonocularDensityFusion::CParameters* CMonocularDensityFusion::GetParameters()
    {
        return m_IsEnabled ? &m_Parameters : nullptr;
    }

    bool CMonocularDensityFusion::InitializeFusionImage(const TImage<bool>* pMaskImage, const CImageSize& Size)
    {
        m_pDensityFusionImage = new TImage<EVP::VisualSpace::Fusion::CDensityFusionPixel>(Size);
        if (pMaskImage && pMaskImage->IsValid() && (pMaskImage->GetSize() == Size))
        {
            const bool* pMaskPixel = pMaskImage->GetBeginReadOnlyBuffer();
            const EVP::VisualSpace::Fusion::CDensityFusionPixel* const pFinalDensityFusionPixel = m_pDensityFusionImage->GetEndReadOnlyBuffer();
            for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
            {
                pDensityFusionPixel->SetActive(*pMaskPixel++);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    void CMonocularDensityFusion::UpdateSamplesPerFusion()
    {
        if ((m_TotalLoadedSamples + m_PendingImages.size()) >= m_Parameters.GetSamplesPerFusion())
        {
            ClearSamples();
            ClearPendingImages();
        }
    }

    bool CMonocularDensityFusion::IsMasking() const
    {
        return m_IsMaskingEnabled && m_Parameters.GetEnableMasking();
    }

    void CMonocularDensityFusion::LoadPendingImages()
    {
        list<const TImage<byte>*>::const_iterator EndPendingSampleImages = m_PendingImages.end();
        for (list<const TImage<byte>*>::const_iterator ppPendingImage = m_PendingImages.begin(); ppPendingImage != EndPendingSampleImages; ++ppPendingImage)
        {
            const TImage<byte>* pPendingImage = *ppPendingImage;
            LoadImage(pPendingImage);
            RELEASE_OBJECT(pPendingImage)
        }
        m_PendingImages.clear();
    }

    void CMonocularDensityFusion::ClearPendingImages()
    {
        if (m_PendingImages.size())
        {
            list<const TImage<byte>*>::const_iterator EndPendingSampleImages = m_PendingImages.end();
            for (list<const TImage<byte>*>::const_iterator ppPendingImage = m_PendingImages.begin(); ppPendingImage != EndPendingSampleImages; ++ppPendingImage)
            {
                const TImage<byte>* pPendingImage = *ppPendingImage;
                RELEASE_OBJECT(pPendingImage)
            }
            m_PendingImages.clear();
        }
    }

    void CMonocularDensityFusion::LoadImage(const TImage<byte>* pSampleImage)
    {
        if (m_OutputActiveZone.IsFullScopeActive())
        {
            const EVP::VisualSpace::Fusion::CDensityFusionPixel* const pFinalDensityFusionPixel = m_pDensityFusionImage->GetEndReadOnlyBuffer();
            const byte* pInputPixel = pSampleImage->GetBeginReadOnlyBuffer();
            if (IsMasking())
            {
                if (m_TotalLoadedSamples++)
                {
                    for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel, ++pInputPixel)
                        if (pDensityFusionPixel->IsActive())
                        {
                            pDensityFusionPixel->AddSample(*pInputPixel);
                        }
                }
                else
                    for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel, ++pInputPixel)
                        if (pDensityFusionPixel->IsActive())
                        {
                            pDensityFusionPixel->AddIntialSample(*pInputPixel);
                        }
            }
            else
            {
                if (m_TotalLoadedSamples++)
                    for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
                    {
                        pDensityFusionPixel->AddSample(*pInputPixel++);
                    }
                else
                    for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
                    {
                        pDensityFusionPixel->AddIntialSample(*pInputPixel++);
                    }
            }
        }
        else
        {
            const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
            const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
            const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
            const byte* pInputPixel = pSampleImage->GetReadOnlyBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            if (IsMasking())
            {
                if (m_TotalLoadedSamples++)
                {
                    for (uint y = 0; y < ActiveHeight; ++y, pInputPixel += Offset, pDensityFusionPixel += Offset)
                        for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel, ++pInputPixel)
                            if (pDensityFusionPixel->IsActive())
                            {
                                pDensityFusionPixel->AddSample(*pInputPixel);
                            }
                }
                else
                    for (uint y = 0; y < ActiveHeight; ++y, pInputPixel += Offset, pDensityFusionPixel += Offset)
                        for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel, ++pInputPixel)
                            if (pDensityFusionPixel->IsActive())
                            {
                                pDensityFusionPixel->AddIntialSample(*pInputPixel);
                            }
            }
            else
            {
                if (m_TotalLoadedSamples++)
                    for (uint y = 0; y < ActiveHeight; ++y, pInputPixel += Offset, pDensityFusionPixel += Offset)
                        for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel)
                        {
                            pDensityFusionPixel->AddSample(*pInputPixel++);
                        }
                else
                    for (uint y = 0; y < ActiveHeight; ++y, pInputPixel += Offset, pDensityFusionPixel += Offset)
                        for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel)
                        {
                            pDensityFusionPixel->AddIntialSample(*pInputPixel++);
                        }
            }
        }
    }

    void CMonocularDensityFusion::ClearSamples()
    {
        m_TotalLoadedSamples = 0;
        if (m_OutputActiveZone.IsFullScopeActive())
        {
            const EVP::VisualSpace::Fusion::CDensityFusionPixel* const pFinalDensityFusionPixel = m_pDensityFusionImage->GetEndReadOnlyBuffer();
            if (IsMasking())
            {
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
                    if (pDensityFusionPixel->IsActive())
                    {
                        pDensityFusionPixel->ClearSamples();
                    }
            }
            else
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
                {
                    pDensityFusionPixel->ClearSamples();
                }
        }
        else
        {
            const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
            const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
            const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
            EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            if (IsMasking())
            {
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel)
                        if (pDensityFusionPixel->IsActive())
                        {
                            pDensityFusionPixel->ClearSamples();
                        }
            }
            else
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel)
                    {
                        pDensityFusionPixel->ClearSamples();
                    }
        }
    }

    void CMonocularDensityFusion::MeanFusion()
    {
        if (m_OutputActiveZone.IsFullScopeActive())
        {
            const EVP::VisualSpace::Fusion::CDensityFusionPixel* const pFinalDensityFusionPixel = m_pDensityFusionImage->GetEndReadOnlyBuffer();
            real* pOutputPixel = m_pOutputImage->GetBeginWritableBuffer();
            if (IsMasking())
            {
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel, ++pOutputPixel)
                    if (pDensityFusionPixel->IsActive())
                    {
                        *pOutputPixel = pDensityFusionPixel->MeanFusion();
                    }
            }
            else
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
                {
                    *pOutputPixel++ = pDensityFusionPixel->MeanFusion();
                }
        }
        else
        {
            const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
            const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
            const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
            EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            real* pOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            if (IsMasking())
            {
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel, ++pOutputPixel)
                        if (pDensityFusionPixel->IsActive())
                        {
                            *pOutputPixel = pDensityFusionPixel->MeanFusion();
                        }
            }
            else
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel)
                    {
                        *pOutputPixel++ = pDensityFusionPixel->MeanFusion();
                    }
        }
    }

    void CMonocularDensityFusion::EpanechnikovFusion(const real BandwidthFactor, const real RangePrecision)
    {
        if (m_OutputActiveZone.IsFullScopeActive())
        {
            const EVP::VisualSpace::Fusion::CDensityFusionPixel* const pFinalDensityFusionPixel = m_pDensityFusionImage->GetEndReadOnlyBuffer();
            real* pOutputPixel = m_pOutputImage->GetBeginWritableBuffer();
            if (IsMasking())
            {
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel, ++pOutputPixel)
                    if (pDensityFusionPixel->IsActive())
                    {
                        *pOutputPixel = pDensityFusionPixel->EpanechnikovFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                    }
            }
            else
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
                {
                    *pOutputPixel++ = pDensityFusionPixel->EpanechnikovFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                }
        }
        else
        {
            const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
            const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
            const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
            EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            real* pOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            if (IsMasking())
            {
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel, ++pOutputPixel)
                        if (pDensityFusionPixel->IsActive())
                        {
                            *pOutputPixel = pDensityFusionPixel->EpanechnikovFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                        }
            }
            else
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel)
                    {
                        *pOutputPixel++ = pDensityFusionPixel->EpanechnikovFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                    }
        }
    }

    void CMonocularDensityFusion::GaussianFusion(const real BandwidthFactor, const real RangePrecision)
    {
        if (m_OutputActiveZone.IsFullScopeActive())
        {
            const EVP::VisualSpace::Fusion::CDensityFusionPixel* const pFinalDensityFusionPixel = m_pDensityFusionImage->GetEndReadOnlyBuffer();
            real* pOutputPixel = m_pOutputImage->GetBeginWritableBuffer();
            if (IsMasking())
            {
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel, ++pOutputPixel)
                    if (pDensityFusionPixel->IsActive())
                    {
                        *pOutputPixel = pDensityFusionPixel->GaussianFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                    }
            }
            else
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
                {
                    *pOutputPixel++ = pDensityFusionPixel->GaussianFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                }
        }
        else
        {
            EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            real* pOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
            const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
            const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
            if (IsMasking())
            {
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel, ++pOutputPixel)
                        if (pDensityFusionPixel->IsActive())
                        {
                            *pOutputPixel = pDensityFusionPixel->GaussianFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                        }
            }
            else
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel)
                    {
                        *pOutputPixel++ = pDensityFusionPixel->GaussianFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                    }
        }
    }

    void CMonocularDensityFusion::DiscreteEpanechnikovFusion(const real BandwidthFactor, const real RangePrecision)
    {
        if (m_OutputActiveZone.IsFullScopeActive())
        {
            const EVP::VisualSpace::Fusion::CDensityFusionPixel* const pFinalDensityFusionPixel = m_pDensityFusionImage->GetEndReadOnlyBuffer();
            real* pOutputPixel = m_pOutputImage->GetBeginWritableBuffer();
            if (IsMasking())
            {
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel, ++pOutputPixel)
                    if (pDensityFusionPixel->IsActive())
                    {
                        *pOutputPixel = pDensityFusionPixel->DiscreteEpanechnikovFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                    }
            }
            else
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
                {
                    *pOutputPixel++ = pDensityFusionPixel->DiscreteEpanechnikovFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                }
        }
        else
        {
            const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
            const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
            const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
            EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            real* pOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            if (IsMasking())
            {
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel, ++pOutputPixel)
                        if (pDensityFusionPixel->IsActive())
                        {
                            *pOutputPixel = pDensityFusionPixel->DiscreteEpanechnikovFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                        }
            }
            else
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel)
                    {
                        *pOutputPixel++ = pDensityFusionPixel->DiscreteEpanechnikovFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                    }
        }
    }

    void CMonocularDensityFusion::DiscreteGaussianFusion(const real BandwidthFactor, const real RangePrecision)
    {
        if (m_OutputActiveZone.IsFullScopeActive())
        {
            const EVP::VisualSpace::Fusion::CDensityFusionPixel* const pFinalDensityFusionPixel = m_pDensityFusionImage->GetEndReadOnlyBuffer();
            real* pOutputPixel = m_pOutputImage->GetBeginWritableBuffer();
            if (IsMasking())
            {
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel, ++pOutputPixel)
                    if (pDensityFusionPixel->IsActive())
                    {
                        *pOutputPixel = pDensityFusionPixel->DiscreteGaussianFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                    }
            }
            else
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
                {
                    *pOutputPixel++ = pDensityFusionPixel->DiscreteGaussianFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                }
        }
        else
        {
            const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
            const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
            const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
            EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            real* pOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            if (IsMasking())
            {
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel, ++pOutputPixel)
                        if (pDensityFusionPixel->IsActive())
                        {
                            *pOutputPixel = pDensityFusionPixel->DiscreteGaussianFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                        }
            }
            else
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel)
                    {
                        *pOutputPixel++ = pDensityFusionPixel->DiscreteGaussianFusion(BandwidthFactor, RangePrecision, m_pGlobalMaxima);
                    }
        }
    }

    TImage<real> CMonocularDensityFusion::GetRangeImage() const
    {
        TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
        if (m_OutputActiveZone.IsFullScopeActive())
        {
            const EVP::VisualSpace::Fusion::CDensityFusionPixel* const pFinalDensityFusionPixel = m_pDensityFusionImage->GetEndReadOnlyBuffer();
            real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();
            if (IsMasking())
            {
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel, ++pOutputPixel)
                    if (pDensityFusionPixel->IsActive())
                    {
                        *pOutputPixel = pDensityFusionPixel->GetRange();
                    }
            }
            else
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
                {
                    *pOutputPixel++ = pDensityFusionPixel->GetRange();
                }
        }
        else
        {
            const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
            const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
            const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
            EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            if (IsMasking())
            {
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel, ++pOutputPixel)
                        if (pDensityFusionPixel->IsActive())
                        {
                            *pOutputPixel = pDensityFusionPixel->GetRange();
                        }
            }
            else
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel)
                    {
                        *pOutputPixel++ = pDensityFusionPixel->GetRange();
                    }
        }
        return SelectedChannelImage;
    }

    TImage<real> CMonocularDensityFusion::GetStandardDeviationImage() const
    {
        TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
        if (m_OutputActiveZone.IsFullScopeActive())
        {
            const EVP::VisualSpace::Fusion::CDensityFusionPixel* const pFinalDensityFusionPixel = m_pDensityFusionImage->GetEndReadOnlyBuffer();
            real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();
            if (IsMasking())
            {
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel, ++pOutputPixel)
                    if (pDensityFusionPixel->IsActive())
                    {
                        *pOutputPixel = pDensityFusionPixel->GetStandardDeviation();
                    }
            }
            else
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
                {
                    *pOutputPixel++ = pDensityFusionPixel->GetStandardDeviation();
                }
        }
        else
        {
            const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
            const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
            const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
            EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            if (IsMasking())
            {
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel, ++pOutputPixel)
                        if (pDensityFusionPixel->IsActive())
                        {
                            *pOutputPixel = pDensityFusionPixel->GetStandardDeviation();
                        }
            }
            else
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel)
                    {
                        *pOutputPixel++ = pDensityFusionPixel->GetStandardDeviation();
                    }
        }
        return SelectedChannelImage;
    }

    TImage<real> CMonocularDensityFusion::GetDispersionImage() const
    {
        TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
        if (m_OutputActiveZone.IsFullScopeActive())
        {
            const EVP::VisualSpace::Fusion::CDensityFusionPixel* const pFinalDensityFusionPixel = m_pDensityFusionImage->GetEndReadOnlyBuffer();
            real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();
            if (IsMasking())
            {
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel, ++pOutputPixel)
                    if (pDensityFusionPixel->IsActive())
                    {
                        *pOutputPixel = pDensityFusionPixel->GetDispersionValue();
                    }
            }
            else
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
                {
                    *pOutputPixel++ = pDensityFusionPixel->GetDispersionValue();
                }
        }
        else
        {
            const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
            const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
            const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
            EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            if (IsMasking())
            {
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel, ++pOutputPixel)
                        if (pDensityFusionPixel->IsActive())
                        {
                            *pOutputPixel = pDensityFusionPixel->GetDispersionValue();
                        }
            }
            else
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel)
                    {
                        *pOutputPixel++ = pDensityFusionPixel->GetDispersionValue();
                    }
        }
        return SelectedChannelImage;
    }

    TImage<real> CMonocularDensityFusion::GetSignalToNoiseRatioImage() const
    {
        TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
        if (m_OutputActiveZone.IsFullScopeActive())
        {
            const EVP::VisualSpace::Fusion::CDensityFusionPixel* const pFinalDensityFusionPixel = m_pDensityFusionImage->GetEndReadOnlyBuffer();
            real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();
            if (IsMasking())
            {
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel, ++pOutputPixel)
                    if (pDensityFusionPixel->IsActive())
                    {
                        *pOutputPixel = pDensityFusionPixel->GetSignalToNoiseRatio();
                    }
            }
            else
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
                {
                    *pOutputPixel++ = pDensityFusionPixel->GetSignalToNoiseRatio();
                }
        }
        else
        {
            const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
            const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
            const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
            EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            if (IsMasking())
            {
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel, ++pOutputPixel)
                        if (pDensityFusionPixel->IsActive())
                        {
                            *pOutputPixel = pDensityFusionPixel->GetSignalToNoiseRatio();
                        }
            }
            else
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel)
                    {
                        *pOutputPixel++ = pDensityFusionPixel->GetSignalToNoiseRatio();
                    }
        }
        return SelectedChannelImage;
    }

    TImage<real> CMonocularDensityFusion::GetGlobalMaximaImage() const
    {
        TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
        if (m_OutputActiveZone.IsFullScopeActive())
        {
            const EVP::VisualSpace::Fusion::CDensityFusionPixel* const pFinalDensityFusionPixel = m_pDensityFusionImage->GetEndReadOnlyBuffer();
            real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();
            if (IsMasking())
            {
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel, ++pOutputPixel)
                    if (pDensityFusionPixel->IsActive())
                    {
                        *pOutputPixel = pDensityFusionPixel->GetGlobalMaxima(m_pGlobalMaxima);
                    }
            }
            else
                for (EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetBeginWritableBuffer(); pDensityFusionPixel < pFinalDensityFusionPixel; ++pDensityFusionPixel)
                {
                    *pOutputPixel++ = pDensityFusionPixel->GetGlobalMaxima(m_pGlobalMaxima);
                }
        }
        else
        {
            const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
            const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
            const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
            EVP::VisualSpace::Fusion::CDensityFusionPixel* pDensityFusionPixel = m_pDensityFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
            if (IsMasking())
            {
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel, ++pOutputPixel)
                        if (pDensityFusionPixel->IsActive())
                        {
                            *pOutputPixel = pDensityFusionPixel->GetGlobalMaxima(m_pGlobalMaxima);
                        }
            }
            else
                for (uint y = 0; y < ActiveHeight; ++y, pDensityFusionPixel += Offset, pOutputPixel += Offset)
                    for (uint x = 0; x < ActiveWidth; ++x, ++pDensityFusionPixel)
                    {
                        *pOutputPixel++ = pDensityFusionPixel->GetGlobalMaxima(m_pGlobalMaxima);
                    }
        }
        return SelectedChannelImage;
    }

    bool CMonocularDensityFusion::DisplayByMode(const TImage<real>* pImage, const CParameters::DisplayMode Mode)
    {
        switch (Mode)
        {
            case CParameters::eRoundedIntensity:
                return CImageExporter::DisplayRoundedIntensity(&m_OutputActiveZone, pImage, m_pDisplayImage);
            case CParameters::eScaledIntensity:
                return CImageExporter::DisplayScaledIntensity(&m_OutputActiveZone, pImage, m_pDisplayImage);
            case CParameters::eColorMapped:
                return CImageExporter::DisplayColorMapped(&m_OutputActiveZone, pImage, m_pDisplayImage, m_Parameters.GetColorMap());
        }
        return false;
    }
}
