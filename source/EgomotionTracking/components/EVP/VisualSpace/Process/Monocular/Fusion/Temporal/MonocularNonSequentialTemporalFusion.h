/*
 * MonocularNonSequentialTemporalFusion.h
 *
 *  Created on: Apr 24, 2012
 *      Author: david
 */

#pragma once

#include "../../../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../../../Visualization/Miscellaneous/TristimulusColorMap.h"
#include "../../../Base/ImageProcessBase.h"
#include "NonSequentialTemporalFusionPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                namespace Fusion
                {
                    namespace Temporal
                    {
                        class CMonocularNonSequentialTemporalFusion: public TImageProcessBase<byte, real, CDiscreteTristimulusPixel>
                        {
                        public:

                            class CParameters: public CParametersBase
                            {
                            public:

                                enum SamplingMode
                                {
                                    eDirect, eIndirect
                                };

                                enum FusionMethod
                                {
                                    eMean, eDiscreteEpanechnikov, eDiscreteGaussian, eContinuousEpanechnikov, eContinuousGaussian
                                };

                                enum DisplayChannel
                                {
                                    eRange, eStandardDeviation, eFusion, eDispersion, eSignalToNoiseRatio, eBandWidth, eGlobalMaxima
                                };

                                enum DisplayMode
                                {
                                    eRoundedIntensity, eScaledIntensity, eColorMapped
                                };

                                CParameters(CMonocularNonSequentialTemporalFusion* pHost) :
                                    CParametersBase(), m_pHost(pHost), m_pColorMap(NULL), m_ColorMapOwnerShip(true), m_SamplesPerFusion(30), m_SamplingMode(eDirect), m_FusionMethod(eContinuousGaussian), m_DisplayChannel(eFusion), m_DisplayMode(eScaledIntensity), m_AutoCleanEnabled(true), m_OptimizationPrecision(real(0.001953125)), m_ScopeFactor(_REAL_THREE_)
                                {
                                    m_pColorMap = Visualization::CTristimulusColorMap::LoadPredefinedColorMap(Visualization::CTristimulusColorMap::eRGB_Hot);
                                }

                                ~CParameters() override;

                                CParametersBase::ParameterChangeResult SetSamplesPerFusion(const uint SamplesPerFusion, const bool Wait = true);
                                uint GetSamplesPerFusion() const;

                                CParametersBase::ParameterChangeResult SetSamplingMode(const SamplingMode Mode, const bool Wait = true);
                                SamplingMode GetSamplingMode() const;

                                CParametersBase::ParameterChangeResult SetFusionMethod(const FusionMethod Method, const bool Wait = true);
                                FusionMethod GetFusionMethod() const;

                                CParametersBase::ParameterChangeResult SetDisplayChannel(const DisplayChannel Channel, const bool Wait = true);
                                DisplayChannel GetDisplayChannel() const;

                                CParametersBase::ParameterChangeResult SetDisplayMode(const DisplayMode Mode, const bool Wait = true);
                                DisplayMode GetDisplayMode() const;

                                CParametersBase::ParameterChangeResult SetAutoCleanEnabled(const bool AutoCleanEnabled, const bool Wait = true);
                                bool GetAutoCleanEnabled() const;

                                CParametersBase::ParameterChangeResult SetOptimizationPrecision(const real OptimizationPrecision, const bool Wait = true);
                                real GetOptimizationPrecision() const;

                                CParametersBase::ParameterChangeResult SetScopeFactor(const real ScopeFactor, const bool Wait = true);
                                real GetScopeFactor() const;

                                CParametersBase::ParameterChangeResult SetColorMap(const Visualization::CTristimulusColorMap* pColorMap, const bool OwnerShip, const bool Wait = true);
                                CParametersBase::ParameterChangeResult SetColorMap(const Visualization::CTristimulusColorMap::PredefinedColorMap ColorMap, const bool Wait = true);
                                const Visualization::CTristimulusColorMap* GetColorMap() const;

                            protected:

                                CMonocularNonSequentialTemporalFusion* m_pHost;
                                const Visualization::CTristimulusColorMap* m_pColorMap;
                                bool m_ColorMapOwnerShip;
                                uint m_SamplesPerFusion;
                                SamplingMode m_SamplingMode;
                                FusionMethod m_FusionMethod;
                                DisplayChannel m_DisplayChannel;
                                DisplayMode m_DisplayMode;
                                bool m_AutoCleanEnabled;
                                real m_OptimizationPrecision;
                                real m_ScopeFactor;
                            };

                            CMonocularNonSequentialTemporalFusion(CImageActiveZone* pActiveZone, const TImage<byte>* pInputImage, Threading::CMutex* pInputImageMutex);
                            ~CMonocularNonSequentialTemporalFusion() override;

                            bool Execute(const Identifier TrialId) override;
                            bool Display() override;
                            CParameters* GetParameters() override;

                        protected:

                            void LoadPendingImages();
                            void ClearPendingImages();
                            void LoadImage(const TImage<byte>* pSampleImage);
                            void ClearSamples();

                            void MeanFusion();
                            void DiscreteEpanechnikovFusion(const real ScopeFactor);
                            void DiscreteGaussianFusion(const real ScopeFactor);
                            void ContinuousEpanechnikovFusion(const real OptimizationPrecision, const real ScopeFactor);
                            void ContinuousGaussianFusion(const real OptimizationPrecision, const real ScopeFactor);

                            TImage<real> GetRangeImage() const;
                            TImage<real> GetStandardDeviationImage() const;
                            TImage<real> GetDispersionImage() const;
                            TImage<real> GetSignalToNoiseRatioImage() const;
                            TImage<real> GetBandWidthImage() const;
                            TImage<real> GetGlobalMaximaImage() const;
                            bool DisplayByMode(const TImage<real>* pImage, const CParameters::DisplayMode Mode);

                            CParameters m_Parameters;
                            TImage<CNonSequentialTemporalFusionPixel>* m_pTemporalFusionImage;
                            uint m_TotalLoadedSamples;
                            list<const TImage<byte>*> m_PendingImages;
                            uint* m_pGlobalMaximaIntensityLocations;
                        };
                    }
                }
            }
        }
    }
}

