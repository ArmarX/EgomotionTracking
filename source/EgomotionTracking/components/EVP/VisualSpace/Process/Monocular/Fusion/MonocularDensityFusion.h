/*
 * MonocularDensityFusion.h
 *
 *  Created on: 20.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../../Visualization/Miscellaneous/TristimulusColorMap.h"
#include "../../Base/ImageProcessBase.h"
#include "DensityFusionPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                class CMonocularDensityFusion: public TImageProcessBase<byte, real, CDiscreteTristimulusPixel>
                {
                public:

                    class CParameters: public CParametersBase
                    {
                    public:

                        enum SamplingMode
                        {
                            eDirect, eIndirect
                        };

                        enum FusionMethod
                        {
                            eMean, eEpanechnikov, eGaussian, eDiscreteEpanechnikov, eDiscreteGaussian
                        };

                        enum DisplayChannel
                        {
                            eRange, eStandardDeviation, eFusion, eDispersion, eSignalToNoiseRatio, eGlobalMaxima
                        };

                        enum DisplayMode
                        {
                            eRoundedIntensity, eScaledIntensity, eColorMapped
                        };

                        CParameters(CMonocularDensityFusion* pHost) :
                            CParametersBase(), m_pHost(pHost), m_pColorMap(NULL), m_ColorMapOwnerShip(true), m_SamplesPerFusion(30), m_SamplingMode(eIndirect), m_FusionMethod(eGaussian), m_DisplayChannel(eFusion), m_DisplayMode(eScaledIntensity), m_AutoCleanEnabled(true), m_AdaptiveBandwidthFactor(_REAL_ONE_), m_RangePrecision(real(1.0 / 32.0)), m_EnableMasking(false)
                        {
                            m_pColorMap = Visualization::CTristimulusColorMap::LoadPredefinedColorMap(Visualization::CTristimulusColorMap::eRGB_Hot);
                        }

                        ~CParameters() override;

                        CParametersBase::ParameterChangeResult SetSamplesPerFusion(const uint SamplesPerFusion, const bool Wait = true);
                        uint GetSamplesPerFusion() const;

                        CParametersBase::ParameterChangeResult SetSamplingMode(const SamplingMode Mode, const bool Wait = true);
                        SamplingMode GetSamplingMode() const;

                        CParametersBase::ParameterChangeResult SetFusionMethod(const FusionMethod Method, const bool Wait = true);
                        FusionMethod GetFusionMethod() const;

                        CParametersBase::ParameterChangeResult SetDisplayChannel(const DisplayChannel Channel, const bool Wait = true);
                        DisplayChannel GetDisplayChannel() const;

                        CParametersBase::ParameterChangeResult SetDisplayMode(const DisplayMode Mode, const bool Wait = true);
                        DisplayMode GetDisplayMode() const;

                        CParametersBase::ParameterChangeResult SetAutoCleanEnabled(const bool AutoCleanEnabled, const bool Wait = true);
                        bool GetAutoCleanEnabled() const;

                        CParametersBase::ParameterChangeResult SetAdaptiveBandwidthFactor(const real AdaptiveBandwidthFactor, const bool Wait = true);
                        real GetAdaptiveBandwidthFactor() const;

                        CParametersBase::ParameterChangeResult SetRangePrecision(const real RangePrecision, const bool Wait = true);
                        real GetRangePrecision() const;

                        CParametersBase::ParameterChangeResult SetColorMap(const Visualization::CTristimulusColorMap* pColorMap, const bool OwnerShip, const bool Wait = true);
                        CParametersBase::ParameterChangeResult SetColorMap(const Visualization::CTristimulusColorMap::PredefinedColorMap ColorMap, const bool Wait = true);
                        const Visualization::CTristimulusColorMap* GetColorMap() const;

                        CParametersBase::ParameterChangeResult SetEnableMasking(const bool EnableMasking, const bool Wait = true);
                        bool GetEnableMasking() const;

                    protected:

                        CMonocularDensityFusion* m_pHost;
                        const Visualization::CTristimulusColorMap* m_pColorMap;
                        bool m_ColorMapOwnerShip;
                        uint m_SamplesPerFusion;
                        SamplingMode m_SamplingMode;
                        FusionMethod m_FusionMethod;
                        DisplayChannel m_DisplayChannel;
                        DisplayMode m_DisplayMode;
                        bool m_AutoCleanEnabled;
                        real m_AdaptiveBandwidthFactor;
                        real m_RangePrecision;
                        bool m_EnableMasking;
                    };

                    CMonocularDensityFusion(CImageActiveZone* pActiveZone, const TImage<byte>* pInputImage, Threading::CMutex* pInputImageMutex);
                    CMonocularDensityFusion(CImageActiveZone* pActiveZone, const TImage<bool>* pMaskImage, const TImage<byte>* pInputImage, Threading::CMutex* pInputImageMutex);
                    ~CMonocularDensityFusion() override;

                    bool Execute(const Identifier TrialId) override;
                    bool Display() override;
                    CParameters* GetParameters() override;

                protected:

                    friend class CMonocularDensityFusion::CParameters;

                    bool InitializeFusionImage(const TImage<bool>* pMaskImage, const CImageSize& Size);
                    void UpdateSamplesPerFusion();

                    bool IsMasking() const;

                    void LoadPendingImages();
                    void ClearPendingImages();
                    void LoadImage(const TImage<byte>* pSampleImage);
                    void ClearSamples();

                    void MeanFusion();
                    void EpanechnikovFusion(const real BandwidthFactor, const real RangePrecision);
                    void GaussianFusion(const real BandwidthFactor, const real RangePrecision);
                    void DiscreteEpanechnikovFusion(const real BandwidthFactor, const real RangePrecision);
                    void DiscreteGaussianFusion(const real BandwidthFactor, const real RangePrecision);

                    TImage<real> GetRangeImage() const;
                    TImage<real> GetStandardDeviationImage() const;
                    TImage<real> GetDispersionImage() const;
                    TImage<real> GetSignalToNoiseRatioImage() const;
                    TImage<real> GetGlobalMaximaImage() const;
                    bool DisplayByMode(const TImage<real>* pImage, const CParameters::DisplayMode Mode);

                    CParameters m_Parameters;
                    TImage<EVP::VisualSpace::Fusion::CDensityFusionPixel>* m_pDensityFusionImage;
                    uint m_TotalLoadedSamples;
                    list<const TImage<byte>*> m_PendingImages;
                    uint* m_pGlobalMaxima;
                    bool m_IsMaskingEnabled;
                };
            }
        }
    }
}

