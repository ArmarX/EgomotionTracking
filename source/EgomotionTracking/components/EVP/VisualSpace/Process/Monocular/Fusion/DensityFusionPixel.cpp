/*
 * DensityFusionPixel.cpp
 *
 *  Created on: 21.10.2011
 *      Author: gonzalez
 */

#include "../../../../Foundation/Files/OutFile.h"
#include "DensityFusionPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Fusion
        {
#ifdef _FUSION_USE_LOOKUP_TABLE_
            INITIALIZE_EXPONENTIAL_LOOKUP_TABLE(CDensityFusionPixel)

#endif

            CDensityFusionPixel::CDensityFusionPixel() :
                m_FusionBins(nullptr), m_TotalSamples(0), m_Accumulator(0), m_SquareAccumulator(0), m_Minimal(0), m_Maximal(0), m_Fusion(_REAL_ZERO_), m_IsActive(true)
            {

                m_FusionBins = new FusionBin[256];
                memset(m_FusionBins, 0, sizeof(FusionBin) * 256);

#ifdef _FUSION_USE_LOOKUP_TABLE_

                LOAD_EXPONENTIAL_LOOKUP_TABLE(CDensityFusionPixel)

#endif
            }

            CDensityFusionPixel::~CDensityFusionPixel()
            {
                RELEASE_ARRAY(m_FusionBins)
            }

            void CDensityFusionPixel::SetActive(const bool Active)
            {
                m_IsActive = Active;
            }

            void CDensityFusionPixel::AddIntialSample(const byte SampleValue)
            {
                ++m_TotalSamples;
                m_Accumulator = m_Maximal = m_Minimal = SampleValue;
                m_SquareAccumulator = SampleValue * SampleValue;
                ++m_FusionBins[SampleValue];
            }

            void CDensityFusionPixel::AddSample(const byte SampleValue)
            {
                m_Accumulator += SampleValue;
                m_SquareAccumulator += SampleValue * SampleValue;
                if (SampleValue > m_Maximal)
                {
                    m_Maximal = SampleValue;
                }
                else if (SampleValue < m_Minimal)
                {
                    m_Minimal = SampleValue;
                }
                ++m_FusionBins[SampleValue];
                ++m_TotalSamples;
            }

            void CDensityFusionPixel::ClearSamples()
            {
                if (m_TotalSamples)
                {
                    memset(m_FusionBins + m_Minimal, 0, sizeof(FusionBin) * (m_Maximal - m_Minimal + 1));
                    m_TotalSamples = 0;
                }
            }

            uint CDensityFusionPixel::GetMaximalFrequency() const
            {
                uint MaximalFrequency = 1;
                const FusionBin* pSamples = m_FusionBins + m_Minimal;
                for (uint Value = m_Minimal; Value <= m_Maximal; ++Value, ++pSamples)
                    if (*pSamples > MaximalFrequency)
                    {
                        MaximalFrequency = *pSamples;
                    }
                return MaximalFrequency;
            }

            real CDensityFusionPixel::GetStandardDeviation() const
            {
                const real Mean = real(m_Accumulator) / real(m_TotalSamples);
                return RealSqrt((real(m_SquareAccumulator) / real(m_TotalSamples)) - (Mean * Mean));
            }

            real CDensityFusionPixel::PreFusionGetStandardDeviation()
            {
                m_Fusion = real(m_Accumulator) / real(m_TotalSamples);
                return RealSqrt((real(m_SquareAccumulator) / real(m_TotalSamples)) - (m_Fusion * m_Fusion));
            }

            real CDensityFusionPixel::GetSignalToNoiseRatio() const
            {
                return (real(m_Accumulator) / real(m_TotalSamples)) / GetStandardDeviation();
            }

            real CDensityFusionPixel::MeanFusion()
            {
                m_Fusion = real(m_Accumulator) / real(m_TotalSamples);
                return m_Fusion;
            }

            byte CDensityFusionPixel::DiscreteEpanechnikovFusion(const real BandwidthFactor, const real RangePrecision, uint* pGlobalMaxima)
            {
                if (m_Maximal == m_Minimal)
                {
                    m_Fusion = m_Maximal;
                    return m_Maximal;
                }
                const uint TotalGlobalMaxima = GetGlobalMaxima(pGlobalMaxima);
                if (TotalGlobalMaxima)
                {
                    if (TotalGlobalMaxima > 1)
                    {
                        uint SelectedIndex = 0;
                        const real AdaptiveBandWidth = GetStandardDeviation() * BandwidthFactor;
                        const real SquareBandWidthNormalization = _REAL_ONE_ / (AdaptiveBandWidth * AdaptiveBandWidth);
                        real MaximalDensity = EstimateNonNormalizedEpanechnikovDensity(pGlobalMaxima[0], AdaptiveBandWidth, SquareBandWidthNormalization);
                        for (uint i = 1; i < TotalGlobalMaxima; ++i)
                        {
                            const real Density = EstimateNonNormalizedEpanechnikovDensity(pGlobalMaxima[i], AdaptiveBandWidth, SquareBandWidthNormalization);
                            if (Density > MaximalDensity)
                            {
                                MaximalDensity = Density;
                                SelectedIndex = i;
                            }
                        }
                        m_Fusion = pGlobalMaxima[SelectedIndex];
                        return pGlobalMaxima[SelectedIndex];
                    }
                    m_Fusion = pGlobalMaxima[0];
                    return pGlobalMaxima[0];
                }
                else
                {
                    m_Fusion = real(m_Accumulator) / real(m_TotalSamples);
                    return SafeRoundToByte(m_Fusion);
                }
            }

#ifdef _FUSION_USE_LOOKUP_TABLE_

            byte CDensityFusionPixel::DiscreteGaussianFusion(const real BandwidthFactor, const real RangePrecision, uint* pGlobalMaxima)
            {
                if (m_Maximal == m_Minimal)
                {
                    m_Fusion = m_Maximal;
                    return m_Maximal;
                }
                const uint TotalGlobalMaxima = GetGlobalMaxima(pGlobalMaxima);
                if (TotalGlobalMaxima)
                {
                    if (TotalGlobalMaxima > 1)
                    {
                        uint SelectedIndex = 0;
                        const real AdaptiveBandWidth = PreFusionGetStandardDeviation() * BandwidthFactor;
                        const real BandWidthExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(AdaptiveBandWidth);
                        const real LUTScaleFactor = BandWidthExponentFactor * s_ELT.m_SamplesPerUnit;
                        const real LUTMaximalDelta = RealSqrt(s_ELT.m_MinimalArgumentValue / BandWidthExponentFactor);
                        real MaximalDensity = EstimateNonNormalizedGaussianDensity(pGlobalMaxima[0], LUTScaleFactor, LUTMaximalDelta);
                        for (uint i = 1; i < TotalGlobalMaxima; ++i)
                        {
                            const real Density = EstimateNonNormalizedGaussianDensity(pGlobalMaxima[i], LUTScaleFactor, LUTMaximalDelta);
                            if (Density > MaximalDensity)
                            {
                                MaximalDensity = Density;
                                SelectedIndex = i;
                            }
                        }
                        m_Fusion = pGlobalMaxima[SelectedIndex];
                        return pGlobalMaxima[SelectedIndex];
                    }
                    m_Fusion = pGlobalMaxima[0];
                    return pGlobalMaxima[0];
                }
                else
                {
                    m_Fusion = real(m_Accumulator) / real(m_TotalSamples);
                    return SafeRoundToByte(m_Fusion);
                }
            }

#else

            byte CDensityFusionPixel::DiscreteGaussianFusion(const real BandwidthFactor, const real RangePrecision, uint* pGlobalMaxima)
            {
                if (m_Maximal == m_Minimal)
                {
                    m_IntensityFusion = m_Maximal;
                    return m_Maximal;
                }
                const uint TotalGlobalMaxima = GetGlobalMaxima(pGlobalMaxima);
                if (TotalGlobalMaxima)
                {
                    if (TotalGlobalMaxima > 1)
                    {
                        uint SelectedIndex = 0;
                        const real AdaptiveBandWidth = GetIntensityStandardDeviation() * BandwidthFactor;
                        const real BandWidthExponentFactor = GaussianExponentFactor(AdaptiveBandWidth);
                        real MaximalDensity = EstimateNonNormalizedGaussianDensity(pGlobalMaxima[0], BandWidthExponentFactor);
                        for (uint i = 1; i < TotalGlobalMaxima; ++i)
                        {
                            const real Density = EstimateNonNormalizedGaussianDensity(pGlobalMaxima[i], BandWidthExponentFactor);
                            if (Density > MaximalDensity)
                            {
                                MaximalDensity = Density;
                                SelectedIndex = i;
                            }
                        }
                        m_IntensityFusion = pGlobalMaxima[SelectedIndex];
                        return pGlobalMaxima[SelectedIndex];
                    }
                    m_IntensityFusion = pGlobalMaxima[0];
                    return pGlobalMaxima[0];
                }
                else
                {
                    m_IntensityFusion = real(m_Accumulator) / real(m_TotalSamples);
                    return RoundPositiverealTobyte(m_IntensityFusion);
                }
            }

#endif

            real CDensityFusionPixel::EpanechnikovFusion(const real BandwidthFactor, const real RangePrecision, uint* pGlobalMaxima)
            {
                if (m_Maximal == m_Minimal)
                {
                    m_Fusion = m_Maximal;
                    return m_Fusion;
                }
                const uint TotalGlobalMaxima = GetGlobalMaxima(pGlobalMaxima);
                const real AdaptiveBandWidth = PreFusionGetStandardDeviation() * BandwidthFactor;
                const real SquareBandWidthNormalization = _REAL_ONE_ / (AdaptiveBandWidth * AdaptiveBandWidth);
                if (TotalGlobalMaxima)
                {
                    m_Fusion = pGlobalMaxima[0];
                    if (TotalGlobalMaxima > 1)
                    {
                        real MaximalDensity = EstimateNonNormalizedEpanechnikovDensity(m_Fusion, AdaptiveBandWidth, SquareBandWidthNormalization);
                        for (uint i = 1; i < TotalGlobalMaxima; ++i)
                        {
                            const real Density = EstimateNonNormalizedEpanechnikovDensity(pGlobalMaxima[i], AdaptiveBandWidth, SquareBandWidthNormalization);
                            if (Density > MaximalDensity)
                            {
                                MaximalDensity = Density;
                                m_Fusion = pGlobalMaxima[i];
                            }
                        }
                    }
                }
                real ValueA = TMax(m_Fusion - _REAL_HALF_, real(m_Minimal));
                real ValueB = TMin(m_Fusion + _REAL_HALF_, real(m_Maximal));
                real DensityA = EstimateNonNormalizedEpanechnikovDensity(ValueA, AdaptiveBandWidth, SquareBandWidthNormalization);
                real DensityB = EstimateNonNormalizedEpanechnikovDensity(ValueB, AdaptiveBandWidth, SquareBandWidthNormalization);
                real DistanceAB = ((DensityB > DensityA) ? (m_Fusion - ValueA) : (ValueB - m_Fusion));
                do
                {
                    if (DensityB > DensityA)
                    {
                        ValueA += DistanceAB;
                        DensityA = EstimateNonNormalizedEpanechnikovDensity(ValueA, AdaptiveBandWidth, SquareBandWidthNormalization);
                    }
                    else
                    {
                        ValueB -= DistanceAB;
                        DensityB = EstimateNonNormalizedEpanechnikovDensity(ValueB, AdaptiveBandWidth, SquareBandWidthNormalization);
                    }
                    DistanceAB = (ValueB - ValueA) * _REAL_HALF_;
                }
                while (DistanceAB >= RangePrecision);
                m_Fusion = DensityA > DensityB ? ValueA : ValueB;
                return m_Fusion;
            }

#ifdef _FUSION_USE_LOOKUP_TABLE_

            real CDensityFusionPixel::GaussianFusion(const real BandwidthFactor, const real RangePrecision, uint* pGlobalMaxima)
            {
                if (m_Maximal == m_Minimal)
                {
                    m_Fusion = m_Maximal;
                    return m_Fusion;
                }
                const uint TotalGlobalMaxima = GetGlobalMaxima(pGlobalMaxima);
                const real AdaptiveBandWidth = PreFusionGetStandardDeviation() * BandwidthFactor;
                //h = \left(\frac{4\hat{\sigma}^5}{3n}\right)^{\frac{1}{5}} \approx 1.06 \hat{\sigma} n^{-1/5}
                const real BandWidthExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(AdaptiveBandWidth);
                const real LUTScaleFactor = BandWidthExponentFactor * s_ELT.m_SamplesPerUnit;
                const real LUTMaximalDelta = RealSqrt(s_ELT.m_MinimalArgumentValue / BandWidthExponentFactor);
                if (TotalGlobalMaxima)
                {
                    m_Fusion = pGlobalMaxima[0];
                    if (TotalGlobalMaxima > 1)
                    {
                        real MaximalDensity = EstimateNonNormalizedGaussianDensity(m_Fusion, LUTScaleFactor, LUTMaximalDelta);
                        for (uint i = 1; i < TotalGlobalMaxima; ++i)
                        {
                            const real Density = EstimateNonNormalizedGaussianDensity(pGlobalMaxima[i], LUTScaleFactor, LUTMaximalDelta);
                            if (Density > MaximalDensity)
                            {
                                MaximalDensity = Density;
                                m_Fusion = pGlobalMaxima[i];
                            }
                        }
                    }
                }
                real ValueA = TMax(m_Fusion - _REAL_HALF_, real(m_Minimal));
                real ValueB = TMin(m_Fusion + _REAL_HALF_, real(m_Maximal));
                real DensityA = EstimateNonNormalizedGaussianDensity(ValueA, LUTScaleFactor, LUTMaximalDelta);
                real DensityB = EstimateNonNormalizedGaussianDensity(ValueB, LUTScaleFactor, LUTMaximalDelta);
                real DistanceAB = ((DensityB > DensityA) ? (m_Fusion - ValueA) : (ValueB - m_Fusion));
                do
                {
                    if (DensityB > DensityA)
                    {
                        ValueA += DistanceAB;
                        DensityA = EstimateNonNormalizedGaussianDensity(ValueA, LUTScaleFactor, LUTMaximalDelta);
                    }
                    else
                    {
                        ValueB -= DistanceAB;
                        DensityB = EstimateNonNormalizedGaussianDensity(ValueB, LUTScaleFactor, LUTMaximalDelta);
                    }
                    DistanceAB = (ValueB - ValueA) * _REAL_HALF_;
                }
                while (DistanceAB >= RangePrecision);
                m_Fusion = DensityA > DensityB ? ValueA : ValueB;
                return m_Fusion;
            }

#else

            real CDensityFusionPixel::GaussianFusion(const real BandwidthFactor, const real RangePrecision, uint* pGlobalMaxima)
            {
                if (m_Maximal == m_Minimal)
                {
                    m_IntensityFusion = m_Maximal;
                    return m_IntensityFusion;
                }
                const uint TotalGlobalMaxima = GetGlobalMaxima(pGlobalMaxima);
                const real AdaptiveBandWidth = PreFusionGetStandardDeviation(m_IntensityFusion) * BandwidthFactor;
                const real BandWidthExponentFactor = GaussianExponentFactor(AdaptiveBandWidth);
                if (TotalGlobalMaxima)
                {
                    m_IntensityFusion = pGlobalMaxima[0];
                    if (TotalGlobalMaxima > 1)
                    {
                        real MaximalDensity = EstimateNonNormalizedGaussianDensity(m_IntensityFusion, BandWidthExponentFactor);
                        for (uint i = 1; i < TotalGlobalMaxima; ++i)
                        {
                            const real Density = EstimateNonNormalizedGaussianDensity(pGlobalMaxima[i], BandWidthExponentFactor);
                            if (Density > MaximalDensity)
                            {
                                MaximalDensity = Density;
                                m_IntensityFusion = pGlobalMaxima[i];
                            }
                        }
                    }
                }
                real ValueA = TMax(m_IntensityFusion - _REAL_HALF_, real(m_Minimal));
                real ValueB = TMin(m_IntensityFusion + _REAL_HALF_, real(m_Maximal));
                real DensityA = EstimateNonNormalizedGaussianDensity(ValueA, BandWidthExponentFactor);
                real DensityB = EstimateNonNormalizedGaussianDensity(ValueB, BandWidthExponentFactor);
                real DistanceAB = ((DensityB > DensityA) ? (m_IntensityFusion - ValueA) : (ValueB - m_IntensityFusion));
                do
                {
                    if (DensityB > DensityA)
                    {
                        ValueA += DistanceAB;
                        DensityA = EstimateNonNormalizedGaussianDensity(ValueA, BandWidthExponentFactor);
                    }
                    else
                    {
                        ValueB -= DistanceAB;
                        DensityB = EstimateNonNormalizedGaussianDensity(ValueB, BandWidthExponentFactor);
                    }
                    DistanceAB *= _REAL_HALF_;
                }
                while (DistanceAB >= RangePrecision);
                m_IntensityFusion = ((ValueA * DensityA) + (ValueB * DensityB)) / (DensityA + DensityB);
                return m_IntensityFusion;
            }

#endif

            uint CDensityFusionPixel::GetGlobalMaxima(uint* pGlobalMaxima) const
            {
                uint MaximalFrequency = m_FusionBins[m_Minimal];
                for (uint i = m_Minimal + 1; i <= m_Maximal; ++i)
                    if (m_FusionBins[i] > MaximalFrequency)
                    {
                        MaximalFrequency = m_FusionBins[i];
                    }
                uint TotalGlobalMaxima = 0;
                for (uint i = m_Minimal; i <= m_Maximal; ++i)
                    if (m_FusionBins[i] == MaximalFrequency)
                    {
                        pGlobalMaxima[TotalGlobalMaxima++] = i;
                    }
                return TotalGlobalMaxima;
            }

            bool CDensityFusionPixel::ExportToFile(const_string pPathFileName, const CPixelLocation* pLocation) const
            {
                if (pPathFileName)
                {
                    std::ostringstream OutputText;
                    OutputText << "Density Fusion Pixel" << g_EndLine;
                    if (pLocation)
                    {
                        OutputText << g_EndLineTabulator << "Location" << g_Tabulator << "x = " << pLocation->m_x << g_Tabulator << "y = " << pLocation->m_y;
                    }
                    OutputText << g_EndLineTabulator << "Minimal" << g_Tabulator << m_Maximal;
                    OutputText << g_EndLineTabulator << "Maximal" << g_Tabulator << m_Minimal;
                    OutputText << g_EndLineTabulator << "Mean" << g_Tabulator << (real(m_Accumulator) / real(m_TotalSamples));
                    OutputText << g_EndLineTabulator << "Range" << g_Tabulator << (m_Maximal - m_Minimal);
                    OutputText << g_EndLineTabulator << "Maximal Frequency" << g_Tabulator << GetMaximalFrequency();
                    OutputText << g_EndLineTabulator << "Standard Deviation" << g_Tabulator << GetStandardDeviation();
                    OutputText << g_EndLineTabulator << "Fusion" << g_Tabulator << m_Fusion;
                    OutputText << g_EndLineTabulator << "Dispersion" << g_Tabulator << GetDispersionValue();
                    OutputText << g_EndLineTabulator << "Absolute Dispersion" << g_Tabulator << RealAbs(GetDispersionValue());
                    OutputText << g_EndLineTabulator << "Signal-to-Noise Ratio" << g_Tabulator << GetSignalToNoiseRatio();
                    OutputText << g_EndLineTabulator << "Intensity" << g_Tabulator << "Frequency";
                    const FusionBin* pSamples = m_FusionBins + m_Minimal;
                    for (uint Value = m_Minimal; Value <= m_Maximal; ++Value)
                    {
                        OutputText << g_EndLineTabulator << Value << g_Tabulator << *pSamples++;
                    }
                    return Files::COutFile::WriteStringToFile(pPathFileName, OutputText.str());
                }
                return false;
            }

            real CDensityFusionPixel::EstimateNonNormalizedEpanechnikovDensity(const real ValueLocation, const real AdaptiveBandWidth, const real SquareBandWidthNormalization)
            {
                const uint FinalSupportValue = TMin(uint(ValueLocation + (AdaptiveBandWidth + _REAL_ONE_)), m_Maximal);
                uint Value = TMax(DownToInteger(ValueLocation - (AdaptiveBandWidth + _REAL_ONE_)), DownToInteger(m_Minimal));
                const FusionBin* pSamples = m_FusionBins + Value;
                real Density = _REAL_ZERO_;
                while (Value <= FinalSupportValue)
                {
                    if (*pSamples)
                    {
                        const real Delta = RealAbs(real(Value) - ValueLocation);
                        if (Delta < AdaptiveBandWidth)
                        {
                            Density += (_REAL_ONE_ - ((Delta * Delta) * SquareBandWidthNormalization)) * real(*pSamples);
                        }
                    }
                    ++Value;
                    ++pSamples;
                }
                return Density;
            }

#ifdef _FUSION_USE_LOOKUP_TABLE_

            real CDensityFusionPixel::EstimateNonNormalizedGaussianDensity(const real ValueLocation, const real LUTScaleFactor, const real LUTMaximalDelta)
            {
                const FusionBin* pSamples = m_FusionBins + m_Minimal;
                real Density = _REAL_ZERO_;
                for (uint Value = m_Minimal; Value <= m_Maximal; ++Value, ++pSamples)
                    if (*pSamples)
                    {
                        const real Delta = ValueLocation - real(Value);
                        if (Delta < LUTMaximalDelta)
                        {
                            Density += s_ELT.m_pExp[RoundPositiveRealToInteger(Delta * Delta * LUTScaleFactor)] * real(*pSamples);
                        }
                    }
                return Density;
            }

#else

            real CDensityFusionPixel::EstimateNonNormalizedGaussianDensity(const real ValueLocation, const real BandWidthExponentFactor)
            {
                const FusionBin* pSamples = m_FusionBins + m_Minimal;
                real Density = _REAL_ZERO_;
                for (uint Value = m_Minimal; Value <= m_Maximal; ++Value, ++pSamples)
                    if (*pSamples)
                    {
                        const real Delta = real(Value) - ValueLocation;
                        Density += RealExp((Delta * Delta) * BandWidthExponentFactor) * real(*pSamples);
                    }
                return Density;
            }

#endif

        }
    }
}
