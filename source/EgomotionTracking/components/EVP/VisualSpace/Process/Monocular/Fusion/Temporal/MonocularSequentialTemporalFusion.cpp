/*
 * MonocularSequentialTemporalFusion.cpp
 *
 *  Created on: Apr 24, 2012
 *      Author: david
 */

#include "MonocularSequentialTemporalFusion.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                namespace Fusion
                {
                    namespace Temporal
                    {

                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //CMonocularSequentialTemporalFusion::CParameters
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        CMonocularSequentialTemporalFusion::CParameters::~CParameters()
                        {
                            RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                        }

                        CParametersBase::ParameterChangeResult CMonocularSequentialTemporalFusion::CParameters::SetSamplesPerFusion(const uint SamplesPerFusion, const bool Wait)
                        {
                            if (SamplesPerFusion < 2)
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (SamplesPerFusion == m_SamplesPerFusion)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_SamplesPerFusion = SamplesPerFusion;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        uint CMonocularSequentialTemporalFusion::CParameters::GetSamplesPerFusion() const
                        {
                            return m_SamplesPerFusion;
                        }

                        CParametersBase::ParameterChangeResult CMonocularSequentialTemporalFusion::CParameters::SetSamplingMode(const SamplingMode Mode, const bool Wait)
                        {
                            if (!((Mode == CParameters::eDirect) || (Mode == CParameters::eIndirect)))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (Mode == m_SamplingMode)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_SamplingMode = Mode;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        CMonocularSequentialTemporalFusion::CParameters::SamplingMode CMonocularSequentialTemporalFusion::CParameters::GetSamplingMode() const
                        {
                            return m_SamplingMode;
                        }

                        CParametersBase::ParameterChangeResult CMonocularSequentialTemporalFusion::CParameters::SetFusionMethod(const FusionMethod Method, const bool Wait)
                        {
                            if (!((Method == CParameters::eMean) || (Method == CParameters::eEpanechnikov) || (Method == CParameters::eGaussian)))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (Method == m_FusionMethod)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_FusionMethod = Method;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        CMonocularSequentialTemporalFusion::CParameters::FusionMethod CMonocularSequentialTemporalFusion::CParameters::GetFusionMethod() const
                        {
                            return m_FusionMethod;
                        }

                        CParametersBase::ParameterChangeResult CMonocularSequentialTemporalFusion::CParameters::SetDisplayChannel(const DisplayChannel Channel, const bool Wait)
                        {
                            if (!((Channel == CParameters::eRange) || (Channel == CParameters::eStandardDeviation) || (Channel == CParameters::eFusion) || (Channel == CParameters::eDispersion) || (Channel == CParameters::eSignalToNoiseRatio)))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (Channel == m_DisplayChannel)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_DisplayChannel = Channel;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        CMonocularSequentialTemporalFusion::CParameters::DisplayChannel CMonocularSequentialTemporalFusion::CParameters::GetDisplayChannel() const
                        {
                            return m_DisplayChannel;
                        }

                        CParametersBase::ParameterChangeResult CMonocularSequentialTemporalFusion::CParameters::SetDisplayMode(const DisplayMode Mode, const bool Wait)
                        {
                            if (!((Mode == CParameters::eRoundedIntensity) || (Mode == CParameters::eScaledIntensity) || (Mode == CParameters::eColorMapped)))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (Mode == m_DisplayMode)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_DisplayMode = Mode;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        CMonocularSequentialTemporalFusion::CParameters::DisplayMode CMonocularSequentialTemporalFusion::CParameters::GetDisplayMode() const
                        {
                            return m_DisplayMode;
                        }

                        CParametersBase::ParameterChangeResult CMonocularSequentialTemporalFusion::CParameters::SetAutoCleanEnabled(const bool AutoCleanEnabled, const bool Wait)
                        {
                            if (AutoCleanEnabled == m_AutoCleanEnabled)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_AutoCleanEnabled = AutoCleanEnabled;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        bool CMonocularSequentialTemporalFusion::CParameters::GetAutoCleanEnabled() const
                        {
                            return m_AutoCleanEnabled;
                        }

                        CParametersBase::ParameterChangeResult CMonocularSequentialTemporalFusion::CParameters::SetSamplingPrecision(const real SamplingPrecision, const bool Wait)
                        {
                            if ((SamplingPrecision <= _REAL_EPSILON_) || (SamplingPrecision > _REAL_FOURTH_))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (SamplingPrecision == m_SamplingPrecision)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_SamplingPrecision = SamplingPrecision;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        real CMonocularSequentialTemporalFusion::CParameters::GetSamplingPrecision() const
                        {
                            return m_SamplingPrecision;
                        }

                        CParametersBase::ParameterChangeResult CMonocularSequentialTemporalFusion::CParameters::SetOptimizationPrecision(const real OptimizationPrecision, const bool Wait)
                        {
                            if ((OptimizationPrecision <= _REAL_EPSILON_) || (OptimizationPrecision > _REAL_FOURTH_))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (OptimizationPrecision == m_OptimizationPrecision)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                m_OptimizationPrecision = OptimizationPrecision;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        real CMonocularSequentialTemporalFusion::CParameters::GetOptimizationPrecision() const
                        {
                            return m_OptimizationPrecision;
                        }

                        CParametersBase::ParameterChangeResult CMonocularSequentialTemporalFusion::CParameters::SetColorMap(const Visualization::CTristimulusColorMap* pColorMap, const bool OwnerShip, const bool Wait)
                        {
                            if (!pColorMap)
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (pColorMap == m_pColorMap)
                            {
                                return CParametersBase::eNoChange;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                                m_ColorMapOwnerShip = OwnerShip;
                                m_pColorMap = pColorMap;
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        CParametersBase::ParameterChangeResult CMonocularSequentialTemporalFusion::CParameters::SetColorMap(const Visualization::CTristimulusColorMap::PredefinedColorMap ColorMap, const bool Wait)
                        {
                            if (!Visualization::CTristimulusColorMap::IsValidPredefinedColorMap(ColorMap))
                            {
                                return CParametersBase::eFailure_ParametersOutofScope;
                            }
                            if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                            {
                                RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                                m_ColorMapOwnerShip = true;
                                m_pColorMap = Visualization::CTristimulusColorMap::LoadPredefinedColorMap(ColorMap);
                                return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                            }
                            return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                        }

                        const Visualization::CTristimulusColorMap* CMonocularSequentialTemporalFusion::CParameters::GetColorMap() const
                        {
                            return m_pColorMap;
                        }

                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //CMonocularSequentialTemporalFusion
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        CMonocularSequentialTemporalFusion::CMonocularSequentialTemporalFusion(CImageActiveZone* pActiveZone, const TImage<byte>* pInputImage, Threading::CMutex* pInputImageMutex) :
                            TImageProcessBase<byte, real, CDiscreteTristimulusPixel>(CImageProcessBase::eDensityFusion, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this), m_pTemporalFusionImage(nullptr), m_TotalLoadedSamples(0)
                        {
                            if (m_IsEnabled)
                            {
                                const CImageSize& Size = m_pInputActiveZone->GetSize();
                                m_pOutputImage = new TImage<real>(Size);
                                m_pDisplayImage = new TImage<CDiscreteTristimulusPixel>(Size);
                                m_pInternalInputImage = new TImage<byte>(Size);
                                m_pTemporalFusionImage = new TImage<CSequentialTemporalFusionPixel>(Size);
                                m_OutputActiveZone.SetConnection(m_pInputActiveZone);
                            }
                        }

                        CMonocularSequentialTemporalFusion::~CMonocularSequentialTemporalFusion()
                        {
                            ClearPendingImages();
                            m_OutputActiveZone.Disconnect(true, true);
                            RELEASE_OBJECT(m_pOutputImage)
                            RELEASE_OBJECT(m_pDisplayImage)
                            RELEASE_OBJECT(m_pInternalInputImage)
                            RELEASE_OBJECT(m_pTemporalFusionImage)
                        }

                        bool CMonocularSequentialTemporalFusion::Execute(const Identifier TrialId)
                        {
                            if (m_IsEnabled && m_OutputActiveZone.IsEnabled())
                            {
                                if (StartExecuting(TrialId))
                                {

                                    START_PROCESS_EXECUTION_LOG("Execute", TrialId)

                                    LoadInputImage();

                                    switch (m_Parameters.GetSamplingMode())
                                    {
                                        case CParameters::eDirect:
                                            LoadImage(m_pInternalInputImage);
                                            break;
                                        case CParameters::eIndirect:
                                            m_PendingImages.push_back(m_pInternalInputImage->Clone(true));
                                            break;
                                    }

                                    if ((m_TotalLoadedSamples + m_PendingImages.size()) == m_Parameters.GetSamplesPerFusion())
                                    {
                                        if (m_PendingImages.size())
                                        {
                                            LoadPendingImages();
                                        }
                                        switch (m_Parameters.GetFusionMethod())
                                        {
                                            case CParameters::eMean:
                                                MeanFusion();
                                                break;
                                            case CParameters::eEpanechnikov:
                                                EpanechnikovFusion(m_Parameters.GetSamplingPrecision(), m_Parameters.GetOptimizationPrecision());
                                                break;
                                            case CParameters::eGaussian:
                                                GaussianFusion(m_Parameters.GetSamplingPrecision(), m_Parameters.GetOptimizationPrecision());
                                                break;
                                        }
                                        if (m_Parameters.GetAutoCleanEnabled())
                                        {
                                            ClearSamples();
                                        }
                                    }

                                    STOP_PROCESS_EXECUTION_LOG

                                    return FinishExecution();
                                }
                            }
                            return false;
                        }

                        bool CMonocularSequentialTemporalFusion::Display()
                        {
                            if (m_IsEnabled && m_OutputActiveZone.IsEnabled() && GetTotalExecutions())
                            {
                                if (StartDisplaying())
                                {
                                    bool Result = false;
                                    m_pDisplayImage->Clear();
                                    switch (m_Parameters.GetDisplayChannel())
                                    {
                                        case CParameters::eRange:
                                        {
                                            const TImage<real> RangeImage = GetRangeImage();
                                            Result = DisplayByMode(&RangeImage, m_Parameters.GetDisplayMode());
                                        }
                                        break;
                                        case CParameters::eStandardDeviation:
                                        {
                                            const TImage<real> RangeImage = GetStandardDeviationImage();
                                            Result = DisplayByMode(&RangeImage, m_Parameters.GetDisplayMode());
                                        }
                                        break;
                                        case CParameters::eFusion:
                                        {
                                            Result = DisplayByMode(m_pOutputImage, m_Parameters.GetDisplayMode());
                                        }
                                        break;
                                        case CParameters::eDispersion:
                                        {
                                            const TImage<real> DispersionImage = GetDispersionImage();
                                            Result = DisplayByMode(&DispersionImage, m_Parameters.GetDisplayMode());
                                        }
                                        break;
                                        case CParameters::eSignalToNoiseRatio:
                                        {
                                            const TImage<real> SignalToNoiseRatioImage = GetSignalToNoiseRatioImage();
                                            Result = DisplayByMode(&SignalToNoiseRatioImage, m_Parameters.GetDisplayMode());
                                        }
                                        break;
                                    }
                                    Result &= FinishDisplaying();
                                    return Result;
                                }
                            }
                            return false;
                        }

                        CMonocularSequentialTemporalFusion::CParameters* CMonocularSequentialTemporalFusion::GetParameters()
                        {
                            return m_IsEnabled ? &m_Parameters : nullptr;
                        }

                        void CMonocularSequentialTemporalFusion::LoadPendingImages()
                        {
                            list<const TImage<byte>*>::const_iterator EndPendingSampleImages = m_PendingImages.end();
                            for (list<const TImage<byte>*>::const_iterator ppPendingImage = m_PendingImages.begin(); ppPendingImage != EndPendingSampleImages; ++ppPendingImage)
                            {
                                const TImage<byte>* pPendingImage = *ppPendingImage;
                                LoadImage(pPendingImage);
                                RELEASE_OBJECT(pPendingImage)
                            }
                            m_PendingImages.clear();
                        }

                        void CMonocularSequentialTemporalFusion::ClearPendingImages()
                        {
                            if (m_PendingImages.size())
                            {
                                list<const TImage<byte>*>::const_iterator EndPendingSampleImages = m_PendingImages.end();
                                for (list<const TImage<byte>*>::const_iterator ppPendingImage = m_PendingImages.begin(); ppPendingImage != EndPendingSampleImages; ++ppPendingImage)
                                {
                                    const TImage<byte>* pPendingImage = *ppPendingImage;
                                    RELEASE_OBJECT(pPendingImage)
                                }
                                m_PendingImages.clear();
                            }
                        }

                        void CMonocularSequentialTemporalFusion::LoadImage(const TImage<byte>* pSampleImage)
                        {
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                const byte* pInputPixel = pSampleImage->GetBeginReadOnlyBuffer();
                                if (m_TotalLoadedSamples++)
                                    for (CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                    {
                                        pTemporalFusionPixel->AddSubsequentSample(*pInputPixel++);
                                    }
                                else
                                    for (CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                    {
                                        pTemporalFusionPixel->AddIntialSample(*pInputPixel++);
                                    }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                const byte* pInputPixel = pSampleImage->GetReadOnlyBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                if (m_TotalLoadedSamples++)
                                    for (uint y = 0; y < ActiveHeight; ++y, pInputPixel += Offset, pTemporalFusionPixel += Offset)
                                        for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                        {
                                            pTemporalFusionPixel->AddSubsequentSample(*pInputPixel++);
                                        }
                                else
                                    for (uint y = 0; y < ActiveHeight; ++y, pInputPixel += Offset, pTemporalFusionPixel += Offset)
                                        for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                        {
                                            pTemporalFusionPixel->AddIntialSample(*pInputPixel++);
                                        }
                            }
                        }

                        void CMonocularSequentialTemporalFusion::ClearSamples()
                        {
                            m_TotalLoadedSamples = 0;
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                for (CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    pTemporalFusionPixel->ClearSamples();
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        pTemporalFusionPixel->ClearSamples();
                                    }
                            }
                        }

                        void CMonocularSequentialTemporalFusion::MeanFusion()
                        {
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = m_pOutputImage->GetBeginWritableBuffer();
                                for (CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->MeanFusion();
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->MeanFusion();
                                    }
                            }
                        }

                        void CMonocularSequentialTemporalFusion::EpanechnikovFusion(const real SamplingPrecision, const real OptimizationPrecision)
                        {
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = m_pOutputImage->GetBeginWritableBuffer();
                                for (CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->EpanechnikovFusion(SamplingPrecision, OptimizationPrecision);
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->EpanechnikovFusion(SamplingPrecision, OptimizationPrecision);
                                    }
                            }
                        }

                        void CMonocularSequentialTemporalFusion::GaussianFusion(const real SamplingPrecision, const real OptimizationPrecision)
                        {
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = m_pOutputImage->GetBeginWritableBuffer();
                                for (CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->GaussianFusion(SamplingPrecision, OptimizationPrecision);
                                }
                            }
                            else
                            {
                                CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->GaussianFusion(SamplingPrecision, OptimizationPrecision);
                                    }
                            }
                        }

                        TImage<real> CMonocularSequentialTemporalFusion::GetRangeImage() const
                        {
                            TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();
                                for (CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->GetRange();
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->GetRange();
                                    }
                            }
                            return SelectedChannelImage;
                        }

                        TImage<real> CMonocularSequentialTemporalFusion::GetStandardDeviationImage() const
                        {
                            TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();
                                for (CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->GetStandardDeviation();
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->GetStandardDeviation();
                                    }
                            }
                            return SelectedChannelImage;
                        }

                        TImage<real> CMonocularSequentialTemporalFusion::GetDispersionImage() const
                        {
                            TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();
                                for (CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->GetDispersion();
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->GetDispersion();
                                    }
                            }
                            return SelectedChannelImage;
                        }

                        TImage<real> CMonocularSequentialTemporalFusion::GetSignalToNoiseRatioImage() const
                        {
                            TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();

                                for (CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->GetSignalToNoiseRatio();
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());

                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->GetSignalToNoiseRatio();
                                    }
                            }
                            return SelectedChannelImage;
                        }

                        TImage<real> CMonocularSequentialTemporalFusion::GetGlobalMaximaImage() const
                        {
                            TImage<real> SelectedChannelImage(m_pOutputImage->GetSize());
                            if (m_OutputActiveZone.IsFullScopeActive())
                            {
                                const CSequentialTemporalFusionPixel* const pFinalDensityFusionPixel = m_pTemporalFusionImage->GetEndReadOnlyBuffer();
                                real* pOutputPixel = SelectedChannelImage.GetBeginWritableBuffer();
                                for (CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetBeginWritableBuffer(); pTemporalFusionPixel < pFinalDensityFusionPixel; ++pTemporalFusionPixel)
                                {
                                    *pOutputPixel++ = pTemporalFusionPixel->GetMaximal();
                                }
                            }
                            else
                            {
                                const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                                const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                                const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                                CSequentialTemporalFusionPixel* pTemporalFusionPixel = m_pTemporalFusionImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                real* pOutputPixel = SelectedChannelImage.GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                                for (uint y = 0; y < ActiveHeight; ++y, pTemporalFusionPixel += Offset, pOutputPixel += Offset)
                                    for (uint x = 0; x < ActiveWidth; ++x, ++pTemporalFusionPixel)
                                    {
                                        *pOutputPixel++ = pTemporalFusionPixel->GetMaximal();
                                    }
                            }
                            return SelectedChannelImage;
                        }

                        bool CMonocularSequentialTemporalFusion::DisplayByMode(const TImage<real>* pImage, const CParameters::DisplayMode Mode)
                        {
                            switch (Mode)
                            {
                                case CParameters::eRoundedIntensity:
                                    return CImageExporter::DisplayRoundedIntensity(&m_OutputActiveZone, pImage, m_pDisplayImage);
                                case CParameters::eScaledIntensity:
                                    return CImageExporter::DisplayScaledIntensity(&m_OutputActiveZone, pImage, m_pDisplayImage);
                                case CParameters::eColorMapped:
                                    return CImageExporter::DisplayColorMapped(&m_OutputActiveZone, pImage, m_pDisplayImage, m_Parameters.GetColorMap());
                            }
                            return false;
                        }
                    }
                }
            }
        }
    }
}
