/*
 * NonSequentialTemporalFusionPixel.h
 *
 *  Created on: 15.05.2012
 *      Author: gonzalez
 */

#pragma once

#include "../../../../../GlobalSettings.h"
#include "../../../../../Foundation/DataTypes/PixelLocation.h"
#include "../../../../../Foundation/Files/OutFile.h"
#include "../../../../../Foundation/Mathematics/1D/Common1D.h"
#include "../../../../../Foundation/Mathematics/1D/ExponentialLookUpTable.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                namespace Fusion
                {
                    namespace Temporal
                    {
                        class CNonSequentialTemporalFusionPixel
                        {

#ifdef _FUSION_USE_LOOKUP_TABLE_
                            ////////////////////////////////
                            DECLARE_EXPONENTIAL_LOOKUP_TABLE
                            ////////////////////////////////
#endif
                            typedef byte FusionBin;

                        public:

                            CNonSequentialTemporalFusionPixel();
                            ~CNonSequentialTemporalFusionPixel();

                            void SetActive(const bool Active);
                            bool IsActive() const;

                            void AddIntialSample(const uint SampleValue);
                            void AddSubsequentSample(const uint SampleValue);
                            void ClearSamples();

                            const FusionBin* GetFusionBins() const;
                            uint GetTotalSamples() const;
                            real GetMean() const;
                            real GetMedian() const;
                            real GetStandardDeviation() const;
                            real GetBandWidth() const;
                            uint GetMinimal() const;
                            uint GetMaximal() const;
                            uint GetRange() const;
                            real GetFusion() const;
                            real GetDispersion() const;
                            real GetSignalToNoiseRatio() const;
                            uint GetMaximalFrequency() const;
                            uint GetGlobalMaxima(uint* pGlobalMaximaIntensityLocations) const;
                            bool ExportToFile(const_string pPathFileName, const CPixelLocation* pLocation) const;

                            real MeanFusion();
                            uint DiscreteEpanechnikovFusion(uint* pGlobalMaximaIntensityLocations, const real ScopeFactor = _REAL_THREE_);
                            real ContinuousEpanechnikovFusion(uint* pGlobalMaximaIntensityLocations, const real RangePrecision = _REAL_MILI_, const real ScopeFactor = _REAL_THREE_);
                            uint DiscreteGaussianFusion(uint* pGlobalMaximaIntensityLocations, const real ScopeFactor = _REAL_THREE_);
                            real ContinuousGaussianFusion(uint* pGlobalMaximaIntensityLocations, const real RangePrecision = _REAL_MILI_, const real ScopeFactor = _REAL_THREE_);

                        protected:

                            real EstimateNonNormalizedEpanechnikovDensity(const real Intensity, const real BandWidth) const;

#ifdef _FUSION_USE_LOOKUP_TABLE_
                            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            real EstimateNonNormalizedGaussianDensity(const real Intensity, const real ScopeBandWidth, const real LUTBandWidthExponenFactor) const;
                            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#else
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            real EstimateNonNormalizedGaussianDensity(const real Intensity, const real ScopeBandWidth, const real BandWidthExponenFactor) const;
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif

                            bool m_IsActive;
                            uint m_TotalSamples;
                            uint m_Accumulator;
                            uint m_SquareAccumulator;
                            uint m_Minimal;
                            uint m_Maximal;
                            uint m_MaximalFrequency;
                            real m_Fusion;
                            FusionBin* m_pFusionBins;
                        };
                    }
                }
            }
        }
    }
}

