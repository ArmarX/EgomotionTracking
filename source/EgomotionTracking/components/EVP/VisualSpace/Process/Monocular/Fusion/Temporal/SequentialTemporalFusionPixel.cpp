/*
 * TemporalFusionPixel.cpp
 *
 *  Created on: Apr 20, 2012
 *      Author: david
 */

#include "SequentialTemporalFusionPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                namespace Fusion
                {
                    namespace Temporal
                    {
                        CSequentialTemporalFusionPixel::CSequentialTemporalFusionPixel() :
                            m_Accumulator(0), m_SquareAccumulator(0), m_Minimal(_BYTE_MAX_), m_Maximal(_BYTE_MIN_), m_Fusion(_REAL_ZERO_), m_IntensitySamples()
                        {
                        }

                        CSequentialTemporalFusionPixel::~CSequentialTemporalFusionPixel()
                            = default;

                        void CSequentialTemporalFusionPixel::SetMaximalSamplingScope(const uint MaximalSamplingScope)
                        {
                            m_IntensitySamples.reserve(MaximalSamplingScope);
                        }

                        void CSequentialTemporalFusionPixel::AddIntialSample(const byte Intensity)
                        {
                            m_Accumulator = Intensity;
                            m_SquareAccumulator = Intensity * Intensity;
                            m_Maximal = Intensity;
                            m_Minimal = Intensity;
                            m_IntensitySamples.push_back(Intensity);
                        }

                        void CSequentialTemporalFusionPixel::AddSubsequentSample(const byte Intensity)
                        {
                            m_Accumulator += Intensity;
                            m_SquareAccumulator += Intensity * Intensity;
                            if (Intensity > m_Maximal)
                            {
                                m_Maximal = Intensity;
                            }
                            else if (Intensity < m_Minimal)
                            {
                                m_Minimal = Intensity;
                            }
                            m_IntensitySamples.push_back(Intensity);
                        }

                        uint CSequentialTemporalFusionPixel::GetTotalSamples() const
                        {
                            return m_IntensitySamples.size();
                        }

                        void CSequentialTemporalFusionPixel::ClearSamples()
                        {
                            m_IntensitySamples.clear();
                        }

                        uint CSequentialTemporalFusionPixel::GetMinimal() const
                        {
                            return m_Minimal;
                        }

                        uint CSequentialTemporalFusionPixel::GetMaximal() const
                        {
                            return m_Maximal;
                        }

                        uint CSequentialTemporalFusionPixel::GetRange() const
                        {
                            if (m_IntensitySamples.size())
                            {
                                return m_Maximal - m_Minimal + 1;
                            }
                            return 0;
                        }

                        real CSequentialTemporalFusionPixel::GetMean() const
                        {
                            if (m_IntensitySamples.size())
                            {
                                return real(m_Accumulator) / real(m_IntensitySamples.size());
                            }
                            return _REAL_ZERO_;
                        }

                        real CSequentialTemporalFusionPixel::GetMedian() const
                        {
                            vector<byte> SortedSamples = m_IntensitySamples;
                            TSort(SortedSamples.begin(), SortedSamples.end());
                            const uint TotalSamples = m_IntensitySamples.size();
                            const real ContinousMedianLocation = real(TotalSamples) * _REAL_HALF_;
                            const uint LowerBound = DownToInteger(ContinousMedianLocation);
                            const real A = SortedSamples[LowerBound];
                            return ((ContinousMedianLocation - LowerBound) * (SortedSamples[TMin(LowerBound, TotalSamples - 1u)] - A)) + A;
                        }

                        real CSequentialTemporalFusionPixel::GetStandardDeviation() const
                        {
                            if (m_IntensitySamples.size())
                            {
                                const uint TotalSamples = m_IntensitySamples.size();
                                const real Mean = real(m_Accumulator) / real(TotalSamples);
                                const real Variance = (m_SquareAccumulator / real(TotalSamples)) - (Mean * Mean);
                                if (Variance > _REAL_EPSILON_)
                                {
                                    return RealSqrt(Variance);
                                }
                            }
                            return _REAL_ZERO_;
                        }

                        real CSequentialTemporalFusionPixel::GetDispersion() const
                        {
                            return m_Fusion - (real(m_Accumulator) / real(m_IntensitySamples.size()));
                        }

                        real CSequentialTemporalFusionPixel::GetFusion() const
                        {
                            return m_Fusion;
                        }

                        real CSequentialTemporalFusionPixel::GetSignalToNoiseRatio() const
                        {
                            const uint TotalSamples = m_IntensitySamples.size();
                            const real Mean = real(m_Accumulator) / real(TotalSamples);
                            const real StandardDeviation = RealSqrt((m_SquareAccumulator / real(TotalSamples)) - Mean * Mean);
                            return (StandardDeviation > _REAL_EPSILON_) ? real(20.0) * RealLog10(Mean / StandardDeviation) : _REAL_ZERO_;
                        }

                        const vector<byte>& CSequentialTemporalFusionPixel::GetSamples() const
                        {
                            return m_IntensitySamples;
                        }

                        byte CSequentialTemporalFusionPixel::GetSampleByIndex(const uint Index) const
                        {
                            return m_IntensitySamples[Index];
                        }

                        real CSequentialTemporalFusionPixel::MeanFusion()
                        {
                            m_Fusion = real(m_Accumulator) / real(m_IntensitySamples.size());
                            return m_Fusion;
                        }

                        real CSequentialTemporalFusionPixel::EpanechnikovFusion(const real SamplingPrecision, const real OptimizationPrecision)
                        {
                            const real Range = m_Maximal - m_Minimal;
                            if (Range >= SamplingPrecision)
                            {
                                const uint TotalSamples = m_IntensitySamples.size();
                                const real Mean = m_Accumulator / real(TotalSamples);
                                const real BandWidth = TMax(_REAL_ONE_, _REAL_THREE_ * RealPow(real(4.0) / real(3 * TotalSamples) * RealPow((m_SquareAccumulator / real(TotalSamples)) - Mean * Mean, real(2.5)), real(1.0 / 5.0)));
                                const real KernelBandWidthFactor = _REAL_ONE_ / (BandWidth * BandWidth);
                                vector<byte>::const_iterator SelectedSamplesBegin = m_IntensitySamples.begin();
                                vector<byte>::const_iterator SelectedSamplesEnd = m_IntensitySamples.end();
                                real MaximalDensity = _REAL_ZERO_;
                                for (real SlidingIntensiyValue = m_Minimal; SlidingIntensiyValue <= m_Maximal; SlidingIntensiyValue += SamplingPrecision)
                                {
                                    real SamplingDensity = _REAL_ZERO_;
                                    for (vector<byte>::const_iterator pSample = SelectedSamplesBegin; pSample != SelectedSamplesEnd; ++pSample)
                                    {
                                        const real Deviation = RealAbs(SlidingIntensiyValue - real(*pSample));
                                        if (Deviation < BandWidth)
                                        {
                                            SamplingDensity += _REAL_ONE_ - (Deviation * Deviation * KernelBandWidthFactor);
                                        }
                                    }
                                    if (SamplingDensity > MaximalDensity)
                                    {
                                        MaximalDensity = SamplingDensity;
                                        m_Fusion = SlidingIntensiyValue;
                                    }
                                }
                                real A = TMax(m_Fusion - SamplingPrecision, real(m_Minimal)), B = TMin(m_Fusion + SamplingPrecision, real(m_Maximal)), DensityA = _REAL_ZERO_, DensityB = _REAL_ZERO_;
                                for (vector<byte>::const_iterator pSample = m_IntensitySamples.begin(); pSample != SelectedSamplesEnd; ++pSample)
                                {
                                    const real DeviationA = RealAbs(A - real(*pSample));
                                    const real DeviationB = RealAbs(B - real(*pSample));
                                    if (DeviationA < BandWidth)
                                    {
                                        DensityA += _REAL_ONE_ - (DeviationA * DeviationA * KernelBandWidthFactor);
                                    }
                                    if (DeviationB < BandWidth)
                                    {
                                        DensityB += _REAL_ONE_ - (DeviationB * DeviationB * KernelBandWidthFactor);
                                    }
                                }
                                do
                                {
                                    if (DensityA > DensityB)
                                    {
                                        B = m_Fusion;
                                        DensityB = MaximalDensity;
                                    }
                                    else if (DensityB > DensityA)
                                    {
                                        A = m_Fusion;
                                        DensityA = MaximalDensity;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                    m_Fusion = (A * DensityA + B * DensityB) / (DensityA + DensityB);
                                    MaximalDensity = _REAL_ZERO_;
                                    for (vector<byte>::const_iterator pSample = SelectedSamplesBegin; pSample != SelectedSamplesEnd; ++pSample)
                                    {
                                        const real Deviation = RealAbs(m_Fusion - real(*pSample));
                                        if (Deviation < BandWidth)
                                        {
                                            MaximalDensity += _REAL_ONE_ - (Deviation * Deviation * KernelBandWidthFactor);
                                        }
                                    }
                                }
                                while ((B - A) > OptimizationPrecision);
                            }
                            else
                            {
                                m_Fusion = (m_Accumulator / real(m_IntensitySamples.size()));
                            }
                            return m_Fusion;
                        }

                        real CSequentialTemporalFusionPixel::GaussianFusion(const real SamplingPrecision, const real OptimizationPrecision)
                        {
                            const real Range = m_Maximal - m_Minimal;
                            if (Range >= SamplingPrecision)
                            {
                                const uint TotalSamples = m_IntensitySamples.size();
                                const real Mean = m_Accumulator / real(TotalSamples);
                                const real SquareBandWidth = RealPow(real(4.0) / real(3 * TotalSamples) * RealPow((m_SquareAccumulator / real(TotalSamples)) - Mean * Mean, real(2.5)), real(2.0 / 5.0));
                                const real KernelBandWidthFactor = -_REAL_ONE_ / (_REAL_TWO_ * SquareBandWidth);
                                vector<byte>::const_iterator SelectedSamplesBegin = m_IntensitySamples.begin();
                                vector<byte>::const_iterator SelectedSamplesEnd = m_IntensitySamples.end();
                                real MaximalDensity = _REAL_ZERO_;
                                for (real SlidingIntensiyValue = m_Minimal; SlidingIntensiyValue <= m_Maximal; SlidingIntensiyValue += SamplingPrecision)
                                {
                                    real SamplingDensity = _REAL_ZERO_;
                                    for (vector<byte>::const_iterator pSample = SelectedSamplesBegin; pSample != SelectedSamplesEnd; ++pSample)
                                    {
                                        const real Deviation = SlidingIntensiyValue - real(*pSample);
                                        SamplingDensity += RealExp(Deviation * Deviation * KernelBandWidthFactor);
                                    }
                                    if (SamplingDensity > MaximalDensity)
                                    {
                                        MaximalDensity = SamplingDensity;
                                        m_Fusion = SlidingIntensiyValue;
                                    }
                                }
                                real A = TMax(m_Fusion - SamplingPrecision, real(m_Minimal)), B = TMin(m_Fusion + SamplingPrecision, real(m_Maximal)), DensityA = _REAL_ZERO_, DensityB = _REAL_ZERO_;
                                for (vector<byte>::const_iterator pSample = m_IntensitySamples.begin(); pSample != SelectedSamplesEnd; ++pSample)
                                {
                                    const real DeviationA = A - real(*pSample);
                                    const real DeviationB = B - real(*pSample);
                                    DensityA += RealExp(DeviationA * DeviationA * KernelBandWidthFactor);
                                    DensityB += RealExp(DeviationB * DeviationB * KernelBandWidthFactor);
                                }
                                do
                                {
                                    if (DensityA > DensityB)
                                    {
                                        B = m_Fusion;
                                        DensityB = MaximalDensity;
                                    }
                                    else if (DensityB > DensityA)
                                    {
                                        A = m_Fusion;
                                        DensityA = MaximalDensity;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                    m_Fusion = (A * DensityA + B * DensityB) / (DensityA + DensityB);
                                    MaximalDensity = _REAL_ZERO_;
                                    for (vector<byte>::const_iterator pSample = SelectedSamplesBegin; pSample != SelectedSamplesEnd; ++pSample)
                                    {
                                        const real Deviation = m_Fusion - real(*pSample);
                                        MaximalDensity += RealExp(Deviation * Deviation * KernelBandWidthFactor);
                                    }
                                }
                                while ((B - A) > OptimizationPrecision);
                            }
                            else
                            {
                                m_Fusion = (m_Accumulator / real(m_IntensitySamples.size()));
                            }
                            return m_Fusion;
                        }

                        bool CSequentialTemporalFusionPixel::ExportToFile(const_string pPathFileName, const real SamplingPrecision, const CPixelLocation* pLocation) const
                        {
                            std::ostringstream OutputText;
                            OutputText << "Temporal Fusion Pixel" << g_EndLine << "[General Information]" << g_EndLine;
                            if (pLocation)
                            {
                                OutputText << g_Tabulator << "Location" << g_Tabulator << "(" << pLocation->m_x << g_Tabulator << "," << pLocation->m_y << ")" << g_EndLine;
                            }
                            const real Mean = GetMean();
                            const real StandardDeviation = GetStandardDeviation();
                            OutputText << g_Tabulator << "Minimal" << g_Tabulator << GetMinimal() << g_EndLine;
                            OutputText << g_Tabulator << "Maximal" << g_Tabulator << GetMaximal() << g_EndLine;
                            OutputText << g_Tabulator << "Mean" << g_Tabulator << Mean << g_EndLine;
                            OutputText << g_Tabulator << "Median" << g_Tabulator << GetMedian() << g_EndLine;
                            OutputText << g_Tabulator << "Range" << g_Tabulator << GetRange() << g_EndLine;
                            OutputText << g_Tabulator << "Standard Deviation" << g_Tabulator << StandardDeviation << g_EndLine;
                            OutputText << g_Tabulator << "Fusion" << g_Tabulator << m_Fusion << g_EndLine;
                            OutputText << g_Tabulator << "Signal to Noise Ratio" << g_Tabulator << GetSignalToNoiseRatio() << g_EndLine;
                            OutputText << "[Curves]" << g_EndLine << "Samples" << g_EndLine;
                            OutputText << "Index" << g_Tabulator << "Intensity" << g_Tabulator << "Scope Maximal" << g_Tabulator << "Scope Minimal" << g_Tabulator << "Scope Range" << g_Tabulator << "Scope Mean" << g_Tabulator << "Scope Standard Deviation" << g_EndLine;
                            const uint TotalSamples = m_IntensitySamples.size();
                            uint ScopeMaximal = m_IntensitySamples[0];
                            uint ScopeMinimal = ScopeMaximal;
                            uint Accumulator = 0;
                            uint SquareAccumulator = 0;
                            for (uint i = 0; i < TotalSamples; ++i)
                            {
                                const uint Intensity = m_IntensitySamples[i];
                                Accumulator += Intensity;
                                SquareAccumulator += Intensity * Intensity;
                                if (Intensity > ScopeMaximal)
                                {
                                    ScopeMaximal = Intensity;
                                }
                                else if (Intensity < ScopeMinimal)
                                {
                                    ScopeMinimal = Intensity;
                                }
                                const real Mean = real(Accumulator) / real(i + 1);
                                const real StandardDeviation = RealSqrt((m_SquareAccumulator / real(i + 1)) - Mean * Mean);
                                OutputText << i << g_Tabulator << Intensity << g_Tabulator << ScopeMaximal << g_Tabulator << ScopeMinimal << g_Tabulator << (ScopeMaximal - ScopeMinimal) << g_Tabulator << Mean << g_Tabulator << StandardDeviation << g_EndLine;
                            }
                            const real GaussianKernelBandWidth = RealPow(real(4.0) / real(3 * TotalSamples) * RealPow(StandardDeviation, real(5.0)), real(1.0 / 5.0));
                            const real GaussianKernelBandWidthFactor = -_REAL_ONE_ / (_REAL_TWO_ * GaussianKernelBandWidth * GaussianKernelBandWidth);
                            const real EpanechnikovKernelBandWidth = TMax(_REAL_ONE_, _REAL_THREE_ * GaussianKernelBandWidth);
                            const real EpanechnikovKernelBandWidthFactor = _REAL_ONE_ / (EpanechnikovKernelBandWidth * EpanechnikovKernelBandWidth);
                            vector<byte>::const_iterator SelectedSamplesBegin = m_IntensitySamples.begin(), SelectedSamplesEnd = m_IntensitySamples.end();
                            OutputText << "Kernel Density Estimation" << g_EndLine << "Intensity" << g_Tabulator << "KDE Epanechnikov-Density ( H_E = " << EpanechnikovKernelBandWidth << ")" << g_Tabulator << "KDE Gaussian-Density( H_G = " << GaussianKernelBandWidth << ")" << g_EndLine;
                            real TotalEpanechnikovDensity = _REAL_ZERO_, TotalGaussianDensity = _REAL_ZERO_;
                            vector<real> EpanechnikovPDF;
                            vector<real> GaussianPDF;
                            for (real SlidingIntensity = m_Minimal; SlidingIntensity <= m_Maximal; SlidingIntensity += SamplingPrecision)
                            {
                                real EpanechnikovDensity = _REAL_ZERO_;
                                real GaussianDensity = _REAL_ZERO_;
                                for (vector<byte>::const_iterator pSample = SelectedSamplesBegin; pSample != SelectedSamplesEnd; ++pSample)
                                {
                                    const real Deviation = SlidingIntensity - real(*pSample);
                                    GaussianDensity += RealExp(Deviation * Deviation * GaussianKernelBandWidthFactor);
                                    if (RealAbs(Deviation) < EpanechnikovKernelBandWidth)
                                    {
                                        EpanechnikovDensity += _REAL_ONE_ - (Deviation * Deviation * EpanechnikovKernelBandWidthFactor);
                                    }
                                }
                                EpanechnikovPDF.push_back(EpanechnikovDensity);
                                TotalEpanechnikovDensity += EpanechnikovDensity;
                                GaussianPDF.push_back(GaussianDensity);
                                TotalGaussianDensity += GaussianDensity;
                            }
                            uint Index = 0;
                            for (real SlidingIntensity = m_Minimal; SlidingIntensity <= m_Maximal; SlidingIntensity += SamplingPrecision, ++Index)
                            {
                                OutputText << SlidingIntensity << g_Tabulator << (EpanechnikovPDF[Index] / TotalEpanechnikovDensity) << g_Tabulator << (GaussianPDF[Index] / TotalGaussianDensity) << g_EndLine;
                            }
                            return Files::COutFile::WriteStringToFile(pPathFileName, OutputText.str());
                        }
                    }
                }
            }
        }
    }
}

