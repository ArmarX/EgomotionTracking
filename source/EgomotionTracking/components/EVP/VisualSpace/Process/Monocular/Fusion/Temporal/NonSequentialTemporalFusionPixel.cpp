/*
 * NonSequentialTemporalFusionPixel.cpp
 *
 *  Created on: 15.05.2012
 *      Author: gonzalez
 */

#include "NonSequentialTemporalFusionPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                namespace Fusion
                {
                    namespace Temporal
                    {

#ifdef _FUSION_USE_LOOKUP_TABLE_
                        ///////////////////////////////////////////////////////////////////////
                        INITIALIZE_EXPONENTIAL_LOOKUP_TABLE(CNonSequentialTemporalFusionPixel)
                        ///////////////////////////////////////////////////////////////////////
#endif
                        CNonSequentialTemporalFusionPixel::CNonSequentialTemporalFusionPixel() :
                            m_IsActive(false), m_TotalSamples(0), m_Accumulator(0), m_SquareAccumulator(0), m_Minimal(0), m_Maximal(0), m_MaximalFrequency(0), m_Fusion(_REAL_ZERO_), m_pFusionBins(nullptr)
                        {
                            m_pFusionBins = new FusionBin[256];
                            memset(m_pFusionBins, 0, sizeof(FusionBin) * 256);
#ifdef _FUSION_USE_LOOKUP_TABLE_
                            ////////////////////////////////////////////////////////////////
                            LOAD_EXPONENTIAL_LOOKUP_TABLE(CNonSequentialTemporalFusionPixel)
                            ////////////////////////////////////////////////////////////////
#endif
                        }

                        CNonSequentialTemporalFusionPixel::~CNonSequentialTemporalFusionPixel()
                        {
                            RELEASE_ARRAY(m_pFusionBins)
                        }

                        void CNonSequentialTemporalFusionPixel::SetActive(const bool Active)
                        {
                            m_IsActive = Active;
                        }

                        bool CNonSequentialTemporalFusionPixel::IsActive() const
                        {
                            return m_IsActive;
                        }

                        void CNonSequentialTemporalFusionPixel::AddIntialSample(const uint SampleValue)
                        {
                            if (m_TotalSamples)
                            {
                                memset(m_pFusionBins + m_Minimal, 0, sizeof(FusionBin) * (m_Maximal - m_Minimal + 1));
                            }
                            m_MaximalFrequency = m_TotalSamples = 1;
                            m_Accumulator = m_Maximal = m_Minimal = SampleValue;
                            m_SquareAccumulator = SampleValue * SampleValue;
                            ++m_pFusionBins[SampleValue];
                        }

                        void CNonSequentialTemporalFusionPixel::AddSubsequentSample(const uint SampleValue)
                        {
                            if (SampleValue > m_Maximal)
                            {
                                m_Maximal = SampleValue;
                            }
                            else if (SampleValue < m_Minimal)
                            {
                                m_Minimal = SampleValue;
                            }
                            m_Accumulator += SampleValue;
                            m_SquareAccumulator += SampleValue * SampleValue;
                            if ((++m_pFusionBins[SampleValue]) > m_MaximalFrequency)
                            {
                                m_MaximalFrequency = m_pFusionBins[SampleValue];
                            }
                            ++m_TotalSamples;
                        }

                        void CNonSequentialTemporalFusionPixel::ClearSamples()
                        {
                            memset(m_pFusionBins + m_Minimal, 0, sizeof(FusionBin) * (m_Maximal - m_Minimal + 1));
                            m_TotalSamples = 0;
                        }

                        const CNonSequentialTemporalFusionPixel::FusionBin* CNonSequentialTemporalFusionPixel::GetFusionBins() const
                        {
                            return m_pFusionBins;
                        }

                        uint CNonSequentialTemporalFusionPixel::GetTotalSamples() const
                        {
                            return m_TotalSamples;
                        }

                        real CNonSequentialTemporalFusionPixel::GetMean() const
                        {
                            if (m_TotalSamples)
                            {
                                return real(m_Accumulator) / real(m_TotalSamples);
                            }
                            return _REAL_ZERO_;
                        }

                        real CNonSequentialTemporalFusionPixel::GetMedian() const
                        {
                            if (m_TotalSamples)
                            {
                                if (m_TotalSamples > 1)
                                {
                                    const real ContinousMedianSamples = real(m_TotalSamples) * _REAL_HALF_;
                                    const uint DiscreteMedianSamples = uint(ContinousMedianSamples);
                                    uint Accumulator = 0;
                                    for (uint i = m_Minimal; i <= m_Maximal; ++i)
                                    {
                                        const uint NextAccumulator = Accumulator + m_pFusionBins[i];
                                        if (NextAccumulator >= DiscreteMedianSamples)
                                        {
                                            return real(i) - ((real(Accumulator) - real(NextAccumulator)) * (real(i) - ContinousMedianSamples));
                                        }
                                        Accumulator = NextAccumulator;
                                    }
                                }
                                return m_Minimal;
                            }
                            return _REAL_ZERO_;
                        }

                        real CNonSequentialTemporalFusionPixel::GetStandardDeviation() const
                        {
                            if (m_TotalSamples > 1)
                            {
                                const real Mean = real(m_Accumulator) / real(m_TotalSamples);
                                const real Variance = ((m_SquareAccumulator) / real(m_TotalSamples)) - (Mean * Mean);
                                if (Variance > _REAL_EPSILON_)
                                {
                                    return RealSqrt(Variance);
                                }
                            }
                            return _REAL_ZERO_;
                        }

                        real CNonSequentialTemporalFusionPixel::GetBandWidth() const
                        {
                            if (m_Maximal - m_Minimal)
                            {
                                const real Mean = real(m_Accumulator) / real(m_TotalSamples);
                                const real Variance = ((m_SquareAccumulator) / real(m_TotalSamples)) - (Mean * Mean);
                                if (Variance > _REAL_EPSILON_)
                                {
                                    return RealPow(real(4.0 / 3.0f) * RealPow(Variance, real(2.5)) / real(m_TotalSamples), real(1.0 / 5.0));
                                }
                            }
                            return _REAL_ZERO_;
                        }

                        uint CNonSequentialTemporalFusionPixel::GetMinimal() const
                        {
                            return m_Minimal;
                        }

                        uint CNonSequentialTemporalFusionPixel::GetMaximal() const
                        {
                            return m_Maximal;
                        }

                        uint CNonSequentialTemporalFusionPixel::GetRange() const
                        {
                            return m_Maximal - m_Minimal;
                        }

                        real CNonSequentialTemporalFusionPixel::GetFusion() const
                        {
                            return m_Fusion;
                        }

                        real CNonSequentialTemporalFusionPixel::GetDispersion() const
                        {
                            if (m_TotalSamples > 1)
                            {
                                return m_Fusion - (real(m_Accumulator) / real(m_TotalSamples));
                            }
                            return _REAL_ZERO_;
                        }

                        real CNonSequentialTemporalFusionPixel::GetSignalToNoiseRatio() const
                        {
                            if (m_TotalSamples > 1)
                            {
                                const real Mean = real(m_Accumulator) / real(m_TotalSamples);
                                const real Variance = ((m_SquareAccumulator) / real(m_TotalSamples)) - (Mean * Mean);
                                if (Variance > _REAL_EPSILON_)
                                {
                                    return Mean / RealSqrt(Variance);
                                }
                            }
                            return _REAL_ZERO_;
                        }

                        uint CNonSequentialTemporalFusionPixel::GetMaximalFrequency() const
                        {
                            return m_MaximalFrequency;
                        }

                        uint CNonSequentialTemporalFusionPixel::GetGlobalMaxima(uint* pGlobalMaximaIntensityLocations) const
                        {
                            if (m_TotalSamples)
                            {
                                uint TotalGlobalMaxima = 0;
                                if (m_Maximal - m_Minimal)
                                {
                                    for (uint i = m_Minimal; i <= m_Maximal; ++i)
                                        if (m_pFusionBins[i] == m_MaximalFrequency)
                                        {
                                            pGlobalMaximaIntensityLocations[TotalGlobalMaxima++] = i;
                                        }
                                }
                                else
                                {
                                    pGlobalMaximaIntensityLocations[TotalGlobalMaxima++] = m_Maximal;
                                }
                                return TotalGlobalMaxima;
                            }
                            return m_TotalSamples;
                        }

                        bool CNonSequentialTemporalFusionPixel::ExportToFile(const_string pPathFileName, const CPixelLocation* pLocation) const
                        {
                            if (pPathFileName)
                            {
                                std::ostringstream OutputText;
                                OutputText << "Non-Sequential Temporal Fusion Pixel" << g_EndLine;
                                if (pLocation)
                                {
                                    OutputText << g_EndLineTabulator << "Location" << g_Tabulator << "x = " << pLocation->m_x << g_Tabulator << "y = " << pLocation->m_y;
                                }
                                OutputText << g_EndLineTabulator << "Minimal" << g_Tabulator << m_Minimal;
                                OutputText << g_EndLineTabulator << "Maximal" << g_Tabulator << m_Maximal;
                                OutputText << g_EndLineTabulator << "Mean" << g_Tabulator << GetMean();
                                OutputText << g_EndLineTabulator << "Range" << g_Tabulator << GetRange();
                                OutputText << g_EndLineTabulator << "Maximal Frequency" << g_Tabulator << GetMaximalFrequency();
                                OutputText << g_EndLineTabulator << "Standard Deviation" << g_Tabulator << GetStandardDeviation();
                                OutputText << g_EndLineTabulator << "Fusion" << g_Tabulator << m_Fusion;
                                OutputText << g_EndLineTabulator << "Dispersion" << g_Tabulator << GetDispersion();
                                OutputText << g_EndLineTabulator << "Absolute Dispersion" << g_Tabulator << RealAbs(GetDispersion());
                                OutputText << g_EndLineTabulator << "Signal-to-Noise Ratio" << g_Tabulator << GetSignalToNoiseRatio();
                                OutputText << g_EndLineTabulator << "Intensity" << g_Tabulator << "Frequency";
                                const FusionBin* pFusionBin = m_pFusionBins + m_Minimal;
                                for (uint Intensity = m_Minimal; Intensity <= m_Maximal; ++Intensity)
                                {
                                    OutputText << g_EndLineTabulator << Intensity << g_Tabulator << *pFusionBin++;
                                }
                                return Files::COutFile::WriteStringToFile(pPathFileName, OutputText.str());
                            }
                            return false;
                        }

                        real CNonSequentialTemporalFusionPixel::MeanFusion()
                        {
                            m_Fusion = real(m_Accumulator) / real(m_TotalSamples);
                            return m_Fusion;
                        }

                        uint CNonSequentialTemporalFusionPixel::DiscreteEpanechnikovFusion(uint* pGlobalMaximaIntensityLocations, const real ScopeFactor)
                        {
                            if (m_Maximal == m_Minimal)
                            {
                                m_Fusion = m_Maximal;
                                return m_Fusion;
                            }
                            uint TotalGlobalMaxima = 0;
                            for (uint i = m_Minimal; i <= m_Maximal; ++i)
                                if (m_pFusionBins[i] == m_MaximalFrequency)
                                {
                                    pGlobalMaximaIntensityLocations[TotalGlobalMaxima++] = i;
                                }
                            if (TotalGlobalMaxima > 1)
                            {
                                const real Mean = real(m_Accumulator) / real(m_TotalSamples);
                                const real BandWidth = RealPow(real(4.0 / 3.0f) * RealPow((m_SquareAccumulator / real(m_TotalSamples)) - (Mean * Mean), real(2.5)) / real(m_TotalSamples), real(1.0 / 5.0)) * ScopeFactor;
                                real MaximalDensity = EstimateNonNormalizedEpanechnikovDensity(pGlobalMaximaIntensityLocations[0], BandWidth);
                                uint SelectedIndex = 0;
                                for (uint i = 1; i < TotalGlobalMaxima; ++i)
                                {
                                    const real Density = EstimateNonNormalizedEpanechnikovDensity(pGlobalMaximaIntensityLocations[i], BandWidth);
                                    if (Density > MaximalDensity)
                                    {
                                        MaximalDensity = Density;
                                        SelectedIndex = i;
                                    }
                                }
                                m_Fusion = pGlobalMaximaIntensityLocations[SelectedIndex];
                                return m_Fusion;
                            }
                            m_Fusion = pGlobalMaximaIntensityLocations[0];
                            return m_Fusion;
                        }

                        real CNonSequentialTemporalFusionPixel::ContinuousEpanechnikovFusion(uint* pGlobalMaximaIntensityLocations, const real RangePrecision, const real ScopeFactor)
                        {
                            if (m_Maximal == m_Minimal)
                            {
                                m_Fusion = m_Maximal;
                                return m_Fusion;
                            }
                            uint TotalGlobalMaxima = 0;
                            for (uint i = m_Minimal; i <= m_Maximal; ++i)
                                if (m_pFusionBins[i] == m_MaximalFrequency)
                                {
                                    pGlobalMaximaIntensityLocations[TotalGlobalMaxima++] = i;
                                }
                            const real Mean = real(m_Accumulator) / real(m_TotalSamples);
                            const real BandWidth = Mathematics::_1D::NormalDistribution::DetermineBandWidthByVariance((m_SquareAccumulator / real(m_TotalSamples)) - (Mean * Mean), m_TotalSamples) * ScopeFactor;
                            if (TotalGlobalMaxima > 1)
                            {
                                real MaximalDensity = EstimateNonNormalizedEpanechnikovDensity(pGlobalMaximaIntensityLocations[0], BandWidth);
                                uint SelectedIndex = 0;
                                for (uint i = 1; i < TotalGlobalMaxima; ++i)
                                {
                                    const real Density = EstimateNonNormalizedEpanechnikovDensity(pGlobalMaximaIntensityLocations[i], BandWidth);
                                    if (Density > MaximalDensity)
                                    {
                                        MaximalDensity = Density;
                                        SelectedIndex = i;
                                    }
                                }
                                m_Fusion = pGlobalMaximaIntensityLocations[SelectedIndex];
                            }
                            else
                            {
                                m_Fusion = pGlobalMaximaIntensityLocations[0];
                            }
                            real IntensityA = TMax(m_Fusion - _REAL_HALF_, real(m_Minimal));
                            real IntensityB = TMin(m_Fusion + _REAL_HALF_, real(m_Maximal));
                            real DensityA = EstimateNonNormalizedEpanechnikovDensity(IntensityA, BandWidth);
                            real DensityB = EstimateNonNormalizedEpanechnikovDensity(IntensityB, BandWidth);
                            real DistanceAB = ((DensityB > DensityA) ? (m_Fusion - IntensityA) : (IntensityB - m_Fusion));
                            while (DistanceAB >= RangePrecision)
                            {
                                if (DensityB > DensityA)
                                {
                                    IntensityA += DistanceAB;
                                    DensityA = EstimateNonNormalizedEpanechnikovDensity(IntensityA, BandWidth);
                                }
                                else
                                {
                                    IntensityB -= DistanceAB;
                                    DensityB = EstimateNonNormalizedEpanechnikovDensity(IntensityB, BandWidth);
                                }
                                DistanceAB = (IntensityB - IntensityA) * _REAL_HALF_;
                            }
                            m_Fusion = DensityA > DensityB ? IntensityA : IntensityB;
                            return m_Fusion;
                        }

                        uint CNonSequentialTemporalFusionPixel::DiscreteGaussianFusion(uint* pGlobalMaximaIntensityLocations, const real ScopeFactor)
                        {
                            if (m_Maximal == m_Minimal)
                            {
                                m_Fusion = m_Maximal;
                                return m_Fusion;
                            }
                            uint TotalGlobalMaxima = 0;
                            for (uint i = m_Minimal; i <= m_Maximal; ++i)
                                if (m_pFusionBins[i] == m_MaximalFrequency)
                                {
                                    pGlobalMaximaIntensityLocations[TotalGlobalMaxima++] = i;
                                }
                            if (TotalGlobalMaxima > 1)
                            {
                                const real Mean = real(m_Accumulator) / real(m_TotalSamples);
                                const real StandardDeviation = RealSqrt((m_SquareAccumulator / real(m_TotalSamples)) - (Mean * Mean));
                                const real ScopeBandWidth = StandardDeviation * ScopeFactor;
                                const real BandWidth = Mathematics::_1D::NormalDistribution::DetermineBandWidthByStandardDeviation(StandardDeviation, m_TotalSamples);
#ifdef _FUSION_USE_LOOKUP_TABLE_
                                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                const real BandWidthExponenFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(BandWidth) * s_ELT.m_SamplesPerUnit;
#else
                                const real BandWidthExponenFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(BandWidth);
#endif
                                real MaximalDensity = EstimateNonNormalizedGaussianDensity(pGlobalMaximaIntensityLocations[0], ScopeBandWidth, BandWidthExponenFactor);
                                uint SelectedIndex = 0;
                                for (uint i = 1; i < TotalGlobalMaxima; ++i)
                                {
                                    const real Density = EstimateNonNormalizedGaussianDensity(pGlobalMaximaIntensityLocations[i], ScopeBandWidth, BandWidthExponenFactor);
                                    if (Density > MaximalDensity)
                                    {
                                        MaximalDensity = Density;
                                        SelectedIndex = i;
                                    }
                                }
                                m_Fusion = pGlobalMaximaIntensityLocations[SelectedIndex];
                                return m_Fusion;
                            }
                            m_Fusion = pGlobalMaximaIntensityLocations[0];
                            return m_Fusion;
                        }

                        real CNonSequentialTemporalFusionPixel::ContinuousGaussianFusion(uint* pGlobalMaximaIntensityLocations, const real RangePrecision, const real ScopeFactor)
                        {
                            if (m_Maximal == m_Minimal)
                            {
                                m_Fusion = m_Maximal;
                                return m_Fusion;
                            }
                            uint TotalGlobalMaxima = 0;
                            for (uint i = m_Minimal; i <= m_Maximal; ++i)
                                if (m_pFusionBins[i] == m_MaximalFrequency)
                                {
                                    pGlobalMaximaIntensityLocations[TotalGlobalMaxima++] = i;
                                }
                            const real Mean = real(m_Accumulator) / real(m_TotalSamples);
                            const real StandardDeviation = RealSqrt((m_SquareAccumulator / real(m_TotalSamples)) - (Mean * Mean));
                            const real ScopeBandWidth = StandardDeviation * ScopeFactor;
                            const real BandWidth = Mathematics::_1D::NormalDistribution::DetermineBandWidthByStandardDeviation(StandardDeviation, m_TotalSamples);
#ifdef _FUSION_USE_LOOKUP_TABLE_
                            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            const real BandWidthExponenFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(BandWidth) * s_ELT.m_SamplesPerUnit;
#else
                            const real BandWidthExponenFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(BandWidth);
#endif
                            if (TotalGlobalMaxima > 1)
                            {
                                real MaximalDensity = EstimateNonNormalizedGaussianDensity(pGlobalMaximaIntensityLocations[0], ScopeBandWidth, BandWidthExponenFactor);
                                uint SelectedIndex = 0;
                                for (uint i = 1; i < TotalGlobalMaxima; ++i)
                                {
                                    const real Density = EstimateNonNormalizedGaussianDensity(pGlobalMaximaIntensityLocations[i], ScopeBandWidth, BandWidthExponenFactor);
                                    if (Density > MaximalDensity)
                                    {
                                        MaximalDensity = Density;
                                        SelectedIndex = i;
                                    }
                                }
                                m_Fusion = pGlobalMaximaIntensityLocations[SelectedIndex];
                            }
                            else
                            {
                                m_Fusion = pGlobalMaximaIntensityLocations[0];
                            }
                            real IntensityA = TMax(m_Fusion - _REAL_HALF_, real(m_Minimal));
                            real IntensityB = TMin(m_Fusion + _REAL_HALF_, real(m_Maximal));
                            real DensityA = EstimateNonNormalizedGaussianDensity(IntensityA, ScopeBandWidth, BandWidthExponenFactor);
                            real DensityB = EstimateNonNormalizedGaussianDensity(IntensityB, ScopeBandWidth, BandWidthExponenFactor);
                            real DistanceAB = ((DensityB > DensityA) ? (m_Fusion - IntensityA) : (IntensityB - m_Fusion));
                            while (DistanceAB >= RangePrecision)
                            {
                                if (DensityB > DensityA)
                                {
                                    IntensityA += DistanceAB;
                                    DensityA = EstimateNonNormalizedGaussianDensity(IntensityA, ScopeBandWidth, BandWidthExponenFactor);
                                }
                                else
                                {
                                    IntensityB -= DistanceAB;
                                    DensityB = EstimateNonNormalizedGaussianDensity(IntensityB, ScopeBandWidth, BandWidthExponenFactor);
                                }
                                DistanceAB = (IntensityB - IntensityA) * _REAL_HALF_;
                            }
                            m_Fusion = DensityA > DensityB ? IntensityA : IntensityB;
                            return m_Fusion;
                        }

                        real CNonSequentialTemporalFusionPixel::EstimateNonNormalizedEpanechnikovDensity(const real Intensity, const real BandWidth) const
                        {
                            real Density = _REAL_ZERO_;
                            const uint UpperBound = TMin(uint(Intensity + BandWidth), m_Maximal);
                            for (uint i = TMax(int(RealCeil(Intensity - BandWidth)), int(m_Minimal)); i <= UpperBound; ++i)
                                if (m_pFusionBins[i])
                                {
                                    const real NormalizedDelta = (real(i) - Intensity) / BandWidth;
                                    Density += (_REAL_ONE_ - (NormalizedDelta * NormalizedDelta)) * real(m_pFusionBins[i]);
                                }
                            return Density;
                        }

#ifdef _FUSION_USE_LOOKUP_TABLE_
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        real CNonSequentialTemporalFusionPixel::EstimateNonNormalizedGaussianDensity(const real Intensity, const real ScopeBandWidth, const real LUTBandWidthExponenFactor) const
                        {
                            real Density = _REAL_ZERO_;
                            const uint UpperBound = TMin(uint(RealCeil(Intensity + ScopeBandWidth)), m_Maximal);
                            for (uint i = TMax(int(Intensity - ScopeBandWidth), int(m_Minimal)); i <= UpperBound; ++i)
                                if (m_pFusionBins[i])
                                {
                                    const real Delta = real(i) - Intensity;
                                    const int Index = RoundNegativeRealToInteger(Delta * Delta * LUTBandWidthExponenFactor);
                                    if (Index > s_ELT.m_MinimalIndex)
                                    {
                                        Density += s_ELT.m_pExp[Index] * real(m_pFusionBins[i]);
                                    }
                                }
                            return Density;
                        }
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#else
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        real CNonSequentialTemporalFusionPixel::EstimateNonNormalizedGaussianDensity(const real Intensity, const real ScopeBandWidth, const real BandWidthExponenFactor) const
                        {
                            real Density = _REAL_ZERO_;
                            const uint UpperBound = TMin(uint(RealCeil(Intensity + ScopeBandWidth)), m_Maximal);
                            for (uint i = TMax(uint(Intensity - ScopeBandWidth), m_Minimal); i <= UpperBound; ++i)
                                if (m_pFusionBins[i])
                                {
                                    const real Delta = real(i) - Intensity;
                                    Density += RealExp((Delta * Delta) * BandWidthExponenFactor) * real(m_pFusionBins[i]);
                                }
                            return Density;
                        }
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif
                    }
                }
            }
        }
    }
}
