/*
 * DensityFusionPixel.h
 *
 *  Created on: 21.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/DataTypes/PixelLocation.h"
#include "../../../../Foundation/Mathematics/1D/Common1D.h"
#include "../../../../Foundation/Mathematics/1D/ExponentialLookUpTable.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Fusion
        {
            class CDensityFusionPixel
            {

#ifdef _FUSION_USE_LOOKUP_TABLE_
                DECLARE_EXPONENTIAL_LOOKUP_TABLE
#endif

            public:

                //NOTE: use ushort when planning to fusing more than 255 images
                //typedef ushort FusionBin;
                typedef byte FusionBin;

                CDensityFusionPixel();

                virtual ~CDensityFusionPixel();

                void SetActive(const bool Active);

                void AddIntialSample(const byte SampleValue) _EVP_FAST_CALL_;

                void AddSample(const byte SampleValue) _EVP_FAST_CALL_;

                void ClearSamples();

                //TODO SEND TO CPP
                inline  bool IsActive() const
                {
                    return m_IsActive;
                }

                inline const FusionBin* GetSamples() const
                {
                    return m_FusionBins;
                }

                inline  uint GetTotalSamples() const
                {
                    return m_TotalSamples;
                }

                inline  uint GetValueAccumulator() const
                {
                    return m_Accumulator;
                }

                inline  uint GetMinimalValue() const
                {
                    return m_Minimal;
                }

                inline  uint GetMaximalValue() const
                {
                    return m_Maximal;
                }

                inline  real GetFusionValue() const
                {
                    return m_Fusion;
                }

                inline  real GetMeanValue() const
                {
                    return real(m_Accumulator) / real(m_TotalSamples);
                }

                inline  real GetRange() const
                {
                    return real(m_Maximal - m_Minimal);
                }

                inline  real GetDispersionValue() const
                {
                    return m_Fusion - (real(m_Accumulator) / real(m_TotalSamples));
                }

                uint GetMaximalFrequency() const;

                real GetStandardDeviation() const;

                real GetSignalToNoiseRatio() const;

                real MeanFusion();

                byte DiscreteEpanechnikovFusion(const real BandwidthFactor, const real RangePrecision, uint* pGlobalMaxima);

                byte DiscreteGaussianFusion(const real BandwidthFactor, const real RangePrecision, uint* pGlobalMaxima);

                real EpanechnikovFusion(const real BandwidthFactor, const real RangePrecision, uint* pGlobalMaxima);

                real GaussianFusion(const real BandwidthFactor, const real RangePrecision, uint* pGlobalMaxima);

                uint GetGlobalMaxima(uint* pGlobalMaxima) const _EVP_FAST_CALL_;

                bool ExportToFile(const_string pPathFileName, const CPixelLocation* pLocation) const;

            protected:

                real PreFusionGetStandardDeviation();

                real EstimateNonNormalizedEpanechnikovDensity(const real ValueLocation, const real AdaptiveBandWidth, const real SquareNormalizationBandWidth);
#ifdef _FUSION_USE_LOOKUP_TABLE_
                real EstimateNonNormalizedGaussianDensity(const real ValueLocation, const real LUTScaleFactor, const real LUTMaximalDelta);
#else
                real EstimateNonNormalizedGaussianDensity(const real ValueLocation, const real BandWidthExponentFactor);
#endif

                FusionBin* m_FusionBins;
                uint m_TotalSamples;
                uint m_Accumulator;
                uint m_SquareAccumulator;
                uint m_Minimal;
                uint m_Maximal;
                real m_Fusion;
                bool m_IsActive;
            };
        }
    }
}

