/*
 * SaliencyPixelSetAnalyzer.cpp
 *
 *  Created on: 25.03.2011
 *      Author: gonzalez
 */

#include "../../../../Foundation/Files/OutFile.h"
#include "SaliencyPixelSetAnalyzer.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            CSaliencyPixelSetAnalyzer::CSaliencyPixelSetAnalyzer()
            {
                m_MinimalSaliency = m_MaximalSaliency = m_TotalSaliency = _REAL_ZERO_;
            }

            CSaliencyPixelSetAnalyzer::~CSaliencyPixelSetAnalyzer()
            {
                m_Pixels.clear();
            }

            bool CSaliencyPixelSetAnalyzer::AnalyzePercentile(const SaliencySource Source)
            {
                if (m_Pixels.size())
                {
                    m_TotalSaliency = _REAL_ZERO_;
                    real SubTotalSaliency = _REAL_ZERO_;
                    list<CSaliencyPixel*>::const_iterator EndActiveSaliencyPixels = m_Pixels.end();
                    switch (Source)
                    {
                        case eDirectSaliency:
                        {
                            m_Pixels.sort(SortSaliencyPixelByMagnitude);
                            m_MinimalSaliency = m_Pixels.front()->GetMagnitude();
                            m_MaximalSaliency = m_Pixels.back()->GetMagnitude();
                            for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_Pixels.begin(); ppSaliencyPixel != EndActiveSaliencyPixels; ++ppSaliencyPixel)
                            {
                                m_TotalSaliency += (*ppSaliencyPixel)->GetMagnitude();
                            }
                            const real NormalizationFactor = _REAL_ONE_ / m_TotalSaliency;
                            for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_Pixels.begin(); ppSaliencyPixel != EndActiveSaliencyPixels; ++ppSaliencyPixel)
                            {
                                SubTotalSaliency += (*ppSaliencyPixel)->GetMagnitude();
                                (*ppSaliencyPixel)->SetPercentile(SubTotalSaliency * NormalizationFactor);
                            }
                        }
                        break;
                        case eOptimizedSaliency:
                        {
                            m_Pixels.sort(SortSaliencyPixelByOptimizedMagnitude);
                            m_MinimalSaliency = m_Pixels.front()->GetOptimizedMagnitude();
                            m_MaximalSaliency = m_Pixels.back()->GetOptimizedMagnitude();
                            for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_Pixels.begin(); ppSaliencyPixel != EndActiveSaliencyPixels; ++ppSaliencyPixel)
                            {
                                m_TotalSaliency += (*ppSaliencyPixel)->GetOptimizedMagnitude();
                            }
                            const real NormalizationFactor = _REAL_ONE_ / m_TotalSaliency;
                            for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_Pixels.begin(); ppSaliencyPixel != EndActiveSaliencyPixels; ++ppSaliencyPixel)
                            {
                                SubTotalSaliency += (*ppSaliencyPixel)->GetOptimizedMagnitude();
                                (*ppSaliencyPixel)->SetPercentile(SubTotalSaliency * NormalizationFactor);
                            }
                        }
                        break;
                    }
                    return true;
                }
                return false;
            }

            real CSaliencyPixelSetAnalyzer::GetSaliencyMagitudeAtPercentileReference(const real PercentileReference, const SaliencySource Source)
            {
                if (m_Pixels.size())
                {
                    if (PercentileReference <= _REAL_ZERO_)
                    {
                        return m_MinimalSaliency;
                    }
                    if (PercentileReference >= _REAL_ONE_)
                    {
                        return m_MaximalSaliency;
                    }
                    list<CSaliencyPixel*>::const_iterator EndActiveSaliencyPixels = m_Pixels.end();
                    switch (Source)
                    {
                        case eDirectSaliency:
                            for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_Pixels.begin(); ppSaliencyPixel != EndActiveSaliencyPixels; ++ppSaliencyPixel)
                                if ((*ppSaliencyPixel)->GetPercentile() >= PercentileReference)
                                {
                                    const CSaliencyPixel* pSaliencyPixelB = *ppSaliencyPixel;
                                    const CSaliencyPixel* pSaliencyPixelA = *(--ppSaliencyPixel);
                                    return (PercentileReference - pSaliencyPixelA->GetPercentile()) * ((pSaliencyPixelB->GetMagnitude() - pSaliencyPixelA->GetMagnitude()) / (pSaliencyPixelB->GetPercentile() - pSaliencyPixelA->GetPercentile())) + pSaliencyPixelA->GetMagnitude();
                                }
                            break;
                        case eOptimizedSaliency:
                            for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_Pixels.begin(); ppSaliencyPixel != EndActiveSaliencyPixels; ++ppSaliencyPixel)
                                if ((*ppSaliencyPixel)->GetPercentile() >= PercentileReference)
                                {
                                    const CSaliencyPixel* pSaliencyPixelB = *ppSaliencyPixel;
                                    const CSaliencyPixel* pSaliencyPixelA = *(--ppSaliencyPixel);
                                    return (PercentileReference - pSaliencyPixelA->GetPercentile()) * ((pSaliencyPixelB->GetOptimizedMagnitude() - pSaliencyPixelA->GetOptimizedMagnitude()) / (pSaliencyPixelB->GetPercentile() - pSaliencyPixelA->GetPercentile())) + pSaliencyPixelA->GetOptimizedMagnitude();
                                }
                            break;
                    }
                }
                return _REAL_ZERO_;
            }

            bool CSaliencyPixelSetAnalyzer::ExportPercentileCurve(const_string pPathFileName, const uint Intervals, const SaliencySource Source)
            {
                if (m_Pixels.size())
                {
                    std::ostringstream OutputText;
                    OutputText << "Saliency Pixel Set" << g_EndLine;
                    OutputText << "Cardinality" << g_Tabulator << m_Pixels.size() << g_EndLine;
                    OutputText << "Total Saliency" << g_Tabulator << m_TotalSaliency << g_EndLine;
                    OutputText << "Mean Saliency" << g_Tabulator << m_TotalSaliency / real(m_Pixels.size()) << g_EndLine;
                    OutputText << "Maximal Saliency" << g_Tabulator << m_MaximalSaliency << g_EndLine;
                    OutputText << "Minimal Saliency" << g_Tabulator << m_MinimalSaliency << g_EndLine;
                    OutputText << "Range Saliency" << g_Tabulator << m_MaximalSaliency - m_MinimalSaliency << g_EndLine;
                    OutputText << "Percentile Curves" << g_EndLineTabulator << "Percentile" << g_Tabulator << "Normalized Magnitude" << g_Tabulator << "Magnitude" << g_EndLine;
                    const real Scale = _REAL_ONE_ / (m_MaximalSaliency - m_MinimalSaliency);
                    const real Delta = _REAL_ONE_ / (real(Intervals));
                    real LastMagnitude = _REAL_MIN_;
                    list<CSaliencyPixel*>::const_iterator EndActiveSaliencyPixels = m_Pixels.end();
                    switch (Source)
                    {
                        case eDirectSaliency:
                            for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_Pixels.begin(); ppSaliencyPixel != EndActiveSaliencyPixels; ++ppSaliencyPixel)
                            {
                                const CSaliencyPixel* pSaliencyPixel = *ppSaliencyPixel;
                                const real ScaledMagnitude = (pSaliencyPixel->GetMagnitude() - m_MinimalSaliency) * Scale;
                                if ((ScaledMagnitude - LastMagnitude) >= Delta)
                                {
                                    LastMagnitude = ScaledMagnitude;
                                    OutputText << g_Tabulator << pSaliencyPixel->GetPercentile() << g_Tabulator << ScaledMagnitude << g_Tabulator << pSaliencyPixel->GetMagnitude() << g_EndLine;
                                }
                            }
                            break;
                        case eOptimizedSaliency:
                            for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_Pixels.begin(); ppSaliencyPixel != EndActiveSaliencyPixels; ++ppSaliencyPixel)
                            {
                                const CSaliencyPixel* pSaliencyPixel = *ppSaliencyPixel;
                                const real ScaledMagnitude = (pSaliencyPixel->GetOptimizedMagnitude() - m_MinimalSaliency) * Scale;
                                if ((ScaledMagnitude - LastMagnitude) >= Delta)
                                {
                                    LastMagnitude = ScaledMagnitude;
                                    OutputText << g_Tabulator << pSaliencyPixel->GetOptimizedMagnitude() << g_Tabulator << ScaledMagnitude << g_Tabulator << pSaliencyPixel->GetOptimizedMagnitude() << g_EndLine;
                                }
                            }
                            break;
                    }
                    return Files::COutFile::WriteStringToFile(pPathFileName, OutputText.str());
                }
                return false;
            }

            bool CSaliencyPixelSetAnalyzer::ExportPDF(const_string pPathFileName, const uint Intervals, const real SigmaFactor, const SaliencySource Source)
            {
                if (m_Pixels.size())
                {
                    real* pPDF = new real[Intervals];
                    const real RangeSaliency = m_MaximalSaliency - m_MinimalSaliency;
                    const real Scale = _REAL_ONE_ / (RangeSaliency);
                    const real Increment = RangeSaliency / real(Intervals);
                    const real Sigma = RangeSaliency * SigmaFactor;
                    const real MaximalDeviation = Sigma * _REAL_THREE_;
                    const real ExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(Sigma);
                    real CurrentMagnitude = m_MinimalSaliency, TotalAccumulator = _REAL_ZERO_;
                    list<CSaliencyPixel*>::const_iterator BeginActiveSaliencyPixels = m_Pixels.begin();
                    list<CSaliencyPixel*>::const_iterator EndActiveSaliencyPixels = m_Pixels.end();
                    switch (Source)
                    {
                        case eDirectSaliency:
                            for (uint i = 0; i < Intervals; ++i, CurrentMagnitude += Increment)
                            {
                                real DensityAccumulator = _REAL_ZERO_;
                                for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = BeginActiveSaliencyPixels; ppSaliencyPixel != EndActiveSaliencyPixels; ++ppSaliencyPixel)
                                {
                                    const real Delta = (*ppSaliencyPixel)->GetMagnitude() - CurrentMagnitude;
                                    if (RealAbs(Delta) > MaximalDeviation)
                                    {
                                        if (DensityAccumulator)
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            BeginActiveSaliencyPixels = ppSaliencyPixel;
                                        }
                                        continue;
                                    }
                                    DensityAccumulator += RealExp(Delta * Delta * ExponentFactor);
                                }
                                pPDF[i] = DensityAccumulator;
                                TotalAccumulator += DensityAccumulator;
                            }
                            break;
                        case eOptimizedSaliency:
                            for (uint i = 0; i < Intervals; ++i, CurrentMagnitude += Increment)
                            {
                                real DensityAccumulator = _REAL_ZERO_;
                                for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = BeginActiveSaliencyPixels; ppSaliencyPixel != EndActiveSaliencyPixels; ++ppSaliencyPixel)
                                {
                                    const real Delta = (*ppSaliencyPixel)->GetOptimizedMagnitude() - CurrentMagnitude;
                                    if (RealAbs(Delta) > MaximalDeviation)
                                    {
                                        if (DensityAccumulator)
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            BeginActiveSaliencyPixels = ppSaliencyPixel;
                                        }
                                        continue;
                                    }
                                    DensityAccumulator += RealExp(Delta * Delta * ExponentFactor);
                                }
                                pPDF[i] = DensityAccumulator;
                                TotalAccumulator += DensityAccumulator;
                            }
                            break;
                    }
                    const real NormalizationTotalAccumulator = _REAL_ONE_ / TotalAccumulator;
                    std::ostringstream OutputText;
                    OutputText << "Saliency Pixel Set" << g_EndLine;
                    OutputText << "Cardinality" << g_Tabulator << m_Pixels.size() << g_EndLine;
                    OutputText << "Total Saliency" << g_Tabulator << m_TotalSaliency << g_EndLine;
                    OutputText << "Mean Saliency" << g_Tabulator << m_TotalSaliency / real(m_Pixels.size()) << g_EndLine;
                    OutputText << "Maximal Saliency" << g_Tabulator << m_MaximalSaliency << g_EndLine;
                    OutputText << "Minimal Saliency" << g_Tabulator << m_MinimalSaliency << g_EndLine;
                    OutputText << "Range Saliency" << g_Tabulator << RangeSaliency << g_EndLine;
                    OutputText << "PDF Curve" << g_EndLineTabulator << "Magnitude" << g_Tabulator << "Normalized Magnitude" << g_Tabulator << "Density" << g_EndLine;
                    CurrentMagnitude = m_MinimalSaliency;
                    for (uint i = 0; i < Intervals; ++i, CurrentMagnitude += Increment)
                    {
                        OutputText << g_Tabulator << CurrentMagnitude << g_Tabulator << (CurrentMagnitude - m_MinimalSaliency) * Scale << g_Tabulator << pPDF[i] * NormalizationTotalAccumulator << g_EndLine;
                    }
                    RELEASE_ARRAY(pPDF)
                    return Files::COutFile::WriteStringToFile(pPathFileName, OutputText.str());
                }
                return false;
            }

            void CSaliencyPixelSetAnalyzer::AddPixel(CSaliencyPixel* pSaliencyPixel)
            {
                m_Pixels.push_back(pSaliencyPixel);
            }

            void CSaliencyPixelSetAnalyzer::Clear()
            {
                m_MinimalSaliency = m_MaximalSaliency = m_TotalSaliency = _REAL_ZERO_;
                m_Pixels.clear();
            }

            const list<CSaliencyPixel*>& CSaliencyPixelSetAnalyzer::GetPixels() const
            {
                return m_Pixels;
            }

            real CSaliencyPixelSetAnalyzer::GetTotalSaliency() const
            {
                return m_TotalSaliency;
            }

            real CSaliencyPixelSetAnalyzer::GetRangeSaliency() const
            {
                return (m_MaximalSaliency - m_MinimalSaliency);
            }

            real CSaliencyPixelSetAnalyzer::GetMeanSaliency() const
            {
                return m_Pixels.size() ? m_TotalSaliency / real(m_Pixels.size()) : _REAL_ZERO_;
            }

            uint CSaliencyPixelSetAnalyzer::GetTotalPixels() const
            {
                return m_Pixels.size();
            }

            bool CSaliencyPixelSetAnalyzer::SortSaliencyPixelByOptimizedMagnitude(const CSaliencyPixel* plhs, const CSaliencyPixel* prhs)
            {
                return plhs->GetOptimizedMagnitude() < prhs->GetOptimizedMagnitude();
            }

            bool CSaliencyPixelSetAnalyzer::SortSaliencyPixelByMagnitude(const CSaliencyPixel* plhs, const CSaliencyPixel* prhs)
            {
                return plhs->GetMagnitude() < prhs->GetMagnitude();
            }
        }
    }
}
