/*
 * EdgeSaliencyBlock.cpp
 *
 *  Created on: 29.03.2011
 *      Author: gonzalez
 */

#include "EdgeSaliencyBlock.h"
#include "SaliencyPixel.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            CEdgeSaliencyBlock::CEdgeSaliencyBlock(CSaliencyPixel* pSaliencyPixel) :
                CSaliencyBlock(pSaliencyPixel)
            {
            }

            CEdgeSaliencyBlock::~CEdgeSaliencyBlock()
                = default;
        }
    }
}
