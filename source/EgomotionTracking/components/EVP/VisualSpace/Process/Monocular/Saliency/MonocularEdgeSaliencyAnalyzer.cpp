/*
 * MonocularEdgeSaliencyAnalyzer.cpp
 *
 *  Created on: 17.12.2011
 *      Author: gonzalez
 */

#include "MonocularEdgeSaliencyAnalyzer.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularEdgeSaliencyAnalyzer::CParameters
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                CMonocularEdgeSaliencyAnalyzer::CParameters::~CParameters()
                    = default;

                CParametersBase::ParameterChangeResult CMonocularEdgeSaliencyAnalyzer::CParameters::SetTreeExpansionMode(const Saliency::CSaliencyTree::ExtractionMode Mode, const bool Wait)
                {
                    if (!((Mode == Saliency::CSaliencyTree::eMinimalDistance) || (Mode == Saliency::CSaliencyTree::eMaximalSaliency)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (Mode == m_TreeExpansionMode)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_TreeExpansionMode = Mode;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                Saliency::CSaliencyTree::ExtractionMode CMonocularEdgeSaliencyAnalyzer::CParameters::GetTreeExpansionMode() const
                {
                    return m_TreeExpansionMode;
                }

                CParametersBase::ParameterChangeResult CMonocularEdgeSaliencyAnalyzer::CParameters::SetMinimalPathLength(const uint MinimalPathLength, const bool Wait)
                {
                    if (MinimalPathLength < 4)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (MinimalPathLength == m_MinimalPathLength)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_MinimalPathLength = MinimalPathLength;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                uint CMonocularEdgeSaliencyAnalyzer::CParameters::GetMinimalPathLength() const
                {
                    return m_MinimalPathLength;
                }

                CParametersBase::ParameterChangeResult CMonocularEdgeSaliencyAnalyzer::CParameters::SetCharacterizationMode(Saliency::CSaliencyPoint::CharacterizationMode Mode, const bool Wait)
                {
                    if ((Mode == Saliency::CSaliencyPoint::eLineal) || (Mode == Saliency::CSaliencyPoint::ePower) || (Mode == Saliency::CSaliencyPoint::eApeture) || (Mode == Saliency::CSaliencyPoint::ePowerApeture))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (Mode == m_CharacterizationMode)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_CharacterizationMode = Mode;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                Saliency::CSaliencyPoint::CharacterizationMode CMonocularEdgeSaliencyAnalyzer::CParameters::GetCharacterizationMode() const
                {
                    return m_CharacterizationMode;
                }

                CParametersBase::ParameterChangeResult CMonocularEdgeSaliencyAnalyzer::CParameters::SetSpreadRegularizationDelta(const real SpreadRegularizationDelta, const bool Wait)
                {
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularEdgeSaliencyAnalyzer::CParameters::GetSpreadRegularizationDelta() const
                {
                    return m_SpreadRegularizationDelta;
                }

                CParametersBase::ParameterChangeResult CMonocularEdgeSaliencyAnalyzer::CParameters::SetCharacterizationGraphDistance(const uint CharacterizationGraphDistance, const bool Wait)
                {
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                uint CMonocularEdgeSaliencyAnalyzer::CParameters::GetCharacterizationGraphDistance() const
                {
                    return m_CharacterizationGraphDistance;
                }

                CParametersBase::ParameterChangeResult CMonocularEdgeSaliencyAnalyzer::CParameters::SetCharaterizationThresholdTransitional(const real CharaterizationThresholdTransitional, const bool Wait)
                {
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularEdgeSaliencyAnalyzer::CParameters::GetCharaterizationThresholdTransitional() const
                {
                    return m_CharaterizationThresholdTransitional;
                }

                CParametersBase::ParameterChangeResult CMonocularEdgeSaliencyAnalyzer::CParameters::SetCharaterizationThresholdElongated(const real CharaterizationThresholdElongated, const bool Wait)
                {
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularEdgeSaliencyAnalyzer::CParameters::GetCharaterizationThresholdElongated() const
                {
                    return m_CharaterizationThresholdElongated;
                }

                CParametersBase::ParameterChangeResult CMonocularEdgeSaliencyAnalyzer::CParameters::SetCharaterizationPower(const real CharaterizationPower, const bool Wait)
                {
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularEdgeSaliencyAnalyzer::CParameters::GetCharaterizationPower() const
                {
                    return m_CharaterizationPower;
                }

                CParametersBase::ParameterChangeResult CMonocularEdgeSaliencyAnalyzer::CParameters::SetEnableSmoothingPaths(const bool EnableSmoothingPaths, const bool Wait)
                {
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                bool CMonocularEdgeSaliencyAnalyzer::CParameters::GetEnableSmoothingPaths() const
                {
                    return m_EnableSmoothingPaths;
                }

                CParametersBase::ParameterChangeResult CMonocularEdgeSaliencyAnalyzer::CParameters::SetPathSmoothingSigma(const real PathSmoothingSigma, const bool Wait)
                {
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularEdgeSaliencyAnalyzer::CParameters::GetPathSmoothingSigma() const
                {
                    return m_PathSmoothingSigma;
                }

                CParametersBase::ParameterChangeResult CMonocularEdgeSaliencyAnalyzer::CParameters::SetLineSegmentUncertaintyThreshold(const real LineSegmentUncertaintyThreshold, const bool Wait)
                {
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularEdgeSaliencyAnalyzer::CParameters::GetLineSegmentUncertaintyThreshold() const
                {
                    return m_LineSegmentUncertaintyThreshold;
                }

                CParametersBase::ParameterChangeResult CMonocularEdgeSaliencyAnalyzer::CParameters::SetLineSegmentMinimalMainAxisLength(const real LineSegmentMinimalMainAxisLength, const bool Wait)
                {
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularEdgeSaliencyAnalyzer::CParameters::GetLineSegmentMinimalMainAxisLength() const
                {
                    return m_LineSegmentMinimalMainAxisLength;
                }

                CParametersBase::ParameterChangeResult CMonocularEdgeSaliencyAnalyzer::CParameters::SetLineSegmentMaximalSecondaryAxisLength(const real LineSegmentMaximalSecondaryAxisLength, const bool Wait)
                {
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularEdgeSaliencyAnalyzer::CParameters::GetLineSegmentMaximalSecondaryAxisLength() const
                {
                    return m_LineSegmentMaximalSecondaryAxisLength;
                }

                CParametersBase::ParameterChangeResult CMonocularEdgeSaliencyAnalyzer::CParameters::SetLineSegmentMinimalPointNodes(const real LineSegmentMinimalPointNodes, const bool Wait)
                {
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularEdgeSaliencyAnalyzer::CParameters::GetLineSegmentMinimalPointNodes() const
                {
                    return m_LineSegmentMinimalPointNodes;
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularEdgeSaliencyAnalyzer
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                CMonocularEdgeSaliencyAnalyzer::CMonocularEdgeSaliencyAnalyzer(CMonocularSaliencyExtractor* pMonocularSaliencyExtractor) :
                    CMonocularSaliencyAnalyzer(CImageProcessBase::eEdgeSaliencyAnalyzer, pMonocularSaliencyExtractor), m_Parameters(this), m_SmoothingKernel(m_Parameters.GetPathSmoothingSigma())
                {
                    m_SmoothingKernel.Normalize();
                }

                CMonocularEdgeSaliencyAnalyzer::~CMonocularEdgeSaliencyAnalyzer()
                    = default;

                bool CMonocularEdgeSaliencyAnalyzer::Execute(const Identifier TrialId)
                {
                    if (m_IsEnabled && m_OutputActiveZone.IsEnabled())
                    {
                        if (StartExecuting(TrialId))
                        {
                            START_PROCESS_EXECUTION_LOG("Execute", TrialId)

                            ExtractTrees(TrialId);

                            STOP_PROCESS_EXECUTION_LOG

                            return FinishExecution();
                        }
                    }
                    return false;
                }

                bool CMonocularEdgeSaliencyAnalyzer::Display()
                {
                    return false;
                }

                CMonocularEdgeSaliencyAnalyzer::CParameters* CMonocularEdgeSaliencyAnalyzer::GetParameters()
                {
                    return m_IsEnabled ? (&m_Parameters) : nullptr;
                }

                bool CMonocularEdgeSaliencyAnalyzer::ExtractTrees(const Identifier /*TrialId*/)
                {
                    ClearForest();
                    const list<Saliency::CSaliencyBlock*>& SaliencyBlocks = m_pMonocularSaliencyExtractor->GetSaliencyBlocks();
                    if (SaliencyBlocks.size())
                    {
                        list<Saliency::CSaliencyBlock*>::const_iterator EndBlocks = SaliencyBlocks.end();
                        const bool EnableSmoothPaths = m_Parameters.GetEnableSmoothingPaths();
                        const Saliency::CSaliencyTree::ExtractionMode Mode = m_Parameters.GetTreeExpansionMode();
                        const Saliency::CSaliencyPoint::CharacterizationMode CharacterizationMode = m_Parameters.GetCharacterizationMode();
                        const uint PathMinimalLengthThreshold = m_Parameters.GetMinimalPathLength();
                        const real SpreadRegularizationDelta = m_Parameters.GetSpreadRegularizationDelta();
                        const uint CharacterizationGraphDistance = m_Parameters.GetCharacterizationGraphDistance();
                        const real CharaterizationThresholdTransitional = m_Parameters.GetCharaterizationThresholdTransitional();
                        const real CharaterizationThresholdElongated = m_Parameters.GetCharaterizationThresholdElongated();
                        if (EnableSmoothPaths)
                        {
                            const uint SmoothingRadius = m_SmoothingKernel.GetRadius();
                            const uint SmoothingDiameter = m_SmoothingKernel.GetWidth();
                            const real* pSmoothingKernel = m_SmoothingKernel.GetKernelReadOnlyBuffer();
                            const real CharaterizationPower = m_Parameters.GetCharaterizationPower();
                            coordinate pOctoConnectivityDeltas[8] = { 0 };
                            m_OutputActiveZone.GetOctoConnectivityDeltas(pOctoConnectivityDeltas);
                            for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = SaliencyBlocks.begin(); ppSaliencyBlock != EndBlocks; ++ppSaliencyBlock)
                            {
                                Saliency::CSaliencyTree* pSaliencyTree = new Saliency::CSaliencyTree(m_pOutputImage->GetWritableBufferAt((*ppSaliencyBlock)->GetRoot()->GetDiscreteLocation()), *ppSaliencyBlock, pOctoConnectivityDeltas, Mode);
                                pSaliencyTree->Prunning(PathMinimalLengthThreshold);
                                pSaliencyTree->ExtractPaths(PathMinimalLengthThreshold);
                                pSaliencyTree->OutSpreadingPaths(SpreadRegularizationDelta);
                                pSaliencyTree->SmoothPaths(pSmoothingKernel, SmoothingRadius, SmoothingDiameter);
                                pSaliencyTree->CharacterizationPaths(CharacterizationGraphDistance, CharaterizationThresholdTransitional, CharaterizationThresholdElongated, CharacterizationMode, CharaterizationPower, EnableSmoothPaths);
                                pSaliencyTree->SegmentationPaths();
                                m_Forest.push_back(pSaliencyTree);
                            }
                        }
                        return true;
                    }
                    return false;
                }
            }
        }
    }
}
