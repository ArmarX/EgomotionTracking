/*
 * SaliencyPixel.h
 *
 *  Created on: 25.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../VisualSpace/Common/Image.h"
#include "../../../../Foundation/Mathematics/2D/Vector2D.h"
#include "../../../../Foundation/Mathematics/2D/StructuralTensor2D.h"
#include "../../../../Foundation/Mathematics/1D/BicubicLookUpTable.h"

#include "../../Monocular/Saliency/SaliencyBlock.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            class CSaliencyBlock;
            class CCharacterizedSaliencySegment;
            class CSaliencyPixel
            {
            public:

                enum CharacterizationType
                {
                    eUndefined = 0, eCompact = 1, eTransitional = 2, eElongated = 3, eSingular = 4
                };

                CSaliencyPixel();

                virtual ~CSaliencyPixel();

                inline  bool IsEnabled() const
                {
                    return m_IsEnabled;
                }

                inline void SetEnabled(const bool Enabled)
                {
                    m_IsEnabled = Enabled;
                }

                inline  bool IsSelected() const
                {
                    return m_IsSelected;
                }

                inline void SetSelected(const bool Selected)
                {
                    m_IsSelected = Selected;
                }

                inline  uint GetConnectivityGrade() const
                {
                    return m_ConnectedPixels.size();
                }

                inline const CSaliencyBlock* GetBlock() const
                {
                    return m_pSaliencyBlock;
                }

                inline  bool IsBlockConnected(const CSaliencyBlock* pSaliencyBlock, const CSaliencyPixel* pSaliencyPixel) const
                {
                    return (m_pSaliencyBlock == pSaliencyBlock) && (m_ContinousLocation.GetSquareDistance(pSaliencyPixel->m_ContinousLocation) <= _REAL_TWO_);
                }

                inline  bool IsAttachable(const real MinimalMagnitude) const
                {
                    return m_IsSelected && (!m_pSaliencyBlock) && (m_SaliencyMagnitude > MinimalMagnitude);
                }

                inline  bool IsInRangeAttachable(const real MinimalMagnitude, const CSaliencyPixel* pSaliencyPixel) const
                {
                    return m_IsSelected && (!m_pSaliencyBlock) && (m_SaliencyMagnitude > MinimalMagnitude) && (m_ContinousLocation.GetSquareDistance(pSaliencyPixel->m_ContinousLocation) <= _REAL_TWO_);
                }

                bool IsRimPixel(const real MinimalMagnitude, const coordinate* pTetraConnectivityDeltas) const
                {
                    if (m_SaliencyMagnitude > MinimalMagnitude)
                        for (uint i = 0; i < 4; ++i)
                            if ((m_SaliencyMagnitude > (this + pTetraConnectivityDeltas[i])->m_SaliencyMagnitude) && (m_SaliencyMagnitude > (this - pTetraConnectivityDeltas[i])->m_SaliencyMagnitude))
                            {
                                return true;
                            }
                    return false;
                }

                inline  bool IsLinkable(const CSaliencyPixel* pSaliencyPixel) const
                {
                    return m_IsSelected && (m_ContinousLocation.GetSquareDistance(pSaliencyPixel->m_ContinousLocation) <= _REAL_TWO_);
                }

                inline void SetBlock(const CSaliencyBlock* pSaliencyBlock)
                {
                    m_pSaliencyBlock = pSaliencyBlock;
                }

                inline void ClearBlock()
                {
                    m_pSaliencyBlock = NULL;
                }

                inline  real GetConnectivityDiameter() const
                {
                    return m_ConnectivityDiameter;
                }

                inline void SetConnectivityDiameter(const real Diameter)
                {
                    m_ConnectivityDiameter = Diameter;
                }

                inline  real GetMagnitude() const
                {
                    return m_SaliencyMagnitude;
                }

                inline void SetMagnitude(const real SaliencyMagnitude)
                {
                    m_SaliencyMagnitude = SaliencyMagnitude;
                }

                inline void ScaleMagnitude(const real Scale)
                {
                    m_SaliencyMagnitude *= Scale;
                }

                inline  real GetPercentile() const
                {
                    return m_Percentile;
                }

                inline void SetPercentile(const real Percentile)
                {
                    m_Percentile = Percentile;
                }

                inline  real GetOptimizedMagnitude() const
                {
                    return m_OptimizedSaliencyMagnitude;
                }

                inline void SetOptimizedMagnitude(const real OptimizedSaliencyMagnitude)
                {
                    m_OptimizedSaliencyMagnitude = OptimizedSaliencyMagnitude;
                }

                inline void AddSaliencyOrientationOffset(const Mathematics::_2D::CVector2D& SaliencyOffset)
                {
                    m_SaliencyOrientation += SaliencyOffset;
                }

                inline void AddSaliencyOrientationOffset(const real Dx, const real Dy)
                {
                    m_SaliencyOrientation.AddOffset(Dx, Dy);
                }

                inline void SetOrientation(const real Dx, const real Dy)
                {
                    m_SaliencyOrientation.SetValue(Dx, Dy);
                }

                inline const Mathematics::_2D::CVector2D& GetOrientation() const
                {
                    return m_SaliencyOrientation;
                }

                inline  real GetCoherence(const CSaliencyPixel* pPixel) const
                {
                    return RealAbs(m_SaliencyOrientation.ScalarProduct(pPixel->m_SaliencyOrientation));
                }

                inline const Mathematics::_2D::CVector2D& GetContinousLocation() const
                {
                    return m_ContinousLocation;
                }

                inline  bool IsDiscretelyConnected(const CSaliencyPixel* pSaliencyPixel) const
                {
                    return (m_DiscreteLocation.GetManhattanDistance(pSaliencyPixel->m_DiscreteLocation) <= 2);
                }

                inline  Mathematics::_2D::CVector2D GetIntialContinousLocation() const
                {
                    return Mathematics::_2D::CVector2D(m_DiscreteLocation);
                }

                inline void SetContinousLocation(const Mathematics::_2D::CVector2D& ContinousLocation)
                {
                    m_ContinousLocation = ContinousLocation;
                }

                inline void SetOptimization(const Mathematics::_2D::CVector2D& ContinousLocation, const real OptimizedSaliencyMagnitude)
                {
                    m_ContinousLocation = ContinousLocation;
                    m_OptimizedSaliencyMagnitude = OptimizedSaliencyMagnitude;
                }

                inline const CPixelLocation& GetDiscreteLocation() const
                {
                    return m_DiscreteLocation;
                }

                inline void Initialize(const bool Enabled, const CPixelLocation& DiscreteLocation)
                {
                    m_IsEnabled = Enabled;
                    m_DiscreteLocation = DiscreteLocation;
                    m_ContinousLocation = m_DiscreteLocation;
                }

                inline void SetDiscreteLocation(const CPixelLocation& DiscreteLocation)
                {
                    m_DiscreteLocation = DiscreteLocation;
                }

                inline void Normalization()
                {
                    m_SaliencyMagnitude = m_SaliencyOrientation.Normalize();
                }

                inline void WeightedNormalization(const real Weight)
                {
                    m_SaliencyMagnitude = m_SaliencyOrientation.Normalize() * Weight;
                }

                inline  Mathematics::_2D::CVector2D GetForwardsContinousLocation() const
                {
                    return m_ContinousLocation + m_SaliencyOrientation * _REAL_SQRT2_;
                }

                inline  Mathematics::_2D::CVector2D GetBackwardsContinousLocation() const
                {
                    return m_ContinousLocation - m_SaliencyOrientation * _REAL_SQRT2_;
                }

                inline void AddConnectedPixel(CSaliencyPixel* pSaliencyPixel)
                {
                    m_ConnectedPixels.push_back(pSaliencyPixel);
                }

                inline const list<CSaliencyPixel*>& GetConnectedPixels() const
                {
                    return m_ConnectedPixels;
                }

                inline  bool IsSingleConnected() const
                {
                    return (m_ConnectedPixels.size() == 1);
                }

                inline  bool IsDualConnected() const
                {
                    return (m_ConnectedPixels.size() == 2);
                }

                inline void StartBrodcast()
                {
                    m_pBroadcastingSaliencyPixel = this;
                    m_BroadCastDistance = _REAL_ZERO_;
                }

                inline void SetBroadCastDistance(const real BroadCastDistance)
                {
                    m_BroadCastDistance = BroadCastDistance;
                }

                inline  real GetBroadCastDistance() const
                {
                    return m_BroadCastDistance;
                }

                inline const CSaliencyPixel* GetBroadcastingSaliencyPixel() const
                {
                    return m_pBroadcastingSaliencyPixel;
                }

                void Reset();

                bool Broadcasting(const CSaliencyPixel* pBroadcastingSaliencyPixel, const CSaliencyPixel* pPreSaliencyPixel, const bool UseContinousLocation);
                bool OptimizeSaliency(const real Precision, const TImage<CSaliencyPixel>* pSaliencyPixelImage, const real MinimalMagnitude, const real Displacement, const uint TotalSamples, real* pSamples, Mathematics::_2D::CVector2D* pLocations, const bool UseRefinementOptimization);
                bool SelectPeakSingleLevelCheck(const TImage<CSaliencyPixel>* pSaliencyPixelImage, const real MinimalMagnitude, const real Coherence);

                static  real GetMagnitudeByBicubicInterpolation(const Mathematics::_2D::CVector2D& SubpixelLocation, const TImage<CSaliencyPixel>* pSaliencyImage) _EVP_FAST_CALL_;

                inline  CharacterizationType GetCharacterizationType() const
                {
                    return m_Type;
                }

                inline void SetCharacterizationType(const CharacterizationType Type)
                {
                    m_Type = Type;
                }

                void SetStructuralSingular()
                {
                    m_Type = eSingular;
                    m_MainAxis = Mathematics::_2D::CVector2D::s_Direction_Axis_X_2D;
                    m_SecondaryRadius = m_MainRadius = _REAL_ONE_;
                }

                bool StructuralCharacterize(const Mathematics::_2D::CVector2D& MainAxis, const real MainAxisLength, const real SecondaryAxisLength)
                {
                    m_MainAxis = MainAxis;
                    m_MainRadius = MainAxisLength;
                    m_SecondaryRadius = SecondaryAxisLength;
                    if (m_SecondaryRadius < _REAL_EPSILON_)
                    {
                        m_Type = eElongated;
                        return false;
                    }
                    return true;
                }

                real GetLinealEccentricity() const
                {
                    return m_MainRadius / m_SecondaryRadius;
                }

                void CharacterizeLineal(const real ThresholdTransitional, const real ThresholdElongated)
                {
                    const real Characterization = m_MainRadius / m_SecondaryRadius;
                    if (Characterization > ThresholdElongated)
                    {
                        m_Type = eElongated;
                    }
                    else if (Characterization > ThresholdTransitional)
                    {
                        m_Type = eTransitional;
                    }
                    else
                    {
                        m_Type = eCompact;
                    }
                }

                real GetPowerEccentricity(const real Power) const
                {
                    return RealPow(m_MainRadius, Power) / RealPow(m_SecondaryRadius, Power);
                }

                void CharacterizePower(const real ThresholdTransitional, const real ThresholdElongated, const real Power)
                {
                    const real Characterization = RealPow(m_MainRadius, Power) / RealPow(m_SecondaryRadius, Power);
                    if (Characterization > ThresholdElongated)
                    {
                        m_Type = eElongated;
                    }
                    else if (Characterization > ThresholdTransitional)
                    {
                        m_Type = eTransitional;
                    }
                    else
                    {
                        m_Type = eCompact;
                    }
                }

                real GetApetureEccentricity() const
                {
                    return _REAL_ONE_ - (RealAtan2(m_SecondaryRadius, m_MainRadius) * real(1.2732395447351626861521414298709)); //4/PI
                }

                void CharacterizeApeture(const real ThresholdTransitional, const real ThresholdElongated)
                {
                    const real Characterization = _REAL_ONE_ - (RealAtan2(m_SecondaryRadius, m_MainRadius) * real(1.2732395447351626861521414298709)); //4/PI
                    if (Characterization > ThresholdElongated)
                    {
                        m_Type = eElongated;
                    }
                    else if (Characterization > ThresholdTransitional)
                    {
                        m_Type = eTransitional;
                    }
                    else
                    {
                        m_Type = eCompact;
                    }
                }

                real GetPowerApetureEccentricity(const real Power) const
                {
                    return RealPow(_REAL_ONE_ - (RealAtan2(m_SecondaryRadius, m_MainRadius) * real(1.2732395447351626861521414298709)), Power); //4/PI
                }

                void CharacterizePowerApeture(const real ThresholdTransitional, const real ThresholdElongated, const real Power)
                {
                    const real Characterization = RealPow(_REAL_ONE_ - (RealAtan2(m_SecondaryRadius, m_MainRadius) * real(1.2732395447351626861521414298709)), Power); //4/PI
                    if (Characterization > ThresholdElongated)
                    {
                        m_Type = eElongated;
                    }
                    else if (Characterization > ThresholdTransitional)
                    {
                        m_Type = eTransitional;
                    }
                    else
                    {
                        m_Type = eCompact;
                    }
                }

                inline CCharacterizedSaliencySegment* GetCharacterizedSaliencySegment()
                {
                    return m_pCharacterizedSaliencySegment;
                }

                inline void SetCharacterizedSaliencySegment(CCharacterizedSaliencySegment* pCharacterizedSaliencySegment)
                {
                    m_pCharacterizedSaliencySegment = pCharacterizedSaliencySegment;
                }

                inline  bool IsSegmentable(const CharacterizationType Type) const
                {
                    return (!m_pCharacterizedSaliencySegment) && (m_Type == Type);
                }

                inline void SetExpansionList(list<CSaliencyPixel*>* pExpansionList)
                {
                    m_pExpansionList = pExpansionList;
                }

                inline list<CSaliencyPixel*>* GetExpansionList()
                {
                    return m_pExpansionList;
                }

            protected:

                list<CSaliencyPixel*>* m_pExpansionList;

                bool m_IsEnabled;
                bool m_IsSelected;

                real m_Percentile;
                real m_ConnectivityDiameter;

                real m_SaliencyMagnitude;
                real m_OptimizedSaliencyMagnitude;
                real m_BroadCastDistance;
                CPixelLocation m_DiscreteLocation;
                Mathematics::_2D::CVector2D m_SaliencyOrientation;
                Mathematics::_2D::CVector2D m_ContinousLocation;
                list<CSaliencyPixel*> m_ConnectedPixels;
                const CSaliencyPixel* m_pBroadcastingSaliencyPixel;
                const CSaliencyBlock* m_pSaliencyBlock;

                CharacterizationType m_Type;
                real m_MainRadius;
                real m_SecondaryRadius;
                Mathematics::_2D::CVector2D m_MainAxis;
                CCharacterizedSaliencySegment* m_pCharacterizedSaliencySegment;

#ifdef _SALIENCY_PIXEL_USE_LOOKUP_TABLE_

                DECLARE_BICUBIC_LOOKUP_TABLE
#endif

            };
        }
    }
}

