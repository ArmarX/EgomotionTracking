/*
 * SaliencyTree.cpp
 *
 *  Created on: 13.04.2011
 *      Author: gonzalez
 */

#include "SaliencyNode.h"
#include "SaliencySegment.h"
#include "SaliencyPoint.h"
#include "SaliencyPath.h"
#include "SaliencyTree.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            IDENTIFIABLE_INITIALIZATION(CSaliencyTree)

            CSaliencyTree::CSaliencyTree(CSaliencyNode* pRootSaliencyNode, const CSaliencyBlock* pSaliencyBlock, const int* pDeltas, const ExtractionMode Mode) :
                IDENTIFIABLE_CONTRUCTOR(CSaliencyTree), m_Active(true), m_Linked(false), m_pSaliencyBlock(pSaliencyBlock)
            {
                memset(m_SegmentsCharacterizationTypeHistogram, 0, sizeof(uint) * 3);
                memset(m_SegmentsCharacterizationTypeAccumulativeLength, 0, sizeof(real) * 3);
                memset(m_DescendancyDistributionHistogram, 0, sizeof(uint) * 8);
                switch (Mode)
                {
                    case eMaximalSaliency:
                        ExpansionMaximalSaliencyBased(pRootSaliencyNode, pDeltas);
                        break;
                    case eMinimalDistance:
                        ExpansionMinimalDistanceBased(pRootSaliencyNode, pDeltas);
                        break;
                }
            }

            CSaliencyTree::~CSaliencyTree()
            {
                list<CSaliencyNode*>::iterator EndSaliencyNodes = m_SaliencyNodes.end();
                for (list<CSaliencyNode*>::iterator ppSaliencyNode = m_SaliencyNodes.begin(); ppSaliencyNode != EndSaliencyNodes; ++ppSaliencyNode)
                {
                    (*ppSaliencyNode)->Reset();
                }
                if (m_SaliencyPaths.size())
                {
                    list<CSaliencyPath*>::iterator EndSaliencyPaths = m_SaliencyPaths.end();
                    for (list<CSaliencyPath*>::iterator ppSaliencyPath = m_SaliencyPaths.begin(); ppSaliencyPath != EndSaliencyPaths; ++ppSaliencyPath)
                        RELEASE_OBJECT_DIRECT(*ppSaliencyPath)
                    }
            }

            void CSaliencyTree::Prunning(const uint PathMinimalLengthThreshold)
            {
                BasicPrunning();
                ExtendedPrunning(PathMinimalLengthThreshold);
            }

            void CSaliencyTree::ExtractPaths(const uint PathMinimalLengthThreshold)
            {
                list<CSaliencyNode*>::const_iterator EndSaliencyNodes = m_SaliencyNodes.end();
                for (list<CSaliencyNode*>::const_iterator ppSaliencyNode = m_SaliencyNodes.begin(); ppSaliencyNode != EndSaliencyNodes; ++ppSaliencyNode)
                    if ((*ppSaliencyNode)->IsActivePathBottomNode())
                    {
                        CSaliencyPath* pSaliencyPath = new CSaliencyPath(*ppSaliencyNode, this);
                        if (pSaliencyPath->GetLength() > PathMinimalLengthThreshold)
                        {
                            if (pSaliencyPath->GetBottomNode()->IsDynamicPathEndNode())
                            {
                                m_PathEndNodes.push_back(pSaliencyPath->GetBottomNode());
                            }
                            if (pSaliencyPath->GetTopNode()->IsDynamicPathEndNode())
                            {
                                m_PathEndNodes.push_back(pSaliencyPath->GetTopNode());
                            }
                            m_SaliencyPaths.push_back(pSaliencyPath);
                        }
                        else
                            RELEASE_OBJECT_DIRECT(pSaliencyPath)
                        }
            }

            void CSaliencyTree::OutSpreadingPaths(const real RegularLengthDelta)
            {
                list<CSaliencyPath*>::iterator EndSaliencyPaths = m_SaliencyPaths.end();
                for (list<CSaliencyPath*>::iterator ppSaliencyPath = m_SaliencyPaths.begin(); ppSaliencyPath != EndSaliencyPaths; ++ppSaliencyPath)
                {
                    (*ppSaliencyPath)->OutSpreading(RegularLengthDelta);
                }
            }

            void CSaliencyTree::SmoothPaths(const real* pSmoothingKernel, const uint SmoothingRadius, const uint SmoothingDiameter)
            {
                list<CSaliencyPath*>::iterator EndSaliencyPaths = m_SaliencyPaths.end();
                for (list<CSaliencyPath*>::iterator ppSaliencyPath = m_SaliencyPaths.begin(); ppSaliencyPath != EndSaliencyPaths; ++ppSaliencyPath)
                {
                    (*ppSaliencyPath)->Smoothing(pSmoothingKernel, SmoothingRadius, SmoothingDiameter);
                }
            }

            void CSaliencyTree::SubPixelOptimizePaths(TImage<CSaliencyPixel>* pSaliencyImage, real Scope, int Iterations, bool Refinement)
            {
                list<CSaliencyPath*>::iterator EndSaliencyPaths = m_SaliencyPaths.end();
                for (list<CSaliencyPath*>::iterator ppSaliencyPath = m_SaliencyPaths.begin(); ppSaliencyPath != EndSaliencyPaths; ++ppSaliencyPath)
                {
                    (*ppSaliencyPath)->SubPixelOptimization(pSaliencyImage, Scope, Iterations, Refinement);
                }
            }

            /*void CSaliencyTree::CharacterizationTree(const uint MaximalPathDistance, const real ThresholdTransitional, const real ThresholdElongated, const CSaliencyPoint::CharacterizationMode Mode, const real Power, const bool UseSmoothLocations)
             {
             list<CSaliencyNode*> ActiveSaliencyNodes;
             list<CSaliencyNode*>::const_iterator EndSaliencyNodes = m_SaliencyNodes.end();
             for (list<CSaliencyNode*>::const_iterator ppSaliencyNode = m_SaliencyNodes.begin(); ppSaliencyNode != EndSaliencyNodes; ++ppSaliencyNode)
             if ((*ppSaliencyNode)->IsActive())
             {
             (*ppSaliencyNode)->ClearSourceBroadcastingPixel();
             ActiveSaliencyNodes.push_back(*ppSaliencyNode);
             }

             list<CSaliencyNode*> ExpansionSaliencyNodes;
             real Cxx = _REAL_ZERO_, Cxy = _REAL_ZERO_, Cyy = _REAL_ZERO_;
             while (ActiveSaliencyNodes.size())
             {
             CSaliencyNode* pCharacterizationSaliencyNode = ActiveSaliencyNodes.front();
             ActiveSaliencyNodes.pop_front();

             const Mathematics::_2D::CVector2D& Center = pCharacterizationSaliencyNode->GetSourceContinousLocation();
             real Axx = _REAL_ZERO_, Axy = _REAL_ZERO_, Ayy = _REAL_ZERO_;

             ExpansionSaliencyNodes.push_back(pCharacterizationSaliencyNode);
             while (ExpansionSaliencyNodes.size())
             {
             CSaliencyNode* pExpansionSaliencyNode = ExpansionSaliencyNodes.front();
             ExpansionSaliencyNodes.pop_front();
             const list<CSaliencyPixel*>& SourceConnectedPixels = pExpansionSaliencyNode->GetSourceConnectedPixels();
             list<CSaliencyPixel*>::const_iterator EndSourceConnectedPixels = SourceConnectedPixels.end();
             for (list<CSaliencyPixel*>::const_iterator ppSourceConnectedPixel = SourceConnectedPixels.begin(); ppSourceConnectedPixel != EndSourceConnectedPixels; ++ppSourceConnectedPixel)
             {
             CSaliencyPixel* pSourceConnectedPixel = *ppSourceConnectedPixel;
             if (pSourceConnectedPixel->IsActive() && pSourceConnectedPixel->Broadcasting(pCharacterizationSaliencyNode, pExpansionSaliencyNode, true))
             {
             Center.GetCovariance(pSourceConnectedPixel->GetContinousLocation(), Cxx, Cxy, Cyy);
             Axx += Cxx;
             Axy += Cxy;
             Ayy += Cyy;
             pSourceConnectedPixel->SetBroadCastDistance(_REAL_ZERO_);
             ExpansionSaliencyNodes.push_back(pSourceConnectedPixel);
             }
             }
             }
             }
             }*/

            void CSaliencyTree::CharacterizationPaths(const uint MaximalPathDistance, const real ThresholdTransitional, const real ThresholdElongated, const CSaliencyPoint::CharacterizationMode Mode, const real Power, const bool UseSmoothLocations)
            {
                list<CSaliencyPath*>::iterator EndSaliencyPaths = m_SaliencyPaths.end();
                for (list<CSaliencyPath*>::iterator ppSaliencyPath = m_SaliencyPaths.begin(); ppSaliencyPath != EndSaliencyPaths; ++ppSaliencyPath)
                {
                    (*ppSaliencyPath)->Characterization(MaximalPathDistance, ThresholdTransitional, ThresholdElongated, Mode, Power, UseSmoothLocations);
                }
            }

            void CSaliencyTree::SegmentationPaths()
            {
                list<CSaliencyPath*>::iterator EndSaliencyPaths = m_SaliencyPaths.end();
                for (list<CSaliencyPath*>::iterator ppSaliencyPath = m_SaliencyPaths.begin(); ppSaliencyPath != EndSaliencyPaths; ++ppSaliencyPath)
                {
                    const list<CSaliencySegment*>& Segments = (*ppSaliencyPath)->Segmentation();
                    if (Segments.size())
                    {
                        list<CSaliencySegment*>::const_iterator EndSegments = Segments.end();
                        for (list<CSaliencySegment*>::const_iterator ppSaliencySegment = Segments.begin(); ppSaliencySegment != EndSegments; ++ppSaliencySegment)
                        {
                            const CSaliencySegment* pSaliencySegment = *ppSaliencySegment;
                            CSaliencyPoint::CharacterizationType Type = pSaliencySegment->GetCharacterizationType();
                            ++m_SegmentsCharacterizationTypeHistogram[Type];
                            m_SegmentsCharacterizationTypeAccumulativeLength[Type] += pSaliencySegment->GetLength();
                            m_SaliencySegment.push_back(pSaliencySegment);
                        }
                    }
                }
                if (m_SaliencySegment.size())
                {
                    m_SaliencySegment.sort(SortSaliencySegmentByLenghtDescending);
                }
            }

            void CSaliencyTree::ExtractLineSegments(real LineSegmentUncertaintyThreshold, real LineSegmentMinimalMainAxisLength, real LineSegmentMaximalSecondaryAxisLength, int LineSegmentMinimalPointNodes)
            {
                list<CSaliencyPath*>::iterator EndSaliencyPaths = m_SaliencyPaths.end();
                for (list<CSaliencyPath*>::iterator ppSaliencyPath = m_SaliencyPaths.begin(); ppSaliencyPath != EndSaliencyPaths; ++ppSaliencyPath)
                {
                    (*ppSaliencyPath)->ExtractLineSegments(LineSegmentUncertaintyThreshold, LineSegmentMinimalMainAxisLength, LineSegmentMaximalSecondaryAxisLength, LineSegmentMinimalPointNodes);
                }
            }

            bool CSaliencyTree::IsGeometricSalient(const real MinimalSegmentLength)
            {
                list<CSaliencyPath*>::iterator EndSaliencyPaths = m_SaliencyPaths.end();
                for (list<CSaliencyPath*>::iterator ppSaliencyPath = m_SaliencyPaths.begin(); ppSaliencyPath != EndSaliencyPaths; ++ppSaliencyPath)
                    if ((*ppSaliencyPath)->IsGeometricSalient(MinimalSegmentLength))
                    {
                        return true;
                    }
                return false;
            }

            void CSaliencyTree::GetMinimalDistanceToPathEndNodes(CSaliencyTree* pSaliencyTree, real& MinimalDistance)
            {
                list<const CSaliencyNode*>::const_iterator EndSaliencyNodesA = m_PathEndNodes.end();
                list<const CSaliencyNode*>::const_iterator BeginSaliencyNodesB = pSaliencyTree->m_PathEndNodes.begin();
                list<const CSaliencyNode*>::const_iterator EndSaliencyNodesB = pSaliencyTree->m_PathEndNodes.end();
                for (list<const CSaliencyNode*>::const_iterator ppSaliencyNodeA = m_PathEndNodes.begin(); ppSaliencyNodeA != EndSaliencyNodesA; ++ppSaliencyNodeA)
                {
                    const EVP::Mathematics::_2D::CVector2D& A = (*ppSaliencyNodeA)->GetSourceContinousLocation();
                    for (list<const CSaliencyNode*>::const_iterator ppSaliencyNodeB = BeginSaliencyNodesB; ppSaliencyNodeB != EndSaliencyNodesB; ++ppSaliencyNodeB)
                    {
                        const real Distance = A.GetDistance((*ppSaliencyNodeB)->GetSourceContinousLocation());
                        if (Distance < MinimalDistance)
                        {
                            MinimalDistance = Distance;
                        }
                    }
                }
            }

            bool CSaliencyTree::SortSaliencySegmentByLenghtDescending(const CSaliencySegment* plhs, const CSaliencySegment* prhs)
            {
                return plhs->GetLinearLength() > prhs->GetLinearLength();
            }

            void CSaliencyTree::BasicPrunning()
            {
                list<CSaliencyNode*> LeafSaliencyNodes;
                list<CSaliencyNode*>::const_iterator EndSaliencyNodes = m_SaliencyNodes.end();
                for (list<CSaliencyNode*>::const_iterator ppSaliencyNode = m_SaliencyNodes.begin(); ppSaliencyNode != EndSaliencyNodes; ++ppSaliencyNode)
                {
                    CSaliencyNode* pSaliencyNode = *ppSaliencyNode;
                    pSaliencyNode->ClearHeuristic();
                    if (pSaliencyNode->IsStaticLeaf())
                    {
                        LeafSaliencyNodes.push_back(pSaliencyNode);
                    }
                }
                if (LeafSaliencyNodes.size())
                {
                    list<CSaliencyNode*>::const_iterator EndLeafSaliencyNodes = LeafSaliencyNodes.end();
                    for (list<CSaliencyNode*>::const_iterator ppSaliencyNode = LeafSaliencyNodes.begin(); ppSaliencyNode != EndLeafSaliencyNodes; ++ppSaliencyNode)
                    {
                        (*ppSaliencyNode)->BackPropagation();
                    }
                    for (list<CSaliencyNode*>::const_iterator ppSaliencyNode = LeafSaliencyNodes.begin(); ppSaliencyNode != EndLeafSaliencyNodes; ++ppSaliencyNode)
                    {
                        (*ppSaliencyNode)->DeactivateRedundatLeaf();
                    }
                }
            }

            void CSaliencyTree::FilterActiveNodes(const list<CSaliencyNode*>& SaliencyNodes, list<CSaliencyNode*>& ActiveSaliencyNodes)
            {
                ActiveSaliencyNodes.clear();
                list<CSaliencyNode*>::const_iterator EndSaliencyNodes = SaliencyNodes.end();
                for (list<CSaliencyNode*>::const_iterator ppSaliencyNode = SaliencyNodes.begin(); ppSaliencyNode != EndSaliencyNodes; ++ppSaliencyNode)
                    if ((*ppSaliencyNode)->IsActive())
                    {
                        ActiveSaliencyNodes.push_back(*ppSaliencyNode);
                    }
            }

            void CSaliencyTree::FilterDynamicLeafNodes(const list<CSaliencyNode*>& SaliencyNodes, list<CSaliencyNode*>& DynamicLeafSaliencyNodes)
            {
                DynamicLeafSaliencyNodes.clear();
                list<CSaliencyNode*>::const_iterator EndSaliencyNodes = SaliencyNodes.end();
                for (list<CSaliencyNode*>::const_iterator ppSaliencyNode = SaliencyNodes.begin(); ppSaliencyNode != EndSaliencyNodes; ++ppSaliencyNode)
                    if ((*ppSaliencyNode)->IsDynamicLeaf())
                    {
                        DynamicLeafSaliencyNodes.push_back(*ppSaliencyNode);
                    }
            }

            void CSaliencyTree::ExtendedPrunning(const uint PathMinimalLengthThreshold)
            {
                list<CSaliencyNode*> SourceSaliencyNodes[2];
                list<CSaliencyNode*> LeafSaliencyNodes;
                list<CSaliencyNode*> RedundantPathRootSaliencyNodes;
                uint Toggler = 0;
                FilterActiveNodes(m_SaliencyNodes, SourceSaliencyNodes[Toggler]);
                do
                {
                    FilterDynamicLeafNodes(SourceSaliencyNodes[Toggler], LeafSaliencyNodes);
                    list<CSaliencyNode*>::iterator EndLeafSaliencyNodes = LeafSaliencyNodes.end();
                    for (list<CSaliencyNode*>::iterator ppSaliencyNode = LeafSaliencyNodes.begin(); ppSaliencyNode != EndLeafSaliencyNodes; ++ppSaliencyNode)
                    {
                        CSaliencyNode* pAuxiliarSaliencyNode = *ppSaliencyNode;
                        CSaliencyNode* pSaliencyNode = pAuxiliarSaliencyNode;
                        uint PathLength = 0;
                        while (pSaliencyNode->GetTreeParent())
                        {
                            ++PathLength;
                            if (pSaliencyNode->GetTotalActiveSibilings())
                            {
                                pAuxiliarSaliencyNode->SetPathParent(pSaliencyNode->GetTreeParent());
                                break;
                            }
                            pSaliencyNode = pSaliencyNode->GetTreeParent();
                        }
                        if (!pSaliencyNode->GetTreeParent())
                        {
                            pAuxiliarSaliencyNode->SetPathParent(pSaliencyNode);
                        }
                        pAuxiliarSaliencyNode->SetHeuristic(PathLength);
                        if (PathLength < PathMinimalLengthThreshold)
                        {
                            RedundantPathRootSaliencyNodes.push_back(pAuxiliarSaliencyNode);
                        }
                    }
                    const uint TotalRedundantPath = RedundantPathRootSaliencyNodes.size();
                    if (!TotalRedundantPath)
                    {
                        break;
                    }
                    if (TotalRedundantPath > 1)
                    {
                        RedundantPathRootSaliencyNodes.sort(SortRedundantPathRoots);
                    }
                    uint TotalDisabledPath = 0;
                    list<CSaliencyNode*>::iterator EndRedundantPathRootSaliencyNodes = RedundantPathRootSaliencyNodes.end();
                    for (list<CSaliencyNode*>::iterator ppSaliencyNode = RedundantPathRootSaliencyNodes.begin(); ppSaliencyNode != EndRedundantPathRootSaliencyNodes; ++ppSaliencyNode)
                    {
                        CSaliencyNode* pSaliencyNode = *ppSaliencyNode;
                        CSaliencyNode* pAuxiliarSaliencyNode = pSaliencyNode->GetPathParent();
                        if (pAuxiliarSaliencyNode->GetHeuristic() > 0)
                        {
                            ++TotalDisabledPath;
                            pAuxiliarSaliencyNode->SetHeuristic(-_REAL_ONE_);
                            while (pSaliencyNode != pAuxiliarSaliencyNode)
                            {
                                pSaliencyNode->SetActive(false);
                                pSaliencyNode = pSaliencyNode->GetTreeParent();
                            }
                        }
                    }
                    if (!TotalDisabledPath)
                    {
                        break;
                    }
                    FilterActiveNodes(SourceSaliencyNodes[Toggler], SourceSaliencyNodes[(Toggler + 1) & 0X1]);
                    if (SourceSaliencyNodes[(Toggler + 1) & 0X1].size())
                    {
                        Toggler = (Toggler + 1) & 0X1;
                        RedundantPathRootSaliencyNodes.clear();
                    }
                    else
                    {
                        break;
                    }
                }
                while (true);
            }

            void CSaliencyTree::ExpansionMaximalSaliencyBased(CSaliencyNode* pRootSaliencyNode, const int* pDeltas)
            {
                pRootSaliencyNode->SetTreeParent(pRootSaliencyNode);
                pRootSaliencyNode->SetActive(true);
                m_SaliencyNodes.push_back(pRootSaliencyNode);
                m_BoundingBox.SetInitial(pRootSaliencyNode->GetSourceContinousLocation());
                list<CSaliencyNode*> GlobalHeuristicSortedList;
                GlobalHeuristicSortedList.push_back(pRootSaliencyNode);
                while (GlobalHeuristicSortedList.size())
                {
                    CSaliencyNode* pParentNode = GlobalHeuristicSortedList.back();
                    GlobalHeuristicSortedList.pop_back();
                    for (uint i = 0; i < 8; ++i)
                    {
                        CSaliencyNode* pChildNode = pParentNode + pDeltas[i];
                        if (pChildNode->IsLinkable(m_pSaliencyBlock))
                        {
                            pParentNode->AddChild(pChildNode);
                            pChildNode->SetActive(true);
                            pChildNode->SetTreeParent(pParentNode);
                            const real HeuristicValue = pChildNode->GetSourceOptimizedMagnitude();
                            pChildNode->SetHeuristic(HeuristicValue);
                            SortedInsertionByHeuristic(GlobalHeuristicSortedList, pChildNode, HeuristicValue);
                            m_SaliencyNodes.push_back(pChildNode);
                            m_BoundingBox.Extend(pChildNode->GetSourceContinousLocation());
                        }
                    }
                    ++m_DescendancyDistributionHistogram[pParentNode->GetTotalChildren()];
                }
                pRootSaliencyNode->SetTreeParent(nullptr);
            }

            void CSaliencyTree::ExpansionMinimalDistanceBased(CSaliencyNode* pRootSaliencyNode, const int* pDeltas)
            {
                list<CSaliencyNode*> GlobalHeuristicSortedList;
                list<CSaliencyNode*> LocalHeuristicSortedList;
                pRootSaliencyNode->SetTreeParent(pRootSaliencyNode);
                pRootSaliencyNode->SetActive(true);
                m_SaliencyNodes.push_back(pRootSaliencyNode);
                m_BoundingBox.Extend(pRootSaliencyNode->GetSourceContinousLocation());
                GlobalHeuristicSortedList.push_back(pRootSaliencyNode);
                while (GlobalHeuristicSortedList.size())
                {
                    CSaliencyNode* pParentNode = GlobalHeuristicSortedList.front();
                    GlobalHeuristicSortedList.pop_front();
                    const EVP::Mathematics::_2D::CVector2D& ParentContinousLocation = pParentNode->GetSourceContinousLocation();
                    for (uint i = 0; i < 8; ++i)
                    {
                        CSaliencyNode* pChildNode = pParentNode + pDeltas[i];
                        if (pChildNode->IsLinkable(m_pSaliencyBlock))
                        {
                            const real HeuristicValue = pChildNode->GetSourceContinousLocation().GetDistance(ParentContinousLocation);
                            if (pChildNode->VerifyOptimalLinkageDistanceBased(pParentNode, pDeltas, HeuristicValue))
                            {
                                pChildNode->SetTreeParent(pParentNode);
                                pChildNode->SetHeuristic(HeuristicValue);
                                SortedInsertionByHeuristic(LocalHeuristicSortedList, pChildNode, HeuristicValue);
                            }
                        }
                    }
                    const uint TotalChildrenCandidates = LocalHeuristicSortedList.size();
                    if (TotalChildrenCandidates)
                    {
                        if (TotalChildrenCandidates > 1)
                            switch (TotalChildrenCandidates)
                            {
                                case 2:
                                {
                                    CSaliencyNode* pChildNodeA = LocalHeuristicSortedList.front();
                                    CSaliencyNode* pChildNodeB = LocalHeuristicSortedList.back();
                                    if (pChildNodeA->IsSourceDiscretelyConnected(pChildNodeB) && (pChildNodeA->GetSourceContinousDistance(pChildNodeB) < pChildNodeB->GetHeuristic()))
                                    {
                                        pChildNodeB->SetTreeParent(nullptr);
                                    }
                                    break;
                                }
                                default:
                                {
                                    list<CSaliencyNode*>::iterator EndLocalHeuristicSortedList = LocalHeuristicSortedList.end();
                                    for (list<CSaliencyNode*>::iterator ppPixelNodeA = LocalHeuristicSortedList.begin(); ppPixelNodeA != EndLocalHeuristicSortedList; ++ppPixelNodeA)
                                    {
                                        CSaliencyNode* pChildNodeA = *ppPixelNodeA;
                                        if (pChildNodeA->GetTreeParent())
                                        {
                                            list<CSaliencyNode*>::iterator ppPixelNodeB = ppPixelNodeA;
                                            for (++ppPixelNodeB; ppPixelNodeB != EndLocalHeuristicSortedList; ++ppPixelNodeB)
                                            {
                                                CSaliencyNode* pChildNodeB = *ppPixelNodeB;
                                                if (pChildNodeB->GetTreeParent() && pChildNodeA->IsSourceDiscretelyConnected(pChildNodeB) && (pChildNodeA->GetSourceContinousDistance(pChildNodeB) < pChildNodeB->GetHeuristic()))
                                                {
                                                    pChildNodeB->SetTreeParent(nullptr);
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                        for (list<CSaliencyNode*>::iterator ppPixelNode = LocalHeuristicSortedList.begin(); ppPixelNode != LocalHeuristicSortedList.end(); ++ppPixelNode)
                        {
                            CSaliencyNode* pChildNode = *ppPixelNode;
                            if (pChildNode->GetTreeParent())
                            {
                                pChildNode->SetActive(true);
                                pParentNode->AddChild(pChildNode);
                                const real HeuristicValue = pChildNode->GetMinimalExpansionDistance(m_pSaliencyBlock, pDeltas);
                                pChildNode->SetHeuristic(HeuristicValue);
                                if (HeuristicValue != _REAL_MAX_)
                                {
                                    SortedInsertionByHeuristic(GlobalHeuristicSortedList, pChildNode, HeuristicValue);
                                }
                                m_SaliencyNodes.push_back(pChildNode);
                                m_BoundingBox.Extend(pChildNode->GetSourceContinousLocation());
                            }
                        }
                        LocalHeuristicSortedList.clear();
                    }
                    ++m_DescendancyDistributionHistogram[pParentNode->GetTotalChildren()];
                }
                pRootSaliencyNode->SetTreeParent(nullptr);
            }

            void CSaliencyTree::SortedInsertionByHeuristic(list<CSaliencyNode*>& HeuristicSortedList, CSaliencyNode* pSaliencyNode, const real Heuristic)
            {
                switch (HeuristicSortedList.size())
                {
                    case 0:
                        HeuristicSortedList.push_back(pSaliencyNode);
                        break;
                    case 1:
                        if (Heuristic > HeuristicSortedList.back()->GetHeuristic())
                        {
                            HeuristicSortedList.push_back(pSaliencyNode);
                        }
                        else
                        {
                            HeuristicSortedList.push_front(pSaliencyNode);
                        }
                        break;
                    case 2:
                        if (Heuristic > HeuristicSortedList.back()->GetHeuristic())
                        {
                            HeuristicSortedList.push_back(pSaliencyNode);
                        }
                        else if (Heuristic < HeuristicSortedList.front()->GetHeuristic())
                        {
                            HeuristicSortedList.push_front(pSaliencyNode);
                        }
                        else
                        {
                            HeuristicSortedList.insert(HeuristicSortedList.end(), pSaliencyNode);
                        }
                        break;
                    default:
                    {
                        if (Heuristic > HeuristicSortedList.back()->GetHeuristic())
                        {
                            HeuristicSortedList.push_back(pSaliencyNode);
                        }
                        else if (Heuristic < HeuristicSortedList.front()->GetHeuristic())
                        {
                            HeuristicSortedList.push_front(pSaliencyNode);
                        }
                        else
                        {
                            list<CSaliencyNode*>::iterator End = HeuristicSortedList.end();
                            for (list<CSaliencyNode*>::iterator ppPixelNode = ++HeuristicSortedList.begin(); ppPixelNode != End; ++ppPixelNode)
                                if (Heuristic < (*ppPixelNode)->GetHeuristic())
                                {
                                    HeuristicSortedList.insert(ppPixelNode, pSaliencyNode);
                                    break;
                                }
                        }
                    }
                    break;
                }
            }

            bool CSaliencyTree::SortRedundantPathRoots(CSaliencyNode* plhs, CSaliencyNode* prhs)
            {
                if (plhs->GetHeuristic() == prhs->GetHeuristic())
                {
                    return plhs->GetDistanceToParent() > prhs->GetDistanceToParent();
                }
                return plhs->GetHeuristic() < prhs->GetHeuristic();
            }
        }
    }
}
