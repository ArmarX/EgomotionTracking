/*
 * SaliencyPixel.cpp
 *
 *  Created on: 15.09.2011
 *      Author: gonzalez
 */

#include "SaliencyPixel.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
#ifdef _SALIENCY_PIXEL_USE_LOOKUP_TABLE_
            INITIALIZE_BICUBIC_LOOKUP_TABLE(CSaliencyPixel)
#endif

            CSaliencyPixel::CSaliencyPixel() :
                m_pExpansionList(nullptr), m_IsEnabled(false), m_IsSelected(false), m_Percentile(_REAL_ZERO_), m_ConnectivityDiameter(_REAL_ZERO_), m_SaliencyMagnitude(_REAL_ZERO_), m_OptimizedSaliencyMagnitude(_REAL_ZERO_), m_BroadCastDistance(_REAL_ZERO_), m_DiscreteLocation(), m_pBroadcastingSaliencyPixel(nullptr), m_pSaliencyBlock(nullptr), m_Type(eUndefined), m_pCharacterizedSaliencySegment(nullptr)
            {

#ifdef _SALIENCY_PIXEL_USE_LOOKUP_TABLE_

                LOAD_BICUBIC_LOOKUP_TABLE(CSaliencyPixel)

#endif
            }

            CSaliencyPixel::~CSaliencyPixel()
                = default;

            void CSaliencyPixel::Reset()
            {
                m_pExpansionList = nullptr;
                m_IsSelected = false;
                m_Type = eUndefined;
                m_pCharacterizedSaliencySegment = nullptr;
                m_pSaliencyBlock = nullptr;
                m_pBroadcastingSaliencyPixel = nullptr;
                m_BroadCastDistance = m_OptimizedSaliencyMagnitude = m_SaliencyMagnitude = m_ConnectivityDiameter = m_Percentile = _REAL_ZERO_;
                m_SaliencyOrientation.SetZero();
                m_ConnectedPixels.clear();
                m_ContinousLocation = m_DiscreteLocation;
            }

            bool CSaliencyPixel::Broadcasting(const CSaliencyPixel* pBroadcastingSaliencyPixel, const CSaliencyPixel* pPreSaliencyPixel, const bool UseContinousLocation)
            {
                if (m_pBroadcastingSaliencyPixel == pBroadcastingSaliencyPixel)
                {
                    return false;
                }
                else
                {
                    m_pBroadcastingSaliencyPixel = pBroadcastingSaliencyPixel;
                    if (UseContinousLocation)
                    {
                        m_BroadCastDistance = pPreSaliencyPixel->m_BroadCastDistance + m_ContinousLocation.GetDistance(pPreSaliencyPixel->m_ContinousLocation);
                    }
                    else
                    {
                        m_BroadCastDistance = pPreSaliencyPixel->m_BroadCastDistance + m_DiscreteLocation.GetEuclideanDistance(pPreSaliencyPixel->m_DiscreteLocation);
                    }
                    return m_ConnectedPixels.size();
                }
            }

            bool CSaliencyPixel::SelectPeakSingleLevelCheck(const TImage<CSaliencyPixel>* pSaliencyPixelImage, const real MinimalMagnitude, const real Coherence)
            {
                m_IsSelected = false;
                if (m_SaliencyMagnitude > MinimalMagnitude)
                {
                    const CSaliencyPixel* pForwardsAuxiliarSaliencyPixel = pSaliencyPixelImage->GetReadOnlyBufferAt(m_SaliencyOrientation.GetForwardsDiscreteLocation(m_DiscreteLocation));
                    if ((pForwardsAuxiliarSaliencyPixel->m_SaliencyMagnitude > MinimalMagnitude) && (m_SaliencyMagnitude > pForwardsAuxiliarSaliencyPixel->m_SaliencyMagnitude) && (RealAbs(m_SaliencyOrientation.ScalarProduct(pForwardsAuxiliarSaliencyPixel->m_SaliencyOrientation)) > Coherence))
                    {
                        const CSaliencyPixel* pBackwardsAuxiliarSaliencyPixel = pSaliencyPixelImage->GetReadOnlyBufferAt(m_SaliencyOrientation.GetBackwardsDiscreteLocation(m_DiscreteLocation));
                        m_IsSelected = (pBackwardsAuxiliarSaliencyPixel->m_SaliencyMagnitude > MinimalMagnitude) && (m_SaliencyMagnitude > pBackwardsAuxiliarSaliencyPixel->m_SaliencyMagnitude) && (RealAbs(m_SaliencyOrientation.ScalarProduct(pBackwardsAuxiliarSaliencyPixel->m_SaliencyOrientation)) > Coherence);
                    }
                }
                return m_IsSelected;
            }

            bool CSaliencyPixel::OptimizeSaliency(const real Precision, const TImage<CSaliencyPixel>* pSaliencyPixelImage, const real MinimalMagnitude, const real Displacement, const uint TotalSamples, real* pSamples, Mathematics::_2D::CVector2D* pLocations, const bool UseRefinementOptimization)
            {
                m_IsSelected = false;
                Mathematics::_2D::CVector2D Delta = m_SaliencyOrientation * Displacement;
                Mathematics::_2D::CVector2D Location = m_ContinousLocation - m_SaliencyOrientation;
                for (uint i = 0; i < TotalSamples; ++i, Location += Delta)
                {
                    pLocations[i] = Location;
                    pSamples[i] = GetMagnitudeByBicubicInterpolation(Location, pSaliencyPixelImage);
                }
                m_OptimizedSaliencyMagnitude = _REAL_ZERO_;
                for (uint i = 0, j = 1, k = 2; k < TotalSamples; ++i, ++j, ++k)
                    if ((pSamples[j] > MinimalMagnitude) && (pSamples[j] > pSamples[i]) && (pSamples[j] > pSamples[k]))
                    {
                        if (m_IsSelected)
                        {
                            if (pSamples[j] > m_OptimizedSaliencyMagnitude)
                            {
                                m_OptimizedSaliencyMagnitude = pSamples[j];
                                m_ContinousLocation = pLocations[j];
                            }
                        }
                        else
                        {
                            m_IsSelected = true;
                            m_OptimizedSaliencyMagnitude = pSamples[j];
                            m_ContinousLocation = pLocations[j];
                        }
                    }
                if (m_IsSelected && UseRefinementOptimization)
                {
                    CVector2D A = m_ContinousLocation - Delta;
                    CVector2D B = m_ContinousLocation + Delta;
                    real CurrentIncrementLenght = Displacement;
                    real SaliencyA = GetMagnitudeByBicubicInterpolation(A, pSaliencyPixelImage);
                    real SaliencyB = GetMagnitudeByBicubicInterpolation(B, pSaliencyPixelImage);
                    do
                    {
                        CurrentIncrementLenght *= _REAL_HALF_;
                        CVector2D MidLocation = CVector2D::CreateMidPoint(A, B);
                        const real CurrentMagnitude = GetMagnitudeByBicubicInterpolation(MidLocation, pSaliencyPixelImage);
                        if (CurrentMagnitude > SaliencyB)
                        {
                            B = MidLocation;
                            SaliencyB = CurrentMagnitude;
                        }
                        else
                        {
                            A = MidLocation;
                            SaliencyA = CurrentMagnitude;
                        }
                    }
                    while ((CurrentIncrementLenght > Precision) && (RealAbs(SaliencyA - SaliencyB) > _REAL_EPSILON_));
                    if (SaliencyA > SaliencyB)
                    {
                        m_ContinousLocation = A;
                        m_OptimizedSaliencyMagnitude = SaliencyA;
                    }
                    else
                    {
                        m_ContinousLocation = B;
                        m_OptimizedSaliencyMagnitude = SaliencyB;
                    }
                }
                return m_IsSelected;
            }

            real CSaliencyPixel::GetMagnitudeByBicubicInterpolation(const CVector2D& SubpixelLocation, const TImage<CSaliencyPixel>* pSaliencyImage)
            {
                const coordinate x = SubpixelLocation.GetTruncatedDiscreteX();
                const coordinate y = SubpixelLocation.GetTruncatedDiscreteY();
                const real Dx = real(x) - SubpixelLocation.GetX();
                const real Dy = SubpixelLocation.GetY() - real(y);

#ifdef _SALIENCY_PIXEL_USE_LOOKUP_TABLE_

                //const real Kmy[] = { s_BLT.m_pBicubic[RoundToInteger(s_BLT.m_SamplesPerUnit*(Dy + _REAL_ONE_))], s_BLT.m_pBicubic[RoundToInteger(s_BLT.m_SamplesPerUnit * Dy)], s_BLT.m_pBicubic[RoundToInteger(s_BLT.m_SamplesPerUnit*(Dy - _REAL_ONE_))], s_BLT.m_pBicubic[RoundToInteger(s_BLT.m_SamplesPerUnit*(Dy - _REAL_TWO_))] };
                //const real Kmx[] = { s_BLT.m_pBicubic[RoundToInteger(s_BLT.m_SamplesPerUnit*(Dx - _REAL_ONE_))], s_BLT.m_pBicubic[RoundToInteger(s_BLT.m_SamplesPerUnit * Dx)], s_BLT.m_pBicubic[RoundToInteger(s_BLT.m_SamplesPerUnit*(Dx + _REAL_ONE_))], s_BLT.m_pBicubic[RoundToInteger(s_BLT.m_SamplesPerUnit*(Dx + _REAL_TWO_))] };

                const real Kmy[] = { s_BLT.m_pBicubic[DownToInteger(s_BLT.m_SamplesPerUnit * (Dy + _REAL_ONE_))], s_BLT.m_pBicubic[DownToInteger(s_BLT.m_SamplesPerUnit * Dy)], s_BLT.m_pBicubic[DownToInteger(s_BLT.m_SamplesPerUnit * (Dy - _REAL_ONE_))], s_BLT.m_pBicubic[DownToInteger(s_BLT.m_SamplesPerUnit * (Dy - _REAL_TWO_))] };
                const real Kmx[] = { s_BLT.m_pBicubic[DownToInteger(s_BLT.m_SamplesPerUnit * (Dx - _REAL_ONE_))], s_BLT.m_pBicubic[DownToInteger(s_BLT.m_SamplesPerUnit * Dx)], s_BLT.m_pBicubic[DownToInteger(s_BLT.m_SamplesPerUnit * (Dx + _REAL_ONE_))], s_BLT.m_pBicubic[DownToInteger(s_BLT.m_SamplesPerUnit * (Dx + _REAL_TWO_))] };

#else

                const real Kmy[] =
                {   Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dy + _REAL_ONE_), Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dy), Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dy - _REAL_ONE_), Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dy - _REAL_TWO_)};
                const real Kmx[] =
                {   Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dx - _REAL_ONE_), Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dx), Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dx + _REAL_ONE_), Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dx + _REAL_TWO_)};

#endif

                const real* pKyEnd = Kmy + 4;
                const real* pKxEnd = Kmx + 4;
                //const CSaliencyPixel* pSaliencyPixel = this - InitialOffset;
                const CSaliencyPixel* pSaliencyPixel = pSaliencyImage->GetReadOnlyBufferAt(x - 1, y - 1);
                //PassOffsetFixed and Initial offset as uint and first parametes such as fast call
                const uint Offset = pSaliencyImage->GetWidth() - 4;
                real Accumulator = _REAL_ZERO_;
                for (const real* pKy = Kmy; pKy < pKyEnd; ++pKy, pSaliencyPixel += Offset)
                    for (const real* pKx = Kmx; pKx < pKxEnd; ++pKx, ++pSaliencyPixel)
                    {
                        Accumulator += pSaliencyPixel->m_SaliencyMagnitude * *pKy * *pKx;
                    }
                return Accumulator;
            }
        }
    }
}
