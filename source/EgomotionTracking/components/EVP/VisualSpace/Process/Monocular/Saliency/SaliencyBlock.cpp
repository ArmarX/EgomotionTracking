/*
 * SaliencyBlock.cpp
 *
 *  Created on: 25.03.2011
 *      Author: gonzalez
 */

#include "SaliencyPixel.h"
#include "SaliencyBlock.h"
#include "CharacterizedSaliencySegment.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            IDENTIFIABLE_INITIALIZATION(CSaliencyBlock)
            ////////////////////////////////////////////
#ifdef _SALIENCYBLOCK_USE_LOOKUP_TABLE_
            INITIALIZE_EXPONENTIAL_LOOKUP_TABLE(CSaliencyBlock)

#endif

            CSaliencyBlock::CSaliencyBlock(CSaliencyPixel* pSaliencyPixel) :
                IDENTIFIABLE_CONTRUCTOR(CSaliencyBlock), m_Selected(false), m_pRoot(nullptr), m_BoundingBox(pSaliencyPixel->GetContinousLocation()), m_AccumulatorSaliency(pSaliencyPixel->GetMagnitude()), m_MaximalSaliency(m_AccumulatorSaliency), m_MinimalSaliency(m_AccumulatorSaliency), m_MaximalPathDistance(_REAL_ZERO_)
            {
                memset(m_ConnectivityDistribution, 0, sizeof(uint) * 8);
                memset(m_CharacterizedSaliencySegmentDistribution, 0, sizeof(uint) * 5);
                m_SaliencyPixels.push_back(pSaliencyPixel);
                pSaliencyPixel->SetBlock(this);

#ifdef _SALIENCYBLOCK_USE_LOOKUP_TABLE_

                LOAD_EXPONENTIAL_LOOKUP_TABLE(CSaliencyBlock)

#endif
            }

            CSaliencyBlock::~CSaliencyBlock()
            {
                if (m_CharacterizedSaliencySegments.size())
                {
                    list<CCharacterizedSaliencySegment*>::iterator EndSaliencyPixels = m_CharacterizedSaliencySegments.end();
                    for (list<CCharacterizedSaliencySegment*>::iterator ppCharacterizedSaliencySegment = m_CharacterizedSaliencySegments.begin(); ppCharacterizedSaliencySegment != EndSaliencyPixels; ++ppCharacterizedSaliencySegment)
                        RELEASE_OBJECT_DIRECT(*ppCharacterizedSaliencySegment)
                    }
                list<CSaliencyPixel*>::iterator EndSaliencyPixels = m_SaliencyPixels.end();
                for (list<CSaliencyPixel*>::iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel)
                {
                    (*ppSaliencyPixel)->Reset();
                }
            }

            void CSaliencyBlock::SetSelected(const bool Selected)
            {
                m_Selected = Selected;
            }

            bool CSaliencyBlock::IsSelected() const
            {
                return m_Selected;
            }

            void CSaliencyBlock::AddPixel(CSaliencyPixel* pSaliencyPixel)
            {
                pSaliencyPixel->SetBlock(this);
                const real Saliency = pSaliencyPixel->GetMagnitude();
                m_AccumulatorSaliency += Saliency;
                if (Saliency < m_MinimalSaliency)
                {
                    m_MinimalSaliency = Saliency;
                }
                else if (Saliency > m_MaximalSaliency)
                {
                    m_MaximalSaliency = Saliency;
                }
                m_BoundingBox.Extend(pSaliencyPixel->GetContinousLocation());
                m_SaliencyPixels.push_back(pSaliencyPixel);
            }

            real CSaliencyBlock::GetActiveZoneWidth() const
            {
                return m_BoundingBox.GetWidth();
            }

            real CSaliencyBlock::GetActiveZoneHeigth() const
            {
                return m_BoundingBox.GetHeight();
            }

            uint CSaliencyBlock::GetSize() const
            {
                return m_SaliencyPixels.size();
            }

            real CSaliencyBlock::GetMeanSaliency() const
            {
                return m_AccumulatorSaliency / real(m_SaliencyPixels.size());
            }

            real CSaliencyBlock::GetRangeSaliency() const
            {
                return m_MaximalSaliency - m_MinimalSaliency;
            }

            real CSaliencyBlock::GetMaximalSaliency() const
            {
                return m_MaximalSaliency;
            }

            real CSaliencyBlock::GetMinimalSaliency() const
            {
                return m_MinimalSaliency;
            }

            bool CSaliencyBlock::GetApertureEccentricityRange(real& MaximalApertureEccentricity, real& MinimalApertureEccentricity) const
            {
                if (m_SaliencyPixels.size())
                {
                    MaximalApertureEccentricity = _REAL_ZERO_;
                    MinimalApertureEccentricity = _REAL_MAX_;
                    list<CSaliencyPixel*>::const_iterator EndSaliencyPixels = m_SaliencyPixels.end();
                    for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel)
                    {
                        const real ApetureEccentricity = (*ppSaliencyPixel)->GetApetureEccentricity();
                        if (ApetureEccentricity > MaximalApertureEccentricity)
                        {
                            MaximalApertureEccentricity = ApetureEccentricity;
                        }
                        if (ApetureEccentricity < MinimalApertureEccentricity)
                        {
                            MinimalApertureEccentricity = ApetureEccentricity;
                        }
                    }
                    return true;
                }
                return false;
            }

            CSaliencyPixel* CSaliencyBlock::GetRoot()
            {
                return m_pRoot;
            }

            const list<CSaliencyPixel*>& CSaliencyBlock::GetPixels() const
            {
                return m_SaliencyPixels;
            }

            real CSaliencyBlock::GetMaximalPathDistance() const
            {
                return m_MaximalPathDistance;
            }

            const CContinuousBoundingBox2D& CSaliencyBlock::GetBoundingBox() const
            {
                return m_BoundingBox;
            }

            void CSaliencyBlock::Characterization(const real MaximalPathDistance, const real ThresholdTransitional, const real ThresholdElongated, const CharacterizationMode Mode, const real Power, const real ExponentFactor)
            {
                list<Saliency::CSaliencyPixel*>::const_iterator EndPixels = m_SaliencyPixels.end();
                for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                {
                    (*ppSaliencyPixel)->StartBrodcast();
                }
                list<Saliency::CSaliencyPixel*> ExpansionList;
                Mathematics::_2D::CStructuralTensor2D StructuralTensor2D;

#ifdef _SALIENCYBLOCK_USE_LOOKUP_TABLE_

                const real Scale = ExponentFactor * s_ELT.m_SamplesPerUnit;

#endif
                for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                {
                    Saliency::CSaliencyPixel* pCharacterizingSaliencyPixel = *ppSaliencyPixel;
                    ExpansionList.push_back(pCharacterizingSaliencyPixel);
                    pCharacterizingSaliencyPixel->StartBrodcast();
                    const Mathematics::_2D::CVector2D& Center = pCharacterizingSaliencyPixel->GetContinousLocation();
                    real Axx = _REAL_ZERO_, Axy = _REAL_ZERO_, Ayy = _REAL_ZERO_, Aw = _REAL_ZERO_;
                    while (ExpansionList.size())
                    {
                        Saliency::CSaliencyPixel* pExpansionSaliencyNode = ExpansionList.front();
                        ExpansionList.pop_front();
                        list<Saliency::CSaliencyPixel*>::const_iterator EndSourceConnectedPixels = pExpansionSaliencyNode->GetConnectedPixels().end();
                        for (list<Saliency::CSaliencyPixel*>::const_iterator ppSourceConnectedPixel = pExpansionSaliencyNode->GetConnectedPixels().begin(); ppSourceConnectedPixel != EndSourceConnectedPixels; ++ppSourceConnectedPixel)
                        {
                            Saliency::CSaliencyPixel* pSourceConnectedPixel = *ppSourceConnectedPixel;
                            if (pSourceConnectedPixel->Broadcasting(pCharacterizingSaliencyPixel, pExpansionSaliencyNode, true))
                            {
                                const real BroadCastDistance = pSourceConnectedPixel->GetBroadCastDistance();
                                if (BroadCastDistance < MaximalPathDistance)
                                {
                                    ExpansionList.push_back(pSourceConnectedPixel);

#ifdef _SALIENCYBLOCK_USE_LOOKUP_TABLE_

                                    Center.AddWeightedCovariance(pSourceConnectedPixel->GetContinousLocation(), s_ELT.m_pExp[RoundNegativeRealToInteger(BroadCastDistance * BroadCastDistance * Scale)], Axx, Axy, Ayy, Aw);

#else

                                    Center.AddWeightedCovariance(pSourceConnectedPixel->GetContinousLocation(), RealExp(BroadCastDistance * BroadCastDistance * ExponentFactor), Axx, Axy, Ayy, Aw);
#endif

                                }
                            }
                        }
                    }
                    if (StructuralTensor2D.Decomposition(Axx / Aw, Axy / Aw, Ayy / Aw))
                    {
                        pCharacterizingSaliencyPixel->StructuralCharacterize(StructuralTensor2D.GetUnitaryMainAxis(), StructuralTensor2D.GetMainAxisLength(), StructuralTensor2D.GetSecondaryAxisLength());
                    }
                    else
                    {
                        pCharacterizingSaliencyPixel->SetStructuralSingular();
                    }
                }
                switch (Mode)
                {
                    case eLineal:
                        for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                        {
                            (*ppSaliencyPixel)->CharacterizeLineal(ThresholdTransitional, ThresholdElongated);
                        }
                        break;
                    case ePower:
                        for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                        {
                            (*ppSaliencyPixel)->CharacterizePower(ThresholdTransitional, ThresholdElongated, Power);
                        }
                        break;
                    case eApeture:
                        for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                        {
                            (*ppSaliencyPixel)->CharacterizeApeture(ThresholdTransitional, ThresholdElongated);
                        }
                        break;
                    case ePowerApeture:
                        for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                        {
                            (*ppSaliencyPixel)->CharacterizePowerApeture(ThresholdTransitional, ThresholdElongated, Power);
                        }
                        break;
                }
            }

            void CSaliencyBlock::Segmentation()
            {
                list<Saliency::CSaliencyPixel*> ExpansionList;
                list<Saliency::CSaliencyPixel*>::const_iterator EndPixels = m_SaliencyPixels.end();
                for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                    if (!(*ppSaliencyPixel)->GetCharacterizedSaliencySegment())
                    {
                        Saliency::CCharacterizedSaliencySegment* pCharacterizedSaliencySegment = new Saliency::CCharacterizedSaliencySegment(this, *ppSaliencyPixel);
                        m_CharacterizedSaliencySegments.push_back(pCharacterizedSaliencySegment);
                        ++m_CharacterizedSaliencySegmentDistribution[pCharacterizedSaliencySegment->GetCharacterizationType()];
                    }
                list<CCharacterizedSaliencySegment*>::const_iterator EndCharacterizedSaliencySegments = m_CharacterizedSaliencySegments.end();
                for (list<CCharacterizedSaliencySegment*>::const_iterator ppCharacterizedSaliencySegment = m_CharacterizedSaliencySegments.begin(); ppCharacterizedSaliencySegment != EndCharacterizedSaliencySegments; ++ppCharacterizedSaliencySegment)
                {
                    (*ppCharacterizedSaliencySegment)->DetermineIncidenceSegments();
                }
            }

            void CSaliencyBlock::GeometricRegression(const uint MinimalPixels, const real MaximalUncertainty, const real MinimalLength, const real MaximalDeviation, const CCharacterizedSaliencySegment::RegressionWeightingMode Mode, const real Power)
            {
                list<CCharacterizedSaliencySegment*>::const_iterator EndCharacterizedSaliencySegments = m_CharacterizedSaliencySegments.end();
                for (list<CCharacterizedSaliencySegment*>::const_iterator ppCharacterizedSaliencySegment = m_CharacterizedSaliencySegments.begin(); ppCharacterizedSaliencySegment != EndCharacterizedSaliencySegments; ++ppCharacterizedSaliencySegment)
                {
                    (*ppCharacterizedSaliencySegment)->GeometricRegression(MinimalPixels, MaximalUncertainty, MinimalLength, MaximalDeviation, Mode, Power);
                }
            }

            void CSaliencyBlock::PerceptualOrganization(const CImageSize& ImageSize, const real MaximalOrientationDeviation)
            {
                list<CCharacterizedSaliencySegment*>::const_iterator EndCharacterizedSaliencySegments = m_CharacterizedSaliencySegments.end();
                for (list<CCharacterizedSaliencySegment*>::const_iterator ppCharacterizedSaliencySegment = m_CharacterizedSaliencySegments.begin(); ppCharacterizedSaliencySegment != EndCharacterizedSaliencySegments; ++ppCharacterizedSaliencySegment)
                {
                    (*ppCharacterizedSaliencySegment)->PerceptualOrganizationCollinearity(ImageSize, MaximalOrientationDeviation);
                }

                /*
                 list<CCharacterizedSaliencySegment*>::const_iterator EndCharacterizedSaliencySegments = m_CharacterizedSaliencySegments.end();
                 for (list<CCharacterizedSaliencySegment*>::const_iterator ppCharacterizedSaliencySegment = m_CharacterizedSaliencySegments.begin(); ppCharacterizedSaliencySegment != EndCharacterizedSaliencySegments; ++ppCharacterizedSaliencySegment)
                 (*ppCharacterizedSaliencySegment)->PerceptualOrganizationCollinearity();
                 */
            }

            const list<CCharacterizedSaliencySegment*>& CSaliencyBlock::GetCharacterizedSaliencySegments() const
            {
                return m_CharacterizedSaliencySegments;
            }

            const uint* CSaliencyBlock::GetCharacterizedSaliencySegmentDistribution() const
            {
                return m_CharacterizedSaliencySegmentDistribution;
            }

            bool CSaliencyBlock::IsBoundingBoxInteresecting(const CSaliencyBlock* pSaliencyBlock) const
            {
                return m_BoundingBox.Intersects(pSaliencyBlock->m_BoundingBox);
            }

            real CSaliencyBlock::GetBoundingBoxDistance(const CSaliencyBlock* pSaliencyBlock) const
            {
                return m_BoundingBox.GetDistance(pSaliencyBlock->m_BoundingBox);
            }

            real CSaliencyBlock::GetPixelContinousLocationDistance(const CSaliencyBlock* pSaliencyBlock) const
            {
                real MinimalDistance = _REAL_MAX_;
                list<CSaliencyPixel*>::const_iterator ExternEndSaliencyPixels = pSaliencyBlock->m_SaliencyPixels.end();
                list<CSaliencyPixel*>::const_iterator ExternBeginSaliencyPixels = pSaliencyBlock->m_SaliencyPixels.begin();
                list<CSaliencyPixel*>::const_iterator EndSaliencyPixels = m_SaliencyPixels.end();
                for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel)
                {
                    const Mathematics::_2D::CVector2D& Location = (*ppSaliencyPixel)->GetContinousLocation();
                    for (list<CSaliencyPixel*>::const_iterator ppExternSaliencyPixel = ExternBeginSaliencyPixels; ppExternSaliencyPixel != ExternEndSaliencyPixels; ++ppExternSaliencyPixel)
                    {
                        const real Distance = Location.GetSquareDistance((*ppExternSaliencyPixel)->GetContinousLocation());
                        if (Distance < MinimalDistance)
                        {
                            MinimalDistance = Distance;
                        }
                    }
                }
                return RealSqrt(MinimalDistance);
            }

            list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*> CSaliencyBlock::GetGeometricPrimitives() const
            {
                list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*> BlockGeometricPrimitives;
                list<CCharacterizedSaliencySegment*>::const_iterator EndCharacterizedSaliencySegments = m_CharacterizedSaliencySegments.end();
                for (list<CCharacterizedSaliencySegment*>::const_iterator ppCharacterizedSaliencySegment = m_CharacterizedSaliencySegments.begin(); ppCharacterizedSaliencySegment != EndCharacterizedSaliencySegments; ++ppCharacterizedSaliencySegment)
                {
                    const list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>& SegmentGeometricPrimitives = (*ppCharacterizedSaliencySegment)->GetGeometricPrimitives();
                    BlockGeometricPrimitives.insert(BlockGeometricPrimitives.end(), SegmentGeometricPrimitives.begin(), SegmentGeometricPrimitives.end());
                }
                return BlockGeometricPrimitives;
            }

            void CSaliencyBlock::ComputeConnectivityGrade(const int* pDeltas)
            {
                list<CSaliencyPixel*>::const_iterator EndSaliencyPixels = m_SaliencyPixels.end();
                for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel)
                {
                    CSaliencyPixel* pSaliencyPixel = *ppSaliencyPixel;
                    for (uint i = 0; i < 8; ++i)
                    {
                        CSaliencyPixel* pAuxiliarSaliencyPixel = pSaliencyPixel + pDeltas[i];
                        if (pAuxiliarSaliencyPixel->IsBlockConnected(this, pSaliencyPixel))
                        {
                            pSaliencyPixel->AddConnectedPixel(pAuxiliarSaliencyPixel);
                        }
                    }
                    ++m_ConnectivityDistribution[pSaliencyPixel->GetConnectivityGrade()];
                }
            }

            void CSaliencyBlock::SortedInsert(const CSaliencyPixel* pSaliencyPixel, list<const CSaliencyPixel*>& SortedList)
            {
                switch (SortedList.size())
                {
                    case 0:
                        SortedList.push_back(pSaliencyPixel);
                        return;
                    case 1:
                        if (pSaliencyPixel->GetBroadCastDistance() >= SortedList.back()->GetBroadCastDistance())
                        {
                            SortedList.push_back(pSaliencyPixel);
                        }
                        else
                        {
                            SortedList.push_front(pSaliencyPixel);
                        }
                        return;
                    case 2:
                    {
                        const real CurrentBroadCastDistance = pSaliencyPixel->GetBroadCastDistance();
                        if (CurrentBroadCastDistance >= SortedList.back()->GetBroadCastDistance())
                        {
                            SortedList.push_back(pSaliencyPixel);
                        }
                        else if (CurrentBroadCastDistance <= SortedList.front()->GetBroadCastDistance())
                        {
                            SortedList.push_front(pSaliencyPixel);
                        }
                        else
                        {
                            SortedList.insert(SortedList.end(), pSaliencyPixel);
                        }
                    }
                    return;
                    default:
                    {
                        const real CurrentBroadCastDistance = pSaliencyPixel->GetBroadCastDistance();
                        if (CurrentBroadCastDistance >= SortedList.back()->GetBroadCastDistance())
                        {
                            SortedList.push_back(pSaliencyPixel);
                        }
                        else if (CurrentBroadCastDistance <= SortedList.front()->GetBroadCastDistance())
                        {
                            SortedList.push_front(pSaliencyPixel);
                            return;
                        }
                        else
                        {
                            list<const CSaliencyPixel*>::iterator Begin = SortedList.begin();
                            for (list<const CSaliencyPixel*>::iterator ppSaliencyPixel = --SortedList.end(); ppSaliencyPixel != Begin; --ppSaliencyPixel)
                                if (CurrentBroadCastDistance >= (*ppSaliencyPixel)->GetBroadCastDistance())
                                {
                                    SortedList.insert(++ppSaliencyPixel, pSaliencyPixel);
                                    return;
                                }
                            SortedList.insert(++Begin, pSaliencyPixel);
                        }
                    }
                    return;
                }
            }

            real CSaliencyBlock::ComputeRoot(const bool UseContinousLocation)
            {
                list<CSaliencyPixel*> InitialCandidates;
                if (m_ConnectivityDistribution[1])
                {
                    list<CSaliencyPixel*>::const_iterator EndSaliencyPixel = m_SaliencyPixels.end();
                    for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndSaliencyPixel; ++ppSaliencyPixel)
                        if ((*ppSaliencyPixel)->IsSingleConnected())
                        {
                            InitialCandidates.push_back(*ppSaliencyPixel);
                        }
                }
                real GlobalMaximalTotalDistance = _REAL_ZERO_;
                list<const CSaliencyPixel*> BroadCastingExpasion;
                list<CSaliencyPixel*>::iterator SelectionEndSaliencyPixel = InitialCandidates.size() ? InitialCandidates.end() : m_SaliencyPixels.end();
                for (list<CSaliencyPixel*>::iterator ppSaliencyPixel = (InitialCandidates.size() ? InitialCandidates.begin() : m_SaliencyPixels.begin()); ppSaliencyPixel != SelectionEndSaliencyPixel; ++ppSaliencyPixel)
                {
                    CSaliencyPixel* pBroadcastingSaliencyPixel = *ppSaliencyPixel;
                    pBroadcastingSaliencyPixel->StartBrodcast();
                    BroadCastingExpasion.push_back(pBroadcastingSaliencyPixel);
                    real TotalDistance = _REAL_ZERO_;
                    while (BroadCastingExpasion.size())
                    {
                        const CSaliencyPixel* pPreSaliencyPixel = BroadCastingExpasion.front();
                        BroadCastingExpasion.pop_front();
                        list<CSaliencyPixel*>::const_iterator EndConnectedPixels = pPreSaliencyPixel->GetConnectedPixels().end();
                        for (list<CSaliencyPixel*>::const_iterator ppPosSaliencyPixel = pPreSaliencyPixel->GetConnectedPixels().begin(); ppPosSaliencyPixel != EndConnectedPixels; ++ppPosSaliencyPixel)
                            if ((*ppPosSaliencyPixel)->Broadcasting(pBroadcastingSaliencyPixel, pPreSaliencyPixel, UseContinousLocation))
                            {
                                SortedInsert(*ppPosSaliencyPixel, BroadCastingExpasion);
                            }
                        const real CurrenttBroadCastDistance = pPreSaliencyPixel->GetBroadCastDistance();
                        TotalDistance += CurrenttBroadCastDistance;
                        if (CurrenttBroadCastDistance > m_MaximalPathDistance)
                        {
                            m_MaximalPathDistance = CurrenttBroadCastDistance;
                        }
                    }
                    pBroadcastingSaliencyPixel->SetConnectivityDiameter(TotalDistance);
                    if (TotalDistance > GlobalMaximalTotalDistance)
                    {
                        GlobalMaximalTotalDistance = TotalDistance;
                        m_pRoot = pBroadcastingSaliencyPixel;
                    }
                }
                return m_MaximalPathDistance;
            }

            real CSaliencyBlock::ComputeRootingDistance()
            {
                list<CSaliencyPixel*>::iterator EndSaliencyPixels = m_SaliencyPixels.end();
                for (list<CSaliencyPixel*>::iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel)
                {
                    (*ppSaliencyPixel)->StartBrodcast();
                }

                list<const CSaliencyPixel*> BroadCastingExpasion;
                BroadCastingExpasion.push_back(m_pRoot);
                real MaximalBroadCastDistance = _REAL_ZERO_;
                while (BroadCastingExpasion.size())
                {
                    const CSaliencyPixel* pPreSaliencyPixel = BroadCastingExpasion.front();
                    BroadCastingExpasion.pop_front();
                    list<CSaliencyPixel*>::const_iterator EndConnectedPixels = pPreSaliencyPixel->GetConnectedPixels().end();
                    for (list<CSaliencyPixel*>::const_iterator ppPosSaliencyPixel = pPreSaliencyPixel->GetConnectedPixels().begin(); ppPosSaliencyPixel != EndConnectedPixels; ++ppPosSaliencyPixel)
                        if ((*ppPosSaliencyPixel)->Broadcasting(m_pRoot, pPreSaliencyPixel, true))
                        {
                            SortedInsert(*ppPosSaliencyPixel, BroadCastingExpasion);
                        }
                    if (pPreSaliencyPixel->GetBroadCastDistance() > MaximalBroadCastDistance)
                    {
                        MaximalBroadCastDistance = pPreSaliencyPixel->GetBroadCastDistance();
                    }
                }
                return MaximalBroadCastDistance;
            }

            bool CSaliencyBlock::SortSaliencyPixelsByMagnitude(CSaliencyPixel* lhs, CSaliencyPixel* rhs)
            {
                return lhs->GetMagnitude() > rhs->GetMagnitude();
            }
        }
    }
}
