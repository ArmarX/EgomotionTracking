/*
 * CharacterizedSaliencySegment.cpp
 *
 *  Created on: 22.12.2011
 *      Author: gonzalez
 */

#include "../../../../Foundation/Mathematics/2D/StructuralTensor2D.h"
#include "../../../../Foundation/Mathematics/1D/Common1D.h"
#include "CharacterizedSaliencySegment.h"
#include "SaliencyPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            IDENTIFIABLE_INITIALIZATION(CCharacterizedSaliencySegment)

            CCharacterizedSaliencySegment::CCharacterizedSaliencySegment(CSaliencyBlock* pSaliencyBlock, CSaliencyPixel* pSaliencyPixel) :
                IDENTIFIABLE_CONTRUCTOR(CCharacterizedSaliencySegment), m_CharacterizationType(eUndefined), m_pSaliencyBlock(pSaliencyBlock), m_BoundingBox2D(pSaliencyPixel->GetContinousLocation()), m_EigenBoundingBox2D(Mathematics::_2D::CVector2D::s_Point_At_Origin_2D), m_RegressionGeometricPrimitives(0)
            {
                ExtractSegment(pSaliencyPixel);
                DetermineStructure();
            }

            CCharacterizedSaliencySegment::~CCharacterizedSaliencySegment()
            {
                ClearGeometricPrimitives();
            }

            uint CCharacterizedSaliencySegment::GeometricRegression(const uint MinimalPixels, const real MaximalUncertainty, const real MinimalLength, const real MaximalDeviation, const RegressionWeightingMode Mode, const real Power)
            {
                ClearGeometricPrimitives();
                if (m_SaliencyPixels.size() > MinimalPixels)
                    switch (m_CharacterizationType)
                    {
                        case CCharacterizedSaliencySegment::eElongated:
                            m_RegressionGeometricPrimitives = MultipleLineSegmentsRegression(m_SaliencyPixels, MinimalPixels, MaximalUncertainty, MinimalLength, MaximalDeviation * _REAL_TWO_, Mode, Power);
                            break;
                        case CCharacterizedSaliencySegment::eTransitional:
                        //m_RegressionGeometricPrimitives = MultipleLineSegmentsRegression(m_SaliencyPixels, MinimalPixels, MaximalUncertainty, MinimalLength, MaximalDeviation, Mode, Power);
                        //break;
                        case CCharacterizedSaliencySegment::eCompact:
                        case CCharacterizedSaliencySegment::eUndefined:
                        case CCharacterizedSaliencySegment::eSingular:
                            break;
                    }
                return m_RegressionGeometricPrimitives.size();
            }

            const CSaliencyBlock* CCharacterizedSaliencySegment::GetSaliencyBlock() const
            {
                return m_pSaliencyBlock;
            }

            const list<CSaliencyPixel*>& CCharacterizedSaliencySegment::GetSaliencyPixels() const
            {
                return m_SaliencyPixels;
            }

            const CContinuousBoundingBox2D& CCharacterizedSaliencySegment::GetBoundingBox() const
            {
                return m_BoundingBox2D;
            }

            const CContinuousBoundingBox2D& CCharacterizedSaliencySegment::GetEigenBoundingBox() const
            {
                return m_EigenBoundingBox2D;
            }

            const Mathematics::_2D::CVector2D& CCharacterizedSaliencySegment::GetGeometricCentroid() const
            {
                return m_GeometricCentroid;
            }

            vector<Mathematics::_2D::CVector2D> CCharacterizedSaliencySegment::GetEigenBoundingBoxPoints(const bool Axes) const
            {
                vector<Mathematics::_2D::CVector2D> EigenBoundingBoxPoints;
                if (m_CharacterizationType != eSingular)
                {
                    EigenBoundingBoxPoints.reserve(Axes ? 9 : 4);
                    const Mathematics::_2D::CVector2D& MaxPoint = m_EigenBoundingBox2D.GetMaxPoint();
                    const Mathematics::_2D::CVector2D& MinPoint = m_EigenBoundingBox2D.GetMinPoint();
                    const Mathematics::_2D::CVector2D Dxp = m_MainAxis * MaxPoint.GetX();
                    const Mathematics::_2D::CVector2D Dxn = m_MainAxis * MinPoint.GetX();
                    const Mathematics::_2D::CVector2D Dyp = m_SecondaryAxis * MaxPoint.GetY();
                    const Mathematics::_2D::CVector2D Dyn = m_SecondaryAxis * MinPoint.GetY();
                    EigenBoundingBoxPoints.push_back(m_GeometricCentroid + Dxp + Dyn);
                    EigenBoundingBoxPoints.push_back(m_GeometricCentroid + Dxn + Dyn);
                    EigenBoundingBoxPoints.push_back(m_GeometricCentroid + Dxn + Dyp);
                    EigenBoundingBoxPoints.push_back(m_GeometricCentroid + Dxp + Dyp);
                    if (Axes)
                    {
                        EigenBoundingBoxPoints.push_back(m_GeometricCentroid + Dxp);
                        EigenBoundingBoxPoints.push_back(m_GeometricCentroid + Dxn);
                        EigenBoundingBoxPoints.push_back(m_GeometricCentroid + Dyp);
                        EigenBoundingBoxPoints.push_back(m_GeometricCentroid + Dyn);
                        EigenBoundingBoxPoints.push_back(m_GeometricCentroid);
                    }
                }
                return EigenBoundingBoxPoints;
            }

            CCharacterizedSaliencySegment::CharacterizationType CCharacterizedSaliencySegment::GetCharacterizationType() const
            {
                return m_CharacterizationType;
            }

            const list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>& CCharacterizedSaliencySegment::GetGeometricPrimitives() const
            {
                return m_RegressionGeometricPrimitives;
            }

            void CCharacterizedSaliencySegment::DetermineIncidenceSegments()
            {
                CCharacterizedSaliencySegment* pLastAdded = nullptr;
                list<CSaliencyPixel*>::const_iterator EndPixels = m_SaliencyPixels.end();
                for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                {
                    list<CSaliencyPixel*>::const_iterator EndConnectedSaliencyPixel = (*ppSaliencyPixel)->GetConnectedPixels().end();
                    for (list<CSaliencyPixel*>::const_iterator ppConnectedSaliencyPixel = (*ppSaliencyPixel)->GetConnectedPixels().begin(); ppConnectedSaliencyPixel != EndConnectedSaliencyPixel; ++ppConnectedSaliencyPixel)
                        if ((*ppConnectedSaliencyPixel)->GetCharacterizedSaliencySegment() != this)
                        {
                            CCharacterizedSaliencySegment* pIncident = (*ppConnectedSaliencyPixel)->GetCharacterizedSaliencySegment();
                            if (pIncident && (pIncident != pLastAdded))
                            {
                                m_IncidenceSegments.push_back(pIncident);
                                pLastAdded = pIncident;
                            }
                        }
                }
                if (m_IncidenceSegments.size() > 1)
                {
                    m_IncidenceSegments.sort();
                    m_IncidenceSegments.unique();
                }
            }

            const list<CCharacterizedSaliencySegment*>& CCharacterizedSaliencySegment::GetIncidenceSegments()
            {
                return m_IncidenceSegments;
            }

            bool CCharacterizedSaliencySegment::IsFreeCircularZone(const Mathematics::_2D::CVector2D& Center, const real Radius, const list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>& RegressionGeometricPrimitives, Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D* pRegressionGeometricPrimitiveA, Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D* pRegressionGeometricPrimitiveB)
            {
                bool FreeCircularZone = true;
                list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>::const_iterator EndRegressionGeometricPrimitives = RegressionGeometricPrimitives.end();
                for (list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>::const_iterator ppRegressionGeometricPrimitive = RegressionGeometricPrimitives.begin(); ppRegressionGeometricPrimitive != EndRegressionGeometricPrimitives; ++ppRegressionGeometricPrimitive)
                {
                    Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D* pRegressionGeometricPrimitive = *ppRegressionGeometricPrimitive;
                    if ((pRegressionGeometricPrimitive != pRegressionGeometricPrimitiveA) && (pRegressionGeometricPrimitive != pRegressionGeometricPrimitiveB) && pRegressionGeometricPrimitive->IsInsideCircularZone(Center, Radius))
                    {
                        FreeCircularZone = false;
                        break;
                    }
                }
                return FreeCircularZone;
            }

            void CCharacterizedSaliencySegment::PerceptualOrganizationCollinearity(const CImageSize& ImageSize, const real MaximalOrientationDeviation)
            {
                if (m_RegressionGeometricPrimitives.size() > 1)
                {
                    list<Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D*> RegressionLineSegments;
                    list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>::const_iterator EndRegressionGeometricPrimitives = m_RegressionGeometricPrimitives.end();
                    for (list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>::const_iterator ppRegressionGeometricPrimitive = m_RegressionGeometricPrimitives.begin(); ppRegressionGeometricPrimitive != EndRegressionGeometricPrimitives; ++ppRegressionGeometricPrimitive)
                        if ((*ppRegressionGeometricPrimitive)->IsActive() && ((*ppRegressionGeometricPrimitive)->GetGeometricPrimitiveTypeId() == Mathematics::_2D::Geometry::CGeometricPrimitive2D::eLine))
                        {
                            RegressionLineSegments.push_back(dynamic_cast<Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D*>(*ppRegressionGeometricPrimitive));
                        }
                    if (RegressionLineSegments.size() > 1)
                    {
                        Identifier IdCounter = 0;
                        CRegressionLineSegmentCollinearity2D CurrentCollinearity;
                        RegressionLineSegments.sort(Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D::SortRegressionLineSegmentByLenghtDescent);
                        list<Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D*>::const_iterator EndRegressionLineSegments = RegressionLineSegments.end();
                        for (list<Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D*>::const_iterator ppRegressionLineSegmentA = RegressionLineSegments.begin(); ppRegressionLineSegmentA != EndRegressionLineSegments; ++ppRegressionLineSegmentA)
                            if ((*ppRegressionLineSegmentA)->IsActive())
                            {
                                CurrentCollinearity.m_pRegressionLineSegmentA = *ppRegressionLineSegmentA;
                                //list<CRegressionLineSegmentCollinearity2D> CollinearityCandidatesSideA;
                                //list<CRegressionLineSegmentCollinearity2D> CollinearityCandidatesSideB;

                                list<Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D*>::const_iterator ppRegressionLineSegmentB = ppRegressionLineSegmentA;
                                for (++ppRegressionLineSegmentB; ppRegressionLineSegmentB != EndRegressionLineSegments; ++ppRegressionLineSegmentB)
                                    if ((*ppRegressionLineSegmentB)->IsActive())
                                    {
                                        CurrentCollinearity.m_pRegressionLineSegmentB = *ppRegressionLineSegmentB;
                                        CurrentCollinearity.m_OrientationDeviation = CurrentCollinearity.m_pRegressionLineSegmentA->GetOrientationDeviation(CurrentCollinearity.m_pRegressionLineSegmentB) * _REAL_INVERSE_HALF_PI_;
                                        if (CurrentCollinearity.m_OrientationDeviation < MaximalOrientationDeviation)
                                        {
                                            CurrentCollinearity.m_EndPointSignedDistance = CurrentCollinearity.m_pRegressionLineSegmentA->GetClosestEndpointsDistance(CurrentCollinearity.m_pRegressionLineSegmentB, CurrentCollinearity.m_PointA, CurrentCollinearity.m_PointB);
                                            if (RealAbs(CurrentCollinearity.m_EndPointSignedDistance) < TMax(CurrentCollinearity.m_pRegressionLineSegmentA->GetLength(), CurrentCollinearity.m_pRegressionLineSegmentB->GetLength()))
                                            {
                                                CurrentCollinearity.m_GroupingZoneRadius = RealAbs(CurrentCollinearity.m_EndPointSignedDistance) * _REAL_HALF_;
                                                CurrentCollinearity.m_MidPointAB.SetMidPoint(CurrentCollinearity.m_PointA, CurrentCollinearity.m_PointB);
                                                CurrentCollinearity.m_Active = IsFreeCircularZone(CurrentCollinearity.m_MidPointAB, CurrentCollinearity.m_GroupingZoneRadius, m_RegressionGeometricPrimitives, CurrentCollinearity.m_pRegressionLineSegmentA, CurrentCollinearity.m_pRegressionLineSegmentB);
                                                if (CurrentCollinearity.m_Active)
                                                {
                                                    CurrentCollinearity.m_pRegressionLineSegmentA->GetEndPointPerpendicularDistance(CurrentCollinearity.m_pRegressionLineSegmentB, CurrentCollinearity.m_EndPointAPerpendicularDistance, CurrentCollinearity.m_EndPointBPerpendicularDistance);
                                                    CurrentCollinearity.m_EndPointsMaximalPerpendicularDistance = TMax(CurrentCollinearity.m_EndPointAPerpendicularDistance, CurrentCollinearity.m_EndPointBPerpendicularDistance);
                                                    CurrentCollinearity.m_MaximalLinesDeviation = TMax(CurrentCollinearity.m_pRegressionLineSegmentA->GetMaximalDeviation(), CurrentCollinearity.m_pRegressionLineSegmentB->GetMaximalDeviation());
                                                    CurrentCollinearity.m_Active = (CurrentCollinearity.m_EndPointsMaximalPerpendicularDistance < _REAL_SQRT2_);
                                                    //if
                                                    {
                                                        CurrentCollinearity.m_IntersectionPointAvailable = (CurrentCollinearity.m_pRegressionLineSegmentA->CalculateIntersectionPoint(CurrentCollinearity.m_pRegressionLineSegmentB, CurrentCollinearity.m_IntersectionLinesPoint) && ImageSize.IsInside(CurrentCollinearity.m_IntersectionLinesPoint));
                                                        CurrentCollinearity.m_Id = IdCounter++;

                                                        /*if (Mathematics::_1D::IsNegative(CurrentCollinearity.m_EndPointSignedDistance))
                                                         {
                                                         CurrentCollinearity.m_EndPointSignedDistance = -CurrentCollinearity.m_EndPointSignedDistance;
                                                         CollinearityCandidatesSideA.push_back(CurrentCollinearity);
                                                         }
                                                         else
                                                         CollinearityCandidatesSideB.push_back(CurrentCollinearity);*/

                                                        m_CollinearityCandidates.push_back(CurrentCollinearity);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                if (m_CollinearityCandidates.size())
                                {
                                    /*if (CollinearityCandidatesSideA.size() > 1)
                                     CollinearityCandidatesSideA.sort(CRegressionLineSegmentCollinearity2D::SortCollinearityByEndPointDirectDistanceDescent);
                                     if (CollinearityCandidatesSideB.size() > 1)
                                     CollinearityCandidatesSideB.sort(CRegressionLineSegmentCollinearity2D::SortCollinearityByEndPointDirectDistanceDescent);*/
                                }
                            }
                    }
                }
            }

            void CCharacterizedSaliencySegment::PerceptualOrganizationEndPointContinuation(const CImageSize& ImageSize)
            {

            }

            void CCharacterizedSaliencySegment::ExtractSegment(CSaliencyPixel* pSaliencyPixel)
            {
                const CSaliencyPixel::CharacterizationType CharacterizationType = pSaliencyPixel->GetCharacterizationType();
                m_CharacterizationType = CCharacterizedSaliencySegment::CharacterizationType(CharacterizationType);
                pSaliencyPixel->SetCharacterizedSaliencySegment(this);
                m_SaliencyPixels.push_back(pSaliencyPixel);
                list<CSaliencyPixel*> ExpandingSaliencyPixels;
                ExpandingSaliencyPixels.push_back(pSaliencyPixel);
                m_GeometricCentroid = pSaliencyPixel->GetContinousLocation();
                while (ExpandingSaliencyPixels.size())
                {
                    CSaliencyPixel* pExpandingSaliencyPixel = ExpandingSaliencyPixels.front();
                    ExpandingSaliencyPixels.pop_front();
                    list<CSaliencyPixel*>::const_iterator EndConnectedPixels = pExpandingSaliencyPixel->GetConnectedPixels().end();
                    for (list<CSaliencyPixel*>::const_iterator ppSaliencyConnectedPixels = pExpandingSaliencyPixel->GetConnectedPixels().begin(); ppSaliencyConnectedPixels != EndConnectedPixels; ++ppSaliencyConnectedPixels)
                        if ((*ppSaliencyConnectedPixels)->IsSegmentable(CharacterizationType))
                        {
                            CSaliencyPixel* pConnectedSaliencyPixel = *ppSaliencyConnectedPixels;
                            pConnectedSaliencyPixel->SetCharacterizedSaliencySegment(this);
                            ExpandingSaliencyPixels.push_back(pConnectedSaliencyPixel);
                            m_SaliencyPixels.push_back(pConnectedSaliencyPixel);
                            m_BoundingBox2D.Extend(pConnectedSaliencyPixel->GetContinousLocation());
                            m_GeometricCentroid += pConnectedSaliencyPixel->GetContinousLocation();
                        }
                }
                m_GeometricCentroid /= real(m_SaliencyPixels.size());
            }

            void CCharacterizedSaliencySegment::DetermineStructure()
            {
                const uint TotalPixels = m_SaliencyPixels.size();
                switch (TotalPixels)
                {
                    case 1:
                        m_CharacterizationType = eSingular;
                        break;
                    case 2:
                        m_CharacterizationType = eSingular;
                        m_MainAxis = m_SaliencyPixels.front()->GetContinousLocation() - m_SaliencyPixels.back()->GetContinousLocation();
                        m_EigenBoundingBox2D.Extend(m_MainAxis.Normalize(), _REAL_ZERO_);
                        m_SecondaryAxis = m_MainAxis;
                        m_SecondaryAxis.Rotate(_REAL_HALF_PI_);
                        break;
                    default:
                    {
                        real Axx = _REAL_ZERO_, Axy = _REAL_ZERO_, Ayy = _REAL_ZERO_;
                        list<CSaliencyPixel*>::const_iterator EndPixels = m_SaliencyPixels.end();
                        for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                        {
                            m_GeometricCentroid.AddCovariance((*ppSaliencyPixel)->GetContinousLocation(), Axx, Axy, Ayy);
                        }
                        const real Normalization = _REAL_ONE_ / (real(TotalPixels));
                        Mathematics::_2D::CStructuralTensor2D StructuralTensor2D(Axx * Normalization, Axy * Normalization, Ayy * Normalization);
                        if (StructuralTensor2D.Decomposition())
                        {
                            m_MainAxis = StructuralTensor2D.GetUnitaryMainAxis();
                            m_SecondaryAxis = StructuralTensor2D.GetUnitarySecondaryAxis();
                            for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SaliencyPixels.begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                            {
                                const Mathematics::_2D::CVector2D Delta = (*ppSaliencyPixel)->GetContinousLocation() - m_GeometricCentroid;
                                m_EigenBoundingBox2D.Extend(m_MainAxis.ScalarProduct(Delta), m_SecondaryAxis.ScalarProduct(Delta));
                            }
                        }
                        else
                        {
                            m_CharacterizationType = eSingular;
                        }
                    }
                    break;
                }
            }

            void CCharacterizedSaliencySegment::ClearGeometricPrimitives()
            {
                if (m_RegressionGeometricPrimitives.size())
                {
                    list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>::iterator EndGeometricPrimitives = m_RegressionGeometricPrimitives.end();
                    for (list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>::iterator ppGeometricPrimitive2D = m_RegressionGeometricPrimitives.begin(); ppGeometricPrimitive2D != EndGeometricPrimitives; ++ppGeometricPrimitive2D)
                        RELEASE_OBJECT_DIRECT(*ppGeometricPrimitive2D)
                        m_RegressionGeometricPrimitives.clear();
                }
            }

            list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*> CCharacterizedSaliencySegment::MultipleLineSegmentsRegression(const list<CSaliencyPixel*>& SaliencyPixels, const uint MinimalPixels, const real MaximalUncertainty, const real MinimalLength, const real MaximalDeviation, const RegressionWeightingMode Mode, const real Power)
            {
                list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*> RegressionLineSegments;
                if (SaliencyPixels.size() > MinimalPixels)
                {
                    Mathematics::_2D::CVector2D MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint;
                    real MainAxisLength = _REAL_ZERO_, SecondaryAxisLength = _REAL_ZERO_, CurrentMaximalDeviation = _REAL_ZERO_, CurrentMeanDeviation = _REAL_ZERO_;
                    bool RegressionResult = false;
                    switch (Mode)
                    {
                        case eNonWeighting:
                            RegressionResult = LeastSquaresLineFitting(SaliencyPixels, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, CurrentMaximalDeviation, CurrentMeanDeviation);
                            break;
                        case eLinealEccentricityWeighting:
                            RegressionResult = LinealEccentricityWeightingLeastSquaresLineFitting(SaliencyPixels, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, CurrentMaximalDeviation, CurrentMeanDeviation);
                            break;
                        case ePowerEccentricityWeighting:
                            RegressionResult = PowerEccentricityWeightingLeastSquaresLineFitting(SaliencyPixels, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, CurrentMaximalDeviation, CurrentMeanDeviation, Power);
                            break;
                        case eApetureEccentricityWeighting:
                            RegressionResult = ApetureEccentricityWeightingLeastSquaresLineFitting(SaliencyPixels, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, CurrentMaximalDeviation, CurrentMeanDeviation);
                            break;
                        case ePowerApetureEccentricityWeighting:
                            RegressionResult = PowerApetureEccentricityWeightingLeastSquaresLineFitting(SaliencyPixels, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, CurrentMaximalDeviation, CurrentMeanDeviation, Power);
                            break;
                    }
                    if (RegressionResult && (MainAxisLength > MinimalLength))
                    {
                        if ((CurrentMaximalDeviation < MaximalDeviation) && (SecondaryAxisLength < MaximalDeviation) && ((SecondaryAxisLength / MainAxisLength) < MaximalUncertainty))
                        {
                            RegressionLineSegments.push_back(new Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D(A, B, Center, MainAxis, SecondaryAxis, MinPoint, MaxPoint, CurrentMaximalDeviation, CurrentMeanDeviation, this));
                        }
                        else
                        {
                            const list<list<CSaliencyPixel*>*> SubSets = SplitSubSetsSaliencyPixels(SaliencyPixels);
                            if (SubSets.size())
                            {
                                list<list<CSaliencyPixel*>*>::const_iterator EndSubSets = SubSets.end();
                                for (list<list<CSaliencyPixel*>*>::const_iterator ppSubset = SubSets.begin(); ppSubset != EndSubSets; ++ppSubset)
                                {
                                    if ((*ppSubset)->size() >= MinimalPixels)
                                    {
                                        list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*> SubSetRegressionLineSegments = MultipleLineSegmentsRegression(**ppSubset, MinimalPixels, MaximalUncertainty, MinimalLength, MaximalDeviation, Mode, Power);
                                        RegressionLineSegments.insert(RegressionLineSegments.end(), SubSetRegressionLineSegments.begin(), SubSetRegressionLineSegments.end());
                                    }
                                    RELEASE_OBJECT_DIRECT(*ppSubset)
                                }
                            }
                        }
                    }
                }
                return RegressionLineSegments;
            }

            list<list<CSaliencyPixel*>*> CCharacterizedSaliencySegment::SplitSubSetsSaliencyPixels(const list<CSaliencyPixel*>& SaliencyPixels)
            {
                list<list<CSaliencyPixel*>*> SubSets;
                list<CSaliencyPixel*> SortedDeviationList = SaliencyPixels;
                SortedDeviationList.sort(SortPixelsByEccentricityDescent);
                list<CSaliencyPixel*>::const_iterator EndSaliencyPixels = SortedDeviationList.end();
                for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = SortedDeviationList.begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel)
                {
                    (*ppSaliencyPixel)->SetExpansionList(nullptr);
                }
                while (SortedDeviationList.size())
                {
                    CSaliencyPixel* pSaliencyPixel = SortedDeviationList.front();
                    SortedDeviationList.pop_front();
                    if (!pSaliencyPixel->GetExpansionList())
                    {
                        list<CSaliencyPixel*>* pExpansionList = nullptr;
                        real MinimalDeviationDistance = _REAL_MAX_;
                        list<CSaliencyPixel*>::const_iterator EndConnectedPixels = pSaliencyPixel->GetConnectedPixels().end();
                        for (list<CSaliencyPixel*>::const_iterator ppSaliencyConnectedPixels = pSaliencyPixel->GetConnectedPixels().begin(); ppSaliencyConnectedPixels != EndConnectedPixels; ++ppSaliencyConnectedPixels)
                            if ((*ppSaliencyConnectedPixels)->GetExpansionList())
                            {
                                const real CurrentDeviationDistance = RealAbs((*ppSaliencyConnectedPixels)->GetLinealEccentricity() - pSaliencyPixel->GetLinealEccentricity());
                                if (CurrentDeviationDistance < MinimalDeviationDistance)
                                {
                                    pExpansionList = (*ppSaliencyConnectedPixels)->GetExpansionList();
                                    MinimalDeviationDistance = CurrentDeviationDistance;
                                }
                            }
                        if (!pExpansionList)
                        {
                            pExpansionList = new list<CSaliencyPixel*>;
                            SubSets.push_back(pExpansionList);
                        }
                        pExpansionList->push_back(pSaliencyPixel);
                        pSaliencyPixel->SetExpansionList(pExpansionList);
                    }
                }
                EndSaliencyPixels = SaliencyPixels.end();
                for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = SaliencyPixels.begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel)
                {
                    (*ppSaliencyPixel)->SetExpansionList(nullptr);
                }
                if (SubSets.size() == 1)
                {
                    RELEASE_OBJECT_DIRECT(SubSets.front())
                    SubSets.clear();
                }
                return SubSets;
            }

            bool CCharacterizedSaliencySegment::PowerApetureEccentricityWeightingLeastSquaresLineFitting(const list<CSaliencyPixel*>& SaliencyPixels, Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, Mathematics::_2D::CVector2D& A, Mathematics::_2D::CVector2D& B, Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint, real& MainAxisLength, real& SecondaryAxisLength, real& MaximalDeviation, real& MeanDeviation, const real Power)
            {
                const uint TotalPixels = SaliencyPixels.size();

                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN


                Mathematics::ND::LinearAlgebra::CMatrixND X(TotalPixels, 2);
                Mathematics::ND::LinearAlgebra::CMatrixND Y(TotalPixels, 1);
                Mathematics::ND::LinearAlgebra::CMatrixND W(TotalPixels, TotalPixels);
                W.setZero();
                uint Index = 0;
                Mathematics::_2D::CVector2D WeightedContinousLocationAccumulator;
                real WeightAccumulator = _REAL_ZERO_;
                list<CSaliencyPixel*>::const_iterator EndSaliencyPixels = SaliencyPixels.end();
                for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = SaliencyPixels.begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel, ++Index)
                {
                    const Mathematics::_2D::CVector2D& P = (*ppSaliencyPixel)->GetContinousLocation();
                    const real Weight = (*ppSaliencyPixel)->GetPowerApetureEccentricity(Power);
                    X(Index, 0) = P.GetX();
                    X(Index, 1) = _REAL_ONE_;
                    Y(Index, 0) = P.GetY();
                    W(Index, Index) = Weight;
                    WeightedContinousLocationAccumulator += P * Weight;
                    WeightAccumulator += Weight;
                }

                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN


                const Mathematics::ND::LinearAlgebra::CMatrixND XtW = X.transpose() * W;
                const Mathematics::ND::LinearAlgebra::CMatrixND Xs = XtW * X;
                const Mathematics::_2D::CVector2D Centroid = WeightedContinousLocationAccumulator / WeightAccumulator;
                if (Mathematics::_1D::IsZero(Xs.determinant()))
                {
                    return ManageSingularRegression(SaliencyPixels, Centroid, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, MaximalDeviation, MeanDeviation);
                }
                //try
                //{
                Mathematics::ND::LinearAlgebra::CMatrixND LSF = Xs.inverse() * (XtW * Y);
                DetermineAxes(LSF(0, 0), LSF(1, 0), Centroid, MainAxis, SecondaryAxis, Center);
                DetemineLine(SaliencyPixels, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, MaximalDeviation, MeanDeviation);
                return true;
                //} catch (Mathematics::ND::LinearAlgebra::CMatrixBaseException& E)
                //{
                //                  g_ErrorStringOutput << "BEGIN EVP::[" << __FILE__ << ":" << __LINE__ << "]" << g_EndLine;
                //                  g_ErrorStringOutput << E.what() << g_EndLine;
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("X", X);
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("W", W);
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("Y", Y);
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("Xs", Xs);
                //                  g_ErrorStringOutput << "END EVP::[" << __FILE__ << ":" << __LINE__ << "]" << g_EndLine;
                //}
                return false;
            }

            bool CCharacterizedSaliencySegment::ApetureEccentricityWeightingLeastSquaresLineFitting(const list<CSaliencyPixel*>& SaliencyPixels, Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, Mathematics::_2D::CVector2D& A, Mathematics::_2D::CVector2D& B, Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint, real& MainAxisLength, real& SecondaryAxisLength, real& MaximalDeviation, real& MeanDeviation)
            {
                const uint TotalPixels = SaliencyPixels.size();

                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN

                Mathematics::ND::LinearAlgebra::CMatrixND X(TotalPixels, 2);
                Mathematics::ND::LinearAlgebra::CMatrixND Y(TotalPixels, 1);
                Mathematics::ND::LinearAlgebra::CMatrixND W(TotalPixels, TotalPixels);
                W.setZero();
                uint Index = 0;
                Mathematics::_2D::CVector2D WeightedContinousLocationAccumulator;
                real WeightAccumulator = _REAL_ZERO_;
                list<CSaliencyPixel*>::const_iterator EndSaliencyPixels = SaliencyPixels.end();
                for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = SaliencyPixels.begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel, ++Index)
                {
                    const Mathematics::_2D::CVector2D& P = (*ppSaliencyPixel)->GetContinousLocation();
                    const real Weight = (*ppSaliencyPixel)->GetApetureEccentricity();
                    X(Index, 0) = P.GetX();
                    X(Index, 1) = _REAL_ONE_;
                    Y(Index, 0) = P.GetY();
                    W(Index, Index) = Weight;
                    WeightedContinousLocationAccumulator += P * Weight;
                    WeightAccumulator += Weight;
                }

                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN


                const Mathematics::ND::LinearAlgebra::CMatrixND XtW = X.transpose() * W;
                const Mathematics::ND::LinearAlgebra::CMatrixND Xs = XtW * X;
                const Mathematics::_2D::CVector2D Centroid = WeightedContinousLocationAccumulator / WeightAccumulator;
                if (Mathematics::_1D::IsZero(Xs.determinant()))
                {
                    return ManageSingularRegression(SaliencyPixels, Centroid, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, MaximalDeviation, MeanDeviation);
                }
                //              try
                //              {
                Mathematics::ND::LinearAlgebra::CMatrixND LSF = Xs.inverse() * (XtW * Y);
                DetermineAxes(LSF(0, 0), LSF(1, 0), Centroid, MainAxis, SecondaryAxis, Center);
                DetemineLine(SaliencyPixels, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, MaximalDeviation, MeanDeviation);
                return true;
                //              } catch (Mathematics::ND::LinearAlgebra::CMatrixBaseException& E)
                //              {
                //                  g_ErrorStringOutput << "BEGIN EVP::[" << __FILE__ << ":" << __LINE__ << "]" << g_EndLine;
                //                  g_ErrorStringOutput << E.what() << g_EndLine;
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("X", X);
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("W", W);
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("Y", Y);
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("Xs", Xs);
                //                  g_ErrorStringOutput << "END EVP::[" << __FILE__ << ":" << __LINE__ << "]" << g_EndLine;
                //              }
                //              return false;
            }

            bool CCharacterizedSaliencySegment::PowerEccentricityWeightingLeastSquaresLineFitting(const list<CSaliencyPixel*>& SaliencyPixels, Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, Mathematics::_2D::CVector2D& A, Mathematics::_2D::CVector2D& B, Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint, real& MainAxisLength, real& SecondaryAxisLength, real& MaximalDeviation, real& MeanDeviation, const real Power)
            {
                const uint TotalPixels = SaliencyPixels.size();

                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN


                Mathematics::ND::LinearAlgebra::CMatrixND X(TotalPixels, 2);
                Mathematics::ND::LinearAlgebra::CMatrixND Y(TotalPixels, 1);
                Mathematics::ND::LinearAlgebra::CMatrixND W(TotalPixels, TotalPixels);
                W.setZero();
                uint Index = 0;
                Mathematics::_2D::CVector2D WeightedContinousLocationAccumulator;
                real WeightAccumulator = _REAL_ZERO_;
                list<CSaliencyPixel*>::const_iterator EndSaliencyPixels = SaliencyPixels.end();
                for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = SaliencyPixels.begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel, ++Index)
                {
                    const Mathematics::_2D::CVector2D& P = (*ppSaliencyPixel)->GetContinousLocation();
                    const real Weight = (*ppSaliencyPixel)->GetPowerEccentricity(Power);
                    X(Index, 0) = P.GetX();
                    X(Index, 1) = _REAL_ONE_;
                    Y(Index, 0) = P.GetY();
                    W(Index, Index) = Weight;
                    WeightedContinousLocationAccumulator += P * Weight;
                    WeightAccumulator += Weight;
                }

                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN

                const Mathematics::ND::LinearAlgebra::CMatrixND XtW = X.transpose() * W;
                const Mathematics::ND::LinearAlgebra::CMatrixND Xs = XtW * X;
                const Mathematics::_2D::CVector2D Centroid = WeightedContinousLocationAccumulator / WeightAccumulator;
                if (Mathematics::_1D::IsZero(Xs.determinant()))
                {
                    return ManageSingularRegression(SaliencyPixels, Centroid, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, MaximalDeviation, MeanDeviation);
                }
                //              try
                //              {
                Mathematics::ND::LinearAlgebra::CMatrixND LSF = Xs.inverse() * (XtW * Y);
                DetermineAxes(LSF(0, 0), LSF(1, 0), Centroid, MainAxis, SecondaryAxis, Center);
                DetemineLine(SaliencyPixels, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, MaximalDeviation, MeanDeviation);
                return true;
                //              } catch (Mathematics::ND::LinearAlgebra::CMatrixBaseException& E)
                //              {
                //                  g_ErrorStringOutput << "BEGIN EVP::[" << __FILE__ << ":" << __LINE__ << "]" << g_EndLine;
                //                  g_ErrorStringOutput << E.what() << g_EndLine;
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("X", X);
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("W", W);
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("Y", Y);
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("Xs", Xs);
                //                  g_ErrorStringOutput << "END EVP::[" << __FILE__ << ":" << __LINE__ << "]" << g_EndLine;
                //              }
                return false;
            }

            bool CCharacterizedSaliencySegment::LinealEccentricityWeightingLeastSquaresLineFitting(const list<CSaliencyPixel*>& SaliencyPixels, Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, Mathematics::_2D::CVector2D& A, Mathematics::_2D::CVector2D& B, Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint, real& MainAxisLength, real& SecondaryAxisLength, real& MaximalDeviation, real& MeanDeviation)
            {
                const uint TotalPixels = SaliencyPixels.size();

                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN

                Mathematics::ND::LinearAlgebra::CMatrixND X(TotalPixels, 2);
                Mathematics::ND::LinearAlgebra::CMatrixND Y(TotalPixels, 1);
                Mathematics::ND::LinearAlgebra::CMatrixND W(TotalPixels, TotalPixels);
                W.setZero();
                uint Index = 0;
                Mathematics::_2D::CVector2D WeightedContinousLocationAccumulator;
                real WeightAccumulator = _REAL_ZERO_;
                list<CSaliencyPixel*>::const_iterator EndSaliencyPixels = SaliencyPixels.end();
                for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = SaliencyPixels.begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel, ++Index)
                {
                    const Mathematics::_2D::CVector2D& P = (*ppSaliencyPixel)->GetContinousLocation();
                    const real Weight = (*ppSaliencyPixel)->GetLinealEccentricity();
                    X(Index, 0) = P.GetX();
                    X(Index, 1) = _REAL_ONE_;
                    Y(Index, 0) = P.GetY();
                    W(Index, Index) = Weight;
                    WeightedContinousLocationAccumulator += P * Weight;
                    WeightAccumulator += Weight;
                }

                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN


                const Mathematics::ND::LinearAlgebra::CMatrixND XtW = X.transpose() * W;
                const Mathematics::ND::LinearAlgebra::CMatrixND Xs = XtW * X;
                const Mathematics::_2D::CVector2D Centroid = WeightedContinousLocationAccumulator / WeightAccumulator;
                if (Mathematics::_1D::IsZero(Xs.determinant()))
                {
                    return ManageSingularRegression(SaliencyPixels, Centroid, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, MaximalDeviation, MeanDeviation);
                }
                //              try
                //              {
                Mathematics::ND::LinearAlgebra::CMatrixND LSF = Xs.inverse() * (XtW * Y);
                DetermineAxes(LSF(0, 0), LSF(1, 0), Centroid, MainAxis, SecondaryAxis, Center);
                DetemineLine(SaliencyPixels, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, MaximalDeviation, MeanDeviation);
                return true;
                //              } catch (Mathematics::ND::LinearAlgebra::CMatrixBaseException& E)
                //              {
                //                  g_ErrorStringOutput << "BEGIN EVP::[" << __FILE__ << ":" << __LINE__ << "]" << g_EndLine;
                //                  g_ErrorStringOutput << E.what() << g_EndLine;
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("X", X);
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("W", W);
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("Y", Y);
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("Xs", Xs);
                //                  g_ErrorStringOutput << "END EVP::[" << __FILE__ << ":" << __LINE__ << "]" << g_EndLine;
                //              }
                return false;
            }

            bool CCharacterizedSaliencySegment::LeastSquaresLineFitting(const list<CSaliencyPixel*>& SaliencyPixels, Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, Mathematics::_2D::CVector2D& A, Mathematics::_2D::CVector2D& B, Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint, real& MainAxisLength, real& SecondaryAxisLength, real& MaximalDeviation, real& MeanDeviation)
            {
                const uint TotalPixels = SaliencyPixels.size();
                Mathematics::ND::LinearAlgebra::CMatrixND X(TotalPixels, 2);
                Mathematics::ND::LinearAlgebra::CMatrixND Y(TotalPixels, 1);
                uint Index = 0;
                Mathematics::_2D::CVector2D ContinousLocationAccumulator;
                list<CSaliencyPixel*>::const_iterator EndSaliencyPixels = SaliencyPixels.end();
                for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = SaliencyPixels.begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel, ++Index)
                {
                    const Mathematics::_2D::CVector2D& P = (*ppSaliencyPixel)->GetContinousLocation();
                    X(Index, 0) = P.GetX();
                    X(Index, 1) = _REAL_ONE_;
                    Y(Index, 0) = P.GetY();
                    ContinousLocationAccumulator += P;
                }
                const Mathematics::ND::LinearAlgebra::CMatrixND Xs = X.transpose() * X;
                const Mathematics::_2D::CVector2D Centroid = ContinousLocationAccumulator / real(TotalPixels);
                if (Mathematics::_1D::IsZero(Xs.determinant()))
                {
                    return ManageSingularRegression(SaliencyPixels, Centroid, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, MaximalDeviation, MeanDeviation);
                }
                //              try
                //              {
                const Mathematics::ND::LinearAlgebra::CMatrixND Xsi = Xs.inverse();
                const Mathematics::ND::LinearAlgebra::CMatrixND LSF = Xsi * (X.transpose() * Y);
                DetermineAxes(LSF(0, 0), LSF(1, 0), Centroid, MainAxis, SecondaryAxis, Center);
                DetemineLine(SaliencyPixels, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, MaximalDeviation, MeanDeviation);
                return true;
                //              } catch (Mathematics::ND::LinearAlgebra::CMatrixBaseException& E)
                //              {
                //                  g_ErrorStringOutput << "BEGIN EVP::[" << __FILE__ << ":" << __LINE__ << "]" << g_EndLine;
                //                  g_ErrorStringOutput << E.what() << g_EndLine;
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("X", X);
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("Y", Y);
                //                  Mathematics::ND::LinearAlgebra::DisplayMatrix("Xs", Xs);
                //                  g_ErrorStringOutput << "END EVP::[" << __FILE__ << ":" << __LINE__ << "]" << g_EndLine;
                //              }
                return false;
            }

            bool CCharacterizedSaliencySegment::ManageSingularRegression(const list<CSaliencyPixel*>& SaliencyPixels, const Mathematics::_2D::CVector2D& Centroid, Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, Mathematics::_2D::CVector2D& A, Mathematics::_2D::CVector2D& B, Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint, real& MainAxisLength, real& SecondaryAxisLength, real& MaximalDeviation, real& MeanDeviation)
            {
                real Cxx = _REAL_ZERO_, Cyy = _REAL_ZERO_;
                list<CSaliencyPixel*>::const_iterator EndSaliencyPixels = SaliencyPixels.end();
                for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = SaliencyPixels.begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel)
                {
                    const Mathematics::_2D::CVector2D& P = (*ppSaliencyPixel)->GetContinousLocation();
                    Centroid.AddPartialCovariance(P, Cxx, Cyy);
                }
                const bool VericalSingular = Mathematics::_1D::IsZero(Cxx / real(SaliencyPixels.size()));
                const bool HorizontalSingular = Mathematics::_1D::IsZero(Cyy / real(SaliencyPixels.size()));
                if (VericalSingular || HorizontalSingular)
                {
                    if (VericalSingular && HorizontalSingular)
                    {
                        return false;
                    }
                    if (VericalSingular)
                    {
                        MainAxis = Mathematics::_2D::CVector2D::s_Direction_Axis_Y_2D;
                        SecondaryAxis = Mathematics::_2D::CVector2D::s_Direction_Axis_X_2D;
                        Center = Centroid;
                        DetemineLine(SaliencyPixels, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, MaximalDeviation, MeanDeviation);
                    }
                    else
                    {
                        MainAxis = Mathematics::_2D::CVector2D::s_Direction_Axis_X_2D;
                        SecondaryAxis = Mathematics::_2D::CVector2D::s_Direction_Axis_Y_2D;
                        Center = Centroid;
                        DetemineLine(SaliencyPixels, MainAxis, SecondaryAxis, Center, A, B, MinPoint, MaxPoint, MainAxisLength, SecondaryAxisLength, MaximalDeviation, MeanDeviation);
                    }
                    return true;
                }
                return false;
            }

            void CCharacterizedSaliencySegment::DetermineAxes(const real Slope, const real Offset, const Mathematics::_2D::CVector2D& Centroid, Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center)
            {
                const real Alpha = RealAtan(Slope);
                const real Dx = RealCosinus(Alpha);
                const real Dy = RealSinus(Alpha);
                MainAxis = Mathematics::_2D::CVector2D::CreateCannonicalForm(Dx, Dy);
                SecondaryAxis = Mathematics::_2D::CVector2D::CreateCannonicalForm(-Dy, Dx);
                Center = Mathematics::_1D::IsZero(Slope) ? Centroid : (Mathematics::_2D::CVector2D(Centroid.GetX(), Centroid.GetX() * Slope + Offset) + Mathematics::_2D::CVector2D((Centroid.GetY() - Offset) / Slope, Centroid.GetY())) * _REAL_HALF_;
            }

            void CCharacterizedSaliencySegment::DetemineLine(const list<CSaliencyPixel*>& SaliencyPixels, const Mathematics::_2D::CVector2D& MainAxis, const Mathematics::_2D::CVector2D& SecondaryAxis, const Mathematics::_2D::CVector2D& Center, Mathematics::_2D::CVector2D& A, Mathematics::_2D::CVector2D& B, Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint, real& MainAxisLength, real& SecondaryAxisLength, real& MaximalDeviation, real& MeanDeviation)
            {
                real E1n = _REAL_ZERO_, E1p = _REAL_ZERO_, E0n = _REAL_ZERO_, E0p = _REAL_ZERO_;
                MaximalDeviation = MeanDeviation = _REAL_ZERO_;
                list<CSaliencyPixel*>::const_iterator EndSaliencyPixels = SaliencyPixels.end();
                for (list<CSaliencyPixel*>::const_iterator ppSaliencyPixel = SaliencyPixels.begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel)
                {
                    const CSaliencyPixel* pSaliencyPixel = *ppSaliencyPixel;
                    const Mathematics::_2D::CVector2D Delta = pSaliencyPixel->GetContinousLocation() - Center;
                    const real E0 = MainAxis.ScalarProduct(Delta);
                    const real E1 = SecondaryAxis.ScalarProduct(Delta);
                    if (E0 > E0p)
                    {
                        E0p = E0;
                    }
                    else if (E0 < E0n)
                    {
                        E0n = E0;
                    }
                    if (E1 > E1p)
                    {
                        E1p = E1;
                    }
                    else if (E1 < E1n)
                    {
                        E1n = E1;
                    }
                    const real Deviation = RealAbs(E1);
                    MeanDeviation += Deviation;
                    if (Deviation > MaximalDeviation)
                    {
                        MaximalDeviation = Deviation;
                    }
                }
                A = Center + MainAxis * E0n;
                B = Center + MainAxis * E0p;
                MainAxisLength = E0p - E0n;
                SecondaryAxisLength = E1p - E1n;
                MinPoint.SetValue(E0n, E1n);
                MaxPoint.SetValue(E0p, E1p);
                MeanDeviation /= real(SaliencyPixels.size());
            }

            bool CCharacterizedSaliencySegment::SortPixelsByEccentricityDescent(const CSaliencyPixel* plhs, const CSaliencyPixel* prhs)
            {
                return plhs->GetLinealEccentricity() > prhs->GetLinealEccentricity();
            }
        }
    }
}
