/*
 * MonocularSaliencyExtractor.h
 *
 *  Created on: 03.12.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../Base/ImageProcessBase.h"
#include "SaliencyPixel.h"
#include "SaliencyBlock.h"
#include "SaliencyPixelSetAnalyzer.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                class CMonocularSaliencyExtractor: public TImageProcessBase<real, Saliency::CSaliencyPixel, CDiscreteTristimulusPixel>
                {
                public:

                    class CParameters: public CParametersBase
                    {
                    public:

                        enum DisplayChannel
                        {
                            eSaliencyMagnitude, eSaliencyPhase, eSelectedPixels, eBlocksMagnitude, eBlockConnectivity, eBlockDiameter, eBlockRooting, eBlocksEccentricity
                        };

                        enum DisplayMode
                        {
                            eScaledIntensity, eColorMapped
                        };

                        CParameters(CMonocularSaliencyExtractor* pHost) :
                            CParametersBase(), m_pHost(pHost), m_UseConfidenceMap(false), m_pColorMap(NULL), m_ColorMapOwnerShip(true), m_DisplayChannel(eSaliencyMagnitude), m_DisplayMode(eColorMapped)
                        {
                            m_pColorMap = Visualization::CTristimulusColorMap::LoadPredefinedColorMap(Visualization::CTristimulusColorMap::eRGB_Hot);
                        }

                        ~CParameters() override;

                        CParametersBase::ParameterChangeResult SetUseConfidenceMap(const bool UseConfidence, const bool Wait = true);
                        bool GetUseConfidenceMap() const;

                        CParametersBase::ParameterChangeResult SetDisplayChannel(const DisplayChannel Channel, const bool Wait = true);
                        DisplayChannel GetDisplayChannel() const;

                        CParametersBase::ParameterChangeResult SetDisplayMode(const DisplayMode Mode, const bool Wait = true);
                        DisplayMode GetDisplayMode() const;

                        CParametersBase::ParameterChangeResult SetColorMap(const Visualization::CTristimulusColorMap* pColorMap, const bool OwnerShip, const bool Wait = true);
                        CParametersBase::ParameterChangeResult SetColorMap(const Visualization::CTristimulusColorMap::PredefinedColorMap ColorMap, const bool Wait = true);
                        const Visualization::CTristimulusColorMap* GetColorMap() const;

                    protected:

                        CMonocularSaliencyExtractor* m_pHost;
                        bool m_UseConfidenceMap;
                        const Visualization::CTristimulusColorMap* m_pColorMap;
                        bool m_ColorMapOwnerShip;
                        DisplayChannel m_DisplayChannel;
                        DisplayMode m_DisplayMode;
                    };

                    CMonocularSaliencyExtractor(const CImageProcessBase::ProcessTypeId TypeId, CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, Threading::CMutex* pInputImageMutex);
                    CMonocularSaliencyExtractor(const CImageProcessBase::ProcessTypeId TypeId, CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, const TImage<bool>* pInputMaskImage, Threading::CMutex* pInputImageMutex);
                    ~CMonocularSaliencyExtractor() override;

                    bool Display() override;
                    CParameters* GetParameters() override;

                    bool SetConfidenceImage(const TImage<real>* pConfidenceImage);
                    const TImage<real>* GetConfidenceImage() const;
                    const list<Saliency::CSaliencyBlock*>& GetSaliencyBlocks() const;
                    const Saliency::CSaliencyPixelSetAnalyzer& GetSaliencyPixelSetAnalyzer() const;
                    const Saliency::CSaliencyPixelSetAnalyzer& GetSelectedSaliencyPixelSetAnalyzer() const;
                    bool ExportBlockPixels(const_string pPathFileName, const CImageActiveZone* pImageActiveZone) const;
                    bool ExportBlocks(const_string pPathFileName, const CImageActiveZone* pImageActiveZone) const;

                protected:

                    void LoadSaliencyPixelSetAnalyzer();
                    virtual  bool Extract(const Identifier TrialId) = 0;

                    static  bool SortSaliencyBlockBySizeAscent(const Saliency::CSaliencyBlock* plhs, const Saliency::CSaliencyBlock* prhs);

                    bool DisplaySaliencyMagnitude();
                    bool DisplaySaliencyPhase();
                    bool DisplaySelectedPixels();
                    bool DisplayBlocksMagnitude();
                    bool DisplayBlocksEccentricity();
                    bool DisplayBlocksConnectivity();
                    bool DisplayBlocksDiameter();
                    bool DisplayBlocksRooting();

                    CParameters* m_pParameters;
                    const TImage<bool>* m_pInputMaskImage;
                    const TImage<real>* m_pConfidenceImage;
                    list<Saliency::CSaliencyBlock*> m_SelectedSaliencyBlocks;
                    list<Saliency::CSaliencyBlock*> m_ResidualSaliencyBlocks;
                    Saliency::CSaliencyPixelSetAnalyzer m_SaliencyPixelSetAnalyzer;
                    Saliency::CSaliencyPixelSetAnalyzer m_SelectedSaliencyPixelSetAnalyzer;
                };
            }
        }
    }
}

