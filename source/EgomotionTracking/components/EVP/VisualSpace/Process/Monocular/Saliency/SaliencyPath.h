/*
 * SaliencyPath.h
 *
 *  Created on: 13.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/Miscellaneous/IdentifiableInstance.h"
#include "../../../../Foundation/Mathematics/1D/BicubicLookUpTable.h"
#include "../../../../VisualSpace/Common/Image.h"
#include "../../../Common/ContinuousBoundingBox2D.h"
#include "SaliencyPoint.h"
#include "SaliencySegment.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            class CSaliencyPixel;
            class CSaliencyNode;
            class CSaliencyTree;

            class CSaliencyPath: public CIdentifiableInstance
            {
                IDENTIFIABLE

            public:

                CSaliencyPath(const CSaliencyNode* pPixelNode, const CSaliencyTree* pPixelTree);
                ~CSaliencyPath() override;

                void OutSpreading(const real RegularLengthDelta);
                void Smoothing(const real* pSmoothingKernel, const uint SmoothingRadius, const uint SmoothingDiameter);
                void SubPixelOptimization(TImage<CSaliencyPixel>* pSaliencyImage, real Scope, int Iterations, bool Refinement);
                void Characterization(const uint MaximalPathDistance, const real ThresholdTransitional, const real ThresholdElongated, const CSaliencyPoint::CharacterizationMode Mode, const real Power, const bool UseSmoothLocations);
                const list<CSaliencySegment*>& Segmentation();
                void ExtractLineSegments(real LineSegmentUncertaintyThreshold, real LineSegmentMinimalMainAxisLength, real LineSegmentMaximalSecondaryAxisLength, int LineSegmentMinimalPointNodes);

                inline const vector<const CSaliencyNode*>& GetNodes() const
                {
                    return m_SaliencyNodes;
                }

                inline const vector<CSaliencyPoint*>& GetPoints() const
                {
                    return m_SaliencyPoints;
                }

                inline const list<CSaliencySegment*>& GetSegments() const
                {
                    return m_SaliencySegments;
                }

                inline const CSaliencyTree* GetTree() const
                {
                    return m_pPixelTree;
                }

                inline  uint GetLength() const
                {
                    return m_SaliencyNodes.size();
                }

                inline Identifier GetInstanceId() const
                {
                    return m_InstanceId;
                }

                inline const CContinuousBoundingBox2D& GetBoundingBox() const
                {
                    return m_BoundingBox;
                }

                bool IsGeometricSalient(real MinimalLength);

                inline const CSaliencyNode* GetBottomNode() const
                {
                    return m_SaliencyNodes.front();
                }

                inline const CSaliencyNode* GetTopNode() const
                {
                    return m_SaliencyNodes.back();
                }

            protected:

                static bool SortSaliencySegmentByLengthDescending(const CSaliencySegment* plhs, const CSaliencySegment* prhs);
                void IndirectOptimizeLocation(CSaliencyPoint* pSaliencyPoint, TImage<CSaliencyPixel>* pSaliencyImage, real Scope, int Iterations, real* pSampleMagnitudes, Mathematics::_2D::CVector2D* pSampleLocations);
                void DirectOptimizeLocation(CSaliencyPoint* pSaliencyPoint, TImage<CSaliencyPixel>* pSaliencyImage, real Scope, int Iterations);

                const CSaliencyTree* m_pPixelTree;
                vector<const CSaliencyNode*> m_SaliencyNodes;
                vector<CSaliencyPoint*> m_SaliencyPoints;
                list<CSaliencySegment*> m_SaliencySegments;
                CContinuousBoundingBox2D m_BoundingBox;
            };
        }
    }
}

