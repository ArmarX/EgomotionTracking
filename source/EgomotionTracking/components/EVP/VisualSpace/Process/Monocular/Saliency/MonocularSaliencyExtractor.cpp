/*
 * MonocularSaliencyExtractor.cpp
 *
 *  Created on: 03.12.2011
 *      Author: gonzalez
 */

#include "../../../../Visualization/Geometry/2D/SVG/SVGGraphic.h"
#include "../../../../Foundation/Mathematics/2D/Geometry/Line2D.h"
#include "MonocularSaliencyExtractor.h"
#include "CharacterizedSaliencySegment.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularSaliencyExtractor::CParameters
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                CMonocularSaliencyExtractor::CParameters::~CParameters()
                {
                    RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                }

                CParametersBase::ParameterChangeResult CMonocularSaliencyExtractor::CParameters::SetUseConfidenceMap(const bool UseConfidenceMap, const bool Wait)
                {
                    if (UseConfidenceMap == m_UseConfidenceMap)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_UseConfidenceMap = UseConfidenceMap;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                bool CMonocularSaliencyExtractor::CParameters::GetUseConfidenceMap() const
                {
                    return m_UseConfidenceMap;
                }

                CParametersBase::ParameterChangeResult CMonocularSaliencyExtractor::CParameters::SetDisplayChannel(const DisplayChannel Channel, const bool Wait)
                {
                    if (!((Channel == CParameters::eSaliencyMagnitude) || (Channel == CParameters::eSaliencyPhase) || (Channel == CParameters::eSelectedPixels) || (Channel == CParameters::eBlocksMagnitude) || (Channel == CParameters::eBlockConnectivity) || (Channel == CParameters::eBlockDiameter) || (Channel == CParameters::eBlockRooting) || (Channel == CParameters::eBlocksEccentricity)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (Channel == m_DisplayChannel)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_DisplayChannel = Channel;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                CMonocularSaliencyExtractor::CParameters::DisplayChannel CMonocularSaliencyExtractor::CParameters::GetDisplayChannel() const
                {
                    return m_DisplayChannel;
                }

                CParametersBase::ParameterChangeResult CMonocularSaliencyExtractor::CParameters::SetDisplayMode(const DisplayMode Mode, const bool Wait)
                {
                    if (!((Mode == CParameters::eScaledIntensity) || (Mode == CParameters::eColorMapped)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (Mode == m_DisplayMode)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_DisplayMode = Mode;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                CMonocularSaliencyExtractor::CParameters::DisplayMode CMonocularSaliencyExtractor::CParameters::GetDisplayMode() const
                {
                    return m_DisplayMode;
                }

                CParametersBase::ParameterChangeResult CMonocularSaliencyExtractor::CParameters::SetColorMap(const Visualization::CTristimulusColorMap* pColorMap, const bool OwnerShip, const bool Wait)
                {
                    if (!pColorMap)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (pColorMap == m_pColorMap)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                        m_ColorMapOwnerShip = OwnerShip;
                        m_pColorMap = pColorMap;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                CParametersBase::ParameterChangeResult CMonocularSaliencyExtractor::CParameters::SetColorMap(const Visualization::CTristimulusColorMap::PredefinedColorMap ColorMap, const bool Wait)
                {
                    if (!Visualization::CTristimulusColorMap::IsValidPredefinedColorMap(ColorMap))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                        m_ColorMapOwnerShip = true;
                        m_pColorMap = Visualization::CTristimulusColorMap::LoadPredefinedColorMap(ColorMap);
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                const Visualization::CTristimulusColorMap* CMonocularSaliencyExtractor::CParameters::GetColorMap() const
                {
                    return m_pColorMap;
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularSaliencyExtractor
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                CMonocularSaliencyExtractor::CMonocularSaliencyExtractor(const CImageProcessBase::ProcessTypeId TypeId, CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, Threading::CMutex* pInputImageMutex) :
                    TImageProcessBase<real, Saliency::CSaliencyPixel, CDiscreteTristimulusPixel>(TypeId, pActiveZone, pInputImage, pInputImageMutex), m_pParameters(nullptr), m_pInputMaskImage(nullptr), m_pConfidenceImage(nullptr)
                {
                    if (m_IsEnabled)
                    {
                        const CImageSize& Size = m_pInputActiveZone->GetSize();
                        m_pOutputImage = new TImage<Saliency::CSaliencyPixel>(Size);
                        m_pDisplayImage = new TImage<CDiscreteTristimulusPixel>(Size);
                        m_pInternalInputImage = new TImage<real>(Size);
                        Saliency::CSaliencyPixel* pSaliencyPixel = m_pOutputImage->GetBeginWritableBuffer();
                        const coordinate Width = Size.GetWidth();
                        const coordinate Height = Size.GetHeight();
                        for (CPixelLocation Location; Location.m_y < Height; ++Location.m_y)
                            for (Location.m_x = 0; Location.m_x < Width; ++Location.m_x, ++pSaliencyPixel)
                            {
                                pSaliencyPixel->Initialize(m_pInputActiveZone->IsDiscreteLocationInside(Location, true), Location);
                            }
                    }
                }

                CMonocularSaliencyExtractor::CMonocularSaliencyExtractor(const CImageProcessBase::ProcessTypeId TypeId, CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, const TImage<bool>* pInputMaskImage, Threading::CMutex* pInputImageMutex) :
                    TImageProcessBase<real, Saliency::CSaliencyPixel, CDiscreteTristimulusPixel>(TypeId, pActiveZone, pInputImage, pInputImageMutex), m_pParameters(nullptr), m_pInputMaskImage(pInputMaskImage), m_pConfidenceImage(nullptr)
                {
                    const CImageSize& Size = m_pInputActiveZone->GetSize();
                    m_IsEnabled &= m_pInputMaskImage && (m_pInputMaskImage->GetSize() == Size);
                    if (m_IsEnabled)
                    {
                        m_pOutputImage = new TImage<Saliency::CSaliencyPixel>(Size);
                        m_pDisplayImage = new TImage<CDiscreteTristimulusPixel>(Size);
                        m_pInternalInputImage = new TImage<real>(Size);
                        Saliency::CSaliencyPixel* pSaliencyPixel = m_pOutputImage->GetBeginWritableBuffer();
                        const coordinate Width = Size.GetWidth();
                        const coordinate Height = Size.GetHeight();
                        const bool* pMaskPixel = pInputMaskImage->GetBeginReadOnlyBuffer();
                        for (CPixelLocation Location; Location.m_y < Height; ++Location.m_y)
                            for (Location.m_x = 0; Location.m_x < Width; ++Location.m_x, ++pSaliencyPixel, ++pMaskPixel)
                            {
                                pSaliencyPixel->Initialize((*pMaskPixel) && m_pInputActiveZone->IsDiscreteLocationInside(Location, true), Location);
                            }
                    }
                }

                CMonocularSaliencyExtractor::~CMonocularSaliencyExtractor()
                {
                    if (m_IsEnabled)
                    {
                        m_OutputActiveZone.Disconnect(true, true);
                        RELEASE_OBJECT(m_pOutputImage)
                        RELEASE_OBJECT(m_pDisplayImage)
                        RELEASE_OBJECT(m_pInternalInputImage)
                    }
                }

                bool CMonocularSaliencyExtractor::Display()
                {
                    if (m_IsEnabled && m_OutputActiveZone.IsEnabled() && GetTotalExecutions())
                    {
                        if (StartDisplaying())
                        {
                            bool Result = false;
                            switch (m_pParameters->GetDisplayChannel())
                            {
                                case CParameters::eSaliencyMagnitude:
                                    Result = DisplaySaliencyMagnitude();
                                    break;
                                case CParameters::eSaliencyPhase:
                                    Result = DisplaySaliencyPhase();
                                    break;
                                case CParameters::eSelectedPixels:
                                    m_pDisplayImage->Clear();
                                    Result = DisplaySelectedPixels();
                                    break;
                                case CParameters::eBlocksMagnitude:
                                    m_pDisplayImage->Clear();
                                    Result = DisplayBlocksMagnitude();
                                    break;
                                case CParameters::eBlockConnectivity:
                                    m_pDisplayImage->Clear();
                                    Result = DisplayBlocksConnectivity();
                                    break;
                                case CParameters::eBlockDiameter:
                                    m_pDisplayImage->Clear();
                                    Result = DisplayBlocksDiameter();
                                    break;
                                case CParameters::eBlockRooting:
                                    m_pDisplayImage->Clear();
                                    Result = DisplayBlocksRooting();
                                    break;
                                case CParameters::eBlocksEccentricity:
                                    m_pDisplayImage->Clear();
                                    Result = DisplayBlocksEccentricity();
                                    break;
                            }
                            return FinishDisplaying() && Result;
                        }
                    }
                    return false;
                }

                CMonocularSaliencyExtractor::CParameters* CMonocularSaliencyExtractor::GetParameters()
                {
                    return m_IsEnabled ? m_pParameters : nullptr;
                }

                bool CMonocularSaliencyExtractor::SetConfidenceImage(const TImage<real>* pConfidenceImage)
                {
                    if (pConfidenceImage && pConfidenceImage->IsValid() && (m_pConfidenceImage != pConfidenceImage) && (pConfidenceImage->GetSize() == m_OutputActiveZone.GetSize()))
                    {
                        m_pConfidenceImage = pConfidenceImage;
                        return true;
                    }
                    return false;
                }

                const TImage<real>* CMonocularSaliencyExtractor::GetConfidenceImage() const
                {
                    return m_pConfidenceImage;
                }

                const list<Saliency::CSaliencyBlock*>& CMonocularSaliencyExtractor::GetSaliencyBlocks() const
                {
                    return m_SelectedSaliencyBlocks;
                }

                const Saliency::CSaliencyPixelSetAnalyzer& CMonocularSaliencyExtractor::GetSaliencyPixelSetAnalyzer() const
                {
                    return m_SaliencyPixelSetAnalyzer;
                }

                const Saliency::CSaliencyPixelSetAnalyzer& CMonocularSaliencyExtractor::GetSelectedSaliencyPixelSetAnalyzer() const
                {
                    return m_SelectedSaliencyPixelSetAnalyzer;
                }

                bool CMonocularSaliencyExtractor::ExportBlockPixels(const_string pPathFileName, const CImageActiveZone* pImageActiveZone) const
                {
                    if (pPathFileName && m_SelectedSaliencyPixelSetAnalyzer.GetTotalPixels())
                    {
                        Displaying::SVG::CSVGGraphic Plotter(m_OutputActiveZone.GetWidth(), m_OutputActiveZone.GetHeight());
                        Displaying::SVG::CSVGGroup* pGroupPixelsConnectivity = Plotter.CreateGroup();
                        Displaying::SVG::CSVGGroup* pGroupOptimizations = Plotter.CreateGroup();
                        Displaying::SVG::CSVGGroup* pGroupPixels = Plotter.CreateGroup();
                        Displaying::SVG::CSVGGroup* pGroupNodes = Plotter.CreateGroup();
                        Displaying::SVG::CSVGGroup* pGroupNodesConnectivity = Plotter.CreateGroup();
                        const CRGBAColor PixelColors[5] = { CRGBAColor(255, 255, 0, 255), CRGBAColor(255, 0, 0, 255), CRGBAColor(0, 255, 0, 255), CRGBAColor(0, 0, 255, 255), CRGBAColor(255, 0, 255, 255) };
                        const CRGBAColor ConnectionPixelColor(128, 128, 128, 255);
                        const CRGBAColor ConnectionOptimizationColor(64, 64, 64, 255);
                        const CRGBAColor ConnectionNodesColor(0, 0, 0, 255);
                        const real PixelRadius = real(0.2);
                        const real NodeRadius = PixelRadius * _REAL_FOURTH_;
                        const real ConnectionPixelStrockeWidth = PixelRadius / real(4.0);
                        const real ConnectionOptimizationStrockeWidth = PixelRadius / real(16.0);
                        list<Saliency::CSaliencyPixel*>::const_iterator EndSaliencyPixel = m_SelectedSaliencyPixelSetAnalyzer.GetPixels().end();
                        if (pImageActiveZone)
                            for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SelectedSaliencyPixelSetAnalyzer.GetPixels().begin(); ppSaliencyPixel != EndSaliencyPixel; ++ppSaliencyPixel)
                            {
                                const Saliency::CSaliencyPixel* pSaliencyPixel = *ppSaliencyPixel;
                                if (pSaliencyPixel->GetBlock() && pSaliencyPixel->GetBlock()->IsSelected())
                                {
                                    const CPixelLocation& DiscreteLocation = pSaliencyPixel->GetDiscreteLocation();
                                    if (pImageActiveZone->IsDiscreteLocationInside(DiscreteLocation, true))
                                    {
                                        const Mathematics::_2D::CVector2D& ContinousLocation = pSaliencyPixel->GetContinousLocation();
                                        const CRGBAColor& NodeColor = PixelColors[pSaliencyPixel->GetCharacterizationType()];

                                        pGroupPixels->Add(new Displaying::SVG::CSVGRectangle(DiscreteLocation, PixelRadius, NodeColor, NodeColor, _REAL_ZERO_, Displaying::SVG::CSVGPrimitive::eOnlyFill));

                                        pGroupPixels->Add(new Displaying::SVG::CSVGText(realValueToString(pSaliencyPixel->GetLinealEccentricity()), DiscreteLocation, 0.25, ConnectionNodesColor));

                                        pGroupOptimizations->Add(new Displaying::SVG::CSVGLine(DiscreteLocation, ContinousLocation, ConnectionOptimizationStrockeWidth, ConnectionOptimizationColor));
                                        pGroupNodes->Add(new Displaying::SVG::CSVGCircle(ContinousLocation, NodeRadius, _REAL_ZERO_, NodeColor, NodeColor, Displaying::SVG::CSVGPrimitive::eOnlyFill));

                                        list<Saliency::CSaliencyPixel*>::const_iterator EndConnectedPixels = pSaliencyPixel->GetConnectedPixels().end();
                                        for (list<Saliency::CSaliencyPixel*>::const_iterator ppConnectedSaliencyPixel = pSaliencyPixel->GetConnectedPixels().begin(); ppConnectedSaliencyPixel != EndConnectedPixels; ++ppConnectedSaliencyPixel)
                                            if (pSaliencyPixel > (*ppConnectedSaliencyPixel))
                                            {
                                                pGroupPixelsConnectivity->Add(new Displaying::SVG::CSVGLine(DiscreteLocation, (*ppConnectedSaliencyPixel)->GetDiscreteLocation(), ConnectionPixelStrockeWidth, ConnectionPixelColor));
                                                if (ContinousLocation.GetSquareDistance((*ppConnectedSaliencyPixel)->GetContinousLocation()) < _REAL_TWO_)
                                                {
                                                    pGroupNodesConnectivity->Add(new Displaying::SVG::CSVGLine(ContinousLocation, (*ppConnectedSaliencyPixel)->GetContinousLocation(), ConnectionOptimizationStrockeWidth, ConnectionNodesColor));
                                                }
                                            }
                                    }
                                }
                            }
                        return Plotter.SaveFile(Displaying::SVG::CSVGPrimitive::eSVG, pPathFileName);
                    }
                    return false;
                }

                bool CMonocularSaliencyExtractor::ExportBlocks(const_string pPathFileName, const CImageActiveZone* pImageActiveZone) const
                {
                    if (pPathFileName && m_SelectedSaliencyBlocks.size())
                    {
                        Displaying::SVG::CSVGGraphic Plotter(m_OutputActiveZone.GetWidth(), m_OutputActiveZone.GetHeight());
                        const CRGBAColor PixelColors[5] = { CRGBAColor(255, 255, 0, 255), CRGBAColor(255, 0, 0, 255), CRGBAColor(0, 255, 0, 255), CRGBAColor(0, 0, 255, 255), CRGBAColor(255, 0, 255, 255) };
                        const CRGBAColor ConnectionPixelColor(128, 128, 128, 255);
                        const CRGBAColor ConnectionOptimizationColor(64, 64, 64, 255);
                        const CRGBAColor EigenBoundingBoxColor(128, 128, 128, 255);
                        const CRGBAColor ConnectionNodesColor(0, 0, 0, 255);
                        const CRGBAColor LineColor(0, 0, 0, 128);
                        const CRGBAColor EndPointColor(255, 0, 0, 196);
                        const CRGBAColor CollinearityColor(0, 255, 255, 255);
                        const real LineStrockeWidth = real(0.125);
                        const real PixelRadius = real(0.2);
                        const real EndPointRadius = PixelRadius * real(2.0);
                        const real NodeRadius = PixelRadius * _REAL_FOURTH_;
                        //const real ConnectionPixelStrockeWidth = PixelRadius * _REAL_FOURTH_;
                        const real ConnectionOptimizationStrockeWidth = PixelRadius / real(16.0);
                        const real EigenBoundingBoxStrockeWidth = PixelRadius / real(12.0);
                        const real EndPointStrockeWidth = PixelRadius;
                        //const real CollinearityStrockeWidth = PixelRadius / real(2.0);
                        list<Saliency::CSaliencyBlock*>::const_iterator EndSelectedSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                        for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSelectedSaliencyBlocks; ++ppSaliencyBlock)
                        {
                            if (pImageActiveZone && (!pImageActiveZone->IsFullyInside((*ppSaliencyBlock)->GetBoundingBox())))
                            {
                                continue;
                            }
                            Saliency::CSaliencyBlock* pSaliencyBlock = *ppSaliencyBlock;
                            Displaying::SVG::CSVGGroup* pGroupBlock = Plotter.CreateGroup();
                            //Displaying::SVG::CSVGGroup* pGroupPixelsConnectivity = pGroupBlock->CreateGroup();
                            Displaying::SVG::CSVGGroup* pGroupEigneBoxes = pGroupBlock->CreateGroup();
                            Displaying::SVG::CSVGGroup* pGroupOptimizations = pGroupBlock->CreateGroup();
                            Displaying::SVG::CSVGGroup* pGroupPixels = pGroupBlock->CreateGroup();
                            Displaying::SVG::CSVGGroup* pGroupNodes = pGroupBlock->CreateGroup();
                            Displaying::SVG::CSVGGroup* pGroupNodesConnectivity = pGroupBlock->CreateGroup();
                            Displaying::SVG::CSVGGroup* pGroupGeometricPrimitives = pGroupBlock->CreateGroup();
                            list<Saliency::CCharacterizedSaliencySegment*>::const_iterator EndCharacterizedSaliencySegments = pSaliencyBlock->GetCharacterizedSaliencySegments().end();
                            for (list<Saliency::CCharacterizedSaliencySegment*>::const_iterator ppCharacterizedSaliencySegment = pSaliencyBlock->GetCharacterizedSaliencySegments().begin(); ppCharacterizedSaliencySegment != EndCharacterizedSaliencySegments; ++ppCharacterizedSaliencySegment)
                                if ((*ppCharacterizedSaliencySegment)->GetCharacterizationType() != Saliency::CCharacterizedSaliencySegment::eSingular)
                                {
                                    if (false)
                                        if ((*ppCharacterizedSaliencySegment)->GetCharacterizationType() != Saliency::CCharacterizedSaliencySegment::eElongated)
                                        {
                                            const vector<Mathematics::_2D::CVector2D> EigenBoundingBoxPoints = (*ppCharacterizedSaliencySegment)->GetEigenBoundingBoxPoints(true);
                                            for (uint i = 0; i < 4; ++i)
                                            {
                                                pGroupEigneBoxes->Add(new Displaying::SVG::CSVGLine(EigenBoundingBoxPoints[i], EigenBoundingBoxPoints[(i + 1) % 4], EigenBoundingBoxStrockeWidth, EigenBoundingBoxColor));
                                            }
                                            pGroupEigneBoxes->Add(new Displaying::SVG::CSVGLine(EigenBoundingBoxPoints[4], EigenBoundingBoxPoints[5], EigenBoundingBoxStrockeWidth, EigenBoundingBoxColor));
                                            pGroupEigneBoxes->Add(new Displaying::SVG::CSVGLine(EigenBoundingBoxPoints[6], EigenBoundingBoxPoints[7], EigenBoundingBoxStrockeWidth, EigenBoundingBoxColor));
                                        }
                                    list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>::const_iterator EndGeometricPrimitives = (*ppCharacterizedSaliencySegment)->GetGeometricPrimitives().end();
                                    for (list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>::const_iterator ppGeometricPrimitive = (*ppCharacterizedSaliencySegment)->GetGeometricPrimitives().begin(); ppGeometricPrimitive != EndGeometricPrimitives; ++ppGeometricPrimitive)
                                        if ((*ppGeometricPrimitive)->GetGeometricPrimitiveTypeId() == Mathematics::_2D::Geometry::CGeometricPrimitive2D::eLine)
                                        {
                                            Mathematics::_2D::Geometry::CLine2D* pLine = dynamic_cast<Mathematics::_2D::Geometry::CLine2D*>(*ppGeometricPrimitive);
                                            if (pLine->HasSourcePoints())
                                            {
                                                pGroupGeometricPrimitives->Add(new Displaying::SVG::CSVGLine(pLine->GetSourcePointA(), pLine->GetSourcePointB(), LineStrockeWidth, LineColor));
                                                pGroupGeometricPrimitives->Add(new Displaying::SVG::CSVGCircle(pLine->GetSourcePointA(), EndPointRadius, EndPointStrockeWidth, EndPointColor, EndPointColor, Displaying::SVG::CSVGPrimitive::eOnlyStroke));
                                                pGroupGeometricPrimitives->Add(new Displaying::SVG::CSVGCircle(pLine->GetSourcePointB(), EndPointRadius, EndPointStrockeWidth, EndPointColor, EndPointColor, Displaying::SVG::CSVGPrimitive::eOnlyStroke));
                                            }
                                        }

                                    /*--------------------------------------------------------------------
                                     if ((*ppCharacterizedSaliencySegment)->m_CollinearityCandidates.size())
                                     {
                                     list<Saliency::CCharacterizedSaliencySegment::CRegressionLineSegmentCollinearity2D>::iterator EndCollinearityCandidates = (*ppCharacterizedSaliencySegment)->m_CollinearityCandidates.end();
                                     for (list<Saliency::CCharacterizedSaliencySegment::CRegressionLineSegmentCollinearity2D>::iterator ppRegressionLineSegmentCollinearity = (*ppCharacterizedSaliencySegment)->m_CollinearityCandidates.begin(); ppRegressionLineSegmentCollinearity != EndCollinearityCandidates; ++ppRegressionLineSegmentCollinearity)
                                     {
                                     Saliency::CCharacterizedSaliencySegment::CRegressionLineSegmentCollinearity2D& Collinearity = (*ppRegressionLineSegmentCollinearity);
                                     const real Radius = RealAbs(Collinearity.m_EndPointSignedDistance) * _REAL_HALF_;

                                     if (Collinearity.m_Active)
                                     {
                                     pGroupGeometricPrimitives->Add(new Displaying::SVG::CSVGLine(Collinearity.m_PointA, Collinearity.m_PointB, CollinearityStrockeWidth, CollinearityColor));
                                     pGroupGeometricPrimitives->Add(new Displaying::SVG::CSVGCircle(Collinearity.m_MidPointAB, Radius, CollinearityStrockeWidth, CollinearityColor, CollinearityColor, Displaying::SVG::CSVGPrimitive::eOnlyStroke));
                                     }
                                     else
                                     {
                                     pGroupGeometricPrimitives->Add(new Displaying::SVG::CSVGLine(Collinearity.m_PointA, Collinearity.m_PointB, CollinearityStrockeWidth, EndPointColor));
                                     pGroupGeometricPrimitives->Add(new Displaying::SVG::CSVGCircle(Collinearity.m_MidPointAB, Radius, CollinearityStrockeWidth, EndPointColor, EndPointColor, Displaying::SVG::CSVGPrimitive::eOnlyStroke));
                                     }

                                     char cad[128];
                                     sprintf(cad, "[%i,%s,%s,%s]", Collinearity.m_Id, realValueToString(Collinearity.m_EndPointsMaximalPerpendicularDistance).c_str(), realValueToString(Collinearity.m_MaximalLinesDeviation).c_str(), realValueToString(Collinearity.m_OrientationDeviation * _REAL_RADIANS_TO_DEGREES_).c_str());
                                     pGroupGeometricPrimitives->Add(new Displaying::SVG::CSVGText(cad, Collinearity.m_MidPointAB, 0.125, ConnectionNodesColor));

                                     //pGroupGeometricPrimitives->AddDensityPrimitive(new Displaying::SVG::CSVGEllipticalArc(Mathematics::_2D::CVector2D(Collinearity.m_PointA, Collinearity.m_PointB), Radius, Radius, _REAL_ZERO_, _REAL_ZERO_, _REAL_PI_, CollinearityStrockeWidth, CollinearityColor));
                                     }
                                     }
                                     --------------------------------------------------------------------*/
                                }

                            const uint* pCharacterizedSaliencySegmentDistribution = pSaliencyBlock->GetCharacterizedSaliencySegmentDistribution();

                            if (pCharacterizedSaliencySegmentDistribution[Saliency::CCharacterizedSaliencySegment::eElongated])
                            {

                                list<Saliency::CSaliencyPixel*>::const_iterator EndSaliencyPixel = pSaliencyBlock->GetPixels().end();
                                for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = pSaliencyBlock->GetPixels().begin(); ppSaliencyPixel != EndSaliencyPixel; ++ppSaliencyPixel)
                                {
                                    const Saliency::CSaliencyPixel* pSaliencyPixel = *ppSaliencyPixel;
                                    const CPixelLocation& DiscreteLocation = pSaliencyPixel->GetDiscreteLocation();
                                    const Mathematics::_2D::CVector2D& ContinousLocation = pSaliencyPixel->GetContinousLocation();
                                    const CRGBAColor& NodeColor = PixelColors[pSaliencyPixel->GetCharacterizationType()];

                                    pGroupPixels->Add(new Displaying::SVG::CSVGRectangle(DiscreteLocation, PixelRadius, NodeColor, NodeColor, _REAL_ZERO_, Displaying::SVG::CSVGPrimitive::eOnlyFill));
                                    pGroupOptimizations->Add(new Displaying::SVG::CSVGLine(DiscreteLocation, ContinousLocation, ConnectionOptimizationStrockeWidth, ConnectionOptimizationColor));
                                    pGroupNodes->Add(new Displaying::SVG::CSVGCircle(ContinousLocation, NodeRadius, _REAL_ZERO_, NodeColor, NodeColor, Displaying::SVG::CSVGPrimitive::eOnlyFill));

                                    list<Saliency::CSaliencyPixel*>::const_iterator EndConnectedPixels = pSaliencyPixel->GetConnectedPixels().end();
                                    for (list<Saliency::CSaliencyPixel*>::const_iterator ppConnectedSaliencyPixel = pSaliencyPixel->GetConnectedPixels().begin(); ppConnectedSaliencyPixel != EndConnectedPixels; ++ppConnectedSaliencyPixel)
                                        if (pSaliencyPixel > (*ppConnectedSaliencyPixel))
                                        {
                                            //pGroupPixelsConnectivity->Add(new Displaying::SVG::CSVGLine(DiscreteLocation, (*ppConnectedSaliencyPixel)->GetDiscreteLocation(), ConnectionPixelStrockeWidth, ConnectionPixelColor));
                                            if (ContinousLocation.GetSquareDistance((*ppConnectedSaliencyPixel)->GetContinousLocation()) < _REAL_TWO_)
                                            {
                                                pGroupNodesConnectivity->Add(new Displaying::SVG::CSVGLine(ContinousLocation, (*ppConnectedSaliencyPixel)->GetContinousLocation(), ConnectionOptimizationStrockeWidth, ConnectionNodesColor));
                                            }
                                        }
                                }
                            }
                        }
                        return Plotter.SaveFile(Displaying::SVG::CSVGPrimitive::eSVG, pPathFileName);
                    }
                    return false;
                }

                void CMonocularSaliencyExtractor::LoadSaliencyPixelSetAnalyzer()
                {
                    m_SaliencyPixelSetAnalyzer.Clear();
                    const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                    const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                    const coordinate Offset = m_OutputActiveZone.GetHorizontalOffset();
                    Saliency::CSaliencyPixel* pSaliencyPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                    for (uint i = 0; i < ActiveHeight; ++i, pSaliencyPixel += Offset)
                        for (uint j = 0; j < ActiveWidth; ++j, ++pSaliencyPixel)
                            if (pSaliencyPixel->IsEnabled())
                            {
                                m_SaliencyPixelSetAnalyzer.AddPixel(pSaliencyPixel);
                            }
                }

                bool CMonocularSaliencyExtractor::DisplaySaliencyMagnitude()
                {
                    m_pDisplayImage->Clear();
                    const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                    const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                    const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                    const Saliency::CSaliencyPixel* pSaliencyPixel = m_pOutputImage->GetReadOnlyBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                    real Maximal = _REAL_MIN_, Minimal = _REAL_MAX_;
                    for (uint i = 0; i < ActiveHeight; ++i, pSaliencyPixel += Offset)
                        for (uint j = 0; j < ActiveWidth; ++j, ++pSaliencyPixel)
                            if (pSaliencyPixel->IsEnabled())
                            {
                                const real Magnitude = pSaliencyPixel->GetMagnitude();
                                if (Magnitude > Maximal)
                                {
                                    Maximal = Magnitude;
                                }
                                if (Magnitude < Minimal)
                                {
                                    Minimal = Magnitude;
                                }
                            }
                    const real Range = Maximal - Minimal;
                    if (Range > _REAL_EPSILON_)
                    {
                        pSaliencyPixel = m_pOutputImage->GetReadOnlyBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                        CDiscreteTristimulusPixel* pDisplayPixel = m_pDisplayImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                        switch (m_pParameters->GetDisplayMode())
                        {
                            case CParameters::eScaledIntensity:
                            {
                                const real Scale = _REAL_255_ / Range;
                                for (uint i = 0; i < ActiveHeight; ++i, pSaliencyPixel += Offset, pDisplayPixel += Offset)
                                    for (uint j = 0; j < ActiveWidth; ++j, ++pSaliencyPixel, ++pDisplayPixel)
                                        if (pSaliencyPixel->IsEnabled())
                                        {
                                            *pDisplayPixel = (pSaliencyPixel->GetMagnitude() - Minimal) * Scale;
                                        }
                            }
                            break;
                            case CParameters::eColorMapped:
                            {
                                const Visualization::CTristimulusColorMap* pTristimulusColorMap = m_pParameters->GetColorMap();
                                const real Scale = pTristimulusColorMap->GetColorLookupTableScaleFactor() / Range;
                                const CDiscreteTristimulusPixel* pLUT = pTristimulusColorMap->GetColorLookupTable();
                                for (uint i = 0; i < ActiveHeight; ++i, pSaliencyPixel += Offset, pDisplayPixel += Offset)
                                    for (uint j = 0; j < ActiveWidth; ++j, ++pSaliencyPixel, ++pDisplayPixel)
                                        if (pSaliencyPixel->IsEnabled())
                                        {
                                            *pDisplayPixel = pLUT[RoundPositiveRealToInteger((pSaliencyPixel->GetMagnitude() - Minimal) * Scale)];
                                        }
                            }
                            break;
                        }
                        return true;
                    }
                    return false;
                }

                bool CMonocularSaliencyExtractor::SortSaliencyBlockBySizeAscent(const Saliency::CSaliencyBlock* plhs, const Saliency::CSaliencyBlock* prhs)
                {
                    return plhs->GetSize() < prhs->GetSize();
                }

                bool CMonocularSaliencyExtractor::DisplaySaliencyPhase()
                {
                    m_pDisplayImage->Clear();
                    const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                    const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                    const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                    const Saliency::CSaliencyPixel* pSaliencyPixel = m_pOutputImage->GetReadOnlyBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                    CDiscreteTristimulusPixel* pDisplayPixel = m_pDisplayImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                    switch (m_pParameters->GetDisplayMode())
                    {
                        case CParameters::eScaledIntensity:
                        {
                            const real Scale = _REAL_255_ / _REAL_2PI_;
                            for (uint i = 0; i < ActiveHeight; ++i, pSaliencyPixel += Offset, pDisplayPixel += Offset)
                                for (uint j = 0; j < ActiveWidth; ++j, ++pSaliencyPixel, ++pDisplayPixel)
                                    if (pSaliencyPixel->IsEnabled())
                                    {
                                        *pDisplayPixel = pSaliencyPixel->GetOrientation().GetAbsoluteAngle() * Scale;
                                    }
                        }
                        break;
                        case CParameters::eColorMapped:
                        {
                            const Visualization::CTristimulusColorMap* pTristimulusColorMap = m_pParameters->GetColorMap();
                            const real Scale = pTristimulusColorMap->GetColorLookupTableScaleFactor() / _REAL_2PI_;
                            const CDiscreteTristimulusPixel* pLUT = pTristimulusColorMap->GetColorLookupTable();
                            for (uint i = 0; i < ActiveHeight; ++i, pSaliencyPixel += Offset, pDisplayPixel += Offset)
                                for (uint j = 0; j < ActiveWidth; ++j, ++pSaliencyPixel, ++pDisplayPixel)
                                    if (pSaliencyPixel->IsEnabled())
                                    {
                                        *pDisplayPixel = pLUT[RoundPositiveRealToInteger(pSaliencyPixel->GetOrientation().GetAbsoluteAngle() * Scale)];
                                    }
                        }
                        break;
                    }
                    return true;
                }

                bool CMonocularSaliencyExtractor::DisplaySelectedPixels()
                {
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate Width = m_OutputActiveZone.GetWidth();
                    const Saliency::CSaliencyPixel* pBaseInputPixel = m_pOutputImage->GetReadOnlyBufferAt(X0, Y0);
                    real Maximal = _REAL_MIN_;
                    real Minimal = _REAL_MAX_;
                    for (coordinate y = Y0; y < Y1; ++y, pBaseInputPixel += Width)
                    {
                        const Saliency::CSaliencyPixel* pInputPixel = pBaseInputPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pInputPixel)
                            if (pInputPixel->IsSelected())
                            {
                                if (pInputPixel->GetMagnitude() > Maximal)
                                {
                                    Maximal = pInputPixel->GetMagnitude();
                                }
                                if (pInputPixel->GetMagnitude() < Minimal)
                                {
                                    Minimal = pInputPixel->GetMagnitude();
                                }
                            }
                    }
                    const real Range = Maximal - Minimal;
                    if (Range > _REAL_EPSILON_)
                    {
                        pBaseInputPixel = m_pOutputImage->GetReadOnlyBufferAt(X0, Y0);
                        CDiscreteTristimulusPixel* pBaseDisplayPixel = m_pDisplayImage->GetWritableBufferAt(X0, Y0);
                        switch (m_pParameters->GetDisplayMode())
                        {
                            case CParameters::eScaledIntensity:
                            {
                                const real Scale = _REAL_255_ / Range;
                                for (coordinate y = Y0; y < Y1; ++y, pBaseInputPixel += Width, pBaseDisplayPixel += Width)
                                {
                                    const Saliency::CSaliencyPixel* pInputPixel = pBaseInputPixel;
                                    CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                                    for (coordinate x = X0; x < X1; ++x, ++pInputPixel, ++pDisplayPixel)
                                        if (pInputPixel->IsSelected())
                                        {
                                            *pDisplayPixel = (pInputPixel->GetMagnitude() - Minimal) * Scale;
                                        }
                                }
                            }
                            break;
                            case CParameters::eColorMapped:
                            {
                                const real Scale = m_pParameters->GetColorMap()->GetColorLookupTableScaleFactor() / Range;
                                const CDiscreteTristimulusPixel* pLUT = m_pParameters->GetColorMap()->GetColorLookupTable();
                                for (coordinate y = Y0; y < Y1; ++y, pBaseInputPixel += Width, pBaseDisplayPixel += Width)
                                {
                                    const Saliency::CSaliencyPixel* pInputPixel = pBaseInputPixel;
                                    CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                                    for (coordinate x = X0; x < X1; ++x, ++pInputPixel, ++pDisplayPixel)
                                        if (pInputPixel->IsSelected())
                                        {
                                            *pDisplayPixel = pLUT[RoundPositiveRealToInteger((pInputPixel->GetMagnitude() - Minimal) * Scale)];
                                        }
                                }
                            }
                            break;
                        }
                        return true;
                    }
                    return false;
                }

                bool CMonocularSaliencyExtractor::DisplayBlocksEccentricity()
                {
                    if (m_SelectedSaliencyBlocks.size())
                    {
                        real Maximal = _REAL_ZERO_, Minimal = _REAL_MAX_, CurrentMaximal = _REAL_ZERO_, CurrentMinimal = _REAL_MAX_;
                        list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                        for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = ++m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                            if ((*ppSaliencyBlock)->GetApertureEccentricityRange(CurrentMaximal, CurrentMinimal))
                            {
                                if (CurrentMaximal > Maximal)
                                {
                                    Maximal = CurrentMaximal;
                                }
                                if (CurrentMinimal < Minimal)
                                {
                                    Minimal = CurrentMinimal;
                                }
                            }
                        const real Range = Maximal - Minimal;
                        if (Range > _REAL_EPSILON_)
                        {
                            switch (m_pParameters->GetDisplayMode())
                            {
                                case CParameters::eScaledIntensity:
                                {
                                    const real Scale = _REAL_255_ / Range;
                                    for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                                    {
                                        list<Saliency::CSaliencyPixel*>::const_iterator EndPixels = (*ppSaliencyBlock)->GetPixels().end();
                                        for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = (*ppSaliencyBlock)->GetPixels().begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                                        {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
                                            memset(m_pDisplayImage->GetWritableBufferAt((*ppSaliencyPixel)->GetDiscreteLocation()), SafeRoundToByte(((*ppSaliencyPixel)->GetApetureEccentricity() - Minimal) * Scale), sizeof(CDiscreteTristimulusPixel));
#pragma GCC diagnostic pop
                                        }
                                    }
                                }
                                break;
                                case CParameters::eColorMapped:
                                {
                                    const real Scale = m_pParameters->GetColorMap()->GetColorLookupTableScaleFactor() / Range;
                                    const CDiscreteTristimulusPixel* pLUT = m_pParameters->GetColorMap()->GetColorLookupTable();
                                    for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                                    {
                                        list<Saliency::CSaliencyPixel*>::const_iterator EndPixels = (*ppSaliencyBlock)->GetPixels().end();
                                        for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = (*ppSaliencyBlock)->GetPixels().begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                                        {
                                            m_pDisplayImage->SetValueAt((*ppSaliencyPixel)->GetDiscreteLocation(), pLUT[RoundPositiveRealToInteger(((*ppSaliencyPixel)->GetApetureEccentricity() - Minimal) * Scale)]);
                                        }
                                    }
                                }
                                break;
                            }
                            return true;
                        }
                    }
                    return false;
                }

                bool CMonocularSaliencyExtractor::DisplayBlocksMagnitude()
                {
                    if (m_SelectedSaliencyBlocks.size())
                    {
                        real Maximal = m_SelectedSaliencyBlocks.front()->GetMaximalSaliency();
                        real Minimal = Maximal;
                        list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                        for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = ++m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                            if ((*ppSaliencyBlock)->GetMaximalSaliency() > Maximal)
                            {
                                Maximal = (*ppSaliencyBlock)->GetMaximalSaliency();
                            }
                            else if ((*ppSaliencyBlock)->GetMinimalSaliency() < Minimal)
                            {
                                Minimal = (*ppSaliencyBlock)->GetMinimalSaliency();
                            }
                        const real Range = Maximal - Minimal;
                        if (Range > _REAL_EPSILON_)
                        {
                            switch (m_pParameters->GetDisplayMode())
                            {
                                case CParameters::eScaledIntensity:
                                {
                                    const real Scale = _REAL_255_ / Range;
                                    for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                                    {
                                        list<Saliency::CSaliencyPixel*>::const_iterator EndPixels = (*ppSaliencyBlock)->GetPixels().end();
                                        for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = (*ppSaliencyBlock)->GetPixels().begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                                        {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
                                            memset(m_pDisplayImage->GetWritableBufferAt((*ppSaliencyPixel)->GetDiscreteLocation()), SafeRoundToByte(((*ppSaliencyPixel)->GetMagnitude() - Minimal) * Scale), sizeof(CDiscreteTristimulusPixel));
#pragma GCC diagnostic pop
                                        }
                                    }
                                }
                                break;
                                case CParameters::eColorMapped:
                                {
                                    const real Scale = m_pParameters->GetColorMap()->GetColorLookupTableScaleFactor() / Range;
                                    const CDiscreteTristimulusPixel* pLUT = m_pParameters->GetColorMap()->GetColorLookupTable();
                                    for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                                    {
                                        list<Saliency::CSaliencyPixel*>::const_iterator EndPixels = (*ppSaliencyBlock)->GetPixels().end();
                                        for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = (*ppSaliencyBlock)->GetPixels().begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                                        {
                                            m_pDisplayImage->SetValueAt((*ppSaliencyPixel)->GetDiscreteLocation(), pLUT[RoundPositiveRealToInteger(((*ppSaliencyPixel)->GetMagnitude() - Minimal) * Scale)]);
                                        }
                                    }
                                }
                                break;
                            }
                            return true;
                        }
                    }
                    return false;
                }

                bool CMonocularSaliencyExtractor::DisplayBlocksConnectivity()
                {
                    if (m_SelectedSaliencyBlocks.size())
                    {
                        list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                        switch (m_pParameters->GetDisplayMode())
                        {
                            case CParameters::eScaledIntensity:
                            {
                                const real Scale = _REAL_255_ / real(7.0);
                                CDiscreteTristimulusPixel IntensityTable[8];
                                for (uint i = 0; i < 8; ++i)
                                {
                                    IntensityTable[i] = real(i) * Scale;
                                }
                                for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                                {
                                    list<Saliency::CSaliencyPixel*>::const_iterator EndPixels = (*ppSaliencyBlock)->GetPixels().end();
                                    for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = (*ppSaliencyBlock)->GetPixels().begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                                    {
                                        m_pDisplayImage->SetValueAt((*ppSaliencyPixel)->GetDiscreteLocation(), IntensityTable[(*ppSaliencyPixel)->GetConnectivityGrade()]);
                                    }
                                }
                            }
                            break;
                            case CParameters::eColorMapped:
                            {
                                CDiscreteTristimulusPixel LeafColor(128, 0, 0);
                                CDiscreteTristimulusPixel IntensityTable[8] = { CDiscreteTristimulusPixel::s_RGB_White, LeafColor, CDiscreteTristimulusPixel::s_RGB_Green, CDiscreteTristimulusPixel::s_RGB_Blue, CDiscreteTristimulusPixel::s_RGB_Yellow, CDiscreteTristimulusPixel::s_RGB_Cyan, CDiscreteTristimulusPixel::s_RGB_Gray128, CDiscreteTristimulusPixel::s_RGB_Gray64 };
                                for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                                {
                                    list<Saliency::CSaliencyPixel*>::const_iterator EndPixels = (*ppSaliencyBlock)->GetPixels().end();
                                    for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = (*ppSaliencyBlock)->GetPixels().begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                                    {
                                        m_pDisplayImage->SetValueAt((*ppSaliencyPixel)->GetDiscreteLocation(), IntensityTable[(*ppSaliencyPixel)->GetConnectivityGrade()]);
                                    }
                                    m_pDisplayImage->SetValueAt((*ppSaliencyBlock)->GetRoot()->GetDiscreteLocation(), CDiscreteTristimulusPixel::s_RGB_Red);
                                }
                            }
                            break;
                        }
                        return true;
                    }
                    return false;
                }

                bool CMonocularSaliencyExtractor::DisplayBlocksDiameter()
                {
                    if (m_SelectedSaliencyBlocks.size())
                    {
                        switch (m_pParameters->GetDisplayMode())
                        {
                            case CParameters::eScaledIntensity:
                            {
                                list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                                for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                                {
                                    real Maximal = (*ppSaliencyBlock)->GetPixels().front()->GetConnectivityDiameter();
                                    real Minimal = Maximal;
                                    list<Saliency::CSaliencyPixel*>::const_iterator EndPixels = (*ppSaliencyBlock)->GetPixels().end();
                                    for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = ++(*ppSaliencyBlock)->GetPixels().begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                                        if ((*ppSaliencyPixel)->GetConnectivityDiameter() > Maximal)
                                        {
                                            Maximal = (*ppSaliencyPixel)->GetConnectivityDiameter();
                                        }
                                        else if ((*ppSaliencyPixel)->GetConnectivityDiameter() < Minimal)
                                        {
                                            Minimal = (*ppSaliencyPixel)->GetConnectivityDiameter();
                                        }
                                    const real Scale = _REAL_255_ / (Maximal - Minimal);
                                    for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = (*ppSaliencyBlock)->GetPixels().begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                                    {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
                                        memset(m_pDisplayImage->GetWritableBufferAt((*ppSaliencyPixel)->GetDiscreteLocation()), SafeRoundToByte(((*ppSaliencyPixel)->GetConnectivityDiameter() - Minimal) * Scale), sizeof(CDiscreteTristimulusPixel));
#pragma GCC diagnostic pop
                                    }
                                }
                            }
                            break;
                            case CParameters::eColorMapped:
                            {
                                const CDiscreteTristimulusPixel* pLUT = m_pParameters->GetColorMap()->GetColorLookupTable();
                                const real ColorLookupTableScaleFactor = m_pParameters->GetColorMap()->GetColorLookupTableScaleFactor();
                                list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                                for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                                {
                                    real Maximal = (*ppSaliencyBlock)->GetPixels().front()->GetConnectivityDiameter();
                                    real Minimal = Maximal;
                                    list<Saliency::CSaliencyPixel*>::const_iterator EndPixels = (*ppSaliencyBlock)->GetPixels().end();
                                    for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = ++(*ppSaliencyBlock)->GetPixels().begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                                        if ((*ppSaliencyPixel)->GetConnectivityDiameter() > Maximal)
                                        {
                                            Maximal = (*ppSaliencyPixel)->GetConnectivityDiameter();
                                        }
                                        else if ((*ppSaliencyPixel)->GetConnectivityDiameter() < Minimal)
                                        {
                                            Minimal = (*ppSaliencyPixel)->GetConnectivityDiameter();
                                        }
                                    const real Scale = ColorLookupTableScaleFactor / (Maximal - Minimal);
                                    for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = (*ppSaliencyBlock)->GetPixels().begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                                    {
                                        m_pDisplayImage->SetValueAt((*ppSaliencyPixel)->GetDiscreteLocation(), pLUT[RoundPositiveRealToInteger(((*ppSaliencyPixel)->GetConnectivityDiameter() - Minimal) * Scale)]);
                                    }
                                }
                            }
                            break;
                        }
                        return true;
                    }
                    return false;
                }

                bool CMonocularSaliencyExtractor::DisplayBlocksRooting()
                {
                    if (m_SelectedSaliencyBlocks.size())
                    {
                        switch (m_pParameters->GetDisplayMode())
                        {
                            case CParameters::eScaledIntensity:
                            {
                                list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                                for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                                {
                                    const real Scale = _REAL_255_ / (*ppSaliencyBlock)->ComputeRootingDistance();
                                    list<Saliency::CSaliencyPixel*>::const_iterator EndPixels = (*ppSaliencyBlock)->GetPixels().end();
                                    for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = (*ppSaliencyBlock)->GetPixels().begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                                    {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
                                        memset(m_pDisplayImage->GetWritableBufferAt((*ppSaliencyPixel)->GetDiscreteLocation()), SafeRoundToByte((*ppSaliencyPixel)->GetBroadCastDistance() * Scale), sizeof(CDiscreteTristimulusPixel));
#pragma GCC diagnostic pop
                                    }
                                }
                            }
                            break;
                            case CParameters::eColorMapped:
                            {
                                const CDiscreteTristimulusPixel* pLUT = m_pParameters->GetColorMap()->GetColorLookupTable();
                                const real ColorLookupTableScaleFactor = m_pParameters->GetColorMap()->GetColorLookupTableScaleFactor();
                                list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                                for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                                {
                                    const real Scale = ColorLookupTableScaleFactor / (*ppSaliencyBlock)->ComputeRootingDistance();
                                    list<Saliency::CSaliencyPixel*>::const_iterator EndPixels = (*ppSaliencyBlock)->GetPixels().end();
                                    for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = (*ppSaliencyBlock)->GetPixels().begin(); ppSaliencyPixel != EndPixels; ++ppSaliencyPixel)
                                    {
                                        m_pDisplayImage->SetValueAt((*ppSaliencyPixel)->GetDiscreteLocation(), pLUT[RoundPositiveRealToInteger((*ppSaliencyPixel)->GetBroadCastDistance()*Scale)]);
                                    }
                                }
                            }
                            break;
                        }
                        return true;
                    }
                    return false;
                }
            }
        }
    }
}
