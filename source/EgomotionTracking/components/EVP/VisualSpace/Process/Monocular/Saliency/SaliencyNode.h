/*
 * SaliencyNode.h
 *
 *  Created on: 12.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "SaliencyPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            class CSaliencyNode
            {
            public:

                CSaliencyNode() :
                    m_IsActive(false), m_Heuristic(_REAL_ZERO_), m_pSourcePixel(NULL), m_pPathParent(NULL), m_pTreeParent(NULL)
                {
                }

                virtual ~CSaliencyNode()
                {

                }

                inline void SetActive(const bool Active)
                {
                    m_IsActive = Active;
                }

                inline bool IsActive() const
                {
                    return m_IsActive;
                }

                inline void SetTreeParent(CSaliencyNode* pTreeParent)
                {
                    m_pTreeParent = pTreeParent;
                }

                inline CSaliencyNode* GetTreeParent()
                {
                    return m_pTreeParent;
                }

                inline const CSaliencyNode* GetReadOnlyTreeParent() const
                {
                    return m_pTreeParent;
                }

                inline void SetPathParent(CSaliencyNode* pPathParent)
                {
                    m_pPathParent = pPathParent;
                }

                inline const CSaliencyNode* GetReadOnlyPathParent() const
                {
                    return m_pPathParent;
                }

                inline CSaliencyNode* GetPathParent()
                {
                    return m_pPathParent;
                }

                inline real GetDistanceToParent() const
                {
                    return m_pSourcePixel->GetContinousLocation().GetSquareDistance(m_pTreeParent->m_pSourcePixel->GetContinousLocation());
                }

                inline void SetSourcePixel(CSaliencyPixel* pSourcePixel)
                {
                    m_pSourcePixel = pSourcePixel;
                }

                inline real GetSourceOptimizedMagnitude() const
                {
                    return m_pSourcePixel->GetOptimizedMagnitude();
                }

                inline bool IsSourceDiscretelyConnected(const CSaliencyNode* pSaliencyNode) const
                {
                    return m_pSourcePixel->IsDiscretelyConnected(pSaliencyNode->m_pSourcePixel);
                }

                inline real GetSourceContinousDistance(const CSaliencyNode* pSaliencyNode) const
                {
                    return m_pSourcePixel->GetContinousLocation().GetDistance(pSaliencyNode->m_pSourcePixel->GetContinousLocation());
                }

                inline const Mathematics::_2D::CVector2D& GetSourceContinousLocation() const
                {
                    return m_pSourcePixel->GetContinousLocation();
                }

                inline const CPixelLocation& GetSourceDiscreteLocation() const
                {
                    return m_pSourcePixel->GetDiscreteLocation();
                }

                inline const list<CSaliencyPixel*>& GetSourceConnectedPixels() const
                {
                    return m_pSourcePixel->GetConnectedPixels();
                }

                inline bool IsLinkable(const CSaliencyBlock* pBlock) const
                {
                    return (!m_pTreeParent) && (m_pSourcePixel->GetBlock() == pBlock);
                }

                inline bool VerifyOptimalLinkageDistanceBased(const CSaliencyNode* pParentCandidate, const int* pDeltas, const real DistanceToParentCandidate) const
                {
                    const Mathematics::_2D::CVector2D& ContinousLocation = m_pSourcePixel->GetContinousLocation();
                    for (uint i = 0; i < 8; ++i)
                    {
                        const CSaliencyNode* pSaliencyNode = this + pDeltas[i];
                        if (pSaliencyNode->m_pTreeParent && (pSaliencyNode != pParentCandidate) && (!pSaliencyNode->m_NodeChildren.size()) && (pSaliencyNode->m_pSourcePixel->GetContinousLocation().GetDistance(ContinousLocation) < DistanceToParentCandidate))
                        {
                            return false;
                        }
                    }
                    return true;
                }

                inline void ClearChildren()
                {
                    m_NodeChildren.clear();
                }

                inline void AddChild(const CSaliencyNode* pChild)
                {
                    m_NodeChildren.push_back(pChild);
                }

                inline void SetHeuristic(const real Heuristic)
                {
                    m_Heuristic = Heuristic;
                }

                inline void ClearHeuristic()
                {
                    m_Heuristic = _REAL_ZERO_;
                }

                inline real GetHeuristic() const
                {
                    return m_Heuristic;
                }

                inline bool IsStaticLeaf() const
                {
                    return m_pTreeParent && (!m_NodeChildren.size());
                }

                inline bool IsDynamicPathEndNode() const
                {
                    vector<const CSaliencyNode*>::const_iterator EndNodeChildren = m_NodeChildren.begin();
                    if (m_pTreeParent)
                    {
                        for (vector<const CSaliencyNode*>::const_iterator ppSaliencyPixelNode = m_NodeChildren.begin(); ppSaliencyPixelNode != EndNodeChildren; ++ppSaliencyPixelNode)
                            if ((*ppSaliencyPixelNode)->IsActive())
                            {
                                return false;
                            }
                    }
                    else
                    {
                        uint ActiveChildren = 0;
                        for (vector<const CSaliencyNode*>::const_iterator ppSaliencyPixelNode = m_NodeChildren.begin(); ppSaliencyPixelNode != EndNodeChildren; ++ppSaliencyPixelNode)
                            if ((*ppSaliencyPixelNode)->IsActive() && (ActiveChildren++))
                            {
                                return false;
                            }
                    }
                    return true;
                }

                inline bool IsDynamicLeaf() const
                {
                    if (m_pTreeParent)
                    {
                        for (vector<const CSaliencyNode*>::const_iterator ppSaliencyPixelNode = m_NodeChildren.begin(); ppSaliencyPixelNode != m_NodeChildren.end(); ++ppSaliencyPixelNode)
                            if ((*ppSaliencyPixelNode)->IsActive())
                            {
                                return false;
                            }
                        return true;
                    }
                    return false;
                }

                inline bool IsActivePathBottomNode() const
                {
                    if (m_IsActive && m_pTreeParent)
                    {
                        uint TotalActiveChildren = 0;
                        for (vector<const CSaliencyNode*>::const_iterator ppSaliencyPixelNode = m_NodeChildren.begin(); ppSaliencyPixelNode != m_NodeChildren.end(); ++ppSaliencyPixelNode)
                            if ((*ppSaliencyPixelNode)->IsActive())
                            {
                                ++TotalActiveChildren;
                            }
                        return (TotalActiveChildren != 1);
                    }
                    return false;
                }

                inline uint GetTotalChildren() const
                {
                    return m_NodeChildren.size();
                }

                inline uint GetTotalActiveChildren() const
                {
                    uint TotalActiveChildren = 0;
                    for (vector<const CSaliencyNode*>::const_iterator ppSaliencyPixelNode = m_NodeChildren.begin(); ppSaliencyPixelNode != m_NodeChildren.end(); ++ppSaliencyPixelNode)
                        if ((*ppSaliencyPixelNode)->IsActive())
                        {
                            ++TotalActiveChildren;
                        }
                    return TotalActiveChildren;
                }

                inline uint GetTotalActiveSibilings() const
                {
                    if (m_pTreeParent)
                    {
                        uint TotalActiveSibilings = 0;
                        for (vector<const CSaliencyNode*>::const_iterator ppSaliencyPixelNode = m_pTreeParent->m_NodeChildren.begin(); ppSaliencyPixelNode != m_pTreeParent->m_NodeChildren.end(); ++ppSaliencyPixelNode)
                            if (((*ppSaliencyPixelNode) != this) && (*ppSaliencyPixelNode)->IsActive())
                            {
                                ++TotalActiveSibilings;
                            }
                        return TotalActiveSibilings;
                    }
                    return 0;
                }

                inline bool IsDynamicSplitter() const
                {
                    if (m_pTreeParent)
                    {
                        if (m_NodeChildren.size() > 1)
                        {
                            uint TotalActiveChildren = 0;
                            vector<const CSaliencyNode*>::const_iterator EndNodeChildren = m_NodeChildren.end();
                            for (vector<const CSaliencyNode*>::const_iterator ppSaliencyPixelNode = m_NodeChildren.begin(); ppSaliencyPixelNode != EndNodeChildren; ++ppSaliencyPixelNode)
                                if ((*ppSaliencyPixelNode)->IsActive())
                                    if (TotalActiveChildren++)
                                    {
                                        return true;
                                    }
                        }
                        return false;
                    }
                    return true;
                }

                inline void BackPropagation()
                {
                    CSaliencyNode* pSaliencyNode = this;
                    while (pSaliencyNode->m_pTreeParent)
                    {
                        const real NextHeuristic = pSaliencyNode->m_Heuristic + 1;
                        if (NextHeuristic > pSaliencyNode->m_pTreeParent->m_Heuristic)
                        {
                            pSaliencyNode->m_pTreeParent->m_Heuristic = NextHeuristic;
                            pSaliencyNode = pSaliencyNode->m_pTreeParent;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                inline real GetMinimalExpansionDistance(const CSaliencyBlock* pBlock, const int* pDeltas) const
                {
                    real MinimalExpansionDistance = _REAL_MAX_;
                    const Mathematics::_2D::CVector2D& ContinousLocation = m_pSourcePixel->GetContinousLocation();
                    for (uint i = 0; i < 8; ++i)
                    {
                        const CSaliencyNode* pSaliencyNode = pDeltas[i] + this;
                        if ((!pSaliencyNode->m_pTreeParent) && (pSaliencyNode->m_pSourcePixel->GetBlock() == pBlock))
                        {
                            const real ExpansionDistance = pSaliencyNode->m_pSourcePixel->GetContinousLocation().GetDistance(ContinousLocation);
                            if (ExpansionDistance < MinimalExpansionDistance)
                            {
                                MinimalExpansionDistance = ExpansionDistance;
                            }
                        }
                    }
                    return MinimalExpansionDistance;
                }

                inline void DeactivateRedundatLeaf()
                {
                    if (m_IsActive && m_pTreeParent)
                    {
                        m_IsActive = (m_pTreeParent->m_Heuristic == (m_Heuristic + 1));
                    }
                }

                inline void Reset()
                {
                    m_IsActive = false;
                    m_Heuristic = _REAL_ZERO_;
                    m_pPathParent = NULL;
                    m_pTreeParent = NULL;
                    m_NodeChildren.clear();
                }

            protected:

                bool m_IsActive;
                real m_Heuristic;
                CSaliencyPixel* m_pSourcePixel;
                CSaliencyNode* m_pPathParent;
                CSaliencyNode* m_pTreeParent;
                vector<const CSaliencyNode*> m_NodeChildren;
            };
        }
    }
}

