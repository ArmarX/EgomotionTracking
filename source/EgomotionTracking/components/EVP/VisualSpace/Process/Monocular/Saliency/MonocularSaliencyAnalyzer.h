/*
 * MonocularSaliencyAnalyzer.h
 *
 *  Created on: 16.12.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../Visualization/Geometry/2D/SVG/SVGGraphic.h"
#include "../../Base/ImageProcessBase.h"
#include "MonocularSaliencyExtractor.h"
#include "SaliencyPixel.h"
#include "SaliencyNode.h"
#include "SaliencyPath.h"
#include "SaliencyTree.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                class CMonocularSaliencyAnalyzer: public TImageProcessBase<Saliency::CSaliencyPixel, Saliency::CSaliencyNode, CDiscreteTristimulusPixel>
                {
                public:

                    enum VisualizationGroup
                    {
                        eNodes = 0x01, eLinks = 0x02, eOptimizationNodes = 0x04, eOptimizationLinks = 0x8, ePoints = 0x10, eEllipses = 0x20, ePointLinks = 0x40, eSegments = 0x80, eAll = 0XFF
                    };

                    enum VisualizationMode
                    {
                        eBylayer, eByTree
                    };

                    CMonocularSaliencyAnalyzer(const CImageProcessBase::ProcessTypeId TypeId, CMonocularSaliencyExtractor* pMonocularSaliencyExtractor);
                    ~CMonocularSaliencyAnalyzer() override;

                    const list<Saliency::CSaliencyTree*>& GetForest();
                    bool ExportForest(const_string pPathFileName, const CImageActiveZone* pImageActiveZone, const VisualizationGroup Flags, const VisualizationMode Mode, const bool DisplaySmoothedPointLocations);

                protected:

                    void ClearForest();

                    void DisplayNodes(Displaying::SVG::CSVGGroup* pGroupNodes, Saliency::CSaliencyTree* pSaliencyTree, const real NodeRadius, const CRGBAColor& NodeColor);
                    void DisplayLinks(Displaying::SVG::CSVGGroup* pGroupLinks, Saliency::CSaliencyTree* pSaliencyTree, const real ActiveConnectionLinkWidth, const real InactiveConnectionLinkWidth, const real RootNodeRadius, const CRGBAColor& ConnectionNodeColor, const CRGBAColor& RootNodeColor);
                    void DisplayOptimizationNodes(Displaying::SVG::CSVGGroup* pGroupOptimizationNodes, Saliency::CSaliencyTree* pSaliencyTree, const real OptimizedNodeRadius, const CRGBAColor& OptimizedNodeColor);
                    void DisplayOptimizationLinks(Displaying::SVG::CSVGGroup* pGroupOptimizationLinks, Saliency::CSaliencyTree* pSaliencyTree, const real OptimizationConnectionLinkWidth, const CRGBAColor& OptimizationConnectionNodeColor);
                    void DisplayPoints(Displaying::SVG::CSVGGroup* pGroupPoints, Saliency::CSaliencyTree* pSaliencyTree, const real PointRadius, const CRGBAColor* pCharacterizationColors, const bool DisplaySmoothedPointLocations);
                    void DisplayEllipses(Displaying::SVG::CSVGGroup* pGroupEllipses, Saliency::CSaliencyTree* pSaliencyTree, const real EllipseStrokeWidth, const CRGBAColor* pCharacterizationColors, const bool DisplaySmoothedPointLocations);
                    void DisplayPointLinks(Displaying::SVG::CSVGGroup* pGroupPointLinks, Saliency::CSaliencyTree* pSaliencyTree, const real PointConnectionLinkWidth, const CRGBAColor& PointConnectionColor, const bool DisplaySmoothedPointLocations);
                    void DisplaySegments(Displaying::SVG::CSVGGroup* pGroupSegments, Saliency::CSaliencyTree* pSaliencyTree);

                    void ExportForestByLayer(Displaying::SVG::CSVGGraphic& Plotter, const CImageActiveZone* pImageActiveZone, const VisualizationGroup Flags, const bool DisplaySmoothedPointLocations);
                    void ExportForestByTree(Displaying::SVG::CSVGGraphic& Plotter, const CImageActiveZone* pImageActiveZone, const VisualizationGroup Flags, const bool DisplaySmoothedPointLocations);

                    CMonocularSaliencyExtractor* m_pMonocularSaliencyExtractor;
                    list<Saliency::CSaliencyTree*> m_Forest;
                };
            }
        }
    }
}

