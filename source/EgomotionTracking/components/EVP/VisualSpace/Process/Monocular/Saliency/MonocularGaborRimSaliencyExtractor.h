/*
 * MonocularGaborRimSaliencyExtractor.h
 *
 *  Created on: 05.12.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../Kernels/ConvolutionGaborKernel2D.h"
#include "../../../Kernels/ConvolutionDoGKernel2D.h"
#include "MonocularSaliencyExtractor.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                class CMonocularGaborRimSaliencyExtractor: public CMonocularSaliencyExtractor
                {
                public:

                    class CParameters: public CMonocularSaliencyExtractor::CParameters
                    {
                    public:

                        CParameters(CMonocularGaborRimSaliencyExtractor* pHost) :
                            CMonocularSaliencyExtractor::CParameters(pHost), m_KernelRadius(2), m_KernelLambda(_REAL_ONE_), m_KernelSigma(real(5.0 / 3.0)), m_KernelGamma1(_REAL_ONE_), m_KernelGamma2(real(4.0)), m_TotalExtractionBands(4), m_OutterGaussianSigma(real(2.0 / 3.0)), m_InnerGaussianSigma(real(1.8 / 3.0)), m_UseSaliencyPercentile(false), m_SaliencyPercentilThreshold(_REAL_ZERO_), m_UseRimPercentile(true), m_RimPercentilThreshold(real(0.05)), m_MinimalBlockSize(24), m_MinimalPathLength(8), m_OptimizationScope(_REAL_ONE_), m_OptimizationIterations(10), m_UseRefinementOptimization(true), m_OptimizationPrecision(real(0.001))
                        {
                        }

                        CParametersBase::ParameterChangeResult SetKernelRadius(const uint KernelRadius, const bool Wait = true);
                        uint GetKernelRadius() const;

                        CParametersBase::ParameterChangeResult SetKernelLambda(const real KernelLambda, const bool Wait = true);
                        real GetKernelLambda() const;

                        CParametersBase::ParameterChangeResult SetKernelSigma(const real KernelSigma, const bool Wait = true);
                        real GetKernelSigma() const;

                        CParametersBase::ParameterChangeResult SetKernelGamma1(const real KernelGamma1, const bool Wait = true);
                        real GetKernelGamma1() const;

                        CParametersBase::ParameterChangeResult SetKernelGamma2(const real KernelGamma2, const bool Wait = true);
                        real GetKernelGamma2() const;

                        CParametersBase::ParameterChangeResult SetTotalExtractionBands(const uint TotalExtractionBands, const bool Wait = true);
                        uint GetTotalExtractionBands() const;

                        CParametersBase::ParameterChangeResult SetOutterGaussianSigma(const real OutterGaussianSigma, const bool Wait = true);
                        real GetOutterGaussianSigma();

                        CParametersBase::ParameterChangeResult SetInnerGaussianSigma(const real InnerGaussianSigma, const bool Wait = true);
                        real GetInnerGaussianSigma();

                        CParametersBase::ParameterChangeResult SetUseSaliencyPercentile(const bool UseSaliencyPercentile, const bool Wait = true);
                        bool GetUseSaliencyPercentile() const;

                        CParametersBase::ParameterChangeResult SetSaliencyPercentilThreshold(const real SaliencyPercentilThreshold, const bool Wait = true);
                        real GetSaliencyPercentilThreshold() const;

                        CParametersBase::ParameterChangeResult SetUseRimPercentile(const bool UseRimPercentile, const bool Wait = true);
                        bool GetUseRimPercentile() const;

                        CParametersBase::ParameterChangeResult SetRimPercentilThreshold(const real RimPercentilThreshold, const bool Wait = true);
                        real GetRimPercentilThreshold() const;

                        CParametersBase::ParameterChangeResult SetUseRimCoherence(const bool UseRimCoherence, const bool Wait = true);
                        bool GetUseRimCoherence() const;

                        CParametersBase::ParameterChangeResult SetRimCoherenceThreshold(const real RimCoherenceThreshold, const bool Wait = true);
                        real GetRimCoherenceThreshold() const;

                        CParametersBase::ParameterChangeResult SetRimDominanceLevels(const uint DominanceLevels, const bool Wait = true);
                        uint GetRimDominanceLevels() const;

                        CParametersBase::ParameterChangeResult SetMinimalBlockSize(const uint MinimalBlockSize, const bool Wait = true);
                        uint GetMinimalBlockSize() const;

                        CParametersBase::ParameterChangeResult SetMinimalPathLength(const uint MinimalPathLength, const bool Wait = true);
                        uint GetMinimalPathLength() const;

                        CParametersBase::ParameterChangeResult SetOptimizationScope(const real OptimizationScope, const bool Wait = true);
                        real GetOptimizationScope() const;

                        CParametersBase::ParameterChangeResult SetOptimizationIterations(const uint OptimizationIterations, const bool Wait = true);
                        uint GetOptimizationIterations() const;

                        CParametersBase::ParameterChangeResult SetUseRefinementOptimization(const bool UseRefinementOptimization, const bool Wait = true);
                        bool GetUseRefinementOptimization() const;

                        CParametersBase::ParameterChangeResult SetOptimizationPrecision(const real OptimizationPrecision, const bool Wait = true);
                        real GetOptimizationPrecision() const;

                    protected:

                        //KERNELS
                        uint m_KernelRadius;
                        real m_KernelLambda;
                        real m_KernelSigma;
                        real m_KernelGamma1;
                        real m_KernelGamma2;
                        //EXTRACTION
                        uint m_TotalExtractionBands;
                        real m_OutterGaussianSigma;
                        real m_InnerGaussianSigma;
                        //SELECTION
                        bool m_UseSaliencyPercentile;
                        real m_SaliencyPercentilThreshold;
                        //FILTERING
                        bool m_UseRimPercentile;
                        real m_RimPercentilThreshold;
                        uint m_MinimalBlockSize;
                        uint m_MinimalPathLength;
                        //OPTIMIZATION
                        real m_OptimizationScope;
                        uint m_OptimizationIterations;
                        bool m_UseRefinementOptimization;
                        real m_OptimizationPrecision;
                    };

                    CMonocularGaborRimSaliencyExtractor(CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, Threading::CMutex* pInputImageMutex);
                    ~CMonocularGaborRimSaliencyExtractor() override;

                    bool Execute(const Identifier TrialId) override;

                    CParameters* GetGaborRimSaliencyParameters();

                protected:

                    friend class CMonocularGaborRimSaliencyExtractor::CParameters;

                    bool UpdateTotalExtractionBands();
                    bool UpdateDoGKernel();
                    void ClearGaborKernels();

                    bool Extract(const Identifier TrialId) override;
                    void ComputeGaborSaliency(const Identifier TrialId);
                    void ComputeRimSaliency(const Identifier TrialId);
                    void DetectSaliencyMagnitudRims(const Identifier TrialId);
                    void ExtractSaliencyMagnitudBlocks(const Identifier TrialId);
                    void ClearBlocks();

                    CParameters m_Parameters;
                    list<Kernels::CConvolutionGaborKernel2D*> m_GaborKernels;
                    Kernels::CConvolutionDoGKernel2D* m_pConvolutionDoGKernel2D;
                    CImageActiveZone m_SaliencyActiveZone;
                    CImageActiveZone m_RimActiveZone;
                    CImageActiveZone m_SelectedRimActiveZone;
                    real m_SaliencyThreshold;
                    real m_RimThreshold;

                    static  bool GetGaborKernelOffsetActiveZoneUpdate(const void* pData, coordinate& OffsetX0, coordinate& OffsetY0, coordinate& OffsetX1, coordinate& OffsetY1);
                    static  bool GetDoGKernelOffsetActiveZoneUpdate(const void* pData, coordinate& OffsetX0, coordinate& OffsetY0, coordinate& OffsetX1, coordinate& OffsetY1);
                };
            }
        }
    }
}

