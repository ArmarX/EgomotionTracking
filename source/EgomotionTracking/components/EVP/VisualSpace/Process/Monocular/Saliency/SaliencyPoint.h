/*
 * SaliencyPoint.h
 *
 *  Created on: 12.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/Mathematics/2D/Vector2D.h"
#include "../../../../Foundation/Mathematics/2D/StructuralTensor2D.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            class CSaliencyPath;
            class CSaliencySegment;

            class CSaliencyPoint
            {
            public:

                enum CharacterizationType
                {
                    eUndefined = -1, eCompact = 0, eTransitional = 1, eElongated = 2, eSingular = 3
                };

                enum CharacterizationMode
                {
                    eLineal, ePower, eApeture, ePowerApeture
                };

                CSaliencyPoint(const uint Index, const Mathematics::_2D::CVector2D& ContinousLocation, CSaliencyPath* pPath, CSaliencyPoint* pPrevious) :
                    m_Type(eUndefined), m_Index(Index), m_ZoneSize(0), m_Deviation(_REAL_ZERO_), m_Vxx(_REAL_ZERO_), m_Vxy(_REAL_ZERO_), m_Vyy(_REAL_ZERO_), m_MainRadius(_REAL_ZERO_), m_SecondaryRadius(_REAL_ZERO_), m_ContinousLocation(ContinousLocation), m_SmoothContinousLocation(ContinousLocation), m_MainAxis(), m_SecondaryAxis(), m_pSubsequent(NULL), m_pPrevious(pPrevious), m_pSegmentNode(NULL)
                {
                    if (m_pPrevious)
                    {
                        m_pPrevious->m_pSubsequent = this;
                    }
                }

                virtual ~CSaliencyPoint()
                {
                }

                inline void SetContinousLocation(const Mathematics::_2D::CVector2D& ContinousLocation)
                {
                    m_ContinousLocation = ContinousLocation;
                }

                inline const Mathematics::_2D::CVector2D& GetContinousLocation() const
                {
                    return m_ContinousLocation;
                }

                inline void SetSmoothContinousLocation(const Mathematics::_2D::CVector2D& SmoothContinousLocation)
                {
                    m_SmoothContinousLocation = SmoothContinousLocation;
                }

                inline const Mathematics::_2D::CVector2D& GetSmoothContinousLocation() const
                {
                    return m_SmoothContinousLocation;
                }

                inline const CSaliencyPoint* GetPreviousPoint() const
                {
                    return m_pPrevious;
                }

                inline const CSaliencyPoint* GetSubsequentPoint() const
                {
                    return m_pSubsequent;
                }

                inline CSaliencyPoint* GetWritablePreviousPoint()
                {
                    return m_pPrevious;
                }

                inline CSaliencyPoint* GetWritableSubsequentPoint()
                {
                    return m_pSubsequent;
                }

                inline const Mathematics::_2D::CVector2D& GetMainAxis() const
                {
                    return m_MainAxis;
                }

                inline const Mathematics::_2D::CVector2D& GetSecondaryAxis() const
                {
                    return m_SecondaryAxis;
                }

                inline real GetMainRadius() const
                {
                    return m_MainRadius;
                }

                inline real GetSecondaryRadius() const
                {
                    return m_SecondaryRadius;
                }

                inline bool IsSingular() const
                {
                    return (m_Type == eSingular);
                }

                inline real GetEccentricity() const
                {
                    if (m_SecondaryRadius < _REAL_EPSILON_)
                    {
                        return _REAL_MAX_;
                    }
                    return m_MainRadius / m_SecondaryRadius;
                }

                inline real GetPowerEccentricity(const real Power) const
                {
                    if (m_SecondaryRadius < _REAL_EPSILON_)
                    {
                        return _REAL_MAX_;
                    }
                    const real Factor = RealPow(m_SecondaryRadius, Power);
                    if (Factor < _REAL_EPSILON_)
                    {
                        return _REAL_MAX_;
                    }
                    return RealPow(m_MainRadius, Power) / Factor;
                }

                inline real GetAperture() const
                {
                    return (RealAtan2(m_MainRadius, m_SecondaryRadius) * _REAL_INVERSE_PI_);
                }

                inline void AddToSmoothCharacterization(CSaliencyPoint* pPointNode)
                {
                    real Cxx, Cxy, Cyy;
                    m_SmoothContinousLocation.GetCovariance(pPointNode->m_SmoothContinousLocation, Cxx, Cxy, Cyy);
                    m_Vxx += Cxx;
                    m_Vxy += Cxy;
                    m_Vyy += Cyy;
                    ++m_ZoneSize;
                    pPointNode->m_Vxx += Cxx;
                    pPointNode->m_Vxy += Cxy;
                    pPointNode->m_Vyy += Cyy;
                    ++pPointNode->m_ZoneSize;
                }

                void AddToCharacterization(CSaliencyPoint* pPointNode)
                {
                    real Cxx, Cxy, Cyy;
                    m_ContinousLocation.GetCovariance(pPointNode->m_ContinousLocation, Cxx, Cxy, Cyy);
                    m_Vxx += Cxx;
                    m_Vxy += Cxy;
                    m_Vyy += Cyy;
                    ++m_ZoneSize;
                    pPointNode->m_Vxx += Cxx;
                    pPointNode->m_Vxy += Cxy;
                    pPointNode->m_Vyy += Cyy;
                    ++pPointNode->m_ZoneSize;
                }

                bool StructuralCharacterize()
                {
                    const real Normalization = _REAL_ONE_ / (real(m_ZoneSize));
                    Mathematics::_2D::CStructuralTensor2D StructuralTensor2D(m_Vxx * Normalization, m_Vxy * Normalization, m_Vyy * Normalization);
                    if (StructuralTensor2D.IsSingular())
                    {
                        m_Type = eSingular;
                        m_MainAxis = Mathematics::_2D::CVector2D::s_Direction_Axis_X_2D;
                        m_SecondaryAxis = Mathematics::_2D::CVector2D::s_Direction_Axis_Y_2D;
                        m_MainRadius = _REAL_ONE_;
                        m_SecondaryRadius = _REAL_ONE_;
                        return false;
                    }
                    else
                    {
                        StructuralTensor2D.Decomposition();
                        m_MainAxis = StructuralTensor2D.GetUnitaryMainAxis();
                        m_SecondaryAxis = StructuralTensor2D.GetUnitarySecondaryAxis();
                        m_MainRadius = StructuralTensor2D.GetMainAxisLength();
                        m_SecondaryRadius = StructuralTensor2D.GetSecondaryAxisLength();
                        if (m_SecondaryRadius < _REAL_EPSILON_)
                        {
                            m_Type = eElongated;
                            return false;
                        }
                        return true;
                    }
                }

                void CharacterizeLineal(const real ThresholdTransitional, const real ThresholdElongated)
                {
                    if (StructuralCharacterize())
                    {
                        const real Characterization = m_MainRadius / m_SecondaryRadius;
                        if (Characterization > ThresholdElongated)
                        {
                            m_Type = eElongated;
                        }
                        else if (Characterization > ThresholdTransitional)
                        {
                            m_Type = eTransitional;
                        }
                        else
                        {
                            m_Type = eCompact;
                        }
                    }
                }

                void CharacterizePower(const real ThresholdTransitional, const real ThresholdElongated, const real Power)
                {
                    if (StructuralCharacterize())
                    {
                        const real Characterization = RealPow(m_MainRadius, Power) / RealPow(m_SecondaryRadius, Power);
                        if (Characterization > ThresholdElongated)
                        {
                            m_Type = eElongated;
                        }
                        else if (Characterization > ThresholdTransitional)
                        {
                            m_Type = eTransitional;
                        }
                        else
                        {
                            m_Type = eCompact;
                        }
                    }
                }

                void CharacterizeApeture(const real ThresholdTransitional, const real ThresholdElongated)
                {
                    if (StructuralCharacterize())
                    {
                        const real Characterization = RealAtan2(m_MainRadius, m_SecondaryRadius) * _REAL_INVERSE_HALF_PI_;
                        if (Characterization > ThresholdElongated)
                        {
                            m_Type = eElongated;
                        }
                        else if (Characterization > ThresholdTransitional)
                        {
                            m_Type = eTransitional;
                        }
                        else
                        {
                            m_Type = eCompact;
                        }
                    }
                }

                void CharacterizePowerApeture(const real ThresholdTransitional, const real ThresholdElongated, const real Power = _REAL_ZERO_)
                {
                    if (StructuralCharacterize())
                    {
                        const real Characterization = RealAtan2(RealPow(m_MainRadius, Power), RealPow(m_SecondaryRadius, Power)) * _REAL_INVERSE_HALF_PI_;
                        if (Characterization > ThresholdElongated)
                        {
                            m_Type = eElongated;
                        }
                        else if (Characterization > ThresholdTransitional)
                        {
                            m_Type = eTransitional;
                        }
                        else
                        {
                            m_Type = eCompact;
                        }
                    }
                }

                inline CharacterizationType GetCharacterizationType() const
                {
                    return m_Type;
                }

                inline void SetSegmentNode(CSaliencySegment* pSegmentNode)
                {
                    m_pSegmentNode = pSegmentNode;
                }

                inline const CSaliencySegment* GetSegmentNode() const
                {
                    return m_pSegmentNode;
                }

                inline const CSaliencySegment* GetSegmentNodeIncidence() const
                {
                    if (m_pPrevious && (m_pPrevious->m_pSegmentNode != m_pSegmentNode))
                    {
                        return m_pPrevious->m_pSegmentNode;
                    }
                    if (m_pSubsequent && (m_pSubsequent->m_pSegmentNode != m_pSegmentNode))
                    {
                        return m_pSubsequent->m_pSegmentNode;
                    }
                    return NULL;
                }

                inline CSaliencySegment* GetWritableSegmentNodeIncidence()
                {
                    if (m_pPrevious && (m_pPrevious->m_pSegmentNode != m_pSegmentNode))
                    {
                        return m_pPrevious->m_pSegmentNode;
                    }
                    if (m_pSubsequent && (m_pSubsequent->m_pSegmentNode != m_pSegmentNode))
                    {
                        return m_pSubsequent->m_pSegmentNode;
                    }
                    return NULL;
                }

                inline uint GetIndex() const
                {
                    return m_Index;
                }

                inline void SetDeviation(const real Deviation)
                {
                    m_Deviation = Deviation;
                }

                inline real GetDeviation() const
                {
                    return m_Deviation;
                }

                inline bool IsInner() const
                {
                    return (m_pSubsequent && m_pPrevious);
                }

                inline Mathematics::_2D::CVector2D GetLinkageBisectionAxis() const
                {
                    const real Angle = ((m_pPrevious->m_ContinousLocation - m_ContinousLocation).GetAbsoluteAngle() + (m_pSubsequent->m_ContinousLocation - m_ContinousLocation).GetAbsoluteAngle()) * _REAL_HALF_;
                    return Mathematics::_2D::CVector2D(RealCosinus(Angle), RealSinus(Angle));
                }

            protected:

                CharacterizationType m_Type;
                uint m_Index;
                uint m_ZoneSize;
                real m_Deviation;
                real m_Vxx;
                real m_Vxy;
                real m_Vyy;
                real m_MainRadius;
                real m_SecondaryRadius;
                Mathematics::_2D::CVector2D m_ContinousLocation;
                Mathematics::_2D::CVector2D m_SmoothContinousLocation;
                Mathematics::_2D::CVector2D m_MainAxis;
                Mathematics::_2D::CVector2D m_SecondaryAxis;
                CSaliencyPoint* m_pSubsequent;
                CSaliencyPoint* m_pPrevious;
                CSaliencySegment* m_pSegmentNode;
            };
        }
    }
}

