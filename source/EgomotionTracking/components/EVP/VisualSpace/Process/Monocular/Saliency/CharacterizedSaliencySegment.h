/*
 * CharacterizedSaliencySegment.h
 *
 *  Created on: 22.12.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../VisualSpace/Common/ContinuousBoundingBox2D.h"
#include "../../../../Foundation/DataTypes/ImageSize.h"
#include "../../../../Foundation/Mathematics/2D/Geometry/Regression/RegressionGeometricPrimitive2D.h"
#include "../../../../Foundation/Mathematics/2D/Geometry/Regression/RegressionLineSegment2D.h"
#include "../../../../Foundation/Miscellaneous/IdentifiableInstance.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            class CSaliencyBlock;
            class CSaliencyPixel;
            class CCharacterizedSaliencySegment: public CIdentifiableInstance
            {
                IDENTIFIABLE

            public:

                enum CharacterizationType
                {
                    eUndefined = 0, eCompact = 1, eTransitional = 2, eElongated = 3, eSingular = 4
                };

                enum RegressionWeightingMode
                {
                    eNonWeighting, eLinealEccentricityWeighting, ePowerEccentricityWeighting, eApetureEccentricityWeighting, ePowerApetureEccentricityWeighting
                };

                CCharacterizedSaliencySegment(CSaliencyBlock* pSaliencyBlock, CSaliencyPixel* pSaliencyPixel);
                ~CCharacterizedSaliencySegment() override;

                uint GeometricRegression(const uint MinimalPixels, const real MaximalUncertainty, const real MinimalLength, const real MaximalDeviation, const RegressionWeightingMode Mode, const real Power);

                const CSaliencyBlock* GetSaliencyBlock() const;
                const list<CSaliencyPixel*>& GetSaliencyPixels() const;
                const CContinuousBoundingBox2D& GetBoundingBox() const;
                const CContinuousBoundingBox2D& GetEigenBoundingBox() const;
                const Mathematics::_2D::CVector2D& GetGeometricCentroid() const;
                vector<Mathematics::_2D::CVector2D> GetEigenBoundingBoxPoints(const bool Axes) const;
                CharacterizationType GetCharacterizationType() const;
                const list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>& GetGeometricPrimitives() const;

                void DetermineIncidenceSegments();
                const list<CCharacterizedSaliencySegment*>& GetIncidenceSegments();

                void PerceptualOrganizationCollinearity(const CImageSize& ImageSize, const real MaximalOrientationDeviation);
                void PerceptualOrganizationEndPointContinuation(const CImageSize& ImageSize);

            protected:

                void ExtractSegment(CSaliencyPixel* pSaliencyPixel);
                void DetermineStructure();
                void ClearGeometricPrimitives();

                list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*> MultipleLineSegmentsRegression(const list<CSaliencyPixel*>& SaliencyPixels, const uint MinimalPixels, const real MaximalUncertainty, const real MinimalLength, const real MaximalDeviation, const RegressionWeightingMode Mode, const real Power);
                list<list<CSaliencyPixel*>*> SplitSubSetsSaliencyPixels(const list<CSaliencyPixel*>& SaliencyPixels);

                bool PowerApetureEccentricityWeightingLeastSquaresLineFitting(const list<CSaliencyPixel*>& SaliencyPixels, Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, Mathematics::_2D::CVector2D& A, Mathematics::_2D::CVector2D& B, Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint, real& MainAxisLength, real& SecondaryAxisLength, real& MaximalDeviation, real& MeanDeviation, const real Power);
                bool ApetureEccentricityWeightingLeastSquaresLineFitting(const list<CSaliencyPixel*>& SaliencyPixels, Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, Mathematics::_2D::CVector2D& A, Mathematics::_2D::CVector2D& B, Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint, real& MainAxisLength, real& SecondaryAxisLength, real& MaximalDeviation, real& MeanDeviation);
                bool PowerEccentricityWeightingLeastSquaresLineFitting(const list<CSaliencyPixel*>& SaliencyPixels, Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, Mathematics::_2D::CVector2D& A, Mathematics::_2D::CVector2D& B, Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint, real& MainAxisLength, real& SecondaryAxisLength, real& MaximalDeviation, real& MeanDeviation, const real Power);
                bool LinealEccentricityWeightingLeastSquaresLineFitting(const list<CSaliencyPixel*>& SaliencyPixels, Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, Mathematics::_2D::CVector2D& A, Mathematics::_2D::CVector2D& B, Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint, real& MainAxisLength, real& SecondaryAxisLength, real& MaximalDeviation, real& MeanDeviation);
                bool LeastSquaresLineFitting(const list<CSaliencyPixel*>& SaliencyPixels, Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, Mathematics::_2D::CVector2D& A, Mathematics::_2D::CVector2D& B, Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint, real& MainAxisLength, real& SecondaryAxisLength, real& MaximalDeviation, real& MeanDeviation);
                bool ManageSingularRegression(const list<CSaliencyPixel*>& SaliencyPixels, const Mathematics::_2D::CVector2D& Centroid, Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, Mathematics::_2D::CVector2D& A, Mathematics::_2D::CVector2D& B, Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint, real& MainAxisLength, real& SecondaryAxisLength, real& MaximalDeviation, real& MeanDeviation);
                void DetermineAxes(const real Slope, const real Offset, const Mathematics::_2D::CVector2D& Centroid, Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center);
                void DetemineLine(const list<CSaliencyPixel*>& SaliencyPixels, const Mathematics::_2D::CVector2D& MainAxis, const Mathematics::_2D::CVector2D& SecondaryAxis, const Mathematics::_2D::CVector2D& Center, Mathematics::_2D::CVector2D& A, Mathematics::_2D::CVector2D& B, Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint, real& MainAxisLength, real& SecondaryAxisLength, real& MaximalDeviation, real& MeanDeviation);

                bool IsFreeCircularZone(const Mathematics::_2D::CVector2D& Center, const real Radius, const list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>& RegressionGeometricPrimitives, Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D* pRegressionGeometricPrimitiveA, Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D* pRegressionGeometricPrimitiveB);

                static bool SortPixelsByEccentricityDescent(const CSaliencyPixel* plhs, const CSaliencyPixel* prhs);

                CharacterizationType m_CharacterizationType;
                CSaliencyBlock* m_pSaliencyBlock;
                list<CSaliencyPixel*> m_SaliencyPixels;
                CContinuousBoundingBox2D m_BoundingBox2D;
                CContinuousBoundingBox2D m_EigenBoundingBox2D;
                Mathematics::_2D::CVector2D m_GeometricCentroid;
                Mathematics::_2D::CVector2D m_MainAxis;
                Mathematics::_2D::CVector2D m_SecondaryAxis;
                list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*> m_RegressionGeometricPrimitives;
                list<CCharacterizedSaliencySegment*> m_IncidenceSegments;

                //TODO MAKE THEM SINGLE FILE CLASS, CAPSEL METHODS

            public:

                class CRegressionLineSegmentCollinearity2D
                {
                public:

                    Identifier m_Id;
                    Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D* m_pRegressionLineSegmentA;
                    Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D* m_pRegressionLineSegmentB;
                    Mathematics::_2D::CVector2D m_IntersectionLinesPoint;
                    Mathematics::_2D::CVector2D m_PointA;
                    Mathematics::_2D::CVector2D m_PointB;
                    Mathematics::_2D::CVector2D m_MidPointAB;
                    bool m_Active;
                    bool m_IntersectionPointAvailable;
                    real m_EndPointSignedDistance;
                    real m_GroupingZoneRadius;
                    real m_OrientationDeviation;
                    real m_EndPointAPerpendicularDistance;
                    real m_EndPointBPerpendicularDistance;
                    real m_EndPointsMaximalPerpendicularDistance;
                    real m_MaximalLinesDeviation;

                    static bool SortCollinearityByEndPointDirectDistanceDescent(const CRegressionLineSegmentCollinearity2D& lhs, const CRegressionLineSegmentCollinearity2D& rhs)
                    {
                        return lhs.m_EndPointSignedDistance < rhs.m_EndPointSignedDistance;
                    }
                };

                list<CRegressionLineSegmentCollinearity2D> m_CollinearityCandidates;

            };
        }
    }
}

