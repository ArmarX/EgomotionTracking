/*
 * SaliencyBlock.h
 *
 *  Created on: 25.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/Miscellaneous/IdentifiableInstance.h"
#include "../../../../Foundation/Mathematics/1D/ExponentialLookUpTable.h"
#include "../../../../Foundation/Mathematics/2D/Vector2D.h"
#include "../../../../VisualSpace/Common/ContinuousBoundingBox2D.h"
#include "CharacterizedSaliencySegment.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            class CSaliencyPixel;

            class CSaliencyBlock: public CIdentifiableInstance
            {
                IDENTIFIABLE

#ifdef _SALIENCYBLOCK_USE_LOOKUP_TABLE_
                DECLARE_EXPONENTIAL_LOOKUP_TABLE
#endif

            public:

                enum CharacterizationMode
                {
                    eLineal, ePower, eApeture, ePowerApeture
                };

                CSaliencyBlock(CSaliencyPixel* pSaliencyPixel);
                ~CSaliencyBlock() override;

                void SetSelected(const bool Selected);
                bool IsSelected() const;

                void AddPixel(CSaliencyPixel* pSaliencyPixel);
                real GetActiveZoneWidth() const;
                real GetActiveZoneHeigth() const;
                uint GetSize() const;
                real GetMeanSaliency() const;
                real GetRangeSaliency() const;
                real GetMaximalSaliency() const;
                real GetMinimalSaliency() const;
                bool GetApertureEccentricityRange(real& MaximalApertureEccentricity, real& MinimalApertureEccentricity) const;
                void ComputeConnectivityGrade(const int* pDeltas);
                real ComputeRoot(const bool UseContinousLocation);
                real ComputeRootingDistance();
                CSaliencyPixel* GetRoot();
                const list<CSaliencyPixel*>& GetPixels() const;
                real GetMaximalPathDistance() const;
                const CContinuousBoundingBox2D& GetBoundingBox() const;

                void Characterization(const real MaximalPathDistance, const real ThresholdTransitional, const real ThresholdElongated, const CharacterizationMode Mode, const real Power, const real ExponentFactor);
                void Segmentation();
                void GeometricRegression(const uint MinimalPixels, const real MaximalUncertainty, const real MinimalLength, const real MaximalDeviation, const CCharacterizedSaliencySegment::RegressionWeightingMode Mode, const real Power);
                void PerceptualOrganization(const CImageSize& ImageSize, const real MaximalOrientationDeviation);

                const list<CCharacterizedSaliencySegment*>& GetCharacterizedSaliencySegments() const;
                const uint* GetCharacterizedSaliencySegmentDistribution() const;
                bool IsBoundingBoxInteresecting(const CSaliencyBlock* pSaliencyBlock) const;
                real GetBoundingBoxDistance(const CSaliencyBlock* pSaliencyBlock) const;
                real GetPixelContinousLocationDistance(const CSaliencyBlock* pSaliencyBlock) const;

                list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*> GetGeometricPrimitives() const;

            protected:

                static void SortedInsert(const CSaliencyPixel* pSaliencyPixel, list<const CSaliencyPixel*>& SortedList);
                static bool SortSaliencyPixelsByDiameter(CSaliencyPixel* lhs, CSaliencyPixel* rhs);
                static bool SortSaliencyPixelsByMagnitude(CSaliencyPixel* lhs, CSaliencyPixel* rhs);

                bool m_Selected;
                CSaliencyPixel* m_pRoot;
                list<CSaliencyPixel*> m_SaliencyPixels;
                CContinuousBoundingBox2D m_BoundingBox;
                real m_AccumulatorSaliency;
                real m_MaximalSaliency;
                real m_MinimalSaliency;
                real m_MaximalPathDistance;
                uint m_ConnectivityDistribution[8];
                uint m_CharacterizedSaliencySegmentDistribution[5];

                list<CCharacterizedSaliencySegment*> m_CharacterizedSaliencySegments;
            };
        }
    }
}

