/*
 * SaliencySegment.h
 *
 *  Created on: 13.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/Mathematics/2D/StructuralTensor2D.h"
#include "../../../../Foundation/Mathematics/ND/LinearAlgebra.h"
#include "../../../Common/ContinuousBoundingBox2D.h"
#include "SaliencyPoint.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            //TODO [SALIENCY_GEOMETRY2D]
            class CSaliencyPath;

            class CSaliencySegment
            {
            public:

                enum RegressionWeightingMode
                {
                    eNonWeighting, eEccentricityWeighting, eApertureWeighting
                };

                CSaliencySegment(CSaliencyPoint* pPointNode, CSaliencyPath* pPath);

                virtual ~CSaliencySegment();

                void DetectSegmentNodeIncidences();

                inline const list<CSaliencyPoint*>& GetNodes() const
                {
                    return m_PointNodes;
                }

                inline  CSaliencyPoint::CharacterizationType GetCharacterizationType() const
                {
                    return m_CharacterizationType;
                }

                inline const list<CSaliencySegment*>& GetSegmentNodeIncidences() const
                {
                    return m_SegmentNodeIncidences;
                }

                //TODO [SALIENCY_GEOMETRY2D]
                //
                /*
                 inline TVector<CGeometricPrimitive2D*>& GetGeometricPrimitives()
                 {
                 return m_RegressionGeometricPrimitives;
                 }*/

                inline const CSaliencyPath* GetPath() const
                {
                    return m_pPath;
                }

                uint MultiLineRegression(real UncertaintyThreshold, real MinimalMainAxisLength, real MaximalSecondaryAxisLength, int MinimalPointNodes, RegressionWeightingMode Mode);

                inline const CContinuousBoundingBox2D& GetBoundingBox() const
                {
                    return m_BoundingBox;
                }

                inline  real GetLength() const
                {
                    return m_Length;
                }

                inline  real GetLinearLength() const
                {
                    return m_PointNodes.size() ? m_PointNodes.front()->GetContinousLocation().GetDistance(m_PointNodes.back()->GetContinousLocation()) : _REAL_ZERO_;
                }

                inline  real GetBoundingBoxDistance(const CSaliencySegment* pSaliencySegment)
                {
                    return m_BoundingBox.GetDistance(pSaliencySegment->m_BoundingBox);
                }

                real GetDistance(const CSaliencySegment* pSaliencySegment);

            protected:

                static bool SortPointNodeByIndex(CSaliencyPoint* plhs, CSaliencyPoint* prhs);

                //TODO [SALIENCY_GEOMETRY2D]
                //static bool SortLineSegmentByIndex(CSaliencyLineSegment* plhs, CSaliencyLineSegment* prhs);
                //TODO [SALIENCY_GEOMETRY2D]
                //inline TVector<CSaliencyLineSegment*> MultiLineRegression(TVector<CSaliencyPoint*>::iterator ppInitialNode, TVector<CSaliencyPoint*>::iterator ppFinalNode, int MinimalPointNodes, real UncertaintyThreshold, real MinimalMainAxisLength, real MaximalSecondaryAxisLength, RegressionWeightingMode Mode);

                void WeightedLeastSquaresLineFitting(Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, real& E0p, real& E0n, real& E1p, real& E1n, vector<CSaliencyPoint*>::iterator ppInitialNode, vector<CSaliencyPoint*>::iterator ppFinalNode, int TotalNodes, RegressionWeightingMode Mode);
                bool CovarianceLineFitting(Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, real& E0p, real& E0n, real& E1p, real& E1n, vector<CSaliencyPoint*>::iterator ppInitialNode, vector<CSaliencyPoint*>::iterator ppFinalNode, int TotalNodes, RegressionWeightingMode Mode);

                CSaliencyPath* m_pPath;
                CSaliencyPoint::CharacterizationType m_CharacterizationType;
                real m_Length;
                CContinuousBoundingBox2D m_BoundingBox;
                list<CSaliencyPoint*> m_PointNodes;
                list<CSaliencySegment*> m_SegmentNodeIncidences;
                //TODO [SALIENCY_GEOMETRY2D]
                //TVector<CGeometricPrimitive2D*> m_RegressionGeometricPrimitives;

            };
        }
    }
}

