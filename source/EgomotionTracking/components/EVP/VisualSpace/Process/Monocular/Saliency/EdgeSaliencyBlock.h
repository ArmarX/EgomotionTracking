/*
 * EdgeSaliencyBlock.h
 *
 *  Created on: 29.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "SaliencyBlock.h"
#include "../../../../VisualSpace/Common/Image.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            class CEdgeSaliencyBlock: public CSaliencyBlock
            {
            public:

                CEdgeSaliencyBlock(CSaliencyPixel* pSaliencyPixel);
                ~CEdgeSaliencyBlock() override;

            protected:

            };
        }
    }
}

