/*
 * SaliencyTree.h
 *
 *  Created on: 13.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/Miscellaneous/IdentifiableInstance.h"
#include "../../../Common/ContinuousBoundingBox2D.h"
#include "SaliencySegment.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            class CSaliencyBlock;
            class CSaliencyPoint;
            class CSaliencyNode;
            class CSaliencySegment;
            class CSaliencyPath;

            class CSaliencyTree: public CIdentifiableInstance
            {
                IDENTIFIABLE

            public:

                enum ExtractionMode
                {
                    eMinimalDistance, eMaximalSaliency
                };

                CSaliencyTree(CSaliencyNode* pRootSaliencyNode, const CSaliencyBlock* pSaliencyBlock, const int* pDeltas, const ExtractionMode Mode);
                ~CSaliencyTree() override;

                void Prunning(const uint PathMinimalLengthThreshold);
                void ExtractPaths(const uint PathMinimalLengthThreshold);
                void OutSpreadingPaths(const real RegularLengthDelta);
                void SmoothPaths(const real* pSmoothingKernel, const uint SmoothingRadius, const uint SmoothingDiameter);
                void SubPixelOptimizePaths(TImage<CSaliencyPixel>* pSaliencyImage, real Scope, int Iterations, bool Refinement);
                //void CharacterizationTree(const uint MaximalPathDistance, const real ThresholdTransitional, const real ThresholdElongated, const CSaliencyPoint::CharacterizationMode Mode, const real Power, const bool UseSmoothLocations);
                void CharacterizationPaths(const uint MaximalPathDistance, const real ThresholdTransitional, const real ThresholdElongated, const CSaliencyPoint::CharacterizationMode Mode, const real Power, const bool UseSmoothLocations);
                void SegmentationPaths();
                void ExtractLineSegments(real LineSegmentUncertaintyThreshold, real LineSegmentMinimalMainAxisLength, real LineSegmentMaximalSecondaryAxisLength, int LineSegmentMinimalPointNodes);

                inline  bool IsActive() const
                {
                    return m_Active;
                }

                inline void SetActive(const bool Active)
                {
                    m_Active = Active;
                }

                inline  bool IsLinked() const
                {
                    return m_Linked;
                }

                inline void SetLinked(const bool Linked)
                {
                    m_Linked = Linked;
                }

                inline  uint GetTotalNodes() const
                {
                    return m_SaliencyNodes.size();
                }

                inline const list<CSaliencyNode*>& GetNodes() const
                {
                    return m_SaliencyNodes;
                }

                inline const list<const CSaliencyNode*>& GetPathEndNodes() const
                {
                    return m_PathEndNodes;
                }

                inline const list<CSaliencyPath*>& GetPaths() const
                {
                    return m_SaliencyPaths;
                }

                inline const list<const CSaliencySegment*>& GetSegments() const
                {
                    return m_SaliencySegment;
                }

                inline CContinuousBoundingBox2D& GetBoundingBox()
                {
                    return m_BoundingBox;
                }

                bool IsGeometricSalient(const real MinimalSegmentLength);

                inline  bool Intersects(const CSaliencyTree* pSaliencyTree) const
                {
                    return m_BoundingBox.Intersects(pSaliencyTree->m_BoundingBox);
                }

                real GetBoundingBoxDistance(const CSaliencyTree* pSaliencyTree)
                {
                    return m_BoundingBox.GetDistance(pSaliencyTree->m_BoundingBox);
                }

                void GetMinimalDistanceToPathEndNodes(CSaliencyTree* pSaliencyTree, real& MinimalDistance);

                inline  bool HasSegments() const
                {
                    return m_SaliencySegment.size();
                }

                inline  bool HasSegments(const CSaliencyPoint::CharacterizationType Type) const
                {
                    return m_SegmentsCharacterizationTypeHistogram[Type];
                }

                inline  real GetAccumulativeLength() const
                {
                    return m_SegmentsCharacterizationTypeAccumulativeLength[0] + m_SegmentsCharacterizationTypeAccumulativeLength[1] + m_SegmentsCharacterizationTypeAccumulativeLength[2];
                }

                inline  bool IsElongatedDominant() const
                {
                    return m_SegmentsCharacterizationTypeAccumulativeLength[CSaliencyPoint::eElongated] > TMax(m_SegmentsCharacterizationTypeAccumulativeLength[CSaliencyPoint::eTransitional], m_SegmentsCharacterizationTypeAccumulativeLength[CSaliencyPoint::eCompact]);
                }

                inline  bool IsElongatedExtrictlyDominant(const real MaximalDominanceRatio = _REAL_HALF_) const
                {
                    return m_SegmentsCharacterizationTypeAccumulativeLength[CSaliencyPoint::eElongated] ? ((m_SegmentsCharacterizationTypeAccumulativeLength[CSaliencyPoint::eTransitional] + m_SegmentsCharacterizationTypeAccumulativeLength[CSaliencyPoint::eCompact]) / m_SegmentsCharacterizationTypeAccumulativeLength[CSaliencyPoint::eElongated]) < MaximalDominanceRatio : false;
                }

                inline  real GetAccumulativeLength(const CSaliencyPoint::CharacterizationType Type) const
                {
                    return m_SegmentsCharacterizationTypeAccumulativeLength[Type];
                }

                inline  real GetMaximalSegmentLinearLength() const
                {
                    return (m_SaliencySegment.size()) ? m_SaliencySegment.front()->GetLinearLength() : _REAL_ZERO_;
                }

            protected:

                void FilterActiveNodes(const list<CSaliencyNode*>& SaliencyNodes, list<CSaliencyNode*>& ActiveSaliencyNodes);
                void FilterDynamicLeafNodes(const list<CSaliencyNode*>& SaliencyNodes, list<CSaliencyNode*>& DynamicLeafSaliencyNodes);
                void BasicPrunning();
                void ExtendedPrunning(const uint PathMinimalLengthThreshold);
                void ExpansionMaximalSaliencyBased(CSaliencyNode* pRootSaliencyNode, const int* pDeltas);
                void ExpansionMinimalDistanceBased(CSaliencyNode* pRootSaliencyNode, const int* pDeltas);
                void SortedInsertionByHeuristic(list<CSaliencyNode*>& HeuristicSortedList, CSaliencyNode* pSaliencyNode, const real Heuristic);

                static bool SortSaliencySegmentByLenghtDescending(const CSaliencySegment* plhs, const CSaliencySegment* prhs);
                static bool SortRedundantPathRoots(CSaliencyNode* plhs, CSaliencyNode* prhs);

                bool m_Active;
                bool m_Linked;
                const CSaliencyBlock* m_pSaliencyBlock;
                uint m_SegmentsCharacterizationTypeHistogram[3];
                real m_SegmentsCharacterizationTypeAccumulativeLength[3];
                uint m_DescendancyDistributionHistogram[8];
                list<CSaliencyNode*> m_SaliencyNodes;
                list<const CSaliencyNode*> m_PathEndNodes;
                list<const CSaliencySegment*> m_SaliencySegment;
                list<CSaliencyPath*> m_SaliencyPaths;
                CContinuousBoundingBox2D m_BoundingBox;
            };
        }
    }
}

