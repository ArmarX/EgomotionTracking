/*
 * MonocularEdgeSaliencyAnalyzer.h
 *
 *  Created on: 17.12.2011
 *      Author: gonzalez
 */

#pragma once

#include "MonocularSaliencyAnalyzer.h"
#include "../../../Kernels/ConvolutionGaussianKernel1D.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                class CMonocularEdgeSaliencyAnalyzer: public CMonocularSaliencyAnalyzer
                {
                public:

                    class CParameters: public CParametersBase
                    {
                    public:

                        CParameters(CMonocularEdgeSaliencyAnalyzer* pHost) :
                            CParametersBase(), m_pHost(pHost), m_TreeExpansionMode(Saliency::CSaliencyTree::eMinimalDistance), m_CharacterizationMode(Saliency::CSaliencyPoint::eLineal), m_MinimalPathLength(8), m_SpreadRegularizationDelta(real(2.0 / 3.0)), m_CharacterizationGraphDistance(24), m_CharaterizationThresholdTransitional(3), m_CharaterizationThresholdElongated(8), m_CharaterizationPower(_REAL_TWO_), m_EnableSmoothingPaths(true), m_PathSmoothingSigma(real(2.0)), m_LineSegmentUncertaintyThreshold(real(0.125)), m_LineSegmentMinimalMainAxisLength(real(4.0)), m_LineSegmentMaximalSecondaryAxisLength(real(4.0)), m_LineSegmentMinimalPointNodes(16)
                        {
                        }

                        ~CParameters() override;

                        CParametersBase::ParameterChangeResult SetTreeExpansionMode(const Saliency::CSaliencyTree::ExtractionMode Mode, const bool Wait = true);
                        Saliency::CSaliencyTree::ExtractionMode GetTreeExpansionMode() const;

                        CParametersBase::ParameterChangeResult SetMinimalPathLength(const uint MinimalPathLength, const bool Wait = true);
                        uint GetMinimalPathLength() const;

                        CParametersBase::ParameterChangeResult SetCharacterizationMode(Saliency::CSaliencyPoint::CharacterizationMode Mode, const bool Wait = true);
                        Saliency::CSaliencyPoint::CharacterizationMode GetCharacterizationMode() const;

                        CParametersBase::ParameterChangeResult SetSpreadRegularizationDelta(const real SpreadRegularizationDelta, const bool Wait = true);
                        real GetSpreadRegularizationDelta() const;

                        CParametersBase::ParameterChangeResult SetCharacterizationGraphDistance(const uint CharacterizationGraphDistance, const bool Wait = true);
                        uint GetCharacterizationGraphDistance() const;

                        CParametersBase::ParameterChangeResult SetCharaterizationThresholdTransitional(const real CharaterizationThresholdTransitional, const bool Wait = true);
                        real GetCharaterizationThresholdTransitional() const;

                        CParametersBase::ParameterChangeResult SetCharaterizationThresholdElongated(const real CharaterizationThresholdElongated, const bool Wait = true);
                        real GetCharaterizationThresholdElongated() const;

                        CParametersBase::ParameterChangeResult SetCharaterizationPower(const real CharaterizationPower, const bool Wait = true);
                        real GetCharaterizationPower() const;

                        CParametersBase::ParameterChangeResult SetEnableSmoothingPaths(const bool EnableSmoothingPaths, const bool Wait = true);
                        bool GetEnableSmoothingPaths() const;

                        CParametersBase::ParameterChangeResult SetPathSmoothingSigma(const real PathSmoothingSigma, const bool Wait = true);
                        real GetPathSmoothingSigma() const;

                        CParametersBase::ParameterChangeResult SetLineSegmentUncertaintyThreshold(const real LineSegmentUncertaintyThreshold, const bool Wait = true);
                        real GetLineSegmentUncertaintyThreshold() const;

                        CParametersBase::ParameterChangeResult SetLineSegmentMinimalMainAxisLength(const real LineSegmentMinimalMainAxisLength, const bool Wait = true);
                        real GetLineSegmentMinimalMainAxisLength() const;

                        CParametersBase::ParameterChangeResult SetLineSegmentMaximalSecondaryAxisLength(const real LineSegmentMaximalSecondaryAxisLength, const bool Wait = true);
                        real GetLineSegmentMaximalSecondaryAxisLength() const;

                        CParametersBase::ParameterChangeResult SetLineSegmentMinimalPointNodes(const real LineSegmentMinimalPointNodes, const bool Wait = true);
                        real GetLineSegmentMinimalPointNodes() const;

                    protected:

                        CMonocularEdgeSaliencyAnalyzer* m_pHost;

                        Saliency::CSaliencyTree::ExtractionMode m_TreeExpansionMode;
                        Saliency::CSaliencyPoint::CharacterizationMode m_CharacterizationMode;
                        uint m_MinimalPathLength;
                        real m_SpreadRegularizationDelta;
                        uint m_CharacterizationGraphDistance;
                        real m_CharaterizationThresholdTransitional;
                        real m_CharaterizationThresholdElongated;
                        real m_CharaterizationPower;
                        bool m_EnableSmoothingPaths;
                        real m_PathSmoothingSigma;
                        real m_LineSegmentUncertaintyThreshold;
                        real m_LineSegmentMinimalMainAxisLength;
                        real m_LineSegmentMaximalSecondaryAxisLength;
                        uint m_LineSegmentMinimalPointNodes;
                    };

                    CMonocularEdgeSaliencyAnalyzer(CMonocularSaliencyExtractor* pMonocularSaliencyExtractor);
                    ~CMonocularEdgeSaliencyAnalyzer() override;

                    bool Execute(const Identifier TrialId) override;
                    bool Display() override;
                    CParameters* GetParameters() override;

                protected:

                    virtual  bool ExtractTrees(const Identifier TrialId);

                    CParameters m_Parameters;
                    Kernels::CConvolutionGaussianKernel1D m_SmoothingKernel;
                };
            }
        }
    }
}

