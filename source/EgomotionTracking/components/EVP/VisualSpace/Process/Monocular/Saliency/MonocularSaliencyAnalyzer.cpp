/*
 * MonocularSaliencyAnalyzer.cpp
 *
 *  Created on: 16.12.2011
 *      Author: gonzalez
 */

#include "MonocularSaliencyAnalyzer.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                CMonocularSaliencyAnalyzer::CMonocularSaliencyAnalyzer(const CImageProcessBase::ProcessTypeId TypeId, CMonocularSaliencyExtractor* pMonocularSaliencyExtractor) :
                    TImageProcessBase<Saliency::CSaliencyPixel, Saliency::CSaliencyNode, CDiscreteTristimulusPixel> (TypeId, (pMonocularSaliencyExtractor ? pMonocularSaliencyExtractor->GetOutputActiveZone() : nullptr), (pMonocularSaliencyExtractor ? pMonocularSaliencyExtractor->GetOutputImageReadOnly() : nullptr), (pMonocularSaliencyExtractor ? pMonocularSaliencyExtractor->GetExecutionMutex() : nullptr)), m_pMonocularSaliencyExtractor(pMonocularSaliencyExtractor)
                {
                    if (m_IsEnabled)
                    {
                        const CImageSize& Size = m_pInputActiveZone->GetSize();
                        m_pOutputImage = new TImage<Saliency::CSaliencyNode> (Size);
                        m_pDisplayImage = new TImage<CDiscreteTristimulusPixel> (Size);
                        Saliency::CSaliencyPixel* pSaliencyPixel = const_cast<Saliency::CSaliencyPixel*>(m_pExternalInputImage->GetBeginReadOnlyBuffer());
                        Saliency::CSaliencyNode* pSaliencyNode = m_pOutputImage->GetBeginWritableBuffer();
                        const uint Height = Size.GetHeight();
                        const uint Width = Size.GetWidth();
                        for (uint y = 0; y < Height; ++y)
                            for (uint x = 0; x < Width; ++x, ++pSaliencyNode)
                            {
                                pSaliencyNode->SetSourcePixel(pSaliencyPixel++);
                            }
                        m_OutputActiveZone.SetConnection(m_pInputActiveZone);
                    }
                }

                CMonocularSaliencyAnalyzer::~CMonocularSaliencyAnalyzer()
                {
                    if (m_IsEnabled)
                    {
                        ClearForest();
                        RELEASE_OBJECT(m_pOutputImage)
                        RELEASE_OBJECT(m_pDisplayImage)
                    }
                }

                const list<Saliency::CSaliencyTree*>& CMonocularSaliencyAnalyzer::GetForest()
                {
                    return m_Forest;
                }

                bool CMonocularSaliencyAnalyzer::ExportForest(const_string pPathFileName, const CImageActiveZone* pImageActiveZone, const VisualizationGroup Flags, const VisualizationMode Mode, const bool DisplaySmoothedPointLocations)
                {
                    if (pPathFileName && Flags && m_Forest.size() && m_IsEnabled && m_OutputActiveZone.IsEnabled() && GetTotalExecutions())
                    {
                        Displaying::SVG::CSVGGraphic Plotter(m_OutputActiveZone.GetWidth(), m_OutputActiveZone.GetHeight());
                        switch (Mode)
                        {
                            case eBylayer:
                                ExportForestByLayer(Plotter, pImageActiveZone, Flags, DisplaySmoothedPointLocations);
                                break;
                            case eByTree:
                                ExportForestByTree(Plotter, pImageActiveZone, Flags, DisplaySmoothedPointLocations);
                                break;
                        }
                        return Plotter.SaveFile(Displaying::SVG::CSVGPrimitive::eSVG, pPathFileName);
                    }
                    return false;
                }

                void CMonocularSaliencyAnalyzer::ClearForest()
                {
                    if (m_Forest.size())
                    {
                        list<Saliency::CSaliencyTree*>::const_iterator EndTrees = m_Forest.end();
                        for (list<Saliency::CSaliencyTree*>::const_iterator ppSaliencyTree = m_Forest.begin(); ppSaliencyTree != EndTrees; ++ppSaliencyTree)
                            RELEASE_OBJECT_DIRECT(*ppSaliencyTree)
                            m_Forest.clear();
                    }
                }

                void CMonocularSaliencyAnalyzer::DisplayNodes(Displaying::SVG::CSVGGroup* pGroupNodes, Saliency::CSaliencyTree* pSaliencyTree, const real NodeRadius, const CRGBAColor& NodeColor)
                {
                    list<Saliency::CSaliencyNode*>::const_iterator EndNodes = pSaliencyTree->GetNodes().end();
                    for (list<Saliency::CSaliencyNode*>::const_iterator ppSaliencyNode = pSaliencyTree->GetNodes().begin(); ppSaliencyNode != EndNodes; ++ppSaliencyNode)
                    {
                        pGroupNodes->Add(new Displaying::SVG::CSVGRectangle((*ppSaliencyNode)->GetSourceDiscreteLocation(), NodeRadius, NodeColor, NodeColor, _REAL_ZERO_, Displaying::SVG::CSVGPrimitive::eOnlyFill));
                    }
                }

                void CMonocularSaliencyAnalyzer::DisplayLinks(Displaying::SVG::CSVGGroup* pGroupLinks, Saliency::CSaliencyTree* pSaliencyTree, const real ActiveConnectionLinkWidth, const real InactiveConnectionLinkWidth, const real RootNodeRadius, const CRGBAColor& ConnectionNodeColor, const CRGBAColor& RootNodeColor)
                {
                    list<Saliency::CSaliencyNode*>::const_iterator EndNodes = pSaliencyTree->GetNodes().end();
                    for (list<Saliency::CSaliencyNode*>::const_iterator ppSaliencyNode = pSaliencyTree->GetNodes().begin(); ppSaliencyNode != EndNodes; ++ppSaliencyNode)
                    {
                        const Saliency::CSaliencyNode* pSaliencyNode = *ppSaliencyNode;
                        const Saliency::CSaliencyNode* pParentSaliencyNode = pSaliencyNode->GetReadOnlyTreeParent();
                        if (pParentSaliencyNode)
                        {
                            pGroupLinks->Add(new Displaying::SVG::CSVGLine(pSaliencyNode->GetSourceDiscreteLocation(), pParentSaliencyNode->GetSourceDiscreteLocation(), pParentSaliencyNode->IsActive() ? ActiveConnectionLinkWidth : InactiveConnectionLinkWidth, ConnectionNodeColor));
                        }
                        else
                        {
                            pGroupLinks->Add(new Displaying::SVG::CSVGCircle(pSaliencyNode->GetSourceDiscreteLocation(), RootNodeRadius, ActiveConnectionLinkWidth, RootNodeColor, RootNodeColor, Displaying::SVG::CSVGPrimitive::eOnlyStroke));
                        }
                    }
                }

                void CMonocularSaliencyAnalyzer::DisplayOptimizationNodes(Displaying::SVG::CSVGGroup* pGroupOptimizationNodes, Saliency::CSaliencyTree* pSaliencyTree, const real OptimizedNodeRadius, const CRGBAColor& OptimizedNodeColor)
                {
                    list<Saliency::CSaliencyNode*>::const_iterator EndNodes = pSaliencyTree->GetNodes().end();
                    for (list<Saliency::CSaliencyNode*>::const_iterator ppSaliencyNode = pSaliencyTree->GetNodes().begin(); ppSaliencyNode != EndNodes; ++ppSaliencyNode)
                    {
                        pGroupOptimizationNodes->Add(new Displaying::SVG::CSVGCircle((*ppSaliencyNode)->GetSourceContinousLocation(), OptimizedNodeRadius, _REAL_ZERO_, OptimizedNodeColor, OptimizedNodeColor, Displaying::SVG::CSVGPrimitive::eOnlyFill));
                    }
                }

                void CMonocularSaliencyAnalyzer::DisplayOptimizationLinks(Displaying::SVG::CSVGGroup* pGroupOptimizationLinks, Saliency::CSaliencyTree* pSaliencyTree, const real OptimizationConnectionLinkWidth, const CRGBAColor& OptimizationConnectionNodeColor)
                {
                    list<Saliency::CSaliencyNode*>::const_iterator EndNodes = pSaliencyTree->GetNodes().end();
                    for (list<Saliency::CSaliencyNode*>::const_iterator ppSaliencyNode = pSaliencyTree->GetNodes().begin(); ppSaliencyNode != EndNodes; ++ppSaliencyNode)
                    {
                        pGroupOptimizationLinks->Add(new Displaying::SVG::CSVGLine((*ppSaliencyNode)->GetSourceDiscreteLocation(), (*ppSaliencyNode)->GetSourceContinousLocation(), OptimizationConnectionLinkWidth, OptimizationConnectionNodeColor));
                    }
                }

                void CMonocularSaliencyAnalyzer::DisplayPoints(Displaying::SVG::CSVGGroup* pGroupPoints, Saliency::CSaliencyTree* pSaliencyTree, const real PointRadius, const CRGBAColor* pCharacterizationColors, const bool DisplaySmoothedPointLocations)
                {
                    list<Saliency::CSaliencyPath*>::const_iterator EndPaths = pSaliencyTree->GetPaths().end();
                    if (DisplaySmoothedPointLocations)
                        for (list<Saliency::CSaliencyPath*>::const_iterator ppSaliencyPath = pSaliencyTree->GetPaths().begin(); ppSaliencyPath != EndPaths; ++ppSaliencyPath)
                        {
                            vector<Saliency::CSaliencyPoint*>::const_iterator EndPoints = (*ppSaliencyPath)->GetPoints().end();
                            for (vector<Saliency::CSaliencyPoint*>::const_iterator ppSaliencyPoint = (*ppSaliencyPath)->GetPoints().begin(); ppSaliencyPoint != EndPoints; ++ppSaliencyPoint)
                            {
                                Saliency::CSaliencyPoint* pSaliencyPoint = *ppSaliencyPoint;
                                pGroupPoints->Add(new Displaying::SVG::CSVGCircle(pSaliencyPoint->GetSmoothContinousLocation(), PointRadius, _REAL_ZERO_, pCharacterizationColors[pSaliencyPoint->GetCharacterizationType()], pCharacterizationColors[pSaliencyPoint->GetCharacterizationType()], Displaying::SVG::CSVGPrimitive::eOnlyFill));
                            }
                        }
                    else
                        for (list<Saliency::CSaliencyPath*>::const_iterator ppSaliencyPath = pSaliencyTree->GetPaths().begin(); ppSaliencyPath != EndPaths; ++ppSaliencyPath)
                        {
                            vector<Saliency::CSaliencyPoint*>::const_iterator EndPoints = (*ppSaliencyPath)->GetPoints().end();
                            for (vector<Saliency::CSaliencyPoint*>::const_iterator ppSaliencyPoint = (*ppSaliencyPath)->GetPoints().begin(); ppSaliencyPoint != EndPoints; ++ppSaliencyPoint)
                            {
                                Saliency::CSaliencyPoint* pSaliencyPoint = *ppSaliencyPoint;
                                pGroupPoints->Add(new Displaying::SVG::CSVGCircle(pSaliencyPoint->GetContinousLocation(), PointRadius, _REAL_ZERO_, pCharacterizationColors[pSaliencyPoint->GetCharacterizationType()], pCharacterizationColors[pSaliencyPoint->GetCharacterizationType()], Displaying::SVG::CSVGPrimitive::eOnlyFill));
                            }
                        }
                }

                void CMonocularSaliencyAnalyzer::DisplayEllipses(Displaying::SVG::CSVGGroup* pGroupEllipses, Saliency::CSaliencyTree* pSaliencyTree, const real EllipseStrokeWidth, const CRGBAColor* pCharacterizationColors, const bool DisplaySmoothedPointLocations)
                {
                    list<Saliency::CSaliencyPath*>::const_iterator EndPaths = pSaliencyTree->GetPaths().end();
                    if (DisplaySmoothedPointLocations)
                        for (list<Saliency::CSaliencyPath*>::const_iterator ppSaliencyPath = pSaliencyTree->GetPaths().begin(); ppSaliencyPath != EndPaths; ++ppSaliencyPath)
                        {
                            vector<Saliency::CSaliencyPoint*>::const_iterator EndPoints = (*ppSaliencyPath)->GetPoints().end();
                            for (vector<Saliency::CSaliencyPoint*>::const_iterator ppSaliencyPoint = (*ppSaliencyPath)->GetPoints().begin(); ppSaliencyPoint != EndPoints; ++ppSaliencyPoint)
                            {
                                Saliency::CSaliencyPoint* pSaliencyPoint = *ppSaliencyPoint;
                                pGroupEllipses->Add(new Displaying::SVG::CSVGEllipse(pSaliencyPoint->GetSmoothContinousLocation(), pSaliencyPoint->GetMainRadius(), pSaliencyPoint->GetSecondaryRadius(), pSaliencyPoint->GetMainAxis().GetAngle(), EllipseStrokeWidth, pCharacterizationColors[pSaliencyPoint->GetCharacterizationType()], pCharacterizationColors[pSaliencyPoint->GetCharacterizationType()], Displaying::SVG::CSVGPrimitive::eOnlyStroke));
                            }
                        }
                    else
                        for (list<Saliency::CSaliencyPath*>::const_iterator ppSaliencyPath = pSaliencyTree->GetPaths().begin(); ppSaliencyPath != EndPaths; ++ppSaliencyPath)
                        {
                            vector<Saliency::CSaliencyPoint*>::const_iterator EndPoints = (*ppSaliencyPath)->GetPoints().end();
                            for (vector<Saliency::CSaliencyPoint*>::const_iterator ppSaliencyPoint = (*ppSaliencyPath)->GetPoints().begin(); ppSaliencyPoint != EndPoints; ++ppSaliencyPoint)
                            {
                                Saliency::CSaliencyPoint* pSaliencyPoint = *ppSaliencyPoint;
                                pGroupEllipses->Add(new Displaying::SVG::CSVGEllipse(pSaliencyPoint->GetContinousLocation(), pSaliencyPoint->GetMainRadius(), pSaliencyPoint->GetSecondaryRadius(), pSaliencyPoint->GetMainAxis().GetAngle(), EllipseStrokeWidth, pCharacterizationColors[pSaliencyPoint->GetCharacterizationType()], pCharacterizationColors[pSaliencyPoint->GetCharacterizationType()], Displaying::SVG::CSVGPrimitive::eOnlyStroke));
                            }
                        }
                }

                void CMonocularSaliencyAnalyzer::DisplayPointLinks(Displaying::SVG::CSVGGroup* pGroupPointLinks, Saliency::CSaliencyTree* pSaliencyTree, const real PointConnectionLinkWidth, const CRGBAColor& PointConnectionColor, const bool DisplaySmoothedPointLocations)
                {
                    list<Saliency::CSaliencyPath*>::const_iterator EndPaths = pSaliencyTree->GetPaths().end();
                    if (DisplaySmoothedPointLocations)
                        for (list<Saliency::CSaliencyPath*>::const_iterator ppSaliencyPath = pSaliencyTree->GetPaths().begin(); ppSaliencyPath != EndPaths; ++ppSaliencyPath)
                        {
                            vector<Saliency::CSaliencyPoint*>::const_iterator EndPoints = (*ppSaliencyPath)->GetPoints().end();
                            for (vector<Saliency::CSaliencyPoint*>::const_iterator ppSaliencyPoint = (*ppSaliencyPath)->GetPoints().begin(); ppSaliencyPoint != EndPoints; ++ppSaliencyPoint)
                            {
                                Saliency::CSaliencyPoint* pSaliencyPoint = *ppSaliencyPoint;
                                const Saliency::CSaliencyPoint* pSaliencySubsequentPoint = pSaliencyPoint->GetSubsequentPoint();
                                if (pSaliencySubsequentPoint)
                                {
                                    pGroupPointLinks->Add(new Displaying::SVG::CSVGLine(pSaliencyPoint->GetSmoothContinousLocation(), pSaliencySubsequentPoint->GetSmoothContinousLocation(), PointConnectionLinkWidth, PointConnectionColor));
                                }
                            }
                        }
                    else
                        for (list<Saliency::CSaliencyPath*>::const_iterator ppSaliencyPath = pSaliencyTree->GetPaths().begin(); ppSaliencyPath != EndPaths; ++ppSaliencyPath)
                        {
                            vector<Saliency::CSaliencyPoint*>::const_iterator EndPoints = (*ppSaliencyPath)->GetPoints().end();
                            for (vector<Saliency::CSaliencyPoint*>::const_iterator ppSaliencyPoint = (*ppSaliencyPath)->GetPoints().begin(); ppSaliencyPoint != EndPoints; ++ppSaliencyPoint)
                            {
                                Saliency::CSaliencyPoint* pSaliencyPoint = *ppSaliencyPoint;
                                const Saliency::CSaliencyPoint* pSaliencySubsequentPoint = pSaliencyPoint->GetSubsequentPoint();
                                if (pSaliencySubsequentPoint)
                                {
                                    pGroupPointLinks->Add(new Displaying::SVG::CSVGLine(pSaliencyPoint->GetContinousLocation(), pSaliencySubsequentPoint->GetContinousLocation(), PointConnectionLinkWidth, PointConnectionColor));
                                }
                            }
                        }
                }

                void CMonocularSaliencyAnalyzer::DisplaySegments(Displaying::SVG::CSVGGroup* pGroupSegments, Saliency::CSaliencyTree* pSaliencyTree)
                {
                    list<Saliency::CSaliencyPath*>::const_iterator EndPaths = pSaliencyTree->GetPaths().end();
                    for (list<Saliency::CSaliencyPath*>::const_iterator ppSaliencyPath = pSaliencyTree->GetPaths().begin(); ppSaliencyPath != EndPaths; ++ppSaliencyPath)
                    {
                        list<Saliency::CSaliencySegment*>::const_iterator EndSegment = (*ppSaliencyPath)->GetSegments().end();
                        for (list<Saliency::CSaliencySegment*>::const_iterator ppSaliencySegment = (*ppSaliencyPath)->GetSegments().begin(); ppSaliencySegment != EndSegment; ++ppSaliencySegment)
                        {
                            //Saliency::CSaliencySegment* pSaliencySegment = *ppSaliencySegment;
                        }
                    }
                }

                void CMonocularSaliencyAnalyzer::ExportForestByLayer(Displaying::SVG::CSVGGraphic& Plotter, const CImageActiveZone* pImageActiveZone, const VisualizationGroup Flags, const bool DisplaySmoothedPointLocations)
                {
                    Displaying::SVG::CSVGGroup* pGroupLinks = (Flags & eLinks) ? Plotter.CreateGroup() : nullptr;
                    Displaying::SVG::CSVGGroup* pGroupOptimizationLinks = (Flags & eOptimizationLinks) ? Plotter.CreateGroup() : nullptr;
                    Displaying::SVG::CSVGGroup* pGroupNodes = (Flags & eNodes) ? Plotter.CreateGroup() : nullptr;
                    Displaying::SVG::CSVGGroup* pGroupOptimizationNodes = (Flags & eOptimizationNodes) ? Plotter.CreateGroup() : nullptr;
                    Displaying::SVG::CSVGGroup* pGroupEllipses = (Flags & eEllipses) ? Plotter.CreateGroup() : nullptr;
                    Displaying::SVG::CSVGGroup* pGroupPointLinks = (Flags & ePointLinks) ? Plotter.CreateGroup() : nullptr;
                    Displaying::SVG::CSVGGroup* pGroupPoints = (Flags & ePoints) ? Plotter.CreateGroup() : nullptr;
                    //Displaying::SVG::CSVGGroup* pGroupSegments = (Flags & eSegments) ? Plotter.CreateGroup() : NULL;

                    const CRGBAColor RootNodeColor(255, 0, 0, 255);
                    const CRGBAColor NodeColor(0, 0, 0, 255);
                    const CRGBAColor OptimizedNodeColor(32, 32, 32, 255);
                    const CRGBAColor ConnectionNodeColor(64, 64, 64, 255);
                    const CRGBAColor OptimizationConnectionNodeColor(128, 128, 128, 255);
                    const CRGBAColor PointConnectionColor(128, 128, 128, 128);
                    const CRGBAColor EllipseCharacterizationColors[4] = { CRGBAColor(255, 0, 0, 128), CRGBAColor(0, 255, 0, 128), CRGBAColor(0, 0, 255, 128), CRGBAColor(255, 0, 255, 255) };
                    const CRGBAColor CircleCharacterizationColors[4] = { CRGBAColor(255, 0, 0, 255), CRGBAColor(0, 255, 0, 255), CRGBAColor(0, 0, 255, 255), CRGBAColor(255, 0, 255, 255) };
                    const real RootNodeRadius = real(0.2);
                    const real NodeRadius = RootNodeRadius;
                    const real OptimizedNodeRadius = NodeRadius * _REAL_FOURTH_;
                    const real PointRadius = OptimizedNodeRadius * _REAL_HALF_;
                    const real ActiveConnectionLinkWidth = NodeRadius * _REAL_HALF_;
                    const real InactiveConnectionLinkWidth = ActiveConnectionLinkWidth * _REAL_HALF_;
                    const real EllipseStrokeWidth = ActiveConnectionLinkWidth * _REAL_HALF_;
                    const real OptimizationConnectionLinkWidth = InactiveConnectionLinkWidth * _REAL_HALF_;
                    const real PointConnectionLinkWidth = ActiveConnectionLinkWidth;

                    list<Saliency::CSaliencyTree*>::const_iterator EndTrees = m_Forest.end();
                    for (list<Saliency::CSaliencyTree*>::const_iterator ppSaliencyTree = m_Forest.begin(); ppSaliencyTree != EndTrees; ++ppSaliencyTree)
                    {
                        Saliency::CSaliencyTree* pSaliencyTree = *ppSaliencyTree;
                        if (pSaliencyTree->IsActive() && ((pImageActiveZone && pImageActiveZone->IsPartiallyInside(pSaliencyTree->GetBoundingBox())) || (!pImageActiveZone)))
                        {
                            if (pGroupNodes)
                            {
                                DisplayNodes(pGroupNodes, pSaliencyTree, NodeRadius, NodeColor);
                            }
                            if (pGroupLinks)
                            {
                                DisplayLinks(pGroupLinks, pSaliencyTree, ActiveConnectionLinkWidth, InactiveConnectionLinkWidth, RootNodeRadius, ConnectionNodeColor, RootNodeColor);
                            }
                            if (pGroupOptimizationNodes)
                            {
                                DisplayOptimizationNodes(pGroupOptimizationNodes, pSaliencyTree, OptimizedNodeRadius, OptimizedNodeColor);
                            }
                            if (pGroupOptimizationLinks)
                            {
                                DisplayOptimizationLinks(pGroupOptimizationLinks, pSaliencyTree, OptimizationConnectionLinkWidth, OptimizationConnectionNodeColor);
                            }
                            if (pGroupEllipses)
                            {
                                DisplayEllipses(pGroupEllipses, pSaliencyTree, EllipseStrokeWidth, EllipseCharacterizationColors, DisplaySmoothedPointLocations);
                            }
                            if (pGroupPoints)
                            {
                                DisplayPoints(pGroupPoints, pSaliencyTree, PointRadius, CircleCharacterizationColors, DisplaySmoothedPointLocations);
                            }
                            if (pGroupPointLinks)
                            {
                                DisplayPointLinks(pGroupPointLinks, pSaliencyTree, PointConnectionLinkWidth, PointConnectionColor, DisplaySmoothedPointLocations);
                            }
                            /*if (pGroupSegments)
                             DisplaySegments(pGroupSegments, pSaliencyTree);*/
                        }
                    }
                }

                void CMonocularSaliencyAnalyzer::ExportForestByTree(Displaying::SVG::CSVGGraphic& Plotter, const CImageActiveZone* pImageActiveZone, const VisualizationGroup Flags, const bool DisplaySmoothedPointLocations)
                {
                    /*const CRGBAColor RootNodeColor(255, 0, 0, 255);
                     const CRGBAColor NodeColor(0, 0, 0, 255);
                     const CRGBAColor OptimizedNodeColor(32, 32, 32, 255);
                     const CRGBAColor ConnectionNodeColor(64, 64, 64, 255);
                     const CRGBAColor OptimizationConnectionNodeColor(128, 128, 128, 255);
                     const CRGBAColor PointConnectionColor(196, 196, 196, 255);
                     const CRGBAColor CharacterizationColors[4] = { CRGBAColor(255, 0, 0, 255), CRGBAColor(0, 255, 0, 255), CRGBAColor(0, 0, 255, 255), CRGBAColor(255, 0, 255, 255) };
                     const real RootNodeRadius = real(0.5);
                     const real NodeRadius = RootNodeRadius * _REAL_HALF_;
                     const real OptimizedNodeRadius = NodeRadius;
                     const real PointRadius = NodeRadius * _REAL_HALF_;
                     const real ActiveConnectionLinkWidth = NodeRadius * _REAL_HALF_;
                     const real InactiveConnectionLinkWidth = ActiveConnectionLinkWidth * _REAL_HALF_;
                     const real EllipseStrokeWidth = ActiveConnectionLinkWidth;
                     const real OptimizationConnectionLinkWidth = ActiveConnectionLinkWidth;
                     const real PointConnectionLinkWidth = ActiveConnectionLinkWidth;

                     list<Saliency::CSaliencyTree*>::const_iterator EndTrees = m_Forest.end();
                     for (list<Saliency::CSaliencyTree*>::const_iterator pppSaliencyTree = m_Forest.begin(); pppSaliencyTree != EndTrees; ++pppSaliencyTree)
                     {
                     Saliency::CSaliencyTree* pSaliencyTree = *pppSaliencyTree;
                     if (pSaliencyTree->IsActive() && ((pImageActiveZone && pImageActiveZone->IsPartiallyInside(pSaliencyTree->GetBoundingBox())) || (!pImageActiveZone)))
                     {
                     Displaying::SVG::CSVGGroup* pGroup = Plotter.CreateGroup();
                     if (Flags & eNodes)
                     DisplayNodes(pGroup->CreateGroup(), pSaliencyTree, NodeRadius, NodeColor);
                     if (Flags & eLinks)
                     DisplayLinks(pGroup->CreateGroup(), pSaliencyTree, ActiveConnectionLinkWidth, InactiveConnectionLinkWidth, RootNodeRadius, ConnectionNodeColor, RootNodeColor);
                     if (Flags & eOptimizationNodes)
                     DisplayOptimizationNodes(pGroup->CreateGroup(), pSaliencyTree, OptimizedNodeRadius, OptimizedNodeColor);
                     if (Flags & eOptimizationLinks)
                     DisplayOptimizationLinks(pGroup->CreateGroup(), pSaliencyTree, OptimizationConnectionLinkWidth, OptimizationConnectionNodeColor);
                     if (Flags & ePoints)
                     DisplayPoints(pGroup->CreateGroup(), pSaliencyTree, PointRadius, EllipseStrokeWidth, CharacterizationColors, DisplaySmoothedPointLocations);
                     if (Flags & ePointLinks)
                     DisplayPointLinks(pGroup->CreateGroup(), pSaliencyTree, PointConnectionLinkWidth, PointConnectionColor, DisplaySmoothedPointLocations);
                     //if (Flags & eSegments)
                     //DisplaySegments(pGroup->CreateGroup(), pSaliencyTree);
                     }
                     }*/
                }

            }
        }
    }
}
