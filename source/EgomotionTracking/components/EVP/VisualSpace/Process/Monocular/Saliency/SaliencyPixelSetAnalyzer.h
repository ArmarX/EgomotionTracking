/*
 * SaliencyPixelSetAnalyzer.h
 *
 *  Created on: 25.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/Mathematics/1D/Common1D.h"
#include "SaliencyPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            class CSaliencyPixelSetAnalyzer
            {
            public:

                enum SaliencySource
                {
                    eDirectSaliency, eOptimizedSaliency
                };

                CSaliencyPixelSetAnalyzer();
                virtual ~CSaliencyPixelSetAnalyzer();

                bool AnalyzePercentile(const SaliencySource Source);
                real GetSaliencyMagitudeAtPercentileReference(const real PercentileReference, const SaliencySource Source);
                bool ExportPercentileCurve(const_string pPathFileName, const uint Intervals, const SaliencySource Source);
                bool ExportPDF(const_string pPathFileName, const uint Intervals, const real SigmaFactor, const SaliencySource Source);

                void AddPixel(CSaliencyPixel* pSaliencyPixel);
                void Clear();

                const list<CSaliencyPixel*>& GetPixels() const;
                real GetTotalSaliency() const;
                real GetRangeSaliency() const;
                real GetMeanSaliency() const;
                uint GetTotalPixels() const;

            protected:

                static bool SortSaliencyPixelByOptimizedMagnitude(const CSaliencyPixel* plhs, const CSaliencyPixel* prhs);
                static bool SortSaliencyPixelByMagnitude(const CSaliencyPixel* plhs, const CSaliencyPixel* prhs);

                real m_TotalSaliency;
                real m_MaximalSaliency;
                real m_MinimalSaliency;
                list<CSaliencyPixel*> m_Pixels;
            };
        }
    }
}

