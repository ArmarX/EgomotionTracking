/*
 * MonocularGaborEdgeSaliencyExtractor.h
 *
 *  Created on: 05.12.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../Foundation/Mathematics/1D/Common1D.h"
#include "../../../../Foundation/Mathematics/2D/Geometry/LineSegment2D.h"
#include "../../../../Foundation/Mathematics/2D/Geometry/Point2D.h"
#include "../../../Kernels/ConvolutionGaborKernel2D.h"
#include "MonocularSaliencyExtractor.h"
#include "SaliencyBlock.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                class CMonocularGaborEdgeSaliencyExtractor: public CMonocularSaliencyExtractor
                {
                public:

                    class CParameters: public CMonocularSaliencyExtractor::CParameters
                    {
                    public:

                        CParameters(CMonocularGaborEdgeSaliencyExtractor* pHost) :
                            CMonocularSaliencyExtractor::CParameters(pHost), m_KernelRadius(2), m_KernelLambda(_REAL_ONE_), m_KernelSigma(real(5.0 / 3.0)), m_KernelGamma1(_REAL_ONE_), m_KernelGamma2(real(4.0)), m_TotalExtractionBands(2), m_UseSaliencyPercentile(false), m_SaliencyPercentilThreshold(_REAL_ZERO_), m_UseEdgePercentile(false), m_EdgePercentilThreshold(_REAL_ZERO_), m_UseEdgeCoherence(true), m_EdgeCoherenceThreshold(_REAL_HALF_), m_MinimalBlockSize(12), m_MinimalPathLength(8), m_OptimizationScope(_REAL_ONE_), m_OptimizationIterations(10), m_UseRefinementOptimization(true), m_OptimizationPrecision(real(0.001)), m_MaximalCharacterizationPathDistance(real(12.0)), m_CharacterizationThresholdElongated(real(7.0)), m_CharacterizationThresholdTransitional(real(3.0)), m_CharacterizationMode(Saliency::CSaliencyBlock::eLineal), m_CharacterizationPower(_REAL_TWO_), m_LineRegressionMinimalPixels(2), m_LineRegressionMaximalUncertainty(0.125), m_LineRegressionMinimalLength(_REAL_ONE_), m_LineRegressionMaximalDeviation(_REAL_SQRT2_), m_LineRegressionPower(real(0.125)), m_LineRegressionMode(Saliency::CCharacterizedSaliencySegment::eNonWeighting)
                        {
                        }

                        CParametersBase::ParameterChangeResult SetKernelRadius(const uint KernelRadius, const bool Wait = true);
                        uint GetKernelRadius() const;

                        CParametersBase::ParameterChangeResult SetKernelLambda(const real KernelLambda, const bool Wait = true);
                        real GetKernelLambda() const;

                        CParametersBase::ParameterChangeResult SetKernelSigma(const real KernelSigma, const bool Wait = true);
                        real GetKernelSigma() const;

                        CParametersBase::ParameterChangeResult SetKernelGamma1(const real KernelGamma1, const bool Wait = true);
                        real GetKernelGamma1() const;

                        CParametersBase::ParameterChangeResult SetKernelGamma2(const real KernelGamma2, const bool Wait = true);
                        real GetKernelGamma2() const;

                        CParametersBase::ParameterChangeResult SetTotalExtractionBands(const uint TotalExtractionBands, const bool Wait = true);
                        uint GetTotalExtractionBands() const;

                        CParametersBase::ParameterChangeResult SetUseSaliencyPercentile(const bool UseSaliencyPercentile, const bool Wait = true);
                        bool GetUseSaliencyPercentile() const;

                        CParametersBase::ParameterChangeResult SetSaliencyPercentilThreshold(const real SaliencyPercentilThreshold, const bool Wait = true);
                        real GetSaliencyPercentilThreshold() const;

                        CParametersBase::ParameterChangeResult SetUseEdgePercentile(const bool UseEdgePercentile, const bool Wait = true);
                        bool GetUseEdgePercentile() const;

                        CParametersBase::ParameterChangeResult SetEdgePercentilThreshold(const real EdgePercentilThreshold, const bool Wait = true);
                        real GetEdgePercentilThreshold() const;

                        CParametersBase::ParameterChangeResult SetUseEdgeCoherence(const bool UseEdgeCoherence, const bool Wait = true);
                        bool GetUseEdgeCoherence() const;

                        CParametersBase::ParameterChangeResult SetEdgeCoherenceThreshold(const real EdgeCoherenceThreshold, const bool Wait = true);
                        real GetEdgeCoherenceThreshold() const;

                        CParametersBase::ParameterChangeResult SetEdgeDominanceLevels(const uint DominanceLevels, const bool Wait = true);
                        uint GetEdgeDominanceLevels() const;

                        CParametersBase::ParameterChangeResult SetMinimalBlockSize(const uint MinimalBlockSize, const bool Wait = true);
                        uint GetMinimalBlockSize() const;

                        CParametersBase::ParameterChangeResult SetMinimalPathLength(const uint MinimalPathLength, const bool Wait = true);
                        uint GetMinimalPathLength() const;

                        CParametersBase::ParameterChangeResult SetOptimizationScope(const real OptimizationScope, const bool Wait = true);
                        real GetOptimizationScope() const;

                        CParametersBase::ParameterChangeResult SetOptimizationIterations(const uint OptimizationIterations, const bool Wait = true);
                        uint GetOptimizationIterations() const;

                        CParametersBase::ParameterChangeResult SetUseRefinementOptimization(const bool UseRefinementOptimization, const bool Wait = true);
                        bool GetUseRefinementOptimization() const;

                        CParametersBase::ParameterChangeResult SetOptimizationPrecision(const real OptimizationPrecision, const bool Wait = true);
                        real GetOptimizationPrecision() const;

                        CParametersBase::ParameterChangeResult SetMaximalCharacterizationPathDistance(const real MaximalCharacterizationPathDistance, const bool Wait = true);
                        real GetMaximalCharacterizationPathDistance() const;

                        CParametersBase::ParameterChangeResult SetCharacterizationThresholdElongated(const real CharacterizationThresholdElongated, const bool Wait = true);
                        real GetCharacterizationThresholdElongated() const;

                        CParametersBase::ParameterChangeResult SetCharacterizationThresholdTransitional(const real CharacterizationThresholdTransitional, const bool Wait = true);
                        real GetCharacterizationThresholdTransitional() const;

                        CParametersBase::ParameterChangeResult SetCharacterizationMode(const Saliency::CSaliencyBlock::CharacterizationMode CharacterizationMode, const bool Wait = true);
                        Saliency::CSaliencyBlock::CharacterizationMode GetCharacterizationMode() const;

                        CParametersBase::ParameterChangeResult SetCharacterizationPower(const real CharacterizationPower, const bool Wait = true);
                        real GetCharacterizationPower() const;

                        CParametersBase::ParameterChangeResult SetLineRegressionMinimalPixels(const uint LineRegressionMinimalPixels, const bool Wait = true);
                        uint GetLineRegressionMinimalPixels() const;

                        CParametersBase::ParameterChangeResult SetLineRegressionMaximalUncertainty(const real LineRegressionMaximalUncertainty, const bool Wait = true);
                        real GetLineRegressionMaximalUncertainty() const;

                        CParametersBase::ParameterChangeResult SetLineRegressionMinimalLength(const real LineRegressionMinimalLength, const bool Wait = true);
                        real GetLineRegressionMinimalLength() const;

                        CParametersBase::ParameterChangeResult SetLineRegressionMaximalDeviation(const real CharacterizationPower, const bool Wait = true);
                        real GetLineRegressionMaximalDeviation() const;

                        CParametersBase::ParameterChangeResult SetLineRegressionPower(const real LineRegressionPower, const bool Wait = true);
                        real GetLineRegressionPower() const;

                        CParametersBase::ParameterChangeResult SetLineRegressionMode(const Saliency::CCharacterizedSaliencySegment::RegressionWeightingMode LineRegressionPower, const bool Wait = true);
                        Saliency::CCharacterizedSaliencySegment::RegressionWeightingMode GetLineRegressionMode() const;

                    protected:

                        //KERNELS
                        uint m_KernelRadius;
                        real m_KernelLambda;
                        real m_KernelSigma;
                        real m_KernelGamma1;
                        real m_KernelGamma2;
                        //EXTRACTION
                        uint m_TotalExtractionBands;
                        //SELECTION
                        bool m_UseSaliencyPercentile;
                        real m_SaliencyPercentilThreshold;
                        //FILTERING
                        bool m_UseEdgePercentile;
                        real m_EdgePercentilThreshold;
                        bool m_UseEdgeCoherence;
                        real m_EdgeCoherenceThreshold;
                        uint m_MinimalBlockSize;
                        uint m_MinimalPathLength;
                        //OPTIMIZATION
                        real m_OptimizationScope;
                        uint m_OptimizationIterations;
                        bool m_UseRefinementOptimization;
                        real m_OptimizationPrecision;
                        //CHARACTERIZATION
                        real m_MaximalCharacterizationPathDistance;
                        real m_CharacterizationThresholdElongated;
                        real m_CharacterizationThresholdTransitional;
                        Saliency::CSaliencyBlock::CharacterizationMode m_CharacterizationMode;
                        real m_CharacterizationPower;
                        //REGRESSION
                        uint m_LineRegressionMinimalPixels;
                        real m_LineRegressionMaximalUncertainty;
                        real m_LineRegressionMinimalLength;
                        real m_LineRegressionMaximalDeviation;
                        real m_LineRegressionPower;
                        Saliency::CCharacterizedSaliencySegment::RegressionWeightingMode m_LineRegressionMode;
                    };

                    CMonocularGaborEdgeSaliencyExtractor(CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, Threading::CMutex* pInputImageMutex);
                    CMonocularGaborEdgeSaliencyExtractor(CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, const TImage<bool>* pInputMaskImage, Threading::CMutex* pInputImageMutex);
                    ~CMonocularGaborEdgeSaliencyExtractor() override;

                    bool Execute(const Identifier TrialId) override;

                    CParameters* GetGaborEdgeSaliencyParameters();

                    //CeBIT TMP
                    void GetLineSegment2D(list<Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D*>& LineSegment2D, const real MinimalLength);
                    void GetIntersectionPoints2D(list<EVP::Mathematics::_2D::Geometry::CPoint2D*>& IntersectionPoints2D, const real MinimalLength);
                    //CeBIT TMP

                protected:

                    friend class CMonocularGaborEdgeSaliencyExtractor::CParameters;

                    bool UpdateTotalExtractionBands();
                    void ClearGaborKernels();

                    bool Extract(const Identifier TrialId) override;

                    void ComputeGaborEdgeSaliency(const Identifier TrialId);
                    void DetectSaliencyMagnitudEdges(const Identifier TrialId);
                    void ExtractSaliencyMagnitudBlocks(const Identifier TrialId);
                    void CharacterizeBlocks(const Identifier TrialId);
                    void SegmentCharacterizedBlocks(const Identifier TrialId);
                    void GeometricRegression(const Identifier TrialId);
                    void PerceptualOrganization(const Identifier TrialId);
                    void FilterBlocksByGeometricProperties(const Identifier TrialId);
                    void ClearBlocks();

                    list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*> GetGeometricPrimitives() const;

                    CParameters m_Parameters;
                    list<Kernels::CConvolutionGaborKernel2D*> m_GaborKernels;
                    CImageActiveZone m_SaliencyActiveZone;
                    real m_SaliencyThreshold;
                    real m_EdgeThreshold;

                    static  bool GetGaborKernelOffsetActiveZoneUpdate(const void* pData, coordinate& OffsetX0, coordinate& OffsetY0, coordinate& OffsetX1, coordinate& OffsetY1);
                };
            }
        }
    }
}

