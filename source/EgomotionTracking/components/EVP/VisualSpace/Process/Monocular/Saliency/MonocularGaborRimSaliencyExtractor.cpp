/*
 * MonocularGaborRimSaliencyExtractor.cpp
 *
 *  Created on: 05.12.2011
 *      Author: gonzalez
 */

#include "MonocularGaborRimSaliencyExtractor.h"
#include "RimSaliencyBlock.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularGaborRimSaliencyExtractor::CParameters
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetKernelRadius(const uint KernelRadius, const bool Wait)
                {
                    if (KernelRadius < 1)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (KernelRadius == m_KernelRadius)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_KernelRadius = KernelRadius;
                        CParametersBase::ParameterChangeResult Result = pHost->UpdateTotalExtractionBands() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                uint CMonocularGaborRimSaliencyExtractor::CParameters::GetKernelRadius() const
                {
                    return m_KernelRadius;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetKernelLambda(const real KernelLambda, const bool Wait)
                {
                    if ((KernelLambda < _REAL_ZERO_) || (KernelLambda > _REAL_TWO_))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (KernelLambda == m_KernelRadius)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_KernelLambda = KernelLambda;
                        CParametersBase::ParameterChangeResult Result = pHost->UpdateTotalExtractionBands() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborRimSaliencyExtractor::CParameters::GetKernelLambda() const
                {
                    return m_KernelLambda;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetKernelSigma(const real KernelSigma, const bool Wait)
                {
                    if ((KernelSigma < _REAL_ZERO_) || (KernelSigma > real(8.0)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (KernelSigma == m_KernelSigma)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_KernelSigma = KernelSigma;
                        CParametersBase::ParameterChangeResult Result = pHost->UpdateTotalExtractionBands() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborRimSaliencyExtractor::CParameters::GetKernelSigma() const
                {
                    return m_KernelSigma;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetKernelGamma1(const real KernelGamma1, const bool Wait)
                {
                    if ((KernelGamma1 < _REAL_ZERO_) || (KernelGamma1 > real(8.0)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (KernelGamma1 == m_KernelGamma1)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_KernelGamma1 = KernelGamma1;
                        CParametersBase::ParameterChangeResult Result = pHost->UpdateTotalExtractionBands() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborRimSaliencyExtractor::CParameters::GetKernelGamma1() const
                {
                    return m_KernelGamma1;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetKernelGamma2(const real KernelGamma2, const bool Wait)
                {
                    if ((KernelGamma2 < _REAL_ZERO_) || (KernelGamma2 > real(8.0)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (KernelGamma2 == m_KernelGamma2)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_KernelGamma2 = KernelGamma2;
                        CParametersBase::ParameterChangeResult Result = pHost->UpdateTotalExtractionBands() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborRimSaliencyExtractor::CParameters::GetKernelGamma2() const
                {
                    return m_KernelGamma2;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetTotalExtractionBands(const uint TotalExtractionBands, const bool Wait)
                {
                    if (TotalExtractionBands < 2)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (TotalExtractionBands == m_TotalExtractionBands)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_TotalExtractionBands = TotalExtractionBands;
                        CParametersBase::ParameterChangeResult Result = pHost->UpdateTotalExtractionBands() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                uint CMonocularGaborRimSaliencyExtractor::CParameters::GetTotalExtractionBands() const
                {
                    return m_TotalExtractionBands;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetOutterGaussianSigma(const real OutterGaussianSigma, const bool Wait)
                {
                    if ((OutterGaussianSigma < _REAL_FOURTH_) || (OutterGaussianSigma > _REAL_THREE_) || (OutterGaussianSigma == m_InnerGaussianSigma))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (OutterGaussianSigma == m_OutterGaussianSigma)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_OutterGaussianSigma = OutterGaussianSigma;
                        CParametersBase::ParameterChangeResult Result = pHost->UpdateDoGKernel() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborRimSaliencyExtractor::CParameters::GetOutterGaussianSigma()
                {
                    return m_OutterGaussianSigma;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetInnerGaussianSigma(const real InnerGaussianSigma, const bool Wait)
                {
                    if ((InnerGaussianSigma < _REAL_FOURTH_) || (InnerGaussianSigma > _REAL_THREE_) || (InnerGaussianSigma == m_OutterGaussianSigma))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (InnerGaussianSigma == m_InnerGaussianSigma)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_InnerGaussianSigma = InnerGaussianSigma;
                        CParametersBase::ParameterChangeResult Result = pHost->UpdateDoGKernel() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborRimSaliencyExtractor::CParameters::GetInnerGaussianSigma()
                {
                    return m_InnerGaussianSigma;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetUseSaliencyPercentile(const bool UseSaliencyPercentile, const bool Wait)
                {
                    if (UseSaliencyPercentile == m_UseSaliencyPercentile)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_UseSaliencyPercentile = UseSaliencyPercentile;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                bool CMonocularGaborRimSaliencyExtractor::CParameters::GetUseSaliencyPercentile() const
                {
                    return m_UseSaliencyPercentile;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetSaliencyPercentilThreshold(const real SaliencyPercentilThreshold, const bool Wait)
                {
                    if ((SaliencyPercentilThreshold < _REAL_ZERO_) || (SaliencyPercentilThreshold > _REAL_ONE_))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (SaliencyPercentilThreshold == m_SaliencyPercentilThreshold)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_SaliencyPercentilThreshold = SaliencyPercentilThreshold;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborRimSaliencyExtractor::CParameters::GetSaliencyPercentilThreshold() const
                {
                    return m_SaliencyPercentilThreshold;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetUseRimPercentile(const bool UseRimPercentile, const bool Wait)
                {
                    if (UseRimPercentile == m_UseRimPercentile)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_UseRimPercentile = UseRimPercentile;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                bool CMonocularGaborRimSaliencyExtractor::CParameters::GetUseRimPercentile() const
                {
                    return m_UseRimPercentile;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetRimPercentilThreshold(const real RimPercentilThreshold, const bool Wait)
                {
                    if ((RimPercentilThreshold < _REAL_ZERO_) || (RimPercentilThreshold > _REAL_ONE_))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (RimPercentilThreshold == m_RimPercentilThreshold)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_RimPercentilThreshold = RimPercentilThreshold;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborRimSaliencyExtractor::CParameters::GetRimPercentilThreshold() const
                {
                    return m_RimPercentilThreshold;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetMinimalBlockSize(const uint MinimalBlockSize, const bool Wait)
                {
                    if (MinimalBlockSize < 1)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (MinimalBlockSize == m_MinimalBlockSize)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_MinimalBlockSize = MinimalBlockSize;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                uint CMonocularGaborRimSaliencyExtractor::CParameters::GetMinimalBlockSize() const
                {
                    return m_MinimalBlockSize;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetMinimalPathLength(const uint MinimalPathLength, const bool Wait)
                {
                    if (MinimalPathLength < 1)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (MinimalPathLength == m_MinimalBlockSize)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_MinimalPathLength = MinimalPathLength;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                uint CMonocularGaborRimSaliencyExtractor::CParameters::GetMinimalPathLength() const
                {
                    return m_MinimalPathLength;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetOptimizationScope(const real OptimizationScope, const bool Wait)
                {
                    if ((OptimizationScope < _REAL_HALF_) || (OptimizationScope > _REAL_SQRT2_))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (OptimizationScope == m_OptimizationScope)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_OptimizationScope = OptimizationScope;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborRimSaliencyExtractor::CParameters::GetOptimizationScope() const
                {
                    return m_OptimizationScope;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetOptimizationIterations(const uint OptimizationIterations, const bool Wait)
                {
                    if ((OptimizationIterations < 10) || (OptimizationIterations > 100))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (OptimizationIterations == m_OptimizationIterations)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_OptimizationIterations = OptimizationIterations;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                uint CMonocularGaborRimSaliencyExtractor::CParameters::GetOptimizationIterations() const
                {
                    return m_OptimizationIterations;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetUseRefinementOptimization(const bool UseRefinementOptimization, const bool Wait)
                {
                    if (UseRefinementOptimization == m_UseRefinementOptimization)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_UseRefinementOptimization = UseRefinementOptimization;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                bool CMonocularGaborRimSaliencyExtractor::CParameters::GetUseRefinementOptimization() const
                {
                    return m_UseRefinementOptimization;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborRimSaliencyExtractor::CParameters::SetOptimizationPrecision(const real OptimizationPrecision, const bool Wait)
                {
                    if ((OptimizationPrecision < real(0.0000001)) || (OptimizationPrecision > real(0.01)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (OptimizationPrecision == m_OptimizationScope)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborRimSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborRimSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_OptimizationPrecision = OptimizationPrecision;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborRimSaliencyExtractor::CParameters::GetOptimizationPrecision() const
                {
                    return m_OptimizationPrecision;
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularGaborRimSaliencyExtractor
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                CMonocularGaborRimSaliencyExtractor::CMonocularGaborRimSaliencyExtractor(CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, Threading::CMutex* pInputImageMutex) :
                    CMonocularSaliencyExtractor(CImageProcessBase::eGaborRimSaliencyExtractor, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this), m_pConvolutionDoGKernel2D(nullptr), m_SaliencyThreshold(_REAL_ZERO_), m_RimThreshold(_REAL_ZERO_)
                {
                    if (m_IsEnabled)
                    {
                        m_pParameters = &m_Parameters;
                        UpdateTotalExtractionBands();
                        UpdateDoGKernel();
                        m_SaliencyActiveZone.SetConnection(m_pInputActiveZone, this, CMonocularGaborRimSaliencyExtractor::GetGaborKernelOffsetActiveZoneUpdate);
                        m_RimActiveZone.SetConnection(&m_SaliencyActiveZone, this, CMonocularGaborRimSaliencyExtractor::GetDoGKernelOffsetActiveZoneUpdate);
                        m_SelectedRimActiveZone.SetConnection(&m_RimActiveZone, 1);
                        m_OutputActiveZone.SetConnection(&m_SelectedRimActiveZone, 1);
                    }
                }

                CMonocularGaborRimSaliencyExtractor::~CMonocularGaborRimSaliencyExtractor()
                {
                    if (m_IsEnabled)
                    {
                        ClearBlocks();
                        ClearGaborKernels();
                        m_SaliencyActiveZone.Disconnect();
                        RELEASE_OBJECT(m_pConvolutionDoGKernel2D)
                    }
                }

                bool CMonocularGaborRimSaliencyExtractor::Execute(const Identifier TrialId)
                {
                    if (m_IsEnabled && m_OutputActiveZone.IsEnabled())
                    {
                        if (StartExecuting(TrialId))
                        {
                            START_PROCESS_EXECUTION_LOG("Execute", TrialId)

                            LoadInputImage();

                            Extract(TrialId);

                            STOP_PROCESS_EXECUTION_LOG

                            return FinishExecution();
                        }
                    }
                    return false;
                }

                CMonocularGaborRimSaliencyExtractor::CParameters* CMonocularGaborRimSaliencyExtractor::GetGaborRimSaliencyParameters()
                {
                    return m_IsEnabled ? &m_Parameters : nullptr;
                }

                bool CMonocularGaborRimSaliencyExtractor::UpdateTotalExtractionBands()
                {
                    ClearGaborKernels();
                    const uint TotalExtractionBands = m_Parameters.GetTotalExtractionBands();
                    const uint Radius = m_Parameters.GetKernelRadius();
                    const real Lambda = m_Parameters.GetKernelLambda();
                    const real Sigma = m_Parameters.GetKernelSigma();
                    const real Gamma1 = m_Parameters.GetKernelGamma1();
                    const real Gamma2 = m_Parameters.GetKernelGamma2();
                    const real Delta = _REAL_HALF_PI_ / real(TotalExtractionBands);
                    real Alpha = _REAL_ZERO_;
                    real GlobalMinimalNonZero = _REAL_MAX_;
                    for (uint i = 0; i < TotalExtractionBands; ++i, Alpha += Delta)
                    {
                        Kernels::CConvolutionGaborKernel2D* pGaborKernel = new Kernels::CConvolutionGaborKernel2D(Radius, Lambda, Sigma, Gamma1, Gamma2, Mathematics::_2D::CVector2D(RealCosinus(Alpha), RealSinus(Alpha)));
                        const real MinimalNonZero = pGaborKernel->GetMinimalNonZero();
                        if (MinimalNonZero < GlobalMinimalNonZero)
                        {
                            GlobalMinimalNonZero = MinimalNonZero;
                        }
                        m_GaborKernels.push_back(pGaborKernel);
                    }
                    const real ScaleValue = _REAL_ONE_ / GlobalMinimalNonZero;
                    list<Kernels::CConvolutionGaborKernel2D*>::iterator EndGaborKernels = m_GaborKernels.end();
                    for (list<Kernels::CConvolutionGaborKernel2D*>::iterator ppGaborKernel = m_GaborKernels.begin(); ppGaborKernel != EndGaborKernels; ++ppGaborKernel)
                    {
                        (*ppGaborKernel)->Scale(ScaleValue);
                    }
                    return m_SaliencyActiveZone.UpdateConnection();
                }

                bool CMonocularGaborRimSaliencyExtractor::UpdateDoGKernel()
                {
                    RELEASE_OBJECT(m_pConvolutionDoGKernel2D)
                    m_pConvolutionDoGKernel2D = new Kernels::CConvolutionDoGKernel2D(m_Parameters.GetOutterGaussianSigma(), m_Parameters.GetInnerGaussianSigma(), Kernels::CConvolutionDoGKernel2D::eIndirectMaximalNormalized);
                    return true;
                }

                void CMonocularGaborRimSaliencyExtractor::ClearGaborKernels()
                {
                    if (m_GaborKernels.size())
                    {
                        list<Kernels::CConvolutionGaborKernel2D*>::const_iterator EndGaborKernels = m_GaborKernels.end();
                        for (list<Kernels::CConvolutionGaborKernel2D*>::const_iterator ppGaborKernel = m_GaborKernels.begin(); ppGaborKernel != EndGaborKernels; ++ppGaborKernel)
                            RELEASE_OBJECT_DIRECT(*ppGaborKernel)
                            m_GaborKernels.clear();
                    }
                }

                bool CMonocularGaborRimSaliencyExtractor::Extract(const Identifier TrialId)
                {
                    ClearBlocks();
                    ComputeGaborSaliency(TrialId);
                    ComputeRimSaliency(TrialId);
                    DetectSaliencyMagnitudRims(TrialId);
                    ExtractSaliencyMagnitudBlocks(TrialId);
                    return m_SelectedSaliencyBlocks.size();
                }

                void CMonocularGaborRimSaliencyExtractor::ComputeGaborSaliency(const Identifier /*TrialId*/)
                {
                    START_PROCESS_EXECUTION_TIMER("ComputeGaborSaliency")

                    const coordinate X0 = m_SaliencyActiveZone.GetX0();
                    const coordinate Y0 = m_SaliencyActiveZone.GetY0();
                    const coordinate X1 = m_SaliencyActiveZone.GetX1();
                    const coordinate Y1 = m_SaliencyActiveZone.GetY1();
                    const uint Width = m_SaliencyActiveZone.GetWidth();
                    const uint KernelDiameter = m_GaborKernels.front()->GetWidth();
                    const uint KernelSubDiameter = KernelDiameter - 1;
                    const uint Offset = Width - KernelDiameter;
                    list<Kernels::CConvolutionGaborKernel2D*>::const_iterator EndGaborKernels = m_GaborKernels.end();
                    for (list<Kernels::CConvolutionGaborKernel2D*>::const_iterator ppGaborKernel = m_GaborKernels.begin(); ppGaborKernel != EndGaborKernels; ++ppGaborKernel)
                    {
                        const Kernels::CConvolutionGaborKernel2D* pGaborKernel = *ppGaborKernel;
                        const real Nx = pGaborKernel->GetOrientationX();
                        const real Ny = pGaborKernel->GetOrientationY();
                        const real* pKernelBase = pGaborKernel->GetKernelReadOnlyBuffer();
                        const real* pKernelBaseDual = pKernelBase + KernelSubDiameter;
                        const real* pExternBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferAt(m_pInputActiveZone->GetUpperLeftLocation());
                        Saliency::CSaliencyPixel* pBaseSaliencyPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseSaliencyPixel += Width, pExternBaseInputPixel += Width)
                        {
                            const real* pInternBaseInputPixel = pExternBaseInputPixel;
                            Saliency::CSaliencyPixel* pSaliencyPixel = pBaseSaliencyPixel;
                            for (coordinate x = X0; x < X1; ++x, ++pSaliencyPixel, ++pInternBaseInputPixel)
                            {
                                const real* pKernel = pKernelBase;
                                const real* pInputPixel = pInternBaseInputPixel;
                                real Acumulator = _REAL_ZERO_, AcumulatorDual = _REAL_ZERO_;
                                for (uint i = 0; i < KernelDiameter; ++i, pInputPixel += Offset)
                                {
                                    const real* pKernelDual = pKernelBaseDual - i;
                                    for (uint j = 0; j < KernelDiameter; ++j, pKernelDual += KernelDiameter)
                                    {
                                        Acumulator += *pInputPixel * *pKernel++;
                                        AcumulatorDual += *pInputPixel++ * *pKernelDual;
                                    }
                                }
                                pSaliencyPixel->AddSaliencyOrientationOffset(Nx * Acumulator + Ny * AcumulatorDual, Ny * Acumulator - Nx * AcumulatorDual);
                            }
                        }
                    }
                    Saliency::CSaliencyPixel* pBaseSaliencyPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; ++y, pBaseSaliencyPixel += Width)
                    {
                        Saliency::CSaliencyPixel* pSaliencyPixel = pBaseSaliencyPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pSaliencyPixel)
                        {
                            pSaliencyPixel->Normalization();
                        }
                    }

                    STOP_PROCESS_EXECUTION_TIMER("ComputeGaborSaliency")
                }

                void CMonocularGaborRimSaliencyExtractor::ComputeRimSaliency(const Identifier /*TrialId*/)
                {
                    START_PROCESS_EXECUTION_TIMER("ComputeRimSaliency")

                    const coordinate X0 = m_RimActiveZone.GetX0();
                    const coordinate Y0 = m_RimActiveZone.GetY0();
                    const coordinate X1 = m_RimActiveZone.GetX1();
                    const coordinate Y1 = m_RimActiveZone.GetY1();
                    const uint Width = m_RimActiveZone.GetWidth();
                    const uint KernelDiameter = m_pConvolutionDoGKernel2D->GetWidth();
                    const uint Offset = Width - KernelDiameter;
                    const real* pKernelBase = m_pConvolutionDoGKernel2D->GetKernelReadOnlyBuffer();
                    Saliency::CSaliencyPixel* pExternBasePseudoInputPixel = m_pOutputImage->GetWritableBufferAt(m_SaliencyActiveZone.GetUpperLeftLocation());
                    Saliency::CSaliencyPixel* pBaseSaliencyPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; ++y, pBaseSaliencyPixel += Width, pExternBasePseudoInputPixel += Width)
                    {
                        const Saliency::CSaliencyPixel* pInternBasePseudoInputPixel = pExternBasePseudoInputPixel;
                        Saliency::CSaliencyPixel* pSaliencyPixel = pBaseSaliencyPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pSaliencyPixel, ++pInternBasePseudoInputPixel)
                        {
                            const Mathematics::_2D::CVector2D& SaliencyOrientation = pSaliencyPixel->GetOrientation();
                            const real* pKernel = pKernelBase;
                            const Saliency::CSaliencyPixel* pInputPixel = pInternBasePseudoInputPixel;
                            real Acumulator = _REAL_ZERO_;
                            for (uint i = 0; i < KernelDiameter; ++i, pInputPixel += Offset)
                                for (uint j = 0; j < KernelDiameter; ++j, ++pInputPixel)
                                {
                                    Acumulator -= RealPow(SaliencyOrientation.ScalarProduct(pInputPixel->GetOrientation()) * *pKernel++, real(3.0));
                                }
                            pSaliencyPixel->SetMagnitude(Acumulator);
                        }
                    }
                    if (m_pConfidenceImage && m_Parameters.GetUseConfidenceMap())
                    {
                        const real* pBaseConfidencePixel = m_pConfidenceImage->GetReadOnlyBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseConfidencePixel += Width, pBaseSaliencyPixel += Width)
                        {
                            const real* pConfidencePixel = pBaseConfidencePixel;
                            Saliency::CSaliencyPixel* pSaliencyPixel = pBaseSaliencyPixel;
                            for (coordinate x = X0; x < X1; ++x, ++pSaliencyPixel)
                            {
                                pSaliencyPixel->ScaleMagnitude(*pConfidencePixel++);
                            }
                        }
                    }
                    if (m_Parameters.GetUseSaliencyPercentile())
                    {
                        m_SaliencyPixelSetAnalyzer.AnalyzePercentile(Saliency::CSaliencyPixelSetAnalyzer::eDirectSaliency);
                        m_SaliencyThreshold = m_SaliencyPixelSetAnalyzer.GetSaliencyMagitudeAtPercentileReference(m_Parameters.GetSaliencyPercentilThreshold(), Saliency::CSaliencyPixelSetAnalyzer::eDirectSaliency);
                    }

                    STOP_PROCESS_EXECUTION_TIMER("ComputeRimSaliency")
                }

                void CMonocularGaborRimSaliencyExtractor::DetectSaliencyMagnitudRims(const Identifier /*TrialId*/)
                {
                    START_PROCESS_EXECUTION_TIMER("DetectSaliencyMagnitudRims")

                    m_SelectedSaliencyPixelSetAnalyzer.Clear();
                    const coordinate X0 = m_SelectedRimActiveZone.GetX0();
                    const coordinate Y0 = m_SelectedRimActiveZone.GetY0();
                    const coordinate X1 = m_SelectedRimActiveZone.GetX1();
                    const coordinate Y1 = m_SelectedRimActiveZone.GetY1();
                    const uint Width = m_SelectedRimActiveZone.GetWidth();
                    coordinate TetraConnectivityDeltas[4] = { 0 };
                    m_SelectedRimActiveZone.GetTetraConnectivityDeltas(TetraConnectivityDeltas);
                    Saliency::CSaliencyPixel* pBaseSaliencyPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                    const real MinimalMagnitude = m_Parameters.GetUseSaliencyPercentile() ? m_SaliencyThreshold : _REAL_MIN_;
                    const real OptimizationPrecision = m_Parameters.GetOptimizationPrecision();
                    const bool UseRefinementOptimization = m_Parameters.GetUseRefinementOptimization();
                    const uint OptimizationIterations = m_Parameters.GetOptimizationIterations();
                    const uint TotalOptimizationIterations = OptimizationIterations * 2 + 1;
                    const real Displacement = m_Parameters.GetOptimizationScope() / real(OptimizationIterations);
                    real* pSamples = new real[TotalOptimizationIterations];
                    Mathematics::_2D::CVector2D* pLocations = new Mathematics::_2D::CVector2D[TotalOptimizationIterations];
                    for (coordinate y = Y0; y < Y1; ++y, pBaseSaliencyPixel += Width)
                    {
                        Saliency::CSaliencyPixel* pSaliencyPixel = pBaseSaliencyPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pSaliencyPixel)
                            if (pSaliencyPixel->IsRimPixel(MinimalMagnitude, TetraConnectivityDeltas))
                            {
                                if (pSaliencyPixel->OptimizeSaliency(OptimizationPrecision, m_pOutputImage, MinimalMagnitude, Displacement, TotalOptimizationIterations, pSamples, pLocations, UseRefinementOptimization))
                                {
                                    pSaliencyPixel->SetSelected(true);
                                    m_SelectedSaliencyPixelSetAnalyzer.AddPixel(pSaliencyPixel);
                                }
                            }
                    }
                    if (m_Parameters.GetUseRimPercentile())
                    {
                        m_SelectedSaliencyPixelSetAnalyzer.AnalyzePercentile(Saliency::CSaliencyPixelSetAnalyzer::eDirectSaliency);
                        m_RimThreshold = m_SelectedSaliencyPixelSetAnalyzer.GetSaliencyMagitudeAtPercentileReference(m_Parameters.GetRimPercentilThreshold(), Saliency::CSaliencyPixelSetAnalyzer::eDirectSaliency);
                    }
                    RELEASE_ARRAY(pSamples)
                    RELEASE_ARRAY(pLocations)

                    STOP_PROCESS_EXECUTION_TIMER("DetectSaliencyMagnitudRims")
                }

                void CMonocularGaborRimSaliencyExtractor::ExtractSaliencyMagnitudBlocks(const Identifier /*TrialId*/)
                {
                    START_PROCESS_EXECUTION_TIMER("ExtractSaliencyMagnitudBlocks")

                    coordinate pOctoConnectivityDeltas[8] = { 0 };
                    m_OutputActiveZone.GetOctoConnectivityDeltas(pOctoConnectivityDeltas);
                    const real MinimalMagnitude = m_Parameters.GetUseRimPercentile() ? m_RimThreshold : _REAL_ONE_;
                    const uint MinimalSize = m_Parameters.GetMinimalBlockSize();
                    //const uint MinimalPathLength = m_Parameters.GetMinimalPathLength();
                    list<Saliency::CSaliencyPixel*>::const_iterator EndSaliencyPixels = m_SelectedSaliencyPixelSetAnalyzer.GetPixels().end();
                    for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SelectedSaliencyPixelSetAnalyzer.GetPixels().begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel)
                        if ((*ppSaliencyPixel)->IsAttachable(MinimalMagnitude))
                        {
                            Saliency::CRimSaliencyBlock* pRimSaliencyBlock = new Saliency::CRimSaliencyBlock(*ppSaliencyPixel);
                            list<Saliency::CSaliencyPixel*> ExpansionList;
                            ExpansionList.push_back(*ppSaliencyPixel);
                            while (ExpansionList.size())
                            {
                                Saliency::CSaliencyPixel* pSaliencyPixel = ExpansionList.front();
                                ExpansionList.pop_front();
                                for (uint i = 0; i < 8; ++i)
                                {
                                    Saliency::CSaliencyPixel* pAuxiliarSaliencyPixel = pSaliencyPixel + pOctoConnectivityDeltas[i];
                                    if (pAuxiliarSaliencyPixel->IsInRangeAttachable(MinimalMagnitude, pSaliencyPixel))
                                    {
                                        pRimSaliencyBlock->AddPixel(pAuxiliarSaliencyPixel);
                                        ExpansionList.push_back(pAuxiliarSaliencyPixel);
                                    }
                                }
                            }
                            if (pRimSaliencyBlock->GetSize() > MinimalSize)
                            {
                                pRimSaliencyBlock->ComputeConnectivityGrade(pOctoConnectivityDeltas);
                                //if (pRimSaliencyBlock->ComputeRoot() > MinimalPathLength)

                                {
                                    pRimSaliencyBlock->SetSelected(true);
                                    m_SelectedSaliencyBlocks.push_back(pRimSaliencyBlock);
                                }
                                //else
                                //m_ResidualSaliencyBlocks.push_back(pRimSaliencyBlock);
                            }
                            else
                            {
                                m_ResidualSaliencyBlocks.push_back(pRimSaliencyBlock);
                            }
                        }

                    STOP_PROCESS_EXECUTION_TIMER("ExtractSaliencyMagnitudBlocks")
                }

                void CMonocularGaborRimSaliencyExtractor::ClearBlocks()
                {
                    if (m_SelectedSaliencyBlocks.size())
                    {
                        list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                        for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlocks = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlocks != EndSaliencyBlocks; ++ppSaliencyBlocks)
                            RELEASE_OBJECT_DIRECT(*ppSaliencyBlocks)
                            m_SelectedSaliencyBlocks.clear();
                    }
                    if (m_ResidualSaliencyBlocks.size())
                    {
                        list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_ResidualSaliencyBlocks.end();
                        for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlocks = m_ResidualSaliencyBlocks.begin(); ppSaliencyBlocks != EndSaliencyBlocks; ++ppSaliencyBlocks)
                            RELEASE_OBJECT_DIRECT(*ppSaliencyBlocks)
                            m_ResidualSaliencyBlocks.clear();
                    }
                }

                bool CMonocularGaborRimSaliencyExtractor::GetGaborKernelOffsetActiveZoneUpdate(const void* pData, coordinate& OffsetX0, coordinate& OffsetY0, coordinate& OffsetX1, coordinate& OffsetY1)
                {
                    if (pData)
                    {
                        const CMonocularGaborRimSaliencyExtractor* pMonocularGaborRimSaliencyExtractor = reinterpret_cast<const CMonocularGaborRimSaliencyExtractor*>(pData);
                        if (pMonocularGaborRimSaliencyExtractor)
                        {
                            OffsetY1 = OffsetY0 = OffsetX1 = OffsetX0 = pMonocularGaborRimSaliencyExtractor->m_Parameters.GetKernelRadius();
                            return true;
                        }
                    }
                    return false;
                }

                bool CMonocularGaborRimSaliencyExtractor::GetDoGKernelOffsetActiveZoneUpdate(const void* pData, coordinate& OffsetX0, coordinate& OffsetY0, coordinate& OffsetX1, coordinate& OffsetY1)
                {
                    if (pData)
                    {
                        const CMonocularGaborRimSaliencyExtractor* pMonocularGaborRimSaliencyExtractor = reinterpret_cast<const CMonocularGaborRimSaliencyExtractor*>(pData);
                        if (pMonocularGaborRimSaliencyExtractor)
                        {
                            OffsetY1 = OffsetY0 = OffsetX1 = OffsetX0 = pMonocularGaborRimSaliencyExtractor->m_pConvolutionDoGKernel2D->GetRadius();
                            return true;
                        }
                    }
                    return false;
                }
            }
        }
    }
}
