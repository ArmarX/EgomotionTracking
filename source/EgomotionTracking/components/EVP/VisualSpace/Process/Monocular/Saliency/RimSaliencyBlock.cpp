/*
 * RimSaliencyBlock.cpp
 *
 *  Created on: 29.03.2011
 *      Author: gonzalez
 */

#include "RimSaliencyBlock.h"
#include "SaliencyPixel.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            CRimSaliencyBlock::CRimSaliencyBlock(CSaliencyPixel* pSaliencyPixel) :
                CSaliencyBlock(pSaliencyPixel)
            {
            }

            CRimSaliencyBlock::~CRimSaliencyBlock()
                = default;
        }
    }
}
