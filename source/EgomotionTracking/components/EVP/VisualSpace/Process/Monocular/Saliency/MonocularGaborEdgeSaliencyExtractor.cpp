/*
 * MonocularGaborEdgeSaliencyExtractor.cpp
 *
 *  Created on: 05.12.2011
 *      Author: gonzalez
 */

#include "MonocularGaborEdgeSaliencyExtractor.h"
#include "EdgeSaliencyBlock.h"

#include "../../../../Foundation/Mathematics/2D/StructuralTensor2D.h"
#include "CharacterizedSaliencySegment.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularGaborEdgeSaliencyExtractor::CParameters
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetKernelRadius(const uint KernelRadius, const bool Wait)
                {
                    if (KernelRadius < 1)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (KernelRadius == m_KernelRadius)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_KernelRadius = KernelRadius;
                        CParametersBase::ParameterChangeResult Result = pHost->UpdateTotalExtractionBands() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                uint CMonocularGaborEdgeSaliencyExtractor::CParameters::GetKernelRadius() const
                {
                    return m_KernelRadius;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetKernelLambda(const real KernelLambda, const bool Wait)
                {
                    if ((KernelLambda < _REAL_ZERO_) || (KernelLambda > _REAL_TWO_))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (KernelLambda == m_KernelRadius)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_KernelLambda = KernelLambda;
                        CParametersBase::ParameterChangeResult Result = pHost->UpdateTotalExtractionBands() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetKernelLambda() const
                {
                    return m_KernelLambda;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetKernelSigma(const real KernelSigma, const bool Wait)
                {
                    if ((KernelSigma < _REAL_ZERO_) || (KernelSigma > real(8.0)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (KernelSigma == m_KernelSigma)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_KernelSigma = KernelSigma;
                        CParametersBase::ParameterChangeResult Result = pHost->UpdateTotalExtractionBands() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetKernelSigma() const
                {
                    return m_KernelSigma;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetKernelGamma1(const real KernelGamma1, const bool Wait)
                {
                    if ((KernelGamma1 < _REAL_ZERO_) || (KernelGamma1 > real(8.0)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (KernelGamma1 == m_KernelGamma1)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_KernelGamma1 = KernelGamma1;
                        CParametersBase::ParameterChangeResult Result = pHost->UpdateTotalExtractionBands() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetKernelGamma1() const
                {
                    return m_KernelGamma1;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetKernelGamma2(const real KernelGamma2, const bool Wait)
                {
                    if ((KernelGamma2 < _REAL_ZERO_) || (KernelGamma2 > real(8.0)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (KernelGamma2 == m_KernelGamma2)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_KernelGamma2 = KernelGamma2;
                        CParametersBase::ParameterChangeResult Result = pHost->UpdateTotalExtractionBands() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetKernelGamma2() const
                {
                    return m_KernelGamma2;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetTotalExtractionBands(const uint TotalExtractionBands, const bool Wait)
                {
                    if (TotalExtractionBands < 2)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (TotalExtractionBands == m_TotalExtractionBands)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_TotalExtractionBands = TotalExtractionBands;
                        CParametersBase::ParameterChangeResult Result = pHost->UpdateTotalExtractionBands() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                uint CMonocularGaborEdgeSaliencyExtractor::CParameters::GetTotalExtractionBands() const
                {
                    return m_TotalExtractionBands;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetUseSaliencyPercentile(const bool UseSaliencyPercentile, const bool Wait)
                {
                    if (UseSaliencyPercentile == m_UseSaliencyPercentile)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_UseSaliencyPercentile = UseSaliencyPercentile;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                bool CMonocularGaborEdgeSaliencyExtractor::CParameters::GetUseSaliencyPercentile() const
                {
                    return m_UseSaliencyPercentile;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetSaliencyPercentilThreshold(const real SaliencyPercentilThreshold, const bool Wait)
                {
                    if ((SaliencyPercentilThreshold < _REAL_ZERO_) || (SaliencyPercentilThreshold > _REAL_ONE_))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (SaliencyPercentilThreshold == m_SaliencyPercentilThreshold)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_SaliencyPercentilThreshold = SaliencyPercentilThreshold;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetSaliencyPercentilThreshold() const
                {
                    return m_SaliencyPercentilThreshold;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetUseEdgePercentile(const bool UseEdgePercentile, const bool Wait)
                {
                    if (UseEdgePercentile == m_UseEdgePercentile)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_UseEdgePercentile = UseEdgePercentile;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                bool CMonocularGaborEdgeSaliencyExtractor::CParameters::GetUseEdgePercentile() const
                {
                    return m_UseEdgePercentile;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetEdgePercentilThreshold(const real EdgePercentilThreshold, const bool Wait)
                {
                    if ((EdgePercentilThreshold < _REAL_ZERO_) || (EdgePercentilThreshold > _REAL_ONE_))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (EdgePercentilThreshold == m_EdgePercentilThreshold)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_EdgePercentilThreshold = EdgePercentilThreshold;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetEdgePercentilThreshold() const
                {
                    return m_EdgePercentilThreshold;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetUseEdgeCoherence(const bool UseEdgeCoherence, const bool Wait)
                {
                    if (UseEdgeCoherence == m_UseEdgeCoherence)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_UseEdgeCoherence = UseEdgeCoherence;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                bool CMonocularGaborEdgeSaliencyExtractor::CParameters::GetUseEdgeCoherence() const
                {
                    return m_UseEdgeCoherence;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetEdgeCoherenceThreshold(const real EdgeCoherenceThreshold, const bool Wait)
                {
                    if ((EdgeCoherenceThreshold < -_REAL_ONE_) || (EdgeCoherenceThreshold > _REAL_ONE_))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (EdgeCoherenceThreshold == m_EdgeCoherenceThreshold)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_EdgeCoherenceThreshold = EdgeCoherenceThreshold;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetEdgeCoherenceThreshold() const
                {
                    return m_EdgeCoherenceThreshold;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetMinimalBlockSize(const uint MinimalBlockSize, const bool Wait)
                {
                    if (MinimalBlockSize < 1)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (MinimalBlockSize == m_MinimalBlockSize)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_MinimalBlockSize = MinimalBlockSize;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                uint CMonocularGaborEdgeSaliencyExtractor::CParameters::GetMinimalBlockSize() const
                {
                    return m_MinimalBlockSize;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetMinimalPathLength(const uint MinimalPathLength, const bool Wait)
                {
                    if (MinimalPathLength < 1)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (MinimalPathLength == m_MinimalBlockSize)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_MinimalPathLength = MinimalPathLength;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                uint CMonocularGaborEdgeSaliencyExtractor::CParameters::GetMinimalPathLength() const
                {
                    return m_MinimalPathLength;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetOptimizationScope(const real OptimizationScope, const bool Wait)
                {
                    if ((OptimizationScope < _REAL_HALF_) || (OptimizationScope > _REAL_SQRT2_))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (OptimizationScope == m_OptimizationScope)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_OptimizationScope = OptimizationScope;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetOptimizationScope() const
                {
                    return m_OptimizationScope;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetOptimizationIterations(const uint OptimizationIterations, const bool Wait)
                {
                    if ((OptimizationIterations < 10) || (OptimizationIterations > 100))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (OptimizationIterations == m_OptimizationIterations)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_OptimizationIterations = OptimizationIterations;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                uint CMonocularGaborEdgeSaliencyExtractor::CParameters::GetOptimizationIterations() const
                {
                    return m_OptimizationIterations;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetUseRefinementOptimization(const bool UseRefinementOptimization, const bool Wait)
                {
                    if (UseRefinementOptimization == m_UseRefinementOptimization)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_UseRefinementOptimization = UseRefinementOptimization;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                bool CMonocularGaborEdgeSaliencyExtractor::CParameters::GetUseRefinementOptimization() const
                {
                    return m_UseRefinementOptimization;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetOptimizationPrecision(const real OptimizationPrecision, const bool Wait)
                {
                    if ((OptimizationPrecision < real(0.0000001)) || (OptimizationPrecision > real(0.01)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (OptimizationPrecision == m_OptimizationScope)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_OptimizationPrecision = OptimizationPrecision;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetOptimizationPrecision() const
                {
                    return m_OptimizationPrecision;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetMaximalCharacterizationPathDistance(const real MaximalCharacterizationPathDistance, const bool Wait)
                {
                    if ((MaximalCharacterizationPathDistance < _REAL_THREE_) || (MaximalCharacterizationPathDistance > real(64.0)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (MaximalCharacterizationPathDistance == m_MaximalCharacterizationPathDistance)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_MaximalCharacterizationPathDistance = MaximalCharacterizationPathDistance;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetMaximalCharacterizationPathDistance() const
                {
                    return m_MaximalCharacterizationPathDistance;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetCharacterizationThresholdElongated(const real CharacterizationThresholdElongated, const bool Wait)
                {
                    if ((CharacterizationThresholdElongated < _REAL_THREE_) || (CharacterizationThresholdElongated > real(24.0)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (CharacterizationThresholdElongated == m_CharacterizationThresholdElongated)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_CharacterizationThresholdElongated = CharacterizationThresholdElongated;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetCharacterizationThresholdElongated() const
                {
                    return m_CharacterizationThresholdElongated;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetCharacterizationThresholdTransitional(const real CharacterizationThresholdTransitional, const bool Wait)
                {
                    if ((CharacterizationThresholdTransitional < _REAL_TWO_) || (CharacterizationThresholdTransitional > real(16.0)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (CharacterizationThresholdTransitional == m_CharacterizationThresholdTransitional)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_CharacterizationThresholdTransitional = CharacterizationThresholdTransitional;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetCharacterizationThresholdTransitional() const
                {
                    return m_CharacterizationThresholdTransitional;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetCharacterizationMode(const Saliency::CSaliencyBlock::CharacterizationMode CharacterizationMode, const bool Wait)
                {
                    if (!((CharacterizationMode == Saliency::CSaliencyBlock::eLineal) || (CharacterizationMode == Saliency::CSaliencyBlock::ePower) || (CharacterizationMode == Saliency::CSaliencyBlock::eApeture) || (CharacterizationMode == Saliency::CSaliencyBlock::ePowerApeture)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (CharacterizationMode == m_CharacterizationMode)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_CharacterizationMode = CharacterizationMode;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                Saliency::CSaliencyBlock::CharacterizationMode CMonocularGaborEdgeSaliencyExtractor::CParameters::GetCharacterizationMode() const
                {
                    return m_CharacterizationMode;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetCharacterizationPower(const real CharacterizationPower, const bool Wait)
                {
                    if ((CharacterizationPower < _REAL_ONE_) || (CharacterizationPower > real(16.0)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (CharacterizationPower == m_CharacterizationPower)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_CharacterizationPower = CharacterizationPower;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetCharacterizationPower() const
                {
                    return m_CharacterizationPower;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetLineRegressionMinimalPixels(const uint LineRegressionMinimalPixels, const bool Wait)
                {
                    if (LineRegressionMinimalPixels < 2)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (LineRegressionMinimalPixels == m_LineRegressionMinimalPixels)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_LineRegressionMinimalPixels = LineRegressionMinimalPixels;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                uint CMonocularGaborEdgeSaliencyExtractor::CParameters::GetLineRegressionMinimalPixels() const
                {
                    return m_LineRegressionMinimalPixels;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetLineRegressionMaximalUncertainty(const real LineRegressionMaximalUncertainty, const bool Wait)
                {
                    if ((LineRegressionMaximalUncertainty < real(0.05)) || (LineRegressionMaximalUncertainty > real(0.25)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (LineRegressionMaximalUncertainty == m_LineRegressionMaximalUncertainty)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_LineRegressionMaximalUncertainty = LineRegressionMaximalUncertainty;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetLineRegressionMaximalUncertainty() const
                {
                    return m_LineRegressionMaximalUncertainty;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetLineRegressionMinimalLength(const real LineRegressionMinimalLength, const bool Wait)
                {
                    if (LineRegressionMinimalLength < _REAL_HALF_)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (LineRegressionMinimalLength == m_LineRegressionMinimalLength)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_LineRegressionMinimalLength = LineRegressionMinimalLength;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetLineRegressionMinimalLength() const
                {
                    return m_LineRegressionMinimalLength;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetLineRegressionMaximalDeviation(const real LineRegressionMaximalDeviation, const bool Wait)
                {
                    if (LineRegressionMaximalDeviation < _REAL_ONE_)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (LineRegressionMaximalDeviation == m_LineRegressionMaximalDeviation)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_LineRegressionMaximalDeviation = LineRegressionMaximalDeviation;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetLineRegressionMaximalDeviation() const
                {
                    return m_LineRegressionMaximalDeviation;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetLineRegressionPower(const real LineRegressionPower, const bool Wait)
                {
                    if (LineRegressionPower < _REAL_EPSILON_)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (LineRegressionPower == m_LineRegressionPower)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_LineRegressionPower = LineRegressionPower;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularGaborEdgeSaliencyExtractor::CParameters::GetLineRegressionPower() const
                {
                    return m_LineRegressionPower;
                }

                CParametersBase::ParameterChangeResult CMonocularGaborEdgeSaliencyExtractor::CParameters::SetLineRegressionMode(const Saliency::CCharacterizedSaliencySegment::RegressionWeightingMode LineRegressionMode, const bool Wait)
                {
                    if (LineRegressionMode < _REAL_ONE_)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (LineRegressionMode == m_LineRegressionMode)
                    {
                        return CParametersBase::eNoChange;
                    }
                    CMonocularGaborEdgeSaliencyExtractor* pHost = reinterpret_cast<CMonocularGaborEdgeSaliencyExtractor*>(m_pHost);
                    if (pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_LineRegressionMode = LineRegressionMode;
                        return pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                Saliency::CCharacterizedSaliencySegment::RegressionWeightingMode CMonocularGaborEdgeSaliencyExtractor::CParameters::GetLineRegressionMode() const
                {
                    return m_LineRegressionMode;
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularGaborEdgeSaliencyExtractor
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                CMonocularGaborEdgeSaliencyExtractor::CMonocularGaborEdgeSaliencyExtractor(CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, Threading::CMutex* pInputImageMutex) :
                    CMonocularSaliencyExtractor(CImageProcessBase::eGaborEdgeSaliencyExtractor, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this), m_SaliencyThreshold(_REAL_ZERO_), m_EdgeThreshold(_REAL_ZERO_)
                {
                    if (m_IsEnabled)
                    {
                        m_pParameters = &m_Parameters;
                        UpdateTotalExtractionBands();
                        m_SaliencyActiveZone.SetConnection(m_pInputActiveZone, this, CMonocularGaborEdgeSaliencyExtractor::GetGaborKernelOffsetActiveZoneUpdate);
                        m_OutputActiveZone.SetConnection(&m_SaliencyActiveZone, 1);
                    }
                }

                CMonocularGaborEdgeSaliencyExtractor::CMonocularGaborEdgeSaliencyExtractor(CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, const TImage<bool>* pInputMaskImage, Threading::CMutex* pInputImageMutex) :
                    CMonocularSaliencyExtractor(CImageProcessBase::eGaborEdgeSaliencyExtractor, pActiveZone, pInputImage, pInputMaskImage, pInputImageMutex), m_Parameters(this), m_SaliencyThreshold(_REAL_ZERO_), m_EdgeThreshold(_REAL_ZERO_)
                {
                    if (m_IsEnabled)
                    {
                        m_pParameters = &m_Parameters;
                        UpdateTotalExtractionBands();
                        m_SaliencyActiveZone.SetConnection(m_pInputActiveZone, this, CMonocularGaborEdgeSaliencyExtractor::GetGaborKernelOffsetActiveZoneUpdate);
                        m_OutputActiveZone.SetConnection(&m_SaliencyActiveZone, 1);
                    }
                }

                CMonocularGaborEdgeSaliencyExtractor::~CMonocularGaborEdgeSaliencyExtractor()
                {
                    if (m_IsEnabled)
                    {
                        ClearBlocks();
                        ClearGaborKernels();
                        m_SaliencyActiveZone.Disconnect();
                    }
                }

                bool CMonocularGaborEdgeSaliencyExtractor::Execute(const Identifier TrialId)
                {
                    if (m_IsEnabled && m_OutputActiveZone.IsEnabled())
                    {
                        if (StartExecuting(TrialId))
                        {
                            START_PROCESS_EXECUTION_LOG("Execute", TrialId)

                            LoadInputImage();

                            Extract(TrialId);

                            STOP_PROCESS_EXECUTION_LOG

                            return FinishExecution();
                        }
                    }
                    return false;
                }

                CMonocularGaborEdgeSaliencyExtractor::CParameters* CMonocularGaborEdgeSaliencyExtractor::GetGaborEdgeSaliencyParameters()
                {
                    return m_IsEnabled ? &m_Parameters : nullptr;
                }

                bool CMonocularGaborEdgeSaliencyExtractor::UpdateTotalExtractionBands()
                {
                    ClearGaborKernels();
                    const uint TotalExtractionBands = m_Parameters.GetTotalExtractionBands();
                    const uint Radius = m_Parameters.GetKernelRadius();
                    const real Lambda = m_Parameters.GetKernelLambda();
                    const real Sigma = m_Parameters.GetKernelSigma();
                    const real Gamma1 = m_Parameters.GetKernelGamma1();
                    const real Gamma2 = m_Parameters.GetKernelGamma2();
                    const real Delta = _REAL_HALF_PI_ / real(TotalExtractionBands);
                    real Alpha = _REAL_ZERO_;
                    //real GlobalMinimalNonZero = _REAL_MAX_;
                    for (uint i = 0; i < TotalExtractionBands; ++i, Alpha += Delta)
                    {
                        Kernels::CConvolutionGaborKernel2D* pGaborKernel = new Kernels::CConvolutionGaborKernel2D(Radius, Lambda, Sigma, Gamma1, Gamma2, Mathematics::_2D::CVector2D(RealCosinus(Alpha), RealSinus(Alpha)));
                        //const real MinimalNonZero = pGaborKernel->GetMinimalNonZero();
                        //if (MinimalNonZero < GlobalMinimalNonZero)
                        //  GlobalMinimalNonZero = MinimalNonZero;
                        m_GaborKernels.push_back(pGaborKernel);
                    }
                    /*const real ScaleValue = _REAL_ONE_ / GlobalMinimalNonZero;
                    list<Kernels::CConvolutionGaborKernel2D*>::iterator EndGaborKernels = m_GaborKernels.end();
                    for (list<Kernels::CConvolutionGaborKernel2D*>::iterator ppGaborKernel = m_GaborKernels.begin(); ppGaborKernel != EndGaborKernels; ++ppGaborKernel)
                        (*ppGaborKernel)->Scale(ScaleValue);*/
                    return m_SaliencyActiveZone.UpdateConnection();
                }

                void CMonocularGaborEdgeSaliencyExtractor::ClearGaborKernels()
                {
                    if (m_GaborKernels.size())
                    {
                        list<Kernels::CConvolutionGaborKernel2D*>::const_iterator EndGaborKernels = m_GaborKernels.end();
                        for (list<Kernels::CConvolutionGaborKernel2D*>::const_iterator ppGaborKernel = m_GaborKernels.begin(); ppGaborKernel != EndGaborKernels; ++ppGaborKernel)
                            RELEASE_OBJECT_DIRECT(*ppGaborKernel)
                            m_GaborKernels.clear();
                    }
                }

                bool CMonocularGaborEdgeSaliencyExtractor::Extract(const Identifier TrialId)
                {
                    ClearBlocks();
                    ComputeGaborEdgeSaliency(TrialId);
                    DetectSaliencyMagnitudEdges(TrialId);
                    ExtractSaliencyMagnitudBlocks(TrialId);
                    CharacterizeBlocks(TrialId);
                    SegmentCharacterizedBlocks(TrialId);
                    GeometricRegression(TrialId);
                    PerceptualOrganization(TrialId);

                    //FilterBlocksByGeometricProperties(TrialId);
                    return m_SelectedSaliencyBlocks.size();
                }

                list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*> CMonocularGaborEdgeSaliencyExtractor::GetGeometricPrimitives() const
                {
                    list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*> ImageGeometricPrimitives;
                    list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                    for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlocks = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlocks != EndSaliencyBlocks; ++ppSaliencyBlocks)
                    {
                        const list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*> BlockGeometricPrimitives = (*ppSaliencyBlocks)->GetGeometricPrimitives();
                        ImageGeometricPrimitives.insert(ImageGeometricPrimitives.end(), BlockGeometricPrimitives.begin(), BlockGeometricPrimitives.end());
                    }
                    return ImageGeometricPrimitives;
                }

                void CMonocularGaborEdgeSaliencyExtractor::ComputeGaborEdgeSaliency(const Identifier /*TrialId*/)
                {
                    START_PROCESS_EXECUTION_TIMER("ComputeGaborEdgeSaliency")

                    const uint ActiveWidth = m_SaliencyActiveZone.GetActiveWidth();
                    const uint ActiveHeight = m_SaliencyActiveZone.GetActiveHeight();
                    const uint Offset = m_SaliencyActiveZone.GetHorizontalOffset();
                    const uint KernelDiameter = m_GaborKernels.front()->GetWidth();
                    const uint ConvolutionOffset = m_SaliencyActiveZone.GetWidth() - KernelDiameter;
                    list<Kernels::CConvolutionGaborKernel2D*>::const_iterator EndGaborKernels = m_GaborKernels.end();
                    for (list<Kernels::CConvolutionGaborKernel2D*>::const_iterator ppGaborKernel = m_GaborKernels.begin(); ppGaborKernel != EndGaborKernels; ++ppGaborKernel)
                    {
                        const Kernels::CConvolutionGaborKernel2D* pGaborKernel = *ppGaborKernel;
                        const real Nx = pGaborKernel->GetOrientationX();
                        const real Ny = pGaborKernel->GetOrientationY();
                        const real* pKernelBase = pGaborKernel->GetKernelReadOnlyBuffer();
                        const real* pKernelBaseDual = pKernelBase + KernelDiameter - 1;
                        const real* pInternBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferAt(m_pInputActiveZone->GetUpperLeftLocation());
                        Saliency::CSaliencyPixel* pSaliencyPixel = m_pOutputImage->GetWritableBufferAt(m_SaliencyActiveZone.GetUpperLeftLocation());
                        for (uint i = 0; i < ActiveHeight; ++i, pSaliencyPixel += Offset, pInternBaseInputPixel += Offset)
                            for (uint j = 0; j < ActiveWidth; ++j, ++pSaliencyPixel, ++pInternBaseInputPixel)
                                if (pSaliencyPixel->IsEnabled())
                                {
                                    const real* pKernel = pKernelBase;
                                    const real* pInputPixel = pInternBaseInputPixel;
                                    real Acumulator = _REAL_ZERO_, AcumulatorDual = _REAL_ZERO_;
                                    for (uint k = 0; k < KernelDiameter; ++k, pInputPixel += ConvolutionOffset)
                                    {
                                        const real* pKernelDual = pKernelBaseDual - k;
                                        for (uint l = 0; l < KernelDiameter; ++l, pKernelDual += KernelDiameter)
                                        {
                                            Acumulator += *pInputPixel * *pKernel++;
                                            AcumulatorDual += *pInputPixel++ * *pKernelDual;
                                        }
                                    }
                                    pSaliencyPixel->AddSaliencyOrientationOffset(Nx * Acumulator + Ny * AcumulatorDual, Ny * Acumulator - Nx * AcumulatorDual);
                                }
                    }
                    Saliency::CSaliencyPixel* pSaliencyPixel = m_pOutputImage->GetWritableBufferAt(m_SaliencyActiveZone.GetUpperLeftLocation());
                    if (m_pConfidenceImage && m_Parameters.GetUseConfidenceMap())
                    {
                        const real* pConfidencePixel = m_pConfidenceImage->GetReadOnlyBufferAt(m_SaliencyActiveZone.GetUpperLeftLocation());
                        for (uint i = 0; i < ActiveHeight; ++i, pConfidencePixel += Offset, pSaliencyPixel += Offset)
                            for (uint j = 0; j < ActiveWidth; ++j, ++pSaliencyPixel, ++pConfidencePixel)
                                if (pSaliencyPixel->IsEnabled())
                                {
                                    pSaliencyPixel->WeightedNormalization(*pConfidencePixel);
                                }
                    }
                    else
                    {
                        for (uint i = 0; i < ActiveHeight; ++i, pSaliencyPixel += Offset)
                            for (uint j = 0; j < ActiveWidth; ++j, ++pSaliencyPixel)
                                if (pSaliencyPixel->IsEnabled())
                                {
                                    pSaliencyPixel->Normalization();
                                }
                    }
                    if (m_Parameters.GetUseSaliencyPercentile())
                    {
                        m_SaliencyPixelSetAnalyzer.AnalyzePercentile(Saliency::CSaliencyPixelSetAnalyzer::eDirectSaliency);
                        m_SaliencyThreshold = m_SaliencyPixelSetAnalyzer.GetSaliencyMagitudeAtPercentileReference(m_Parameters.GetSaliencyPercentilThreshold(), Saliency::CSaliencyPixelSetAnalyzer::eDirectSaliency);
                    }

                    STOP_PROCESS_EXECUTION_TIMER("ComputeGaborEdgeSaliency")
                }

                void CMonocularGaborEdgeSaliencyExtractor::DetectSaliencyMagnitudEdges(const Identifier /*TrialId*/)
                {
                    START_PROCESS_EXECUTION_TIMER("DetectSaliencyMagnitudEdges")

                    m_SelectedSaliencyPixelSetAnalyzer.Clear();
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const uint Width = m_OutputActiveZone.GetWidth();
                    Saliency::CSaliencyPixel* pBaseSaliencyPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                    const real EdgeCoherenceThreshold = m_Parameters.GetEdgeCoherenceThreshold();
                    const real MinimalMagnitude = m_Parameters.GetUseSaliencyPercentile() ? m_SaliencyThreshold : _REAL_EPSILON_;
                    const real OptimizationPrecision = m_Parameters.GetOptimizationPrecision();
                    const bool UseRefinementOptimization = m_Parameters.GetUseRefinementOptimization();
                    const uint OptimizationIterations = m_Parameters.GetOptimizationIterations();
                    const uint TotalOptimizationIterations = OptimizationIterations * 2 + 1;
                    const real Displacement = m_Parameters.GetOptimizationScope() / real(OptimizationIterations);
                    real* pSamples = new real[TotalOptimizationIterations];
                    Mathematics::_2D::CVector2D* pLocations = new Mathematics::_2D::CVector2D[TotalOptimizationIterations];
                    for (coordinate y = Y0; y < Y1; ++y, pBaseSaliencyPixel += Width)
                    {
                        Saliency::CSaliencyPixel* pSaliencyPixel = pBaseSaliencyPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pSaliencyPixel)
                            if (pSaliencyPixel->SelectPeakSingleLevelCheck(m_pOutputImage, MinimalMagnitude, EdgeCoherenceThreshold))
                                if (pSaliencyPixel->OptimizeSaliency(OptimizationPrecision, m_pOutputImage, MinimalMagnitude, Displacement, TotalOptimizationIterations, pSamples, pLocations, UseRefinementOptimization))
                                {
                                    m_SelectedSaliencyPixelSetAnalyzer.AddPixel(pSaliencyPixel);
                                }

                    }
                    if (m_Parameters.GetUseEdgePercentile())
                    {
                        m_SelectedSaliencyPixelSetAnalyzer.AnalyzePercentile(Saliency::CSaliencyPixelSetAnalyzer::eOptimizedSaliency);
                        m_EdgeThreshold = m_SelectedSaliencyPixelSetAnalyzer.GetSaliencyMagitudeAtPercentileReference(m_Parameters.GetEdgePercentilThreshold(), Saliency::CSaliencyPixelSetAnalyzer::eOptimizedSaliency);
                    }
                    RELEASE_ARRAY(pSamples)
                    RELEASE_ARRAY(pLocations)

                    STOP_PROCESS_EXECUTION_TIMER("DetectSaliencyMagnitudEdges")
                }

                void CMonocularGaborEdgeSaliencyExtractor::ExtractSaliencyMagnitudBlocks(const Identifier /*TrialId*/)
                {
                    START_PROCESS_EXECUTION_TIMER("ExtractSaliencyMagnitudBlocks")

                    coordinate pOctoConnectivityDeltas[8] = { 0 };
                    m_OutputActiveZone.GetOctoConnectivityDeltas(pOctoConnectivityDeltas);
                    const real MinimalMagnitude = m_Parameters.GetUseEdgePercentile() ? m_EdgeThreshold : _REAL_EPSILON_;
                    const uint MinimalSize = m_Parameters.GetMinimalBlockSize();
                    const uint MinimalPathLength = m_Parameters.GetMinimalPathLength();
                    list<Saliency::CSaliencyPixel*>::const_iterator EndSaliencyPixels = m_SelectedSaliencyPixelSetAnalyzer.GetPixels().end();
                    for (list<Saliency::CSaliencyPixel*>::const_iterator ppSaliencyPixel = m_SelectedSaliencyPixelSetAnalyzer.GetPixels().begin(); ppSaliencyPixel != EndSaliencyPixels; ++ppSaliencyPixel)
                        if ((*ppSaliencyPixel)->IsAttachable(MinimalMagnitude))
                        {
                            Saliency::CEdgeSaliencyBlock* pEdgeSaliencyBlock = new Saliency::CEdgeSaliencyBlock(*ppSaliencyPixel);
                            list<Saliency::CSaliencyPixel*> ExpansionList;
                            ExpansionList.push_back(*ppSaliencyPixel);
                            while (ExpansionList.size())
                            {
                                Saliency::CSaliencyPixel* pSaliencyPixel = ExpansionList.front();
                                ExpansionList.pop_front();
                                for (uint i = 0; i < 8; ++i)
                                {
                                    Saliency::CSaliencyPixel* pAuxiliarSaliencyPixel = pSaliencyPixel + pOctoConnectivityDeltas[i];
                                    if (pAuxiliarSaliencyPixel->IsInRangeAttachable(MinimalMagnitude, pSaliencyPixel))
                                    {
                                        pEdgeSaliencyBlock->AddPixel(pAuxiliarSaliencyPixel);
                                        ExpansionList.push_back(pAuxiliarSaliencyPixel);
                                    }
                                }
                            }
                            if (pEdgeSaliencyBlock->GetSize() > MinimalSize)
                            {
                                pEdgeSaliencyBlock->ComputeConnectivityGrade(pOctoConnectivityDeltas);
                                if (pEdgeSaliencyBlock->ComputeRoot(true) > MinimalPathLength)
                                {
                                    pEdgeSaliencyBlock->SetSelected(true);
                                    m_SelectedSaliencyBlocks.push_back(pEdgeSaliencyBlock);
                                }
                                else
                                {
                                    m_ResidualSaliencyBlocks.push_back(pEdgeSaliencyBlock);
                                }
                            }
                            else
                            {
                                m_ResidualSaliencyBlocks.push_back(pEdgeSaliencyBlock);
                            }
                        }

                    STOP_PROCESS_EXECUTION_TIMER("ExtractSaliencyMagnitudBlocks")
                }

                void CMonocularGaborEdgeSaliencyExtractor::CharacterizeBlocks(const Identifier TrialId)
                {
                    START_PROCESS_EXECUTION_TIMER("CharacterizeBlocks")

                    const real MaximalPathDistance = m_Parameters.GetMaximalCharacterizationPathDistance();
                    const real ThresholdElongated = m_Parameters.GetCharacterizationThresholdElongated();
                    const real ThresholdTransitional = m_Parameters.GetCharacterizationThresholdTransitional();
                    const Saliency::CSaliencyBlock::CharacterizationMode Mode = m_Parameters.GetCharacterizationMode();
                    const real Power = m_Parameters.GetCharacterizationPower();
                    const real ExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(MaximalPathDistance * _REAL_THIRD_);
                    list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                    for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlocks = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlocks != EndSaliencyBlocks; ++ppSaliencyBlocks)
                    {
                        (*ppSaliencyBlocks)->Characterization(MaximalPathDistance, ThresholdTransitional, ThresholdElongated, Mode, Power, ExponentFactor);
                    }

                    STOP_PROCESS_EXECUTION_TIMER("CharacterizeBlocks")
                }

                void CMonocularGaborEdgeSaliencyExtractor::SegmentCharacterizedBlocks(const Identifier TrialId)
                {
                    START_PROCESS_EXECUTION_TIMER("SegmentCharacterizedBlocks")

                    list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                    for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlocks = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlocks != EndSaliencyBlocks; ++ppSaliencyBlocks)
                    {
                        (*ppSaliencyBlocks)->Segmentation();
                    }

                    STOP_PROCESS_EXECUTION_TIMER("SegmentCharacterizedBlocks")
                }

                void CMonocularGaborEdgeSaliencyExtractor::GeometricRegression(const Identifier TrialId)
                {
                    START_PROCESS_EXECUTION_TIMER("GeometricRegression")

                    const uint MinimalPixels = m_Parameters.GetLineRegressionMinimalPixels();
                    const real MaximalUncertainty = m_Parameters.GetLineRegressionMaximalUncertainty();
                    const real MinimalLength = m_Parameters.GetLineRegressionMinimalLength();
                    const real MaximalDeviation = m_Parameters.GetLineRegressionMaximalDeviation();
                    const real Power = m_Parameters.GetLineRegressionPower();
                    const Saliency::CCharacterizedSaliencySegment::RegressionWeightingMode Mode = m_Parameters.GetLineRegressionMode();
                    list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                    for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlocks = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlocks != EndSaliencyBlocks; ++ppSaliencyBlocks)
                    {
                        (*ppSaliencyBlocks)->GeometricRegression(MinimalPixels, MaximalUncertainty, MinimalLength, MaximalDeviation, Mode, Power);
                    }

                    STOP_PROCESS_EXECUTION_TIMER("GeometricRegression")
                }

                void CMonocularGaborEdgeSaliencyExtractor::GetLineSegment2D(list<Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D*>& LineSegment2D, const real MinimalLength)
                {
                    if (LineSegment2D.size())
                    {
                        LineSegment2D.clear();
                    }
                    const list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*> GeometricPrimitives = GetGeometricPrimitives();
                    list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>::const_iterator EndGeometricPrimitives = GeometricPrimitives.end();
                    for (list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>::const_iterator ppGeometricPrimitive = GeometricPrimitives.begin(); ppGeometricPrimitive != EndGeometricPrimitives; ++ppGeometricPrimitive)
                        if ((*ppGeometricPrimitive)->GetGeometricPrimitiveTypeId() == Mathematics::_2D::Geometry::CGeometricPrimitive2D::eLine)
                        {
                            Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D* pRegressionLineSegment2D = dynamic_cast<Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D*>(*ppGeometricPrimitive);
                            if (pRegressionLineSegment2D->GetLength() > MinimalLength)
                            {
                                LineSegment2D.push_back(pRegressionLineSegment2D);
                            }
                        }
                }

                void CMonocularGaborEdgeSaliencyExtractor::GetIntersectionPoints2D(list<EVP::Mathematics::_2D::Geometry::CPoint2D*>& IntersectionPoints2D, const real MinimalLength)
                {
                    list<Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D*> LineSegment2D;
                    GetLineSegment2D(LineSegment2D, MinimalLength);
                    list<Mathematics::_2D::Geometry::CPoint2D*> IntersectionPoints;
                    Mathematics::_2D::CVector2D X;
                    const CImageSize& ImageSize = m_pInputActiveZone->GetSize();
                    list<Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D*>::const_iterator EndRegressionLineSegments = LineSegment2D.end();
                    for (list<Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D*>::const_iterator ppRegressionLineSegment2DA = LineSegment2D.begin(); ppRegressionLineSegment2DA != EndRegressionLineSegments; ++ppRegressionLineSegment2DA)
                    {
                        Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D* pRegressionLineSegment2DA = *ppRegressionLineSegment2DA;
                        list<Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D*>::const_iterator ppRegressionLineSegment2DB = ppRegressionLineSegment2DA;
                        for (++ppRegressionLineSegment2DB; ppRegressionLineSegment2DB != EndRegressionLineSegments; ++ppRegressionLineSegment2DB)
                        {
                            Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D* pRegressionLineSegment2DB = *ppRegressionLineSegment2DB;
                            if (pRegressionLineSegment2DA->CalculateIntersectionPoint(pRegressionLineSegment2DB, X) && ImageSize.IsInside(X))
                            {
                                IntersectionPoints.push_back(new Mathematics::_2D::Geometry::CPoint2D(X, pRegressionLineSegment2DA, pRegressionLineSegment2DB));
                            }
                        }
                    }
                }

                void CMonocularGaborEdgeSaliencyExtractor::PerceptualOrganization(const Identifier /*TrialId*/)
                {
                    START_PROCESS_EXECUTION_TIMER("PerceptualOrganization")

                    list<Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D*> LineSegment2D;
                    const list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*> GeometricPrimitives = GetGeometricPrimitives();
                    list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>::const_iterator EndGeometricPrimitives = GeometricPrimitives.end();
                    for (list<Mathematics::_2D::Geometry::Regression::CRegressionGeometricPrimitive2D*>::const_iterator ppGeometricPrimitive = GeometricPrimitives.begin(); ppGeometricPrimitive != EndGeometricPrimitives; ++ppGeometricPrimitive)
                        if ((*ppGeometricPrimitive)->GetGeometricPrimitiveTypeId() == Mathematics::_2D::Geometry::CGeometricPrimitive2D::eLine)
                        {
                            Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D* pRegressionLineSegment2D = dynamic_cast<Mathematics::_2D::Geometry::Regression::CRegressionLineSegment2D*>(*ppGeometricPrimitive);
                            LineSegment2D.push_back(pRegressionLineSegment2D);
                        }

                    /*const real MaximalOrientationDeviation = real(5.0) * _REAL_DEGREES_TO_RADIANS_;
                     list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                     for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlocks = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlocks != EndSaliencyBlocks; ++ppSaliencyBlocks)
                     (*ppSaliencyBlocks)->PerceptualOrganization(ImageSize, MaximalOrientationDeviation);
                     */

                    STOP_PROCESS_EXECUTION_TIMER("PerceptualOrganization")
                }

                void CMonocularGaborEdgeSaliencyExtractor::FilterBlocksByGeometricProperties(const Identifier /*TrialId*/)
                {
                    START_PROCESS_EXECUTION_TIMER("FilterMagnitudBlocksByProximity")

                    /*if (m_SelectedSaliencyBlocks.size() > 1)
                     {
                     m_SelectedSaliencyBlocks.sort(SortSaliencyBlockBySizeAscent);
                     list<Saliency::CSaliencyBlock*> FilteredSelectedSaliencyBlocks;
                     list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                     for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                     (*ppSaliencyBlock)->SetSelected(false);
                     for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlockA = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlockA != EndSaliencyBlocks; ++ppSaliencyBlockA)
                     {
                     Saliency::CSaliencyBlock* pSaliencyBlockA = *ppSaliencyBlockA;
                     const real BoundingBoxDiagonalLenght = pSaliencyBlockA->GetBoundingBox().GetDiagonalLenght();
                     const real Size = pSaliencyBlockA->GetSize();
                     const real MinimalDistance = TMin(Size, BoundingBoxDiagonalLenght);
                     list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlockB = ppSaliencyBlockA;
                     for (++ppSaliencyBlockB; ppSaliencyBlockB != EndSaliencyBlocks; ++ppSaliencyBlockB)
                     {
                     Saliency::CSaliencyBlock* pSaliencyBlockB = *ppSaliencyBlockB;
                     const real Distance = pSaliencyBlockA->IsBoundingBoxInteresecting(pSaliencyBlockB) ? pSaliencyBlockA->GetPixelContinousLocationDistance(pSaliencyBlockB) : pSaliencyBlockA->GetBoundingBoxDistance(pSaliencyBlockB);
                     if (Distance < MinimalDistance)
                     {
                     pSaliencyBlockA->SetSelected(true);
                     pSaliencyBlockB->SetSelected(true);
                     }
                     }
                     if (pSaliencyBlockA->IsSelected())
                     FilteredSelectedSaliencyBlocks.push_back(pSaliencyBlockA);
                     else
                     m_ResidualSaliencyBlocks.push_back(pSaliencyBlockA);
                     }
                     m_SelectedSaliencyBlocks = FilteredSelectedSaliencyBlocks;
                     }*/

                    STOP_PROCESS_EXECUTION_TIMER("FilterMagnitudBlocksByProximity")
                }

                void CMonocularGaborEdgeSaliencyExtractor::ClearBlocks()
                {
                    if (m_SelectedSaliencyBlocks.size())
                    {
                        list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_SelectedSaliencyBlocks.end();
                        for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = m_SelectedSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                            RELEASE_OBJECT_DIRECT(*ppSaliencyBlock)
                            m_SelectedSaliencyBlocks.clear();
                    }
                    if (m_ResidualSaliencyBlocks.size())
                    {
                        list<Saliency::CSaliencyBlock*>::const_iterator EndSaliencyBlocks = m_ResidualSaliencyBlocks.end();
                        for (list<Saliency::CSaliencyBlock*>::const_iterator ppSaliencyBlock = m_ResidualSaliencyBlocks.begin(); ppSaliencyBlock != EndSaliencyBlocks; ++ppSaliencyBlock)
                            RELEASE_OBJECT_DIRECT(*ppSaliencyBlock)
                            m_ResidualSaliencyBlocks.clear();
                    }
                }

                bool CMonocularGaborEdgeSaliencyExtractor::GetGaborKernelOffsetActiveZoneUpdate(const void* pData, coordinate& OffsetX0, coordinate& OffsetY0, coordinate& OffsetX1, coordinate& OffsetY1)
                {
                    if (pData)
                    {
                        const CMonocularGaborEdgeSaliencyExtractor* pMonocularGaborEdgeSaliencyExtractor = reinterpret_cast<const CMonocularGaborEdgeSaliencyExtractor*>(pData);
                        if (pMonocularGaborEdgeSaliencyExtractor)
                        {
                            OffsetY1 = OffsetY0 = OffsetX1 = OffsetX0 = pMonocularGaborEdgeSaliencyExtractor->m_Parameters.GetKernelRadius();
                            return true;
                        }
                    }
                    return false;
                }

            }
        }
    }
}
