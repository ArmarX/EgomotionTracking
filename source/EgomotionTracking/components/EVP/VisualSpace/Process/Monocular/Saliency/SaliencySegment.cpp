/*
 * SaliencySegment.cpp
 *
 *  Created on: 13.04.2011
 *      Author: gonzalez
 */

#include "SaliencySegment.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            CSaliencySegment::CSaliencySegment(CSaliencyPoint* pPointNode, CSaliencyPath* pPath) :
                m_pPath(pPath), m_CharacterizationType(pPointNode->GetCharacterizationType()), m_Length(_REAL_ZERO_)
            {
                pPointNode->SetSegmentNode(this);
                list<CSaliencyPoint*> ExpansionList;
                ExpansionList.push_back(pPointNode);
                m_PointNodes.push_back(pPointNode);
                m_BoundingBox.SetInitial(pPointNode->GetContinousLocation());
                while (ExpansionList.size())
                {
                    pPointNode = ExpansionList.back();
                    ExpansionList.pop_back();
                    CSaliencyPoint* pPreviousPoint = pPointNode->GetWritablePreviousPoint();
                    if (pPreviousPoint && (!pPreviousPoint->GetSegmentNode()) && (pPreviousPoint->GetCharacterizationType() == m_CharacterizationType))
                    {
                        pPreviousPoint->SetSegmentNode(this);
                        m_PointNodes.push_back(pPreviousPoint);
                        m_BoundingBox.Extend(pPreviousPoint->GetContinousLocation());
                        ExpansionList.push_back(pPreviousPoint);
                    }
                    CSaliencyPoint* pSubsequentPoint = pPointNode->GetWritableSubsequentPoint();
                    if (pSubsequentPoint && (!pSubsequentPoint->GetSegmentNode()) && (pSubsequentPoint->GetCharacterizationType() == m_CharacterizationType))
                    {
                        pSubsequentPoint->SetSegmentNode(this);
                        m_PointNodes.push_back(pSubsequentPoint);
                        m_BoundingBox.Extend(pSubsequentPoint->GetContinousLocation());
                        ExpansionList.push_back(pSubsequentPoint);
                    }
                }
                if (m_PointNodes.size() > 1)
                {
                    m_PointNodes.sort(SortPointNodeByIndex);
                    Mathematics::_2D::CVector2D PreviousLocation = m_PointNodes.front()->GetContinousLocation();
                    list<CSaliencyPoint*>::iterator EndPointNodes = m_PointNodes.end();
                    for (list<CSaliencyPoint*>::iterator ppPointNode = ++m_PointNodes.begin(); ppPointNode != EndPointNodes; ++ppPointNode)
                    {
                        const Mathematics::_2D::CVector2D& CurrentLocation = (*ppPointNode)->GetContinousLocation();
                        m_Length += CurrentLocation.GetDistance(PreviousLocation);
                        PreviousLocation = CurrentLocation;
                    }
                }
            }

            CSaliencySegment::~CSaliencySegment()
            {
                /*for (TVector<CGeometricPrimitive2D*>::iterator ppGeometricPrimitive2D = m_RegressionGeometricPrimitives.begin(); ppGeometricPrimitive2D != m_RegressionGeometricPrimitives.end(); ++ppGeometricPrimitive2D)
                 RELEASE_OBJECT_DIRECT(*ppGeometricPrimitive2D)*/
            }

            void CSaliencySegment::DetectSegmentNodeIncidences()
            {
                list<CSaliencyPoint*>::iterator EndPointNodes = m_PointNodes.end();
                for (list<CSaliencyPoint*>::iterator ppPointNode = m_PointNodes.begin(); ppPointNode != EndPointNodes; ++ppPointNode)
                    if ((*ppPointNode)->GetSegmentNodeIncidence())
                    {
                        m_SegmentNodeIncidences.push_back((*ppPointNode)->GetWritableSegmentNodeIncidence());
                    }
            }

            /*uint CSaliencySegmentNode::MultiLineRegression(real UncertaintyThreshold, real MinimalMainAxisLength, real MaximalSecondaryAxisLength, int MinimalPointNodes, RegressionWeightingMode Mode)
             {
             TVector<CSaliencyLineSegment*> LineSegments = MultiLineRegression(m_PointNodes.begin(), m_PointNodes.end(), MinimalPointNodes, UncertaintyThreshold, MinimalMainAxisLength, MaximalSecondaryAxisLength, Mode);
             if (LineSegments.size() > 1)
             TSort(LineSegments.begin(), LineSegments.end(), SortLineSegmentByIndex);
             for (TVector<CSaliencyLineSegment*>::iterator ppLineSegment = LineSegments.begin(); ppLineSegment != LineSegments.end(); ++ppLineSegment)
             m_RegressionGeometricPrimitives.push_back(*ppLineSegment);
             return LineSegments.size();
             }*/

            bool CSaliencySegment::SortPointNodeByIndex(CSaliencyPoint* plhs, CSaliencyPoint* prhs)
            {
                return plhs->GetIndex() < prhs->GetIndex();
            }

            /*
             bool CSaliencySegmentNode::SortLineSegmentByIndex(CSaliencyLineSegment* plhs, CSaliencyLineSegment* prhs)
             {
             return Min(plhs->GetPointNodeA()->GetIndexInMesh(), plhs->GetPointNodeB()->GetIndexInMesh()) < Min(prhs->GetPointNodeA()->GetIndexInMesh(), prhs->GetPointNodeB()->GetIndexInMesh());
             }*/

            void CSaliencySegment::WeightedLeastSquaresLineFitting(Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, real& E0p, real& E0n, real& E1p, real& E1n, vector<CSaliencyPoint*>::iterator ppInitialNode, vector<CSaliencyPoint*>::iterator ppFinalNode, int TotalNodes, RegressionWeightingMode Mode)
            {
                //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN

                Mathematics::ND::LinearAlgebra::CMatrixND X(TotalNodes, 2);
                Mathematics::ND::LinearAlgebra::CMatrixND Y(TotalNodes, 1);
                Mathematics::ND::LinearAlgebra::CMatrixND B;
                uint Index = 0;
                real AW = _REAL_ZERO_, AWX = _REAL_ZERO_, AWY = _REAL_ZERO_;
                switch (Mode)
                {
                    case eNonWeighting:
                    {
                        for (vector<CSaliencyPoint*>::iterator ppPointNode = ppInitialNode; ppPointNode != ppFinalNode; ++ppPointNode, ++Index)
                        {
                            const Mathematics::_2D::CVector2D& P = (*ppPointNode)->GetSmoothContinousLocation();
                            X(Index, 0) = P.GetX();
                            X(Index, 1) = _REAL_ONE_;
                            Y(Index, 0) = P.GetY();
                            AWX += P.GetX();
                            AWY += P.GetY();
                        }
                        AW = TotalNodes;
                        B = (X.transpose() * X).inverse() * (X.transpose() * Y);
                    }
                    break;
                    case eEccentricityWeighting:
                    {

                        //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN

                        Mathematics::ND::LinearAlgebra::CMatrixND W(TotalNodes, TotalNodes);
                        W.setZero();
                        for (vector<CSaliencyPoint*>::iterator ppPointNode = ppInitialNode; ppPointNode != ppFinalNode; ++ppPointNode, ++Index)
                        {
                            CSaliencyPoint* pPointNode = *ppPointNode;
                            const Mathematics::_2D::CVector2D& P = pPointNode->GetSmoothContinousLocation();
                            X(Index, 0) = P.GetX();
                            X(Index, 1) = _REAL_ONE_;
                            Y(Index, 0) = P.GetY();
                            const real Weight = pPointNode->GetEccentricity();
                            W(Index, Index) = Weight;
                            AWX += Weight * P.GetX();
                            AWY += Weight * P.GetY();
                            AW += Weight;
                        }
                        B = (X.transpose() * W * X).inverse() * (X.transpose() * W * Y);
                    }
                    break;
                    case eApertureWeighting:
                    {
                        //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN

                        Mathematics::ND::LinearAlgebra::CMatrixND W(TotalNodes, TotalNodes);
                        W.setZero();
                        for (vector<CSaliencyPoint*>::iterator ppPointNode = ppInitialNode; ppPointNode != ppFinalNode; ++ppPointNode, ++Index)
                        {
                            CSaliencyPoint* pPointNode = *ppPointNode;
                            const Mathematics::_2D::CVector2D& P = pPointNode->GetSmoothContinousLocation();
                            X(Index, 0) = P.GetX();
                            X(Index, 1) = _REAL_ONE_;
                            Y(Index, 0) = P.GetY();
                            const real Weight = pPointNode->GetAperture();
                            W(Index, Index) = Weight;
                            AWX += Weight * P.GetX();
                            AWY += Weight * P.GetY();
                            AW += Weight;
                        }
                        B = (X.transpose() * W * X).inverse() * (X.transpose() * W * Y);
                    }
                    break;
                }
                const real Slope = B(0, 0);
                const real Offset = B(1, 0);
                const real Alpha = RealAtan(Slope);
                MainAxis.SetValue(RealCosinus(Alpha), RealSinus(Alpha));
                SecondaryAxis.SetValue(-MainAxis.GetY(), MainAxis.GetX());
                MainAxis.SetCannonicalForm();
                SecondaryAxis.SetCannonicalForm();
                Mathematics::_2D::CVector2D Centroid(AWX / AW, AWY / AW);
                Center = (Mathematics::_2D::CVector2D(Centroid.GetX(), Centroid.GetX() * Slope + Offset) + Mathematics::_2D::CVector2D((Centroid.GetY() - Offset) / Slope, Centroid.GetY())) * _REAL_HALF_;
                E1n = E1p = E0n = E0p = _REAL_ZERO_;
                for (vector<CSaliencyPoint*>::iterator ppPointNode = ppInitialNode + 1; ppPointNode != ppFinalNode; ++ppPointNode)
                {
                    CSaliencyPoint* pPointNode = *ppPointNode;
                    Mathematics::_2D::CVector2D Delta = pPointNode->GetSmoothContinousLocation() - Center;
                    const real E0 = MainAxis.ScalarProduct(Delta);
                    const real E1 = SecondaryAxis.ScalarProduct(Delta);
                    if (E0 > E0p)
                    {
                        E0p = E0;
                    }
                    else if (E0 < E0n)
                    {
                        E0n = E0;
                    }
                    if (E1 > E1p)
                    {
                        E1p = E1;
                    }
                    else if (E1 < E1n)
                    {
                        E1n = E1;
                    }
                    pPointNode->SetDeviation(RealAbs(E1));
                }
            }

            bool CSaliencySegment::CovarianceLineFitting(Mathematics::_2D::CVector2D& MainAxis, Mathematics::_2D::CVector2D& SecondaryAxis, Mathematics::_2D::CVector2D& Center, real& E0p, real& E0n, real& E1p, real& E1n, vector<CSaliencyPoint*>::iterator ppInitialNode, vector<CSaliencyPoint*>::iterator ppFinalNode, int TotalNodes, RegressionWeightingMode Mode)
            {
                real AW = _REAL_ZERO_, AWX = _REAL_ZERO_, AWY = _REAL_ZERO_;
                switch (Mode)
                {
                    case eNonWeighting:
                        for (vector<CSaliencyPoint*>::iterator ppPointNode = ppInitialNode; ppPointNode != ppFinalNode; ++ppPointNode)
                        {
                            const Mathematics::_2D::CVector2D& P = (*ppPointNode)->GetSmoothContinousLocation();
                            AWX += P.GetX();
                            AWY += P.GetY();
                        }
                        AW = TotalNodes;
                        break;
                    case eEccentricityWeighting:
                        for (vector<CSaliencyPoint*>::iterator ppPointNode = ppInitialNode; ppPointNode != ppFinalNode; ++ppPointNode)
                        {
                            CSaliencyPoint* pPointNode = *ppPointNode;
                            const Mathematics::_2D::CVector2D& P = pPointNode->GetSmoothContinousLocation();
                            const real Weight = pPointNode->GetEccentricity();
                            AWX += Weight * P.GetX();
                            AWY += Weight * P.GetY();
                            AW += Weight;
                        }
                        break;
                    case eApertureWeighting:
                        for (vector<CSaliencyPoint*>::iterator ppPointNode = ppInitialNode; ppPointNode != ppFinalNode; ++ppPointNode)
                        {
                            CSaliencyPoint* pPointNode = *ppPointNode;
                            const Mathematics::_2D::CVector2D& P = pPointNode->GetSmoothContinousLocation();
                            const real Weight = pPointNode->GetAperture();
                            AWX += Weight * P.GetX();
                            AWY += Weight * P.GetY();
                            AW += Weight;
                        }
                        break;
                }
                Center.SetValue(AWX / AW, AWY / AW);
                real CovarianceXX = _REAL_ZERO_, CovarianceXY = _REAL_ZERO_, CovarianceYY = _REAL_ZERO_, AccumulatorXX = _REAL_ZERO_, AccumulatorXY = _REAL_ZERO_, AccumulatorYY = _REAL_ZERO_;
                switch (Mode)
                {
                    case eNonWeighting:
                        for (vector<CSaliencyPoint*>::iterator ppPointNode = ppInitialNode; ppPointNode != ppFinalNode; ++ppPointNode)
                        {
                            (*ppPointNode)->GetSmoothContinousLocation().GetCovariance(Center, CovarianceXX, CovarianceXY, CovarianceYY);
                            AccumulatorXX += CovarianceXX;
                            AccumulatorXY += CovarianceXY;
                            AccumulatorYY += CovarianceYY;
                        }
                        break;
                    case eEccentricityWeighting:
                        for (vector<CSaliencyPoint*>::iterator ppPointNode = ppInitialNode; ppPointNode != ppFinalNode; ++ppPointNode)
                        {
                            CSaliencyPoint* pPointNode = *ppPointNode;
                            pPointNode->GetSmoothContinousLocation().GetCovariance(Center, CovarianceXX, CovarianceXY, CovarianceYY);
                            const real Weight = pPointNode->GetEccentricity();
                            AccumulatorXX += CovarianceXX * Weight;
                            AccumulatorXY += CovarianceXY * Weight;
                            AccumulatorYY += CovarianceYY * Weight;
                        }
                        break;
                    case eApertureWeighting:
                        for (vector<CSaliencyPoint*>::iterator ppPointNode = ppInitialNode; ppPointNode != ppFinalNode; ++ppPointNode)
                        {
                            CSaliencyPoint* pPointNode = *ppPointNode;
                            pPointNode->GetSmoothContinousLocation().GetCovariance(Center, CovarianceXX, CovarianceXY, CovarianceYY);
                            const real Weight = pPointNode->GetAperture();
                            AccumulatorXX += CovarianceXX * Weight;
                            AccumulatorXY += CovarianceXY * Weight;
                            AccumulatorYY += CovarianceYY * Weight;
                        }
                        break;
                }
                Mathematics::_2D::CStructuralTensor2D StructuralTensor2D(AccumulatorXX / AW, AccumulatorXY / AW, AccumulatorYY / AW);
                if (StructuralTensor2D.IsSingular())
                {
                    return false;
                }
                MainAxis = StructuralTensor2D.GetUnitaryMainAxis();
                SecondaryAxis = StructuralTensor2D.GetUnitarySecondaryAxis();
                E1n = E1p = E0n = E0p = _REAL_ZERO_;
                for (vector<CSaliencyPoint*>::iterator ppPointNode = ppInitialNode; ppPointNode != ppFinalNode; ++ppPointNode)
                {
                    CSaliencyPoint* pPointNode = *ppPointNode;
                    const Mathematics::_2D::CVector2D Delta = pPointNode->GetSmoothContinousLocation() - Center;
                    const real E0 = MainAxis.ScalarProduct(Delta);
                    const real E1 = SecondaryAxis.ScalarProduct(Delta);
                    if (E0 > E0p)
                    {
                        E0p = E0;
                    }
                    else if (E0 < E0n)
                    {
                        E0n = E0;
                    }
                    if (E1 > E1p)
                    {
                        E1p = E1;
                    }
                    else if (E1 < E1n)
                    {
                        E1n = E1;
                    }
                    pPointNode->SetDeviation(RealAbs(E1));
                }
                return true;
            }

            /*
             TVector<CSaliencyLineSegment*> CSaliencySegmentNode::MultiLineRegression(TVector<CSaliencyPoint*>::iterator ppInitialNode, TVector<CSaliencyPoint*>::iterator ppFinalNode, int MinimalPointNodes, real UncertaintyThreshold, real MinimalMainAxisLength, real MaximalSecondaryAxisLength, RegressionWeightingMode Mode)
             {
             TVector<CSaliencyLineSegment*> LineSegments;
             TVector<CSaliencyPoint*>::iterator ppPreFinalNode = ppFinalNode - 1;
             int TotalNodes = IntegerAbsolute((*ppPreFinalNode)->GetIndexInMesh() - (*ppInitialNode)->GetIndexInMesh()) + 1;
             if (TotalNodes > MinimalPointNodes)
             {
             CVector2D MainAxis;
             CVector2D SecondaryAxis;
             CVector2D Center;
             real E0p = _REAL_ZERO_;
             real E0n = _REAL_ZERO_;
             real E1p = _REAL_ZERO_;
             real E1n = _REAL_ZERO_;
             WeightedLeastSquaresLineFitting(MainAxis, SecondaryAxis, Center, E0p, E0n, E1p, E1n, ppInitialNode, ppFinalNode, TotalNodes, Mode);

             real MainAxisLength = E0p - E0n;
             if (MainAxisLength < MinimalMainAxisLength)
             return LineSegments;

             real SecondaryAxisLength = E1p - E1n;
             real Uncertainty = SecondaryAxisLength / MainAxisLength;

             if ((Uncertainty < UncertaintyThreshold) && (SecondaryAxisLength < MaximalSecondaryAxisLength))
             {
             LineSegments.push_back(new CSaliencyLineSegment(*ppInitialNode, *ppPreFinalNode, Center, MainAxis, SecondaryAxis, E0p, E0n, E1p, E1n, this));
             }
             else
             {
             real MaximalDeviation = _REAL_ZERO_;
             real CurrentDeviation = _REAL_ZERO_;
             TVector<CSaliencyPoint*>::iterator ppPointNode = ppInitialNode;
             TVector<CSaliencyPoint*>::iterator ppMaximalDeviationPointNode;
             while (ppPointNode != ppFinalNode)
             {
             CurrentDeviation = (*ppPointNode)->GetDeviation();
             if (CurrentDeviation > MaximalDeviation)
             {
             MaximalDeviation = CurrentDeviation;
             ppMaximalDeviationPointNode = ppPointNode;
             }
             ++ppPointNode;
             }
             if ((IntegerAbsolute((*ppInitialNode)->GetIndexInMesh() - (*ppMaximalDeviationPointNode)->GetIndexInMesh())) > MinimalPointNodes)
             {
             TVector<CSaliencyLineSegment*> LineSegmentsA = MultiLineRegression(ppInitialNode, ppMaximalDeviationPointNode, MinimalPointNodes, UncertaintyThreshold, MinimalMainAxisLength, MaximalSecondaryAxisLength, Mode);
             LineSegments.insert(LineSegments.begin(), LineSegmentsA.begin(), LineSegmentsA.end());
             }
             if ((IntegerAbsolute((*ppMaximalDeviationPointNode)->GetIndexInMesh() - (*ppPreFinalNode)->GetIndexInMesh())) > MinimalPointNodes)
             {
             TVector<CSaliencyLineSegment*> LineSegmentsB = MultiLineRegression(ppMaximalDeviationPointNode + 1, ppFinalNode, MinimalPointNodes, UncertaintyThreshold, MinimalMainAxisLength, MaximalSecondaryAxisLength, Mode);
             LineSegments.insert(LineSegments.end(), LineSegmentsB.begin(), LineSegmentsB.end());
             }
             }
             }
             return LineSegments;
             }*/

            real CSaliencySegment::GetDistance(const CSaliencySegment* pSaliencySegment)
            {
                if (pSaliencySegment == this)
                {
                    return _REAL_ZERO_;
                }
                real MinimalDistance = _REAL_MAX_;
                list<CSaliencyPoint*>::const_iterator EndPointNodesA = m_PointNodes.end();
                list<CSaliencyPoint*>::const_iterator BeginPointNodesB = pSaliencySegment->m_PointNodes.begin();
                list<CSaliencyPoint*>::const_iterator EndPointNodesB = pSaliencySegment->m_PointNodes.end();
                for (list<CSaliencyPoint*>::const_iterator ppPointNodesA = m_PointNodes.begin(); ppPointNodesA != EndPointNodesA; ++ppPointNodesA)
                {
                    const Mathematics::_2D::CVector2D& A = (*ppPointNodesA)->GetContinousLocation();
                    for (list<CSaliencyPoint*>::const_iterator ppPointNodesB = BeginPointNodesB; ppPointNodesB != EndPointNodesB; ++ppPointNodesB)
                    {
                        const real ABS = A.GetSquareDistance((*ppPointNodesB)->GetContinousLocation());
                        if (ABS < MinimalDistance)
                        {
                            MinimalDistance = ABS;
                        }
                    }
                }
                MinimalDistance = RealSqrt(MinimalDistance);
                return MinimalDistance;
            }
        }
    }
}
