/*
 * SaliencyPath.cpp
 *
 *  Created on: 13.04.2011
 *      Author: gonzalez
 */

#include "SaliencyPixel.h"
#include "SaliencyNode.h"
#include "SaliencyPath.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Saliency
        {
            IDENTIFIABLE_INITIALIZATION(CSaliencyPath)

            CSaliencyPath::CSaliencyPath(const CSaliencyNode* pSaliencyNode, const CSaliencyTree* pPixelTree) :
                IDENTIFIABLE_CONTRUCTOR(CSaliencyPath), m_pPixelTree(pPixelTree)
            {
                m_SaliencyNodes.push_back(pSaliencyNode);
                m_BoundingBox.SetInitial(pSaliencyNode->GetSourceContinousLocation());
                pSaliencyNode = pSaliencyNode->GetReadOnlyTreeParent();
                while (!pSaliencyNode->IsDynamicSplitter())
                {
                    m_SaliencyNodes.push_back(pSaliencyNode);
                    m_BoundingBox.Extend(pSaliencyNode->GetSourceContinousLocation());
                    pSaliencyNode = pSaliencyNode->GetReadOnlyTreeParent();
                }
                m_SaliencyNodes.push_back(pSaliencyNode);
            }

            CSaliencyPath::~CSaliencyPath()
            {
                for (vector<CSaliencyPoint*>::const_iterator ppPointNode = m_SaliencyPoints.begin(); ppPointNode != m_SaliencyPoints.end(); ++ppPointNode)
                    RELEASE_OBJECT_DIRECT(*ppPointNode)
                    m_SaliencyPoints.clear();
                if (m_SaliencySegments.size())
                {
                    for (list<CSaliencySegment*>::const_iterator ppSegmentNode = m_SaliencySegments.begin(); ppSegmentNode != m_SaliencySegments.end(); ++ppSegmentNode)
                        RELEASE_OBJECT_DIRECT(*ppSegmentNode)
                        m_SaliencySegments.clear();
                }
            }

            void CSaliencyPath::OutSpreading(const real RegularLengthDelta)
            {
                real AccumulatorDeltas = _REAL_ZERO_;
                EVP::Mathematics::_2D::CVector2D PreviousContinousLocation = m_SaliencyNodes.front()->GetSourceContinousLocation();
                vector<real> AccumualtiveDeltas, Deltas;
                AccumualtiveDeltas.reserve(m_SaliencyNodes.size());
                Deltas.reserve(m_SaliencyNodes.size());
                vector<const CSaliencyNode*>::const_iterator EndSaliencyNodes = m_SaliencyNodes.end();
                for (vector<const CSaliencyNode*>::const_iterator ppPixelNode = m_SaliencyNodes.begin(); ppPixelNode != EndSaliencyNodes; ++ppPixelNode)
                {
                    const EVP::Mathematics::_2D::CVector2D& CurrentContinousLocation = (*ppPixelNode)->GetSourceContinousLocation();
                    const real Delta = CurrentContinousLocation.GetDistance(PreviousContinousLocation);
                    Deltas.push_back(Delta);
                    AccumulatorDeltas += Delta;
                    AccumualtiveDeltas.push_back(AccumulatorDeltas);
                    PreviousContinousLocation = CurrentContinousLocation;
                }
                const uint SubTotalCells = DownToInteger(m_SaliencyNodes.size()) - 1;
                uint IndexPre = 0;
                real CurrentDistributedDelta = _REAL_ZERO_;
                CSaliencyPoint* pPreviousSaliencyPoint = nullptr;
                const real AdjustedRegularLengthDelta = AccumulatorDeltas / RealRound(AccumulatorDeltas / RegularLengthDelta);
                m_SaliencyPoints.reserve(UpperToInteger(AccumulatorDeltas / AdjustedRegularLengthDelta) + 2);
                while ((CurrentDistributedDelta - AccumulatorDeltas) < AdjustedRegularLengthDelta)
                {
                    if (CurrentDistributedDelta > AccumualtiveDeltas[IndexPre])
                        while (CurrentDistributedDelta > AccumualtiveDeltas[TMin(SubTotalCells, IndexPre + 1)])
                        {
                            if (++IndexPre == SubTotalCells)
                            {
                                break;
                            }
                        }
                    else
                        while (CurrentDistributedDelta < AccumualtiveDeltas[TMax(0, int(IndexPre) - 1)])
                        {
                            IndexPre = TMax(0, int(IndexPre) - 1);
                        }
                    uint IndexPos = TMin(SubTotalCells, IndexPre + 1);
                    const real DBA = AccumualtiveDeltas[IndexPos] - AccumualtiveDeltas[IndexPre];
                    pPreviousSaliencyPoint = new CSaliencyPoint(m_SaliencyPoints.size(), (DBA > _REAL_EPSILON_) ? EVP::Mathematics::_2D::CVector2D::CreateComplementaryLinealCombination(m_SaliencyNodes[IndexPre]->GetSourceContinousLocation(), m_SaliencyNodes[IndexPos]->GetSourceContinousLocation(), (AccumualtiveDeltas[IndexPos] - CurrentDistributedDelta) / DBA) : m_SaliencyNodes[IndexPos]->GetSourceContinousLocation(), this, pPreviousSaliencyPoint);
                    m_SaliencyPoints.push_back(pPreviousSaliencyPoint);
                    CurrentDistributedDelta += AdjustedRegularLengthDelta;
                }
            }

            void CSaliencyPath::Smoothing(const real* pSmoothingKernel, const uint SmoothingRadius, const uint SmoothingDiameter)
            {
                const int EndIndex = DownToInteger(m_SaliencyPoints.size()) - int(SmoothingRadius);
                if ((EndIndex - SmoothingRadius) < 1)
                {
                    return;
                }
                for (int i = int(SmoothingRadius), j0 = 0; i < EndIndex; ++i, ++j0)
                {
                    EVP::Mathematics::_2D::CVector2D SmoothLocation;
                    for (uint j = j0, k = 0; k < SmoothingDiameter; ++j, ++k)
                    {
                        SmoothLocation += m_SaliencyPoints[j]->GetContinousLocation() * pSmoothingKernel[k];
                    }
                    m_SaliencyPoints[i]->SetSmoothContinousLocation(SmoothLocation);
                }
            }

            void CSaliencyPath::SubPixelOptimization(TImage<CSaliencyPixel>* pSaliencyImage, real Scope, int Iterations, bool Refinement)
            {
                if (m_SaliencyPoints.size() > 2)
                {
                    vector<CSaliencyPoint*>::iterator BeginSaliencyPoints = m_SaliencyPoints.begin();
                    vector<CSaliencyPoint*>::iterator EndSaliencyPoints = m_SaliencyPoints.end();
                    ++BeginSaliencyPoints;
                    --EndSaliencyPoints;
                    if (Refinement)
                    {
                        int DoubleIterations = Iterations * 2;
                        real* pSampleMagnitudes = new real[DoubleIterations];
                        EVP::Mathematics::_2D::CVector2D* pSampleLocations = new EVP::Mathematics::_2D::CVector2D[DoubleIterations];
                        for (vector<CSaliencyPoint*>::iterator ppSaliencyPoint = BeginSaliencyPoints; ppSaliencyPoint != EndSaliencyPoints; ++ppSaliencyPoint)
                        {
                            CSaliencyPoint* pSaliencyPoint = *ppSaliencyPoint;
                            if (pSaliencyPoint->IsInner())
                            {
                                IndirectOptimizeLocation(pSaliencyPoint, pSaliencyImage, Scope, Iterations, pSampleMagnitudes, pSampleLocations);
                            }
                        }
                        RELEASE_ARRAY(pSampleMagnitudes)
                        RELEASE_ARRAY(pSampleLocations)
                    }
                    else
                        for (vector<CSaliencyPoint*>::iterator ppSaliencyPoint = BeginSaliencyPoints; ppSaliencyPoint != EndSaliencyPoints; ++ppSaliencyPoint)
                        {
                            CSaliencyPoint* pSaliencyPoint = *ppSaliencyPoint;
                            if (pSaliencyPoint->IsInner())
                            {
                                DirectOptimizeLocation(pSaliencyPoint, pSaliencyImage, Scope, Iterations);
                            }
                        }
                }
            }

            bool CSaliencyPath::SortSaliencySegmentByLengthDescending(const CSaliencySegment* plhs, const CSaliencySegment* prhs)
            {
                return plhs->GetLength() > prhs->GetLength();
            }

            void CSaliencyPath::IndirectOptimizeLocation(CSaliencyPoint* pSaliencyPoint, TImage<CSaliencyPixel>* pSaliencyImage, real Scope, int Iterations, real* pSampleMagnitudes, EVP::Mathematics::_2D::CVector2D* pSampleLocations)
            {
                EVP::Mathematics::_2D::CVector2D InitialLocation = pSaliencyPoint->GetSmoothContinousLocation();
                EVP::Mathematics::_2D::CVector2D LinkageBisectionAxis = pSaliencyPoint->GetLinkageBisectionAxis();
                EVP::Mathematics::_2D::CVector2D CurrentLocation = InitialLocation - LinkageBisectionAxis * Scope;
                EVP::Mathematics::_2D::CVector2D& MaximumLocation = CurrentLocation;
                EVP::Mathematics::_2D::CVector2D Delta = LinkageBisectionAxis * (Scope / real(Iterations));
                int DoubleIterations = Iterations * 2;
                real MaximalMagnitude = _REAL_ZERO_;
                for (int i = 0; i < DoubleIterations; ++i, CurrentLocation += Delta)
                {
                    real Magnitude = CSaliencyPixel::GetMagnitudeByBicubicInterpolation(CurrentLocation, pSaliencyImage);
                    pSampleMagnitudes[i] = Magnitude;
                    pSampleLocations[i] = CurrentLocation;
                    if (Magnitude > MaximalMagnitude)
                    {
                        MaximalMagnitude = Magnitude;
                        MaximumLocation = CurrentLocation;
                    }
                }
                real MinimalDistance = _REAL_MAX_;
                for (int i = 0, j = 1, k = 2; k < DoubleIterations; ++i, ++j, ++k)
                    if ((pSampleMagnitudes[j] > pSampleMagnitudes[i]) && (pSampleMagnitudes[j] > pSampleMagnitudes[k]))
                    {
                        real CurrentDistance = InitialLocation.GetSquareDistance(pSampleLocations[j]);
                        if (CurrentDistance < MinimalDistance)
                        {
                            MinimalDistance = CurrentDistance;
                            MaximumLocation = pSampleLocations[j];
                            MaximalMagnitude = pSampleMagnitudes[j];
                        }
                    }
                pSaliencyPoint->SetSmoothContinousLocation(MaximumLocation);
            }

            void CSaliencyPath::DirectOptimizeLocation(CSaliencyPoint* pSaliencyPoint, TImage<CSaliencyPixel>* pSaliencyImage, real Scope, int Iterations)
            {
                real MaximalMagnitude = _REAL_ZERO_;
                EVP::Mathematics::_2D::CVector2D LinkageBisectionAxis = pSaliencyPoint->GetLinkageBisectionAxis();
                EVP::Mathematics::_2D::CVector2D CurrentLocation = pSaliencyPoint->GetSmoothContinousLocation() - LinkageBisectionAxis * Scope;
                EVP::Mathematics::_2D::CVector2D& MaximumLocation = CurrentLocation;
                EVP::Mathematics::_2D::CVector2D Delta = LinkageBisectionAxis * (Scope / real(Iterations));
                int DoubleIterations = Iterations * 2;
                for (int i = 0; i < DoubleIterations; ++i, CurrentLocation += Delta)
                {
                    real Magnitude = CSaliencyPixel::GetMagnitudeByBicubicInterpolation(CurrentLocation, pSaliencyImage);
                    if (Magnitude > MaximalMagnitude)
                    {
                        MaximalMagnitude = Magnitude;
                        MaximumLocation = CurrentLocation;
                    }
                }
                pSaliencyPoint->SetSmoothContinousLocation(MaximumLocation);
            }

            void CSaliencyPath::Characterization(const uint MaximalPathDistance, const real ThresholdTransitional, const real ThresholdElongated, const CSaliencyPoint::CharacterizationMode Mode, const real Power, const bool UseSmoothLocations)
            {
                const uint TotalPointNodes = m_SaliencyPoints.size();
                if (UseSmoothLocations)
                    switch (Mode)
                    {
                        case CSaliencyPoint::eLineal:
                            for (uint i = 0, ip = 1; i < TotalPointNodes; ++i, ++ip)
                            {
                                CSaliencyPoint* pSaliencyPoint = m_SaliencyPoints[i];
                                const uint Scope = TMin(i + MaximalPathDistance, TotalPointNodes);
                                for (uint j = ip; j < Scope; ++j)
                                {
                                    pSaliencyPoint->AddToSmoothCharacterization(m_SaliencyPoints[j]);
                                }
                                pSaliencyPoint->CharacterizeLineal(ThresholdTransitional, ThresholdElongated);
                            }
                            break;
                        case CSaliencyPoint::ePower:
                            for (uint i = 0, ip = 1; i < TotalPointNodes; ++i, ++ip)
                            {
                                CSaliencyPoint* pSaliencyPoint = m_SaliencyPoints[i];
                                const uint Scope = TMin(i + MaximalPathDistance, TotalPointNodes);
                                for (uint j = ip; j < Scope; ++j)
                                {
                                    pSaliencyPoint->AddToSmoothCharacterization(m_SaliencyPoints[j]);
                                }
                                pSaliencyPoint->CharacterizePower(ThresholdTransitional, ThresholdElongated, Power);
                            }
                            break;
                        case CSaliencyPoint::eApeture:
                            for (uint i = 0, ip = 1; i < TotalPointNodes; ++i, ++ip)
                            {
                                CSaliencyPoint* pSaliencyPoint = m_SaliencyPoints[i];
                                const uint Scope = TMin(i + MaximalPathDistance, TotalPointNodes);
                                for (uint j = ip; j < Scope; ++j)
                                {
                                    pSaliencyPoint->AddToSmoothCharacterization(m_SaliencyPoints[j]);
                                }
                                pSaliencyPoint->CharacterizeApeture(ThresholdTransitional, ThresholdElongated);
                            }
                            break;
                        case CSaliencyPoint::ePowerApeture:
                            for (uint i = 0, ip = 1; i < TotalPointNodes; ++i, ++ip)
                            {
                                CSaliencyPoint* pSaliencyPoint = m_SaliencyPoints[i];
                                const uint Scope = TMin(i + MaximalPathDistance, TotalPointNodes);
                                for (uint j = ip; j < Scope; ++j)
                                {
                                    pSaliencyPoint->AddToSmoothCharacterization(m_SaliencyPoints[j]);
                                }
                                pSaliencyPoint->CharacterizePowerApeture(ThresholdTransitional, ThresholdElongated, Power);
                            }
                            break;
                    }
                else
                    switch (Mode)
                    {
                        case CSaliencyPoint::eLineal:
                            for (uint i = 0, ip = 1; i < TotalPointNodes; ++i, ++ip)
                            {
                                CSaliencyPoint* pSaliencyPoint = m_SaliencyPoints[i];
                                const uint Scope = TMin(i + MaximalPathDistance, TotalPointNodes);
                                for (uint j = ip; j < Scope; ++j)
                                {
                                    pSaliencyPoint->AddToCharacterization(m_SaliencyPoints[j]);
                                }
                                pSaliencyPoint->CharacterizeLineal(ThresholdTransitional, ThresholdElongated);
                            }
                            break;
                        case CSaliencyPoint::ePower:
                            for (uint i = 0, ip = 1; i < TotalPointNodes; ++i, ++ip)
                            {
                                CSaliencyPoint* pSaliencyPoint = m_SaliencyPoints[i];
                                const uint Scope = TMin(i + MaximalPathDistance, TotalPointNodes);
                                for (uint j = ip; j < Scope; ++j)
                                {
                                    pSaliencyPoint->AddToCharacterization(m_SaliencyPoints[j]);
                                }
                                pSaliencyPoint->CharacterizePower(ThresholdTransitional, ThresholdElongated, Power);
                            }
                            break;
                        case CSaliencyPoint::eApeture:
                            for (uint i = 0, ip = 1; i < TotalPointNodes; ++i, ++ip)
                            {
                                CSaliencyPoint* pSaliencyPoint = m_SaliencyPoints[i];
                                const uint Scope = TMin(i + MaximalPathDistance, TotalPointNodes);
                                for (uint j = ip; j < Scope; ++j)
                                {
                                    pSaliencyPoint->AddToCharacterization(m_SaliencyPoints[j]);
                                }
                                pSaliencyPoint->CharacterizeApeture(ThresholdTransitional, ThresholdElongated);
                            }
                            break;
                        case CSaliencyPoint::ePowerApeture:
                            for (uint i = 0, ip = 1; i < TotalPointNodes; ++i, ++ip)
                            {
                                CSaliencyPoint* pSaliencyPoint = m_SaliencyPoints[i];
                                const uint Scope = TMin(i + MaximalPathDistance, TotalPointNodes);
                                for (uint j = ip; j < Scope; ++j)
                                {
                                    pSaliencyPoint->AddToCharacterization(m_SaliencyPoints[j]);
                                }
                                pSaliencyPoint->CharacterizePowerApeture(ThresholdTransitional, ThresholdElongated, Power);
                            }
                            break;
                    }
            }

            const list<CSaliencySegment*>& CSaliencyPath::Segmentation()
            {
                vector<CSaliencyPoint*>::iterator EndSaliencyPoints = m_SaliencyPoints.end();
                for (vector<CSaliencyPoint*>::iterator ppPointNode = m_SaliencyPoints.begin(); ppPointNode != EndSaliencyPoints; ++ppPointNode)
                    if (!(*ppPointNode)->GetSegmentNode())
                    {
                        m_SaliencySegments.push_back(new CSaliencySegment(*ppPointNode, this));
                    }
                m_SaliencySegments.sort(SortSaliencySegmentByLengthDescending);
                list<CSaliencySegment*>::iterator EndSaliencySegments = m_SaliencySegments.end();
                for (list<CSaliencySegment*>::iterator ppSegmentNode = m_SaliencySegments.begin(); ppSegmentNode != EndSaliencySegments; ++ppSegmentNode)
                {
                    (*ppSegmentNode)->DetectSegmentNodeIncidences();
                }
                return m_SaliencySegments;
            }

            void CSaliencyPath::ExtractLineSegments(real LineSegmentUncertaintyThreshold, real LineSegmentMinimalMainAxisLength, real LineSegmentMaximalSecondaryAxisLength, int LineSegmentMinimalPointNodes)
            {
                list<CSaliencySegment*>::iterator EndSaliencySegments = m_SaliencySegments.end();
                for (list<CSaliencySegment*>::iterator ppSegmentNode = m_SaliencySegments.begin(); ppSegmentNode != EndSaliencySegments; ++ppSegmentNode)
                    if ((*ppSegmentNode)->GetCharacterizationType() == CSaliencyPoint::eElongated)
                    {
                        //pSaliencySegment->MultiLineRegression(LineSegmentUncertaintyThreshold, LineSegmentMinimalMainAxisLength, LineSegmentMaximalSecondaryAxisLength, LineSegmentMinimalPointNodes, CSaliencySegment::eEccentricityWeighting);*/
                    }
            }

            bool CSaliencyPath::IsGeometricSalient(real MinimalLength)
            {
                /*TList<CSaliencySegment*>::iterator EndSaliencySegments = m_SaliencySegments.end();
                 for (TList<CSaliencySegment*>::iterator ppSegmentNode = m_SaliencySegments.begin(); ppSegmentNode != EndSaliencySegments; ++ppSegmentNode)
                 if ((*ppSegmentNode)->GetLength() > MinimalLength)
                 return true;*/
                return false;
            }
        }
    }
}
