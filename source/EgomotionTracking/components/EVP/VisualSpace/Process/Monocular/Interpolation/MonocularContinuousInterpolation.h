/*
 * MonocularContinuousInterpolation.h
 *
 *  Created on: 23.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../Kernels/InterpolationKernel2D.h"
#include "../../Base/ImageProcessBase.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                namespace Interpolation
                {
                    class CMonocularContinuousInterpolation: public TImageProcessBase<real, real, CDiscreteTristimulusPixel>
                    {
                    public:

                        class CParameters: public CParametersBase
                        {
                        public:

                            enum InterpolationMethod
                            {
                                eRadial, eBicubic
                            };

                            enum DisplayMode
                            {
                                eRoundedIntensity, eScaledIntensity
                            };

                            CParameters(CMonocularContinuousInterpolation* pHost) :
                                CParametersBase(), m_pHost(pHost), m_InterpolationMethod(eBicubic), m_DisplayMode(eScaledIntensity), m_EnableMaskImageUsage(false)
                            {
                            }

                            CParametersBase::ParameterChangeResult SetInterpolationMethod(const InterpolationMethod Method, const bool Wait = true);
                            InterpolationMethod GetInterpolationMethod() const;

                            CParametersBase::ParameterChangeResult SetDisplayMode(const DisplayMode Mode, const bool Wait = true);
                            DisplayMode GetDisplayMode() const;

                            CParametersBase::ParameterChangeResult SetEnableMaskImageUsage(const bool EnableMaskImageUsage, const bool Wait = true);
                            bool GetEnableMaskImageUsage() const;

                        protected:

                            CMonocularContinuousInterpolation* m_pHost;
                            InterpolationMethod m_InterpolationMethod;
                            DisplayMode m_DisplayMode;
                            bool m_EnableMaskImageUsage;
                        };

                        CMonocularContinuousInterpolation(const uint ScaleFactor, CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, Threading::CMutex* pInputImageMutex);
                        CMonocularContinuousInterpolation(const uint ScaleFactor, CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, const TImage<bool>* pInputMaksImage, Threading::CMutex* pInputImageMutex);
                        ~CMonocularContinuousInterpolation() override;

                        bool Execute(const Identifier TrialId) override;
                        bool Display() override;
                        CParameters* GetParameters() override;

                        uint GetScaleFactor() const;
                        const TImage<bool>* GetOutputMaksImage(const uint Margin) const;

                    protected:

                        friend class CMonocularContinuousInterpolation::CParameters;

                        void Interpolate();

                        void UpdateInterpolationMethod();

                        void DisplayIntensityRounding();
                        void DisplayScaledIntensity();
                        void GetExtrema(real& Maximal, real& Minimal) const;

                        CParameters m_Parameters;
                        uint m_ScaleFactor;
                        CImageActiveZone m_InternalActiveZone;
                        Kernels::CInterpolationKernel2D* m_pInterpolationKernel;
                        const TImage<bool>* m_pInputMaksImage;
                    };
                }
            }
        }
    }
}

