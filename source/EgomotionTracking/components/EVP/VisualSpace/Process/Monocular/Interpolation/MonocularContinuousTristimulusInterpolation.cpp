/*
 * MonocularContinuousTristimulusInterpolation.cpp
 *
 *  Created on: 23.10.2011
 *      Author: gonzalez
 */

#include "MonocularContinuousTristimulusInterpolation.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularContinuousTristimulusInterpolation::CParameters
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                CParametersBase::ParameterChangeResult CMonocularContinuousTristimulusInterpolation::CParameters::SetInterpolationMethod(const InterpolationMethod Method, const bool Wait)
                {
                    if (!((Method == CParameters::eBicubic) || (Method == CParameters::eRadial)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (Method == m_InterpolationMethod)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_InterpolationMethod = Method;
                        m_pHost->UpdateInterpolationMethod();
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;

                }

                CMonocularContinuousTristimulusInterpolation::CParameters::InterpolationMethod CMonocularContinuousTristimulusInterpolation::CParameters::GetInterpolationMethod() const
                {
                    return m_InterpolationMethod;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousTristimulusInterpolation::CParameters::SetDisplayMode(const DisplayMode Mode, const bool Wait)
                {
                    if (!((Mode == CParameters::eRoundedIntensity) || (Mode == CParameters::eScaledIntensity)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (Mode == m_DisplayMode)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_DisplayMode = Mode;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                CMonocularContinuousTristimulusInterpolation::CParameters::DisplayMode CMonocularContinuousTristimulusInterpolation::CParameters::GetDisplayMode() const
                {
                    return m_DisplayMode;
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularContinuousTristimulusInterpolation
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                CMonocularContinuousTristimulusInterpolation::CMonocularContinuousTristimulusInterpolation(const uint ScaleFactor, CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, Threading::CMutex* pInputImageMutex) :
                    TImageProcessBase<CContinuousTristimulusPixel, CContinuousTristimulusPixel, CDiscreteTristimulusPixel> (CImageProcessBase::eContinuousTristimulusInterpolation, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this), m_InternalActiveZone(m_pInputActiveZone, 2, true), m_pInterpolationKernel(nullptr)
                {
                    if (m_IsEnabled)
                    {
                        m_OutputActiveZone.SetConnection(&m_InternalActiveZone, real(ScaleFactor));
                        m_pOutputImage = new TImage<CContinuousTristimulusPixel> (m_OutputActiveZone.GetSize());
                        m_pDisplayImage = new TImage<CDiscreteTristimulusPixel> (m_OutputActiveZone.GetSize());
                        m_pInternalInputImage = new TImage<CContinuousTristimulusPixel> (m_pInputActiveZone->GetSize());
                        m_pInterpolationKernel = new Kernels::CInterpolationKernel2D(ScaleFactor);
                        UpdateInterpolationMethod();
                    }
                }

                CMonocularContinuousTristimulusInterpolation::~CMonocularContinuousTristimulusInterpolation()
                {
                    m_OutputActiveZone.Disconnect(true, true);
                    RELEASE_OBJECT(m_pOutputImage)
                    RELEASE_OBJECT(m_pDisplayImage)
                    RELEASE_OBJECT(m_pInternalInputImage)
                    RELEASE_OBJECT(m_pInterpolationKernel)
                }

                bool CMonocularContinuousTristimulusInterpolation::Execute(const Identifier TrialId)
                {
                    if (m_IsEnabled && m_OutputActiveZone.IsEnabled())
                    {
                        if (StartExecuting(TrialId))
                        {
                            START_PROCESS_EXECUTION_LOG("Execute", TrialId)

                            LoadInputImage();

                            Interpolate();

                            STOP_PROCESS_EXECUTION_LOG

                            return FinishExecution();
                        }
                    }
                    return false;
                }

                bool CMonocularContinuousTristimulusInterpolation::Display()
                {
                    if (m_IsEnabled && m_OutputActiveZone.IsEnabled() && GetTotalExecutions())
                    {
                        if (StartDisplaying())
                        {
                            m_pDisplayImage->Clear();
                            switch (m_Parameters.GetDisplayMode())
                            {
                                case CParameters::eRoundedIntensity:
                                    DisplayIntensityRounding();
                                    break;
                                case CParameters::eScaledIntensity:
                                    DisplayScaledIntensity();
                                    break;
                            }
                            return FinishDisplaying();
                        }
                    }
                    return false;
                }

                CMonocularContinuousTristimulusInterpolation::CParameters* CMonocularContinuousTristimulusInterpolation::GetParameters()
                {
                    return m_IsEnabled ? &m_Parameters : nullptr;
                }

                void CMonocularContinuousTristimulusInterpolation::Interpolate()
                {
                    const coordinate X0 = m_InternalActiveZone.GetX0();
                    const coordinate Y0 = m_InternalActiveZone.GetY0();
                    const coordinate X1 = m_InternalActiveZone.GetX1();
                    const coordinate Y1 = m_InternalActiveZone.GetY1();
                    const uint ScaleFactor = m_pInterpolationKernel->GetScaleFactor();
                    const uint InputWidth = m_InternalActiveZone.GetWidth();
                    const uint OutputWidth = m_OutputActiveZone.GetWidth();
                    const uint OutputOffset = OutputWidth * ScaleFactor;
                    const Kernels::CInterpolationKernel2D::InterpolationContribution* pBaseContributions = m_pInterpolationKernel->GetReadOnlyKernelBuffer();
                    const CContinuousTristimulusPixel* pAbsoluteBaseInputPixel = m_pInternalInputImage->GetBeginReadOnlyBuffer();
                    CContinuousTristimulusPixel* pAbsoluteBaseOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                    for (coordinate ys = Y0; ys < Y1; ++ys, pAbsoluteBaseOutputPixel += OutputOffset)
                    {
                        CContinuousTristimulusPixel* pExternalBaseOutputPixel = pAbsoluteBaseOutputPixel;
                        for (coordinate xs = X0; xs < X1; ++xs, pExternalBaseOutputPixel += ScaleFactor)
                        {
                            const Kernels::CInterpolationKernel2D::InterpolationContribution* pContributions = pBaseContributions;
                            CContinuousTristimulusPixel* pInternalBaseOutputPixel = pExternalBaseOutputPixel;
                            for (uint i = 0; i < ScaleFactor; ++i, pInternalBaseOutputPixel += OutputWidth)
                            {
                                CContinuousTristimulusPixel* pOutputPixel = pInternalBaseOutputPixel;
                                for (uint j = 0; j < ScaleFactor; ++j, ++pContributions)
                                {
                                    const real* pContribution = pContributions->m_W;
                                    CContinuousTristimulusPixel Accumulator;
                                    for (coordinate dy = -1; dy < 3; ++dy)
                                        for (coordinate dx = -1; dx < 3; ++dx)
                                        {
                                            Accumulator.AddWeightedValue(pAbsoluteBaseInputPixel[InputWidth * (ys + dy) + xs + dx], *pContribution++);
                                        }
                                    *pOutputPixel++ = Accumulator;
                                }
                            }
                        }
                    }
                }

                void CMonocularContinuousTristimulusInterpolation::UpdateInterpolationMethod()
                {
                    switch (m_Parameters.GetInterpolationMethod())
                    {
                        case CParameters::eBicubic:
                            m_pInterpolationKernel->LoadBicubic();
                            break;
                        case CParameters::eRadial:
                            m_pInterpolationKernel->LoadRadial();
                            break;
                    }
                }

                void CMonocularContinuousTristimulusInterpolation::DisplayIntensityRounding()
                {
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate Width = m_OutputActiveZone.GetWidth();
                    const CContinuousTristimulusPixel* pBaseOutputPixel = m_pOutputImage->GetReadOnlyBufferAt(X0, Y0);
                    CDiscreteTristimulusPixel* pBaseDisplayPixel = m_pDisplayImage->GetWritableBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseDisplayPixel += Width)
                    {
                        const CContinuousTristimulusPixel* pOutputPixel = pBaseOutputPixel;
                        CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                        for (coordinate x = X0; x < X1; ++x)
                        {
                            *pDisplayPixel++ = *pOutputPixel++;
                        }
                    }
                }

                void CMonocularContinuousTristimulusInterpolation::DisplayScaledIntensity()
                {
                    CContinuousTristimulusPixel Minimal, Maximal;
                    GetExtrema(Maximal, Minimal);
                    const CContinuousTristimulusPixel Range(Maximal, Minimal);
                    const CContinuousTristimulusPixel Scale = Range.GetConditionalScaled(_REAL_255_);
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate Width = m_OutputActiveZone.GetWidth();
                    const CContinuousTristimulusPixel* pBaseOutputPixel = m_pOutputImage->GetReadOnlyBufferAt(X0, Y0);
                    CDiscreteTristimulusPixel* pBaseDisplayPixel = m_pDisplayImage->GetWritableBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseDisplayPixel += Width)
                    {
                        const CContinuousTristimulusPixel* pOutputPixel = pBaseOutputPixel;
                        CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pDisplayPixel)
                        {
                            pDisplayPixel->SetShiftedScaledValue(*pOutputPixel++, Scale, Minimal);
                        }
                    }
                }

                void CMonocularContinuousTristimulusInterpolation::GetExtrema(CContinuousTristimulusPixel& Maximal, CContinuousTristimulusPixel& Minimal) const
                {
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate Width = m_OutputActiveZone.GetWidth();
                    const CContinuousTristimulusPixel* pBaseOutputPixel = m_pOutputImage->GetReadOnlyBufferAt(X0, Y0);
                    Minimal = Maximal = *pBaseOutputPixel;
                    for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width)
                    {
                        const CContinuousTristimulusPixel* pOutputPixel = pBaseOutputPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pOutputPixel)
                        {
                            Maximal.SetChannelsToMaximalValue(pOutputPixel);
                            Minimal.SetChannelsToMinimalValue(pOutputPixel);
                        }
                    }
                }
            }
        }
    }
}
