/*
 * MonocularContinuousTristimulusInterpolation.h
 *
 *  Created on: 23.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../../Kernels/InterpolationKernel2D.h"
#include "../../Base/ImageProcessBase.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                class CMonocularContinuousTristimulusInterpolation: public TImageProcessBase<CContinuousTristimulusPixel, CContinuousTristimulusPixel, CDiscreteTristimulusPixel>
                {
                public:

                    class CParameters: public CParametersBase
                    {
                    public:

                        enum InterpolationMethod
                        {
                            eRadial, eBicubic
                        };

                        enum DisplayMode
                        {
                            eRoundedIntensity, eScaledIntensity
                        };

                        CParameters(CMonocularContinuousTristimulusInterpolation* pHost) :
                            CParametersBase(), m_pHost(pHost), m_InterpolationMethod(eBicubic), m_DisplayMode(eScaledIntensity)
                        {
                        }

                        CParametersBase::ParameterChangeResult SetInterpolationMethod(const InterpolationMethod Method, const bool Wait = true);
                        InterpolationMethod GetInterpolationMethod() const;

                        CParametersBase::ParameterChangeResult SetDisplayMode(const DisplayMode Mode, const bool Wait = true);
                        DisplayMode GetDisplayMode() const;

                    protected:

                        CMonocularContinuousTristimulusInterpolation* m_pHost;
                        InterpolationMethod m_InterpolationMethod;
                        DisplayMode m_DisplayMode;
                    };

                    CMonocularContinuousTristimulusInterpolation(const uint ScaleFactor, CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, Threading::CMutex* pInputImageMutex);
                    ~CMonocularContinuousTristimulusInterpolation() override;

                    bool Execute(const Identifier TrialId) override;
                    bool Display() override;
                    CParameters* GetParameters() override;

                protected:

                    friend class CMonocularContinuousTristimulusInterpolation::CParameters;

                    void Interpolate();

                    void UpdateInterpolationMethod();

                    void DisplayIntensityRounding();
                    void DisplayScaledIntensity();
                    void GetExtrema(CContinuousTristimulusPixel& Maximal, CContinuousTristimulusPixel& Minimal) const;

                    CParameters m_Parameters;
                    CImageActiveZone m_InternalActiveZone;
                    Kernels::CInterpolationKernel2D* m_pInterpolationKernel;

                };
            }
        }
    }
}

