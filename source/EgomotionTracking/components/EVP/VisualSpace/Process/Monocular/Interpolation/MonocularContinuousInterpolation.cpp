/*
 * MonocularContinuousInterpolation.cpp
 *
 *  Created on: 23.10.2011
 *      Author: gonzalez
 */

#include "MonocularContinuousInterpolation.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                namespace Interpolation
                {
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //CMonocularContinuousInterpolation::CParameters
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    CParametersBase::ParameterChangeResult CMonocularContinuousInterpolation::CParameters::SetInterpolationMethod(const InterpolationMethod Method, const bool Wait)
                    {
                        if (!((Method == CParameters::eBicubic) || (Method == CParameters::eRadial)))
                        {
                            return CParametersBase::eFailure_ParametersOutofScope;
                        }
                        if (Method == m_InterpolationMethod)
                        {
                            return CParametersBase::eNoChange;
                        }
                        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                        {
                            m_InterpolationMethod = Method;
                            m_pHost->UpdateInterpolationMethod();
                            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                        }
                        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;

                    }

                    CMonocularContinuousInterpolation::CParameters::InterpolationMethod CMonocularContinuousInterpolation::CParameters::GetInterpolationMethod() const
                    {
                        return m_InterpolationMethod;
                    }

                    CParametersBase::ParameterChangeResult CMonocularContinuousInterpolation::CParameters::SetDisplayMode(const DisplayMode Mode, const bool Wait)
                    {
                        if (!((Mode == CParameters::eRoundedIntensity) || (Mode == CParameters::eScaledIntensity)))
                        {
                            return CParametersBase::eFailure_ParametersOutofScope;
                        }
                        if (Mode == m_DisplayMode)
                        {
                            return CParametersBase::eNoChange;
                        }
                        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                        {
                            m_DisplayMode = Mode;
                            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                        }
                        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                    }

                    CMonocularContinuousInterpolation::CParameters::DisplayMode CMonocularContinuousInterpolation::CParameters::GetDisplayMode() const
                    {
                        return m_DisplayMode;
                    }

                    CParametersBase::ParameterChangeResult CMonocularContinuousInterpolation::CParameters::SetEnableMaskImageUsage(const bool EnableMaskImageUsage, const bool Wait)
                    {
                        if (EnableMaskImageUsage == m_EnableMaskImageUsage)
                        {
                            return CParametersBase::eNoChange;
                        }
                        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                        {
                            m_EnableMaskImageUsage = EnableMaskImageUsage;
                            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                        }
                        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                    }

                    bool CMonocularContinuousInterpolation::CParameters::GetEnableMaskImageUsage() const
                    {
                        return m_EnableMaskImageUsage;
                    }

                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //CMonocularContinuousInterpolation
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    CMonocularContinuousInterpolation::CMonocularContinuousInterpolation(const uint ScaleFactor, CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, Threading::CMutex* pInputImageMutex) :
                        TImageProcessBase<real, real, CDiscreteTristimulusPixel>(CImageProcessBase::eContinuousInterpolation, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this), m_ScaleFactor(ScaleFactor), m_InternalActiveZone(m_pInputActiveZone, 2, true), m_pInterpolationKernel(nullptr), m_pInputMaksImage(nullptr)
                    {
                        if (m_IsEnabled)
                        {
                            m_OutputActiveZone.SetConnection(&m_InternalActiveZone, real(m_ScaleFactor));
                            m_pOutputImage = new TImage<real>(m_OutputActiveZone.GetSize());
                            m_pDisplayImage = new TImage<CDiscreteTristimulusPixel>(m_OutputActiveZone.GetSize());
                            m_pInternalInputImage = new TImage<real>(m_pInputActiveZone->GetSize());
                            m_pInterpolationKernel = new Kernels::CInterpolationKernel2D(m_ScaleFactor);
                            UpdateInterpolationMethod();
                        }
                    }

                    CMonocularContinuousInterpolation::CMonocularContinuousInterpolation(const uint ScaleFactor, CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, const TImage<bool>* pInputMaksImage, Threading::CMutex* pInputImageMutex) :
                        TImageProcessBase<real, real, CDiscreteTristimulusPixel>(CImageProcessBase::eContinuousInterpolation, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this), m_ScaleFactor(ScaleFactor), m_InternalActiveZone(m_pInputActiveZone, 2, true), m_pInterpolationKernel(nullptr), m_pInputMaksImage(pInputMaksImage)
                    {
                        if (m_IsEnabled && ((!m_pInputMaksImage) || (m_pInputMaksImage && (m_pInputMaksImage->GetSize() == m_pInputActiveZone->GetSize()))))
                        {
                            m_OutputActiveZone.SetConnection(&m_InternalActiveZone, real(m_ScaleFactor));
                            m_pOutputImage = new TImage<real>(m_OutputActiveZone.GetSize());
                            m_pDisplayImage = new TImage<CDiscreteTristimulusPixel>(m_OutputActiveZone.GetSize());
                            m_pInternalInputImage = new TImage<real>(m_pInputActiveZone->GetSize());
                            m_pInterpolationKernel = new Kernels::CInterpolationKernel2D(m_ScaleFactor);
                            UpdateInterpolationMethod();
                        }
                    }

                    CMonocularContinuousInterpolation::~CMonocularContinuousInterpolation()
                    {
                        m_OutputActiveZone.Disconnect(true, true);
                        RELEASE_OBJECT(m_pOutputImage)
                        RELEASE_OBJECT(m_pDisplayImage)
                        RELEASE_OBJECT(m_pInternalInputImage)
                        RELEASE_OBJECT(m_pInterpolationKernel)
                    }

                    bool CMonocularContinuousInterpolation::Execute(const Identifier TrialId)
                    {
                        if (m_IsEnabled && m_OutputActiveZone.IsEnabled())
                        {
                            if (StartExecuting(TrialId))
                            {
                                START_PROCESS_EXECUTION_LOG("Execute", TrialId)

                                LoadInputImage();

                                Interpolate();

                                STOP_PROCESS_EXECUTION_LOG

                                return FinishExecution();
                            }
                        }
                        return false;
                    }

                    bool CMonocularContinuousInterpolation::Display()
                    {
                        if (m_IsEnabled && m_OutputActiveZone.IsEnabled() && GetTotalExecutions())
                        {
                            if (StartDisplaying())
                            {
                                m_pDisplayImage->Clear();
                                switch (m_Parameters.GetDisplayMode())
                                {
                                    case CParameters::eRoundedIntensity:
                                        DisplayIntensityRounding();
                                        break;
                                    case CParameters::eScaledIntensity:
                                        DisplayScaledIntensity();
                                        break;
                                }
                                return FinishDisplaying();
                            }
                        }
                        return false;
                    }

                    CMonocularContinuousInterpolation::CParameters* CMonocularContinuousInterpolation::GetParameters()
                    {
                        return m_IsEnabled ? &m_Parameters : nullptr;
                    }

                    uint CMonocularContinuousInterpolation::GetScaleFactor() const
                    {
                        return m_ScaleFactor;
                    }

                    const TImage<bool>* CMonocularContinuousInterpolation::GetOutputMaksImage(const uint Margin) const
                    {
                        if (m_pInputMaksImage)
                        {
                            TImage<bool>* pOutputMaksImage = new TImage<bool>(m_pOutputImage->GetSize());
                            pOutputMaksImage->Clear();
                            const coordinate X0 = m_InternalActiveZone.GetX0();
                            const coordinate Y0 = m_InternalActiveZone.GetY0();
                            const coordinate X1 = m_InternalActiveZone.GetX1();
                            const coordinate Y1 = m_InternalActiveZone.GetY1();
                            const uint ScaleFactor = m_pInterpolationKernel->GetScaleFactor();
                            const uint OutputWidth = m_OutputActiveZone.GetWidth();
                            const uint OutputOffset = OutputWidth * ScaleFactor;
                            const bool* pInputMaskPixel = m_pInputMaksImage->GetReadOnlyBufferAt(X0, Y0);
                            const uint MaskOffset = m_InternalActiveZone.GetWidth() - m_InternalActiveZone.GetActiveWidth();
                            bool* pAbsoluteBaseOutputPixel = pOutputMaksImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                            for (coordinate ys = Y0; ys < Y1; ++ys, pAbsoluteBaseOutputPixel += OutputOffset, pInputMaskPixel += MaskOffset)
                            {
                                bool* pExternalBaseOutputPixel = pAbsoluteBaseOutputPixel;
                                for (coordinate xs = X0; xs < X1; ++xs, ++pInputMaskPixel, pExternalBaseOutputPixel += ScaleFactor)
                                    if (*pInputMaskPixel)
                                    {
                                        bool* pInternalBaseOutputPixel = pExternalBaseOutputPixel;
                                        for (uint i = 0; i < ScaleFactor; ++i, pInternalBaseOutputPixel += OutputWidth)
                                        {
                                            memset(pInternalBaseOutputPixel, true, ScaleFactor);
                                        }
                                    }
                            }
                            if (Margin)
                            {
                                const coordinate Radius = Margin;
                                TImage<bool>* pOutputMaksImageMargin = new TImage<bool>(pOutputMaksImage->GetSize());
                                pOutputMaksImageMargin->Clear();
                                const coordinate Height = pOutputMaksImage->GetHeight();
                                const coordinate Width = pOutputMaksImage->GetWidth();
                                const coordinate SubHeight = Height - 1;
                                const coordinate SubWidth = Width - 1;
                                const bool* pInPutPixel = pOutputMaksImage->GetBeginWritableBuffer();
                                bool* pOutPutPixel = pOutputMaksImageMargin->GetBeginWritableBuffer();
                                for (coordinate Y = 0; Y < Height; ++Y)
                                    for (coordinate X = 0; X < Width; ++X, ++pOutPutPixel, ++pInPutPixel)
                                        if (*pInPutPixel)
                                        {
                                            bool IsActive = true;
                                            const coordinate XS0 = TMax(X - Radius, 0);
                                            const coordinate YS1 = TMin(Y + Radius, SubHeight);
                                            const coordinate XS1 = TMin(X + Radius, SubWidth);
                                            for (coordinate YS = TMax(Y - Radius, 0); IsActive && (YS <= YS1); ++YS)
                                                for (coordinate XS = XS0; XS <= XS1; ++XS)
                                                    if (!pOutputMaksImage->GetValueAt(XS, YS))
                                                    {
                                                        IsActive = false;
                                                        break;
                                                    }
                                            *pOutPutPixel = IsActive;
                                        }
                                RELEASE_OBJECT(pOutputMaksImage)
                                return pOutputMaksImageMargin;
                            }
                            return pOutputMaksImage;
                        }
                        return nullptr;
                    }

                    void CMonocularContinuousInterpolation::Interpolate()
                    {
                        const coordinate X0 = m_InternalActiveZone.GetX0();
                        const coordinate Y0 = m_InternalActiveZone.GetY0();
                        const coordinate X1 = m_InternalActiveZone.GetX1();
                        const coordinate Y1 = m_InternalActiveZone.GetY1();
                        const uint ScaleFactor = m_pInterpolationKernel->GetScaleFactor();
                        const uint InputWidth = m_InternalActiveZone.GetWidth();
                        const uint OutputWidth = m_OutputActiveZone.GetWidth();
                        const uint OutputOffset = OutputWidth * ScaleFactor;
                        const Kernels::CInterpolationKernel2D::InterpolationContribution* pBaseContributions = m_pInterpolationKernel->GetReadOnlyKernelBuffer();
                        const real* pAbsoluteBaseInputPixel = m_pInternalInputImage->GetBeginReadOnlyBuffer();
                        real* pAbsoluteBaseOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                        const bool UseMask = m_pInputMaksImage && m_Parameters.GetEnableMaskImageUsage();
                        if (UseMask)
                        {
                            const bool* pInputMaskPixel = m_pInputMaksImage->GetReadOnlyBufferAt(X0, Y0);
                            const uint MaskOffset = InputWidth - m_InternalActiveZone.GetActiveWidth();
                            for (coordinate ys = Y0; ys < Y1; ++ys, pAbsoluteBaseOutputPixel += OutputOffset, pInputMaskPixel += MaskOffset)
                            {
                                real* pExternalBaseOutputPixel = pAbsoluteBaseOutputPixel;
                                for (coordinate xs = X0; xs < X1; ++xs, ++pInputMaskPixel, pExternalBaseOutputPixel += ScaleFactor)
                                    if (*pInputMaskPixel)
                                    {
                                        const Kernels::CInterpolationKernel2D::InterpolationContribution* pContributions = pBaseContributions;
                                        real* pInternalBaseOutputPixel = pExternalBaseOutputPixel;
                                        for (uint i = 0; i < ScaleFactor; ++i, pInternalBaseOutputPixel += OutputWidth)
                                        {
                                            real* pOutputPixel = pInternalBaseOutputPixel;
                                            for (uint j = 0; j < ScaleFactor; ++j, ++pContributions)
                                            {
                                                const real* pContribution = pContributions->m_W;
                                                real Accumulator = _REAL_ZERO_;
                                                for (coordinate dy = -1; dy < 3; ++dy)
                                                    for (coordinate dx = -1; dx < 3; ++dx)
                                                    {
                                                        Accumulator += pAbsoluteBaseInputPixel[InputWidth * (ys + dy) + (xs + dx)] * *pContribution++;
                                                    }
                                                *pOutputPixel++ = Accumulator;
                                            }
                                        }
                                    }
                            }
                        }
                        else
                        {
                            for (coordinate ys = Y0; ys < Y1; ++ys, pAbsoluteBaseOutputPixel += OutputOffset)
                            {
                                real* pExternalBaseOutputPixel = pAbsoluteBaseOutputPixel;
                                for (coordinate xs = X0; xs < X1; ++xs, pExternalBaseOutputPixel += ScaleFactor)
                                {
                                    const Kernels::CInterpolationKernel2D::InterpolationContribution* pContributions = pBaseContributions;
                                    real* pInternalBaseOutputPixel = pExternalBaseOutputPixel;
                                    for (uint i = 0; i < ScaleFactor; ++i, pInternalBaseOutputPixel += OutputWidth)
                                    {
                                        real* pOutputPixel = pInternalBaseOutputPixel;
                                        for (uint j = 0; j < ScaleFactor; ++j, ++pContributions)
                                        {
                                            const real* pContribution = pContributions->m_W;
                                            real Accumulator = _REAL_ZERO_;
                                            for (coordinate dy = -1; dy < 3; ++dy)
                                                for (coordinate dx = -1; dx < 3; ++dx)
                                                {
                                                    Accumulator += pAbsoluteBaseInputPixel[InputWidth * (ys + dy) + (xs + dx)] * *pContribution++;
                                                }
                                            *pOutputPixel++ = Accumulator;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    void CMonocularContinuousInterpolation::UpdateInterpolationMethod()
                    {
                        switch (m_Parameters.GetInterpolationMethod())
                        {
                            case CParameters::eBicubic:
                                m_pInterpolationKernel->LoadBicubic();
                                break;
                            case CParameters::eRadial:
                                m_pInterpolationKernel->LoadRadial();
                                break;
                        }
                    }

                    void CMonocularContinuousInterpolation::DisplayIntensityRounding()
                    {
                        const coordinate X0 = m_OutputActiveZone.GetX0();
                        const coordinate Y0 = m_OutputActiveZone.GetY0();
                        const coordinate X1 = m_OutputActiveZone.GetX1();
                        const coordinate Y1 = m_OutputActiveZone.GetY1();
                        const coordinate Width = m_OutputActiveZone.GetWidth();
                        const real* pBaseOutputPixel = m_pOutputImage->GetReadOnlyBufferAt(X0, Y0);
                        CDiscreteTristimulusPixel* pBaseDisplayPixel = m_pDisplayImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseDisplayPixel += Width)
                        {
                            const real* pOutputPixel = pBaseOutputPixel;
                            CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                            for (coordinate x = X0; x < X1; ++x)
                            {
                                *pDisplayPixel++ = *pOutputPixel++;
                            }
                        }
                    }

                    void CMonocularContinuousInterpolation::DisplayScaledIntensity()
                    {
                        real Minimal = _REAL_ZERO_, Maximal = _REAL_ZERO_;
                        GetExtrema(Maximal, Minimal);
                        const real Range = Maximal - Minimal;
                        const real Scale = (Range > _REAL_EPSILON_) ? _REAL_255_ / Range : _REAL_ZERO_;
                        const coordinate X0 = m_OutputActiveZone.GetX0();
                        const coordinate Y0 = m_OutputActiveZone.GetY0();
                        const coordinate X1 = m_OutputActiveZone.GetX1();
                        const coordinate Y1 = m_OutputActiveZone.GetY1();
                        const coordinate Width = m_OutputActiveZone.GetWidth();
                        const real* pBaseOutputPixel = m_pOutputImage->GetReadOnlyBufferAt(X0, Y0);
                        CDiscreteTristimulusPixel* pBaseDisplayPixel = m_pDisplayImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseDisplayPixel += Width)
                        {
                            const real* pOutputPixel = pBaseOutputPixel;
                            CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                            for (coordinate x = X0; x < X1; ++x, ++pDisplayPixel)
                            {
                                pDisplayPixel->SetShiftedScaledValue(*pOutputPixel++, Scale, Minimal);
                            }
                        }
                    }

                    void CMonocularContinuousInterpolation::GetExtrema(real& Maximal, real& Minimal) const
                    {
                        const coordinate X0 = m_OutputActiveZone.GetX0();
                        const coordinate Y0 = m_OutputActiveZone.GetY0();
                        const coordinate X1 = m_OutputActiveZone.GetX1();
                        const coordinate Y1 = m_OutputActiveZone.GetY1();
                        const coordinate Width = m_OutputActiveZone.GetWidth();
                        const real* pBaseOutputPixel = m_pOutputImage->GetReadOnlyBufferAt(X0, Y0);
                        Minimal = Maximal = *pBaseOutputPixel;
                        for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width)
                        {
                            const real* pOutputPixel = pBaseOutputPixel;
                            for (coordinate x = X0; x < X1; ++x)
                                if (*pOutputPixel > Maximal)
                                {
                                    Maximal = *pOutputPixel;
                                }
                                else if (*pOutputPixel < Minimal)
                                {
                                    Minimal = *pOutputPixel;
                                }
                        }
                    }
                }
            }
        }
    }
}
