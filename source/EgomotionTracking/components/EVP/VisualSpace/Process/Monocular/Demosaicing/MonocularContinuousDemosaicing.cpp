/*
 * MonocularContinuousDemosaicing.cpp
 *
 *  Created on: 17.10.2011
 *      Author: gonzalez
 */

#include "MonocularContinuousDemosaicing.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                namespace Demosaicing
                {
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //CMonocularContinuousDemosaicing::CParameters
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    CParametersBase::ParameterChangeResult CMonocularContinuousDemosaicing::CParameters::SetDemosaicingMethod(const DemosaicingMethod Method, const bool Wait)
                    {
                        if (Method == m_DemosaicingMethod)
                        {
                            return CParametersBase::eNoChange;
                        }
                        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                        {
                            m_DemosaicingMethod = Method;
                            CParametersBase::ParameterChangeResult Result = m_pHost->UpdateSelectedMethod() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                            return m_pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                        }
                        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                    }

                    CMonocularContinuousDemosaicing::CParameters::DemosaicingMethod CMonocularContinuousDemosaicing::CParameters::GetDemosaicingMethod() const
                    {
                        return m_DemosaicingMethod;
                    }

                    CParametersBase::ParameterChangeResult CMonocularContinuousDemosaicing::CParameters::SetBayerPatternType(const BayerPatternType BayerPattern, const bool Wait)
                    {
                        if (BayerPattern == eUnknownBayerPattern)
                        {
                            return CParametersBase::eFailure_ParametersOutofScope;
                        }
                        if (BayerPattern == m_BayerPattern)
                        {
                            return CParametersBase::eNoChange;
                        }
                        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                        {
                            m_BayerPattern = BayerPattern;
                            CParametersBase::ParameterChangeResult Result = m_pHost->UpdateBayerPatternMap(m_BayerPattern) ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                            return m_pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                        }
                        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                    }

                    BayerPatternType CMonocularContinuousDemosaicing::CParameters::GetBayerPatternType() const
                    {
                        return m_BayerPattern;
                    }

                    CParametersBase::ParameterChangeResult CMonocularContinuousDemosaicing::CParameters::SetDisplayMode(const DisplayMode Mode, const bool Wait)
                    {
                        if (!((Mode == eRoundedIntensity) || (Mode == eScaledIntensity)))
                        {
                            return CParametersBase::eFailure_ParametersOutofScope;
                        }
                        if (Mode == m_DisplayMode)
                        {
                            return CParametersBase::eNoChange;
                        }
                        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                        {
                            m_DisplayMode = Mode;
                            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                        }
                        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                    }

                    CMonocularContinuousDemosaicing::CParameters::DisplayMode CMonocularContinuousDemosaicing::CParameters::GetDisplayMode() const
                    {
                        return m_DisplayMode;
                    }

                    CParametersBase::ParameterChangeResult CMonocularContinuousDemosaicing::CParameters::SetGaussianKernelSigma(const real GaussianKernelSigma, const bool Wait)
                    {
                        if (GaussianKernelSigma <= _REAL_ZERO_)
                        {
                            return CParametersBase::eFailure_ParametersOutofScope;
                        }
                        if (GaussianKernelSigma == m_GaussianKernelSigma)
                        {
                            return CParametersBase::eNoChange;
                        }
                        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                        {
                            m_GaussianKernelSigma = GaussianKernelSigma;
                            CParametersBase::ParameterChangeResult Result = m_pHost->UpdateGaussianKernelSigma(m_GaussianKernelSigma) ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                            return m_pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                        }
                        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                    }

                    real CMonocularContinuousDemosaicing::CParameters::GetGaussianKernelSigma() const
                    {
                        return m_GaussianKernelSigma;
                    }

                    CParametersBase::ParameterChangeResult CMonocularContinuousDemosaicing::CParameters::SetAdaptiveBaseSpatialKernelSigma(const real AdaptiveBaseSpatialKernelSigma, const bool Wait)
                    {
                        if (AdaptiveBaseSpatialKernelSigma <= _REAL_ZERO_)
                        {
                            return CParametersBase::eFailure_ParametersOutofScope;
                        }
                        if (AdaptiveBaseSpatialKernelSigma == m_AdaptiveBaseSpatialKernelSigma)
                        {
                            return CParametersBase::eNoChange;
                        }
                        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                        {
                            m_AdaptiveBaseSpatialKernelSigma = AdaptiveBaseSpatialKernelSigma;
                            CParametersBase::ParameterChangeResult Result = m_pHost->UpdateAdaptiveBaseSpatialKernelSigma(m_AdaptiveBaseSpatialKernelSigma) ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                            return m_pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                        }
                        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                    }

                    real CMonocularContinuousDemosaicing::CParameters::GeAdaptiveBaseSpatialKernelSigma() const
                    {
                        return m_AdaptiveBaseSpatialKernelSigma;
                    }

                    CParametersBase::ParameterChangeResult CMonocularContinuousDemosaicing::CParameters::SetAdaptiveRangeKernelSigma(const real AdaptiveRangeKernelSigma, const bool Wait)
                    {
                        if (AdaptiveRangeKernelSigma <= _REAL_ZERO_)
                        {
                            return CParametersBase::eFailure_ParametersOutofScope;
                        }
                        if (AdaptiveRangeKernelSigma == m_AdaptiveRangeKernelSigma)
                        {
                            return CParametersBase::eNoChange;
                        }
                        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                        {
                            m_AdaptiveRangeKernelSigma = AdaptiveRangeKernelSigma;
                            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                        }
                        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                    }

                    real CMonocularContinuousDemosaicing::CParameters::GeAdaptiveRangeKernelSigma() const
                    {
                        return m_AdaptiveRangeKernelSigma;
                    }

                    CParametersBase::ParameterChangeResult CMonocularContinuousDemosaicing::CParameters::SetLuminanceContribution(const real LuminanceContribution, const bool Wait)
                    {
                        if (LuminanceContribution < _REAL_ZERO_)
                        {
                            return CParametersBase::eFailure_ParametersOutofScope;
                        }
                        if (LuminanceContribution == m_LuminanceContribution)
                        {
                            return CParametersBase::eNoChange;
                        }
                        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                        {
                            m_LuminanceContribution = LuminanceContribution;
                            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                        }
                        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                    }

                    real CMonocularContinuousDemosaicing::CParameters::GetLuminanceContribution() const
                    {
                        return m_LuminanceContribution;
                    }

                    CParametersBase::ParameterChangeResult CMonocularContinuousDemosaicing::CParameters::SetChromaticContribution(const real ChromaticContribution, const bool Wait)
                    {
                        if (ChromaticContribution < _REAL_ZERO_)
                        {
                            return CParametersBase::eFailure_ParametersOutofScope;
                        }
                        if (ChromaticContribution == m_ChromaticContribution)
                        {
                            return CParametersBase::eNoChange;
                        }
                        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                        {
                            m_ChromaticContribution = ChromaticContribution;
                            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                        }
                        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                    }

                    real CMonocularContinuousDemosaicing::CParameters::GetChromaticContribution() const
                    {
                        return m_ChromaticContribution;
                    }
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //CMonocularContinuousDemosaicing
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _DEMOSAICING_USE_LOOKUP_TABLE_
                    INITIALIZE_EXPONENTIAL_LOOKUP_TABLE(CMonocularContinuousDemosaicing)
#endif

                    CMonocularContinuousDemosaicing::CMonocularContinuousDemosaicing(const BayerPatternType BayerPattern, CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, Threading::CMutex* pInputImageMutex) :
                        TImageProcessBase<real, CContinuousTristimulusPixel, CDiscreteTristimulusPixel>(CImageProcessBase::eContinuousDemosaicing, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this, BayerPattern), m_pGaussianKernel(nullptr), m_pAdaptiveBaseKernel(nullptr), m_pAdaptiveImage(nullptr)
                    {
                        if (m_IsEnabled)
                        {
                            const CImageSize& Size = m_pInputActiveZone->GetSize();
                            m_pOutputImage = new TImage<CContinuousTristimulusPixel>(Size);
                            m_pDisplayImage = new TImage<CDiscreteTristimulusPixel>(Size);
                            m_pInternalInputImage = new TImage<real>(Size);
                            m_pAdaptiveImage = new TImage<CContinuousTristimulusPixel>(Size);
                            m_pGaussianKernel = new Kernels::CConvolutionGaussianKernel2D(m_Parameters.GetGaussianKernelSigma());
                            m_pAdaptiveBaseKernel = new Kernels::CConvolutionGaussianKernel2D(m_Parameters.GeAdaptiveBaseSpatialKernelSigma());
                            m_pGaussianKernel->ScaleMinimal();
                            m_pAdaptiveBaseKernel->ScaleMinimal();
                            m_OutputActiveZone.SetConnection(m_pInputActiveZone, this, CMonocularContinuousDemosaicing::UpdateOutputActiveZone);
                            UpdateBayerPatternMap(m_Parameters.GetBayerPatternType());
#ifdef _DEMOSAICING_USE_LOOKUP_TABLE_
                            LOAD_EXPONENTIAL_LOOKUP_TABLE(CMonocularContinuousDemosaicing)
#endif
                        }
                    }

                    CMonocularContinuousDemosaicing::~CMonocularContinuousDemosaicing()
                    {
                        m_OutputActiveZone.Disconnect(true, true);
                        RELEASE_OBJECT(m_pOutputImage)
                        RELEASE_OBJECT(m_pDisplayImage)
                        RELEASE_OBJECT(m_pInternalInputImage)
                        RELEASE_OBJECT(m_pAdaptiveImage)
                        RELEASE_OBJECT(m_pGaussianKernel)
                        RELEASE_OBJECT(m_pAdaptiveBaseKernel)
                    }

                    bool CMonocularContinuousDemosaicing::Execute(const Identifier TrialId)
                    {
                        if (m_IsEnabled && m_OutputActiveZone.IsEnabled())
                        {
                            if (StartExecuting(TrialId))
                            {
                                START_PROCESS_EXECUTION_LOG("Execute", TrialId)

                                LoadInputImage();

                                switch (m_Parameters.GetDemosaicingMethod())
                                {
                                    case CParameters::eBilinear:
                                        BilinearDemosaicing();
                                        break;
                                    case CParameters::eDirectionalGradient:
                                        DirectionalWeightedGradientDemosaicing();
                                        break;
                                    case CParameters::eGaussian:
                                        GaussianDemosaicing();
                                        break;
                                    case CParameters::eAdaptive:
                                        DirectionalWeightedGradientDemosaicing();
                                        AdaptiveDemosaicing();
                                        break;
                                }

                                STOP_PROCESS_EXECUTION_LOG

                                return FinishExecution();
                            }
                        }
                        return false;
                    }

                    bool CMonocularContinuousDemosaicing::Display()
                    {
                        if (m_IsEnabled && m_OutputActiveZone.IsEnabled() && GetTotalExecutions())
                        {
                            if (StartDisplaying())
                            {
                                //m_pDisplayImage->Clear();//REMOVE CImageExporter Does this internally
                                bool Result = false;
                                switch (m_Parameters.GetDisplayMode())
                                {
                                    case CParameters::eRoundedIntensity:
                                        Result = CImageExporter::DisplayRounded(&m_OutputActiveZone, m_pOutputImage, m_pDisplayImage);
                                        break;
                                    case CParameters::eScaledIntensity:
                                        Result = CImageExporter::DisplayScaled(&m_OutputActiveZone, m_pOutputImage, m_pDisplayImage);
                                        break;
                                }
                                return FinishDisplaying() && Result;
                            }
                        }
                        return false;
                    }

                    CMonocularContinuousDemosaicing::CParameters* CMonocularContinuousDemosaicing::GetParameters()
                    {
                        return m_IsEnabled ? &m_Parameters : nullptr;
                    }

                    bool CMonocularContinuousDemosaicing::UpdateSelectedMethod()
                    {
                        return m_OutputActiveZone.UpdateConnection();
                    }

                    bool CMonocularContinuousDemosaicing::UpdateBayerPatternMap(const BayerPatternType BayerPattern)
                    {
                        return LoadBayerPatternMap(BayerPattern, m_BayerPatternMapYX);
                    }

                    bool CMonocularContinuousDemosaicing::UpdateGaussianKernelSigma(const real GaussianKernelSigma)
                    {
                        m_pGaussianKernel->SetStandardDeviation(GaussianKernelSigma);
                        m_pGaussianKernel->ScaleMinimal();
                        if (m_Parameters.GetDemosaicingMethod() == CParameters::eGaussian)
                        {
                            return m_OutputActiveZone.UpdateConnection();
                        }
                        return true;
                    }

                    bool CMonocularContinuousDemosaicing::UpdateAdaptiveBaseSpatialKernelSigma(const real AdaptiveBaseSpatialKernelSigma)
                    {
                        m_pAdaptiveBaseKernel->SetStandardDeviation(AdaptiveBaseSpatialKernelSigma);
                        m_pAdaptiveBaseKernel->ScaleMinimal();
                        if (m_Parameters.GetDemosaicingMethod() == CParameters::eAdaptive)
                        {
                            return m_OutputActiveZone.UpdateConnection();
                        }
                        return true;
                    }

                    bool CMonocularContinuousDemosaicing::UpdateOutputActiveZone(const void* pData, coordinate& OffsetX0, coordinate& OffsetY0, coordinate& OffsetX1, coordinate& OffsetY1)
                    {
                        if (pData)
                        {
                            const CMonocularContinuousDemosaicing* pHost = reinterpret_cast<const CMonocularContinuousDemosaicing*>(pData);
                            if (pHost)
                            {
                                switch (pHost->m_Parameters.GetDemosaicingMethod())
                                {
                                    case CParameters::eBilinear:
                                        OffsetX0 = OffsetY0 = OffsetX1 = OffsetY1 = 1;
                                        break;
                                    case CParameters::eDirectionalGradient:
                                        OffsetX0 = OffsetY0 = OffsetX1 = OffsetY1 = 2;
                                        break;
                                    case CParameters::eGaussian:
                                        OffsetX0 = OffsetY0 = OffsetX1 = OffsetY1 = pHost->m_pGaussianKernel->GetRadius();
                                        break;
                                    case CParameters::eAdaptive:
                                        OffsetX0 = OffsetY0 = OffsetX1 = OffsetY1 = pHost->m_pAdaptiveBaseKernel->GetRadius() + 2;
                                        break;
                                }
                                return true;
                            }
                        }
                        return false;
                    }

                    void CMonocularContinuousDemosaicing::GaussianDemosaicing()
                    {
                        const coordinate SX0 = m_pInputActiveZone->GetX0();
                        const coordinate SY0 = m_pInputActiveZone->GetY0();
                        const coordinate SX1 = SX0 + m_pGaussianKernel->GetWidth();
                        const coordinate SY1 = SY0 + m_pGaussianKernel->GetWidth();
                        const coordinate DX0 = m_OutputActiveZone.GetX0();
                        const coordinate DY0 = m_OutputActiveZone.GetY0();
                        const coordinate DX1 = m_OutputActiveZone.GetX1();
                        const coordinate DY1 = m_OutputActiveZone.GetY1();
                        const coordinate Width = m_OutputActiveZone.GetWidth();
                        const real* pBaseKernel = m_pGaussianKernel->GetKernelReadOnlyBuffer();
                        const real* pBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferAt(SX0, SY0);
                        CContinuousTristimulusPixel* pBaseOutputPixel = m_pOutputImage->GetWritableBufferAt(DX0, DY0);
                        real ChannelsAccumulators[6] = { _REAL_ZERO_ };
                        real* KernelAccumulators = ChannelsAccumulators + 3;
                        for (coordinate y = DY0, y0 = SY0, y1 = SY1; y < DY1; ++y, ++y0, ++y1, pBaseInputPixel += Width, pBaseOutputPixel += Width)
                        {
                            const real* pExternInputPixel = pBaseInputPixel;
                            CContinuousTristimulusPixel* pOutputPixel = pBaseOutputPixel;
                            for (coordinate x = DX0, x0 = SX0, x1 = SX1; x < DX1; ++x, ++x0, ++x1, ++pExternInputPixel, ++pOutputPixel)
                            {
                                const real* pKernel = pBaseKernel;
                                const real* pInternBaseInputPixel = pExternInputPixel;
                                memset(ChannelsAccumulators, 0, sizeof(real) * 6);
                                for (coordinate ys = y0; ys < y1; ++ys, pInternBaseInputPixel += Width)
                                {
                                    const real* pInputPixel = pInternBaseInputPixel;
                                    for (coordinate xs = x0; xs < x1; ++xs, ++pInputPixel, ++pKernel)
                                    {
                                        ChannelsAccumulators[m_BayerPatternMapYX[ys & 0x1][xs & 0x1]] += *pInputPixel * *pKernel;
                                        KernelAccumulators[m_BayerPatternMapYX[ys & 0x1][xs & 0x1]] += *pKernel;
                                    }
                                }
                                pOutputPixel->SetValue(ChannelsAccumulators, KernelAccumulators);
                            }
                        }
                    }

                    void CMonocularContinuousDemosaicing::BilinearDemosaicing()
                    {
                        const coordinate X0 = m_OutputActiveZone.GetX0();
                        const coordinate Y0 = m_OutputActiveZone.GetY0();
                        const coordinate X1 = m_OutputActiveZone.GetX1();
                        const coordinate Y1 = m_OutputActiveZone.GetY1();
                        const coordinate Width = m_OutputActiveZone.GetWidth();
                        const real* pBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferLineAt(Y0);
                        CContinuousTristimulusPixel* pBaseOutputPixel = m_pOutputImage->GetWritableBufferLineAt(Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseInputPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMapYX[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 : X0 + 1;
                            const real* pInputPixel = pBaseInputPixel + XBase;
                            CContinuousTristimulusPixel* pOutputPixel = pBaseOutputPixel + XBase;
                            if (m_BayerPatternMapYX[y & 0x1][(XBase - 1) & 0x1] == ChromaticSpaceRGB::eRed)
                                for (coordinate x = XBase; x < X1; x += 2, pOutputPixel += 2, pInputPixel += 2)
                                {
                                    pOutputPixel->SetValue((pInputPixel[-1] + pInputPixel[1]) * _REAL_HALF_, *pInputPixel, (pInputPixel[-Width] + pInputPixel[Width]) * _REAL_HALF_);
                                }
                            else
                                for (coordinate x = XBase; x < X1; x += 2, pOutputPixel += 2, pInputPixel += 2)
                                {
                                    pOutputPixel->SetValue((pInputPixel[-Width] + pInputPixel[Width]) * _REAL_HALF_, *pInputPixel, (pInputPixel[-1] + pInputPixel[1]) * _REAL_HALF_);
                                }
                        }
                        pBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferLineAt(Y0);
                        pBaseOutputPixel = m_pOutputImage->GetWritableBufferLineAt(Y0);
                        const coordinate NE = 1 - Width, NW = -Width - 1, SW = Width - 1, SE = Width + 1;
                        for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseInputPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMapYX[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 + 1 : X0;
                            const real* pInputPixel = pBaseInputPixel + XBase;
                            CContinuousTristimulusPixel* pOutputPixel = pBaseOutputPixel + XBase;
                            if (m_BayerPatternMapYX[y & 0x1][XBase & 0x1] == ChromaticSpaceRGB::eRed)
                                for (coordinate x = XBase; x < X1; x += 2, pOutputPixel += 2, pInputPixel += 2)
                                {
                                    pOutputPixel->SetValue(*pInputPixel, (pInputPixel[1] + pInputPixel[-1] + pInputPixel[-Width] + pInputPixel[Width]) * _REAL_FOURTH_, (pInputPixel[NE] + pInputPixel[NW] + pInputPixel[SW] + pInputPixel[SE]) * _REAL_FOURTH_);
                                }
                            else
                                for (coordinate x = XBase; x < X1; x += 2, pOutputPixel += 2, pInputPixel += 2)
                                {
                                    pOutputPixel->SetValue((pInputPixel[NE] + pInputPixel[NW] + pInputPixel[SW] + pInputPixel[SE]) * _REAL_FOURTH_, (pInputPixel[1] + pInputPixel[-1] + pInputPixel[-Width] + pInputPixel[Width]) * _REAL_FOURTH_, *pInputPixel);
                                }
                        }
                    }

                    void CMonocularContinuousDemosaicing::DirectionalWeightedGradientDemosaicing()
                    {
                        const real LuminanceContribution = m_Parameters.GetLuminanceContribution();
                        const real ChromaticContribution = m_Parameters.GetChromaticContribution();
                        const coordinate Offset = (m_Parameters.GetDemosaicingMethod() == CParameters::eAdaptive) ? m_pAdaptiveBaseKernel->GetRadius() : 0;
                        const coordinate X0 = m_OutputActiveZone.GetX0() - Offset;
                        const coordinate Y0 = m_OutputActiveZone.GetY0() - Offset;
                        const coordinate X1 = m_OutputActiveZone.GetX1() + Offset;
                        const coordinate Y1 = m_OutputActiveZone.GetY1() + Offset;
                        const coordinate Width = m_OutputActiveZone.GetWidth();
                        const real* pBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferAt(X0, Y0);
                        CContinuousTristimulusPixel* pBaseOutputPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseInputPixel += Width)
                        {
                            const real* pPixel = pBaseInputPixel;
                            CContinuousTristimulusPixel* pRGBPixel = pBaseOutputPixel;
                            for (coordinate x = X0; x < X1; ++x, ++pRGBPixel)
                            {
                                pRGBPixel->SetChannelValue(m_BayerPatternMapYX[y & 0x1][x & 0x1], *pPixel++);
                            }
                        }
                        const coordinate N2 = Width * -2, S2 = Width * 2;
                        pBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferLineAt(Y0);
                        pBaseOutputPixel = m_pOutputImage->GetWritableBufferLineAt(Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseInputPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMapYX[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 + 1 : X0;
                            const real* pInputPixel = pBaseInputPixel + XBase;
                            CContinuousTristimulusPixel* pOutputPixel = pBaseOutputPixel + XBase;
                            for (coordinate x = XBase; x < X1; x += 2, pOutputPixel += 2, pInputPixel += 2)
                            {
                                const real V = RealAbs(pInputPixel[-Width] - pInputPixel[Width]) * LuminanceContribution;
                                const real N = GradientWeightingKernel(V + RealAbs(*pInputPixel - pInputPixel[N2]));
                                const real S = GradientWeightingKernel(V + RealAbs(*pInputPixel - pInputPixel[S2]));
                                const real H = RealAbs(pInputPixel[1] - pInputPixel[-1]) * LuminanceContribution;
                                const real E = GradientWeightingKernel(H + RealAbs(*pInputPixel - pInputPixel[2]));
                                const real W = GradientWeightingKernel(H + RealAbs(*pInputPixel - pInputPixel[-2]));
                                pOutputPixel->SetChannelValue(ChromaticSpaceRGB::eGreen, (N * pInputPixel[-Width] + S * pInputPixel[Width] + E * pInputPixel[1] + W * pInputPixel[-1]) / (N + S + E + W));
                            }
                        }
                        const coordinate NE = -Width + 1, NW = -Width - 1, SE = Width + 1, SW = Width - 1;
                        pBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferLineAt(Y0);
                        pBaseOutputPixel = m_pOutputImage->GetWritableBufferLineAt(Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseInputPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMapYX[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 + 1 : X0;
                            const ChromaticSpaceRGB::ChannelContent CurrentChannel = (m_BayerPatternMapYX[y & 0x1][XBase & 0x1] == ChromaticSpaceRGB::eRed) ? ChromaticSpaceRGB::eBlue : ChromaticSpaceRGB::eRed;
                            const real* pInputPixel = pBaseInputPixel + XBase;
                            CContinuousTristimulusPixel* pOutputPixel = pBaseOutputPixel + XBase;
                            for (coordinate x = XBase; x < X1; x += 2, pOutputPixel += 2, pInputPixel += 2)
                            {
                                const real CurrentGreenValue = pOutputPixel->GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen);
                                const real F = RealAbs(pInputPixel[NE] - pInputPixel[SW]) * ChromaticContribution;
                                const real WNE = GradientWeightingKernel(F + RealAbs(CurrentGreenValue - pOutputPixel[NE].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen)));
                                const real WSW = GradientWeightingKernel(F + RealAbs(CurrentGreenValue - pOutputPixel[SW].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen)));
                                const real B = RealAbs(pInputPixel[NW] - pInputPixel[SE]) * ChromaticContribution;
                                const real WNW = GradientWeightingKernel(B + RealAbs(CurrentGreenValue - pOutputPixel[NW].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen)));
                                const real WSE = GradientWeightingKernel(B + RealAbs(CurrentGreenValue - pOutputPixel[SE].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen)));
                                pOutputPixel->SetChannelValue(CurrentChannel, (WNE * pInputPixel[NE] + WNW * pInputPixel[NW] + WSE * pInputPixel[SE] + WSW * pInputPixel[SW]) / (WNE + WSE + WNW + WSW));
                            }
                        }
                        pBaseOutputPixel = m_pOutputImage->GetWritableBufferLineAt(Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseInputPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMapYX[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 : X0 + 1;
                            CContinuousTristimulusPixel* pOutputPixel = pBaseOutputPixel + XBase;
                            for (coordinate x = XBase; x < X1; x += 2, pOutputPixel += 2)
                            {
                                const real CurrentGreenValue = pOutputPixel->GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen);
                                const real GN = RealAbs(CurrentGreenValue - pOutputPixel[N2].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen));
                                const real GS = RealAbs(CurrentGreenValue - pOutputPixel[S2].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen));
                                const real GE = RealAbs(CurrentGreenValue - pOutputPixel[ 2].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen));
                                const real GW = RealAbs(CurrentGreenValue - pOutputPixel[-2].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen));
                                real V = RealAbs(pOutputPixel[-Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) - pOutputPixel[Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed)) * ChromaticContribution;
                                real N = GradientWeightingKernel(V + GN);
                                real S = GradientWeightingKernel(V + GS);
                                real H = RealAbs(pOutputPixel[1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) - pOutputPixel[-1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed)) * ChromaticContribution;
                                real E = GradientWeightingKernel(H + GE);
                                real W = GradientWeightingKernel(H + GW);
                                pOutputPixel->SetChannelValue(ChromaticSpaceRGB::eRed, (N * pOutputPixel[-Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) + S * pOutputPixel[Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) + E * pOutputPixel[1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) + W * pOutputPixel[-1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed)) / (N + S + E + W));
                                V = RealAbs(pOutputPixel[-Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) - pOutputPixel[Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue)) * ChromaticContribution;
                                N = GradientWeightingKernel(V + GN);
                                S = GradientWeightingKernel(V + GS);
                                H = RealAbs(pOutputPixel[1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) - pOutputPixel[-1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue)) * ChromaticContribution;
                                E = GradientWeightingKernel(H + GE);
                                W = GradientWeightingKernel(H + GW);
                                pOutputPixel->SetChannelValue(ChromaticSpaceRGB::eBlue, (N * pOutputPixel[-Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) + S * pOutputPixel[Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) + E * pOutputPixel[1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) + W * pOutputPixel[-1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue)) / (N + S + E + W));
                            }
                        }
                    }

                    void CMonocularContinuousDemosaicing::AdaptiveDemosaicing()
                    {
                        const real AdaptiveRangeExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(m_Parameters.GeAdaptiveRangeKernelSigma());
                        const coordinate DX0 = m_OutputActiveZone.GetX0();
                        const coordinate DY0 = m_OutputActiveZone.GetY0();
                        const coordinate DX1 = m_OutputActiveZone.GetX1();
                        const coordinate DY1 = m_OutputActiveZone.GetY1();
                        const coordinate SX0 = DX0 - m_pAdaptiveBaseKernel->GetRadius();
                        const coordinate SY0 = DY0 - m_pAdaptiveBaseKernel->GetRadius();
                        const coordinate SX1 = SX0 + m_pAdaptiveBaseKernel->GetWidth();
                        const coordinate SY1 = SY0 + m_pAdaptiveBaseKernel->GetWidth();
                        const coordinate Width = m_OutputActiveZone.GetWidth();
                        const real* pBaseGaussianKernel = m_pAdaptiveBaseKernel->GetKernelReadOnlyBuffer();
                        const CContinuousTristimulusPixel* pInputAbsoluteBaseZonePixel = m_pOutputImage->GetReadOnlyBufferAt(SX0, SY0);
                        const CContinuousTristimulusPixel* pInputBaseCenterPixel = m_pOutputImage->GetReadOnlyBufferAt(DY0, DX0);
                        CContinuousTristimulusPixel* pOutputBasePixel = m_pAdaptiveImage->GetWritableBufferAt(DX0, DY0);
                        real ChannelsAccumulators[6] = { _REAL_ZERO_ };
                        real* KernelAccumulators = ChannelsAccumulators + 3;

#ifdef _DEMOSAICING_USE_LOOKUP_TABLE_

                        const real MaximalDistance = s_ELT.m_MinimalArgumentValue / AdaptiveRangeExponentFactor;
                        const real Scale = s_ELT.m_SamplesPerUnit * AdaptiveRangeExponentFactor;

#endif
                        for (coordinate y = DY0, y0 = SY0, y1 = SY1; y < DY1; ++y, ++y0, ++y1, pInputAbsoluteBaseZonePixel += Width, pInputBaseCenterPixel += Width, pOutputBasePixel += Width)
                        {
                            const CContinuousTristimulusPixel* pInputExternBaseZonePixel = pInputAbsoluteBaseZonePixel;
                            const CContinuousTristimulusPixel* pInputCenterPixel = pInputBaseCenterPixel;
                            CContinuousTristimulusPixel* pOutputPixel = pOutputBasePixel;
                            for (coordinate x = DX0, x0 = SX0, x1 = SX1; x < DX1; ++x, ++x0, ++x1, ++pInputExternBaseZonePixel, ++pInputCenterPixel, ++pOutputPixel)
                            {
                                const CContinuousTristimulusPixel* pInputInternBaseZonePixel = pInputExternBaseZonePixel;
                                const real* pKernel = pBaseGaussianKernel;
                                memset(ChannelsAccumulators, 0, sizeof(real) * 6);
                                for (coordinate ys = y0; ys < y1; ++ys, pInputInternBaseZonePixel += Width)
                                {
                                    const CContinuousTristimulusPixel* pInputZonePixel = pInputInternBaseZonePixel;
                                    for (coordinate xs = x0; xs < x1; ++xs, ++pInputZonePixel, ++pKernel)
                                    {
#ifdef _DEMOSAICING_USE_LOOKUP_TABLE_
                                        const real Distance = pInputZonePixel->GetRGBFixedWeightedSquareEuclideanDistance(pInputCenterPixel);
                                        if (Distance < MaximalDistance)
                                        {
                                            const real AdaptiveKernel = s_ELT.m_pExp[RoundPositiveRealToInteger(Scale * Distance)] * *pKernel;
                                            const ChromaticSpaceRGB::ChannelContent CurrentChannel = m_BayerPatternMapYX[ys & 0x1][xs & 0x1];
                                            KernelAccumulators[CurrentChannel] += AdaptiveKernel;
                                            ChannelsAccumulators[CurrentChannel] += pInputZonePixel->GetReadOnlyChannelReferenceByIndex(CurrentChannel) * AdaptiveKernel;
                                        }
#else

                                        const real AdaptiveKernel = RealExp(AdaptiveRangeExponentFactor * pInputZonePixel->GetRGBFixedWeightedSquareEuclideanDistance(pInputCenterPixel)) * *pKernel;
                                        const ChromaticSpaceRGB::ChannelContent CurrentChannel = m_BayerPatternMapYX[ys & 0x1][xs & 0x1];
                                        KernelAccumulators[CurrentChannel] += AdaptiveKernel;
                                        ChannelsAccumulators[CurrentChannel] += pInputZonePixel->GetReadOnlyChannelReferenceByIndex(CurrentChannel) * AdaptiveKernel;
#endif
                                    }
                                }
                                pOutputPixel->SetValue(ChannelsAccumulators, KernelAccumulators);
                                //const real  Homogenity = KernelAccumulators[0] + KernelAccumulators[1] + KernelAccumulators[2];
                                //const real  Homogenity = RealSqrt(KernelAccumulators[0]*KernelAccumulators[0] + KernelAccumulators[1]*KernelAccumulators[1] + KernelAccumulators[2]*KernelAccumulators[2]);
                            }
                        }
                        m_pOutputImage->Copy(m_pAdaptiveImage);
                    }

                    real CMonocularContinuousDemosaicing::GradientWeightingKernel(const real Gradient) const
                    {
                        return _REAL_ONE_ / (_REAL_ONE_ + (Gradient * Gradient));
                    }
                }
            }
        }
    }
}
