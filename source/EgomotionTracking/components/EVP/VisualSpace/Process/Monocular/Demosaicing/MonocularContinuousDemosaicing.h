/*
 * MonocularContinuousDemosaicing.h
 *
 *  Created on: 17.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../Foundation/Mathematics/1D/Common1D.h"
#include "../../../../Foundation/Mathematics/1D/ExponentialLookUpTable.h"
#include "../../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../../../Foundation/DataTypes/BayerPattern.h"
#include "../../../Kernels/ConvolutionGaussianKernel2D.h"
#include "../../Base/ImageProcessBase.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                namespace Demosaicing
                {
                    class CMonocularContinuousDemosaicing: public TImageProcessBase<real, CContinuousTristimulusPixel, CDiscreteTristimulusPixel>
                    {
                    public:

                        class CParameters: public CParametersBase
                        {
                        public:

                            enum DemosaicingMethod
                            {
                                eBilinear, eDirectionalGradient, eGaussian, eAdaptive
                            };

                            enum DisplayMode
                            {
                                eRoundedIntensity, eScaledIntensity
                            };

                            CParameters(CMonocularContinuousDemosaicing* pHost, const BayerPatternType BayerPattern) :
                                m_pHost(pHost), m_DemosaicingMethod(eDirectionalGradient), m_BayerPattern(BayerPattern), m_DisplayMode(eScaledIntensity), m_GaussianKernelSigma(_REAL_THIRD_), m_AdaptiveBaseSpatialKernelSigma(_REAL_TWO_THIRDS_), m_AdaptiveRangeKernelSigma(real(32.0)), m_LuminanceContribution(real(16.0)), m_ChromaticContribution(real(1.0 / 16.0))
                            {
                            }

                            CParametersBase::ParameterChangeResult SetDemosaicingMethod(const DemosaicingMethod Method, const bool Wait = true);
                            DemosaicingMethod GetDemosaicingMethod() const;

                            CParametersBase::ParameterChangeResult SetBayerPatternType(const BayerPatternType BayerPattern, const bool Wait = true);
                            BayerPatternType GetBayerPatternType() const;

                            CParametersBase::ParameterChangeResult SetDisplayMode(const DisplayMode Mode, const bool Wait = true);
                            DisplayMode GetDisplayMode() const;

                            CParametersBase::ParameterChangeResult SetGaussianKernelSigma(const real GaussianKernelSigma, const bool Wait = true);
                            real GetGaussianKernelSigma() const;

                            CParametersBase::ParameterChangeResult SetAdaptiveBaseSpatialKernelSigma(const real AdaptiveBaseSpatialKernelSigma, const bool Wait = true);
                            real GeAdaptiveBaseSpatialKernelSigma() const;

                            CParametersBase::ParameterChangeResult SetAdaptiveRangeKernelSigma(const real AdaptiveRangeKernelSigma, const bool Wait = true);
                            real GeAdaptiveRangeKernelSigma() const;

                            CParametersBase::ParameterChangeResult SetLuminanceContribution(const real LuminanceContribution, const bool Wait = true);
                            real GetLuminanceContribution() const;

                            CParametersBase::ParameterChangeResult SetChromaticContribution(const real ChromaticContribution, const bool Wait = true);
                            real GetChromaticContribution() const;

                        protected:

                            CMonocularContinuousDemosaicing* m_pHost;
                            DemosaicingMethod m_DemosaicingMethod;
                            BayerPatternType m_BayerPattern;
                            DisplayMode m_DisplayMode;
                            real m_GaussianKernelSigma;
                            real m_AdaptiveBaseSpatialKernelSigma;
                            real m_AdaptiveRangeKernelSigma;
                            real m_LuminanceContribution;
                            real m_ChromaticContribution;
                        };

                        CMonocularContinuousDemosaicing(const BayerPatternType BayerPattern, CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, Threading::CMutex* pInputImageMutex);

                        ~CMonocularContinuousDemosaicing() override;

                        bool Execute(const Identifier TrialId) override;
                        bool Display() override;
                        CParameters* GetParameters() override;

                    protected:

                        friend class CMonocularContinuousDemosaicing::CParameters;

                        bool UpdateSelectedMethod();
                        bool UpdateBayerPatternMap(const BayerPatternType BayerPattern);
                        bool UpdateGaussianKernelSigma(const real GaussianKernelSigma);
                        bool UpdateAdaptiveBaseSpatialKernelSigma(const real AdaptiveBaseKernelSigma);
                        static  bool UpdateOutputActiveZone(const void* pData, coordinate& OffsetX0, coordinate& OffsetY0, coordinate& OffsetX1, coordinate& OffsetY1);

                        void GaussianDemosaicing();
                        void BilinearDemosaicing();
                        void DirectionalWeightedGradientDemosaicing();
                        void AdaptiveDemosaicing();
                        inline  real GradientWeightingKernel(const real Gradient) const;

                        CParameters m_Parameters;
                        Kernels::CConvolutionGaussianKernel2D* m_pGaussianKernel;
                        Kernels::CConvolutionGaussianKernel2D* m_pAdaptiveBaseKernel;
                        TImage<CContinuousTristimulusPixel>* m_pAdaptiveImage;
                        ChromaticSpaceRGB::ChannelContent m_BayerPatternMapYX[2][2];

#ifdef _DEMOSAICING_USE_LOOKUP_TABLE_
                        DECLARE_EXPONENTIAL_LOOKUP_TABLE
#endif
                    };
                }
            }
        }
    }
}

