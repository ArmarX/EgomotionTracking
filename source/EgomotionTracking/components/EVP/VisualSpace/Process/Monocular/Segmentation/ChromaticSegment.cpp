/*
 * ChromaticSegment.cpp
 *
 *  Created on: 28.10.2011
 *      Author: gonzalez
 */

#include "ChromaticSegment.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Segmentation
        {
            CChromaticSegment::CChromaticSegment(CChromaticPixel* pChromaticPixel)
            {
                AddPixel(pChromaticPixel);
            }

            CChromaticSegment::~CChromaticSegment()
            {
                list<CChromaticPixel*>::const_iterator EndChromaticPixel = m_ChromaticPixels.end();
                for (list<CChromaticPixel*>::const_iterator ppChromaticPixel = m_ChromaticPixels.begin(); ppChromaticPixel != EndChromaticPixel; ++ppChromaticPixel)
                {
                    (*ppChromaticPixel)->Unlink();
                }
                m_ChromaticPixels.clear();
            }

            void CChromaticSegment::AddPixel(CChromaticPixel* pChromaticPixel)
            {
                m_ChromaticPixels.push_back(pChromaticPixel);
                pChromaticPixel->Link(this);
            }

            const list<CChromaticPixel*>& CChromaticSegment::GetChromaticPixels() const
            {
                return m_ChromaticPixels;
            }

            const list<CChromaticPixel*>& CChromaticSegment::GetMinimalExternalChromaticPixels() const
            {
                return m_MinimalExternalChromaticPixels;
            }

            const list<CChromaticPixel*>& CChromaticSegment::GetExternalChromaticPixels() const
            {
                return m_ExternalChromaticPixels;
            }

            const list<CChromaticPixel*>& CChromaticSegment::GetInternalChromaticPixels() const
            {
                return m_InternalChromaticPixels;
            }

            uint CChromaticSegment::GetSize() const
            {
                return m_ChromaticPixels.size();
            }

            real CChromaticSegment::DelineateBoundry()
            {
                list<CChromaticPixel*>::const_iterator EndChromaticPixel = m_ChromaticPixels.end();
                for (list<CChromaticPixel*>::const_iterator ppChromaticPixel = m_ChromaticPixels.begin(); ppChromaticPixel != EndChromaticPixel; ++ppChromaticPixel)
                    if ((*ppChromaticPixel)->UpdateLinkageStatus() == CChromaticPixel::eInternal)
                    {
                        m_InternalChromaticPixels.push_back(*ppChromaticPixel);
                    }
                    else
                    {
                        m_ExternalChromaticPixels.push_back(*ppChromaticPixel);
                    }
                return real(m_InternalChromaticPixels.size()) / real(m_ExternalChromaticPixels.size());
            }

            void CChromaticSegment::ReduceBoundry()
            {
                list<CChromaticPixel*>::const_iterator EndExternalChromaticPixels = m_ExternalChromaticPixels.end();
                for (list<CChromaticPixel*>::const_iterator ppExternalChromaticPixel = m_ExternalChromaticPixels.begin(); ppExternalChromaticPixel != EndExternalChromaticPixels; ++ppExternalChromaticPixel)
                    if ((*ppExternalChromaticPixel)->UpdateExternalLinkageStatus() == CChromaticPixel::eExternalMinimal)
                    {
                        m_MinimalExternalChromaticPixels.push_back(*ppExternalChromaticPixel);
                    }
            }

            void CChromaticSegment::ExtractContoures()
            {
                list<CChromaticPixel*>::const_iterator EndMinimalExternalChromaticPixels = m_MinimalExternalChromaticPixels.end();
                for (list<CChromaticPixel*>::const_iterator ppMinimalExternalChromaticPixel = m_MinimalExternalChromaticPixels.begin(); ppMinimalExternalChromaticPixel != EndMinimalExternalChromaticPixels; ++ppMinimalExternalChromaticPixel)
                {
                    CChromaticPixel* pChromaticPixel = *ppMinimalExternalChromaticPixel;
                    if (pChromaticPixel->m_Status == CChromaticPixel::eExternalMinimal)
                    {
                        pChromaticPixel->m_Status = CChromaticPixel::eContour;
                    }
                }
            }

            CContinuousTristimulusPixel CChromaticSegment::GetMeanContinuousTristimulus()
            {
                CContinuousTristimulusPixel AccumulatorContinuousTristimulus;
                list<CChromaticPixel*>::const_iterator EndChromaticPixel = m_InternalChromaticPixels.end();
                for (list<CChromaticPixel*>::const_iterator ppChromaticPixel = m_InternalChromaticPixels.begin(); ppChromaticPixel != EndChromaticPixel; ++ppChromaticPixel)
                {
                    AccumulatorContinuousTristimulus += (*ppChromaticPixel)->GetExternalTristimulus();
                }
                return AccumulatorContinuousTristimulus / real(m_InternalChromaticPixels.size());
            }
        }
    }
}
