/*
 * MonocularContinuousChromaticSegmentation.cpp
 *
 *  Created on: 23.10.2011
 *      Author: gonzalez
 */

#include "MonocularContinuousChromaticSegmentation.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularContinuousChromaticSegmentation::CParameters
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                CMonocularContinuousChromaticSegmentation::CParameters::~CParameters()
                {
                    RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousChromaticSegmentation::CParameters::SetChromaticSpace(const ChromaticSpace SelectedSpace, const bool Wait)
                {
                    if (!(
#ifdef _USE_COLOR_SPACE_RGB_
                            (SelectedSpace == eRGB)
#endif
#ifdef _USE_COLOR_SPACE_XYZ_
                            || (SelectedSpace == eXYZ)
#endif
#ifdef _USE_COLOR_SPACE_LAB_
                            || (SelectedSpace == eLab)
#endif
#ifdef _USE_COLOR_SPACE_HSL_
                            || (SelectedSpace == eHSL)
#endif
#ifdef _USE_COLOR_SPACE_HSI_
                            || (SelectedSpace == eHSI)
#endif
#ifdef _USE_COLOR_SPACE_HSV_
                            || (SelectedSpace == eHSV)
#endif
                        ))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (SelectedSpace == m_ChromaticSpace)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_ChromaticSpace = SelectedSpace;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                ChromaticSpace CMonocularContinuousChromaticSegmentation::CParameters::GetChromaticSpace() const
                {
                    return m_ChromaticSpace;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousChromaticSegmentation::CParameters::SetDistanceMetric(const DistanceMetric SelectedMetric, const bool Wait)
                {
                    if (!((SelectedMetric == CParameters::eManhatan) || (SelectedMetric == CParameters::eSquareEuclidean) || (SelectedMetric == CParameters::eEuclidean) || (SelectedMetric == CParameters::eMinkowski) || (SelectedMetric == CParameters::eChebyshev)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (SelectedMetric == m_DistanceMetric)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_DistanceMetric = SelectedMetric;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                CMonocularContinuousChromaticSegmentation::CParameters::DistanceMetric CMonocularContinuousChromaticSegmentation::CParameters::GetDistanceMetric() const
                {
                    return m_DistanceMetric;
                }

#ifdef _USE_COLOR_SPACE_LAB_

                CParametersBase::ParameterChangeResult CMonocularContinuousChromaticSegmentation::CParameters::SetDistanceMetric(const ChromaticSpaceLAB::WhiteSpot SelectedWhiteSpot, const bool Wait)
                {
                    if (!((SelectedWhiteSpot == ChromaticSpaceLAB::eD50) || (SelectedWhiteSpot == ChromaticSpaceLAB::eD55) || (SelectedWhiteSpot == ChromaticSpaceLAB::eD65) || (SelectedWhiteSpot == ChromaticSpaceLAB::eD75)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (SelectedWhiteSpot == m_WhiteSpot)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_WhiteSpot = SelectedWhiteSpot;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                ChromaticSpaceLAB::WhiteSpot CMonocularContinuousChromaticSegmentation::CParameters::GetWhiteSpot() const
                {
                    return m_WhiteSpot;
                }
#endif

                CParametersBase::ParameterChangeResult CMonocularContinuousChromaticSegmentation::CParameters::SetDisplayMode(const DisplayMode Mode, const bool Wait)
                {
                    if (!((Mode == CParameters::eColorMappedSize) || (Mode == CParameters::eColorIndexed) || (Mode == CParameters::eMeanContinuousTristimulus) || (Mode == CParameters::eInverseMeanContinuousTristimulus)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (Mode == m_DisplayMode)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_DisplayMode = Mode;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                CMonocularContinuousChromaticSegmentation::CParameters::DisplayMode CMonocularContinuousChromaticSegmentation::CParameters::GetDisplayMode() const
                {
                    return m_DisplayMode;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousChromaticSegmentation::CParameters::SetColorMap(const Visualization::CTristimulusColorMap* pColorMap, const bool OwnerShip, const bool Wait)
                {
                    if (!pColorMap)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (pColorMap == m_pColorMap)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                        m_ColorMapOwnerShip = OwnerShip;
                        m_pColorMap = pColorMap;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousChromaticSegmentation::CParameters::SetColorMap(const Visualization::CTristimulusColorMap::PredefinedColorMap ColorMap, const bool Wait)
                {
                    if (!Visualization::CTristimulusColorMap::IsValidPredefinedColorMap(ColorMap))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                        m_ColorMapOwnerShip = true;
                        m_pColorMap = Visualization::CTristimulusColorMap::LoadPredefinedColorMap(ColorMap);
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                const Visualization::CTristimulusColorMap* CMonocularContinuousChromaticSegmentation::CParameters::GetColorMap() const
                {
                    return m_pColorMap;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousChromaticSegmentation::CParameters::SetDistanceMode(const DistanceMode Mode, const bool Wait)
                {
                    if (!((Mode == CParameters::eChromatic) || (Mode == CParameters::eSaliency)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (Mode == m_DistanceMode)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_DistanceMode = Mode;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                CMonocularContinuousChromaticSegmentation::CParameters::DistanceMode CMonocularContinuousChromaticSegmentation::CParameters::GetDistanceMode() const
                {
                    return m_DistanceMode;
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularContinuousChromaticSegmentation
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                CMonocularContinuousChromaticSegmentation::CMonocularContinuousChromaticSegmentation(CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, const TImage<real>* pSaliencyImage, Threading::CMutex* pInputImageMutex) :
                    TImageProcessBase<CContinuousTristimulusPixel, Segmentation::CChromaticPixel, CDiscreteTristimulusPixel> (CImageProcessBase::eContinuousChromaticSegmentation, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this), m_pExternalSaliencyImage(pSaliencyImage), m_pInternalSaliencyImage(nullptr)
                {
                    if (m_IsEnabled)
                    {
                        m_OutputActiveZone.SetConnection(m_pInputActiveZone);
                        const CImageSize& Size = m_OutputActiveZone.GetSize();
                        m_pOutputImage = new TImage<Segmentation::CChromaticPixel> (Size);
                        m_pDisplayImage = new TImage<CDiscreteTristimulusPixel> (Size);
                        m_pInternalInputImage = new TImage<CContinuousTristimulusPixel> (Size);
                        if (m_pExternalSaliencyImage)
                        {
                            m_pInternalSaliencyImage = new TImage<real> (m_pExternalSaliencyImage->GetSize());
                        }
                        InitializeChromaticImage(m_pInternalSaliencyImage);
                        InitializeChromaticLinks(5);
                    }
                }

                CMonocularContinuousChromaticSegmentation::CMonocularContinuousChromaticSegmentation(CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, Threading::CMutex* pInputImageMutex) :
                    TImageProcessBase<CContinuousTristimulusPixel, Segmentation::CChromaticPixel, CDiscreteTristimulusPixel> (CImageProcessBase::eContinuousChromaticSegmentation, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this), m_pExternalSaliencyImage(nullptr), m_pInternalSaliencyImage(nullptr)
                {
                    if (m_IsEnabled)
                    {
                        m_OutputActiveZone.SetConnection(m_pInputActiveZone);
                        const CImageSize& Size = m_OutputActiveZone.GetSize();
                        m_pOutputImage = new TImage<Segmentation::CChromaticPixel> (Size);
                        m_pDisplayImage = new TImage<CDiscreteTristimulusPixel> (Size);
                        m_pInternalInputImage = new TImage<CContinuousTristimulusPixel> (Size);
                        InitializeChromaticImage(nullptr);
                        InitializeChromaticLinks(5);
                    }
                }

                CMonocularContinuousChromaticSegmentation::~CMonocularContinuousChromaticSegmentation()
                {
                    FinalizeChromaticLinks();
                    ClearSegments();
                    m_OutputActiveZone.Disconnect(true, true);
                    RELEASE_OBJECT(m_pOutputImage)
                    RELEASE_OBJECT(m_pDisplayImage)
                    RELEASE_OBJECT(m_pInternalInputImage)
                    RELEASE_OBJECT(m_pInternalSaliencyImage)
                }

                bool CMonocularContinuousChromaticSegmentation::Execute(const Identifier TrialId)
                {
                    if (m_IsEnabled && m_OutputActiveZone.IsEnabled())
                    {
                        if (StartExecuting(TrialId))
                        {
                            START_PROCESS_EXECUTION_LOG("Execute", TrialId)

                            LoadInputImage();
                            LoadSaliencyImage();
                            UpdateDistances(true, m_pExternalSaliencyImage ? m_Parameters.GetDistanceMode() : CParameters::eChromatic);
                            Segment(DetermineDistanceByPercentil(real(0.7)));

                            STOP_PROCESS_EXECUTION_LOG

                            return FinishExecution();
                        }
                    }
                    return false;
                }

                bool CMonocularContinuousChromaticSegmentation::Display()
                {
                    if (m_IsEnabled && m_OutputActiveZone.IsEnabled() && GetTotalExecutions())
                    {
                        if (StartDisplaying())
                        {
                            m_pDisplayImage->Clear();
                            switch (m_Parameters.GetDisplayMode())
                            {
                                case CParameters::eColorMappedSize:
                                    DisplaySegmentsBySize();
                                    break;
                                case CParameters::eColorIndexed:
                                    DisplaySegmentsByIndex();
                                    break;
                                case CParameters::eMeanContinuousTristimulus:
                                    DisplaySegmentsByMeanContinuousTristimulus();
                                    break;
                                case CParameters::eInverseMeanContinuousTristimulus:
                                    DisplaySegmentsByInverseMeanContinuousTristimulus();
                                    break;
                            }
                            return FinishDisplaying();
                        }
                    }
                    return false;
                }

                CMonocularContinuousChromaticSegmentation::CParameters* CMonocularContinuousChromaticSegmentation::GetParameters()
                {
                    return m_IsEnabled ? &m_Parameters : nullptr;
                }

                void CMonocularContinuousChromaticSegmentation::InitializeChromaticImage(const TImage<real>* pSaliencyImage)
                {
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate Width = m_OutputActiveZone.GetWidth();
                    const CContinuousTristimulusPixel* pBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferAt(X0, Y0);
                    Segmentation::CChromaticPixel* pBaseChromaticPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                    if (pSaliencyImage)
                    {
                        const real* pBaseSaliencyPixel = pSaliencyImage->GetReadOnlyBufferAt(X0, Y0);
                        for (CPixelLocation Location(X0, Y0); Location.m_y < Y1; ++Location.m_y, pBaseInputPixel += Width, pBaseChromaticPixel += Width, pBaseSaliencyPixel += Width)
                        {
                            const CContinuousTristimulusPixel* pInputPixel = pBaseInputPixel;
                            const real* pSaliencyPixel = pBaseSaliencyPixel;
                            Segmentation::CChromaticPixel* pChromaticPixel = pBaseChromaticPixel;
                            for (Location.m_x = X0; Location.m_x < X1; ++Location.m_x, ++pChromaticPixel)
                            {
                                pChromaticPixel->Initialize(Location, pInputPixel++, pSaliencyPixel++);
                            }
                        }
                    }
                    else
                        for (CPixelLocation Location(X0, Y0); Location.m_y < Y1; ++Location.m_y, pBaseInputPixel += Width, pBaseChromaticPixel += Width)
                        {
                            const CContinuousTristimulusPixel* pInputPixel = pBaseInputPixel;
                            Segmentation::CChromaticPixel* pChromaticPixel = pBaseChromaticPixel;
                            for (Location.m_x = X0; Location.m_x < X1; ++Location.m_x, ++pChromaticPixel)
                            {
                                pChromaticPixel->Initialize(Location, pInputPixel++, nullptr);
                            }
                        }
                }

                void CMonocularContinuousChromaticSegmentation::InitializeChromaticLinks(const uint SubSampligFactor)
                {
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate Width = m_OutputActiveZone.GetWidth();
                    const coordinate Deltas[] = { 1, -Width + 1, -Width, -Width - 1 };
                    Segmentation::CChromaticPixel* pBaseChromaticPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; ++y, pBaseChromaticPixel += Width)
                    {
                        Segmentation::CChromaticPixel* pChromaticPixel = pBaseChromaticPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pChromaticPixel)
                        {
                            const bool IsSelected = (!(y % SubSampligFactor)) && (!(x % SubSampligFactor));
                            for (uint i = 0, j = 4; i < 4; ++i, ++j)
                            {
                                Segmentation::CChromaticPixel* pAuxiliarChromaticPixel = pChromaticPixel + Deltas[i];
                                if (pAuxiliarChromaticPixel->IsLinkable())
                                {
                                    Segmentation::CChromaticLink* pChromaticLink = new Segmentation::CChromaticLink(pChromaticPixel, pAuxiliarChromaticPixel);
                                    pChromaticPixel->SetLink(pChromaticLink, i);
                                    pAuxiliarChromaticPixel->SetLink(pChromaticLink, j);
                                    m_ChromaticLinks.push_back(pChromaticLink);
                                    if (IsSelected)
                                    {
                                        m_SelectedChromaticLinks.push_back(pChromaticLink);
                                    }
                                }
                            }
                        }
                    }
                }

                void CMonocularContinuousChromaticSegmentation::FinalizeChromaticLinks()
                {
                    vector<Segmentation::CChromaticLink*>::const_iterator EndChromaticLinks = m_ChromaticLinks.end();
                    for (vector<Segmentation::CChromaticLink*>::const_iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                        RELEASE_OBJECT_DIRECT(*ppChromaticLink)
                        m_ChromaticLinks.clear();
                    m_SelectedChromaticLinks.clear();
                }

                void CMonocularContinuousChromaticSegmentation::ClearSegments()
                {
                    list<Segmentation::CChromaticSegment*>::iterator EndSelectedChromaticSegments = m_SelectedChromaticSegments.end();
                    for (list<Segmentation::CChromaticSegment*>::iterator ppChromaticSegments = m_SelectedChromaticSegments.begin(); ppChromaticSegments != EndSelectedChromaticSegments; ++ppChromaticSegments)
                        RELEASE_OBJECT_DIRECT(*ppChromaticSegments)
                        m_SelectedChromaticSegments.clear();
                    list<Segmentation::CChromaticSegment*>::iterator EndSpuriousChromaticSegments = m_SpuriousChromaticSegments.end();
                    for (list<Segmentation::CChromaticSegment*>::iterator ppChromaticSegments = m_SpuriousChromaticSegments.begin(); ppChromaticSegments != EndSpuriousChromaticSegments; ++ppChromaticSegments)
                        RELEASE_OBJECT_DIRECT(*ppChromaticSegments)
                        m_SpuriousChromaticSegments.clear();
                }

                void CMonocularContinuousChromaticSegmentation::LoadSaliencyImage()
                {
                    if (m_pExternalSaliencyImage && (m_Parameters.GetDistanceMode() == CParameters::eSaliency))
                    {
                        if (m_pExternalInputImageMutex)
                        {
                            m_pExternalInputImageMutex->BlockingLock();
                            m_pInternalSaliencyImage->Copy(m_pExternalSaliencyImage);
                            m_pExternalInputImageMutex->Unlock();
                        }
                        else
                        {
                            m_pInternalSaliencyImage->Copy(m_pExternalSaliencyImage);
                        }
                    }
                }

                void CMonocularContinuousChromaticSegmentation::UpdateDistances(const bool SortSelected, CParameters::DistanceMode Mode)
                {
                    switch (m_Parameters.GetChromaticSpace())
                    {
#ifdef _USE_COLOR_SPACE_RGB_
                        case eRGB:
                            UpdateRGBDistances(m_Parameters.GetDistanceMetric(), Mode);
                            break;
#endif
#ifdef _USE_COLOR_SPACE_XYZ_
                        case eXYZ:
                            UpdateXYZDistances(m_Parameters.GetDistanceMetric(), Mode);
                            break;
#endif
#ifdef _USE_COLOR_SPACE_LAB_
                        case eLab:
                            UpdateLabDistances(m_Parameters.GetDistanceMetric(), m_Parameters.GetWhiteSpot(), Mode);
                            break;
#endif
#ifdef _USE_COLOR_SPACE_HSL_
                        case eHSL:
                            UpdateHSLDistances(m_Parameters.GetDistanceMetric(), Mode);
                            break;
#endif
#ifdef _USE_COLOR_SPACE_HSI_
                        case eHSI:
                            UpdateHSIDistances(m_Parameters.GetDistanceMetric(), Mode);
                            break;
#endif
#ifdef _USE_COLOR_SPACE_HSV_
                        case eHSV:
                            UpdateHSVDistances(m_Parameters.GetDistanceMetric(), Mode);
                            break;
#endif

#ifdef _USE_COLOR_SPACE_RGBA_
                        case eRGBA:
                            return;
#endif
                        case eUnknownColorSpace:
                            return;
                    }
                    if (SortSelected)
                    {
                        TSort(m_SelectedChromaticLinks.begin(), m_SelectedChromaticLinks.end(), CMonocularContinuousChromaticSegmentation::SortByDistance);
                    }
                }

                real CMonocularContinuousChromaticSegmentation::DetermineDistanceByPercentil(const real Percentil)
                {
                    return m_SelectedChromaticLinks[RoundPositiveRealToInteger(real(m_SelectedChromaticLinks.size()) * Percentil)]->GetDistance();
                }

                void CMonocularContinuousChromaticSegmentation::Segment(const real MaximalDistance)
                {
                    ClearSegments();
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1() - 1;
                    const coordinate Y1 = m_OutputActiveZone.GetY1() - 1;
                    const coordinate Width = m_OutputActiveZone.GetWidth();
                    Segmentation::CChromaticPixel* pBaseChromaticPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; ++y, pBaseChromaticPixel += Width)
                    {
                        Segmentation::CChromaticPixel* pChromaticPixel = pBaseChromaticPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pChromaticPixel)
                            if (pChromaticPixel->IsLinkable())
                            {
                                Segmentation::CChromaticSegment* pChromaticSegment = new Segmentation::CChromaticSegment(pChromaticPixel);
                                list<Segmentation::CChromaticPixel*> ExpansionList;
                                ExpansionList.push_back(pChromaticPixel);
                                while (ExpansionList.size())
                                {
                                    ExpansionList.front()->Expand(MaximalDistance, ExpansionList);
                                    ExpansionList.pop_front();
                                }
                                if (pChromaticSegment->DelineateBoundry() >= _REAL_ONE_)
                                {
                                    pChromaticSegment->ReduceBoundry();
                                    m_SelectedChromaticSegments.push_back(pChromaticSegment);
                                }
                                else
                                {
                                    m_SpuriousChromaticSegments.push_back(pChromaticSegment);
                                }
                            }
                    }
                }

                void CMonocularContinuousChromaticSegmentation::DisplaySegmentsBySize()
                {
                    uint MinimalSize = m_SelectedChromaticSegments.front()->GetSize();
                    uint MaximalSize = MinimalSize;
                    list<Segmentation::CChromaticSegment*>::const_iterator EndChromaticSegments = m_SelectedChromaticSegments.end();
                    for (list<Segmentation::CChromaticSegment*>::const_iterator ppChromaticSegments = m_SelectedChromaticSegments.begin(); ppChromaticSegments != EndChromaticSegments; ++ppChromaticSegments)
                        if ((*ppChromaticSegments)->GetSize() > MaximalSize)
                        {
                            MaximalSize = (*ppChromaticSegments)->GetSize();
                        }
                        else if ((*ppChromaticSegments)->GetSize() < MinimalSize)
                        {
                            MinimalSize = (*ppChromaticSegments)->GetSize();
                        }
                    const Visualization::CTristimulusColorMap* pColorMap = m_Parameters.GetColorMap();
                    const CDiscreteTristimulusPixel* pColorLUT = pColorMap->GetColorLookupTable();
                    const real LUTScale = pColorMap->GetColorLookupTableScaleFactor() / real(MaximalSize - MinimalSize);
                    for (list<Segmentation::CChromaticSegment*>::const_iterator ppChromaticSegments = m_SelectedChromaticSegments.begin(); ppChromaticSegments != EndChromaticSegments; ++ppChromaticSegments)
                        if ((*ppChromaticSegments)->GetSize() > MinimalSize)
                        {
                            const list<Segmentation::CChromaticPixel*>& ChromaticPixels = (*ppChromaticSegments)->GetChromaticPixels();
                            const CDiscreteTristimulusPixel Color = pColorLUT[RoundPositiveRealToInteger(LUTScale * real(ChromaticPixels.size() - MinimalSize))];
                            list<Segmentation::CChromaticPixel*>::const_iterator EndChromaticPixels = ChromaticPixels.end();
                            for (list<Segmentation::CChromaticPixel*>::const_iterator ppChromaticPixel = ChromaticPixels.begin(); ppChromaticPixel != EndChromaticPixels; ++ppChromaticPixel)
                            {
                                m_pDisplayImage->SetValueAt((*ppChromaticPixel)->GetLocation(), Color);
                            }
                        }
                }

                void CMonocularContinuousChromaticSegmentation::DisplaySegmentsByIndex()
                {
                    list<Segmentation::CChromaticSegment*>::const_iterator EndChromaticSegments = m_SelectedChromaticSegments.end();
                    for (list<Segmentation::CChromaticSegment*>::const_iterator ppChromaticSegments = m_SelectedChromaticSegments.begin(); ppChromaticSegments != EndChromaticSegments; ++ppChromaticSegments)
                    {
                        const CDiscreteTristimulusPixel ExternalColor = CDiscreteTristimulusPixel::GenerateRandom(128, (*ppChromaticSegments)->GetSize());
                        const CDiscreteTristimulusPixel InternalColor = ExternalColor * _REAL_TWO_THIRDS_;
                        const list<Segmentation::CChromaticPixel*>& MinimalExternalChromaticPixels = (*ppChromaticSegments)->GetMinimalExternalChromaticPixels();
                        list<Segmentation::CChromaticPixel*>::const_iterator EndMinimalExternalChromaticPixels = MinimalExternalChromaticPixels.end();
                        for (list<Segmentation::CChromaticPixel*>::const_iterator ppChromaticPixel = MinimalExternalChromaticPixels.begin(); ppChromaticPixel != EndMinimalExternalChromaticPixels; ++ppChromaticPixel)
                        {
                            m_pDisplayImage->SetValueAt((*ppChromaticPixel)->GetLocation(), ExternalColor);
                        }
                        const list<Segmentation::CChromaticPixel*>& InternalChromaticPixels = (*ppChromaticSegments)->GetInternalChromaticPixels();
                        list<Segmentation::CChromaticPixel*>::const_iterator EndInternalChromaticPixels = InternalChromaticPixels.end();
                        for (list<Segmentation::CChromaticPixel*>::const_iterator ppChromaticPixel = InternalChromaticPixels.begin(); ppChromaticPixel != EndInternalChromaticPixels; ++ppChromaticPixel)
                        {
                            m_pDisplayImage->SetValueAt((*ppChromaticPixel)->GetLocation(), InternalColor);
                        }
                    }
                }

                void CMonocularContinuousChromaticSegmentation::DisplaySegmentsByMeanContinuousTristimulus()
                {
                    list<Segmentation::CChromaticSegment*>::const_iterator EndChromaticSegments = m_SelectedChromaticSegments.end();
                    for (list<Segmentation::CChromaticSegment*>::const_iterator ppChromaticSegments = m_SelectedChromaticSegments.begin(); ppChromaticSegments != EndChromaticSegments; ++ppChromaticSegments)
                    {
                        const list<Segmentation::CChromaticPixel*>& ChromaticPixels = (*ppChromaticSegments)->GetChromaticPixels();
                        const CDiscreteTristimulusPixel Color = (*ppChromaticSegments)->GetMeanContinuousTristimulus();
                        list<Segmentation::CChromaticPixel*>::const_iterator EndChromaticPixels = ChromaticPixels.end();
                        for (list<Segmentation::CChromaticPixel*>::const_iterator ppChromaticPixel = ChromaticPixels.begin(); ppChromaticPixel != EndChromaticPixels; ++ppChromaticPixel)
                        {
                            m_pDisplayImage->SetValueAt((*ppChromaticPixel)->GetLocation(), Color);
                        }
                    }
                }

                void CMonocularContinuousChromaticSegmentation::DisplaySegmentsByInverseMeanContinuousTristimulus()
                {
                    list<Segmentation::CChromaticSegment*>::const_iterator EndChromaticSegments = m_SelectedChromaticSegments.end();
                    for (list<Segmentation::CChromaticSegment*>::const_iterator ppChromaticSegments = m_SelectedChromaticSegments.begin(); ppChromaticSegments != EndChromaticSegments; ++ppChromaticSegments)
                    {
                        const list<Segmentation::CChromaticPixel*>& ChromaticPixels = (*ppChromaticSegments)->GetChromaticPixels();
                        const CDiscreteTristimulusPixel Color = (*ppChromaticSegments)->GetMeanContinuousTristimulus();
                        const CDiscreteTristimulusPixel InverseColor(_REAL_255_ - Color.GetReadOnlyChannelValueByIndex(0), _REAL_255_ - Color.GetReadOnlyChannelValueByIndex(1), _REAL_255_ - Color.GetReadOnlyChannelValueByIndex(2));
                        list<Segmentation::CChromaticPixel*>::const_iterator EndChromaticPixels = ChromaticPixels.end();
                        for (list<Segmentation::CChromaticPixel*>::const_iterator ppChromaticPixel = ChromaticPixels.begin(); ppChromaticPixel != EndChromaticPixels; ++ppChromaticPixel)
                        {
                            m_pDisplayImage->SetValueAt((*ppChromaticPixel)->GetLocation(), InverseColor);
                        }
                    }
                }

#ifdef _USE_COLOR_SPACE_RGB_

                void CMonocularContinuousChromaticSegmentation::UpdateRGBDistances(CParameters::DistanceMetric Metric, CParameters::DistanceMode Mode)
                {
                    vector<Segmentation::CChromaticLink*>::iterator EndChromaticLinks = m_ChromaticLinks.end();
                    switch (Mode)
                    {
                        case CParameters::eChromatic:
                            switch (Metric)
                            {
                                case CParameters::eSquareEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateRGBSquareEuclideanDistance();
                                    }
                                    break;
                                case CParameters::eEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateRGBEuclideanDistance();
                                    }
                                    break;
                                case CParameters::eManhatan:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateRGBManhattanDistance();
                                    }
                                    break;
                                case CParameters::eMinkowski:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateRGBMinkowskiDistance(_REAL_TWO_, _REAL_HALF_);
                                    }
                                    break;
                                case CParameters::eChebyshev:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateRGBChebyshevDistance();
                                    }
                                    break;
                            }
                            break;
                        case CParameters::eSaliency:
                            switch (Metric)
                            {
                                case CParameters::eSquareEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateRGBSquareEuclideanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateRGBEuclideanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eManhatan:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateRGBManhattanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eMinkowski:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateRGBMinkowskiDistanceSaliencyMode(_REAL_TWO_, _REAL_HALF_);
                                    }
                                    break;
                                case CParameters::eChebyshev:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateRGBChebyshevDistanceSaliencyMode();
                                    }
                                    break;
                            }
                            break;
                    }
                }

#endif

#ifdef _USE_COLOR_SPACE_HSL_

                void CMonocularContinuousChromaticSegmentation::ConvertRGBToXYZ()
                {
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate Width = m_OutputActiveZone.GetWidth();
                    Segmentation::CChromaticPixel* pBaseChromaticPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; ++y, pBaseChromaticPixel += Width)
                    {
                        Segmentation::CChromaticPixel* pChromaticPixel = pBaseChromaticPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pChromaticPixel)
                        {
                            pChromaticPixel->ConvertRGBToXYZ();
                        }
                    }
                }

                void CMonocularContinuousChromaticSegmentation::UpdateXYZDistances(CParameters::DistanceMetric Metric, CParameters::DistanceMode Mode)
                {
                    ConvertRGBToXYZ();
                    vector<Segmentation::CChromaticLink*>::iterator EndChromaticLinks = m_ChromaticLinks.end();
                    switch (Mode)
                    {
                        case CParameters::eChromatic:
                            switch (Metric)
                            {
                                case CParameters::eSquareEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateXYZSquareEuclideanDistance();
                                    }
                                    break;
                                case CParameters::eEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateXYZEuclideanDistance();
                                    }
                                    break;
                                case CParameters::eManhatan:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateXYZManhattanDistance();
                                    }
                                    break;
                                case CParameters::eMinkowski:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateXYZMinkowskiDistance(_REAL_TWO_, _REAL_HALF_);
                                    }
                                    break;
                                case CParameters::eChebyshev:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateXYZChebyshevDistance();
                                    }
                                    break;
                            }
                            break;
                        case CParameters::eSaliency:
                            switch (Metric)
                            {
                                case CParameters::eSquareEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateXYZSquareEuclideanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateXYZEuclideanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eManhatan:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateXYZManhattanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eMinkowski:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateXYZMinkowskiDistanceSaliencyMode(_REAL_TWO_, _REAL_HALF_);
                                    }
                                    break;
                                case CParameters::eChebyshev:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateXYZChebyshevDistanceSaliencyMode();
                                    }
                                    break;
                            }
                            break;
                    }
                }
#endif

#ifdef _USE_COLOR_SPACE_LAB_

                void CMonocularContinuousChromaticSegmentation::ConvertRGBToLab(ChromaticSpaceLAB::WhiteSpot SelectedWhiteSpot)
                {
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate Width = m_OutputActiveZone.GetWidth();
                    Segmentation::CChromaticPixel* pBaseChromaticPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                    switch (SelectedWhiteSpot)
                    {
                        case ChromaticSpaceLAB::eD50:
                            for (coordinate y = Y0; y < Y1; ++y, pBaseChromaticPixel += Width)
                            {
                                Segmentation::CChromaticPixel* pChromaticPixel = pBaseChromaticPixel;
                                for (coordinate x = X0; x < X1; ++x, ++pChromaticPixel)
                                {
                                    pChromaticPixel->ConvertRGBToLabD50();
                                }
                            }
                            break;
                        case ChromaticSpaceLAB::eD55:
                            for (coordinate y = Y0; y < Y1; ++y, pBaseChromaticPixel += Width)
                            {
                                Segmentation::CChromaticPixel* pChromaticPixel = pBaseChromaticPixel;
                                for (coordinate x = X0; x < X1; ++x, ++pChromaticPixel)
                                {
                                    pChromaticPixel->ConvertRGBToLabD55();
                                }
                            }
                            break;
                        case ChromaticSpaceLAB::eD65:
                            for (coordinate y = Y0; y < Y1; ++y, pBaseChromaticPixel += Width)
                            {
                                Segmentation::CChromaticPixel* pChromaticPixel = pBaseChromaticPixel;
                                for (coordinate x = X0; x < X1; ++x, ++pChromaticPixel)
                                {
                                    pChromaticPixel->ConvertRGBToLabD65();
                                }
                            }
                            break;
                        case ChromaticSpaceLAB::eD75:
                            for (coordinate y = Y0; y < Y1; ++y, pBaseChromaticPixel += Width)
                            {
                                Segmentation::CChromaticPixel* pChromaticPixel = pBaseChromaticPixel;
                                for (coordinate x = X0; x < X1; ++x, ++pChromaticPixel)
                                {
                                    pChromaticPixel->ConvertRGBToLabD75();
                                }
                            }
                            break;
                        case ChromaticSpaceLAB::eUnknownWhiteSpot:
                            break;
                    }
                }

                void CMonocularContinuousChromaticSegmentation::UpdateLabDistances(CParameters::DistanceMetric Metric, ChromaticSpaceLAB::WhiteSpot SelectedWhiteSpot, CParameters::DistanceMode Mode)
                {
                    ConvertRGBToLab(SelectedWhiteSpot);
                    vector<Segmentation::CChromaticLink*>::iterator EndChromaticLinks = m_ChromaticLinks.end();
                    switch (Mode)
                    {
                        case CParameters::eChromatic:
                            switch (Metric)
                            {
                                case CParameters::eSquareEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateLabSquareEuclideanDistance();
                                    }
                                    break;
                                case CParameters::eEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateLabEuclideanDistance();
                                    }
                                    break;
                                case CParameters::eManhatan:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateLabManhattanDistance();
                                    }
                                    break;
                                case CParameters::eMinkowski:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateLabMinkowskiDistance(_REAL_TWO_, _REAL_HALF_);
                                    }
                                    break;
                                case CParameters::eChebyshev:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateLabChebyshevDistance();
                                    }
                                    break;
                            }
                            break;
                        case CParameters::eSaliency:
                            switch (Metric)
                            {
                                case CParameters::eSquareEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateLabSquareEuclideanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateLabEuclideanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eManhatan:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateLabManhattanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eMinkowski:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateLabMinkowskiDistanceSaliencyMode(_REAL_TWO_, _REAL_HALF_);
                                    }
                                    break;
                                case CParameters::eChebyshev:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateLabChebyshevDistanceSaliencyMode();
                                    }
                                    break;
                            }
                            break;
                    }
                }
#endif

#ifdef _USE_COLOR_SPACE_XYZ_

                void CMonocularContinuousChromaticSegmentation::ConvertRGBToHSL()
                {
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate Width = m_OutputActiveZone.GetWidth();
                    Segmentation::CChromaticPixel* pBaseChromaticPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; ++y, pBaseChromaticPixel += Width)
                    {
                        Segmentation::CChromaticPixel* pChromaticPixel = pBaseChromaticPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pChromaticPixel)
                        {
                            pChromaticPixel->ConvertRGBToHSL();
                        }
                    }
                }

                void CMonocularContinuousChromaticSegmentation::UpdateHSLDistances(CParameters::DistanceMetric Metric, CParameters::DistanceMode Mode)
                {
                    ConvertRGBToHSL();
                    vector<Segmentation::CChromaticLink*>::iterator EndChromaticLinks = m_ChromaticLinks.end();
                    switch (Mode)
                    {
                        case CParameters::eChromatic:
                            switch (Metric)
                            {
                                case CParameters::eSquareEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSLSquareEuclideanDistance();
                                    }
                                    break;
                                case CParameters::eEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSLEuclideanDistance();
                                    }
                                    break;
                                case CParameters::eManhatan:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSLManhattanDistance();
                                    }
                                    break;
                                case CParameters::eMinkowski:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSLMinkowskiDistance(_REAL_TWO_, _REAL_HALF_);
                                    }
                                    break;
                                case CParameters::eChebyshev:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSLChebyshevDistance();
                                    }
                                    break;
                            }
                            break;
                        case CParameters::eSaliency:
                            switch (Metric)
                            {
                                case CParameters::eSquareEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSLSquareEuclideanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSLEuclideanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eManhatan:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSLManhattanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eMinkowski:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSLMinkowskiDistanceSaliencyMode(_REAL_TWO_, _REAL_HALF_);
                                    }
                                    break;
                                case CParameters::eChebyshev:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSLChebyshevDistanceSaliencyMode();
                                    }
                                    break;
                            }
                            break;
                    }
                }
#endif

#ifdef _USE_COLOR_SPACE_HSI_

                void CMonocularContinuousChromaticSegmentation::ConvertRGBToHSI()
                {
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate Width = m_OutputActiveZone.GetWidth();
                    Segmentation::CChromaticPixel* pBaseChromaticPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; ++y, pBaseChromaticPixel += Width)
                    {
                        Segmentation::CChromaticPixel* pChromaticPixel = pBaseChromaticPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pChromaticPixel)
                        {
                            pChromaticPixel->ConvertRGBToHSI();
                        }
                    }
                }

                void CMonocularContinuousChromaticSegmentation::UpdateHSIDistances(CParameters::DistanceMetric Metric, CParameters::DistanceMode Mode)
                {
                    ConvertRGBToHSI();
                    vector<Segmentation::CChromaticLink*>::iterator EndChromaticLinks = m_ChromaticLinks.end();
                    switch (Mode)
                    {
                        case CParameters::eChromatic:
                            switch (Metric)
                            {
                                case CParameters::eSquareEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSISquareEuclideanDistance();
                                    }
                                    break;
                                case CParameters::eEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSIEuclideanDistance();
                                    }
                                    break;
                                case CParameters::eManhatan:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSIManhattanDistance();
                                    }
                                    break;
                                case CParameters::eMinkowski:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSIMinkowskiDistance(_REAL_TWO_, _REAL_HALF_);
                                    }
                                    break;
                                case CParameters::eChebyshev:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSIChebyshevDistance();
                                    }
                                    break;
                            }
                            break;
                        case CParameters::eSaliency:
                            switch (Metric)
                            {
                                case CParameters::eSquareEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSISquareEuclideanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSIEuclideanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eManhatan:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSIManhattanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eMinkowski:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSIMinkowskiDistanceSaliencyMode(_REAL_TWO_, _REAL_HALF_);
                                    }
                                    break;
                                case CParameters::eChebyshev:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSIChebyshevDistanceSaliencyMode();
                                    }
                                    break;
                            }
                            break;
                    }
                }
#endif

#ifdef _USE_COLOR_SPACE_HSV_

                void CMonocularContinuousChromaticSegmentation::ConvertRGBToHSV()
                {
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate Width = m_OutputActiveZone.GetWidth();
                    Segmentation::CChromaticPixel* pBaseChromaticPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; ++y, pBaseChromaticPixel += Width)
                    {
                        Segmentation::CChromaticPixel* pChromaticPixel = pBaseChromaticPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pChromaticPixel)
                        {
                            pChromaticPixel->ConvertRGBToHSV();
                        }
                    }
                }

                void CMonocularContinuousChromaticSegmentation::UpdateHSVDistances(CParameters::DistanceMetric Metric, CParameters::DistanceMode Mode)
                {
                    ConvertRGBToHSV();
                    vector<Segmentation::CChromaticLink*>::iterator EndChromaticLinks = m_ChromaticLinks.end();
                    switch (Mode)
                    {
                        case CParameters::eChromatic:
                            switch (Metric)
                            {
                                case CParameters::eSquareEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSVSquareEuclideanDistance();
                                    }
                                    break;
                                case CParameters::eEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSVEuclideanDistance();
                                    }
                                    break;
                                case CParameters::eManhatan:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSVManhattanDistance();
                                    }
                                    break;
                                case CParameters::eMinkowski:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSVMinkowskiDistance(_REAL_TWO_, _REAL_HALF_);
                                    }
                                    break;
                                case CParameters::eChebyshev:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSVChebyshevDistance();
                                    }
                                    break;
                            }
                            break;
                        case CParameters::eSaliency:
                            switch (Metric)
                            {
                                case CParameters::eSquareEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSVSquareEuclideanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eEuclidean:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSVEuclideanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eManhatan:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSVManhattanDistanceSaliencyMode();
                                    }
                                    break;
                                case CParameters::eMinkowski:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSVMinkowskiDistanceSaliencyMode(_REAL_TWO_, _REAL_HALF_);
                                    }
                                    break;
                                case CParameters::eChebyshev:
                                    for (vector<Segmentation::CChromaticLink*>::iterator ppChromaticLink = m_ChromaticLinks.begin(); ppChromaticLink != EndChromaticLinks; ++ppChromaticLink)
                                    {
                                        (*ppChromaticLink)->UpdateHSVChebyshevDistanceSaliencyMode();
                                    }
                                    break;
                            }
                            break;
                    }
                }
#endif

                bool CMonocularContinuousChromaticSegmentation::SortByDistance(const Segmentation::CChromaticLink* plhs, const Segmentation::CChromaticLink* prhs)
                {
                    return plhs->GetDistance() < prhs->GetDistance();
                }
            }
        }
    }
}
