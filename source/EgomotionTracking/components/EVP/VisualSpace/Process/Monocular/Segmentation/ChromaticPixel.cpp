/*
 * ChromaticPixel.cpp
 *
 *  Created on: 28.10.2011
 *      Author: gonzalez
 */

#include "ChromaticPixel.h"
#include "ChromaticSegment.h"
#include "ChromaticLink.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Segmentation
        {
            void CChromaticPixel::Attach(CChromaticPixel* pChromaticPixel)
            {
                m_pChromaticSegment->AddPixel(pChromaticPixel);
            }

            CChromaticPixel::Status CChromaticPixel::UpdateLinkageStatus()
            {
                for (uint i = 0; i < 8; i += 2)
                    if (m_ChromaticLinks[i])
                    {
                        if (!m_ChromaticLinks[i]->IsInternal())
                        {
                            m_Status = eExternal;
                            return m_Status;
                        }
                    }
                    else
                    {
                        m_Status = eExternalActiveZoneLimit;
                        return m_Status;
                    }
                m_Status = eInternal;
                return m_Status;
            }

            CChromaticPixel::Status CChromaticPixel::UpdateExternalLinkageStatus()
            {
                if (m_Status == eExternalActiveZoneLimit)
                {
                    m_Status = eExternalMinimal;
                    return m_Status;
                }
                for (uint i = 0; i < 8; i += 2)
                    if (m_ChromaticLinks[i] && m_ChromaticLinks[i]->IsInternal(this))
                    {
                        m_Status = eExternalMinimal;
                        return m_Status;
                    }
                return m_Status;
            }

            void CChromaticPixel::Expand(const real MaximalDistance, list<Segmentation::CChromaticPixel*>& ExpansionList)
            {
                for (uint i = 0; i < 8; ++i)
                    if (m_ChromaticLinks[i])
                    {
                        m_ChromaticLinks[i]->Attach(MaximalDistance, ExpansionList);
                    }
            }
        }
    }
}
