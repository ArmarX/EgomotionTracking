/*
 * ChromaticPixel.h
 *
 *  Created on: 28.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/DataTypes/PixelLocation.h"
#include "../../../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../../../Foundation/DataTypes/ColorSpaces.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Segmentation
        {
            class CChromaticLink;
            class CChromaticSegment;

            class CChromaticPixel
            {
                friend class CChromaticSegment;
                friend class CChromaticLink;

            public:

                enum Status
                {
                    eUnlinkable, eLinkable, eLinked, eInternal, eExternal, eExternalActiveZoneLimit, eExternalMinimal, eContour
                };

                CChromaticPixel() :
                    m_Status(eUnlinkable), m_Location(), m_InternalTristimulus(), m_pExternalTristimulus(NULL), m_pSaliency(NULL), m_pChromaticSegment(NULL)
                {
                    memset(m_ChromaticLinks, 0, sizeof(CChromaticLink*) * 8);
                }

                virtual ~CChromaticPixel()
                {
                }

                void Initialize(const CPixelLocation& Location, const CContinuousTristimulusPixel* pExternalTristimulus, const real* pSaliency)
                {
                    m_Location = Location;
                    m_pExternalTristimulus = pExternalTristimulus;
                    m_pSaliency = pSaliency;
                    m_Status = eLinkable;
                }

                inline void SetLink(CChromaticLink* pChromaticLink, const uint LocationIndex)
                {
                    m_ChromaticLinks[LocationIndex] = pChromaticLink;
                }

                inline  bool IsLinkable() const
                {
                    return (m_Status == eLinkable);
                }

                inline  bool IsLinked() const
                {
                    return (m_Status == eLinked);
                }

                inline const CPixelLocation& GetLocation() const
                {
                    return m_Location;
                }

                inline void Link(CChromaticSegment* pChromaticSegment)
                {
                    m_pChromaticSegment = pChromaticSegment;
                    m_Status = eLinked;
                }

                inline void Unlink()
                {
                    m_pChromaticSegment = NULL;
                    m_Status = eLinkable;
                }

                inline CChromaticSegment* GetChromaticSegment()
                {
                    return m_pChromaticSegment;
                }

                inline const CContinuousTristimulusPixel& GetExternalTristimulus()
                {
                    return *m_pExternalTristimulus;
                }

                void Attach(CChromaticPixel* pChromaticPixel);

                Status UpdateLinkageStatus();

                Status UpdateExternalLinkageStatus();

                void Expand(const real MaximalDistance, list<Segmentation::CChromaticPixel*>& ExpansionList);

#ifdef _USE_COLOR_SPACE_RGB_

                inline  real GetRGBManhattanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_pExternalTristimulus->GetRGBChebyshevDistance(*pChromaticPixel->m_pExternalTristimulus);
                }

                inline  real GetRGBSquareEuclideanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_pExternalTristimulus->GetRGBSquareEuclideanDistance(*pChromaticPixel->m_pExternalTristimulus);
                }

                inline  real GetRGBEuclideanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_pExternalTristimulus->GetRGBEuclideanDistance(*pChromaticPixel->m_pExternalTristimulus);
                }

                inline  real GetRGBChebyshevDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_pExternalTristimulus->GetRGBChebyshevDistance(*pChromaticPixel->m_pExternalTristimulus);
                }

                inline  real GetRGBMinkowskiDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const
                {
                    return m_pExternalTristimulus->GetRGBMinkowskiDistance(*pChromaticPixel->m_pExternalTristimulus, P, IP);
                }
#endif

#ifdef _USE_COLOR_SPACE_HSL_

                inline void ConvertRGBToHSL()
                {
                    m_InternalTristimulus.SetFromContinuousRGBToHSL(m_pExternalTristimulus);
                }

                inline  real GetHSLManhattanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetHSLManhattanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetHSLSquareEuclideanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetHSLSquareEuclideanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetHSLEuclideanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetHSLEuclideanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetHSLChebyshevHSLDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetHSLChebyshevDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetHSLMinkowskiDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const
                {
                    return m_InternalTristimulus.GetHSLMinkowskiDistance(pChromaticPixel->m_InternalTristimulus, P, IP);
                }
#endif

#ifdef _USE_COLOR_SPACE_XYZ_

                inline void ConvertRGBToXYZ()
                {
                    m_InternalTristimulus.SetFromContinuousRGBToXYZ(m_pExternalTristimulus);
                }

                inline  real GetXYZManhattanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetXYZManhattanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetXYZChebyshevDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetXYZChebyshevDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetXYZSquareEuclideanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetXYZSquareEuclideanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetXYZEuclideanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetXYZEuclideanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetXYZMinkowskiDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const
                {
                    return m_InternalTristimulus.GetXYZMinkowskiDistance(pChromaticPixel->m_InternalTristimulus, P, IP);
                }
#endif

#ifdef _USE_COLOR_SPACE_LAB_

                inline void ConvertRGBToLabD50()
                {
                    m_InternalTristimulus.SetFromContinuousRGBToLAB_D50(m_pExternalTristimulus);
                }

                inline void ConvertRGBToLabD55()
                {
                    m_InternalTristimulus.SetFromContinuousRGBToLAB_D55(m_pExternalTristimulus);
                }

                inline void ConvertRGBToLabD65()
                {
                    m_InternalTristimulus.SetFromContinuousRGBToLAB_D65(m_pExternalTristimulus);
                }

                inline void ConvertRGBToLabD75()
                {
                    m_InternalTristimulus.SetFromContinuousRGBToLAB_D75(m_pExternalTristimulus);
                }

                inline  real GetLabChebyshevDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetLABChebyshevDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetLabManhattanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetLABManhattanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetLabSquareEuclideanLabDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetLABSquareEuclideanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetLabEuclideanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetLABEuclideanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetLabMinkowskiDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const
                {
                    return m_InternalTristimulus.GetLABMinkowskiDistance(pChromaticPixel->m_InternalTristimulus, P, IP);
                }
#endif

#ifdef _USE_COLOR_SPACE_HSI_

                inline void ConvertRGBToHSI()
                {
                    m_InternalTristimulus.SetFromContinuousRGBToHSI(m_pExternalTristimulus);
                }

                inline  real GetHSIManhattanHSIDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetHSIManhattanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetHSIChebyshevHSIDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetHSIChebyshevDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetHSISquareEuclideanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetHSISquareEuclideanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetHSIEuclideanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetHSIEuclideanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetHSIMinkowskiDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const
                {
                    return m_InternalTristimulus.GetHSIMinkowskiDistance(pChromaticPixel->m_InternalTristimulus, P, IP);
                }
#endif

#ifdef _USE_COLOR_SPACE_HSV_

                inline void ConvertRGBToHSV()
                {
                    m_InternalTristimulus.SetFromContinuousRGBToHSV(m_pExternalTristimulus);
                }

                inline  real GetHSVManhattanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetHSVManhattanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetHSVChebyshevDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetHSVChebyshevDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetHSVSquareEuclideanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetHSVSquareEuclideanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetHSVEuclideanDistance(const CChromaticPixel* pChromaticPixel) const
                {
                    return m_InternalTristimulus.GetHSVEuclideanDistance(pChromaticPixel->m_InternalTristimulus);
                }

                inline  real GetHSVMinkowskiDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const
                {
                    return m_InternalTristimulus.GetHSVMinkowskiDistance(pChromaticPixel->m_InternalTristimulus, P, IP);
                }
#endif

            protected:

                Status m_Status;
                CPixelLocation m_Location;
                CContinuousTristimulusPixel m_InternalTristimulus;
                const CContinuousTristimulusPixel* m_pExternalTristimulus;
                const real* m_pSaliency;
                CChromaticSegment* m_pChromaticSegment;
                CChromaticLink* m_ChromaticLinks[8];

            };
        }
    }
}

