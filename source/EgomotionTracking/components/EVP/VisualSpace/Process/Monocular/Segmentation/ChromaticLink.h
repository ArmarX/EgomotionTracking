/*
 * ChromaticLink.h
 *
 *  Created on: 28.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "ChromaticPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Segmentation
        {
            class CChromaticLink
            {
            public:

                CChromaticLink(CChromaticPixel* pPixelA, CChromaticPixel* pPixelB) :
                    m_pPixelA(pPixelA), m_pPixelB(pPixelB), m_Distance(_REAL_MAX_)
                {
                }

                virtual ~CChromaticLink()
                {
                }

#ifdef _USE_COLOR_SPACE_RGB_

                inline void UpdateRGBManhattanDistance()
                {
                    m_Distance = m_pPixelA->m_pExternalTristimulus->GetRGBManhattanDistance(*m_pPixelB->m_pExternalTristimulus);
                }

                inline void UpdateRGBSquareEuclideanDistance()
                {
                    m_Distance = m_pPixelA->m_pExternalTristimulus->GetRGBSquareEuclideanDistance(*m_pPixelB->m_pExternalTristimulus);
                }

                inline void UpdateRGBEuclideanDistance()
                {
                    m_Distance = m_pPixelA->m_pExternalTristimulus->GetRGBEuclideanDistance(*m_pPixelB->m_pExternalTristimulus);
                }

                inline void UpdateRGBMinkowskiDistance(const real P, const real IP)
                {
                    m_Distance = m_pPixelA->m_pExternalTristimulus->GetRGBMinkowskiDistance(*m_pPixelB->m_pExternalTristimulus, P, IP);
                }

                inline void UpdateRGBChebyshevDistance()
                {
                    m_Distance = m_pPixelA->m_pExternalTristimulus->GetRGBChebyshevDistance(*m_pPixelB->m_pExternalTristimulus);
                }

                inline void UpdateRGBManhattanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_pExternalTristimulus->GetRGBManhattanDistance(*m_pPixelB->m_pExternalTristimulus);
                }

                inline void UpdateRGBSquareEuclideanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_pExternalTristimulus->GetRGBSquareEuclideanDistance(*m_pPixelB->m_pExternalTristimulus);
                }

                inline void UpdateRGBEuclideanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_pExternalTristimulus->GetRGBEuclideanDistance(*m_pPixelB->m_pExternalTristimulus);
                }

                inline void UpdateRGBMinkowskiDistanceSaliencyMode(const real P, const real IP)
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_pExternalTristimulus->GetRGBMinkowskiDistance(*m_pPixelB->m_pExternalTristimulus, P, IP);
                }

                inline void UpdateRGBChebyshevDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_pExternalTristimulus->GetRGBChebyshevDistance(*m_pPixelB->m_pExternalTristimulus);
                }
#endif

#ifdef _USE_COLOR_SPACE_XYZ_

                inline void UpdateXYZManhattanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetXYZManhattanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateXYZSquareEuclideanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetXYZSquareEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateXYZEuclideanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetXYZEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateXYZMinkowskiDistance(const real P, const real IP)
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetXYZMinkowskiDistance(m_pPixelB->m_InternalTristimulus, P, IP);
                }

                inline void UpdateXYZChebyshevDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetXYZChebyshevDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateXYZManhattanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetXYZManhattanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateXYZSquareEuclideanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetXYZSquareEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateXYZEuclideanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetXYZEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateXYZMinkowskiDistanceSaliencyMode(const real P, const real IP)
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetXYZMinkowskiDistance(m_pPixelB->m_InternalTristimulus, P, IP);
                }

                inline void UpdateXYZChebyshevDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetXYZChebyshevDistance(m_pPixelB->m_InternalTristimulus);
                }
#endif

#ifdef _USE_COLOR_SPACE_LAB_

                inline void UpdateLabManhattanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetLABManhattanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateLabSquareEuclideanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetLABSquareEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateLabEuclideanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetLABEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateLabMinkowskiDistance(const real P, const real IP)
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetLABMinkowskiDistance(m_pPixelB->m_InternalTristimulus, P, IP);
                }

                inline void UpdateLabChebyshevDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetLABChebyshevDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateLabManhattanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetLABManhattanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateLabSquareEuclideanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetLABSquareEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateLabEuclideanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency * *m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency * *m_pPixelB->m_pSaliency) + _REAL_ONE_) * m_pPixelA->m_InternalTristimulus.GetLABEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateLabMinkowskiDistanceSaliencyMode(const real P, const real IP)
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetLABMinkowskiDistance(m_pPixelB->m_InternalTristimulus, P, IP);
                }

                inline void UpdateLabChebyshevDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetLABChebyshevDistance(m_pPixelB->m_InternalTristimulus);
                }
#endif

#ifdef _USE_COLOR_SPACE_HSL_

                inline void UpdateHSLManhattanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSLManhattanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSLSquareEuclideanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSLSquareEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSLEuclideanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSLEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSLMinkowskiDistance(const real P, const real IP)
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSLMinkowskiDistance(m_pPixelB->m_InternalTristimulus, P, IP);
                }

                inline void UpdateHSLChebyshevDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSLChebyshevDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSLManhattanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSLManhattanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSLSquareEuclideanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSLSquareEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSLEuclideanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSLEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSLMinkowskiDistanceSaliencyMode(const real P, const real IP)
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSLMinkowskiDistance(m_pPixelB->m_InternalTristimulus, P, IP);
                }

                inline void UpdateHSLChebyshevDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSLChebyshevDistance(m_pPixelB->m_InternalTristimulus);
                }
#endif

#ifdef _USE_COLOR_SPACE_HSI_

                inline void UpdateHSIManhattanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSIManhattanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSISquareEuclideanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSISquareEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSIEuclideanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSIEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSIMinkowskiDistance(const real P, const real IP)
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSIMinkowskiDistance(m_pPixelB->m_InternalTristimulus, P, IP);
                }

                inline void UpdateHSIChebyshevDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSIChebyshevDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSIManhattanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSIManhattanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSISquareEuclideanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSISquareEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSIEuclideanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSIEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSIMinkowskiDistanceSaliencyMode(const real P, const real IP)
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSIMinkowskiDistance(m_pPixelB->m_InternalTristimulus, P, IP);
                }

                inline void UpdateHSIChebyshevDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSIChebyshevDistance(m_pPixelB->m_InternalTristimulus);
                }
#endif

#ifdef _USE_COLOR_SPACE_HSV_

                inline void UpdateHSVManhattanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSVManhattanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSVSquareEuclideanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSVSquareEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSVEuclideanDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSVEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSVMinkowskiDistance(const real P, const real IP)
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSVMinkowskiDistance(m_pPixelB->m_InternalTristimulus, P, IP);
                }

                inline void UpdateHSVChebyshevDistance()
                {
                    m_Distance = m_pPixelA->m_InternalTristimulus.GetHSVChebyshevDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSVManhattanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSVManhattanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSVSquareEuclideanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSVSquareEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSVEuclideanDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSVEuclideanDistance(m_pPixelB->m_InternalTristimulus);
                }

                inline void UpdateHSVMinkowskiDistanceSaliencyMode(const real P, const real IP)
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSVMinkowskiDistance(m_pPixelB->m_InternalTristimulus, P, IP);
                }

                inline void UpdateHSVChebyshevDistanceSaliencyMode()
                {
                    m_Distance = ((*m_pPixelA->m_pSaliency) + (*m_pPixelB->m_pSaliency)) * m_pPixelA->m_InternalTristimulus.GetHSVChebyshevDistance(m_pPixelB->m_InternalTristimulus);
                }
#endif
                inline  real GetDistance() const
                {
                    return m_Distance;
                }

                inline CChromaticPixel* GetPixelA()
                {
                    return m_pPixelA;
                }

                inline CChromaticPixel* GetPixelB()
                {
                    return m_pPixelB;
                }

                inline  bool IsLinkable() const
                {
                    return (m_pPixelA->IsLinkable() || m_pPixelB->IsLinkable());
                }

                inline  bool IsInternal(CChromaticPixel* pPixel) const
                {
                    if (m_pPixelA->GetChromaticSegment() == m_pPixelB->GetChromaticSegment())
                    {
                        if (pPixel == m_pPixelA)
                        {
                            return (m_pPixelB->m_Status == CChromaticPixel::eInternal);
                        }
                        if (pPixel == m_pPixelB)
                        {
                            return (m_pPixelA->m_Status == CChromaticPixel::eInternal);
                        }
                    }
                    return false;
                }

                inline  bool IsInternal() const
                {
                    return (m_pPixelA->GetChromaticSegment() == m_pPixelB->GetChromaticSegment());
                }

                void Attach(const real MaximalDistance, list<Segmentation::CChromaticPixel*>& ExpansionList)
                {
                    if (m_Distance < MaximalDistance)
                    {
                        if (m_pPixelA->IsLinkable())
                        {
                            m_pPixelB->Attach(m_pPixelA);
                            ExpansionList.push_back(m_pPixelA);
                        }
                        else if (m_pPixelB->IsLinkable())
                        {
                            m_pPixelA->Attach(m_pPixelB);
                            ExpansionList.push_back(m_pPixelB);
                        }
                    }
                }

            protected:

                CChromaticPixel* m_pPixelA;
                CChromaticPixel* m_pPixelB;
                real m_Distance;
            };
        }
    }
}

