/*
 * MonocularContinuousChromaticSegmentation.h
 *
 *  Created on: 23.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../../../Foundation/DataTypes/ColorSpaces.h"
#include "../../../../Visualization/Miscellaneous/TristimulusColorMap.h"
#include "../../Base/ImageProcessBase.h"

#include "ChromaticPixel.h"
#include "ChromaticLink.h"
#include "ChromaticSegment.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                class CMonocularContinuousChromaticSegmentation: public TImageProcessBase<CContinuousTristimulusPixel, Segmentation::CChromaticPixel, CDiscreteTristimulusPixel>
                {
                public:

                    class CParameters: public CParametersBase
                    {
                    public:

                        enum DisplayMode
                        {
                            eColorMappedSize, eColorIndexed, eMeanContinuousTristimulus, eInverseMeanContinuousTristimulus
                        };

                        enum DistanceMetric
                        {
                            eManhatan, eSquareEuclidean, eEuclidean, eMinkowski, eChebyshev
                        };

                        enum DistanceMode
                        {
                            eChromatic, eSaliency
                        };

                        CParameters(CMonocularContinuousChromaticSegmentation* pHost) :
                            CParametersBase(), m_pHost(pHost), m_pColorMap(NULL), m_ColorMapOwnerShip(true), m_ChromaticSpace(eLab), m_DistanceMetric(eEuclidean), m_DisplayMode(eColorIndexed), m_DistanceMode(eSaliency)
#ifdef _USE_COLOR_SPACE_LAB_
                            , m_WhiteSpot(ChromaticSpaceLAB::eD65)
#endif
                        {
                            m_pColorMap = Visualization::CTristimulusColorMap::LoadPredefinedColorMap(Visualization::CTristimulusColorMap::eRGB_Hot);
                        }

                        ~CParameters() override;

                        CParametersBase::ParameterChangeResult SetChromaticSpace(const ChromaticSpace SelectedSpace, const bool Wait = true);
                        ChromaticSpace GetChromaticSpace() const;

                        CParametersBase::ParameterChangeResult SetDistanceMetric(const DistanceMetric SelectedMetric, const bool Wait = true);
                        DistanceMetric GetDistanceMetric() const;

#ifdef _USE_COLOR_SPACE_LAB_
                        CParametersBase::ParameterChangeResult SetDistanceMetric(const ChromaticSpaceLAB::WhiteSpot SelectedWhiteSpot, const bool Wait = true);
                        ChromaticSpaceLAB::WhiteSpot GetWhiteSpot() const;
#endif

                        CParametersBase::ParameterChangeResult SetDisplayMode(const DisplayMode Mode, const bool Wait = true);
                        DisplayMode GetDisplayMode() const;

                        CParametersBase::ParameterChangeResult SetColorMap(const Visualization::CTristimulusColorMap* pColorMap, const bool OwnerShip, const bool Wait = true);
                        CParametersBase::ParameterChangeResult SetColorMap(const Visualization::CTristimulusColorMap::PredefinedColorMap ColorMap, const bool Wait = true);
                        const Visualization::CTristimulusColorMap* GetColorMap() const;

                        CParametersBase::ParameterChangeResult SetDistanceMode(const DistanceMode Mode, const bool Wait = true);
                        DistanceMode GetDistanceMode() const;

                    protected:

                        CMonocularContinuousChromaticSegmentation* m_pHost;
                        const Visualization::CTristimulusColorMap* m_pColorMap;
                        bool m_ColorMapOwnerShip;
                        ChromaticSpace m_ChromaticSpace;
                        DistanceMetric m_DistanceMetric;
                        DisplayMode m_DisplayMode;
                        DistanceMode m_DistanceMode;
#ifdef _USE_COLOR_SPACE_LAB_
                        ChromaticSpaceLAB::WhiteSpot m_WhiteSpot;
#endif

                    };

                    CMonocularContinuousChromaticSegmentation(CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, const TImage<real>* pSaliencyImage, Threading::CMutex* pInputImageMutex);
                    CMonocularContinuousChromaticSegmentation(CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, Threading::CMutex* pInputImageMutex);
                    ~CMonocularContinuousChromaticSegmentation() override;

                    bool Execute(const Identifier TrialId) override;
                    bool Display() override;
                    CParameters* GetParameters() override;

                protected:

                    friend class CMonocularContinuousChromaticSegmentation::CParameters;

                    void InitializeChromaticImage(const TImage<real>* pSaliencyImage);
                    void InitializeChromaticLinks(const uint SubSampligFactor);
                    void FinalizeChromaticLinks();
                    void ClearSegments();

                    void LoadSaliencyImage();
                    void UpdateDistances(const bool SortSelected, CParameters::DistanceMode Mode);
                    real DetermineDistanceByPercentil(const real Percentil);
                    void Segment(const real MaximalDistance);

                    void DisplaySegmentsBySize();
                    void DisplaySegmentsByIndex();
                    void DisplaySegmentsByMeanContinuousTristimulus();
                    void DisplaySegmentsByInverseMeanContinuousTristimulus();

                    static  bool SortByDistance(const Segmentation::CChromaticLink* plhs, const Segmentation::CChromaticLink* prhs);

#ifdef _USE_COLOR_SPACE_RGB_
                    void UpdateRGBDistances(CParameters::DistanceMetric Metric, CParameters::DistanceMode Mode);
#endif
#ifdef _USE_COLOR_SPACE_XYZ_
                    void ConvertRGBToXYZ();
                    void UpdateXYZDistances(CParameters::DistanceMetric Metric, CParameters::DistanceMode Mode);
#endif
#ifdef _USE_COLOR_SPACE_LAB_
                    void ConvertRGBToLab(ChromaticSpaceLAB::WhiteSpot SelectedWhiteSpot);
                    void UpdateLabDistances(CParameters::DistanceMetric Metric, ChromaticSpaceLAB::WhiteSpot SelectedWhiteSpot, CParameters::DistanceMode Mode);
#endif
#ifdef _USE_COLOR_SPACE_HSL_
                    void ConvertRGBToHSL();
                    void UpdateHSLDistances(CParameters::DistanceMetric Metric, CParameters::DistanceMode Mode);
#endif
#ifdef _USE_COLOR_SPACE_HSI_
                    void ConvertRGBToHSI();
                    void UpdateHSIDistances(CParameters::DistanceMetric Metric, CParameters::DistanceMode Mode);
#endif
#ifdef _USE_COLOR_SPACE_HSV_
                    void ConvertRGBToHSV();
                    void UpdateHSVDistances(CParameters::DistanceMetric Metric, CParameters::DistanceMode Mode);
#endif

                    CParameters m_Parameters;
                    const TImage<real>* m_pExternalSaliencyImage;
                    TImage<real>* m_pInternalSaliencyImage;
                    list<Segmentation::CChromaticSegment*> m_SelectedChromaticSegments;
                    list<Segmentation::CChromaticSegment*> m_SpuriousChromaticSegments;
                    vector<Segmentation::CChromaticLink*> m_ChromaticLinks;
                    vector<Segmentation::CChromaticLink*> m_SelectedChromaticLinks;
                };
            }
        }
    }
}

