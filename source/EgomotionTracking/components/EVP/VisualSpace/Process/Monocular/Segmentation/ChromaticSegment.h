/*
 * ChromaticSegment.h
 *
 *  Created on: 28.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "ChromaticPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Segmentation
        {
            class CChromaticSegment
            {
            public:

                CChromaticSegment(CChromaticPixel* pChromaticPixel);
                virtual ~CChromaticSegment();

                void AddPixel(CChromaticPixel* pChromaticPixel);
                const list<CChromaticPixel*>& GetChromaticPixels() const;
                const list<CChromaticPixel*>& GetMinimalExternalChromaticPixels() const;
                const list<CChromaticPixel*>& GetExternalChromaticPixels() const;
                const list<CChromaticPixel*>& GetInternalChromaticPixels() const;

                uint GetSize() const;

                real DelineateBoundry();
                void ReduceBoundry();
                void ExtractContoures();
                CContinuousTristimulusPixel GetMeanContinuousTristimulus();

            protected:

                list<CChromaticPixel*> m_ChromaticPixels;
                list<CChromaticPixel*> m_MinimalExternalChromaticPixels;
                list<CChromaticPixel*> m_ExternalChromaticPixels;
                list<CChromaticPixel*> m_InternalChromaticPixels;
            };
        }
    }
}

