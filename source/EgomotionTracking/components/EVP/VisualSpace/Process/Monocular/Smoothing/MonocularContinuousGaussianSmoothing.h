/*
 * MonocularContinuousGaussianSmoothing.h
 *
 *  Created on: Mar 16, 2012
 *      Author: david
 */

#pragma once

#include "../../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../../Visualization/Miscellaneous/TristimulusColorMap.h"
#include "../../../Kernels/ConvolutionGaussianKernel2D.h"
#include "../../Base/ImageProcessBase.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                class CMonocularContinuousGaussianSmoothing: public TImageProcessBase<real, real, CDiscreteTristimulusPixel>
                {
                public:

                    class CParameters: public CParametersBase
                    {
                    public:

                        enum DisplayMode
                        {
                            eRoundedIntensity, eScaledIntensity, eColorMapped
                        };

                        CParameters(CMonocularContinuousGaussianSmoothing* pHost) :
                            CParametersBase(), m_pHost(pHost), m_pColorMap(NULL), m_ColorMapOwnerShip(true), m_SmoothingSigma(_REAL_ONE_), m_DisplayMode(eRoundedIntensity)
                        {
                            m_pColorMap = Visualization::CTristimulusColorMap::LoadPredefinedColorMap(Visualization::CTristimulusColorMap::eRGB_Hot);
                        }

                        ~CParameters() override;

                        CParametersBase::ParameterChangeResult SetSmoothingSigma(const real SmoothingSigma, const bool Wait = true);
                        real GetSmoothingSigma() const;

                        CParametersBase::ParameterChangeResult SetDisplayMode(const DisplayMode Mode, const bool Wait = true);
                        DisplayMode GetDisplayMode() const;

                        CParametersBase::ParameterChangeResult SetColorMap(const Visualization::CTristimulusColorMap* pColorMap, const bool OwnerShip, const bool Wait = true);
                        CParametersBase::ParameterChangeResult SetColorMap(const Visualization::CTristimulusColorMap::PredefinedColorMap ColorMap, const bool Wait = true);
                        const Visualization::CTristimulusColorMap* GetColorMap() const;

                    protected:

                        CMonocularContinuousGaussianSmoothing* m_pHost;
                        const Visualization::CTristimulusColorMap* m_pColorMap;
                        bool m_ColorMapOwnerShip;
                        real m_SmoothingSigma;
                        DisplayMode m_DisplayMode;
                    };

                    CMonocularContinuousGaussianSmoothing(CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, Threading::CMutex* pInputImageMutex);
                    ~CMonocularContinuousGaussianSmoothing() override;

                    bool Execute(const Identifier TrialId) override;
                    bool Display() override;
                    CParameters* GetParameters() override;

                protected:

                    bool UpdateSmoothingSigma();

                    CParameters m_Parameters;
                    Kernels::CConvolutionGaussianKernel2D* m_pSpatialSmoothingKernel;
                };
            }
        }
    }
}

