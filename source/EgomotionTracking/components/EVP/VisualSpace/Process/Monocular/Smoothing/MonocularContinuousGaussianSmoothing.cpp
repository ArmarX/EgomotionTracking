/*
 * MonocularContinuousGaussianSmoothing.cpp
 *
 *  Created on: Mar 16, 2012
 *      Author: david
 */

#include "MonocularContinuousGaussianSmoothing.h"
#include "../../../Common/ImageExporter.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularContinuousGaussianSmoothing::CParameters
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                CMonocularContinuousGaussianSmoothing::CParameters::~CParameters()
                {
                    RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousGaussianSmoothing::CParameters::SetSmoothingSigma(const real SmoothingSigma, const bool Wait)
                {
                    if ((SmoothingSigma <= _REAL_ZERO_) || (SmoothingSigma > real(6.0)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (SmoothingSigma == m_SmoothingSigma)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_SmoothingSigma = SmoothingSigma;
                        CParametersBase::ParameterChangeResult Result = m_pHost->UpdateSmoothingSigma() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return m_pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularContinuousGaussianSmoothing::CParameters::GetSmoothingSigma() const
                {
                    return m_SmoothingSigma;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousGaussianSmoothing::CParameters::SetDisplayMode(const DisplayMode Mode, const bool Wait)
                {
                    if (!((Mode == CParameters::eRoundedIntensity) || (Mode == CParameters::eScaledIntensity) || (Mode == CParameters::eColorMapped)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (Mode == m_DisplayMode)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_DisplayMode = Mode;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                CMonocularContinuousGaussianSmoothing::CParameters::DisplayMode CMonocularContinuousGaussianSmoothing::CParameters::GetDisplayMode() const
                {
                    return m_DisplayMode;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousGaussianSmoothing::CParameters::SetColorMap(const Visualization::CTristimulusColorMap* pColorMap, const bool OwnerShip, const bool Wait)
                {
                    if (!pColorMap)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (pColorMap == m_pColorMap)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                        m_ColorMapOwnerShip = OwnerShip;
                        m_pColorMap = pColorMap;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousGaussianSmoothing::CParameters::SetColorMap(const Visualization::CTristimulusColorMap::PredefinedColorMap ColorMap, const bool Wait)
                {
                    if (!Visualization::CTristimulusColorMap::IsValidPredefinedColorMap(ColorMap))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                        m_ColorMapOwnerShip = true;
                        m_pColorMap = Visualization::CTristimulusColorMap::LoadPredefinedColorMap(ColorMap);
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                const Visualization::CTristimulusColorMap* CMonocularContinuousGaussianSmoothing::CParameters::GetColorMap() const
                {
                    return m_pColorMap;
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularContinuousGaussianSmoothing
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                CMonocularContinuousGaussianSmoothing::CMonocularContinuousGaussianSmoothing(CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, Threading::CMutex* pInputImageMutex) :
                    TImageProcessBase<real, real, CDiscreteTristimulusPixel> (CImageProcessBase::eGaussianSmoothing, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this), m_pSpatialSmoothingKernel(nullptr)
                {
                    if (m_IsEnabled)
                    {
                        const CImageSize& Size = m_pInputActiveZone->GetSize();
                        m_pOutputImage = new TImage<real> (Size);
                        m_pDisplayImage = new TImage<CDiscreteTristimulusPixel> (Size);
                        m_pInternalInputImage = new TImage<real> (Size);
                        UpdateSmoothingSigma();
                        m_OutputActiveZone.SetConnection(m_pInputActiveZone, m_pSpatialSmoothingKernel, Kernels::TConvolutionKernel2D<real>::GetKernelOffsetActiveZoneUpdate);
                    }
                }

                CMonocularContinuousGaussianSmoothing::~CMonocularContinuousGaussianSmoothing()
                {
                    m_OutputActiveZone.Disconnect(true, true);
                    RELEASE_OBJECT(m_pOutputImage)
                    RELEASE_OBJECT(m_pDisplayImage)
                    RELEASE_OBJECT(m_pInternalInputImage)
                    RELEASE_OBJECT(m_pSpatialSmoothingKernel)
                }

                bool CMonocularContinuousGaussianSmoothing::Execute(const Identifier TrialId)
                {
                    const uint ActiveHeight = m_OutputActiveZone.GetActiveHeight();
                    const uint ActiveWidth = m_OutputActiveZone.GetActiveWidth();
                    const uint Offset = m_OutputActiveZone.GetHorizontalOffset();
                    const uint KernelDiameter = m_pSpatialSmoothingKernel->GetWidth();
                    const uint ConvolutionOffset = m_OutputActiveZone.GetWidth() - KernelDiameter;
                    const real* pKernelBase = m_pSpatialSmoothingKernel->GetKernelReadOnlyBuffer();
                    const real* pInternBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferAt(m_pInputActiveZone->GetUpperLeftLocation());
                    real* pOutputPixel = m_pOutputImage->GetWritableBufferAt(m_OutputActiveZone.GetUpperLeftLocation());
                    for (uint i = 0; i < ActiveHeight; ++i, pOutputPixel += Offset, pInternBaseInputPixel += Offset)
                        for (uint j = 0; j < ActiveWidth; ++j)
                        {
                            const real* pKernel = pKernelBase;
                            const real* pInputPixel = pInternBaseInputPixel++;
                            real Acumulator = _REAL_ZERO_;
                            for (uint k = 0; k < KernelDiameter; ++k, pInputPixel += ConvolutionOffset)
                                for (uint l = 0; l < KernelDiameter; ++l)
                                {
                                    Acumulator += *pInputPixel++ * *pKernel++;
                                }
                            *pOutputPixel++ = Acumulator;
                        }
                    return true;
                }

                bool CMonocularContinuousGaussianSmoothing::Display()
                {
                    if (m_IsEnabled && m_OutputActiveZone.IsEnabled() && GetTotalExecutions())
                    {
                        if (StartDisplaying())
                        {
                            bool Result = false;
                            switch (m_Parameters.GetDisplayMode())
                            {
                                case CParameters::eRoundedIntensity:
                                    Result = CImageExporter::DisplayRoundedIntensity(&m_OutputActiveZone, m_pOutputImage, m_pDisplayImage);
                                    break;
                                case CParameters::eScaledIntensity:
                                    Result = CImageExporter::DisplayScaledIntensity(&m_OutputActiveZone, m_pOutputImage, m_pDisplayImage);
                                    break;
                                case CParameters::eColorMapped:
                                    Result = CImageExporter::DisplayColorMapped(&m_OutputActiveZone, m_pOutputImage, m_pDisplayImage, m_Parameters.GetColorMap());
                                    break;
                            }
                            return FinishDisplaying() && Result;
                        }
                    }
                    return false;
                }

                CMonocularContinuousGaussianSmoothing::CParameters* CMonocularContinuousGaussianSmoothing::GetParameters()
                {
                    return m_IsEnabled ? &m_Parameters : nullptr;
                }

                bool CMonocularContinuousGaussianSmoothing::UpdateSmoothingSigma()
                {
                    if (m_pSpatialSmoothingKernel)
                    {
                        m_pSpatialSmoothingKernel->SetStandardDeviation(m_Parameters.GetSmoothingSigma());
                    }
                    else
                    {
                        m_pSpatialSmoothingKernel = new Kernels::CConvolutionGaussianKernel2D(m_Parameters.GetSmoothingSigma());
                    }
                    m_pSpatialSmoothingKernel->Normalize();
                    return m_OutputActiveZone.UpdateConnection();

                }
            }
        }
    }
}
