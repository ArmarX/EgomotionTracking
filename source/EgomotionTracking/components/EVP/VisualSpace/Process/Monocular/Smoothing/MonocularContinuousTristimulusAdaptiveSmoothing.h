/*
 * MonocularContinuousTristimulusAdaptiveSmoothing.h
 *
 *  Created on: 23.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../../../Foundation/Mathematics/1D/Common1D.h"
#include "../../../../Foundation/Mathematics/1D/ExponentialLookUpTable.h"
#include "../../../../Visualization/Miscellaneous/TristimulusColorMap.h"
#include "../../../Kernels/ConvolutionGaussianKernel2D.h"
#include "../../Base/ImageProcessBase.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                class CMonocularContinuousTristimulusAdaptiveSmoothing: public TImageProcessBase<CContinuousTristimulusPixel, CContinuousTristimulusPixel, CDiscreteTristimulusPixel>
                {
                public:

                    class CParameters: public CParametersBase
                    {
                    public:

                        enum RangeModulation
                        {
                            eFixedRangeSigma, eAutoRangeSigmaEstimationByPercentil
                        };

                        enum DisplayChannel
                        {
                            eSimilarity, eAdaptiveSmoothed
                        };

                        enum DisplayMode
                        {
                            eRoundedIntensity, eScaledIntensity, eColorMapped
                        };

                        CParameters(CMonocularContinuousTristimulusAdaptiveSmoothing* pHost) :
                            CParametersBase(), m_pHost(pHost), m_pColorMap(NULL), m_ColorMapOwnerShip(true), m_AdaptiveSmoothingSigma(_REAL_ONE_), m_AdaptiveRangeSigma(_REAL_THREE_), m_RangePercentil(real(0.8)), m_RangeModulation(eAutoRangeSigmaEstimationByPercentil), m_DisplayChannel(eAdaptiveSmoothed), m_DisplayMode(eScaledIntensity), m_EnabledUpdateSaliencyMap(false)
                        {
                            m_pColorMap = Visualization::CTristimulusColorMap::LoadPredefinedColorMap(Visualization::CTristimulusColorMap::eRGB_Hot);
                        }

                        ~CParameters() override;

                        CParametersBase::ParameterChangeResult SetAdaptiveSmoothingSigma(const real AdaptiveSmoothingSigma, const bool Wait = true);
                        real GetAdaptiveSmoothingSigma() const;

                        CParametersBase::ParameterChangeResult SetAdaptiveRangeSigma(const real AdaptiveRangeSigma, const bool Wait = true);
                        real GetAdaptiveRangeSigma() const;

                        CParametersBase::ParameterChangeResult SetRangePercentil(const real RangePercentil, const bool Wait = true);
                        real GetRangePercentil() const;

                        CParametersBase::ParameterChangeResult SetRangeModulation(const RangeModulation Mode, const bool Wait = true);
                        RangeModulation GetRangeModulation() const;

                        CParametersBase::ParameterChangeResult SetDisplayChannel(const DisplayChannel Channel, const bool Wait = true);
                        DisplayChannel GetDisplayChannel() const;

                        CParametersBase::ParameterChangeResult SetDisplayMode(const DisplayMode Mode, const bool Wait = true);
                        DisplayMode GetDisplayMode() const;

                        CParametersBase::ParameterChangeResult SetColorMap(const Visualization::CTristimulusColorMap* pColorMap, const bool OwnerShip, const bool Wait = true);
                        CParametersBase::ParameterChangeResult SetColorMap(const Visualization::CTristimulusColorMap::PredefinedColorMap ColorMap, const bool Wait = true);
                        const Visualization::CTristimulusColorMap* GetColorMap() const;

                        CParametersBase::ParameterChangeResult SetEnabledUpdateSaliencyMap(const bool Enabled, const bool Wait = true);
                        bool GetEnabledUpdateSaliencyMap() const;

                    protected:

                        CMonocularContinuousTristimulusAdaptiveSmoothing* m_pHost;
                        const Visualization::CTristimulusColorMap* m_pColorMap;
                        bool m_ColorMapOwnerShip;
                        real m_AdaptiveSmoothingSigma;
                        real m_AdaptiveRangeSigma;
                        real m_RangePercentil;
                        RangeModulation m_RangeModulation;
                        DisplayChannel m_DisplayChannel;
                        DisplayMode m_DisplayMode;
                        bool m_EnabledUpdateSaliencyMap;
                    };

                    CMonocularContinuousTristimulusAdaptiveSmoothing(CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, Threading::CMutex* pInputImageMutex);
                    ~CMonocularContinuousTristimulusAdaptiveSmoothing() override;

                    bool Execute(const Identifier TrialId) override;
                    bool Display() override;
                    CParameters* GetParameters() override;

                    real GetAdaptiveRangeSigma() const;
                    const TImage<real>* GetSaliencyImage() const;

                protected:

                    friend class CMonocularContinuousTristimulusAdaptiveSmoothing::CParameters;

                    struct DensityDeviation
                    {
                        real m_Density;
                        real m_Deviation;
                    };

                    real DetermineDeltaSigma(const real Percentile, const uint SubSamplingFactor);
                    void AdaptiveSmoothing(const real AdaptiveRangeSigma);
                    void UpdateSaliencyMap();
                    bool UpdateAdaptiveSmoothingSigma();
                    bool DisplayByMode(const TImage<CContinuousTristimulusPixel>* pImage);
                    bool DisplayByMode(const TImage<real>* pImage);
                    bool DisplayIntensityRounding(const TImage<CContinuousTristimulusPixel>* pImage);
                    bool DisplayIntensityRounding(const TImage<real>* pImage);
                    bool DisplayScaledIntensity(const TImage<CContinuousTristimulusPixel>* pImage);
                    bool DisplayScaledIntensity(const TImage<real>* pImage);
                    bool DisplayColorMapped(const TImage<real>* pImage, const real LUTScale, const CDiscreteTristimulusPixel* pLUT);
                    CContinuousTristimulusPixel GetExtrema(const TImage<CContinuousTristimulusPixel>* pImage, CContinuousTristimulusPixel& Maximal, CContinuousTristimulusPixel& Minimal) const;
                    real GetExtrema(const TImage<real>* pImage, real& Maximal, real& Minimal) const;
                    static  bool SortDensityDeviation(const DensityDeviation& lhs, const DensityDeviation& rhs);

                    CParameters m_Parameters;
                    Kernels::CConvolutionGaussianKernel2D* m_pSpatialSmoothingKernel;
                    TImage<real>* m_pSimilarityImage;
                    real m_KernelPartialIntegration;
                    real m_AdaptiveRangeSigma;

#ifdef _SMOOTHING_USE_LOOKUP_TABLE_

                    DECLARE_EXPONENTIAL_LOOKUP_TABLE

#endif

                };
            }
        }
    }
}

