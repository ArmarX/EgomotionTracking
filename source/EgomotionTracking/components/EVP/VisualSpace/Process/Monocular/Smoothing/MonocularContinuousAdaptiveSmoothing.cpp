/*
 * MonocularContinuousAdaptiveSmoothing.cpp
 *
 *  Created on: 23.10.2011
 *      Author: gonzalez
 */

#include "MonocularContinuousAdaptiveSmoothing.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularContinuousAdaptiveSmoothing::CParameters
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                CMonocularContinuousAdaptiveSmoothing::CParameters::~CParameters()
                {
                    RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousAdaptiveSmoothing::CParameters::SetAdaptiveSmoothingSigma(const real AdaptiveSmoothingSigma, const bool Wait)
                {
                    if ((AdaptiveSmoothingSigma <= _REAL_ZERO_) || (AdaptiveSmoothingSigma > real(6.0)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (AdaptiveSmoothingSigma == m_AdaptiveSmoothingSigma)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_AdaptiveSmoothingSigma = AdaptiveSmoothingSigma;
                        CParametersBase::ParameterChangeResult Result = m_pHost->UpdateAdaptiveSmoothingSigma() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessParametersChange;
                        return m_pHost->EndSettingsChangeBlockExecution() ? Result : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularContinuousAdaptiveSmoothing::CParameters::GetAdaptiveSmoothingSigma() const
                {
                    return m_AdaptiveSmoothingSigma;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousAdaptiveSmoothing::CParameters::SetAdaptiveRangeSigma(const real AdaptiveRangeSigma, const bool Wait)
                {
                    if ((AdaptiveRangeSigma <= _REAL_ZERO_) || (AdaptiveRangeSigma > _REAL_255_))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (AdaptiveRangeSigma == m_AdaptiveRangeSigma)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_AdaptiveRangeSigma = AdaptiveRangeSigma;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularContinuousAdaptiveSmoothing::CParameters::GetAdaptiveRangeSigma() const
                {
                    return m_AdaptiveRangeSigma;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousAdaptiveSmoothing::CParameters::SetRangePercentil(const real RangePercentil, const bool Wait)
                {
                    if ((RangePercentil <= _REAL_ZERO_) || (RangePercentil >= _REAL_ONE_))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (RangePercentil == m_RangePercentil)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_RangePercentil = RangePercentil;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                real CMonocularContinuousAdaptiveSmoothing::CParameters::GetRangePercentil() const
                {
                    return m_RangePercentil;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousAdaptiveSmoothing::CParameters::SetRangeModulation(const RangeModulation Modulation, const bool Wait)
                {
                    if (!((Modulation == eFixedRangeSigma) || (Modulation == eAutoRangeSigmaEstimationByPercentil)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (Modulation == m_RangeModulation)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_RangeModulation = Modulation;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                CMonocularContinuousAdaptiveSmoothing::CParameters::RangeModulation CMonocularContinuousAdaptiveSmoothing::CParameters::GetRangeModulation() const
                {
                    return m_RangeModulation;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousAdaptiveSmoothing::CParameters::SetDisplayChannel(const DisplayChannel Channel, const bool Wait)
                {
                    if (!((Channel == eSimilarity) || (Channel == eAdaptiveSmoothed)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (Channel == m_DisplayChannel)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_DisplayChannel = Channel;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                CMonocularContinuousAdaptiveSmoothing::CParameters::DisplayChannel CMonocularContinuousAdaptiveSmoothing::CParameters::GetDisplayChannel() const
                {
                    return m_DisplayChannel;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousAdaptiveSmoothing::CParameters::SetDisplayMode(const DisplayMode Mode, const bool Wait)
                {
                    if (!((Mode == CParameters::eRoundedIntensity) || (Mode == CParameters::eScaledIntensity) || (Mode == CParameters::eColorMapped)))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (Mode == m_DisplayMode)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_DisplayMode = Mode;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                CMonocularContinuousAdaptiveSmoothing::CParameters::DisplayMode CMonocularContinuousAdaptiveSmoothing::CParameters::GetDisplayMode() const
                {
                    return m_DisplayMode;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousAdaptiveSmoothing::CParameters::SetColorMap(const Visualization::CTristimulusColorMap* pColorMap, const bool OwnerShip, const bool Wait)
                {
                    if (!pColorMap)
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (pColorMap == m_pColorMap)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                        m_ColorMapOwnerShip = OwnerShip;
                        m_pColorMap = pColorMap;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousAdaptiveSmoothing::CParameters::SetColorMap(const Visualization::CTristimulusColorMap::PredefinedColorMap ColorMap, const bool Wait)
                {
                    if (!Visualization::CTristimulusColorMap::IsValidPredefinedColorMap(ColorMap))
                    {
                        return CParametersBase::eFailure_ParametersOutofScope;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        RELEASE_OBJECT_BY_OWNERSHIP(m_pColorMap, m_ColorMapOwnerShip)
                        m_ColorMapOwnerShip = true;
                        m_pColorMap = Visualization::CTristimulusColorMap::LoadPredefinedColorMap(ColorMap);
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                const Visualization::CTristimulusColorMap* CMonocularContinuousAdaptiveSmoothing::CParameters::GetColorMap() const
                {
                    return m_pColorMap;
                }

                CParametersBase::ParameterChangeResult CMonocularContinuousAdaptiveSmoothing::CParameters::SetEnabledUpdateSaliencyMap(const bool Enabled, const bool Wait)
                {
                    if (Enabled == m_EnabledUpdateSaliencyMap)
                    {
                        return CParametersBase::eNoChange;
                    }
                    if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                    {
                        m_EnabledUpdateSaliencyMap = Enabled;
                        return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                    }
                    return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                }

                bool CMonocularContinuousAdaptiveSmoothing::CParameters::GetEnabledUpdateSaliencyMap() const
                {
                    return m_EnabledUpdateSaliencyMap;
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //CMonocularContinuousAdaptiveSmoothing
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _SMOOTHING_USE_LOOKUP_TABLE_
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                INITIALIZE_EXPONENTIAL_LOOKUP_TABLE(CMonocularContinuousAdaptiveSmoothing)
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif

                CMonocularContinuousAdaptiveSmoothing::CMonocularContinuousAdaptiveSmoothing(CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, Threading::CMutex* pInputImageMutex) :
                    TImageProcessBase<real, real, CDiscreteTristimulusPixel> (CImageProcessBase::eContinuousAdaptiveSmoothing, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this), m_pSpatialSmoothingKernel(nullptr), m_pSimilarityImage(nullptr), m_KernelPartialIntegration(_REAL_ZERO_), m_AdaptiveRangeSigma(_REAL_ZERO_)
                {
                    if (m_IsEnabled)
                    {
                        const CImageSize& Size = m_pInputActiveZone->GetSize();
                        m_pOutputImage = new TImage<real> (Size);
                        m_pDisplayImage = new TImage<CDiscreteTristimulusPixel> (Size);
                        m_pInternalInputImage = new TImage<real> (Size);
                        m_pSimilarityImage = new TImage<real> (Size);
                        UpdateAdaptiveSmoothingSigma();
                        m_OutputActiveZone.SetConnection(m_pInputActiveZone, m_pSpatialSmoothingKernel, Kernels::TConvolutionKernel2D<real>::GetKernelOffsetActiveZoneUpdate);
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _SMOOTHING_USE_LOOKUP_TABLE_
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        LOAD_EXPONENTIAL_LOOKUP_TABLE(CMonocularContinuousAdaptiveSmoothing)
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif
                    }
                }

                CMonocularContinuousAdaptiveSmoothing::~CMonocularContinuousAdaptiveSmoothing()
                {
                    m_OutputActiveZone.Disconnect(true, true);
                    RELEASE_OBJECT(m_pOutputImage)
                    RELEASE_OBJECT(m_pDisplayImage)
                    RELEASE_OBJECT(m_pInternalInputImage)
                    RELEASE_OBJECT(m_pSimilarityImage)
                    RELEASE_OBJECT(m_pSpatialSmoothingKernel)
                }

                bool CMonocularContinuousAdaptiveSmoothing::Execute(const Identifier TrialId)
                {
                    if (m_IsEnabled && m_OutputActiveZone.IsEnabled())
                    {
                        if (StartExecuting(TrialId))
                        {

                            START_PROCESS_EXECUTION_LOG("Execute", TrialId)

                            LoadInputImage();

                            switch (m_Parameters.GetRangeModulation())
                            {
                                case CParameters::eAutoRangeSigmaEstimationByPercentil:
                                    m_AdaptiveRangeSigma = DetermineDeltaSigma(m_Parameters.GetRangePercentil(), m_pSpatialSmoothingKernel->GetWidth());
                                    AdaptiveSmoothing(m_AdaptiveRangeSigma);
                                    break;
                                case CParameters::eFixedRangeSigma:
                                    AdaptiveSmoothing(m_Parameters.GetAdaptiveRangeSigma());
                                    break;
                            }

                            if (m_Parameters.GetEnabledUpdateSaliencyMap())
                            {
                                UpdateSaliencyMap();
                            }

                            STOP_PROCESS_EXECUTION_LOG

                            return FinishExecution();
                        }
                    }
                    return false;
                }

                bool CMonocularContinuousAdaptiveSmoothing::Display()
                {
                    if (m_IsEnabled && m_OutputActiveZone.IsEnabled() && GetTotalExecutions())
                    {
                        if (StartDisplaying())
                        {
                            m_pDisplayImage->Clear();

                            switch (m_Parameters.GetDisplayChannel())
                            {
                                case CParameters::eSimilarity:
                                    DisplayByMode(m_pSimilarityImage);
                                    break;
                                case CParameters::eAdaptiveSmoothed:
                                    DisplayByMode(m_pOutputImage);
                                    break;
                            }

                            return FinishDisplaying();
                        }
                    }
                    return false;
                }

                CMonocularContinuousAdaptiveSmoothing::CParameters* CMonocularContinuousAdaptiveSmoothing::GetParameters()
                {
                    return m_IsEnabled ? &m_Parameters : nullptr;
                }

                real CMonocularContinuousAdaptiveSmoothing::GetAdaptiveRangeSigma() const
                {
                    return m_IsEnabled ? m_AdaptiveRangeSigma : _REAL_ZERO_;
                }

                const TImage<real>* CMonocularContinuousAdaptiveSmoothing::GetSaliencyImage() const
                {
                    return m_IsEnabled ? const_cast<const TImage<real>*>(m_pSimilarityImage) : nullptr;
                }

                real CMonocularContinuousAdaptiveSmoothing::DetermineDeltaSigma(const real Percentile, const uint SubSamplingFactor)
                {
                    DensityDeviation CurrenDensityDeviation;
                    vector<DensityDeviation> DeltaDistribution;
                    uint TotalAnalyzedPixels = uint(RealCeil(real(m_OutputActiveZone.GetActiveWidth()) / real(SubSamplingFactor))) * uint(RealCeil(real(m_OutputActiveZone.GetActiveHeight()) / real(SubSamplingFactor)));
                    DeltaDistribution.reserve(TotalAnalyzedPixels * (m_pSpatialSmoothingKernel->GetSize().GetArea() - 1));
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate KernelRadius = m_pSpatialSmoothingKernel->GetRadius();
                    const uint Width = m_OutputActiveZone.GetWidth();
                    const uint Offset = Width * SubSamplingFactor;
                    const real* pBaseKernel = m_pSpatialSmoothingKernel->GetKernelReadOnlyBuffer();
                    const real* pExternalBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferAt(m_pInputActiveZone->GetUpperLeftLocation());
                    const real* pBaseCenterInputPixel = m_pInternalInputImage->GetReadOnlyBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; y += SubSamplingFactor, pExternalBaseInputPixel += Offset, pBaseCenterInputPixel += Offset)
                    {
                        const real* pBaseInputPixel = pExternalBaseInputPixel;
                        const real* pCenterInputPixel = pBaseCenterInputPixel;
                        for (coordinate x = X0; x < X1; x += SubSamplingFactor, pBaseInputPixel += SubSamplingFactor, pCenterInputPixel += SubSamplingFactor)
                        {
                            const real* pInputPixel = pBaseInputPixel;
                            const real* pKernel = pBaseKernel;
                            const real CentralValue = *pCenterInputPixel;
                            for (coordinate Dy = -KernelRadius; Dy <= KernelRadius; ++Dy, pInputPixel += Width)
                                for (coordinate Dx = -KernelRadius; Dx <= KernelRadius; ++Dx)
                                    if (Dx || Dy)
                                    {
                                        CurrenDensityDeviation.m_Density = *pKernel++;
                                        CurrenDensityDeviation.m_Deviation = RealAbs(CentralValue - pInputPixel[Dx]);
                                        DeltaDistribution.push_back(CurrenDensityDeviation);
                                    }
                        }
                    }
                    TSort(DeltaDistribution.begin(), DeltaDistribution.end(), SortDensityDeviation);
                    const real DensityThreshold = m_KernelPartialIntegration * real(TotalAnalyzedPixels) * Percentile;
                    const uint TotalSamples = DeltaDistribution.size();
                    real TotalDensityAccumulator = _REAL_ZERO_;
                    for (uint i = 0; i < TotalSamples; ++i)
                    {
                        TotalDensityAccumulator += DeltaDistribution[i].m_Density;
                        if (TotalDensityAccumulator >= DensityThreshold)
                        {
                            return DeltaDistribution[i].m_Deviation;
                        }
                    }
                    return _REAL_ZERO_;
                }

                void CMonocularContinuousAdaptiveSmoothing::AdaptiveSmoothing(const real AdaptiveRangeSigma)
                {
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const uint KernelDiameter = m_pSpatialSmoothingKernel->GetWidth();
                    const uint Width = m_OutputActiveZone.GetWidth();
                    const real SimilarityExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(AdaptiveRangeSigma);

#ifdef _SMOOTHING_USE_LOOKUP_TABLE_

                    const real LUTSimilarityExponentFactor = s_ELT.m_SamplesPerUnit * SimilarityExponentFactor;
                    const real MaximalDelta = RealFloor(RealSqrt(s_ELT.m_MinimalArgumentValue / SimilarityExponentFactor));
#endif
                    const real* pBaseKernel = m_pSpatialSmoothingKernel->GetKernelReadOnlyBuffer();
                    const real* pExternalBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferAt(m_pInputActiveZone->GetUpperLeftLocation());
                    const real* pBaseCenterInputPixel = m_pInternalInputImage->GetReadOnlyBufferAt(X0, Y0);
                    real* pBaseOutputPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                    real* pBaseSimilaratixPixel = m_pSimilarityImage->GetWritableBufferAt(X0, Y0);

                    for (coordinate y = Y0; y < Y1; ++y, pExternalBaseInputPixel += Width, pBaseCenterInputPixel += Width, pBaseOutputPixel += Width, pBaseSimilaratixPixel += Width)
                    {
                        const real* pBaseInputPixel = pExternalBaseInputPixel;
                        const real* pCenterInputPixel = pBaseCenterInputPixel;
                        real* pOutputPixel = pBaseOutputPixel;
                        real* pSimilarityPixel = pBaseSimilaratixPixel;

                        for (coordinate x = X0; x < X1; ++x, ++pBaseInputPixel, ++pCenterInputPixel)
                        {
                            const real* pKernel = pBaseKernel;
                            const real* pInputPixel = pBaseInputPixel;
                            const real CentralValue = *pCenterInputPixel;
                            real Accumulator = _REAL_ZERO_;
                            real SimilarityAccumulator = _REAL_ZERO_;

#ifdef _SMOOTHING_USE_LOOKUP_TABLE_

                            for (uint i = 0; i < KernelDiameter; ++i, pInputPixel += Width)
                                for (uint j = 0; j < KernelDiameter; ++j, ++pKernel)
                                {
                                    const real Delta = RealAbs(CentralValue - pInputPixel[j]);
                                    if (Delta < MaximalDelta)
                                    {
                                        const real Similarity = s_ELT.m_pExp[RoundNegativeRealToInteger(LUTSimilarityExponentFactor * Delta * Delta)] * *pKernel;
                                        Accumulator += pInputPixel[j] * Similarity;
                                        SimilarityAccumulator += Similarity;
                                    }
                                }

#else
                            for (uint i = 0; i < KernelDiameter; ++i, pInputPixel += Width)
                                for (uint j = 0; j < KernelDiameter; ++j)
                                {
                                    const real Delta = RealAbs(CentralValue - pInputPixel[j]);
                                    const real Similarity = RealExp(Delta * Delta * SimilarityExponentFactor) * *pKernel++;
                                    Accumulator += pInputPixel[j] * Similarity;
                                    SimilarityAccumulator += Similarity;
                                }
#endif
                            *pOutputPixel += Accumulator / SimilarityAccumulator;
                            *pSimilarityPixel++ = SimilarityAccumulator;
                        }
                    }
                }

                void CMonocularContinuousAdaptiveSmoothing::UpdateSaliencyMap()
                {
                    real Maximal = _REAL_ZERO_, Minimal = _REAL_ZERO_;
                    const real Range = GetExtrema(m_pSimilarityImage, Maximal, Minimal);
                    if (Range > _REAL_EPSILON_)
                    {
                        const real Scale = _REAL_ONE_ / Range;
                        const real Offset = Minimal * Scale + _REAL_ONE_;
                        const coordinate X0 = m_OutputActiveZone.GetX0();
                        const coordinate Y0 = m_OutputActiveZone.GetY0();
                        const coordinate X1 = m_OutputActiveZone.GetX1();
                        const coordinate Y1 = m_OutputActiveZone.GetY1();
                        const uint Width = m_OutputActiveZone.GetWidth();
                        real* pBaseSimilarityPixel = m_pSimilarityImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseSimilarityPixel += Width)
                        {
                            real* pSimilarityPixel = pBaseSimilarityPixel;
                            for (coordinate x = X0; x < X1; ++x, ++pSimilarityPixel)
                            {
                                *pSimilarityPixel = Offset - (*pSimilarityPixel * Scale);
                            }
                        }
                    }
                }

                bool CMonocularContinuousAdaptiveSmoothing::UpdateAdaptiveSmoothingSigma()
                {
                    if (m_pSpatialSmoothingKernel)
                    {
                        m_pSpatialSmoothingKernel->SetStandardDeviation(m_Parameters.GetAdaptiveSmoothingSigma());
                    }
                    else
                    {
                        m_pSpatialSmoothingKernel = new Kernels::CConvolutionGaussianKernel2D(m_Parameters.GetAdaptiveSmoothingSigma());
                    }
                    m_pSpatialSmoothingKernel->ScaleMinimal();
                    m_KernelPartialIntegration = _REAL_ZERO_;
                    const coordinate KernelRadius = m_pSpatialSmoothingKernel->GetRadius();
                    const real* pKernel = m_pSpatialSmoothingKernel->GetKernelReadOnlyBuffer();
                    for (coordinate Dy = -KernelRadius; Dy <= KernelRadius; ++Dy)
                        for (coordinate Dx = -KernelRadius; Dx <= KernelRadius; ++Dx, ++pKernel)
                            if (Dx || Dy)
                            {
                                m_KernelPartialIntegration += *pKernel;
                            }
                    return m_OutputActiveZone.UpdateConnection();
                }

                bool CMonocularContinuousAdaptiveSmoothing::DisplayByMode(const TImage<real>* pImage)
                {
                    switch (m_Parameters.GetDisplayMode())
                    {
                        case CParameters::eRoundedIntensity:
                            return DisplayIntensityRounding(pImage);
                        case CParameters::eScaledIntensity:
                            return DisplayScaledIntensity(pImage);
                        case CParameters::eColorMapped:
                        {
                            const Visualization::CTristimulusColorMap* pColorMap = m_Parameters.GetColorMap();
                            return DisplayColorMapped(pImage, pColorMap->GetColorLookupTableScaleFactor(), pColorMap->GetColorLookupTable());
                        }
                    }
                    return false;
                }

                bool CMonocularContinuousAdaptiveSmoothing::DisplayIntensityRounding(const TImage<real>* pImage)
                {
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate Width = m_OutputActiveZone.GetWidth();
                    const real* pBaseOutputPixel = pImage->GetReadOnlyBufferAt(X0, Y0);
                    CDiscreteTristimulusPixel* pBaseDisplayPixel = m_pDisplayImage->GetWritableBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseDisplayPixel += Width)
                    {
                        const real* pOutputPixel = pBaseOutputPixel;
                        CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                        for (coordinate x = X0; x < X1; ++x)
                        {
                            *pDisplayPixel++ = *pOutputPixel++;
                        }
                    }
                    return true;
                }

                bool CMonocularContinuousAdaptiveSmoothing::DisplayScaledIntensity(const TImage<real>* pImage)
                {
                    real Minimal = _REAL_ZERO_, Maximal = _REAL_ZERO_;
                    const real Range = GetExtrema(pImage, Maximal, Minimal);
                    if (Range > _REAL_EPSILON_)
                    {
                        const real Scale = _REAL_255_ / Range;
                        const coordinate X0 = m_OutputActiveZone.GetX0();
                        const coordinate Y0 = m_OutputActiveZone.GetY0();
                        const coordinate X1 = m_OutputActiveZone.GetX1();
                        const coordinate Y1 = m_OutputActiveZone.GetY1();
                        const coordinate Width = m_OutputActiveZone.GetWidth();
                        const real* pBaseOutputPixel = pImage->GetReadOnlyBufferAt(X0, Y0);
                        CDiscreteTristimulusPixel* pBaseDisplayPixel = m_pDisplayImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseDisplayPixel += Width)
                        {
                            const real* pOutputPixel = pBaseOutputPixel;
                            CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                            for (coordinate x = X0; x < X1; ++x)
                            {
                                *pDisplayPixel++ = (*pOutputPixel++ - Minimal) * Scale;
                            }
                        }
                        return true;
                    }
                    return false;
                }

                bool CMonocularContinuousAdaptiveSmoothing::DisplayColorMapped(const TImage<real>* pImage, const real LUTScale, const CDiscreteTristimulusPixel* pLUT)
                {
                    real Minimal = _REAL_ZERO_, Maximal = _REAL_ZERO_;
                    const real Range = GetExtrema(pImage, Maximal, Minimal);
                    if (Range > _REAL_EPSILON_)
                    {
                        const real Scale = LUTScale / Range;
                        const coordinate X0 = m_OutputActiveZone.GetX0();
                        const coordinate Y0 = m_OutputActiveZone.GetY0();
                        const coordinate X1 = m_OutputActiveZone.GetX1();
                        const coordinate Y1 = m_OutputActiveZone.GetY1();
                        const coordinate Width = m_OutputActiveZone.GetWidth();
                        const real* pBaseOutputPixel = pImage->GetReadOnlyBufferAt(X0, Y0);
                        CDiscreteTristimulusPixel* pBaseDisplayPixel = m_pDisplayImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseDisplayPixel += Width)
                        {
                            const real* pOutputPixel = pBaseOutputPixel;
                            CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                            for (coordinate x = X0; x < X1; ++x)
                            {
                                *pDisplayPixel++ = pLUT[RoundPositiveRealToInteger((*pOutputPixel++ - Minimal) * Scale)];
                            }
                        }
                        return true;
                    }
                    return false;
                }

                real CMonocularContinuousAdaptiveSmoothing::GetExtrema(const TImage<real>* pImage, real& Maximal, real& Minimal) const
                {
                    const coordinate X0 = m_OutputActiveZone.GetX0();
                    const coordinate Y0 = m_OutputActiveZone.GetY0();
                    const coordinate X1 = m_OutputActiveZone.GetX1();
                    const coordinate Y1 = m_OutputActiveZone.GetY1();
                    const coordinate Width = m_OutputActiveZone.GetWidth();
                    const real* pBaseOutputPixel = pImage->GetReadOnlyBufferAt(X0, Y0);
                    Minimal = Maximal = *pBaseOutputPixel;
                    for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width)
                    {
                        const real* pOutputPixel = pBaseOutputPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pOutputPixel)
                            if (*pOutputPixel > Maximal)
                            {
                                Maximal = *pOutputPixel;
                            }
                            else if (*pOutputPixel < Minimal)
                            {
                                Minimal = *pOutputPixel;
                            }
                    }
                    return Maximal - Minimal;
                }

                bool CMonocularContinuousAdaptiveSmoothing::SortDensityDeviation(const DensityDeviation& lhs, const DensityDeviation& rhs)
                {
                    if (lhs.m_Deviation == rhs.m_Deviation)
                    {
                        return lhs.m_Density > rhs.m_Density;
                    }
                    return lhs.m_Deviation < rhs.m_Deviation;
                }
            }
        }
    }
}
