/*
 * MonocularContinuousTristimulusDesaturation.cpp
 *
 *  Created on: 23.10.2011
 *      Author: gonzalez
 */

#include "MonocularContinuousTristimulusDesaturation.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                namespace Desaturation
                {
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //CMonocularContinuousTristimulusDesaturation::CParameters
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    CParametersBase::ParameterChangeResult CMonocularContinuousTristimulusDesaturation::CParameters::SetDesaturationMethod(const DesaturationMethod Method, const bool Wait)
                    {
                        if (!((Method == CParameters::eLightness) || (Method == CParameters::eLuma) || (Method == CParameters::eMean)))
                        {
                            return CParametersBase::eFailure_ParametersOutofScope;
                        }
                        if (Method == m_DesaturationMethod)
                        {
                            return CParametersBase::eNoChange;
                        }
                        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                        {
                            m_DesaturationMethod = Method;
                            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                        }
                        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;

                    }

                    CMonocularContinuousTristimulusDesaturation::CParameters::DesaturationMethod CMonocularContinuousTristimulusDesaturation::CParameters::GetDesaturationMethod() const
                    {
                        return m_DesaturationMethod;
                    }

                    CParametersBase::ParameterChangeResult CMonocularContinuousTristimulusDesaturation::CParameters::SetDisplayMode(const DisplayMode Mode, const bool Wait)
                    {
                        if (!((Mode == CParameters::eRoundedIntensity) || (Mode == CParameters::eScaledIntensity)))
                        {
                            return CParametersBase::eFailure_ParametersOutofScope;
                        }
                        if (Mode == m_DisplayMode)
                        {
                            return CParametersBase::eNoChange;
                        }
                        if (m_pHost->BeginSettingsChangeBlockExecution(Wait))
                        {
                            m_DisplayMode = Mode;
                            return m_pHost->EndSettingsChangeBlockExecution() ? CParametersBase::eSuccess : CParametersBase::eFailure_ProcessesSynchronization;
                        }
                        return Wait ? CParametersBase::eFailure_ProcessesSynchronization : CParametersBase::eRetry_ProcessesRunning;
                    }

                    CMonocularContinuousTristimulusDesaturation::CParameters::DisplayMode CMonocularContinuousTristimulusDesaturation::CParameters::GetDisplayMode() const
                    {
                        return m_DisplayMode;
                    }

                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //CMonocularContinuousTristimulusDesaturation
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    CMonocularContinuousTristimulusDesaturation::CMonocularContinuousTristimulusDesaturation(CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, Threading::CMutex* pInputImageMutex) :
                        TImageProcessBase<CContinuousTristimulusPixel, real, CDiscreteTristimulusPixel>(CImageProcessBase::eContinuousTristimulusDesaturation, pActiveZone, pInputImage, pInputImageMutex), m_Parameters(this)
                    {
                        if (m_IsEnabled)
                        {
                            m_OutputActiveZone.SetConnection(m_pInputActiveZone);
                            const CImageSize& Size = m_OutputActiveZone.GetSize();
                            m_pOutputImage = new TImage<real>(Size);
                            m_pDisplayImage = new TImage<CDiscreteTristimulusPixel>(Size);
                            m_pInternalInputImage = new TImage<CContinuousTristimulusPixel>(Size);
                        }
                    }

                    CMonocularContinuousTristimulusDesaturation::~CMonocularContinuousTristimulusDesaturation()
                    {
                        m_OutputActiveZone.Disconnect(true, true);
                        RELEASE_OBJECT(m_pOutputImage)
                        RELEASE_OBJECT(m_pDisplayImage)
                        RELEASE_OBJECT(m_pInternalInputImage)
                    }

                    bool CMonocularContinuousTristimulusDesaturation::Execute(const Identifier TrialId)
                    {
                        if (m_IsEnabled && m_OutputActiveZone.IsEnabled())
                        {
                            if (StartExecuting(TrialId))
                            {
                                START_PROCESS_EXECUTION_LOG("Execute", TrialId)

                                LoadInputImage();

                                switch (m_Parameters.GetDesaturationMethod())
                                {
                                    case CParameters::eLuma:
                                        LumaDesaturate();
                                        break;
                                    case CParameters::eLightness:
                                        LightnessDesaturate();
                                        break;
                                    case CParameters::eMean:
                                        MeanDesaturate();
                                        break;
                                }

                                STOP_PROCESS_EXECUTION_LOG

                                return FinishExecution();
                            }
                        }
                        return false;
                    }

                    bool CMonocularContinuousTristimulusDesaturation::Display()
                    {
                        if (m_IsEnabled && m_OutputActiveZone.IsEnabled() && GetTotalExecutions())
                        {
                            if (StartDisplaying())
                            {
                                m_pDisplayImage->Clear();
                                switch (m_Parameters.GetDisplayMode())
                                {
                                    case CParameters::eRoundedIntensity:
                                        DisplayIntensityRounding();
                                        break;
                                    case CParameters::eScaledIntensity:
                                        DisplayScaledIntensity();
                                        break;
                                }
                                return FinishDisplaying();
                            }
                        }
                        return false;
                    }

                    CMonocularContinuousTristimulusDesaturation::CParameters* CMonocularContinuousTristimulusDesaturation::GetParameters()
                    {
                        return m_IsEnabled ? &m_Parameters : nullptr;
                    }

                    void CMonocularContinuousTristimulusDesaturation::LumaDesaturate()
                    {
                        const coordinate X0 = m_OutputActiveZone.GetX0();
                        const coordinate Y0 = m_OutputActiveZone.GetY0();
                        const coordinate X1 = m_OutputActiveZone.GetX1();
                        const coordinate Y1 = m_OutputActiveZone.GetY1();
                        const uint Width = m_OutputActiveZone.GetWidth();
                        const CContinuousTristimulusPixel* pBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferAt(X0, Y0);
                        real* pBaseOutputPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseInputPixel += Width, pBaseOutputPixel += Width)
                        {
                            const CContinuousTristimulusPixel* pInputPixel = pBaseInputPixel;
                            real* pOutputPixel = pBaseOutputPixel;
                            for (coordinate x = X0; x < X1; ++x, ++pInputPixel)
                            {
                                *pOutputPixel++ = pInputPixel->GetLumaDesaturatedValue();
                            }
                        }
                    }

                    void CMonocularContinuousTristimulusDesaturation::LightnessDesaturate()
                    {
                        const coordinate X0 = m_OutputActiveZone.GetX0();
                        const coordinate Y0 = m_OutputActiveZone.GetY0();
                        const coordinate X1 = m_OutputActiveZone.GetX1();
                        const coordinate Y1 = m_OutputActiveZone.GetY1();
                        const uint Width = m_OutputActiveZone.GetWidth();
                        const CContinuousTristimulusPixel* pBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferAt(X0, Y0);
                        real* pBaseOutputPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseInputPixel += Width, pBaseOutputPixel += Width)
                        {
                            const CContinuousTristimulusPixel* pInputPixel = pBaseInputPixel;
                            real* pOutputPixel = pBaseOutputPixel;
                            for (coordinate x = X0; x < X1; ++x, ++pInputPixel)
                            {
                                *pOutputPixel++ = pInputPixel->GetLightnessDesaturatedValue();
                            }
                        }
                    }

                    void CMonocularContinuousTristimulusDesaturation::MeanDesaturate()
                    {
                        const coordinate X0 = m_OutputActiveZone.GetX0();
                        const coordinate Y0 = m_OutputActiveZone.GetY0();
                        const coordinate X1 = m_OutputActiveZone.GetX1();
                        const coordinate Y1 = m_OutputActiveZone.GetY1();
                        const uint Width = m_OutputActiveZone.GetWidth();
                        const CContinuousTristimulusPixel* pBaseInputPixel = m_pInternalInputImage->GetReadOnlyBufferAt(X0, Y0);
                        real* pBaseOutputPixel = m_pOutputImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseInputPixel += Width, pBaseOutputPixel += Width)
                        {
                            const CContinuousTristimulusPixel* pInputPixel = pBaseInputPixel;
                            real* pOutputPixel = pBaseOutputPixel;
                            for (coordinate x = X0; x < X1; ++x, ++pInputPixel)
                            {
                                *pOutputPixel++ = pInputPixel->GetMeanDesaturatedValue();
                            }
                        }
                    }

                    void CMonocularContinuousTristimulusDesaturation::DisplayIntensityRounding()
                    {
                        const coordinate X0 = m_OutputActiveZone.GetX0();
                        const coordinate Y0 = m_OutputActiveZone.GetY0();
                        const coordinate X1 = m_OutputActiveZone.GetX1();
                        const coordinate Y1 = m_OutputActiveZone.GetY1();
                        const coordinate Width = m_OutputActiveZone.GetWidth();
                        const real* pBaseOutputPixel = m_pOutputImage->GetReadOnlyBufferAt(X0, Y0);
                        CDiscreteTristimulusPixel* pBaseDisplayPixel = m_pDisplayImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseDisplayPixel += Width)
                        {
                            const real* pOutputPixel = pBaseOutputPixel;
                            CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                            for (coordinate x = X0; x < X1; ++x)
                            {
                                *pDisplayPixel++ = *pOutputPixel++;
                            }
                        }
                    }

                    void CMonocularContinuousTristimulusDesaturation::DisplayScaledIntensity()
                    {
                        real Minimal = _REAL_ZERO_, Maximal = _REAL_ZERO_;
                        GetExtrema(Maximal, Minimal);
                        const real Range = Maximal - Minimal;
                        const real Scale = (Range > _REAL_EPSILON_) ? _REAL_255_ / Range : _REAL_ZERO_;
                        const coordinate X0 = m_OutputActiveZone.GetX0();
                        const coordinate Y0 = m_OutputActiveZone.GetY0();
                        const coordinate X1 = m_OutputActiveZone.GetX1();
                        const coordinate Y1 = m_OutputActiveZone.GetY1();
                        const coordinate Width = m_OutputActiveZone.GetWidth();
                        const real* pBaseOutputPixel = m_pOutputImage->GetReadOnlyBufferAt(X0, Y0);
                        CDiscreteTristimulusPixel* pBaseDisplayPixel = m_pDisplayImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width, pBaseDisplayPixel += Width)
                        {
                            const real* pOutputPixel = pBaseOutputPixel;
                            CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                            for (coordinate x = X0; x < X1; ++x)
                            {
                                *pDisplayPixel++ = (*pOutputPixel++ - Minimal) * Scale;
                            }
                        }
                    }

                    void CMonocularContinuousTristimulusDesaturation::GetExtrema(real& Maximal, real& Minimal) const
                    {
                        const coordinate X0 = m_OutputActiveZone.GetX0();
                        const coordinate Y0 = m_OutputActiveZone.GetY0();
                        const coordinate X1 = m_OutputActiveZone.GetX1();
                        const coordinate Y1 = m_OutputActiveZone.GetY1();
                        const coordinate Width = m_OutputActiveZone.GetWidth();
                        const real* pBaseOutputPixel = m_pOutputImage->GetReadOnlyBufferAt(X0, Y0);
                        Minimal = Maximal = *pBaseOutputPixel;
                        for (coordinate y = Y0; y < Y1; ++y, pBaseOutputPixel += Width)
                        {
                            const real* pOutputPixel = pBaseOutputPixel;
                            for (coordinate x = X0; x < X1; ++x, ++pOutputPixel)
                                if (*pOutputPixel > Maximal)
                                {
                                    Maximal = *pOutputPixel;
                                }
                                else if (*pOutputPixel < Minimal)
                                {
                                    Minimal = *pOutputPixel;
                                }
                        }
                    }
                }
            }
        }
    }
}
