/*
 * MonocularContinuousTristimulusDesaturation.h
 *
 *  Created on: 23.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../Base/ImageProcessBase.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            namespace Monocular
            {
                namespace Desaturation
                {
                    class CMonocularContinuousTristimulusDesaturation: public TImageProcessBase<CContinuousTristimulusPixel, real, CDiscreteTristimulusPixel>
                    {
                    public:

                        class CParameters: public CParametersBase
                        {
                        public:

                            enum DesaturationMethod
                            {
                                eLuma, eLightness, eMean
                            };

                            enum DisplayMode
                            {
                                eRoundedIntensity, eScaledIntensity
                            };

                            CParameters(CMonocularContinuousTristimulusDesaturation* pHost) :
                                CParametersBase(), m_pHost(pHost), m_DesaturationMethod(eLuma), m_DisplayMode(eScaledIntensity)
                            {
                            }

                            CParametersBase::ParameterChangeResult SetDesaturationMethod(const DesaturationMethod Method, const bool Wait = true);
                            DesaturationMethod GetDesaturationMethod() const;

                            CParametersBase::ParameterChangeResult SetDisplayMode(const DisplayMode Mode, const bool Wait = true);
                            DisplayMode GetDisplayMode() const;

                        protected:

                            CMonocularContinuousTristimulusDesaturation* m_pHost;
                            DesaturationMethod m_DesaturationMethod;
                            DisplayMode m_DisplayMode;
                        };

                        CMonocularContinuousTristimulusDesaturation(CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, Threading::CMutex* pInputImageMutex);
                        ~CMonocularContinuousTristimulusDesaturation() override;

                        bool Execute(const Identifier TrialId) override;
                        bool Display() override;
                        CParameters* GetParameters() override;

                    protected:

                        friend class CMonocularContinuousTristimulusDesaturation::CParameters;

                        void LumaDesaturate();
                        void LightnessDesaturate();
                        void MeanDesaturate();

                        void DisplayIntensityRounding();
                        void DisplayScaledIntensity();
                        void GetExtrema(real& Maximal, real& Minimal) const;

                        CParameters m_Parameters;
                    };
                }
            }
        }
    }
}

