/*
 * StereoImageProcess.h
 *
 *  Created on: 22.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"
#include "../../../Foundation/Process/Process.h"
#include "../../../Foundation/Miscellaneous/IdentifiableInstance.h"
#include "../../Common/Image.h"
#include "../../Common/ImageActiveZone.h"

#define _LEFT_ 0
#define _RIGHT_ 1
#define _STEREO_ 2

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            class CStereoImageProcessBase: public EVP::Process::CProcess
            {
            public:

                enum ProcessTypeId
                {
                    eStereoImageDiscreteRGBToContinousDesaturator, eStereoImageContinuousRGBDesaturator, eStereoImageDiscreteRGBSmoother, eStereoImageDiscreteToContinuousRGBSmoother, eStereoImageContinuousRGBSmoother, eStereoImageContinuousSmoother, eStereoImageContinuousAdaptiveBilateralSmoother, eStereoImageContinuousRGBAdaptiveBilateralSmoother, eStereoImageDiscreteToContinousRGBAdaptiveBilateralSmoother, eStereoImageContinuousInterpolator, eStereoImageContinuousRGBInterpolator, eStereoImageDiscreteToContinuousRGBInterpolator, eStereoImageContinuousSubsampler, eStereoImageContinuousRGBSubsampler, eStereoImageHistogramFuser, eStereoImageBayerMeanFuser, eStereoImageFuser, eStereoImageChromaticFuser, eStereoImageGaborEdgeSaliencyExtractor, eStereoImageGaborPhaseRimSaliencyExtractor, eStereoImageChromaticSegmenter, eStereoImageSingleChannelHDRSynthesizer, eStereoImageMultiChannelHDRSynthesizer, eStereoImageBayerMultiChannelHDRSynthesizer, eStereoImageBayerHDRSynthesizer, eStereoImageEdgeSaliencyAnalyzer, eStereoImageRimSaliencyAnalyzer, eStereoImageSingleChannelPixelBasedHDRSynthesizer
                };

                static std::string ProcessTypeIdToString(ProcessTypeId TypeId);

                CStereoImageProcessBase(ProcessTypeId ProcessTypeId) :
                    EVP::Process::CProcess(ProcessTypeIdToString(ProcessTypeId))
                {
                    m_ProcessTypeId = ProcessTypeId;
                }

                inline  ProcessTypeId GetTypeId() const
                {
                    return m_ProcessTypeId;
                }

            protected:

                ProcessTypeId m_ProcessTypeId;
            };

            template<typename InputDataType, typename OutputDataType, typename DisplayDataType> class TStereoImageProcess: public CStereoImageProcessBase
            {
            public:

                TStereoImageProcess(const ProcessTypeId TypeId, const CImageActiveZone* pActiveZone, const TImage<InputDataType>** pInputImages) :
                    CStereoImageProcessBase(TypeId), m_ppInputImages(pInputImages), m_ppOutputImages(NULL), m_ppDisplayImages(NULL)
                {
                    m_IsEnabled = m_ppInputImages && m_ppInputImages[0] && m_ppInputImages[1] && (m_ppInputImages[0] != m_ppInputImages[1]) && (m_ppInputImages[0]->SizeEquals(m_ppInputImages[1]));
                    if (m_IsEnabled)
                    {
                        m_InputActiveZone.Set(pActiveZone, m_ppInputImages[0]->GetSize());
                        m_OutputActiveZone = m_InputActiveZone;
                        m_IsEnabled = m_InputActiveZone.IsEnabled();
                    }
                }

                ~TStereoImageProcess() override
                {
                    if (m_ppOutputImages)
                    {
                        for (uint i = 0; i < _STEREO_; ++i)
                            RELEASE_ARRAY(m_ppOutputImages[i])
                            RELEASE_ARRAY(m_ppOutputImages)
                        }
                    if (m_ppDisplayImages)
                    {
                        for (uint i = 0; i < _STEREO_; ++i)
                            RELEASE_ARRAY(m_ppDisplayImages[i])
                            RELEASE_ARRAY(m_ppDisplayImages)
                        }
                }

                inline  CImageActiveZone* GetInputActiveZone() const
                {
                    return &m_InputActiveZone;
                }

                inline  CImageActiveZone* GetOutputActiveZone() const
                {
                    return &m_OutputActiveZone;
                }

                inline  TImage<InputDataType>** GetInputImages()
                {
                    return m_ppInputImages;
                }

                inline TImage<OutputDataType>** GetOutputImages()
                {
                    return m_ppOutputImages;
                }

                inline  TImage<OutputDataType>** GetOutputImagesReadOnly() const
                {
                    return const_cast<const TImage<OutputDataType>**>(m_ppOutputImages);
                }

                inline TImage<DisplayDataType>** GetDisplayImages()
                {
                    return m_ppDisplayImages;
                }

                inline  TImage<DisplayDataType>** GetDisplayImagesReadOnly() const
                {
                    return const_cast<const TImage<DisplayDataType>**>(m_ppOutputImages);
                }

            protected:

                CImageActiveZone m_InputActiveZone;
                CImageActiveZone m_OutputActiveZone;
                const TImage<InputDataType>** m_ppInputImages;
                TImage<OutputDataType>** m_ppOutputImages;
                TImage<DisplayDataType>** m_ppDisplayImages;
            };
        }
    }
}

