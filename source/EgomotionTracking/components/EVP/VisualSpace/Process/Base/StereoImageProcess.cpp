/*
 * StereoImageProcess.cpp
 *
 *  Created on: 02.05.2011
 *      Author: gonzalez
 */

#include "StereoImageProcess.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            std::string CStereoImageProcessBase::ProcessTypeIdToString(ProcessTypeId TypeId)
            {
                switch (TypeId)
                {
                    case eStereoImageDiscreteRGBToContinousDesaturator:
                        return std::string("StereoImageDiscreteRGBToContinousDesaturator");
                        break;
                    case eStereoImageContinuousRGBDesaturator:
                        return std::string("StereoImageContinuousRGBDesaturator");
                        break;
                    case eStereoImageDiscreteRGBSmoother:
                        return std::string("StereoImageDiscreteRGBSmoother");
                        break;
                    case eStereoImageDiscreteToContinuousRGBSmoother:
                        return std::string("StereoImageDiscreteToContinuousRGBSmoother");
                        break;
                    case eStereoImageContinuousRGBSmoother:
                        return std::string("StereoImageContinuousRGBSmoother");
                        break;
                    case eStereoImageContinuousSmoother:
                        return std::string("StereoImageContinuousSmoother");
                        break;
                    case eStereoImageContinuousAdaptiveBilateralSmoother:
                        return std::string("StereoImageContinuousAdaptiveBilateralSmoother");
                        break;
                    case eStereoImageContinuousRGBAdaptiveBilateralSmoother:
                        return std::string("StereoImageContinuousRGBAdaptiveBilateralSmoother");
                        break;
                    case eStereoImageDiscreteToContinousRGBAdaptiveBilateralSmoother:
                        return std::string("StereoImageDiscreteToContinousRGBAdaptiveBilateralSmoother");
                        break;
                    case eStereoImageContinuousInterpolator:
                        return std::string("StereoImageContinuousInterpolator");
                        break;
                    case eStereoImageContinuousRGBInterpolator:
                        return std::string("StereoImageContinuousRGBInterpolator");
                        break;
                    case eStereoImageDiscreteToContinuousRGBInterpolator:
                        return std::string("StereoImageDiscreteToContinuousRGBInterpolator");
                        break;
                    case eStereoImageContinuousSubsampler:
                        return std::string("StereoImageContinuousSubsampler");
                        break;
                    case eStereoImageContinuousRGBSubsampler:
                        return std::string("StereoImageContinuousRGBSubsampler");
                        break;
                    case eStereoImageHistogramFuser:
                        return std::string("StereoImageHistogramFuser");
                        break;
                    case eStereoImageBayerMeanFuser:
                        return std::string("StereoImageBayerMeanFuser");
                        break;
                    case eStereoImageFuser:
                        return std::string("StereoImageFuser");
                        break;
                    case eStereoImageChromaticFuser:
                        return std::string("StereoImageChromaticFuser");
                        break;
                    case eStereoImageGaborEdgeSaliencyExtractor:
                        return std::string("StereoImageGaborEdgeSaliencyExtractor");
                        break;
                    case eStereoImageGaborPhaseRimSaliencyExtractor:
                        return std::string("StereoImageGaborPhaseRimSaliencyExtractor");
                        break;
                    case eStereoImageChromaticSegmenter:
                        return std::string("StereoImageChromaticSegmenter");
                        break;
                    case eStereoImageSingleChannelHDRSynthesizer:
                        return std::string("StereoImageSingleChannelHDRSynthesizer");
                        break;
                    case eStereoImageMultiChannelHDRSynthesizer:
                        return std::string("StereoImageMultiChannelHDRSynthesizer");
                        break;
                    case eStereoImageBayerMultiChannelHDRSynthesizer:
                        return std::string("StereoImageBayerMultiChannelHDRSynthesizer");
                        break;
                    case eStereoImageBayerHDRSynthesizer:
                        return std::string("StereoImageBayerHDRSynthesizer");
                        break;
                    case eStereoImageEdgeSaliencyAnalyzer:
                        return std::string("StereoImageEdgeSaliencyAnalyzer");
                        break;
                    case eStereoImageRimSaliencyAnalyzer:
                        return std::string("StereoImageRimSaliencyAnalyzer");
                        break;
                    case eStereoImageSingleChannelPixelBasedHDRSynthesizer:
                        return std::string("StereoImageSingleChannelPixelBasedHDRSynthesizer");
                        break;
                }
                return std::string("Unknown");
            }
        }
    }
}
