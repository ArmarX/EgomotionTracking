/*
 * ImageProcessBase.h
 *
 *  Created on: 17.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"
#include "../../../Foundation/Process/Process.h"
#include "../../../Foundation/Miscellaneous/IdentifiableInstance.h"
#include "../../../VisualSpace/Common/ImageExporter.h"
#include "../../../Visualization/Miscellaneous/ColorMap.h"
#include "../../Common/Image.h"
#include "../../Common/ImageActiveZone.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Process
        {
            class CImageProcessBase: public EVP::Process::CProcess
            {
            public:

                enum ProcessTypeId
                {
                    eDiscreteDemosaicing, eContinuousDemosaicing, eDensityFusion, eContinuousTristimulusInterpolation, eContinuousInterpolation, eContinuousTristimulusDesaturation, eContinuousTristimulusAdaptiveSmoothing, eContinuousAdaptiveSmoothing, eContinuousChromaticSegmentation, eGaborEdgeSaliencyExtractor, eGaborRimSaliencyExtractor, eEdgeSaliencyAnalyzer, eLensUndistortion, eGaussianSmoothing
                };

                static  std::string ProcessTypeIdToString(ProcessTypeId TypeId)
                {
                    switch (TypeId)
                    {
                        case eDiscreteDemosaicing:
                            return std::string("Discrete Demosaicing");
                        case eContinuousDemosaicing:
                            return std::string("Continuous Demosaicing");
                        case eDensityFusion:
                            return std::string("Density Fusion");
                        case eContinuousTristimulusInterpolation:
                            return std::string("Continuous Tristimulus Interpolation");
                        case eContinuousInterpolation:
                            return std::string("Continuous Interpolation");
                        case eContinuousTristimulusDesaturation:
                            return std::string("Continuous Tristimulus Desaturation");
                        case eContinuousTristimulusAdaptiveSmoothing:
                            return std::string("Continuous Tristimulus Adaptive Smoothing");
                        case eContinuousAdaptiveSmoothing:
                            return std::string("Continuous Adaptive Smoothing");
                        case eContinuousChromaticSegmentation:
                            return std::string("Continuous Chromatic Segmentation");
                        case eGaborEdgeSaliencyExtractor:
                            return std::string("Gabor Edge Saliency Extractor");
                        case eGaborRimSaliencyExtractor:
                            return std::string("Gabor Rim Saliency Extractor");
                        case eEdgeSaliencyAnalyzer:
                            return std::string("Edge Saliency Analyzer");
                        case eLensUndistortion:
                            return std::string("Lens Undistortion");
                        case eGaussianSmoothing:
                            return std::string("Gaussian Smoothing");
                    }
                    return g_EmptyString;
                }

                CImageProcessBase(ProcessTypeId ProcessTypeId) :
                    EVP::Process::CProcess(ProcessTypeIdToString(ProcessTypeId)), m_ProcessTypeId(ProcessTypeId)
                {
                }

                ~CImageProcessBase() override
                {
                }

                ProcessTypeId GetTypeId() const
                {
                    return m_ProcessTypeId;
                }

            protected:

                ProcessTypeId m_ProcessTypeId;
            };

            class CParametersBase;

            template<typename InputDataType, typename OutputDataType, typename DisplayDataType> class TImageProcessBase: public CImageProcessBase
            {
            public:

                TImageProcessBase(const CImageProcessBase::ProcessTypeId TypeId, CImageActiveZone* pActiveZone, const TImage<InputDataType>* pInputImage, Threading::CMutex* pInputImageMutex) :
                    CImageProcessBase(TypeId), m_pInputActiveZone(pActiveZone), m_OutputActiveZone(), m_pOutputImage(NULL), m_pDisplayImage(NULL), m_pInternalInputImage(NULL), m_pExternalInputImageMutex(pInputImageMutex), m_pExternalInputImage(pInputImage)
                {
                    m_IsEnabled = (m_pExternalInputImage && m_pInputActiveZone && m_pInputActiveZone->IsEnabled() && (m_pExternalInputImage->GetSize() == m_pInputActiveZone->GetSize()));
                }

                ~TImageProcessBase() override
                {
                    RELEASE_OBJECT(m_pInternalInputImage)
                    RELEASE_OBJECT(m_pOutputImage)
                    RELEASE_OBJECT(m_pDisplayImage)
                }

                virtual  bool Display() = 0;

                virtual CParametersBase* GetParameters() = 0;

                CImageActiveZone* GetInputActiveZone()
                {
                    return m_pInputActiveZone;
                }

                CImageActiveZone* GetOutputActiveZone()
                {
                    return &m_OutputActiveZone;
                }

                TImage<InputDataType>* GetInputImage()
                {
                    return m_pExternalInputImage;
                }

                TImage<OutputDataType>* GetOutputImage()
                {
                    return m_pOutputImage;
                }

                const TImage<OutputDataType>* GetOutputImageReadOnly() const
                {
                    return const_cast<const TImage<OutputDataType>*>(m_pOutputImage);
                }

                TImage<DisplayDataType>* GetDisplayImage()
                {
                    return m_pDisplayImage;
                }

                const TImage<DisplayDataType>* GetDisplayImageReadOnly() const
                {
                    return const_cast<const TImage<DisplayDataType>*>(m_pDisplayImage);
                }

                Threading::CMutex* GetInputImageMutex()
                {
                    return m_pExternalInputImageMutex;
                }

            protected:

                void LoadInputImage()
                {
                    if (m_pExternalInputImageMutex)
                    {
                        m_pExternalInputImageMutex->BlockingLock();
                        m_pInternalInputImage->Copy(m_pExternalInputImage);
                        m_pExternalInputImageMutex->Unlock();
                    }
                    else
                    {
                        m_pInternalInputImage->Copy(m_pExternalInputImage);
                    }
                }

                CImageActiveZone* m_pInputActiveZone;
                CImageActiveZone m_OutputActiveZone;
                TImage<OutputDataType>* m_pOutputImage;
                TImage<DisplayDataType>* m_pDisplayImage;
                TImage<InputDataType>* m_pInternalInputImage;

                Threading::CMutex* m_pExternalInputImageMutex;
                const TImage<InputDataType>* m_pExternalInputImage;
            };

            class CParametersBase
            {
            public:

                enum ParameterChangeResult
                {
                    eNoChange, eSuccess, eFailure_ParametersOutofScope, eRetry_ProcessesRunning, eFailure_ProcessesSynchronization, eFailure_ProcessParametersChange
                };

                CParametersBase()
                {

                }

                virtual ~CParametersBase()
                {

                }
            };
        }
    }
}

