/*
 * CameraGeometricCalibration.cpp
 *
 *  Created on: 07.04.2011
 *      Author: gonzalez
 */

#include "CameraGeometricCalibration.h"

using namespace EVP::Mathematics::_2D;
using namespace EVP::Mathematics::_3D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace GeometricCalibration
            {
                CCameraGeometricCalibration::CCameraGeometricCalibration() :
                    m_FocalLengthX(_REAL_ZERO_), m_FocalLengthY(_REAL_ZERO_)
                {
                    memset(m_DistortionCoefficients, 0, sizeof(real) * _CAMERA_GEOMETRIC_CALIBRATION_TOTAL_DISTORTION_COEFFICIENTS_);
                }

                CCameraGeometricCalibration::~CCameraGeometricCalibration()
                    = default;

                const CImageSize& CCameraGeometricCalibration::GetImageSize() const
                {
                    return m_ImageSize;
                }

                uint CCameraGeometricCalibration::GetImageWidth() const
                {
                    return m_ImageSize.GetWidth();
                }

                uint CCameraGeometricCalibration::GetImageHeight() const
                {
                    return m_ImageSize.GetHeight();
                }

                real CCameraGeometricCalibration::GetFocalLengthX() const
                {
                    return m_FocalLengthX;
                }

                real CCameraGeometricCalibration::GetFocalLengthY() const
                {
                    return m_FocalLengthY;
                }

                const real* CCameraGeometricCalibration::GetDistortionCoefficients() const
                {
                    return m_DistortionCoefficients;
                }

                const Mathematics::_2D::CVector2D& CCameraGeometricCalibration::GetImagePrincipalPoint() const
                {
                    return m_ImagePrincipalPoint;
                }

                const Mathematics::_3D::CVector3D& CCameraGeometricCalibration::GetTranslation() const
                {
                    return m_Translation;
                }

                const Mathematics::_3D::CMatrix3D& CCameraGeometricCalibration::GetRotation() const
                {
                    return m_Rotation;
                }

                const Mathematics::_3D::CVector3D& CCameraGeometricCalibration::GetTranslationInverse() const
                {
                    return m_TranslationInverse;
                }

                const Mathematics::_3D::CMatrix3D& CCameraGeometricCalibration::GetRotationInverse() const
                {
                    return m_RotationInverse;
                }

                Mathematics::_3D::CMatrix3D CCameraGeometricCalibration::GetCalibrationMatrix() const
                {
                    Mathematics::_3D::CMatrix3D K;
                    K[0][0] = m_FocalLengthX;
                    K[0][2] = m_ImagePrincipalPoint.GetX();
                    K[1][1] = m_FocalLengthY;
                    K[1][2] = m_ImagePrincipalPoint.GetY();
                    K[2][2] = _REAL_ONE_;
                    return K;
                }

                const TImage<real>* CCameraGeometricCalibration::ComputeDistanceToPrincipalPointImage() const
                {
                    if (m_ImageSize.IsValid())
                    {
                        const coordinate ImageWidth = m_ImageSize.GetWidth();
                        const coordinate ImageHeight = m_ImageSize.GetHeight();
                        TImage<real>* pDistanceToPrincipalPointImage = new TImage<real> (m_ImageSize);
                        real* pDistancePixel = pDistanceToPrincipalPointImage->GetBeginWritableBuffer();
                        for (CPixelLocation Location; Location.m_y < ImageHeight; ++Location.m_y)
                            for (Location.m_x = 0; Location.m_x < ImageWidth; ++Location.m_x, ++pDistancePixel)
                            {
                                *pDistancePixel = m_ImagePrincipalPoint.GetDistance(Location);
                            }
                        return pDistanceToPrincipalPointImage;
                    }
                    return nullptr;
                }
            }
        }
    }
}
