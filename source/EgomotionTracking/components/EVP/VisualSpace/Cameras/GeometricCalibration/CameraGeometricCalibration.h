/*
 * CameraGeometricCalibration.h
 *
 *  Created on: 07.04.2011
 *      Author: gonzalez
 */

#pragma once

#define _CAMERA_GEOMETRIC_CALIBRATION_TOTAL_DISTORTION_COEFFICIENTS_ 4

#include "../../../GlobalSettings.h"
#include "../../../Foundation/Mathematics/2D/Vector2D.h"
#include "../../../Foundation/Mathematics/3D/Vector3D.h"
#include "../../../Foundation/Mathematics/3D/Matrix3D.h"
#include "../../Common/Image.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace GeometricCalibration
            {
                class CCameraGeometricCalibration
                {
                public:

                    enum ImageMode
                    {
                        eDistorted, eUndistorted
                    };

                    CCameraGeometricCalibration();
                    virtual ~CCameraGeometricCalibration();

                    virtual  bool MapPointFromSpaceToImage(const Mathematics::_3D::CVector3D& SpacePoint, Mathematics::_2D::CVector2D& ImagePoint, const ImageMode Mode) = 0;
                    virtual  bool MapPointFromImageToSpace(const Mathematics::_2D::CVector2D& ImagePoint, Mathematics::_3D::CVector3D& SpacePoint, const real Depth, const ImageMode Mode) = 0;
                    virtual  real GetDistanceToPrincipalPoint(const Mathematics::_2D::CVector2D& ImagePoint, const ImageMode Mode) const = 0;

                    const CImageSize& GetImageSize() const;
                    uint GetImageWidth() const;
                    uint GetImageHeight() const;
                    real GetFocalLengthX() const;
                    real GetFocalLengthY() const;
                    const real* GetDistortionCoefficients() const;
                    const Mathematics::_2D::CVector2D& GetImagePrincipalPoint() const;
                    const Mathematics::_3D::CVector3D& GetTranslation() const;
                    const Mathematics::_3D::CMatrix3D& GetRotation() const;
                    const Mathematics::_3D::CVector3D& GetTranslationInverse() const;
                    const Mathematics::_3D::CMatrix3D& GetRotationInverse() const;
                    Mathematics::_3D::CMatrix3D GetCalibrationMatrix() const;
                    const TImage<real>* ComputeDistanceToPrincipalPointImage() const;

                protected:

                    CImageSize m_ImageSize;
                    real m_FocalLengthX;
                    real m_FocalLengthY;
                    real m_DistortionCoefficients[_CAMERA_GEOMETRIC_CALIBRATION_TOTAL_DISTORTION_COEFFICIENTS_];
                    Mathematics::_2D::CVector2D m_ImagePrincipalPoint;
                    Mathematics::_3D::CVector3D m_Translation;
                    Mathematics::_3D::CMatrix3D m_Rotation;
                    Mathematics::_3D::CVector3D m_TranslationInverse;
                    Mathematics::_3D::CMatrix3D m_RotationInverse;
                };
            }
        }
    }
}

