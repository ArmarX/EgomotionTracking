/*
 * MappingPixel.cpp
 *
 *  Created on: 01.03.2012
 *      Author: gonzalez
 */

#include "MappingPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace GeometricCalibration
            {
                CMappingPixel::CMappingPixel() :
                    m_pMapping(nullptr)
                {
                }

                CMappingPixel::~CMappingPixel()
                {
                    RELEASE_OBJECT(m_pMapping)
                }

                void CMappingPixel::SetContinousLocation(const Mathematics::_2D::CVector2D& ContinousLocation, const CImageActiveZone& ActiveZone)
                {
                    const CPixelLocation InterpolationBase(DownToInteger(ContinousLocation.GetX()) - 1, DownToInteger(ContinousLocation.GetY()) - 1);
                    if (ActiveZone.IsRegionInside(InterpolationBase.m_x, InterpolationBase.m_y, InterpolationBase.m_x + 3, InterpolationBase.m_y + 3, false))
                    {
                        if (!m_pMapping)
                        {
                            m_pMapping = new Mapping;
                        }
                        m_pMapping->m_InterpolationSource = ContinousLocation.GetRoundedLocation();
                        m_pMapping->m_InterpolationBase = InterpolationBase;
                        const real Dx = real(DownToInteger(ContinousLocation.GetX())) - ContinousLocation.GetX();
                        const real Dy = ContinousLocation.GetY() - real(DownToInteger(ContinousLocation.GetY()));
                        const real Kmy[4] = { Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dy + _REAL_ONE_), Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dy), Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dy - _REAL_ONE_), Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dy - _REAL_TWO_) };
                        const real Kmx[4] = { Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dx - _REAL_ONE_), Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dx), Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dx + _REAL_ONE_), Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(Dx + _REAL_TWO_) };
                        real Accumulator = _REAL_ZERO_;
                        for (uint i = 0; i < 4; ++i)
                            for (uint j = 0; j < 4; ++j)
                            {
                                const real Wij = Kmy[i] * Kmx[j];
                                m_pMapping->m_InterpolationContribution.m_Wij[i][j] = Wij;
                                Accumulator += Wij;
                            }
                        for (uint i = 0; i < 16; ++i)
                        {
                            m_pMapping->m_InterpolationContribution.m_W[i] /= Accumulator;
                        }
                    }
                }

                bool CMappingPixel::IsInsideSourceImage() const
                {
                    return m_pMapping;
                }

                const CPixelLocation& CMappingPixel::GetDiscreteBicubicInterpolationBaseLocation() const
                {
                    return m_pMapping->m_InterpolationBase;
                }

                const CPixelLocation& CMappingPixel::GetDiscreteNonInterpolationLocation() const
                {
                    return m_pMapping->m_InterpolationSource;
                }

                const real* CMappingPixel::GetInterpolationContribution() const
                {
                    return m_pMapping->m_InterpolationContribution.m_W;
                }
            }
        }
    }
}
