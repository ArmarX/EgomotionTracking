/*
 * StereoCameraLensUndistorter.h
 *
 *  Created on: 19.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "CameraGeometricCalibration.h"
#include "StereoCameraGeometricCalibration.h"

#include "../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../Common/Image.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace GeometricCalibration
            {
                class CStereoCameraLensUndistorter
                {
                public:

                    CStereoCameraLensUndistorter();
                    virtual ~CStereoCameraLensUndistorter();

                    virtual bool LoadFromStereoCalibration(CStereoCameraGeometricCalibration* pStereoCameraGeometricCalibration) = 0;
                    virtual bool LoadFromFile(const_string pPathFileName) = 0;
                    virtual bool Undistort(TImage<byte>* pInputDiscreteIntensityImages[2], TImage<byte>* pOutputDiscreteIntensityImages[2]) = 0;
                    virtual bool Undistort(TImage<real>* pInputContinuousIntensityImages[2], TImage<real>* pOutputContinuousIntensityImages[2]) = 0;
                    virtual bool Undistort(TImage<CDiscreteTristimulusPixel>* pInputDiscreteRGBImages[2], TImage<CDiscreteTristimulusPixel>* pOutputDiscreteRGBImages[2]) = 0;
                    virtual bool Undistort(TImage<CContinuousTristimulusPixel>* pInputContinuousRGBImages[2], TImage<CContinuousTristimulusPixel>* pOutputContinuousRGBImages[2]) = 0;

                    const CStereoCameraGeometricCalibration* GetStereoCameraGeometricCalibration();
                    const CCameraGeometricCalibration* GetLeftCameraGeometricCalibration();
                    const CCameraGeometricCalibration* GetRightCameraGeometricCalibration();

                protected:

                    CStereoCameraGeometricCalibration* m_pStereoCameraGeometricCalibration;
                    CCameraGeometricCalibration* m_pLeftCameraGeometricCalibration;
                    CCameraGeometricCalibration* m_pRightCameraGeometricCalibration;
                };
            }
        }
    }
}

