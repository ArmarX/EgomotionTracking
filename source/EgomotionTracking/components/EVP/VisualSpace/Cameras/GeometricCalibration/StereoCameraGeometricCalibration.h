/*
 * StereoCameraGeometricCalibration.h
 *
 *  Created on: 11.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"
#include "../../../Foundation/Mathematics/2D/Vector2D.h"
#include "../../../Foundation/Mathematics/3D/Vector3D.h"
#include "../../../Foundation/Mathematics/3D/Matrix3D.h"
#include "../../Common/Image.h"
#include "CameraGeometricCalibration.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace GeometricCalibration
            {
                class CStereoCameraGeometricCalibration
                {
                public:

                    CStereoCameraGeometricCalibration();
                    virtual ~CStereoCameraGeometricCalibration();

                    virtual  bool LoadFromFile(const_string pPathFileName) = 0;
                    virtual  bool Calculate3DPoint(const Mathematics::_2D::CVector2D& LeftImagePoint, const Mathematics::_2D::CVector2D& RightImagePoint, Mathematics::_3D::CVector3D& SpacePoint, Mathematics::_3D::CVector3D& LeftOptimizationSpacePoint, Mathematics::_3D::CVector3D& RightOptimizationSpacePoint, const CCameraGeometricCalibration::ImageMode ImageMode) = 0;
                    virtual  bool CalculateEpipolarLineInLeftImage(const Mathematics::_2D::CVector2D& RightImagePoint, Mathematics::_2D::CVector2D& EpipolarLinePointA, Mathematics::_2D::CVector2D& EpipolarLinePointB) = 0;
                    virtual  bool CalculateEpipolarLineInRightImage(const Mathematics::_2D::CVector2D& LeftImagePoint, Mathematics::_2D::CVector2D& EpipolarLinePointA, Mathematics::_2D::CVector2D& EpipolarLinePointB) = 0;

                    virtual  bool CalculateEpipolarLineSegmentInLeftImage(const Mathematics::_2D::CVector2D& RightImagePoint, const real MinimalDistance, const real MaximalDistance, Mathematics::_2D::CVector2D& MinimalDistanceEpipolarLinePoint, Mathematics::_2D::CVector2D& MaximalDistanceEpipolarLinePoint) = 0;
                    virtual  bool CalculateEpipolarLineSegmentInRightImage(const Mathematics::_2D::CVector2D& LeftImagePoint, const real MinimalDistance, const real MaximalDistance, Mathematics::_2D::CVector2D& MinimalDistanceEpipolarLinePoint, Mathematics::_2D::CVector2D& MaximalDistanceEpipolarLinePoint) = 0;

                    const CCameraGeometricCalibration* GetLeftCameraGeometricCalibration() const;
                    const CCameraGeometricCalibration* GetRightCameraGeometricCalibration() const;

                protected:

                    CCameraGeometricCalibration* m_pLeftCameraGeometricCalibration;
                    CCameraGeometricCalibration* m_pRightCameraGeometricCalibration;
                };
            }
        }
    }
}

