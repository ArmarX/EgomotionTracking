/*
 * CameraLensUndistorter.h
 *
 *  Created on: 09.01.2012
 *      Author: gonzalez
 */

#pragma once

#include "CameraGeometricCalibration.h"

#include "../../../Foundation/Miscellaneous/IdentifiableInstance.h"
#include "../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../../Foundation/DataTypes/PixelLocation.h"
#include "../../../Foundation/Mathematics/2D/Vector2D.h"
#include "../../../Foundation/Mathematics/1D/BicubicLookUpTable.h"
#include "../../../Foundation/Time/TimeLogger.h"
#include "../../Common/Image.h"
#include "../../Common/ImageActiveZone.h"
#include "../../Common/ContinuousBoundingBox2D.h"
#include "MappingPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace GeometricCalibration
            {
                class CCameraLensUndistorter: public CIdentifiableInstance
                {
                    IDENTIFIABLE

                public:

                    enum UndistortionMode
                    {
                        eFullImage, eMaximalRectangularImageRegion, eClippingImageRegion
                    };

                    CCameraLensUndistorter();
                    ~CCameraLensUndistorter() override;

                    bool LoadFromCalibration(const CCameraGeometricCalibration* pCameraGeometricCalibration, CImageActiveZone* pInputActiveZone);

                    // Full Image
                    bool FullImageUndistort(const TImage<byte>* pInputImage, TImage<byte>* pOutputImage) const;
                    bool FullImageUndistort(const TImage<real>* pInputImage, TImage<real>* pOutputImage) const;
                    bool FullImageUndistort(const TImage<CDiscreteTristimulusPixel>* pInputImage, TImage<CDiscreteTristimulusPixel>* pOutputImage) const;
                    bool FullImageUndistort(const TImage<CDiscreteTristimulusPixel>* pInputImage, TImage<CContinuousTristimulusPixel>* pOutputImage) const;
                    bool FullImageUndistort(const TImage<CContinuousTristimulusPixel>* pInputImage, TImage<CContinuousTristimulusPixel>* pOutputImage) const;

                    // Maximal Rectangular Image Region
                    bool MaximalRectangularImageRegionUndistort(const TImage<byte>* pInputImage, TImage<byte>* pOutputImage) const;
                    bool MaximalRectangularImageRegionUndistort(const TImage<real>* pInputImage, TImage<real>* pOutputImage) const;
                    bool MaximalRectangularImageRegionUndistort(const TImage<CDiscreteTristimulusPixel>* pInputImage, TImage<CDiscreteTristimulusPixel>* pOutputImage) const;
                    bool MaximalRectangularImageRegionUndistort(const TImage<CDiscreteTristimulusPixel>* pInputImage, TImage<CContinuousTristimulusPixel>* pOutputImage) const;
                    bool MaximalRectangularImageRegionUndistort(const TImage<CContinuousTristimulusPixel>* pInputImage, TImage<CContinuousTristimulusPixel>* pOutputImage) const;

                    // Clipping Image Region
                    bool ClippingImageRegionUndistort(const TImage<byte>* pInputImage, TImage<byte>* pOutputImage) const;
                    bool ClippingImageRegionUndistort(const TImage<real>* pInputImage, TImage<real>* pOutputImage) const;
                    bool ClippingImageRegionUndistort(const TImage<CDiscreteTristimulusPixel>* pInputImage, TImage<CDiscreteTristimulusPixel>* pOutputImage) const;
                    bool ClippingImageRegionUndistort(const TImage<CDiscreteTristimulusPixel>* pInputImage, TImage<CContinuousTristimulusPixel>* pOutputImage) const;
                    bool ClippingImageRegionUndistort(const TImage<CContinuousTristimulusPixel>* pInputImage, TImage<CContinuousTristimulusPixel>* pOutputImage) const;

                    const real* GetDistortionCoefficients() const;
                    const Mathematics::_2D::CVector2D& GetImagePrincipalPointInSourcecoordinates() const;
                    Mathematics::_2D::CVector2D GetImagePrincipalPointInFullcoordinates() const;
                    CImageActiveZone* GetWritableActiveZoneByMode(const UndistortionMode Mode);
                    const CImageActiveZone* GetReadOnlyActiveZoneByMode(const UndistortionMode Mode) const;
                    real GetActiveAreaRatioByMode(const UndistortionMode ModeA, const UndistortionMode ModeB) const;
                    const TImage<bool>* GetFullUndistortedImageMask(const uint Margin) const;

                    CImageActiveZone* GetInterpolationActiveZone();

                    const Mathematics::_2D::CVector2D& GetReferenceUpperLeftCorner() const;

                protected:

                    Mathematics::_2D::CVector2D DetermineDistortedContinousLocation(const Mathematics::_2D::CVector2D& UndistortedContinousLocation) const;
                    Mathematics::_2D::CVector2D DetermineUndistortedContinousLocation(const Mathematics::_2D::CVector2D& DistortedContinousLocation, real* pDistanceToPrinicpalPoint = NULL) const;
                    void InitializeFullZone(const CImageActiveZone& MaximalActiveZone);
                    void InitializeMaximalRectangularZone(const CImageActiveZone& MaximalActiveZone, CImageActiveZone& MaximalRectangularUndistortedImageActiveZone);
                    void InitializeClippingZone(const CImageActiveZone& MaximalActiveZone, CImageActiveZone& ClippingUndistortedImageActiveZone);
                    void InitializeMappingImage(const CImageActiveZone& MaximalActiveZone);
                    bool Initialize();
                    template<typename InputPixelType, typename OutputPixelType> bool IsValid(const TImage<InputPixelType>* pInputImage, TImage<OutputPixelType>* pOutputImage, const CImageActiveZone* pActiveZone) const;

                    bool m_IsInitialized;
                    real m_FocalLengthX;
                    real m_FocalLengthY;
                    Mathematics::_2D::CVector2D m_ImagePrincipalPoint;
                    real m_DistortionCoefficients[_CAMERA_GEOMETRIC_CALIBRATION_TOTAL_DISTORTION_COEFFICIENTS_];
                    TImage<CMappingPixel>* m_pMappingImage;
                    CImageActiveZone* m_pInputActiveZone;
                    CImageActiveZone m_InterpolationActiveZone;
                    CImageActiveZone m_FullImageActiveZone;
                    CImageActiveZone m_MaximalRectangularImageRegionActiveZone;
                    CImageActiveZone m_ClippingImageRegionActiveZone;
                    CContinuousBoundingBox2D m_FullImageBoundingBox;
                };
            }
        }
    }
}

