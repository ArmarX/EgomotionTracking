/*
 * CameraLensUndistorter.cpp
 *
 *  Created on: 09.01.2012
 *      Author: gonzalez
 */

#include "CameraLensUndistorter.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace GeometricCalibration
            {
                IDENTIFIABLE_INITIALIZATION(CCameraLensUndistorter)

                CCameraLensUndistorter::CCameraLensUndistorter() :
                    IDENTIFIABLE_CONTRUCTOR(CCameraLensUndistorter), m_IsInitialized(false), m_FocalLengthX(_REAL_ZERO_), m_FocalLengthY(_REAL_ZERO_), m_ImagePrincipalPoint(), m_pMappingImage(nullptr), m_pInputActiveZone(nullptr)
                {
                }

                CCameraLensUndistorter::~CCameraLensUndistorter()
                {
                    RELEASE_OBJECT(m_pMappingImage)
                }

                bool CCameraLensUndistorter::LoadFromCalibration(const CCameraGeometricCalibration* pCameraGeometricCalibration, CImageActiveZone* pInputActiveZone)
                {
                    m_IsInitialized = false;
                    if (pCameraGeometricCalibration && pInputActiveZone && pInputActiveZone->IsEnabled() && (pCameraGeometricCalibration->GetImageSize() == pInputActiveZone->GetSize()))
                    {
                        m_pInputActiveZone = pInputActiveZone;
                        m_InterpolationActiveZone.SetConnection(m_pInputActiveZone, 2);
                        m_ImagePrincipalPoint = pCameraGeometricCalibration->GetImagePrincipalPoint();
                        m_FocalLengthX = pCameraGeometricCalibration->GetFocalLengthX();
                        m_FocalLengthY = pCameraGeometricCalibration->GetFocalLengthY();
                        memcpy(m_DistortionCoefficients, pCameraGeometricCalibration->GetDistortionCoefficients(), sizeof(real) * _CAMERA_GEOMETRIC_CALIBRATION_TOTAL_DISTORTION_COEFFICIENTS_);
                        m_IsInitialized = Initialize();
                    }
                    return m_IsInitialized;
                }

                bool CCameraLensUndistorter::FullImageUndistort(const TImage<byte>* pInputImage, TImage<byte>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_FullImageActiveZone))
                    {
                        TIME_LOGGER_BEGIN("FullImageUndistort")

                        pOutputImage->Clear();
                        const uint Width = m_FullImageActiveZone.GetWidth();
                        const uint Height = m_FullImageActiveZone.GetHeight();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_FullImageActiveZone.GetUpperLeftLocation());
                        byte* pOutputPixel = pOutputImage->GetWritableBufferAt(m_FullImageActiveZone.GetUpperLeftLocation());
                        for (uint i = 0; i < Height; ++i)
                            for (uint j = 0; j < Width; ++j, ++pMappingPixel, ++pOutputPixel)
                                if (pMappingPixel->IsInsideSourceImage())
                                {
                                    const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                    const byte* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                    real Accumulator = _REAL_ZERO_;
                                    for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                    {
                                        Accumulator += real(*pInputPixel++) * *pInterpolationContributions++;
                                        Accumulator += real(*pInputPixel++) * *pInterpolationContributions++;
                                        Accumulator += real(*pInputPixel++) * *pInterpolationContributions++;
                                        Accumulator += real(*pInputPixel++) * *pInterpolationContributions++;
                                    }
                                    *pOutputPixel = SafeRoundToByte(Accumulator);
                                }

                        TIME_LOGGER_END("FullImageUndistort")

                        return true;
                    }
                    return false;
                }

                bool CCameraLensUndistorter::FullImageUndistort(const TImage<real>* pInputImage, TImage<real>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_FullImageActiveZone))
                    {
                        TIME_LOGGER_BEGIN("FullImageUndistort")

                        pOutputImage->Clear();
                        const uint Width = m_FullImageActiveZone.GetWidth();
                        const uint Height = m_FullImageActiveZone.GetHeight();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_FullImageActiveZone.GetUpperLeftLocation());
                        real* pOutputPixel = pOutputImage->GetWritableBufferAt(m_FullImageActiveZone.GetUpperLeftLocation());
                        for (uint i = 0; i < Height; ++i)
                            for (uint j = 0; j < Width; ++j, ++pMappingPixel, ++pOutputPixel)
                                if (pMappingPixel->IsInsideSourceImage())
                                {
                                    const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                    const real* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                    real Accumulator = _REAL_ZERO_;
                                    for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                    {
                                        Accumulator += *pInputPixel++ * *pInterpolationContributions++;
                                        Accumulator += *pInputPixel++ * *pInterpolationContributions++;
                                        Accumulator += *pInputPixel++ * *pInterpolationContributions++;
                                        Accumulator += *pInputPixel++ * *pInterpolationContributions++;
                                    }
                                    *pOutputPixel = Accumulator;
                                }

                        TIME_LOGGER_END("FullImageUndistort")

                        return true;
                    }
                    return false;
                }

                bool CCameraLensUndistorter::FullImageUndistort(const TImage<CDiscreteTristimulusPixel>* pInputImage, TImage<CDiscreteTristimulusPixel>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_FullImageActiveZone))
                    {
                        TIME_LOGGER_BEGIN("FullImageUndistort")

                        pOutputImage->Clear();
                        const uint Width = m_FullImageActiveZone.GetWidth();
                        const uint Height = m_FullImageActiveZone.GetHeight();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_FullImageActiveZone.GetUpperLeftLocation());
                        CDiscreteTristimulusPixel* pOutputPixel = pOutputImage->GetWritableBufferAt(m_FullImageActiveZone.GetUpperLeftLocation());
                        for (uint i = 0; i < Height; ++i)
                            for (uint j = 0; j < Width; ++j, ++pMappingPixel, ++pOutputPixel)
                                if (pMappingPixel->IsInsideSourceImage())
                                {
                                    const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                    const CDiscreteTristimulusPixel* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                    CContinuousTristimulusPixel Accumulator;
                                    for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                    {
                                        Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                        Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                        Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                        Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    }
                                    *pOutputPixel = Accumulator;
                                }

                        TIME_LOGGER_END("FullImageUndistort")

                        return true;
                    }
                    return false;
                }

                bool CCameraLensUndistorter::FullImageUndistort(const TImage<CDiscreteTristimulusPixel>* pInputImage, TImage<CContinuousTristimulusPixel>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_FullImageActiveZone))
                    {
                        TIME_LOGGER_BEGIN("FullImageUndistort")

                        pOutputImage->Clear();
                        const uint Width = m_FullImageActiveZone.GetWidth();
                        const uint Height = m_FullImageActiveZone.GetHeight();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_FullImageActiveZone.GetUpperLeftLocation());
                        CContinuousTristimulusPixel* pOutputPixel = pOutputImage->GetWritableBufferAt(m_FullImageActiveZone.GetUpperLeftLocation());
                        for (uint i = 0; i < Height; ++i)
                            for (uint j = 0; j < Width; ++j, ++pMappingPixel, ++pOutputPixel)
                                if (pMappingPixel->IsInsideSourceImage())
                                {
                                    const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                    const CDiscreteTristimulusPixel* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                    CContinuousTristimulusPixel Accumulator;
                                    for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                    {
                                        Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                        Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                        Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                        Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    }
                                    *pOutputPixel = Accumulator;
                                }

                        TIME_LOGGER_END("FullImageUndistort")

                        return true;
                    }
                    return false;
                }

                bool CCameraLensUndistorter::FullImageUndistort(const TImage<CContinuousTristimulusPixel>* pInputImage, TImage<CContinuousTristimulusPixel>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_FullImageActiveZone))
                    {
                        TIME_LOGGER_BEGIN("FullImageUndistort")

                        pOutputImage->Clear();
                        const uint Width = m_FullImageActiveZone.GetWidth();
                        const uint Height = m_FullImageActiveZone.GetHeight();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_FullImageActiveZone.GetUpperLeftLocation());
                        CContinuousTristimulusPixel* pOutputPixel = pOutputImage->GetWritableBufferAt(m_FullImageActiveZone.GetUpperLeftLocation());
                        for (uint i = 0; i < Height; ++i)
                            for (uint j = 0; j < Width; ++j, ++pMappingPixel, ++pOutputPixel)
                                if (pMappingPixel->IsInsideSourceImage())
                                {
                                    const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                    const CContinuousTristimulusPixel* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                    CContinuousTristimulusPixel Accumulator;
                                    for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                    {
                                        Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                        Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                        Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                        Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    }
                                    *pOutputPixel = Accumulator;
                                }

                        TIME_LOGGER_END("FullImageUndistort")

                        return true;
                    }
                    return false;
                }

                bool CCameraLensUndistorter::MaximalRectangularImageRegionUndistort(const TImage<byte>* pInputImage, TImage<byte>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_MaximalRectangularImageRegionActiveZone))
                    {
                        TIME_LOGGER_BEGIN("MaximalRectangularImageRegionUndistort")

                        pOutputImage->Clear();
                        const coordinate X0 = m_MaximalRectangularImageRegionActiveZone.GetX0();
                        const coordinate Y0 = m_MaximalRectangularImageRegionActiveZone.GetY0();
                        const coordinate X1 = m_MaximalRectangularImageRegionActiveZone.GetX1();
                        const coordinate Y1 = m_MaximalRectangularImageRegionActiveZone.GetY1();
                        const uint MappingOffset = m_MaximalRectangularImageRegionActiveZone.GetWidth() - m_MaximalRectangularImageRegionActiveZone.GetActiveWidth();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_MaximalRectangularImageRegionActiveZone.GetUpperLeftLocation());
                        byte* pOutputPixel = pOutputImage->GetBeginWritableBuffer();
                        for (coordinate Y = Y0; Y < Y1; ++Y, pMappingPixel += MappingOffset)
                            for (coordinate X = X0; X < X1; ++X, ++pMappingPixel, ++pOutputPixel)
                            {
                                const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                const byte* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                real Accumulator = _REAL_ZERO_;
                                for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                {
                                    Accumulator += real(*pInputPixel++) * *pInterpolationContributions++;
                                    Accumulator += real(*pInputPixel++) * *pInterpolationContributions++;
                                    Accumulator += real(*pInputPixel++) * *pInterpolationContributions++;
                                    Accumulator += real(*pInputPixel++) * *pInterpolationContributions++;
                                }
                                *pOutputPixel = SafeRoundToByte(Accumulator);
                            }

                        TIME_LOGGER_END("MaximalRectangularImageRegionUndistort")

                        return true;
                    }
                    return false;
                }

                bool CCameraLensUndistorter::MaximalRectangularImageRegionUndistort(const TImage<real>* pInputImage, TImage<real>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_MaximalRectangularImageRegionActiveZone))
                    {
                        TIME_LOGGER_BEGIN("MaximalRectangularImageRegionUndistort")

                        pOutputImage->Clear();
                        const coordinate X0 = m_MaximalRectangularImageRegionActiveZone.GetX0();
                        const coordinate Y0 = m_MaximalRectangularImageRegionActiveZone.GetY0();
                        const coordinate X1 = m_MaximalRectangularImageRegionActiveZone.GetX1();
                        const coordinate Y1 = m_MaximalRectangularImageRegionActiveZone.GetY1();
                        const uint MappingOffset = m_MaximalRectangularImageRegionActiveZone.GetWidth() - m_MaximalRectangularImageRegionActiveZone.GetActiveWidth();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_MaximalRectangularImageRegionActiveZone.GetUpperLeftLocation());
                        real* pOutputPixel = pOutputImage->GetBeginWritableBuffer();
                        for (coordinate Y = Y0; Y < Y1; ++Y, pMappingPixel += MappingOffset)
                            for (coordinate X = X0; X < X1; ++X, ++pMappingPixel, ++pOutputPixel)
                            {
                                const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                const real* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                real Accumulator = _REAL_ZERO_;
                                for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                {
                                    Accumulator += *pInputPixel++ * *pInterpolationContributions++;
                                    Accumulator += *pInputPixel++ * *pInterpolationContributions++;
                                    Accumulator += *pInputPixel++ * *pInterpolationContributions++;
                                    Accumulator += *pInputPixel++ * *pInterpolationContributions++;
                                }
                                *pOutputPixel = Accumulator;
                            }
                        TIME_LOGGER_END("MaximalRectangularImageRegionUndistort")

                        return true;
                    }
                    return false;
                }

                bool CCameraLensUndistorter::MaximalRectangularImageRegionUndistort(const TImage<CDiscreteTristimulusPixel>* pInputImage, TImage<CDiscreteTristimulusPixel>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_MaximalRectangularImageRegionActiveZone))
                    {
                        TIME_LOGGER_BEGIN("MaximalRectangularImageRegionUndistort")

                        pOutputImage->Clear();
                        const coordinate X0 = m_MaximalRectangularImageRegionActiveZone.GetX0();
                        const coordinate Y0 = m_MaximalRectangularImageRegionActiveZone.GetY0();
                        const coordinate X1 = m_MaximalRectangularImageRegionActiveZone.GetX1();
                        const coordinate Y1 = m_MaximalRectangularImageRegionActiveZone.GetY1();
                        const uint MappingOffset = m_MaximalRectangularImageRegionActiveZone.GetWidth() - m_MaximalRectangularImageRegionActiveZone.GetActiveWidth();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_MaximalRectangularImageRegionActiveZone.GetUpperLeftLocation());
                        CDiscreteTristimulusPixel* pOutputPixel = pOutputImage->GetBeginWritableBuffer();
                        for (coordinate Y = Y0; Y < Y1; ++Y, pMappingPixel += MappingOffset)
                            for (coordinate X = X0; X < X1; ++X, ++pMappingPixel, ++pOutputPixel)
                            {
                                const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                const CDiscreteTristimulusPixel* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                CContinuousTristimulusPixel Accumulator;
                                for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                {
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                }
                                *pOutputPixel = Accumulator;
                            }
                        TIME_LOGGER_END("MaximalRectangularImageRegionUndistort")

                        return true;
                    }
                    return false;
                }

                bool CCameraLensUndistorter::MaximalRectangularImageRegionUndistort(const TImage<CDiscreteTristimulusPixel>* pInputImage, TImage<CContinuousTristimulusPixel>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_MaximalRectangularImageRegionActiveZone))
                    {
                        TIME_LOGGER_BEGIN("MaximalRectangularImageRegionUndistort")

                        pOutputImage->Clear();
                        const coordinate X0 = m_MaximalRectangularImageRegionActiveZone.GetX0();
                        const coordinate Y0 = m_MaximalRectangularImageRegionActiveZone.GetY0();
                        const coordinate X1 = m_MaximalRectangularImageRegionActiveZone.GetX1();
                        const coordinate Y1 = m_MaximalRectangularImageRegionActiveZone.GetY1();
                        const uint MappingOffset = m_MaximalRectangularImageRegionActiveZone.GetWidth() - m_MaximalRectangularImageRegionActiveZone.GetActiveWidth();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_MaximalRectangularImageRegionActiveZone.GetUpperLeftLocation());
                        CContinuousTristimulusPixel* pOutputPixel = pOutputImage->GetBeginWritableBuffer();
                        for (coordinate Y = Y0; Y < Y1; ++Y, pMappingPixel += MappingOffset)
                            for (coordinate X = X0; X < X1; ++X, ++pMappingPixel, ++pOutputPixel)
                            {
                                const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                const CDiscreteTristimulusPixel* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                CContinuousTristimulusPixel Accumulator;
                                for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                {
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                }
                                *pOutputPixel = Accumulator;
                            }
                        TIME_LOGGER_END("MaximalRectangularImageRegionUndistort")

                        return true;
                    }
                    return false;
                }

                bool CCameraLensUndistorter::MaximalRectangularImageRegionUndistort(const TImage<CContinuousTristimulusPixel>* pInputImage, TImage<CContinuousTristimulusPixel>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_MaximalRectangularImageRegionActiveZone))
                    {
                        TIME_LOGGER_BEGIN("MaximalRectangularImageRegionUndistort")

                        pOutputImage->Clear();
                        const coordinate X0 = m_MaximalRectangularImageRegionActiveZone.GetX0();
                        const coordinate Y0 = m_MaximalRectangularImageRegionActiveZone.GetY0();
                        const coordinate X1 = m_MaximalRectangularImageRegionActiveZone.GetX1();
                        const coordinate Y1 = m_MaximalRectangularImageRegionActiveZone.GetY1();
                        const uint MappingOffset = m_MaximalRectangularImageRegionActiveZone.GetWidth() - m_MaximalRectangularImageRegionActiveZone.GetActiveWidth();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_MaximalRectangularImageRegionActiveZone.GetUpperLeftLocation());
                        CContinuousTristimulusPixel* pOutputPixel = pOutputImage->GetBeginWritableBuffer();
                        for (coordinate Y = Y0; Y < Y1; ++Y, pMappingPixel += MappingOffset)
                            for (coordinate X = X0; X < X1; ++X, ++pMappingPixel, ++pOutputPixel)
                            {
                                const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                const CContinuousTristimulusPixel* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                CContinuousTristimulusPixel Accumulator;
                                for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                {
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                }
                                *pOutputPixel = Accumulator;
                            }
                        TIME_LOGGER_END("MaximalRectangularImageRegionUndistort")

                        return true;
                    }
                    return false;
                }

                bool CCameraLensUndistorter::ClippingImageRegionUndistort(const TImage<byte>* pInputImage, TImage<byte>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_ClippingImageRegionActiveZone))
                    {
                        TIME_LOGGER_BEGIN("ClippingImageRegionUndistort")

                        pOutputImage->Clear();
                        const coordinate X0 = m_ClippingImageRegionActiveZone.GetX0();
                        const coordinate Y0 = m_ClippingImageRegionActiveZone.GetY0();
                        const coordinate X1 = m_ClippingImageRegionActiveZone.GetX1();
                        const coordinate Y1 = m_ClippingImageRegionActiveZone.GetY1();
                        const uint MappingOffset = m_ClippingImageRegionActiveZone.GetWidth() - m_ClippingImageRegionActiveZone.GetActiveWidth();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_ClippingImageRegionActiveZone.GetUpperLeftLocation());
                        byte* pOutputPixel = pOutputImage->GetBeginWritableBuffer();
                        for (coordinate Y = Y0; Y < Y1; ++Y, pMappingPixel += MappingOffset)
                            for (coordinate X = X0; X < X1; ++X, ++pMappingPixel, ++pOutputPixel)
                            {
                                const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                const byte* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                real Accumulator = _REAL_ZERO_;
                                for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                {
                                    Accumulator += real(*pInputPixel++) * *pInterpolationContributions++;
                                    Accumulator += real(*pInputPixel++) * *pInterpolationContributions++;
                                    Accumulator += real(*pInputPixel++) * *pInterpolationContributions++;
                                    Accumulator += real(*pInputPixel++) * *pInterpolationContributions++;
                                }
                                *pOutputPixel = SafeRoundToByte(Accumulator);
                            }

                        TIME_LOGGER_END("ClippingImageRegionUndistort")

                        return true;
                    }
                    return false;
                }

                bool CCameraLensUndistorter::ClippingImageRegionUndistort(const TImage<real>* pInputImage, TImage<real>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_ClippingImageRegionActiveZone))
                    {
                        TIME_LOGGER_BEGIN("ClippingImageRegionUndistort")

                        pOutputImage->Clear();
                        const coordinate X0 = m_ClippingImageRegionActiveZone.GetX0();
                        const coordinate Y0 = m_ClippingImageRegionActiveZone.GetY0();
                        const coordinate X1 = m_ClippingImageRegionActiveZone.GetX1();
                        const coordinate Y1 = m_ClippingImageRegionActiveZone.GetY1();
                        const uint MappingOffset = m_ClippingImageRegionActiveZone.GetWidth() - m_ClippingImageRegionActiveZone.GetActiveWidth();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_ClippingImageRegionActiveZone.GetUpperLeftLocation());
                        real* pOutputPixel = pOutputImage->GetBeginWritableBuffer();
                        for (coordinate Y = Y0; Y < Y1; ++Y, pMappingPixel += MappingOffset)
                            for (coordinate X = X0; X < X1; ++X, ++pMappingPixel, ++pOutputPixel)
                            {
                                const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                const real* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                real Accumulator = _REAL_ZERO_;
                                for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                {
                                    Accumulator += *pInputPixel++ * *pInterpolationContributions++;
                                    Accumulator += *pInputPixel++ * *pInterpolationContributions++;
                                    Accumulator += *pInputPixel++ * *pInterpolationContributions++;
                                    Accumulator += *pInputPixel++ * *pInterpolationContributions++;
                                }
                                *pOutputPixel = Accumulator;
                            }
                        TIME_LOGGER_END("ClippingImageRegionUndistort")

                        return true;
                    }
                    return false;
                }

                bool CCameraLensUndistorter::ClippingImageRegionUndistort(const TImage<CDiscreteTristimulusPixel>* pInputImage, TImage<CDiscreteTristimulusPixel>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_ClippingImageRegionActiveZone))
                    {
                        TIME_LOGGER_BEGIN("ClippingImageRegionUndistort")

                        pOutputImage->Clear();
                        const coordinate X0 = m_ClippingImageRegionActiveZone.GetX0();
                        const coordinate Y0 = m_ClippingImageRegionActiveZone.GetY0();
                        const coordinate X1 = m_ClippingImageRegionActiveZone.GetX1();
                        const coordinate Y1 = m_ClippingImageRegionActiveZone.GetY1();
                        const uint MappingOffset = m_ClippingImageRegionActiveZone.GetWidth() - m_ClippingImageRegionActiveZone.GetActiveWidth();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_ClippingImageRegionActiveZone.GetUpperLeftLocation());
                        CDiscreteTristimulusPixel* pOutputPixel = pOutputImage->GetBeginWritableBuffer();
                        for (coordinate Y = Y0; Y < Y1; ++Y, pMappingPixel += MappingOffset)
                            for (coordinate X = X0; X < X1; ++X, ++pMappingPixel, ++pOutputPixel)
                            {
                                const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                const CDiscreteTristimulusPixel* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                CContinuousTristimulusPixel Accumulator;
                                for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                {
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                }
                                *pOutputPixel = Accumulator;
                            }
                        TIME_LOGGER_END("ClippingImageRegionUndistort")

                        return true;
                    }
                    return false;
                }

                bool CCameraLensUndistorter::ClippingImageRegionUndistort(const TImage<CDiscreteTristimulusPixel>* pInputImage, TImage<CContinuousTristimulusPixel>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_ClippingImageRegionActiveZone))
                    {
                        TIME_LOGGER_BEGIN("ClippingImageRegionUndistort")

                        pOutputImage->Clear();
                        const coordinate X0 = m_ClippingImageRegionActiveZone.GetX0();
                        const coordinate Y0 = m_ClippingImageRegionActiveZone.GetY0();
                        const coordinate X1 = m_ClippingImageRegionActiveZone.GetX1();
                        const coordinate Y1 = m_ClippingImageRegionActiveZone.GetY1();
                        const uint MappingOffset = m_ClippingImageRegionActiveZone.GetWidth() - m_ClippingImageRegionActiveZone.GetActiveWidth();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_ClippingImageRegionActiveZone.GetUpperLeftLocation());
                        CContinuousTristimulusPixel* pOutputPixel = pOutputImage->GetBeginWritableBuffer();
                        for (coordinate Y = Y0; Y < Y1; ++Y, pMappingPixel += MappingOffset)
                            for (coordinate X = X0; X < X1; ++X, ++pMappingPixel, ++pOutputPixel)
                            {
                                const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                const CDiscreteTristimulusPixel* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                CContinuousTristimulusPixel Accumulator;
                                for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                {
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                }
                                *pOutputPixel = Accumulator;
                            }
                        TIME_LOGGER_END("ClippingImageRegionUndistort")

                        return true;
                    }
                    return false;
                }

                bool CCameraLensUndistorter::ClippingImageRegionUndistort(const TImage<CContinuousTristimulusPixel>* pInputImage, TImage<CContinuousTristimulusPixel>* pOutputImage) const
                {
                    if (IsValid(pInputImage, pOutputImage, &m_ClippingImageRegionActiveZone))
                    {
                        TIME_LOGGER_BEGIN("ClippingImageRegionUndistort")

                        pOutputImage->Clear();
                        const coordinate X0 = m_ClippingImageRegionActiveZone.GetX0();
                        const coordinate Y0 = m_ClippingImageRegionActiveZone.GetY0();
                        const coordinate X1 = m_ClippingImageRegionActiveZone.GetX1();
                        const coordinate Y1 = m_ClippingImageRegionActiveZone.GetY1();
                        const uint MappingOffset = m_ClippingImageRegionActiveZone.GetWidth() - m_ClippingImageRegionActiveZone.GetActiveWidth();
                        const uint ConvolutionOffset = pInputImage->GetWidth() - 4;
                        const CMappingPixel* pMappingPixel = m_pMappingImage->GetReadOnlyBufferAt(m_ClippingImageRegionActiveZone.GetUpperLeftLocation());
                        CContinuousTristimulusPixel* pOutputPixel = pOutputImage->GetBeginWritableBuffer();
                        for (coordinate Y = Y0; Y < Y1; ++Y, pMappingPixel += MappingOffset)
                            for (coordinate X = X0; X < X1; ++X, ++pMappingPixel, ++pOutputPixel)
                            {
                                const real* pInterpolationContributions = pMappingPixel->GetInterpolationContribution();
                                const CContinuousTristimulusPixel* pInputPixel = pInputImage->GetReadOnlyBufferAt(pMappingPixel->GetDiscreteBicubicInterpolationBaseLocation());
                                CContinuousTristimulusPixel Accumulator;
                                for (uint i = 0; i < 4; ++i, pInputPixel += ConvolutionOffset)
                                {
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                    Accumulator.AddWeightedValue(*pInputPixel++, *pInterpolationContributions++);
                                }
                                *pOutputPixel = Accumulator;
                            }
                        TIME_LOGGER_END("ClippingImageRegionUndistort")

                        return true;
                    }
                    return false;
                }

                const real* CCameraLensUndistorter::GetDistortionCoefficients() const
                {
                    return m_DistortionCoefficients;
                }

                const Mathematics::_2D::CVector2D& CCameraLensUndistorter::GetImagePrincipalPointInSourcecoordinates() const
                {
                    return m_ImagePrincipalPoint;
                }

                Mathematics::_2D::CVector2D CCameraLensUndistorter::GetImagePrincipalPointInFullcoordinates() const
                {
                    return m_ImagePrincipalPoint - m_FullImageBoundingBox.GetMinPoint();
                }

                real CCameraLensUndistorter::GetActiveAreaRatioByMode(const UndistortionMode ModeA, const UndistortionMode ModeB) const
                {
                    if (m_IsInitialized)
                    {
                        const CImageActiveZone* pActiveZoneA = GetReadOnlyActiveZoneByMode(ModeA);
                        const CImageActiveZone* pActiveZoneB = GetReadOnlyActiveZoneByMode(ModeB);
                        if (pActiveZoneA && pActiveZoneB)
                        {
                            return real(pActiveZoneA->GetActiveArea()) / real(pActiveZoneB->GetActiveArea());
                        }
                    }
                    return _REAL_ZERO_;
                }

                const TImage<bool>* CCameraLensUndistorter::GetFullUndistortedImageMask(const uint Margin) const
                {
                    if (m_IsInitialized)
                    {
                        TImage<bool>* pFullUndistortedImageMask = new TImage<bool> (m_pMappingImage->GetSize());
                        bool* pFullUndistortedImageMaskPixel = pFullUndistortedImageMask->GetBeginWritableBuffer();
                        const CMappingPixel* const pFinalMappingImagePixel = m_pMappingImage->GetEndReadOnlyBuffer();
                        for (const CMappingPixel* pMappingImagePixel = m_pMappingImage->GetBeginReadOnlyBuffer(); pMappingImagePixel < pFinalMappingImagePixel; ++pMappingImagePixel)
                        {
                            *pFullUndistortedImageMaskPixel++ = pMappingImagePixel->IsInsideSourceImage();
                        }
                        if (Margin)
                        {
                            const coordinate Radius = Margin;
                            TImage<bool>* pFullUndistortedImageMaskMargin = new TImage<bool> (m_pMappingImage->GetSize());
                            pFullUndistortedImageMaskMargin->Clear();
                            const coordinate Height = pFullUndistortedImageMask->GetHeight();
                            const coordinate Width = pFullUndistortedImageMask->GetWidth();
                            const coordinate SubHeight = Height - 1;
                            const coordinate SubWidth = Width - 1;
                            const bool* pInPutPixel = pFullUndistortedImageMask->GetBeginWritableBuffer();
                            bool* pOutPutPixel = pFullUndistortedImageMaskMargin->GetBeginWritableBuffer();
                            for (coordinate Y = 0; Y < Height; ++Y)
                                for (coordinate X = 0; X < Width; ++X, ++pOutPutPixel, ++pInPutPixel)
                                    if (*pInPutPixel)
                                    {
                                        bool IsActive = true;
                                        const coordinate XS0 = TMax(X - Radius, 0);
                                        const coordinate YS1 = TMin(Y + Radius, SubHeight);
                                        const coordinate XS1 = TMin(X + Radius, SubWidth);
                                        for (coordinate YS = TMax(Y - Radius, 0); IsActive && (YS <= YS1); ++YS)
                                        {
                                            const coordinate DY = YS - Y;
                                            for (coordinate XS = XS0; XS <= XS1; ++XS)
                                                if ((RealHypotenuse(XS - X, DY) <= Radius) && (!pFullUndistortedImageMask->GetValueAt(XS, YS)))
                                                {
                                                    IsActive = false;
                                                    break;
                                                }
                                        }
                                        *pOutPutPixel = IsActive;
                                    }
                            RELEASE_OBJECT(pFullUndistortedImageMask)
                            return pFullUndistortedImageMaskMargin;
                        }
                        return pFullUndistortedImageMask;
                    }
                    return nullptr;
                }

                CImageActiveZone* CCameraLensUndistorter::GetInterpolationActiveZone()
                {
                    return &m_InterpolationActiveZone;
                }

                const Mathematics::_2D::CVector2D& CCameraLensUndistorter::GetReferenceUpperLeftCorner() const
                {
                    return m_FullImageBoundingBox.GetMinPoint();
                }

                Mathematics::_2D::CVector2D CCameraLensUndistorter::DetermineDistortedContinousLocation(const Mathematics::_2D::CVector2D& UndistortedContinousLocation) const
                {
                    Mathematics::_2D::CVector2D Delta = UndistortedContinousLocation - m_ImagePrincipalPoint;
                    Delta.ScaleAnisotropic(m_FocalLengthX, m_FocalLengthY);
                    const real X = Delta.GetX();
                    const real Y = Delta.GetY();
                    const real SquareRadius = Delta.GetSquareLength();
                    const real RadialDistortion = _REAL_ONE_ + m_DistortionCoefficients[0] * SquareRadius + m_DistortionCoefficients[1] * SquareRadius * SquareRadius;
                    const real TangentialDistortionX = m_DistortionCoefficients[2] * (_REAL_TWO_ * X * Y) + m_DistortionCoefficients[3] * (SquareRadius + _REAL_TWO_ * X * X);
                    const real TangentialDistortionY = m_DistortionCoefficients[2] * (SquareRadius + _REAL_TWO_ * Y * Y) + m_DistortionCoefficients[3] * (_REAL_TWO_ * X * Y);
                    return Mathematics::_2D::CVector2D((m_FocalLengthX * (RadialDistortion * X + TangentialDistortionX)) + m_ImagePrincipalPoint.GetX(), (m_FocalLengthY * (RadialDistortion * Y + TangentialDistortionY)) + m_ImagePrincipalPoint.GetY());
                }

                Mathematics::_2D::CVector2D CCameraLensUndistorter::DetermineUndistortedContinousLocation(const Mathematics::_2D::CVector2D& DistortedContinousLocation, real* pDistanceToPrinicpalPoint) const
                {
                    Mathematics::_2D::CVector2D Delta = DistortedContinousLocation - m_ImagePrincipalPoint;
                    Delta.ScaleAnisotropic(m_FocalLengthX, m_FocalLengthY);
                    const real X = Delta.GetX();
                    const real Y = Delta.GetY();
                    real Xa = X;
                    real Ya = Y;
                    real MaximalIncrement = _REAL_MAX_;
                    do
                    {
                        const real SquareRadius = (Xa * Xa) + (Ya * Ya);
                        const real RadialDistortion = _REAL_ONE_ + m_DistortionCoefficients[0] * SquareRadius + m_DistortionCoefficients[1] * SquareRadius * SquareRadius;
                        const real TangentialDistortionX = m_DistortionCoefficients[2] * (_REAL_TWO_ * Xa * Ya) + m_DistortionCoefficients[3] * (SquareRadius + _REAL_TWO_ * Xa * Xa);
                        const real TangentialDistortionY = m_DistortionCoefficients[2] * (SquareRadius + _REAL_TWO_ * Ya * Ya) + m_DistortionCoefficients[3] * (_REAL_TWO_ * Xa * Ya);
                        const real Xb = (X - TangentialDistortionX) / RadialDistortion;
                        const real Yb = (Y - TangentialDistortionY) / RadialDistortion;
                        MaximalIncrement = TMax(RealAbs(Xb - Xa), RealAbs(Yb - Ya));
                        Xa = Xb;
                        Ya = Yb;
                    }
                    while (MaximalIncrement > _REAL_EPSILON_);
                    if (pDistanceToPrinicpalPoint)
                    {
                        *pDistanceToPrinicpalPoint = RealHypotenuse(Xa * m_FocalLengthX, Ya * m_FocalLengthY);
                    }
                    return Mathematics::_2D::CVector2D((Xa * m_FocalLengthX) + m_ImagePrincipalPoint.GetX(), (Ya * m_FocalLengthY) + m_ImagePrincipalPoint.GetY());
                }

                CImageActiveZone* CCameraLensUndistorter::GetWritableActiveZoneByMode(const UndistortionMode Mode)
                {
                    switch (Mode)
                    {
                        case eFullImage:
                            return &m_FullImageActiveZone;
                        case eMaximalRectangularImageRegion:
                            return &m_MaximalRectangularImageRegionActiveZone;
                        case eClippingImageRegion:
                            return &m_ClippingImageRegionActiveZone;
                    }
                    return nullptr;
                }

                const CImageActiveZone* CCameraLensUndistorter::GetReadOnlyActiveZoneByMode(const UndistortionMode Mode) const
                {
                    switch (Mode)
                    {
                        case eFullImage:
                            return &m_FullImageActiveZone;
                        case eMaximalRectangularImageRegion:
                            return &m_MaximalRectangularImageRegionActiveZone;
                        case eClippingImageRegion:
                            return &m_ClippingImageRegionActiveZone;
                    }
                    return nullptr;
                }

                void CCameraLensUndistorter::InitializeFullZone(const CImageActiveZone& MaximalActiveZone)
                {
                    const coordinate X0 = MaximalActiveZone.GetX0();
                    const coordinate Y0 = MaximalActiveZone.GetY0();
                    const coordinate X1 = MaximalActiveZone.GetX1();
                    const coordinate Y1 = MaximalActiveZone.GetY1();
                    m_FullImageBoundingBox.SetEmpty();
                    Mathematics::_2D::CVector2D UpperEdgePoint(X0, Y0), LowerEdgePoint(X0, Y1 - 1);
                    for (coordinate X = X0; X < X1; ++X)
                    {
                        UpperEdgePoint.SetX(real(X));
                        LowerEdgePoint.SetX(real(X));
                        m_FullImageBoundingBox.Extend(DetermineUndistortedContinousLocation(UpperEdgePoint));
                        m_FullImageBoundingBox.Extend(DetermineUndistortedContinousLocation(LowerEdgePoint));
                    }
                    Mathematics::_2D::CVector2D LeftEdgePoint(X0, Y0), RightEdgePoint(X1 - 1, Y0);
                    for (coordinate Y = Y0; Y < Y1; ++Y)
                    {
                        LeftEdgePoint.SetY(real(Y));
                        RightEdgePoint.SetY(real(Y));
                        m_FullImageBoundingBox.Extend(DetermineUndistortedContinousLocation(LeftEdgePoint));
                        m_FullImageBoundingBox.Extend(DetermineUndistortedContinousLocation(RightEdgePoint));
                    }
                    m_FullImageActiveZone.Set(m_FullImageBoundingBox.GetEffectiveDiscreteSize());
                }

                void CCameraLensUndistorter::InitializeMaximalRectangularZone(const CImageActiveZone& MaximalActiveZone, CImageActiveZone& MaximalRectangularUndistortedImageActiveZone)
                {
                    const coordinate X0 = MaximalActiveZone.GetX0();
                    const coordinate Y0 = MaximalActiveZone.GetY0();
                    const coordinate X1 = MaximalActiveZone.GetX1();
                    const coordinate Y1 = MaximalActiveZone.GetY1();
                    Mathematics::_2D::CVector2D UpperEdgePoint(X0, Y0), LowerEdgePoint(X0, Y1 - 1);
                    CContinuousBoundingBox2D UpperEdgeBoundingBox, LowerEdgeBoundingBox;
                    for (coordinate X = X0; X < X1; ++X)
                    {
                        UpperEdgePoint.SetX(real(X));
                        LowerEdgePoint.SetX(real(X));
                        UpperEdgeBoundingBox.Extend(DetermineUndistortedContinousLocation(UpperEdgePoint));
                        LowerEdgeBoundingBox.Extend(DetermineUndistortedContinousLocation(LowerEdgePoint));
                    }
                    Mathematics::_2D::CVector2D LeftEdgePoint(X0, Y0), RightEdgePoint(X1 - 1, Y0);
                    CContinuousBoundingBox2D LeftEdgeBoundingBox, RightEdgeBoundingBox;
                    for (coordinate Y = Y0; Y < Y1; ++Y)
                    {
                        LeftEdgePoint.SetY(real(Y));
                        RightEdgePoint.SetY(real(Y));
                        LeftEdgeBoundingBox.Extend(DetermineUndistortedContinousLocation(LeftEdgePoint));
                        RightEdgeBoundingBox.Extend(DetermineUndistortedContinousLocation(RightEdgePoint));
                    }
                    const Mathematics::_2D::CVector2D& MinPoint = m_FullImageBoundingBox.GetMinPoint();
                    const Mathematics::_2D::CVector2D BasePoint(RealFloor(MinPoint.GetX()), RealFloor(MinPoint.GetY()));
                    const coordinate SX0 = coordinate(RealCeil(LeftEdgeBoundingBox.GetMaxPoint().GetX()) - BasePoint.GetX() + _REAL_TWO_);
                    const coordinate SY0 = coordinate(RealCeil(UpperEdgeBoundingBox.GetMaxPoint().GetY()) - BasePoint.GetY() + _REAL_TWO_);
                    const coordinate SX1 = coordinate(RealFloor(RightEdgeBoundingBox.GetMinPoint().GetX()) - BasePoint.GetX() - _REAL_TWO_);
                    const coordinate SY1 = coordinate(RealFloor(LowerEdgeBoundingBox.GetMinPoint().GetY()) - BasePoint.GetY() - _REAL_TWO_);
                    MaximalRectangularUndistortedImageActiveZone.Set(m_FullImageActiveZone.GetSize(), SX0, SY0, SX1, SY1);
                }

                void CCameraLensUndistorter::InitializeClippingZone(const CImageActiveZone& MaximalActiveZone, CImageActiveZone& ClippingUndistortedImageActiveZone)
                {
                    const coordinate X0 = MaximalActiveZone.GetX0();
                    const coordinate Y0 = MaximalActiveZone.GetY0();
                    const coordinate X1 = MaximalActiveZone.GetX1();
                    const coordinate Y1 = MaximalActiveZone.GetY1();
                    const Mathematics::_2D::CVector2D& MinPoint = m_FullImageBoundingBox.GetMinPoint();
                    const Mathematics::_2D::CVector2D BasePoint(RealFloor(MinPoint.GetX()), RealFloor(MinPoint.GetY()));
                    const coordinate SX0 = coordinate(X0 - BasePoint.GetX());
                    const coordinate SY0 = coordinate(Y0 - BasePoint.GetY());
                    const coordinate SX1 = coordinate(X1 - BasePoint.GetX());
                    const coordinate SY1 = coordinate(Y1 - BasePoint.GetY());
                    ClippingUndistortedImageActiveZone.Set(m_FullImageActiveZone.GetSize(), SX0, SY0, SX1, SY1);
                }

                void CCameraLensUndistorter::InitializeMappingImage(const CImageActiveZone& MaximalActiveZone)
                {
                    if (m_pMappingImage && (m_pMappingImage->GetSize() != m_FullImageActiveZone.GetSize()))
                        RELEASE_OBJECT(m_pMappingImage)
                        if (!m_pMappingImage)
                        {
                            m_pMappingImage = new TImage<CMappingPixel> (m_FullImageActiveZone.GetSize());
                        }
                    CMappingPixel* pMappingPixel = m_pMappingImage->GetBeginWritableBuffer();
                    const Mathematics::_2D::CVector2D& MinPoint = m_FullImageBoundingBox.GetMinPoint();
                    const Mathematics::_2D::CVector2D BasePoint(RealFloor(MinPoint.GetX()), RealFloor(MinPoint.GetY()));
                    const coordinate Width = m_FullImageActiveZone.GetWidth();
                    const coordinate Height = m_FullImageActiveZone.GetHeight();
                    for (CPixelLocation DiscreteLocation; DiscreteLocation.m_y < Height; ++DiscreteLocation.m_y)
                        for (DiscreteLocation.m_x = 0; DiscreteLocation.m_x < Width; ++DiscreteLocation.m_x, ++pMappingPixel)
                        {
                            pMappingPixel->SetContinousLocation(DetermineDistortedContinousLocation(Mathematics::_2D::CVector2D(DiscreteLocation) + BasePoint), MaximalActiveZone);
                        }
                }

                bool CCameraLensUndistorter::Initialize()
                {
                    if (m_InterpolationActiveZone.IsEnabled())
                    {
                        InitializeFullZone(CImageActiveZone(m_InterpolationActiveZone.GetSize(), 0));
                        InitializeMaximalRectangularZone(m_InterpolationActiveZone, m_MaximalRectangularImageRegionActiveZone);
                        InitializeClippingZone(m_InterpolationActiveZone, m_ClippingImageRegionActiveZone);
                        InitializeMappingImage(m_InterpolationActiveZone);
                        return true;
                    }
                    return false;
                }

                template<typename InputPixelType, typename OutputPixelType> bool CCameraLensUndistorter::IsValid(const TImage<InputPixelType>* pInputImage, TImage<OutputPixelType>* pOutputImage, const CImageActiveZone* pActiveZone) const
                {
                    return m_IsInitialized && m_pMappingImage && pInputImage && pOutputImage && (((void*) pInputImage) != ((void*) pOutputImage)) && (pInputImage->GetSize() == m_InterpolationActiveZone.GetSize()) && pActiveZone && pActiveZone->IsEnabled() && (pActiveZone->GetActiveHeight() == pOutputImage->GetHeight()) && (pActiveZone->GetActiveWidth() == pOutputImage->GetWidth());
                }

            }
        }
    }
}
