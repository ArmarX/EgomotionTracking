/*
 * StereoCameraGeometricCalibration.cpp
 *
 *  Created on: 11.04.2011
 *      Author: gonzalez
 */

#include "StereoCameraGeometricCalibration.h"

using namespace EVP::Mathematics::_2D;
using namespace EVP::Mathematics::_3D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace GeometricCalibration
            {
                CStereoCameraGeometricCalibration::CStereoCameraGeometricCalibration() :
                    m_pLeftCameraGeometricCalibration(nullptr), m_pRightCameraGeometricCalibration(nullptr)
                {
                }

                CStereoCameraGeometricCalibration::~CStereoCameraGeometricCalibration()
                    = default;

                const CCameraGeometricCalibration* CStereoCameraGeometricCalibration::GetLeftCameraGeometricCalibration() const
                {
                    return m_pLeftCameraGeometricCalibration;
                }

                const CCameraGeometricCalibration* CStereoCameraGeometricCalibration::GetRightCameraGeometricCalibration() const
                {
                    return m_pRightCameraGeometricCalibration;
                }
            }
        }
    }
}
