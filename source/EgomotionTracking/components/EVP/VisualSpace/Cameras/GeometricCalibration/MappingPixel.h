/*
 * MappingPixel.h
 *
 *  Created on: 01.03.2012
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"
#include "../../../Foundation/Mathematics/2D/Vector2D.h"
#include "../../../Foundation/Mathematics/1D/BicubicLookUpTable.h"
#include "../../Common/ImageActiveZone.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace GeometricCalibration
            {
                class CMappingPixel
                {
                public:

                    union InterpolationContribution
                    {
                        real m_Wij[4][4];
                        real m_W[16];
                    };

                    CMappingPixel();
                    ~CMappingPixel();

                    void SetContinousLocation(const Mathematics::_2D::CVector2D& ContinousLocation, const CImageActiveZone& ActiveZone);

                    bool IsInsideSourceImage() const;
                    const CPixelLocation& GetDiscreteBicubicInterpolationBaseLocation() const;
                    const CPixelLocation& GetDiscreteNonInterpolationLocation() const;
                    const real* GetInterpolationContribution() const;

                protected:

                    struct Mapping
                    {
                        CPixelLocation m_InterpolationSource;
                        CPixelLocation m_InterpolationBase;
                        InterpolationContribution m_InterpolationContribution;
                    };
                    Mapping* m_pMapping;
                };
            }
        }
    }
}

