/*
 * StereoCameraLensUndistorter.cpp
 *
 *  Created on: 19.04.2011
 *      Author: gonzalez
 */

#include "StereoCameraLensUndistorter.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace GeometricCalibration
            {
                CStereoCameraLensUndistorter::CStereoCameraLensUndistorter()
                {
                    m_pStereoCameraGeometricCalibration = nullptr;
                    m_pLeftCameraGeometricCalibration = nullptr;
                    m_pRightCameraGeometricCalibration = nullptr;
                }

                CStereoCameraLensUndistorter::~CStereoCameraLensUndistorter()
                    = default;

                const CStereoCameraGeometricCalibration* CStereoCameraLensUndistorter::GetStereoCameraGeometricCalibration()
                {
                    return m_pStereoCameraGeometricCalibration;
                }

                const CCameraGeometricCalibration* CStereoCameraLensUndistorter::GetLeftCameraGeometricCalibration()
                {
                    return m_pLeftCameraGeometricCalibration;
                }

                const CCameraGeometricCalibration* CStereoCameraLensUndistorter::GetRightCameraGeometricCalibration()
                {
                    return m_pRightCameraGeometricCalibration;
                }
            }
        }
    }
}
