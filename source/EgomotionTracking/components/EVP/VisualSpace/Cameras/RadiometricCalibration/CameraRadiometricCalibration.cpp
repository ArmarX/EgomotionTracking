/*
 * CameraRadiometricCalibration.cpp
 *
 *  Created on: 07.04.2011
 *      Author: gonzalez
 */

#include "CameraRadiometricCalibration.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                CCameraRadiometricCalibration::CCameraRadiometricCalibration(const DeviceIdentifier CameraId) :
                    m_pGlobalMultipleChannelRadiometricResponceFunction(nullptr), m_pLocalRadiometricResponceFunction(nullptr)
                {
                }

                CCameraRadiometricCalibration::~CCameraRadiometricCalibration()
                {
                    RELEASE_OBJECT(m_pGlobalMultipleChannelRadiometricResponceFunction)
                    RELEASE_OBJECT(m_pLocalRadiometricResponceFunction)
                }
            }
        }
    }
}
