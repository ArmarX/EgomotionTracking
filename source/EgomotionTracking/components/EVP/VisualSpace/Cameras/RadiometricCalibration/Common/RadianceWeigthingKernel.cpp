/*
 * RadianceWeigthingKernel.cpp
 *
 *  Created on: Apr 17, 2012
 *      Author: david
 */

#include "RadianceWeigthingKernel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                std::string CRadianceWeigthingKernel::RegressionWeigthingKernelToString(const KernelType Kernel)
                {
                    switch (Kernel)
                    {
                        case eUnknownKernelType:
                            return std::string("Unknown");
                        case eUniform:
                            return std::string("Uniform");
                        case eTriangular:
                            return std::string("Triangular");
                        case eEpanechnikov:
                            return std::string("Epanechnikov");
                        case eBiweight:
                            return std::string("Biweight");
                        case eTriweight:
                            return std::string("Triweight");
                        case eTricube:
                            return std::string("Tricube");
                        case eCosine:
                            return std::string("Cosine");
                        case eGaussian:
                            return std::string("Gaussian");
                        case eGaussianBox:
                            return std::string("Gaussian-Box");
                    }
                    return std::string("Error");
                }

                CRadianceWeigthingKernel::CRadianceWeigthingKernel() :
                    m_GaussianBoxKernelRadius(_RADIANCE_WEIGTHING_KERNEL_MINIMAL_DEFAULT_GAUSSIAN_BOX_RADIUS_), m_CommonGaussianKernelExponentFactor(Mathematics::_1D::NormalDistribution::DetermineExponentFactor(Mathematics::_1D::NormalDistribution::DetermineNonNormalizedStandardDeviation(_RADIANCE_WEIGTHING_KERNEL_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_DENSITY_, _RADIANCE_WEIGTHING_KERNEL_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_NORMALIZED_DEVIATION_)))
                {
                }

                CRadianceWeigthingKernel::CRadianceWeigthingKernel(const CRadianceWeigthingKernel& RadianceWeigthingKernel)
                    = default;

                CRadianceWeigthingKernel::~CRadianceWeigthingKernel()
                    = default;

                bool CRadianceWeigthingKernel::SetGaussianBoxKernelRadius(const real GaussianBoxKernelRadius)
                {
                    if ((GaussianBoxKernelRadius >= _RADIANCE_WEIGTHING_KERNEL_MINIMAL_GAUSSIAN_BOX_RADIUS_) && (GaussianBoxKernelRadius <= _RADIANCE_WEIGTHING_KERNEL_MAXIMAL_GAUSSIAN_BOX_RADIUS_))
                    {
                        m_GaussianBoxKernelRadius = GaussianBoxKernelRadius;
                        return true;
                    }
                    return false;
                }

                bool CRadianceWeigthingKernel::SetCommonGaussianKernelStandardDeviation(const real StandardDeviation)
                {
                    if ((StandardDeviation > _REAL_ZERO_) && (StandardDeviation <= _REAL_THREE_))
                    {
                        m_CommonGaussianKernelExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(StandardDeviation);
                        return true;
                    }
                    return false;
                }

                bool CRadianceWeigthingKernel::SetCommonGaussianKernelCutOff(const real CutOffDensity, const real CutOffNormalizedDeviation)
                {
                    if ((CutOffDensity > _REAL_ZERO_) && (CutOffDensity < _REAL_ONE_) && (CutOffNormalizedDeviation > _REAL_ZERO_) && (CutOffNormalizedDeviation < _REAL_ONE_))
                    {
                        m_CommonGaussianKernelExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(Mathematics::_1D::NormalDistribution::DetermineNonNormalizedStandardDeviation(CutOffDensity, CutOffNormalizedDeviation));
                        return true;
                    }
                    return false;
                }

                real CRadianceWeigthingKernel::GetGaussianBoxKernelRadius() const
                {
                    return m_GaussianBoxKernelRadius;
                }

                real CRadianceWeigthingKernel::GetCommonGaussianKernelExponentFactor() const
                {
                    return m_CommonGaussianKernelExponentFactor;
                }

                real CRadianceWeigthingKernel::GetCommonGaussianKernelStandardDeviation() const
                {
                    return RealSqrt(_REAL_ONE_ / (-_REAL_TWO_ * m_CommonGaussianKernelExponentFactor));
                }

                real CRadianceWeigthingKernel::GetRegressionWeigthingByUniformKernel(const real /*Intensity*/) const
                {
                    return _REAL_ONE_;
                }

                real CRadianceWeigthingKernel::GetRegressionWeigthingByTriangularKernel(const real Intensity) const
                {
                    return _REAL_ONE_ - RealAbs((Intensity - _REAL_127_5_) * _REAL_1_127_5_);
                }

                real CRadianceWeigthingKernel::GetRegressionWeigthingByEpanechnikovKernel(const real Intensity) const
                {
                    const real NormalizedDeviation = (Intensity - _REAL_127_5_) * _REAL_1_127_5_;
                    return _REAL_ONE_ - (NormalizedDeviation * NormalizedDeviation);
                }

                real CRadianceWeigthingKernel::GetRegressionWeigthingByBiweightKernel(const real Intensity) const
                {
                    const real NormalizedDeviation = (Intensity - _REAL_127_5_) * _REAL_1_127_5_;
                    const real Inner = _REAL_ONE_ - (NormalizedDeviation * NormalizedDeviation);
                    return Inner * Inner;
                }

                real CRadianceWeigthingKernel::GetRegressionWeigthingByTriweightKernel(const real Intensity) const
                {
                    const real NormalizedDeviation = (Intensity - _REAL_127_5_) * _REAL_1_127_5_;
                    const real Inner = _REAL_ONE_ - (NormalizedDeviation * NormalizedDeviation);
                    return Inner * Inner * Inner;
                }

                real CRadianceWeigthingKernel::GetRegressionWeigthingByTricubeKernel(const real Intensity) const
                {
                    const real AbsoluteNormalizedDeviation = RealAbs((Intensity - _REAL_127_5_) * _REAL_1_127_5_);
                    const real Inner = _REAL_ONE_ - (AbsoluteNormalizedDeviation * AbsoluteNormalizedDeviation * AbsoluteNormalizedDeviation);
                    return Inner * Inner * Inner;
                }

                real CRadianceWeigthingKernel::GetRegressionWeigthingByCosineKernel(const real Intensity) const
                {
                    return RealCosinus((Intensity - _REAL_127_5_) * _REAL_1_127_5_ * _REAL_HALF_PI_);
                }

                real CRadianceWeigthingKernel::GetRegressionWeigthingByGaussianKernel(const real Intensity) const
                {
                    const real NormalizedDeviation = (Intensity - _REAL_127_5_) * _REAL_1_127_5_;
                    return RealExp(NormalizedDeviation * NormalizedDeviation * m_CommonGaussianKernelExponentFactor);
                }

                real CRadianceWeigthingKernel::GetRegressionWeigthingByGaussianBoxKernel(const real Intensity) const
                {
                    if ((Intensity >= m_GaussianBoxKernelRadius) && (Intensity <= (_REAL_255_ - m_GaussianBoxKernelRadius)))
                    {
                        return _REAL_ONE_;
                    }
                    const real GaussianBoxRadiusNormalizedDeviation = (Intensity < m_GaussianBoxKernelRadius) ? ((m_GaussianBoxKernelRadius - Intensity) / m_GaussianBoxKernelRadius) : (_REAL_ONE_ - ((_REAL_255_ - Intensity) / m_GaussianBoxKernelRadius));
                    return RealExp(GaussianBoxRadiusNormalizedDeviation * GaussianBoxRadiusNormalizedDeviation * m_CommonGaussianKernelExponentFactor);
                }

                real CRadianceWeigthingKernel::GetRegressionWeigthingByKernel(const real Intensity, const KernelType Kernel) const
                {
                    switch (Kernel)
                    {
                        case eGaussian:
                        {
                            const real NormalizedDeviation = (Intensity - _REAL_127_5_) * _REAL_1_127_5_;
                            return RealExp(NormalizedDeviation * NormalizedDeviation * m_CommonGaussianKernelExponentFactor);
                        }
                        case eEpanechnikov:
                        {
                            const real NormalizedDeviation = (Intensity - _REAL_127_5_) * _REAL_1_127_5_;
                            return _REAL_ONE_ - (NormalizedDeviation * NormalizedDeviation);
                        }
                        case eGaussianBox:
                        {
                            if ((Intensity >= m_GaussianBoxKernelRadius) && (Intensity <= (_REAL_255_ - m_GaussianBoxKernelRadius)))
                            {
                                return _REAL_ONE_;
                            }
                            const real GaussianBoxRadiusNormalizedDeviation = (Intensity < m_GaussianBoxKernelRadius) ? ((m_GaussianBoxKernelRadius - Intensity) / m_GaussianBoxKernelRadius) : (_REAL_ONE_ - ((_REAL_255_ - Intensity) / m_GaussianBoxKernelRadius));
                            return RealExp(GaussianBoxRadiusNormalizedDeviation * GaussianBoxRadiusNormalizedDeviation * m_CommonGaussianKernelExponentFactor);
                        }
                        case eTriangular:
                            return _REAL_ONE_ - RealAbs((Intensity - _REAL_127_5_) * _REAL_1_127_5_);
                        case eBiweight:
                        {
                            const real NormalizedDeviation = (Intensity - _REAL_127_5_) * _REAL_1_127_5_;
                            const real Inner = _REAL_ONE_ - (NormalizedDeviation * NormalizedDeviation);
                            return Inner * Inner;
                        }

                        case eTriweight:
                        {
                            const real NormalizedDeviation = (Intensity - _REAL_127_5_) * _REAL_1_127_5_;
                            const real Inner = _REAL_ONE_ - (NormalizedDeviation * NormalizedDeviation);
                            return Inner * Inner * Inner;
                        }
                        case eTricube:
                        {
                            const real AbsoluteNormalizedDeviation = RealAbs((Intensity - _REAL_127_5_) * _REAL_1_127_5_);
                            const real Inner = _REAL_ONE_ - (AbsoluteNormalizedDeviation * AbsoluteNormalizedDeviation * AbsoluteNormalizedDeviation);
                            return Inner * Inner * Inner;
                        }
                        case eCosine:
                        {
                            return RealCosinus((Intensity - _REAL_127_5_) * _REAL_1_127_5_ * _REAL_HALF_PI_);
                        }
                        case eUniform:
                            return _REAL_ONE_;
                        case eUnknownKernelType:
                            return _REAL_ZERO_;
                    }
                    return _REAL_ZERO_;
                }

                real CRadianceWeigthingKernel::GetUpperIntensityAtDensity(const KernelType Kernel, const real MinimalKernelDensity) const
                {
                    if ((MinimalKernelDensity >= _REAL_ZERO_) && (MinimalKernelDensity <= _REAL_ONE_))
                        switch (Kernel)
                        {
                            case eUnknownKernelType:
                                return _REAL_ZERO_;
                            case eUniform:
                                return _REAL_255_;
                            case eTriangular:
                                return _REAL_127_5_ + (_REAL_ONE_ - MinimalKernelDensity) * _REAL_127_5_;
                            case eEpanechnikov:
                                return _REAL_127_5_ + RealSqrt((_REAL_ONE_ - MinimalKernelDensity)) * _REAL_127_5_;
                            case eBiweight:
                                return _REAL_127_5_ + RealSqrt(_REAL_ONE_ - RealSqrt(MinimalKernelDensity)) * _REAL_127_5_;
                            case eTriweight:
                                return _REAL_127_5_ + RealSqrt(_REAL_ONE_ - RealPow(MinimalKernelDensity, _REAL_THIRD_)) * _REAL_127_5_;
                            case eTricube:
                                return _REAL_127_5_ + RealPow(_REAL_ONE_ - RealPow(MinimalKernelDensity, _REAL_THIRD_), _REAL_THIRD_) * _REAL_127_5_;
                            case eCosine:
                                return _REAL_127_5_ + (RealArcCosinus(MinimalKernelDensity) / _REAL_HALF_PI_) * _REAL_127_5_;
                            case eGaussian:
                                return _REAL_127_5_ + RealSqrt(RealLog(MinimalKernelDensity) / m_CommonGaussianKernelExponentFactor) * _REAL_127_5_;
                            case eGaussianBox:
                                return (MinimalKernelDensity == _REAL_ONE_) ? (_REAL_255_ - m_GaussianBoxKernelRadius) : _REAL_255_ + (RealSqrt(RealLog(MinimalKernelDensity) / m_CommonGaussianKernelExponentFactor) - _REAL_ONE_) * m_GaussianBoxKernelRadius;
                        }
                    return _REAL_ZERO_;
                }

                real CRadianceWeigthingKernel::GetLowerIntensityAtDensity(const KernelType Kernel, const real MinimalKernelDensity) const
                {
                    if ((MinimalKernelDensity >= _REAL_ZERO_) && (MinimalKernelDensity <= _REAL_ONE_))
                        switch (Kernel)
                        {
                            case eUnknownKernelType:
                                return _REAL_ZERO_;
                            case eUniform:
                                return _REAL_255_;
                            case eTriangular:
                                return _REAL_127_5_ - (_REAL_ONE_ - MinimalKernelDensity) * _REAL_127_5_;
                            case eEpanechnikov:
                                return _REAL_127_5_ - RealSqrt((_REAL_ONE_ - MinimalKernelDensity)) * _REAL_127_5_;
                            case eBiweight:
                                return _REAL_127_5_ - RealSqrt(_REAL_ONE_ - RealSqrt(MinimalKernelDensity)) * _REAL_127_5_;
                            case eTriweight:
                                return _REAL_127_5_ - RealSqrt(_REAL_ONE_ - RealPow(MinimalKernelDensity, _REAL_THIRD_)) * _REAL_127_5_;
                            case eTricube:
                                return _REAL_127_5_ - RealPow(_REAL_ONE_ - RealPow(MinimalKernelDensity, _REAL_THIRD_), _REAL_THIRD_) * _REAL_127_5_;
                            case eCosine:
                                return _REAL_127_5_ - (RealArcCosinus(MinimalKernelDensity) / _REAL_HALF_PI_) * _REAL_127_5_;
                            case eGaussian:
                                return _REAL_127_5_ - RealSqrt(RealLog(MinimalKernelDensity) / m_CommonGaussianKernelExponentFactor) * _REAL_127_5_;
                            case eGaussianBox:
                                return (MinimalKernelDensity == _REAL_ONE_) ? m_GaussianBoxKernelRadius : (_REAL_ONE_ - RealSqrt(RealLog(MinimalKernelDensity) / m_CommonGaussianKernelExponentFactor)) * m_GaussianBoxKernelRadius;
                        }
                    return _REAL_ZERO_;
                }

                void CRadianceWeigthingKernel::operator=(const CRadianceWeigthingKernel& RadianceWeigthingKernel)
                {
                    m_GaussianBoxKernelRadius = RadianceWeigthingKernel.m_GaussianBoxKernelRadius;
                    m_CommonGaussianKernelExponentFactor = RadianceWeigthingKernel.m_CommonGaussianKernelExponentFactor;
                }

                bool CRadianceWeigthingKernel::LoadFromFile(Files::CInFile& InputBinaryFile)
                {
                    if (InputBinaryFile.IsReady())
                        if (InputBinaryFile.ReadBuffer(&m_GaussianBoxKernelRadius, sizeof(real)))
                        {
                            return InputBinaryFile.ReadBuffer(&m_CommonGaussianKernelExponentFactor, sizeof(real));
                        }
                    return false;
                }

                bool CRadianceWeigthingKernel::SaveToFile(Files::COutFile& OutputBinaryFile) const
                {
                    if (OutputBinaryFile.IsReady())
                        if (OutputBinaryFile.WriteBuffer(&m_GaussianBoxKernelRadius, sizeof(real)))
                        {
                            return OutputBinaryFile.WriteBuffer(&m_CommonGaussianKernelExponentFactor, sizeof(real));
                        }
                    return false;
                }
            }
        }
    }
}

