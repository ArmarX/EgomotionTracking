/*
 * CalibratedExposure.h
 *
 *  Created on: 01.05.2012
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "Exposure.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CCalibratedExposure: public CExposure
                {
                public:

                    static  list<CCalibratedExposure> MergeExposureSets(const list<CCalibratedExposure>& Lhs, const list<CCalibratedExposure>& rhs);

                    CCalibratedExposure(const uint ParametricIndex, const real MaximalSamplingDensity, const real ScopeMinimalRadiance, const real ScopeMaximalRadiance, const real ScopeMinimalExposureTime, const real CentralExposureTime, const real ScopeMaximalExposureTime);
                    ~CCalibratedExposure() override;

                    real GetMaximalSamplingDensity() const;
                    real GetScopeMinimalRadiance() const;
                    real GetScopeMaximalRadiance() const;
                    real GetScopeRangeRadiance() const;
                    real GetScopeMinimalExposureTime() const;
                    real GetScopeMaximalExposureTime() const;
                    real GetScopeRangeExposureTime() const;

                    real GetTimeDeltaToCentralExposureTime(const real Time) const;
                    real GetAbsoluteTimeDeltaToCentralExposureTime(const real Time) const;
                    bool IsContainedExposureTimeScope(const real Time) const;
                    bool IsContainedExposureTimeScope(const real T0, const real T1) const;
                    bool IsContainedExposureRadianceScope(const real Radiance) const;

                    bool HasCommonTemporalScope(const real T0, const real T1) const;
                    bool HasCommonTemporalScope(const CCalibratedExposure& CalibratedExposure) const;
                    real GetCommonTemporalScope(const CCalibratedExposure& CalibratedExposure) const;

                    bool HasCommonRadianceScope(const CCalibratedExposure& CalibratedExposure) const;
                    real GetCommonRadianceScope(const CCalibratedExposure& CalibratedExposure) const;

                    void operator=(const CCalibratedExposure& CalibratedExposure);

                protected:

                    real m_MaximalSamplingDensity;
                    real m_ScopeMinimalRadiance;
                    real m_ScopeMaximalRadiance;
                    real m_ScopeMinimalExposureTime;
                    real m_ScopeMaximalExposureTime;
                };
            }
        }
    }
}
