/*
 * CalibratedExposure.cpp
 *
 *  Created on: 01.05.2012
 *      Author: gonzalez
 */

#include "CalibratedExposure.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {

                list<CCalibratedExposure> CCalibratedExposure::MergeExposureSets(const list<CCalibratedExposure>& Lhs, const list<CCalibratedExposure>& rhs)
                {
                    list<CCalibratedExposure> MergedExposureSets;
                    MergedExposureSets.insert(MergedExposureSets.end(), Lhs.begin(), Lhs.end());
                    MergedExposureSets.insert(MergedExposureSets.end(), rhs.begin(), rhs.end());
                    MergedExposureSets.sort(CExposure::SortExposuresByParametricIndex);
                    MergedExposureSets.unique(CExposure::EqualsExposuresByParametricIndex);
                    return MergedExposureSets;
                }

                CCalibratedExposure::CCalibratedExposure(const uint ParametricIndex, const real MaximalSamplingDensity, const real ScopeMinimalRadiance, const real ScopeMaximalRadiance, const real ScopeMinimalExposureTime, const real CentralExposureTime, const real ScopeMaximalExposureTime) :
                    CExposure(CentralExposureTime, ParametricIndex), m_MaximalSamplingDensity(MaximalSamplingDensity), m_ScopeMinimalRadiance(ScopeMinimalRadiance), m_ScopeMaximalRadiance(ScopeMaximalRadiance), m_ScopeMinimalExposureTime(ScopeMinimalExposureTime), m_ScopeMaximalExposureTime(ScopeMaximalExposureTime)
                {
                }

                CCalibratedExposure::~CCalibratedExposure()
                    = default;

                real CCalibratedExposure::GetMaximalSamplingDensity() const
                {
                    return m_MaximalSamplingDensity;
                }

                real CCalibratedExposure::GetScopeMinimalRadiance() const
                {
                    return m_ScopeMinimalRadiance;
                }

                real CCalibratedExposure::GetScopeMaximalRadiance() const
                {
                    return m_ScopeMaximalRadiance;
                }

                real CCalibratedExposure::GetScopeRangeRadiance() const
                {
                    return m_ScopeMaximalRadiance - m_ScopeMinimalRadiance;
                }

                real CCalibratedExposure::GetScopeMinimalExposureTime() const
                {
                    return m_ScopeMinimalExposureTime;
                }

                real CCalibratedExposure::GetScopeMaximalExposureTime() const
                {
                    return m_ScopeMaximalExposureTime;
                }

                real CCalibratedExposure::GetScopeRangeExposureTime() const
                {
                    return m_ScopeMaximalExposureTime - m_ScopeMinimalExposureTime;
                }

                real CCalibratedExposure::GetTimeDeltaToCentralExposureTime(const real Time) const
                {
                    return Time - m_Time;
                }

                real CCalibratedExposure::GetAbsoluteTimeDeltaToCentralExposureTime(const real Time) const
                {
                    return RealAbs(Time - m_Time);
                }

                bool CCalibratedExposure::IsContainedExposureTimeScope(const real Time) const
                {
                    return (Time >= m_ScopeMinimalExposureTime) && (Time <= m_ScopeMaximalExposureTime);
                }

                bool CCalibratedExposure::IsContainedExposureTimeScope(const real T0, const real T1) const
                {
                    return (T0 <= m_ScopeMaximalExposureTime) && (T1 >= m_ScopeMinimalExposureTime);
                }

                bool CCalibratedExposure::IsContainedExposureRadianceScope(const real Radiance) const
                {
                    return (Radiance >= m_ScopeMinimalRadiance) && (Radiance <= m_ScopeMaximalRadiance);
                }

                bool CCalibratedExposure::HasCommonTemporalScope(const real T0, const real T1) const
                {
                    return (m_ScopeMinimalExposureTime < T1) && (m_ScopeMaximalExposureTime > T0);
                }

                bool CCalibratedExposure::HasCommonTemporalScope(const CCalibratedExposure& CalibratedExposure) const
                {
                    return (m_ScopeMinimalExposureTime < CalibratedExposure.m_ScopeMaximalExposureTime) && (m_ScopeMaximalExposureTime > CalibratedExposure.m_ScopeMinimalExposureTime);
                }

                real CCalibratedExposure::GetCommonTemporalScope(const CCalibratedExposure& CalibratedExposure) const
                {
                    if ((m_ScopeMinimalExposureTime < CalibratedExposure.m_ScopeMaximalExposureTime) && (m_ScopeMaximalExposureTime > CalibratedExposure.m_ScopeMinimalExposureTime))
                    {
                        return TMin(m_ScopeMaximalExposureTime, CalibratedExposure.m_ScopeMaximalExposureTime) - TMax(m_ScopeMinimalExposureTime, CalibratedExposure.m_ScopeMinimalExposureTime);
                    }
                    return _REAL_ZERO_;
                }

                bool CCalibratedExposure::HasCommonRadianceScope(const CCalibratedExposure& CalibratedExposure) const
                {
                    return (m_ScopeMinimalRadiance < CalibratedExposure.m_ScopeMaximalRadiance) && (m_ScopeMaximalRadiance > CalibratedExposure.m_ScopeMinimalRadiance);
                }

                real CCalibratedExposure::GetCommonRadianceScope(const CCalibratedExposure& CalibratedExposure) const
                {
                    if ((m_ScopeMinimalRadiance < CalibratedExposure.m_ScopeMaximalRadiance) && (m_ScopeMaximalRadiance > CalibratedExposure.m_ScopeMinimalExposureTime))
                    {
                        return TMin(m_ScopeMaximalRadiance, CalibratedExposure.m_ScopeMaximalRadiance) - TMax(m_ScopeMinimalRadiance, CalibratedExposure.m_ScopeMinimalRadiance);
                    }
                    return _REAL_ZERO_;
                }

                void CCalibratedExposure::operator=(const CCalibratedExposure& CalibratedExposure)
                {
                    m_Time = CalibratedExposure.m_Time;
                    m_ParametricIndex = CalibratedExposure.m_ParametricIndex;
                    m_MaximalSamplingDensity = CalibratedExposure.m_MaximalSamplingDensity;
                    m_ScopeMinimalRadiance = CalibratedExposure.m_ScopeMinimalRadiance;
                    m_ScopeMaximalRadiance = CalibratedExposure.m_ScopeMaximalRadiance;
                    m_ScopeMinimalExposureTime = CalibratedExposure.m_ScopeMinimalExposureTime;
                    m_ScopeMaximalExposureTime = CalibratedExposure.m_ScopeMaximalExposureTime;
                }
            }
        }
    }
}
