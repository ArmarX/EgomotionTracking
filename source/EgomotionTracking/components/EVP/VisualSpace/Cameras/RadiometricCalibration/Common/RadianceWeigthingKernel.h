/*
 * RadianceWeigthingKernel.h
 *
 *  Created on: Apr 17, 2012
 *      Author: david
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/Mathematics/1D/Common1D.h"
#include "../../../../Foundation/Files/InFile.h"
#include "../../../../Foundation/Files/OutFile.h"

#define _RADIANCE_WEIGTHING_KERNEL_MINIMAL_GAUSSIAN_BOX_RADIUS_ real(8.0)
#define _RADIANCE_WEIGTHING_KERNEL_MAXIMAL_GAUSSIAN_BOX_RADIUS_ real(64.0)
#define _RADIANCE_WEIGTHING_KERNEL_MINIMAL_DEFAULT_GAUSSIAN_BOX_RADIUS_ real(64.0)
#define _RADIANCE_WEIGTHING_KERNEL_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_DENSITY_ real(0.01)
#define _RADIANCE_WEIGTHING_KERNEL_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_NORMALIZED_DEVIATION_ real(0.95)

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CRadianceWeigthingKernel
                {
                public:

                    enum KernelType
                    {
                        eUnknownKernelType = 0, eUniform, eTriangular, eEpanechnikov, eBiweight, eTriweight, eTricube, eCosine, eGaussian, eGaussianBox
                    };

                    static  std::string RegressionWeigthingKernelToString(const KernelType Kernel);

                    CRadianceWeigthingKernel();
                    CRadianceWeigthingKernel(const CRadianceWeigthingKernel& RadianceWeigthingKernel);
                    virtual ~CRadianceWeigthingKernel();

                    bool SetGaussianBoxKernelRadius(const real GaussianBoxKernelRadius);
                    bool SetCommonGaussianKernelStandardDeviation(const real StandardDeviation);
                    bool SetCommonGaussianKernelCutOff(const real CutOffDensity, const real CutOffNormalizedDeviation);

                    real GetGaussianBoxKernelRadius() const;
                    real GetCommonGaussianKernelExponentFactor() const;
                    real GetCommonGaussianKernelStandardDeviation() const;

                    real GetRegressionWeigthingByUniformKernel(const real Intensity) const;
                    real GetRegressionWeigthingByTriangularKernel(const real Intensity) const;
                    real GetRegressionWeigthingByEpanechnikovKernel(const real Intensity) const;
                    real GetRegressionWeigthingByBiweightKernel(const real Intensity) const;
                    real GetRegressionWeigthingByTriweightKernel(const real Intensity) const;
                    real GetRegressionWeigthingByTricubeKernel(const real Intensity) const;
                    real GetRegressionWeigthingByCosineKernel(const real Intensity) const;
                    real GetRegressionWeigthingByGaussianKernel(const real Intensity) const;
                    real GetRegressionWeigthingByGaussianBoxKernel(const real Intensity) const;

                    real GetRegressionWeigthingByKernel(const real Intensity, const KernelType Kernel) const;
                    real GetUpperIntensityAtDensity(const KernelType Kernel, const real MinimalKernelDensity) const;
                    real GetLowerIntensityAtDensity(const KernelType Kernel, const real MinimalKernelDensity) const;

                    void operator=(const CRadianceWeigthingKernel& RadianceWeigthingKernel);
                    bool LoadFromFile(Files::CInFile& InputBinaryFile);
                    bool SaveToFile(Files::COutFile& OutputBinaryFile) const;

                protected:

                    real m_GaussianBoxKernelRadius;
                    real m_CommonGaussianKernelExponentFactor;
                };
            }
        }
    }
}

