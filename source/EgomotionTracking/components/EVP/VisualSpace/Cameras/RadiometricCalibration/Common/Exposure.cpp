/*
 * Exposure.cpp
 *
 *  Created on: Apr 17, 2012
 *      Author: david
 */

#include "Exposure.h"
#include "../../../../Foundation/Files/OutFile.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                list<CExposure> CExposure::MergeExposureSets(const list<CExposure>& Lhs, const list<CExposure>& rhs)
                {
                    list<CExposure> MergedExposureSets;
                    MergedExposureSets.insert(MergedExposureSets.end(), Lhs.begin(), Lhs.end());
                    MergedExposureSets.insert(MergedExposureSets.end(), rhs.begin(), rhs.end());
                    MergedExposureSets.sort(CExposure::SortExposuresByParametricIndex);
                    MergedExposureSets.unique(CExposure::EqualsExposuresByParametricIndex);
                    return MergedExposureSets;
                }

                bool CExposure::ExportExposureSetToFile(const list<CExposure>& ExposureSet, const_string pFileName)
                {
                    if (pFileName && (ExposureSet.size() > 0))
                    {
                        std::ostringstream Content;
                        Content.precision(_REAL_EPSILON_DISPLAY_DIGITS_);
                        list<CExposure>::const_iterator EndExposures = ExposureSet.end();
                        for (list<CExposure>::const_iterator pExposure = ExposureSet.begin(); pExposure != EndExposures; ++pExposure)
                        {
                            Content << pExposure->GetParametricIndex() << "\t" << pExposure->GetTime() << "\n";
                        }
                        return Files::COutFile::WriteStringToFile(pFileName, Content.str());
                    }
                    return false;
                }

                CExposure::CExposure(const real Time, const uint ParametricIndex) :
                    m_Time(Time), m_ParametricIndex(ParametricIndex)
                {
                }

                CExposure::~CExposure()
                    = default;

                real CExposure::GetTime() const
                {
                    return m_Time;
                }

                bool CExposure::HasParametricIndex() const
                {
                    return (m_ParametricIndex != (-1u));
                }

                uint CExposure::GetParametricIndex() const
                {
                    return m_ParametricIndex;
                }

                bool CExposure::EqualsExposuresByParametricIndex(const CExposure& lhs, const CExposure& rhs)
                {
                    return (lhs.m_ParametricIndex == rhs.m_ParametricIndex);
                }

                bool CExposure::SortExposuresByParametricIndex(const CExposure& lhs, const CExposure& rhs)
                {
                    return lhs.m_ParametricIndex < rhs.m_ParametricIndex;
                }
            }
        }
    }
}
