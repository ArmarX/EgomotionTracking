/*
 * Exposure.h
 *
 *  Created on: Apr 17, 2012
 *      Author: david
 */

#pragma once

#include "../../../../GlobalSettings.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CExposure
                {
                public:

                    static  list<CExposure> MergeExposureSets(const list<CExposure>& Lhs, const list<CExposure>& rhs);
                    static  bool ExportExposureSetToFile(const list<CExposure>& ExposureSet, const_string pFileName);

                    CExposure(const real Time, const uint ParametricIndex = -1u);
                    virtual ~CExposure();

                    real GetTime() const;

                    bool HasParametricIndex() const;
                    uint GetParametricIndex() const;

                    static  bool EqualsExposuresByParametricIndex(const CExposure& lhs, const CExposure& rhs);
                    static  bool SortExposuresByParametricIndex(const CExposure& lhs, const CExposure& rhs);

                protected:

                    real m_Time;
                    uint m_ParametricIndex;
                };
            }
        }
    }
}

