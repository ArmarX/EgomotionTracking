/*
 * CameraRadiometricCalibration.h
 *
 *  Created on: 07.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"
#include "Global/GlobalMultipleChannelRadiometricResponceFunction.h"
#include "Local/LocalRadiometricResponceFunction.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CCameraRadiometricCalibration
                {
                public:

                    CCameraRadiometricCalibration(const DeviceIdentifier CameraId);
                    virtual ~CCameraRadiometricCalibration();

                protected:

                    CGlobalMultipleChannelRadiometricResponceFunction* m_pGlobalMultipleChannelRadiometricResponceFunction;
                    CLocalRadiometricResponceFunction* m_pLocalRadiometricResponceFunction;
                };
            }
        }
    }
}

