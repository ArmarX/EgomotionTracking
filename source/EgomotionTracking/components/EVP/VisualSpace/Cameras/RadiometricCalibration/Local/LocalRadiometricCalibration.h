/*
 * LocalRadiometricCalibration.h
 *
 *  Created on: Apr 17, 2012
 *      Author: david
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/DataTypes/BayerPattern.h"
#include "../../../../Foundation/DataTypes/ColorSpaces.h"
#include "../../../Common/Image.h"
#include "../Common/Exposure.h"
#include "../Common/RadianceWeigthingKernel.h"
#include "../Global/GlobalRadiometricResponceFunction.h"
#include "LocalRadiometricPixel.h"
#include "LocalRadiometricResponceFunction.h"

#define _LOCAL_RADIOMETRIC_CALIBRATION_MINIMAL_EXPOSURES_ 4u
#define _LOCAL_RADIOMETRIC_CALIBRATION_MINIMAL_SELECTED_SAMPLING_CELLS_ 32u
#define _LOCAL_RADIOMETRIC_CALIBRATION_MINIMAL_GAUSSIAN_BOX_RADIUS_ real(8.0)
#define _LOCAL_RADIOMETRIC_CALIBRATION_MAXIMAL_GAUSSIAN_BOX_RADIUS_ real(32.0)
#define _LOCAL_RADIOMETRIC_CALIBRATION_MINIMAL_DEFAULT_GAUSSIAN_BOX_RADIUS_ real(16.0)
#define _LOCAL_RADIOMETRIC_CALIBRATION_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_DENSITY_ real(0.01)
#define _LOCAL_RADIOMETRIC_CALIBRATION_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_NORMALIZED_DEVIATION_ real(0.95)

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CLocalRadiometricCalibration
                {
                public:

                    CLocalRadiometricCalibration(const CGlobalRadiometricResponceFunction* pGlobalRadiometricResponceFunction, const TImage<real>* pRadianceImage);
                    virtual ~CLocalRadiometricCalibration();

                    bool IsEnabled() const;
                    DeviceIdentifier GetCameraId() const;
                    BayerPatternType GetBayerPattern() const;
                    ChromaticSpaceRGB::ChannelContent GetChannelContent() const;
                    const CLocalRadiometricResponceFunction* GetLocalRadiometricResponceFunction() const;

                    bool AddExposure(const uint ReferenceIndex, const uint ExposureIndex, const real ExposureTime);
                    bool ClearExposures();

                    bool SetGaussianBoxKernelRadius(const real GaussianBoxKernelRadius);
                    bool SetCommonGaussianKernelStandardDeviation(const real StandardDeviation);
                    bool SetCommonGaussianKernelCutOff(const real CutOffDensity, const real CutOffNormalizedDeviation);

                    real GetGaussianBoxKernelRadius() const;
                    real GetCommonGaussianKernelStandardDeviation() const;

                    bool Calibrate(const CRadianceWeigthingKernel::KernelType Kernel);

                protected:

                    struct ReferenceExposureImage
                    {
                        const uint m_ReferenceIndex;
                        const uint m_ExposureIndex;
                        const real m_ExposureTime;
                        const TImage<real>* m_pRadianceImage;
                    };

                    bool LoadSamples(const CRadianceWeigthingKernel::KernelType Kernel);
                    void ClearSamples();
                    void LoadReferencedSamples(list<ReferenceExposureImage>& BlockExposureImages);
                    void AddSynthesizeExposure(const TImage<real>* pRadianceImage, const real ExposureTime, const CRadianceWeigthingKernel::KernelType Kernel);
                    void Synthesize();
                    void ClearSynthesize();
                    static  bool SortReferenceExposureImages(const ReferenceExposureImage& lhs, const ReferenceExposureImage& rhs);

                    const CGlobalRadiometricResponceFunction* m_pGlobalRadiometricResponceFunction;
                    const TImage<real>* m_pRadianceImage;
                    TImage<real>* m_pRadianceImageIntern;
                    TImage<real>* m_pRadianceReferenceImage;
                    TImage<real>* m_pWeightedRadianceAccumulatorImage;
                    TImage<real>* m_pWeightingAccumulatorImage;
                    TImage<CLocalRadiometricPixel>* m_pRadiometricImage;
                    CLocalRadiometricResponceFunction* m_pLocalRadiometricResponceFunction;
                    list<CLocalRadiometricPixel*> m_RadiometricPixels;
                    CRadianceWeigthingKernel m_RadianceWeigthingKernel;
                    list<ReferenceExposureImage> m_ReferenceExposureImages;
                };
            }
        }
    }
}

