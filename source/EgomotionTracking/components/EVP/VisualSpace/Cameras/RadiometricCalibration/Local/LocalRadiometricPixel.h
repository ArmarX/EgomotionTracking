/*
 * GlobalRadiometricPixel.h
 *
 *  Created on: 21.09.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/Mathematics/ND/LinearAlgebra.h"
#include "../../../../Foundation/DataTypes/PixelLocation.h"
#include "../Common/Exposure.h"
#include "../Common/RadianceWeigthingKernel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CLocalRadiometricPixel
                {
                public:

                    CLocalRadiometricPixel();
                    ~CLocalRadiometricPixel();

                    CLocalRadiometricPixel* Initialize(const coordinate X, const coordinate Y, const real* pRadiance, const real* pRadianceReference);

                    void AddExposure(const real ExposureTime);
                    void ClearExposures();
                    bool Calibrate(const CRadianceWeigthingKernel& RadianceWeigthingKernel, const CRadianceWeigthingKernel::KernelType Kernel);

                    const CPixelLocation& GetLocation() const;
                    real GetSlope() const;
                    real GetOffset() const;
                    real GetRMSDeviation() const;
                    real GetRangeDeviation() const;
                    real GetPeakNegativeDeviation() const;
                    real GetPeakPositiveDeviation() const;

                protected:

                    struct RadianceReferencedSample
                    {
                        const real m_Radiance;
                        const real m_RadianceReference;
                        const real m_ExposureTime;
                    };

                    CPixelLocation m_Location;
                    const real* m_pRadiance;
                    const real* m_pRadianceReference;
                    list<RadianceReferencedSample> m_RadianceReferencedSamples;
                    real m_Slope;
                    real m_Offset;
                    real m_RMSDeviation;
                    real m_PeakNegativeDeviation;
                    real m_PeakPositiveDeviation;
                };
            }
        }
    }
}

