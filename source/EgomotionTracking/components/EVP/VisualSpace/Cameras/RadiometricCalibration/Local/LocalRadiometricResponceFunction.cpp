/*
 * LocalRadiometricResponceFunction.cpp
 *
 *  Created on: 27.04.2012
 *      Author: gonzalez
 */

#include "LocalRadiometricResponceFunction.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                const CLocalRadiometricResponceFunction* CLocalRadiometricResponceFunction::MergeChannels(const CLocalRadiometricResponceFunction* pResponceFunctionChannelRed, const CLocalRadiometricResponceFunction* pResponceFunctionChannelGreen, const CLocalRadiometricResponceFunction* pResponceFunctionChannelBlue)
                {
                    if (pResponceFunctionChannelRed && pResponceFunctionChannelGreen && pResponceFunctionChannelBlue && pResponceFunctionChannelRed->IsEnabled() && pResponceFunctionChannelGreen->IsEnabled() && pResponceFunctionChannelBlue->IsEnabled() && (pResponceFunctionChannelRed->GetChannelContent() == ChromaticSpaceRGB::eRed) && (pResponceFunctionChannelGreen->GetChannelContent() == ChromaticSpaceRGB::eGreen) && (pResponceFunctionChannelBlue->GetChannelContent() == ChromaticSpaceRGB::eBlue) && (pResponceFunctionChannelRed->GetBayerPattern() != eUnknownBayerPattern) && (pResponceFunctionChannelRed->GetBayerPattern() == pResponceFunctionChannelGreen->GetBayerPattern()) && (pResponceFunctionChannelGreen->GetBayerPattern() == pResponceFunctionChannelBlue->GetBayerPattern()) && pResponceFunctionChannelRed->GetCameraId() && (pResponceFunctionChannelRed->GetCameraId() == pResponceFunctionChannelGreen->GetCameraId()) && (pResponceFunctionChannelGreen->GetCameraId() == pResponceFunctionChannelBlue->GetCameraId()))
                    {
                        const TImage<CLocalPixelResponce>* pPixelResponceImageChannelRed = pResponceFunctionChannelRed->GetLocalRadiometricResponceImage();
                        const TImage<CLocalPixelResponce>* pPixelResponceImageChannelGreen = pResponceFunctionChannelGreen->GetLocalRadiometricResponceImage();
                        const TImage<CLocalPixelResponce>* pPixelResponceImageChannelBlue = pResponceFunctionChannelBlue->GetLocalRadiometricResponceImage();
                        if ((pPixelResponceImageChannelRed->GetSize() == pPixelResponceImageChannelGreen->GetSize()) && (pPixelResponceImageChannelGreen->GetSize() == pPixelResponceImageChannelBlue->GetSize()))
                        {
                            ChromaticSpaceRGB::ChannelContent BayerPatternMapYX[2][2];
                            if (LoadBayerPatternMap(pResponceFunctionChannelRed->GetBayerPattern(), BayerPatternMapYX))
                            {
                                const CImageSize& Size = pPixelResponceImageChannelRed->GetSize();
                                const uint Width = Size.GetWidth();
                                const uint Height = Size.GetHeight();
                                CLocalRadiometricResponceFunction* pMergeResponceFunction = new CLocalRadiometricResponceFunction(Size);
                                const CLocalPixelResponce* pPixelResponcePerChannel[3] = { nullptr };
                                pPixelResponcePerChannel[ChromaticSpaceRGB::eRed] = pPixelResponceImageChannelRed->GetBeginReadOnlyBuffer();
                                pPixelResponcePerChannel[ChromaticSpaceRGB::eGreen] = pPixelResponceImageChannelGreen->GetBeginReadOnlyBuffer();
                                pPixelResponcePerChannel[ChromaticSpaceRGB::eBlue] = pPixelResponceImageChannelBlue->GetBeginReadOnlyBuffer();
                                CLocalPixelResponce* pMergePixelResponce = pMergeResponceFunction->m_pPixelResponceImage->GetBeginWritableBuffer();
                                for (uint Y = 0; Y < Height; ++Y)
                                    for (uint X = 0; X < Width; ++X, ++pMergePixelResponce, ++pPixelResponcePerChannel[ChromaticSpaceRGB::eRed], ++pPixelResponcePerChannel[ChromaticSpaceRGB::eGreen], ++pPixelResponcePerChannel[ChromaticSpaceRGB::eBlue])
                                    {
                                        *pMergePixelResponce = *pPixelResponcePerChannel[BayerPatternMapYX[Y & 0X1][X & 0X1]];
                                    }
                                return pMergeResponceFunction;
                            }
                        }
                    }
                    return nullptr;
                }

                bool CLocalRadiometricResponceFunction::SaveMergedChannels(const_string pPathFileName, const CLocalRadiometricResponceFunction* pResponceFunctionChannelRed, const CLocalRadiometricResponceFunction* pResponceFunctionChannelGreen, const CLocalRadiometricResponceFunction* pResponceFunctionChannelBlue)
                {
                    const CLocalRadiometricResponceFunction* pMergeResponceFunction = MergeChannels(pResponceFunctionChannelRed, pResponceFunctionChannelGreen, pResponceFunctionChannelBlue);
                    if (pMergeResponceFunction)
                    {
                        const bool Result = pMergeResponceFunction->SaveResponceFunctionToFile(pPathFileName);
                        RELEASE_OBJECT(pMergeResponceFunction)
                        return Result;
                    }
                    return false;
                }
                const CLocalRadiometricResponceFunction* CLocalRadiometricResponceFunction::CreateFromFile(const_string pPathFileName)
                {
                    if (Files::CFile::Exists(pPathFileName))
                    {
                        CLocalRadiometricResponceFunction* pLocalRadiometricResponceFunction = new CLocalRadiometricResponceFunction();
                        if (pLocalRadiometricResponceFunction->LoadResponceFunctionFile(pPathFileName))
                        {
                            return pLocalRadiometricResponceFunction;
                        }
                        RELEASE_OBJECT(pLocalRadiometricResponceFunction)
                    }
                    return nullptr;
                }

                CLocalRadiometricResponceFunction::CLocalRadiometricResponceFunction() :
                    m_CameraId(0), m_BayerPattern(eUnknownBayerPattern), m_ChannelContent(ChromaticSpaceRGB::eUnknownChannelContent), m_pPixelResponceImage(nullptr)
                {
                }

                CLocalRadiometricResponceFunction::CLocalRadiometricResponceFunction(const CImageSize& Size) :
                    m_CameraId(0), m_BayerPattern(eUnknownBayerPattern), m_ChannelContent(ChromaticSpaceRGB::eUnknownChannelContent), m_pPixelResponceImage(nullptr)
                {
                    if (Size.IsValid())
                    {
                        m_pPixelResponceImage = new TImage<CLocalPixelResponce> (Size);
                    }
                }

                CLocalRadiometricResponceFunction::~CLocalRadiometricResponceFunction()
                {
                    RELEASE_OBJECT(m_pPixelResponceImage)
                }

                bool CLocalRadiometricResponceFunction::IsEnabled() const
                {
                    return m_pPixelResponceImage && m_pPixelResponceImage->IsValid();
                }

                bool CLocalRadiometricResponceFunction::LoadResponceFunctionFromParameters(const DeviceIdentifier CameraId, const BayerPatternType BayerPattern, const ChromaticSpaceRGB::ChannelContent ChannelContent, const TImage<CLocalRadiometricPixel>* pLocalRadiometricImage)
                {
                    if (CameraId && (((BayerPattern != eUnknownBayerPattern) && (ChannelContent != ChromaticSpaceRGB::eUnknownChannelContent)) || ((BayerPattern == eUnknownBayerPattern) && (ChannelContent == ChromaticSpaceRGB::eUnknownChannelContent))))
                    {
                        m_CameraId = CameraId;
                        m_BayerPattern = BayerPattern;
                        m_ChannelContent = ChannelContent;
                        RELEASE_OBJECT(m_pPixelResponceImage)
                        m_pPixelResponceImage = new TImage<CLocalPixelResponce> (pLocalRadiometricImage->GetSize());
                        const CLocalRadiometricPixel* pLocalRadiometricPixel = pLocalRadiometricImage->GetBeginReadOnlyBuffer();
                        const CLocalPixelResponce* const pPixelResponceEnd = m_pPixelResponceImage->GetEndReadOnlyBuffer();
                        CLocalPixelResponce* pPixelResponce = m_pPixelResponceImage->GetBeginWritableBuffer();
                        while (pPixelResponce < pPixelResponceEnd)
                        {
                            pPixelResponce->SetFromRadiometricPixel(pLocalRadiometricPixel++);
                            ++pPixelResponce;
                        }
                        return true;
                    }
                    return false;
                }

                bool CLocalRadiometricResponceFunction::LoadResponceFunctionFile(const_string pPathFileName)
                {
                    if (Files::CFile::Exists(pPathFileName))
                    {
                        Files::CInFile InputBinaryFile(pPathFileName, Files::CFile::eBinary);
                        if (InputBinaryFile.IsReady())
                            if (InputBinaryFile.ReadBuffer(&m_CameraId, sizeof(DeviceIdentifier)))
                                if (InputBinaryFile.ReadBuffer(&m_BayerPattern, sizeof(BayerPatternType)))
                                    if (InputBinaryFile.ReadBuffer(&m_ChannelContent, sizeof(ChromaticSpaceRGB::ChannelContent)))
                                    {
                                        CImageSize Size;
                                        if (InputBinaryFile.ReadBuffer(&Size, sizeof(CImageSize)) && Size.IsValid())
                                        {
                                            RELEASE_OBJECT(m_pPixelResponceImage)
                                            m_pPixelResponceImage = new TImage<CLocalPixelResponce> (Size);
                                            if (InputBinaryFile.ReadBuffer(m_pPixelResponceImage->GetBeginWritableBuffer(), m_pPixelResponceImage->GetBufferSize()))
                                            {
                                                return InputBinaryFile.Close();
                                            }
                                        }
                                    }
                    }
                    return false;
                }

                bool CLocalRadiometricResponceFunction::SaveResponceFunctionToFile(const_string pPathFileName) const
                {
                    if (m_pPixelResponceImage && m_pPixelResponceImage->IsValid())
                    {
                        Files::COutFile OutputBinaryFile(pPathFileName, Files::CFile::eBinary);
                        if (OutputBinaryFile.IsReady())
                            if (OutputBinaryFile.WriteBuffer(&m_CameraId, sizeof(DeviceIdentifier)))
                                if (OutputBinaryFile.WriteBuffer(&m_BayerPattern, sizeof(BayerPatternType)))
                                    if (OutputBinaryFile.WriteBuffer(&m_ChannelContent, sizeof(ChromaticSpaceRGB::ChannelContent)))
                                        if (OutputBinaryFile.WriteBuffer(&m_pPixelResponceImage->GetSize(), sizeof(CImageSize)))
                                            if (OutputBinaryFile.WriteBuffer(m_pPixelResponceImage->GetBeginReadOnlyBuffer(), m_pPixelResponceImage->GetBufferSize()))
                                            {
                                                return OutputBinaryFile.Close();
                                            }
                    }
                    return false;
                }

                bool CLocalRadiometricResponceFunction::ExportSlopeToImage(const_string pPathFileName, const Visualization::CTristimulusColorMap* pColorMap) const
                {
                    if (m_pPixelResponceImage && m_pPixelResponceImage->IsValid())
                    {
                        TImage<real> Image(m_pPixelResponceImage->GetSize());
                        real Maximal = _REAL_MIN_, Minimal = _REAL_MAX_;
                        const real* const pIntensityPixelEnd = Image.GetEndReadOnlyBuffer();
                        real* pIntensityPixel = Image.GetBeginWritableBuffer();
                        const CLocalPixelResponce* pLocalPixelResponce = m_pPixelResponceImage->GetBeginReadOnlyBuffer();
                        while (pIntensityPixel < pIntensityPixelEnd)
                        {
                            if (pLocalPixelResponce->IsValid())
                            {
                                const real Value = pLocalPixelResponce->GetSlope();
                                if (Value > Maximal)
                                {
                                    Maximal = Value;
                                }
                                if (Value < Minimal)
                                {
                                    Minimal = Value;
                                }
                                *pIntensityPixel++ = Value;
                            }
                            else
                            {
                                *pIntensityPixel++ = _REAL_MAX_;
                            }
                            ++pLocalPixelResponce;
                        }
                        return ExportImage(pPathFileName, Image, Maximal, Minimal, pColorMap);
                    }
                    return false;
                }

                bool CLocalRadiometricResponceFunction::ExportOffsetToImage(const_string pPathFileName, const Visualization::CTristimulusColorMap* pColorMap) const
                {
                    if (m_pPixelResponceImage && m_pPixelResponceImage->IsValid())
                    {
                        TImage<real> Image(m_pPixelResponceImage->GetSize());
                        real Maximal = _REAL_MIN_, Minimal = _REAL_MAX_;
                        const real* const pIntensityPixelEnd = Image.GetEndReadOnlyBuffer();
                        real* pIntensityPixel = Image.GetBeginWritableBuffer();
                        const CLocalPixelResponce* pLocalPixelResponce = m_pPixelResponceImage->GetBeginReadOnlyBuffer();
                        while (pIntensityPixel < pIntensityPixelEnd)
                        {
                            if (pLocalPixelResponce->IsValid())
                            {
                                const real Value = pLocalPixelResponce->GetOffset();
                                if (Value > Maximal)
                                {
                                    Maximal = Value;
                                }
                                if (Value < Minimal)
                                {
                                    Minimal = Value;
                                }
                                *pIntensityPixel++ = Value;
                            }
                            else
                            {
                                *pIntensityPixel++ = _REAL_MAX_;
                            }
                            ++pLocalPixelResponce;
                        }
                        return ExportImage(pPathFileName, Image, Maximal, Minimal, pColorMap);
                    }
                    return false;
                }

                bool CLocalRadiometricResponceFunction::ExportRMSDeviationToImage(const_string pPathFileName, const Visualization::CTristimulusColorMap* pColorMap) const
                {
                    if (m_pPixelResponceImage && m_pPixelResponceImage->IsValid())
                    {
                        TImage<real> Image(m_pPixelResponceImage->GetSize());
                        real Maximal = _REAL_MIN_, Minimal = _REAL_MAX_;
                        const real* const pIntensityPixelEnd = Image.GetEndReadOnlyBuffer();
                        real* pIntensityPixel = Image.GetBeginWritableBuffer();
                        const CLocalPixelResponce* pLocalPixelResponce = m_pPixelResponceImage->GetBeginReadOnlyBuffer();
                        while (pIntensityPixel < pIntensityPixelEnd)
                        {
                            if (pLocalPixelResponce->IsValid())
                            {
                                const real Value = pLocalPixelResponce->GetRMSDeviation();
                                if (Value > Maximal)
                                {
                                    Maximal = Value;
                                }
                                if (Value < Minimal)
                                {
                                    Minimal = Value;
                                }
                                *pIntensityPixel++ = Value;
                            }
                            else
                            {
                                *pIntensityPixel++ = _REAL_MAX_;
                            }
                            ++pLocalPixelResponce;
                        }
                        return ExportImage(pPathFileName, Image, Maximal, Minimal, pColorMap);
                    }
                    return false;
                }

                bool CLocalRadiometricResponceFunction::ExportPeakNegativeDeviationToImage(const_string pPathFileName, const Visualization::CTristimulusColorMap* pColorMap) const
                {
                    if (m_pPixelResponceImage && m_pPixelResponceImage->IsValid())
                    {
                        TImage<real> Image(m_pPixelResponceImage->GetSize());
                        real Maximal = _REAL_MIN_, Minimal = _REAL_MAX_;
                        const real* const pIntensityPixelEnd = Image.GetEndReadOnlyBuffer();
                        real* pIntensityPixel = Image.GetBeginWritableBuffer();
                        const CLocalPixelResponce* pLocalPixelResponce = m_pPixelResponceImage->GetBeginReadOnlyBuffer();
                        while (pIntensityPixel < pIntensityPixelEnd)
                        {
                            if (pLocalPixelResponce->IsValid())
                            {
                                const real Value = pLocalPixelResponce->GetPeakNegativeDeviation();
                                if (Value > Maximal)
                                {
                                    Maximal = Value;
                                }
                                if (Value < Minimal)
                                {
                                    Minimal = Value;
                                }
                                *pIntensityPixel++ = Value;
                            }
                            else
                            {
                                *pIntensityPixel++ = _REAL_MAX_;
                            }
                            ++pLocalPixelResponce;
                        }
                        return ExportImage(pPathFileName, Image, Maximal, Minimal, pColorMap);
                    }
                    return false;
                }

                bool CLocalRadiometricResponceFunction::ExportPeakPositiveDeviationToImage(const_string pPathFileName, const Visualization::CTristimulusColorMap* pColorMap) const
                {
                    if (m_pPixelResponceImage && m_pPixelResponceImage->IsValid())
                    {
                        TImage<real> Image(m_pPixelResponceImage->GetSize());
                        real Maximal = _REAL_MIN_, Minimal = _REAL_MAX_;
                        const real* const pIntensityPixelEnd = Image.GetEndReadOnlyBuffer();
                        real* pIntensityPixel = Image.GetBeginWritableBuffer();
                        const CLocalPixelResponce* pLocalPixelResponce = m_pPixelResponceImage->GetBeginReadOnlyBuffer();
                        while (pIntensityPixel < pIntensityPixelEnd)
                        {
                            if (pLocalPixelResponce->IsValid())
                            {
                                const real Value = pLocalPixelResponce->GetPeakPositiveDeviation();
                                if (Value > Maximal)
                                {
                                    Maximal = Value;
                                }
                                if (Value < Minimal)
                                {
                                    Minimal = Value;
                                }
                                *pIntensityPixel++ = Value;
                            }
                            else
                            {
                                *pIntensityPixel++ = _REAL_MAX_;
                            }
                            ++pLocalPixelResponce;
                        }
                        return ExportImage(pPathFileName, Image, Maximal, Minimal, pColorMap);
                    }
                    return false;
                }

                bool CLocalRadiometricResponceFunction::ExportRangeDeviationToImage(const_string pPathFileName, const Visualization::CTristimulusColorMap* pColorMap) const
                {
                    if (m_pPixelResponceImage && m_pPixelResponceImage->IsValid())
                    {
                        TImage<real> Image(m_pPixelResponceImage->GetSize());
                        real Maximal = _REAL_MIN_, Minimal = _REAL_MAX_;
                        const real* const pIntensityPixelEnd = Image.GetEndReadOnlyBuffer();
                        real* pIntensityPixel = Image.GetBeginWritableBuffer();
                        const CLocalPixelResponce* pLocalPixelResponce = m_pPixelResponceImage->GetBeginReadOnlyBuffer();
                        while (pIntensityPixel < pIntensityPixelEnd)
                        {
                            if (pLocalPixelResponce->IsValid())
                            {
                                const real Value = pLocalPixelResponce->GetRangeDeviation();
                                if (Value > Maximal)
                                {
                                    Maximal = Value;
                                }
                                if (Value < Minimal)
                                {
                                    Minimal = Value;
                                }
                                *pIntensityPixel++ = Value;
                            }
                            else
                            {
                                *pIntensityPixel++ = _REAL_MAX_;
                            }
                            ++pLocalPixelResponce;
                        }
                        return ExportImage(pPathFileName, Image, Maximal, Minimal, pColorMap);
                    }
                    return false;
                }

                DeviceIdentifier CLocalRadiometricResponceFunction::GetCameraId() const
                {
                    return m_CameraId;
                }

                BayerPatternType CLocalRadiometricResponceFunction::GetBayerPattern() const
                {
                    return m_BayerPattern;
                }

                ChromaticSpaceRGB::ChannelContent CLocalRadiometricResponceFunction::GetChannelContent() const
                {
                    return m_ChannelContent;
                }

                const TImage<CLocalPixelResponce>* CLocalRadiometricResponceFunction::GetLocalRadiometricResponceImage() const
                {
                    return m_pPixelResponceImage;
                }

                bool CLocalRadiometricResponceFunction::ExportImage(const_string pPathFileName, const TImage<real>& Image, const real Maximal, const real Minimal, const Visualization::CTristimulusColorMap* pColorMap) const
                {
                    const real Range = Maximal - Minimal;
                    if (pPathFileName && Image.IsValid() && pColorMap && (Range > _REAL_EPSILON_))
                    {
                        TImage<CDiscreteTristimulusPixel> ExportingImage(Image.GetSize());
                        ExportingImage.Clear();
                        const CDiscreteTristimulusPixel* const pRGBPixelEnd = ExportingImage.GetEndReadOnlyBuffer();
                        CDiscreteTristimulusPixel* pRGBPixel = ExportingImage.GetBeginWritableBuffer();
                        const CDiscreteTristimulusPixel* pLUT = pColorMap->GetColorLookupTable();
                        const real* pIntenistyPixel = Image.GetBeginReadOnlyBuffer();
                        const real Scale = pColorMap->GetColorLookupTableScaleFactor() / Range;
                        while (pRGBPixel < pRGBPixelEnd)
                        {
                            if (*pIntenistyPixel != _REAL_MAX_)
                            {
                                *pRGBPixel = pLUT[RoundPositiveRealToInteger((*pIntenistyPixel - Minimal) * Scale)];
                            }
                            ++pIntenistyPixel;
                            ++pRGBPixel;
                        }
                        return CImageExporter::ExportToBMP(pPathFileName, &ExportingImage);
                    }
                    return false;
                }
            }
        }
    }
}
