/*
 * LocalPixelResponce.cpp
 *
 *  Created on: 27.04.2012
 *      Author: gonzalez
 */

#include "LocalPixelResponce.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                CLocalPixelResponce::CLocalPixelResponce() :
                    m_Slope(_REAL_ZERO_), m_Offset(_REAL_ZERO_), m_RMSDeviation(_REAL_ZERO_), m_PeakNegativeDeviation(_REAL_ZERO_), m_PeakPositiveDeviation(_REAL_ZERO_)
                {
                }

                CLocalPixelResponce::~CLocalPixelResponce()
                    = default;

                bool CLocalPixelResponce::IsValid() const
                {
                    return m_Slope > _REAL_EPSILON_;
                }

                bool CLocalPixelResponce::SetFromRadiometricPixel(const CLocalRadiometricPixel* pLocalRadiometricPixel)
                {
                    if (pLocalRadiometricPixel)
                    {
                        m_Slope = pLocalRadiometricPixel->GetSlope();
                        m_Offset = pLocalRadiometricPixel->GetOffset();
                        m_RMSDeviation = pLocalRadiometricPixel->GetRMSDeviation();
                        m_PeakNegativeDeviation = pLocalRadiometricPixel->GetPeakNegativeDeviation();
                        m_PeakPositiveDeviation = pLocalRadiometricPixel->GetPeakPositiveDeviation();
                        return true;
                    }
                    return false;
                }

                real CLocalPixelResponce::GetSlope() const
                {
                    return m_Slope;
                }

                real CLocalPixelResponce::GetOffset() const
                {
                    return m_Offset;
                }

                real CLocalPixelResponce::GetRMSDeviation() const
                {
                    return m_RMSDeviation;
                }

                real CLocalPixelResponce::GetRangeDeviation() const
                {
                    return m_PeakPositiveDeviation - m_PeakNegativeDeviation;
                }

                real CLocalPixelResponce::GetPeakNegativeDeviation() const
                {
                    return m_PeakNegativeDeviation;
                }

                real CLocalPixelResponce::GetPeakPositiveDeviation() const
                {
                    return m_PeakPositiveDeviation;
                }
            }
        }
    }
}

