/*
 * LocalRadiometricResponceFunction.h
 *
 *  Created on: 27.04.2012
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/DataTypes/BayerPattern.h"
#include "../../../../Foundation/DataTypes/ColorSpaces.h"
#include "../../../../Foundation/Files/InFile.h"
#include "../../../../Foundation/Files/OutFile.h"
#include "../../../../Visualization/Miscellaneous/TristimulusColorMap.h"
#include "../../../Common/Image.h"
#include "../../../Common/ImageExporter.h"
#include "LocalRadiometricPixel.h"
#include "LocalPixelResponce.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CLocalRadiometricResponceFunction
                {
                public:

                    static const CLocalRadiometricResponceFunction* MergeChannels(const CLocalRadiometricResponceFunction* pResponceFunctionChannelRed, const CLocalRadiometricResponceFunction* pResponceFunctionChannelGreen, const CLocalRadiometricResponceFunction* pResponceFunctionChannelBlue);
                    static  bool SaveMergedChannels(const_string pPathFileName, const CLocalRadiometricResponceFunction* pResponceFunctionChannelRed, const CLocalRadiometricResponceFunction* pResponceFunctionChannelGreen, const CLocalRadiometricResponceFunction* pResponceFunctionChannelBlue);
                    static const CLocalRadiometricResponceFunction* CreateFromFile(const_string pPathFileName);

                    CLocalRadiometricResponceFunction();
                    virtual ~CLocalRadiometricResponceFunction();

                    bool IsEnabled() const;
                    bool LoadResponceFunctionFromParameters(const DeviceIdentifier CameraId, const BayerPatternType BayerPattern, const ChromaticSpaceRGB::ChannelContent ChannelContent, const TImage<CLocalRadiometricPixel>* pLocalRadiometricImage);
                    bool LoadResponceFunctionFile(const_string pPathFileName);
                    bool SaveResponceFunctionToFile(const_string pPathFileName) const;

                    bool ExportSlopeToImage(const_string pPathFileName, const Visualization::CTristimulusColorMap* pColorMap) const;
                    bool ExportOffsetToImage(const_string pPathFileName, const Visualization::CTristimulusColorMap* pColorMap) const;
                    bool ExportRMSDeviationToImage(const_string pPathFileName, const Visualization::CTristimulusColorMap* pColorMap) const;
                    bool ExportPeakNegativeDeviationToImage(const_string pPathFileName, const Visualization::CTristimulusColorMap* pColorMap) const;
                    bool ExportPeakPositiveDeviationToImage(const_string pPathFileName, const Visualization::CTristimulusColorMap* pColorMap) const;
                    bool ExportRangeDeviationToImage(const_string pPathFileName, const Visualization::CTristimulusColorMap* pColorMap) const;

                    DeviceIdentifier GetCameraId() const;
                    BayerPatternType GetBayerPattern() const;
                    ChromaticSpaceRGB::ChannelContent GetChannelContent() const;
                    const TImage<CLocalPixelResponce>* GetLocalRadiometricResponceImage() const;

                protected:

                    bool ExportImage(const_string pPathFileName, const TImage<real>& Image, const real Maximal, const real Minimal, const Visualization::CTristimulusColorMap* pColorMap) const;

                    CLocalRadiometricResponceFunction(const CImageSize& Size);

                    DeviceIdentifier m_CameraId;
                    BayerPatternType m_BayerPattern;
                    ChromaticSpaceRGB::ChannelContent m_ChannelContent;
                    TImage<CLocalPixelResponce>* m_pPixelResponceImage;
                };
            }
        }
    }
}

