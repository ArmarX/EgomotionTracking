/*
 * LocalRadiometricCalibration.cpp
 *
 *  Created on: Apr 17, 2012
 *      Author: david
 */

#include "LocalRadiometricCalibration.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                CLocalRadiometricCalibration::CLocalRadiometricCalibration(const CGlobalRadiometricResponceFunction* pGlobalRadiometricResponceFunction, const TImage<real>* pRadianceImage) :
                    m_pGlobalRadiometricResponceFunction(pGlobalRadiometricResponceFunction), m_pRadianceImage(pRadianceImage), m_pRadianceImageIntern(nullptr), m_pRadianceReferenceImage(nullptr), m_pWeightedRadianceAccumulatorImage(nullptr), m_pWeightingAccumulatorImage(nullptr), m_pRadiometricImage(nullptr), m_pLocalRadiometricResponceFunction(nullptr)
                {
                    if (m_pGlobalRadiometricResponceFunction && m_pGlobalRadiometricResponceFunction->IsIdealResponceFunctionReady() && m_pRadianceImage && m_pRadianceImage->IsValid())
                    {
                        m_RadianceWeigthingKernel = m_pGlobalRadiometricResponceFunction->GetRadianceWeigthingKernel();
                        const uint Width = m_pRadianceImage->GetWidth();
                        const uint Height = m_pRadianceImage->GetHeight();
                        m_pRadianceImageIntern = new TImage<real>(Width, Height);
                        m_pRadianceReferenceImage = new TImage<real>(Width, Height);
                        m_pWeightedRadianceAccumulatorImage = new TImage<real>(Width, Height);
                        m_pWeightingAccumulatorImage = new TImage<real>(Width, Height);
                        m_pRadiometricImage = new TImage<CLocalRadiometricPixel>(Width, Height);
                        const real* pRadianceReferencePixel = m_pRadianceReferenceImage->GetBeginReadOnlyBuffer();
                        const real* pRadiancePixel = m_pRadianceImageIntern->GetBeginReadOnlyBuffer();
                        CLocalRadiometricPixel* pLocalRadiometricPixel = m_pRadiometricImage->GetBeginWritableBuffer();
                        const BayerPatternType BayerPattern = m_pGlobalRadiometricResponceFunction->GetBayerPattern();
                        if (BayerPattern == eUnknownBayerPattern)
                        {
                            for (uint Y = 0; Y < Height; ++Y)
                                for (uint X = 0; X < Width; ++X, ++pLocalRadiometricPixel, ++pRadiancePixel, ++pRadianceReferencePixel)
                                {
                                    m_RadiometricPixels.push_back(pLocalRadiometricPixel->Initialize(X, Y, pRadiancePixel, pRadianceReferencePixel));
                                }
                        }
                        else
                        {
                            bool FlagMapYX[2][2];
                            if (LoadBayerPatternFlagMap(BayerPattern, m_pGlobalRadiometricResponceFunction->GetChannelContent(), FlagMapYX))
                                for (uint Y = 0; Y < Height; ++Y)
                                    for (uint X = 0; X < Width; ++X, ++pLocalRadiometricPixel, ++pRadiancePixel, ++pRadianceReferencePixel)
                                        if (FlagMapYX[Y & 0X1][X & 0X1])
                                        {
                                            m_RadiometricPixels.push_back(pLocalRadiometricPixel->Initialize(X, Y, pRadiancePixel, pRadianceReferencePixel));
                                        }
                        }
                    }
                }

                CLocalRadiometricCalibration::~CLocalRadiometricCalibration()
                {
                    ClearSamples();
                    RELEASE_OBJECT(m_pRadianceImageIntern)
                    RELEASE_OBJECT(m_pRadianceReferenceImage)
                    RELEASE_OBJECT(m_pWeightedRadianceAccumulatorImage)
                    RELEASE_OBJECT(m_pWeightingAccumulatorImage)
                    RELEASE_OBJECT(m_pRadiometricImage)
                    RELEASE_OBJECT(m_pLocalRadiometricResponceFunction)
                }

                bool CLocalRadiometricCalibration::IsEnabled() const
                {
                    return m_RadiometricPixels.size();
                }

                DeviceIdentifier CLocalRadiometricCalibration::GetCameraId() const
                {
                    return m_pGlobalRadiometricResponceFunction ? m_pGlobalRadiometricResponceFunction->GetCameraId() : 0;
                }

                BayerPatternType CLocalRadiometricCalibration::GetBayerPattern() const
                {
                    return m_pGlobalRadiometricResponceFunction ? m_pGlobalRadiometricResponceFunction->GetBayerPattern() : eUnknownBayerPattern;
                }

                ChromaticSpaceRGB::ChannelContent CLocalRadiometricCalibration::GetChannelContent() const
                {
                    return m_pGlobalRadiometricResponceFunction ? m_pGlobalRadiometricResponceFunction->GetChannelContent() : ChromaticSpaceRGB::eUnknownChannelContent;
                }

                const CLocalRadiometricResponceFunction* CLocalRadiometricCalibration::GetLocalRadiometricResponceFunction() const
                {
                    return m_pLocalRadiometricResponceFunction;
                }

                bool CLocalRadiometricCalibration::AddExposure(const uint ReferenceIndex, const uint ExposureIndex, const real ExposureTime)
                {
                    if (m_RadiometricPixels.size())
                    {
                        ReferenceExposureImage Sample = { ReferenceIndex, ExposureIndex, ExposureTime, m_pRadianceImage->Clone(true) };
                        m_ReferenceExposureImages.push_back(Sample);
                        return true;
                    }
                    return false;
                }

                bool CLocalRadiometricCalibration::ClearExposures()
                {
                    ClearSamples();
                    if (m_RadiometricPixels.size())
                    {
                        list<CLocalRadiometricPixel*>::const_iterator RadiometricCellsEnd = m_RadiometricPixels.end();
                        for (list<CLocalRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin(); ppRadiometricCell != RadiometricCellsEnd; ++ppRadiometricCell)
                        {
                            (*ppRadiometricCell)->ClearExposures();
                        }
                        return true;
                    }
                    return false;
                }

                bool CLocalRadiometricCalibration::SetGaussianBoxKernelRadius(const real GaussianBoxKernelRadius)
                {
                    return m_RadianceWeigthingKernel.SetGaussianBoxKernelRadius(GaussianBoxKernelRadius);
                }

                bool CLocalRadiometricCalibration::SetCommonGaussianKernelStandardDeviation(const real StandardDeviation)
                {
                    return m_RadianceWeigthingKernel.SetCommonGaussianKernelStandardDeviation(StandardDeviation);
                }

                bool CLocalRadiometricCalibration::SetCommonGaussianKernelCutOff(const real CutOffDensity, const real CutOffNormalizedDeviation)
                {
                    return m_RadianceWeigthingKernel.SetCommonGaussianKernelCutOff(CutOffDensity, CutOffNormalizedDeviation);
                }

                real CLocalRadiometricCalibration::GetGaussianBoxKernelRadius() const
                {
                    return m_RadianceWeigthingKernel.GetGaussianBoxKernelRadius();
                }

                real CLocalRadiometricCalibration::GetCommonGaussianKernelStandardDeviation() const
                {
                    return m_RadianceWeigthingKernel.GetCommonGaussianKernelStandardDeviation();
                }

                bool CLocalRadiometricCalibration::Calibrate(const CRadianceWeigthingKernel::KernelType Kernel)
                {
                    if (LoadSamples(Kernel))
                    {
                        list<CLocalRadiometricPixel*>::const_iterator RadiometricCellsEnd = m_RadiometricPixels.end();
                        for (list<CLocalRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin(); ppRadiometricCell != RadiometricCellsEnd; ++ppRadiometricCell)
                        {
                            (*ppRadiometricCell)->Calibrate(m_RadianceWeigthingKernel, Kernel);
                        }
                        ClearSamples();
                        RELEASE_OBJECT(m_pLocalRadiometricResponceFunction)
                        m_pLocalRadiometricResponceFunction = new CLocalRadiometricResponceFunction();
                        return m_pLocalRadiometricResponceFunction->LoadResponceFunctionFromParameters(m_pGlobalRadiometricResponceFunction->GetCameraId(), m_pGlobalRadiometricResponceFunction->GetBayerPattern(), m_pGlobalRadiometricResponceFunction->GetChannelContent(), m_pRadiometricImage);
                    }
                    return false;
                }

                bool CLocalRadiometricCalibration::LoadSamples(const CRadianceWeigthingKernel::KernelType Kernel)
                {
                    if (m_ReferenceExposureImages.size() >= _LOCAL_RADIOMETRIC_CALIBRATION_MINIMAL_EXPOSURES_)
                    {
                        m_ReferenceExposureImages.sort(SortReferenceExposureImages);
                        ClearSynthesize();
                        list<ReferenceExposureImage> BlockExposureImages;
                        uint CurrentReferenceIndex = m_ReferenceExposureImages.front().m_ReferenceIndex;
                        list<ReferenceExposureImage>::const_iterator ReferenceExposureImagesEnd = m_ReferenceExposureImages.end();
                        for (list<ReferenceExposureImage>::const_iterator pReferenceExposureImage = m_ReferenceExposureImages.begin(), pNextReferenceExposureImage = ++m_ReferenceExposureImages.begin(); pReferenceExposureImage != ReferenceExposureImagesEnd; ++pReferenceExposureImage, ++pNextReferenceExposureImage)
                        {
                            AddSynthesizeExposure(pReferenceExposureImage->m_pRadianceImage, pReferenceExposureImage->m_ExposureTime, Kernel);
                            BlockExposureImages.push_back(*pReferenceExposureImage);
                            if ((pNextReferenceExposureImage->m_ReferenceIndex != CurrentReferenceIndex) || (pNextReferenceExposureImage == ReferenceExposureImagesEnd))
                            {
                                Synthesize();
                                LoadReferencedSamples(BlockExposureImages);
                                BlockExposureImages.clear();
                                ClearSynthesize();
                                if (pNextReferenceExposureImage != ReferenceExposureImagesEnd)
                                {
                                    CurrentReferenceIndex = pNextReferenceExposureImage->m_ReferenceIndex;
                                }
                            }
                        }
                        return true;
                    }
                    return false;
                }

                void CLocalRadiometricCalibration::ClearSamples()
                {
                    list<ReferenceExposureImage>::iterator ReferenceExposureImagesEnd = m_ReferenceExposureImages.end();
                    for (list<ReferenceExposureImage>::iterator pReferenceExposureImage = m_ReferenceExposureImages.begin(); pReferenceExposureImage != ReferenceExposureImagesEnd; ++pReferenceExposureImage)
                        RELEASE_OBJECT_DIRECT(pReferenceExposureImage->m_pRadianceImage)
                        m_ReferenceExposureImages.clear();
                }

                void CLocalRadiometricCalibration::LoadReferencedSamples(list<ReferenceExposureImage>& BlockExposureImages)
                {
                    list<CLocalRadiometricPixel*>::const_iterator RadiometricCellsEnd = m_RadiometricPixels.end();
                    list<CLocalRadiometricPixel*>::const_iterator RadiometricCellsBegin = m_RadiometricPixels.begin();
                    list<ReferenceExposureImage>::iterator ReferenceExposureImageEnd = BlockExposureImages.end();
                    for (list<ReferenceExposureImage>::iterator pReferenceExposureImage = BlockExposureImages.begin(); pReferenceExposureImage != ReferenceExposureImageEnd; ++pReferenceExposureImage)
                    {
                        const real ExposureTime = pReferenceExposureImage->m_ExposureTime;
                        m_pRadianceImageIntern->Copy(pReferenceExposureImage->m_pRadianceImage);
                        for (list<CLocalRadiometricPixel*>::const_iterator ppRadiometricCell = RadiometricCellsBegin; ppRadiometricCell != RadiometricCellsEnd; ++ppRadiometricCell)
                        {
                            (*ppRadiometricCell)->AddExposure(ExposureTime);
                        }
                    }
                }

                void CLocalRadiometricCalibration::AddSynthesizeExposure(const TImage<real>* pRadianceImage, const real ExposureTime, const CRadianceWeigthingKernel::KernelType Kernel)
                {
                    const uint Width = m_pRadianceImage->GetWidth();
                    const uint Height = m_pRadianceImage->GetHeight();
                    const real* pRadiancePixel = pRadianceImage->GetBeginReadOnlyBuffer();
                    real* pWeightedRadianceAccumulatorPixel = m_pWeightedRadianceAccumulatorImage->GetBeginWritableBuffer();
                    real* pWeightingAccumulatorPixel = m_pWeightingAccumulatorImage->GetBeginWritableBuffer();
                    const real ExposureSlope = m_pGlobalRadiometricResponceFunction->GetIntensitySlope() / ExposureTime;
                    const real ExposureOffset = m_pGlobalRadiometricResponceFunction->GetIntensityOffset() / ExposureTime;
                    const real MinimalIntensity = m_pGlobalRadiometricResponceFunction->GetMinimalIntensity();
                    const BayerPatternType BayerPattern = m_pGlobalRadiometricResponceFunction->GetBayerPattern();
                    if (BayerPattern == eUnknownBayerPattern)
                    {
                        for (uint Y = 0; Y < Height; ++Y)
                            for (uint X = 0; X < Width; ++X, ++pRadiancePixel, ++pWeightedRadianceAccumulatorPixel, ++pWeightingAccumulatorPixel)
                                if (*pRadiancePixel > MinimalIntensity)
                                {
                                    const real RegressionWeigthing = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(*pRadiancePixel, Kernel);
                                    *pWeightingAccumulatorPixel += RegressionWeigthing;
                                    *pWeightedRadianceAccumulatorPixel += ((*pRadiancePixel * ExposureSlope) + ExposureOffset) * RegressionWeigthing;
                                }
                    }
                    else
                    {
                        bool FlagMapYX[2][2];
                        if (LoadBayerPatternFlagMap(BayerPattern, m_pGlobalRadiometricResponceFunction->GetChannelContent(), FlagMapYX))
                            for (uint Y = 0; Y < Height; ++Y)
                                for (uint X = 0; X < Width; ++X, ++pRadiancePixel, ++pWeightedRadianceAccumulatorPixel, ++pWeightingAccumulatorPixel)
                                    if (FlagMapYX[Y & 0X1][X & 0X1] && (*pRadiancePixel > MinimalIntensity))
                                    {
                                        const real RegressionWeigthing = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(*pRadiancePixel, Kernel);
                                        *pWeightingAccumulatorPixel += RegressionWeigthing;
                                        *pWeightedRadianceAccumulatorPixel += ((*pRadiancePixel * ExposureSlope) + ExposureOffset) * RegressionWeigthing;
                                    }
                    }
                }

                void CLocalRadiometricCalibration::Synthesize()
                {
                    const uint Width = m_pRadianceImage->GetWidth();
                    const uint Height = m_pRadianceImage->GetHeight();
                    const real* pWeightedRadianceAccumulatorPixel = m_pWeightedRadianceAccumulatorImage->GetBeginReadOnlyBuffer();
                    const real* pWeightingAccumulatorPixel = m_pWeightingAccumulatorImage->GetBeginReadOnlyBuffer();
                    real* pRadianceReferencePixel = m_pRadianceReferenceImage->GetBeginWritableBuffer();
                    const BayerPatternType BayerPattern = m_pGlobalRadiometricResponceFunction->GetBayerPattern();
                    if (BayerPattern == eUnknownBayerPattern)
                    {
                        for (uint Y = 0; Y < Height; ++Y)
                            for (uint X = 0; X < Width; ++X, ++pWeightedRadianceAccumulatorPixel, ++pWeightingAccumulatorPixel, ++pRadianceReferencePixel)
                            {
                                *pRadianceReferencePixel = (*pWeightingAccumulatorPixel > _REAL_EPSILON_) ? (*pWeightedRadianceAccumulatorPixel / *pWeightingAccumulatorPixel) : _REAL_ONE_;
                            }
                    }
                    else
                    {
                        bool FlagMapYX[2][2];
                        if (LoadBayerPatternFlagMap(BayerPattern, m_pGlobalRadiometricResponceFunction->GetChannelContent(), FlagMapYX))
                            for (uint Y = 0; Y < Height; ++Y)
                                for (uint X = 0; X < Width; ++X, ++pWeightedRadianceAccumulatorPixel, ++pWeightingAccumulatorPixel, ++pRadianceReferencePixel)
                                    if (FlagMapYX[Y & 0X1][X & 0X1])
                                    {
                                        *pRadianceReferencePixel = (*pWeightingAccumulatorPixel > _REAL_EPSILON_) ? (*pWeightedRadianceAccumulatorPixel / *pWeightingAccumulatorPixel) : _REAL_ONE_;
                                    }
                    }
                }

                void CLocalRadiometricCalibration::ClearSynthesize()
                {
                    m_pWeightedRadianceAccumulatorImage->Clear();
                    m_pWeightingAccumulatorImage->Clear();
                    m_pRadianceReferenceImage->Clear();
                }

                bool CLocalRadiometricCalibration::SortReferenceExposureImages(const ReferenceExposureImage& lhs, const ReferenceExposureImage& rhs)
                {
                    if (lhs.m_ReferenceIndex == rhs.m_ReferenceIndex)
                    {
                        return lhs.m_ExposureIndex < rhs.m_ExposureIndex;
                    }
                    return lhs.m_ReferenceIndex < rhs.m_ReferenceIndex;
                }
            }
        }
    }
}
