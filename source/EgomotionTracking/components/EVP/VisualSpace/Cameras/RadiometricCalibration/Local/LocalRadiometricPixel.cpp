/*
 * LocalRadiometricPixel.cpp
 *
 *  Created on: 21.09.2011
 *      Author: gonzalez
 */

#include "LocalRadiometricPixel.h"
#include "LocalRadiometricCalibration.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {

                CLocalRadiometricPixel::CLocalRadiometricPixel() :
                    m_Location(), m_pRadiance(nullptr), m_pRadianceReference(nullptr), m_Slope(_REAL_ZERO_), m_Offset(_REAL_ZERO_), m_RMSDeviation(_REAL_ZERO_), m_PeakNegativeDeviation(_REAL_ZERO_), m_PeakPositiveDeviation(_REAL_ZERO_)
                {
                }

                CLocalRadiometricPixel::~CLocalRadiometricPixel()
                    = default;

                CLocalRadiometricPixel* CLocalRadiometricPixel::Initialize(const coordinate X, const coordinate Y, const real* pRadiance, const real* pRadianceReference)
                {
                    m_pRadiance = pRadiance;
                    m_pRadianceReference = pRadianceReference;
                    m_Location.Set(X, Y);
                    return this;
                }

                void CLocalRadiometricPixel::AddExposure(const real ExposureTime)
                {
                    if ((*m_pRadiance > real(2.0)) && (*m_pRadiance < real(253.0)))
                    {
                        RadianceReferencedSample Sample = { *m_pRadiance, *m_pRadianceReference, ExposureTime };
                        m_RadianceReferencedSamples.push_back(Sample);
                    }
                }

                void CLocalRadiometricPixel::ClearExposures()
                {
                    m_RadianceReferencedSamples.clear();
                }

                bool CLocalRadiometricPixel::Calibrate(const CRadianceWeigthingKernel& RadianceWeigthingKernel, const CRadianceWeigthingKernel::KernelType Kernel)
                {
                    const uint TotalSamples = m_RadianceReferencedSamples.size();
                    if (TotalSamples >= _LOCAL_RADIOMETRIC_CALIBRATION_MINIMAL_EXPOSURES_)
                    {
                        list<RadianceReferencedSample>::const_iterator pRadianceReferencedSample = m_RadianceReferencedSamples.begin();

                        //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN


                        Mathematics::ND::LinearAlgebra::CMatrixND A(TotalSamples, 2);
                        Mathematics::ND::LinearAlgebra::CMatrixND B(TotalSamples, 1);
                        Mathematics::ND::LinearAlgebra::CMatrixND W(TotalSamples, TotalSamples);
                        W.setZero();
                        for (uint Row = 0; Row < TotalSamples; ++Row, ++pRadianceReferencedSample)
                        {
                            const real X = pRadianceReferencedSample->m_RadianceReference * pRadianceReferencedSample->m_ExposureTime;
                            const real I = pRadianceReferencedSample->m_Radiance;
                            A(Row, 0) = X;
                            A(Row, 1) = _REAL_ONE_;
                            B(Row, 0) = I;
                            W(Row, Row) = RadianceWeigthingKernel.GetRegressionWeigthingByKernel(pRadianceReferencedSample->m_Radiance, Kernel);
                        }
                        //                      try
                        //                      {
                        Mathematics::ND::LinearAlgebra::CMatrixND S = A.transpose() * W * A;
                        Mathematics::ND::LinearAlgebra::CMatrixND X = S.inverse() * (A.transpose() * W * B);
                        m_Slope = X(0, 0);
                        m_Offset = X(1, 0);
                        m_RMSDeviation = _REAL_ZERO_;
                        m_PeakNegativeDeviation = _REAL_ZERO_;
                        m_PeakPositiveDeviation = _REAL_ZERO_;
                        pRadianceReferencedSample = m_RadianceReferencedSamples.begin();
                        for (uint i = 0; i < TotalSamples; ++i, ++pRadianceReferencedSample)
                        {
                            const real Energy = pRadianceReferencedSample->m_RadianceReference * pRadianceReferencedSample->m_ExposureTime;
                            const real ModelRadiance = (m_Slope * Energy + m_Offset);
                            const real Deviation = ModelRadiance - pRadianceReferencedSample->m_Radiance;
                            //g_ConsoleStringOutput << Energy << "\t" << pRadianceReferencedSample->m_Radiance << "\t" << ModelRadiance << "\t" << RealAbs(Deviation) << g_EndLine;
                            if (Deviation < m_PeakNegativeDeviation)
                            {
                                m_PeakNegativeDeviation = Deviation;
                            }
                            if (Deviation > m_PeakPositiveDeviation)
                            {
                                m_PeakPositiveDeviation = Deviation;
                            }
                            m_RMSDeviation += Deviation * Deviation;
                        }
                        m_RMSDeviation = RealSqrt(m_RMSDeviation / real(TotalSamples));
                        return true;
                        //                      } catch (Mathematics::ND::LinearAlgebra::CMatrixBaseException& E)
                        //                      {
                        //                          g_ErrorStringOutput << E.what() << g_EndLine;
                        //                      }
                    }
                    m_Slope = _REAL_ZERO_;
                    m_Offset = _REAL_ZERO_;
                    m_RMSDeviation = _REAL_ZERO_;
                    return false;
                }

                const CPixelLocation& CLocalRadiometricPixel::GetLocation() const
                {
                    return m_Location;
                }

                real CLocalRadiometricPixel::GetSlope() const
                {
                    return m_Slope;
                }

                real CLocalRadiometricPixel::GetOffset() const
                {
                    return m_Offset;
                }

                real CLocalRadiometricPixel::GetRMSDeviation() const
                {
                    return m_RMSDeviation;
                }

                real CLocalRadiometricPixel::GetRangeDeviation() const
                {
                    return m_PeakPositiveDeviation - m_PeakNegativeDeviation;
                }

                real CLocalRadiometricPixel::GetPeakNegativeDeviation() const
                {
                    return m_PeakNegativeDeviation;
                }

                real CLocalRadiometricPixel::GetPeakPositiveDeviation() const
                {
                    return m_PeakPositiveDeviation;
                }
            }
        }
    }
}
