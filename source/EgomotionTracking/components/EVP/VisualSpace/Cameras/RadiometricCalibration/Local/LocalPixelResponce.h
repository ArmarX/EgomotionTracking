/*
 * LocalPixelResponce.h
 *
 *  Created on: 27.04.2012
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "LocalRadiometricPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CLocalPixelResponce
                {
                public:

                    CLocalPixelResponce();
                    virtual ~CLocalPixelResponce();

                    bool IsValid() const;
                    bool SetFromRadiometricPixel(const CLocalRadiometricPixel* pLocalRadiometricPixel);

                    real GetSlope() const;
                    real GetOffset() const;
                    real GetRMSDeviation() const;
                    real GetRangeDeviation() const;
                    real GetPeakNegativeDeviation() const;
                    real GetPeakPositiveDeviation() const;

                protected:

                    real m_Slope;
                    real m_Offset;
                    real m_RMSDeviation;
                    real m_PeakNegativeDeviation;
                    real m_PeakPositiveDeviation;
                };
            }
        }
    }
}

