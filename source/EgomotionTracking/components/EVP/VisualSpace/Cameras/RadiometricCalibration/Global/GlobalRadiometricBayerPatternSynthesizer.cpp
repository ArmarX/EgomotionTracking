/*
 * GlobalRadiometricBayerPatternSynthesizer.cpp
 *
 *  Created on: 12.05.2012
 *      Author: gonzalez
 */

#include "GlobalRadiometricBayerPatternSynthesizer.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                CGlobalRadiometricBayerPatternSynthesizer::CGlobalRadiometricBayerPatternSynthesizer(const TImage<real>* pContinousSourceImage, const CGlobalMultipleChannelRadiometricResponceFunction* pGlobalMultipleChannelRadiometricResponceFunction, const list<CExposure>& Exposures, const uint SamplesPerUnit) :
                    m_Enabled(false), m_Exposures(EnsureExposuresOrder(Exposures)), m_pDiscreteSourceImage(nullptr), m_pContinousSourceImage(pContinousSourceImage), m_pGlobalMultipleChannelRadiometricResponceFunction(pGlobalMultipleChannelRadiometricResponceFunction), m_pDensityWeightedLogRadianceImage(nullptr), m_pDensityWeightedImage(nullptr), m_pLogRadianceImage(nullptr), m_pDensityWeightedExposureImage(nullptr), m_pOptimalExposureImage(nullptr), m_pRedChannelLookUpTable(nullptr), m_pGreenChannelLookUpTable(nullptr), m_pBlueChannelLookUpTable(nullptr), m_pLogTimes(nullptr), m_pUnifiedExposureHistogram(nullptr), m_pRedChannelExposureHistogram(nullptr), m_pGreenChannelExposureHistogram(nullptr), m_pBlueChannelExposureHistogram(nullptr), m_TotalIntegratedExposures(0), m_MinimalExposureParametricIndex(0), m_MaximalExposureParametricIndex(0), m_LastExposureTotalIntegratedPixels(0), m_LastSynthesizeTotalUnsampledPixels(0), m_MinimalIntensity(_REAL_ZERO_), m_MaximalIntensity(_REAL_255_), m_LastExposureTotalIntegratedKernelDensity(_REAL_ZERO_), m_OptimalExposureWeighting(_REAL_ZERO_), m_OptimalExposureMode(eOptimalExposureExtractionDisabled), m_Kernel(CRadianceWeigthingKernel::eGaussian)
                {
                    if ((m_Exposures.size() == Exposures.size()) && m_pContinousSourceImage && m_pContinousSourceImage->IsValid() && m_pGlobalMultipleChannelRadiometricResponceFunction && m_pGlobalMultipleChannelRadiometricResponceFunction->AreIdealResponceFunctionsReady())
                    {
                        const CImageSize& Size = m_pContinousSourceImage->GetSize();
                        m_pDensityWeightedLogRadianceImage = new TImage<real>(Size);
                        m_pDensityWeightedImage = new TImage<real>(Size);
                        m_pLogRadianceImage = new TImage<real>(Size);
                        m_pDensityWeightedExposureImage = new TImage<real>(Size);
                        m_pOptimalExposureImage = new TImage<real>(Size);
                        m_pUnifiedExposureHistogram = new uint[256];
                        m_pRedChannelExposureHistogram = new uint[256];
                        m_pGreenChannelExposureHistogram = new uint[256];
                        m_pBlueChannelExposureHistogram = new uint[256];
                        ClearImages();
                        ClearHistograms();
                        LoadLogTimesTable();
                        LoadLookUpTables(SamplesPerUnit);
                        LoadIntensityLimits(_REAL_ONE_);
                        m_Enabled = LoadBayerPatternMap(m_pGlobalMultipleChannelRadiometricResponceFunction->GetBayerPattern(), m_BayerPatternMapYX);
                    }
                }

                CGlobalRadiometricBayerPatternSynthesizer::CGlobalRadiometricBayerPatternSynthesizer(const TImage<byte>* pDiscreteSourceImage, const CGlobalMultipleChannelRadiometricResponceFunction* pGlobalMultipleChannelRadiometricResponceFunction, const list<CExposure>& Exposures) :
                    m_Enabled(false), m_Exposures(EnsureExposuresOrder(Exposures)), m_pDiscreteSourceImage(pDiscreteSourceImage), m_pContinousSourceImage(nullptr), m_pGlobalMultipleChannelRadiometricResponceFunction(pGlobalMultipleChannelRadiometricResponceFunction), m_pDensityWeightedLogRadianceImage(nullptr), m_pDensityWeightedImage(nullptr), m_pLogRadianceImage(nullptr), m_pDensityWeightedExposureImage(nullptr), m_pOptimalExposureImage(nullptr), m_pRedChannelLookUpTable(nullptr), m_pGreenChannelLookUpTable(nullptr), m_pBlueChannelLookUpTable(nullptr), m_pLogTimes(nullptr), m_pUnifiedExposureHistogram(nullptr), m_pRedChannelExposureHistogram(nullptr), m_pGreenChannelExposureHistogram(nullptr), m_pBlueChannelExposureHistogram(nullptr), m_TotalIntegratedExposures(0), m_MinimalExposureParametricIndex(0), m_MaximalExposureParametricIndex(0), m_LastExposureTotalIntegratedPixels(0), m_LastSynthesizeTotalUnsampledPixels(0), m_MinimalIntensity(_REAL_ZERO_), m_MaximalIntensity(_REAL_255_), m_LastExposureTotalIntegratedKernelDensity(_REAL_ZERO_), m_OptimalExposureWeighting(_REAL_ZERO_), m_OptimalExposureMode(eOptimalExposureExtractionDisabled), m_Kernel(CRadianceWeigthingKernel::eGaussian)
                {
                    if ((m_Exposures.size() == Exposures.size()) && m_pDiscreteSourceImage && m_pDiscreteSourceImage->IsValid() && m_pGlobalMultipleChannelRadiometricResponceFunction && m_pGlobalMultipleChannelRadiometricResponceFunction->AreIdealResponceFunctionsReady())
                    {
                        const CImageSize& Size = m_pDiscreteSourceImage->GetSize();
                        m_pDensityWeightedLogRadianceImage = new TImage<real>(Size);
                        m_pDensityWeightedImage = new TImage<real>(Size);
                        m_pLogRadianceImage = new TImage<real>(Size);
                        m_pDensityWeightedExposureImage = new TImage<real>(Size);
                        m_pOptimalExposureImage = new TImage<real>(Size);
                        m_pUnifiedExposureHistogram = new uint[256];
                        m_pRedChannelExposureHistogram = new uint[256];
                        m_pGreenChannelExposureHistogram = new uint[256];
                        m_pBlueChannelExposureHistogram = new uint[256];
                        ClearImages();
                        ClearHistograms();
                        LoadLogTimesTable();
                        LoadLookUpTables(1);
                        LoadIntensityLimits(_REAL_ONE_);
                        m_Enabled = LoadBayerPatternMap(m_pGlobalMultipleChannelRadiometricResponceFunction->GetBayerPattern(), m_BayerPatternMapYX);
                    }
                }

                CGlobalRadiometricBayerPatternSynthesizer::~CGlobalRadiometricBayerPatternSynthesizer()
                {
                    RELEASE_OBJECT(m_pDensityWeightedLogRadianceImage)
                    RELEASE_OBJECT(m_pDensityWeightedImage)
                    RELEASE_OBJECT(m_pLogRadianceImage)
                    RELEASE_OBJECT(m_pDensityWeightedExposureImage)
                    RELEASE_OBJECT(m_pOptimalExposureImage)
                    RELEASE_OBJECT(m_pRedChannelLookUpTable)
                    RELEASE_OBJECT(m_pGreenChannelLookUpTable)
                    RELEASE_OBJECT(m_pBlueChannelLookUpTable)
                    RELEASE_OBJECT(m_pLogTimes)
                }

                bool CGlobalRadiometricBayerPatternSynthesizer::StartExposureBracketing()
                {
                    if (m_Enabled)
                    {
                        m_TotalIntegratedExposures = 0;
                        m_OptimalExposureWeighting = _REAL_ZERO_;
                        ClearImages();
                        return true;
                    }
                    return false;
                }

                bool CGlobalRadiometricBayerPatternSynthesizer::AddExposure(const uint ParametricIndex)
                {
                    if (m_Enabled && (ParametricIndex >= m_MinimalExposureParametricIndex) && (ParametricIndex <= m_MaximalExposureParametricIndex))
                        if (m_pDiscreteSourceImage ? AddDiscreteExposure(ParametricIndex) : (m_pRedChannelLookUpTable ? AddSemiContinousExposure(ParametricIndex) : AddContinousExposure(ParametricIndex)))
                        {
                            ++m_TotalIntegratedExposures;
                            return true;
                        }
                    return false;
                }

                bool CGlobalRadiometricBayerPatternSynthesizer::Synthesize()
                {
                    if (m_Enabled && (m_TotalIntegratedExposures > _GLOBAL_RADIOMETRIC_SYNTHESIZER_MINIMAL_INTEGRATED_EXPOSURES_))
                    {
                        const real* pDensityWeightedPixel = m_pDensityWeightedImage->GetBeginReadOnlyBuffer();
                        const real* pDensityWeightedLogRadiancePixel = m_pDensityWeightedLogRadianceImage->GetBeginReadOnlyBuffer();
                        const real* const pLogRadiancePixelEnd = m_pLogRadianceImage->GetEndReadOnlyBuffer();
                        real* pLogRadiancePixel = m_pLogRadianceImage->GetBeginWritableBuffer();
                        m_LastSynthesizeTotalUnsampledPixels = 0;
                        m_pLogRadianceImage->Clear();
                        while (pLogRadiancePixel < pLogRadiancePixelEnd)
                            if (*pDensityWeightedPixel > _REAL_EPSILON_)
                            {
                                *pLogRadiancePixel++ = *pDensityWeightedLogRadiancePixel++ / *pDensityWeightedPixel++;
                            }
                            else
                            {
                                ++pDensityWeightedPixel;
                                ++pLogRadiancePixel;
                                ++pDensityWeightedLogRadiancePixel;
                                ++m_LastSynthesizeTotalUnsampledPixels;
                            }
                        return true;
                    }
                    return false;
                }

                bool CGlobalRadiometricBayerPatternSynthesizer::GetSynthesizeDensityStatistics(real& DensityAccumulator, real& MaximalDensity, real& MinimalDensity, real& MeanDensity, real& StandarDeviationDensity, uint& TotalCriticalPixels, const real CriticalDensity) const
                {
                    if (m_Enabled)
                    {
                        const real* const ppDensityWeightedPixelEnd = m_pDensityWeightedImage->GetEndReadOnlyBuffer();
                        const real* pDensityWeightedPixel = m_pDensityWeightedImage->GetBeginReadOnlyBuffer();
                        DensityAccumulator = _REAL_ZERO_;
                        MaximalDensity = _REAL_MIN_;
                        MinimalDensity = _REAL_MAX_;
                        TotalCriticalPixels = 0;
                        real DensitySquareAccumulator = _REAL_ZERO_;
                        while (pDensityWeightedPixel < ppDensityWeightedPixelEnd)
                        {
                            const real Density = *pDensityWeightedPixel++;
                            if (Density < CriticalDensity)
                            {
                                ++TotalCriticalPixels;
                            }
                            if (Density > MaximalDensity)
                            {
                                MaximalDensity = Density;
                            }
                            if (Density < MinimalDensity)
                            {
                                MinimalDensity = Density;
                            }
                            DensityAccumulator += Density;
                            DensitySquareAccumulator += Density * Density;
                        }
                        const uint TotalPixels = m_pDensityWeightedImage->GetArea();
                        MeanDensity = DensityAccumulator / real(TotalPixels);
                        StandarDeviationDensity = RealSqrt((DensitySquareAccumulator / real(TotalPixels)) - (MeanDensity * MeanDensity));
                        return true;
                    }
                    return false;
                }

                bool CGlobalRadiometricBayerPatternSynthesizer::GetSynthesizeRadianceStatistics(real& MaximalRadiance, real& MinimalRadiance, real& MeanRadiance, real& StandarDeviationRadiance, uint& TotalEncodingBits) const
                {
                    if (m_Enabled)
                    {
                        const real* const pLogRadiancePixelEnd = m_pLogRadianceImage->GetEndReadOnlyBuffer();
                        const real* pLogRadiancePixel = m_pLogRadianceImage->GetBeginReadOnlyBuffer();
                        MeanRadiance = _REAL_ZERO_;
                        MaximalRadiance = _REAL_MIN_;
                        MinimalRadiance = _REAL_MAX_;
                        real RadianceSquareAccumulator = _REAL_ZERO_;
                        while (pLogRadiancePixel < pLogRadiancePixelEnd)
                        {
                            const real Radiance = RealExp(*pLogRadiancePixel++);
                            if (Radiance > MaximalRadiance)
                            {
                                MaximalRadiance = Radiance;
                            }
                            if (Radiance < MinimalRadiance)
                            {
                                MinimalRadiance = Radiance;
                            }
                            MeanRadiance += Radiance;
                            RadianceSquareAccumulator += Radiance * Radiance;
                        }
                        const uint TotalPixels = m_pDensityWeightedImage->GetArea();
                        MeanRadiance /= real(TotalPixels);
                        StandarDeviationRadiance = RealSqrt((RadianceSquareAccumulator / real(TotalPixels)) - (MeanRadiance * MeanRadiance));
                        TotalEncodingBits = RealCeil(RealLog2(MaximalRadiance - MinimalRadiance));
                        return true;
                    }
                    return false;
                }

                bool CGlobalRadiometricBayerPatternSynthesizer::ExtractOptimalExposureImage()
                {
                    if (m_Enabled && (m_TotalIntegratedExposures > _GLOBAL_RADIOMETRIC_SYNTHESIZER_MINIMAL_INTEGRATED_EXPOSURES_) && (m_OptimalExposureWeighting > _REAL_EPSILON_))
                    {
                        switch (m_OptimalExposureMode)
                        {
                            case eOptimalExposureExtractionDisabled:
                                return false;
                            case eWeightingGlobalDensity:
                            {
                                const real NormaliztionFactor = _REAL_ONE_ / m_OptimalExposureWeighting;
                                const real* const pDensityWeightedExposurePixelEnd = m_pDensityWeightedExposureImage->GetEndReadOnlyBuffer();
                                const real* pDensityWeightedExposurePixel = m_pDensityWeightedExposureImage->GetBeginReadOnlyBuffer();
                                real* pOptimalExposurePixel = m_pOptimalExposureImage->GetBeginWritableBuffer();
                                while (pDensityWeightedExposurePixel < pDensityWeightedExposurePixelEnd)
                                {
                                    *pOptimalExposurePixel++ = *pDensityWeightedExposurePixel++ * NormaliztionFactor;
                                }
                            }
                            break;
                            case eMaximalDensity:
                                m_pOptimalExposureImage->Copy(m_pDensityWeightedExposureImage);
                                break;
                        }
                        return true;
                    }
                    return false;
                }

                const list<CExposure>& CGlobalRadiometricBayerPatternSynthesizer::GetExposures() const
                {
                    return m_Exposures;
                }

                bool CGlobalRadiometricBayerPatternSynthesizer::SetOptimalExposureExtractionMode(const OptimalExposureExtractionMode Mode)
                {
                    if (m_Enabled && ((Mode == eWeightingGlobalDensity) || (Mode == eMaximalDensity)))
                    {
                        if (m_OptimalExposureMode != Mode)
                        {
                            m_OptimalExposureMode = Mode;
                            StartExposureBracketing();
                        }
                        return true;
                    }
                    return false;
                }

                const TImage<real>* CGlobalRadiometricBayerPatternSynthesizer::GetLogRadianceImage() const
                {
                    return m_pLogRadianceImage;
                }

                const TImage<real>* CGlobalRadiometricBayerPatternSynthesizer::GetDensityWeightedImage() const
                {
                    return m_pDensityWeightedImage;
                }

                const TImage<real>* CGlobalRadiometricBayerPatternSynthesizer::GetOptimalExposureImage() const
                {
                    return m_pOptimalExposureImage;
                }

                DeviceIdentifier CGlobalRadiometricBayerPatternSynthesizer::GetCameraId() const
                {
                    return m_Enabled ? m_pGlobalMultipleChannelRadiometricResponceFunction->GetCameraId() : 0;
                }

                BayerPatternType CGlobalRadiometricBayerPatternSynthesizer::GetBayerPattern() const
                {
                    return m_Enabled ? m_pGlobalMultipleChannelRadiometricResponceFunction->GetBayerPattern() : eUnknownBayerPattern;
                }

                const uint* CGlobalRadiometricBayerPatternSynthesizer::GetUnifiedExposureHistogram() const
                {
                    return m_pUnifiedExposureHistogram;
                }

                const uint* CGlobalRadiometricBayerPatternSynthesizer::GetRedChannelExposureHistogram() const
                {
                    return m_pRedChannelExposureHistogram;
                }

                const uint* CGlobalRadiometricBayerPatternSynthesizer::GetGreenChannelExposureHistogram() const
                {
                    return m_pGreenChannelExposureHistogram;
                }

                const uint* CGlobalRadiometricBayerPatternSynthesizer::GetBlueChannelExposureHistogram() const
                {
                    return m_pBlueChannelExposureHistogram;
                }

                bool CGlobalRadiometricBayerPatternSynthesizer::IsEnabled() const
                {
                    return m_Enabled;
                }

                const CGlobalMultipleChannelRadiometricResponceFunction* CGlobalRadiometricBayerPatternSynthesizer::GetGlobalMultipleChannelRadiometricResponceFunction() const
                {
                    return m_pGlobalMultipleChannelRadiometricResponceFunction;
                }

                uint CGlobalRadiometricBayerPatternSynthesizer::GetTotalIntegratedExposures() const
                {
                    return m_TotalIntegratedExposures;
                }

                uint CGlobalRadiometricBayerPatternSynthesizer::GetLastExposureTotalIntegratedPixels() const
                {
                    return m_LastExposureTotalIntegratedPixels;
                }

                real CGlobalRadiometricBayerPatternSynthesizer::GetLastExposureTotalIntegratedKernelDensity() const
                {
                    return m_LastExposureTotalIntegratedKernelDensity;
                }

                uint CGlobalRadiometricBayerPatternSynthesizer::GetLastSynthesizeTotalUnsampledPixels() const
                {
                    return m_LastSynthesizeTotalUnsampledPixels;
                }

                list<CExposure> CGlobalRadiometricBayerPatternSynthesizer::EnsureExposuresOrder(const list<CExposure>& Exposures)
                {
                    list<CExposure> SafeOrderExposures = Exposures;
                    SafeOrderExposures.sort(CExposure::SortExposuresByParametricIndex);
                    SafeOrderExposures.unique(CExposure::EqualsExposuresByParametricIndex);
                    return SafeOrderExposures;
                }

                void CGlobalRadiometricBayerPatternSynthesizer::LoadIntensityLimits(const real IntensityMargin)
                {
                    const CGlobalRadiometricResponceFunction* pRedChannelResponceFunction = m_pGlobalMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction();
                    const CGlobalRadiometricResponceFunction* pGreenChannelResponceFunction = m_pGlobalMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction();
                    const CGlobalRadiometricResponceFunction* pBlueChannelResponceFunction = m_pGlobalMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction();
                    const real RedMinimalIntensity = pRedChannelResponceFunction->GetMinimalIntensity();
                    const real GreenMinimalIntensity = pGreenChannelResponceFunction->GetMinimalIntensity();
                    const real BlueMinimalIntensity = pBlueChannelResponceFunction->GetMinimalIntensity();
                    m_MinimalIntensity = TMax(IntensityMargin, TMax(TMax(RedMinimalIntensity, GreenMinimalIntensity), BlueMinimalIntensity));
                    m_MaximalIntensity = _REAL_255_ - IntensityMargin;
                    /*const real T0 = m_Exposures.front().GetTime();
                    const real T1 = m_Exposures.back().GetTime();
                    const real R0 = pRedChannelResponceFunction->GetIdealRadiance(m_MinimalIntensity, T1);
                    const real R1 = pRedChannelResponceFunction->GetIdealRadiance(m_MaximalIntensity, T0);
                    const real G0 = pGreenChannelResponceFunction->GetIdealRadiance(m_MinimalIntensity, T1);
                    const real G1 = pGreenChannelResponceFunction->GetIdealRadiance(m_MaximalIntensity, T0);
                    const real B0 = pBlueChannelResponceFunction->GetIdealRadiance(m_MinimalIntensity, T1);
                    const real B1 = pBlueChannelResponceFunction->GetIdealRadiance(m_MaximalIntensity, T0);
                    m_MinimalLogRadiance = RealLog(TMin(TMin(R0, G0), B0));
                    m_MaximalLogRadiance = RealLog(TMax(TMax(R1, G1), B1));*/
                }

                void CGlobalRadiometricBayerPatternSynthesizer::LoadLogTimesTable()
                {
                    RELEASE_ARRAY(m_pLogTimes)
                    if (m_Exposures.size())
                    {
                        m_MinimalExposureParametricIndex = m_Exposures.front().GetParametricIndex();
                        m_MaximalExposureParametricIndex = m_Exposures.back().GetParametricIndex();
                        const uint TotalExposureIndices = m_MaximalExposureParametricIndex - m_MinimalExposureParametricIndex + 1;
                        m_pLogTimes = new real[TotalExposureIndices];
                        memset(m_pLogTimes, 0, sizeof(real) * TotalExposureIndices);
                        list<CExposure>::const_iterator ExposuresEnd = m_Exposures.end();
                        for (list<CExposure>::const_iterator pExposure = m_Exposures.begin(); pExposure != ExposuresEnd; ++pExposure)
                        {
                            m_pLogTimes[pExposure->GetParametricIndex()] = RealLog(pExposure->GetTime());
                        }
                    }
                }

                void CGlobalRadiometricBayerPatternSynthesizer::LoadLookUpTables(const uint SamplesPerUnit)
                {
                    RELEASE_OBJECT(m_pRedChannelLookUpTable)
                    RELEASE_OBJECT(m_pGreenChannelLookUpTable)
                    RELEASE_OBJECT(m_pBlueChannelLookUpTable)
                    if (SamplesPerUnit && m_pGlobalMultipleChannelRadiometricResponceFunction && m_pGlobalMultipleChannelRadiometricResponceFunction->AreIdealResponceFunctionsReady())
                    {
                        m_pRedChannelLookUpTable = new CGlobalRadiometricResponceFunctionLookUpTable();
                        m_pGreenChannelLookUpTable = new CGlobalRadiometricResponceFunctionLookUpTable();
                        m_pBlueChannelLookUpTable = new CGlobalRadiometricResponceFunctionLookUpTable();
                        m_pRedChannelLookUpTable->LoadLookUpTable(m_pGlobalMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction(), m_Exposures, SamplesPerUnit, m_Kernel);
                        m_pGreenChannelLookUpTable->LoadLookUpTable(m_pGlobalMultipleChannelRadiometricResponceFunction->GetGreenChannelResponceFunction(), m_Exposures, SamplesPerUnit, m_Kernel);
                        m_pBlueChannelLookUpTable->LoadLookUpTable(m_pGlobalMultipleChannelRadiometricResponceFunction->GetBlueChannelResponceFunction(), m_Exposures, SamplesPerUnit, m_Kernel);
                    }
                }

                void CGlobalRadiometricBayerPatternSynthesizer::ClearHistograms()
                {
                    memset(m_pUnifiedExposureHistogram, 0, sizeof(uint) * 256);
                    memset(m_pRedChannelExposureHistogram, 0, sizeof(uint) * 256);
                    memset(m_pGreenChannelExposureHistogram, 0, sizeof(uint) * 256);
                    memset(m_pBlueChannelExposureHistogram, 0, sizeof(uint) * 256);
                }

                void CGlobalRadiometricBayerPatternSynthesizer::ClearImages()
                {
                    m_pDensityWeightedLogRadianceImage->Clear();
                    m_pDensityWeightedImage->Clear();
                    m_pLogRadianceImage->Clear();
                    m_pDensityWeightedExposureImage->Clear();
                    m_pOptimalExposureImage->Clear();
                }

                bool CGlobalRadiometricBayerPatternSynthesizer::AddDiscreteExposure(const uint ParametricIndex)
                {
                    const real* ppChannelLUTs[3] = { nullptr };
                    const real* ppDensityWeigthingKernels[3] = { nullptr };
                    uint* ppHistogramChannels[3] = { nullptr };
                    ppChannelLUTs[ChromaticSpaceRGB::eRed] = m_pRedChannelLookUpTable->GetLookUpTable()->GetReadOnlyBufferLineAt(ParametricIndex);
                    ppChannelLUTs[ChromaticSpaceRGB::eGreen] = m_pGreenChannelLookUpTable->GetLookUpTable()->GetReadOnlyBufferLineAt(ParametricIndex);
                    ppChannelLUTs[ChromaticSpaceRGB::eBlue] = m_pBlueChannelLookUpTable->GetLookUpTable()->GetReadOnlyBufferLineAt(ParametricIndex);
                    ppDensityWeigthingKernels[ChromaticSpaceRGB::eRed] = m_pRedChannelLookUpTable->GetWeigthingKernel();
                    ppDensityWeigthingKernels[ChromaticSpaceRGB::eGreen] = m_pGreenChannelLookUpTable->GetWeigthingKernel();
                    ppDensityWeigthingKernels[ChromaticSpaceRGB::eBlue] = m_pBlueChannelLookUpTable->GetWeigthingKernel();
                    ppHistogramChannels[ChromaticSpaceRGB::eRed] = m_pRedChannelExposureHistogram;
                    ppHistogramChannels[ChromaticSpaceRGB::eGreen] = m_pGreenChannelExposureHistogram;
                    ppHistogramChannels[ChromaticSpaceRGB::eBlue] = m_pBlueChannelExposureHistogram;
                    ClearHistograms();
                    const uint HalfWidth = m_pDiscreteSourceImage->GetWidth() / 2;
                    const uint Height = m_pDiscreteSourceImage->GetHeight();
                    const byte* pDiscreteSourcePixel = m_pDiscreteSourceImage->GetBeginReadOnlyBuffer();
                    real* pDensityWeightedPixel = m_pDensityWeightedImage->GetBeginWritableBuffer();
                    real* pDensityWeightedLogRadiancePixel = m_pDensityWeightedLogRadianceImage->GetBeginWritableBuffer();
                    if (m_pDiscreteSourceImage->GetWidth() & 0X1)
                        for (uint Y = 0; Y < Height; ++Y)
                        {
                            ChromaticSpaceRGB::ChannelContent ChannelA = m_BayerPatternMapYX[Y & 0X1][0];
                            ChromaticSpaceRGB::ChannelContent ChannelB = m_BayerPatternMapYX[Y & 0X1][1];
                            const real* pLUTChannelA = ppChannelLUTs[ChannelA];
                            const real* pLUTChannelB = ppChannelLUTs[ChannelB];
                            const real* pDensityWeigthingKernelA = ppDensityWeigthingKernels[ChannelA];
                            const real* pDensityWeigthingKernelB = ppDensityWeigthingKernels[ChannelB];
                            uint* pHistogramChannelA = ppHistogramChannels[ChannelA];
                            uint* pHistogramChannelB = ppHistogramChannels[ChannelB];
                            for (uint X = 0; X < HalfWidth; ++X)
                            {
                                if ((*pDiscreteSourcePixel >= m_MinimalIntensity) && (*pDiscreteSourcePixel <= m_MaximalIntensity))
                                {
                                    ++pHistogramChannelA[*pDiscreteSourcePixel];
                                    *pDensityWeightedPixel++ += pDensityWeigthingKernelA[*pDiscreteSourcePixel];
                                    *pDensityWeightedLogRadiancePixel++ += pLUTChannelA[*pDiscreteSourcePixel++];
                                }
                                else
                                {
                                    ++pDiscreteSourcePixel;
                                    ++pDensityWeightedPixel;
                                    ++pDensityWeightedLogRadiancePixel;
                                }
                                if ((*pDiscreteSourcePixel >= m_MinimalIntensity) && (*pDiscreteSourcePixel <= m_MaximalIntensity))
                                {
                                    ++pHistogramChannelB[*pDiscreteSourcePixel];
                                    *pDensityWeightedPixel++ += pDensityWeigthingKernelB[*pDiscreteSourcePixel];
                                    *pDensityWeightedLogRadiancePixel++ += pLUTChannelB[*pDiscreteSourcePixel++];
                                }
                                else
                                {
                                    ++pDiscreteSourcePixel;
                                    ++pDensityWeightedPixel;
                                    ++pDensityWeightedLogRadiancePixel;
                                }
                            }
                            if ((*pDiscreteSourcePixel >= m_MinimalIntensity) && (*pDiscreteSourcePixel <= m_MaximalIntensity))
                            {
                                ++pHistogramChannelA[*pDiscreteSourcePixel];
                                *pDensityWeightedPixel++ += pDensityWeigthingKernelA[*pDiscreteSourcePixel];
                                *pDensityWeightedLogRadiancePixel++ += pLUTChannelA[*pDiscreteSourcePixel++];
                            }
                            else
                            {
                                ++pDiscreteSourcePixel;
                                ++pDensityWeightedPixel;
                                ++pDensityWeightedLogRadiancePixel;
                            }
                        }
                    else
                        for (uint Y = 0; Y < Height; ++Y)
                        {
                            ChromaticSpaceRGB::ChannelContent ChannelA = m_BayerPatternMapYX[Y & 0X1][0];
                            ChromaticSpaceRGB::ChannelContent ChannelB = m_BayerPatternMapYX[Y & 0X1][1];
                            const real* pLUTChannelA = ppChannelLUTs[ChannelA];
                            const real* pLUTChannelB = ppChannelLUTs[ChannelB];
                            const real* pDensityWeigthingKernelA = ppDensityWeigthingKernels[ChannelA];
                            const real* pDensityWeigthingKernelB = ppDensityWeigthingKernels[ChannelB];
                            uint* pHistogramChannelA = ppHistogramChannels[ChannelA];
                            uint* pHistogramChannelB = ppHistogramChannels[ChannelB];
                            for (uint X = 0; X < HalfWidth; ++X)
                            {
                                if ((*pDiscreteSourcePixel >= m_MinimalIntensity) && (*pDiscreteSourcePixel <= m_MaximalIntensity))
                                {
                                    ++pHistogramChannelA[*pDiscreteSourcePixel];
                                    *pDensityWeightedPixel++ += pDensityWeigthingKernelA[*pDiscreteSourcePixel];
                                    *pDensityWeightedLogRadiancePixel++ += pLUTChannelA[*pDiscreteSourcePixel++];
                                }
                                else
                                {
                                    ++pDiscreteSourcePixel;
                                    ++pDensityWeightedPixel;
                                    ++pDensityWeightedLogRadiancePixel;
                                }
                                if ((*pDiscreteSourcePixel >= m_MinimalIntensity) && (*pDiscreteSourcePixel <= m_MaximalIntensity))
                                {
                                    ++pHistogramChannelB[*pDiscreteSourcePixel];
                                    *pDensityWeightedPixel++ += pDensityWeigthingKernelB[*pDiscreteSourcePixel];
                                    *pDensityWeightedLogRadiancePixel++ += pLUTChannelB[*pDiscreteSourcePixel++];
                                }
                                else
                                {
                                    ++pDiscreteSourcePixel;
                                    ++pDensityWeightedPixel;
                                    ++pDensityWeightedLogRadiancePixel;
                                }
                            }
                        }
                    m_LastExposureTotalIntegratedPixels = 0;
                    m_LastExposureTotalIntegratedKernelDensity = _REAL_ZERO_;
                    const real* pRedChannelDensityWeigthingKernel = ppDensityWeigthingKernels[ChromaticSpaceRGB::eRed];
                    const real* pGreenChannelDensityWeigthingKernel = ppDensityWeigthingKernels[ChromaticSpaceRGB::eRed];
                    const real* pBlueChannelDensityWeigthingKernel = ppDensityWeigthingKernels[ChromaticSpaceRGB::eRed];
                    for (uint i = m_MinimalIntensity; i <= m_MaximalIntensity; ++i)
                    {
                        m_LastExposureTotalIntegratedKernelDensity += real(m_pRedChannelExposureHistogram[i]) * pRedChannelDensityWeigthingKernel[i];
                        m_LastExposureTotalIntegratedKernelDensity += real(m_pGreenChannelExposureHistogram[i]) * pGreenChannelDensityWeigthingKernel[i];
                        m_LastExposureTotalIntegratedKernelDensity += real(m_pBlueChannelExposureHistogram[i]) * pBlueChannelDensityWeigthingKernel[i];
                        m_pUnifiedExposureHistogram[i] = m_pRedChannelExposureHistogram[i] + m_pGreenChannelExposureHistogram[i] + m_pBlueChannelExposureHistogram[i];
                        m_LastExposureTotalIntegratedPixels += m_pUnifiedExposureHistogram[i];
                    }
                    if (m_LastExposureTotalIntegratedPixels)
                        switch (m_OptimalExposureMode)
                        {
                            case eOptimalExposureExtractionDisabled:
                                break;
                            case eWeightingGlobalDensity:
                                if (m_LastExposureTotalIntegratedKernelDensity > _REAL_EPSILON_)
                                {
                                    const real NormalizedIntegratedKernelDensity = m_LastExposureTotalIntegratedKernelDensity / real(m_pDiscreteSourceImage->GetArea());
                                    m_OptimalExposureWeighting += NormalizedIntegratedKernelDensity;
                                    const byte* pDiscreteSourcePixel = m_pDiscreteSourceImage->GetBeginReadOnlyBuffer();
                                    const real* const pDensityWeightedExposurePixelEnd = m_pDensityWeightedExposureImage->GetEndReadOnlyBuffer();
                                    real* pDensityWeightedExposurePixel = m_pDensityWeightedExposureImage->GetBeginWritableBuffer();
                                    while (pDensityWeightedExposurePixel < pDensityWeightedExposurePixelEnd)
                                    {
                                        *pDensityWeightedExposurePixel++ = real(*pDiscreteSourcePixel++) * NormalizedIntegratedKernelDensity;
                                    }
                                }
                                break;
                            case eMaximalDensity:
                                if (m_LastExposureTotalIntegratedKernelDensity > m_OptimalExposureWeighting)
                                {
                                    m_OptimalExposureWeighting = m_LastExposureTotalIntegratedKernelDensity;
                                    const byte* pDiscreteSourcePixel = m_pDiscreteSourceImage->GetBeginReadOnlyBuffer();
                                    const real* const pDensityWeightedExposurePixelEnd = m_pDensityWeightedExposureImage->GetEndReadOnlyBuffer();
                                    real* pDensityWeightedExposurePixel = m_pDensityWeightedExposureImage->GetBeginWritableBuffer();
                                    while (pDensityWeightedExposurePixel < pDensityWeightedExposurePixelEnd)
                                    {
                                        *pDensityWeightedExposurePixel++ = *pDiscreteSourcePixel++;
                                    }
                                }
                                break;
                        }
                    return true;
                }

                bool CGlobalRadiometricBayerPatternSynthesizer::AddSemiContinousExposure(const uint ParametricIndex)
                {
                    const real* ppChannelLUTs[3] = { nullptr };
                    const real* ppDensityWeigthingKernels[3] = { nullptr };
                    uint* ppHistogramChannels[3] = { nullptr };
                    ppChannelLUTs[ChromaticSpaceRGB::eRed] = m_pRedChannelLookUpTable->GetLookUpTable()->GetReadOnlyBufferLineAt(ParametricIndex);
                    ppChannelLUTs[ChromaticSpaceRGB::eGreen] = m_pGreenChannelLookUpTable->GetLookUpTable()->GetReadOnlyBufferLineAt(ParametricIndex);
                    ppChannelLUTs[ChromaticSpaceRGB::eBlue] = m_pBlueChannelLookUpTable->GetLookUpTable()->GetReadOnlyBufferLineAt(ParametricIndex);
                    ppDensityWeigthingKernels[ChromaticSpaceRGB::eRed] = m_pRedChannelLookUpTable->GetWeigthingKernel();
                    ppDensityWeigthingKernels[ChromaticSpaceRGB::eGreen] = m_pGreenChannelLookUpTable->GetWeigthingKernel();
                    ppDensityWeigthingKernels[ChromaticSpaceRGB::eBlue] = m_pBlueChannelLookUpTable->GetWeigthingKernel();
                    ppHistogramChannels[ChromaticSpaceRGB::eRed] = m_pRedChannelExposureHistogram;
                    ppHistogramChannels[ChromaticSpaceRGB::eGreen] = m_pGreenChannelExposureHistogram;
                    ppHistogramChannels[ChromaticSpaceRGB::eBlue] = m_pBlueChannelExposureHistogram;
                    ClearHistograms();
                    const uint HalfWidth = m_pContinousSourceImage->GetWidth() / 2;
                    const uint Height = m_pContinousSourceImage->GetHeight();
                    const uint SamplesPerUnit = m_pRedChannelLookUpTable->GetSamplesPerUnit();
                    const real* pContinousSourcePixel = m_pContinousSourceImage->GetBeginReadOnlyBuffer();
                    real* pDensityWeightedPixel = m_pDensityWeightedImage->GetBeginWritableBuffer();
                    real* ppDensityWeightedLogRadiancePixel = m_pDensityWeightedLogRadianceImage->GetBeginWritableBuffer();
                    if (m_pContinousSourceImage->GetWidth() & 0X1)
                        for (uint Y = 0; Y < Height; ++Y)
                        {
                            ChromaticSpaceRGB::ChannelContent ChannelA = m_BayerPatternMapYX[Y & 0X1][0];
                            ChromaticSpaceRGB::ChannelContent ChannelB = m_BayerPatternMapYX[Y & 0X1][1];
                            const real* pLUTChannelA = ppChannelLUTs[ChannelA];
                            const real* pLUTChannelB = ppChannelLUTs[ChannelB];
                            const real* pDensityWeigthingKernelA = ppDensityWeigthingKernels[ChannelA];
                            const real* pDensityWeigthingKernelB = ppDensityWeigthingKernels[ChannelB];
                            uint* pHistogramChannelA = ppHistogramChannels[ChannelA];
                            uint* pHistogramChannelB = ppHistogramChannels[ChannelB];
                            for (uint X = 0; X < HalfWidth; ++X)
                            {
                                if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                {
                                    ++pHistogramChannelA[RoundPositiveRealToInteger(*pContinousSourcePixel)];
                                    const uint LUTIndex = RoundPositiveRealToInteger(*pContinousSourcePixel++ * SamplesPerUnit);
                                    const real DensityWeight = pDensityWeigthingKernelA[LUTIndex];
                                    m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                    *pDensityWeightedPixel++ += DensityWeight;
                                    *ppDensityWeightedLogRadiancePixel++ += pLUTChannelA[LUTIndex];
                                }
                                else
                                {
                                    ++pContinousSourcePixel;
                                    ++pDensityWeightedPixel;
                                    ++ppDensityWeightedLogRadiancePixel;
                                }
                                if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                {
                                    ++pHistogramChannelB[RoundPositiveRealToInteger(*pContinousSourcePixel)];
                                    const uint LUTIndex = RoundPositiveRealToInteger(*pContinousSourcePixel++ * SamplesPerUnit);
                                    const real DensityWeight = pDensityWeigthingKernelB[LUTIndex];
                                    m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                    *pDensityWeightedPixel++ += DensityWeight;
                                    *ppDensityWeightedLogRadiancePixel++ += pLUTChannelB[LUTIndex];
                                }
                                else
                                {
                                    ++pContinousSourcePixel;
                                    ++pDensityWeightedPixel;
                                    ++ppDensityWeightedLogRadiancePixel;
                                }
                            }
                            if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                            {
                                ++pHistogramChannelA[RoundPositiveRealToInteger(*pContinousSourcePixel)];
                                const uint LUTIndex = RoundPositiveRealToInteger(*pContinousSourcePixel++ * SamplesPerUnit);
                                const real DensityWeight = pDensityWeigthingKernelA[LUTIndex];
                                m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                *pDensityWeightedPixel++ += DensityWeight;
                                *ppDensityWeightedLogRadiancePixel++ += pLUTChannelA[LUTIndex];
                            }
                            else
                            {
                                ++pContinousSourcePixel;
                                ++pDensityWeightedPixel;
                                ++ppDensityWeightedLogRadiancePixel;
                            }
                        }
                    else
                        for (uint Y = 0; Y < Height; ++Y)
                        {
                            ChromaticSpaceRGB::ChannelContent ChannelA = m_BayerPatternMapYX[Y & 0X1][0];
                            ChromaticSpaceRGB::ChannelContent ChannelB = m_BayerPatternMapYX[Y & 0X1][1];
                            const real* pLUTChannelA = ppChannelLUTs[ChannelA];
                            const real* pLUTChannelB = ppChannelLUTs[ChannelB];
                            const real* pDensityWeigthingKernelA = ppDensityWeigthingKernels[ChannelA];
                            const real* pDensityWeigthingKernelB = ppDensityWeigthingKernels[ChannelB];
                            uint* pHistogramChannelA = ppHistogramChannels[ChannelA];
                            uint* pHistogramChannelB = ppHistogramChannels[ChannelB];
                            for (uint X = 0; X < HalfWidth; ++X)
                            {
                                if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                {
                                    ++pHistogramChannelA[RoundPositiveRealToInteger(*pContinousSourcePixel)];
                                    const uint LUTIndex = RoundPositiveRealToInteger(*pContinousSourcePixel++ * SamplesPerUnit);
                                    const real DensityWeight = pDensityWeigthingKernelA[LUTIndex];
                                    m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                    *pDensityWeightedPixel++ += DensityWeight;
                                    *ppDensityWeightedLogRadiancePixel++ += pLUTChannelA[LUTIndex];
                                }
                                else
                                {
                                    ++pContinousSourcePixel;
                                    ++pDensityWeightedPixel;
                                    ++ppDensityWeightedLogRadiancePixel;
                                }
                                if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                {
                                    ++pHistogramChannelB[RoundPositiveRealToInteger(*pContinousSourcePixel)];
                                    const uint LUTIndex = RoundPositiveRealToInteger(*pContinousSourcePixel++ * SamplesPerUnit);
                                    const real DensityWeight = pDensityWeigthingKernelB[LUTIndex];
                                    m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                    *pDensityWeightedPixel++ += DensityWeight;
                                    *ppDensityWeightedLogRadiancePixel++ += pLUTChannelB[LUTIndex];
                                }
                                else
                                {
                                    ++pContinousSourcePixel;
                                    ++pDensityWeightedPixel;
                                    ++ppDensityWeightedLogRadiancePixel;
                                }
                            }
                        }
                    m_LastExposureTotalIntegratedPixels = 0;
                    for (uint i = m_MinimalIntensity; i <= m_MaximalIntensity; ++i)
                    {
                        m_pUnifiedExposureHistogram[i] = m_pRedChannelExposureHistogram[i] + m_pGreenChannelExposureHistogram[i] + m_pBlueChannelExposureHistogram[i];
                        m_LastExposureTotalIntegratedPixels += m_pUnifiedExposureHistogram[i];
                    }
                    if (m_LastExposureTotalIntegratedPixels)
                        switch (m_OptimalExposureMode)
                        {
                            case eOptimalExposureExtractionDisabled:
                                break;
                            case eWeightingGlobalDensity:
                                if (m_LastExposureTotalIntegratedKernelDensity > _REAL_EPSILON_)
                                {
                                    const real NormalizedIntegratedKernelDensity = m_LastExposureTotalIntegratedKernelDensity / real(m_pContinousSourceImage->GetArea());
                                    m_OptimalExposureWeighting += NormalizedIntegratedKernelDensity;
                                    const real* pContinousSourcePixel = m_pContinousSourceImage->GetBeginReadOnlyBuffer();
                                    const real* const pDensityWeightedExposureEnd = m_pDensityWeightedExposureImage->GetEndReadOnlyBuffer();
                                    real* pDensityWeightedExposure = m_pDensityWeightedExposureImage->GetBeginWritableBuffer();
                                    while (pDensityWeightedExposure < pDensityWeightedExposureEnd)
                                    {
                                        *pDensityWeightedExposure++ = *pContinousSourcePixel++ * NormalizedIntegratedKernelDensity;
                                    }
                                }
                                break;
                            case eMaximalDensity:
                                if (m_LastExposureTotalIntegratedKernelDensity > m_OptimalExposureWeighting)
                                {
                                    m_OptimalExposureWeighting = m_LastExposureTotalIntegratedKernelDensity;
                                    m_pDensityWeightedExposureImage->Copy(m_pContinousSourceImage);
                                }
                                break;
                        }

                    return true;
                }

                bool CGlobalRadiometricBayerPatternSynthesizer::AddContinousExposure(const uint ParametricIndex)
                {
                    real ChannelSlopes[3] = { _REAL_ZERO_ };
                    real ChannelOffsets[3] = { _REAL_ZERO_ };
                    const CRadianceWeigthingKernel* ppChannelRadianceWeigthingKernels[3] = { nullptr };
                    uint* ppHistogramChannels[3] = { nullptr };
                    const CGlobalRadiometricResponceFunction* pRedChannelResponceFunction = m_pGlobalMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction();
                    const CGlobalRadiometricResponceFunction* pGreenChannelResponceFunction = m_pGlobalMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction();
                    const CGlobalRadiometricResponceFunction* pBlueChannelResponceFunction = m_pGlobalMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction();
                    ChannelSlopes[ChromaticSpaceRGB::eRed] = pRedChannelResponceFunction->GetIntensitySlope();
                    ChannelSlopes[ChromaticSpaceRGB::eGreen] = pGreenChannelResponceFunction->GetIntensitySlope();
                    ChannelSlopes[ChromaticSpaceRGB::eBlue] = pBlueChannelResponceFunction->GetIntensitySlope();
                    ChannelOffsets[ChromaticSpaceRGB::eRed] = pRedChannelResponceFunction->GetIntensityOffset();
                    ChannelOffsets[ChromaticSpaceRGB::eGreen] = pGreenChannelResponceFunction->GetIntensityOffset();
                    ChannelOffsets[ChromaticSpaceRGB::eBlue] = pBlueChannelResponceFunction->GetIntensityOffset();
                    ppChannelRadianceWeigthingKernels[ChromaticSpaceRGB::eRed] = &pRedChannelResponceFunction->GetRadianceWeigthingKernel();
                    ppChannelRadianceWeigthingKernels[ChromaticSpaceRGB::eGreen] = &pGreenChannelResponceFunction->GetRadianceWeigthingKernel();
                    ppChannelRadianceWeigthingKernels[ChromaticSpaceRGB::eBlue] = &pBlueChannelResponceFunction->GetRadianceWeigthingKernel();
                    ppHistogramChannels[ChromaticSpaceRGB::eRed] = m_pRedChannelExposureHistogram;
                    ppHistogramChannels[ChromaticSpaceRGB::eGreen] = m_pGreenChannelExposureHistogram;
                    ppHistogramChannels[ChromaticSpaceRGB::eBlue] = m_pBlueChannelExposureHistogram;
                    ClearHistograms();
                    const real LogTime = m_pLogTimes[ParametricIndex];
                    const uint HalfWidth = m_pContinousSourceImage->GetWidth() / 2;
                    const uint Height = m_pContinousSourceImage->GetHeight();
                    const real* pContinousSourcePixel = m_pContinousSourceImage->GetBeginReadOnlyBuffer();
                    real* pDensityWeightedPixel = m_pDensityWeightedImage->GetBeginWritableBuffer();
                    real* DensityWeightedLogRadiancePixel = m_pDensityWeightedLogRadianceImage->GetBeginWritableBuffer();
                    if (m_pContinousSourceImage->GetWidth() & 0X1)
                        for (uint Y = 0; Y < Height; ++Y)
                        {
                            ChromaticSpaceRGB::ChannelContent ChannelA = m_BayerPatternMapYX[Y & 0X1][0];
                            ChromaticSpaceRGB::ChannelContent ChannelB = m_BayerPatternMapYX[Y & 0X1][1];
                            const real SlopeChannelA = ChannelSlopes[ChannelA];
                            const real SlopeChannelB = ChannelSlopes[ChannelB];
                            const real OffsetChannelA = ChannelOffsets[ChannelA];
                            const real OffsetChannelB = ChannelOffsets[ChannelB];
                            const CRadianceWeigthingKernel* pRadianceWeigthingKernelA = ppChannelRadianceWeigthingKernels[ChannelA];
                            const CRadianceWeigthingKernel* pRadianceWeigthingKernelB = ppChannelRadianceWeigthingKernels[ChannelB];
                            uint* pHistogramChannelA = ppHistogramChannels[ChannelA];
                            uint* pHistogramChannelB = ppHistogramChannels[ChannelB];
                            for (uint X = 0; X < HalfWidth; ++X)
                            {
                                if ((*pContinousSourcePixel >= m_MinimalIntensity)) // && (*pContinousSourcePixel <= m_MaximalIntensity))
                                {
                                    ++pHistogramChannelA[RoundPositiveRealToInteger(*pContinousSourcePixel)];
                                    const real DensityWeight = pRadianceWeigthingKernelA->GetRegressionWeigthingByKernel(*pContinousSourcePixel, m_Kernel);
                                    *pDensityWeightedPixel++ += DensityWeight;
                                    *DensityWeightedLogRadiancePixel++ += (RealLog(SlopeChannelA * *pContinousSourcePixel++ + OffsetChannelA) - LogTime) * DensityWeight;
                                    m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                }
                                else
                                {
                                    ++pContinousSourcePixel;
                                    ++pDensityWeightedPixel;
                                    ++DensityWeightedLogRadiancePixel;
                                }
                                if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                {
                                    ++pHistogramChannelB[RoundPositiveRealToInteger(*pContinousSourcePixel)];
                                    const real DensityWeight = pRadianceWeigthingKernelB->GetRegressionWeigthingByKernel(*pContinousSourcePixel, m_Kernel);
                                    *pDensityWeightedPixel++ += DensityWeight;
                                    *DensityWeightedLogRadiancePixel++ += (RealLog(SlopeChannelB * *pContinousSourcePixel++ + OffsetChannelB) - LogTime) * DensityWeight;
                                    m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                }
                                else
                                {
                                    ++pContinousSourcePixel;
                                    ++pDensityWeightedPixel;
                                    ++DensityWeightedLogRadiancePixel;
                                }
                            }
                            if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                            {
                                ++pHistogramChannelA[RoundPositiveRealToInteger(*pContinousSourcePixel)];
                                const real DensityWeight = pRadianceWeigthingKernelA->GetRegressionWeigthingByKernel(*pContinousSourcePixel, m_Kernel);
                                *pDensityWeightedPixel++ += DensityWeight;
                                *DensityWeightedLogRadiancePixel++ += (RealLog(SlopeChannelA * *pContinousSourcePixel++ + OffsetChannelA) - LogTime) * DensityWeight;
                                m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                            }
                            else
                            {
                                ++pContinousSourcePixel;
                                ++pDensityWeightedPixel;
                                ++DensityWeightedLogRadiancePixel;
                            }
                        }
                    else
                        for (uint Y = 0; Y < Height; ++Y)
                        {
                            ChromaticSpaceRGB::ChannelContent ChannelA = m_BayerPatternMapYX[Y & 0X1][0];
                            ChromaticSpaceRGB::ChannelContent ChannelB = m_BayerPatternMapYX[Y & 0X1][1];
                            const real SlopeChannelA = ChannelSlopes[ChannelA];
                            const real SlopeChannelB = ChannelSlopes[ChannelB];
                            const real OffsetChannelA = ChannelOffsets[ChannelA];
                            const real OffsetChannelB = ChannelOffsets[ChannelB];
                            const CRadianceWeigthingKernel* pRadianceWeigthingKernelA = ppChannelRadianceWeigthingKernels[ChannelA];
                            const CRadianceWeigthingKernel* pRadianceWeigthingKernelB = ppChannelRadianceWeigthingKernels[ChannelB];
                            uint* pHistogramChannelA = ppHistogramChannels[ChannelA];
                            uint* pHistogramChannelB = ppHistogramChannels[ChannelB];
                            for (uint X = 0; X < HalfWidth; ++X)
                            {
                                if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                {
                                    ++pHistogramChannelA[RoundPositiveRealToInteger(*pContinousSourcePixel)];
                                    const real DensityWeight = pRadianceWeigthingKernelA->GetRegressionWeigthingByKernel(*pContinousSourcePixel, m_Kernel);
                                    *pDensityWeightedPixel++ += DensityWeight;
                                    *DensityWeightedLogRadiancePixel++ += (RealLog(SlopeChannelA * *pContinousSourcePixel++ + OffsetChannelA) - LogTime) * DensityWeight;
                                    m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                }
                                else
                                {
                                    ++pContinousSourcePixel;
                                    ++pDensityWeightedPixel;
                                    ++DensityWeightedLogRadiancePixel;
                                }
                                if ((*pContinousSourcePixel >= m_MinimalIntensity) && (*pContinousSourcePixel <= m_MaximalIntensity))
                                {
                                    ++pHistogramChannelB[RoundPositiveRealToInteger(*pContinousSourcePixel)];
                                    const real DensityWeight = pRadianceWeigthingKernelB->GetRegressionWeigthingByKernel(*pContinousSourcePixel, m_Kernel);
                                    *pDensityWeightedPixel++ += DensityWeight;
                                    *DensityWeightedLogRadiancePixel++ += (RealLog(SlopeChannelB * *pContinousSourcePixel++ + OffsetChannelB) - LogTime) * DensityWeight;
                                    m_LastExposureTotalIntegratedKernelDensity += DensityWeight;
                                }
                                else
                                {
                                    ++pContinousSourcePixel;
                                    ++pDensityWeightedPixel;
                                    ++DensityWeightedLogRadiancePixel;
                                }
                            }
                        }
                    m_LastExposureTotalIntegratedPixels = 0;
                    for (uint i = m_MinimalIntensity; i <= m_MaximalIntensity; ++i)
                    {
                        m_pUnifiedExposureHistogram[i] = m_pRedChannelExposureHistogram[i] + m_pGreenChannelExposureHistogram[i] + m_pBlueChannelExposureHistogram[i];
                        m_LastExposureTotalIntegratedPixels += m_pUnifiedExposureHistogram[i];
                    }
                    if (m_LastExposureTotalIntegratedPixels)
                        switch (m_OptimalExposureMode)
                        {
                            case eOptimalExposureExtractionDisabled:
                                break;
                            case eWeightingGlobalDensity:
                                if (m_LastExposureTotalIntegratedKernelDensity > _REAL_EPSILON_)
                                {
                                    const real NormalizedIntegratedKernelDensity = m_LastExposureTotalIntegratedKernelDensity / real(m_pContinousSourceImage->GetArea());
                                    m_OptimalExposureWeighting += NormalizedIntegratedKernelDensity;
                                    const real* pContinousSourcePixel = m_pContinousSourceImage->GetBeginReadOnlyBuffer();
                                    const real* const pDensityWeightedExposureEnd = m_pDensityWeightedExposureImage->GetEndReadOnlyBuffer();
                                    real* pDensityWeightedExposure = m_pDensityWeightedExposureImage->GetBeginWritableBuffer();
                                    while (pDensityWeightedExposure < pDensityWeightedExposureEnd)
                                    {
                                        *pDensityWeightedExposure++ = *pContinousSourcePixel++ * NormalizedIntegratedKernelDensity;
                                    }
                                }
                                break;
                            case eMaximalDensity:
                                if (m_LastExposureTotalIntegratedKernelDensity > m_OptimalExposureWeighting)
                                {
                                    m_OptimalExposureWeighting = m_LastExposureTotalIntegratedKernelDensity;
                                    m_pDensityWeightedExposureImage->Copy(m_pContinousSourceImage);
                                }
                                break;
                        }
                    return true;
                }
            }
        }
    }
}
