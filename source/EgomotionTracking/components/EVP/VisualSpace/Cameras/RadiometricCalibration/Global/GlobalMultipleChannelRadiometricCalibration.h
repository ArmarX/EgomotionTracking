/*
 * GlobalMultipleChannelRadiometricCalibration.h
 *
 *  Created on: Apr 18, 2012
 *      Author: david
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/DataTypes/BayerPattern.h"
#include "../../../../Foundation/DataTypes/ColorSpaces.h"
#include "../../../Common/Image.h"

#include "GlobalRadiometricCalibration.h"
#include "GlobalMultipleChannelRadiometricResponceFunction.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CGlobalMultipleChannelRadiometricCalibration
                {
                public:

                    CGlobalMultipleChannelRadiometricCalibration(const DeviceIdentifier CameraId, const TImage<real>* pSourceImage, const BayerPatternType BayerPattern);
                    virtual ~CGlobalMultipleChannelRadiometricCalibration();

                    bool IsEnabled() const;
                    DeviceIdentifier GetCameraId() const;
                    BayerPatternType GetBayerPattern() const;

                    bool AddExposure(const real ExposureTime);
                    bool ClearExposures();
                    uint GetTotalExposures() const;

                    bool SetGaussianBoxKernelRadius(const real GaussianBoxKernelRadius);
                    bool SetCommonGaussianKernelStandardDeviation(const real StandardDeviation);
                    bool SetCommonGaussianKernelCutOff(const real CutOffDensity, const real CutOffNormalizedDeviation);

                    real GetGaussianBoxKernelRadius() const;
                    real GetCommonGaussianKernelStandardDeviation() const;

                    bool Calibrate(const CGlobalRadiometricCalibration::PixelSelectionCriterion Criterion, const real DominaceRadius, const uint MaximalSamples, const real SmoothingLambda, const CRadianceWeigthingKernel::KernelType Kernel);
                    bool SaveResponceFunctionToFile(const_string pPathFileName, const Files::CFile::FileMode Mode) const;

                    const CGlobalRadiometricCalibration* GetRedChannelCalibration() const;
                    const CGlobalRadiometricCalibration* GetGreenChannelCalibration() const;
                    const CGlobalRadiometricCalibration* GetBlueChannelCalibration() const;
                    const CGlobalMultipleChannelRadiometricResponceFunction* GetMultipleChannelRadiometricResponceFunction() const;

                protected:

                    CGlobalRadiometricCalibration* m_pRedChannelCalibration;
                    CGlobalRadiometricCalibration* m_pGreenChannelCalibration;
                    CGlobalRadiometricCalibration* m_pBlueChannelCalibration;
                    CGlobalMultipleChannelRadiometricResponceFunction* m_pMultipleChannelRadiometricResponceFunction;
                };
            }
        }
    }
}

