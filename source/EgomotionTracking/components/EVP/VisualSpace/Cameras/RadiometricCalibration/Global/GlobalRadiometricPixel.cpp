/*
 * GlobalRadiometricPixel.cpp
 *
 *  Created on: 21.09.2011
 *      Author: gonzalez
 */

#include "GlobalRadiometricPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                CGlobalRadiometricPixel::CGlobalRadiometricPixel() :
                    m_pRadiometricPixel(nullptr)
                {
                }

                CGlobalRadiometricPixel::~CGlobalRadiometricPixel()
                {
                    RELEASE_OBJECT(m_pRadiometricPixel)
                }

                CGlobalRadiometricPixel* CGlobalRadiometricPixel::Initialize(const coordinate X, const coordinate Y, const real* pValueSource)
                {
                    if (!m_pRadiometricPixel)
                    {
                        m_pRadiometricPixel = new RadiometricPixel;
                    }
                    m_pRadiometricPixel->m_IsActive = true;
                    m_pRadiometricPixel->m_pIntensityValueSource = pValueSource;
                    m_pRadiometricPixel->m_Criterion = _REAL_ZERO_;
                    m_pRadiometricPixel->m_Location.Set(X, Y);
                    return this;
                }

                void CGlobalRadiometricPixel::CalculateStandardDeviationCriterion()
                {
                    if (m_pRadiometricPixel->m_Values.size())
                    {
                        real Accumulator = _REAL_ZERO_;
                        list<real>::const_iterator ValuesEnd = m_pRadiometricPixel->m_Values.end();
                        for (list<real>::const_iterator pValue = m_pRadiometricPixel->m_Values.begin(); pValue != ValuesEnd; ++pValue)
                        {
                            Accumulator += *pValue;
                        }
                        const real Mean = Accumulator / real(m_pRadiometricPixel->m_Values.size());
                        real SquareDeviationAccumulator = _REAL_ZERO_;
                        for (list<real>::const_iterator pValue = m_pRadiometricPixel->m_Values.begin(); pValue != ValuesEnd; ++pValue)
                        {
                            const real Deviation = *pValue - Mean;
                            SquareDeviationAccumulator += Deviation * Deviation;
                        }
                        m_pRadiometricPixel->m_Criterion = RealSqrt(SquareDeviationAccumulator / real(m_pRadiometricPixel->m_Values.size()));
                        m_pRadiometricPixel->m_IsActive = true;
                    }
                }

                void CGlobalRadiometricPixel::CalculateRangeCriterion()
                {
                    if (m_pRadiometricPixel->m_Values.size())
                    {
                        real Maximal = m_pRadiometricPixel->m_Values.back();
                        real Minimal = Maximal;
                        list<real>::const_iterator ValuesEnd = m_pRadiometricPixel->m_Values.end();
                        for (list<real>::const_iterator pValue = m_pRadiometricPixel->m_Values.begin(); pValue != ValuesEnd; ++pValue)
                            if (*pValue > Maximal)
                            {
                                Maximal = *pValue;
                            }
                            else if (*pValue < Minimal)
                            {
                                Minimal = *pValue;
                            }
                        m_pRadiometricPixel->m_Criterion = Maximal - Minimal;
                        m_pRadiometricPixel->m_IsActive = true;
                    }
                }

                bool CGlobalRadiometricPixel::IsInitialized() const
                {
                    return m_pRadiometricPixel;
                }

                bool CGlobalRadiometricPixel::IsActive() const
                {
                    return m_pRadiometricPixel->m_IsActive;
                }

                void CGlobalRadiometricPixel::SetActive(const bool Active)
                {
                    if (m_pRadiometricPixel)
                    {
                        m_pRadiometricPixel->m_IsActive = Active;
                    }
                }

                void CGlobalRadiometricPixel::AddExposure()
                {
                    m_pRadiometricPixel->m_Values.push_back(*m_pRadiometricPixel->m_pIntensityValueSource);
                }

                void CGlobalRadiometricPixel::ClearExposures()
                {
                    m_pRadiometricPixel->m_IsActive = true;
                    m_pRadiometricPixel->m_Values.clear();
                    m_pRadiometricPixel->m_Criterion = _REAL_ZERO_;
                }

                real CGlobalRadiometricPixel::GetCriterion() const
                {
                    return m_pRadiometricPixel->m_Criterion;
                }

                const CPixelLocation& CGlobalRadiometricPixel::GetLocation() const
                {
                    return m_pRadiometricPixel->m_Location;
                }

                const list<real>& CGlobalRadiometricPixel::GetValues() const
                {
                    return m_pRadiometricPixel->m_Values;
                }
            }
        }
    }
}
