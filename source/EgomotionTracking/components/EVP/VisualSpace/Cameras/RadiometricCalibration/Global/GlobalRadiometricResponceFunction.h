/*
 * GlobalRadiometricResponceFunction.h
 *
 *  Created on: Apr 17, 2012
 *      Author: david
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/DataTypes/BayerPattern.h"
#include "../../../../Foundation/DataTypes/ColorSpaces.h"
#include "../../../../Foundation/Files/InFile.h"
#include "../../../../Foundation/Files/OutFile.h"
#include "../Common/RadianceWeigthingKernel.h"
#include "GlobalRadiometricCalibration.h"

#define _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_DIGITS_ 3
#define _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_INCREMENTS_PER_UNIT_ 1
#define _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_ ","

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CGlobalRadiometricResponceFunction
                {
                public:

                    static const CGlobalRadiometricResponceFunction* CreateFromFile(const_string pPathFileName);

                    CGlobalRadiometricResponceFunction();
                    CGlobalRadiometricResponceFunction(const DeviceIdentifier CameraId, const BayerPatternType BayerPattern, const ChromaticSpaceRGB::ChannelContent ChannelContent);
                    virtual ~CGlobalRadiometricResponceFunction();

                    bool LoadResponceFunctionFromParameters(const DeviceIdentifier CameraId, const BayerPatternType BayerPattern, const ChromaticSpaceRGB::ChannelContent ChannelContent, const uint TotalExpositions, const uint TotalSelectedPixels, const real DominanceRadius, const real SmoothingLambda, const CGlobalRadiometricCalibration::PixelSelectionCriterion PixelSelectionCriterion, const real* pRegressionResponseFunction, const real* pRegressionWeightingFunction, const CRadianceWeigthingKernel& RadianceWeigthingKernel);
                    bool LoadResponceFunctionFromBinaryFile(const_string pPathFileName);
                    bool SaveResponceFunctionToFile(const_string pPathFileName, const Files::CFile::FileMode Mode) const;

                    bool IsIdealResponceFunctionReady() const;
                    bool EstimateIdealResponceFunction(const CRadianceWeigthingKernel::KernelType Kernel);
                    real GetRegressionResponceFunction(const byte DiscreteIntensity) const;
                    real GetRegressionResponceFunction(const real Intensity) const;

                    real GetIdealResponce(const real Intensity) const;
                    real GetIdealRadiance(const real Intensity, const real ExposureTime) const;
                    real GetIdealExposureTime(const real Intensity, const real Radiance) const;
                    real GetIdealIntensity(const real Radiance, const real ExposureTime) const;

                    DeviceIdentifier GetCameraId() const;
                    BayerPatternType GetBayerPattern() const;
                    ChromaticSpaceRGB::ChannelContent GetChannelContent() const;
                    uint GetTotalExpositions() const;
                    uint GetTotalSelectedPixels() const;
                    real GetDominanceRadius() const;
                    real GetSmoothingLambda() const;
                    CGlobalRadiometricCalibration::PixelSelectionCriterion GetPixelSelectionCriterion() const;
                    const real* GetRegressionResponseFunction() const;
                    const real* GetRegressionWeightingFunction() const;
                    const real* GetModelEstimationWeightingFunction() const;
                    real GetIntensitySlope() const;
                    real GetIntensityOffset() const;
                    real GetMinimalIntensity() const;
                    const CRadianceWeigthingKernel& GetRadianceWeigthingKernel() const;

                protected:

                    friend class CGlobalMultipleChannelRadiometricResponceFunction;

                    void Create();
                    void Destroy();
                    bool SaveResponceFunctionToTextFile(const_string pPathFileName) const;
                    bool SaveResponceFunctionToBinaryFile(const_string pPathFileName) const;
                    bool SaveResponceFunctionToBinaryFile(Files::COutFile& OutputBinaryFile) const;
                    bool LoadResponceFunctionFromBinaryFile(Files::CInFile& InputBinaryFile);

                    DeviceIdentifier m_CameraId;
                    BayerPatternType m_BayerPattern;
                    ChromaticSpaceRGB::ChannelContent m_ChannelContent;
                    uint m_TotalExpositions;
                    uint m_TotalSelectedPixels;
                    real m_DominanceRadius;
                    real m_SmoothingLambda;
                    CGlobalRadiometricCalibration::PixelSelectionCriterion m_PixelSelectionCriterion;
                    real* m_pRegressionResponseFunction;
                    real* m_pRegressionWeightingFunction;
                    real* m_pModelEstimationWeightingFunction;
                    real m_IntensitySlope;
                    real m_IntensityOffset;
                    real m_MinimalIntensity;
                    CRadianceWeigthingKernel m_RadianceWeigthingKernel;
                };
            }
        }
    }
}

