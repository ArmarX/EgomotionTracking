/*
 * GlobalMultipleChannelRadiometricResponceFunction.cpp
 *
 *  Created on: Apr 18, 2012
 *      Author: david
 */

#include "GlobalMultipleChannelRadiometricResponceFunction.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                CGlobalMultipleChannelRadiometricResponceFunction* CGlobalMultipleChannelRadiometricResponceFunction::CreateFromFile(const_string pPathFileName)
                {
                    if (Files::CFile::Exists(pPathFileName))
                    {
                        CGlobalMultipleChannelRadiometricResponceFunction* pMultipleChannelResponceFunction = new CGlobalMultipleChannelRadiometricResponceFunction();
                        if (pMultipleChannelResponceFunction->LoadResponceFunctionFromBinaryFile(pPathFileName))
                        {
                            return pMultipleChannelResponceFunction;
                        }
                        RELEASE_OBJECT(pMultipleChannelResponceFunction)
                    }
                    return nullptr;
                }

                CGlobalMultipleChannelRadiometricResponceFunction::CGlobalMultipleChannelRadiometricResponceFunction() :
                    m_OwnerShip(false), m_pRedChannelResponceFunction(nullptr), m_pGreenChannelResponceFunction(nullptr), m_pBlueChannelResponceFunction(nullptr)
                {
                }

                CGlobalMultipleChannelRadiometricResponceFunction::~CGlobalMultipleChannelRadiometricResponceFunction()
                {
                    RELEASE_OBJECT_BY_OWNERSHIP(m_pRedChannelResponceFunction, m_OwnerShip)
                    RELEASE_OBJECT_BY_OWNERSHIP(m_pGreenChannelResponceFunction, m_OwnerShip)
                    RELEASE_OBJECT_BY_OWNERSHIP(m_pBlueChannelResponceFunction, m_OwnerShip)
                }

                bool CGlobalMultipleChannelRadiometricResponceFunction::AreIdealResponceFunctionsReady() const
                {
                    return m_pRedChannelResponceFunction && m_pGreenChannelResponceFunction && m_pBlueChannelResponceFunction && m_pRedChannelResponceFunction->IsIdealResponceFunctionReady() && m_pGreenChannelResponceFunction->IsIdealResponceFunctionReady() && m_pBlueChannelResponceFunction->IsIdealResponceFunctionReady();
                }

                bool CGlobalMultipleChannelRadiometricResponceFunction::LoadResponceFunctions(const CGlobalRadiometricResponceFunction* pRedChannelResponceFunction, const CGlobalRadiometricResponceFunction* pGreenChannelResponceFunction, const CGlobalRadiometricResponceFunction* pBlueRadiometricResponceFunction, const bool OwnerShip)
                {
                    if (pRedChannelResponceFunction && pGreenChannelResponceFunction && pBlueRadiometricResponceFunction && (pRedChannelResponceFunction->GetChannelContent() == ChromaticSpaceRGB::eRed) && (pGreenChannelResponceFunction->GetChannelContent() == ChromaticSpaceRGB::eGreen) && (pBlueRadiometricResponceFunction->GetChannelContent() == ChromaticSpaceRGB::eBlue) && (pRedChannelResponceFunction->GetBayerPattern() == pGreenChannelResponceFunction->GetBayerPattern()) && (pGreenChannelResponceFunction->GetBayerPattern() == pBlueRadiometricResponceFunction->GetBayerPattern()) && (pRedChannelResponceFunction->GetCameraId() == pGreenChannelResponceFunction->GetCameraId()) && (pGreenChannelResponceFunction->GetCameraId() == pBlueRadiometricResponceFunction->GetCameraId()))
                    {
                        RELEASE_OBJECT_BY_OWNERSHIP(m_pRedChannelResponceFunction, m_OwnerShip)
                        RELEASE_OBJECT_BY_OWNERSHIP(m_pGreenChannelResponceFunction, m_OwnerShip)
                        RELEASE_OBJECT_BY_OWNERSHIP(m_pBlueChannelResponceFunction, m_OwnerShip)
                        m_OwnerShip = OwnerShip;
                        m_pRedChannelResponceFunction = pRedChannelResponceFunction;
                        m_pGreenChannelResponceFunction = pGreenChannelResponceFunction;
                        m_pBlueChannelResponceFunction = pBlueRadiometricResponceFunction;
                        return true;
                    }
                    return false;
                }

                bool CGlobalMultipleChannelRadiometricResponceFunction::LoadResponceFunctionFromBinaryFile(const_string pPathFileName)
                {
                    RELEASE_OBJECT_BY_OWNERSHIP(m_pRedChannelResponceFunction, m_OwnerShip)
                    RELEASE_OBJECT_BY_OWNERSHIP(m_pGreenChannelResponceFunction, m_OwnerShip)
                    RELEASE_OBJECT_BY_OWNERSHIP(m_pBlueChannelResponceFunction, m_OwnerShip)
                    if (Files::CFile::Exists(pPathFileName))
                    {
                        Files::CInFile InputBinaryFile(pPathFileName, Files::CFile::eBinary);
                        if (InputBinaryFile.IsReady())
                        {
                            CGlobalRadiometricResponceFunction* pRedChannelResponceFunction = new CGlobalRadiometricResponceFunction();
                            CGlobalRadiometricResponceFunction* pGreenChannelResponceFunction = new CGlobalRadiometricResponceFunction();
                            CGlobalRadiometricResponceFunction* pBlueChannelResponceFunction = new CGlobalRadiometricResponceFunction();
                            if (pRedChannelResponceFunction->LoadResponceFunctionFromBinaryFile(InputBinaryFile))
                                if (pGreenChannelResponceFunction->LoadResponceFunctionFromBinaryFile(InputBinaryFile))
                                    if (pBlueChannelResponceFunction->LoadResponceFunctionFromBinaryFile(InputBinaryFile))
                                        if (InputBinaryFile.Close())
                                        {
                                            m_pRedChannelResponceFunction = pRedChannelResponceFunction;
                                            m_pGreenChannelResponceFunction = m_pRedChannelResponceFunction;//pGreenChannelResponceFunction;
                                            m_pBlueChannelResponceFunction = m_pRedChannelResponceFunction;//pBlueChannelResponceFunction;
                                            m_OwnerShip = true;
                                            return true;
                                        }
                            RELEASE_OBJECT(pRedChannelResponceFunction)
                            RELEASE_OBJECT(pGreenChannelResponceFunction)
                            RELEASE_OBJECT(pBlueChannelResponceFunction)
                        }
                    }
                    return false;
                }

                bool CGlobalMultipleChannelRadiometricResponceFunction::SaveResponceFunctionToFile(const_string pPathFileName, const Files::CFile::FileMode Mode) const
                {
                    if (m_pRedChannelResponceFunction && m_pGreenChannelResponceFunction && m_pBlueChannelResponceFunction)
                        switch (Mode)
                        {
                            case Files::CFile::eText:
                                return SaveResponceFunctionToTextFile(pPathFileName);
                            case Files::CFile::eBinary:
                                return SaveResponceFunctionToBinaryFile(pPathFileName);
                        }
                    return false;
                }

                DeviceIdentifier CGlobalMultipleChannelRadiometricResponceFunction::GetCameraId() const
                {
                    return m_pRedChannelResponceFunction ? m_pRedChannelResponceFunction->GetCameraId() : 0;
                }

                BayerPatternType CGlobalMultipleChannelRadiometricResponceFunction::GetBayerPattern() const
                {
                    return m_pRedChannelResponceFunction ? m_pRedChannelResponceFunction->GetBayerPattern() : eUnknownBayerPattern;
                }

                const CGlobalRadiometricResponceFunction* CGlobalMultipleChannelRadiometricResponceFunction::GetRedChannelResponceFunction() const
                {
                    return m_pRedChannelResponceFunction;
                }

                const CGlobalRadiometricResponceFunction* CGlobalMultipleChannelRadiometricResponceFunction::GetGreenChannelResponceFunction() const
                {
                    return m_pGreenChannelResponceFunction;
                }

                const CGlobalRadiometricResponceFunction* CGlobalMultipleChannelRadiometricResponceFunction::GetBlueChannelResponceFunction() const
                {
                    return m_pBlueChannelResponceFunction;
                }

                bool CGlobalMultipleChannelRadiometricResponceFunction::SaveResponceFunctionToTextFile(const_string pPathFileName) const
                {
                    std::ostringstream OutputText;
                    OutputText.precision(_GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_DIGITS_);
                    const real MinimalIntensities[3] = { m_pRedChannelResponceFunction->GetMinimalIntensity(), m_pGreenChannelResponceFunction->GetMinimalIntensity(), m_pBlueChannelResponceFunction->GetMinimalIntensity() };
                    OutputText << "Global Multiple Channel Radiometric Camera Calibration" << g_EndLine;
                    OutputText << g_EndLine << "[General Information]" << g_EndLine << g_EndLine;
                    OutputText << "Camera Device Identifier" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << DeviceIdentifierToString(m_pRedChannelResponceFunction->GetCameraId()) << g_EndLine;
                    OutputText << "Bayer Pattern" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << BayerPatternToString(m_pRedChannelResponceFunction->GetBayerPattern()) << g_EndLine;
                    OutputText << "Channel Content" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "Red" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "Green" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "Blue" << g_EndLine;
                    OutputText << "Total Expositions" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pRedChannelResponceFunction->GetTotalExpositions() << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pGreenChannelResponceFunction->GetTotalExpositions() << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pBlueChannelResponceFunction->GetTotalExpositions() << g_EndLine;
                    OutputText << "Total Selected Pixels" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pRedChannelResponceFunction->GetTotalSelectedPixels() << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pGreenChannelResponceFunction->GetTotalSelectedPixels() << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pBlueChannelResponceFunction->GetTotalSelectedPixels() << g_EndLine;
                    OutputText << "Pixel Selection Criterion" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << CGlobalRadiometricCalibration::PixelSelectionCriterionToString(m_pRedChannelResponceFunction->GetPixelSelectionCriterion()) << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << CGlobalRadiometricCalibration::PixelSelectionCriterionToString(m_pGreenChannelResponceFunction->GetPixelSelectionCriterion()) << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << CGlobalRadiometricCalibration::PixelSelectionCriterionToString(m_pBlueChannelResponceFunction->GetPixelSelectionCriterion()) << g_EndLine;
                    OutputText << "Dominance Radius" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pRedChannelResponceFunction->GetDominanceRadius() << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pGreenChannelResponceFunction->GetDominanceRadius() << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pBlueChannelResponceFunction->GetDominanceRadius() << g_EndLine;
                    OutputText << "Smoothing Lambda" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pRedChannelResponceFunction->GetSmoothingLambda() << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pGreenChannelResponceFunction->GetSmoothingLambda() << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pBlueChannelResponceFunction->GetSmoothingLambda() << g_EndLine;
                    OutputText << "Exponential Domain Slope" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pRedChannelResponceFunction->GetIntensitySlope() << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pGreenChannelResponceFunction->GetIntensitySlope() << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pBlueChannelResponceFunction->GetIntensitySlope() << g_EndLine;
                    OutputText << "Exponential Domain Offset" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pRedChannelResponceFunction->GetIntensityOffset() << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pGreenChannelResponceFunction->GetIntensityOffset() << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << m_pBlueChannelResponceFunction->GetIntensityOffset() << g_EndLine;
                    OutputText << "Minimal Integrable Continuous Intensity" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << MinimalIntensities[0] << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << MinimalIntensities[1] << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << MinimalIntensities[2] << g_EndLine;
                    OutputText << g_EndLine << "[Response Curves]" << g_EndLine << g_EndLine;
                    OutputText << "Continuous Intensity" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "Discrete Intensity" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "Regression Weights Red Channel" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "Regression Weights Green Channel" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "Regression Weights Blue Channel" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "Model Estimation Weights Red Channel" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "Model Estimation Weights Green Channel" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "Model Estimation Weights Blue Channel" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "G_R(I)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "G_G(I)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "G_B(I)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "e^G_R(I)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "e^G_G(I)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "e^G_B(I)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "Gp_R(I) = ln((Slope_R*I)+Offset_R)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "Gp_G(I) = ln((Slope_G*I)+Offset_G)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "Gp_B(I) = ln((Slope_B*I)+Offset_B)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "e^Gp_R(I)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "e^Gp_G(I)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "e^Gp_B(I)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "e^G_R(I)-e^Gp_R(Continuous Intensity)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "e^G_G(I)-e^Gp_G(Continuous Intensity)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "e^G_B(I)-e^Gp_B(Continuous Intensity)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "|e^G_R(Continuous Intensity)-e^Gp_R(Continuous Intensity)|" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "|e^G_G(Continuous Intensity)-e^Gp_G(Continuous Intensity)|" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                    OutputText << "|e^G_B(Continuous Intensity)-e^Gp_B(Continuous Intensity)|";
                    const real Increment = _REAL_ONE_ / real(_GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_INCREMENTS_PER_UNIT_);
                    const real* pRegressionResponseFunctionRed = m_pRedChannelResponceFunction->GetRegressionResponseFunction();
                    const real* pRegressionResponseFunctionGreen = m_pGreenChannelResponceFunction->GetRegressionResponseFunction();
                    const real* pRegressionResponseFunctionBlue = m_pBlueChannelResponceFunction->GetRegressionResponseFunction();
                    const real* pRegressionWeightingFunctionRed = m_pRedChannelResponceFunction->GetRegressionWeightingFunction();
                    const real* pRegressionWeightingFunctionGreen = m_pGreenChannelResponceFunction->GetRegressionWeightingFunction();
                    const real* pRegressionWeightingFunctionBlue = m_pBlueChannelResponceFunction->GetRegressionWeightingFunction();
                    const real* pModelEstimationWeightingFunctionRed = m_pRedChannelResponceFunction->GetModelEstimationWeightingFunction();
                    const real* pModelEstimationWeightingFunctionGreen = m_pGreenChannelResponceFunction->GetModelEstimationWeightingFunction();
                    const real* pModelEstimationWeightingFunctionBlue = m_pBlueChannelResponceFunction->GetModelEstimationWeightingFunction();
                    for (real ContinuousIntensity = _REAL_ZERO_; ContinuousIntensity <= _REAL_255_; ContinuousIntensity += Increment)
                    {
                        const uint DiscreteIntensity = uint(RealRound(ContinuousIntensity));
                        const real eG_R = RealExp(pRegressionResponseFunctionRed[DiscreteIntensity]);
                        const real eG_G = RealExp(pRegressionResponseFunctionGreen[DiscreteIntensity]);
                        const real eG_B = RealExp(pRegressionResponseFunctionBlue[DiscreteIntensity]);
                        const real eGp_R = m_pRedChannelResponceFunction->GetIdealResponce(ContinuousIntensity);
                        const real eGp_G = m_pGreenChannelResponceFunction->GetIdealResponce(ContinuousIntensity);
                        const real eGp_B = m_pBlueChannelResponceFunction->GetIdealResponce(ContinuousIntensity);
                        const real Gp_R = RealLog(eGp_R);
                        const real Gp_G = RealLog(eGp_G);
                        const real Gp_B = RealLog(eGp_B);
                        OutputText << g_EndLine << ContinuousIntensity << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << DiscreteIntensity << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << pRegressionWeightingFunctionRed[DiscreteIntensity] << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << pRegressionWeightingFunctionGreen[DiscreteIntensity] << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << pRegressionWeightingFunctionBlue[DiscreteIntensity] << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << pModelEstimationWeightingFunctionRed[DiscreteIntensity] << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << pModelEstimationWeightingFunctionGreen[DiscreteIntensity] << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << pModelEstimationWeightingFunctionBlue[DiscreteIntensity] << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << pRegressionResponseFunctionRed[DiscreteIntensity] << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << pRegressionResponseFunctionGreen[DiscreteIntensity] << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << pRegressionResponseFunctionBlue[DiscreteIntensity] << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << eG_R << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << eG_G << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << eG_B << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        if (ContinuousIntensity > MinimalIntensities[0])
                        {
                            OutputText << Gp_R << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        }
                        else
                        {
                            OutputText << "NAN\t";
                        }
                        if (ContinuousIntensity > MinimalIntensities[1])
                        {
                            OutputText << Gp_G << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        }
                        else
                        {
                            OutputText << "NAN\t";
                        }
                        if (ContinuousIntensity > MinimalIntensities[2])
                        {
                            OutputText << Gp_B << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        }
                        else
                        {
                            OutputText << "NAN\t";
                        }
                        if (ContinuousIntensity > MinimalIntensities[0])
                        {
                            OutputText << eGp_R << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        }
                        else
                        {
                            OutputText << "NAN\t";
                        }
                        if (ContinuousIntensity > MinimalIntensities[1])
                        {
                            OutputText << eGp_G << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        }
                        else
                        {
                            OutputText << "NAN\t";
                        }
                        if (ContinuousIntensity > MinimalIntensities[2])
                        {
                            OutputText << eGp_B << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        }
                        else
                        {
                            OutputText << "NAN\t";
                        }
                        if (ContinuousIntensity > MinimalIntensities[0])
                        {
                            OutputText << eG_R - eGp_R << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        }
                        else
                        {
                            OutputText << "NAN\t";
                        }
                        if (ContinuousIntensity > MinimalIntensities[1])
                        {
                            OutputText << eG_G - eGp_G << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        }
                        else
                        {
                            OutputText << "NAN\t";
                        }
                        if (ContinuousIntensity > MinimalIntensities[2])
                        {
                            OutputText << eG_B - eGp_B << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        }
                        else
                        {
                            OutputText << "NAN\t";
                        }
                        if (ContinuousIntensity > MinimalIntensities[0])
                        {
                            OutputText << RealAbs(eG_R - eGp_R) << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        }
                        else
                        {
                            OutputText << "NAN\t";
                        }
                        if (ContinuousIntensity > MinimalIntensities[1])
                        {
                            OutputText << RealAbs(eG_G - eGp_G) << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        }
                        else
                        {
                            OutputText << "NAN\t";
                        }
                        if (ContinuousIntensity > MinimalIntensities[2])
                        {
                            OutputText << RealAbs(eG_B - eGp_B);
                        }
                        else
                        {
                            OutputText << "NAN";
                        }
                    }
                    OutputText << g_EndLine << "For more information see IEEE-RAS publication: Gonzalez-Aguirre, D.; Asfour, T.; Dillmann, R.; , \"Eccentricity edge-graphs from HDR images for object recognition by humanoid robots,\" Humanoid Robots (Humanoids), 2010 10th IEEE-RAS International Conference on , vol., no., pp.144-151, 6-8 Dec. 2010 doi: 10.1109/ICHR.2010.5686336";
                    return Files::COutFile::WriteStringToFile(pPathFileName, OutputText.str());
                }

                bool CGlobalMultipleChannelRadiometricResponceFunction::SaveResponceFunctionToBinaryFile(const_string pPathFileName) const
                {
                    Files::COutFile OutputBinaryFile(pPathFileName, Files::CFile::eBinary);
                    if (OutputBinaryFile.IsReady())
                    {
                        if (m_pRedChannelResponceFunction->SaveResponceFunctionToBinaryFile(OutputBinaryFile))
                            if (m_pGreenChannelResponceFunction->SaveResponceFunctionToBinaryFile(OutputBinaryFile))
                                if (m_pBlueChannelResponceFunction->SaveResponceFunctionToBinaryFile(OutputBinaryFile))
                                {
                                    return OutputBinaryFile.Close();
                                }
                    }
                    return false;
                }
            }
        }
    }
}
