/*
 * GlobalRadiometricDistribution.h
 *
 *  Created on: 13.05.2012
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/DataTypes/BayerPattern.h"
#include "../../../../Foundation/DataTypes/ColorSpaces.h"
#include "../../../Common/Image.h"
#include "../Common/Exposure.h"
#include "../Common/CalibratedExposure.h"
#include "../Common/RadianceWeigthingKernel.h"
#include "GlobalMultipleChannelRadiometricResponceFunction.h"
#include "GlobalRadiometricResponceFunctionLookUpTable.h"
#include "GlobalRadiometricBayerPatternSynthesizer.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CGlobalRadiometricDistribution
                {
                public:

                    enum OptimizationCriterion
                    {
                        eUnkownSelectionCriterion, eEnsureMinimalDensity, eEnsureMinimalExposures
                    };

                    CGlobalRadiometricDistribution();
                    ~CGlobalRadiometricDistribution();

                    bool AddSynthesizer(const CGlobalRadiometricBayerPatternSynthesizer* pBayerPatternSynthesizer);
                    bool RemoveAllSynthesizers();
                    bool LoadConfiguration(const CRadianceWeigthingKernel::KernelType Kernel, const real ScopeKernelDensity, const real IntensityMargin = _REAL_ONE_);
                    list<CCalibratedExposure> GetMinimalExposureSet() const;

                    bool LoadDistributionFromSynthesizers(const real CriticallKernelDensity);
                    list<CCalibratedExposure> SelectExposureIndicesByCriterion(const OptimizationCriterion Criterion);

                    real GetMaximalIntensity() const;
                    real GetMinimalIntensity() const;
                    real GetUpperIntensityAtScopeDensity() const;
                    real GetLowerIntensityAtScopeDensity() const;
                    real GetScopeKernelDensity() const;
                    real GetConsistentSafeMaximalKernelDensity() const;
                    uint GetTotalUnderExposedPixels() const;
                    uint GetTotalOverExposedPixels() const;
                    const list<CExposure>& GetExposures() const;
                    const std::vector<CCalibratedExposure>& GetCalibratedExposures() const;
                    const list<const CGlobalRadiometricBayerPatternSynthesizer*>& GetSynthesizers() const;

                protected:

                    struct ExposureBin
                    {
                        bool m_Active;
                        real m_LogTime;
                        ushort m_Index;
                        ushort m_ScopeIndexA;
                        ushort m_ScopeIndexB;
                        real m_SamplingDensityAccumulator;
                        real m_ScopeSamplingDensityAccumulator;
                        real m_MinimalDensity;
                    };

                    struct ExposureDualIndex
                    {
                        ushort m_IndexA;
                        ushort m_IndexB;
                        real m_ExposureTimeThreshold;
                    };

                    bool ClearDistribution();
                    bool LoadExposuresFromSynthesizers();
                    bool UpdateMinimalIntensity(const real IntensityMargin);
                    bool UpdateConsistentSafeMaximalKernelDensity(const CRadianceWeigthingKernel::KernelType Kernel);
                    bool CreateCalibratedExposures(const CRadianceWeigthingKernel::KernelType Kernel);
                    bool CreateExposureDistribution();
                    bool CreateExposureDualIndices();
                    list<ExposureBin*> LoadExposureScopeFrequencyDistribution();
                    list<ExposureBin*> DetermineExposureScopeFrequencyDistribution(const list<ExposureBin*>& ExposureDistribution);
                    ExposureBin* GetExposureBinMinimalIntegrationTime(const list<ExposureBin*>& ExposureScopeFrequencyDistribution);
                    ExposureBin* GetExposureBinByMinimalDensity(const list<ExposureBin*>& ExposureScopeFrequencyDistribution);
                    ExposureBin* GetExposureBinByMaximalScopeFrequency(const list<ExposureBin*>& ExposureScopeFrequencyDistribution);
                    bool ConfigureExposureScopeByDensity(const real ScopeKernelDensity);
                    bool MapExposureFrequencyDistribution(const CGlobalMultipleChannelRadiometricResponceFunction* pGlobalMultipleChannelRadiometricResponceFunction, const TImage<real>* pLogRadianceImage, const TImage<real>* pDensityWeightedImage, const BayerPatternType BayerPattern, const real CriticallKernelDensity);

                    CRadianceWeigthingKernel::KernelType m_Kernel;
                    ExposureBin* m_pExposureDistribution;
                    ExposureDualIndex* m_pExposureDualIndices;
                    real m_MaximalIntensity;
                    real m_MinimalIntensity;
                    real m_UpperIntensityAtScopeDensity;
                    real m_LowerIntensityAtScopeDensity;
                    real m_ScopeKernelDensity;
                    real m_ConsistentSafeMaximalKernelDensity;
                    real m_ScaleFactorLogExposureTime;
                    uint m_TotalUnderExposedPixels;
                    uint m_TotalOverExposedPixels;
                    list<CExposure> m_Exposures;
                    std::vector<CCalibratedExposure> m_CalibratedExposures;
                    list<const CGlobalRadiometricResponceFunction*> m_GlobalRadiometricResponceFunctions;
                    CRadianceWeigthingKernel m_RadianceWeigthingKernel;
                    list<const CGlobalRadiometricBayerPatternSynthesizer*> m_Synthesizers;
                };
            }
        }
    }
}
