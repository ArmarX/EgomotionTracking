/*
 * GlobalRadiometricCalibration.cpp
 *
 *  Created on: Apr 17, 2012
 *      Author: david
 */

#include "GlobalRadiometricCalibration.h"
#include "GlobalRadiometricResponceFunction.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                std::string CGlobalRadiometricCalibration::PixelSelectionCriterionToString(const PixelSelectionCriterion Criterion)
                {
                    switch (Criterion)
                    {
                        case eUnknownPixelSelectionCriterion:
                            return std::string("Unknown");
                        case eMaximalStandardDeviation:
                            return std::string("Maximal Standard Deviation");
                        case eMinimalStandardDeviation:
                            return std::string("Minimal Standard Deviation");
                        case eMaximalRange:
                            return std::string("Maximal Range");
                        case eMinimalRange:
                            return std::string("Minimal Range");
                    }
                    return std::string("Error");
                }

                CGlobalRadiometricCalibration::CGlobalRadiometricCalibration(const DeviceIdentifier CameraId, const TImage<real>* pSourceImage, const BayerPatternType BayerPattern, const ChromaticSpaceRGB::ChannelContent ChannelContent) :
                    m_CameraId(CameraId), m_BayerPattern(BayerPattern), m_ChannelContent(ChannelContent), m_pSourceImage(pSourceImage), m_pRadiometricImage(nullptr), m_pRadiometricResponceFunction(nullptr)
                {
                    if (m_pSourceImage && m_pSourceImage->IsValid())
                    {
                        bool FlagMapYX[2][2];
                        if (LoadBayerPatternFlagMap(m_BayerPattern, m_ChannelContent, FlagMapYX))
                        {
                            const uint Width = m_pSourceImage->GetWidth();
                            const uint Height = m_pSourceImage->GetHeight();
                            m_pRadiometricImage = new TImage<CGlobalRadiometricPixel>(Width, Height);
                            const real* pSourcePixel = m_pSourceImage->GetBeginReadOnlyBuffer();
                            CGlobalRadiometricPixel* pGlobalRadiometricPixel = m_pRadiometricImage->GetBeginWritableBuffer();
                            for (uint Y = 0; Y < Height; ++Y)
                                for (uint X = 0; X < Width; ++X, ++pGlobalRadiometricPixel, ++pSourcePixel)
                                    if (FlagMapYX[Y & 0X1][X & 0X1])
                                    {
                                        m_RadiometricPixels.push_back(pGlobalRadiometricPixel->Initialize(X, Y, pSourcePixel));
                                    }
                            m_pRadiometricResponceFunction = new CGlobalRadiometricResponceFunction(m_CameraId, m_BayerPattern, m_ChannelContent);
                        }
                    }
                }

                CGlobalRadiometricCalibration::CGlobalRadiometricCalibration(const DeviceIdentifier CameraId, const TImage<real>* pSourceImage) :
                    m_CameraId(CameraId), m_BayerPattern(eUnknownBayerPattern), m_ChannelContent(ChromaticSpaceRGB::eUnknownChannelContent), m_pSourceImage(pSourceImage), m_pRadiometricImage(nullptr), m_pRadiometricResponceFunction(nullptr)
                {
                    if (m_pSourceImage && m_pSourceImage->IsValid())
                    {
                        const uint Width = m_pSourceImage->GetWidth();
                        const uint Height = m_pSourceImage->GetHeight();
                        m_pRadiometricImage = new TImage<CGlobalRadiometricPixel>(Width, Height);
                        const real* pSourcePixel = m_pSourceImage->GetBeginReadOnlyBuffer();
                        CGlobalRadiometricPixel* pGlobalRadiometricPixel = m_pRadiometricImage->GetBeginWritableBuffer();
                        for (uint Y = 0; Y < Height; ++Y)
                            for (uint X = 0; X < Width; ++X, ++pGlobalRadiometricPixel, ++pSourcePixel)
                            {
                                m_RadiometricPixels.push_back(pGlobalRadiometricPixel->Initialize(X, Y, pSourcePixel));
                            }
                        m_pRadiometricResponceFunction = new CGlobalRadiometricResponceFunction();
                    }
                }

                CGlobalRadiometricCalibration::~CGlobalRadiometricCalibration()
                {
                    RELEASE_OBJECT(m_pRadiometricImage)
                    RELEASE_OBJECT(m_pRadiometricResponceFunction)
                }

                bool CGlobalRadiometricCalibration::IsEnabled() const
                {
                    return m_RadiometricPixels.size();
                }

                DeviceIdentifier CGlobalRadiometricCalibration::GetCameraId() const
                {
                    return m_CameraId;
                }

                BayerPatternType CGlobalRadiometricCalibration::GetBayerPattern() const
                {
                    return m_BayerPattern;
                }

                ChromaticSpaceRGB::ChannelContent CGlobalRadiometricCalibration::GetChannelContent() const
                {
                    return m_ChannelContent;
                }

                bool CGlobalRadiometricCalibration::AddExposure(const real ExposureTime)
                {
                    if (m_RadiometricPixels.size())
                    {
                        list<CGlobalRadiometricPixel*>::const_iterator RadiometricCellsEnd = m_RadiometricPixels.end();
                        for (list<CGlobalRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin(); ppRadiometricCell != RadiometricCellsEnd; ++ppRadiometricCell)
                        {
                            (*ppRadiometricCell)->AddExposure();
                        }
                        m_Exposures.push_back(CExposure(ExposureTime));
                        return true;
                    }
                    return false;
                }

                bool CGlobalRadiometricCalibration::ClearExposures()
                {
                    if (m_Exposures.size())
                    {
                        list<CGlobalRadiometricPixel*>::const_iterator RadiometricCellsEnd = m_RadiometricPixels.end();
                        for (list<CGlobalRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin(); ppRadiometricCell != RadiometricCellsEnd; ++ppRadiometricCell)
                        {
                            (*ppRadiometricCell)->ClearExposures();
                        }
                        m_Exposures.clear();
                        return true;
                    }
                    return false;
                }

                uint CGlobalRadiometricCalibration::GetTotalExposures() const
                {
                    return m_Exposures.size();
                }

                bool CGlobalRadiometricCalibration::SetGaussianBoxKernelRadius(const real GaussianBoxKernelRadius)
                {
                    return m_RadianceWeigthingKernel.SetGaussianBoxKernelRadius(GaussianBoxKernelRadius);
                }

                bool CGlobalRadiometricCalibration::SetCommonGaussianKernelStandardDeviation(const real StandardDeviation)
                {
                    return m_RadianceWeigthingKernel.SetCommonGaussianKernelStandardDeviation(StandardDeviation);
                }

                bool CGlobalRadiometricCalibration::SetCommonGaussianKernelCutOff(const real CutOffDensity, const real CutOffNormalizedDeviation)
                {
                    return m_RadianceWeigthingKernel.SetCommonGaussianKernelCutOff(CutOffDensity, CutOffNormalizedDeviation);
                }

                real CGlobalRadiometricCalibration::GetGaussianBoxKernelRadius() const
                {
                    return m_RadianceWeigthingKernel.GetGaussianBoxKernelRadius();
                }

                real CGlobalRadiometricCalibration::GetCommonGaussianKernelStandardDeviation() const
                {
                    return m_RadianceWeigthingKernel.GetCommonGaussianKernelStandardDeviation();
                }

                bool CGlobalRadiometricCalibration::Calibrate(const PixelSelectionCriterion Criterion, const real DominaceRadius, const list<CGlobalRadiometricPixel*>& SelectedPixels, const real SmoothingLambda, const CRadianceWeigthingKernel::KernelType Kernel)
                {
                    const uint TotalExposures = m_Exposures.size();
                    if (TotalExposures >= _GLOBAL_RADIOMETRIC_CALIBRATION_MINIMAL_EXPOSURES_)
                    {
                        const uint TotalSelectedPixels = SelectedPixels.size();
                        if (TotalSelectedPixels >= _GLOBAL_RADIOMETRIC_CALIBRATION_MINIMAL_SELECTED_SAMPLING_CELLS_)
                        {
                            list<CGlobalRadiometricPixel*>::const_iterator ppGlobalRadiometricPixel = SelectedPixels.begin();
                            const uint TotalRows = TotalSelectedPixels * TotalExposures + 256 + 1;
                            const uint TotalCols = TotalSelectedPixels + 256;


                            //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN


                            Mathematics::ND::LinearAlgebra::CMatrixND A = Mathematics::ND::LinearAlgebra::CreateZeroMatrix(TotalRows, TotalCols);
                            Mathematics::ND::LinearAlgebra::CMatrixND B = Mathematics::ND::LinearAlgebra::CreateZeroMatrix(TotalRows, 1);
                            uint Row = 0;
                            for (uint i = 0, ip = 256; i < TotalSelectedPixels; ++i, ++ip, ++ppGlobalRadiometricPixel)
                            {
                                list<CExposure>::const_iterator pExposure = m_Exposures.begin();
                                list<real>::const_iterator pValue = (*ppGlobalRadiometricPixel)->GetValues().begin();
                                for (uint j = 0; j < TotalExposures; ++j, ++pExposure, ++pValue, ++Row)
                                {
                                    const real Wij = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(*pValue, Kernel);
                                    A(Row, RoundToInteger(*pValue)) = Mathematics::ND::LinearAlgebra::MatrixReal(Wij);
                                    A(Row, ip) = Mathematics::ND::LinearAlgebra::MatrixReal(-Wij);
                                    B(Row, 0) = Mathematics::ND::LinearAlgebra::MatrixReal(Wij * RealLog(pExposure->GetTime()));
                                }
                            }
                            A(Row++, 128) = _REAL_ONE_;
                            for (uint i = 0; i < 255; ++i, ++Row)
                            {
                                const real Wij = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(real(i), Kernel);
                                A(Row, i) = Mathematics::ND::LinearAlgebra::MatrixReal(SmoothingLambda * Wij);
                                A(Row, i + 1) = Mathematics::ND::LinearAlgebra::MatrixReal(-_REAL_TWO_ * SmoothingLambda * Wij);
                                A(Row, i + 2) = Mathematics::ND::LinearAlgebra::MatrixReal(SmoothingLambda * Wij);
                            }
                            //try
                            //{
                            Mathematics::ND::LinearAlgebra::CMatrixND X = Mathematics::ND::LinearAlgebra::PseudoInverseSVD(A) * B;
                            real ResponseFunction[256] = { _REAL_ZERO_ };
                            real RegressionWeightingFunction[256] = { _REAL_ZERO_ };
                            for (uint i = 0; i < 256; ++i)
                            {
                                ResponseFunction[i] = real(X(i, 0));
                                RegressionWeightingFunction[i] = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(real(i), Kernel);
                            }
                            if (m_pRadiometricResponceFunction->LoadResponceFunctionFromParameters(m_CameraId, m_BayerPattern, m_ChannelContent, TotalExposures, TotalSelectedPixels, DominaceRadius, SmoothingLambda, Criterion, ResponseFunction, RegressionWeightingFunction, m_RadianceWeigthingKernel))
                            {
                                return m_pRadiometricResponceFunction->EstimateIdealResponceFunction(Kernel);
                            }
                            //} catch (Mathematics::ND::LinearAlgebra::CMatrixBaseException& E)
                            //{
                            //  g_ErrorStringOutput << E.what() << g_EndLine;
                            //}
                        }
                    }
                    return false;
                }

                bool CGlobalRadiometricCalibration::Calibrate(const PixelSelectionCriterion Criterion, const real DominaceRadius, const uint MaximalSamples, const real SmoothingLambda, const CRadianceWeigthingKernel::KernelType Kernel)
                {
                    if (m_Exposures.size() >= _GLOBAL_RADIOMETRIC_CALIBRATION_MINIMAL_EXPOSURES_)
                    {
                        return Calibrate(Criterion, DominaceRadius, SelectPixelsByCriterion(Criterion, DominaceRadius, TMax(MaximalSamples, _GLOBAL_RADIOMETRIC_CALIBRATION_MINIMAL_SELECTED_SAMPLING_CELLS_)), SmoothingLambda, Kernel);
                    }
                    return false;
                }

                list<CGlobalRadiometricPixel*> CGlobalRadiometricCalibration::SelectPixelsByCriterion(const PixelSelectionCriterion Criterion, const real DominaceRadius, const uint MaximalSamples)
                {
                    list<CGlobalRadiometricPixel*> SelectedPixels;
                    list<CGlobalRadiometricPixel*>::const_iterator RadiometricCellsEnd = m_RadiometricPixels.end();
                    switch (Criterion)
                    {
                        case eUnknownPixelSelectionCriterion:
                            return SelectedPixels;
                            break;
                        case eMaximalStandardDeviation:
                        case eMinimalStandardDeviation:
                            for (list<CGlobalRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin(); ppRadiometricCell != RadiometricCellsEnd; ++ppRadiometricCell)
                            {
                                (*ppRadiometricCell)->CalculateStandardDeviationCriterion();
                            }
                            break;
                        case eMaximalRange:
                        case eMinimalRange:
                            for (list<CGlobalRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin(); ppRadiometricCell != RadiometricCellsEnd; ++ppRadiometricCell)
                            {
                                (*ppRadiometricCell)->CalculateRangeCriterion();
                            }
                            break;
                    }
                    switch (Criterion)
                    {
                        case eUnknownPixelSelectionCriterion:
                        case eMaximalStandardDeviation:
                        case eMaximalRange:
                            m_RadiometricPixels.sort(SortRadiometricPixelsByCriterionDescent);
                            break;
                        case eMinimalStandardDeviation:
                        case eMinimalRange:
                            m_RadiometricPixels.sort(SortRadiometricPixelsByCriterionAscent);
                            break;
                    }
                    const list<CPixelLocation> LocationDeltas = ComputeLocationDeltas(DominaceRadius);
                    list<CPixelLocation>::const_iterator LocationDeltasEnd = LocationDeltas.end();
                    list<CPixelLocation>::const_iterator LocationDeltasBegin = LocationDeltas.begin();
                    const coordinate Width = m_pRadiometricImage->GetWidth();
                    const coordinate Height = m_pRadiometricImage->GetHeight();
                    RadiometricCellsEnd = m_RadiometricPixels.end();
                    for (list<CGlobalRadiometricPixel*>::const_iterator ppRadiometricCell = m_RadiometricPixels.begin(); ppRadiometricCell != RadiometricCellsEnd; ++ppRadiometricCell)
                        if ((*ppRadiometricCell)->IsActive())
                        {
                            SelectedPixels.push_back(*ppRadiometricCell);
                            if (SelectedPixels.size() < MaximalSamples)
                            {
                                const CPixelLocation& PixelLocation = (*ppRadiometricCell)->GetLocation();
                                for (list<CPixelLocation>::const_iterator pLocationDelta = LocationDeltasBegin; pLocationDelta != LocationDeltasEnd; ++pLocationDelta)
                                {
                                    const CPixelLocation DeactivatingLocation = PixelLocation + *pLocationDelta;
                                    if ((DeactivatingLocation.m_x >= 0) && (DeactivatingLocation.m_y >= 0) && (DeactivatingLocation.m_x < Width) && (DeactivatingLocation.m_y < Height))
                                    {
                                        m_pRadiometricImage->GetWritableBufferAt(DeactivatingLocation)->SetActive(false);
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    return SelectedPixels;
                }

                const CGlobalRadiometricResponceFunction* CGlobalRadiometricCalibration::GetRadiometricResponceFunction() const
                {
                    return m_pRadiometricResponceFunction;
                }

                list<CPixelLocation> CGlobalRadiometricCalibration::ComputeLocationDeltas(const real DominaceRadius)
                {
                    list<CPixelLocation> DeltaLocations;
                    const coordinate DiscreteRadius = TMax(UpperToInteger(DominaceRadius), 1);
                    for (CPixelLocation DeltaLocation(-DiscreteRadius, -DiscreteRadius); DeltaLocation.m_y <= DiscreteRadius; ++DeltaLocation.m_y)
                        for (DeltaLocation.m_x = -DiscreteRadius; DeltaLocation.m_x <= DiscreteRadius; ++DeltaLocation.m_x)
                            if (DeltaLocation.GetLength() <= DominaceRadius)
                            {
                                DeltaLocations.push_back(DeltaLocation);
                            }
                    return DeltaLocations;
                }

                bool CGlobalRadiometricCalibration::SortRadiometricPixelsByCriterionAscent(const CGlobalRadiometricPixel* plhs, const CGlobalRadiometricPixel* rlhs)
                {
                    return plhs->GetCriterion() < rlhs->GetCriterion();
                }

                bool CGlobalRadiometricCalibration::SortRadiometricPixelsByCriterionDescent(const CGlobalRadiometricPixel* plhs, const CGlobalRadiometricPixel* rlhs)
                {
                    return plhs->GetCriterion() > rlhs->GetCriterion();
                }
            }
        }
    }
}
