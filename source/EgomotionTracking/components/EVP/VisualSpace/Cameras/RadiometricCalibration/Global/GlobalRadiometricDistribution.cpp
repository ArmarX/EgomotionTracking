/*
 * GlobalRadiometricDistribution.cpp
 *
 *  Created on: 13.05.2012
 *      Author: gonzalez
 */

#include "GlobalRadiometricDistribution.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {

                CGlobalRadiometricDistribution::CGlobalRadiometricDistribution() :
                    m_Kernel(CRadianceWeigthingKernel::eGaussian), m_pExposureDistribution(nullptr), m_pExposureDualIndices(nullptr), m_MaximalIntensity(_REAL_255_), m_MinimalIntensity(_REAL_ZERO_), m_UpperIntensityAtScopeDensity(_REAL_255_), m_LowerIntensityAtScopeDensity(_REAL_ZERO_), m_ScopeKernelDensity(_REAL_EPSILON_), m_ConsistentSafeMaximalKernelDensity(_REAL_EPSILON_), m_ScaleFactorLogExposureTime(_REAL_ZERO_), m_TotalUnderExposedPixels(0), m_TotalOverExposedPixels(0)
                {
                }

                CGlobalRadiometricDistribution::~CGlobalRadiometricDistribution()
                {
                    RELEASE_ARRAY(m_pExposureDistribution)
                    RELEASE_ARRAY(m_pExposureDualIndices)
                }

                bool CGlobalRadiometricDistribution::AddSynthesizer(const CGlobalRadiometricBayerPatternSynthesizer* pBayerPatternSynthesizer)
                {
                    if (pBayerPatternSynthesizer)
                    {
                        const DeviceIdentifier CameraId = pBayerPatternSynthesizer->GetCameraId();
                        list<const CGlobalRadiometricBayerPatternSynthesizer*>::const_iterator SynthesizersEnd = m_Synthesizers.end();
                        for (list<const CGlobalRadiometricBayerPatternSynthesizer*>::const_iterator ppGlobalRadiometricBayerPatternSynthesizer = m_Synthesizers.begin(); ppGlobalRadiometricBayerPatternSynthesizer != SynthesizersEnd; ++ppGlobalRadiometricBayerPatternSynthesizer)
                            if ((*ppGlobalRadiometricBayerPatternSynthesizer)->GetCameraId() == CameraId)
                            {
                                return false;
                            }
                        m_Synthesizers.push_front(pBayerPatternSynthesizer);
                        const CGlobalMultipleChannelRadiometricResponceFunction* pGlobalMultipleChannelRadiometricResponceFunction = pBayerPatternSynthesizer->GetGlobalMultipleChannelRadiometricResponceFunction();
                        m_GlobalRadiometricResponceFunctions.push_back(pGlobalMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction());
                        m_GlobalRadiometricResponceFunctions.push_back(pGlobalMultipleChannelRadiometricResponceFunction->GetGreenChannelResponceFunction());
                        m_GlobalRadiometricResponceFunctions.push_back(pGlobalMultipleChannelRadiometricResponceFunction->GetBlueChannelResponceFunction());
                        return true;
                    }
                    return false;
                }

                bool CGlobalRadiometricDistribution::RemoveAllSynthesizers()
                {
                    if (m_Synthesizers.size())
                    {
                        m_Synthesizers.clear();
                        return true;
                    }
                    return false;
                }

                bool CGlobalRadiometricDistribution::LoadConfiguration(const CRadianceWeigthingKernel::KernelType Kernel, const real ScopeKernelDensity, const real IntensityMargin)
                {
                    if (LoadExposuresFromSynthesizers())
                    {
                        if (UpdateMinimalIntensity(IntensityMargin))
                        {
                            if (UpdateConsistentSafeMaximalKernelDensity(Kernel))
                            {
                                if (CreateCalibratedExposures(Kernel))
                                {
                                    if (CreateExposureDistribution())
                                    {
                                        if (CreateExposureDualIndices())
                                        {
                                            return ConfigureExposureScopeByDensity(ScopeKernelDensity);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return false;
                }

                bool CGlobalRadiometricDistribution::LoadDistributionFromSynthesizers(const real CriticallKernelDensity)
                {
                    if (m_Synthesizers.size() && m_CalibratedExposures.size())
                    {
                        m_TotalUnderExposedPixels = 0;
                        m_TotalOverExposedPixels = 0;
                        bool Result = ClearDistribution();
                        list<const CGlobalRadiometricBayerPatternSynthesizer*>::const_iterator SynthesizersEnd = m_Synthesizers.end();
                        for (list<const CGlobalRadiometricBayerPatternSynthesizer*>::const_iterator ppGlobalRadiometricBayerPatternSynthesizer = m_Synthesizers.begin(); ppGlobalRadiometricBayerPatternSynthesizer != SynthesizersEnd; ++ppGlobalRadiometricBayerPatternSynthesizer)
                        {
                            const CGlobalRadiometricBayerPatternSynthesizer* pBayerPatternSynthesizer = *ppGlobalRadiometricBayerPatternSynthesizer;
                            Result &= MapExposureFrequencyDistribution(pBayerPatternSynthesizer->GetGlobalMultipleChannelRadiometricResponceFunction(), pBayerPatternSynthesizer->GetLogRadianceImage(), pBayerPatternSynthesizer->GetDensityWeightedImage(), pBayerPatternSynthesizer->GetBayerPattern(), CriticallKernelDensity);
                        }
                        return Result;
                    }
                    return false;
                }

                bool CGlobalRadiometricDistribution::ClearDistribution()
                {
                    const uint TotalExposures = m_CalibratedExposures.size();
                    if (TotalExposures)
                    {
                        ExposureBin* pExposureBin = m_pExposureDistribution;
                        for (uint i = 0; i < TotalExposures; ++i, ++pExposureBin)
                        {
                            pExposureBin->m_Active = true;
                            pExposureBin->m_SamplingDensityAccumulator = _REAL_ZERO_;
                            pExposureBin->m_ScopeSamplingDensityAccumulator = _REAL_ZERO_;
                            pExposureBin->m_MinimalDensity = _REAL_MAX_;
                        }
                        return true;
                    }
                    return false;
                }

                bool CGlobalRadiometricDistribution::LoadExposuresFromSynthesizers()
                {
                    m_Exposures.clear();
                    list<const CGlobalRadiometricBayerPatternSynthesizer*>::const_iterator SynthesizersEnd = m_Synthesizers.end();
                    for (list<const CGlobalRadiometricBayerPatternSynthesizer*>::const_iterator ppGlobalRadiometricBayerPatternSynthesizer = m_Synthesizers.begin(); ppGlobalRadiometricBayerPatternSynthesizer != SynthesizersEnd; ++ppGlobalRadiometricBayerPatternSynthesizer)
                    {
                        const list<CExposure>& Exposures = (*ppGlobalRadiometricBayerPatternSynthesizer)->GetExposures();
                        m_Exposures.insert(m_Exposures.end(), Exposures.begin(), Exposures.end());
                    }
                    m_Exposures.sort(CExposure::SortExposuresByParametricIndex);
                    m_Exposures.unique(CExposure::EqualsExposuresByParametricIndex);
                    return m_Exposures.size();
                }

                bool CGlobalRadiometricDistribution::UpdateMinimalIntensity(const real IntensityMargin)
                {
                    if (m_GlobalRadiometricResponceFunctions.size())
                    {
                        m_MaximalIntensity = _REAL_255_ - IntensityMargin;
                        m_MinimalIntensity = IntensityMargin;
                        list<const CGlobalRadiometricResponceFunction*>::const_iterator GlobalRadiometricResponceFunctionsEnd = m_GlobalRadiometricResponceFunctions.end();
                        for (list<const CGlobalRadiometricResponceFunction*>::const_iterator ppGlobalRadiometricResponceFunction = m_GlobalRadiometricResponceFunctions.begin(); ppGlobalRadiometricResponceFunction != GlobalRadiometricResponceFunctionsEnd; ++ppGlobalRadiometricResponceFunction)
                        {
                            const real MinimalIntensity = (*ppGlobalRadiometricResponceFunction)->GetMinimalIntensity();
                            if (MinimalIntensity > m_MinimalIntensity)
                            {
                                m_MinimalIntensity = MinimalIntensity;
                            }
                        }
                        m_MinimalIntensity += _REAL_EPSILON_;
                        return true;
                    }
                    return false;
                }

                bool CGlobalRadiometricDistribution::UpdateConsistentSafeMaximalKernelDensity(const CRadianceWeigthingKernel::KernelType Kernel)
                {
                    m_Kernel = Kernel;
                    m_ConsistentSafeMaximalKernelDensity = _REAL_ONE_;
                    real PreviousTime = m_Exposures.front().GetTime();
                    list<const CGlobalRadiometricResponceFunction*>::const_iterator GlobalRadiometricResponceFunctionsBegin = m_GlobalRadiometricResponceFunctions.begin(), GlobalRadiometricResponceFunctionsEnd = m_GlobalRadiometricResponceFunctions.end();
                    list<CExposure>::const_iterator ExposuresEnd = m_Exposures.end();
                    for (list<CExposure>::const_iterator pExposure = ++m_Exposures.begin(); pExposure != ExposuresEnd; ++pExposure)
                    {
                        const real CurrentTime = pExposure->GetTime();
                        for (list<const CGlobalRadiometricResponceFunction*>::const_iterator ppGlobalRadiometricResponceFunction = GlobalRadiometricResponceFunctionsBegin; ppGlobalRadiometricResponceFunction != GlobalRadiometricResponceFunctionsEnd; ++ppGlobalRadiometricResponceFunction)
                        {
                            const CGlobalRadiometricResponceFunction* pResponceFunction = *ppGlobalRadiometricResponceFunction;
                            const real EA1 = pResponceFunction->GetIdealRadiance(m_MaximalIntensity, PreviousTime);
                            const real EA0 = pResponceFunction->GetIdealRadiance(m_MinimalIntensity, PreviousTime);
                            const real EB1 = pResponceFunction->GetIdealRadiance(m_MaximalIntensity, CurrentTime);
                            const real EB0 = pResponceFunction->GetIdealRadiance(m_MinimalIntensity, CurrentTime);
                            if ((EA0 > EB1) || (EA1 < EB0))
                            {
                                m_ConsistentSafeMaximalKernelDensity = _REAL_ZERO_;
                                return false;
                            }
                            const real E1 = TMin(EA1, EB1);
                            const real E0 = TMax(EA0, EB0);
                            const real E = (E1 + E0) * _REAL_HALF_;
                            const real IA = TMax(RealFloor(pResponceFunction->GetIdealIntensity(E, PreviousTime)) - _REAL_ONE_, m_MinimalIntensity);
                            const real IB = TMin(RealCeil(pResponceFunction->GetIdealIntensity(E, CurrentTime)) + _REAL_ONE_, m_MaximalIntensity);
                            const real DIA = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(IA, Kernel);
                            const real DIB = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(IB, Kernel);
                            m_ConsistentSafeMaximalKernelDensity = TMin(m_ConsistentSafeMaximalKernelDensity, TMin(DIA, DIB));
                        }
                        PreviousTime = CurrentTime;
                    }
                    m_ConsistentSafeMaximalKernelDensity = TMax(m_ConsistentSafeMaximalKernelDensity - _REAL_EPSILON_, _REAL_EPSILON_);
                    return true;
                }

                bool CGlobalRadiometricDistribution::CreateCalibratedExposures(const CRadianceWeigthingKernel::KernelType Kernel)
                {
                    m_CalibratedExposures.clear();
                    m_CalibratedExposures.reserve(m_Exposures.size());
                    const real DensityConditionedUpperIntensity = TMin(RealCeil(m_RadianceWeigthingKernel.GetUpperIntensityAtDensity(Kernel, m_ConsistentSafeMaximalKernelDensity)) + _REAL_ONE_, m_MaximalIntensity);
                    const real DensityConditionedLowerIntensity = TMax(RealFloor(m_RadianceWeigthingKernel.GetLowerIntensityAtDensity(Kernel, m_ConsistentSafeMaximalKernelDensity)) - _REAL_ONE_, m_MinimalIntensity);
                    list<const CGlobalRadiometricResponceFunction*>::const_iterator GlobalRadiometricResponceFunctionsBegin = m_GlobalRadiometricResponceFunctions.begin(), GlobalRadiometricResponceFunctionsEnd = m_GlobalRadiometricResponceFunctions.end();
                    list<CExposure>::const_iterator ExposuresEnd = m_Exposures.end();
                    for (list<CExposure>::const_iterator pExposure = m_Exposures.begin(); pExposure != ExposuresEnd; ++pExposure)
                    {
                        const real ExposureTime = pExposure->GetTime();
                        real CommonE1 = _REAL_MAX_, CommonE0 = _REAL_MIN_, CommonT0 = _REAL_MIN_, CommonT1 = _REAL_MAX_;
                        for (list<const CGlobalRadiometricResponceFunction*>::const_iterator ppGlobalRadiometricResponceFunction = GlobalRadiometricResponceFunctionsBegin; ppGlobalRadiometricResponceFunction != GlobalRadiometricResponceFunctionsEnd; ++ppGlobalRadiometricResponceFunction)
                        {
                            const CGlobalRadiometricResponceFunction* pResponceFunction = *ppGlobalRadiometricResponceFunction;
                            const real E1 = pResponceFunction->GetIdealRadiance(DensityConditionedUpperIntensity, ExposureTime);
                            const real E0 = pResponceFunction->GetIdealRadiance(DensityConditionedLowerIntensity, ExposureTime);
                            const real T0 = pResponceFunction->GetIdealExposureTime(DensityConditionedLowerIntensity, E1);
                            const real T1 = pResponceFunction->GetIdealExposureTime(DensityConditionedUpperIntensity, E0);
                            if (E1 < CommonE1)
                            {
                                CommonE1 = E1;
                            }
                            if (E0 > CommonE0)
                            {
                                CommonE0 = E0;
                            }
                            if (T1 < CommonT1)
                            {
                                CommonT1 = T1;
                            }
                            if (T0 > CommonT0)
                            {
                                CommonT0 = T0;
                            }
                        }
                        if ((CommonT0 >= CommonT1) || (CommonE0 >= CommonE1))
                        {
                            m_CalibratedExposures.clear();
                            return false;
                        }
                        m_CalibratedExposures.push_back(CCalibratedExposure(pExposure->GetParametricIndex(), m_ConsistentSafeMaximalKernelDensity, CommonE0, CommonE1, CommonT0, ExposureTime, CommonT1));
                    }
                    return true;
                }

                list<CCalibratedExposure> CGlobalRadiometricDistribution::GetMinimalExposureSet() const
                {
                    const uint TotalCalibratedExposures = m_CalibratedExposures.size();
                    list<CCalibratedExposure> SelectedCalibratedExposures;
                    if (TotalCalibratedExposures)
                    {
                        CCalibratedExposure SelectedCalibratedExposure = m_CalibratedExposures[0];
                        SelectedCalibratedExposures.push_back(SelectedCalibratedExposure);
                        for (uint i = 1; i < TotalCalibratedExposures; ++i)
                            if (SelectedCalibratedExposure.HasCommonRadianceScope(m_CalibratedExposures[i]))
                            {
                                CCalibratedExposure MinimalOverlappingExposure = m_CalibratedExposures[i];
                                real MinimalOverlapping = SelectedCalibratedExposure.GetCommonRadianceScope(MinimalOverlappingExposure);
                                for (uint j = i + 1; j < TotalCalibratedExposures; ++j)
                                    if (SelectedCalibratedExposure.HasCommonRadianceScope(m_CalibratedExposures[j]))
                                    {
                                        const real Overlapping = SelectedCalibratedExposure.GetCommonRadianceScope(m_CalibratedExposures[j]);
                                        if (Overlapping < MinimalOverlapping)
                                        {
                                            MinimalOverlapping = Overlapping;
                                            MinimalOverlappingExposure = m_CalibratedExposures[j];
                                            i = j;
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                SelectedCalibratedExposures.push_back(MinimalOverlappingExposure);
                                SelectedCalibratedExposure = MinimalOverlappingExposure;
                            }
                            else
                            {
                                SelectedCalibratedExposure = m_CalibratedExposures[i];
                                SelectedCalibratedExposures.push_back(SelectedCalibratedExposure);
                            }
                    }
                    return SelectedCalibratedExposures;
                }

                bool CGlobalRadiometricDistribution::CreateExposureDistribution()
                {
                    RELEASE_ARRAY(m_pExposureDistribution)
                    const uint TotalExposures = m_CalibratedExposures.size();
                    if (TotalExposures)
                    {
                        m_pExposureDistribution = new ExposureBin[TotalExposures];
                        memset(m_pExposureDistribution, 0, sizeof(ExposureBin) * TotalExposures);
                        return true;
                    }
                    return false;
                }

                bool CGlobalRadiometricDistribution::CreateExposureDualIndices()
                {
                    RELEASE_ARRAY(m_pExposureDualIndices)
                    const uint TotalExposures = m_CalibratedExposures.size();
                    if (TotalExposures)
                    {
                        LongDoubleReal MinimalTemporalSegment = LDBL_MAX;
                        for (uint i = 1; i < TotalExposures; ++i)
                        {
                            const LongDoubleReal DeltaLogT = fabsl(logl(LongDoubleReal(m_CalibratedExposures[i].GetTime())) - logl(LongDoubleReal(m_CalibratedExposures[i - 1].GetTime())));
                            if (DeltaLogT > LDBL_EPSILON)
                            {
                                MinimalTemporalSegment = TMin(DeltaLogT, MinimalTemporalSegment);
                            }
                        }
                        const LongDoubleReal MinimalExposureTime = logl(LongDoubleReal(m_CalibratedExposures.front().GetScopeMinimalExposureTime()));
                        const LongDoubleReal MaximalExposureTime = logl(LongDoubleReal(m_CalibratedExposures.back().GetScopeMaximalExposureTime()));
                        const LongDoubleReal TemporalRange = MaximalExposureTime - MinimalExposureTime;
                        const uint TotalIndices = ceill((TemporalRange * LongDoubleReal(3.0)) / MinimalTemporalSegment);
                        m_pExposureDualIndices = new ExposureDualIndex[TotalIndices + 1];
                        memset(m_pExposureDualIndices, 0, sizeof(ExposureDualIndex) * (TotalIndices + 1));
                        ExposureDualIndex* pExposureIndex = m_pExposureDualIndices + TotalIndices;
                        pExposureIndex->m_IndexB = pExposureIndex->m_IndexA = TotalExposures - 1;
                        pExposureIndex->m_ExposureTimeThreshold = _REAL_MAX_;
                        pExposureIndex = m_pExposureDualIndices;
                        const LongDoubleReal TemporalDelta = TemporalRange / LongDoubleReal(TotalIndices);
                        m_ScaleFactorLogExposureTime = real(LongDoubleReal(TotalIndices) / TemporalRange);
                        LongDoubleReal LTA = MinimalExposureTime, LTB = MinimalExposureTime + TemporalDelta;
                        uint SearchIndex = 0;
                        for (uint i = 0; i < TotalIndices; ++i, LTA += TemporalDelta, LTB += TemporalDelta, ++pExposureIndex)
                        {
                            const DoubleReal TA = DoubleReal(expl(LTA));
                            const DoubleReal TB = DoubleReal(expl(LTB));
                            for (uint j = SearchIndex; j < TotalExposures; ++j)
                                if (m_CalibratedExposures[j].HasCommonTemporalScope(TA, TB))
                                {
                                    real MinimalDeviationA = m_CalibratedExposures[j].GetAbsoluteTimeDeltaToCentralExposureTime(TA);
                                    real MinimalDeviationB = m_CalibratedExposures[j].GetAbsoluteTimeDeltaToCentralExposureTime(TB);
                                    uint BestMatchIndexA = j, BestMatchIndexB = j;
                                    for (uint k = j + 1; k < TotalExposures; ++k)
                                        if (m_CalibratedExposures[k].HasCommonTemporalScope(TA, TB))
                                        {
                                            const real DeviationA = m_CalibratedExposures[k].GetAbsoluteTimeDeltaToCentralExposureTime(TA);
                                            if (DeviationA < MinimalDeviationA)
                                            {
                                                MinimalDeviationA = DeviationA;
                                                BestMatchIndexA = k;
                                            }
                                            const real DeviationB = m_CalibratedExposures[k].GetAbsoluteTimeDeltaToCentralExposureTime(TB);
                                            if (DeviationB < MinimalDeviationB)
                                            {
                                                MinimalDeviationB = DeviationB;
                                                BestMatchIndexB = k;
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    pExposureIndex->m_IndexA = BestMatchIndexA;
                                    pExposureIndex->m_IndexB = BestMatchIndexB;
                                    pExposureIndex->m_ExposureTimeThreshold = (BestMatchIndexA != BestMatchIndexB) ? (RealLog(m_CalibratedExposures[BestMatchIndexA].GetTime()) + RealLog(m_CalibratedExposures[BestMatchIndexB].GetTime())) * _REAL_HALF_ : _REAL_MAX_;
                                    SearchIndex = BestMatchIndexA;
                                    break;
                                }
                        }
                        return true;
                    }
                    return false;
                }

                list<CGlobalRadiometricDistribution::ExposureBin*> CGlobalRadiometricDistribution::LoadExposureScopeFrequencyDistribution()
                {
                    list<ExposureBin*> ExposureScopeFrequencyDistribution;
                    ExposureBin* pExposureBin = m_pExposureDistribution;
                    const uint TotalExposures = m_CalibratedExposures.size();
                    for (uint i = 0; i < TotalExposures; ++i, ++pExposureBin)
                    {
                        pExposureBin->m_ScopeSamplingDensityAccumulator = _REAL_ZERO_;
                        pExposureBin->m_Active = (pExposureBin->m_SamplingDensityAccumulator && pExposureBin->m_Active);
                        if (pExposureBin->m_Active)
                        {
                            for (uint i = pExposureBin->m_ScopeIndexA; i <= pExposureBin->m_ScopeIndexB; ++i)
                                if (m_pExposureDistribution[i].m_Active)
                                {
                                    pExposureBin->m_ScopeSamplingDensityAccumulator += m_pExposureDistribution[i].m_SamplingDensityAccumulator;
                                }
                            ExposureScopeFrequencyDistribution.push_back(pExposureBin);
                        }
                    }
                    return ExposureScopeFrequencyDistribution;
                }

                list<CGlobalRadiometricDistribution::ExposureBin*> CGlobalRadiometricDistribution::DetermineExposureScopeFrequencyDistribution(const list<ExposureBin*>& ExposureDistribution)
                {
                    list<ExposureBin*> ExposureScopeFrequencyDistribution;
                    list<ExposureBin*>::const_iterator ExposureDistributionEnd = ExposureDistribution.end();
                    for (list<ExposureBin*>::const_iterator ppExposureBin = ExposureDistribution.begin(); ppExposureBin != ExposureDistributionEnd; ++ppExposureBin)
                    {
                        ExposureBin* pExposureBin = *ppExposureBin;
                        pExposureBin->m_ScopeSamplingDensityAccumulator = _REAL_ZERO_;
                        pExposureBin->m_Active = ((pExposureBin->m_SamplingDensityAccumulator > _REAL_ONE_) && pExposureBin->m_Active);
                        if (pExposureBin->m_Active)
                        {
                            for (uint i = pExposureBin->m_ScopeIndexA; i <= pExposureBin->m_ScopeIndexB; ++i)
                                if (m_pExposureDistribution[i].m_Active)
                                {
                                    pExposureBin->m_ScopeSamplingDensityAccumulator += m_pExposureDistribution[i].m_SamplingDensityAccumulator;
                                }
                            ExposureScopeFrequencyDistribution.push_back(pExposureBin);
                        }
                    }
                    return ExposureScopeFrequencyDistribution;
                }

                CGlobalRadiometricDistribution::ExposureBin* CGlobalRadiometricDistribution::GetExposureBinMinimalIntegrationTime(const list<ExposureBin*>& ExposureScopeFrequencyDistribution)
                {
                    ExposureBin* pSelectedExposureBin = nullptr;
                    uint MinimalIndex = m_CalibratedExposures.size();
                    list<ExposureBin*>::const_iterator ExposureBinEnd = ExposureScopeFrequencyDistribution.end();
                    for (list<ExposureBin*>::const_iterator ppExposureBin = ExposureScopeFrequencyDistribution.begin(); ppExposureBin != ExposureBinEnd; ++ppExposureBin)
                    {
                        ExposureBin* pExposureBin = *ppExposureBin;
                        if (pExposureBin->m_Active && (pExposureBin->m_ScopeSamplingDensityAccumulator > _REAL_ONE_) && (pExposureBin->m_Index < MinimalIndex))
                        {
                            pSelectedExposureBin = pExposureBin;
                            MinimalIndex = pExposureBin->m_Index;
                        }
                    }
                    return pSelectedExposureBin;
                }

                CGlobalRadiometricDistribution::ExposureBin* CGlobalRadiometricDistribution::GetExposureBinByMinimalDensity(const list<ExposureBin*>& ExposureScopeFrequencyDistribution)
                {
                    ExposureBin* pSelectedExposureBin = nullptr;
                    real MinimalDensity = _REAL_MAX_;
                    list<ExposureBin*>::const_iterator ExposureBinEnd = ExposureScopeFrequencyDistribution.end();
                    for (list<ExposureBin*>::const_iterator ppExposureBin = ExposureScopeFrequencyDistribution.begin(); ppExposureBin != ExposureBinEnd; ++ppExposureBin)
                    {
                        ExposureBin* pExposureBin = *ppExposureBin;
                        if (pExposureBin->m_Active && (pExposureBin->m_ScopeSamplingDensityAccumulator > _REAL_ONE_) && (pExposureBin->m_MinimalDensity < MinimalDensity))
                        {
                            pSelectedExposureBin = pExposureBin;
                            MinimalDensity = pExposureBin->m_MinimalDensity;
                        }
                    }
                    return pSelectedExposureBin;
                }

                CGlobalRadiometricDistribution::ExposureBin* CGlobalRadiometricDistribution::GetExposureBinByMaximalScopeFrequency(const list<ExposureBin*>& ExposureScopeFrequencyDistribution)
                {
                    ExposureBin* pSelectedExposureBin = nullptr;
                    uint MaximalScopeFrequency = 0;
                    list<ExposureBin*>::const_iterator ExposureBinEnd = ExposureScopeFrequencyDistribution.end();
                    for (list<ExposureBin*>::const_iterator ppExposureBin = ExposureScopeFrequencyDistribution.begin(); ppExposureBin != ExposureBinEnd; ++ppExposureBin)
                    {
                        ExposureBin* pExposureBin = *ppExposureBin;
                        if (pExposureBin->m_Active && (pExposureBin->m_ScopeSamplingDensityAccumulator > _REAL_ONE_) && (pExposureBin->m_ScopeSamplingDensityAccumulator > MaximalScopeFrequency))
                        {
                            pSelectedExposureBin = pExposureBin;
                            MaximalScopeFrequency = pExposureBin->m_ScopeSamplingDensityAccumulator;
                        }
                    }
                    return pSelectedExposureBin;
                }

                list<CCalibratedExposure> CGlobalRadiometricDistribution::SelectExposureIndicesByCriterion(const OptimizationCriterion Criterion)
                {
                    list<CCalibratedExposure> SelectedExposures;
                    list<ExposureBin*> ExposureScopeFrequencyDistribution = LoadExposureScopeFrequencyDistribution();
                    if (ExposureScopeFrequencyDistribution.size())
                    {
                        ExposureBin* pExposureBin = nullptr;
                        do
                        {
                            switch (Criterion)
                            {
                                case eUnkownSelectionCriterion:
                                    pExposureBin = GetExposureBinMinimalIntegrationTime(ExposureScopeFrequencyDistribution);
                                    break;
                                case eEnsureMinimalDensity:
                                    pExposureBin = GetExposureBinByMinimalDensity(ExposureScopeFrequencyDistribution);
                                    break;
                                case eEnsureMinimalExposures:
                                    pExposureBin = GetExposureBinByMaximalScopeFrequency(ExposureScopeFrequencyDistribution);
                                    break;
                            }
                            if (pExposureBin)
                            {
                                ExposureBin* pExposureBinA = m_pExposureDistribution + pExposureBin->m_ScopeIndexA;
                                ExposureBin* pExposureBinB = m_pExposureDistribution + pExposureBin->m_ScopeIndexB;
                                const real BandWidth = TMax(pExposureBin->m_LogTime - pExposureBinA->m_LogTime, pExposureBinB->m_LogTime - pExposureBin->m_LogTime);
                                for (ExposureBin* pExposureBinSliding = pExposureBinA; pExposureBinSliding <= pExposureBinB; ++pExposureBinSliding)
                                    if (pExposureBinSliding->m_Active)
                                    {
                                        const real NormalizedTimeDelta = (pExposureBinSliding->m_LogTime - pExposureBin->m_LogTime) / BandWidth;
                                        pExposureBinSliding->m_SamplingDensityAccumulator *= (NormalizedTimeDelta * NormalizedTimeDelta);
                                    }
                                SelectedExposures.push_back(m_CalibratedExposures[pExposureBin->m_Index]);
                                ExposureScopeFrequencyDistribution = DetermineExposureScopeFrequencyDistribution(ExposureScopeFrequencyDistribution);
                            }
                            else
                            {
                                break;
                            }
                        }
                        while (ExposureScopeFrequencyDistribution.size());
                        SelectedExposures.sort(CExposure::SortExposuresByParametricIndex);
                    }
                    return SelectedExposures;
                }

                real CGlobalRadiometricDistribution::GetMaximalIntensity() const
                {
                    return m_MaximalIntensity;
                }

                real CGlobalRadiometricDistribution::GetMinimalIntensity() const
                {
                    return m_MinimalIntensity;
                }

                real CGlobalRadiometricDistribution::GetUpperIntensityAtScopeDensity() const
                {
                    return m_UpperIntensityAtScopeDensity;
                }

                real CGlobalRadiometricDistribution::GetLowerIntensityAtScopeDensity() const
                {
                    return m_LowerIntensityAtScopeDensity;
                }

                real CGlobalRadiometricDistribution::GetScopeKernelDensity() const
                {
                    return m_ScopeKernelDensity;
                }

                real CGlobalRadiometricDistribution::GetConsistentSafeMaximalKernelDensity() const
                {
                    return m_ConsistentSafeMaximalKernelDensity;
                }

                uint CGlobalRadiometricDistribution::GetTotalUnderExposedPixels() const
                {
                    return m_TotalUnderExposedPixels;
                }

                uint CGlobalRadiometricDistribution::GetTotalOverExposedPixels() const
                {
                    return m_TotalOverExposedPixels;
                }

                const list<CExposure>& CGlobalRadiometricDistribution::GetExposures() const
                {
                    return m_Exposures;
                }

                const std::vector<CCalibratedExposure>& CGlobalRadiometricDistribution::GetCalibratedExposures() const
                {
                    return m_CalibratedExposures;
                }

                const list<const CGlobalRadiometricBayerPatternSynthesizer*>& CGlobalRadiometricDistribution::GetSynthesizers() const
                {
                    return m_Synthesizers;
                }

                bool CGlobalRadiometricDistribution::ConfigureExposureScopeByDensity(const real ScopeKernelDensity)
                {
                    m_ScopeKernelDensity = TMin(TMax(ScopeKernelDensity, m_ConsistentSafeMaximalKernelDensity), _REAL_ONE_ - _REAL_MILI_);
                    const uint TotalExposures = m_CalibratedExposures.size();
                    const real MinimalLogExposureTime = RealLog(m_CalibratedExposures.front().GetScopeMinimalExposureTime());
                    const real MaximalLogExposureTime = RealLog(m_CalibratedExposures.back().GetScopeMaximalExposureTime());
                    list<const CGlobalRadiometricResponceFunction*>::const_iterator GlobalRadiometricResponceFunctionsBegin = m_GlobalRadiometricResponceFunctions.begin(), GlobalRadiometricResponceFunctionsEnd = m_GlobalRadiometricResponceFunctions.end();
                    m_UpperIntensityAtScopeDensity = TMin(RealCeil(m_RadianceWeigthingKernel.GetUpperIntensityAtDensity(m_Kernel, m_ScopeKernelDensity)) + _REAL_ONE_, m_MaximalIntensity);
                    m_LowerIntensityAtScopeDensity = TMax(RealFloor(m_RadianceWeigthingKernel.GetLowerIntensityAtDensity(m_Kernel, m_ScopeKernelDensity)) - _REAL_ONE_, m_MinimalIntensity);
                    memset(m_pExposureDistribution, 0, sizeof(ExposureBin) * TotalExposures);
                    ExposureBin* pExposureBin = m_pExposureDistribution;
                    for (uint i = 0; i < TotalExposures; ++i, ++pExposureBin)
                    {
                        pExposureBin->m_Active = true;
                        pExposureBin->m_Index = i;
                        pExposureBin->m_MinimalDensity = _REAL_MAX_;
                        const real Time = m_CalibratedExposures[i].GetTime();
                        real CommonT0 = _REAL_MIN_, CommonT1 = _REAL_MAX_;
                        for (list<const CGlobalRadiometricResponceFunction*>::const_iterator ppGlobalRadiometricResponceFunction = GlobalRadiometricResponceFunctionsBegin; ppGlobalRadiometricResponceFunction != GlobalRadiometricResponceFunctionsEnd; ++ppGlobalRadiometricResponceFunction)
                        {
                            const CGlobalRadiometricResponceFunction* pResponceFunction = *ppGlobalRadiometricResponceFunction;
                            const real T0 = pResponceFunction->GetIdealExposureTime(m_LowerIntensityAtScopeDensity, pResponceFunction->GetIdealRadiance(m_UpperIntensityAtScopeDensity, Time));
                            const real T1 = pResponceFunction->GetIdealExposureTime(m_UpperIntensityAtScopeDensity, pResponceFunction->GetIdealRadiance(m_LowerIntensityAtScopeDensity, Time));
                            if (T0 > CommonT0)
                            {
                                CommonT0 = T0;
                            }
                            if (T1 < CommonT1)
                            {
                                CommonT1 = T1;
                            }
                        }
                        if (CommonT0 >= CommonT1)
                        {
                            memset(m_pExposureDistribution, 0, sizeof(ExposureBin) * TotalExposures);
                            return false;
                        }
                        const ExposureDualIndex* pExposureDualIndex0 = m_pExposureDualIndices + RoundPositiveRealToInteger((TMin(TMax(RealLog(CommonT0), MinimalLogExposureTime), MaximalLogExposureTime) - MinimalLogExposureTime) * m_ScaleFactorLogExposureTime);
                        const ExposureDualIndex* pExposureDualIndex1 = m_pExposureDualIndices + RoundPositiveRealToInteger((TMax(TMin(RealLog(CommonT1), MaximalLogExposureTime), MinimalLogExposureTime) - MinimalLogExposureTime) * m_ScaleFactorLogExposureTime);
                        pExposureBin->m_LogTime = RealLog(Time);
                        pExposureBin->m_ScopeIndexA = (CommonT0 > pExposureDualIndex0->m_ExposureTimeThreshold) ? pExposureDualIndex0->m_IndexB : pExposureDualIndex0->m_IndexA;
                        pExposureBin->m_ScopeIndexB = (CommonT1 > pExposureDualIndex1->m_ExposureTimeThreshold) ? pExposureDualIndex1->m_IndexB : pExposureDualIndex1->m_IndexA;
                    }
                    return true;
                }

                bool CGlobalRadiometricDistribution::MapExposureFrequencyDistribution(const CGlobalMultipleChannelRadiometricResponceFunction* pGlobalMultipleChannelRadiometricResponceFunction, const TImage<real>* pLogRadianceImage, const TImage<real>* pDensityWeightedImage, const BayerPatternType BayerPattern, const real CriticallKernelDensity)
                {
                    if (pGlobalMultipleChannelRadiometricResponceFunction && pLogRadianceImage && pDensityWeightedImage && pGlobalMultipleChannelRadiometricResponceFunction->AreIdealResponceFunctionsReady() && pLogRadianceImage->IsValid() && pDensityWeightedImage->IsValid())
                    {
                        ChromaticSpaceRGB::ChannelContent BayerPatternMapYX[2][2];
                        if (LoadBayerPatternMap(BayerPattern, BayerPatternMapYX))
                        {
                            real OptimalResponces[3] = { _REAL_ZERO_ };
                            OptimalResponces[ChromaticSpaceRGB::eRed] = RealLog(pGlobalMultipleChannelRadiometricResponceFunction->GetRedChannelResponceFunction()->GetIdealResponce(_REAL_127_5_));
                            OptimalResponces[ChromaticSpaceRGB::eGreen] = RealLog(pGlobalMultipleChannelRadiometricResponceFunction->GetGreenChannelResponceFunction()->GetIdealResponce(_REAL_127_5_));
                            OptimalResponces[ChromaticSpaceRGB::eBlue] = RealLog(pGlobalMultipleChannelRadiometricResponceFunction->GetBlueChannelResponceFunction()->GetIdealResponce(_REAL_127_5_));
                            const real MinimalLogExposureTime = RealLog(m_CalibratedExposures.front().GetScopeMinimalExposureTime());
                            const real MaximalLogExposureTime = RealLog(m_CalibratedExposures.back().GetScopeMaximalExposureTime());
                            const uint Width = pLogRadianceImage->GetWidth();
                            const uint Height = pLogRadianceImage->GetHeight();
                            const real* pLogRadiancePixel = pLogRadianceImage->GetBeginReadOnlyBuffer();
                            const real* ppDensityWeightedPixel = pDensityWeightedImage->GetBeginReadOnlyBuffer();
                            for (uint Y = 0; Y < Height; ++Y)
                            {
                                const real SelectedOptimalResponce[2] = { OptimalResponces[BayerPatternMapYX[Y & 0X1][0]], OptimalResponces[BayerPatternMapYX[Y & 0X1][1]] };
                                for (uint X = 0; X < Width; ++X)
                                {
                                    const real DensityWeight = *ppDensityWeightedPixel++;
                                    const real LogRadiance = *pLogRadiancePixel++;
                                    if (DensityWeight < CriticallKernelDensity)
                                    {
                                        real OptimalLogExposureTime = SelectedOptimalResponce[X & 0X1] - LogRadiance;
                                        if (OptimalLogExposureTime > MaximalLogExposureTime)
                                        {
                                            OptimalLogExposureTime = MaximalLogExposureTime;
                                            ++m_TotalUnderExposedPixels;
                                        }
                                        else if (OptimalLogExposureTime < MinimalLogExposureTime)
                                        {
                                            OptimalLogExposureTime = MinimalLogExposureTime;
                                            ++m_TotalOverExposedPixels;
                                        }
                                        const ExposureDualIndex* pOptimalExposureIndex = m_pExposureDualIndices + RoundPositiveRealToInteger((OptimalLogExposureTime - MinimalLogExposureTime) * m_ScaleFactorLogExposureTime);
                                        ExposureBin* pOptimalExposureBin = m_pExposureDistribution + (OptimalLogExposureTime > pOptimalExposureIndex->m_ExposureTimeThreshold ? pOptimalExposureIndex->m_IndexB : pOptimalExposureIndex->m_IndexA);
                                        pOptimalExposureBin->m_SamplingDensityAccumulator += _REAL_ONE_;
                                        if (DensityWeight < pOptimalExposureBin->m_MinimalDensity)
                                        {
                                            pOptimalExposureBin->m_MinimalDensity = DensityWeight;
                                        }
                                    }
                                }
                            }
                            return true;
                        }
                    }
                    return false;
                }
            }
        }
    }
}
