/*
 * GlobalRadiometricResponceFunctionLookUpTable.cpp
 *
 *  Created on: Apr 19, 2012
 *      Author: david
 */

#include "GlobalRadiometricResponceFunctionLookUpTable.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                CGlobalRadiometricResponceFunctionLookUpTable::CGlobalRadiometricResponceFunctionLookUpTable() :
                    m_pResponceFunction(nullptr), m_pLookUpTable(nullptr), m_pWeigthingKernel(nullptr), m_pParametricIndices(nullptr), m_SamplesPerUnit(0)
                {
                }

                CGlobalRadiometricResponceFunctionLookUpTable::~CGlobalRadiometricResponceFunctionLookUpTable()
                {
                    RELEASE_OBJECT(m_pLookUpTable)
                    RELEASE_ARRAY(m_pWeigthingKernel)
                    RELEASE_ARRAY(m_pParametricIndices)
                }

                bool CGlobalRadiometricResponceFunctionLookUpTable::LoadLookUpTable(const CGlobalRadiometricResponceFunction* pResponceFunction, const list<CExposure>& Exposures, const uint SamplesPerUnit, const CRadianceWeigthingKernel::KernelType Kernel)
                {
                    if (pResponceFunction && pResponceFunction->IsIdealResponceFunctionReady() && Exposures.size() && (SamplesPerUnit >= 1) && (Kernel != CRadianceWeigthingKernel::eUnknownKernelType))
                    {
                        const uint TotalExposures = LoadExposures(Exposures);
                        if (TotalExposures == Exposures.size())
                        {
                            m_pResponceFunction = pResponceFunction;
                            m_SamplesPerUnit = SamplesPerUnit;
                            const uint TotalSamples = m_SamplesPerUnit * 255 + 1;
                            RELEASE_OBJECT(m_pLookUpTable)
                            m_pLookUpTable = new TImage<real>(TotalSamples, TotalExposures);
                            m_pLookUpTable->Clear();
                            RELEASE_ARRAY(m_pWeigthingKernel)
                            m_pWeigthingKernel = new real[TotalSamples];
                            memset(m_pWeigthingKernel, 0, sizeof(real) * TotalSamples);
                            RELEASE_ARRAY(m_pParametricIndices)
                            m_pParametricIndices = new uint[TotalExposures];
                            memset(m_pParametricIndices, 0, sizeof(uint) * TotalSamples);
                            const CRadianceWeigthingKernel& RadianceWeigthingKernel = m_pResponceFunction->GetRadianceWeigthingKernel();
                            const LongDoubleReal ContinousIntensityDelta = LongDoubleReal(1.0) / LongDoubleReal(m_SamplesPerUnit);
                            LongDoubleReal ContinousIntensity = LongDoubleReal(0.0);
                            for (uint i = 0; i < TotalSamples; ++i, ContinousIntensity += ContinousIntensityDelta)
                                if (ContinousIntensity <= _REAL_255_)
                                {
                                    m_pWeigthingKernel[i] = RadianceWeigthingKernel.GetRegressionWeigthingByKernel(ContinousIntensity, Kernel);
                                }
                            real* pResponceFunctionSample = m_pLookUpTable->GetBeginWritableBuffer();
                            const LongDoubleReal Slope = LongDoubleReal(m_pResponceFunction->GetIntensitySlope());
                            const LongDoubleReal Offset = LongDoubleReal(m_pResponceFunction->GetIntensityOffset());
                            const LongDoubleReal MinimalIntensity = LongDoubleReal(m_pResponceFunction->GetMinimalIntensity());
                            uint ExposureIndex = 0;
                            list<CExposure>::const_iterator ExposuresEnd = m_Exposures.end();
                            for (list<CExposure>::const_iterator pExposure = m_Exposures.begin(); pExposure != ExposuresEnd; ++pExposure, ++ExposureIndex)
                            {
                                m_pParametricIndices[ExposureIndex] = pExposure->GetParametricIndex();
                                const LongDoubleReal LogExposureTime = logl(LongDoubleReal(pExposure->GetTime()));
                                LongDoubleReal ContinousIntensity = LongDoubleReal(0.0);
                                for (uint i = 0; i < TotalSamples; ++i, ContinousIntensity += ContinousIntensityDelta, ++pResponceFunctionSample)
                                    if (ContinousIntensity > MinimalIntensity)
                                    {
                                        *pResponceFunctionSample = real(LongDoubleReal(m_pWeigthingKernel[i]) * (logl(ContinousIntensity * Slope + Offset) - LogExposureTime));
                                    }
                            }
                            return true;
                        }
                    }
                    return false;
                }

                bool CGlobalRadiometricResponceFunctionLookUpTable::IsEnabled() const
                {
                    return m_pLookUpTable;
                }

                const CGlobalRadiometricResponceFunction* CGlobalRadiometricResponceFunctionLookUpTable::GetResponceFunction() const
                {
                    return m_pResponceFunction;
                }

                const TImage<real>* CGlobalRadiometricResponceFunctionLookUpTable::GetLookUpTable() const
                {
                    return m_pLookUpTable;
                }

                const real* CGlobalRadiometricResponceFunctionLookUpTable::GetLookUpTableByExposure(const CExposure& Exposure) const
                {
                    if (m_pLookUpTable && Exposure.HasParametricIndex())
                    {
                        const uint ParametricIndex = Exposure.GetParametricIndex();
                        const uint TotalExposures = m_pLookUpTable->GetHeight();
                        for (uint i = 0; i < TotalExposures; ++i)
                            if (ParametricIndex == m_pParametricIndices[i])
                            {
                                return m_pLookUpTable->GetReadOnlyBufferLineAt(i);
                            }
                    }
                    return nullptr;
                }

                const real* CGlobalRadiometricResponceFunctionLookUpTable::GetWeigthingKernel() const
                {
                    return m_pWeigthingKernel;
                }

                uint CGlobalRadiometricResponceFunctionLookUpTable::GetSamplesPerUnit() const
                {
                    return m_SamplesPerUnit;
                }

                const list<CExposure>& CGlobalRadiometricResponceFunctionLookUpTable::GetExposures() const
                {
                    return m_Exposures;
                }

                uint CGlobalRadiometricResponceFunctionLookUpTable::LoadExposures(const list<CExposure>& Exposures)
                {
                    m_Exposures.clear();
                    if (Exposures.size())
                    {
                        m_Exposures.insert(m_Exposures.end(), Exposures.begin(), Exposures.end());
                        m_Exposures.sort(CExposure::SortExposuresByParametricIndex);
                        m_Exposures.unique(CExposure::EqualsExposuresByParametricIndex);
                    }
                    return m_Exposures.size();
                }
            }
        }
    }
}
