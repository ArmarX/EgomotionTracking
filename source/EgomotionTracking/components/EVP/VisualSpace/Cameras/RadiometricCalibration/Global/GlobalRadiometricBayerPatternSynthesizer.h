/*
 * GlobalRadiometricBayerPatternSynthesizer.h
 *
 *  Created on: 12.05.2012
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/DataTypes/BayerPattern.h"
#include "../../../../Foundation/DataTypes/ColorSpaces.h"
#include "../../../Common/Image.h"
#include "../Common/Exposure.h"
#include "../Common/CalibratedExposure.h"
#include "../Common/RadianceWeigthingKernel.h"
#include "GlobalMultipleChannelRadiometricResponceFunction.h"
#include "GlobalRadiometricResponceFunctionLookUpTable.h"

#define _GLOBAL_RADIOMETRIC_SYNTHESIZER_MINIMAL_INTEGRATED_EXPOSURES_   4

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CGlobalRadiometricBayerPatternSynthesizer
                {
                public:

                    enum OptimalExposureExtractionMode
                    {
                        eOptimalExposureExtractionDisabled, eWeightingGlobalDensity, eMaximalDensity
                    };

                    CGlobalRadiometricBayerPatternSynthesizer(const TImage<real>* pContinousSourceImage, const CGlobalMultipleChannelRadiometricResponceFunction* pGlobalMultipleChannelRadiometricResponceFunction, const list<CExposure>& Exposures, const uint SamplesPerUnit = 0);
                    CGlobalRadiometricBayerPatternSynthesizer(const TImage<byte>* pDiscreteSourceImage, const CGlobalMultipleChannelRadiometricResponceFunction* pGlobalMultipleChannelRadiometricResponceFunction, const list<CExposure>& Exposures);
                    virtual ~CGlobalRadiometricBayerPatternSynthesizer();

                    bool StartExposureBracketing();
                    bool AddExposure(const uint ParametricIndex);
                    bool Synthesize();
                    bool GetSynthesizeDensityStatistics(real& DensityAccumulator, real& MaximalDensity, real& MinimalDensity, real& MeanDensity, real& StandarDeviationDensity, uint& TotalCriticalPixels, const real CriticalDensity = _REAL_EPSILON_) const;
                    bool GetSynthesizeRadianceStatistics(real& MaximalRadiance, real& MinimalRadiance, real& MeanRadiance, real& StandarDeviationRadiance, uint& TotalEncodingBits) const;
                    bool ExtractOptimalExposureImage();
                    const list<CExposure>& GetExposures() const;

                    bool SetOptimalExposureExtractionMode(const OptimalExposureExtractionMode Mode);

                    const TImage<real>* GetLogRadianceImage() const;
                    const TImage<real>* GetDensityWeightedImage() const;
                    const TImage<real>* GetOptimalExposureImage() const;
                    DeviceIdentifier GetCameraId() const;
                    BayerPatternType GetBayerPattern() const;

                    const uint* GetUnifiedExposureHistogram() const;
                    const uint* GetRedChannelExposureHistogram() const;
                    const uint* GetGreenChannelExposureHistogram() const;
                    const uint* GetBlueChannelExposureHistogram() const;

                    bool IsEnabled() const;
                    const CGlobalMultipleChannelRadiometricResponceFunction* GetGlobalMultipleChannelRadiometricResponceFunction() const;
                    uint GetTotalIntegratedExposures() const;
                    uint GetLastExposureTotalIntegratedPixels() const;
                    real GetLastExposureTotalIntegratedKernelDensity() const;
                    uint GetLastSynthesizeTotalUnsampledPixels() const;

                protected:

                    static  list<CExposure> EnsureExposuresOrder(const list<CExposure>& Exposures);
                    void LoadIntensityLimits(const real IntensityMargin);
                    void LoadLogTimesTable();
                    void LoadLookUpTables(const uint SamplesPerUnit);
                    void ClearHistograms();
                    void ClearImages();
                    bool AddDiscreteExposure(const uint ParametricIndex);
                    bool AddSemiContinousExposure(const uint ParametricIndex);
                    bool AddContinousExposure(const uint ParametricIndex);

                    bool m_Enabled;
                    const list<CExposure> m_Exposures;
                    const TImage<byte>* m_pDiscreteSourceImage;
                    const TImage<real>* m_pContinousSourceImage;
                    const CGlobalMultipleChannelRadiometricResponceFunction* m_pGlobalMultipleChannelRadiometricResponceFunction;
                    TImage<real>* m_pDensityWeightedLogRadianceImage;
                    TImage<real>* m_pDensityWeightedImage;
                    TImage<real>* m_pLogRadianceImage;
                    TImage<real>* m_pDensityWeightedExposureImage;
                    TImage<real>* m_pOptimalExposureImage;
                    CGlobalRadiometricResponceFunctionLookUpTable* m_pRedChannelLookUpTable;
                    CGlobalRadiometricResponceFunctionLookUpTable* m_pGreenChannelLookUpTable;
                    CGlobalRadiometricResponceFunctionLookUpTable* m_pBlueChannelLookUpTable;
                    real* m_pLogTimes;
                    uint* m_pUnifiedExposureHistogram;
                    uint* m_pRedChannelExposureHistogram;
                    uint* m_pGreenChannelExposureHistogram;
                    uint* m_pBlueChannelExposureHistogram;
                    uint m_TotalIntegratedExposures;
                    uint m_MinimalExposureParametricIndex;
                    uint m_MaximalExposureParametricIndex;
                    uint m_LastExposureTotalIntegratedPixels;
                    uint m_LastSynthesizeTotalUnsampledPixels;
                    real m_MinimalIntensity;
                    real m_MaximalIntensity;
                    real m_LastExposureTotalIntegratedKernelDensity;
                    real m_OptimalExposureWeighting;
                    OptimalExposureExtractionMode m_OptimalExposureMode;
                    CRadianceWeigthingKernel::KernelType m_Kernel;
                    ChromaticSpaceRGB::ChannelContent m_BayerPatternMapYX[2][2];
                };
            }
        }
    }
}
