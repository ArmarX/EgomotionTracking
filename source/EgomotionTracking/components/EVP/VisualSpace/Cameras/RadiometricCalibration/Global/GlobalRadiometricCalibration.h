/*
 * GlobalRadiometricCalibration.h
 *
 *  Created on: Apr 17, 2012
 *      Author: david
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/DataTypes/BayerPattern.h"
#include "../../../../Foundation/DataTypes/ColorSpaces.h"
#include "../../../Common/Image.h"
#include "../Common/Exposure.h"
#include "../Common/RadianceWeigthingKernel.h"
#include "GlobalRadiometricPixel.h"

#define _GLOBAL_RADIOMETRIC_CALIBRATION_MINIMAL_EXPOSURES_ 4u
#define _GLOBAL_RADIOMETRIC_CALIBRATION_MINIMAL_SELECTED_SAMPLING_CELLS_ 32u
#define _GLOBAL_RADIOMETRIC_CALIBRATION_MINIMAL_GAUSSIAN_BOX_RADIUS_ real(8.0)
#define _GLOBAL_RADIOMETRIC_CALIBRATION_MAXIMAL_GAUSSIAN_BOX_RADIUS_ real(32.0)
#define _GLOBAL_RADIOMETRIC_CALIBRATION_MINIMAL_DEFAULT_GAUSSIAN_BOX_RADIUS_ real(16.0)
#define _GLOBAL_RADIOMETRIC_CALIBRATION_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_DENSITY_ real(0.01)
#define _GLOBAL_RADIOMETRIC_CALIBRATION_MINIMAL_DEFAULT_GAUSSIAN_BOX_CUTOFF_NORMALIZED_DEVIATION_ real(0.95)

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CGlobalRadiometricResponceFunction;

                class CGlobalRadiometricCalibration
                {
                public:

                    enum PixelSelectionCriterion
                    {
                        eUnknownPixelSelectionCriterion, eMaximalStandardDeviation, eMinimalStandardDeviation, eMaximalRange, eMinimalRange
                    };

                    static  std::string PixelSelectionCriterionToString(const PixelSelectionCriterion Criterion);

                    CGlobalRadiometricCalibration(const DeviceIdentifier CameraId, const TImage<real>* pSourceImage, const BayerPatternType BayerPattern, const ChromaticSpaceRGB::ChannelContent ChannelContent);
                    CGlobalRadiometricCalibration(const DeviceIdentifier CameraId, const TImage<real>* pSourceImage);
                    virtual ~CGlobalRadiometricCalibration();

                    bool IsEnabled() const;
                    DeviceIdentifier GetCameraId() const;
                    BayerPatternType GetBayerPattern() const;
                    ChromaticSpaceRGB::ChannelContent GetChannelContent() const;

                    bool AddExposure(const real ExposureTime);
                    bool ClearExposures();
                    uint GetTotalExposures() const;

                    bool SetGaussianBoxKernelRadius(const real GaussianBoxKernelRadius);
                    bool SetCommonGaussianKernelStandardDeviation(const real StandardDeviation);
                    bool SetCommonGaussianKernelCutOff(const real CutOffDensity, const real CutOffNormalizedDeviation);

                    real GetGaussianBoxKernelRadius() const;
                    real GetCommonGaussianKernelStandardDeviation() const;

                    bool Calibrate(const PixelSelectionCriterion Criterion, const real DominaceRadius, const list<CGlobalRadiometricPixel*>& SelectedPixels, const real SmoothingLambda, const CRadianceWeigthingKernel::KernelType Kernel);
                    bool Calibrate(const PixelSelectionCriterion Criterion, const real DominaceRadius, const uint MaximalSamples, const real SmoothingLambda, const CRadianceWeigthingKernel::KernelType Kernel);

                    const CGlobalRadiometricResponceFunction* GetRadiometricResponceFunction() const;

                protected:

                    list<CGlobalRadiometricPixel*> SelectPixelsByCriterion(const PixelSelectionCriterion Criterion, const real DominaceRadius, const uint MaximalSamples);

                    static  list<CPixelLocation> ComputeLocationDeltas(const real DominaceRadius);
                    static  bool SortRadiometricPixelsByCriterionAscent(const CGlobalRadiometricPixel* plhs, const CGlobalRadiometricPixel* rlhs);
                    static  bool SortRadiometricPixelsByCriterionDescent(const CGlobalRadiometricPixel* plhs, const CGlobalRadiometricPixel* rlhs);

                    const DeviceIdentifier m_CameraId;
                    const BayerPatternType m_BayerPattern;
                    const ChromaticSpaceRGB::ChannelContent m_ChannelContent;
                    const TImage<real>* m_pSourceImage;
                    TImage<CGlobalRadiometricPixel>* m_pRadiometricImage;
                    list<CGlobalRadiometricPixel*> m_RadiometricPixels;
                    list<CExposure> m_Exposures;
                    CRadianceWeigthingKernel m_RadianceWeigthingKernel;
                    CGlobalRadiometricResponceFunction* m_pRadiometricResponceFunction;
                };
            }
        }
    }
}

