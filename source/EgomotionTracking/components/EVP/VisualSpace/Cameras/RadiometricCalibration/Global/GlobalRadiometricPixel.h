/*
 * GlobalRadiometricPixel.h
 *
 *  Created on: 21.09.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../../Foundation/Mathematics/1D/Common1D.h"
#include "../../../../Foundation/Mathematics/3D/Vector3D.h"
#include "../../../../Foundation/Mathematics/ND/LinearAlgebra.h"
#include "../../../../Foundation/DataTypes/PixelLocation.h"
#include "../Common/Exposure.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CGlobalRadiometricPixel
                {
                public:

                    CGlobalRadiometricPixel();
                    ~CGlobalRadiometricPixel();

                    CGlobalRadiometricPixel* Initialize(const coordinate X, const coordinate Y, const real* pValueSource);

                    void CalculateStandardDeviationCriterion();
                    void CalculateRangeCriterion();

                    bool IsInitialized() const;
                    bool IsActive() const;
                    void SetActive(const bool Active);

                    void AddExposure();
                    void ClearExposures();

                    real GetCriterion() const;
                    const CPixelLocation& GetLocation() const;
                    const list<real>& GetValues() const;

                protected:

                    struct RadiometricPixel
                    {
                        bool m_IsActive;
                        list<real> m_Values;
                        const real* m_pIntensityValueSource;
                        real m_Criterion;
                        CPixelLocation m_Location;
                    };
                    RadiometricPixel* m_pRadiometricPixel;
                };
            }
        }
    }
}

