/*
 * GlobalRadiometricResponceFunctionLookUpTable.h
 *
 *  Created on: Apr 19, 2012
 *      Author: david
 */

#pragma once

#include "../../../../GlobalSettings.h"
#include "../../../Common/Image.h"
#include "../Common/Exposure.h"
#include "../Common/RadianceWeigthingKernel.h"
#include "GlobalRadiometricResponceFunction.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CGlobalRadiometricResponceFunctionLookUpTable
                {
                public:

                    CGlobalRadiometricResponceFunctionLookUpTable();
                    virtual ~CGlobalRadiometricResponceFunctionLookUpTable();

                    bool LoadLookUpTable(const CGlobalRadiometricResponceFunction* pResponceFunction, const list<CExposure>& Exposures, const uint SamplesPerUnit, const CRadianceWeigthingKernel::KernelType Kernel);

                    bool IsEnabled() const;
                    const CGlobalRadiometricResponceFunction* GetResponceFunction() const;
                    const TImage<real>* GetLookUpTable() const;
                    const real* GetLookUpTableByExposure(const CExposure& Exposure) const;
                    const real* GetWeigthingKernel() const;
                    uint GetSamplesPerUnit() const;
                    const list<CExposure>& GetExposures() const;

                protected:

                    uint LoadExposures(const list<CExposure>& Exposures);

                    const CGlobalRadiometricResponceFunction* m_pResponceFunction;
                    TImage<real>* m_pLookUpTable;
                    real* m_pWeigthingKernel;
                    uint* m_pParametricIndices;
                    uint m_SamplesPerUnit;
                    list<CExposure> m_Exposures;
                };
            }
        }
    }
}

