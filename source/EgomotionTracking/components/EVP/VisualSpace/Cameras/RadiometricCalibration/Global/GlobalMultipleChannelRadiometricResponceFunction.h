/*
 * GlobalMultipleChannelRadiometricResponceFunction.h
 *
 *  Created on: Apr 18, 2012
 *      Author: david
 */

#pragma once

#include "GlobalRadiometricResponceFunction.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                class CGlobalMultipleChannelRadiometricResponceFunction
                {
                public:

                    static CGlobalMultipleChannelRadiometricResponceFunction* CreateFromFile(const_string pPathFileName);

                    CGlobalMultipleChannelRadiometricResponceFunction();
                    virtual ~CGlobalMultipleChannelRadiometricResponceFunction();

                    bool AreIdealResponceFunctionsReady() const;
                    bool LoadResponceFunctions(const CGlobalRadiometricResponceFunction* pRedRadiometricResponceFunction, const CGlobalRadiometricResponceFunction* pGreenRadiometricResponceFunction, const CGlobalRadiometricResponceFunction* pBlueRadiometricResponceFunction, const bool OwnerShip);
                    bool LoadResponceFunctionFromBinaryFile(const_string pPathFileName);
                    bool SaveResponceFunctionToFile(const_string pPathFileName, const Files::CFile::FileMode Mode) const;

                    DeviceIdentifier GetCameraId() const;
                    BayerPatternType GetBayerPattern() const;
                    const CGlobalRadiometricResponceFunction* GetRedChannelResponceFunction() const;
                    const CGlobalRadiometricResponceFunction* GetGreenChannelResponceFunction() const;
                    const CGlobalRadiometricResponceFunction* GetBlueChannelResponceFunction() const;

                protected:

                    bool SaveResponceFunctionToTextFile(const_string pPathFileName) const;
                    bool SaveResponceFunctionToBinaryFile(const_string pPathFileName) const;

                    bool m_OwnerShip;
                    const CGlobalRadiometricResponceFunction* m_pRedChannelResponceFunction;
                    const CGlobalRadiometricResponceFunction* m_pGreenChannelResponceFunction;
                    const CGlobalRadiometricResponceFunction* m_pBlueChannelResponceFunction;
                };
            }
        }
    }
}

