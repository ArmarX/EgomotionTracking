/*
 * GlobalMultipleChannelRadiometricCalibration.cpp
 *
 *  Created on: Apr 18, 2012
 *      Author: david
 */

#include "GlobalMultipleChannelRadiometricCalibration.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                CGlobalMultipleChannelRadiometricCalibration::CGlobalMultipleChannelRadiometricCalibration(const DeviceIdentifier CameraId, const TImage<real>* pSourceImage, const BayerPatternType BayerPattern) :
                    m_pRedChannelCalibration(nullptr), m_pGreenChannelCalibration(nullptr), m_pBlueChannelCalibration(nullptr), m_pMultipleChannelRadiometricResponceFunction(nullptr)
                {
                    m_pRedChannelCalibration = new CGlobalRadiometricCalibration(CameraId, pSourceImage, BayerPattern, ChromaticSpaceRGB::eRed);
                    m_pGreenChannelCalibration = new CGlobalRadiometricCalibration(CameraId, pSourceImage, BayerPattern, ChromaticSpaceRGB::eGreen);
                    m_pBlueChannelCalibration = new CGlobalRadiometricCalibration(CameraId, pSourceImage, BayerPattern, ChromaticSpaceRGB::eBlue);
                    m_pMultipleChannelRadiometricResponceFunction = new CGlobalMultipleChannelRadiometricResponceFunction();
                    m_pMultipleChannelRadiometricResponceFunction->LoadResponceFunctions(m_pRedChannelCalibration->GetRadiometricResponceFunction(), m_pGreenChannelCalibration->GetRadiometricResponceFunction(), m_pBlueChannelCalibration->GetRadiometricResponceFunction(), false);
                }

                CGlobalMultipleChannelRadiometricCalibration::~CGlobalMultipleChannelRadiometricCalibration()
                {
                    RELEASE_OBJECT(m_pRedChannelCalibration)
                    RELEASE_OBJECT(m_pGreenChannelCalibration)
                    RELEASE_OBJECT(m_pBlueChannelCalibration)
                    RELEASE_OBJECT(m_pMultipleChannelRadiometricResponceFunction)
                }

                bool CGlobalMultipleChannelRadiometricCalibration::IsEnabled() const
                {
                    return m_pRedChannelCalibration->IsEnabled() && m_pGreenChannelCalibration->IsEnabled() && m_pBlueChannelCalibration->IsEnabled();
                }

                DeviceIdentifier CGlobalMultipleChannelRadiometricCalibration::GetCameraId() const
                {
                    return m_pRedChannelCalibration->GetCameraId();
                }

                BayerPatternType CGlobalMultipleChannelRadiometricCalibration::GetBayerPattern() const
                {
                    return m_pRedChannelCalibration->GetBayerPattern();
                }

                bool CGlobalMultipleChannelRadiometricCalibration::AddExposure(const real ExposureTime)
                {
                    const bool RedChannel = m_pRedChannelCalibration->AddExposure(ExposureTime);
                    const bool GreenChannel = m_pGreenChannelCalibration->AddExposure(ExposureTime);
                    const bool BlueChannel = m_pBlueChannelCalibration->AddExposure(ExposureTime);
                    return (RedChannel && GreenChannel && BlueChannel);
                }

                bool CGlobalMultipleChannelRadiometricCalibration::ClearExposures()
                {
                    const bool RedChannel = m_pRedChannelCalibration->ClearExposures();
                    const bool GreenChannel = m_pGreenChannelCalibration->ClearExposures();
                    const bool BlueChannel = m_pBlueChannelCalibration->ClearExposures();
                    return (RedChannel && GreenChannel && BlueChannel);
                }

                uint CGlobalMultipleChannelRadiometricCalibration::GetTotalExposures() const
                {
                    return m_pRedChannelCalibration->GetTotalExposures();
                }

                bool CGlobalMultipleChannelRadiometricCalibration::SetGaussianBoxKernelRadius(const real GaussianBoxKernelRadius)
                {
                    const bool RedChannel = m_pRedChannelCalibration->SetGaussianBoxKernelRadius(GaussianBoxKernelRadius);
                    const bool GreenChannel = m_pGreenChannelCalibration->SetGaussianBoxKernelRadius(GaussianBoxKernelRadius);
                    const bool BlueChannel = m_pBlueChannelCalibration->SetGaussianBoxKernelRadius(GaussianBoxKernelRadius);
                    return RedChannel && GreenChannel && BlueChannel;
                }

                bool CGlobalMultipleChannelRadiometricCalibration::SetCommonGaussianKernelStandardDeviation(const real StandardDeviation)
                {
                    const bool RedChannel = m_pRedChannelCalibration->SetCommonGaussianKernelStandardDeviation(StandardDeviation);
                    const bool GreenChannel = m_pGreenChannelCalibration->SetCommonGaussianKernelStandardDeviation(StandardDeviation);
                    const bool BlueChannel = m_pBlueChannelCalibration->SetCommonGaussianKernelStandardDeviation(StandardDeviation);
                    return RedChannel && GreenChannel && BlueChannel;
                }

                bool CGlobalMultipleChannelRadiometricCalibration::SetCommonGaussianKernelCutOff(const real CutOffDensity, const real CutOffNormalizedDeviation)
                {
                    const bool RedChannel = m_pRedChannelCalibration->SetCommonGaussianKernelCutOff(CutOffDensity, CutOffNormalizedDeviation);
                    const bool GreenChannel = m_pGreenChannelCalibration->SetCommonGaussianKernelCutOff(CutOffDensity, CutOffNormalizedDeviation);
                    const bool BlueChannel = m_pBlueChannelCalibration->SetCommonGaussianKernelCutOff(CutOffDensity, CutOffNormalizedDeviation);
                    return RedChannel && GreenChannel && BlueChannel;
                }

                real CGlobalMultipleChannelRadiometricCalibration::GetGaussianBoxKernelRadius() const
                {
                    return m_pRedChannelCalibration->GetGaussianBoxKernelRadius();
                }

                real CGlobalMultipleChannelRadiometricCalibration::GetCommonGaussianKernelStandardDeviation() const
                {
                    return m_pRedChannelCalibration->GetCommonGaussianKernelStandardDeviation();
                }

                bool CGlobalMultipleChannelRadiometricCalibration::Calibrate(const CGlobalRadiometricCalibration::PixelSelectionCriterion Criterion, const real DominaceRadius, const uint MaximalSamples, const real SmoothingLambda, const CRadianceWeigthingKernel::KernelType Kernel)
                {
                    const bool RedChannel = m_pRedChannelCalibration->Calibrate(Criterion, DominaceRadius, MaximalSamples, SmoothingLambda, Kernel);
                    const bool GreenChannel = m_pGreenChannelCalibration->Calibrate(Criterion, DominaceRadius, MaximalSamples, SmoothingLambda, Kernel);
                    const bool BlueChannel = m_pBlueChannelCalibration->Calibrate(Criterion, DominaceRadius, MaximalSamples, SmoothingLambda, Kernel);
                    return RedChannel && GreenChannel && BlueChannel;
                }

                bool CGlobalMultipleChannelRadiometricCalibration::SaveResponceFunctionToFile(const_string pPathFileName, const Files::CFile::FileMode Mode) const
                {
                    return m_pMultipleChannelRadiometricResponceFunction->SaveResponceFunctionToFile(pPathFileName, Mode);
                }

                const CGlobalRadiometricCalibration* CGlobalMultipleChannelRadiometricCalibration::GetRedChannelCalibration() const
                {
                    return m_pRedChannelCalibration;
                }

                const CGlobalRadiometricCalibration* CGlobalMultipleChannelRadiometricCalibration::GetGreenChannelCalibration() const
                {
                    return m_pGreenChannelCalibration;
                }

                const CGlobalRadiometricCalibration* CGlobalMultipleChannelRadiometricCalibration::GetBlueChannelCalibration() const
                {
                    return m_pBlueChannelCalibration;
                }

                const CGlobalMultipleChannelRadiometricResponceFunction* CGlobalMultipleChannelRadiometricCalibration::GetMultipleChannelRadiometricResponceFunction() const
                {
                    return m_pMultipleChannelRadiometricResponceFunction;
                }
            }
        }
    }
}
