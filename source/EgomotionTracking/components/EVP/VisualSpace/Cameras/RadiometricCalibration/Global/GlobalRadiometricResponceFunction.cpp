/*
 * GlobalRadiometricResponceFunction.cpp
 *
 *  Created on: Apr 17, 2012
 *      Author: david
 */

#include "GlobalRadiometricResponceFunction.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace RadiometricCalibration
            {
                const CGlobalRadiometricResponceFunction* CGlobalRadiometricResponceFunction::CreateFromFile(const_string pPathFileName)
                {
                    if (Files::CFile::Exists(pPathFileName))
                    {
                        CGlobalRadiometricResponceFunction* pRadiometricResponceFunction = new CGlobalRadiometricResponceFunction();
                        if (pRadiometricResponceFunction->LoadResponceFunctionFromBinaryFile(pPathFileName))
                        {
                            return pRadiometricResponceFunction;
                        }
                        RELEASE_OBJECT(pRadiometricResponceFunction)
                    }
                    return nullptr;
                }

                CGlobalRadiometricResponceFunction::CGlobalRadiometricResponceFunction() :
                    m_CameraId(0), m_BayerPattern(eUnknownBayerPattern), m_ChannelContent(ChromaticSpaceRGB::eUnknownChannelContent), m_TotalExpositions(0), m_TotalSelectedPixels(0), m_DominanceRadius(_REAL_ZERO_), m_SmoothingLambda(_REAL_ZERO_), m_PixelSelectionCriterion(CGlobalRadiometricCalibration::eUnknownPixelSelectionCriterion), m_pRegressionResponseFunction(nullptr), m_pRegressionWeightingFunction(nullptr), m_pModelEstimationWeightingFunction(nullptr), m_IntensitySlope(_REAL_ZERO_), m_IntensityOffset(_REAL_ZERO_), m_MinimalIntensity(_REAL_ZERO_)
                {
                }

                CGlobalRadiometricResponceFunction::CGlobalRadiometricResponceFunction(const DeviceIdentifier CameraId, const BayerPatternType BayerPattern, const ChromaticSpaceRGB::ChannelContent ChannelContent) :
                    m_CameraId(CameraId), m_BayerPattern(BayerPattern), m_ChannelContent(ChannelContent), m_TotalExpositions(0), m_TotalSelectedPixels(0), m_DominanceRadius(_REAL_ZERO_), m_SmoothingLambda(_REAL_ZERO_), m_PixelSelectionCriterion(CGlobalRadiometricCalibration::eUnknownPixelSelectionCriterion), m_pRegressionResponseFunction(nullptr), m_pRegressionWeightingFunction(nullptr), m_pModelEstimationWeightingFunction(nullptr), m_IntensitySlope(_REAL_ZERO_), m_IntensityOffset(_REAL_ZERO_), m_MinimalIntensity(_REAL_ZERO_)
                {
                }

                CGlobalRadiometricResponceFunction::~CGlobalRadiometricResponceFunction()
                {
                    Destroy();
                }

                bool CGlobalRadiometricResponceFunction::LoadResponceFunctionFromParameters(const DeviceIdentifier CameraId, const BayerPatternType BayerPattern, const ChromaticSpaceRGB::ChannelContent ChannelContent, const uint TotalExpositions, const uint TotalSelectedPixels, const real DominanceRadius, const real SmoothingLambda, const CGlobalRadiometricCalibration::PixelSelectionCriterion PixelSelectionCriterion, const real* pRegressionResponseFunction, const real* pRegressionWeightingFunction, const CRadianceWeigthingKernel& RadianceWeigthingKernel)
                {
                    if (CameraId && (((BayerPattern != eUnknownBayerPattern) && (ChannelContent != ChromaticSpaceRGB::eUnknownChannelContent)) || ((BayerPattern == eUnknownBayerPattern) && (ChannelContent == ChromaticSpaceRGB::eUnknownChannelContent))))
                    {
                        Create();
                        m_CameraId = CameraId;
                        m_BayerPattern = BayerPattern;
                        m_ChannelContent = ChannelContent;
                        m_TotalExpositions = TotalExpositions;
                        m_TotalSelectedPixels = TotalSelectedPixels;
                        m_DominanceRadius = DominanceRadius;
                        m_SmoothingLambda = SmoothingLambda;
                        m_PixelSelectionCriterion = PixelSelectionCriterion;
                        memcpy(m_pRegressionResponseFunction, pRegressionResponseFunction, sizeof(real) * 256);
                        memcpy(m_pRegressionWeightingFunction, pRegressionWeightingFunction, sizeof(real) * 256);
                        m_IntensitySlope = _REAL_ZERO_;
                        m_IntensityOffset = _REAL_ZERO_;
                        m_MinimalIntensity = _REAL_ZERO_;
                        m_RadianceWeigthingKernel = RadianceWeigthingKernel;
                        return true;
                    }
                    return false;
                }

                bool CGlobalRadiometricResponceFunction::LoadResponceFunctionFromBinaryFile(const_string pPathFileName)
                {
                    if (Files::CFile::Exists(pPathFileName))
                    {
                        Files::CInFile InputBinaryFile(pPathFileName, Files::CFile::eBinary);
                        if (LoadResponceFunctionFromBinaryFile(InputBinaryFile))
                        {
                            return InputBinaryFile.Close();
                        }
                    }
                    return false;
                }

                bool CGlobalRadiometricResponceFunction::SaveResponceFunctionToFile(const_string pPathFileName, const Files::CFile::FileMode Mode) const
                {
                    switch (Mode)
                    {
                        case Files::CFile::eText:
                            return SaveResponceFunctionToTextFile(pPathFileName);
                        case Files::CFile::eBinary:
                            return SaveResponceFunctionToBinaryFile(pPathFileName);
                    }
                    return false;
                }

                bool CGlobalRadiometricResponceFunction::IsIdealResponceFunctionReady() const
                {
                    return (m_IntensitySlope > _REAL_EPSILON_);
                }

                bool CGlobalRadiometricResponceFunction::EstimateIdealResponceFunction(const CRadianceWeigthingKernel::KernelType Kernel)
                {
                    if (m_pRegressionResponseFunction)
                    {
                        real Deltas[256] = { _REAL_ZERO_ };
                        real Accumulator = _REAL_ZERO_;
                        real SquareAccumulator = _REAL_ZERO_;
                        for (uint i = 1; i < 255; ++i)
                        {
                            const real AbsoluteDeviation = RealAbs(m_pRegressionResponseFunction[i] - m_pRegressionResponseFunction[i + 1]) + RealAbs(m_pRegressionResponseFunction[i] - m_pRegressionResponseFunction[i - 1]);
                            Deltas[i] = AbsoluteDeviation;
                            Accumulator += AbsoluteDeviation;
                            SquareAccumulator += AbsoluteDeviation * AbsoluteDeviation;
                        }
                        const real MeanDelta = Accumulator / real(254.0);
                        const real VarianceDelta = (SquareAccumulator / real(254.0)) - MeanDelta * MeanDelta;
                        const real ExponentFactor = -_REAL_ONE_ / (_REAL_TWO_ * VarianceDelta);
                        for (uint i = 1; i < 255; ++i)
                        {
                            const real Deviation = Deltas[i] - MeanDelta;
                            Deltas[i] = RealExp(Deviation * Deviation * ExponentFactor);
                        }


                        //TODO VERIFY FUNCTION, CHANGES WERE DONE FROM NEWMAT TO EIGEN


                        Mathematics::ND::LinearAlgebra::CMatrixND X(256, 2);
                        Mathematics::ND::LinearAlgebra::CMatrixND Y(256, 1);
                        Mathematics::ND::LinearAlgebra::CMatrixND W(256, 256);
                        W.setZero();
                        for (uint i = 0; i < 256; ++i)
                        {
                            m_pModelEstimationWeightingFunction[i] = m_RadianceWeigthingKernel.GetRegressionWeigthingByKernel(i, Kernel) * Deltas[i];
                            X(i, 0) = Mathematics::ND::LinearAlgebra::MatrixReal(i);
                            X(i, 1) = Mathematics::ND::LinearAlgebra::MatrixReal(_REAL_ONE_);
                            Y(i, 0) = Mathematics::ND::LinearAlgebra::MatrixReal(RealExp(m_pRegressionResponseFunction[i]));
                            W(i, i) = Mathematics::ND::LinearAlgebra::MatrixReal(m_pModelEstimationWeightingFunction[i]);
                        }
                        //try
                        //{
                        Mathematics::ND::LinearAlgebra::CMatrixND B = (X.transpose() * W * X).inverse() * (X.transpose() * W * Y);
                        m_IntensitySlope = real(B(0, 0));
                        m_IntensityOffset = real(B(1, 0));
                        m_MinimalIntensity = TMax(-m_IntensityOffset / m_IntensitySlope, _REAL_EPSILON_);
                        return true;
                        //} catch (Mathematics::ND::LinearAlgebra::CMatrixBaseException & E)
                        //{
                        //  g_ErrorStringOutput << E.what() << g_EndLine;
                        //}
                    }
                    return false;
                }

                real CGlobalRadiometricResponceFunction::GetRegressionResponceFunction(const byte DiscreteIntensity) const
                {
                    return m_pRegressionResponseFunction ? m_pRegressionResponseFunction[DiscreteIntensity] : _REAL_ZERO_;
                }

                real CGlobalRadiometricResponceFunction::GetRegressionResponceFunction(const real Intensity) const
                {
                    if (m_pRegressionResponseFunction && (Intensity >= _REAL_ZERO_) && (Intensity <= _REAL_255_))
                    {
                        const int DiscreteIntensity = DownToInteger(Intensity);
                        return (DiscreteIntensity < 255) ? (m_pRegressionResponseFunction[DiscreteIntensity] + ((m_pRegressionResponseFunction[DiscreteIntensity + 1] - m_pRegressionResponseFunction[DiscreteIntensity]) * (Intensity - real(DiscreteIntensity)))) : m_pRegressionResponseFunction[255];
                    }
                    return _REAL_ZERO_;
                }

                real CGlobalRadiometricResponceFunction::GetIdealResponce(const real Intensity) const
                {
                    if (m_pRegressionResponseFunction && (Intensity >= m_MinimalIntensity) && (Intensity <= _REAL_255_))
                    {
                        return m_IntensitySlope * Intensity + m_IntensityOffset;
                    }
                    return _REAL_ZERO_;
                }

                real CGlobalRadiometricResponceFunction::GetIdealRadiance(const real Intensity, const real ExposureTime) const
                {
                    if (m_pRegressionResponseFunction && (Intensity >= m_MinimalIntensity) && (Intensity <= _REAL_255_) && (ExposureTime > _REAL_EPSILON_))
                    {
                        return (m_IntensitySlope * Intensity + m_IntensityOffset) / ExposureTime;
                    }
                    return _REAL_ZERO_;
                }

                real CGlobalRadiometricResponceFunction::GetIdealExposureTime(const real Intensity, const real Radiance) const
                {
                    if (m_pRegressionResponseFunction && (Intensity >= m_MinimalIntensity) && (Intensity <= _REAL_255_))
                    {
                        return (m_IntensitySlope * Intensity + m_IntensityOffset) / Radiance;
                    }
                    return _REAL_ZERO_;
                }

                real CGlobalRadiometricResponceFunction::GetIdealIntensity(const real Radiance, const real ExposureTime) const
                {
                    const real Energy = Radiance * ExposureTime - m_IntensityOffset;
                    if (Energy >= _REAL_ZERO_)
                    {
                        return Energy / m_IntensitySlope;
                    }
                    return _REAL_ZERO_;
                }

                DeviceIdentifier CGlobalRadiometricResponceFunction::GetCameraId() const
                {
                    return m_CameraId;
                }

                BayerPatternType CGlobalRadiometricResponceFunction::GetBayerPattern() const
                {
                    return m_BayerPattern;
                }

                ChromaticSpaceRGB::ChannelContent CGlobalRadiometricResponceFunction::GetChannelContent() const
                {
                    return m_ChannelContent;
                }

                uint CGlobalRadiometricResponceFunction::GetTotalExpositions() const
                {
                    return m_TotalExpositions;
                }

                uint CGlobalRadiometricResponceFunction::GetTotalSelectedPixels() const
                {
                    return m_TotalSelectedPixels;
                }

                real CGlobalRadiometricResponceFunction::GetDominanceRadius() const
                {
                    return m_DominanceRadius;
                }

                real CGlobalRadiometricResponceFunction::GetSmoothingLambda() const
                {
                    return m_SmoothingLambda;
                }

                CGlobalRadiometricCalibration::PixelSelectionCriterion CGlobalRadiometricResponceFunction::GetPixelSelectionCriterion() const
                {
                    return m_PixelSelectionCriterion;
                }

                const real* CGlobalRadiometricResponceFunction::GetRegressionResponseFunction() const
                {
                    return m_pRegressionResponseFunction;
                }

                const real* CGlobalRadiometricResponceFunction::GetRegressionWeightingFunction() const
                {
                    return m_pRegressionWeightingFunction;
                }

                const real* CGlobalRadiometricResponceFunction::GetModelEstimationWeightingFunction() const
                {
                    return m_pModelEstimationWeightingFunction;
                }

                real CGlobalRadiometricResponceFunction::GetIntensitySlope() const
                {
                    return m_IntensitySlope;
                }

                real CGlobalRadiometricResponceFunction::GetIntensityOffset() const
                {
                    return m_IntensityOffset;
                }

                real CGlobalRadiometricResponceFunction::GetMinimalIntensity() const
                {
                    return m_MinimalIntensity;
                }

                const CRadianceWeigthingKernel& CGlobalRadiometricResponceFunction::GetRadianceWeigthingKernel() const
                {
                    return m_RadianceWeigthingKernel;
                }

                void CGlobalRadiometricResponceFunction::Create()
                {
                    if (!m_pRegressionResponseFunction)
                    {
                        m_pRegressionResponseFunction = new real[256];
                    }
                    if (!m_pRegressionWeightingFunction)
                    {
                        m_pRegressionWeightingFunction = new real[256];
                    }
                    if (!m_pModelEstimationWeightingFunction)
                    {
                        m_pModelEstimationWeightingFunction = new real[256];
                    }
                    memset(m_pRegressionResponseFunction, 0, sizeof(real) * 256);
                    memset(m_pRegressionWeightingFunction, 0, sizeof(real) * 256);
                    memset(m_pModelEstimationWeightingFunction, 0, sizeof(real) * 256);
                }

                void CGlobalRadiometricResponceFunction::Destroy()
                {
                    RELEASE_ARRAY(m_pRegressionResponseFunction)
                    RELEASE_ARRAY(m_pRegressionWeightingFunction)
                    RELEASE_ARRAY(m_pModelEstimationWeightingFunction)
                }

                bool CGlobalRadiometricResponceFunction::SaveResponceFunctionToTextFile(const_string pPathFileName) const
                {
                    if (m_CameraId && m_pRegressionResponseFunction && m_pRegressionWeightingFunction && m_pModelEstimationWeightingFunction)
                    {
                        std::ostringstream OutputText;
                        OutputText.precision(_GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_DIGITS_);
                        OutputText << "Global Radiometric Camera Calibration" << g_EndLine;
                        OutputText << g_EndLine << "[General Information]" << g_EndLine << g_EndLine;
                        OutputText << "Camera Device Identifier" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_ << DeviceIdentifierToString(m_CameraId) << g_EndLine;
                        OutputText << "Bayer Pattern" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_ << BayerPatternToString(m_BayerPattern) << g_EndLine;
                        OutputText << "Channel Content" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        switch (m_ChannelContent)
                        {
                            case ChromaticSpaceRGB::eRed:
                                OutputText << "Red" << g_EndLine;
                                break;
                            case ChromaticSpaceRGB::eGreen:
                                OutputText << "Green" << g_EndLine;
                                break;
                            case ChromaticSpaceRGB::eBlue:
                                OutputText << "Blue" << g_EndLine;
                                break;
                            case ChromaticSpaceRGB::eUnknownChannelContent:
                                OutputText << "Unknown" << g_EndLine;
                                break;
                        }
                        OutputText << "Total Expositions" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_ << m_TotalExpositions << g_EndLine;
                        OutputText << "Total Selected Pixels" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_ << m_TotalSelectedPixels << g_EndLine;
                        OutputText << "Pixel Selection Criterion" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_ << CGlobalRadiometricCalibration::PixelSelectionCriterionToString(m_PixelSelectionCriterion) << g_EndLine;
                        OutputText << "Dominance Radius" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_ << m_DominanceRadius << g_EndLine;
                        OutputText << "Smoothing Lambda" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_ << m_SmoothingLambda << g_EndLine;
                        OutputText << "Exponential Domain Slope" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_ << m_IntensitySlope << g_EndLine;
                        OutputText << "Exponential Domain Offset" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_ << m_IntensityOffset << g_EndLine;
                        OutputText << "Minimal Integrable Continuous Intensity" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_ << m_MinimalIntensity << g_EndLine;
                        OutputText << g_EndLine << "[Response Curves]" << g_EndLine << g_EndLine;
                        OutputText << "Continuous Intensity" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << "Discrete Intensity" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << "Regression Weights" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << "Model Estimation Weights" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << "G(I)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << "e^G(I)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << "Gp(I) = log((Slope*I)+Offset)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << "e^Gp(I)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << "e^G(I)-e^Gp(Iy)" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                        OutputText << "|e^G(I)-e^Gp(I)|";
                        const real Increment = _REAL_ONE_ / real(_GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_INCREMENTS_PER_UNIT_);
                        for (real ContinuousIntensity = _REAL_ZERO_; ContinuousIntensity <= _REAL_255_; ContinuousIntensity += Increment)
                        {
                            const uint DiscreteIntensity = uint(RealRound(ContinuousIntensity));
                            OutputText << g_EndLine << ContinuousIntensity << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                            OutputText << DiscreteIntensity << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                            OutputText << m_pRegressionWeightingFunction[DiscreteIntensity] << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                            OutputText << m_pModelEstimationWeightingFunction[DiscreteIntensity] << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                            OutputText << m_pRegressionResponseFunction[DiscreteIntensity] << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                            const real eG = RealExp(m_pRegressionResponseFunction[DiscreteIntensity]);
                            OutputText << eG << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                            if (ContinuousIntensity > m_MinimalIntensity)
                            {
                                const real eGp = GetIdealResponce(ContinuousIntensity);
                                const real Gp = RealLog(eGp);
                                OutputText << Gp << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                                OutputText << eGp << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                                OutputText << eG - eGp << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_;
                                OutputText << RealAbs(eG - eGp);
                            }
                            else
                            {
                                OutputText << "NAN" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_ << "NAN" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_ << "NAN" << _GLOBAL_RADIOMETRIC_RESPONCE_FUNTION_DISPLAY_SEPARATOR_ << "NAN";
                            }
                        }
                        OutputText << g_EndLine << "For more information see IEEE-RAS publication: Gonzalez-Aguirre, D.; Asfour, T.; Dillmann, R.; , \"Eccentricity edge-graphs from HDR images for object recognition by humanoid robots,\" Humanoid Robots (Humanoids), 2010 10th IEEE-RAS International Conference on , vol., no., pp.144-151, 6-8 Dec. 2010 doi: 10.1109/ICHR.2010.5686336";
                        return Files::COutFile::WriteStringToFile(pPathFileName, OutputText.str());
                    }
                    return false;
                }

                bool CGlobalRadiometricResponceFunction::SaveResponceFunctionToBinaryFile(const_string pPathFileName) const
                {
                    if (m_CameraId && m_pRegressionResponseFunction && m_pRegressionWeightingFunction && m_pModelEstimationWeightingFunction)
                    {
                        Files::COutFile OutputBinaryFile(pPathFileName, Files::CFile::eBinary);
                        if (SaveResponceFunctionToBinaryFile(OutputBinaryFile))
                        {
                            return OutputBinaryFile.Close();
                        }
                    }
                    return false;
                }

                bool CGlobalRadiometricResponceFunction::SaveResponceFunctionToBinaryFile(Files::COutFile& OutputBinaryFile) const
                {
                    if (m_CameraId && m_pRegressionResponseFunction && m_pRegressionWeightingFunction && m_pModelEstimationWeightingFunction)
                    {
                        if (OutputBinaryFile.IsReady())
                            if (OutputBinaryFile.WriteBuffer(&m_CameraId, sizeof(DeviceIdentifier)))
                                if (OutputBinaryFile.WriteBuffer(&m_BayerPattern, sizeof(BayerPatternType)))
                                    if (OutputBinaryFile.WriteBuffer(&m_ChannelContent, sizeof(ChromaticSpaceRGB::ChannelContent)))
                                        if (OutputBinaryFile.WriteBuffer(&m_TotalExpositions, sizeof(uint)))
                                            if (OutputBinaryFile.WriteBuffer(&m_TotalSelectedPixels, sizeof(uint)))
                                                if (OutputBinaryFile.WriteBuffer(&m_DominanceRadius, sizeof(real)))
                                                    if (OutputBinaryFile.WriteBuffer(&m_SmoothingLambda, sizeof(real)))
                                                        if (OutputBinaryFile.WriteBuffer(&m_PixelSelectionCriterion, sizeof(CGlobalRadiometricCalibration::PixelSelectionCriterion)))
                                                            if (OutputBinaryFile.WriteBuffer(m_pRegressionResponseFunction, sizeof(real) * 256))
                                                                if (OutputBinaryFile.WriteBuffer(m_pRegressionWeightingFunction, sizeof(real) * 256))
                                                                    if (OutputBinaryFile.WriteBuffer(m_pModelEstimationWeightingFunction, sizeof(real) * 256))
                                                                        if (OutputBinaryFile.WriteBuffer(&m_IntensitySlope, sizeof(real)))
                                                                            if (OutputBinaryFile.WriteBuffer(&m_IntensityOffset, sizeof(real)))
                                                                                if (OutputBinaryFile.WriteBuffer(&m_MinimalIntensity, sizeof(real)))
                                                                                {
                                                                                    return m_RadianceWeigthingKernel.SaveToFile(OutputBinaryFile);
                                                                                }
                    }
                    return false;
                }

                bool CGlobalRadiometricResponceFunction::LoadResponceFunctionFromBinaryFile(Files::CInFile& InputBinaryFile)
                {
                    if (InputBinaryFile.IsReady())
                    {
                        Create();
                        if (InputBinaryFile.ReadBuffer(&m_CameraId, sizeof(DeviceIdentifier)))
                            if (InputBinaryFile.ReadBuffer(&m_BayerPattern, sizeof(BayerPatternType)))
                                if (InputBinaryFile.ReadBuffer(&m_ChannelContent, sizeof(ChromaticSpaceRGB::ChannelContent)))
                                    if (InputBinaryFile.ReadBuffer(&m_TotalExpositions, sizeof(uint)))
                                        if (InputBinaryFile.ReadBuffer(&m_TotalSelectedPixels, sizeof(uint)))
                                            if (InputBinaryFile.ReadBuffer(&m_DominanceRadius, sizeof(real)))
                                                if (InputBinaryFile.ReadBuffer(&m_SmoothingLambda, sizeof(real)))
                                                    if (InputBinaryFile.ReadBuffer(&m_PixelSelectionCriterion, sizeof(CGlobalRadiometricCalibration::PixelSelectionCriterion)))
                                                        if (InputBinaryFile.ReadBuffer(m_pRegressionResponseFunction, sizeof(real) * 256))
                                                            if (InputBinaryFile.ReadBuffer(m_pRegressionWeightingFunction, sizeof(real) * 256))
                                                                if (InputBinaryFile.ReadBuffer(m_pModelEstimationWeightingFunction, sizeof(real) * 256))
                                                                    if (InputBinaryFile.ReadBuffer(&m_IntensitySlope, sizeof(real)))
                                                                        if (InputBinaryFile.ReadBuffer(&m_IntensityOffset, sizeof(real)))
                                                                            if (InputBinaryFile.ReadBuffer(&m_MinimalIntensity, sizeof(real)))
                                                                            {
                                                                                return m_RadianceWeigthingKernel.LoadFromFile(InputBinaryFile);
                                                                            }
                        Destroy();
                    }
                    return false;
                }
            }
        }
    }
}
