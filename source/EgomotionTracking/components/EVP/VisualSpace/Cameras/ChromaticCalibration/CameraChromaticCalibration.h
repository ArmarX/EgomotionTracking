/*
 * CameraChromaticCalibration.h
 *
 *  Created on: 07.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"
#include "../../../Foundation/DataTypes/ColorSpaces.h"
#include "../../../Visualization/Miscellaneous/TristimulusColorMap.h"
#include "../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../Common/Image.h"
#include "../../Common/ImageActiveZone.h"
#include "../../Chromatic/ChromaticPixel.h"
#include "../../Chromatic/ChromaticBlock.h"

#define _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_CHROMATIC_SPACE_                        eRGB
#define _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_WHITE_SPOT_                             ChromaticSpaceLAB::eD65
#define _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_DISTANCE_METRIC_                        Chromatic::CChromaticPixel::eEuclidean
#define _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_MINKOWSKI_FACTOR_                       _REAL_ONE_
#define _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_DISTANCE_THRESHOLD_                     real(2.0)
#define _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_EXPANSION_CONNECTIVITY_                 8
#define _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_SPLITTING_CONNECTIVITY_                 4
#define _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_MINIMAL_PIXELS_PER_BLOCK_               16
#define _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_MINIMAL_REMOVE_SPECKLES_                true
#define _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_MAXIMAL_BORDER_CORE_RATIO_              _REAL_ONE_
#define _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_DISPLAY_RESIDUAL_CHROMATIC_BLOCKS_      false
#define _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_DISPLAY_MINIMAL_SIZE_CHROMATIC_BLOCK_   4
#define _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_DISPLAY_BACKGROUND_COLOR_               64,64,64

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace ChromaticCalibration
            {
                class CCameraChromaticCalibration //TODO Make this process in monocular mode
                {
                public:

                    struct Parameters
                    {
                        ChromaticSpace m_ChromaticSpace;
#ifdef _USE_COLOR_SPACE_LAB_
                        ChromaticSpaceLAB::WhiteSpot m_WhiteSpot;
#endif
                        Chromatic::CChromaticPixel::DistanceMetric m_DistanceMetric;
                        real m_MinkowskiFactor;
                        real m_DistanceThreshold;
                        int m_ExpansionConnectivity;
                        int m_SplittingConnectivity;
                        uint m_MinimalPixelsPerBlock;
                        bool m_RemoveSpeckles;
                        real m_MaximalBorderCoreRatio;
                        bool m_DisplayResidualChromaticBlocks;
                        uint m_DisplayMinimalSizeChromaticBlock;
                        CDiscreteTristimulusPixel m_DisplayBackgroundColor;
                    };

                    enum CalibrationPattern
                    {
                        eWhiteBalancePattern, eColorChecker
                    };

                    enum CalibrationFeature
                    {
                        eWhiteBalance, eHue
                    };

                    CCameraChromaticCalibration();
                    virtual ~CCameraChromaticCalibration();

                    bool CalibrateFromImage(TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage, const CImageActiveZone* pActiveZone, CalibrationPattern Pattern, CalibrationFeature Feature);
                    bool CalibrateFromImage(TImage<CContinuousTristimulusPixel>* pContinuousRGBImage, const CImageActiveZone* pActiveZone, CalibrationPattern Pattern, CalibrationFeature Feature);

                    real GetWhiteBalanceRedValue();
                    real GetWhiteBalanceBlueValue();
                    real GetHue();

                    Parameters* GetParameters();
                    TImage<CDiscreteTristimulusPixel>* GetDisplayImage();

                protected:

                    bool EstimateWhiteBalanceFromSinglePattern(TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage, const CImageActiveZone* pActiveZone);
                    bool EstimateWhiteBalanceFromMultiPattern(TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage, const CImageActiveZone* pActiveZone);
                    bool EstimateHueFromMultiPattern(TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage, const CImageActiveZone* pActiveZone);
                    bool EstimateWhiteBalanceFromSinglePattern(TImage<CContinuousTristimulusPixel>* pContinuousRGBImage, const CImageActiveZone* pActiveZone);
                    bool EstimateWhiteBalanceFromMultiPattern(TImage<CContinuousTristimulusPixel>* pContinuousRGBImage, const CImageActiveZone* pActiveZone);
                    bool EstimateHueFromMultiPattern(TImage<CContinuousTristimulusPixel>* pContinuousRGBImage, const CImageActiveZone* pActiveZone);

                    real m_WhiteBalanceRedValue;
                    real m_WhiteBalanceBlueValue;
                    real m_Hue;

                    Parameters m_Parameters;
                    CImageActiveZone m_ActiveZone;
                    coordinate m_pOctoConnectivityDeltas[8];
                    list<Chromatic::CChromaticBlock*> m_SelectedChromaticBlocks;
                    list<Chromatic::CChromaticBlock*> m_ResidualChromaticBlocks;
                    TImage<CContinuousTristimulusPixel>* m_pInputContinuousRGBImage;
                    TImage<CDiscreteTristimulusPixel>* m_pOutputDiscreteRGBImage;
                    TImage<Chromatic::CChromaticPixel>* m_pChromaticPixelImage;

                    static bool SortChromaticBlockByCoreSize(Chromatic::CChromaticBlock* plhs, Chromatic::CChromaticBlock* prhs);
                    bool LoadImage(const TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage, const CImageActiveZone* pActiveZone);
                    bool LoadImage(const TImage<CContinuousTristimulusPixel>* pContinuousRGBImage, const CImageActiveZone* pActiveZone);
                    void LinkPixels();
                    void ClearChromaticBlocks();
                    uint Segmentation(bool RemoveSpeckles);
                    void DistanceTransformation();
                    list<Chromatic::CChromaticBlock*> ExtractPreselectionChromaticBlocks();
                    void FilterSpeckles();
                    void SelectChromaticBlocks(list<Chromatic::CChromaticBlock*>& PreSelectedChromaticBlocks);
                    void DisplaySelectedChromaticBlocks(bool DisplayResidualChromaticBlocksOn, uint MinimalSizeChromaticBlocks);
                    void DisplayDistanceTransformationChromaticBlocks(Visualization::CTristimulusColorMap* pColorMap);
                    void DisplaySelectedChromaticBlocks(TImage<CDiscreteTristimulusPixel>* pOutputDiscreteRGBImage, list<Chromatic::CChromaticBlock*>& ChromaticBlocks);
                    void DisplayResidualChromaticBlocks(TImage<CDiscreteTristimulusPixel>* pOutputDiscreteRGBImage, list<Chromatic::CChromaticBlock*>& ChromaticBlocks, uint MinimalSizeChromaticBlock);
                };
            }
        }
    }
}

