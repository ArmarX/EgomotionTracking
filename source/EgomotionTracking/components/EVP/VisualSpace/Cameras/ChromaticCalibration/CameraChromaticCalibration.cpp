/*
 * CameraChromaticCalibration.cpp
 *
 *  Created on: 07.04.2011
 *      Author: gonzalez
 */

#include "CameraChromaticCalibration.h"

using namespace EVP::VisualSpace::Chromatic;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace ChromaticCalibration
            {
                CCameraChromaticCalibration::CCameraChromaticCalibration()
                {
                    m_pInputContinuousRGBImage = nullptr;
                    m_pChromaticPixelImage = nullptr;
                    m_pOutputDiscreteRGBImage = nullptr;
                    m_Parameters.m_ChromaticSpace = _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_CHROMATIC_SPACE_;
#ifdef _USE_COLOR_SPACE_LAB_
                    m_Parameters.m_WhiteSpot = _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_WHITE_SPOT_;
#endif
                    m_Parameters.m_DistanceMetric = _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_DISTANCE_METRIC_;
                    m_Parameters.m_MinkowskiFactor = _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_MINKOWSKI_FACTOR_;
                    m_Parameters.m_DistanceThreshold = _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_DISTANCE_THRESHOLD_;
                    m_Parameters.m_ExpansionConnectivity = _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_EXPANSION_CONNECTIVITY_;
                    m_Parameters.m_SplittingConnectivity = _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_SPLITTING_CONNECTIVITY_;
                    m_Parameters.m_MinimalPixelsPerBlock = _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_MINIMAL_PIXELS_PER_BLOCK_;
                    m_Parameters.m_RemoveSpeckles = _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_MINIMAL_REMOVE_SPECKLES_;
                    m_Parameters.m_MaximalBorderCoreRatio = _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_MAXIMAL_BORDER_CORE_RATIO_;
                    m_Parameters.m_DisplayResidualChromaticBlocks = _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_DISPLAY_RESIDUAL_CHROMATIC_BLOCKS_;
                    m_Parameters.m_DisplayMinimalSizeChromaticBlock = _CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_DISPLAY_MINIMAL_SIZE_CHROMATIC_BLOCK_;
                    m_Parameters.m_DisplayBackgroundColor.SetValue(_CAMERA_CHROMATIC_CALIBRATION_DEFAULT_VALUE_DISPLAY_BACKGROUND_COLOR_);
                }

                CCameraChromaticCalibration::~CCameraChromaticCalibration()
                {
                    ClearChromaticBlocks();
                    RELEASE_OBJECT(m_pInputContinuousRGBImage)
                    RELEASE_OBJECT(m_pChromaticPixelImage)
                    RELEASE_OBJECT(m_pOutputDiscreteRGBImage)
                }

                bool CCameraChromaticCalibration::CalibrateFromImage(TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage, const CImageActiveZone* pActiveZone, CalibrationPattern Pattern, CalibrationFeature Feature)
                {
                    if (pDiscreteRGBImage)
                    {
                        switch (Pattern)
                        {
                            case eWhiteBalancePattern:
                                switch (Feature)
                                {
                                    case eWhiteBalance:
                                        return EstimateWhiteBalanceFromSinglePattern(pDiscreteRGBImage, pActiveZone);
                                        break;
                                    case eHue:
                                        return false;
                                        break;
                                }
                                break;
                            case eColorChecker:
                                switch (Feature)
                                {
                                    case eWhiteBalance:
                                        return EstimateWhiteBalanceFromMultiPattern(pDiscreteRGBImage, pActiveZone);
                                        break;
                                    case eHue:
                                        return EstimateHueFromMultiPattern(pDiscreteRGBImage, pActiveZone);
                                        break;
                                }
                                break;
                        }
                    }
                    return false;
                }

                bool CCameraChromaticCalibration::EstimateWhiteBalanceFromSinglePattern(TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage, const CImageActiveZone* pActiveZone)
                {
                    if (LoadImage(pDiscreteRGBImage, pActiveZone))
                    {
                        if (Segmentation(m_Parameters.m_RemoveSpeckles))
                        {
                            DisplaySelectedChromaticBlocks(m_Parameters.m_DisplayResidualChromaticBlocks, m_Parameters.m_DisplayMinimalSizeChromaticBlock);
                        }
                    }
                    return false;
                }

                bool CCameraChromaticCalibration::EstimateWhiteBalanceFromMultiPattern(TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage, const CImageActiveZone* pActiveZone)
                {
                    if (LoadImage(pDiscreteRGBImage, pActiveZone))
                    {
                        if (Segmentation(m_Parameters.m_RemoveSpeckles))
                        {
                            DisplaySelectedChromaticBlocks(m_Parameters.m_DisplayResidualChromaticBlocks, m_Parameters.m_DisplayMinimalSizeChromaticBlock);
                        }
                    }
                    return false;
                }

                bool CCameraChromaticCalibration::EstimateHueFromMultiPattern(TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage, const CImageActiveZone* pActiveZone)
                {
                    if (LoadImage(pDiscreteRGBImage, pActiveZone))
                    {
                        if (Segmentation(m_Parameters.m_RemoveSpeckles))
                        {
                            DisplaySelectedChromaticBlocks(m_Parameters.m_DisplayResidualChromaticBlocks, m_Parameters.m_DisplayMinimalSizeChromaticBlock);
                        }
                    }
                    return false;
                }

                bool CCameraChromaticCalibration::CalibrateFromImage(TImage<CContinuousTristimulusPixel>* pContinuousRGBImage, const CImageActiveZone* pActiveZone, CalibrationPattern Pattern, CalibrationFeature Feature)
                {
                    if (pContinuousRGBImage)
                    {
                        switch (Pattern)
                        {
                            case eWhiteBalancePattern:
                                switch (Feature)
                                {
                                    case eWhiteBalance:
                                        return EstimateWhiteBalanceFromSinglePattern(pContinuousRGBImage, pActiveZone);
                                        break;
                                    case eHue:
                                        return false;
                                        break;
                                }
                                break;
                            case eColorChecker:
                                switch (Feature)
                                {
                                    case eWhiteBalance:
                                        return EstimateWhiteBalanceFromMultiPattern(pContinuousRGBImage, pActiveZone);
                                        break;
                                    case eHue:
                                        return EstimateHueFromMultiPattern(pContinuousRGBImage, pActiveZone);
                                        break;
                                }
                                break;
                        }
                    }
                    return false;
                }

                bool CCameraChromaticCalibration::EstimateWhiteBalanceFromSinglePattern(TImage<CContinuousTristimulusPixel>* pContinuousRGBImage, const CImageActiveZone* pActiveZone)
                {
                    if (LoadImage(pContinuousRGBImage, pActiveZone))
                    {
                        if (Segmentation(m_Parameters.m_RemoveSpeckles))
                        {
                            DisplaySelectedChromaticBlocks(m_Parameters.m_DisplayResidualChromaticBlocks, m_Parameters.m_DisplayMinimalSizeChromaticBlock);
                        }
                    }
                    return false;
                }

                bool CCameraChromaticCalibration::EstimateWhiteBalanceFromMultiPattern(TImage<CContinuousTristimulusPixel>* pContinuousRGBImage, const CImageActiveZone* pActiveZone)
                {
                    if (LoadImage(pContinuousRGBImage, pActiveZone))
                    {
                        if (Segmentation(m_Parameters.m_RemoveSpeckles))
                        {
                            DisplaySelectedChromaticBlocks(m_Parameters.m_DisplayResidualChromaticBlocks, m_Parameters.m_DisplayMinimalSizeChromaticBlock);
                        }
                    }
                    return false;
                }

                bool CCameraChromaticCalibration::EstimateHueFromMultiPattern(TImage<CContinuousTristimulusPixel>* pContinuousRGBImage, const CImageActiveZone* pActiveZone)
                {
                    if (LoadImage(pContinuousRGBImage, pActiveZone))
                    {
                        if (Segmentation(m_Parameters.m_RemoveSpeckles))
                        {
                            DisplaySelectedChromaticBlocks(m_Parameters.m_DisplayResidualChromaticBlocks, m_Parameters.m_DisplayMinimalSizeChromaticBlock);
                        }
                    }
                    return false;
                }

                real CCameraChromaticCalibration::GetWhiteBalanceRedValue()
                {
                    return m_WhiteBalanceRedValue;
                }

                real CCameraChromaticCalibration::GetWhiteBalanceBlueValue()
                {
                    return m_WhiteBalanceBlueValue;
                }

                real CCameraChromaticCalibration::GetHue()
                {
                    return m_Hue;
                }

                CCameraChromaticCalibration::Parameters* CCameraChromaticCalibration::GetParameters()
                {
                    return &m_Parameters;
                }

                TImage<CDiscreteTristimulusPixel>* CCameraChromaticCalibration::GetDisplayImage()
                {
                    return m_pOutputDiscreteRGBImage;
                }

                bool CCameraChromaticCalibration::SortChromaticBlockByCoreSize(CChromaticBlock* plhs, CChromaticBlock* prhs)
                {
                    return plhs->GetTotalChromaticPixelsRedundatCore() > prhs->GetTotalChromaticPixelsRedundatCore();
                }

                bool CCameraChromaticCalibration::LoadImage(const TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage, const CImageActiveZone* pActiveZone)
                {
                    if (pDiscreteRGBImage)
                    {
                        const CImageSize& ImageSize = pDiscreteRGBImage->GetSize();
                        if (m_pInputContinuousRGBImage && (m_pInputContinuousRGBImage->GetSize() != ImageSize))
                        {
                            RELEASE_OBJECT(m_pInputContinuousRGBImage)
                            RELEASE_OBJECT(m_pOutputDiscreteRGBImage)
                        }
                        if (!m_pInputContinuousRGBImage)
                        {
                            m_pInputContinuousRGBImage = new TImage<CContinuousTristimulusPixel> (ImageSize);
                            m_pOutputDiscreteRGBImage = new TImage<CDiscreteTristimulusPixel> (ImageSize);
                            m_ActiveZone.Set(pActiveZone, ImageSize);
                            m_ActiveZone.AddMarginOffset(1);
                            m_ActiveZone.GetOctoConnectivityDeltas(m_pOctoConnectivityDeltas);
                            LinkPixels();
                        }
                        const CDiscreteTristimulusPixel* const pDiscreteRGBEnd = pDiscreteRGBImage->GetEndReadOnlyBuffer();
                        const CDiscreteTristimulusPixel* pDiscreteRGB = pDiscreteRGBImage->GetBeginReadOnlyBuffer();
                        CContinuousTristimulusPixel* pContinuousRGB = m_pInputContinuousRGBImage->GetBeginWritableBuffer();
                        while (pDiscreteRGB < pDiscreteRGBEnd)
                        {
                            *pContinuousRGB++ = *pDiscreteRGB++;
                        }
                        ClearChromaticBlocks();
                        return true;
                    }
                    return false;
                }

                bool CCameraChromaticCalibration::LoadImage(const TImage<CContinuousTristimulusPixel>* pContinuousRGBImage, const CImageActiveZone* pActiveZone)
                {
                    if (pContinuousRGBImage)
                    {
                        const CImageSize& ImageSize = pContinuousRGBImage->GetSize();
                        if (m_pInputContinuousRGBImage && (m_pInputContinuousRGBImage->GetSize() != ImageSize))
                        {
                            RELEASE_OBJECT(m_pInputContinuousRGBImage)
                            RELEASE_OBJECT(m_pOutputDiscreteRGBImage)
                        }
                        if (!m_pInputContinuousRGBImage)
                        {
                            m_pInputContinuousRGBImage = new TImage<CContinuousTristimulusPixel> (ImageSize);
                            m_pOutputDiscreteRGBImage = new TImage<CDiscreteTristimulusPixel> (ImageSize);
                            m_ActiveZone.Set(pActiveZone, ImageSize);
                            m_ActiveZone.AddMarginOffset(1);
                            m_ActiveZone.GetOctoConnectivityDeltas(m_pOctoConnectivityDeltas);
                            LinkPixels();
                        }
                        m_pInputContinuousRGBImage->Copy(pContinuousRGBImage);
                        ClearChromaticBlocks();
                        return true;
                    }
                    return false;
                }

                void CCameraChromaticCalibration::LinkPixels()
                {
                    const CImageSize& ImageSize = m_pInputContinuousRGBImage->GetSize();
                    if (m_pChromaticPixelImage && (m_pChromaticPixelImage->GetSize() != ImageSize))
                        RELEASE_OBJECT(m_pInputContinuousRGBImage)
                        if (!m_pChromaticPixelImage)
                        {
                            m_pChromaticPixelImage = new TImage<CChromaticPixel> (ImageSize);
                        }
                    const coordinate X0 = m_ActiveZone.GetX0();
                    const coordinate Y0 = m_ActiveZone.GetY0();
                    const coordinate X1 = m_ActiveZone.GetX1();
                    const coordinate Y1 = m_ActiveZone.GetY1();
                    const coordinate ImageWidth = m_ActiveZone.GetWidth();
                    const coordinate ImageHeight = m_ActiveZone.GetHeight();
                    const CContinuousTristimulusPixel* pContinuousRGB = m_pInputContinuousRGBImage->GetBeginReadOnlyBuffer();
                    CChromaticPixel* pChromaticPixel = m_pChromaticPixelImage->GetBeginWritableBuffer();
                    for (CPixelLocation Location; Location.m_y < ImageHeight; ++Location.m_y)
                    {
                        bool IsAtImageActiveZoneBorder = (Location.m_y <= Y0) || (Location.m_y >= Y1);
                        for (Location.m_x = 0; Location.m_x < ImageWidth; ++Location.m_x, ++pChromaticPixel, ++pContinuousRGB)
                        {
                            if (m_ActiveZone.IsDiscreteLocationInside(Location, true))
                            {
                                pChromaticPixel->SetEnabled(true);
                                pChromaticPixel->SetAtImageActiveZoneBorder(IsAtImageActiveZoneBorder || (Location.m_x <= X0) || (Location.m_x >= X1));
                            }
                            pChromaticPixel->SetLocation(Location);
                            pChromaticPixel->SetChromaticSource(pContinuousRGB);
                        }
                    }
                }

                void CCameraChromaticCalibration::ClearChromaticBlocks()
                {
                    if (m_SelectedChromaticBlocks.size())
                    {
                        list<CChromaticBlock*>::iterator EndChromaticBlocks = m_SelectedChromaticBlocks.end();
                        for (list<CChromaticBlock*>::iterator ppChromaticBlock = m_SelectedChromaticBlocks.begin(); ppChromaticBlock != EndChromaticBlocks; ++ppChromaticBlock)
                            RELEASE_OBJECT_DIRECT(*ppChromaticBlock)
                            m_SelectedChromaticBlocks.clear();
                    }
                    if (m_ResidualChromaticBlocks.size())
                    {
                        list<CChromaticBlock*>::iterator EndChromaticBlocks = m_ResidualChromaticBlocks.end();
                        for (list<CChromaticBlock*>::iterator ppChromaticBlock = m_ResidualChromaticBlocks.begin(); ppChromaticBlock != EndChromaticBlocks; ++ppChromaticBlock)
                            RELEASE_OBJECT_DIRECT(*ppChromaticBlock)
                            m_ResidualChromaticBlocks.clear();
                    }
                }

                list<CChromaticBlock*> CCameraChromaticCalibration::ExtractPreselectionChromaticBlocks()
                {
                    const coordinate X0 = m_ActiveZone.GetX0();
                    const coordinate Y0 = m_ActiveZone.GetY0();
                    const coordinate X1 = m_ActiveZone.GetX1();
                    const coordinate Y1 = m_ActiveZone.GetY1();
                    const coordinate ImageWidth = m_ActiveZone.GetWidth();
                    list<CChromaticBlock*> PreSelectedChromaticBlocks;
                    CChromaticPixel* pBaseChromaticPixel = m_pChromaticPixelImage->GetWritableBufferAt(X0, Y0);
                    for (int y = Y0; y < Y1; ++y, pBaseChromaticPixel += ImageWidth)
                    {
                        CChromaticPixel* pChromaticPixel = pBaseChromaticPixel;
                        for (int x = X0; x < X1; ++x, ++pChromaticPixel)
                            if (!pChromaticPixel->GetReadOnlyChromaticBlock())
                            {
                                CChromaticBlock* pChromaticBlock = new CChromaticBlock(pChromaticPixel);
                                if (pChromaticBlock->Expand(m_Parameters.m_DistanceThreshold, m_Parameters.m_MinkowskiFactor, m_Parameters.m_ChromaticSpace, m_Parameters.m_DistanceMetric, m_pOctoConnectivityDeltas, m_Parameters.m_ExpansionConnectivity) > m_Parameters.m_MinimalPixelsPerBlock)
                                {
                                    pChromaticBlock->SetStatus(CChromaticBlock::ePreSelected);
                                    PreSelectedChromaticBlocks.push_back(pChromaticBlock);
                                }
                                else
                                {
                                    pChromaticBlock->SetStatus(CChromaticBlock::eResidual);
                                    m_ResidualChromaticBlocks.push_back(pChromaticBlock);
                                }
                            }
                    }
                    return PreSelectedChromaticBlocks;
                }

                void CCameraChromaticCalibration::FilterSpeckles()
                {
                    list<CChromaticBlock*> AbsorvedChromaticBlocks;
                    list<CChromaticBlock*>::iterator EndResidualChromaticBlocks = m_ResidualChromaticBlocks.end();
                    for (list<CChromaticBlock*>::iterator ppChromaticBlock = m_ResidualChromaticBlocks.begin(); ppChromaticBlock != EndResidualChromaticBlocks; ++ppChromaticBlock)
                    {
                        CChromaticBlock* pChromaticBlock = *ppChromaticBlock;
                        CChromaticBlock* pAuxiliarChromaticBlock = pChromaticBlock->GetPreselectedSurroundingBlock(m_pOctoConnectivityDeltas, 8);
                        if (pAuxiliarChromaticBlock)
                        {
                            pAuxiliarChromaticBlock->AbsorveFromChildBlock(pChromaticBlock);
                            AbsorvedChromaticBlocks.push_back(pChromaticBlock);
                        }
                    }
                    if (AbsorvedChromaticBlocks.size())
                    {
                        list<CChromaticBlock*>::iterator EndAbsorvedChromaticBlocks = AbsorvedChromaticBlocks.end();
                        for (list<CChromaticBlock*>::iterator ppChromaticBlock = AbsorvedChromaticBlocks.begin(); ppChromaticBlock != EndAbsorvedChromaticBlocks; ++ppChromaticBlock)
                        {
                            (*ppChromaticBlock)->AsimilateToParentBlock();
                        }
                    }
                }

                void CCameraChromaticCalibration::SelectChromaticBlocks(list<CChromaticBlock*>& PreSelectedChromaticBlocks)
                {
                    //CChromaticBlock* pAuxiliarChromaticBlock = NULL;
                    list<CChromaticBlock*>::iterator EndPreSelectedChromaticBlocks = PreSelectedChromaticBlocks.end();
                    for (list<CChromaticBlock*>::iterator ppChromaticBlock = PreSelectedChromaticBlocks.begin(); ppChromaticBlock != EndPreSelectedChromaticBlocks; ++ppChromaticBlock)
                    {
                        CChromaticBlock* pChromaticBlock = *ppChromaticBlock;
                        pChromaticBlock->SplitPixelsByConnectivity(m_pOctoConnectivityDeltas, m_Parameters.m_SplittingConnectivity);
                        if (pChromaticBlock->GetBorderCoreRatio() < m_Parameters.m_MaximalBorderCoreRatio)
                        {
                            pChromaticBlock->FilterSpuriousCore(m_pOctoConnectivityDeltas, m_Parameters.m_SplittingConnectivity);
                            //@TODO REVIEW
                            /*TList<CChromaticBlock*> SubCores = pChromaticBlock->SplitInsolatedSubCores(m_OctoConnectivityDeltas, m_Parameters.m_SplittingConnectivity, m_Parameters.m_MinimalPixelsPerBlock);
                             if (SubCores.size())
                             {
                             m_ResidualChromaticBlocks.push_back(pChromaticBlock);
                             TList<CChromaticBlock*>::iterator EndSubCores = SubCores.end();
                             for (TList<CChromaticBlock*>::iterator ppSubCore = SubCores.begin(); ppSubCore != EndSubCores; ++ppSubCore)
                             {
                             pAuxiliarChromaticBlock = *ppSubCore;
                             if (int(pAuxiliarChromaticBlock->GetChromaticPixelsCore().size()) > m_Parameters.m_MinimalPixelsPerBlock)
                             {
                             pAuxiliarChromaticBlock->SplitBorderPixelsByRedundancy(m_OctoConnectivityDeltas, m_Parameters.m_SplittingConnectivity);
                             pAuxiliarChromaticBlock->ExtractContoures(m_OctoConnectivityDeltas, m_Parameters.m_SplittingConnectivity);
                             pAuxiliarChromaticBlock->ComputeSpatialEigenStructure();
                             pAuxiliarChromaticBlock->SetStatus(CChromaticBlock::eSelected);
                             m_SelectedChromaticBlocks.push_back(pAuxiliarChromaticBlock);
                             }
                             else
                             m_ResidualChromaticBlocks.push_back(pAuxiliarChromaticBlock);
                             }
                             }
                             else
                             {*/
                            if (pChromaticBlock->GetChromaticPixelsCore().size() > m_Parameters.m_MinimalPixelsPerBlock)
                            {
                                pChromaticBlock->SplitBorderPixelsByRedundancy(m_pOctoConnectivityDeltas, m_Parameters.m_SplittingConnectivity);
                                pChromaticBlock->ExtractContoures(m_pOctoConnectivityDeltas, m_Parameters.m_SplittingConnectivity);
                                pChromaticBlock->ComputeSpatialEigenStructure();
                                pChromaticBlock->SetStatus(CChromaticBlock::eSelected);
                                m_SelectedChromaticBlocks.push_back(pChromaticBlock);
                            }
                            else
                            {
                                m_ResidualChromaticBlocks.push_back(pChromaticBlock);
                            }
                            //}
                        }
                        else
                        {
                            m_ResidualChromaticBlocks.push_back(pChromaticBlock);
                        }
                    }
                    if (m_SelectedChromaticBlocks.size() > 1)
                    {
                        m_SelectedChromaticBlocks.sort(SortChromaticBlockByCoreSize);
                    }
                }

                uint CCameraChromaticCalibration::Segmentation(bool RemoveSpeckles)
                {
                    list<CChromaticBlock*> PreSelectedChromaticBlocks = ExtractPreselectionChromaticBlocks();
                    if (RemoveSpeckles)
                    {
                        FilterSpeckles();
                    }
                    SelectChromaticBlocks(PreSelectedChromaticBlocks);
                    return m_SelectedChromaticBlocks.size();
                }

                void CCameraChromaticCalibration::DistanceTransformation()
                {
                    list<CChromaticBlock*>::iterator EndChromaticBlocks = m_SelectedChromaticBlocks.end();
                    for (list<CChromaticBlock*>::iterator ppChromaticBlock = m_SelectedChromaticBlocks.begin(); ppChromaticBlock != EndChromaticBlocks; ++ppChromaticBlock)
                    {
                        (*ppChromaticBlock)->DistanceTransformation(m_pOctoConnectivityDeltas, 8);
                    }
                }

                void CCameraChromaticCalibration::DisplaySelectedChromaticBlocks(bool DisplayResidualChromaticBlocksOn, uint MinimalSizeChromaticBlocks)
                {
                    m_pOutputDiscreteRGBImage->Set(m_Parameters.m_DisplayBackgroundColor);
                    DisplaySelectedChromaticBlocks(m_pOutputDiscreteRGBImage, m_SelectedChromaticBlocks);
                    if (DisplayResidualChromaticBlocksOn)
                    {
                        DisplayResidualChromaticBlocks(m_pOutputDiscreteRGBImage, m_ResidualChromaticBlocks, MinimalSizeChromaticBlocks);
                    }
                }

                void CCameraChromaticCalibration::DisplayDistanceTransformationChromaticBlocks(Visualization::CTristimulusColorMap* pRGBColorMap)
                {
                    m_pOutputDiscreteRGBImage->Set(m_Parameters.m_DisplayBackgroundColor);
                    const CDiscreteTristimulusPixel* pColorLookupTable = pRGBColorMap->GetColorLookupTable();
                    const real ColorLookupTableScaleFactor = pRGBColorMap->GetColorLookupTableScaleFactor();
                    list<CChromaticBlock*>::const_iterator EndChromaticBlocks = m_SelectedChromaticBlocks.end();
                    for (list<CChromaticBlock*>::const_iterator ppChromaticBlock = m_SelectedChromaticBlocks.begin(); ppChromaticBlock != EndChromaticBlocks; ++ppChromaticBlock)
                    {
                        CChromaticBlock* pChromaticBlock = *ppChromaticBlock;
                        if (pChromaticBlock->IsActive())
                        {
                            const list<CChromaticPixel*>& CoreChromaticPixels = pChromaticBlock->GetChromaticPixelsCore();
                            list<CChromaticPixel*>::const_iterator EndChromaticPixelsCore = CoreChromaticPixels.end();
                            real MaximalEdgeDistance = _REAL_ZERO_;
                            for (list<CChromaticPixel*>::const_iterator ppChromaticPixels = CoreChromaticPixels.begin(); ppChromaticPixels != EndChromaticPixelsCore; ++ppChromaticPixels)
                                if ((*ppChromaticPixels)->GetEdgeDistance() > MaximalEdgeDistance)
                                {
                                    MaximalEdgeDistance = (*ppChromaticPixels)->GetEdgeDistance();
                                }
                            const real Normalization = ColorLookupTableScaleFactor / MaximalEdgeDistance;
                            for (list<CChromaticPixel*>::const_iterator ppChromaticPixels = CoreChromaticPixels.begin(); ppChromaticPixels != EndChromaticPixelsCore; ++ppChromaticPixels)
                            {
                                m_pOutputDiscreteRGBImage->SetValueAt((*ppChromaticPixels)->GetLocation(), pColorLookupTable[RoundToInteger((*ppChromaticPixels)->GetEdgeDistance() * Normalization)]);
                            }
                        }
                    }
                }

                void CCameraChromaticCalibration::DisplaySelectedChromaticBlocks(TImage<CDiscreteTristimulusPixel>* pOutputDiscreteRGBImage, list<CChromaticBlock*>& ChromaticBlocks)
                {
                    list<CChromaticBlock*>::iterator EndChromaticBlocks = ChromaticBlocks.end();
                    for (list<CChromaticBlock*>::iterator ppChromaticBlock = ChromaticBlocks.begin(); ppChromaticBlock != EndChromaticBlocks; ++ppChromaticBlock)
                    {
                        CChromaticBlock* pChromaticBlock = *ppChromaticBlock;
                        if (pChromaticBlock->IsActive())
                        {
                            const CDiscreteTristimulusPixel CoreColor(pChromaticBlock->GetMeanRGBValue());
                            const CDiscreteTristimulusPixel NonRedundantBorderColor = CoreColor.GetComplement();
                            const list<CChromaticPixel*>& CoreChromaticPixels = pChromaticBlock->GetChromaticPixelsCore();
                            list<CChromaticPixel*>::const_iterator EndChromaticPixelsCore = CoreChromaticPixels.end();
                            for (list<CChromaticPixel*>::const_iterator ppChromaticPixels = CoreChromaticPixels.begin(); ppChromaticPixels != EndChromaticPixelsCore; ++ppChromaticPixels)
                            {
                                pOutputDiscreteRGBImage->SetValueAt((*ppChromaticPixels)->GetLocation(), CoreColor);
                            }
                            const list<CChromaticPixel*>& ChromaticPixelsNonRedundantBorder = pChromaticBlock->GetChromaticPixelsNonRedundantBorder();
                            EndChromaticPixelsCore = ChromaticPixelsNonRedundantBorder.end();
                            for (list<CChromaticPixel*>::const_iterator ppChromaticPixels = ChromaticPixelsNonRedundantBorder.begin(); ppChromaticPixels != EndChromaticPixelsCore; ++ppChromaticPixels)
                            {
                                pOutputDiscreteRGBImage->SetValueAt((*ppChromaticPixels)->GetLocation(), NonRedundantBorderColor);
                            }
                        }
                    }
                    for (list<CChromaticBlock*>::iterator ppChromaticBlock = ChromaticBlocks.begin(); ppChromaticBlock != EndChromaticBlocks; ++ppChromaticBlock)
                    {
                        CChromaticBlock* pChromaticBlock = *ppChromaticBlock;
                        if (pChromaticBlock->IsActive())
                        {
                            const list<CChromaticContour*>& Contoures = pChromaticBlock->GetChromaticContoures();
                            if (Contoures.size())
                            {
                                const list<CChromaticPixel*>& ChromaticPixels = Contoures.front()->GetChromaticPixels();
                                list<CChromaticPixel*>::const_iterator EndChromaticPixels = ChromaticPixels.end();
                                for (list<CChromaticPixel*>::const_iterator ppChromaticPixels = ChromaticPixels.begin(); ppChromaticPixels != EndChromaticPixels; ++ppChromaticPixels)
                                {
                                    pOutputDiscreteRGBImage->SetValueAt((*ppChromaticPixels)->GetLocation(), CDiscreteTristimulusPixel::s_RGB_Green);
                                }
                            }
                        }
                    }
                }

                void CCameraChromaticCalibration::DisplayResidualChromaticBlocks(TImage<CDiscreteTristimulusPixel>* pOutputDiscreteRGBImage, list<CChromaticBlock*>& ChromaticBlocks, uint MinimalSizeChromaticBlock)
                {
                    list<CChromaticBlock*>::iterator EndChromaticBlocks = ChromaticBlocks.end();
                    for (list<CChromaticBlock*>::iterator ppChromaticBlock = ChromaticBlocks.begin(); ppChromaticBlock != EndChromaticBlocks; ++ppChromaticBlock)
                    {
                        CChromaticBlock* pChromaticBlock = *ppChromaticBlock;
                        if (pChromaticBlock->IsActive())
                        {
                            const list<CChromaticPixel*>& ChromaticPixels = pChromaticBlock->GetChromaticPixels();
                            if (ChromaticPixels.size() > MinimalSizeChromaticBlock)
                            {
                                const CDiscreteTristimulusPixel Color(pChromaticBlock->GetMeanRGBValue());
                                list<CChromaticPixel*>::const_iterator EndChromaticPixels = ChromaticPixels.end();
                                for (list<CChromaticPixel*>::const_iterator ppChromaticPixels = ChromaticPixels.begin(); ppChromaticPixels != EndChromaticPixels; ++ppChromaticPixels)
                                {
                                    pOutputDiscreteRGBImage->SetValueAt((*ppChromaticPixels)->GetLocation(), Color);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
