/*
 * CCameraDevice.cpp
 *
 *  Created on: 05.04.2011
 *      Author: gonzalez
 */

#include "CameraDevice.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace Devices
            {
                CCameraDevice::CCameraDevice(const DeviceIdentifier CameraId) :
                    m_IsEnabled(false), m_CameraId(CameraId), m_OperationStatus(eUknownOperationStatus), m_Type(eUnknownImageType), m_ColorInterpolation(eImageColorInterpolationUnKnown), m_Resolution(eUnknownImageResolution), m_FrameRate(eUnKnownCapturingFrameRate)
                {
                }

                CCameraDevice::~CCameraDevice()
                    = default;

                CCameraDevice::OperationStatus CCameraDevice::GetOperationStatus() const
                {
                    return m_OperationStatus;
                }

                DeviceIdentifier CCameraDevice::GetDeviceIdentifier() const
                {
                    return m_CameraId;
                }

                CCameraDevice::ImageType CCameraDevice::GetImageType() const
                {
                    return m_Type;
                }

                CCameraDevice::ImageColorInterpolation CCameraDevice::GetImageColorInterpolation() const
                {
                    return m_ColorInterpolation;
                }

                CCameraDevice::ImageResolution CCameraDevice::GetImageResolution() const
                {
                    return m_Resolution;
                }

                uint CCameraDevice::GetImageWidth() const
                {
                    switch (m_Resolution)
                    {
                        case e320x240:
                            return 320;
                            break;
                        case e640x480:
                            return 640;
                            break;
                        default:
                            return 0;
                            break;
                    }
                    return 0;
                }

                uint CCameraDevice::GetImageHeight() const
                {
                    switch (m_Resolution)
                    {
                        case e320x240:
                            return 240;
                            break;
                        case e640x480:
                            return 480;
                            break;
                        default:
                            return 0;
                            break;
                    }
                    return 0;
                }

                CCameraDevice::CapturingFrameRate CCameraDevice::GetCapturingFrameRate() const
                {
                    return m_FrameRate;
                }

                CImageActiveZone* CCameraDevice::GetActiveZone()
                {
                    return &m_ImageActiveZone;
                }

                bool CCameraDevice::IsEnabled()
                {
                    return m_IsEnabled;
                }
            }
        }
    }
}
