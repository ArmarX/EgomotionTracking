/*
 * CCameraDevice.h
 *
 *  Created on: 05.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"
#include "../../../Foundation/DataTypes/BayerPattern.h"
#include "../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../Common/Image.h"
#include "../../Common/ImageActiveZone.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace Devices
            {
                class CCameraDevice
                {
                public:

                    enum OperationStatus
                    {
                        eUknownOperationStatus = -1, eClosed, eOpen, eError
                    };

                    enum OperationMode
                    {
                        eUknownOperationMode = -1, eAutomatic, eManual
                    };

                    enum ImageType
                    {
                        eUnknownImageType = -1, eDiscreteIntensity, eContinuousIntensity, eDiscreteRGB, eContinuousRGB
                    };

                    enum ImageResolution
                    {
                        eUnknownImageResolution = -1, e320x240, e640x480, e800x600, e768x576, e1024x768, e1280x960, e1600x1200
                    };

                    enum CapturingFrameRate
                    {
                        eUnKnownCapturingFrameRate = -1, e1_875, e3_75, e7_5, e15, e30, e60
                    };

                    enum ImageColorInterpolation
                    {
                        eImageColorInterpolationUnKnown = -1, eNone, eNearestNeighbor, eEdgeSensing, eApadtive, eBilineal, eBicubic
                    };

                    CCameraDevice(const DeviceIdentifier CameraId);
                    virtual ~CCameraDevice();
                    OperationStatus GetOperationStatus() const;
                    DeviceIdentifier GetDeviceIdentifier() const;
                    ImageType GetImageType() const;
                    ImageColorInterpolation GetImageColorInterpolation() const;
                    ImageResolution GetImageResolution() const;
                    uint GetImageWidth() const;
                    uint GetImageHeight() const;
                    CapturingFrameRate GetCapturingFrameRate() const;
                    CImageActiveZone* GetActiveZone();

                    virtual  real GetMinimalExposureValue() = 0;
                    virtual  real GetMaximalExposureValue() = 0;
                    virtual  real GetStepExposureValue() = 0;
                    virtual  real GetExposureValue() = 0;
                    virtual  real GetExposureTime() = 0;
                    virtual  OperationMode GetExposureMode() = 0;
                    virtual  bool SetExposureMode(const OperationMode ExposureOperationMode) = 0;
                    virtual  bool SetExposureValue(const real ExposureValue) = 0;

                    virtual  real GetMinimalWhiteBalanceRedValue() = 0;
                    virtual  real GetMaximalWhiteBalanceRedValue() = 0;
                    virtual  real GetStepWhiteBalanceRedValue() = 0;
                    virtual  bool SetStepWhiteBalanceRedValue(const real RedValue) = 0;

                    virtual  real GetMinimalWhiteBalanceBlueValue() = 0;
                    virtual  real GetMaximalWhiteBalanceBlueValue() = 0;
                    virtual  real GetStepWhiteBalanceBlueValue() = 0;
                    virtual  bool SetStepWhiteBalanceBlueValue(const real BlueValue) = 0;

                    virtual  OperationMode GetWhiteBalanceMode() = 0;
                    virtual  bool SetWhiteBalanceMode(const OperationMode WhiteBalanceOperationMode) = 0;

                    virtual  std::vector<ImageType> GetSupportedImageTypes() = 0;
                    virtual  std::vector<ImageColorInterpolation> GetSupportedImageColorInterpolations() = 0;
                    virtual  std::vector<ImageResolution> GetSupportedImageResolutions() = 0;
                    virtual  std::vector<CapturingFrameRate> GetSupportedCapturingFrameRates() = 0;
                    virtual  bool IsSupportedConfiguration(const ImageType Type, const ImageColorInterpolation ColorInterpolation, const ImageResolution Resolution, const CapturingFrameRate FrameRate) = 0;

                    virtual  bool OpenCamera(const ImageType Type, const ImageColorInterpolation ColorInterpolation, const ImageResolution Resolution, const CapturingFrameRate FrameRate) = 0;
                    virtual  bool CloseCamera() = 0;

                    virtual  bool CaptureImage(TImage<byte>* pDiscreteIntensityImage) = 0;
                    virtual  bool CaptureImage(TImage<real>* pContinuousIntensityImage) = 0;
                    virtual  bool CaptureImage(TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage) = 0;
                    virtual  bool CaptureImage(TImage<CContinuousTristimulusPixel>* pContinuousRGBImage) = 0;

                    virtual  std::string GetDeviceName() = 0;

                    virtual  BayerPatternType GetBayerPatternType() = 0;

                    bool IsEnabled();

                protected:

                    bool m_IsEnabled;
                    DeviceIdentifier m_CameraId;
                    OperationStatus m_OperationStatus;
                    ImageType m_Type;
                    ImageColorInterpolation m_ColorInterpolation;
                    ImageResolution m_Resolution;
                    CapturingFrameRate m_FrameRate;
                    CImageActiveZone m_ImageActiveZone;
                };
            }
        }
    }
}

