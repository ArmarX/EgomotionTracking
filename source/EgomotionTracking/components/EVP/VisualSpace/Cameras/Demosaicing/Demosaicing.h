/*
 * Demosaicing.h
 *
 *  Created on: 30.05.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../../GlobalSettings.h"
#include "../../../Foundation/Miscellaneous/IdentifiableInstance.h"
#include "../../../Visualization/Miscellaneous/TristimulusColorMap.h"
#include "../../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../../Foundation/DataTypes/BayerPattern.h"
#include "../../../Foundation/DataTypes/ColorSpaces.h"
#include "../../../Foundation/Mathematics/1D/Common1D.h"
#include "../../../Foundation/Mathematics/1D/ExponentialLookUpTable.h"
#include "../../../Foundation/Time/TimeLogger.h"
#include "../../Common/Image.h"
#include "../../Common/ImageActiveZone.h"
#include "../../Kernels/ConvolutionGaussianKernel2D.h"

#define _DEMOSAICING_DEFAULT_GAUSSIAN_STANDARD_DEVIATION_ real(0.3333)
#define _DEMOSAICING_DEFAULT_ADAPTIVE_SPACE_STANDARD_DEVIATION_ real(0.6666)
#define _DEMOSAICING_DEFAULT_ADAPTIVE_RANGE_STANDARD_DEVIATION_ real(32.0)
#define _DEMOSAICING_DEFAULT_LUMINANCE_EDGE_CONTRIBUTION_ real(16.0)
#define _DEMOSAICING_DEFAULT_CHROMATIC_EDGE_CONTRIBUTION_ _REAL_ONE_/real(16.0)

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace Demosaicing
            {
                class CDemosaicing: public CIdentifiableInstance
                {

                    IDENTIFIABLE

#ifdef _DEMOSAICING_USE_LOOKUP_TABLE_
                    DECLARE_EXPONENTIAL_LOOKUP_TABLE
#endif

                public:

                    enum DemosaicingMethod
                    {
                        eBilinear, eDirectionalGradient, eGaussian, eAdaptive
                    };

                    CDemosaicing(const BayerPatternType BayerPattern, CImageActiveZone* pInputActiveZone, const TImage<real>* pSourceContinuousBayerImage);
                    CDemosaicing(const BayerPatternType BayerPattern, CImageActiveZone* pInputActiveZone, const TImage<byte>* pSourceDiscreteBayerImage);

                    ~CDemosaicing() override;

                    bool Demosaicing(const DemosaicingMethod Method);

                    bool SetBayerPattern(const BayerPatternType BayerPattern);
                    BayerPatternType GetBayerPattern() const;

                    bool SetLuminanceEdgeContribution(const real LuminanceEdgeContribution);
                    real GetLuminanceEdgeContribution() const;

                    bool SetChromaticEdgeContribution(const real ChromaticEdgeContribution);
                    real GetChromaticEdgeContribution() const;

                    bool SetGaussianStandardDeviation(const real GaussianStandardDeviation);
                    real GetGaussianStandardDeviation() const;

                    bool SetAdaptiveSpaceStandardDeviation(const real AdaptiveSpaceStandardDeviation);
                    real GetAdaptiveSpaceStandardDeviation() const;

                    bool SetAdaptiveRangeStandardDeviation(const real AdaptiveRangeStandardDeviation);
                    real GetAdaptiveRangeStandardDeviation() const;

                    CImageActiveZone* GetOutputActiveZone(const DemosaicingMethod Method);
                    const TImage<CContinuousTristimulusPixel>* GetOutputImage();

                protected:

                    inline  bool ContinuousGaussianDemosaicing();
                    inline  bool DiscreteGaussianDemosaicing();
                    inline  bool ContinuousBilinearDemosaicing();
                    inline  bool DiscreteBilinearDemosaicing();
                    inline  bool ContinuousDirectionalWeightedGradientDemosaicing();
                    inline  bool DiscreteDirectionalWeightedGradientDemosaicing();
                    inline  bool ContinuousAdaptiveDemosaicing();
                    inline  real GradientWeightingKernel(const real Gradient) const;

                    bool m_IsEnabled;
                    BayerPatternType m_BayerPattern;
                    CImageActiveZone* m_pInputActiveZone;
                    CImageActiveZone m_OutputGaussianActiveZone;
                    CImageActiveZone m_OutputBilinearActiveZone;
                    CImageActiveZone m_OutputDirectionalWeightedGradientActiveZone;
                    CImageActiveZone m_OutputAdaptiveActiveZone;
                    real m_LuminanceEdgeContribution;
                    real m_ChromaticEdgeContribution;
                    real m_AdaptiveRangeStandardDeviation;
                    real m_AdaptiveRangeExponentFactor;
                    const TImage<real>* m_pExternSourceContinuousBayerImage;
                    const TImage<byte>* m_pExternSourceDiscreteBayerImage;
                    TImage<real>* m_pInternSourceContinuousBayerImage;
                    TImage<byte>* m_pInternSourceDiscreteBayerImage;
                    TImage<CContinuousTristimulusPixel>* m_pContinuousRGBImage;
                    TImage<CContinuousTristimulusPixel>* m_pAdaptiveContinuousRGBImage;
                    Kernels::CConvolutionGaussianKernel2D* m_pGaussianDemosaicingKernel;
                    Kernels::CConvolutionGaussianKernel2D* m_pAdaptiveKernel;
                    ChromaticSpaceRGB::ChannelContent m_BayerPatternMap[2][2];
                };
            }
        }
    }
}

