/*
 * Demosaicing.cpp
 *
 *  Created on: 30.05.2011
 *      Author: gonzalez
 */

#include "Demosaicing.h"

using namespace EVP::VisualSpace::Kernels;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace Demosaicing
            {
                IDENTIFIABLE_INITIALIZATION(CDemosaicing)
#ifdef _DEMOSAICING_USE_LOOKUP_TABLE_
                INITIALIZE_EXPONENTIAL_LOOKUP_TABLE(CDemosaicing)
#endif

                CDemosaicing::CDemosaicing(const BayerPatternType BayerPattern, CImageActiveZone* pInputActiveZone, const TImage<real>* pSourceContinuousBayerImage) :
                    IDENTIFIABLE_CONTRUCTOR(CDemosaicing), m_IsEnabled(false), m_BayerPattern(BayerPattern), m_pInputActiveZone(pInputActiveZone), m_LuminanceEdgeContribution(_DEMOSAICING_DEFAULT_LUMINANCE_EDGE_CONTRIBUTION_), m_ChromaticEdgeContribution(_DEMOSAICING_DEFAULT_CHROMATIC_EDGE_CONTRIBUTION_), m_AdaptiveRangeStandardDeviation(_DEMOSAICING_DEFAULT_ADAPTIVE_RANGE_STANDARD_DEVIATION_), m_AdaptiveRangeExponentFactor(Mathematics::_1D::NormalDistribution::DetermineExponentFactor(_DEMOSAICING_DEFAULT_ADAPTIVE_RANGE_STANDARD_DEVIATION_)), m_pExternSourceContinuousBayerImage(pSourceContinuousBayerImage), m_pExternSourceDiscreteBayerImage(nullptr), m_pInternSourceContinuousBayerImage(nullptr), m_pInternSourceDiscreteBayerImage(nullptr), m_pContinuousRGBImage(nullptr), m_pAdaptiveContinuousRGBImage(nullptr), m_pGaussianDemosaicingKernel(nullptr), m_pAdaptiveKernel(nullptr)
                {
                    m_IsEnabled = (m_BayerPattern != eUnknownBayerPattern) && m_pInputActiveZone && m_pExternSourceContinuousBayerImage && m_pExternSourceContinuousBayerImage->IsValid();
                    if (m_IsEnabled)
                    {
                        SetBayerPattern(m_BayerPattern);
                        const CImageSize& Size = m_pExternSourceContinuousBayerImage->GetSize();
                        m_pInternSourceContinuousBayerImage = new TImage<real> (Size);
                        m_pContinuousRGBImage = new TImage<CContinuousTristimulusPixel> (Size);
                        m_pAdaptiveContinuousRGBImage = new TImage<CContinuousTristimulusPixel> (Size);
                        m_pAdaptiveKernel = new CConvolutionGaussianKernel2D(_DEMOSAICING_DEFAULT_ADAPTIVE_SPACE_STANDARD_DEVIATION_);
                        m_pAdaptiveKernel->ScaleMinimal();
                        m_pGaussianDemosaicingKernel = new CConvolutionGaussianKernel2D(_DEMOSAICING_DEFAULT_GAUSSIAN_STANDARD_DEVIATION_);
                        m_pGaussianDemosaicingKernel->ScaleMinimal();
                        m_OutputGaussianActiveZone.SetConnection(m_pInputActiveZone, m_pGaussianDemosaicingKernel, TConvolutionKernel2D<real>::GetKernelOffsetActiveZoneUpdate);
                        m_OutputBilinearActiveZone.SetConnection(m_pInputActiveZone, 1);
                        m_OutputDirectionalWeightedGradientActiveZone.SetConnection(m_pInputActiveZone, 2);
                        m_OutputAdaptiveActiveZone.SetConnection(&m_OutputDirectionalWeightedGradientActiveZone, m_pAdaptiveKernel, TConvolutionKernel2D<real>::GetKernelOffsetActiveZoneUpdate);

#ifdef _DEMOSAICING_USE_LOOKUP_TABLE_
                        LOAD_EXPONENTIAL_LOOKUP_TABLE(CDemosaicing)
#endif
                    }
                }

                CDemosaicing::CDemosaicing(const BayerPatternType BayerPattern, CImageActiveZone* pInputActiveZone, const TImage<byte>* pSourceDiscreteBayerImage) :
                    IDENTIFIABLE_CONTRUCTOR(CDemosaicing), m_IsEnabled(false), m_BayerPattern(BayerPattern), m_pInputActiveZone(pInputActiveZone), m_LuminanceEdgeContribution(_DEMOSAICING_DEFAULT_LUMINANCE_EDGE_CONTRIBUTION_), m_ChromaticEdgeContribution(_DEMOSAICING_DEFAULT_CHROMATIC_EDGE_CONTRIBUTION_), m_AdaptiveRangeStandardDeviation(_DEMOSAICING_DEFAULT_ADAPTIVE_RANGE_STANDARD_DEVIATION_), m_AdaptiveRangeExponentFactor(Mathematics::_1D::NormalDistribution::DetermineExponentFactor(_DEMOSAICING_DEFAULT_ADAPTIVE_RANGE_STANDARD_DEVIATION_)), m_pExternSourceContinuousBayerImage(nullptr), m_pExternSourceDiscreteBayerImage(pSourceDiscreteBayerImage), m_pInternSourceContinuousBayerImage(nullptr), m_pInternSourceDiscreteBayerImage(nullptr), m_pContinuousRGBImage(nullptr), m_pAdaptiveContinuousRGBImage(nullptr), m_pGaussianDemosaicingKernel(nullptr), m_pAdaptiveKernel(nullptr)
                {
                    m_IsEnabled = (m_BayerPattern != eUnknownBayerPattern) && m_pInputActiveZone && m_pExternSourceDiscreteBayerImage && m_pExternSourceDiscreteBayerImage->IsValid();
                    if (m_IsEnabled)
                    {
                        SetBayerPattern(m_BayerPattern);
                        const CImageSize& Size = m_pExternSourceDiscreteBayerImage->GetSize();
                        m_pInternSourceDiscreteBayerImage = new TImage<byte> (Size);
                        m_pContinuousRGBImage = new TImage<CContinuousTristimulusPixel> (Size);
                        m_pAdaptiveContinuousRGBImage = new TImage<CContinuousTristimulusPixel> (Size);
                        m_pAdaptiveKernel = new CConvolutionGaussianKernel2D(_DEMOSAICING_DEFAULT_ADAPTIVE_SPACE_STANDARD_DEVIATION_);
                        m_pAdaptiveKernel->ScaleMinimal();
                        m_pGaussianDemosaicingKernel = new CConvolutionGaussianKernel2D(_DEMOSAICING_DEFAULT_GAUSSIAN_STANDARD_DEVIATION_);
                        m_pGaussianDemosaicingKernel->ScaleMinimal();
                        m_OutputGaussianActiveZone.SetConnection(m_pInputActiveZone, m_pGaussianDemosaicingKernel, TConvolutionKernel2D<real>::GetKernelOffsetActiveZoneUpdate);
                        m_OutputBilinearActiveZone.SetConnection(m_pInputActiveZone, 1);
                        m_OutputDirectionalWeightedGradientActiveZone.SetConnection(m_pInputActiveZone, 2);
                        m_OutputAdaptiveActiveZone.SetConnection(&m_OutputDirectionalWeightedGradientActiveZone, m_pAdaptiveKernel, TConvolutionKernel2D<real>::GetKernelOffsetActiveZoneUpdate);

#ifdef _DEMOSAICING_USE_LOOKUP_TABLE_
                        LOAD_EXPONENTIAL_LOOKUP_TABLE(CDemosaicing)
#endif
                    }
                }

                CDemosaicing::~CDemosaicing()
                {
                    if (m_IsEnabled)
                    {
                        m_OutputAdaptiveActiveZone.Disconnect();
                        m_OutputDirectionalWeightedGradientActiveZone.Disconnect();
                        m_OutputBilinearActiveZone.Disconnect();
                        m_OutputGaussianActiveZone.Disconnect();
                        RELEASE_OBJECT(m_pInternSourceDiscreteBayerImage)
                        RELEASE_OBJECT(m_pInternSourceContinuousBayerImage)
                        RELEASE_OBJECT(m_pContinuousRGBImage)
                        RELEASE_OBJECT(m_pAdaptiveContinuousRGBImage)
                        RELEASE_OBJECT(m_pAdaptiveKernel)
                        RELEASE_OBJECT(m_pGaussianDemosaicingKernel)
                    }
                }

                bool CDemosaicing::Demosaicing(const DemosaicingMethod Method)
                {
                    if (m_IsEnabled)
                    {
                        if (m_pExternSourceContinuousBayerImage)
                        {
                            m_pInternSourceContinuousBayerImage->Copy(m_pExternSourceContinuousBayerImage);
                            switch (Method)
                            {
                                case eBilinear:

                                    TIME_LOGGER_BEGIN("ContinuousBilinearDemosaicing")

                                    ContinuousBilinearDemosaicing();

                                    TIME_LOGGER_END("ContinuousBilinearDemosaicing")

                                    break;
                                case eDirectionalGradient:

                                    TIME_LOGGER_BEGIN("ContinuousDirectionalWeightedGradientDemosaicing")

                                    ContinuousDirectionalWeightedGradientDemosaicing();

                                    TIME_LOGGER_END("ContinuousDirectionalWeightedGradientDemosaicing")

                                    break;
                                case eGaussian:

                                    TIME_LOGGER_BEGIN("ContinuousGaussianDemosaicing")

                                    ContinuousGaussianDemosaicing();

                                    TIME_LOGGER_END("ContinuousGaussianDemosaicing")

                                    break;
                                case eAdaptive:

                                    TIME_LOGGER_BEGIN("ContinuousAdaptiveDemosaicing 1/2")

                                    ContinuousDirectionalWeightedGradientDemosaicing();

                                    TIME_LOGGER_END("ContinuousAdaptiveDemosaicing 1/2")

                                    TIME_LOGGER_BEGIN("ContinuousAdaptiveDemosaicing 2/2")

                                    ContinuousAdaptiveDemosaicing();

                                    TIME_LOGGER_END("ContinuousAdaptiveDemosaicing 2/2")

                                    break;
                            }
                        }
                        else
                        {
                            m_pInternSourceDiscreteBayerImage->Copy(m_pExternSourceDiscreteBayerImage);
                            switch (Method)
                            {
                                case eBilinear:

                                    TIME_LOGGER_BEGIN("DiscreteBilinearDemosaicing")

                                    DiscreteBilinearDemosaicing();

                                    TIME_LOGGER_END("DiscreteBilinearDemosaicing")
                                    break;
                                case eDirectionalGradient:

                                    TIME_LOGGER_BEGIN("DiscreteDirectionalWeightedGradientDemosaicing")

                                    DiscreteDirectionalWeightedGradientDemosaicing();

                                    TIME_LOGGER_END("DiscreteDirectionalWeightedGradientDemosaicing")
                                    break;
                                case eGaussian:

                                    TIME_LOGGER_BEGIN("DiscreteGaussianDemosaicing")

                                    DiscreteGaussianDemosaicing();

                                    TIME_LOGGER_END("DiscreteGaussianDemosaicing")

                                    break;
                                case eAdaptive:

                                    TIME_LOGGER_BEGIN("ContinuousAdaptiveDemosaicing 1/2")

                                    DiscreteDirectionalWeightedGradientDemosaicing();

                                    TIME_LOGGER_END("ContinuousAdaptiveDemosaicing 1/2")

                                    TIME_LOGGER_BEGIN("ContinuousAdaptiveDemosaicing 2/2")

                                    ContinuousAdaptiveDemosaicing();

                                    TIME_LOGGER_END("ContinuousAdaptiveDemosaicing 2/2")

                                    break;
                            }
                        }
                    }
                    return false;
                }

                bool CDemosaicing::SetBayerPattern(const BayerPatternType BayerPattern)
                {
                    m_BayerPattern = BayerPattern;
                    m_IsEnabled = LoadBayerPatternMap(m_BayerPattern, m_BayerPatternMap);
                    /* TODO VERFY
                     if (m_BayerPattern == eUnknownBayerPattern)
                     {
                     m_IsEnabled = false;
                     for (coordinate y = 0, i = 0; y < 2; ++y)
                     for (coordinate x = 0; x < 2; ++x, i += 4)
                     m_BayerPatternMap[y][x] = ChromaticSpaceRGB::eUnknownChannelContent;
                     }
                     else
                     {
                     for (coordinate y = 0, i = 0; y < 2; ++y)
                     for (coordinate x = 0; x < 2; ++x, i += 4)
                     m_BayerPatternMap[y][x] = ChromaticSpaceRGB::ChannelContent((m_BayerPattern & (0x3 << i)) >> i);
                     }
                     */
                    return m_IsEnabled;
                }

                BayerPatternType CDemosaicing::GetBayerPattern() const
                {
                    return m_BayerPattern;
                }

                bool CDemosaicing::SetLuminanceEdgeContribution(const real LuminanceEdgeContribution)
                {
                    if (LuminanceEdgeContribution > _REAL_ZERO_)
                    {
                        m_LuminanceEdgeContribution = LuminanceEdgeContribution;
                        return true;
                    }
                    return false;
                }

                real CDemosaicing::GetLuminanceEdgeContribution() const
                {
                    return m_LuminanceEdgeContribution;
                }

                bool CDemosaicing::SetChromaticEdgeContribution(const real ChromaticEdgeContribution)
                {
                    if (ChromaticEdgeContribution > _REAL_ZERO_)
                    {
                        m_ChromaticEdgeContribution = ChromaticEdgeContribution;
                        return true;
                    }
                    return false;
                }

                real CDemosaicing::GetChromaticEdgeContribution() const
                {
                    return m_ChromaticEdgeContribution;
                }

                bool CDemosaicing::SetGaussianStandardDeviation(const real GaussianStandardDeviation)
                {
                    if (m_IsEnabled && m_pGaussianDemosaicingKernel->SetStandardDeviation(GaussianStandardDeviation))
                    {
                        m_pGaussianDemosaicingKernel->ScaleMinimal();
                        m_OutputGaussianActiveZone.UpdateConnection();
                        m_pContinuousRGBImage->Clear();
                        return true;
                    }
                    return false;
                }

                real CDemosaicing::GetGaussianStandardDeviation() const
                {
                    if (m_IsEnabled)
                    {
                        return m_pGaussianDemosaicingKernel->GetStandardDeviation();
                    }
                    return _REAL_ZERO_;
                }

                bool CDemosaicing::SetAdaptiveSpaceStandardDeviation(const real AdaptiveSpaceStandardDeviation)
                {
                    if (m_IsEnabled && m_pAdaptiveKernel->SetStandardDeviation(AdaptiveSpaceStandardDeviation))
                    {
                        m_pAdaptiveKernel->ScaleMinimal();
                        m_OutputAdaptiveActiveZone.UpdateConnection();
                        m_pAdaptiveContinuousRGBImage->Clear();
                        return true;
                    }
                    return false;
                }

                real CDemosaicing::GetAdaptiveSpaceStandardDeviation() const
                {
                    if (m_IsEnabled)
                    {
                        return m_pAdaptiveKernel->GetStandardDeviation();
                    }
                    return _REAL_ZERO_;
                }

                bool CDemosaicing::SetAdaptiveRangeStandardDeviation(const real AdaptiveRangeStandardDeviation)
                {
                    if (m_IsEnabled && (AdaptiveRangeStandardDeviation > _REAL_ZERO_))
                    {
                        m_AdaptiveRangeStandardDeviation = AdaptiveRangeStandardDeviation;
                        m_AdaptiveRangeExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(m_AdaptiveRangeStandardDeviation);
                        return true;
                    }
                    return false;
                }

                real CDemosaicing::GetAdaptiveRangeStandardDeviation() const
                {
                    if (m_IsEnabled)
                    {
                        return m_AdaptiveRangeStandardDeviation;
                    }
                    return _REAL_ZERO_;
                }

                CImageActiveZone* CDemosaicing::GetOutputActiveZone(const DemosaicingMethod Method)
                {
                    if (m_IsEnabled)
                        switch (Method)
                        {
                            case eGaussian:
                                return &m_OutputGaussianActiveZone;
                                break;
                            case eBilinear:
                                return &m_OutputBilinearActiveZone;
                                break;
                            case eDirectionalGradient:
                                return &m_OutputDirectionalWeightedGradientActiveZone;
                                break;
                            case eAdaptive:
                                return &m_OutputAdaptiveActiveZone;
                                break;
                        }
                    return nullptr;
                }

                const TImage<CContinuousTristimulusPixel>* CDemosaicing::GetOutputImage()
                {
                    if (m_IsEnabled)
                    {
                        return const_cast<const TImage<CContinuousTristimulusPixel>*>(m_pContinuousRGBImage);
                    }
                    return nullptr;
                }

                bool CDemosaicing::ContinuousGaussianDemosaicing()
                {
                    if (m_OutputGaussianActiveZone.IsEnabled())
                    {
                        const coordinate SX0 = m_pInputActiveZone->GetX0();
                        const coordinate SY0 = m_pInputActiveZone->GetY0();
                        const coordinate SX1 = SX0 + m_pGaussianDemosaicingKernel->GetWidth();
                        const coordinate SY1 = SY0 + m_pGaussianDemosaicingKernel->GetWidth();
                        const coordinate DX0 = m_OutputGaussianActiveZone.GetX0();
                        const coordinate DY0 = m_OutputGaussianActiveZone.GetY0();
                        const coordinate DX1 = m_OutputGaussianActiveZone.GetX1();
                        const coordinate DY1 = m_OutputGaussianActiveZone.GetY1();
                        const coordinate Width = m_OutputGaussianActiveZone.GetWidth();
                        const real* pBaseKernel = m_pGaussianDemosaicingKernel->GetKernelReadOnlyBuffer();
                        const real* pBaseInputPixel = m_pInternSourceContinuousBayerImage->GetReadOnlyBufferAt(SX0, SY0);
                        CContinuousTristimulusPixel* pBaseOutputPixel = m_pContinuousRGBImage->GetWritableBufferAt(DX0, DY0);
                        real ChannelsAccumulators[6] = { _REAL_ZERO_ };
                        real* KernelAccumulators = ChannelsAccumulators + 3;
                        for (coordinate y = DY0, y0 = SY0, y1 = SY1; y < DY1; ++y, ++y0, ++y1, pBaseOutputPixel += Width, pBaseInputPixel += Width)
                        {
                            CContinuousTristimulusPixel* pOutputPixel = pBaseOutputPixel;
                            const real* pExternInputPixel = pBaseInputPixel;
                            for (coordinate x = DX0, x0 = SX0, x1 = SX1; x < DX1; ++x, ++x0, ++x1, ++pOutputPixel, ++pExternInputPixel)
                            {
                                const real* pKernel = pBaseKernel;
                                const real* pInternBaseInputPixel = pExternInputPixel;
                                memset(ChannelsAccumulators, 0, sizeof(real) * 6);
                                for (coordinate ys = y0; ys < y1; ++ys, pInternBaseInputPixel += Width)
                                {
                                    const real* pInputPixel = pInternBaseInputPixel;
                                    for (coordinate xs = x0; xs < x1; ++xs, ++pInputPixel, ++pKernel)
                                    {
                                        const ChromaticSpaceRGB::ChannelContent CurrentChannel = m_BayerPatternMap[ys & 0x1][xs & 0x1];
                                        ChannelsAccumulators[CurrentChannel] += *pInputPixel * *pKernel;
                                        KernelAccumulators[CurrentChannel] += *pKernel;
                                    }
                                }
                                pOutputPixel->SetValue(ChannelsAccumulators, KernelAccumulators);
                            }
                        }
                        return true;
                    }
                    return false;
                }

                bool CDemosaicing::DiscreteGaussianDemosaicing()
                {
                    if (m_OutputGaussianActiveZone.IsEnabled())
                    {
                        const coordinate SX0 = m_pInputActiveZone->GetX0();
                        const coordinate SY0 = m_pInputActiveZone->GetY0();
                        const coordinate SX1 = SX0 + m_pGaussianDemosaicingKernel->GetWidth();
                        const coordinate SY1 = SY0 + m_pGaussianDemosaicingKernel->GetWidth();
                        const coordinate DX0 = m_OutputGaussianActiveZone.GetX0();
                        const coordinate DY0 = m_OutputGaussianActiveZone.GetY0();
                        const coordinate DX1 = m_OutputGaussianActiveZone.GetX1();
                        const coordinate DY1 = m_OutputGaussianActiveZone.GetY1();
                        const coordinate Width = m_OutputGaussianActiveZone.GetWidth();
                        const real* pBaseKernel = m_pGaussianDemosaicingKernel->GetKernelReadOnlyBuffer();
                        const byte* pBaseInputPixel = m_pInternSourceDiscreteBayerImage->GetReadOnlyBufferAt(SX0, SY0);
                        CContinuousTristimulusPixel* pBaseOutputPixel = m_pContinuousRGBImage->GetWritableBufferAt(DX0, DY0);
                        real ChannelsAccumulators[6] = { _REAL_ZERO_ };
                        real* KernelAccumulators = ChannelsAccumulators + 3;
                        for (coordinate y = DY0, y0 = SY0, y1 = SY1; y < DY1; ++y, ++y0, ++y1, pBaseOutputPixel += Width, pBaseInputPixel += Width)
                        {
                            CContinuousTristimulusPixel* pOutputPixel = pBaseOutputPixel;
                            const byte* pExternInputPixel = pBaseInputPixel;
                            for (coordinate x = DX0, x0 = SX0, x1 = SX1; x < DX1; ++x, ++x0, ++x1, ++pOutputPixel, ++pExternInputPixel)
                            {
                                const real* pKernel = pBaseKernel;
                                const byte* pInternBaseInputPixel = pExternInputPixel;
                                memset(ChannelsAccumulators, 0, sizeof(real) * 6);
                                for (coordinate ys = y0; ys < y1; ++ys, pInternBaseInputPixel += Width)
                                {
                                    const byte* pInputPixel = pInternBaseInputPixel;
                                    for (coordinate xs = x0; xs < x1; ++xs, ++pInputPixel, ++pKernel)
                                    {
                                        const ChromaticSpaceRGB::ChannelContent CurrentChannel = m_BayerPatternMap[ys & 0x1][xs & 0x1];
                                        ChannelsAccumulators[CurrentChannel] += real(*pInputPixel) * *pKernel;
                                        KernelAccumulators[CurrentChannel] += *pKernel;
                                    }
                                }
                                pOutputPixel->SetValue(ChannelsAccumulators, KernelAccumulators);
                            }
                        }
                        return true;
                    }
                    return false;
                }

                bool CDemosaicing::ContinuousBilinearDemosaicing()
                {
                    if (m_OutputBilinearActiveZone.IsEnabled())
                    {
                        const coordinate Width = m_OutputBilinearActiveZone.GetWidth();
                        const coordinate X0 = m_OutputBilinearActiveZone.GetX0();
                        const coordinate Y0 = m_OutputBilinearActiveZone.GetY0();
                        const coordinate X1 = m_OutputBilinearActiveZone.GetX1();
                        const coordinate Y1 = m_OutputBilinearActiveZone.GetY1();
                        const real* pBaseBayerPatternPixel = m_pInternSourceContinuousBayerImage->GetReadOnlyBufferLineAt(Y0);
                        CContinuousTristimulusPixel* pBaseRGBPixel = m_pContinuousRGBImage->GetWritableBufferLineAt(Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseRGBPixel += Width, pBaseBayerPatternPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMap[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 : X0 + 1;
                            CContinuousTristimulusPixel* pRGBPixel = pBaseRGBPixel + XBase;
                            const real* pPixel = pBaseBayerPatternPixel + XBase;
                            if (m_BayerPatternMap[y & 0x1][(XBase - 1) & 0x1] == ChromaticSpaceRGB::eRed)
                                for (coordinate x = XBase; x < X1; x += 2, pRGBPixel += 2, pPixel += 2)
                                {
                                    pRGBPixel->SetValue((pPixel[-1] + pPixel[1]) * _REAL_HALF_, *pPixel, (pPixel[-Width] + pPixel[Width]) * _REAL_HALF_);
                                }
                            else
                                for (coordinate x = XBase; x < X1; x += 2, pRGBPixel += 2, pPixel += 2)
                                {
                                    pRGBPixel->SetValue((pPixel[-Width] + pPixel[Width]) * _REAL_HALF_, *pPixel, (pPixel[-1] + pPixel[1]) * _REAL_HALF_);
                                }
                        }
                        pBaseBayerPatternPixel = m_pInternSourceContinuousBayerImage->GetReadOnlyBufferLineAt(Y0);
                        pBaseRGBPixel = m_pContinuousRGBImage->GetWritableBufferLineAt(Y0);
                        const coordinate NE = 1 - Width, NW = -Width - 1, SW = Width - 1, SE = Width + 1;
                        for (coordinate y = Y0; y < Y1; ++y, pBaseRGBPixel += Width, pBaseBayerPatternPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMap[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 + 1 : X0;
                            const real* pPixel = pBaseBayerPatternPixel + XBase;
                            CContinuousTristimulusPixel* pRGBPixel = pBaseRGBPixel + XBase;
                            if (m_BayerPatternMap[y & 0x1][XBase & 0x1] == ChromaticSpaceRGB::eRed)
                                for (coordinate x = XBase; x < X1; x += 2, pRGBPixel += 2, pPixel += 2)
                                {
                                    pRGBPixel->SetValue(*pPixel, (pPixel[1] + pPixel[-1] + pPixel[-Width] + pPixel[Width]) * _REAL_FOURTH_, (pPixel[NE] + pPixel[NW] + pPixel[SW] + pPixel[SE]) * _REAL_FOURTH_);
                                }
                            else
                                for (coordinate x = XBase; x < X1; x += 2, pRGBPixel += 2, pPixel += 2)
                                {
                                    pRGBPixel->SetValue((pPixel[NE] + pPixel[NW] + pPixel[SW] + pPixel[SE]) * _REAL_FOURTH_, (pPixel[1] + pPixel[-1] + pPixel[-Width] + pPixel[Width]) * _REAL_FOURTH_, *pPixel);
                                }
                        }
                        return true;
                    }
                    return false;
                }

                bool CDemosaicing::DiscreteBilinearDemosaicing()
                {
                    if (m_OutputBilinearActiveZone.IsEnabled())
                    {
                        const coordinate Width = m_OutputBilinearActiveZone.GetWidth();
                        const coordinate X0 = m_OutputBilinearActiveZone.GetX0();
                        const coordinate Y0 = m_OutputBilinearActiveZone.GetY0();
                        const coordinate X1 = m_OutputBilinearActiveZone.GetX1();
                        const coordinate Y1 = m_OutputBilinearActiveZone.GetY1();
                        const byte* pBaseBayerPatternPixel = m_pInternSourceDiscreteBayerImage->GetReadOnlyBufferLineAt(Y0);
                        CContinuousTristimulusPixel* pBaseRGBPixel = m_pContinuousRGBImage->GetWritableBufferLineAt(Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseRGBPixel += Width, pBaseBayerPatternPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMap[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 : X0 + 1;
                            const byte* pPixel = pBaseBayerPatternPixel + XBase;
                            CContinuousTristimulusPixel* pRGBPixel = pBaseRGBPixel + XBase;
                            if (m_BayerPatternMap[y & 0x1][(XBase - 1) & 0x1] == ChromaticSpaceRGB::eRed)
                                for (coordinate x = XBase; x < X1; x += 2, pRGBPixel += 2, pPixel += 2)
                                {
                                    pRGBPixel->SetValue(real(pPixel[-1] + pPixel[1]) * _REAL_HALF_, *pPixel, real(pPixel[-Width] + pPixel[Width]) * _REAL_HALF_);
                                }
                            else
                                for (coordinate x = XBase; x < X1; x += 2, pRGBPixel += 2, pPixel += 2)
                                {
                                    pRGBPixel->SetValue(real(pPixel[-Width] + pPixel[Width]) * _REAL_HALF_, *pPixel, real(pPixel[-1] + pPixel[1]) * _REAL_HALF_);
                                }
                        }
                        pBaseBayerPatternPixel = m_pInternSourceDiscreteBayerImage->GetReadOnlyBufferLineAt(Y0);
                        pBaseRGBPixel = m_pContinuousRGBImage->GetWritableBufferLineAt(Y0);
                        const coordinate NE = 1 - Width, NW = -Width - 1, SW = Width - 1, SE = Width + 1;
                        for (coordinate y = Y0; y < Y1; ++y, pBaseRGBPixel += Width, pBaseBayerPatternPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMap[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 + 1 : X0;
                            const byte* pPixel = pBaseBayerPatternPixel + XBase;
                            CContinuousTristimulusPixel* pRGBPixel = pBaseRGBPixel + XBase;
                            if (m_BayerPatternMap[y & 0x1][XBase & 0x1] == ChromaticSpaceRGB::eRed)
                                for (coordinate x = XBase; x < X1; x += 2, pRGBPixel += 2, pPixel += 2)
                                {
                                    pRGBPixel->SetValue(*pPixel, real(pPixel[1] + pPixel[-1] + pPixel[-Width] + pPixel[Width]) * _REAL_FOURTH_, real(pPixel[NE] + pPixel[NW] + pPixel[SW] + pPixel[SE]) * _REAL_FOURTH_);
                                }
                            else
                                for (coordinate x = XBase; x < X1; x += 2, pRGBPixel += 2, pPixel += 2)
                                {
                                    pRGBPixel->SetValue(real(pPixel[NE] + pPixel[NW] + pPixel[SW] + pPixel[SE]) * _REAL_FOURTH_, real(pPixel[1] + pPixel[-1] + pPixel[-Width] + pPixel[Width]) * _REAL_FOURTH_, *pPixel);
                                }
                        }
                        return true;
                    }
                    return false;
                }

                bool CDemosaicing::ContinuousDirectionalWeightedGradientDemosaicing()
                {
                    if (m_OutputDirectionalWeightedGradientActiveZone.IsEnabled())
                    {
                        const coordinate Width = m_OutputDirectionalWeightedGradientActiveZone.GetWidth();
                        const coordinate X0 = m_OutputDirectionalWeightedGradientActiveZone.GetX0();
                        const coordinate Y0 = m_OutputDirectionalWeightedGradientActiveZone.GetY0();
                        const coordinate X1 = m_OutputDirectionalWeightedGradientActiveZone.GetX1();
                        const coordinate Y1 = m_OutputDirectionalWeightedGradientActiveZone.GetY1();
                        const real* pBaseBayerPatternPixel = m_pInternSourceContinuousBayerImage->GetReadOnlyBufferAt(X0, Y0);
                        CContinuousTristimulusPixel* pBaseRGBPixel = m_pContinuousRGBImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseRGBPixel += Width, pBaseBayerPatternPixel += Width)
                        {
                            const real* pPixel = pBaseBayerPatternPixel;
                            CContinuousTristimulusPixel* pRGBPixel = pBaseRGBPixel;
                            for (coordinate x = X0; x < X1; ++x, ++pRGBPixel, ++pPixel)
                            {
                                pRGBPixel->SetChannelValue(m_BayerPatternMap[y & 0x1][x & 0x1], *pPixel);
                            }
                        }
                        const coordinate N2 = Width * -2, S2 = Width * 2;
                        pBaseBayerPatternPixel = m_pInternSourceContinuousBayerImage->GetReadOnlyBufferLineAt(Y0);
                        pBaseRGBPixel = m_pContinuousRGBImage->GetWritableBufferLineAt(Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseRGBPixel += Width, pBaseBayerPatternPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMap[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 + 1 : X0;
                            const real* pPixel = pBaseBayerPatternPixel + XBase;
                            CContinuousTristimulusPixel* pRGBPixel = pBaseRGBPixel + XBase;
                            for (coordinate x = XBase; x < X1; x += 2, pRGBPixel += 2, pPixel += 2)
                            {
                                const byte ChannelValue = *pPixel;
                                const real V = RealAbs(pPixel[-Width] - pPixel[Width]) * m_LuminanceEdgeContribution;
                                const real N = GradientWeightingKernel(V + RealAbs(ChannelValue - pPixel[N2]));
                                const real S = GradientWeightingKernel(V + RealAbs(ChannelValue - pPixel[S2]));
                                const real H = RealAbs(pPixel[1] - pPixel[-1]) * m_LuminanceEdgeContribution;
                                const real E = GradientWeightingKernel(H + RealAbs(ChannelValue - pPixel[2]));
                                const real W = GradientWeightingKernel(H + RealAbs(ChannelValue - pPixel[-2]));
                                pRGBPixel->SetChannelValue(ChromaticSpaceRGB::eGreen, (N * real(pPixel[-Width]) + S * real(pPixel[Width]) + E * real(pPixel[1]) + W * real(pPixel[-1])) / (N + S + E + W));
                            }
                        }
                        const coordinate NE = 1 - Width, NW = -1 - Width, SE = Width + 1, SW = Width - 1;
                        pBaseBayerPatternPixel = m_pInternSourceContinuousBayerImage->GetReadOnlyBufferLineAt(Y0);
                        pBaseRGBPixel = m_pContinuousRGBImage->GetWritableBufferLineAt(Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseRGBPixel += Width, pBaseBayerPatternPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMap[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 + 1 : X0;
                            const ChromaticSpaceRGB::ChannelContent CurrentChannel = (m_BayerPatternMap[y & 0x1][XBase & 0x1] == ChromaticSpaceRGB::eRed) ? ChromaticSpaceRGB::eBlue : ChromaticSpaceRGB::eRed;
                            const real* pPixel = pBaseBayerPatternPixel + XBase;
                            CContinuousTristimulusPixel* pRGBPixel = pBaseRGBPixel + XBase;
                            for (coordinate x = XBase; x < X1; x += 2, pRGBPixel += 2, pPixel += 2)
                            {
                                const real CurrentGreenValue = pRGBPixel->GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen);
                                const real F = RealAbs(pPixel[NE] - pPixel[SW]) * m_ChromaticEdgeContribution;
                                const real WNE = GradientWeightingKernel(F + RealAbs(CurrentGreenValue - pRGBPixel[NE].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen)));
                                const real WSW = GradientWeightingKernel(F + RealAbs(CurrentGreenValue - pRGBPixel[SW].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen)));
                                const real B = RealAbs(pPixel[NW] - pPixel[SE]) * m_ChromaticEdgeContribution;
                                const real WNW = GradientWeightingKernel(B + RealAbs(CurrentGreenValue - pRGBPixel[NW].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen)));
                                const real WSE = GradientWeightingKernel(B + RealAbs(CurrentGreenValue - pRGBPixel[SE].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen)));
                                pRGBPixel->SetChannelValue(CurrentChannel, (WNE * pPixel[NE] + WNW * pPixel[NW] + WSE * pPixel[SE] + WSW * pPixel[SW]) / (WNE + WSE + WNW + WSW));
                            }
                        }
                        pBaseRGBPixel = m_pContinuousRGBImage->GetWritableBufferLineAt(Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseRGBPixel += Width, pBaseBayerPatternPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMap[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 : X0 + 1;
                            CContinuousTristimulusPixel* pRGBPixel = pBaseRGBPixel + XBase;
                            for (coordinate x = XBase; x < X1; x += 2, pRGBPixel += 2)
                            {
                                const real CurrentGreenValue = pRGBPixel->GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen);
                                const real GE = RealAbs(CurrentGreenValue - pRGBPixel[ 2].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen));
                                const real GW = RealAbs(CurrentGreenValue - pRGBPixel[-2].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen));
                                const real GN = RealAbs(CurrentGreenValue - pRGBPixel[N2].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen));
                                const real GS = RealAbs(CurrentGreenValue - pRGBPixel[S2].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen));
                                real V = RealAbs(pRGBPixel[-Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) - pRGBPixel[Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed)) * m_ChromaticEdgeContribution;
                                real N = GradientWeightingKernel(V + GN);
                                real S = GradientWeightingKernel(V + GS);
                                real H = RealAbs(pRGBPixel[1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) - pRGBPixel[-1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed)) * m_ChromaticEdgeContribution;
                                real E = GradientWeightingKernel(H + GE);
                                real W = GradientWeightingKernel(H + GW);
                                pRGBPixel->SetChannelValue(ChromaticSpaceRGB::eRed, (N * pRGBPixel[-Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) + S * pRGBPixel[Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) + E * pRGBPixel[1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) + W * pRGBPixel[-1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed)) / (N + S + E + W));
                                V = RealAbs(pRGBPixel[-Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) - pRGBPixel[Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue)) * m_ChromaticEdgeContribution;
                                N = GradientWeightingKernel(V + GN);
                                S = GradientWeightingKernel(V + GS);
                                H = RealAbs(pRGBPixel[1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) - pRGBPixel[-1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue)) * m_ChromaticEdgeContribution;
                                E = GradientWeightingKernel(H + GE);
                                W = GradientWeightingKernel(H + GW);
                                pRGBPixel->SetChannelValue(ChromaticSpaceRGB::eBlue, (N * pRGBPixel[-Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) + S * pRGBPixel[Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) + E * pRGBPixel[1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) + W * pRGBPixel[-1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue)) / (N + S + E + W));
                            }
                        }
                        return true;
                    }
                    return false;
                }

                bool CDemosaicing::DiscreteDirectionalWeightedGradientDemosaicing()
                {
                    if (m_OutputDirectionalWeightedGradientActiveZone.IsEnabled())
                    {
                        const coordinate Width = m_OutputDirectionalWeightedGradientActiveZone.GetWidth();
                        const coordinate X0 = m_OutputDirectionalWeightedGradientActiveZone.GetX0();
                        const coordinate Y0 = m_OutputDirectionalWeightedGradientActiveZone.GetY0();
                        const coordinate X1 = m_OutputDirectionalWeightedGradientActiveZone.GetX1();
                        const coordinate Y1 = m_OutputDirectionalWeightedGradientActiveZone.GetY1();
                        const byte* pBaseBayerPatternPixel = m_pInternSourceDiscreteBayerImage->GetReadOnlyBufferAt(X0, Y0);
                        CContinuousTristimulusPixel* pBaseRGBPixel = m_pContinuousRGBImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseRGBPixel += Width, pBaseBayerPatternPixel += Width)
                        {
                            const byte* pPixel = pBaseBayerPatternPixel;
                            CContinuousTristimulusPixel* pRGBPixel = pBaseRGBPixel;
                            for (coordinate x = X0; x < X1; ++x, ++pRGBPixel, ++pPixel)
                            {
                                pRGBPixel->SetChannelValue(m_BayerPatternMap[y & 0x1][x & 0x1], *pPixel);
                            }
                        }
                        const coordinate N2 = Width * -2, S2 = Width * 2;
                        pBaseBayerPatternPixel = m_pInternSourceDiscreteBayerImage->GetReadOnlyBufferLineAt(Y0);
                        pBaseRGBPixel = m_pContinuousRGBImage->GetWritableBufferLineAt(Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseRGBPixel += Width, pBaseBayerPatternPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMap[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 + 1 : X0;
                            const byte* pPixel = pBaseBayerPatternPixel + XBase;
                            CContinuousTristimulusPixel* pRGBPixel = pBaseRGBPixel + XBase;
                            for (coordinate x = XBase; x < X1; x += 2, pRGBPixel += 2, pPixel += 2)
                            {
                                const byte ChannelValue = *pPixel;
                                const real V = real(abs(pPixel[-Width] - pPixel[Width])) * m_LuminanceEdgeContribution;
                                const real N = GradientWeightingKernel(V + abs(ChannelValue - pPixel[N2]));
                                const real S = GradientWeightingKernel(V + abs(ChannelValue - pPixel[S2]));
                                const real H = real(abs(pPixel[1] - pPixel[-1])) * m_LuminanceEdgeContribution;
                                const real E = GradientWeightingKernel(H + abs(ChannelValue - pPixel[2]));
                                const real W = GradientWeightingKernel(H + abs(ChannelValue - pPixel[-2]));
                                pRGBPixel->SetChannelValue(ChromaticSpaceRGB::eGreen, (N * real(pPixel[-Width]) + S * real(pPixel[Width]) + E * real(pPixel[1]) + W * real(pPixel[-1])) / (N + S + E + W));
                            }
                        }
                        const coordinate NE = 1 - Width, NW = -1 - Width, SE = Width + 1, SW = Width - 1;
                        pBaseBayerPatternPixel = m_pInternSourceDiscreteBayerImage->GetReadOnlyBufferLineAt(Y0);
                        pBaseRGBPixel = m_pContinuousRGBImage->GetWritableBufferLineAt(Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseRGBPixel += Width, pBaseBayerPatternPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMap[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 + 1 : X0;
                            const ChromaticSpaceRGB::ChannelContent CurrentChannel = (m_BayerPatternMap[y & 0x1][XBase & 0x1] == ChromaticSpaceRGB::eRed) ? ChromaticSpaceRGB::eBlue : ChromaticSpaceRGB::eRed;
                            const byte* pPixel = pBaseBayerPatternPixel + XBase;
                            CContinuousTristimulusPixel* pRGBPixel = pBaseRGBPixel + XBase;
                            for (coordinate x = XBase; x < X1; x += 2, pRGBPixel += 2, pPixel += 2)
                            {
                                const real CurrentGreenValue = pRGBPixel->GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen);
                                const real F = real(abs(pPixel[NE] - pPixel[SW])) * m_ChromaticEdgeContribution;
                                const real WNE = GradientWeightingKernel(F + RealAbs(CurrentGreenValue - pRGBPixel[NE].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen)));
                                const real WSW = GradientWeightingKernel(F + RealAbs(CurrentGreenValue - pRGBPixel[SW].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen)));
                                const real B = real(abs(pPixel[NW] - pPixel[SE])) * m_ChromaticEdgeContribution;
                                const real WNW = GradientWeightingKernel(B + RealAbs(CurrentGreenValue - pRGBPixel[NW].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen)));
                                const real WSE = GradientWeightingKernel(B + RealAbs(CurrentGreenValue - pRGBPixel[SE].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen)));
                                pRGBPixel->SetChannelValue(CurrentChannel, (WNE * pPixel[NE] + WNW * pPixel[NW] + WSE * pPixel[SE] + WSW * pPixel[SW]) / (WNE + WSE + WNW + WSW));
                            }
                        }
                        pBaseRGBPixel = m_pContinuousRGBImage->GetWritableBufferLineAt(Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseRGBPixel += Width, pBaseBayerPatternPixel += Width)
                        {
                            const coordinate XBase = (m_BayerPatternMap[y & 0x1][X0 & 0x1] == ChromaticSpaceRGB::eGreen) ? X0 : X0 + 1;
                            CContinuousTristimulusPixel* pRGBPixel = pBaseRGBPixel + XBase;
                            for (coordinate x = XBase; x < X1; x += 2, pRGBPixel += 2)
                            {
                                const real CurrentGreenValue = pRGBPixel->GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen);
                                const real GN = RealAbs(CurrentGreenValue - pRGBPixel[N2].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen));
                                const real GS = RealAbs(CurrentGreenValue - pRGBPixel[S2].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen));
                                const real GE = RealAbs(CurrentGreenValue - pRGBPixel[ 2].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen));
                                const real GW = RealAbs(CurrentGreenValue - pRGBPixel[-2].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eGreen));
                                const real Vr = RealAbs(pRGBPixel[-Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) - pRGBPixel[Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed)) * m_ChromaticEdgeContribution;
                                const real Nr = GradientWeightingKernel(Vr + GN);
                                const real Sr = GradientWeightingKernel(Vr + GS);
                                const real Hr = RealAbs(pRGBPixel[1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) - pRGBPixel[-1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed)) * m_ChromaticEdgeContribution;
                                const real Er = GradientWeightingKernel(Hr + GE);
                                const real Wr = GradientWeightingKernel(Hr + GW);
                                pRGBPixel->SetChannelValue(ChromaticSpaceRGB::eRed, (Nr * pRGBPixel[-Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) + Sr * pRGBPixel[Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) + Er * pRGBPixel[1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed) + Wr * pRGBPixel[-1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eRed)) / (Nr + Sr + Er + Wr));
                                const real Vb = RealAbs(pRGBPixel[-Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) - pRGBPixel[Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue)) * m_ChromaticEdgeContribution;
                                const real Nb = GradientWeightingKernel(Vb + GN);
                                const real Sb = GradientWeightingKernel(Vb + GS);
                                const real Hb = RealAbs(pRGBPixel[1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) - pRGBPixel[-1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue)) * m_ChromaticEdgeContribution;
                                const real Eb = GradientWeightingKernel(Hb + GE);
                                const real Wb = GradientWeightingKernel(Hb + GW);
                                pRGBPixel->SetChannelValue(ChromaticSpaceRGB::eBlue, (Nb * pRGBPixel[-Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) + Sb * pRGBPixel[Width].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) + Eb * pRGBPixel[1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue) + Wb * pRGBPixel[-1].GetReadOnlyChannelReferenceByIndex(ChromaticSpaceRGB::eBlue)) / (Nb + Sb + Eb + Wb));
                            }
                        }
                        return true;
                    }
                    return false;
                }

                bool CDemosaicing::ContinuousAdaptiveDemosaicing()
                {
                    if (m_OutputAdaptiveActiveZone.IsEnabled())
                    {
                        const coordinate Width = m_OutputAdaptiveActiveZone.GetWidth();
                        const coordinate DX0 = m_OutputAdaptiveActiveZone.GetX0();
                        const coordinate DY0 = m_OutputAdaptiveActiveZone.GetY0();
                        const coordinate DX1 = m_OutputAdaptiveActiveZone.GetX1();
                        const coordinate DY1 = m_OutputAdaptiveActiveZone.GetY1();
                        const coordinate SX0 = m_OutputDirectionalWeightedGradientActiveZone.GetX0();
                        const coordinate SY0 = m_OutputDirectionalWeightedGradientActiveZone.GetY0();
                        const coordinate SX1 = SX0 + m_pAdaptiveKernel->GetWidth();
                        const coordinate SY1 = SY0 + m_pAdaptiveKernel->GetWidth();
                        const real* pBaseGaussianKernel = m_pAdaptiveKernel->GetKernelReadOnlyBuffer();
                        const CContinuousTristimulusPixel* pInputAbsoluteBaseZonePixel = m_pContinuousRGBImage->GetReadOnlyBufferAt(SX0, SY0);
                        const CContinuousTristimulusPixel* pInputBaseCenterPixel = m_pContinuousRGBImage->GetReadOnlyBufferAt(DX0, DY0);
                        CContinuousTristimulusPixel* pOutputBasePixel = m_pAdaptiveContinuousRGBImage->GetWritableBufferAt(DX0, DY0);
                        real ChannelsAccumulators[6] = { _REAL_ZERO_ };
                        real* KernelAccumulators = ChannelsAccumulators + 3;
#ifdef _DEMOSAICING_USE_LOOKUP_TABLE_
                        const real MaximalDistance = s_ELT.m_MinimalArgumentValue / m_AdaptiveRangeExponentFactor;
                        const real Scale = s_ELT.m_SamplesPerUnit * m_AdaptiveRangeExponentFactor;
#endif
                        for (coordinate y = DY0, y0 = SY0, y1 = SY1; y < DY1; ++y, ++y0, ++y1, pInputAbsoluteBaseZonePixel += Width, pInputBaseCenterPixel += Width, pOutputBasePixel += Width)
                        {
                            const CContinuousTristimulusPixel* pInputExternBaseZonePixel = pInputAbsoluteBaseZonePixel;
                            const CContinuousTristimulusPixel* pInputCenterPixel = pInputBaseCenterPixel;
                            CContinuousTristimulusPixel* pOutputPixel = pOutputBasePixel;
                            for (coordinate x = DX0, x0 = SX0, x1 = SX1; x < DX1; ++x, ++x0, ++x1, ++pInputExternBaseZonePixel, ++pInputCenterPixel, ++pOutputPixel)
                            {
                                const CContinuousTristimulusPixel* pInputInternBaseZonePixel = pInputExternBaseZonePixel;
                                const real* pKernel = pBaseGaussianKernel;
                                memset(ChannelsAccumulators, 0, sizeof(real) * 6);
                                for (coordinate ys = y0; ys < y1; ++ys, pInputInternBaseZonePixel += Width)
                                {
                                    const CContinuousTristimulusPixel* pInputZonePixel = pInputInternBaseZonePixel;
                                    for (coordinate xs = x0; xs < x1; ++xs, ++pInputZonePixel, ++pKernel)
                                    {
#ifdef _DEMOSAICING_USE_LOOKUP_TABLE_
                                        const real Distance = pInputZonePixel->GetRGBFixedWeightedSquareEuclideanDistance(pInputCenterPixel);
                                        if (Distance < MaximalDistance)
                                        {
                                            const real AdaptiveKernel = s_ELT.m_pExp[RoundToInteger(Distance * Scale)] * *pKernel;
                                            const ChromaticSpaceRGB::ChannelContent CurrentChannel = m_BayerPatternMap[ys & 0x1][xs & 0x1];
                                            KernelAccumulators[CurrentChannel] += AdaptiveKernel;
                                            ChannelsAccumulators[CurrentChannel] += pInputZonePixel->GetReadOnlyChannelReferenceByIndex(CurrentChannel) * AdaptiveKernel;
                                        }
#else

                                        const real AdaptiveKernel = RealExp(m_AdaptiveRangeExponentFactor * pInputZonePixel->GetRGBFixedWeightedSquareEuclideanDistance(pInputCenterPixel)) * *pKernel;
                                        const ChromaticSpaceRGB::ChannelContent CurrentChannel = m_BayerPatternMap[ys & 0x1][xs & 0x1];
                                        KernelAccumulators[CurrentChannel] += AdaptiveKernel;
                                        ChannelsAccumulators[CurrentChannel] += pInputZonePixel->GetReadOnlyChannelReferenceByIndex(CurrentChannel) * AdaptiveKernel;
#endif
                                    }
                                }
                                pOutputPixel->SetValue(ChannelsAccumulators, KernelAccumulators);
                            }
                        }
                        m_pContinuousRGBImage->Copy(m_pAdaptiveContinuousRGBImage);
                        return true;
                    }
                    return true;
                }

                real CDemosaicing::GradientWeightingKernel(const real Gradient) const
                {
                    //real SquareGradient = Gradient * Gradient;
                    //return realInverse(_REAL_ONE_ + (SquareGradient * Gradient) + SquareGradient + Gradient);
                    return _REAL_ONE_ / (_REAL_ONE_ + (Gradient * Gradient));
                    //return RealExp(-X*X);
                }
            }
        }
    }
}

