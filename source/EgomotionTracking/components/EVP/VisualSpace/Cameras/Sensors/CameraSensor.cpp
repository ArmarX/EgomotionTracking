/*
 * CameraSensor.cpp
 *
 *  Created on: 06.04.2011
 *      Author: gonzalez
 */

#include "CameraSensor.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            CCameraSensor::CCameraSensor(Devices::CCameraDevice* pCameraDevice, GeometricCalibration::CCameraGeometricCalibration* pCameraGeometricCalibration, ChromaticCalibration::CCameraChromaticCalibration* pCameraChromaticCalibration)//, RadiometricCalibration::CCameraRadiometricCalibration* pCameraRadiometricCalibration)
            {
                m_pCameraDevice = pCameraDevice;
                m_pCameraGeometricCalibration = pCameraGeometricCalibration;
                m_pCameraChromaticCalibration = pCameraChromaticCalibration;
                //m_pCameraRadiometricCalibration = pCameraRadiometricCalibration;
            }

            CCameraSensor::~CCameraSensor()
                = default;

            Devices::CCameraDevice* CCameraSensor::GetCameraDevice()
            {
                return m_pCameraDevice;
            }

            GeometricCalibration::CCameraGeometricCalibration* CCameraSensor::GetCameraGeometricCalibration()
            {
                return m_pCameraGeometricCalibration;
            }

            ChromaticCalibration::CCameraChromaticCalibration* CCameraSensor::GetCameraChromaticCalibration()
            {
                return m_pCameraChromaticCalibration;
            }

            /*RadiometricCalibration::CCameraRadiometricCalibration* CCameraSensor::GetCameraRadiometricCalibration()
             {
             return m_pCameraRadiometricCalibration;
             }*/
        }
    }
}
