/*
 * StereoCameraSensor.cpp
 *
 *  Created on: 11.04.2011
 *      Author: gonzalez
 */

#include "StereoCameraSensor.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            IDENTIFIABLE_INITIALIZATION(CStereoCameraSensor)

            CStereoCameraSensor::CStereoCameraSensor(CCameraSensor* pLeftCameraSensor, CCameraSensor* pRightCameraSensor, GeometricCalibration::CStereoCameraGeometricCalibration* pStereoCameraGeometricCalibration, GeometricCalibration::CStereoCameraLensUndistorter* pStereoCameraLensUndistorter) :
                IDENTIFIABLE_CONTRUCTOR(CStereoCameraSensor)
            {
                m_pLeftCameraSensor = pLeftCameraSensor;
                m_pRightCameraSensor = pRightCameraSensor;
                m_pStereoCameraGeometricCalibration = pStereoCameraGeometricCalibration;
                m_pStereoCameraLensUndistorter = pStereoCameraLensUndistorter;
            }

            CStereoCameraSensor::~CStereoCameraSensor()
                = default;

            CCameraSensor* CStereoCameraSensor::GetLeftCameraSensor()
            {
                return m_pLeftCameraSensor;
            }

            CCameraSensor* CStereoCameraSensor::GetRightCameraSensor()
            {
                return m_pRightCameraSensor;
            }

            GeometricCalibration::CStereoCameraGeometricCalibration* CStereoCameraSensor::GetStereoCameraGeometricCalibration()
            {
                return m_pStereoCameraGeometricCalibration;
            }

            GeometricCalibration::CStereoCameraLensUndistorter* CStereoCameraSensor::GetStereoCameraLensUndistorter()
            {
                return m_pStereoCameraLensUndistorter;
            }
        }
    }
}
