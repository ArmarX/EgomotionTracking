/*
 * CameraSensor.h
 *
 *  Created on: 06.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../Devices/CameraDevice.h"
#include "../GeometricCalibration/CameraGeometricCalibration.h"
#include "../ChromaticCalibration/CameraChromaticCalibration.h"
//#include "../RadiometricCalibration/CameraRadiometricCalibration.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            class CCameraSensor
            {
            public:

                CCameraSensor(Devices::CCameraDevice* pCameraDevice, GeometricCalibration::CCameraGeometricCalibration* pCameraGeometricCalibration, ChromaticCalibration::CCameraChromaticCalibration* pCameraChromaticCalibration);//, RadiometricCalibration::CCameraRadiometricCalibration* pCameraRadiometricCalibration);
                virtual ~CCameraSensor();

                Devices::CCameraDevice* GetCameraDevice();
                GeometricCalibration::CCameraGeometricCalibration* GetCameraGeometricCalibration();
                ChromaticCalibration::CCameraChromaticCalibration* GetCameraChromaticCalibration();
                //RadiometricCalibration::CCameraRadiometricCalibration* GetCameraRadiometricCalibration();

            protected:

                Devices::CCameraDevice* m_pCameraDevice;
                GeometricCalibration::CCameraGeometricCalibration* m_pCameraGeometricCalibration;
                ChromaticCalibration::CCameraChromaticCalibration* m_pCameraChromaticCalibration;
                //RadiometricCalibration::CCameraRadiometricCalibration* m_pCameraRadiometricCalibration;
            };
        }
    }
}

