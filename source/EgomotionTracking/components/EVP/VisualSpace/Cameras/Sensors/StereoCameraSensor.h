/*
 * StereoCameraSensor.h
 *
 *  Created on: 11.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "CameraSensor.h"
#include "../GeometricCalibration/StereoCameraGeometricCalibration.h"
#include "../GeometricCalibration/StereoCameraLensUndistorter.h"
#include "../../../Foundation/Miscellaneous/IdentifiableInstance.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            class CStereoCameraSensor: public CIdentifiableInstance
            {
                IDENTIFIABLE

            public:

                CStereoCameraSensor(CCameraSensor* pLeftCameraSensor, CCameraSensor* pRightCameraSensor, GeometricCalibration::CStereoCameraGeometricCalibration* pStereoCameraGeometricCalibration, GeometricCalibration::CStereoCameraLensUndistorter* pStereoCameraLensUndistorter);
                ~CStereoCameraSensor() override;

                CCameraSensor* GetLeftCameraSensor();
                CCameraSensor* GetRightCameraSensor();
                GeometricCalibration::CStereoCameraGeometricCalibration* GetStereoCameraGeometricCalibration();
                GeometricCalibration::CStereoCameraLensUndistorter* GetStereoCameraLensUndistorter();

            protected:

                CCameraSensor* m_pLeftCameraSensor;
                CCameraSensor* m_pRightCameraSensor;
                GeometricCalibration::CStereoCameraGeometricCalibration* m_pStereoCameraGeometricCalibration;
                GeometricCalibration::CStereoCameraLensUndistorter* m_pStereoCameraLensUndistorter;
            };
        }
    }
}

