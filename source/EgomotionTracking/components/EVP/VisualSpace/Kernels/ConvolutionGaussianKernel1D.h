/*
 * ConvolutionGaussianKernel1D.h
 *
 *  Created on: 29.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../Foundation/Mathematics/1D/Common1D.h"
#include "ConvolutionKernel1D.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Kernels
        {
            class CConvolutionGaussianKernel1D: public TConvolutionKernel1D<real>
            {
            public:

                CConvolutionGaussianKernel1D(const real StandardDeviation);
                ~CConvolutionGaussianKernel1D() override;

                void Normalize();
                void ScaleMaximal();

                bool SetStandardDeviation(const real StandardDeviation);
                real GetStandardDeviation() const;

            protected:

                void Calculate() override;

                real m_StandardDeviation;
            };
        }
    }
}

