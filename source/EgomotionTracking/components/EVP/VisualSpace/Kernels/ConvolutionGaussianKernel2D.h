/*
 * ConvolutionGaussianKernel2D.h
 *
 *  Created on: 22.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../Foundation/Mathematics/1D/Common1D.h"
#include "../../Foundation/Mathematics/2D/Vector2D.h"
#include "ConvolutionKernel2D.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Kernels
        {
            class CConvolutionGaussianKernel2D: public TConvolutionKernel2D<real>
            {
            public:

                CConvolutionGaussianKernel2D(const real StandardDeviation);
                ~CConvolutionGaussianKernel2D() override;

                void Normalize();
                void ScaleAtStandardDeviation(const real StandardDeviationFactor = _REAL_ONE_, const real Value = _REAL_ONE_);
                void ScaleMaximal(const real Scale = _REAL_ONE_);
                void ScaleMinimal(const real Scale = _REAL_ONE_);

                bool SetStandardDeviation(const real StandardDeviation);
                real GetStandardDeviation() const;
                coordinate GetRadius() const;

            protected:

                void Calculate() override;

                real m_StandardDeviation;
            };
        }
    }
}

