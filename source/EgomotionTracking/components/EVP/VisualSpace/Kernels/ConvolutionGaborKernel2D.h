/*
 * ConvolutionGaborKernel2D.h
 *
 *  Created on: 22.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../Foundation/Mathematics/1D/Common1D.h"
#include "../../Foundation/Mathematics/2D/Vector2D.h"
#include "ConvolutionKernel2D.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Kernels
        {
            class CConvolutionGaborKernel2D: public TConvolutionKernel2D<real>
            {
            public:

                CConvolutionGaborKernel2D(const int Radius, const real Lambda, const real Sigma, const real Gamma1, const real Gamma2, const Mathematics::_2D::CVector2D& Orientation);
                ~CConvolutionGaborKernel2D() override;

                void Scale(const real ScaleValue = _REAL_ONE_);
                void RemoveSpuriousValue(const real Value = _REAL_ONE_);
                real GetMaximal() const;
                real GetMinimalNonZero() const;
                void ScaleMinimalNonZero(const real ScaleValue = _REAL_ONE_);

                inline  real GetLambda() const
                {
                    return m_Lambda;
                }

                inline  real GetSigma() const
                {
                    return m_Sigma;
                }

                inline  real GetGamma1() const
                {
                    return m_Gamma1;
                }

                inline  real GetGamma2() const
                {
                    return m_Gamma2;
                }

                inline const Mathematics::_2D::CVector2D& GetOrientation() const
                {
                    return m_Orientation;
                }

                inline  real GetOrientationX() const
                {
                    return m_Orientation.GetX();
                }

                inline  real GetOrientationY() const
                {
                    return m_Orientation.GetY();
                }

            protected:

                void Calculate() override;

                const real m_Lambda;
                const real m_Sigma;
                const real m_Gamma1;
                const real m_Gamma2;
                const Mathematics::_2D::CVector2D m_Orientation;
            };
        }
    }
}

