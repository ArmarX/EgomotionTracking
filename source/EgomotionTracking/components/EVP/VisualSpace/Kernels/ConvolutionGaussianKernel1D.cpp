/*
 * ConvolutionGaussianKernel1D.cpp
 *
 *  Created on: 29.03.2011
 *      Author: gonzalez
 */

#include "ConvolutionGaussianKernel1D.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Kernels
        {
            CConvolutionGaussianKernel1D::CConvolutionGaussianKernel1D(real StandardDeviation) :
                TConvolutionKernel1D<real> (UpperToInteger(StandardDeviation * _REAL_THREE_))
            {
                m_StandardDeviation = StandardDeviation;
                Calculate();
            }

            CConvolutionGaussianKernel1D::~CConvolutionGaussianKernel1D()
                = default;

            void CConvolutionGaussianKernel1D::Calculate()
            {
                const real ExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(m_StandardDeviation);
                const real ScalarFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(m_StandardDeviation);
                real* pData = m_pKernel;
                for (int x = -m_Radius; x <= m_Radius; ++x, ++pData)
                {
                    *pData = ScalarFactor * RealExp(real(x * x) * ExponentFactor);
                }
            }

            void CConvolutionGaussianKernel1D::Normalize()
            {
                const real* const pDataEnd = m_pKernel + (m_Radius * 2 + 1);
                real* pData = m_pKernel;
                real Accumulator = _REAL_ZERO_;
                while (pData < pDataEnd)
                {
                    Accumulator += *pData++;
                }
                pData = m_pKernel;
                const real Normalization = _REAL_ONE_ / Accumulator;
                while (pData < pDataEnd)
                {
                    *pData = *pData * Normalization;
                    ++pData;
                }
            }

            void CConvolutionGaussianKernel1D::ScaleMaximal()
            {
                const real* const pDataEnd = m_pKernel + (m_Radius * 2 + 1);
                const real Normalizetion = _REAL_ONE_ / m_pKernel[m_Radius];
                real* pData = m_pKernel;
                while (pData < pDataEnd)
                {
                    *pData = *pData * Normalizetion;
                    ++pData;
                }
            }

            bool CConvolutionGaussianKernel1D::SetStandardDeviation(const real StandardDeviation)
            {
                if ((StandardDeviation > _REAL_ZERO_) && (StandardDeviation != m_StandardDeviation))
                {
                    m_StandardDeviation = StandardDeviation;
                    m_Radius = TMax(UpperToInteger(m_StandardDeviation * _REAL_THREE_), 1);
                    RELEASE_ARRAY(m_pKernel)
                    m_pKernel = new real[m_Radius * 2 + 1];
                    Calculate();
                    return true;
                }
                return false;
            }

            real CConvolutionGaussianKernel1D::GetStandardDeviation() const
            {
                return m_StandardDeviation;
            }
        }
    }
}
