/*
 * ConvolutionDoGKernel2D.h
 *
 *  Created on: 04.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../Foundation/Mathematics/1D/Common1D.h"
#include "ConvolutionKernel2D.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Kernels
        {
            class CConvolutionDoGKernel2D: public TConvolutionKernel2D<real>
            {
            public:

                enum DoGConvinationMode
                {
                    eDirect, eIndirectNormalized, eIndirectMaximalNormalized
                };

                CConvolutionDoGKernel2D(const real OutterStandardDeviation, const real InnerStandardDeviation, const DoGConvinationMode Mode);
                ~CConvolutionDoGKernel2D() override;

                real GetOutterStandardDeviation() const;
                real GetInnerStandardDeviation() const;
                coordinate GetRadius() const;

                bool SetInnerStandardDeviation(const real InnerStandardDeviation);
                void Scale(const real Scale);

            protected:

                void Calculate() override;
                inline void CalculateDirect();
                inline void CalculateIndirectNormalized();
                inline void CalculateIndirectMaximalNormalized();

                DoGConvinationMode m_Mode;
                real m_OutterStandardDeviation;
                real m_InnerStandardDeviation;
            };
        }
    }
}

