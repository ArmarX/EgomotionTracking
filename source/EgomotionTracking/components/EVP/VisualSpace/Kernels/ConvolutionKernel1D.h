/*
 * ConvolutionKernel1D.h
 *
 *  Created on: 29.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Kernels
        {
            template<typename DataType> class TConvolutionKernel1D
            {
            public:

                TConvolutionKernel1D(const uint Radius) :
                    m_Radius(0), m_pKernel(0)
                {
                    if (Radius > 0)
                    {
                        m_Radius = Radius;
                        m_pKernel = new DataType[m_Radius * 2 + 1];
                    }
                }

                virtual ~TConvolutionKernel1D()
                {
                    RELEASE_ARRAY(m_pKernel)
                }

                inline uint GetRadius() const
                {
                    return m_Radius;
                }

                inline uint GetWidth() const
                {
                    return m_Radius * 2 + 1;
                }

                inline const DataType* GetKernelReadOnlyBuffer() const
                {
                    return m_pKernel;
                }

                inline DataType* GetKernel()
                {
                    return m_pKernel;
                }

                inline DataType* GetKernelCenter()
                {
                    return m_pKernel + m_Radius;
                }

            protected:

                virtual void Calculate() = 0;

                int m_Radius;
                DataType* m_pKernel;
            };
        }
    }
}

