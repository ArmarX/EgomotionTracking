/*
 * ConvolutionGaussianKernel2D.cpp
 *
 *  Created on: 22.03.2011
 *      Author: gonzalez
 */

#include "ConvolutionGaussianKernel2D.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Kernels
        {
            CConvolutionGaussianKernel2D::CConvolutionGaussianKernel2D(const real StandardDeviation) :
                TConvolutionKernel2D<real>(RoundToInteger(StandardDeviation * _REAL_THREE_) * 2 + 1, TConvolutionKernel2D<real>::eDiameter), m_StandardDeviation(StandardDeviation)
            {
                Calculate();
            }

            CConvolutionGaussianKernel2D::~CConvolutionGaussianKernel2D()
                = default;

            void CConvolutionGaussianKernel2D::Calculate()
            {
                const coordinate Radius = (m_Kernel.GetWidth() - 1) / 2;
                const real ExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(m_StandardDeviation);
                const real ScalarFactor = Mathematics::_1D::NormalDistribution::DetermineCoefficient(m_StandardDeviation);
                real* pData = m_Kernel.GetBeginWritableBuffer();
                for (int y = -Radius; y <= Radius; ++y)
                {
                    const int y2 = y * y;
                    for (int x = -Radius; x <= Radius; ++x, ++pData)
                    {
                        *pData = ScalarFactor * RealExp(real(x * x + y2) * ExponentFactor);
                    }
                }
            }

            void CConvolutionGaussianKernel2D::Normalize()
            {
                const real* const pDataEnd = m_Kernel.GetEndReadOnlyBuffer();
                real* pData = m_Kernel.GetBeginWritableBuffer();
                real Accumulator = _REAL_ZERO_;
                while (pData < pDataEnd)
                {
                    Accumulator += *pData++;
                }
                const real Normalization = _REAL_ONE_ / Accumulator;
                pData = m_Kernel.GetBeginWritableBuffer();
                while (pData < pDataEnd)
                {
                    *pData = *pData * Normalization;
                    ++pData;
                }
            }

            void CConvolutionGaussianKernel2D::ScaleAtStandardDeviation(const real StandardDeviationFactor, const real Value)
            {
                Calculate();
                const real Distance = m_StandardDeviation * StandardDeviationFactor;
                const real ScaleFactor = Value / Mathematics::_1D::NormalDistribution::DetermineNormalizedDensity(Distance, _REAL_ZERO_, m_StandardDeviation);
                const real* const pDataEnd = m_Kernel.GetEndReadOnlyBuffer();
                real* pData = m_Kernel.GetBeginWritableBuffer();
                while (pData < pDataEnd)
                {
                    *pData = *pData * ScaleFactor;
                    ++pData;
                }
            }

            void CConvolutionGaussianKernel2D::ScaleMaximal(const real Scale)
            {
                const coordinate Radius = (m_Kernel.GetWidth() - 1) / 2;
                const real* const pDataEnd = m_Kernel.GetEndReadOnlyBuffer();
                real* pData = m_Kernel.GetBeginWritableBuffer();
                const real Normalization = Scale / m_Kernel.GetReadOnlyReferenceToValueAt(Radius, Radius);
                while (pData < pDataEnd)
                {
                    *pData = *pData * Normalization;
                    ++pData;
                }
            }

            void CConvolutionGaussianKernel2D::ScaleMinimal(const real Scale)
            {
                const real* const pDataEnd = m_Kernel.GetEndReadOnlyBuffer();
                real* pData = m_Kernel.GetBeginWritableBuffer();
                const real Normalization = Scale / m_Kernel.GetReadOnlyReferenceToValueAt(0, 0);
                while (pData < pDataEnd)
                {
                    *pData = *pData * Normalization;
                    ++pData;
                }
            }

            bool CConvolutionGaussianKernel2D::SetStandardDeviation(const real StandardDeviation)
            {
                if ((StandardDeviation > _REAL_ZERO_) && (StandardDeviation != m_StandardDeviation))
                {
                    m_StandardDeviation = StandardDeviation;
                    const coordinate Radius = RoundToInteger(m_StandardDeviation * _REAL_THREE_);
                    const coordinate Diameter = Radius * 2 + 1;
                    return SetSize(Diameter, Diameter);
                }
                return false;
            }

            real CConvolutionGaussianKernel2D::GetStandardDeviation() const
            {
                return m_StandardDeviation;
            }

            coordinate CConvolutionGaussianKernel2D::GetRadius() const
            {
                return (m_Kernel.GetWidth() - 1) / 2;
            }
        }
    }
}
