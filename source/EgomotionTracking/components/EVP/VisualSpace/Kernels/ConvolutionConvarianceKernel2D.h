/*
 * ConvolutionConvarianceKernel2D.h
 *
 *  Created on: 06.10.2011
 *      Author: gonzalez
 */

#pragma once

#include "ConvolutionKernel2D.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Kernels
        {
            class CConvolutionConvarianceKernel2D: public TConvolutionKernel2D<real>
            {
            public:

                CConvolutionConvarianceKernel2D(const int Radius);
                ~CConvolutionConvarianceKernel2D() override;

            protected:

                void Calculate() override;
            };
        }
    }
}

