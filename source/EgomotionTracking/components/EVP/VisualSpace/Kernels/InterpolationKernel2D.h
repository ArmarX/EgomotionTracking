/*
 * InterpolationKernel2D.h
 *
 *  Created on: 25.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../Foundation/Mathematics/1D/Common1D.h"
#include "../../Foundation/Mathematics/1D/BicubicLookUpTable.h"
#include "../../Foundation/Mathematics/2D/Vector2D.h"
#include "ConvolutionKernel2D.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Kernels
        {
            class CInterpolationKernel2D
            {
            public:

                union InterpolationContribution
                {
                    real m_Wij[4][4];
                    real m_W[16];
                };

                CInterpolationKernel2D(const uint ScaleFactor);
                virtual ~CInterpolationKernel2D();

                void LoadRadial(const real Sigma = M_SQRT1_2);
                void LoadBicubic();

                uint GetScaleFactor() const;
                const InterpolationContribution* GetReadOnlyKernelBuffer() const;

            protected:

                void Normalize();
                void ConsoleDisplay();

                real RadialKernel(const real Dx, const real Dy, const real ExponentFactor);

                TImage<InterpolationContribution> m_Kernel;
            };
        }
    }
}

