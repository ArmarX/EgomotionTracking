/*
 * ConvolutionDoGKernel2D.cpp
 *
 *  Created on: 04.04.2011
 *      Author: gonzalez
 */

#include "ConvolutionDoGKernel2D.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Kernels
        {
            CConvolutionDoGKernel2D::CConvolutionDoGKernel2D(const real OutterStandardDeviation, const real InnerStandardDeviation, const DoGConvinationMode Mode) :
                TConvolutionKernel2D<real> (TMax(RoundToInteger(TMax(OutterStandardDeviation, InnerStandardDeviation) * _REAL_THREE_), 1) * 2 + 1, TConvolutionKernel2D<real>::eDiameter), m_Mode(Mode), m_OutterStandardDeviation(TMax(OutterStandardDeviation, InnerStandardDeviation)), m_InnerStandardDeviation(TMin(OutterStandardDeviation, InnerStandardDeviation))
            {
                Calculate();
            }

            CConvolutionDoGKernel2D::~CConvolutionDoGKernel2D()
                = default;

            real CConvolutionDoGKernel2D::GetOutterStandardDeviation() const
            {
                return m_OutterStandardDeviation;
            }

            real CConvolutionDoGKernel2D::GetInnerStandardDeviation() const
            {
                return m_InnerStandardDeviation;
            }

            coordinate CConvolutionDoGKernel2D::GetRadius() const
            {
                return (m_Kernel.GetWidth() - 1) / 2;
            }

            void CConvolutionDoGKernel2D::Calculate()
            {
                switch (m_Mode)
                {
                    case eDirect:
                        CalculateDirect();
                        break;
                    case eIndirectNormalized:
                        CalculateIndirectNormalized();
                        break;
                    case eIndirectMaximalNormalized:
                        CalculateIndirectMaximalNormalized();
                        break;
                }
            }

            void CConvolutionDoGKernel2D::CalculateDirect()
            {
                const real OutterExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(m_OutterStandardDeviation);
                const real InnerExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(m_InnerStandardDeviation);
                const real OutterScalarFactor = Mathematics::_1D::NormalDistribution::DetermineCoefficient(m_OutterStandardDeviation);
                const real InnerScalarFactor = Mathematics::_1D::NormalDistribution::DetermineCoefficient(m_InnerStandardDeviation);
                const coordinate Radius = (m_Kernel.GetHeight() - 1) / 2;
                real* pData = m_Kernel.GetBeginWritableBuffer();
                for (int y = -Radius; y <= Radius; ++y)
                {
                    const int y2 = y * y;
                    for (int x = -Radius; x <= Radius; ++x, ++pData)
                    {
                        const real SquareRadius = real(x * x + y2);
                        *pData = (OutterScalarFactor * RealExp(SquareRadius * OutterExponentFactor)) - (InnerScalarFactor * RealExp(SquareRadius * InnerExponentFactor));
                    }
                }
            }

            void CConvolutionDoGKernel2D::CalculateIndirectNormalized()
            {
                const real OutterExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(m_OutterStandardDeviation);
                const real InnerExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(m_InnerStandardDeviation);
                const coordinate Radius = (m_Kernel.GetHeight() - 1) / 2;
                real* pData = m_Kernel.GetBeginWritableBuffer();
                real Accumulator = _REAL_ZERO_;
                for (int y = -Radius; y <= Radius; ++y)
                {
                    const int y2 = y * y;
                    for (int x = -Radius; x <= Radius; ++x, ++pData)
                    {
                        const real SquareRadius = real(x * x + y2);
                        const real Value = RealExp(SquareRadius * OutterExponentFactor) - RealExp(SquareRadius * InnerExponentFactor);
                        *pData = Value;
                        Accumulator += Value;
                    }
                }
                pData = m_Kernel.GetBeginWritableBuffer();
                const real* const pDataEnd = m_Kernel.GetEndReadOnlyBuffer();
                const real Scale = _REAL_ONE_ / Accumulator;
                while (pData < pDataEnd)
                {
                    *pData = *pData * Scale;
                    ++pData;
                }
            }

            void CConvolutionDoGKernel2D::CalculateIndirectMaximalNormalized()
            {
                const real OutterExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(m_OutterStandardDeviation);
                const real InnerExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(m_InnerStandardDeviation);
                const coordinate Radius = (m_Kernel.GetHeight() - 1) / 2;
                real* pData = m_Kernel.GetBeginWritableBuffer();
                real Maximal = _REAL_MIN_;
                for (int y = -Radius; y <= Radius; ++y)
                {
                    const int y2 = y * y;
                    for (int x = -Radius; x <= Radius; ++x, ++pData)
                    {
                        const real SquareRadius = real(x * x + y2);
                        const real Value = RealExp(SquareRadius * OutterExponentFactor) - RealExp(SquareRadius * InnerExponentFactor);
                        *pData = Value;
                        if (RealAbs(Value) > Maximal)
                        {
                            Maximal = RealAbs(Value);
                        }
                    }
                }
                pData = m_Kernel.GetBeginWritableBuffer();
                const real* const pDataEnd = m_Kernel.GetEndReadOnlyBuffer();
                const real Scale = _REAL_ONE_ / Maximal;
                while (pData < pDataEnd)
                {
                    *pData = *pData * Scale;
                    ++pData;
                }
            }

            bool CConvolutionDoGKernel2D::SetInnerStandardDeviation(const real InnerStandardDeviation)
            {
                if (InnerStandardDeviation < m_OutterStandardDeviation)
                {
                    m_InnerStandardDeviation = InnerStandardDeviation;
                    Calculate();
                    return true;
                }
                return false;
            }

            void CConvolutionDoGKernel2D::Scale(const real Scale)
            {
                const real* const pDataEnd = m_Kernel.GetEndReadOnlyBuffer();
                real* pData = m_Kernel.GetBeginWritableBuffer();
                while (pData < pDataEnd)
                {
                    *pData = *pData * Scale;
                    ++pData;
                }
            }
        }
    }
}
