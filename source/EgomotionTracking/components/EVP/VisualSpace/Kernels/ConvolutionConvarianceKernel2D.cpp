/*
 * ConvolutionConvarianceKernel2D.cpp
 *
 *  Created on: 06.10.2011
 *      Author: gonzalez
 */

#include "ConvolutionConvarianceKernel2D.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Kernels
        {
            CConvolutionConvarianceKernel2D::CConvolutionConvarianceKernel2D(const int Radius) :
                TConvolutionKernel2D<real> (TMax(Radius, 1) * 2 + 1, TConvolutionKernel2D<real>::eDiameter)
            {
                Calculate();
            }

            CConvolutionConvarianceKernel2D::~CConvolutionConvarianceKernel2D()
                = default;

            void CConvolutionConvarianceKernel2D::Calculate()
            {
                real* pCovarianceXY = m_Kernel.GetBeginWritableBuffer();
                const coordinate Radius = (m_Kernel.GetHeight() - 1) / 2;
                for (int Dy = -Radius; Dy <= Radius; ++Dy)
                    for (int Dx = -Radius; Dx <= Radius; ++Dx, ++pCovarianceXY)
                    {
                        *pCovarianceXY = Dx * Dy;
                    }
            }
        }
    }
}
