/*
 * InterpolationKernel2D.cpp
 *
 *  Created on: 25.03.2011
 *      Author: gonzalez
 */

#include "InterpolationKernel2D.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Kernels
        {
            CInterpolationKernel2D::CInterpolationKernel2D(const uint ScaleFactor) :
                m_Kernel(ScaleFactor, ScaleFactor)
            {
            }

            CInterpolationKernel2D::~CInterpolationKernel2D()
                = default;

            void CInterpolationKernel2D::LoadRadial(const real Sigma)
            {
                InterpolationContribution* pInterpolationContribution = m_Kernel.GetBeginWritableBuffer();
                const uint Size = m_Kernel.GetWidth();
                const real ExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(Sigma);
                const real Delta = _REAL_ONE_ / real(Size);
                real ys = _REAL_ZERO_, xs = _REAL_ZERO_;
                for (uint i = 0; i < Size; ++i, ys += Delta, xs = _REAL_ZERO_)
                    for (uint j = 0; j < Size; ++j, xs += Delta, ++pInterpolationContribution)
                        for (int p = 0, yn = -1; p < 4; ++p, ++yn)
                            for (int q = 0, xn = -1; q < 4; ++q, ++xn)
                            {
                                pInterpolationContribution->m_Wij[p][q] = RadialKernel(real(xn) - xs, real(yn) - ys, ExponentFactor);
                            }
                Normalize();
            }

            void CInterpolationKernel2D::LoadBicubic()
            {
                InterpolationContribution* pInterpolationContribution = m_Kernel.GetBeginWritableBuffer();
                const uint Size = m_Kernel.GetWidth();
                const real Delta = _REAL_ONE_ / real(Size);
                real ys = _REAL_ZERO_, xs = _REAL_ZERO_;
                for (uint i = 0; i < Size; ++i, ys += Delta, xs = _REAL_ZERO_)
                    for (uint j = 0; j < Size; ++j, xs += Delta, ++pInterpolationContribution)
                        for (int p = 0, yn = -1; p < 4; ++p, ++yn)
                        {
                            const real Ky = Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(ys - real(yn));
                            for (int q = 0, xn = -1; q < 4; ++q, ++xn)
                            {
                                pInterpolationContribution->m_Wij[p][q] = Ky * Mathematics::_1D::CBicubicLookUpTable::BicubicKernel(real(xn) - xs);
                            }
                        }
                Normalize();
            }

            void CInterpolationKernel2D::Normalize()
            {
                InterpolationContribution* pInterpolationContribution = m_Kernel.GetBeginWritableBuffer();
                const uint Size = m_Kernel.GetWidth();
                for (uint i = 0; i < Size; ++i)
                    for (uint j = 0; j < Size; ++j, ++pInterpolationContribution)
                    {
                        real Accumulator = _REAL_ZERO_;
                        for (int k = 0; k < 16; ++k)
                        {
                            Accumulator += pInterpolationContribution->m_W[k];
                        }
                        const real Normalization = _REAL_ONE_ / Accumulator;
                        for (int k = 0; k < 16; ++k)
                        {
                            pInterpolationContribution->m_W[k] *= Normalization;
                        }
                    }
            }

            uint CInterpolationKernel2D::GetScaleFactor() const
            {
                return m_Kernel.GetWidth();
            }

            const CInterpolationKernel2D::InterpolationContribution* CInterpolationKernel2D::GetReadOnlyKernelBuffer() const
            {
                return m_Kernel.GetBeginReadOnlyBuffer();
            }

            void CInterpolationKernel2D::ConsoleDisplay()
            {
                g_ConsoleStringOutput << g_EndLine;
                const InterpolationContribution* pInterpolationContribution = m_Kernel.GetBeginReadOnlyBuffer();
                const uint Size = m_Kernel.GetWidth();
                for (uint i = 0; i < Size; ++i)
                    for (uint j = 0; j < Size; ++j, ++pInterpolationContribution)
                    {
                        g_ConsoleStringOutput << "[" << i << "," << j << "]\t=\t";
                        for (int k = 0; k < 4; ++k)
                        {
                            g_ConsoleStringOutput << g_EndLine << g_Tabulator;
                            for (int l = 0; l < 4; ++l)
                            {
                                g_ConsoleStringOutput << pInterpolationContribution->m_Wij[k][l] << g_Tabulator;
                            }
                        }
                        g_ConsoleStringOutput << g_EndLine;
                    }
                g_ConsoleStringOutput << g_EndLine;
            }

            real CInterpolationKernel2D::RadialKernel(const real Dx, const real Dy, const real ExponentFactor)
            {
                return RealExp((Dx * Dx + Dy * Dy) * ExponentFactor);
            }
        }
    }
}
