/*
 * ConvolutionKernel2D.h
 *
 *  Created on: 22.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "../../Foundation/DataTypes/ImageSize.h"
#include "../Common/Image.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Kernels
        {
            template<typename DataType> class TConvolutionKernel2D
            {
            public:

                static  bool GetKernelOffsetActiveZoneUpdate(const void* pData, coordinate& OffsetX0, coordinate& OffsetY0, coordinate& OffsetX1, coordinate& OffsetY1)
                {
                    if (pData)
                    {
                        const TConvolutionKernel2D<DataType>* pConvolutionKernel2D = reinterpret_cast<const TConvolutionKernel2D<DataType>*>(pData);
                        if (pConvolutionKernel2D)
                        {
                            const CImageSize& Size = pConvolutionKernel2D->GetSize();
                            OffsetX1 = OffsetX0 = (Size.GetWidth() - 1) / 2;
                            OffsetY1 = OffsetY0 = (Size.GetHeight() - 1) / 2;
                            return true;
                        }
                    }
                    return false;
                }

                enum ContructionMode
                {
                    eRadius, eDiameter
                };

                TConvolutionKernel2D(const coordinate Length, const ContructionMode Mode) :
                    m_Kernel((Mode == eDiameter) ? Length : Length * 2 + 1, (Mode == eDiameter) ? Length : Length * 2 + 1)
                {
                }

                TConvolutionKernel2D(const coordinate Width, const coordinate Height) :
                    m_Kernel(Width, Height)
                {
                }

                virtual ~TConvolutionKernel2D()
                {
                }

                inline bool SetSize(const coordinate Width, const coordinate Height)
                {
                    return m_Kernel.Resize(Width, Height);
                }

                inline const CImageSize& GetSize() const
                {
                    return m_Kernel.GetSize();
                }

                inline coordinate GetWidth() const
                {
                    return m_Kernel.GetWidth();
                }

                inline coordinate GetHeight() const
                {
                    return m_Kernel.GetHeight();
                }

                inline uint GetArea() const
                {
                    return m_Kernel.GetArea();
                }

                inline const DataType* GetKernelReadOnlyBuffer() const
                {
                    return m_Kernel.GetBeginReadOnlyBuffer();
                }

                void ConsoleDisplay() const
                {
                    const coordinate Width = m_Kernel.GetWidth();
                    const coordinate Height = m_Kernel.GetHeight();
                    const DataType* pData = m_Kernel.GetBeginReadOnlyBuffer();
                    g_ConsoleStringOutput << g_EndLine;
                    for (coordinate y = 0; y < Height; ++y)
                    {
                        g_ConsoleStringOutput << "|" << g_Tabulator;
                        for (coordinate x = 0; x < Width; ++x)
                        {
                            g_ConsoleStringOutput << *pData++ << g_DoubleTabulator;
                        }
                        g_ConsoleStringOutput << "|" << g_EndLine;
                    }
                    g_ConsoleStringOutput << g_EndLine;
                }

            protected:

                virtual void Calculate() = 0;

                TImage<DataType> m_Kernel;
            };
        }
    }
}

