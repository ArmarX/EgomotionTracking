/*
 * ConvolutionGaborKernel2D.cpp
 *
 *  Created on: 22.03.2011
 *      Author: gonzalez
 */

#include "ConvolutionGaborKernel2D.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Kernels
        {
            CConvolutionGaborKernel2D::CConvolutionGaborKernel2D(const int Radius, const real Lambda, const real Sigma, const real Gamma1, const real Gamma2, const CVector2D& Orientation) :
                TConvolutionKernel2D<real> (Radius * 2 + 1, TConvolutionKernel2D<real>::eDiameter), m_Lambda(Lambda), m_Sigma(Sigma), m_Gamma1(Gamma1), m_Gamma2(Gamma2), m_Orientation(Orientation)
            {
                Calculate();
            }

            CConvolutionGaborKernel2D::~CConvolutionGaborKernel2D()
                = default;

            void CConvolutionGaborKernel2D::Calculate()
            {
                const coordinate Radius = (m_Kernel.GetWidth() - 1) / 2;
                const real Nx = m_Orientation.GetX();
                const real Ny = m_Orientation.GetY();
                const real ExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(m_Sigma);
                const real AngularNormalization = _REAL_PI_ / (real(Radius) * _REAL_SQRT2_);
                real* pData = m_Kernel.GetBeginWritableBuffer();
                for (int y = -Radius; y <= Radius; ++y)
                {
                    const real Pyy = Ny * real(y);
                    const real Pxy = Nx * real(y);
                    for (int x = -Radius; x <= Radius; ++x, ++pData)
                    {
                        const real ParallelProjection = Nx * real(x) + Pyy;
                        const real Xp = m_Gamma1 * ParallelProjection;
                        const real Yp = m_Gamma2 * (Pxy - Ny * real(x));
                        *pData = RealExp((Xp * Xp + Yp * Yp) * ExponentFactor) * RealSinus(m_Lambda * ParallelProjection * AngularNormalization);
                    }
                }
            }

            void CConvolutionGaborKernel2D::Scale(const real ScaleValue)
            {
                if (RealAbs(ScaleValue) > _REAL_EPSILON_)
                {
                    const real* const pDataEnd = m_Kernel.GetEndReadOnlyBuffer();
                    real* pData = m_Kernel.GetBeginWritableBuffer();
                    while (pData < pDataEnd)
                    {
                        *pData = *pData * ScaleValue;
                        ++pData;
                    }
                }
            }

            void CConvolutionGaborKernel2D::RemoveSpuriousValue(const real Value)
            {
                if (RealAbs(Value) > _REAL_EPSILON_)
                {
                    const real* const pDataEnd = m_Kernel.GetEndReadOnlyBuffer();
                    real* pData = m_Kernel.GetBeginWritableBuffer();
                    while (pData < pDataEnd)
                    {
                        if (RealAbs(*pData) < Value)
                        {
                            *pData = _REAL_ZERO_;
                        }
                        ++pData;
                    }
                }
            }

            void CConvolutionGaborKernel2D::ScaleMinimalNonZero(const real ScaleValue)
            {
                if (RealAbs(ScaleValue) > _REAL_ZERO_)
                {
                    const real* const pDataEnd = m_Kernel.GetEndReadOnlyBuffer();
                    const real* pDataScanning = m_Kernel.GetBeginReadOnlyBuffer();
                    real MinimalNonZero = _REAL_MAX_;
                    while (pDataScanning < pDataEnd)
                    {
                        const real CurrentAbsoluteValue = RealAbs(*pDataScanning++);
                        if ((CurrentAbsoluteValue > _REAL_EPSILON_) && (CurrentAbsoluteValue < MinimalNonZero))
                        {
                            MinimalNonZero = CurrentAbsoluteValue;
                        }
                    }
                    if (MinimalNonZero > _REAL_EPSILON_)
                    {
                        const real ScaleFactor = ScaleValue / MinimalNonZero;
                        real* pData = m_Kernel.GetBeginWritableBuffer();
                        while (pData < pDataEnd)
                        {
                            *pData = *pData * ScaleFactor;
                            if (RealAbs(*pData) < ScaleValue)
                            {
                                *pData = _REAL_ZERO_;
                            }
                            ++pData;
                        }
                    }
                }
            }

            real CConvolutionGaborKernel2D::GetMaximal() const
            {
                const real* const pDataEnd = m_Kernel.GetEndReadOnlyBuffer();
                const real* pDataScanning = m_Kernel.GetBeginReadOnlyBuffer();
                real Maximal = RealAbs(*pDataScanning);
                while (pDataScanning < pDataEnd)
                {
                    const real CurrentAbsoluteValue = RealAbs(*pDataScanning++);
                    if (CurrentAbsoluteValue > Maximal)
                    {
                        Maximal = CurrentAbsoluteValue;
                    }
                }
                return Maximal;
            }

            real CConvolutionGaborKernel2D::GetMinimalNonZero() const
            {
                const real* const pDataEnd = m_Kernel.GetEndReadOnlyBuffer();
                const real* pDataScanning = m_Kernel.GetBeginReadOnlyBuffer();
                real MinimalNonZero = _REAL_MAX_;
                while (pDataScanning < pDataEnd)
                {
                    const real CurrentAbsoluteValue = RealAbs(*pDataScanning++);
                    if ((CurrentAbsoluteValue > _REAL_EPSILON_) && (CurrentAbsoluteValue < MinimalNonZero))
                    {
                        MinimalNonZero = CurrentAbsoluteValue;
                    }
                }
                return MinimalNonZero;
            }
        }
    }
}
