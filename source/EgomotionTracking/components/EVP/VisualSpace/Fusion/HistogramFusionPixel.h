/*
 * HistogramFusionPixel.h
 *
 *  Created on: 05.07.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "../../Foundation/DataTypes/PixelLocation.h"
#include "../../Foundation/Mathematics/1D/Common1D.h"
#include "../../Foundation/Mathematics/1D/ExponentialLookUpTable.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Fusion
        {
            class CHistogramFusionPixel
            {
            public:

                static void SetGlobalParameters(uint TotalSamples, real BandwidthFactor, real RangePrecision = real(0.03125));

                static  uint GetTotalSamples()
                {
                    return s_TotalSamples;
                }

                static  real GetNormalization()
                {
                    return s_Normalization;
                }

                static  real GetBandwidthFactor()
                {
                    return s_BandwidthFactor;
                }

                static  real GetRangePrecision()
                {
                    return s_RangePrecision;
                }

                CHistogramFusionPixel();

                virtual ~CHistogramFusionPixel();

                inline void AddFirstSample(const byte Value) _EVP_FAST_CALL_
                {
                    m_Accumulator = m_Maximal = m_Minimal = Value;
                    ++m_pData[Value];
                }

                inline void AddSample(const byte Value) _EVP_FAST_CALL_
                {
                    m_Accumulator += Value;
                    if (Value > m_Maximal)
                    {
                        m_Maximal = Value;
                    }
                    else if (Value < m_Minimal)
                    {
                        m_Minimal = Value;
                    }
                    ++m_pData[Value];
                }

                inline void ClearSamples()
                {
                    memset(m_pData + m_Minimal, 0, sizeof(ushort) * (m_Maximal - m_Minimal + 1));
                    m_Accumulator = 0;
                }

                real OptimizedContinuousEpanechnikovFusion();
                real ContinuousEpanechnikovFusion();

                real OptimizedContinuousGaussianFusion();
                real ContinuouGaussianFusion();

                ushort GetMaximaFrequency();

                inline  real GetFusionValue() const
                {
                    return m_FusionValue;
                }

                inline real GetMean() const
                {
                    return real(m_Accumulator) * s_Normalization;
                }

                real GetStandardDeviation() const;

                real GetSignalToNoiseRatio() const;

                inline  real GetRange() const
                {
                    return real(m_Maximal - m_Minimal);
                }

                inline   real GetMaximal() const
                {
                    return m_Maximal;
                }

                inline   real GetMinimal() const
                {
                    return m_Minimal;
                }

                const ushort* GetHistogram() const
                {
                    return m_pData;
                }

                real GetDispersion() const
                {
                    return m_FusionValue - (real(m_Accumulator) * s_Normalization);
                }

                bool ExportToFile(const_string pPathFileName, CPixelLocation* pLocation) const;

            protected:

                inline  real EstimateEpanechnikovDensity(const real ValueLocation, const real BandWidth);
                inline  real EstimateGaussianDensity(const real ValueLocation, const real BandWidth, const real BandWidthExponentFactor);
                inline  list<real> ExtractGlobalMaxima(const ushort MaximaFrequency);

                static uint s_TotalSamples;
                static real s_Normalization;
                static real s_BandwidthFactor;
                static real s_RangePrecision;

                real m_FusionValue;
                uint m_Accumulator;
                byte m_Minimal;
                byte m_Maximal;
                ushort* m_pData;

#ifdef _HISTOGRAMFUSIONPIXEL_USE_LOOKUP_TABLE_
                DECLARE_EXPONENTIAL_LOOKUP_TABLE
#endif

            };
        }
    }
}

