/*
 * FusionPixel.cpp
 *
 *  Created on: 25.03.2011
 *      Author: gonzalez
 */

#include "../../Foundation/Files/OutFile.h"
#include "FusionPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Fusion
        {
            CFusionPixel::CFusionPixel() :
                m_Accumulator(_REAL_ZERO_), m_Minimal(_REAL_MAX_), m_Maximal(_REAL_MIN_), m_Mean(_REAL_ZERO_), m_Median(_REAL_ZERO_), m_Range(_REAL_ZERO_), m_StandardDeviation(_REAL_ZERO_), m_FusionValue(_REAL_ZERO_), m_SignalToNoiseRatio(_REAL_ZERO_), m_SpatioTemporalMinimal(_REAL_ZERO_), m_SpatioTemporalMaximal(_REAL_ZERO_), m_SpatioTemporalIntensityAccumulator(_REAL_ZERO_), m_SpatioTemporalKernelAccumulator(_REAL_ZERO_)
            {
            }

            CFusionPixel::~CFusionPixel()
                = default;

            void CFusionPixel::ReserveSamplesStorage(const uint TotalSamples)
            {
                m_Samples.reserve(TotalSamples);
            }

            void CFusionPixel::AddFirstSample(const real Z)
            {
                m_Accumulator = m_Minimal = m_Maximal = Z;
                m_Samples.push_back(Z);
            }

            void CFusionPixel::AddSample(const real Z)
            {
                if (Z > m_Maximal)
                {
                    m_Maximal = Z;
                }
                else if (Z < m_Minimal)
                {
                    m_Minimal = Z;
                }
                m_Accumulator += Z;
                m_Samples.push_back(Z);
            }

            void CFusionPixel::ClearSamples()
            {
                m_Accumulator = m_SignalToNoiseRatio = m_FusionValue = m_StandardDeviation = m_Range = m_Median = m_Mean = _REAL_ZERO_;
                m_Minimal = _REAL_MAX_;
                m_Maximal = _REAL_MIN_;
                m_Samples.clear();
            }

            real CFusionPixel::MeanFusion(const uint OffsetDelta)
            {
                m_Range = m_Maximal - m_Minimal;
                if (OffsetDelta && (m_Range > _FUSION_MINIMAL_DELTA_))
                {
                    std::vector<real> SortedSamples = m_Samples;
                    TSort(SortedSamples.begin(), SortedSamples.end());
                    std::vector<real>::const_iterator BeginSelectedSamples = SortedSamples.begin() + OffsetDelta;
                    std::vector<real>::const_iterator EndSelectedSamples = SortedSamples.end() - OffsetDelta;
                    if ((*EndSelectedSamples - *BeginSelectedSamples) > _FUSION_MINIMAL_DELTA_)
                    {
                        real FusionAccumulator = _REAL_ZERO_;
                        for (std::vector<real>::const_iterator pSample = BeginSelectedSamples; pSample != EndSelectedSamples; ++pSample)
                        {
                            FusionAccumulator += *pSample;
                        }
                        m_FusionValue = FusionAccumulator / real(SortedSamples.size() - OffsetDelta * 2);
                    }
                    else
                    {
                        m_FusionValue = (*EndSelectedSamples + *BeginSelectedSamples) * _REAL_HALF_;
                    }
                }
                else
                {
                    m_FusionValue = m_Mean = (m_Accumulator / real(m_Samples.size()));
                }
                return m_FusionValue;
            }

            real CFusionPixel::EpanechnikovFusion(const real KernelBandWidth, const real Precision, const real SubPrecision, const uint OffsetDelta)
            {
                uint TotalSamples = m_Samples.size();
                m_FusionValue = m_Mean = (m_Accumulator / real(TotalSamples));
                m_Range = m_Maximal - m_Minimal;
                if (m_Range > Precision)
                {
                    real ValueInitial = m_Minimal;
                    real ValueFinal = m_Maximal;
                    real SubRange = m_Range;
                    std::vector<real> SortedSamples;
                    std::vector<real>::const_iterator BeginSelectedSamples = m_Samples.begin();
                    std::vector<real>::const_iterator EndSelectedSamples = m_Samples.end();
                    if (OffsetDelta)
                    {
                        SortedSamples = m_Samples;
                        TSort(SortedSamples.begin(), SortedSamples.end());
                        BeginSelectedSamples = SortedSamples.begin() + OffsetDelta;
                        EndSelectedSamples = SortedSamples.end() - OffsetDelta;
                        ValueInitial = *BeginSelectedSamples;
                        ValueFinal = *EndSelectedSamples;
                        SubRange = ValueFinal - ValueInitial;
                    }
                    if (SubRange > Precision)
                    {
                        real FusionValue = _REAL_ZERO_;
                        real MaximalDensityAccumulator = _REAL_ZERO_;
                        const real SafeKernelBandWidth = TMin(KernelBandWidth, SubRange);
                        for (real Value = ValueInitial; Value < ValueFinal; Value += Precision)
                        {
                            real DensityAccumulator = _REAL_ZERO_;
                            for (std::vector<real>::const_iterator pSample = BeginSelectedSamples; pSample != EndSelectedSamples; ++pSample)
                            {
                                real Variance = RealAbs(*pSample - Value);
                                if (Variance < SafeKernelBandWidth)
                                {
                                    Variance /= SafeKernelBandWidth;
                                    DensityAccumulator += (_REAL_ONE_ - (Variance * Variance));
                                }
                            }
                            if (DensityAccumulator > MaximalDensityAccumulator)
                            {
                                MaximalDensityAccumulator = DensityAccumulator;
                                FusionValue = Value;
                            }
                        }
                        ValueInitial = FusionValue - Precision;
                        ValueFinal = FusionValue + Precision;
                        for (real Value = ValueInitial; Value < ValueFinal; Value += SubPrecision)
                        {
                            real DensityAccumulator = _REAL_ZERO_;
                            for (std::vector<real>::const_iterator pSample = BeginSelectedSamples; pSample != EndSelectedSamples; ++pSample)
                            {
                                real Variance = RealAbs(*pSample - Value);
                                if (Variance < SafeKernelBandWidth)
                                {
                                    Variance /= SafeKernelBandWidth;
                                    DensityAccumulator += (_REAL_ONE_ - (Variance * Variance));
                                }
                            }
                            if (DensityAccumulator > MaximalDensityAccumulator)
                            {
                                MaximalDensityAccumulator = DensityAccumulator;
                                FusionValue = Value;
                            }
                        }
                        m_FusionValue = FusionValue;
                    }
                    else
                    {
                        m_FusionValue = (ValueFinal + ValueInitial) * _REAL_HALF_;
                    }
                }
                return m_FusionValue;
            }

            real CFusionPixel::GetDeviationSemiContinuousGaussianFusion(real Precision, real* pKernel, const uint Radius, const uint OffsetDelta, real* pSemiContinousDensity, const uint MaximalBins, real& SubRange, uint SubSamples)
            {
                real DeviationSemiContinuousGaussianFusion = SubRange = _REAL_ZERO_;
                if (m_Range > Precision)
                {
                    real Minimal = m_Samples.front();
                    real Maximal = Minimal;
                    std::vector<real> SelectedSubSamples;
                    for (uint i = 0; i < SubSamples; ++i)
                    {
                        real Sample = m_Samples[i];
                        if (Sample > Maximal)
                        {
                            Maximal = Sample;
                        }
                        else if (Sample < Minimal)
                        {
                            Minimal = Sample;
                        }
                        SelectedSubSamples.push_back(Sample);
                    }
                    SubRange = Maximal - Minimal;
                    if (SubRange > Precision)
                    {
                        real ValueInitial = Minimal;
                        real ValueFinal = Maximal;
                        std::vector<real> SortedSamples;
                        std::vector<real>::const_iterator BeginSelectedSamples = SelectedSubSamples.begin();
                        std::vector<real>::const_iterator EndSelectedSamples = SelectedSubSamples.end();
                        if (OffsetDelta)
                        {
                            SortedSamples = SelectedSubSamples;
                            TSort(SortedSamples.begin(), SortedSamples.end());
                            BeginSelectedSamples = SortedSamples.begin() + OffsetDelta;
                            EndSelectedSamples = SortedSamples.end() - OffsetDelta;
                            ValueInitial = *BeginSelectedSamples;
                            ValueFinal = *EndSelectedSamples;
                            SubRange = ValueFinal - ValueInitial;
                        }
                        if (SubRange > Precision)
                        {
                            uint TotalBins = TMin(uint(SubRange / Precision) + 1, MaximalBins);
                            const real ScaleFactor = real(TotalBins - 1) / SubRange;
                            memset(pSemiContinousDensity, 0, sizeof(real) * TotalBins);
                            for (std::vector<real>::const_iterator pSample = BeginSelectedSamples; pSample != EndSelectedSamples; ++pSample)
                            {
                                int Location = RoundToInteger((*pSample - ValueInitial) * ScaleFactor);
                                for (uint i = 0, AuxiliarLocation = Location; (i < Radius) && (AuxiliarLocation < TotalBins); ++i, ++AuxiliarLocation)
                                {
                                    pSemiContinousDensity[AuxiliarLocation] += pKernel[i];
                                }
                                for (uint i = 1, AuxiliarLocation = Location - 1; (i < Radius); ++i, --AuxiliarLocation)
                                {
                                    pSemiContinousDensity[AuxiliarLocation] += pKernel[i];
                                }
                            }
                            int MaximalIndex = 0;
                            real MaximalDensity = _REAL_ZERO_;
                            real* pDensityBin = pSemiContinousDensity;
                            for (uint i = 0; i < TotalBins; ++i, ++pDensityBin)
                                if (*pDensityBin > MaximalDensity)
                                {
                                    MaximalDensity = *pDensityBin;
                                    MaximalIndex = i;
                                }
                            DeviationSemiContinuousGaussianFusion = ((real(MaximalIndex) / ScaleFactor) + ValueInitial) - m_FusionValue;
                        }
                        else
                        {
                            m_FusionValue = (ValueFinal + ValueInitial) * _REAL_HALF_;
                        }
                    }
                }
                return DeviationSemiContinuousGaussianFusion;
            }

            real CFusionPixel::GaussianFusion(const real ExponentFactor, const real Precision, const real SubPrecision, const uint OffsetDelta)
            {
                uint TotalSamples = m_Samples.size();
                m_FusionValue = m_Mean = (m_Accumulator / real(TotalSamples));
                m_Range = m_Maximal - m_Minimal;
                if (m_Range > Precision)
                {
                    real ValueInitial = m_Minimal;
                    real ValueFinal = m_Maximal;
                    real SubRange = m_Range;
                    std::vector<real> SortedSamples;
                    std::vector<real>::const_iterator BeginSelectedSamples = m_Samples.begin();
                    std::vector<real>::const_iterator EndSelectedSamples = m_Samples.end();
                    if (OffsetDelta)
                    {
                        SortedSamples = m_Samples;
                        TSort(SortedSamples.begin(), SortedSamples.end());
                        BeginSelectedSamples = SortedSamples.begin() + OffsetDelta;
                        EndSelectedSamples = SortedSamples.end() - OffsetDelta;
                        ValueInitial = *BeginSelectedSamples;
                        ValueFinal = *EndSelectedSamples;
                        SubRange = ValueFinal - ValueInitial;
                    }
                    if (SubRange > Precision)
                    {
                        real FusionValue = _REAL_ZERO_;
                        real MaximalDensityAccumulator = _REAL_ZERO_;
                        for (real Value = ValueInitial; Value < ValueFinal; Value += Precision)
                        {
                            real DensityAccumulator = _REAL_ZERO_;
                            for (std::vector<real>::const_iterator pSample = BeginSelectedSamples; pSample != EndSelectedSamples; ++pSample)
                            {
                                real Variance = *pSample - Value;
                                DensityAccumulator += RealExp(Variance * Variance * ExponentFactor);
                            }
                            if (DensityAccumulator > MaximalDensityAccumulator)
                            {
                                MaximalDensityAccumulator = DensityAccumulator;
                                FusionValue = Value;
                            }
                        }
                        ValueInitial = FusionValue - Precision;
                        ValueFinal = FusionValue + Precision;
                        for (real Value = ValueInitial; Value < ValueFinal; Value += SubPrecision)
                        {
                            real DensityAccumulator = _REAL_ZERO_;
                            for (std::vector<real>::const_iterator pSample = BeginSelectedSamples; pSample != EndSelectedSamples; ++pSample)
                            {
                                real Variance = *pSample - Value;
                                DensityAccumulator += RealExp(Variance * Variance * ExponentFactor);
                            }
                            if (DensityAccumulator > MaximalDensityAccumulator)
                            {
                                MaximalDensityAccumulator = DensityAccumulator;
                                FusionValue = Value;
                            }
                        }
                        m_FusionValue = FusionValue;
                    }
                    else
                    {
                        m_FusionValue = (ValueFinal + ValueInitial) * _REAL_HALF_;
                    }
                }
                return m_FusionValue;
            }

            real CFusionPixel::GetDeviationGaussianFusion(real ExponentFactor, real Precision, real SubPrecision, const uint OffsetDelta, real& SubRange, uint SubSamples)
            {
                real DeviationGaussianFusion = SubRange = _REAL_ZERO_;
                if (m_Range > Precision)
                {
                    real Minimal = m_Samples.front();
                    real Maximal = Minimal;
                    std::vector<real> SelectedSubSamples;
                    for (uint i = 0; i < SubSamples; ++i)
                    {
                        real Sample = m_Samples[i];
                        if (Sample > Maximal)
                        {
                            Maximal = Sample;
                        }
                        else if (Sample < Minimal)
                        {
                            Minimal = Sample;
                        }
                        SelectedSubSamples.push_back(Sample);
                    }
                    SubRange = Maximal - Minimal;
                    if (SubRange > SubPrecision)
                    {
                        real ValueInitial = Minimal;
                        real ValueFinal = Maximal;
                        std::vector<real>::const_iterator BeginSelectedSamples = SelectedSubSamples.begin();
                        std::vector<real>::const_iterator EndSelectedSamples = SelectedSubSamples.end();
                        if (OffsetDelta)
                        {
                            TSort(SelectedSubSamples.begin(), SelectedSubSamples.end());
                            BeginSelectedSamples = SelectedSubSamples.begin() + OffsetDelta;
                            EndSelectedSamples = SelectedSubSamples.end() - OffsetDelta;
                            ValueInitial = *BeginSelectedSamples;
                            ValueFinal = *EndSelectedSamples;
                            SubRange = ValueFinal - ValueInitial;
                        }
                        if (SubRange > Precision)
                        {
                            real SelectedSubFusionValue = _REAL_ZERO_;
                            real MaximalDensityAccumulator = _REAL_ZERO_;
                            for (real Value = ValueInitial; Value < ValueFinal; Value += Precision)
                            {
                                real DensityAccumulator = _REAL_ZERO_;
                                for (std::vector<real>::const_iterator pSample = BeginSelectedSamples; pSample != EndSelectedSamples; ++pSample)
                                {
                                    real Variance = *pSample - Value;
                                    DensityAccumulator += RealExp(Variance * Variance * ExponentFactor);
                                }
                                if (DensityAccumulator > MaximalDensityAccumulator)
                                {
                                    MaximalDensityAccumulator = DensityAccumulator;
                                    SelectedSubFusionValue = Value;
                                }
                            }
                            ValueInitial = SelectedSubFusionValue - Precision;
                            ValueFinal = SelectedSubFusionValue + Precision;
                            for (real Value = ValueInitial; Value < ValueFinal; Value += SubPrecision)
                            {
                                real DensityAccumulator = _REAL_ZERO_;
                                for (std::vector<real>::const_iterator pSample = BeginSelectedSamples; pSample != EndSelectedSamples; ++pSample)
                                {
                                    real Variance = *pSample - Value;
                                    DensityAccumulator += RealExp(Variance * Variance * ExponentFactor);
                                }
                                if (DensityAccumulator > MaximalDensityAccumulator)
                                {
                                    MaximalDensityAccumulator = DensityAccumulator;
                                    SelectedSubFusionValue = Value;
                                }
                            }
                            DeviationGaussianFusion = SelectedSubFusionValue - m_FusionValue;
                        }
                        else
                        {
                            m_FusionValue = (ValueFinal + ValueInitial) * _REAL_HALF_;
                        }
                    }
                }
                return DeviationGaussianFusion;
            }

            real CFusionPixel::SemiContinuousGaussianFusionKernelAdaptive(real Precision, real SigmaFactor, const uint OffsetDelta, real* pKernel, real* pSemiContinousDensity, const uint MaximalBins)
            {
                uint TotalSamples = m_Samples.size();
                m_FusionValue = m_Mean = (m_Accumulator / real(TotalSamples));
                m_Range = m_Maximal - m_Minimal;
                if (m_Range > Precision)
                {
                    real ValueInitial = m_Minimal;
                    real ValueFinal = m_Maximal;
                    real SubRange = m_Range;
                    std::vector<real> SortedSamples;
                    std::vector<real>::const_iterator BeginSelectedSamples = m_Samples.begin();
                    std::vector<real>::const_iterator EndSelectedSamples = m_Samples.end();
                    if (OffsetDelta)
                    {
                        SortedSamples = m_Samples;
                        TSort(SortedSamples.begin(), SortedSamples.end());
                        BeginSelectedSamples = SortedSamples.begin() + OffsetDelta;
                        EndSelectedSamples = SortedSamples.end() - OffsetDelta;
                        ValueInitial = *BeginSelectedSamples;
                        ValueFinal = *EndSelectedSamples;
                        SubRange = ValueFinal - ValueInitial;
                    }
                    if (SubRange > Precision)
                    {
                        uint TotalBins = TMin(uint(SubRange / Precision) + 1, MaximalBins);
                        real ScaleFactor = real(TotalBins - 1) / SubRange;
                        memset(pSemiContinousDensity, 0, sizeof(real) * TotalBins);
                        real Sigma = SubRange * SigmaFactor * ScaleFactor;
                        uint Radius = TMax(TMin(uint(RealCeil(Sigma * _REAL_THREE_)), MaximalBins), uint(1));
                        const real ExponetFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(Sigma);
                        pKernel[1] = RealExp(ExponetFactor);
                        for (uint i = 2; i < Radius; ++i)
                        {
                            pKernel[i] = RealExp(real(i * i) * ExponetFactor);
                        }
                        for (std::vector<real>::const_iterator pSample = BeginSelectedSamples; pSample != EndSelectedSamples; ++pSample)
                        {
                            uint Location = RoundToInteger((*pSample - ValueInitial) * ScaleFactor);
                            for (uint i = 0, AuxiliarLocation = Location; (i < Radius) && (AuxiliarLocation < TotalBins); ++i, ++AuxiliarLocation)
                            {
                                pSemiContinousDensity[AuxiliarLocation] += pKernel[i];
                            }
                            for (uint i = 1, AuxiliarLocation = Location - 1; (i < Radius) ; ++i, --AuxiliarLocation)
                            {
                                pSemiContinousDensity[AuxiliarLocation] += pKernel[i];
                            }
                        }
                        uint MaximalIndex = 0;
                        real MaximalDensity = _REAL_ZERO_;
                        for (uint i = 0; i < TotalBins; ++i, ++pSemiContinousDensity)
                            if (*pSemiContinousDensity > MaximalDensity)
                            {
                                MaximalDensity = *pSemiContinousDensity;
                                MaximalIndex = i;
                            }
                        m_FusionValue = (real(MaximalIndex) / ScaleFactor) + ValueInitial;
                    }
                    else
                    {
                        m_FusionValue = (ValueFinal + ValueInitial) * _REAL_HALF_;
                    }
                }
                return m_FusionValue;
            }

            real CFusionPixel::SemiContinuousGaussianFusion(real Precision, real* pKernel, const uint Radius, const uint OffsetDelta, real* pSemiContinousDensity, const uint MaximalBins)
            {
                uint TotalSamples = m_Samples.size();
                m_FusionValue = m_Mean = (m_Accumulator / real(TotalSamples));
                m_Range = m_Maximal - m_Minimal;
                if (m_Range > Precision)
                {
                    real ValueInitial = m_Minimal;
                    real ValueFinal = m_Maximal;
                    real SubRange = m_Range;
                    std::vector<real> SortedSamples;
                    std::vector<real>::const_iterator BeginSelectedSamples = m_Samples.begin();
                    std::vector<real>::const_iterator EndSelectedSamples = m_Samples.end();
                    if (OffsetDelta)
                    {
                        SortedSamples = m_Samples;
                        TSort(SortedSamples.begin(), SortedSamples.end());
                        BeginSelectedSamples = SortedSamples.begin() + OffsetDelta;
                        EndSelectedSamples = SortedSamples.end() - OffsetDelta;
                        ValueInitial = *BeginSelectedSamples;
                        ValueFinal = *EndSelectedSamples;
                        SubRange = ValueFinal - ValueInitial;
                    }
                    if (SubRange > Precision)
                    {
                        uint TotalBins = TMin(uint(RealCeil(SubRange / Precision)) + 1, MaximalBins);
                        real ScaleFactor = real(TotalBins - 1) / SubRange;
                        memset(pSemiContinousDensity, 0, sizeof(real) * TotalBins);
                        for (std::vector<real>::const_iterator pSample = BeginSelectedSamples; pSample != EndSelectedSamples; ++pSample)
                        {
                            uint Location = RoundToInteger((*pSample - ValueInitial) * ScaleFactor);
                            for (uint i = 0, AuxiliarLocation = Location; (i < Radius) && (AuxiliarLocation < TotalBins); ++i, ++AuxiliarLocation)
                            {
                                pSemiContinousDensity[AuxiliarLocation] += pKernel[i];
                            }
                            for (uint i = 1, AuxiliarLocation = Location - 1; (i < Radius); ++i, --AuxiliarLocation)
                            {
                                pSemiContinousDensity[AuxiliarLocation] += pKernel[i];
                            }
                        }
                        int MaximalIndex = 0;
                        real MaximalDensity = _REAL_ZERO_;
                        for (uint i = 0; i < TotalBins; ++i, ++pSemiContinousDensity)
                            if (*pSemiContinousDensity > MaximalDensity)
                            {
                                MaximalDensity = *pSemiContinousDensity;
                                MaximalIndex = i;
                            }
                        m_FusionValue = (real(MaximalIndex) / ScaleFactor) + ValueInitial;
                    }
                    else
                    {
                        m_FusionValue = (ValueFinal + ValueInitial) * _REAL_HALF_;
                    }
                }
                return m_FusionValue;
            }

            void CFusionPixel::StatisticalAnalysis(const uint OffsetDelta)
            {
                std::vector<real> SortedSamples = m_Samples;
                TSort(SortedSamples.begin(), SortedSamples.end());
                const real MedianPosition = real(m_Samples.size() - 1) * _REAL_HALF_;
                const int PositionA = DownToInteger(MedianPosition);
                const int PositionB = UpperToInteger(MedianPosition);
                m_Median = SortedSamples[PositionA] * (real(PositionB) - MedianPosition) + SortedSamples[PositionB] * (MedianPosition - real(PositionA));
                if (OffsetDelta)
                {
                    real Accumulator = _REAL_ZERO_;
                    std::vector<real>::const_iterator BeginSelectedSamples = SortedSamples.begin() + OffsetDelta;
                    std::vector<real>::const_iterator EndSelectedSamples = SortedSamples.end() - OffsetDelta;
                    for (std::vector<real>::const_iterator pSample = BeginSelectedSamples; pSample != EndSelectedSamples; ++pSample)
                    {
                        Accumulator += *pSample;
                    }
                    const real Normalization = _REAL_ONE_ / real(SortedSamples.size() - OffsetDelta * 2);
                    m_Mean = Accumulator * Normalization;
                    m_Range = *EndSelectedSamples - *BeginSelectedSamples;
                    if (m_Range > _FUSION_MINIMAL_DELTA_)
                    {
                        real AccumulatorStandardDeviation = _REAL_ZERO_;
                        for (std::vector<real>::const_iterator pSample = BeginSelectedSamples; pSample != EndSelectedSamples; ++pSample)
                        {
                            const real Variance = *pSample - m_Mean;
                            AccumulatorStandardDeviation += Variance * Variance;
                        }
                        m_StandardDeviation = RealSqrt(AccumulatorStandardDeviation * Normalization);
                        m_SignalToNoiseRatio = (m_StandardDeviation > _FUSION_MINIMAL_DELTA_) ? real(20.0) * RealLog10(m_Mean / m_StandardDeviation) : _REAL_ZERO_;
                    }
                    else
                    {
                        m_Median = m_Mean;
                        m_SignalToNoiseRatio = m_StandardDeviation = _REAL_ZERO_;
                    }
                }
                else
                {
                    m_Mean = (m_Accumulator / real(m_Samples.size()));
                    m_Range = m_Maximal - m_Minimal;
                    if (m_Range > _FUSION_MINIMAL_DELTA_)
                    {
                        const real Normalization = _REAL_ONE_ / real(SortedSamples.size());
                        real AccumulatorStandardDeviation = _REAL_ZERO_;
                        std::vector<real>::const_iterator EndSelectedSamples = SortedSamples.end();
                        for (std::vector<real>::const_iterator pSample = SortedSamples.begin(); pSample != EndSelectedSamples; ++pSample)
                        {
                            const real Variance = *pSample - m_Mean;
                            AccumulatorStandardDeviation += Variance * Variance;
                        }
                        m_StandardDeviation = RealSqrt(AccumulatorStandardDeviation * Normalization);
                        m_SignalToNoiseRatio = (m_StandardDeviation > _FUSION_MINIMAL_DELTA_) ? real(20.0) * RealLog10(m_Mean / m_StandardDeviation) : _REAL_ZERO_;
                    }
                    else
                    {
                        m_Median = m_Mean;
                        m_SignalToNoiseRatio = m_StandardDeviation = _REAL_ZERO_;
                    }
                }
            }

            real CFusionPixel::GetMaximalAbsoluteDeviation(const uint OffsetDelta)
            {
                if (OffsetDelta)
                {
                    std::vector<real> SortedSamples = m_Samples;
                    TSort(SortedSamples.begin(), SortedSamples.end());
                    return TMax(RealAbs(*(SortedSamples.begin() + OffsetDelta) - m_FusionValue), RealAbs(*(SortedSamples.end() - OffsetDelta) - m_FusionValue));
                }
                else
                {
                    real MaximalAbsoluteDeviation = _REAL_ZERO_;
                    std::vector<real>::const_iterator EndSelectedSamples = m_Samples.end();
                    for (std::vector<real>::const_iterator pSample = m_Samples.begin(); pSample != EndSelectedSamples; ++pSample)
                    {
                        const real Deviation = RealAbs(*pSample - m_FusionValue);
                        if (Deviation > MaximalAbsoluteDeviation)
                        {
                            MaximalAbsoluteDeviation = Deviation;
                        }
                    }
                    return MaximalAbsoluteDeviation;
                }
            }

            real CFusionPixel::GetAccumulatedAbsoluteDeviation(const uint OffsetDelta)
            {
                if (OffsetDelta)
                {
                    std::vector<real> SortedSamples = m_Samples;
                    TSort(SortedSamples.begin(), SortedSamples.end());
                    real AccumulatedAbsoluteDeviation = _REAL_ZERO_;
                    std::vector<real>::const_iterator EndSelectedSamples = SortedSamples.end() - OffsetDelta;
                    for (std::vector<real>::const_iterator pSample = SortedSamples.begin() + OffsetDelta; pSample != EndSelectedSamples; ++pSample)
                    {
                        AccumulatedAbsoluteDeviation += RealAbs(*pSample - m_FusionValue);
                    }
                    return AccumulatedAbsoluteDeviation;
                }
                else
                {
                    real AccumulatedAbsoluteDeviation = _REAL_ZERO_;
                    std::vector<real>::const_iterator EndSelectedSamples = m_Samples.end();
                    for (std::vector<real>::const_iterator pSample = m_Samples.begin(); pSample != EndSelectedSamples; ++pSample)
                    {
                        AccumulatedAbsoluteDeviation += RealAbs(*pSample - m_FusionValue);
                    }
                    return AccumulatedAbsoluteDeviation;
                }
            }

            real CFusionPixel::GetMeanAbsoluteDeviation(const uint OffsetDelta)
            {
                if (OffsetDelta)
                {
                    std::vector<real> SortedSamples = m_Samples;
                    TSort(SortedSamples.begin(), SortedSamples.end());
                    real AccumulatedAbsoluteDeviation = _REAL_ZERO_;
                    std::vector<real>::const_iterator EndSelectedSamples = SortedSamples.end() - OffsetDelta;
                    for (std::vector<real>::const_iterator pSample = SortedSamples.begin() + OffsetDelta; pSample != EndSelectedSamples; ++pSample)
                    {
                        AccumulatedAbsoluteDeviation += RealAbs(*pSample - m_FusionValue);
                    }
                    return AccumulatedAbsoluteDeviation / real(SortedSamples.size() - OffsetDelta * 2);
                }
                else
                {
                    real AccumulatedAbsoluteDeviation = _REAL_ZERO_;
                    std::vector<real>::const_iterator EndSelectedSamples = m_Samples.end();
                    for (std::vector<real>::const_iterator pSample = m_Samples.begin(); pSample != EndSelectedSamples; ++pSample)
                    {
                        AccumulatedAbsoluteDeviation += RealAbs(*pSample - m_FusionValue);
                    }
                    return AccumulatedAbsoluteDeviation / real(m_Samples.size());
                }
            }

            real CFusionPixel::GetRMSDeviation(const uint OffsetDelta)
            {
                if (OffsetDelta)
                {
                    std::vector<real> SortedSamples = m_Samples;
                    TSort(SortedSamples.begin(), SortedSamples.end());
                    real AccumulatorDeviation = _REAL_ZERO_;
                    std::vector<real>::const_iterator EndSelectedSamples = SortedSamples.end() - OffsetDelta;
                    for (std::vector<real>::const_iterator pSample = SortedSamples.begin() + OffsetDelta; pSample != EndSelectedSamples; ++pSample)
                    {
                        const real Deviation = *pSample - m_FusionValue;
                        AccumulatorDeviation += Deviation * Deviation;
                    }
                    return RealSqrt(AccumulatorDeviation / real(SortedSamples.size() - OffsetDelta * 2));
                }
                else
                {
                    real AccumulatorDeviation = _REAL_ZERO_;
                    std::vector<real>::const_iterator EndSelectedSamples = m_Samples.end();
                    for (std::vector<real>::const_iterator pSample = m_Samples.begin(); pSample != EndSelectedSamples; ++pSample)
                    {
                        const real Deviation = *pSample - m_FusionValue;
                        AccumulatorDeviation += Deviation * Deviation;
                    }
                    return RealSqrt(AccumulatorDeviation / real(m_Samples.size()));
                }
            }

            void CFusionPixel::GetExtremFusionDeviations(real& Maximal, real& Minimal, const uint OffsetDelta) const
            {
                if (OffsetDelta)
                {
                    std::vector<real> SortedSamples = m_Samples;
                    TSort(SortedSamples.begin(), SortedSamples.end());
                    std::vector<real>::const_iterator BeginSelectedSamples = SortedSamples.begin() + OffsetDelta;
                    std::vector<real>::const_iterator EndSelectedSamples = SortedSamples.end() - OffsetDelta;
                    const real DeviationA = RealAbs(*BeginSelectedSamples - m_FusionValue);
                    const real DeviationB = RealAbs(*EndSelectedSamples - m_FusionValue);
                    Maximal = TMax(DeviationA, DeviationB);
                    Minimal = TMin(DeviationA, DeviationB);
                }
                else
                {
                    const real DeviationA = m_Maximal - m_FusionValue;
                    const real DeviationB = m_FusionValue - m_Minimal;
                    Maximal = TMax(DeviationA, DeviationB);
                    Minimal = TMin(DeviationA, DeviationB);
                }
            }

            bool CFusionPixel::IsSingular() const
            {
                return (m_Range < _FUSION_MINIMAL_DELTA_) || (m_StandardDeviation < _FUSION_MINIMAL_DELTA_);
            }

            bool CFusionPixel::HasDeviation() const
            {
                return (m_Range > _FUSION_MINIMAL_DELTA_);
            }

            real CFusionPixel::GetMean() const
            {
                return m_Mean;
            }

            real CFusionPixel::GetMeadian() const
            {
                return m_Median;
            }

            real CFusionPixel::GetDispersion() const
            {
                return m_FusionValue - m_Mean;
            }

            real CFusionPixel::GetFusionValue() const
            {
                return m_FusionValue;
            }

            real CFusionPixel::GetMaximal() const
            {
                return m_Maximal;
            }

            real CFusionPixel::GetMinimal() const
            {
                return m_Minimal;
            }

            real CFusionPixel::GetRange() const
            {
                return m_Range;
            }

            real CFusionPixel::GetStandardDeviation() const
            {
                return m_StandardDeviation;
            }

            real CFusionPixel::GetSignalToNoiseRatio() const
            {
                return m_SignalToNoiseRatio;
            }

            const std::vector<real>& CFusionPixel::GetSamples() const
            {
                return m_Samples;
            }

            real CFusionPixel::GetSampleByIndex(const uint Index) const
            {
                return m_Samples[Index];
            }

            void CFusionPixel::GetCharacteristics(real& Minimal, real& Maximal, real& Mean, real& Fusion) const
            {
                Minimal = m_Minimal;
                Maximal = m_Maximal;
                Mean = m_Mean;
                Fusion = m_FusionValue;
            }

            bool CFusionPixel::ExportToFile(const_string pPathFileName, CPixelLocation* pLocation)
            {
                std::ostringstream OutputText;
                OutputText << "Fusion Pixel" << g_EndLine;
                if (pLocation)
                {
                    OutputText << g_EndLineTabulator << "Location" << g_Tabulator << "x = " << pLocation->m_x << g_Tabulator << "y = " << pLocation->m_y;
                }
                OutputText << g_EndLineTabulator << "Minimal" << g_Tabulator << m_Minimal;
                OutputText << g_EndLineTabulator << "Maximal" << g_Tabulator << m_Maximal;
                OutputText << g_EndLineTabulator << "Mean" << g_Tabulator << m_Mean;
                OutputText << g_EndLineTabulator << "Median" << g_Tabulator << m_Median;
                OutputText << g_EndLineTabulator << "Range" << g_Tabulator << m_Range;
                OutputText << g_EndLineTabulator << "Standard Deviation" << g_Tabulator << m_StandardDeviation;
                OutputText << g_EndLineTabulator << "Fusion" << g_Tabulator << m_FusionValue;
                OutputText << g_EndLineTabulator << "Signal to Noise Ratio" << g_Tabulator << m_SignalToNoiseRatio;
                OutputText << g_EndLineTabulator << "Sample Values";
                for (uint i = 0; i < m_Samples.size(); ++i)
                {
                    OutputText << g_EndLine << g_DoubleTabulator << m_Samples[i];
                }
                return Files::COutFile::WriteStringToFile(pPathFileName, OutputText.str());
            }

            bool CFusionPixel::ExportDescriptiveStatisticsToFile(const_string pPathFileName, CPixelLocation* pLocation)
            {
                std::ostringstream OutputText;
                OutputText << "Fusion Pixel" << g_EndLine;
                if (pLocation)
                {
                    OutputText << g_EndLineTabulator << "Location" << g_Tabulator << "x = " << pLocation->m_x << g_Tabulator << "y = " << pLocation->m_y;
                }
                OutputText << g_EndLineTabulator << "Minimal" << g_Tabulator << m_Minimal;
                OutputText << g_EndLineTabulator << "Maximal" << g_Tabulator << m_Maximal;
                OutputText << g_EndLineTabulator << "Mean" << g_Tabulator << m_Mean;
                OutputText << g_EndLineTabulator << "Median" << g_Tabulator << m_Median;
                OutputText << g_EndLineTabulator << "Range" << g_Tabulator << m_Range;
                OutputText << g_EndLineTabulator << "Standard Deviation" << g_Tabulator << m_StandardDeviation;
                OutputText << g_EndLineTabulator << "Fusion" << g_Tabulator << m_FusionValue;
                OutputText << g_EndLineTabulator << "Signal to Noise Ratio" << g_Tabulator << m_SignalToNoiseRatio;
                OutputText << g_EndLine << g_DoubleTabulator << "Sample" << g_Tabulator << "Maximal" << g_Tabulator << "Minimal" << g_Tabulator << "Range" << g_Tabulator << "Mean";
                real ScopeMaximal = _REAL_MIN_, ScopeMinimal = _REAL_MAX_, ScopeAccumaltor = _REAL_ZERO_;
                for (uint i = 0; i < m_Samples.size(); ++i)
                {
                    const real Value = m_Samples[i];
                    if (Value > ScopeMaximal)
                    {
                        ScopeMaximal = Value;
                    }
                    if (Value < ScopeMinimal)
                    {
                        ScopeMinimal = Value;
                    }
                    const real ScopeRange = ScopeMaximal - ScopeMinimal;
                    ScopeAccumaltor += Value;
                    OutputText << g_EndLine << g_DoubleTabulator << Value << g_Tabulator << ScopeMaximal << g_Tabulator << ScopeMinimal << g_Tabulator << ScopeRange << g_Tabulator << ScopeAccumaltor / real(i + 1);
                }
                return Files::COutFile::WriteStringToFile(pPathFileName, OutputText.str());
            }

            bool CFusionPixel::ExportPDFToFile(const_string pPathFileName, CPixelLocation* pLocation, real ExponentFactor)
            {
                std::ostringstream OutputText;
                OutputText << "Fusion Pixel" << g_EndLine;
                if (pLocation)
                {
                    OutputText << g_EndLineTabulator << "Location" << g_Tabulator << "x = " << pLocation->m_x << g_Tabulator << "y = " << pLocation->m_y;
                }
                OutputText << g_EndLineTabulator << "Minimal" << g_Tabulator << m_Minimal;
                OutputText << g_EndLineTabulator << "Maximal" << g_Tabulator << m_Maximal;
                OutputText << g_EndLineTabulator << "Mean" << g_Tabulator << m_Mean;
                OutputText << g_EndLineTabulator << "Median" << g_Tabulator << m_Median;
                OutputText << g_EndLineTabulator << "Range" << g_Tabulator << m_Range;
                OutputText << g_EndLineTabulator << "Standard Deviation" << g_Tabulator << m_StandardDeviation;
                OutputText << g_EndLineTabulator << "Fusion" << g_Tabulator << m_FusionValue;
                OutputText << g_EndLineTabulator << "Signal to Noise Ratio" << g_Tabulator << m_SignalToNoiseRatio;
                OutputText << g_EndLine << g_DoubleTabulator << "Sample" << g_Tabulator << "Density";
                real DensityAccumulator = _REAL_ZERO_;
                std::vector<real> SortedSamples = m_Samples;
                TSort(SortedSamples.begin(), SortedSamples.end());
                uint TotalSortedSamples = SortedSamples.size();
                for (uint i = 0; i < TotalSortedSamples; ++i)
                {
                    real Value = SortedSamples[i];
                    DensityAccumulator = _REAL_ZERO_;
                    for (uint j = 0; j < TotalSortedSamples; ++j)
                    {
                        real DeltaValue = SortedSamples[j] - Value;
                        DensityAccumulator += RealExp(DeltaValue * DeltaValue * ExponentFactor);
                    }
                    OutputText << g_EndLine << g_DoubleTabulator << Value << g_Tabulator << DensityAccumulator;
                }
                uint Minimal = DownToInteger(m_Minimal);
                uint Maximal = UpperToInteger(m_Maximal);
                uint Scope = Maximal - Minimal + 1;
                uint* pHistogram = new uint[Scope];
                memset(pHistogram, 0, sizeof(uint) * Scope);
                for (uint i = 0; i < m_Samples.size(); ++i)
                {
                    ++pHistogram[RoundToInteger(m_Samples[i]) - Minimal];
                }
                OutputText << g_EndLine << g_DoubleTabulator << "Intensity" << g_Tabulator << "Frequency";
                for (uint i = 0; i < Scope; ++i)
                {
                    OutputText << g_EndLine << g_DoubleTabulator << Minimal + i << g_Tabulator << pHistogram[i];
                }
                RELEASE_ARRAY(pHistogram)
                return Files::COutFile::WriteStringToFile(pPathFileName, OutputText.str());
            }

            void CFusionPixel::AddSpatioTemporalSample(const real Intensity, const real SpatialKernelContribution)
            {
                if (Intensity > m_SpatioTemporalMaximal)
                {
                    m_SpatioTemporalMaximal = Intensity;
                }
                if (Intensity < m_SpatioTemporalMinimal)
                {
                    m_SpatioTemporalMinimal = Intensity;
                }
                m_SpatioTemporalIntensityAccumulator += Intensity * SpatialKernelContribution;
                m_SpatioTemporalKernelAccumulator += SpatialKernelContribution;
                SpatioTemporalSample Z =
                { Intensity, SpatialKernelContribution };
                m_SpatioTemporalSamples.push_back(Z);
            }

            void CFusionPixel::ClearSpatioTemporalSamples()
            {
                m_SpatioTemporalMinimal = _REAL_MAX_;
                m_SpatioTemporalMaximal = _REAL_MIN_;
                m_SpatioTemporalIntensityAccumulator = m_SpatioTemporalKernelAccumulator = _REAL_ZERO_;
                m_SpatioTemporalSamples.clear();
            }

            real CFusionPixel::SemiContinuousSpatioTemporalGaussianFusion(real Precision, real* pKernel, const uint Radius, const uint OffsetDelta, real* pSemiContinousDensity, const uint MaximalBins)
            {
                m_FusionValue = m_Mean = (m_SpatioTemporalIntensityAccumulator / m_SpatioTemporalKernelAccumulator);
                m_Range = m_SpatioTemporalMaximal - m_SpatioTemporalMinimal;
                if (m_Range > Precision)
                {
                    real ValueInitial = m_SpatioTemporalMinimal;
                    real ValueFinal = m_SpatioTemporalMaximal;
                    real SubRange = m_Range;
                    std::vector<SpatioTemporalSample> SortedSpatioTemporalSamples;
                    std::vector<SpatioTemporalSample>::const_iterator BeginSpatioTemporalSelectedSamples = m_SpatioTemporalSamples.begin();
                    std::vector<SpatioTemporalSample>::const_iterator EndSpatioTemporalSelectedSamples = m_SpatioTemporalSamples.end();
                    if (OffsetDelta)
                    {
                        SortedSpatioTemporalSamples = m_SpatioTemporalSamples;
                        TSort(SortedSpatioTemporalSamples.begin(), SortedSpatioTemporalSamples.end(), SortSpatioTemporalSampleByIntensity);
                        BeginSpatioTemporalSelectedSamples = SortedSpatioTemporalSamples.begin();
                        EndSpatioTemporalSelectedSamples = SortedSpatioTemporalSamples.end();
                        for (uint i = 0; i < OffsetDelta; ++i)
                        {
                            ++BeginSpatioTemporalSelectedSamples;
                            --EndSpatioTemporalSelectedSamples;
                        }
                        ValueInitial = BeginSpatioTemporalSelectedSamples->m_Intensity;
                        ValueFinal = EndSpatioTemporalSelectedSamples->m_Intensity;
                        SubRange = ValueFinal - ValueInitial;
                    }
                    if (SubRange > Precision)
                    {
                        uint TotalBins = TMin(uint(RealCeil(SubRange / Precision)) + 1, MaximalBins);
                        real ScaleFactor = real(TotalBins - 1) / SubRange;
                        memset(pSemiContinousDensity, 0, sizeof(real) * TotalBins);
                        for (std::vector<SpatioTemporalSample>::const_iterator pSpatioTemporalSample = BeginSpatioTemporalSelectedSamples; pSpatioTemporalSample != EndSpatioTemporalSelectedSamples; ++pSpatioTemporalSample)
                        {
                            int Location = RoundToInteger((pSpatioTemporalSample->m_Intensity - ValueInitial) * ScaleFactor);
                            real SapatialKernel = pSpatioTemporalSample->m_SpatialKernelContribution;
                            for (uint i = 0, AuxiliarLocation = Location; (i < Radius) && (AuxiliarLocation < TotalBins); ++i, ++AuxiliarLocation)
                            {
                                pSemiContinousDensity[AuxiliarLocation] += pKernel[i] * SapatialKernel;
                            }
                            for (uint i = 1, AuxiliarLocation = Location - 1; (i < Radius); ++i, --AuxiliarLocation)
                            {
                                pSemiContinousDensity[AuxiliarLocation] += pKernel[i] * SapatialKernel;
                            }
                        }
                        uint MaximalIndex = 0;
                        real MaximalDensity = _REAL_ZERO_;
                        for (uint i = 0; i < TotalBins; ++i, ++pSemiContinousDensity)
                            if (*pSemiContinousDensity > MaximalDensity)
                            {
                                MaximalDensity = *pSemiContinousDensity;
                                MaximalIndex = i;
                            }
                        m_FusionValue = (real(MaximalIndex) / ScaleFactor) + ValueInitial;
                    }
                    else
                    {
                        m_FusionValue = (ValueFinal + ValueInitial) * _REAL_HALF_;
                    }
                }
                return m_FusionValue;
            }
        }
    }
}
