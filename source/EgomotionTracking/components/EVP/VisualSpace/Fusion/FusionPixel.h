/*
 * FusionPixel.h
 *
 *  Created on: 25.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "../../Foundation/Mathematics/1D/Common1D.h"
#include "../../Foundation/DataTypes/PixelLocation.h"

#define _FUSION_MINIMAL_DELTA_  real(0.0100098)

namespace EVP
{
    namespace VisualSpace
    {
        namespace Fusion
        {
            class CFusionPixel
            {
            public:

                CFusionPixel();

                virtual ~CFusionPixel();

                void ReserveSamplesStorage(const uint TotalSamples) _EVP_FAST_CALL_;
                void AddFirstSample(const real Z) _EVP_FAST_CALL_;
                void AddSample(const real Z) _EVP_FAST_CALL_;
                void ClearSamples();
                real MeanFusion(const uint OffsetDelta);
                real EpanechnikovFusion(const real KernelBandWidth, const real Precision, const real SubPrecision, const uint OffsetDelta);
                real GaussianFusion(const real ExponentFactor, const real Precision, const real SubPrecision, const uint OffsetDelta);

                //TODO VERIFY
                real SemiContinuousGaussianFusionKernelAdaptive(real Precision, real SigmaFactor, const uint OffsetDelta, real* pKernel, real* pSemiContinousDensity, const uint MaximalBins);
                real SemiContinuousGaussianFusion(real Precision, real* pKernel, const uint Radius, const uint OffsetDelta, real* pSemiContinousDensity, const uint MaximalBins);
                real GetDeviationGaussianFusion(real ExponentFactor, real Precision, real SubPrecision, const uint OffsetDelta, real& SubRange, uint SubSamples);
                real GetDeviationSemiContinuousGaussianFusion(real Precision, real* pKernel, const uint Radius, const uint OffsetDelta, real* pSemiContinousDensity, const uint MaximalBins, real& SubRange, uint SubSamples);
                //

                void StatisticalAnalysis(const uint OffsetDelta);
                real GetMaximalAbsoluteDeviation(const uint OffsetDelta);
                real GetAccumulatedAbsoluteDeviation(const uint OffsetDelta);
                real GetMeanAbsoluteDeviation(const uint OffsetDelta);
                real GetRMSDeviation(const uint OffsetDelta);
                void GetExtremFusionDeviations(real& Maximal, real& Minimal, const uint OffsetDelta) const;

                bool IsSingular() const;

                bool HasDeviation() const;
                real GetMean() const;
                real GetMeadian() const;
                real GetDispersion() const;
                real GetFusionValue() const;
                real GetMaximal() const;
                real GetMinimal() const;
                real GetRange() const;
                real GetStandardDeviation() const;
                real GetSignalToNoiseRatio() const;
                const vector<real>& GetSamples() const;
                real GetSampleByIndex(const uint Index) const;
                void GetCharacteristics(real& Minimal, real& Maximal, real& Mean, real& Fusion) const;

                bool ExportToFile(const_string pPathFileName, CPixelLocation* pLocation);
                bool ExportDescriptiveStatisticsToFile(const_string pPathFileName, CPixelLocation* pLocation);
                bool ExportPDFToFile(const_string pPathFileName, CPixelLocation* pLocation, real ExponentFactor);

                //SPATIO-TEMPORAL
                void AddSpatioTemporalSample(const real Intensity, const real Kernel);
                void ClearSpatioTemporalSamples();

                //TODO VERIFY
                real SemiContinuousSpatioTemporalGaussianFusion(real Precision, real* pKernel, const uint Radius, const uint OffsetDelta, real* pSemiContinousDensity, const uint MaximalBins);
                //

            protected:

                real m_Accumulator;
                real m_Minimal;
                real m_Maximal;
                real m_Mean;
                real m_Median;
                real m_Range;
                real m_StandardDeviation;
                real m_FusionValue;
                real m_SignalToNoiseRatio;
                vector<real> m_Samples;

                real m_SpatioTemporalMinimal;
                real m_SpatioTemporalMaximal;
                real m_SpatioTemporalIntensityAccumulator;
                real m_SpatioTemporalKernelAccumulator;
                struct SpatioTemporalSample
                {
                    real m_Intensity;
                    real m_SpatialKernelContribution;
                };

                vector<SpatioTemporalSample> m_SpatioTemporalSamples;

                static bool SortSpatioTemporalSampleByIntensity(const SpatioTemporalSample& lhs, const SpatioTemporalSample& rhs)
                {
                    return lhs.m_Intensity < rhs.m_Intensity;
                }
            };
        }
    }
}

