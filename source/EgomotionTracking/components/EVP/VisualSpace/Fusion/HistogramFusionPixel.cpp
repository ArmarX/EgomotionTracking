/*
 * HistogramFusionPixel.cpp
 *
 *  Created on: 10.07.2011
 *      Author: gonzalez
 */

#include "../../Foundation/Files/OutFile.h"
#include "HistogramFusionPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Fusion
        {
            uint CHistogramFusionPixel::s_TotalSamples = 0;
            real CHistogramFusionPixel::s_Normalization = _REAL_ZERO_;
            real CHistogramFusionPixel::s_BandwidthFactor = _REAL_ZERO_;
            real CHistogramFusionPixel::s_RangePrecision = _REAL_ZERO_;
#ifdef _HISTOGRAMFUSIONPIXEL_USE_LOOKUP_TABLE_
            INITIALIZE_EXPONENTIAL_LOOKUP_TABLE(CHistogramFusionPixel)
#endif

            void CHistogramFusionPixel::SetGlobalParameters(uint TotalSamples, real BandwidthFactor, real RangePrecision)
            {
                s_TotalSamples = TotalSamples;
                s_Normalization = _REAL_ONE_ / (real(s_TotalSamples));
                s_BandwidthFactor = BandwidthFactor;
                s_RangePrecision = RangePrecision;
            }

            CHistogramFusionPixel::CHistogramFusionPixel()
            {
                m_FusionValue = _REAL_ZERO_;
                m_Accumulator = m_Minimal = m_Maximal = 0;
                m_pData = new ushort[256];
                memset(m_pData, 0, sizeof(ushort) * 256);
#ifdef _USE_LOOKUP_TABLES_
                LOAD_EXPONENTIAL_LOOKUP_TABLE(CHistogramFusionPixel)
#endif
            }

            CHistogramFusionPixel::~CHistogramFusionPixel()
            {
                RELEASE_ARRAY(m_pData)
            }

            ushort CHistogramFusionPixel::GetMaximaFrequency()
            {
                const ushort* pDataFinal = m_pData + m_Maximal;
                ushort MaximalFrequency = *pDataFinal;
                for (const ushort* pData = m_pData + m_Minimal; pData < pDataFinal; ++pData)
                    if (*pData > MaximalFrequency)
                    {
                        MaximalFrequency = *pData;
                    }
                return MaximalFrequency;
            }

            list<real> CHistogramFusionPixel::ExtractGlobalMaxima(const ushort MaximaFrequency)
            {
                list<real> Modes;
                for (int Value = m_Minimal; Value <= m_Maximal; ++Value)
                    if (m_pData[Value] == MaximaFrequency)
                    {
                        const int BaseValue = Value;
                        for (int AuxiliarValue = Value + 1; AuxiliarValue <= m_Maximal; ++AuxiliarValue)
                            if (m_pData[AuxiliarValue] == MaximaFrequency)
                            {
                                Value = AuxiliarValue;
                            }
                            else
                            {
                                break;
                            }
                        Modes.push_back((BaseValue == Value) ? real(BaseValue) : (real(BaseValue + Value) * _REAL_HALF_));
                    }
                return Modes;
            }

            real CHistogramFusionPixel::EstimateEpanechnikovDensity(const real ValueLocation, const real BandWidth)
            {
                const int InitialSupportValue = TMax(DownToInteger(ValueLocation - BandWidth), DownToInteger(m_Minimal));
                const int FinalSupportValue = TMin(UpperToInteger(ValueLocation + BandWidth), DownToInteger(m_Maximal));
                const ushort* pData = m_pData + InitialSupportValue;
                real Density = _REAL_ZERO_;
                for (int Value = InitialSupportValue; Value <= FinalSupportValue; ++Value, ++pData)
                    if (*pData)
                    {
                        const real NormalizedDistance = (real(Value) - ValueLocation) / BandWidth;
                        Density += (_REAL_ONE_ - (NormalizedDistance * NormalizedDistance)) * real(*pData);
                    }
                return Density;
            }

            real CHistogramFusionPixel::EstimateGaussianDensity(const real ValueLocation, const real BandWidth, const real BandWidthExponentFactor)
            {
                const int InitialSupportValue = TMax(DownToInteger(ValueLocation - BandWidth), DownToInteger(m_Minimal));
                const int FinalSupportValue = TMin(UpperToInteger(ValueLocation + BandWidth), DownToInteger(m_Maximal));
                const ushort* pData = m_pData + InitialSupportValue;
                real Density = _REAL_ZERO_;
                for (int Value = InitialSupportValue; Value <= FinalSupportValue; ++Value, ++pData)
                    if (*pData)
                    {
                        const real Distance = real(Value) - ValueLocation;
#ifdef _HISTOGRAMFUSIONPIXEL_USE_LOOKUP_TABLE_
                        real Argument = BandWidthExponentFactor * Distance * Distance;
                        if (RealAbs(Argument) < s_ELT.m_MaximalArgumentValue)
                        {
                            Density += s_ELT.m_pExp[RoundPositiveRealToInteger(s_ELT.m_SamplesPerUnit * Argument)] * real(*pData);
                        }
#else
                        Density += RealExp(Distance * Distance * BandWidthExponentFactor) * *pData;
#endif
                    }
                return Density;
            }

            real CHistogramFusionPixel::OptimizedContinuousEpanechnikovFusion()
            {
                if ((m_Maximal - m_Minimal) <= 0)
                {
                    m_FusionValue = real(m_Accumulator) * s_Normalization;
                    return m_FusionValue;
                }
                const real MaximalBandWidth = GetStandardDeviation() * s_BandwidthFactor;
                list<real> Modes = ExtractGlobalMaxima(GetMaximaFrequency());
                switch (Modes.size())
                {
                    case 0:
                        m_FusionValue = real(m_Accumulator) * s_Normalization;
                        break;
                    case 1:
                        m_FusionValue = Modes.front();
                        break;
                    default:
                    {
                        real MaximalDensity = _REAL_ZERO_;
                        list<real>::const_iterator EndModes = Modes.end();
                        for (list<real>::const_iterator pMode = Modes.begin(); pMode != EndModes; ++pMode)
                        {
                            const real Density = EstimateEpanechnikovDensity(*pMode, MaximalBandWidth);
                            if (Density > MaximalDensity)
                            {
                                MaximalDensity = Density;
                                m_FusionValue = *pMode;
                            }
                        }
                    }
                    break;
                }
                real ValueA = TMax(m_FusionValue - _REAL_HALF_, real(m_Minimal));
                real ValueB = TMin(m_FusionValue + _REAL_HALF_, real(m_Maximal));
                real DistanceAB = _REAL_HALF_;
                real DensityA = EstimateEpanechnikovDensity(ValueA, MaximalBandWidth);
                real DensityB = EstimateEpanechnikovDensity(ValueB, MaximalBandWidth);
                do
                {
                    if (DensityB > DensityA)
                    {
                        ValueA += DistanceAB;
                        DensityA = EstimateEpanechnikovDensity(ValueA, MaximalBandWidth);
                    }
                    else
                    {
                        ValueB -= DistanceAB;
                        DensityB = EstimateEpanechnikovDensity(ValueB, MaximalBandWidth);
                    }
                    DistanceAB *= _REAL_HALF_;
                }
                while (DistanceAB > s_RangePrecision);
                m_FusionValue = ((ValueA * DensityA) + (ValueB * DensityB)) / (DensityA + DensityB);
                return m_FusionValue;
            }

            real CHistogramFusionPixel::ContinuousEpanechnikovFusion()
            {
                if ((m_Maximal - m_Minimal) <= 0)
                {
                    m_FusionValue = real(m_Accumulator) * s_Normalization;
                    return m_FusionValue;
                }
                list<real> Modes = ExtractGlobalMaxima(GetMaximaFrequency());
                switch (Modes.size())
                {
                    case 0:
                        m_FusionValue = real(m_Accumulator) * s_Normalization;
                        break;
                    case 1:
                        m_FusionValue = Modes.front();
                        break;
                    default:
                    {
                        const real MaximalBandWidth = GetStandardDeviation() * s_BandwidthFactor;
                        real MaximalDensity = _REAL_ZERO_;
                        list<real>::const_iterator EndModes = Modes.end();
                        for (list<real>::const_iterator pMode = Modes.begin(); pMode != EndModes; ++pMode)
                        {
                            const real Density = EstimateEpanechnikovDensity(*pMode, MaximalBandWidth);
                            if (Density > MaximalDensity)
                            {
                                MaximalDensity = Density;
                                m_FusionValue = *pMode;
                            }
                        }
                    }
                    break;
                }
                return m_FusionValue;
            }

            real CHistogramFusionPixel::OptimizedContinuousGaussianFusion()
            {
                if ((m_Maximal - m_Minimal) <= 0)
                {
                    m_FusionValue = real(m_Accumulator) * s_Normalization;
                    return m_FusionValue;
                }
                const real Sigma = GetStandardDeviation();
                const real MaximalBandWidth = Sigma * s_BandwidthFactor;
                const real BandWidthExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(Sigma);
                list<real> Modes = ExtractGlobalMaxima(GetMaximaFrequency());
                switch (Modes.size())
                {
                    case 0:
                        m_FusionValue = real(m_Accumulator) * s_Normalization;
                        break;
                    case 1:
                        m_FusionValue = Modes.front();
                        break;
                    default:
                    {
                        real MaximalDensity = _REAL_ZERO_;
                        list<real>::const_iterator EndModes = Modes.end();
                        for (list<real>::const_iterator pMode = Modes.begin(); pMode != EndModes; ++pMode)
                        {
                            const real Density = EstimateGaussianDensity(*pMode, MaximalBandWidth, BandWidthExponentFactor);
                            if (Density > MaximalDensity)
                            {
                                MaximalDensity = Density;
                                m_FusionValue = *pMode;
                            }
                        }
                    }
                    break;
                }
                real ValueA = TMax(m_FusionValue - _REAL_HALF_, real(m_Minimal));
                real ValueB = TMin(m_FusionValue + _REAL_HALF_, real(m_Maximal));
                real DistanceAB = _REAL_HALF_;
                real DensityA = EstimateGaussianDensity(ValueA, MaximalBandWidth, BandWidthExponentFactor);
                real DensityB = EstimateGaussianDensity(ValueB, MaximalBandWidth, BandWidthExponentFactor);
                do
                {
                    if (DensityB > DensityA)
                    {
                        ValueA += DistanceAB;
                        DensityA = EstimateGaussianDensity(ValueA, MaximalBandWidth, BandWidthExponentFactor);
                    }
                    else
                    {
                        ValueB -= DistanceAB;
                        DensityB = EstimateGaussianDensity(ValueB, MaximalBandWidth, BandWidthExponentFactor);
                    }
                    DistanceAB *= _REAL_HALF_;
                }
                while (DistanceAB > s_RangePrecision);
                m_FusionValue = ((ValueA * DensityA) + (ValueB * DensityB)) / (DensityA + DensityB);
                return m_FusionValue;
            }

            real CHistogramFusionPixel::ContinuouGaussianFusion()
            {
                if ((m_Maximal - m_Minimal) <= 0)
                {
                    m_FusionValue = real(m_Accumulator) * s_Normalization;
                    return m_FusionValue;
                }
                list<real> Modes = ExtractGlobalMaxima(GetMaximaFrequency());
                switch (Modes.size())
                {
                    case 0:
                        m_FusionValue = real(m_Accumulator) * s_Normalization;
                        break;
                    case 1:
                        m_FusionValue = Modes.front();
                        break;
                    default:
                    {
                        const real Sigma = GetStandardDeviation();
                        const real MaximalBandWidth = Sigma * s_BandwidthFactor;
                        const real BandWidthExponentFactor = Mathematics::_1D::NormalDistribution::DetermineExponentFactor(Sigma);
                        real MaximalDensity = _REAL_ZERO_;
                        list<real>::const_iterator EndModes = Modes.end();
                        for (list<real>::const_iterator pMode = Modes.begin(); pMode != EndModes; ++pMode)
                        {
                            const real Density = EstimateGaussianDensity(*pMode, MaximalBandWidth, BandWidthExponentFactor);
                            if (Density > MaximalDensity)
                            {
                                MaximalDensity = Density;
                                m_FusionValue = *pMode;
                            }
                        }
                    }
                    break;
                }
                return m_FusionValue;
            }

            real CHistogramFusionPixel::GetStandardDeviation() const
            {
                const real Mean = real(m_Accumulator) * s_Normalization;
                real DeviationAccumualtor = _REAL_ZERO_;
                const ushort* pData = m_pData + m_Minimal;
                for (int Value = m_Minimal; Value <= m_Maximal; ++Value, ++pData)
                    if (*pData)
                    {
                        real Deviation = real(Value) - Mean;
                        DeviationAccumualtor += (Deviation * Deviation) * *pData;
                    }
                return RealSqrt(DeviationAccumualtor * s_Normalization);
            }

            real CHistogramFusionPixel::GetSignalToNoiseRatio() const
            {
                const real StandardDeviation = GetStandardDeviation();
                if (StandardDeviation > _REAL_EPSILON_)
                {
                    return real(20.0) * RealLog10(real(m_Accumulator) / StandardDeviation);
                }
                return _REAL_ZERO_;
            }

            bool CHistogramFusionPixel::ExportToFile(const_string pPathFileName, CPixelLocation* pLocation) const
            {
                std::ostringstream OutputText;
                OutputText << "Histogram Fusion Pixel" << g_EndLine;
                if (pLocation)
                {
                    OutputText << g_EndLineTabulator << "Location" << g_Tabulator << "x = " << pLocation->m_x << g_Tabulator << "y = " << pLocation->m_y;
                }
                OutputText << g_EndLineTabulator << "Minimal" << g_Tabulator << DownToInteger(m_Minimal);
                OutputText << g_EndLineTabulator << "Maximal" << g_Tabulator << DownToInteger(m_Maximal);
                OutputText << g_EndLineTabulator << "Mean" << g_Tabulator << real(m_Accumulator) * s_Normalization;
                OutputText << g_EndLineTabulator << "Range" << g_Tabulator << GetRange();
                OutputText << g_EndLineTabulator << "Standard Deviation" << g_Tabulator << GetStandardDeviation();
                OutputText << g_EndLineTabulator << "Fusion" << g_Tabulator << m_FusionValue;
                OutputText << g_EndLineTabulator << "Dispersion" << g_Tabulator << GetDispersion();
                OutputText << g_EndLineTabulator << "Absolute Dispersion" << g_Tabulator << RealAbs(GetDispersion());
                OutputText << g_EndLineTabulator << "Signal to Noise Ratio" << g_Tabulator << GetSignalToNoiseRatio();
                OutputText << g_EndLineTabulator << "Intensity" << g_Tabulator << "Frequency";
                const ushort* pData = m_pData;
                for (int i = 0; i < 256; ++i)
                {
                    OutputText << g_EndLineTabulator << i << g_Tabulator << *pData++;
                }
                return Files::COutFile::WriteStringToFile(pPathFileName, OutputText.str());
            }
        }
    }
}
