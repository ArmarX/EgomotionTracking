/*
 * ContinuousBoundingBox3D.cpp
 *
 *  Created on: 01.04.2011
 *      Author: gonzalez
 */

#include "ContinuousBoundingBox3D.h"

namespace EVP
{
    CContinuousBoundingBox3D::CContinuousBoundingBox3D() :
        m_MinPoint(Mathematics::_3D::CVector3D::s_Point_At_Plus_Infinity_3D), m_MaxPoint(Mathematics::_3D::CVector3D::s_Point_At_Minus_Infinity_3D)
    {
    }

    CContinuousBoundingBox3D::CContinuousBoundingBox3D(const Mathematics::_3D::CVector3D& InitialPoint) :
        m_MinPoint(InitialPoint), m_MaxPoint(InitialPoint)
    {
    }

    CContinuousBoundingBox3D::CContinuousBoundingBox3D(const CContinuousBoundingBox3D& BBox3D)
        = default;

    CContinuousBoundingBox3D::CContinuousBoundingBox3D(const real X0, const real Y0, const real Z0, const real X1, const real Y1, const real Z1) :
        m_MinPoint(TMin(X0, X1), TMin(Y0, Y1), TMin(Z0, Z1)), m_MaxPoint(TMax(X0, X1), TMax(Y0, Y1), TMax(Z0, Z1))
    {
    }

    CContinuousBoundingBox3D::CContinuousBoundingBox3D(const Mathematics::_3D::CVector3D& MinPoint, const Mathematics::_3D::CVector3D& MaxPoint) :
        m_MinPoint(TMin(MinPoint.GetX(), MaxPoint.GetX()), TMin(MinPoint.GetY(), MaxPoint.GetY()), TMin(MinPoint.GetZ(), MaxPoint.GetZ())), m_MaxPoint(TMax(MinPoint.GetX(), MaxPoint.GetX()), TMax(MinPoint.GetY(), MaxPoint.GetY()), TMax(MinPoint.GetZ(), MaxPoint.GetZ()))
    {
    }

    void CContinuousBoundingBox3D::SetBounds(const CContinuousBoundingBox3D& BBox3D)
    {
        m_MinPoint = BBox3D.m_MinPoint;
        m_MaxPoint = BBox3D.m_MaxPoint;
    }

    void CContinuousBoundingBox3D::SetBounds(const real X0, const real Y0, const real Z0, const real X1, const real Y1, const real Z1)
    {
        m_MinPoint.SetValue(TMin(X0, X1), TMin(Y0, Y1), TMin(Z0, Z1));
        m_MaxPoint.SetValue(TMax(X0, X1), TMax(Y0, Y1), TMax(Z0, Z1));
    }

    void CContinuousBoundingBox3D::SetBounds(const Mathematics::_3D::CVector3D& MinPoint, const Mathematics::_3D::CVector3D& MaxPoint)
    {
        m_MinPoint.SetValue(TMin(MinPoint.GetX(), MaxPoint.GetX()), TMin(MinPoint.GetX(), MaxPoint.GetX()), TMin(MinPoint.GetZ(), MaxPoint.GetZ()));
        m_MaxPoint.SetValue(TMax(MinPoint.GetX(), MaxPoint.GetX()), TMax(MinPoint.GetX(), MaxPoint.GetX()), TMax(MinPoint.GetZ(), MaxPoint.GetZ()));
    }

    bool CContinuousBoundingBox3D::GetBounds(real& X0, real& Y0, real& Z0, real& X1, real& Y1, real& Z1) const
    {
        X0 = m_MinPoint.GetX();
        Y0 = m_MinPoint.GetY();
        Z0 = m_MinPoint.GetZ();
        X1 = m_MaxPoint.GetX();
        Y1 = m_MaxPoint.GetY();
        Z1 = m_MaxPoint.GetZ();
        return !IsEmpty();
    }

    bool CContinuousBoundingBox3D::GetBounds(Mathematics::_3D::CVector3D& MinPoint, Mathematics::_3D::CVector3D& MaxPoint) const
    {
        MinPoint = m_MinPoint;
        MaxPoint = m_MaxPoint;
        return !IsEmpty();
    }

    real CContinuousBoundingBox3D::GetDistance(const CContinuousBoundingBox3D& BBox3D) const
    {
        real Dx = _REAL_MAX_, Dy = _REAL_MAX_;
        bool IntersectX = false, IntersectY = false;
        if (BBox3D.m_MinPoint.GetX() < m_MinPoint.GetX())
        {
            if (m_MinPoint.GetX() > BBox3D.m_MaxPoint.GetX())
            {
                Dx = m_MinPoint.GetX() - BBox3D.m_MaxPoint.GetX();
            }
            else
            {
                IntersectX = true;
            }
        }
        else
        {
            if (BBox3D.m_MinPoint.GetX() > m_MaxPoint.GetX())
            {
                Dx = BBox3D.m_MinPoint.GetX() - m_MaxPoint.GetX();
            }
            else
            {
                IntersectX = true;
            }
        }
        if (BBox3D.m_MinPoint.GetY() < m_MinPoint.GetY())
        {
            if (m_MinPoint.GetY() > BBox3D.m_MaxPoint.GetY())
            {
                Dy = m_MinPoint.GetY() - BBox3D.m_MaxPoint.GetY();
            }
            else
            {
                IntersectY = true;
            }
        }
        else
        {
            if (BBox3D.m_MinPoint.GetY() > m_MaxPoint.GetY())
            {
                Dy = BBox3D.m_MinPoint.GetY() - m_MaxPoint.GetY();
            }
            else
            {
                IntersectY = true;
            }
        }
        if (IntersectX || IntersectY)
        {
            if (IntersectX && IntersectY)
            {
                return _REAL_ZERO_;
            }
            if (IntersectX)
            {
                return Dy;
            }
            else
            {
                return Dx;
            }
        }
        return RealHypotenuse(Dx, Dy);
    }

    Mathematics::_3D::CVector3D CContinuousBoundingBox3D::GetCenterPoint() const
    {
        return Mathematics::_3D::CVector3D::CreateMidPoint(m_MaxPoint, m_MinPoint);
    }

    const Mathematics::_3D::CVector3D& CContinuousBoundingBox3D::GetMinPoint() const
    {
        return m_MinPoint;
    }

    const Mathematics::_3D::CVector3D& CContinuousBoundingBox3D::GetMaxPoint() const
    {
        return m_MaxPoint;
    }

    bool CContinuousBoundingBox3D::Intersects(const CContinuousBoundingBox3D& BBox3D) const
    {
        if (IsEmpty() || BBox3D.IsEmpty())
        {
            return false;
        }
        if (BBox3D.m_MinPoint.GetX() < m_MinPoint.GetX())
        {
            if (BBox3D.m_MaxPoint.GetX() < m_MinPoint.GetX())
            {
                return false;
            }
        }
        else if (m_MaxPoint.GetX() < BBox3D.m_MinPoint.GetX())
        {
            return false;
        }

        if (BBox3D.m_MinPoint.GetY() < m_MinPoint.GetY())
        {
            if (BBox3D.m_MaxPoint.GetY() < m_MinPoint.GetY())
            {
                return false;
            }
        }
        else if (m_MaxPoint.GetY() < BBox3D.m_MinPoint.GetY())
        {
            return false;
        }

        if (BBox3D.m_MinPoint.GetZ() < m_MinPoint.GetZ())
        {
            if (BBox3D.m_MaxPoint.GetZ() < m_MinPoint.GetZ())
            {
                return false;
            }
        }
        else if (m_MaxPoint.GetZ() < BBox3D.m_MinPoint.GetZ())
        {
            return false;
        }

        return true;
    }

    bool CContinuousBoundingBox3D::GetIntersection(const CContinuousBoundingBox3D& BBox3D, CContinuousBoundingBox3D& IntersectionBox) const
    {
        if (Intersects(BBox3D))
        {
            const real X0 = TMax(BBox3D.m_MinPoint.GetX(), m_MinPoint.GetX());
            const real Y0 = TMax(BBox3D.m_MinPoint.GetY(), m_MinPoint.GetY());
            const real Z0 = TMax(BBox3D.m_MinPoint.GetZ(), m_MinPoint.GetZ());
            const real X1 = TMin(BBox3D.m_MaxPoint.GetX(), m_MaxPoint.GetX());
            const real Y1 = TMin(BBox3D.m_MaxPoint.GetY(), m_MaxPoint.GetY());
            const real Z1 = TMin(BBox3D.m_MaxPoint.GetZ(), m_MaxPoint.GetZ());
            IntersectionBox.m_MinPoint.SetValue(X0, Y0, Z0);
            IntersectionBox.m_MaxPoint.SetValue(X1, Y1, Z1);
            return true;
        }
        else
        {
            IntersectionBox.SetEmpty();
            return false;
        }
    }

    bool CContinuousBoundingBox3D::IsEmpty() const
    {
        if ((m_MinPoint.GetX() > m_MaxPoint.GetX()) || (m_MinPoint.GetY() > m_MaxPoint.GetY()))
        {
            return true;
        }
        return (RealAbs(m_MaxPoint.GetX() - m_MinPoint.GetX()) < _REAL_EPSILON_) || (RealAbs(m_MaxPoint.GetY() - m_MinPoint.GetY()) < _REAL_EPSILON_) || (RealAbs(m_MaxPoint.GetZ() - m_MinPoint.GetZ()) < _REAL_EPSILON_);
    }

    void CContinuousBoundingBox3D::SetEmpty()
    {
        m_MinPoint = Mathematics::_3D::CVector3D::s_Point_At_Plus_Infinity_3D;
        m_MaxPoint = Mathematics::_3D::CVector3D::s_Point_At_Minus_Infinity_3D;
    }

    bool CContinuousBoundingBox3D::IsInside(const Mathematics::_3D::CVector3D& Point, const bool Inclusive) const
    {
        if (Inclusive)
        {
            return (Point.GetX() >= m_MinPoint.GetX()) && (Point.GetX() <= m_MaxPoint.GetX()) && (Point.GetY() >= m_MinPoint.GetY()) && (Point.GetY() <= m_MaxPoint.GetY()) && (Point.GetZ() >= m_MinPoint.GetZ()) && (Point.GetZ() <= m_MaxPoint.GetZ());
        }
        return (Point.GetX() > m_MinPoint.GetX()) && (Point.GetX() < m_MaxPoint.GetX()) && (Point.GetY() > m_MinPoint.GetY()) && (Point.GetY() < m_MaxPoint.GetY()) && (Point.GetZ() > m_MinPoint.GetZ()) && (Point.GetZ() < m_MaxPoint.GetZ());
    }

    real CContinuousBoundingBox3D::GetVolume() const
    {
        if (IsEmpty())
        {
            return _REAL_ZERO_;
        }
        const Mathematics::_3D::CVector3D Delta = m_MaxPoint - m_MinPoint;
        return Delta.GetX() * Delta.GetY() * Delta.GetZ();
    }

    real CContinuousBoundingBox3D::GetWidth() const
    {
        if (IsEmpty())
        {
            return _REAL_ZERO_;
        }
        return m_MaxPoint.GetX() - m_MinPoint.GetX();
    }

    real CContinuousBoundingBox3D::GetHeight() const
    {
        if (IsEmpty())
        {
            return _REAL_ZERO_;
        }
        return m_MaxPoint.GetY() - m_MinPoint.GetY();
    }

    real CContinuousBoundingBox3D::GetDepth() const
    {
        if (IsEmpty())
        {
            return _REAL_ZERO_;
        }
        return m_MaxPoint.GetZ() - m_MinPoint.GetZ();
    }

    real CContinuousBoundingBox3D::GetDiagonalLenght() const
    {
        return (m_MaxPoint - m_MinPoint).GetLength();
    }

    void CContinuousBoundingBox3D::SetInitial(const Mathematics::_3D::CVector3D& Point)
    {
        m_MinPoint = Point;
        m_MaxPoint = Point;
    }

    void CContinuousBoundingBox3D::Extend(const CContinuousBoundingBox3D& BBox3D)
    {
        m_MaxPoint.SetMaximalComponentValue(BBox3D.m_MaxPoint);
        m_MinPoint.SetMinimalComponentValue(BBox3D.m_MinPoint);
    }

    void CContinuousBoundingBox3D::Extend(const Mathematics::_3D::CVector3D& Point)
    {
        m_MaxPoint.SetMaximalComponentValue(Point);
        m_MinPoint.SetMinimalComponentValue(Point);
    }

    void CContinuousBoundingBox3D::Extend(const real X, const real Y, const real Z)
    {
        if (IsEmpty())
        {
            m_MinPoint.SetValue(X, Y, Z);
            m_MaxPoint.SetValue(X, Y, Z);
        }
        else
        {
            m_MaxPoint.SetMaximalComponentValue(X, Y, Z);
            m_MinPoint.SetMinimalComponentValue(X, Y, Z);
        }
    }

    bool CContinuousBoundingBox3D::Expand(const real Delta)
    {
        if (IsEmpty())
        {
            return false;
        }
        m_MaxPoint.AddOffsetX(Delta);
        m_MaxPoint.AddOffsetY(Delta);
        m_MinPoint.AddOffsetX(-Delta);
        m_MinPoint.AddOffsetY(-Delta);
        m_MinPoint.AddOffsetZ(-Delta);
        m_MinPoint.AddOffsetZ(-Delta);
        return true;
    }

    void CContinuousBoundingBox3D::operator=(const CContinuousBoundingBox3D& BBox3D)
    {
        m_MaxPoint = BBox3D.m_MaxPoint;
        m_MinPoint = BBox3D.m_MinPoint;
    }
}
