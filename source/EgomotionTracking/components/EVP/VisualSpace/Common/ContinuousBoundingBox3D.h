/*
 * ContinuousBoundingBox3D.h
 *
 *  Created on: 01.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "../../Foundation/Mathematics/3D/Vector3D.h"

namespace EVP
{
    //TODO CHANGE TO A COMMON BASE FOR THIS KIND OF DATA TYPES NOT ONLY IN VISUAL SPACE
    class CContinuousBoundingBox3D
    {
    public:

        CContinuousBoundingBox3D();
        CContinuousBoundingBox3D(const Mathematics::_3D::CVector3D& InitialPoint);
        CContinuousBoundingBox3D(const CContinuousBoundingBox3D& BBox3D);
        CContinuousBoundingBox3D(const real X0, const real Y0, const real Z0, const real X1, const real Y1, const real Z1);
        CContinuousBoundingBox3D(const Mathematics::_3D::CVector3D& MinPoint, const Mathematics::_3D::CVector3D& MaxPoint);

        void SetBounds(const CContinuousBoundingBox3D& BBox3D);
        void SetBounds(const real X0, const real Y0, const real Z0, const real X1, const real Y1, const real Z1);
        void SetBounds(const Mathematics::_3D::CVector3D& MinPoint, const Mathematics::_3D::CVector3D& MaxPoint);

        bool GetBounds(real& X0, real& Y0, real& Z0, real& X1, real& Y1, real& Z1) const;
        bool GetBounds(Mathematics::_3D::CVector3D& MinPoint, Mathematics::_3D::CVector3D& MaxPoint) const;

        real GetDistance(const CContinuousBoundingBox3D& BBox3D) const;

        Mathematics::_3D::CVector3D GetCenterPoint() const;
        const Mathematics::_3D::CVector3D& GetMinPoint() const;
        const Mathematics::_3D::CVector3D& GetMaxPoint() const;

        bool Intersects(const CContinuousBoundingBox3D& BBox3D) const;
        bool GetIntersection(const CContinuousBoundingBox3D& BBox3D, CContinuousBoundingBox3D& IntersectionBox) const;
        bool IsEmpty() const;
        void SetEmpty();
        bool IsInside(const Mathematics::_3D::CVector3D& Point, const bool Inclusive = true) const;

        real GetVolume() const;
        real GetWidth() const;
        real GetHeight() const;
        real GetDepth() const;
        real GetDiagonalLenght() const;

        void SetInitial(const Mathematics::_3D::CVector3D& Point);

        void Extend(const CContinuousBoundingBox3D& BBox3D);
        void Extend(const Mathematics::_3D::CVector3D& Point);
        void Extend(const real X, const real Y, const real Z);
        bool Expand(const real Delta);

        void operator=(const CContinuousBoundingBox3D& BBox3D);

    protected:

        Mathematics::_3D::CVector3D m_MinPoint;
        Mathematics::_3D::CVector3D m_MaxPoint;
    };
}

