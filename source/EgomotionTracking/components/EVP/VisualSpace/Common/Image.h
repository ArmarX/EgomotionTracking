/*
 *  Image.h
 *
 *  Created on: 22.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "../../Foundation/DataTypes/ImageSize.h"
#include "../../Foundation/DataTypes/PixelLocation.h"
#include "../../Foundation/Files/File.h"
#include "../../Foundation/Files/InFile.h"
#include "../../Foundation/Files/OutFile.h"

namespace EVP
{
    /**
     \class TImage
     \ingroup Visual Space
     \brief Data structure for the representation of images.
     */

    template<typename PixelType> class TImage
    {
    public:

        TImage() :
            m_Size(), m_OwnerShip(false), m_pBuffer(NULL)
        {
        }

        TImage(const CImageSize& Size) :
            m_Size(Size), m_OwnerShip(false), m_pBuffer(NULL)
        {
            Create();
        }

        TImage(const uint Width, const uint Height) :
            m_Size(Width, Height), m_OwnerShip(false), m_pBuffer(NULL)
        {
            Create();
        }

        TImage(const PixelType* pData, const CImageSize& Size) :
            m_Size(Size), m_OwnerShip(false), m_pBuffer(NULL)
        {
            Create(m_Size, pData);
        }

        TImage(const PixelType* pData, const uint Width, const uint Height) :
            m_Size(Width, Height), m_OwnerShip(false), m_pBuffer(NULL)
        {
            Create(m_Size, pData);
        }

        TImage(const_string pPathFileName, const uint ExtensionMaskFlags = 0) :
            m_Size(), m_OwnerShip(false), m_pBuffer(NULL)
        {
            TImage<PixelType>::LoadFromFile(pPathFileName, ExtensionMaskFlags);
        }

        virtual ~TImage()
        {
            Destroy();
        }

        bool Resize(const CImageSize& Size)
        {
            if (Size.IsValid() && (m_Size != Size))
            {
                Destroy();
                Create(Size);
                return true;
            }
            return false;
        }

        bool Resize(const uint Width, const uint Height)
        {
            return Resize(CImageSize(Width, Height));
        }

        TImage<PixelType>* Reference() const
        {
            return new TImage<PixelType>(m_pBuffer, m_Size);
        }

        TImage<PixelType>* Clone(const bool CopyContent = true) const
        {
            TImage<PixelType>* pCloneImage = new TImage<PixelType>(m_Size);
            if (CopyContent)
            {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
                memcpy(pCloneImage->m_pBuffer, m_pBuffer, GetBufferSize());
#pragma GCC diagnostic pop
            }
            return pCloneImage;
        }

        const TImage<PixelType>* Copy(const PixelType* pBuffer)
        {
            if (pBuffer && m_OwnerShip)
            {
                memcpy(m_pBuffer, pBuffer, GetBufferSize());
                return this;
            }
            return NULL;
        }

        const TImage<PixelType>* Copy(const TImage<PixelType>* pImage)
        {
            if (pImage && m_OwnerShip && (pImage != this) && (pImage->m_Size == m_Size))
            {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
                memcpy(m_pBuffer, pImage->m_pBuffer, GetBufferSize());
#pragma GCC diagnostic pop
                return this;
            }
            return NULL;
        }

        bool SaveToFile(const_string pPathFileName, const uint ExtensionFlags = 0, const bool OverWrite = true) const
        {
            if (pPathFileName && m_pBuffer)
            {
                if ((!OverWrite) && Files::CFile::Exists(pPathFileName))
                {
                    return false;
                }
                Files::COutFile OutputBinaryFile(pPathFileName, Files::CFile::eBinary);
                if (OutputBinaryFile.IsReady())
                    if (OutputBinaryFile.WriteBuffer(&ExtensionFlags, sizeof(uint)))
                        if (OutputBinaryFile.WriteBuffer(&m_Size, sizeof(CImageSize)))
                            if (OutputBinaryFile.WriteValue(uint(sizeof(PixelType))))
                                if (OutputBinaryFile.WriteBuffer(m_pBuffer, GetBufferSize()))
                                {
                                    return OutputBinaryFile.Close();
                                }
            }
            return false;
        }

        bool LoadFromFile(const_string pPathFileName, const uint ExtensionMaskFlags = 0, uint* pExtensionFlagsContent = NULL)
        {
            bool Success = false;
            const uint StoredFileSize = Files::CFile::Exists(pPathFileName);
            if (StoredFileSize)
            {
                Files::CInFile InputBinaryFile(pPathFileName, Files::CFile::eBinary);
                if (InputBinaryFile.IsReady())
                {
                    uint ExtensionFlags = 0;
                    if (InputBinaryFile.ReadBuffer(&ExtensionFlags, sizeof(uint)))
                        if ((!ExtensionMaskFlags) || (ExtensionFlags & ExtensionMaskFlags))
                        {
                            if (ExtensionFlags && pExtensionFlagsContent)
                            {
                                *pExtensionFlagsContent = ExtensionFlags;
                            }
                            CImageSize Size;
                            if (InputBinaryFile.ReadBuffer(&Size, sizeof(CImageSize)))
                            {
                                uint bytesPerPixelType = 0;
                                if (InputBinaryFile.ReadBuffer(&bytesPerPixelType, sizeof(uint)))
                                {
                                    const uint ImageBufferSize = Size.GetArea() * uint(bytesPerPixelType);
                                    if (StoredFileSize == (sizeof(uint) + sizeof(CImageSize) + sizeof(uint) + ImageBufferSize))
                                    {
                                        byte* pFileImageBuffer = new byte[ImageBufferSize];
                                        if (InputBinaryFile.ReadBuffer(pFileImageBuffer, ImageBufferSize))
                                        {
                                            Destroy();
                                            Create(Size);
                                            memcpy(m_pBuffer, pFileImageBuffer, ImageBufferSize);
                                            m_OwnerShip = true;
                                            RELEASE_ARRAY(pFileImageBuffer)
                                            return InputBinaryFile.Close();
                                        }
                                        RELEASE_ARRAY(pFileImageBuffer)
                                    }
                                }
                            }
                        }
                }
            }
            return Success;
        }

        PixelType* GetWritableBufferAt(const CPixelLocation& Location) _EVP_FAST_CALL_
        {
            return m_pBuffer + (m_Size.m_Width * Location.m_y + Location.m_x);
        }

        PixelType* GetWritableBufferAt(const uint X, const uint Y) _EVP_FAST_CALL_
        {
            return m_pBuffer + (m_Size.m_Width * Y + X);
        }

        PixelType* GetBeginWritableBuffer()
        {
            return m_pBuffer;
        }

        PixelType* GetWritableBufferLineAt(const uint Y) _EVP_FAST_CALL_
        {
            return m_pBuffer + (m_Size.m_Width * Y);
        }

        const PixelType* GetReadOnlyBufferAt(const CPixelLocation& Location) const _EVP_FAST_CALL_
        {
            return m_pBuffer + (m_Size.m_Width * Location.m_y + Location.m_x);
        }

        const PixelType* GetReadOnlyBufferAt(const uint X, const uint Y) const _EVP_FAST_CALL_
        {
            return m_pBuffer + (m_Size.m_Width * Y + X);
        }

        const PixelType* GetReadOnlyBufferLineAt(const uint Y) const _EVP_FAST_CALL_
        {
            return m_pBuffer + (m_Size.m_Width * Y);
        }

        const PixelType* GetBeginReadOnlyBuffer() const
        {
            return m_pBuffer;
        }

        const PixelType* GetEndReadOnlyBuffer() const
        {
            return m_pBuffer + m_Size.GetArea();
        }

        const CImageSize& GetSize() const
        {
            return m_Size;
        }

        uint GetWidth() const
        {
            return m_Size.m_Width;
        }

        uint GetHeight() const
        {
            return m_Size.m_Height;
        }

        uint GetArea() const
        {
            return m_Size.GetArea();
        }

        bool SizeEquals(const TImage<PixelType>* pImage) const _EVP_FAST_CALL_
        {
            return pImage && (pImage->m_Size == m_Size);
        }

        void Set(const PixelType& Value)
        {
            const PixelType* const pEndBuffer = m_pBuffer + m_Size.GetArea();
            PixelType* pBuffer = m_pBuffer;
            while (pBuffer < pEndBuffer)
            {
                *pBuffer++ = Value;
            }
        }

        void SetValueAt(const CPixelLocation& Location, const PixelType& Value) _EVP_FAST_CALL_
        {
            m_pBuffer[m_Size.m_Width * Location.m_y + Location.m_x] = Value;
        }

        void SetValueAt(const uint X, const uint Y, const PixelType& Value) _EVP_FAST_CALL_
        {
            m_pBuffer[m_Size.m_Width * Y + X] = Value;
        }

        PixelType& GetReferenceToValueAt(const CPixelLocation& Location) _EVP_FAST_CALL_
        {
            return m_pBuffer[m_Size.m_Width * Location.m_y + Location.m_x];
        }

        const PixelType& GetReadOnlyReferenceToValueAt(const CPixelLocation& Location) const _EVP_FAST_CALL_
        {
            return m_pBuffer[m_Size.m_Width * Location.m_y + Location.m_x];
        }

        PixelType GetValueAt(const uint X, const uint Y) _EVP_FAST_CALL_
        {
            return m_pBuffer[m_Size.m_Width * Y + X];
        }

        PixelType& GetReferenceToValueAt(const uint X, const uint Y) _EVP_FAST_CALL_
        {
            return m_pBuffer[m_Size.m_Width * Y + X];
        }

        const PixelType& GetReadOnlyReferenceToValueAt(const uint X, const uint Y) const _EVP_FAST_CALL_
        {
            return m_pBuffer[m_Size.m_Width * Y + X];
        }

        void Clear()
        {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
            memset(m_pBuffer, 0, sizeof(PixelType) * m_Size.GetArea());
#pragma GCC diagnostic pop
        }

        bool IsValid() const
        {
            return m_pBuffer;
        }

        uint GetBytesPerPixel() const
        {
            return sizeof(PixelType);
        }

        uint GetBytesPerLine() const
        {
            return sizeof(PixelType) * uint(m_Size.m_Width);
        }

        uint GetBufferSize() const
        {
            return sizeof(PixelType) * m_Size.GetArea();
        }

    protected:

        void Create()
        {
            if (m_Size.IsValid())
            {
                RELEASE_ARRAY_BY_OWNERSHIP(m_pBuffer, m_OwnerShip)
                m_pBuffer = new PixelType[m_Size.GetArea()];
                m_OwnerShip = true;
            }
        }

        void Create(const CImageSize& Size)
        {
            if (Size.IsValid())
            {
                m_Size = Size;
                RELEASE_ARRAY_BY_OWNERSHIP(m_pBuffer, m_OwnerShip)
                m_pBuffer = new PixelType[m_Size.GetArea()];
                m_OwnerShip = true;
            }
        }

        void Create(const CImageSize& Size, const PixelType* pExternBuffer)
        {
            if (pExternBuffer && (pExternBuffer != m_pBuffer) && Size.IsValid())
            {
                m_Size = Size;
                RELEASE_ARRAY_BY_OWNERSHIP(m_pBuffer, m_OwnerShip)
                m_pBuffer = const_cast<PixelType*>(pExternBuffer);
                m_OwnerShip = false;
            }
        }

        void Destroy()
        {
            RELEASE_ARRAY_BY_OWNERSHIP(m_pBuffer, m_OwnerShip)
            m_OwnerShip = false;
        }

    private:

        CImageSize m_Size;
        bool m_OwnerShip;
        PixelType* m_pBuffer;
    };

    template<typename PixelType> class TMultipleImage
    {
    public:

        TMultipleImage(const TImage<PixelType>** ppImages, const uint TotalImages) :
            m_TotalImages(TotalImages), m_ppImages(NULL)
        {
            if (ppImages)
            {
                m_ppImages = new TImage<PixelType>* [m_TotalImages];
                for (uint i = 0; i < m_TotalImages; ++i)
                {
                    m_ppImages[i] = ppImages[i]->Clone();
                }
            }
        }

        virtual ~TMultipleImage()
        {
            if (m_ppImages)
            {
                for (uint i = 0; i < m_TotalImages; ++i)
                    RELEASE_OBJECT(m_ppImages[i])
                    RELEASE_ARRAY(m_ppImages)
                }
        }

        TImage<PixelType>** GetWritableImages()
        {
            return m_ppImages;
        }

        const TImage<PixelType>** GetReadOnlyImages() const
        {
            return const_cast<const TImage<PixelType>**>(m_ppImages);
        }

        TImage<PixelType>* GetWritableImageByIndex(const uint Index)
        {
            return (m_ppImages && (Index < m_TotalImages)) ? m_ppImages[Index] : NULL;
        }

        const TImage<PixelType>* GetReadOnlyImageByIndex(const uint Index) const
        {
            return const_cast<const TImage<PixelType>*>((m_ppImages && (Index < m_TotalImages)) ? m_ppImages[Index] : NULL);
        }

    protected:

        uint m_TotalImages;
        TImage<PixelType>** m_ppImages;
    };
}


