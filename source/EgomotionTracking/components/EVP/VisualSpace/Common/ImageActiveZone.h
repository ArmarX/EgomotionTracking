/*
 * ImageActiveZone.h
 *
 *  Created on: 22.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "../../Foundation/DataTypes/ImageSize.h"
#include "../../Foundation/DataTypes/PixelLocation.h"
#include "../../Foundation/Mathematics/2D/Vector2D.h"
#include "ContinuousBoundingBox2D.h"

namespace EVP
{
    class CImageActiveZone
    {
    public:

        typedef  bool(*OffsetUpdateCallBack)(const void* pData, coordinate& OffsetX0, coordinate& OffsetY0, coordinate& OffsetX1, coordinate& OffsetY1);

        enum ConnectionUpdateMethod
        {
            eDirect, eCallBack, eFixedOffsets, eScaling
        };

        CImageActiveZone();
        CImageActiveZone(CImageActiveZone* pSourceZone, const coordinate Offset, const bool Connect);
        CImageActiveZone(const CImageActiveZone* pActiveZone, const CImageSize& Size);//REMOVE THIS
        CImageActiveZone(const CImageSize& Size, const coordinate Margin);
        CImageActiveZone(const coordinate Width, const coordinate Height, const coordinate Margin = 0);
        CImageActiveZone(const coordinate Width, const coordinate Height, const coordinate X0, const coordinate Y0, const coordinate X1, const coordinate Y1);
        virtual ~CImageActiveZone();

        bool IsEnabled() const;
        bool AddMarginOffset(const coordinate Offset);
        bool Set(const CImageActiveZone* pActiveZone, const CImageSize& Size);
        bool Set(const CImageSize& Size);
        bool Set(const CImageSize& Size, const coordinate Margin);
        bool Set(const CImageSize& Size, const coordinate X0, const coordinate Y0, const coordinate X1, const coordinate Y1);
        bool Set(const CImageActiveZone* pActiveZone, const coordinate Margin = 0);
        bool Set(const coordinate Width, const coordinate Height, const coordinate Margin = 0);
        bool Set(const coordinate Width, const coordinate Height, const coordinate X0, const coordinate Y0, const coordinate X1, const coordinate Y1);

        CImageActiveZone Intersection(const CImageActiveZone* pImageActiveZone) const;

        const CImageSize& GetSize() const;
        CImageSize GetActiveSize() const;
        uint GetArea() const;
        uint GetActiveArea() const;
        uint GetActiveWidth() const;
        uint GetActiveHeight() const;
        uint GetHorizontalOffset() const;
        bool IsFullScopeActive() const;
        uint GetWidth() const;
        uint GetHeight() const;

        const CPixelLocation& GetUpperLeftLocation() const;
        const CPixelLocation& GetLowerRightLocation() const;
        coordinate GetX0() const;
        coordinate GetY0() const;
        coordinate GetX1() const;
        coordinate GetY1() const;
        bool IsAtBorder(const CPixelLocation& Location) const;
        bool IsRegionInside(const coordinate X0, const coordinate Y0, const coordinate X1, const coordinate Y1, const bool Inclusive) const;
        bool IsContinuosLocationInside(const Mathematics::_2D::CVector2D& ContinuosLocation) const;
        bool IsDiscreteLocationInside(const CPixelLocation& Location, const bool Inclusive) const;
        bool IsDiscreteLocationInside(const coordinate X, const coordinate Y, const bool Inclusive) const;
        bool IsFullyInside(const CContinuousBoundingBox2D& ContinuousBoundingBox) const;
        bool IsPartiallyInside(const CContinuousBoundingBox2D& ContinuousBoundingBox) const;

        bool GetOctoConnectivityDeltas(coordinate* pOctoConnectivityDeltas) const;
        bool GetTetraConnectivityDeltas(coordinate* pTetraConnectivityDeltas) const;

        void SetConnection(CImageActiveZone* pSourceZone);
        void SetConnection(CImageActiveZone* pSourceZone, const real Scaling);
        void SetConnection(CImageActiveZone* pSourceZone, void* pData, OffsetUpdateCallBack OffsetUpdateFunction);
        void SetConnection(CImageActiveZone* pSourceZone, const coordinate Offset);

        void Disconnect(const bool FromSourceZone = true, const bool FromSubZones = true);

        bool UpdateConnection();

    protected:

        bool UpdateFromSource();
        bool UpdateSubZones();
        void ClearSubZones();
        void RemoveSubZone(CImageActiveZone* pSubZone);
        void AddSubZone(CImageActiveZone* pSubZone);

        ConnectionUpdateMethod m_HierarchicalUpdateMethod;
        CImageActiveZone* m_pSourceZone;
        list<CImageActiveZone*> m_SubZones;

        const void* m_pData;
        coordinate m_FixedOffset;
        real m_ScaleFactor;
        OffsetUpdateCallBack m_pfOffsetUpdateCallBack;

        bool m_IsEnabled;
        CImageSize m_Size;
        CPixelLocation m_UpperLeft;
        CPixelLocation m_LowerRight;
    };

}

