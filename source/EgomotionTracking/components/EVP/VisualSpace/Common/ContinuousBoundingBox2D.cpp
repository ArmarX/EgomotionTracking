/*
 * ContinuousBoundingBox2D.cpp
 *
 *  Created on: 01.04.2011
 *      Author: gonzalez
 */

#include "ContinuousBoundingBox2D.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    CContinuousBoundingBox2D::CContinuousBoundingBox2D() :
        m_MinPoint(CVector2D::s_Point_At_Plus_Infinity_2D), m_MaxPoint(CVector2D::s_Point_At_Minus_Infinity_2D)
    {
    }

    CContinuousBoundingBox2D::CContinuousBoundingBox2D(const Mathematics::_2D::CVector2D& InitialPoint) :
        m_MinPoint(InitialPoint), m_MaxPoint(InitialPoint)
    {
    }

    CContinuousBoundingBox2D::CContinuousBoundingBox2D(const CContinuousBoundingBox2D& BBox2D)
        = default;

    CContinuousBoundingBox2D::CContinuousBoundingBox2D(const real X0, const real Y0, const real X1, const real Y1) :
        m_MinPoint(TMin(X0, X1), TMin(Y0, Y1)), m_MaxPoint(TMax(X0, X1), TMax(Y0, Y1))
    {

    }

    CContinuousBoundingBox2D::CContinuousBoundingBox2D(const CVector2D& MinPoint, const CVector2D& MaxPoint) :
        m_MinPoint(TMin(MinPoint.GetX(), MaxPoint.GetX()), TMin(MinPoint.GetY(), MaxPoint.GetY())), m_MaxPoint(TMax(MinPoint.GetX(), MaxPoint.GetX()), TMax(MinPoint.GetY(), MaxPoint.GetY()))
    {
    }

    void CContinuousBoundingBox2D::SetBounds(const CContinuousBoundingBox2D& BBox2D)
    {
        m_MinPoint = BBox2D.m_MinPoint;
        m_MaxPoint = BBox2D.m_MaxPoint;
    }

    void CContinuousBoundingBox2D::SetBounds(const real X0, const real Y0, const real X1, const real Y1)
    {
        m_MinPoint.SetValue(TMin(X0, X1), TMin(Y0, Y1));
        m_MaxPoint.SetValue(TMax(X0, X1), TMax(Y0, Y1));
    }

    void CContinuousBoundingBox2D::SetBounds(const CVector2D& MinPoint, const CVector2D& MaxPoint)
    {
        m_MinPoint.SetValue(TMin(MinPoint.GetX(), MaxPoint.GetX()), TMin(MinPoint.GetX(), MaxPoint.GetX()));
        m_MaxPoint.SetValue(TMax(MinPoint.GetX(), MaxPoint.GetX()), TMax(MinPoint.GetX(), MaxPoint.GetX()));
    }

    bool CContinuousBoundingBox2D::GetBounds(real& X0, real& Y0, real& X1, real& Y1) const
    {
        X0 = m_MinPoint.GetX();
        Y0 = m_MinPoint.GetY();
        X1 = m_MaxPoint.GetX();
        Y1 = m_MaxPoint.GetY();
        return !IsEmpty();
    }

    bool CContinuousBoundingBox2D::GetBounds(CVector2D& MinPoint, CVector2D& MaxPoint) const
    {
        MinPoint = m_MinPoint;
        MaxPoint = m_MaxPoint;
        return !IsEmpty();
    }

    real CContinuousBoundingBox2D::GetDistance(const CContinuousBoundingBox2D& BBox2D) const
    {
        real Dx = _REAL_MAX_, Dy = _REAL_MAX_;
        bool IntersectX = false, IntersectY = false;
        if (BBox2D.m_MinPoint.GetX() < m_MinPoint.GetX())
        {
            if (m_MinPoint.GetX() > BBox2D.m_MaxPoint.GetX())
            {
                Dx = m_MinPoint.GetX() - BBox2D.m_MaxPoint.GetX();
            }
            else
            {
                IntersectX = true;
            }
        }
        else
        {
            if (BBox2D.m_MinPoint.GetX() > m_MaxPoint.GetX())
            {
                Dx = BBox2D.m_MinPoint.GetX() - m_MaxPoint.GetX();
            }
            else
            {
                IntersectX = true;
            }
        }
        if (BBox2D.m_MinPoint.GetY() < m_MinPoint.GetY())
        {
            if (m_MinPoint.GetY() > BBox2D.m_MaxPoint.GetY())
            {
                Dy = m_MinPoint.GetY() - BBox2D.m_MaxPoint.GetY();
            }
            else
            {
                IntersectY = true;
            }
        }
        else
        {
            if (BBox2D.m_MinPoint.GetY() > m_MaxPoint.GetY())
            {
                Dy = BBox2D.m_MinPoint.GetY() - m_MaxPoint.GetY();
            }
            else
            {
                IntersectY = true;
            }
        }
        if (IntersectX || IntersectY)
        {
            if (IntersectX && IntersectY)
            {
                return _REAL_ZERO_;
            }
            if (IntersectX)
            {
                return Dy;
            }
            else
            {
                return Dx;
            }
        }
        return RealHypotenuse(Dx, Dy);
    }

    CVector2D CContinuousBoundingBox2D::GetCenterPoint() const
    {
        return CVector2D::CreateMidPoint(m_MaxPoint, m_MinPoint);
    }

    const CVector2D& CContinuousBoundingBox2D::GetMinPoint() const
    {
        return m_MinPoint;
    }

    const CVector2D& CContinuousBoundingBox2D::GetMaxPoint() const
    {
        return m_MaxPoint;
    }

    bool CContinuousBoundingBox2D::Intersects(const CContinuousBoundingBox2D& BBox2D) const
    {
        if (IsEmpty() || BBox2D.IsEmpty())
        {
            return false;
        }
        if (BBox2D.m_MinPoint.GetX() < m_MinPoint.GetX())
        {
            if (m_MinPoint.GetX() > BBox2D.m_MaxPoint.GetX())
            {
                return false;
            }
        }
        else
        {
            if (BBox2D.m_MinPoint.GetX() > m_MaxPoint.GetX())
            {
                return false;
            }
        }
        if (BBox2D.m_MinPoint.GetY() < m_MinPoint.GetY())
        {
            if (m_MinPoint.GetY() > BBox2D.m_MaxPoint.GetY())
            {
                return false;
            }
        }
        else
        {
            if (BBox2D.m_MinPoint.GetY() > m_MaxPoint.GetY())
            {
                return false;
            }
        }
        return true;
    }

    bool CContinuousBoundingBox2D::GetIntersection(const CContinuousBoundingBox2D& BBox2D, CContinuousBoundingBox2D& IntersectionBox) const
    {
        if (Intersects(BBox2D))
        {
            const real X0 = TMax(BBox2D.m_MinPoint.GetX(), m_MinPoint.GetX());
            const real Y0 = TMax(BBox2D.m_MinPoint.GetY(), m_MinPoint.GetY());
            const real X1 = TMin(BBox2D.m_MaxPoint.GetX(), m_MaxPoint.GetX());
            const real Y1 = TMin(BBox2D.m_MaxPoint.GetY(), m_MaxPoint.GetY());
            IntersectionBox.m_MinPoint.SetValue(X0, Y0);
            IntersectionBox.m_MaxPoint.SetValue(X1, Y1);
            return true;
        }
        else
        {
            IntersectionBox.SetEmpty();
            return false;
        }
    }

    bool CContinuousBoundingBox2D::IsEmpty() const
    {
        if ((m_MinPoint.GetX() > m_MaxPoint.GetX()) || (m_MinPoint.GetY() > m_MaxPoint.GetY()))
        {
            return true;
        }
        return (RealAbs(m_MaxPoint.GetX() - m_MinPoint.GetX()) < _REAL_EPSILON_) || (RealAbs(m_MaxPoint.GetY() - m_MinPoint.GetY()) < _REAL_EPSILON_);
    }

    void CContinuousBoundingBox2D::SetEmpty()
    {
        m_MinPoint = CVector2D::s_Point_At_Plus_Infinity_2D;
        m_MaxPoint = CVector2D::s_Point_At_Minus_Infinity_2D;
    }

    bool CContinuousBoundingBox2D::IsInside(const CVector2D& Point, const bool Inclusive) const
    {
        if (IsEmpty())
        {
            return false;
        }
        if (Inclusive)
        {
            return (Point.GetX() >= m_MinPoint.GetX()) && (Point.GetX() <= m_MaxPoint.GetX()) && (Point.GetY() >= m_MinPoint.GetY()) && (Point.GetY() <= m_MaxPoint.GetY());
        }
        return (Point.GetX() > m_MinPoint.GetX()) && (Point.GetX() < m_MaxPoint.GetX()) && (Point.GetY() > m_MinPoint.GetY()) && (Point.GetY() < m_MaxPoint.GetY());
    }

    real CContinuousBoundingBox2D::GetArea() const
    {
        if (IsEmpty())
        {
            return _REAL_ZERO_;
        }
        return (m_MaxPoint.GetX() - m_MinPoint.GetX()) * (m_MaxPoint.GetY() - m_MinPoint.GetY());
    }

    real CContinuousBoundingBox2D::GetAspectRatio() const
    {
        if (IsEmpty())
        {
            return _REAL_ZERO_;
        }
        return (m_MaxPoint.GetX() - m_MinPoint.GetX()) / (m_MaxPoint.GetY() - m_MinPoint.GetY());
    }

    real CContinuousBoundingBox2D::GetWidth() const
    {
        if (IsEmpty())
        {
            return _REAL_ZERO_;
        }
        return m_MaxPoint.GetX() - m_MinPoint.GetX();
    }

    real CContinuousBoundingBox2D::GetHeight() const
    {
        if (IsEmpty())
        {
            return _REAL_ZERO_;
        }
        return m_MaxPoint.GetY() - m_MinPoint.GetY();
    }

    real CContinuousBoundingBox2D::GetDiagonalLenght() const
    {
        return (m_MaxPoint - m_MinPoint).GetLength();
    }

    void CContinuousBoundingBox2D::SetInitial(const Mathematics::_2D::CVector2D& Point)
    {
        m_MinPoint = Point;
        m_MaxPoint = Point;
    }

    void CContinuousBoundingBox2D::Extend(const Mathematics::_2D::CVector2D& Point)
    {
        m_MaxPoint.SetMaximalComponentValue(Point);
        m_MinPoint.SetMinimalComponentValue(Point);
    }

    void CContinuousBoundingBox2D::Extend(const real X, const real Y)
    {
        if (IsEmpty())
        {
            m_MinPoint.SetValue(X, Y);
            m_MaxPoint.SetValue(X, Y);
        }
        else
        {
            m_MaxPoint.SetMaximalComponentValue(X, Y);
            m_MinPoint.SetMinimalComponentValue(X, Y);
        }
    }

    bool CContinuousBoundingBox2D::Expand(const real Delta)
    {
        if (IsEmpty())
        {
            return false;
        }
        m_MaxPoint.AddOffsetX(Delta);
        m_MaxPoint.AddOffsetY(Delta);
        m_MinPoint.AddOffsetX(-Delta);
        m_MinPoint.AddOffsetY(-Delta);
        return true;
    }

    CImageSize CContinuousBoundingBox2D::GetEffectiveDiscreteSize() const
    {
        if (IsEmpty())
        {
            return CImageSize();
        }
        return CImageSize(uint(RealCeil(m_MaxPoint.GetX()) - RealFloor(m_MinPoint.GetX()) + _REAL_ONE_), uint(RealCeil(m_MaxPoint.GetY()) - RealFloor(m_MinPoint.GetY()) + _REAL_ONE_));
    }

    void CContinuousBoundingBox2D::operator=(const CContinuousBoundingBox2D& BBox2D)
    {
        m_MaxPoint = BBox2D.m_MaxPoint;
        m_MinPoint = BBox2D.m_MinPoint;
    }
}
