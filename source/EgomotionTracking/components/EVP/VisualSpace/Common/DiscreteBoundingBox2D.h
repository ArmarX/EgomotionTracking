/*
 * DiscreteBoundingBox2D.h
 *
 *  Created on: 01.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "../../Foundation/DataTypes/PixelLocation.h"

namespace EVP
{
    namespace VisualSpace
    {
        class CDiscreteBoundingBox2D
        {
        public:

            CDiscreteBoundingBox2D();
            CDiscreteBoundingBox2D(const CPixelLocation& PixelLocation);
            CDiscreteBoundingBox2D(const CDiscreteBoundingBox2D& BBox2D);
            CDiscreteBoundingBox2D(const coordinate x0, const coordinate y0, const coordinate x1, const coordinate y1);
            CDiscreteBoundingBox2D(const CPixelLocation& MinPixelLocation, const CPixelLocation& MaxPixelLocation);

            void SetBounds(const CDiscreteBoundingBox2D& BBox2D);
            void SetBounds(const coordinate x0, const coordinate y0, const coordinate x1, const coordinate y1);
            void SetBounds(const CPixelLocation& MinPoint, const CPixelLocation& MaxPoint);

            bool GetBounds(coordinate& x0, coordinate& y0, coordinate& x1, coordinate& y1) const;
            bool GetBounds(CPixelLocation& MinPoint, CPixelLocation& MaxPoint) const;

            CPixelLocation GetCenterLocation() const;
            const CPixelLocation& GetMinLocation() const;
            const CPixelLocation& GetMaxLocation() const;

            bool Intersects(const CDiscreteBoundingBox2D& BBox2D) const;
            bool GetIntersection(const CDiscreteBoundingBox2D& BBox2D, CDiscreteBoundingBox2D& IntersectionBox) const;
            bool IsEmpty() const;
            void SetEmpty();
            bool IsInside(const CPixelLocation& Point, bool Inclusive = true) const;

            uint GetArea() const;
            real GetAspectRatio() const;
            uint GetWidth() const;
            uint GetHeight() const;

            void Extend(const CPixelLocation& PixelLocation);
            void Extend(const coordinate X, const coordinate Y);
            void Expand(const coordinate Delta);

        protected:

            CPixelLocation m_MinPoint;
            CPixelLocation m_MaxPoint;
        };
    }
}

