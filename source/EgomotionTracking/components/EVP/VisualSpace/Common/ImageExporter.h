/*
 * ImageExporter.h
 *
 *  Created on: 03.12.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "../../Foundation/Mathematics/2D/Vector2D.h"
#include "../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../Foundation/DataTypes/RGBAColor.h"
#include "../../Foundation/DataTypes/DiscreteRGBAPixel.h"
#include "../../Foundation/DataTypes/ColorSpaces.h"
#include "../../Foundation/Files/OutFile.h"
#include "../../Visualization/Miscellaneous/TristimulusColorMap.h"
#include "ImageActiveZone.h"
#include "Image.h"

namespace EVP
{
    namespace VisualSpace
    {
        class CImageExporter
        {
        public:

            //TODO Make Drawer class
            static  bool DrawCircle(const Mathematics::_2D::CVector2D& Center, const real Radius, const real StrokeWidth, const bool Filled, TImage<CDiscreteTristimulusPixel>* pDisplayImage, const CRGBAColor& Color);
            static  bool DrawActiveZone(const CImageActiveZone* pActiveZone, TImage<CDiscreteTristimulusPixel>* pDisplayImage, const CRGBAColor& Color);
            static  bool DrawLineSegment(TImage<CDiscreteTristimulusPixel>* pDisplayImage, const Mathematics::_2D::CVector2D& A, const Mathematics::_2D::CVector2D& B, const real LineWidth, const CRGBAColor& Color);
            static  bool DrawLineSegmentSmooth(TImage<CDiscreteTristimulusPixel>* pDisplayImage, const Mathematics::_2D::CVector2D& A, const Mathematics::_2D::CVector2D& B, const real LineWidth, const CRGBAColor& Color);

            //TODO Make Imagemapper class
            static  bool DisplayFlagMap(const CImageActiveZone* pActiveZone, const TImage<bool>* pInputImage, TImage<CDiscreteTristimulusPixel>* pDisplayImage);
            static  bool DisplayClappedIntensity(const CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, TImage<CDiscreteTristimulusPixel>* pDisplayImage);
            static  bool DisplayRoundedIntensity(const CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, TImage<CDiscreteTristimulusPixel>* pDisplayImage);
            static  bool DisplayScaledIntensity(const CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, TImage<CDiscreteTristimulusPixel>* pDisplayImage);
            static  bool DisplayColorMapped(const CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, TImage<CDiscreteTristimulusPixel>* pDisplayImage, const Visualization::CTristimulusColorMap* pTristimulusColorMap);
            static  bool DisplayRounded(const CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, TImage<CDiscreteTristimulusPixel>* pDisplayImage);
            static  bool DisplayScaled(const CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, TImage<CDiscreteTristimulusPixel>* pDisplayImage);

            static  bool ExportToBMP(const_string pPathFileName, const TImage<bool>* pImage, const CRGBAColor* pColorTable = NULL);
            static  bool ExportToBMP(const_string pPathFileName, const TImage<byte>* pImage, const CRGBAColor* pColorTable = NULL);
            static  bool ExportToBMP(const_string pPathFileName, const TImage<CDiscreteTristimulusPixel>* pImage);
            static  bool ExportToBMP(const_string pPathFileName, const TImage<CDiscreteRGBAPixel>* pImage);

        protected:

            CImageExporter();
            virtual ~CImageExporter();

            static  real GetExtrema(const CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, real& Maximal, real& Minimal);
            static  CContinuousTristimulusPixel GetExtrema(const CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, CContinuousTristimulusPixel& Maximal, CContinuousTristimulusPixel& Minimal);

            static void LoadColorTables();
            static const CRGBAColor* GetIntensityColorTable();
            static const CRGBAColor* GetBooleanColorTable();
            static  bool SaveToFileBMP(const_string pPathFileName, const uint Width, const uint Height, const uint bytesPerPixel, const byte* pDataBuffer, const CRGBAColor* pColorTable);

            static CRGBAColor s_IntensityColorTable[256];
            static CRGBAColor s_BooleanColorTable[256];
            static bool s_InitializedColorTables;
        };
    }
}

