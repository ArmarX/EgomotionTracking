/*
 * ImageExporter.cpp
 *
 *  Created on: 03.12.2011
 *      Author: gonzalez
 */

#include "ImageExporter.h"

namespace EVP
{
    namespace VisualSpace
    {
        CRGBAColor CImageExporter::s_IntensityColorTable[256];
        CRGBAColor CImageExporter::s_BooleanColorTable[256];
        bool CImageExporter::s_InitializedColorTables = false;

        CImageExporter::CImageExporter()
            = default;

        CImageExporter::~CImageExporter()
            = default;

        bool CImageExporter::DrawCircle(const Mathematics::_2D::CVector2D& Center, const real Radius, const real StrokeWidth, const bool Filled, TImage<CDiscreteTristimulusPixel>* pDisplayImage, const CRGBAColor& Color)
        {
            if (pDisplayImage && pDisplayImage->IsValid() && (Radius > _REAL_ZERO_) && (Filled || (StrokeWidth > _REAL_ZERO_)))
            {
                const CImageSize& Size = pDisplayImage->GetSize();
                const real Cx = Center.GetX();
                const real Cy = Center.GetY();
                if (Filled)
                {
                    const real ZoneRadius = Radius + _REAL_TWO_;
                    const coordinate X0 = RealFloor(Cx - ZoneRadius);
                    const coordinate Y0 = RealFloor(Cy - ZoneRadius);
                    const coordinate X1 = RealCeil(Cx + ZoneRadius);
                    const coordinate Y1 = RealCeil(Cy + ZoneRadius);
                    if (Color.IsOpaque())
                    {
                        CDiscreteTristimulusPixel ColorCopy(Color);
                        for (coordinate y = Y0; y <= Y1; ++y)
                            if (Size.IsYInside(y))
                            {
                                const real Dy = real(y) - Cy;
                                for (coordinate x = X0; x <= X1; ++x)
                                    if (Size.IsXInside(x))
                                    {
                                        const real Dx = real(x) - Cx;
                                        const real Dr = Radius - RealHypotenuse(Dx, Dy);
                                        if (Dr >= _REAL_ZERO_)
                                        {
                                            pDisplayImage->SetValueAt(x, y, ColorCopy);
                                        }
                                        else if (Dr > -_REAL_SQRT2_)
                                        {
                                            const real Weight = _REAL_ONE_ - (Dr * -_REAL_SQRT_1_2_);
                                            pDisplayImage->GetReferenceToValueAt(x, y).ComposeRGBA(Color, Weight * Weight);
                                        }
                                    }
                            }
                    }
                    else
                    {
                        const real FixedAlpha = Color.GetNormalizedAlphaChannel();
                        for (coordinate y = Y0; y <= Y1; ++y)
                            if (Size.IsYInside(y))
                            {
                                const real Dy = real(y) - Cy;
                                for (coordinate x = X0; x <= X1; ++x)
                                    if (Size.IsXInside(x))
                                    {
                                        const real Dx = real(x) - Cx;
                                        const real Dr = Radius - RealHypotenuse(Dx, Dy);
                                        if (Dr >= _REAL_ZERO_)
                                        {
                                            pDisplayImage->GetReferenceToValueAt(x, y).ComposeRGBA(Color, FixedAlpha);
                                        }
                                        else if (Dr > -_REAL_SQRT2_)
                                        {
                                            const real Weight = _REAL_ONE_ - (Dr * -_REAL_SQRT_1_2_);
                                            pDisplayImage->GetReferenceToValueAt(x, y).ComposeRGBA(Color, Weight * Weight * FixedAlpha);
                                        }
                                    }
                            }
                    }
                }
                else
                {
                    const real HalfStrokeWidth = StrokeWidth * _REAL_HALF_;
                    const real ZoneRadius = Radius + HalfStrokeWidth + _REAL_TWO_;
                    const coordinate X0 = RealFloor(Cx - ZoneRadius);
                    const coordinate Y0 = RealFloor(Cy - ZoneRadius);
                    const coordinate X1 = RealCeil(Cx + ZoneRadius);
                    const coordinate Y1 = RealCeil(Cy + ZoneRadius);
                    const real ExtendedStrokeZone = HalfStrokeWidth + _REAL_TWO_;
                    if (Color.IsOpaque())
                    {
                        CDiscreteTristimulusPixel ColorCopy(Color);
                        for (coordinate y = Y0; y <= Y1; ++y)
                            if (Size.IsYInside(y))
                            {
                                const real Dy = real(y) - Cy;
                                for (coordinate x = X0; x <= X1; ++x)
                                    if (Size.IsXInside(x))
                                    {
                                        const real Dx = real(x) - Cx;
                                        const real Dr = RealAbs(Radius - RealHypotenuse(Dx, Dy));
                                        if (Dr < ExtendedStrokeZone)
                                        {
                                            if (Dr < HalfStrokeWidth)
                                            {
                                                pDisplayImage->SetValueAt(x, y, ColorCopy);
                                            }
                                            else
                                            {
                                                const real Weight = _REAL_ONE_ - ((Dr - HalfStrokeWidth) * _REAL_HALF_);
                                                pDisplayImage->GetReferenceToValueAt(x, y).ComposeRGBA(Color, Weight * Weight);
                                            }
                                        }
                                    }
                            }
                    }
                    else
                    {
                        const real FixedAlpha = Color.GetNormalizedAlphaChannel();
                        for (coordinate y = Y0; y <= Y1; ++y)
                            if (Size.IsYInside(y))
                            {
                                const real Dy = real(y) - Cy;
                                for (coordinate x = X0; x <= X1; ++x)
                                    if (Size.IsXInside(x))
                                    {
                                        const real Dx = real(x) - Cx;
                                        const real Dr = RealAbs(Radius - RealHypotenuse(Dx, Dy));
                                        if (Dr < ExtendedStrokeZone)
                                        {
                                            if (Dr < HalfStrokeWidth)
                                            {
                                                pDisplayImage->GetReferenceToValueAt(x, y).ComposeRGBA(Color, FixedAlpha);
                                            }
                                            else
                                            {
                                                const real Weight = _REAL_ONE_ - ((Dr - HalfStrokeWidth) * _REAL_HALF_);
                                                pDisplayImage->GetReferenceToValueAt(x, y).ComposeRGBA(Color, Weight * Weight * FixedAlpha);
                                            }
                                        }
                                    }
                            }
                    }
                }
                return true;
            }
            return false;
        }

        bool CImageExporter::DrawActiveZone(const CImageActiveZone* pActiveZone, TImage<CDiscreteTristimulusPixel>* pDisplayImage, const CRGBAColor& Color)
        {
            if (pActiveZone && pDisplayImage && pActiveZone->IsEnabled() && pDisplayImage->IsValid() && (pActiveZone->GetSize() == pDisplayImage->GetSize()))
            {
                const coordinate X0 = pActiveZone->GetX0();
                const coordinate Y0 = pActiveZone->GetY0();
                const coordinate X1 = pActiveZone->GetX1();
                const coordinate Y1 = pActiveZone->GetY1();
                if (Color.IsOpaque())
                {
                    CDiscreteTristimulusPixel ColorCopy(Color);
                    for (coordinate y = Y0; y <= Y1; ++y)
                    {
                        pDisplayImage->SetValueAt(X0, y, ColorCopy);
                        pDisplayImage->SetValueAt(X1, y, ColorCopy);
                    }
                    for (coordinate x = X0 + 1; x < X1; ++x)
                    {
                        pDisplayImage->SetValueAt(x, Y0, ColorCopy);
                        pDisplayImage->SetValueAt(x, Y1, ColorCopy);
                    }
                }
                else
                {
                    const real FixedAlpha = Color.GetNormalizedAlphaChannel();
                    for (coordinate y = Y0; y <= Y1; ++y)
                    {
                        pDisplayImage->GetReferenceToValueAt(X0, y).ComposeRGBA(Color, FixedAlpha);
                        pDisplayImage->GetReferenceToValueAt(X1, y).ComposeRGBA(Color, FixedAlpha);
                    }
                    for (coordinate x = X0 + 1; x < X1; ++x)
                    {
                        pDisplayImage->GetReferenceToValueAt(x, Y0).ComposeRGBA(Color, FixedAlpha);
                        pDisplayImage->GetReferenceToValueAt(x, Y1).ComposeRGBA(Color, FixedAlpha);
                    }
                }
                return true;
            }
            return false;
        }

        bool CImageExporter::DrawLineSegment(TImage<CDiscreteTristimulusPixel>* pDisplayImage, const Mathematics::_2D::CVector2D& A, const Mathematics::_2D::CVector2D& B, const real LineWidth, const CRGBAColor& Color)
        {
            if (pDisplayImage && A.IsNotAtInfinity() && B.IsNotAtInfinity())
            {
                Mathematics::_2D::CVector2D Direction = B - A;
                const real Lenght = Direction.Normalize();
                if (Lenght > _REAL_EPSILON_)
                {
                    const CDiscreteTristimulusPixel SolidColor(Color);
                    for (real Delta = _REAL_ZERO_; Delta <= Lenght; Delta += _REAL_THIRD_)
                    {
                        const Mathematics::_2D::CVector2D Location = Direction * Delta + A;
                        pDisplayImage->SetValueAt(CPixelLocation(Location.GetX(), Location.GetY()), SolidColor);
                    }
                    return true;
                }
            }
            return false;
        }

        bool CImageExporter::DrawLineSegmentSmooth(TImage<CDiscreteTristimulusPixel>* pDisplayImage, const Mathematics::_2D::CVector2D& A, const Mathematics::_2D::CVector2D& B, const real LineWidth, const CRGBAColor& Color)
        {
            if (pDisplayImage && A.IsNotAtInfinity() && B.IsNotAtInfinity())
            {
                const real SafeLineWidth = TMax(LineWidth, _REAL_ONE_);
                const coordinate X0 = TMax(coordinate(RealFloor(TMin(A.GetX(), B.GetX()) - SafeLineWidth)), 0);
                const coordinate Y0 = TMax(coordinate(RealFloor(TMin(A.GetY(), B.GetY()) - SafeLineWidth)), 0);
                const coordinate X1 = TMin(coordinate(RealCeil(TMax(A.GetX(), B.GetX()) + SafeLineWidth)), coordinate(pDisplayImage->GetWidth()) - 1);
                const coordinate Y1 = TMin(coordinate(RealCeil(TMax(A.GetY(), B.GetY()) + SafeLineWidth)), coordinate(pDisplayImage->GetHeight()) - 1);
                if (((X1 - X0) > 0) || ((Y1 - Y0) > 0))
                {
                    Mathematics::_2D::CVector2D Direction = B - A;
                    const real Lenght = Direction.Normalize();
                    const Mathematics::_2D::CVector2D Normal = Direction.GetPerpendicular();
                    if (Color.IsOpaque())
                    {
                        Mathematics::_2D::CVector2D P(X0, Y0);
                        for (coordinate Y = Y0; Y < Y1; ++Y, P.SetY(Y))
                            for (coordinate X = X0; X < X1; ++X, P.SetX(X))
                            {
                                const Mathematics::_2D::CVector2D Delta = P - A;
                                const real DistanceAlong = Direction.ScalarProduct(Delta);
                                if ((DistanceAlong >= _REAL_ZERO_) && (DistanceAlong <= Lenght))
                                {
                                    const real DistancePerpendicular = RealAbs(Normal.ScalarProduct(Delta));
                                    if (DistancePerpendicular < SafeLineWidth)
                                    {
                                        const real Weight = _REAL_ONE_ - DistancePerpendicular / SafeLineWidth;
                                        pDisplayImage->GetReferenceToValueAt(X, Y).ComposeRGBA(Color, Weight * Weight);
                                    }
                                }
                            }
                    }
                    else
                    {
                        const real FixedAlpha = Color.GetNormalizedAlphaChannel();
                        Mathematics::_2D::CVector2D P(X0, Y0);
                        for (coordinate Y = Y0; Y < Y1; ++Y, P.SetY(Y))
                            for (coordinate X = X0; X < X1; ++X, P.SetX(X))
                            {
                                const Mathematics::_2D::CVector2D Delta = P - A;
                                const real DistanceAlong = Direction.ScalarProduct(Delta);
                                if ((DistanceAlong >= _REAL_ZERO_) && (DistanceAlong <= Lenght))
                                {
                                    const real DistancePerpendicular = RealAbs(Normal.ScalarProduct(Delta));
                                    if (DistancePerpendicular < SafeLineWidth)
                                    {
                                        const real Weight = _REAL_ONE_ - DistancePerpendicular / SafeLineWidth;
                                        pDisplayImage->GetReferenceToValueAt(X, Y).ComposeRGBA(Color, Weight * FixedAlpha);
                                    }
                                }
                            }
                    }
                    return true;
                }
            }
            return false;
        }

        bool CImageExporter::DisplayFlagMap(const CImageActiveZone* pActiveZone, const TImage<bool>* pInputImage, TImage<CDiscreteTristimulusPixel>* pDisplayImage)
        {
            if (pActiveZone && pInputImage && pDisplayImage && pActiveZone->IsEnabled() && pInputImage->IsValid() && pDisplayImage->IsValid() && (pInputImage->GetSize() == pDisplayImage->GetSize()) && (pActiveZone->GetSize() == pInputImage->GetSize()))
            {
                pDisplayImage->Clear();
                const coordinate X0 = pActiveZone->GetX0();
                const coordinate Y0 = pActiveZone->GetY0();
                const coordinate X1 = pActiveZone->GetX1();
                const coordinate Y1 = pActiveZone->GetY1();
                const coordinate Width = pActiveZone->GetWidth();
                const bool* pBaseInputPixel = pInputImage->GetReadOnlyBufferAt(X0, Y0);
                CDiscreteTristimulusPixel* pBaseDisplayPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                for (coordinate y = Y0; y < Y1; ++y, pBaseInputPixel += Width, pBaseDisplayPixel += Width)
                {
                    const bool* pInputPixel = pBaseInputPixel;
                    CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                    for (coordinate x = X0; x < X1; ++x, ++pDisplayPixel)
                        if (*pInputPixel++)
                        {
                            *pDisplayPixel = byte(255);
                        }
                }
                return true;
            }
            return false;
        }

        bool CImageExporter::DisplayClappedIntensity(const CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, TImage<CDiscreteTristimulusPixel>* pDisplayImage)
        {
            if (pActiveZone && pInputImage && pDisplayImage && pActiveZone->IsEnabled() && pInputImage->IsValid() && pDisplayImage->IsValid() && (pInputImage->GetSize() == pDisplayImage->GetSize()) && (pActiveZone->GetSize() == pInputImage->GetSize()))
            {
                pDisplayImage->Clear();
                const coordinate X0 = pActiveZone->GetX0();
                const coordinate Y0 = pActiveZone->GetY0();
                const coordinate X1 = pActiveZone->GetX1();
                const coordinate Y1 = pActiveZone->GetY1();
                const coordinate Width = pActiveZone->GetWidth();
                const real* pBaseInputPixel = pInputImage->GetReadOnlyBufferAt(X0, Y0);
                CDiscreteTristimulusPixel* pBaseDisplayPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                for (coordinate y = Y0; y < Y1; ++y, pBaseInputPixel += Width, pBaseDisplayPixel += Width)
                {
                    const real* pInputPixel = pBaseInputPixel;
                    CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                    for (coordinate x = X0; x < X1; ++x, ++pInputPixel)
                        if (*pInputPixel > _REAL_255_)
                        {
                            *pDisplayPixel++ = byte(255);
                        }
                        else if (*pInputPixel >= _REAL_ZERO_)
                        {
                            *pDisplayPixel++ = *pInputPixel;
                        }
                }
                return true;
            }
            return false;
        }

        bool CImageExporter::DisplayRoundedIntensity(const CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, TImage<CDiscreteTristimulusPixel>* pDisplayImage)
        {
            if (pActiveZone && pInputImage && pDisplayImage && pActiveZone->IsEnabled() && pInputImage->IsValid() && pDisplayImage->IsValid() && (pInputImage->GetSize() == pDisplayImage->GetSize()) && (pActiveZone->GetSize() == pInputImage->GetSize()))
            {
                pDisplayImage->Clear();
                const coordinate X0 = pActiveZone->GetX0();
                const coordinate Y0 = pActiveZone->GetY0();
                const coordinate X1 = pActiveZone->GetX1();
                const coordinate Y1 = pActiveZone->GetY1();
                const coordinate Width = pActiveZone->GetWidth();
                const real* pBaseInputPixel = pInputImage->GetReadOnlyBufferAt(X0, Y0);
                CDiscreteTristimulusPixel* pBaseDisplayPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                for (coordinate y = Y0; y < Y1; ++y, pBaseInputPixel += Width, pBaseDisplayPixel += Width)
                {
                    const real* pInputPixel = pBaseInputPixel;
                    CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                    for (coordinate x = X0; x < X1; ++x)
                    {
                        *pDisplayPixel++ = *pInputPixel++;
                    }
                }
                return true;
            }
            return false;
        }

        bool CImageExporter::DisplayScaledIntensity(const CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, TImage<CDiscreteTristimulusPixel>* pDisplayImage)
        {
            if (pActiveZone && pInputImage && pDisplayImage && pActiveZone->IsEnabled() && pInputImage->IsValid() && pDisplayImage->IsValid() && (pInputImage->GetSize() == pDisplayImage->GetSize()) && (pActiveZone->GetSize() == pInputImage->GetSize()))
            {
                pDisplayImage->Clear();
                real Maximal = _REAL_ZERO_, Minimal = _REAL_ZERO_;
                const real Range = GetExtrema(pActiveZone, pInputImage, Maximal, Minimal);
                if (Range > _REAL_EPSILON_)
                {
                    const real Scale = _REAL_255_ / Range;
                    const coordinate X0 = pActiveZone->GetX0();
                    const coordinate Y0 = pActiveZone->GetY0();
                    const coordinate X1 = pActiveZone->GetX1();
                    const coordinate Y1 = pActiveZone->GetY1();
                    const coordinate Width = pActiveZone->GetWidth();
                    const real* pBaseInputPixel = pInputImage->GetReadOnlyBufferAt(X0, Y0);
                    CDiscreteTristimulusPixel* pBaseDisplayPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; ++y, pBaseInputPixel += Width, pBaseDisplayPixel += Width)
                    {
                        const real* pInputPixel = pBaseInputPixel;
                        CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                        for (coordinate x = X0; x < X1; ++x)
                        {
                            *pDisplayPixel++ = (*pInputPixel++ - Minimal) * Scale;
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        bool CImageExporter::DisplayColorMapped(const CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, TImage<CDiscreteTristimulusPixel>* pDisplayImage, const Visualization::CTristimulusColorMap* pTristimulusColorMap)
        {
            if (pActiveZone && pInputImage && pDisplayImage && pTristimulusColorMap && pActiveZone->IsEnabled() && pInputImage->IsValid() && pDisplayImage->IsValid() && (pInputImage->GetSize() == pDisplayImage->GetSize()) && (pActiveZone->GetSize() == pInputImage->GetSize()))
            {
                pDisplayImage->Clear();
                real Maximal = _REAL_ZERO_, Minimal = _REAL_ZERO_;
                const real Range = GetExtrema(pActiveZone, pInputImage, Maximal, Minimal);
                if (Range > _REAL_EPSILON_)
                {
                    const real Scale = pTristimulusColorMap->GetColorLookupTableScaleFactor() / Range;
                    const coordinate X0 = pActiveZone->GetX0();
                    const coordinate Y0 = pActiveZone->GetY0();
                    const coordinate X1 = pActiveZone->GetX1();
                    const coordinate Y1 = pActiveZone->GetY1();
                    const coordinate Width = pActiveZone->GetWidth();
                    const real* pBaseInputPixel = pInputImage->GetReadOnlyBufferAt(X0, Y0);
                    const CDiscreteTristimulusPixel* pLUT = pTristimulusColorMap->GetColorLookupTable();
                    CDiscreteTristimulusPixel* pBaseDisplayPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; ++y, pBaseInputPixel += Width, pBaseDisplayPixel += Width)
                    {
                        const real* pInputPixel = pBaseInputPixel;
                        CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                        for (coordinate x = X0; x < X1; ++x)
                        {
                            *pDisplayPixel++ = pLUT[RoundPositiveRealToInteger((*pInputPixel++ - Minimal) * Scale)];
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        bool CImageExporter::DisplayRounded(const CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, TImage<CDiscreteTristimulusPixel>* pDisplayImage)
        {
            if (pInputImage && pInputImage->IsValid() && pDisplayImage && pDisplayImage->IsValid() && (pInputImage->GetSize() == pDisplayImage->GetSize()))
            {
                pDisplayImage->Clear();
                if (pActiveZone)
                {
                    if (pActiveZone->IsEnabled())
                    {
                        const coordinate X0 = pActiveZone->GetX0();
                        const coordinate Y0 = pActiveZone->GetY0();
                        const coordinate X1 = pActiveZone->GetX1();
                        const coordinate Y1 = pActiveZone->GetY1();
                        const coordinate Width = pActiveZone->GetWidth();
                        const CContinuousTristimulusPixel* pBaseInputPixel = pInputImage->GetReadOnlyBufferAt(X0, Y0);
                        CDiscreteTristimulusPixel* pBaseDisplayPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                        for (coordinate y = Y0; y < Y1; ++y, pBaseInputPixel += Width, pBaseDisplayPixel += Width)
                        {
                            const CContinuousTristimulusPixel* pInputPixel = pBaseInputPixel;
                            CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                            for (coordinate x = X0; x < X1; ++x)
                            {
                                *pDisplayPixel++ = *pInputPixel++;
                            }
                        }
                        return true;
                    }
                }
                else
                {
                    const CContinuousTristimulusPixel* const pEndInputPixel = pInputImage->GetEndReadOnlyBuffer();
                    const CContinuousTristimulusPixel* pInputPixel = pInputImage->GetBeginReadOnlyBuffer();
                    CDiscreteTristimulusPixel* pDisplayPixel = pDisplayImage->GetBeginWritableBuffer();
                    while (pInputPixel < pEndInputPixel)
                    {
                        *pDisplayPixel++ = *pInputPixel++;
                    }
                    return true;
                }
            }
            return false;
        }

        bool CImageExporter::DisplayScaled(const CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, TImage<CDiscreteTristimulusPixel>* pDisplayImage)
        {
            if (pActiveZone && pInputImage && pDisplayImage && pActiveZone->IsEnabled() && pInputImage->IsValid() && pDisplayImage->IsValid() && (pInputImage->GetSize() == pDisplayImage->GetSize()) && (pActiveZone->GetSize() == pInputImage->GetSize()))
            {
                pDisplayImage->Clear();
                CContinuousTristimulusPixel Maximal, Minimal;
                const CContinuousTristimulusPixel Range = GetExtrema(pActiveZone, pInputImage, Maximal, Minimal);
                if (Range.IsValidRange())
                {
                    const CContinuousTristimulusPixel Scale = Range.GetConditionalScaled(_REAL_255_);
                    const coordinate X0 = pActiveZone->GetX0();
                    const coordinate Y0 = pActiveZone->GetY0();
                    const coordinate X1 = pActiveZone->GetX1();
                    const coordinate Y1 = pActiveZone->GetY1();
                    const coordinate Width = pActiveZone->GetWidth();
                    const CContinuousTristimulusPixel* pBaseInputPixel = pInputImage->GetReadOnlyBufferAt(X0, Y0);
                    CDiscreteTristimulusPixel* pBaseDisplayPixel = pDisplayImage->GetWritableBufferAt(X0, Y0);
                    for (coordinate y = Y0; y < Y1; ++y, pBaseInputPixel += Width, pBaseDisplayPixel += Width)
                    {
                        const CContinuousTristimulusPixel* pInputPixel = pBaseInputPixel;
                        CDiscreteTristimulusPixel* pDisplayPixel = pBaseDisplayPixel;
                        for (coordinate x = X0; x < X1; ++x, ++pDisplayPixel)
                        {
                            pDisplayPixel->SetShiftedScaledValue(*pInputPixel++, Scale, Minimal);
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        void CImageExporter::LoadColorTables()
        {
            if (!s_InitializedColorTables)
            {
                for (uint i = 0; i < 256; ++i)
                {
                    s_IntensityColorTable[i].SetValue(i, i, i, 0);
                    s_BooleanColorTable[i].SetValue(255, 255, 255, 0);
                }
                s_BooleanColorTable[0].SetValue(0, 0, 0, 0);
                s_InitializedColorTables = true;
            }
        }

        const CRGBAColor* CImageExporter::GetIntensityColorTable()
        {
            LoadColorTables();
            return s_IntensityColorTable;
        }

        const CRGBAColor* CImageExporter::GetBooleanColorTable()
        {
            LoadColorTables();
            return s_BooleanColorTable;
        }

        //http://upload.wikimedia.org/wikipedia/commons/c/c4/BMPfileFormat.png
        struct BitMapFileHeader
        {
            //Specifies the file type. It must be set to the signature word BM (0x4D42) to indicate bitmap.
            uint16_t m_Signature;

            //Specifies the size, in bytes, of the bitmap file.
            uint32_t m_FileSize;

            //Reserved; set to zero
            uint16_t m_Reserved1;

            //Reserved; set to zero
            uint16_t m_Reserved2;

            //Specifies the offset, in bytes, from the BitMapFileHeader structure to the bitmap bits
            uint32_t m_OffsetToPixelArray;
        };

        struct BitMapInformatioinHeader
        {
            //Specifies the size of the structure, in bytes. This size does not include the color table or the masks mentioned in the m_TotalColorIndices member.
            uint32_t m_Size;

            //Specifies the width of the bitmap, in pixels.
            uint32_t m_Width;

            //Specifies the height of the bitmap, in pixels.
            //If m_Height is positive, the bitmap is a bottom-up DIB and its origin is the lower left corner.
            //If m_Height is negative, the bitmap is a top-down DIB and its origin is the upper left corner.
            //If m_Height is negative, indicating a top-down DIB, m_Compression must be either BI_RGB or BI_BITFIELDS. Top-down DIBs cannot be compressed.
            int32_t m_Height;

            //Specifies the number of planes for the target device. This value must be set to 1.
            uint16_t m_Planes;

            //Specifies the number of bits per pixel. This member can be one of the following values 1,2,4,8,16,24,32
            uint16_t m_BitCount;

            //Specifies the type of compression for a compressed bottom-up bitmap (top-down DIBs cannot be compressed). This member can be one of the following values.
            //Value     Compression     Comments
            //  0           none        Most common
            //  6           Bit field   For Alpha use
            uint32_t m_Compression;

            //Specifies the size, in bytes, of the image. This may be set to zero for BI_RGB bitmaps.
            uint32_t m_SizeImage;

            //Specifies the horizontal resolution, in pixels per meter, of the target device for the bitmap. An application can use this value to select a bitmap from a resource group that best matches the characteristics of the current device.
            uint32_t m_XPixelsPerMeter;

            //Specifies the vertical resolution, in pixels per meter, of the target device for the bitmap
            uint32_t m_YPixelsPerMeter;

            //Specifies the number of color indices in the color table that are actually used by the bitmap.
            //If this value is zero, the bitmap uses the maximum number of colors corresponding to the value of the m_BitCount member for the compression mode specified by m_Compression.
            //If the bitmap is a packed bitmap the m_TotalColorIndices member must be either zero or the actual size of the color table.
            uint32_t m_TotalColorIndices;

            //Specifies the number of color indexes required for displaying the bitmap. If this value is zero, all colors are required.
            uint32_t m_RequiredColorIndices;
        };

        bool CImageExporter::SaveToFileBMP(const_string pPathFileName, const uint Width, const uint Height, const uint bytesPerPixel, const byte* pDataBuffer, const CRGBAColor* pColorTable)
        {
            if ((bytesPerPixel == 1) && (!pColorTable))
            {
                return false;
            }
            const uint LineBufferSize = Width * bytesPerPixel;
            const uint Padding = (LineBufferSize % 4) ? (4 - (LineBufferSize % 4)) : 0;
            const uint PaddedLineBufferSize = LineBufferSize + Padding;
            BitMapFileHeader FileHeader;
            memset(&FileHeader, 0, sizeof(BitMapFileHeader));
            FileHeader.m_Signature = 0X4D42;
            BitMapInformatioinHeader InformationHeader;
            memset(&InformationHeader, 0, sizeof(BitMapInformatioinHeader));
            InformationHeader.m_Size = 40;
            InformationHeader.m_Width = Width;
            InformationHeader.m_Height = int32_t(Height);
            InformationHeader.m_Planes = 1;
            InformationHeader.m_BitCount = bytesPerPixel * 8;
            InformationHeader.m_SizeImage = PaddedLineBufferSize * Height;
            InformationHeader.m_Compression = 0;
            FileHeader.m_OffsetToPixelArray = 14 + 40 + ((bytesPerPixel == 1) ? 1024 : 0);
            FileHeader.m_FileSize = FileHeader.m_OffsetToPixelArray + InformationHeader.m_SizeImage;
            Files::COutFile OutputBinary(pPathFileName, Files::CFile::eBinary);
            if (OutputBinary.IsReady())
            {
                //ATTENTION: THIS CANNOT BE DONE IN A SINGLE CALL DUE TO BYTE-ALLIGMENT DEVIATIONS.
                OutputBinary.WriteBuffer(&FileHeader.m_Signature, sizeof(uint16_t));
                OutputBinary.WriteBuffer(&FileHeader.m_FileSize, sizeof(uint32_t));
                OutputBinary.WriteBuffer(&FileHeader.m_Reserved1, sizeof(uint16_t));
                OutputBinary.WriteBuffer(&FileHeader.m_Reserved2, sizeof(uint16_t));
                OutputBinary.WriteBuffer(&FileHeader.m_OffsetToPixelArray, sizeof(uint32_t));
                OutputBinary.WriteBuffer(&InformationHeader.m_Size, sizeof(uint32_t));
                OutputBinary.WriteBuffer(&InformationHeader.m_Width, sizeof(uint32_t));
                OutputBinary.WriteBuffer(&InformationHeader.m_Height, sizeof(uint32_t));
                OutputBinary.WriteBuffer(&InformationHeader.m_Planes, sizeof(uint16_t));
                OutputBinary.WriteBuffer(&InformationHeader.m_BitCount, sizeof(uint16_t));
                OutputBinary.WriteBuffer(&InformationHeader.m_Compression, sizeof(uint32_t));
                OutputBinary.WriteBuffer(&InformationHeader.m_SizeImage, sizeof(uint32_t));
                OutputBinary.WriteBuffer(&InformationHeader.m_XPixelsPerMeter, sizeof(uint32_t));
                OutputBinary.WriteBuffer(&InformationHeader.m_YPixelsPerMeter, sizeof(uint32_t));
                OutputBinary.WriteBuffer(&InformationHeader.m_TotalColorIndices, sizeof(uint32_t));
                OutputBinary.WriteBuffer(&InformationHeader.m_RequiredColorIndices, sizeof(uint32_t));
                switch (bytesPerPixel)
                {
                    case 1:
                    {
                        OutputBinary.WriteBuffer(pColorTable, 1024);
                        const byte* pDataBufferBase = pDataBuffer + (Width * (Height - 1));
                        byte* pFormatedImageBuffer = new byte[InformationHeader.m_SizeImage];
                        byte* pOutputBuffer = pFormatedImageBuffer;
                        for (uint y = 0; y < Height; ++y, pOutputBuffer += PaddedLineBufferSize, pDataBufferBase -= LineBufferSize)
                        {
                            memcpy(pOutputBuffer, pDataBufferBase, LineBufferSize);
                        }
                        OutputBinary.WriteBuffer(pFormatedImageBuffer, InformationHeader.m_SizeImage);
                        RELEASE_ARRAY(pFormatedImageBuffer)
                    }
                    break;
                    case 3:
                    {
                        const byte* pDataBufferBase = pDataBuffer + (Width * (Height - 1)) * bytesPerPixel;
                        const int SourceOffset = LineBufferSize * 2;
                        byte* pFormatedImageBuffer = new byte[InformationHeader.m_SizeImage];
                        byte* pOutputBuffer = pFormatedImageBuffer;
                        for (uint y = 0; y < Height; ++y, pOutputBuffer += Padding, pDataBufferBase -= SourceOffset)
                            for (uint x = 0; x < Width; ++x, pOutputBuffer += bytesPerPixel, pDataBufferBase += bytesPerPixel)
                            {
                                pOutputBuffer[ChromaticSpaceRGB::eRed] = pDataBufferBase[ChromaticSpaceRGB::eBlue];
                                pOutputBuffer[ChromaticSpaceRGB::eGreen] = pDataBufferBase[ChromaticSpaceRGB::eGreen];
                                pOutputBuffer[ChromaticSpaceRGB::eBlue] = pDataBufferBase[ChromaticSpaceRGB::eRed];
                            }
                        OutputBinary.WriteBuffer(pFormatedImageBuffer, InformationHeader.m_SizeImage);
                        RELEASE_ARRAY(pFormatedImageBuffer)
                    }
                    break;
                    case 4:
                    {
                        const byte* pDataBufferBase = pDataBuffer + (Width * (Height - 1)) * bytesPerPixel;
                        const int SourceOffset = LineBufferSize * 2;
                        byte* pFormatedImageBuffer = new byte[InformationHeader.m_SizeImage];
                        byte* pOutputBuffer = pFormatedImageBuffer;
                        for (uint y = 0; y < Height; ++y, pOutputBuffer += Padding, pDataBufferBase -= SourceOffset)
                            for (uint x = 0; x < Width; ++x, pOutputBuffer += bytesPerPixel, pDataBufferBase += bytesPerPixel)
                            {
                                pOutputBuffer[ChromaticSpaceRGBA::eRed] = pDataBufferBase[ChromaticSpaceRGBA::eBlue];
                                pOutputBuffer[ChromaticSpaceRGBA::eGreen] = pDataBufferBase[ChromaticSpaceRGBA::eGreen];
                                pOutputBuffer[ChromaticSpaceRGBA::eBlue] = pDataBufferBase[ChromaticSpaceRGBA::eRed];
                                pOutputBuffer[ChromaticSpaceRGBA::eAlpha] = pDataBufferBase[ChromaticSpaceRGBA::eAlpha];
                            }
                        OutputBinary.WriteBuffer(pFormatedImageBuffer, InformationHeader.m_SizeImage);
                        RELEASE_ARRAY(pFormatedImageBuffer)
                    }
                    break;
                }
                return OutputBinary.Close();
            }
            return false;
        }

        bool CImageExporter::ExportToBMP(const_string pPathFileName, const TImage<bool>* pImage, const CRGBAColor* pColorTable)
        {
            if (pPathFileName && pImage && pImage->IsValid())
            {
                return SaveToFileBMP(pPathFileName, pImage->GetWidth(), pImage->GetHeight(), pImage->GetBytesPerPixel(), reinterpret_cast<const byte*>(pImage->GetBeginReadOnlyBuffer()), pColorTable ? pColorTable : GetBooleanColorTable());
            }
            return false;
        }

        bool CImageExporter::ExportToBMP(const_string pPathFileName, const TImage<byte>* pImage, const CRGBAColor* pColorTable)
        {
            if (pPathFileName && pImage && pImage->IsValid())
            {
                return SaveToFileBMP(pPathFileName, pImage->GetWidth(), pImage->GetHeight(), pImage->GetBytesPerPixel(), reinterpret_cast<const byte*>(pImage->GetBeginReadOnlyBuffer()), pColorTable ? pColorTable : GetIntensityColorTable());
            }
            return false;
        }

        bool CImageExporter::ExportToBMP(const_string pPathFileName, const TImage<CDiscreteTristimulusPixel>* pImage)
        {
            if (pPathFileName && pImage && pImage->IsValid())
            {
                return SaveToFileBMP(pPathFileName, pImage->GetWidth(), pImage->GetHeight(), pImage->GetBytesPerPixel(), reinterpret_cast<const byte*>(pImage->GetBeginReadOnlyBuffer()), nullptr);
            }
            return false;
        }

        bool CImageExporter::ExportToBMP(const_string pPathFileName, const TImage<CDiscreteRGBAPixel>* pImage)
        {
            if (pPathFileName && pImage && pImage->IsValid())
            {
                return SaveToFileBMP(pPathFileName, pImage->GetWidth(), pImage->GetHeight(), pImage->GetBytesPerPixel(), reinterpret_cast<const byte*>(pImage->GetBeginReadOnlyBuffer()), nullptr);
            }
            return false;
        }

        real CImageExporter::GetExtrema(const CImageActiveZone* pActiveZone, const TImage<real>* pInputImage, real& Maximal, real& Minimal)
        {
            const uint ActiveWidth = pActiveZone->GetActiveWidth();
            const uint ActiveHeight = pActiveZone->GetActiveHeight();
            const uint Offset = pActiveZone->GetHorizontalOffset();
            const real* pInputPixel = pInputImage->GetReadOnlyBufferAt(pActiveZone->GetUpperLeftLocation());
            Minimal = pInputImage->GetReadOnlyReferenceToValueAt(pActiveZone->GetUpperLeftLocation());
            for (uint i = 0; i < ActiveHeight; ++i, pInputPixel += Offset)
                for (uint j = 0; j < ActiveWidth; ++j, ++pInputPixel)
                    if (*pInputPixel > Maximal)
                    {
                        Maximal = *pInputPixel;
                    }
                    else if (*pInputPixel < Minimal)
                    {
                        Minimal = *pInputPixel;
                    }
            return Maximal - Minimal;
        }

        CContinuousTristimulusPixel CImageExporter::GetExtrema(const CImageActiveZone* pActiveZone, const TImage<CContinuousTristimulusPixel>* pInputImage, CContinuousTristimulusPixel& Maximal, CContinuousTristimulusPixel& Minimal)
        {
            const uint ActiveWidth = pActiveZone->GetActiveWidth();
            const uint ActiveHeight = pActiveZone->GetActiveHeight();
            const uint Offset = pActiveZone->GetHorizontalOffset();
            const CContinuousTristimulusPixel* pInputPixel = pInputImage->GetReadOnlyBufferAt(pActiveZone->GetUpperLeftLocation());
            Minimal = pInputImage->GetReadOnlyReferenceToValueAt(pActiveZone->GetUpperLeftLocation());
            for (uint i = 0; i < ActiveHeight; ++i, pInputPixel += Offset)
                for (uint j = 0; j < ActiveWidth; ++j, ++pInputPixel)
                {
                    Maximal.SetChannelsToMaximalValue(pInputPixel);
                    Minimal.SetChannelsToMinimalValue(pInputPixel);
                }
            return CContinuousTristimulusPixel(Maximal, Minimal);
        }
    }
}
