/*
 * DiscreteBoundingBox2D.cpp
 *
 *  Created on: 01.04.2011
 *      Author: gonzalez
 */

#include "DiscreteBoundingBox2D.h"

namespace EVP
{
    namespace VisualSpace
    {
        CDiscreteBoundingBox2D::CDiscreteBoundingBox2D() :
            m_MinPoint(), m_MaxPoint()
        {
        }

        CDiscreteBoundingBox2D::CDiscreteBoundingBox2D(const CPixelLocation& PixelLocation) :
            m_MinPoint(PixelLocation), m_MaxPoint(PixelLocation)
        {
        }

        CDiscreteBoundingBox2D::CDiscreteBoundingBox2D(const CDiscreteBoundingBox2D& BBox2D)
            = default;

        CDiscreteBoundingBox2D::CDiscreteBoundingBox2D(const coordinate x0, const coordinate y0, const coordinate x1, const coordinate y1)
        {
            m_MinPoint.Set(TMin(x0, x1), TMin(y0, y1));
            m_MinPoint.Set(TMax(x0, x1), TMax(y0, y1));
        }

        CDiscreteBoundingBox2D::CDiscreteBoundingBox2D(const CPixelLocation& MinPixelLocation, const CPixelLocation& MaxPixelLocation)
        {
            m_MinPoint.Set(TMin(MinPixelLocation.m_x, MaxPixelLocation.m_x), TMin(MinPixelLocation.m_y, MaxPixelLocation.m_y));
            m_MaxPoint.Set(TMax(MinPixelLocation.m_x, MaxPixelLocation.m_x), TMax(MinPixelLocation.m_y, MaxPixelLocation.m_y));
        }

        void CDiscreteBoundingBox2D::SetBounds(const CDiscreteBoundingBox2D& BBox2D)
        {
            if (BBox2D.IsEmpty())
            {
                SetEmpty();
            }
            else
            {
                m_MinPoint = BBox2D.m_MinPoint;
                m_MaxPoint = BBox2D.m_MaxPoint;
            }
        }

        void CDiscreteBoundingBox2D::SetBounds(const coordinate x0, const coordinate y0, const coordinate x1, const coordinate y1)
        {
            m_MinPoint.Set(TMin(x0, x1), TMin(y0, y1));
            m_MaxPoint.Set(TMax(x0, x1), TMax(y0, y1));
        }

        void CDiscreteBoundingBox2D::SetBounds(const CPixelLocation& MinPoint, const CPixelLocation& MaxPoint)
        {
            m_MinPoint.Set(TMin(MinPoint.m_x, MaxPoint.m_x), TMin(MinPoint.m_y, MaxPoint.m_y));
            m_MaxPoint.Set(TMax(MinPoint.m_x, MaxPoint.m_x), TMax(MinPoint.m_y, MaxPoint.m_y));
        }

        bool CDiscreteBoundingBox2D::GetBounds(coordinate& x0, coordinate& y0, coordinate& x1, coordinate& y1) const
        {
            x0 = m_MinPoint.m_x;
            y0 = m_MinPoint.m_y;
            x1 = m_MaxPoint.m_x;
            y1 = m_MaxPoint.m_y;
            return !IsEmpty();
        }

        bool CDiscreteBoundingBox2D::GetBounds(CPixelLocation& MinPoint, CPixelLocation& MaxPoint) const
        {
            MinPoint = m_MinPoint;
            MaxPoint = m_MaxPoint;
            return !IsEmpty();
        }

        CPixelLocation CDiscreteBoundingBox2D::GetCenterLocation() const
        {
            return CPixelLocation(coordinate(RealRound(real(m_MaxPoint.m_x + m_MinPoint.m_x) * _REAL_HALF_)), coordinate(RealRound(real(m_MaxPoint.m_y + m_MinPoint.m_y) * _REAL_HALF_)));
        }

        const CPixelLocation& CDiscreteBoundingBox2D::GetMinLocation() const
        {
            return m_MinPoint;
        }

        const CPixelLocation& CDiscreteBoundingBox2D::GetMaxLocation() const
        {
            return m_MaxPoint;
        }

        bool CDiscreteBoundingBox2D::Intersects(const CDiscreteBoundingBox2D& BBox2D) const
        {
            if (BBox2D.m_MinPoint.m_x < m_MinPoint.m_x)
            {
                if (m_MinPoint.m_x > BBox2D.m_MaxPoint.m_x)
                {
                    return false;
                }
            }
            else
            {
                if (BBox2D.m_MinPoint.m_x > m_MaxPoint.m_x)
                {
                    return false;
                }
            }
            if (BBox2D.m_MinPoint.m_y < m_MinPoint.m_y)
            {
                if (m_MinPoint.m_y > BBox2D.m_MaxPoint.m_y)
                {
                    return false;
                }
            }
            else
            {
                if (BBox2D.m_MinPoint.m_y > m_MaxPoint.m_y)
                {
                    return false;
                }
            }
            return true;
        }

        bool CDiscreteBoundingBox2D::GetIntersection(const CDiscreteBoundingBox2D& BBox2D, CDiscreteBoundingBox2D& IntersectionBox) const
        {
            if (Intersects(BBox2D))
            {
                IntersectionBox.m_MinPoint.m_x = BBox2D.m_MinPoint.m_x > m_MinPoint.m_x ? BBox2D.m_MinPoint.m_x : m_MinPoint.m_x;
                IntersectionBox.m_MinPoint.m_y = BBox2D.m_MinPoint.m_y > m_MinPoint.m_y ? BBox2D.m_MinPoint.m_y : m_MinPoint.m_y;
                IntersectionBox.m_MaxPoint.m_x = BBox2D.m_MinPoint.m_x < m_MinPoint.m_x ? BBox2D.m_MinPoint.m_x : m_MinPoint.m_x;
                IntersectionBox.m_MinPoint.m_y = BBox2D.m_MinPoint.m_y < m_MinPoint.m_y ? BBox2D.m_MinPoint.m_y : m_MinPoint.m_y;
                return true;
            }
            else
            {
                IntersectionBox.SetEmpty();
                return false;
            }
        }

        bool CDiscreteBoundingBox2D::IsEmpty() const
        {
            return (m_MinPoint.m_x > m_MaxPoint.m_x) || (m_MinPoint.m_y > m_MaxPoint.m_y);
        }

        void CDiscreteBoundingBox2D::SetEmpty()
        {
            m_MinPoint = CPixelLocation::s_Maximal;
            m_MaxPoint = CPixelLocation::s_Minimal;
        }

        bool CDiscreteBoundingBox2D::IsInside(const CPixelLocation& Point, bool Inclusive) const
        {
            return Inclusive ? (Point.m_x >= m_MinPoint.m_x) && (Point.m_x <= m_MaxPoint.m_x) && (Point.m_y >= m_MinPoint.m_y) && (Point.m_y <= m_MaxPoint.m_y) : (Point.m_x > m_MinPoint.m_x) && (Point.m_x < m_MaxPoint.m_x) && (Point.m_y > m_MinPoint.m_y) && (Point.m_y < m_MaxPoint.m_y);
        }

        uint CDiscreteBoundingBox2D::GetArea() const
        {
            return IsEmpty() ? 0 : (m_MaxPoint.m_x - m_MinPoint.m_x + 1) * (m_MaxPoint.m_y - m_MinPoint.m_y + 1);
        }

        real CDiscreteBoundingBox2D::GetAspectRatio() const
        {
            return IsEmpty() ? _REAL_ZERO_ : (m_MaxPoint.m_x - m_MinPoint.m_x + 1) / (m_MaxPoint.m_y - m_MinPoint.m_y + 1);
        }

        uint CDiscreteBoundingBox2D::GetWidth() const
        {
            return IsEmpty() ? 0 : uint(m_MaxPoint.m_x - m_MinPoint.m_x + 1);
        }

        uint CDiscreteBoundingBox2D::GetHeight() const
        {
            return IsEmpty() ? 0 : uint(m_MaxPoint.m_y - m_MinPoint.m_y + 1);
        }

        void CDiscreteBoundingBox2D::Extend(const CPixelLocation& PixelLocation)
        {
            m_MaxPoint.SetcoordinatesToMaximalValue(PixelLocation);
            m_MinPoint.SetTocoordinatesToMinimalValue(PixelLocation);
        }

        void CDiscreteBoundingBox2D::Extend(const coordinate X, const coordinate Y)
        {
            m_MaxPoint.SetcoordinatesToMaximalValue(X, Y);
            m_MinPoint.SetTocoordinatesToMinimalValue(X, Y);
        }

        void CDiscreteBoundingBox2D::Expand(const coordinate Delta)
        {
            if (!IsEmpty())
            {
                m_MaxPoint.m_x += Delta;
                m_MaxPoint.m_y += Delta;
                m_MinPoint.m_x -= Delta;
                m_MinPoint.m_y -= Delta;
            }
        }
    }
}
