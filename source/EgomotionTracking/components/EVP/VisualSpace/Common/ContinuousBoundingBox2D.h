/*
 * ContinuousBoundingBox2D.h
 *
 *  Created on: 01.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "../../Foundation/Mathematics/2D/Vector2D.h"
#include "../../Foundation/DataTypes/ImageSize.h"

namespace EVP
{
    class CContinuousBoundingBox2D
    {
    public:

        CContinuousBoundingBox2D();
        CContinuousBoundingBox2D(const Mathematics::_2D::CVector2D& InitialPoint);
        CContinuousBoundingBox2D(const CContinuousBoundingBox2D& BBox2D);
        CContinuousBoundingBox2D(const real X0, const real Y0, const real X1, const real Y1);
        CContinuousBoundingBox2D(const Mathematics::_2D::CVector2D& MinPoint, const Mathematics::_2D::CVector2D& MaxPoint);

        void SetBounds(const CContinuousBoundingBox2D& BBox2D);
        void SetBounds(const real X0, const real Y0, const real X1, const real Y1);
        void SetBounds(const Mathematics::_2D::CVector2D& MinPoint, const Mathematics::_2D::CVector2D& MaxPoint);

        bool GetBounds(real& X0, real& Y0, real& X1, real& Y1) const;
        bool GetBounds(Mathematics::_2D::CVector2D& MinPoint, Mathematics::_2D::CVector2D& MaxPoint) const;

        real GetDistance(const CContinuousBoundingBox2D& BBox2D) const;

        Mathematics::_2D::CVector2D GetCenterPoint() const;
        const Mathematics::_2D::CVector2D& GetMinPoint() const;
        const Mathematics::_2D::CVector2D& GetMaxPoint() const;

        bool Intersects(const CContinuousBoundingBox2D& BBox2D) const;
        bool GetIntersection(const CContinuousBoundingBox2D& BBox2D, CContinuousBoundingBox2D& IntersectionBox) const;
        bool IsEmpty() const;
        void SetEmpty();
        bool IsInside(const Mathematics::_2D::CVector2D& Point, const bool Inclusive = true) const;

        real GetArea() const;
        real GetAspectRatio() const;
        real GetWidth() const;
        real GetHeight() const;
        real GetDiagonalLenght() const;

        void SetInitial(const Mathematics::_2D::CVector2D& Point);

        void Extend(const Mathematics::_2D::CVector2D& Point);
        void Extend(const real X, const real Y);
        bool Expand(const real Delta);

        CImageSize GetEffectiveDiscreteSize() const;
        void operator=(const CContinuousBoundingBox2D& BBox2D);

    protected:

        Mathematics::_2D::CVector2D m_MinPoint;
        Mathematics::_2D::CVector2D m_MaxPoint;
    };
}

