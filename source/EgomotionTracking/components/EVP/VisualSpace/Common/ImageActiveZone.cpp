/*
 * ImageActiveZone.cpp
 *
 *  Created on: 22.03.2011
 *      Author: gonzalez
 */

#include "ImageActiveZone.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    CImageActiveZone::CImageActiveZone() :
        m_HierarchicalUpdateMethod(eDirect), m_pSourceZone(nullptr), m_pData(nullptr), m_FixedOffset(0), m_pfOffsetUpdateCallBack(nullptr), m_IsEnabled(false)
    {
    }

    CImageActiveZone::CImageActiveZone(CImageActiveZone* pSourceZone, const coordinate Offset, const bool Connect) :
        m_HierarchicalUpdateMethod(eDirect), m_pSourceZone(nullptr), m_pData(nullptr), m_FixedOffset(0), m_pfOffsetUpdateCallBack(nullptr), m_IsEnabled(false)
    {
        if (pSourceZone)
        {
            if (Connect)
            {
                SetConnection(pSourceZone, Offset);
            }
            else
            {
                Set(pSourceZone->GetWidth(), pSourceZone->GetHeight(), pSourceZone->GetX0(), pSourceZone->GetY0(), pSourceZone->GetX1(), pSourceZone->GetY1());
            }
        }
    }

    CImageActiveZone::CImageActiveZone(const CImageActiveZone* pActiveZone, const CImageSize& Size) :
        m_HierarchicalUpdateMethod(eDirect), m_pSourceZone(nullptr), m_pData(nullptr), m_FixedOffset(0), m_pfOffsetUpdateCallBack(nullptr), m_IsEnabled(false)
    {
        Set(pActiveZone, Size);
    }

    CImageActiveZone::CImageActiveZone(const coordinate Width, const coordinate Height, const coordinate X0, const coordinate Y0, const coordinate X1, const coordinate Y1) :
        m_HierarchicalUpdateMethod(eDirect), m_pSourceZone(nullptr), m_pData(nullptr), m_FixedOffset(0), m_pfOffsetUpdateCallBack(nullptr), m_IsEnabled(false)
    {
        m_IsEnabled = Set(Width, Height, X0, Y0, X1, Y1);
    }

    CImageActiveZone::CImageActiveZone(const CImageSize& Size, const coordinate Margin) :
        m_HierarchicalUpdateMethod(eDirect), m_pSourceZone(nullptr), m_pData(nullptr), m_FixedOffset(0), m_pfOffsetUpdateCallBack(nullptr), m_IsEnabled(false)
    {
        m_IsEnabled = Set(Size, Margin);
    }

    CImageActiveZone::CImageActiveZone(const coordinate Width, const coordinate Height, const coordinate Margin) :
        m_HierarchicalUpdateMethod(eDirect), m_pSourceZone(nullptr), m_pData(nullptr), m_FixedOffset(0), m_pfOffsetUpdateCallBack(nullptr), m_IsEnabled(false)
    {
        m_IsEnabled = Set(Width, Height, Margin, Margin, Width - Margin, Height - Margin);
    }

    CImageActiveZone::~CImageActiveZone()
    {
        ClearSubZones();
    }

    bool CImageActiveZone::IsEnabled() const
    {
        return m_IsEnabled;
    }

    bool CImageActiveZone::AddMarginOffset(const coordinate Offset)
    {
        if (m_IsEnabled && (Offset > 0))
        {
            return Set(m_Size.GetWidth(), m_Size.GetHeight(), m_UpperLeft.m_x + Offset, m_UpperLeft.m_y + Offset, m_LowerRight.m_x - Offset, m_LowerRight.m_y - Offset);
        }
        return false;
    }

    bool CImageActiveZone::Set(const CImageActiveZone* pActiveZone, const CImageSize& Size)
    {
        if (pActiveZone)
        {
            if (m_Size.IsValid() && (m_Size != pActiveZone->m_Size))
            {
                m_IsEnabled = false;
                return m_IsEnabled;
            }
            m_UpperLeft = pActiveZone->m_UpperLeft;
            m_LowerRight = pActiveZone->m_LowerRight;
            m_Size = pActiveZone->m_Size;
            m_IsEnabled = pActiveZone->m_IsEnabled;
            UpdateSubZones();
        }
        else
        {
            Set(Size.GetWidth(), Size.GetHeight(), 0, 0, Size.GetWidth(), Size.GetHeight());
        }
        return m_IsEnabled;
    }

    bool CImageActiveZone::Set(const CImageSize& Size)
    {
        return Set(Size.GetWidth(), Size.GetHeight(), 0, 0, Size.GetWidth(), Size.GetHeight());
    }

    bool CImageActiveZone::Set(const CImageSize& Size, const coordinate Margin)
    {
        return Set(Size.GetWidth(), Size.GetHeight(), Margin, Margin, Size.GetWidth() - Margin, Size.GetHeight() - Margin);
    }

    bool CImageActiveZone::Set(const CImageSize& Size, const coordinate X0, const coordinate Y0, const coordinate X1, const coordinate Y1)
    {
        return Set(Size.GetWidth(), Size.GetHeight(), X0, Y0, X1, Y1);
    }

    bool CImageActiveZone::Set(const CImageActiveZone* pActiveZone, const coordinate Margin)
    {
        m_IsEnabled = pActiveZone && (pActiveZone->GetSize() == m_Size);
        if (m_IsEnabled)
        {
            return Set(pActiveZone->GetWidth(), pActiveZone->GetHeight(), pActiveZone->GetX0() + Margin, pActiveZone->GetY0() + Margin, pActiveZone->GetX1() - Margin, pActiveZone->GetY1() - Margin);
        }
        return false;
    }

    bool CImageActiveZone::Set(const coordinate Width, const coordinate Height, const coordinate Margin)
    {
        return Set(Width, Height, Margin, Margin, Width - Margin, Height - Margin);
    }

    bool CImageActiveZone::Set(const coordinate Width, const coordinate Height, const coordinate X0, const coordinate Y0, const coordinate X1, const coordinate Y1)
    {
        m_IsEnabled = ((Width > 0) && (Height > 0) && (X0 >= 0) && (X0 < Width) && (Y0 >= 0) && (Y0 < Height) && (X1 >= X0) && (X1 <= Width) && (Y1 >= Y0) && (Y1 <= Height));
        if (m_IsEnabled)
        {
            m_Size.Set(Width, Height);
            m_UpperLeft.Set(X0, Y0);
            m_LowerRight.Set(X1, Y1);
        }
        UpdateSubZones();
        return m_IsEnabled;
    }

    CImageActiveZone CImageActiveZone::Intersection(const CImageActiveZone* pImageActiveZone) const
    {
        CImageActiveZone IntersectionActiveZone;
        if (m_IsEnabled && pImageActiveZone && pImageActiveZone->IsEnabled() && (pImageActiveZone->m_Size == m_Size))
        {
            IntersectionActiveZone.Set(m_Size.GetWidth(), m_Size.GetHeight(), TMax(pImageActiveZone->m_UpperLeft.m_x, m_UpperLeft.m_x), TMax(pImageActiveZone->m_UpperLeft.m_y, m_UpperLeft.m_y), TMin(pImageActiveZone->m_LowerRight.m_x, m_LowerRight.m_x), TMin(pImageActiveZone->m_LowerRight.m_y, m_LowerRight.m_y));
        }
        return IntersectionActiveZone;
    }

    const CImageSize& CImageActiveZone::GetSize() const
    {
        return m_Size;
    }

    CImageSize CImageActiveZone::GetActiveSize() const
    {
        return CImageSize(m_LowerRight.m_x - m_UpperLeft.m_x, m_LowerRight.m_y - m_UpperLeft.m_y);
    }

    uint CImageActiveZone::GetArea() const
    {
        return m_Size.GetArea();
    }

    uint CImageActiveZone::GetActiveArea() const
    {
        return uint(m_LowerRight.m_x - m_UpperLeft.m_x) * uint(m_LowerRight.m_y - m_UpperLeft.m_y);
    }

    uint CImageActiveZone::GetActiveWidth() const
    {
        return (m_LowerRight.m_x - m_UpperLeft.m_x);
    }

    uint CImageActiveZone::GetActiveHeight() const
    {
        return (m_LowerRight.m_y - m_UpperLeft.m_y);
    }

    uint CImageActiveZone::GetHorizontalOffset() const
    {
        return m_Size.GetWidth() - (m_LowerRight.m_x - m_UpperLeft.m_x);
    }

    bool CImageActiveZone::IsFullScopeActive() const
    {
        return (m_Size.GetHeight() == GetActiveHeight()) && (m_Size.GetWidth() == GetActiveWidth());
    }

    uint CImageActiveZone::GetWidth() const
    {
        return m_Size.GetWidth();
    }

    uint CImageActiveZone::GetHeight() const
    {
        return m_Size.GetHeight();
    }

    const CPixelLocation& CImageActiveZone::GetUpperLeftLocation() const
    {
        return m_UpperLeft;
    }

    const CPixelLocation& CImageActiveZone::GetLowerRightLocation() const
    {
        return m_LowerRight;
    }

    coordinate CImageActiveZone::GetX0() const
    {
        return m_UpperLeft.m_x;
    }

    coordinate CImageActiveZone::GetY0() const
    {
        return m_UpperLeft.m_y;
    }

    coordinate CImageActiveZone::GetX1() const
    {
        return m_LowerRight.m_x;
    }

    coordinate CImageActiveZone::GetY1() const
    {
        return m_LowerRight.m_y;
    }

    bool CImageActiveZone::IsAtBorder(const CPixelLocation& Location) const
    {
        if ((Location.m_x == m_UpperLeft.m_x) || (Location.m_x == m_LowerRight.m_x))
        {
            return (Location.m_y >= m_UpperLeft.m_y) && (Location.m_y <= m_LowerRight.m_y);
        }
        else if ((Location.m_y == m_UpperLeft.m_y) || (Location.m_y == m_LowerRight.m_y))
        {
            return (Location.m_x >= m_UpperLeft.m_x) && (Location.m_x <= m_LowerRight.m_x);
        }
        return false;
    }

    bool CImageActiveZone::IsRegionInside(const coordinate X0, const coordinate Y0, const coordinate X1, const coordinate Y1, const bool Inclusive) const
    {
        if (Inclusive)
        {
            return (X0 >= m_UpperLeft.m_x) && (X0 <= m_LowerRight.m_x) && (X1 >= m_UpperLeft.m_x) && (X1 <= m_LowerRight.m_x) && (Y0 >= m_UpperLeft.m_y) && (Y0 <= m_LowerRight.m_y) && (Y1 >= m_UpperLeft.m_y) && (Y1 <= m_LowerRight.m_y);
        }
        else
        {
            return (X0 >= m_UpperLeft.m_x) && (X0 < m_LowerRight.m_x) && (X1 >= m_UpperLeft.m_x) && (X1 < m_LowerRight.m_x) && (Y0 >= m_UpperLeft.m_y) && (Y0 < m_LowerRight.m_y) && (Y1 >= m_UpperLeft.m_y) && (Y1 < m_LowerRight.m_y);
        }
    }

    bool CImageActiveZone::IsContinuosLocationInside(const Mathematics::_2D::CVector2D& ContinuosLocation) const
    {
        return (RealFloor(ContinuosLocation.GetX()) >= m_UpperLeft.m_x) && (RealCeil(ContinuosLocation.GetX()) < m_LowerRight.m_x) && (RealFloor(ContinuosLocation.GetY()) >= m_UpperLeft.m_y) && (RealCeil(ContinuosLocation.GetY()) < m_LowerRight.m_y);
    }

    bool CImageActiveZone::IsDiscreteLocationInside(const CPixelLocation& Location, const bool Inclusive) const
    {
        return Inclusive ? ((Location.m_x >= m_UpperLeft.m_x) && (Location.m_x <= m_LowerRight.m_x) && (Location.m_y >= m_UpperLeft.m_y) && (Location.m_y <= m_LowerRight.m_y)) : ((Location.m_x > m_UpperLeft.m_x) && (Location.m_x < m_LowerRight.m_x) && (Location.m_y > m_UpperLeft.m_y) && (Location.m_y < m_LowerRight.m_y));
    }

    bool CImageActiveZone::IsDiscreteLocationInside(const coordinate X, const coordinate Y, const bool Inclusive) const
    {
        return Inclusive ? ((X >= m_UpperLeft.m_x) && (X <= m_LowerRight.m_x) && (Y >= m_UpperLeft.m_y) && (Y <= m_LowerRight.m_y)) : ((X > m_UpperLeft.m_x) && (X < m_LowerRight.m_x) && (Y > m_UpperLeft.m_y) && (Y < m_LowerRight.m_y));
    }

    bool CImageActiveZone::IsFullyInside(const CContinuousBoundingBox2D& ContinuousBoundingBox) const
    {
        return (ContinuousBoundingBox.GetMinPoint().GetX() >= m_UpperLeft.m_x) && (ContinuousBoundingBox.GetMinPoint().GetY() >= m_UpperLeft.m_y) && (ContinuousBoundingBox.GetMaxPoint().GetX() <= m_LowerRight.m_x) && (ContinuousBoundingBox.GetMaxPoint().GetY() <= m_LowerRight.m_y);
    }

    bool CImageActiveZone::IsPartiallyInside(const CContinuousBoundingBox2D& ContinuousBoundingBox) const
    {
        return (ContinuousBoundingBox.GetMinPoint().GetX() <= m_LowerRight.m_x) && (ContinuousBoundingBox.GetMinPoint().GetY() <= m_LowerRight.m_y) && (ContinuousBoundingBox.GetMaxPoint().GetX() >= m_UpperLeft.m_x) && (ContinuousBoundingBox.GetMaxPoint().GetY() >= m_UpperLeft.m_y);
    }

    bool CImageActiveZone::GetOctoConnectivityDeltas(coordinate* pOctoConnectivityDeltas) const
    {
        if (pOctoConnectivityDeltas)
        {
            const coordinate Width = m_Size.GetWidth();
            pOctoConnectivityDeltas[0] = 1;
            pOctoConnectivityDeltas[1] = -Width;
            pOctoConnectivityDeltas[2] = -1;
            pOctoConnectivityDeltas[3] = Width;
            pOctoConnectivityDeltas[4] = 1 - Width;
            pOctoConnectivityDeltas[5] = -1 - Width;
            pOctoConnectivityDeltas[6] = Width - 1;
            pOctoConnectivityDeltas[7] = Width + 1;
            return true;
        }
        return false;
    }

    bool CImageActiveZone::GetTetraConnectivityDeltas(coordinate* pTetraConnectivityDeltas) const
    {
        if (pTetraConnectivityDeltas)
        {
            const coordinate Width = m_Size.GetWidth();
            pTetraConnectivityDeltas[0] = 1;
            pTetraConnectivityDeltas[1] = 1 - Width;
            pTetraConnectivityDeltas[2] = -Width;
            pTetraConnectivityDeltas[3] = -1 - Width;
            return true;
        }
        return false;
    }

    void CImageActiveZone::SetConnection(CImageActiveZone* pSourceZone)
    {
        if (pSourceZone && (pSourceZone != this))
        {
            m_HierarchicalUpdateMethod = eDirect;
            Disconnect(true, false);
            m_pSourceZone = pSourceZone;
            if (m_pSourceZone)
            {
                m_pSourceZone->AddSubZone(this);
                UpdateFromSource();
            }
        }
    }

    void CImageActiveZone::SetConnection(CImageActiveZone* pSourceZone, const real ScaleFactor)
    {
        if (pSourceZone && (pSourceZone != this) && (ScaleFactor > _REAL_ZERO_))
        {
            m_HierarchicalUpdateMethod = eScaling;
            Disconnect(true, false);
            m_ScaleFactor = ScaleFactor;
            m_pSourceZone = pSourceZone;
            m_pSourceZone->AddSubZone(this);
            UpdateFromSource();
        }
    }

    void CImageActiveZone::SetConnection(CImageActiveZone* pSourceZone, void* pData, OffsetUpdateCallBack OffsetUpdateFunction)
    {
        if (pSourceZone && (pSourceZone != this) && pData && OffsetUpdateFunction)
        {
            m_HierarchicalUpdateMethod = eCallBack;
            Disconnect(true, false);
            m_pSourceZone = pSourceZone;
            m_pData = pData;
            m_pfOffsetUpdateCallBack = OffsetUpdateFunction;
            m_pSourceZone->AddSubZone(this);
            UpdateFromSource();
        }
    }

    void CImageActiveZone::SetConnection(CImageActiveZone* pSourceZone, const coordinate Offset)
    {
        if (pSourceZone && (pSourceZone != this))
        {
            m_HierarchicalUpdateMethod = eFixedOffsets;
            Disconnect(true, false);
            m_pSourceZone = pSourceZone;
            m_FixedOffset = Offset;
            m_pSourceZone->AddSubZone(this);
            UpdateFromSource();
        }
    }

    void CImageActiveZone::Disconnect(const bool FromSourceZone, const bool FromSubZones)
    {
        if (m_pSourceZone && FromSourceZone)
        {
            CImageActiveZone* pSourceZone = m_pSourceZone;
            m_pSourceZone = nullptr;
            m_pData = nullptr;
            m_pfOffsetUpdateCallBack = nullptr;
            pSourceZone->RemoveSubZone(this);
        }
        if (FromSubZones)
        {
            ClearSubZones();
        }
    }

    bool CImageActiveZone::UpdateConnection()
    {
        bool Result = true;
        if (m_pSourceZone)
        {
            Result &= UpdateFromSource();
        }
        if (m_SubZones.size())
        {
            Result &= UpdateSubZones();
        }
        return Result;
    }

    bool CImageActiveZone::UpdateFromSource()
    {
        switch (m_HierarchicalUpdateMethod)
        {
            case eDirect:
                Set(m_pSourceZone->GetWidth(), m_pSourceZone->GetHeight(), m_pSourceZone->GetX0(), m_pSourceZone->GetY0(), m_pSourceZone->GetX1(), m_pSourceZone->GetY1());
                m_IsEnabled &= m_pSourceZone->IsEnabled();
                break;
            case eCallBack:
                if (m_pData && m_pfOffsetUpdateCallBack)
                {
                    coordinate OffsetX0 = 0, OffsetY0 = 0, OffsetX1 = 0, OffsetY1 = 0;
                    if (m_pfOffsetUpdateCallBack(m_pData, OffsetX0, OffsetY0, OffsetX1, OffsetY1))
                    {
                        Set(m_pSourceZone->GetWidth(), m_pSourceZone->GetHeight(), m_pSourceZone->GetX0() + OffsetX0, m_pSourceZone->GetY0() + OffsetY0, m_pSourceZone->GetX1() - OffsetX1, m_pSourceZone->GetY1() - OffsetY0);
                        m_IsEnabled &= m_pSourceZone->IsEnabled();
                    }
                    else
                    {
                        m_IsEnabled = false;
                    }
                }
                break;
            case eFixedOffsets:
                Set(m_pSourceZone->GetWidth(), m_pSourceZone->GetHeight(), m_pSourceZone->GetX0() + m_FixedOffset, m_pSourceZone->GetY0() + m_FixedOffset, m_pSourceZone->GetX1() - m_FixedOffset, m_pSourceZone->GetY1() - m_FixedOffset);
                m_IsEnabled &= m_pSourceZone->IsEnabled();
                break;
            case eScaling:
                Set(UpperToInteger(real(m_pSourceZone->GetWidth()) * m_ScaleFactor), UpperToInteger(real(m_pSourceZone->GetHeight()) * m_ScaleFactor), UpperToInteger(m_ScaleFactor * real(m_pSourceZone->m_UpperLeft.m_x)), UpperToInteger(m_ScaleFactor * real(m_pSourceZone->m_UpperLeft.m_y)), DownToInteger(m_ScaleFactor * real(m_pSourceZone->m_LowerRight.m_x)), DownToInteger(m_ScaleFactor * real(m_pSourceZone->m_LowerRight.m_y)));
                m_IsEnabled &= m_pSourceZone->IsEnabled();
                break;
        }
        return m_IsEnabled;
    }

    bool CImageActiveZone::UpdateSubZones()
    {
        if (m_SubZones.size())
        {
            bool AllSubZonesSuccess = true;
            list<CImageActiveZone*>::iterator EndubZones = m_SubZones.end();
            for (list<CImageActiveZone*>::iterator ppImageActiveZone = m_SubZones.begin(); ppImageActiveZone != EndubZones; ++ppImageActiveZone)
            {
                AllSubZonesSuccess &= (*ppImageActiveZone)->UpdateFromSource();
            }
            return AllSubZonesSuccess;
        }
        return true;
    }

    void CImageActiveZone::ClearSubZones()
    {
        if (m_SubZones.size())
        {
            list<CImageActiveZone*>::iterator End = m_SubZones.end();
            for (list<CImageActiveZone*>::iterator ppImageActiveZone = m_SubZones.begin(); ppImageActiveZone != End; ++ppImageActiveZone)
            {
                CImageActiveZone* pSubZone = *ppImageActiveZone;
                pSubZone->m_pData = nullptr;
                pSubZone->m_pfOffsetUpdateCallBack = nullptr;
                pSubZone->m_pSourceZone = nullptr;
            }
            m_SubZones.clear();
        }
    }

    void CImageActiveZone::RemoveSubZone(CImageActiveZone* pSubZone)
    {
        if (pSubZone && m_SubZones.size())
        {
            list<CImageActiveZone*>::iterator End = m_SubZones.end();
            for (list<CImageActiveZone*>::iterator ppImageActiveZone = m_SubZones.begin(); ppImageActiveZone != End; ++ppImageActiveZone)
                if ((*ppImageActiveZone) == pSubZone)
                {
                    m_SubZones.erase(ppImageActiveZone);
                    return;
                }
        }
    }

    void CImageActiveZone::AddSubZone(CImageActiveZone* pSubZone)
    {
        if (pSubZone && (pSubZone != this))
        {
            if (m_SubZones.size())
            {
                list<CImageActiveZone*>::iterator End = m_SubZones.end();
                for (list<CImageActiveZone*>::iterator ppImageActiveZone = m_SubZones.begin(); ppImageActiveZone != End; ++ppImageActiveZone)
                    if ((*ppImageActiveZone) == pSubZone)
                    {
                        return;
                    }
            }
            if (pSubZone->m_HierarchicalUpdateMethod != eScaling)
            {
                pSubZone->m_Size = m_Size;
            }
            m_SubZones.push_back(pSubZone);
        }
    }
}
