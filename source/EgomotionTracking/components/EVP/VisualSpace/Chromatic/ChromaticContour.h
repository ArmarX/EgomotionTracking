/*
 * ChromaticContour.h
 *
 *  Created on: 01.04.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "../Common/DiscreteBoundingBox2D.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Chromatic
        {
            class CChromaticPixel;
            class CChromaticBlock;

            class CChromaticContour
            {
            public:

                CChromaticContour(CChromaticPixel* pChromaticPixel, const int* pDeltas, const uint TotalDeltas);
                virtual ~CChromaticContour();

                const list<CChromaticPixel*>& GetChromaticPixels() const;
                uint GetTotalChromaticPixels() const;
                const CDiscreteBoundingBox2D& GetBoundingBox() const;
                const CChromaticBlock* GetChromaticBlock() const;

            protected:

                const CChromaticBlock* m_pChromaticBlock;
                CDiscreteBoundingBox2D m_BoundingBox;
                list<CChromaticPixel*> m_ChromaticPixels;
            };
        }
    }
}

