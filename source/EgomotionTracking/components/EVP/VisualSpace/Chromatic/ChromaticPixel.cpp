/*
 * ChromaticPixel.cpp
 *
 *  Created on: 30.03.2011
 *      Author: gonzalez
 */

#include "ChromaticPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Chromatic
        {

            CChromaticPixel::CChromaticPixel() :
                m_IsEnabled(false), m_IsAtImageActiveZoneBorder(false), m_Status(eUnknownStatus), m_EdgeDistance(_REAL_MAX_), m_Location(), m_pChromaticBlock(nullptr), m_pRGB(nullptr), m_Tristimulus()
            {
            }

            CChromaticPixel::~CChromaticPixel()
                = default;

            void CChromaticPixel::Reset()
            {
                m_pChromaticBlock = nullptr;
                m_Status = eLinkable;
                m_EdgeDistance = _REAL_MAX_;
            }

            void CChromaticPixel::ClearEdgeDistance()
            {
                m_EdgeDistance = _REAL_MAX_;
            }

            bool CChromaticPixel::SetEdgeDistance(const real EdgeDistance)
            {
                if (EdgeDistance < m_EdgeDistance)
                {
                    m_EdgeDistance = EdgeDistance;
                    return true;
                }
                return false;
            }

            real CChromaticPixel::GetEdgeDistance() const
            {
                return m_EdgeDistance;
            }

            void CChromaticPixel::SetChromaticBlock(CChromaticBlock* pChromaticBlock)
            {
                m_pChromaticBlock = pChromaticBlock;
                m_Status = eLinked;
            }

            CChromaticBlock* CChromaticPixel::GetWritableChromaticBlock()
            {
                return m_pChromaticBlock;
            }

            const CChromaticBlock* CChromaticPixel::GetReadOnlyChromaticBlock() const
            {
                return m_pChromaticBlock;
            }

            void CChromaticPixel::SetConnectivityStatus(const Status CurrentConnectivityStatus)
            {
                m_Status = CurrentConnectivityStatus;
            }

            CChromaticPixel::Status CChromaticPixel::GetStatus() const
            {
                return m_Status;
            }

            bool CChromaticPixel::IsLinkable() const
            {
                return m_IsEnabled && (m_Status == eLinkable);
            }

            bool CChromaticPixel::IsNonRedundatContourLinkable() const
            {
                return m_IsEnabled && (m_Status == eNonRedundantBorder);
            }

            bool CChromaticPixel::IsNonRedundatContourLinkable(const CChromaticBlock* pChromaticBlock) const
            {
                return m_IsEnabled && (m_Status == eNonRedundantBorder) && (pChromaticBlock == m_pChromaticBlock);
            }

            void CChromaticPixel::SetEnabled(const bool Enabled)
            {
                m_IsEnabled = Enabled;
            }

            bool CChromaticPixel::IsEnabled() const
            {
                return m_IsEnabled;
            }

            bool CChromaticPixel::Initialize(const bool Enabled, const CPixelLocation& DiscreteLocation, const CContinuousTristimulusPixel* pRGB)
            {
                m_IsEnabled = Enabled;
                m_Location = DiscreteLocation;
                m_pRGB = pRGB;
                return m_IsEnabled;
            }

            void CChromaticPixel::SetLocation(const CPixelLocation& DiscreteLocation)
            {
                m_Location = DiscreteLocation;
            }

            const CPixelLocation& CChromaticPixel::GetLocation() const
            {
                return m_Location;
            }

            void CChromaticPixel::SetAtImageActiveZoneBorder(const bool IsAtBorder)
            {
                m_IsAtImageActiveZoneBorder = IsAtBorder;
            }

            bool CChromaticPixel::IsAtImageActiveZoneBorder() const
            {
                return m_IsAtImageActiveZoneBorder;
            }

            void CChromaticPixel::SetChromaticSource(const CContinuousTristimulusPixel* pRGB)
            {
                m_pRGB = pRGB;
            }

            const CContinuousTristimulusPixel& CChromaticPixel::GetChromaticValue() const
            {
                return *m_pRGB;
            }

            real CChromaticPixel::GetManhattanRGBDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_pRGB->GetRGBChebyshevDistance(*pChromaticPixel->m_pRGB);
            }

            real CChromaticPixel::GetSquareEuclideanRGBDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_pRGB->GetRGBSquareEuclideanDistance(*pChromaticPixel->m_pRGB);
            }

            real CChromaticPixel::GetEuclideanRGBDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_pRGB->GetRGBEuclideanDistance(*pChromaticPixel->m_pRGB);
            }

            real CChromaticPixel::GetChebyshevRGBDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_pRGB->GetRGBChebyshevDistance(*pChromaticPixel->m_pRGB);
            }

            real CChromaticPixel::GetMinkowskiRGBDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const
            {
                return m_pRGB->GetRGBMinkowskiDistance(*pChromaticPixel->m_pRGB, P, IP);
            }

#ifdef _USE_COLOR_SPACE_HSL_

            void CChromaticPixel::ConvertRGBToHSL()
            {
                m_Tristimulus.SetFromContinuousRGBToHSL(m_pRGB);
            }

            real CChromaticPixel::GetManhattanHSLDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetHSLManhattanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetSquareEuclideanHSLDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetHSLSquareEuclideanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetEuclideanHSLDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetHSLEuclideanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetChebyshevHSLDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetHSLChebyshevDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetMinkowskiHSLDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const
            {
                return m_Tristimulus.GetHSLMinkowskiDistance(pChromaticPixel->m_Tristimulus, P, IP);
            }
#endif

#ifdef _USE_COLOR_SPACE_XYZ_

            void CChromaticPixel::ConvertRGBToXYZ()
            {
                m_Tristimulus.SetFromContinuousRGBToXYZ(m_pRGB);
            }

            real CChromaticPixel::GetManhattanXYZDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetXYZManhattanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetChebyshevXYZDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetXYZChebyshevDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetSquareEuclideanXYZDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetXYZSquareEuclideanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetEuclideanXYZDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetXYZEuclideanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetMinkowskiXYZDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const
            {
                return m_Tristimulus.GetXYZMinkowskiDistance(pChromaticPixel->m_Tristimulus, P, IP);
            }
#endif

#ifdef _USE_COLOR_SPACE_LAB_

            void CChromaticPixel::ConvertRGBToLabD50()
            {
                m_Tristimulus.SetFromContinuousRGBToLAB_D50(m_pRGB);
            }

            void CChromaticPixel::ConvertRGBToLabD55()
            {
                m_Tristimulus.SetFromContinuousRGBToLAB_D55(m_pRGB);
            }

            void CChromaticPixel::ConvertRGBToLabD65()
            {
                m_Tristimulus.SetFromContinuousRGBToLAB_D65(m_pRGB);
            }

            void CChromaticPixel::ConvertRGBToLabD75()
            {
                m_Tristimulus.SetFromContinuousRGBToLAB_D75(m_pRGB);
            }

            real CChromaticPixel::GetChebyshevLabDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetLABChebyshevDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetManhattanLabDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetLABManhattanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetSquareEuclideanLabDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetLABSquareEuclideanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetEuclideanLabDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetLABEuclideanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetMinkowskiLabDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const
            {
                return m_Tristimulus.GetLABMinkowskiDistance(pChromaticPixel->m_Tristimulus, P, IP);
            }
#endif

#ifdef _USE_COLOR_SPACE_HSI_

            void CChromaticPixel::ConvertRGBToHSI()
            {
                m_Tristimulus.SetFromContinuousRGBToHSI(m_pRGB);
            }

            real CChromaticPixel::GetManhattanHSIDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetHSIManhattanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetChebyshevHSIDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetHSIChebyshevDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetSquareEuclideanHSIDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetHSISquareEuclideanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetEuclideanHSIDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetHSIEuclideanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetMinkowskiHSIDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const
            {
                return m_Tristimulus.GetHSIMinkowskiDistance(pChromaticPixel->m_Tristimulus, P, IP);
            }
#endif

#ifdef _USE_COLOR_SPACE_HSV_

            void CChromaticPixel::ConvertRGBToHSV()
            {
                m_Tristimulus.SetFromContinuousRGBToHSV(m_pRGB);
            }

            real CChromaticPixel::GetManhattanHSVDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetHSVManhattanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetChebyshevHSVDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetHSVChebyshevDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetSquareEuclideanHSVDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetHSVSquareEuclideanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetEuclideanHSVDistance(const CChromaticPixel* pChromaticPixel) const
            {
                return m_Tristimulus.GetHSVEuclideanDistance(pChromaticPixel->m_Tristimulus);
            }

            real CChromaticPixel::GetMinkowskiHSVDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const
            {
                return m_Tristimulus.GetHSVMinkowskiDistance(pChromaticPixel->m_Tristimulus, P, IP);
            }
#endif

            bool CChromaticPixel::IsAtBorder(const int* pDeltas, const uint TotalDeltas) const
            {
                for (uint i = 0; i < TotalDeltas; ++i)
                    if ((this + *pDeltas++)->m_pChromaticBlock != m_pChromaticBlock)
                    {
                        return true;
                    }
                return false;
            }

            bool CChromaticPixel::IsDynamicallyNonRedundatBorder(const int* pDeltas, const uint TotalDeltas) const
            {
                for (uint i = 0; i < TotalDeltas; ++i, ++pDeltas)
                    if (((this + *pDeltas)->m_Status == eRedundantCore) && ((this + *pDeltas)->m_pChromaticBlock == m_pChromaticBlock))
                    {
                        return true;
                    }
                return false;
            }

            bool CChromaticPixel::IsDynamicallyRedundantCore(const int* pDeltas, const uint TotalDeltas) const
            {
                if (m_Status == eRedundantCore)
                    for (uint i = 0; i < TotalDeltas; ++i, ++pDeltas)
                        if (((this + *pDeltas)->m_Status == eRedundantCore) && ((this + *pDeltas)->m_pChromaticBlock == m_pChromaticBlock))
                        {
                            return true;
                        }
                return false;
            }
        }
    }
}

