/*
 * ChromaticBlock.h
 *
 *  Created on: 30.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"
#include "../../Foundation/DataTypes/ContinuousTristimulusPixel.h"
#include "../../Foundation/Miscellaneous/IdentifiableInstance.h"
#include "../../Foundation/Mathematics/2D/Vector2D.h"
#include "../../Foundation/Mathematics/2D/StructuralTensor2D.h"
#include "../Common/DiscreteBoundingBox2D.h"
#include "../Common/ContinuousBoundingBox2D.h"
#include "ChromaticContour.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Chromatic
        {
            class CChromaticPixel;

            class CChromaticBlock: public CIdentifiableInstance
            {
                IDENTIFIABLE

            public:

                enum Status
                {
                    eUknowStatus = 0, eResidual, ePreSelected, eSplit, eSubBlock, eSelected
                };

                CChromaticBlock(CChromaticPixel* pChromaticPixel);
                CChromaticBlock(list<CChromaticPixel*>& ChromaticPixelsSubCore, CChromaticBlock* pParent, const int* pDeltas, const uint TotalDeltas);
                ~CChromaticBlock() override;

                inline void SetActive(const bool Active)
                {
                    m_IsActive = Active;
                }

                inline  bool IsActive() const
                {
                    return m_IsActive;
                }

                inline const list<CChromaticPixel*>& GetChromaticPixels() const
                {
                    return m_ChromaticPixels;
                }

                inline const list<CChromaticPixel*>& GetChromaticPixelsAtImageActiveZoneBorder() const
                {
                    return m_ChromaticPixelsAtImageActiveZoneBorder;
                }

                inline const list<CChromaticPixel*>& GetChromaticPixelsRedundantBorder() const
                {
                    return m_ChromaticPixelsRedundantBorder;
                }

                inline const list<CChromaticPixel*>& GetChromaticPixelsNonRedundantBorder() const
                {
                    return m_ChromaticPixelsNonRedundantBorder;
                }

                inline const list<CChromaticPixel*>& GetChromaticPixelsCore() const
                {
                    return m_ChromaticPixelsRedundantCore;
                }

                inline const list<CChromaticContour*>& GetChromaticContoures() const
                {
                    return m_ChromaticContoures;
                }

                inline  uint GetTotalChromaticPixels() const
                {
                    return m_ChromaticPixels.size();
                }

                inline  uint GetTotalChromaticPixelsAtImageActiveZoneBorder() const
                {
                    return m_ChromaticPixelsAtImageActiveZoneBorder.size();
                }

                inline  uint GetTotalChromaticPixelsRedundantBorder() const
                {
                    return m_ChromaticPixelsRedundantBorder.size();
                }

                inline  uint GetTotalChromaticPixelsNonRedundantBorder() const
                {
                    return m_ChromaticPixelsNonRedundantBorder.size();
                }

                inline  uint GetTotalChromaticPixelsRedundatCore() const
                {
                    return m_ChromaticPixelsRedundantCore.size();
                }

                inline  uint GetTotalChromaticContoures() const
                {
                    return m_ChromaticContoures.size();
                }

                inline const CDiscreteBoundingBox2D& GetChromaticPixelsBoundingBox() const
                {
                    return m_ChromaticPixelsBoundingBox;
                }

                /*
                 void SetActive(bool Active);
                 bool IsActive() const;
                 list<CChromaticPixel*>& GetChromaticPixels();
                 list<CChromaticPixel*>& GetChromaticPixelsAtImageActiveZoneBorder();
                 list<CChromaticPixel*>& GetChromaticPixelsRedundantBorder();
                 list<CChromaticPixel*>& GetChromaticPixelsNonRedundantBorder();
                 list<CChromaticPixel*>& GetChromaticPixelsCore();
                 list<CChromaticContour*>& GetChromaticContoures();
                 int GetTotalChromaticPixelsAtImageActiveZoneBorder();
                 int GetTotalChromaticPixelsRedundantBorder();
                 int GetTotalChromaticPixelsNonRedundantBorder();
                 int GetTotalChromaticPixelsRedundatCore();
                 int GetTotalChromaticContoures();
                 const CDiscreteBoundingBox2D& GetChromaticPixelsBoundingBox();*/

                uint Expand(const real DistanceThreshold, const real MinkowskiFactor, const ChromaticSpace ChromaticSpace, const CChromaticPixel::DistanceMetric DistanceMetric, const int* pDeltas, const uint TotalDeltas);
                void SplitPixelsByConnectivity(const int* pDeltas, const uint TotalDeltas);
                void FilterSpuriousCore(const int* pDeltas, const uint TotalDeltas);
                list<CChromaticBlock*> SplitInsolatedSubCores(const int* pDeltas, const uint TotalDeltas, const uint MinimalPixelsPerBlock);
                void SplitBorderPixelsByRedundancy(const int* pDeltas, const uint TotalDeltas);
                void ExtractContoures(const int* pDeltas, const uint TotalDeltas);
                void DistanceTransformation(const int* pDeltas, const uint TotalDeltas);
                void ComputeSpatialEigenStructure();

                real GetBorderCoreRatio();
                const CContinuousTristimulusPixel& GetMeanRGBValue() const;

                Status GetStatus();
                void SetStatus(Status CurrentStatus);

                CChromaticBlock* GetPreselectedSurroundingBlock(const int* pDeltas, const uint TotalDeltas);
                void AbsorveFromChildBlock(CChromaticBlock* pChildChromaticBlock);
                void AsimilateToParentBlock();

                CChromaticBlock* GetParent();
                void SetParent(CChromaticBlock* pChromaticBlock);

            protected:

                //RGB
                inline void ExpandRGBManhatan(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandRGBSquareEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandRGBEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandRGBMinkowski(const real DistanceThreshold, const real P, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandRGBChebyshev(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                //XYZ
                inline void ExpandXYZManhatan(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandXYZSquareEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandXYZEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandXYZMinkowski(const real DistanceThreshold, const real P, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandXYZChebyshev(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                //Lab
                inline void ExpandLabManhatan(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandLabSquareEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandLabEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandLabMinkowski(const real DistanceThreshold, const real P, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandLabChebyshev(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                //HSL
                inline void ExpandHSLManhatan(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandHSLSquareEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandHSLEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandHSLMinkowski(const real DistanceThreshold, const real P, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandHSLChebyshev(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                //HSI
                inline void ExpandHSIManhatan(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandHSISquareEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandHSIEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandHSIMinkowski(const real DistanceThreshold, const real P, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandHSIChebyshev(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                //HSV
                inline void ExpandHSVManhatan(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandHSVSquareEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandHSVEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandHSVMinkowski(const real DistanceThreshold, const real P, const int* pDeltas, const uint TotalDeltas);
                inline void ExpandHSVChebyshev(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas);

                inline void AddChromaticPixel(CChromaticPixel* pChromaticPixel);
                inline void ComputeMeanRGB();
                static bool SortContouresBySize(CChromaticContour* plhs, CChromaticContour* prhs);
                inline void ExtractNonRedundatBorderFromParent(const int* pDeltas, const uint TotalDeltas);

                bool m_IsActive;
                CContinuousTristimulusPixel m_MeanRGB;
                Status m_Status;
                CChromaticBlock* m_pParent;
                bool m_IsSingular;
                Mathematics::_2D::CVector2D m_Centroid;
                Mathematics::_2D::CVector2D m_MainAxis;
                Mathematics::_2D::CVector2D m_SecondaryAxis;
                Mathematics::_2D::CVector2D m_StandarDeviation;
                CContinuousBoundingBox2D m_BlockEigenBoundingBox;
                CDiscreteBoundingBox2D m_ChromaticPixelsBoundingBox;
                list<CChromaticPixel*> m_ChromaticPixels;
                list<CChromaticPixel*> m_ChromaticPixelsAtImageActiveZoneBorder;
                list<CChromaticPixel*> m_ChromaticPixelsRedundantBorder;
                list<CChromaticPixel*> m_ChromaticPixelsNonRedundantBorder;
                list<CChromaticPixel*> m_ChromaticPixelsRedundantCore;

                //TODO [COLOR SEGMENTATION REVIEW]
                //TList<CChromaticPixel*> m_ChromaticPixelsNonRedundantCore;

                list<CChromaticContour*> m_ChromaticContoures;
            };
        }
    }
}

