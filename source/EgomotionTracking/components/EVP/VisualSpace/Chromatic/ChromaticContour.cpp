/*
 * ChromaticContour.cpp
 *
 *  Created on: 01.04.2011
 *      Author: gonzalez
 */

#include "ChromaticPixel.h"
#include "ChromaticBlock.h"
#include "ChromaticContour.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Chromatic
        {
            CChromaticContour::CChromaticContour(CChromaticPixel* pChromaticPixel, const int* pDeltas, const uint TotalDeltas)
            {
                m_pChromaticBlock = pChromaticPixel->GetReadOnlyChromaticBlock();
                m_ChromaticPixels.push_back(pChromaticPixel);
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    pChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    m_BoundingBox.Extend(pChromaticPixel->GetLocation());
                    pChromaticPixel->SetConnectivityStatus(CChromaticPixel::eContour);
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsNonRedundatContourLinkable(m_pChromaticBlock))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            m_ChromaticPixels.push_back(pExpansionChromaticPixel);
                        }
                    }
                }
            }

            CChromaticContour::~CChromaticContour()
                = default;

            const list<CChromaticPixel*>& CChromaticContour::GetChromaticPixels() const
            {
                return m_ChromaticPixels;
            }

            uint  CChromaticContour::GetTotalChromaticPixels() const
            {
                return m_ChromaticPixels.size();
            }

            const CDiscreteBoundingBox2D& CChromaticContour::GetBoundingBox() const
            {
                return m_BoundingBox;
            }

            const CChromaticBlock* CChromaticContour::GetChromaticBlock() const
            {
                return m_pChromaticBlock;
            }
        }
    }
}

