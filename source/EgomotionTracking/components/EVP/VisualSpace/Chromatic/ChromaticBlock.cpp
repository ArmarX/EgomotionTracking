/*
 * ChromaticBlock.cpp
 *
 *  Created on: 30.03.2011
 *      Author: gonzalez
 */

#include "ChromaticPixel.h"
#include "ChromaticBlock.h"
#include "ChromaticContour.h"

using namespace EVP::Mathematics::_2D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Chromatic
        {
            IDENTIFIABLE_INITIALIZATION(CChromaticBlock)

            CChromaticBlock::CChromaticBlock(CChromaticPixel* pBaseChromaticPixel) :
                IDENTIFIABLE_CONTRUCTOR(CChromaticBlock)
            {
                m_pParent = nullptr;
                m_IsActive = true;
                m_IsSingular = false;
                m_Status = eUknowStatus;
                AddChromaticPixel(pBaseChromaticPixel);
            }

            CChromaticBlock::CChromaticBlock(list<CChromaticPixel*>& ChromaticPixelsSubCore, CChromaticBlock* pParent, const int* pDeltas, const uint TotalDeltas) :
                IDENTIFIABLE_CONTRUCTOR(CChromaticBlock)
            {
                m_pParent = pParent;
                m_IsActive = true;
                m_IsSingular = false;
                m_Status = eSubBlock;
                list<CChromaticPixel*>::iterator EndChromaticPixels = ChromaticPixelsSubCore.end();
                for (list<CChromaticPixel*>::iterator ppChromaticPixel = ChromaticPixelsSubCore.begin(); ppChromaticPixel != EndChromaticPixels; ++ppChromaticPixel)
                {
                    AddChromaticPixel(*ppChromaticPixel);
                }
                ComputeMeanRGB();
                m_ChromaticPixelsRedundantCore = m_ChromaticPixels;
                ExtractNonRedundatBorderFromParent(pDeltas, TotalDeltas);
            }

            CChromaticBlock::~CChromaticBlock()
            {
                list<CChromaticPixel*>::iterator EndChromaticPixels = m_ChromaticPixels.end();
                for (list<CChromaticPixel*>::iterator ppChromaticPixel = m_ChromaticPixels.begin(); ppChromaticPixel != EndChromaticPixels; ++ppChromaticPixel)
                {
                    (*ppChromaticPixel)->Reset();
                }
                if (m_ChromaticContoures.size())
                {
                    list<CChromaticContour*>::iterator EndChromaticContoures = m_ChromaticContoures.end();
                    for (list<CChromaticContour*>::iterator ppCChromaticContour = m_ChromaticContoures.begin(); ppCChromaticContour != EndChromaticContoures; ++ppCChromaticContour)
                        RELEASE_OBJECT_DIRECT(*ppCChromaticContour)
                    }
            }

            uint CChromaticBlock::Expand(const real DistanceThreshold, const real MinkowskiFactor, const ChromaticSpace ChromaticSpace, const CChromaticPixel::DistanceMetric DistanceMetric, const int* pDeltas, const uint TotalDeltas)
            {
                switch (ChromaticSpace)
                {
#ifdef _USE_COLOR_SPACE_RGB_
                    case eRGB:
                        switch (DistanceMetric)
                        {
                            case CChromaticPixel::eUnknownDistanceMetric:
                                break;
                            case CChromaticPixel::eManhatan:
                                ExpandRGBManhatan(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eSquareEuclidean:
                                ExpandRGBSquareEuclidean(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eEuclidean:
                                ExpandRGBEuclidean(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eMinkowski:
                                ExpandRGBMinkowski(DistanceThreshold, MinkowskiFactor, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eChebyshev:
                                ExpandRGBChebyshev(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                        }
                        break;
#endif
#ifdef _USE_COLOR_SPACE_XYZ_
                    case eXYZ:
                        switch (DistanceMetric)
                        {
                            case CChromaticPixel::eUnknownDistanceMetric:
                                break;
                            case CChromaticPixel::eManhatan:
                                ExpandXYZManhatan(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eSquareEuclidean:
                                ExpandXYZSquareEuclidean(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eEuclidean:
                                ExpandXYZEuclidean(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eMinkowski:
                                ExpandXYZMinkowski(DistanceThreshold, MinkowskiFactor, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eChebyshev:
                                ExpandXYZChebyshev(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                        }
                        break;
#endif
#ifdef _USE_COLOR_SPACE_LAB_
                    case eLab:
                        switch (DistanceMetric)
                        {
                            case CChromaticPixel::eUnknownDistanceMetric:
                                break;
                            case CChromaticPixel::eManhatan:
                                ExpandLabManhatan(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eSquareEuclidean:
                                ExpandLabSquareEuclidean(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eEuclidean:
                                ExpandLabEuclidean(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eMinkowski:
                                ExpandLabMinkowski(DistanceThreshold, MinkowskiFactor, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eChebyshev:
                                ExpandLabChebyshev(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                        }
                        break;
#endif
#ifdef _USE_COLOR_SPACE_HSL_
                    case eHSL:
                        switch (DistanceMetric)
                        {
                            case CChromaticPixel::eUnknownDistanceMetric:
                                break;
                            case CChromaticPixel::eManhatan:
                                ExpandHSLManhatan(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eSquareEuclidean:
                                ExpandHSLSquareEuclidean(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eEuclidean:
                                ExpandHSLEuclidean(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eMinkowski:
                                ExpandHSLMinkowski(DistanceThreshold, MinkowskiFactor, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eChebyshev:
                                ExpandHSLChebyshev(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                        }
                        break;
#endif
#ifdef _USE_COLOR_SPACE_HSI_
                    case eHSI:
                        switch (DistanceMetric)
                        {
                            case CChromaticPixel::eUnknownDistanceMetric:
                                break;
                            case CChromaticPixel::eManhatan:
                                ExpandHSIManhatan(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eSquareEuclidean:
                                ExpandHSISquareEuclidean(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eEuclidean:
                                ExpandHSIEuclidean(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eMinkowski:
                                ExpandHSIMinkowski(DistanceThreshold, MinkowskiFactor, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eChebyshev:
                                ExpandHSIChebyshev(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                        }
                        break;
#endif
#ifdef _USE_COLOR_SPACE_HSV_
                    case eHSV:
                        switch (DistanceMetric)
                        {
                            case CChromaticPixel::eUnknownDistanceMetric:
                                break;
                            case CChromaticPixel::eManhatan:
                                ExpandHSVManhatan(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eSquareEuclidean:
                                ExpandHSVSquareEuclidean(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eEuclidean:
                                ExpandHSVEuclidean(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eMinkowski:
                                ExpandHSVMinkowski(DistanceThreshold, MinkowskiFactor, pDeltas, TotalDeltas);
                                break;
                            case CChromaticPixel::eChebyshev:
                                ExpandHSVChebyshev(DistanceThreshold, pDeltas, TotalDeltas);
                                break;
                        }
                        break;
#endif
#ifdef _USE_COLOR_SPACE_RGBA_
                    case eRGBA:
                        return 0;
#endif
                    case eUnknownColorSpace:
                        return 0;

                }
                return m_ChromaticPixels.size();
            }

            void CChromaticBlock::SplitPixelsByConnectivity(const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*>::iterator EndChromaticPixels = m_ChromaticPixels.end();
                for (list<CChromaticPixel*>::iterator ppChromaticPixel = m_ChromaticPixels.begin(); ppChromaticPixel != EndChromaticPixels; ++ppChromaticPixel)
                {
                    CChromaticPixel* pChromaticPixel = *ppChromaticPixel;
                    if (pChromaticPixel->IsAtImageActiveZoneBorder())
                    {
                        pChromaticPixel->SetConnectivityStatus(CChromaticPixel::eRedundantBorder);
                        m_ChromaticPixelsAtImageActiveZoneBorder.push_back(pChromaticPixel);
                        m_ChromaticPixelsRedundantBorder.push_back(pChromaticPixel);
                    }
                    else
                    {
                        if (pChromaticPixel->IsAtBorder(pDeltas, TotalDeltas))
                        {
                            pChromaticPixel->SetConnectivityStatus(CChromaticPixel::eRedundantBorder);
                            m_ChromaticPixelsRedundantBorder.push_back(pChromaticPixel);
                        }
                        else
                        {
                            pChromaticPixel->SetConnectivityStatus(CChromaticPixel::eRedundantCore);
                            m_ChromaticPixelsRedundantCore.push_back(pChromaticPixel);
                        }
                    }
                }
            }

            void CChromaticBlock::ExtractNonRedundatBorderFromParent(const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*>::iterator EndChromaticPixels = m_ChromaticPixels.end();
                for (list<CChromaticPixel*>::iterator ppChromaticPixel = m_ChromaticPixels.begin(); ppChromaticPixel != EndChromaticPixels; ++ppChromaticPixel)
                {
                    CChromaticPixel* pChromaticPixel = *ppChromaticPixel;
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pAuxiliarChromaticPixel = pChromaticPixel + pDeltas[i];
                        if (pAuxiliarChromaticPixel->GetReadOnlyChromaticBlock() == m_pParent)
                        {
                            pAuxiliarChromaticPixel->SetChromaticBlock(this);
                            m_ChromaticPixelsNonRedundantBorder.push_back(pAuxiliarChromaticPixel);
                        }
                    }
                }
            }

            void CChromaticBlock::FilterSpuriousCore(const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> FilteredChromaticPixelsCore;
                list<CChromaticPixel*>::iterator EndChromaticPixelsCore = m_ChromaticPixelsRedundantCore.end();
                for (list<CChromaticPixel*>::iterator ppChromaticPixel = m_ChromaticPixelsRedundantCore.begin(); ppChromaticPixel != EndChromaticPixelsCore; ++ppChromaticPixel)
                {
                    CChromaticPixel* pChromaticPixel = *ppChromaticPixel;
                    if (pChromaticPixel->IsDynamicallyRedundantCore(pDeltas, TotalDeltas))
                    {
                        FilteredChromaticPixelsCore.push_back(pChromaticPixel);
                    }
                    else
                    {
                        pChromaticPixel->SetConnectivityStatus(CChromaticPixel::eRedundantBorder);
                        m_ChromaticPixelsRedundantBorder.push_back(pChromaticPixel);
                    }
                }
                if (FilteredChromaticPixelsCore.size() != m_ChromaticPixelsRedundantCore.size())
                {
                    m_ChromaticPixelsRedundantCore = FilteredChromaticPixelsCore;
                }
            }

            list<CChromaticBlock*> CChromaticBlock::SplitInsolatedSubCores(const int* pDeltas, const uint TotalDeltas, const uint MinimalPixelsPerBlock)
            {
                list<CChromaticBlock*> InsolatedSubCores;
                list<CChromaticPixel*> FilteredChromaticPixelsCore;
                list<CChromaticPixel*> ChromaticPixelsSubCore;
                list<CChromaticPixel*> ExpansionSubCore;
                uint TotalChromaticPixels = m_ChromaticPixelsRedundantCore.size();
                list<CChromaticPixel*>::iterator EndChromaticPixelsCore = m_ChromaticPixelsRedundantCore.end();
                for (list<CChromaticPixel*>::iterator ppChromaticPixel = m_ChromaticPixelsRedundantCore.begin(); ppChromaticPixel != EndChromaticPixelsCore; ++ppChromaticPixel)
                {
                    CChromaticPixel* pChromaticPixel = *ppChromaticPixel;
                    if (pChromaticPixel->GetStatus() == CChromaticPixel::eRedundantCore)
                    {
                        pChromaticPixel->SetConnectivityStatus(CChromaticPixel::eNonRedundantCore);
                        ExpansionSubCore.push_back(pChromaticPixel);
                        ChromaticPixelsSubCore.push_back(pChromaticPixel);
                        while (ExpansionSubCore.size())
                        {
                            pChromaticPixel = ExpansionSubCore.front();
                            ExpansionSubCore.pop_front();
                            for (uint i = 0; i < TotalDeltas; ++i)
                            {
                                CChromaticPixel* pAuxiliarChromaticPixel = pChromaticPixel + pDeltas[i];
                                if ((pAuxiliarChromaticPixel->GetStatus() == CChromaticPixel::eRedundantCore) && (pAuxiliarChromaticPixel->GetReadOnlyChromaticBlock() == this))
                                {
                                    pAuxiliarChromaticPixel->SetConnectivityStatus(CChromaticPixel::eNonRedundantCore);
                                    ExpansionSubCore.push_back(pAuxiliarChromaticPixel);
                                    ChromaticPixelsSubCore.push_back(pAuxiliarChromaticPixel);
                                }
                            }
                        }
                        if (ChromaticPixelsSubCore.size() == TotalChromaticPixels)
                        {
                            return InsolatedSubCores;
                        }
                        else
                        {
                            if (ChromaticPixelsSubCore.size() > MinimalPixelsPerBlock)
                            {
                                InsolatedSubCores.push_back(new CChromaticBlock(ChromaticPixelsSubCore, this, pDeltas, TotalDeltas));
                                FilteredChromaticPixelsCore.insert(FilteredChromaticPixelsCore.end(), ChromaticPixelsSubCore.begin(), ChromaticPixelsSubCore.end());
                            }
                            else
                            {
                                list<CChromaticPixel*>::iterator EndChromaticPixelsRedundatCore = ChromaticPixelsSubCore.end();
                                for (list<CChromaticPixel*>::iterator ppChromaticPixel = ChromaticPixelsSubCore.begin(); ppChromaticPixel != EndChromaticPixelsRedundatCore; ++ppChromaticPixel)
                                {
                                    (*ppChromaticPixel)->SetConnectivityStatus(CChromaticPixel::eRedundantBorder);
                                }
                            }
                            ChromaticPixelsSubCore.clear();
                        }
                    }
                }
                if (FilteredChromaticPixelsCore.size() != m_ChromaticPixelsRedundantCore.size())
                {
                    m_ChromaticPixelsRedundantCore = FilteredChromaticPixelsCore;
                }
                return InsolatedSubCores;
            }

            void CChromaticBlock::SplitBorderPixelsByRedundancy(const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*>::iterator EndChromaticPixelsRedundantBorder = m_ChromaticPixelsRedundantBorder.end();
                for (list<CChromaticPixel*>::iterator ppChromaticPixel = m_ChromaticPixelsRedundantBorder.begin(); ppChromaticPixel != EndChromaticPixelsRedundantBorder; ++ppChromaticPixel)
                {
                    CChromaticPixel* pChromaticPixel = *ppChromaticPixel;
                    if (pChromaticPixel->IsDynamicallyNonRedundatBorder(pDeltas, TotalDeltas))
                    {
                        pChromaticPixel->SetConnectivityStatus(CChromaticPixel::eNonRedundantBorder);
                        m_ChromaticPixelsNonRedundantBorder.push_back(pChromaticPixel);
                    }
                }
            }

            void CChromaticBlock::ExtractContoures(const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*>::iterator EndChromaticPixels = m_ChromaticPixelsNonRedundantBorder.end();
                for (list<CChromaticPixel*>::iterator ppChromaticPixel = m_ChromaticPixelsNonRedundantBorder.begin(); ppChromaticPixel != EndChromaticPixels; ++ppChromaticPixel)
                    if ((*ppChromaticPixel)->IsNonRedundatContourLinkable())
                    {
                        m_ChromaticContoures.push_back(new CChromaticContour(*ppChromaticPixel, pDeltas, 8));
                    }
                if (m_ChromaticContoures.size() > 1)
                {
                    m_ChromaticContoures.sort(SortContouresBySize);
                }
            }

            void CChromaticBlock::DistanceTransformation(const int* pDeltas, const uint TotalDeltas)
            {
                if (!m_ChromaticContoures.size())
                {
                    return;
                }
                const list<CChromaticPixel*>& BaseChromaticPixels = m_ChromaticContoures.front()->GetChromaticPixels();
                list<CChromaticPixel*>::const_iterator EndChromaticPixels = BaseChromaticPixels.end();
                for (list<CChromaticPixel*>::const_iterator ppChromaticPixel = BaseChromaticPixels.begin(); ppChromaticPixel != EndChromaticPixels; ++ppChromaticPixel)
                {
                    (*ppChromaticPixel)->ClearEdgeDistance();
                }
                list<CChromaticPixel*> ExpansionList = BaseChromaticPixels;
                while (ExpansionList.size())
                {
                    CChromaticPixel* pChromaticPixel = ExpansionList.front();
                    real CurrentDistance = pChromaticPixel->GetEdgeDistance() + _REAL_ONE_;
                    ExpansionList.pop_front();
                    for (uint i = 0; i < 4; ++i)
                    {
                        CChromaticPixel* pAuxiliarChromaticPixel = pChromaticPixel + pDeltas[i];
                        if (pAuxiliarChromaticPixel->GetReadOnlyChromaticBlock() == this)
                        {
                            if (pAuxiliarChromaticPixel->SetEdgeDistance(CurrentDistance))
                            {
                                ExpansionList.push_back(pAuxiliarChromaticPixel);
                            }
                        }
                    }
                    CurrentDistance = pChromaticPixel->GetEdgeDistance() + _REAL_SQRT2_;
                    for (uint i = 4; i < 8; ++i)
                    {
                        CChromaticPixel* pAuxiliarChromaticPixel = pChromaticPixel + pDeltas[i];
                        if (pAuxiliarChromaticPixel->GetReadOnlyChromaticBlock() == this)
                        {
                            if (pAuxiliarChromaticPixel->SetEdgeDistance(CurrentDistance))
                            {
                                ExpansionList.push_back(pAuxiliarChromaticPixel);
                            }
                        }
                    }
                }
            }

            void CChromaticBlock::ComputeSpatialEigenStructure()
            {
                uint AccumulatorX = 0, AccumulatorY = 0;
                list<CChromaticPixel*>::iterator EndChromaticPixels = m_ChromaticPixelsRedundantCore.end();
                for (list<CChromaticPixel*>::iterator ppChromaticPixel = m_ChromaticPixelsRedundantCore.begin(); ppChromaticPixel != EndChromaticPixels; ++ppChromaticPixel)
                {
                    const CPixelLocation& Location = (*ppChromaticPixel)->GetLocation();
                    AccumulatorX += Location.m_x;
                    AccumulatorY += Location.m_y;
                }
                const real Normalization = _REAL_ONE_ / real(m_ChromaticPixelsRedundantCore.size());
                const real Cx = real(AccumulatorX) * Normalization;
                const real Cy = real(AccumulatorY) * Normalization;
                m_Centroid.SetValue(Cx, Cy);
                real AVxx = _REAL_ZERO_, AVxy = _REAL_ZERO_, AVyy = _REAL_ZERO_;
                for (list<CChromaticPixel*>::iterator ppChromaticPixel = m_ChromaticPixelsRedundantCore.begin(); ppChromaticPixel != EndChromaticPixels; ++ppChromaticPixel)
                {
                    const CPixelLocation& Location = (*ppChromaticPixel)->GetLocation();
                    const real Dx = real(Location.m_x) - Cx;
                    const real Dy = real(Location.m_y) - Cy;
                    AVxx += Dx * Dx;
                    AVxy += Dx * Dy;
                    AVyy += Dy * Dy;
                }
                Mathematics::_2D::CStructuralTensor2D StructuralTensor2D(AVxx * Normalization, AVxy * Normalization, AVyy * Normalization);
                m_IsSingular = StructuralTensor2D.IsSingular();
                if (m_IsSingular)
                {
                    m_MainAxis = CVector2D::s_Direction_Axis_X_2D;
                    m_SecondaryAxis = CVector2D::s_Direction_Axis_Y_2D;
                    m_StandarDeviation.SetValue(_REAL_ONE_, _REAL_ONE_);
                    const CPixelLocation& MinPoint = m_ChromaticPixelsBoundingBox.GetMinLocation();
                    const CPixelLocation& MaxPoint = m_ChromaticPixelsBoundingBox.GetMaxLocation();
                    m_BlockEigenBoundingBox.SetBounds(real(MinPoint.m_x) - Cx, real(MinPoint.m_y) - Cy, real(MaxPoint.m_x) - Cx, real(MaxPoint.m_y) - Cy);
                }
                else
                {
                    m_MainAxis = StructuralTensor2D.GetUnitaryMainAxis();
                    m_MainAxis = StructuralTensor2D.GetUnitarySecondaryAxis();
                    m_StandarDeviation.SetValue(StructuralTensor2D.GetMainAxisLength(), StructuralTensor2D.GetSecondaryAxisLength());
                    for (list<CChromaticPixel*>::iterator ppChromaticPixel = m_ChromaticPixelsRedundantCore.begin(); ppChromaticPixel != EndChromaticPixels; ++ppChromaticPixel)
                    {
                        const CPixelLocation& Location = (*ppChromaticPixel)->GetLocation();
                        const CVector2D Delta(real(Location.m_x) - Cx, real(Location.m_y) - Cy);
                        m_BlockEigenBoundingBox.Extend(m_MainAxis.ScalarProduct(Delta), m_SecondaryAxis.ScalarProduct(Delta));
                    }
                }
            }

            real CChromaticBlock::GetBorderCoreRatio()
            {
                return real(m_ChromaticPixelsRedundantBorder.size()) / real(m_ChromaticPixelsRedundantCore.size());
            }

            const CContinuousTristimulusPixel& CChromaticBlock::GetMeanRGBValue() const
            {
                return m_MeanRGB;
            }

            CChromaticBlock::Status CChromaticBlock::GetStatus()
            {
                return m_Status;
            }

            void CChromaticBlock::SetStatus(Status CurrentStatus)
            {
                m_Status = CurrentStatus;
            }

            CChromaticBlock* CChromaticBlock::GetPreselectedSurroundingBlock(const int* pDeltas, const uint TotalDeltas)
            {
                CChromaticBlock* pChromaticBlock = nullptr;
                CChromaticBlock* pSelectedChromaticBlock = nullptr;
                list<CChromaticPixel*>::const_iterator EndChromaticPixels = m_ChromaticPixels.end();
                for (list<CChromaticPixel*>::const_iterator ppChromaticPixel = m_ChromaticPixels.begin(); ppChromaticPixel != EndChromaticPixels; ++ppChromaticPixel)
                {
                    CChromaticPixel* pChromaticPixel = *ppChromaticPixel;
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        pChromaticBlock = (pChromaticPixel + pDeltas[i])->GetWritableChromaticBlock();
                        if (pChromaticBlock)
                        {
                            if (pChromaticBlock->m_Status == ePreSelected)
                            {
                                if (pChromaticBlock != this)
                                {
                                    if (pSelectedChromaticBlock)
                                    {
                                        if ((pChromaticBlock != pSelectedChromaticBlock))
                                        {
                                            return nullptr;
                                        }
                                    }
                                    else
                                    {
                                        pSelectedChromaticBlock = pChromaticBlock;
                                    }
                                }
                            }
                            else if (pChromaticBlock->m_ChromaticPixels.size() > 1)
                            {
                                return nullptr;
                            }
                        }
                    }
                }
                return pChromaticBlock;
            }

            void CChromaticBlock::AbsorveFromChildBlock(CChromaticBlock* pChildChromaticBlock)
            {
                m_ChromaticPixels.insert(m_ChromaticPixels.end(), pChildChromaticBlock->m_ChromaticPixels.begin(), pChildChromaticBlock->m_ChromaticPixels.end());
                pChildChromaticBlock->SetActive(false);
                pChildChromaticBlock->m_pParent = this;
            }

            void CChromaticBlock::AsimilateToParentBlock()
            {
                if (m_pParent && (!m_IsActive))
                {
                    list<CChromaticPixel*>::iterator EndChromaticPixels = m_ChromaticPixels.end();
                    for (list<CChromaticPixel*>::iterator ppChromaticPixel = m_ChromaticPixels.begin(); ppChromaticPixel != EndChromaticPixels; ++ppChromaticPixel)
                    {
                        (*ppChromaticPixel)->SetChromaticBlock(m_pParent);
                    }
                    m_ChromaticPixels.clear();
                }
            }

            CChromaticBlock* CChromaticBlock::GetParent()
            {
                return m_pParent;
            }

            void CChromaticBlock::SetParent(CChromaticBlock* pChromaticBlock)
            {
                m_pParent = pChromaticBlock;
            }

            bool CChromaticBlock::SortContouresBySize(CChromaticContour* plhs, CChromaticContour* prhs)
            {
                return plhs->GetTotalChromaticPixels() > prhs->GetTotalChromaticPixels();
            }

            void CChromaticBlock::AddChromaticPixel(CChromaticPixel* pChromaticPixel)
            {
                pChromaticPixel->SetChromaticBlock(this);
                m_ChromaticPixelsBoundingBox.Extend(pChromaticPixel->GetLocation());
                m_MeanRGB += pChromaticPixel->GetChromaticValue();
                m_ChromaticPixels.push_back(pChromaticPixel);
            }

            void CChromaticBlock::ComputeMeanRGB()
            {
                if (m_ChromaticPixels.size() > 1)
                {
                    m_MeanRGB.IsotropicInverseScale(real(m_ChromaticPixels.size()));
                }
            }

            void CChromaticBlock::ExpandRGBManhatan(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetManhattanRGBDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandRGBSquareEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetSquareEuclideanRGBDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandRGBEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetEuclideanRGBDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandRGBMinkowski(const real DistanceThreshold, const real P, const int* pDeltas, const uint TotalDeltas)
            {
                const real IP = _REAL_ONE_ / P;
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetMinkowskiRGBDistance(pBaseChromaticPixel, P, IP) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandRGBChebyshev(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetChebyshevRGBDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

#ifdef _USE_COLOR_SPACE_XYZ_

            void CChromaticBlock::ExpandXYZManhatan(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetManhattanXYZDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandXYZSquareEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetSquareEuclideanXYZDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandXYZEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetEuclideanXYZDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            AddChromaticPixel(pExpansionChromaticPixel);
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandXYZMinkowski(const real DistanceThreshold, const real P, const int* pDeltas, const uint TotalDeltas)
            {
                const real IP = _REAL_ONE_ / P;
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetMinkowskiXYZDistance(pBaseChromaticPixel, P, IP) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandXYZChebyshev(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetChebyshevXYZDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

#endif

#ifdef _USE_COLOR_SPACE_LAB_

            void CChromaticBlock::ExpandLabManhatan(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetManhattanLabDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandLabSquareEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetSquareEuclideanLabDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandLabEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetEuclideanLabDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandLabMinkowski(const real DistanceThreshold, const real P, const int* pDeltas, const uint TotalDeltas)
            {
                const real IP = _REAL_ONE_ / P;
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetMinkowskiLabDistance(pBaseChromaticPixel, P, IP) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandLabChebyshev(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetChebyshevLabDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            AddChromaticPixel(pExpansionChromaticPixel);
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

#endif

#ifdef _USE_COLOR_SPACE_HSL_

            void CChromaticBlock::ExpandHSLManhatan(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetManhattanHSLDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            AddChromaticPixel(pExpansionChromaticPixel);
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandHSLSquareEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetSquareEuclideanHSLDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandHSLEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetEuclideanHSLDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandHSLMinkowski(const real DistanceThreshold, const real P, const int* pDeltas, const uint TotalDeltas)
            {
                const real IP = _REAL_ONE_ / P;
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetMinkowskiHSLDistance(pBaseChromaticPixel, P, IP) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandHSLChebyshev(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetChebyshevHSLDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

#endif

#ifdef _USE_COLOR_SPACE_HSI_

            void CChromaticBlock::ExpandHSIManhatan(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetManhattanHSIDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandHSISquareEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetSquareEuclideanHSIDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandHSIEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetEuclideanHSIDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandHSIMinkowski(const real DistanceThreshold, const real P, const int* pDeltas, const uint TotalDeltas)
            {
                const real IP = _REAL_ONE_ / P;
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetMinkowskiHSIDistance(pBaseChromaticPixel, P, IP) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandHSIChebyshev(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetChebyshevHSIDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }
#endif

#ifdef _USE_COLOR_SPACE_HSV_

            void CChromaticBlock::ExpandHSVManhatan(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetManhattanHSVDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandHSVSquareEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetSquareEuclideanHSVDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandHSVEuclidean(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetEuclideanHSVDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandHSVMinkowski(const real DistanceThreshold, const real P, const int* pDeltas, const uint TotalDeltas)
            {
                const real IP = _REAL_ONE_ / P;
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetMinkowskiHSVDistance(pBaseChromaticPixel, P, IP) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }

            void CChromaticBlock::ExpandHSVChebyshev(const real DistanceThreshold, const int* pDeltas, const uint TotalDeltas)
            {
                list<CChromaticPixel*> ExpansionChromaticPixelList = m_ChromaticPixels;
                while (ExpansionChromaticPixelList.size())
                {
                    CChromaticPixel* pBaseChromaticPixel = ExpansionChromaticPixelList.front();
                    ExpansionChromaticPixelList.pop_front();
                    for (uint i = 0; i < TotalDeltas; ++i)
                    {
                        CChromaticPixel* pExpansionChromaticPixel = pBaseChromaticPixel + pDeltas[i];
                        if (pExpansionChromaticPixel->IsLinkable() && (pExpansionChromaticPixel->GetChebyshevHSVDistance(pBaseChromaticPixel) <= DistanceThreshold))
                        {
                            ExpansionChromaticPixelList.push_back(pExpansionChromaticPixel);
                            AddChromaticPixel(pExpansionChromaticPixel);
                        }
                    }
                }
                ComputeMeanRGB();
            }
#endif

        }
    }
}

