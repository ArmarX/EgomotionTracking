/*
 * ChromaticPixel.h
 *
 *  Created on: 30.03.2011
 *      Author: gonzalez
 */

#pragma once

#include "../../GlobalSettings.h"

#ifdef _USE_COLOR_SPACE_RGB_

#include "../../Foundation/DataTypes/PixelLocation.h"
#include "../../Foundation/DataTypes/DiscreteTristimulusPixel.h"
#include "../../Foundation/DataTypes/ContinuousTristimulusPixel.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Chromatic
        {
            class CChromaticBlock;

            class CChromaticPixel
            {
            public:

                enum DistanceMetric
                {
                    eUnknownDistanceMetric = 0, eManhatan, eSquareEuclidean, eEuclidean, eMinkowski, eChebyshev
                };

                enum Status
                {
                    eUnknownStatus = 0, eUnLinkable, eLinkable, eLinked, eRedundantBorder, eRedundantCore, eNonRedundantBorder, eNonRedundantCore, eContour
                };

                CChromaticPixel();
                ~CChromaticPixel();

                void Reset();
                void ClearEdgeDistance();

                bool SetEdgeDistance(const real EdgeDistance);
                real GetEdgeDistance() const;

                void SetChromaticBlock(CChromaticBlock* pChromaticBlock);
                CChromaticBlock* GetWritableChromaticBlock();
                const CChromaticBlock* GetReadOnlyChromaticBlock() const;

                void SetConnectivityStatus(const Status CurrentConnectivityStatus);
                Status GetStatus() const;
                bool IsLinkable() const;
                bool IsNonRedundatContourLinkable() const;
                bool IsNonRedundatContourLinkable(const CChromaticBlock* pChromaticBlock) const;

                void SetEnabled(const bool Enabled);
                bool IsEnabled() const;
                bool Initialize(const bool Enabled, const CPixelLocation& DiscreteLocation, const CContinuousTristimulusPixel* pRGB);

                void SetLocation(const CPixelLocation& DiscreteLocation);
                const CPixelLocation& GetLocation() const;

                void SetAtImageActiveZoneBorder(const bool IsAtBorder);
                bool IsAtImageActiveZoneBorder() const;

                void SetChromaticSource(const CContinuousTristimulusPixel* pRGB);
                const CContinuousTristimulusPixel& GetChromaticValue() const;

                real GetManhattanRGBDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetSquareEuclideanRGBDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetEuclideanRGBDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetChebyshevRGBDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetMinkowskiRGBDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const;

#ifdef _USE_COLOR_SPACE_HSL_

                void ConvertRGBToHSL();
                real GetManhattanHSLDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetSquareEuclideanHSLDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetEuclideanHSLDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetChebyshevHSLDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetMinkowskiHSLDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const;
#endif

#ifdef _USE_COLOR_SPACE_XYZ_

                void ConvertRGBToXYZ();
                real GetManhattanXYZDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetChebyshevXYZDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetSquareEuclideanXYZDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetEuclideanXYZDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetMinkowskiXYZDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const;
#endif

#ifdef _USE_COLOR_SPACE_LAB_

                void ConvertRGBToLabD50();
                void ConvertRGBToLabD55();
                void ConvertRGBToLabD65();
                void ConvertRGBToLabD75();
                real GetChebyshevLabDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetManhattanLabDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetSquareEuclideanLabDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetEuclideanLabDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetMinkowskiLabDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const;
#endif

#ifdef _USE_COLOR_SPACE_HSI_

                void ConvertRGBToHSI();
                real GetManhattanHSIDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetChebyshevHSIDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetSquareEuclideanHSIDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetEuclideanHSIDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetMinkowskiHSIDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const;
#endif

#ifdef _USE_COLOR_SPACE_HSV_

                void ConvertRGBToHSV();
                real GetManhattanHSVDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetChebyshevHSVDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetSquareEuclideanHSVDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetEuclideanHSVDistance(const CChromaticPixel* pChromaticPixel) const;
                real GetMinkowskiHSVDistance(const CChromaticPixel* pChromaticPixel, const real P, const real IP) const;
#endif

                bool IsAtBorder(const int* pDeltas, const uint TotalDeltas) const;
                bool IsDynamicallyNonRedundatBorder(const int* pDeltas, const uint TotalDeltas) const;
                bool IsDynamicallyRedundantCore(const int* pDeltas, const uint TotalDeltas) const;

            protected:

                bool m_IsEnabled;
                bool m_IsAtImageActiveZoneBorder;
                Status m_Status;
                real m_EdgeDistance;
                CPixelLocation m_Location;
                CChromaticBlock* m_pChromaticBlock;
                const CContinuousTristimulusPixel* m_pRGB;
                CContinuousTristimulusPixel m_Tristimulus;
            };
        }
    }
}

#endif /* _USE_COLOR_SPACE_RGB_ */

