#pragma once

/* EgomotionTracking includes. */

#include <EgomotionTracking/components/EgomotionTracking/TrackingControl.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/ParametricLineRenderer.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ImageProcessor/IVTEdgeProcessor.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/MotionModel/SimpleRobotMotionModel.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/LocationStatePredictor/UncertaintyMotionModelLocationStatePredictor.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/LocationUncertaintyModel/RelativeGaussianRobotControlStateUncertaintyModel.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/LocationUncertaintyModel/AbsoluteGaussianLocationStateUncertaintyModel.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/NeckErrorUncertaintyModel/NeckErrorUncertaintyModel.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/UncertaintyScatteringFactorCalculator/UncertaintyModelScatteringFactorCalculator.h>
#include <EgomotionTracking/components/EgomotionTracking/Models/ObservationModel/JunctionPointLineModelObservationModel.h>
#include <EgomotionTracking/components/EgomotionTracking/LocationEstimator/ParticleSwarmLocationEstimator/ParticleSwarmLocationEstimator.h>
#include <EgomotionTracking/components/EgomotionTracking/HeadConfigurationPlanner/MassSpringHeadConfigurationPlanner.h>
#include <EgomotionTracking/components/EgomotionTracking/HeadConfigurationPlanner/ProjectedLinesInViewRelevanceProcessingUnit.h>
#include <EgomotionTracking/components/EgomotionTracking/HeadConfigurationPlanner/SimpleScreenLineRelevanceWeightingFunctionImplementation.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/CameraCalibration/StereoCalibrationGL.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/Threading/DispatchingThread.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationRegion.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Profiling.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/SessionPlayer/SessionPlayer.h>

/* EVP includes. */

#include <Image/ByteImage.h>

#include "EgomotionTrackerSessionPlayer.h"

namespace armarx
{
    class EgomotionTrackerSessionPlayer;

    class SessionPlayerDispatchingThread : public CDispatchingThread
    {
    public:
        SessionPlayerDispatchingThread(CSessionPlayer* pSessionPlayer, EgomotionTrackerSessionPlayer* pEgomotionTrackerSessionPlayer);
        ~SessionPlayerDispatchingThread() override;

        CTrackingControl* GetTrackingControl();
        const CTrackingControl* GetConstTrackingControl();
        const CByteImage* GetOMVisualizationImage() const;
        const CRenderingModel& GetRenderingModel() const;

        void ReinitializeSwarmParticles();

        void SetHeadTarget(const CHeadConfiguration& targetConfiguration);

    protected:

        void DispatchingFunction() override;

        void InitializeRenderer();
        void InitializeCudaRenderer();
        void InitializeRobotCalibration();
        void InitializeObservationModels();
        void InitializeTracking();
        void InitializeParticleSwarmLocationEstimator();
        void InitializeHeadConfigurationPlanner();
        void InitializeDynamicElementStatusEstimation();

        void ConfigureRenderer();
        void ConfigureEdgeProcessor();
        void ConfigureObservationModels();
        void ConfigureTracking();
        void ConfigureParticleSwarmLocationEstimator();
        void ConfigureHeadConfigurationPlaner();
        void ConfigureDynamicElementStatusEstimation();

        void ConfigurePredictionModels();

        bool m_headPlannerEnabled;

        CTrackingControl* m_pTrackingControl;

        CSimpleLocationRegion m_initialisationRegion;

        /* Prediction Models */
        CSimpleRobotMotionModel m_trackingMotionModel;
        CAbsoluteGaussianLocationStateUncertaintyModel m_trackingLocationStateUncertaintyModel;
        CRelativeGaussianRobotControlStateUncertaintyModel m_trackingRobotControlStateUncertaintyModel;
        CUncertaintyMotionModelLocationStatePredictor m_trackingLocationStatePredictor;
        CNeckErrorUncertaintyModel m_neckErrorUncertaintyModel;
        DynamicElementStatusUncertaintyModel m_dynamicElementStatusUncertaintyModel;

        /* Observation Model */
        CJunctionPointLineModelObservationModel m_trackingObservationModel;
        CJunctionPointLineModelObservationModel m_initialisationObservationModel;
        CJunctionPointLineModelObservationModel m_dynamicElementStatusEstimationObservationModel;

        /* Scattering reduction */
        CUncertaintyModelScatteringFactorCalculator m_scatteringFactorCalculatorLocationState;
        CUncertaintyModelScatteringFactorCalculator m_scatteringFactorCalculatorNeckError;

        /* Image Processor */
        CIVTEdgeProcessor m_trackingIVTEdgeProcessor;

        /* Parametric Line Renderer Stuff */
        CParametricLineRenderer m_parametricLineRenderer;
        CRenderingModel m_renderingModel;

        /* Head Configuration Planner */
        CMassSpringHeadConfigurationPlanner m_massSpringHeadConfigurationPlanner;
        CProjectedLinesInViewRelevanceProcessingUnit m_projectedLinesInViewRelevanceProcessingUnit;
        CSimpleScreenLineRelevanceWeightingFunctionImplementation m_screenLineRelevanceWeightingFunctionObject;

        CSessionPlayer* m_pSessionPlayer;

        CSimpleLocationRegion m_trackingLocationRegion;

        CByteImage* m_pVisualizationImage;

        EgomotionTrackerSessionPlayer* m_pEgomotionTrackerSessionPlayer;

    };
}

