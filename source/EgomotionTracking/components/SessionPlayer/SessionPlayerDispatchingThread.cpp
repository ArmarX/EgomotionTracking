#include "SessionPlayerDispatchingThread.h"

#include <EgomotionTracking/components/EgomotionTrackingConfiguration/TrackingConfiguration.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/XMLPrimitives.h>

using namespace armarx;

SessionPlayerDispatchingThread::SessionPlayerDispatchingThread(CSessionPlayer* pSessionPlayer, EgomotionTrackerSessionPlayer* pEgomotionTrackerSessionPlayer) :
    m_pSessionPlayer(pSessionPlayer)
{
    m_pVisualizationImage = new CByteImage(640, 480, CByteImage::eRGB24, false);

    m_pEgomotionTrackerSessionPlayer = pEgomotionTrackerSessionPlayer;

    m_pTrackingControl = new CTrackingControl(1000, 300, 200);

    InitializeRenderer();
    InitializeRobotCalibration();
    InitializeCudaRenderer();
    InitializeObservationModels();
    InitializeTracking();
    InitializeParticleSwarmLocationEstimator();
    InitializeDynamicElementStatusEstimation();
    InitializeHeadConfigurationPlanner();
    ConfigureEdgeProcessor();
}

SessionPlayerDispatchingThread::~SessionPlayerDispatchingThread()
{
    if (m_pVisualizationImage)
    {
        delete m_pVisualizationImage;
    }

    if (m_pTrackingControl)
    {
        delete m_pTrackingControl;
    }
}

void SessionPlayerDispatchingThread::DispatchingFunction()
{
    m_pTrackingControl->DispatchLocalisation();
}

CTrackingControl* SessionPlayerDispatchingThread::GetTrackingControl()
{
    return m_pTrackingControl;
}

const CTrackingControl* SessionPlayerDispatchingThread::GetConstTrackingControl()
{
    return m_pTrackingControl;
}

const CByteImage* SessionPlayerDispatchingThread::GetOMVisualizationImage() const
{
    return m_pVisualizationImage;
}

const CRenderingModel& SessionPlayerDispatchingThread::GetRenderingModel() const
{
    return m_renderingModel;
}

void SessionPlayerDispatchingThread::ReinitializeSwarmParticles()
{
    CParticleSwarmLocationEstimator* pPSLocationEstimator = m_pTrackingControl->GetParticleSwarmLocationEstimator();
    assert(pPSLocationEstimator);
    pPSLocationEstimator->InitializeParticlesInRegion(m_initialisationRegion);
}

void SessionPlayerDispatchingThread::SetHeadTarget(const CHeadConfiguration& targetConfiguration)
{
    m_massSpringHeadConfigurationPlanner.SetTargetHeadConfiguration(targetConfiguration);
}

void SessionPlayerDispatchingThread::InitializeRenderer()
{
    m_parametricLineRenderer.SetVisibleScreenRegion(0, 640, 0, 480);
    m_parametricLineRenderer.SetRenderingModel(&m_renderingModel);
    m_parametricLineRenderer.SetImageSize(640, 480);

    //    m_renderingModel.LoadModelLinesFromFile("/home/SMBAD/heyde/home/TrackingFiles/EgomotionTrackingRessources/smallKitchen/lineModel.xml");
    //    m_renderingModel.LoadModelFaceTrianglesFromFile("/home/SMBAD/heyde/home/TrackingFiles/EgomotionTrackingRessources/smallKitchen/triangleModel.xml");

    if (!m_renderingModel.loadRVLModelFromXMLFile("/home/SMBAD/heyde/home/IV_Models/Mobile_Kitchen/Small_Kitchen_Advanced.xml"))
    {
        std::cout << "RVL failed to load XML file" << std::endl;
        return;
    }

    ConfigureRenderer();

    QDomDocument doc;

    if (!XMLPrimitives::ReadXMLDocumentFromFile(doc, "/home/SMBAD/heyde/home/TrackingFiles/EgomotionTrackingRessources/smallKitchen/RootModelTransformation.xml"))
    {
        return;
    }

    QDomElement rootElement = doc.documentElement();
    SbMatrix transformation;

    if (XMLPrimitives::DeserializeQDomElementToSbMatrix(rootElement, transformation))
    {
        m_renderingModel.SetGlobalModelElementsTransformation(transformation);
    }
}

void SessionPlayerDispatchingThread::InitializeCudaRenderer()
{

}

void SessionPlayerDispatchingThread::InitializeRobotCalibration()
{
    if (m_pSessionPlayer != NULL)
    {
        m_parametricLineRenderer.SetProjectionMatrixGl(m_pSessionPlayer->GetCameraProjectionMatrixGL());
    }
}

void SessionPlayerDispatchingThread::InitializeObservationModels()
{
    m_trackingObservationModel.SetVisualizationImage(m_pVisualizationImage);
    m_initialisationObservationModel.SetVisualizationImage(m_pVisualizationImage);
    m_dynamicElementStatusEstimationObservationModel.SetVisualizationImage(m_pVisualizationImage);
    ConfigureObservationModels();
}

void SessionPlayerDispatchingThread::InitializeTracking()
{
    m_trackingLocationRegion = CSimpleLocationRegion(500, 4000, -500, 3500, 0.0, M_PI);
    m_pTrackingControl->SetLocationRegion(&m_trackingLocationRegion);
    m_pTrackingControl->SetParametricLineRenderer(m_parametricLineRenderer);

    m_pTrackingControl->SetRobotControl(m_pSessionPlayer);
    m_pTrackingControl->SetMasterLocationEstimator(NULL);
    m_pTrackingControl->SetGroundTruthLocationEstimator(m_pSessionPlayer);
    m_pTrackingControl->SetTrackingImageProcessor(&m_trackingIVTEdgeProcessor);

    m_pTrackingControl->SetTrackingMode(CTrackingControl::eEgomotionTracking);
    m_pTrackingControl->SetParametricLineRenderer(m_parametricLineRenderer);
    m_pTrackingControl->SetTrackingObservationModel(&m_trackingObservationModel);
    m_pTrackingControl->SetParticleSwarmLocationEstimatorObservationModel(&m_initialisationObservationModel);

    m_trackingLocationStatePredictor.SetLocationStateUncertaintyModel(&m_trackingLocationStateUncertaintyModel);
    m_trackingLocationStatePredictor.SetRobotControlStateUncertaintyModel(&m_trackingRobotControlStateUncertaintyModel);
    m_trackingLocationStatePredictor.SetRobotMotionModel(&m_trackingMotionModel);
    m_pTrackingControl->SetLocationStatePredictor(&m_trackingLocationStatePredictor);
    m_pTrackingControl->SetNeckErrorUncertaintyModel(&m_neckErrorUncertaintyModel);

    m_pTrackingControl->SetUncertaintyFactorCalculatorLocationState(&m_scatteringFactorCalculatorLocationState);
    m_pTrackingControl->SetUncertaintyFactorCalculatorNeckError(&m_scatteringFactorCalculatorNeckError);

    ConfigurePredictionModels();
    ConfigureTracking();
}

void SessionPlayerDispatchingThread::InitializeParticleSwarmLocationEstimator()
{
    ConfigureParticleSwarmLocationEstimator();
    ReinitializeSwarmParticles();
}

void SessionPlayerDispatchingThread::InitializeHeadConfigurationPlanner()
{
    m_massSpringHeadConfigurationPlanner.SetControllableRobot(m_pSessionPlayer);

    m_massSpringHeadConfigurationPlanner.SetRelevanceFactorProcessingUnit(&m_projectedLinesInViewRelevanceProcessingUnit);

    m_massSpringHeadConfigurationPlanner.SetLocationEstimator(m_pTrackingControl);
    m_projectedLinesInViewRelevanceProcessingUnit.SetParametricLineRenderer(&m_parametricLineRenderer);
    m_projectedLinesInViewRelevanceProcessingUnit.SetScreenLineRelevanceWeightingFunctionObject(&m_screenLineRelevanceWeightingFunctionObject);

    ConfigureHeadConfigurationPlaner();
}

void SessionPlayerDispatchingThread::ConfigureRenderer()
{
    m_parametricLineRenderer.SetMinScreenLineLength(m_pEgomotionTrackerSessionPlayer->minScreenLineLength);
    m_parametricLineRenderer.SetMinSegmentsPerJunction(m_pEgomotionTrackerSessionPlayer->minSegmentsPerJunction);
    m_parametricLineRenderer.SetMaxJunctionPointToSegmentDeviation(m_pEgomotionTrackerSessionPlayer->maxJunctionPointToSegmentDeviation);
    m_parametricLineRenderer.SetJunctionSegmentLength(m_pEgomotionTrackerSessionPlayer->junctionSegmentLength);
    m_parametricLineRenderer.SetMaxJunctionLinesDotProduct(m_pEgomotionTrackerSessionPlayer->maxJunctionLinesDotProduct);
}

void SessionPlayerDispatchingThread::ConfigureEdgeProcessor()
{
    m_trackingIVTEdgeProcessor.SetThresholdCannyLow(m_pEgomotionTrackerSessionPlayer->tresholdCannyLow);
    m_trackingIVTEdgeProcessor.SetThresholdCannyHigh(m_pEgomotionTrackerSessionPlayer->tresholdCannyHigh);
}

void SessionPlayerDispatchingThread::ConfigureObservationModels()
{
    m_trackingObservationModel.SetSamplingMode(m_pEgomotionTrackerSessionPlayer->samplingModeTracking);
    m_trackingObservationModel.SetGaussianWeightingMode(m_pEgomotionTrackerSessionPlayer->gaussianWeightingModeTracking);
    m_trackingObservationModel.SetNumberOfNormalsMode(m_pEgomotionTrackerSessionPlayer->numberOfNormalsModeTracking);
    m_trackingObservationModel.SetNumberOfNormals(m_pEgomotionTrackerSessionPlayer->numberOfNormalsTracking);
    m_trackingObservationModel.SetMinNumberOfVisibleModelElements(m_pEgomotionTrackerSessionPlayer->minNumberOfVisibleModelElementsTracking);
    m_trackingObservationModel.SetDeltaAlpha(Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->deltaAlphaTracking));
    m_trackingObservationModel.SetDesiredPMin(m_pEgomotionTrackerSessionPlayer->desiredPMinTracking);
    m_trackingObservationModel.SetUseNormalOrientationDifferenceMeasure(m_pEgomotionTrackerSessionPlayer->useNormalOrientationDifferenceMeasureTracking);
    m_trackingObservationModel.setNormalOrientationDifferenceScalar(m_pEgomotionTrackerSessionPlayer->normalOrientationDifferenceScalarTracking);
    m_trackingObservationModel.setNormalOrientationDifferenceFactor(m_pEgomotionTrackerSessionPlayer->normalOrientationDifferenceFactorTracking);
    m_initialisationObservationModel.SetSamplingMode(m_pEgomotionTrackerSessionPlayer->samplingModeInitialisation);
    m_initialisationObservationModel.SetGaussianWeightingMode(m_pEgomotionTrackerSessionPlayer->gaussianWeightingModeInitialisation);
    m_initialisationObservationModel.SetNumberOfNormalsMode(m_pEgomotionTrackerSessionPlayer->numberOfNormalsModeInitialisation);
    m_initialisationObservationModel.SetNumberOfNormals(m_pEgomotionTrackerSessionPlayer->numberOfNormalsInitialisation);
    m_initialisationObservationModel.SetMinNumberOfVisibleModelElements(m_pEgomotionTrackerSessionPlayer->minNumberOfVisibleModelElementsInitialisation);
    m_initialisationObservationModel.SetDeltaAlpha(Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->deltaAlphaInitialisation));
    m_initialisationObservationModel.SetDesiredPMin(m_pEgomotionTrackerSessionPlayer->desiredPMinInitialisation);
    m_initialisationObservationModel.SetUseNormalOrientationDifferenceMeasure(m_pEgomotionTrackerSessionPlayer->useNormalOrientationDifferenceMeasureInitialisation);
    m_initialisationObservationModel.setNormalOrientationDifferenceScalar(m_pEgomotionTrackerSessionPlayer->normalOrientationDifferenceScalarInitialisation);
    m_initialisationObservationModel.setNormalOrientationDifferenceFactor(m_pEgomotionTrackerSessionPlayer->normalOrientationDifferenceFactorInitialisation);
    m_dynamicElementStatusEstimationObservationModel.SetSamplingMode(m_pEgomotionTrackerSessionPlayer->samplingModeTracking);
    m_dynamicElementStatusEstimationObservationModel.SetGaussianWeightingMode(m_pEgomotionTrackerSessionPlayer->gaussianWeightingModeTracking);
    m_dynamicElementStatusEstimationObservationModel.SetNumberOfNormalsMode(m_pEgomotionTrackerSessionPlayer->numberOfNormalsModeTracking);
    m_dynamicElementStatusEstimationObservationModel.SetNumberOfNormals(m_pEgomotionTrackerSessionPlayer->numberOfNormalsTracking);
    m_dynamicElementStatusEstimationObservationModel.SetMinNumberOfVisibleModelElements(m_pEgomotionTrackerSessionPlayer->minNumberOfVisibleModelElementsTracking);
    m_dynamicElementStatusEstimationObservationModel.SetDeltaAlpha(Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->deltaAlphaTracking));
    m_dynamicElementStatusEstimationObservationModel.SetDesiredPMin(m_pEgomotionTrackerSessionPlayer->desiredPMinTracking);
    m_dynamicElementStatusEstimationObservationModel.SetUseNormalOrientationDifferenceMeasure(m_pEgomotionTrackerSessionPlayer->useNormalOrientationDifferenceMeasureTracking);
    m_dynamicElementStatusEstimationObservationModel.setNormalOrientationDifferenceScalar(m_pEgomotionTrackerSessionPlayer->normalOrientationDifferenceScalarTracking);
    m_dynamicElementStatusEstimationObservationModel.setNormalOrientationDifferenceFactor(m_pEgomotionTrackerSessionPlayer->normalOrientationDifferenceFactorTracking);
}

void SessionPlayerDispatchingThread::ConfigureTracking()
{
    m_scatteringFactorCalculatorLocationState.SetAgingCoefficient(m_pEgomotionTrackerSessionPlayer->agingCoefficientLocationState);
    m_scatteringFactorCalculatorLocationState.SetScatteringRange(m_pEgomotionTrackerSessionPlayer->minScatteringFactorLocationState, m_pEgomotionTrackerSessionPlayer->maxScatteringFactorLocationState);
    m_scatteringFactorCalculatorNeckError.SetAgingCoefficient(m_pEgomotionTrackerSessionPlayer->agingCoefficientNeckError);
    m_scatteringFactorCalculatorNeckError.SetScatteringRange(m_pEgomotionTrackerSessionPlayer->minScatteringFactorNeckError, m_pEgomotionTrackerSessionPlayer->maxScatteringFactorNeckError);
    m_pTrackingControl->GetTrackingPF()->SetSwarmOptimization(m_pEgomotionTrackerSessionPlayer->swarmOptimizationEnabled, m_pEgomotionTrackerSessionPlayer->velocityFactorLocalBest,
            m_pEgomotionTrackerSessionPlayer->velocityFactorGlobalBest, m_pEgomotionTrackerSessionPlayer->localBestAgingFactor,
            m_pEgomotionTrackerSessionPlayer->bestWeightTresholdSwarmOptimization);
    m_pTrackingControl->GetTrackingPF()->SetAnnealing(m_pEgomotionTrackerSessionPlayer->annealingEnabled, m_pEgomotionTrackerSessionPlayer->numberOfAnnealingLevels,
            m_pEgomotionTrackerSessionPlayer->annealingFactor, m_pEgomotionTrackerSessionPlayer->bestWeightTresholdAnnealing,
            m_pEgomotionTrackerSessionPlayer->conservativeUpdate);
    m_pTrackingControl->GetTrackingPF()->SetResamplingThreshold(m_pEgomotionTrackerSessionPlayer->resamplingTresholdEnabled, m_pEgomotionTrackerSessionPlayer->relativeNEffTreshold);
    m_neckErrorUncertaintyModel.SetUncertaintyMode(m_pEgomotionTrackerSessionPlayer->neckErrorUncertaintyMode);
    m_neckErrorUncertaintyModel.SetErrorLimits(Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->trackingNeckErrorRollMin),
            Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->trackingNeckErrorRollMax),
            Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->trackingNeckErrorPitchMin),
            Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->trackingNeckErrorPitchMax),
            Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->trackingNeckErrorYawMin),
            Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->trackingNeckErrorYawMax));
    m_neckErrorUncertaintyModel.SetSigmaRoll(Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->sigmaRoll));
    m_neckErrorUncertaintyModel.SetSigmaPitch(Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->sigmaPitch));
    m_neckErrorUncertaintyModel.SetSigmaYaw(Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->sigmaYaw));
    m_pTrackingControl->SetScatteringReduction(m_pEgomotionTrackerSessionPlayer->scatteringReductionEnabled);
    m_pTrackingControl->SetLineRegistrationVisualizationMode(m_pEgomotionTrackerSessionPlayer->lineRegistrationVisualizationMode);
    m_pTrackingControl->SetNumberOfActiveParticles(configuration_defaultActiveParticles);
    m_pTrackingControl->GetTrackingPF()->setUseMahalanobisDistanceBasedScatteringReduction(m_pEgomotionTrackerSessionPlayer->useMahalanobisDistanceBasedScatteringReduction);
    m_pTrackingControl->GetTrackingPF()->setCounterTreshold(m_pEgomotionTrackerSessionPlayer->counterTreshold);
    m_pTrackingControl->GetTrackingPF()->setMahalanobisDistanceBasedScatteringReductionResamplingMode(m_pEgomotionTrackerSessionPlayer->mahalanobisDistanceBasedScatterinReductionResamplingMode);
    m_pTrackingControl->GetTrackingPF()->setTotalWeightPercentageTreshold(m_pEgomotionTrackerSessionPlayer->totalWeightPercentageTreshold);
}

void SessionPlayerDispatchingThread::ConfigureParticleSwarmLocationEstimator()
{
    CParticleSwarmLocationEstimator* pPSLocationEstimator = m_pTrackingControl->GetParticleSwarmLocationEstimator();
    assert(pPSLocationEstimator);
    pPSLocationEstimator->SetParameters(m_pEgomotionTrackerSessionPlayer->inertiaFactor, m_pEgomotionTrackerSessionPlayer->cognitionFactor, m_pEgomotionTrackerSessionPlayer->socialFactor);
    m_initialisationRegion.SetMinX(500);
    m_initialisationRegion.SetMinY(-500);
    m_initialisationRegion.SetMinAlpha(Conversions::DegreeToRadians(75));
    m_initialisationRegion.SetMaxX(3000);
    m_initialisationRegion.SetMaxY(3500);
    m_initialisationRegion.SetMaxAlpha(Conversions::DegreeToRadians(105));
}

void SessionPlayerDispatchingThread::ConfigureHeadConfigurationPlaner()
{
    m_massSpringHeadConfigurationPlanner.SetPlanningLimits(Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->headPlannerRollMin),
            Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->headPlannerRollMax),
            Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->headPlannerPitchMin),
            Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->headPlannerPitchMax),
            Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->headPlannerYawMin),
            Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->headPlannerYawMax));
    m_massSpringHeadConfigurationPlanner.InitHeadConfigurationSamplesUniformDistributed(m_pEgomotionTrackerSessionPlayer->rollSamplesCount, m_pEgomotionTrackerSessionPlayer->pitchSamplesCount,
            m_pEgomotionTrackerSessionPlayer->yawSamplesCount);
    SetHeadTarget(CHeadConfiguration(SbVec3f(0.0, 0.0, 0.0)));
    m_massSpringHeadConfigurationPlanner.SetMovingHeadAllowed(m_pEgomotionTrackerSessionPlayer->movingHeadAllowed);
    m_massSpringHeadConfigurationPlanner.SetConfigurationMass(m_pEgomotionTrackerSessionPlayer->configurationMass);
    m_massSpringHeadConfigurationPlanner.SetDampingCoefficient(m_pEgomotionTrackerSessionPlayer->dampingCoefficient);
    m_massSpringHeadConfigurationPlanner.SetTargetFadingCoefficient(m_pEgomotionTrackerSessionPlayer->targetFadingCoefficient);
    m_massSpringHeadConfigurationPlanner.SetSpringCoefficientMode(m_pEgomotionTrackerSessionPlayer->springCoefficientMode);
    m_massSpringHeadConfigurationPlanner.SetMoveToMaxRelevanceFactorThreshold(m_pEgomotionTrackerSessionPlayer->moveToMaxRelevanceFactorTreshold);
    m_massSpringHeadConfigurationPlanner.SetUseMoveToMaxHeuristic(m_pEgomotionTrackerSessionPlayer->useMoveToMaxHeuristic);
}

void SessionPlayerDispatchingThread::ConfigurePredictionModels()
{
    m_trackingRobotControlStateUncertaintyModel.SetRelativeSystematicErrorVelocityX(m_pEgomotionTrackerSessionPlayer->relativeSystematicErrorVelocityX);
    m_trackingRobotControlStateUncertaintyModel.SetRelativeSystematicErrorVelocityY(m_pEgomotionTrackerSessionPlayer->relativeSystematicErrorVelocityY);
    m_trackingRobotControlStateUncertaintyModel.SetRelativeSystematicErrorVelocityAlpha(m_pEgomotionTrackerSessionPlayer->relativeSystematicErrorVelocityAlpha);
    m_trackingRobotControlStateUncertaintyModel.SetAbsoluteSystematicPlatformAxesTiltError(m_pEgomotionTrackerSessionPlayer->absoluteSystematicPlatformAxesTiltError);
    m_trackingRobotControlStateUncertaintyModel.SetSigmaVelocityX(Conversions::PercentageToProbability(m_pEgomotionTrackerSessionPlayer->sigmaVelocityX));
    m_trackingRobotControlStateUncertaintyModel.SetSigmaVelocityY(Conversions::PercentageToProbability(m_pEgomotionTrackerSessionPlayer->sigmaVelocityY));
    m_trackingRobotControlStateUncertaintyModel.SetSigmaVelocityAlpha(Conversions::PercentageToProbability(m_pEgomotionTrackerSessionPlayer->sigmaVelocityAlpha));
    m_trackingRobotControlStateUncertaintyModel.SetSigmaPlatformAxesTiltError(Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->sigmaPlatformAxesTiltError));
    m_trackingLocationStateUncertaintyModel.SetSigmaX(m_pEgomotionTrackerSessionPlayer->sigmaX);
    m_trackingLocationStateUncertaintyModel.SetSigmaY(m_pEgomotionTrackerSessionPlayer->sigmaY);
    m_trackingLocationStateUncertaintyModel.SetSigmaAlpha(Conversions::DegreeToRadians(m_pEgomotionTrackerSessionPlayer->sigmaAlpha));
    m_trackingLocationStatePredictor.SetEnabledRobotControlStateUncertainty(m_pEgomotionTrackerSessionPlayer->robotControlStateUncertaintyEnabled);
    m_trackingLocationStatePredictor.SetEnabledLocationStateUncertainty(m_pEgomotionTrackerSessionPlayer->locationStateUncertaintyEnabled);
}

void SessionPlayerDispatchingThread::InitializeDynamicElementStatusEstimation()
{
    m_pTrackingControl->SetDynamicElementStatusEstimationObservationModel(&m_dynamicElementStatusEstimationObservationModel);
    m_pTrackingControl->SetDynamicElementStatusUncertaintyModel(&m_dynamicElementStatusUncertaintyModel);
    m_pTrackingControl->InitializeDynamicElements();

    ConfigureDynamicElementStatusEstimation();
}

void SessionPlayerDispatchingThread::ConfigureDynamicElementStatusEstimation()
{
    m_dynamicElementStatusUncertaintyModel.SetUncertaintyMode(DynamicElementStatusUncertaintyModel::relativeGaussianUncertainty);
    m_dynamicElementStatusUncertaintyModel.SetSigmaAlpha(0.001);
    m_pTrackingControl->GetDESEControl()->getSingleDynamicElementStatusFilter()->setSwarmOptimization(false, 0.2, 0.05, 0.9, 0.0);
    m_pTrackingControl->GetDESEControl()->getSingleDynamicElementStatusFilter()->setAnnealing(false, 2, 0.5, 0.2, false);
    m_pTrackingControl->GetDESEControl()->getSingleDynamicElementStatusFilter()->setResamplingThreshold(false, 0.9);
}


