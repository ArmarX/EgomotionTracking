/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    EgomotionTracker::components::EgomotionTrackerSessionPlayer
 * @author     Thomas von der Heyde ( tvh242 at hotmail dot com )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "EgomotionTrackerSessionPlayer.h"

#include <EgomotionTracking/components/EgomotionTrackingConfiguration/TrackingConfiguration.h>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>

using namespace armarx;
using namespace visionx;

EgomotionTrackerSessionPlayer::EgomotionTrackerSessionPlayer() : m_pOutputImage(NULL), m_numberOfRepetitions(5), m_status(eSessionPlayerIdle), m_autoExecution(false), m_doStep(false)
{

}

EgomotionTrackerSessionPlayer::~EgomotionTrackerSessionPlayer()
{

}

void EgomotionTrackerSessionPlayer::onInitImageProcessor()
{
    loadConfiguration();
}

void EgomotionTrackerSessionPlayer::onConnectImageProcessor()
{
    m_pOutputImage = new CByteImage(640, 480, CByteImage::eRGB24, false);

    enableResultImages(1, ImageDimension(640, 480), visionx::eRgb);

    m_pSessionPlayer = new CSessionPlayer();
    m_pSessionPlayer->SetSessionDirectory("/home/SMBAD/heyde/home/TrackingFiles/EgomotionTrackingRessources/output/session");
    //    m_pSessionPlayer->SetSessionDirectory("/home/SMBAD/heyde/home/Session1");
    m_pSessionPlayer->SetRobotKinematic(&m_armar3RobotKinematic);
    std::cout << "EgomotionTrackerSessionPlayer loaded " << m_pSessionPlayer->GetNumberOfLoadedRecords() << " records" << std::endl;

    m_pSessionPlayerDispatchingThread = new SessionPlayerDispatchingThread(m_pSessionPlayer, this);

    m_trackingEvaluator.SetTrackingControl(m_pSessionPlayerDispatchingThread->GetConstTrackingControl());

    reset();
    m_status = ePlayingSession;
    m_autoExecution = true;
}

void EgomotionTrackerSessionPlayer::onDisconnectImageProcessor()
{

}

void EgomotionTrackerSessionPlayer::onExitImageProcessor()
{
    if (m_pSessionPlayer != NULL)
    {
        delete m_pSessionPlayer;
    }

    if (m_pSessionPlayerDispatchingThread != NULL)
    {
        delete m_pSessionPlayerDispatchingThread;
    }

    delete m_pOutputImage;
}

void EgomotionTrackerSessionPlayer::process()
{
    if (m_status == eSessionPlayerIdle)
    {

    }
    else
    {
        if (m_autoExecution == true || (m_autoExecution == false && m_doStep == true))
        {
            ::ImageProcessor::CopyImage(m_pSessionPlayerDispatchingThread->GetOMVisualizationImage(), m_pOutputImage);

            if (!playNextFrame())
            {
                m_status = eSessionPlayerIdle;
                std::cout << "Finished!" << std::endl;
            }

            if (m_autoExecution == false)
            {
                std::cout << "Frame: " << m_currentFrame << std::endl;
                m_doStep = false;
            }
        }

        provideResultImages(&m_pOutputImage);
    }
}

bool EgomotionTrackerSessionPlayer::playNextFrame()
{
    if (m_currentFrame == m_pSessionPlayer->GetNumberOfLoadedRecords() /*600*/)
    {
        m_currentFrame = 0;
        ++m_currentRepetition;

        std::cout << "Repetition: " << m_currentRepetition - 1 << std::endl;
        std::cout << "Mean X deviation: " << m_trackingEvaluator.GetDeviationAccumulatorX().GetMean() << std::endl;
        std::cout << "Mean Y deviation: " << m_trackingEvaluator.GetDeviationAccumulatorY().GetMean() << std::endl;
        std::cout << "Mean D deviation: " << m_trackingEvaluator.GetDeviationAccumulatorD().GetMean() << std::endl;
        std::cout << "Mean Alpha deviation: " << m_trackingEvaluator.GetDeviationAccumulatorAlpha().GetMean() << std::endl;

        m_totalDeviationAccumulatorX.AddDeviation(m_trackingEvaluator.GetDeviationAccumulatorX().GetMean());
        m_totalDeviationAccumulatorY.AddDeviation(m_trackingEvaluator.GetDeviationAccumulatorY().GetMean());
        m_totalDeviationAccumulatorD.AddDeviation(m_trackingEvaluator.GetDeviationAccumulatorD().GetMean());
        m_totalDeviationAccumulatorAlpha.AddDeviation(m_trackingEvaluator.GetDeviationAccumulatorAlpha().GetMean());

        if (m_currentRepetition - 1 == m_numberOfRepetitions)
        {
            std::cout << "Mean results: " << std::endl;
            std::cout << "Mean X deviation: " << m_totalDeviationAccumulatorX.GetMean() << std::endl;
            std::cout << "Mean Y deviation: " << m_totalDeviationAccumulatorY.GetMean() << std::endl;
            std::cout << "Mean D deviation: " << m_totalDeviationAccumulatorD.GetMean() << std::endl;
            std::cout << "Mean Alpha deviation: " << m_totalDeviationAccumulatorAlpha.GetMean() << std::endl;

            return false;
        }
        else
        {
            m_pSessionPlayer->Reset();
            m_trackingEvaluator.ResetEvaluation();
            float neckErrorRoll = 0, neckErrorPitch = 0, neckErrorYaw = 0;
            m_pSessionPlayer->GetInitialNeckErrors(neckErrorRoll, neckErrorPitch, neckErrorYaw);
            m_pSessionPlayer->DispatchLocalisation();
            m_pSessionPlayerDispatchingThread->GetTrackingControl()->GetTrackingPF()->InitializeFilter(m_pSessionPlayer->GetLocationEstimate(), neckErrorRoll, neckErrorPitch,
                    neckErrorYaw);
        }
    }

    m_pSessionPlayerDispatchingThread->Dispatch();
    m_pSessionPlayerDispatchingThread->WaitUntilIdle();
    m_trackingEvaluator.DispatchEvaluation();

    ++m_currentFrame;

    return true;
}

void EgomotionTrackerSessionPlayer::reset()
{
    m_totalDeviationAccumulatorX.Reset();
    m_totalDeviationAccumulatorY.Reset();
    m_totalDeviationAccumulatorD.Reset();
    m_totalDeviationAccumulatorAlpha.Reset();

    m_pSessionPlayer->Reset();
    m_trackingEvaluator.ResetEvaluation();
    float neckErrorRoll = 0, neckErrorPitch = 0, neckErrorYaw = 0;
    m_pSessionPlayer->GetInitialNeckErrors(neckErrorRoll, neckErrorPitch, neckErrorYaw);
    m_pSessionPlayer->DispatchLocalisation();
    m_pSessionPlayerDispatchingThread->GetTrackingControl()->GetTrackingPF()->InitializeFilter(m_pSessionPlayer->GetLocationEstimate(), neckErrorRoll, neckErrorPitch, neckErrorYaw);

    m_currentRepetition = 0;
    m_currentFrame = 0;
}

void EgomotionTrackerSessionPlayer::loadConfiguration()
{
    m_numberOfRepetitions = getProperty<int>("numberOfRepetitions").getValue();

    headPlannerEnabled = getProperty<bool>("headPlannerEnabled").getValue();

    minScreenLineLength = getProperty<float>("minScreenLineLength").getValue();
    minSegmentsPerJunction = getProperty<float>("minSegmentsPerJunction").getValue();
    maxJunctionPointToSegmentDeviation = getProperty<float>("maxJunctionPointToSegmentDeviation").getValue();
    junctionSegmentLength  = getProperty<float>("junctionSegmentLength").getValue();
    maxJunctionLinesDotProduct = getProperty<float>("maxJunctionLinesDotProduct").getValue();

    tresholdCannyLow = getProperty<int>("tresholdCannyLow").getValue();
    tresholdCannyHigh = getProperty<int>("tresholdCannyHigh").getValue();

    samplingModeTracking = getProperty<CJunctionPointLineModelObservationModel::CSamplingMode>("samplingModeTracking").getValue();
    gaussianWeightingModeTracking = getProperty<CJunctionPointLineModelObservationModel::CGaussianWeightingMode>("gaussianWeightingModeTracking").getValue();
    numberOfNormalsModeTracking = getProperty<CJunctionPointLineModelObservationModel::CNumberOfNormalsMode>("numberOfNormalsModeTracking").getValue();
    numberOfNormalsTracking = getProperty<int>("numberOfNormalsTracking").getValue();
    minNumberOfVisibleModelElementsTracking = getProperty<int>("minNumberOfVisibleModelElementsTracking").getValue();
    deltaAlphaTracking = getProperty<float>("deltaAlphaTracking").getValue();
    desiredPMinTracking = getProperty<float>("desiredPMinTracking").getValue();
    useNormalOrientationDifferenceMeasureTracking = getProperty<bool>("useNormalOrientationDifferenceMeasureTracking").getValue();
    normalOrientationDifferenceScalarTracking = getProperty<float>("normalOrientationDifferenceScalarTracking").getValue();
    normalOrientationDifferenceFactorTracking = getProperty<float>("normalOrientationDifferenceFactorTracking").getValue();
    samplingModeInitialisation = getProperty<CJunctionPointLineModelObservationModel::CSamplingMode>("samplingModeInitialisation").getValue();
    gaussianWeightingModeInitialisation = getProperty<CJunctionPointLineModelObservationModel::CGaussianWeightingMode>("gaussianWeightingModeInitialisation").getValue();
    numberOfNormalsModeInitialisation = getProperty<CJunctionPointLineModelObservationModel::CNumberOfNormalsMode>("numberOfNormalsModeInitialisation").getValue();
    numberOfNormalsInitialisation = getProperty<int>("numberOfNormalsInitialisation").getValue();
    minNumberOfVisibleModelElementsInitialisation = getProperty<int>("minNumberOfVisibleModelElementsInitialisation").getValue();
    deltaAlphaInitialisation = getProperty<float>("deltaAlphaInitialisation").getValue();
    desiredPMinInitialisation = getProperty<float>("desiredPMinInitialisation").getValue();
    useNormalOrientationDifferenceMeasureInitialisation = getProperty<bool>("useNormalOrientationDifferenceMeasureInitialisation").getValue();
    normalOrientationDifferenceScalarInitialisation = getProperty<float>("normalOrientationDifferenceScalarInitialisation").getValue();
    normalOrientationDifferenceFactorInitialisation = getProperty<float>("normalOrientationDifferenceFactorInitialisation").getValue();

    agingCoefficientLocationState = getProperty<int>("agingCoefficientLocationState").getValue();
    minScatteringFactorLocationState = getProperty<float>("minScatteringFactorLocationState").getValue();
    maxScatteringFactorLocationState = getProperty<float>("maxScatteringFactorLocationState").getValue();
    agingCoefficientNeckError = getProperty<int>("agingCoefficientNeckError").getValue();
    minScatteringFactorNeckError = getProperty<float>("minScatteringFactorNeckError").getValue();
    maxScatteringFactorNeckError = getProperty<float>("maxScatteringFactorNeckError").getValue();
    swarmOptimizationEnabled = getProperty<bool>("swarmOptimizationEnabled").getValue();
    velocityFactorLocalBest = getProperty<float>("velocityFactorLocalBest").getValue();
    velocityFactorGlobalBest = getProperty<float>("velocityFactorGlobalBest").getValue();
    localBestAgingFactor = getProperty<float>("localBestAgingFactor").getValue();
    bestWeightTresholdSwarmOptimization = getProperty<float>("bestWeightTresholdSwarmOptimization").getValue();
    annealingEnabled = getProperty<bool>("annealingEnabled").getValue();
    numberOfAnnealingLevels = getProperty<int>("numberOfAnnealingLevels").getValue();
    annealingFactor = getProperty<float>("annealingFactor").getValue();
    bestWeightTresholdAnnealing = getProperty<float>("bestWeightTresholdAnnealing").getValue();
    conservativeUpdate = getProperty<bool>("conservativeUpdate").getValue();
    resamplingTresholdEnabled = getProperty<bool>("resamplingTresholdEnabled").getValue();
    relativeNEffTreshold = getProperty<float>("relativeNEffTreshold").getValue();
    neckErrorUncertaintyMode = getProperty<CNeckErrorUncertaintyModel::UncertaintyMode>("neckErrorUncertaintyMode").getValue();
    sigmaRoll = getProperty<float>("sigmaRoll").getValue();
    sigmaPitch = getProperty<float>("sigmaPitch").getValue();
    sigmaYaw = getProperty<float>("sigmaYaw").getValue();
    scatteringReductionEnabled = getProperty<bool>("scatteringReductionEnabled").getValue();
    lineRegistrationVisualizationMode = getProperty<CTrackingControl::LineRegistrationVisualizationMode>("lineRegistrationVisualizationMode").getValue();
    trackingNeckErrorRollMin = getProperty<float>("trackingNeckErrorRollMin").getValue();
    trackingNeckErrorRollMax = getProperty<float>("trackingNeckErrorRollMax").getValue();
    trackingNeckErrorPitchMin = getProperty<float>("trackingNeckErrorPitchMin").getValue();
    trackingNeckErrorPitchMax = getProperty<float>("trackingNeckErrorPitchMax").getValue();
    trackingNeckErrorYawMin = getProperty<float>("trackingNeckErrorYawMin").getValue();
    trackingNeckErrorYawMax = getProperty<float>("trackingNeckErrorYawMax").getValue();
    useMahalanobisDistanceBasedScatteringReduction = getProperty<bool>("useMahalanobisDistanceBasedScatteringReduction").getValue();
    counterTreshold = getProperty<int>("counterTreshold").getValue();
    mahalanobisDistanceBasedScatterinReductionResamplingMode = getProperty<CEgomotionTrackingPFExecutionUnit::MahalanobisDistanceBasedScatteringReductionResamplingMode>("mahalanobisDistanceBasedScatteringReductionResamplingMode").getValue();
    totalWeightPercentageTreshold = getProperty<float>("totalWeightPercentageTreshold").getValue();

    inertiaFactor = getProperty<float>("inertiaFactor").getValue();
    cognitionFactor = getProperty<float>("cognitionFactor").getValue();
    socialFactor = getProperty<float>("socialFactor").getValue();

    headPlannerRollMin = getProperty<float>("headPlannerRollMin").getValue();
    headPlannerRollMax = getProperty<float>("headPlannerRollMax").getValue();
    headPlannerPitchMin = getProperty<float>("headPlannerPitchMin").getValue();
    headPlannerPitchMax = getProperty<float>("headPlannerPitchMax").getValue();
    headPlannerYawMin = getProperty<float>("headPlannerYawMin").getValue();
    headPlannerYawMax = getProperty<float>("headPlannerYawMax").getValue();
    rollSamplesCount = getProperty<int>("rollSamplesCount").getValue();
    pitchSamplesCount = getProperty<int>("pitchSamplesCount").getValue();
    yawSamplesCount = getProperty<int>("yawSamplesCount").getValue();
    movingHeadAllowed = getProperty<bool>("movingHeadAllowed").getValue();
    configurationMass = getProperty<float>("configurationMass").getValue();
    dampingCoefficient = getProperty<float>("dampingCoefficient").getValue();
    targetFadingCoefficient = getProperty<float>("targetFadingCoefficient").getValue();
    springCoefficientMode = getProperty<CMassSpringHeadConfigurationPlanner::SpringCoefficientMode>("springCoefficientMode").getValue();
    moveToMaxRelevanceFactorTreshold = getProperty<float>("moveToMaxRelevanceFactorTreshold").getValue();
    useMoveToMaxHeuristic = getProperty<bool>("useMoveToMaxHeuristic").getValue();

    relativeSystematicErrorVelocityX = getProperty<float>("relativeSystematicErrorVelocityX").getValue();
    relativeSystematicErrorVelocityY = getProperty<float>("relativeSystematicErrorVelocityY").getValue();
    relativeSystematicErrorVelocityAlpha = getProperty<float>("relativeSystematicErrorVelocityAlpha").getValue();
    absoluteSystematicPlatformAxesTiltError = getProperty<float>("absoluteSystematicPlatformAxesTiltError").getValue();
    sigmaVelocityX = getProperty<float>("sigmaVelocityX").getValue();
    sigmaVelocityY = getProperty<float>("sigmaVelocityY").getValue();
    sigmaVelocityAlpha = getProperty<float>("sigmaVelocityAlpha").getValue();
    sigmaPlatformAxesTiltError = getProperty<float>("sigmaPlatformAxesTiltError").getValue();
    sigmaX = getProperty<float>("sigmaX").getValue();
    sigmaY = getProperty<float>("sigmaY").getValue();
    sigmaAlpha = getProperty<float>("sigmaAlpha").getValue();
    robotControlStateUncertaintyEnabled = getProperty<bool>("robotControlStateUncertaintyEnabled").getValue();
    locationStateUncertaintyEnabled = getProperty<bool>("locationStateUncertaintyEnabled").getValue();
}

void EgomotionTrackerSessionPlayer::start(const Ice::Current& c)
{
    reset();
    m_status = ePlayingSession;
}

void EgomotionTrackerSessionPlayer::stop(const Ice::Current& c)
{
    m_status = eSessionPlayerIdle;
}

void EgomotionTrackerSessionPlayer::setAutoExecution(bool autoExecution, const Ice::Current& c)
{
    m_autoExecution = autoExecution;
}

bool EgomotionTrackerSessionPlayer::getAutoExecution(const Ice::Current& c)
{
    return m_autoExecution;
}

bool EgomotionTrackerSessionPlayer::doNextStep(const Ice::Current& c)
{
    if (m_status == eSessionPlayerIdle || m_autoExecution == true)
    {
        return false;
    }

    m_doStep = true;
    return true;
}

void EgomotionTrackerSessionPlayer::setNumberOfRepetitions(Ice::Int numberOfRepetitions, const Ice::Current&)
{
    m_numberOfRepetitions = numberOfRepetitions;
}

int EgomotionTrackerSessionPlayer::getNumberOfRepetitions(const Ice::Current& c)
{
    return m_numberOfRepetitions;
}

LocationEstimate EgomotionTrackerSessionPlayer::getLocationEstimate(const ::Ice::Current& c)
{
    LocationEstimate locationEstimate;
    CLocationState locationState = m_pSessionPlayerDispatchingThread->GetTrackingControl()->GetLocationEstimate();
    locationEstimate.x = locationState.GetX();
    locationEstimate.y = locationState.GetY();
    locationEstimate.alpha = locationState.GetAlpha();
    return locationEstimate;
}

LocationEstimate EgomotionTrackerSessionPlayer::getGroundTruthLocation(const ::Ice::Current& c)
{
    LocationEstimate groundTruth;
    CLocationState locationState = m_pSessionPlayerDispatchingThread->GetTrackingControl()->GetGroundTruthLocationEstimator()->GetLocationEstimate();
    groundTruth.x = locationState.GetX();
    groundTruth.y = locationState.GetY();
    groundTruth.alpha = locationState.GetAlpha();
    return groundTruth;
}

PlaneScatteringDirections EgomotionTrackerSessionPlayer::getPlaneScatteringDirections(const ::Ice::Current& c)
{
    PlaneScatteringDirections planeScatteringDirections;
    MeanScatteringDirections meanScatteringDirections;
    Direction direction;
    CPlaneScatteringDirections planeScatteringDirectionsPF = m_pSessionPlayerDispatchingThread->GetTrackingControl()->GetTrackingPF()->GetPlaneScatteringDirections();
    meanScatteringDirections.x = planeScatteringDirectionsPF.GetMean().x;
    meanScatteringDirections.y = planeScatteringDirectionsPF.GetMean().y;
    direction.x = planeScatteringDirectionsPF.GetDirection1().x;
    direction.y = planeScatteringDirectionsPF.GetDirection1().y;
    planeScatteringDirections.meanScattDir = meanScatteringDirections;
    planeScatteringDirections.direction1 = direction;
    planeScatteringDirections.sigma1 = planeScatteringDirectionsPF.GetSigma1();
    planeScatteringDirections.sigma2 = planeScatteringDirectionsPF.GetSigma2();
    return planeScatteringDirections;
}

ParticleVector EgomotionTrackerSessionPlayer::getParticles(const ::Ice::Current& c)
{
    std::vector<Particle> particleVector;
    std::vector<CLocationParticle> particlePFVector;
    particlePFVector.resize(configuration_maxParticles, CLocationParticle(CLocationState(1000, 1000, 0)));
    m_pSessionPlayerDispatchingThread->GetTrackingControl()->GetParticles(particlePFVector);
    int numberOfActiveParticles = m_pSessionPlayerDispatchingThread->GetTrackingControl()->GetNumberOfActiveParticles();

    if (particlePFVector.size() > 0)
    {
        for (int i = 0; i < numberOfActiveParticles; i++)
        {
            Particle particle;
            LocationEstimate locationEstimate;
            locationEstimate.x = particlePFVector[i].GetX();
            locationEstimate.y = particlePFVector[i].GetY();
            locationEstimate.alpha = particlePFVector[i].GetAlpha();
            particle.locEst = locationEstimate;
            particle.neckRollError = particlePFVector[i].GetNeckRollError();
            particle.neckPitchError = particlePFVector[i].GetNeckPitchError();
            particle.neckYawError = particlePFVector[i].GetNeckYawError();
            particle.importanceWeight = particlePFVector[i].GetImportanceWeight();
            particleVector.push_back(particle);
        }
    }

    return particleVector;
}

BoundingBox EgomotionTrackerSessionPlayer::getBoundingBox(const ::Ice::Current& c)
{
    BoundingBox boundingBox;
    SbVec3f min, max;
    m_pSessionPlayerDispatchingThread->GetRenderingModel().CalculateAlignedModelBoundingBox(min, max);
    std::vector<float> minb;
    minb.push_back(min[0]);
    minb.push_back(min[1]);
    minb.push_back(min[2]);
    std::vector<float> maxb;
    maxb.push_back(max[0]);
    maxb.push_back(max[1]);
    maxb.push_back(max[2]);
    boundingBox.min = minb;
    boundingBox.max = maxb;
    return boundingBox;
}

int EgomotionTrackerSessionPlayer::getNumberOfActiveParticles(const ::Ice::Current& c)
{
    int numberOfActiveParticles = m_pSessionPlayerDispatchingThread->GetTrackingControl()->GetNumberOfActiveParticles();
    return numberOfActiveParticles;
}

EgomotionTrackerSessionPlayerStatus EgomotionTrackerSessionPlayer::getStatus(const ::Ice::Current& c)
{
    return m_status;
}
