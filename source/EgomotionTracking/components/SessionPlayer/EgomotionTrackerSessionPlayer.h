/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    EgomotionTracker::components::EgomotionTrackerSessionPlayer
 * @author     Thomas von der Heyde ( tvh242 at hotmail dot com )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

/* VisionX includes. */

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/interface/core/ImageProviderInterface.h>

/* STL includes. */

#include <string>

/* IVT includes. */

#include <Image/ByteImage.h>

/* EgomotionTracking includes. */

#include <EgomotionTracking/components/EgomotionTracking/Misc/RobotKinematic/SimpleArmar3RobotKinematic.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/SessionPlayer/SessionPlayer.h>
#include <EgomotionTracking/components/EgomotionTracking/Misc/Evaluation/TrackingEvaluator.h>

#include <VisionX/interface/components/EgomotionTrackerSessionPlayerInterface.h>

#include "SessionPlayerDispatchingThread.h"

namespace armarx
{
    class SessionPlayerDispatchingThread;

    class EgomotionTrackerSessionPlayerProcessorPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        EgomotionTrackerSessionPlayerProcessorPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<int>("numberOfRepetitions", 5, "Number of times a session is executed");

            /* Configuration */
            defineOptionalProperty<bool>("headPlannerEnabled", false, "Enable use of the head planner.")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);

            /* Renderer configuration */
            defineOptionalProperty<float>("minScreenLineLength", 30.0, "Minimum line length. If a lines length is inferior to this value, it will not be rendered.");
            defineOptionalProperty<float>("minSegmentsPerJunction", 2.0, "Minimum number of segments per junction. If a junctions number of segments is inferior to this value, it will not be rendered.");
            defineOptionalProperty<float>("maxJunctionPointToSegmentDeviation", 3.0, "Maximum segment-junction point deviation. If the distance between a segment and a junction point is greater than this value, the segment will not be considering as belonging to the junction point.");
            defineOptionalProperty<float>("junctionSegmentLength", 25.0, "Minim junction segment length. If a junction segments length is inferior to this value, it will not be rendered.");
            defineOptionalProperty<float>("maxJunctionLinesDotProduct", 0.95, "Maximum dot product between two segments of a same junction point for which they are still considered separate segments");

            /* Edge processor configuration */
            defineOptionalProperty<int>("tresholdCannyLow", 40, "Sets the lower Canny treshold");
            defineOptionalProperty<int>("tresholdCannyHigh", 60, "Sets the higher Canny treshold");

            /* Observation models configuration */
            defineOptionalProperty<CJunctionPointLineModelObservationModel::CSamplingMode>("samplingModeTracking", CJunctionPointLineModelObservationModel::eSampleAllLines, "Sets the sampling mode for the tracking observation model")
            .map("eSampleAllLines", CJunctionPointLineModelObservationModel::eSampleAllLines)
            .map("eSampleJunctionPoints", CJunctionPointLineModelObservationModel::eSampleJunctionPoints);
            defineOptionalProperty<CJunctionPointLineModelObservationModel::CGaussianWeightingMode>("gaussianWeightingModeTracking", CJunctionPointLineModelObservationModel::eWeightAll, "Sets the gaussian weighting mode for the tracking observation model")
            .map("eWeightAll", CJunctionPointLineModelObservationModel::eWeightAll)
            .map("eSmoothedHistogramWeighting", CJunctionPointLineModelObservationModel::eSmoothedHistogramWeighting);
            defineOptionalProperty<CJunctionPointLineModelObservationModel::CNumberOfNormalsMode>("numberOfNormalsModeTracking", CJunctionPointLineModelObservationModel::eLengthDependendNumberOfNormals, "Sets the number of normals mode for the tracking observaton model")
            .map("eLengthDependendNumberOfNormals", CJunctionPointLineModelObservationModel::eLengthDependendNumberOfNormals)
            .map("eFixedNumberOfNormals", CJunctionPointLineModelObservationModel::eFixedNumberOfNormals);
            defineOptionalProperty<int>("numberOfNormalsTracking", 5, "If number of normals mode = fixed, sets the number of normals - else, sets the number of normals per 100px");
            defineOptionalProperty<int>("minNumberOfVisibleModelElementsTracking", 5, "Sets the minimum required number of visible elements (junctions/lines)");
            defineOptionalProperty<float>("deltaAlphaTracking", 2.0, "Tracking delta alpha value used to determine the value of sigma used to calculate a particles weight.");
            defineOptionalProperty<float>("desiredPMinTracking", 0.001, "Tracking PMin value used to determine the value of sigma used to calculate a particles weight.");
            defineOptionalProperty<bool>("useNormalOrientationDifferenceMeasureTracking", false, "Sets whether to use the normal orientation difference as a measure in the observation likelihood function during the tracking phase");
            defineOptionalProperty<float>("normalOrientationDifferenceScalarTracking", 1.0, "Scalar multiplied with the normal orientation difference to controls its importance.");
            defineOptionalProperty<float>("normalOrientationDifferenceFactorTracking", 1.0, "Factor of the normal orientation difference measure used to control its importance.");
            defineOptionalProperty<CJunctionPointLineModelObservationModel::CSamplingMode>("samplingModeInitialisation", CJunctionPointLineModelObservationModel::eSampleAllLines, "Sets the sampling mode for the initialisation observation model")
            .map("eSampleAllLines", CJunctionPointLineModelObservationModel::eSampleAllLines)
            .map("eSampleJunctionPoints", CJunctionPointLineModelObservationModel::eSampleJunctionPoints);
            defineOptionalProperty<CJunctionPointLineModelObservationModel::CGaussianWeightingMode>("gaussianWeightingModeInitialisation", CJunctionPointLineModelObservationModel::eWeightAll, "Sets the gaussian weighting mode for the initialisation observation model")
            .map("eWeightAll", CJunctionPointLineModelObservationModel::eWeightAll)
            .map("eSmoothedHistogramWeighting", CJunctionPointLineModelObservationModel::eSmoothedHistogramWeighting);
            defineOptionalProperty<CJunctionPointLineModelObservationModel::CNumberOfNormalsMode>("numberOfNormalsModeInitialisation", CJunctionPointLineModelObservationModel::eLengthDependendNumberOfNormals, "Sets the number of normals mode for the initialisation observaton model")
            .map("eLengthDependendNumberOfNormals", CJunctionPointLineModelObservationModel::eLengthDependendNumberOfNormals)
            .map("eFixedNumberOfNormals", CJunctionPointLineModelObservationModel::eFixedNumberOfNormals);
            defineOptionalProperty<int>("numberOfNormalsInitialisation", 5, "If number of normals mode = fixed, sets the number of normals - else, sets the number of normals per 100px");
            defineOptionalProperty<int>("minNumberOfVisibleModelElementsInitialisation", 20, "Sets the minimum required number of visible elements (junctions/lines)");
            defineOptionalProperty<float>("deltaAlphaInitialisation", 4.0, "Initialisation PMin value used to determine the value of sigma used to calculate a particles weight.");
            defineOptionalProperty<float>("desiredPMinInitialisation", 0.001, "Initialisation PMin used to determine the value of sigma used to calculate a particles weight.");
            defineOptionalProperty<bool>("useNormalOrientationDifferenceMeasureInitialisation", false, "Sets whether to use the normal orientation difference as a measure in the observation likelihood function during the tracking phase");
            defineOptionalProperty<float>("normalOrientationDifferenceScalarInitialisation", 1.0, "Scalar multiplied with the normal orientation difference to controls its importance.");
            defineOptionalProperty<float>("normalOrientationDifferenceFactorInitialisation", 1.0, "Factor of the normal orientation difference measure used to control its importance.");

            /* Tracking configuration */
            defineOptionalProperty<int>("agingCoefficientLocationState", 30, "Sets the aging coefficient for the location state");
            defineOptionalProperty<float>("minScatteringFactorLocationState", 0.2, "Sets the minimum scattering factor for the location state");
            defineOptionalProperty<float>("maxScatteringFactorLocationState", 1.0, "Sets the maximum scattering factor for the location state");
            defineOptionalProperty<int>("agingCoefficientNeckError", 15, "Sets the aging coefficient for the neck error");
            defineOptionalProperty<float>("minScatteringFactorNeckError", 0.2, "Sets the minimum scattering factor for the neck error");
            defineOptionalProperty<float>("maxScatteringFactorNeckError", 1.0, "Sets the maximum scattering factor for the neck error");
            defineOptionalProperty<bool>("swarmOptimizationEnabled", false, "Enables swarm optimization for the tracking phase")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<float>("velocityFactorLocalBest", 0.2, "Sets the velocity factor for the local best");
            defineOptionalProperty<float>("velocityFactorGlobalBest", 0.05, "Sets the velocity factor for the global best");
            defineOptionalProperty<float>("localBestAgingFactor", 0.9, "Set the aging factor for the local best");
            defineOptionalProperty<float>("bestWeightTresholdSwarmOptimization", 0.0, "If swarm optimization is enabled, a swarm optimization cycle will only be computed if one of the particles has a weight superior to this value.");
            defineOptionalProperty<bool>("annealingEnabled", true, "Enables annealing for the tracking phase")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<int>("numberOfAnnealingLevels", 2, "Sets the number of annealing levels");
            defineOptionalProperty<float>("annealingFactor", 0.5, "Sets the annealing factor");
            defineOptionalProperty<float>("bestWeightTresholdAnnealing", 0.2, "If annealing is enabled, sets the annealing level to 0 if the weight of all particles is inferior to this value, which corresponds to disabling annealing.");
            defineOptionalProperty<bool>("conservativeUpdate", false, "If conservative update is set to TRUE and annealing is enabled, the location estimate will only be update after going through all annealing levels. Else, the location estimate will be update after each annealing cycle regardless of wich annealing level is being computed.")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<bool>("resamplingTresholdEnabled", true, "If resampling treshold is enabled, the particles will only be resampled if the relative effective particle number falls under a certain treshold.")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<float>("relativeNEffTreshold", 0.9, "NEff treshold. If resampling treshold is enabled, the particles will only be resampled if the relative effective particle number falls below this value.");
            defineOptionalProperty<CNeckErrorUncertaintyModel::UncertaintyMode>("neckErrorUncertaintyMode", CNeckErrorUncertaintyModel::eRelativeGaussianNeckUncertainty, "Sets the neck error uncertainty mode")
            .map("eNoNeckErrorUncertainty", CNeckErrorUncertaintyModel::eNoNeckErrorUncertainty)
            .map("eRelativeGaussianNeckUncertainty", CNeckErrorUncertaintyModel::eRelativeGaussianNeckUncertainty)
            .map("eAbsoluteUniformNeckUncertainty", CNeckErrorUncertaintyModel::eAbsoluteUniformNeckUncertainty);
            defineOptionalProperty<float>("sigmaRoll", 1.0, "Sets sigma for the neck roll error uncertainty");
            defineOptionalProperty<float>("sigmaPitch", 1.0, "Sets sigma for the neck pitch error uncertainty");
            defineOptionalProperty<float>("sigmaYaw", 1.0, "Sets sigma for the neck yaw error uncertainty");
            defineOptionalProperty<bool>("scatteringReductionEnabled", true, "Enables scattering reduction when the robot is immobile")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<CTrackingControl::LineRegistrationVisualizationMode>("lineRegistrationVisualizationMode", CTrackingControl::eDrawLinesInInputImage, "Sets the line registration visualization mode")
            .map("eDrawLinesInEdgeImage", CTrackingControl::eDrawLinesInEdgeImage)
            .map("eDrawLinesInInputImage", CTrackingControl::eDrawLinesInInputImage);
            defineOptionalProperty<float>("trackingNeckErrorRollMin", -5.0, "Minimum roll neck error for the tracking phase neck error uncertainty model");
            defineOptionalProperty<float>("trackingNeckErrorRollMax", 5.0, "Maximum roll neck error for the tracking phase neck error uncertainty model");
            defineOptionalProperty<float>("trackingNeckErrorPitchMin", -10.0, "Minimum pitch neck error for the tracking phase neck error uncertainty model");
            defineOptionalProperty<float>("trackingNeckErrorPitchMax", 2.0, "Maximum pitch neck error for the tracking phase neck error uncertainty model");
            defineOptionalProperty<float>("trackingNeckErrorYawMin", -1.0, "Minimum yaw neck error for the tracking phase neck error uncertainty model");
            defineOptionalProperty<float>("trackingNeckErrorYawMax", 1.0, "Maximum yaw neck error for the tracking phase neck error uncertainty model");
            defineOptionalProperty<bool>("useMahalanobisDistanceBasedScatteringReduction", true, "True if particle scattering should be reduced by calculating the particles Mahalanobis distance to the estimated location");
            defineOptionalProperty<int>("counterTreshold", 10, "The number of iterations the determinant of the scattering matrix has to be below the treshold before Mahalanobis distance based scattering reduction activates if it's enabled.");
            defineOptionalProperty<CEgomotionTrackingPFExecutionUnit::MahalanobisDistanceBasedScatteringReductionResamplingMode>("mahalanobisDistanceBasedScatteringReductionResamplingMode", CEgomotionTrackingPFExecutionUnit::eStandardResampling, "Resampling mode used when Mahalanobis distance based scattering reduction is enabled.")
            .map("eStandardResampling", CEgomotionTrackingPFExecutionUnit::eStandardResampling)
            .map("eEigenvectorEigenvalueBasedResampling", CEgomotionTrackingPFExecutionUnit::eEigenvectorEigenvalueBasedResampling)
            .map("eLatticeSurfaceResampling", CEgomotionTrackingPFExecutionUnit::eLatticeSurfaceResampling);
            defineOptionalProperty<float>("totalWeightPercentageTreshold", 0.7, "Percentage of total particle weight used to calculate the resampable particles if Mahalanobis distance based scattering reduction is enabled.");

            /* Location estimator configuration */
            defineOptionalProperty<float>("inertiaFactor", 0.9, "Sets the inertia factor");
            defineOptionalProperty<float>("cognitionFactor", 0.6, "Sets the cognition factor");
            defineOptionalProperty<float>("socialFactor", 0.7, "Sets the social factor");

            /* Head planner configuration */
            defineOptionalProperty<float>("headPlannerRollMin", -19.0, "Sets the minimum neck roll angle for the head planner");
            defineOptionalProperty<float>("headPlannerRollMax", 19.0, "Sets the maximum nech roll angle for the head planner");
            defineOptionalProperty<float>("headPlannerPitchMin", -40.0, "Sets the minimum neck pitch angle for the head planner");
            defineOptionalProperty<float>("headPlannerPitchMax", 31.0, "Sets the maximum neck pitch angle for the head planner");
            defineOptionalProperty<float>("headPlannerYawMin", -90.0, "Sets the minimum neck yaw angle for the head planner");
            defineOptionalProperty<float>("headPlannerYawMax", 90.0, "Sets the maximum neck yaw angle for the head planner");
            defineOptionalProperty<int>("rollSamplesCount", 3, "Sets the number of neck roll samples for the head planner");
            defineOptionalProperty<int>("pitchSamplesCount", 5, "Sets the number of neck pitch samples for the head planner");
            defineOptionalProperty<int>("yawSamplesCount", 7, "Sets the number of neck yaw samples for the head planner");
            defineOptionalProperty<bool>("movingHeadAllowed", true, "Enables moving the head")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<float>("configurationMass", 0.2, "Sets the configuration mass");
            defineOptionalProperty<float>("dampingCoefficient", 0.8, "Sets the damping coefficient");
            defineOptionalProperty<float>("targetFadingCoefficient", 0.05, "Sets the target fading coefficient");
            defineOptionalProperty<CMassSpringHeadConfigurationPlanner::SpringCoefficientMode>("springCoefficientMode", CMassSpringHeadConfigurationPlanner::eTargetFadingMode, "Sets the spring coefficient mode")
            .map("eManualCoefficientSelection", CMassSpringHeadConfigurationPlanner::eManualCoefficientSelection)
            .map("eTargetFadingMode", CMassSpringHeadConfigurationPlanner::eTargetFadingMode);
            defineOptionalProperty<float>("moveToMaxRelevanceFactorTreshold", 0.2, "");
            defineOptionalProperty<bool>("useMoveToMaxHeuristic", true, "")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);

            /* Prediction models configuration */
            defineOptionalProperty<float>("relativeSystematicErrorVelocityX", 0.0, "Relative systematic error factor for the robots X velocity.");
            defineOptionalProperty<float>("relativeSystematicErrorVelocityY", 0.0, "Relative systematic error factor for the robots Y velocity.");
            defineOptionalProperty<float>("relativeSystematicErrorVelocityAlpha", 0.0, "Relative systematic error factor for the robots alpha velocity.");
            defineOptionalProperty<float>("absoluteSystematicPlatformAxesTiltError", 0.0, "Absolute systematic error for the robot platforms axes tilt error.");
            defineOptionalProperty<float>("sigmaVelocityX", 30.0, "Sigma value for the robots X velocity value uncertainty.");
            defineOptionalProperty<float>("sigmaVelocityY", 30.0, "Sigma value for the robots Y velocity value uncertainty.");
            defineOptionalProperty<float>("sigmaVelocityAlpha", 30.0, "Sigma value for the robots alpha velocity value uncertainty.");
            defineOptionalProperty<float>("sigmaPlatformAxesTiltError", 2.0, "Sigma value for the robot platforms tilt error value uncertainty.");
            defineOptionalProperty<float>("sigmaX", 10.0, "Sigma value for the robots X value uncertainty.");
            defineOptionalProperty<float>("sigmaY", 10.0, "Sigma value for the robots Y value uncertainty.");
            defineOptionalProperty<float>("sigmaAlpha", 2.0, "Sigma value for the robots alpha value uncertainty.");
            defineOptionalProperty<bool>("robotControlStateUncertaintyEnabled", true, "Enables adding an uncertainty to the robot control state values.")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
            defineOptionalProperty<bool>("locationStateUncertaintyEnabled", true, "Enables adding an uncertainty to the location state values.")
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);
        }
    };

    class EgomotionTrackerSessionPlayer:
        virtual public visionx::ImageProcessor,
        virtual public EgomotionTrackerSessionPlayerInterface
    {
        friend class SessionPlayerDispatchingThread;

    public:

        EgomotionTrackerSessionPlayer();
        ~EgomotionTrackerSessionPlayer() override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new EgomotionTrackerSessionPlayerProcessorPropertyDefinitions(getConfigIdentifier()));
        }

        /* Inherited from ImageProcessor. */

        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onDisconnectImageProcessor() override;
        void onExitImageProcessor() override;
        void process() override;

        std::string getDefaultName() const override
        {
            return "EgomotionTrackerSessionPlayer";
        }

        void start(const Ice::Current& c = Ice::emptyCurrent) override;
        void stop(const Ice::Current& c = Ice::emptyCurrent) override;
        void setAutoExecution(bool autoExecution, const Ice::Current& c = Ice::emptyCurrent) override;
        bool getAutoExecution(const Ice::Current& c = Ice::emptyCurrent) override;
        bool doNextStep(const Ice::Current& c = Ice::emptyCurrent) override;
        void setNumberOfRepetitions(Ice::Int numberOfRepetitions, const Ice::Current& c = Ice::emptyCurrent) override;
        int getNumberOfRepetitions(const Ice::Current& c = Ice::emptyCurrent) override;
        LocationEstimate getLocationEstimate(const Ice::Current& c = Ice::emptyCurrent) override;
        LocationEstimate getGroundTruthLocation(const Ice::Current& c = Ice::emptyCurrent) override;
        PlaneScatteringDirections getPlaneScatteringDirections(const Ice::Current& c = Ice::emptyCurrent) override;
        ParticleVector getParticles(const Ice::Current& c = Ice::emptyCurrent) override;
        BoundingBox getBoundingBox(const Ice::Current& c = Ice::emptyCurrent) override;
        int getNumberOfActiveParticles(const Ice::Current& c = Ice::emptyCurrent) override;
        EgomotionTrackerSessionPlayerStatus getStatus(const Ice::Current& c = Ice::emptyCurrent) override;

    protected:

        void loadConfiguration();
        bool playNextFrame();
        void reset();

        CByteImage* m_pOutputImage;

        CSimpleArmar3RobotKinematic m_armar3RobotKinematic;
        CSessionPlayer* m_pSessionPlayer;
        CTrackingEvaluator m_trackingEvaluator;

        SessionPlayerDispatchingThread* m_pSessionPlayerDispatchingThread;

        int m_numberOfRepetitions;
        EgomotionTrackerSessionPlayerStatus m_status;
        bool m_autoExecution;
        bool m_doStep;
        int m_currentRepetition;
        int m_currentFrame;
        CDeviationAccumulator m_totalDeviationAccumulatorX;
        CDeviationAccumulator m_totalDeviationAccumulatorY;
        CDeviationAccumulator m_totalDeviationAccumulatorD;
        CDeviationAccumulator m_totalDeviationAccumulatorAlpha;

        /* Configuration */
        bool headPlannerEnabled;

        /* Renderer coniguration */
        float minScreenLineLength;
        float minSegmentsPerJunction;
        float maxJunctionPointToSegmentDeviation;
        float junctionSegmentLength;
        float maxJunctionLinesDotProduct;

        /* Edge processor configuration */
        int tresholdCannyLow;
        int tresholdCannyHigh;

        /* Observation models configuration */
        CJunctionPointLineModelObservationModel::CSamplingMode samplingModeTracking;
        CJunctionPointLineModelObservationModel::CGaussianWeightingMode gaussianWeightingModeTracking;
        CJunctionPointLineModelObservationModel::CNumberOfNormalsMode numberOfNormalsModeTracking;
        int numberOfNormalsTracking;
        int minNumberOfVisibleModelElementsTracking;
        float deltaAlphaTracking;
        float desiredPMinTracking;
        bool useNormalOrientationDifferenceMeasureTracking;
        float normalOrientationDifferenceScalarTracking;
        float normalOrientationDifferenceFactorTracking;
        CJunctionPointLineModelObservationModel::CSamplingMode samplingModeInitialisation;
        CJunctionPointLineModelObservationModel::CGaussianWeightingMode gaussianWeightingModeInitialisation;
        CJunctionPointLineModelObservationModel::CNumberOfNormalsMode numberOfNormalsModeInitialisation;
        int numberOfNormalsInitialisation;
        int minNumberOfVisibleModelElementsInitialisation;
        float deltaAlphaInitialisation;
        float desiredPMinInitialisation;
        bool useNormalOrientationDifferenceMeasureInitialisation;
        float normalOrientationDifferenceScalarInitialisation;
        float normalOrientationDifferenceFactorInitialisation;

        /* Tracking configuration */
        int agingCoefficientLocationState;
        float minScatteringFactorLocationState;
        float maxScatteringFactorLocationState;
        int agingCoefficientNeckError;
        float minScatteringFactorNeckError;
        float maxScatteringFactorNeckError;
        bool swarmOptimizationEnabled;
        float velocityFactorLocalBest;
        float velocityFactorGlobalBest;
        float localBestAgingFactor;
        float bestWeightTresholdSwarmOptimization;
        bool annealingEnabled;
        int numberOfAnnealingLevels;
        float annealingFactor;
        float bestWeightTresholdAnnealing;
        bool conservativeUpdate;
        bool resamplingTresholdEnabled;
        float relativeNEffTreshold;
        CNeckErrorUncertaintyModel::UncertaintyMode neckErrorUncertaintyMode;
        float sigmaRoll;
        float sigmaPitch;
        float sigmaYaw;
        bool scatteringReductionEnabled;
        CTrackingControl::LineRegistrationVisualizationMode lineRegistrationVisualizationMode;
        float trackingNeckErrorRollMin;
        float trackingNeckErrorRollMax;
        float trackingNeckErrorPitchMin;
        float trackingNeckErrorPitchMax;
        float trackingNeckErrorYawMin;
        float trackingNeckErrorYawMax;
        bool useMahalanobisDistanceBasedScatteringReduction;
        int counterTreshold;
        CEgomotionTrackingPFExecutionUnit::MahalanobisDistanceBasedScatteringReductionResamplingMode mahalanobisDistanceBasedScatterinReductionResamplingMode;
        float totalWeightPercentageTreshold;

        /* Location estimator configuration */
        float inertiaFactor;
        float cognitionFactor;
        float socialFactor;

        /* Head planner configuration */
        float headPlannerRollMin;
        float headPlannerRollMax;
        float headPlannerPitchMin;
        float headPlannerPitchMax;
        float headPlannerYawMin;
        float headPlannerYawMax;
        int rollSamplesCount;
        int pitchSamplesCount;
        int yawSamplesCount;
        bool movingHeadAllowed;
        float configurationMass;
        float dampingCoefficient;
        float targetFadingCoefficient;
        CMassSpringHeadConfigurationPlanner::SpringCoefficientMode springCoefficientMode;
        float moveToMaxRelevanceFactorTreshold;
        bool useMoveToMaxHeuristic;

        /* Prediction models configuration */
        float relativeSystematicErrorVelocityX;
        float relativeSystematicErrorVelocityY;
        float relativeSystematicErrorVelocityAlpha;
        float absoluteSystematicPlatformAxesTiltError;
        float sigmaVelocityX;
        float sigmaVelocityY;
        float sigmaVelocityAlpha;
        float sigmaPlatformAxesTiltError;
        float sigmaX;
        float sigmaY;
        float sigmaAlpha;
        bool robotControlStateUncertaintyEnabled;
        bool locationStateUncertaintyEnabled;

    };
}

