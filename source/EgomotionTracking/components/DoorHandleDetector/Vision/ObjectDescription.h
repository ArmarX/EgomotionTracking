/*
 * ObjectDescription.h
 * ============================================================================
 *  Author:     Dr.-Ing. David I. Gonzalez Aguirre
 *  Mail:       david.gonzalez@kit.edu
 * ============================================================================
 */

#pragma once

#include "Common.h"
#include "AttributeMap.h"

namespace Vision
{
    class CObjectDescription : public CAttributeMap
    {
    public:

        CObjectDescription(const std::string& ContextName, const std::string& ClassName, const std::string& InstanceName, const int ContextId, const int ClassId, const int InstanceId);

        ~CObjectDescription() override;

        const std::string& GetContextName() const;

        const std::string& GetClassName() const;

        const std::string& GetInstanceName() const;

        int GetContextId() const;

        int GetClassId() const;

        int GetInstanceId() const;

    protected:

        std::string m_ContextName;

        std::string m_ClassName;

        std::string m_InstanceName;

        int m_ContextId;

        int m_ClassId;

        int m_InstanceId;
    };
}

