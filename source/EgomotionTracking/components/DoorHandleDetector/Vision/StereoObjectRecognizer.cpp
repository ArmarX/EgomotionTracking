/*
 * StereoObjectRecognizer.cpp
 * ============================================================================
 *  Author:     Dr.-Ing. David I. Gonzalez Aguirre
 *  Mail:       david.gonzalez@kit.edu
 * ============================================================================
 */

#include "StereoObjectRecognizer.h"

#define _E_ 1

#define _W_ -1

#define _E2_ 2

#define _W2_ -2

#define _WEIGHTED_DIFFERENCE_(A,B,W) (fabs(A-B)*W)

#define _WEIGHTED_VALUE_(A,W) (A*W)

#define _NORMALIZATION_(A,X,Y,Z,W) ((A)/(X+Y+Z+W) + 0.5f)

#define _LUMINANCEFACTOR_ 0.592261911700928f

#define _CHROMATICFACTOR_ 0.431867688121978f

using namespace std;

namespace Vision
{
    CStereoObjectRecognizer::CStereoObjectRecognizer(const CStereoCalibration* pStereoCalibration, const CByteImage** ppInputImages, CByteImage** ppOutputImages) :
        m_IsInitialized(false),
        m_IsRecognizing(false),
        m_pStereoCalibration(pStereoCalibration),
        m_ppInputImages(ppInputImages),
        m_ppOutputImages(ppOutputImages),
        m_ppInternalInputImages(nullptr),
        m_ppInternalOutputImages(nullptr),
        m_IsSavingPartialResults(false)
    {
        memset(m_pMonocularCalibrations, 0, sizeof(CCalibration*) * 2);

        memset(m_BayerPatterns, 0, sizeof(BayerPatternType) * 2);

        memset(&m_T0, 0, sizeof(timeval));

        memset(&m_T1, 0, sizeof(timeval));

        if (!Initialize())
        {
            _GENERIC_ERROR_;

            return;
        }

        if (!CreateInternalImages())
        {
            _GENERIC_ERROR_;

            return;
        }

        m_pMonocularCalibrations[eLeftCamera] = m_pStereoCalibration->GetLeftCalibration();

        m_pMonocularCalibrations[eRightCamera] = m_pStereoCalibration->GetRightCalibration();
    }

    CStereoObjectRecognizer::~CStereoObjectRecognizer()
    {
        DestroyInternalImages();
    }

    bool CStereoObjectRecognizer::IsInitialized() const
    {
        return m_IsInitialized;
    }

    bool CStereoObjectRecognizer::IsRecognizing() const
    {
        return m_IsRecognizing;
    }

    CStereoCalibration* CStereoObjectRecognizer::GetStereoCalibration()
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return nullptr;
        }

        return const_cast<CStereoCalibration*>(m_pStereoCalibration);
    }

    const CStereoCalibration* CStereoObjectRecognizer::GetStereoCalibration() const
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return nullptr;
        }

        return m_pStereoCalibration;
    }

    const CCalibration* CStereoObjectRecognizer::GetMonocularCalibrations(const MonocularSource Source) const
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return nullptr;
        }

        if (!IsValid(Source))
        {
            _GENERIC_ERROR_;

            return nullptr;
        }

        return m_pMonocularCalibrations[Source];
    }

    const CByteImage** CStereoObjectRecognizer::GetInputImages() const
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return nullptr;
        }

        return m_ppInputImages;
    }

    const CByteImage** CStereoObjectRecognizer::GetOutputImages() const
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return nullptr;
        }

        return const_cast<const CByteImage**>(m_ppOutputImages);
    }

    CByteImage** CStereoObjectRecognizer::GetOutputImages()
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return nullptr;
        }

        return m_ppOutputImages;
    }

    RecognitionOutCome CStereoObjectRecognizer::Recognize(const CObjectDescription* pObjectDescription, const bool ImproveImageReconstruction, const bool DisplayOutput, const int VerboseLevel)
    {
        if (!pObjectDescription)
        {
            _GENERIC_ERROR_;

            return eError;
        }

        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return eError;
        }

        if (!LoadInputImages(ImproveImageReconstruction))
        {
            _GENERIC_ERROR_;

            return eError;
        }

        m_IsRecognizing = true;

        const RecognitionOutCome CurrentOutCome = Execute(pObjectDescription, ImproveImageReconstruction, VerboseLevel);

        m_IsRecognizing = false;

        if (CurrentOutCome == eError)
        {
            _GENERIC_ERROR_;

            return eError;
        }

        if (DisplayOutput)
        {
            if (!Displaying(CurrentOutCome))
            {
                _GENERIC_ERROR_;

                return eError;
            }

            if (!DropOutputImages())
            {
                _GENERIC_ERROR_;

                return eError;
            }
        }

        return CurrentOutCome;
    }

    bool CStereoObjectRecognizer::SetPartialResultsDirectory(const std::string& PartialResultsDirectory)
    {
        if (!PartialResultsDirectory.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!IsInitialized())
        {
            _GENERIC_ERROR_;

            return false;
        }

        m_PartialResultsDirectory = PartialResultsDirectory;

        return true;
    }

    const std::string& CStereoObjectRecognizer::GetPartialResultsDirectory() const
    {
        return m_PartialResultsDirectory;
    }

    void CStereoObjectRecognizer::SetSavePartialResults(const bool Enabled)
    {
        m_IsSavingPartialResults = Enabled;
    }

    bool CStereoObjectRecognizer::IsSavingPartialResults() const
    {
        return m_IsSavingPartialResults;
    }

    bool CStereoObjectRecognizer::Initialize()
    {
        if (!m_pStereoCalibration)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_pStereoCalibration->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_pStereoCalibration->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_ppInputImages)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_ppInputImages[eLeftCamera])
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppInputImages[eLeftCamera]->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppInputImages[eLeftCamera]->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppInputImages[eLeftCamera]->bytesPerPixel <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_ppInputImages[eLeftCamera]->pixels)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_ppInputImages[eRightCamera])
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppInputImages[eRightCamera]->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppInputImages[eRightCamera]->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppInputImages[eRightCamera]->bytesPerPixel <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_ppInputImages[eRightCamera]->pixels)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_ppOutputImages)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_ppOutputImages[eLeftCamera])
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppOutputImages[eLeftCamera]->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppOutputImages[eLeftCamera]->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppOutputImages[eLeftCamera]->bytesPerPixel <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_ppOutputImages[eLeftCamera]->pixels)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_ppOutputImages[eRightCamera])
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppOutputImages[eRightCamera]->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppOutputImages[eRightCamera]->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppOutputImages[eRightCamera]->bytesPerPixel <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_ppOutputImages[eRightCamera]->pixels)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppInputImages[eLeftCamera]->width != m_ppOutputImages[eLeftCamera]->width)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppInputImages[eLeftCamera]->height != m_ppOutputImages[eLeftCamera]->height)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppInputImages[eRightCamera]->width != m_ppOutputImages[eRightCamera]->width)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppInputImages[eRightCamera]->height != m_ppOutputImages[eRightCamera]->height)
        {
            _GENERIC_ERROR_;

            return false;
        }

        const CCalibration* pLeftCalibration = m_pStereoCalibration->GetLeftCalibration();

        if (!pLeftCalibration)
        {
            _GENERIC_ERROR_;

            return false;
        }

        const CCalibration::CCameraParameters& LeftCameraParameters = pLeftCalibration->GetCameraParameters();

        if (LeftCameraParameters.width != m_ppInputImages[eLeftCamera]->width)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (LeftCameraParameters.height != m_ppInputImages[eLeftCamera]->height)
        {
            _GENERIC_ERROR_;

            return false;
        }

        const CCalibration* pRightCalibration = m_pStereoCalibration->GetRightCalibration();

        if (!pRightCalibration)
        {
            _GENERIC_ERROR_;

            return false;
        }

        const CCalibration::CCameraParameters& RightCameraParameters = pRightCalibration->GetCameraParameters();

        if (RightCameraParameters.width != m_ppInputImages[eRightCamera]->width)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (RightCameraParameters.height != m_ppInputImages[eRightCamera]->height)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!DetectBayerPatternType(m_ppInputImages[eLeftCamera], m_BayerPatterns[eLeftCamera], 1, true, std::string("/home/gonzalez/Projects/Software/Walkman/FastHandleRecognition/PartialResults/")))
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!DetectBayerPatternType(m_ppInputImages[eRightCamera], m_BayerPatterns[eRightCamera], 1, true, std::string("/home/gonzalez/Projects/Software/Walkman/FastHandleRecognition/PartialResults/")))
        {
            _GENERIC_ERROR_;

            return false;
        }

        m_IsInitialized = true;

        return true;
    }

    bool CStereoObjectRecognizer::CreateInternalImages()
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!DestroyInternalImages())
        {
            _GENERIC_ERROR_;

            return false;
        }

        try
        {
            m_ppInternalInputImages = new CByteImage*[2];

            m_ppInternalInputImages[eLeftCamera] = new CByteImage(m_ppInputImages[eLeftCamera]->width, m_ppInputImages[eLeftCamera]->height, m_ppInputImages[eLeftCamera]->type);

            m_ppInternalInputImages[eRightCamera] = new CByteImage(m_ppInputImages[eRightCamera]->width, m_ppInputImages[eRightCamera]->height, m_ppInputImages[eRightCamera]->type);

            m_ppInternalOutputImages = new CByteImage*[2];

            m_ppInternalOutputImages[eLeftCamera] = new CByteImage(m_ppOutputImages[eLeftCamera]->width, m_ppOutputImages[eLeftCamera]->height, m_ppOutputImages[eLeftCamera]->type);

            m_ppInternalOutputImages[eRightCamera] = new CByteImage(m_ppOutputImages[eRightCamera]->width, m_ppOutputImages[eRightCamera]->height, m_ppOutputImages[eRightCamera]->type);

            m_InputActiveRegion[eLeftCamera].Set(0, 0, m_ppInputImages[eLeftCamera]->width - 1, m_ppInputImages[eLeftCamera]->height - 1);

            m_InputActiveRegion[eRightCamera].Set(0, 0, m_ppInputImages[eRightCamera]->width - 1, m_ppInputImages[eRightCamera]->height - 1);
        }
        catch (std::exception& /*Exception*/)
        {
            _GENERIC_ERROR_;

            return false;
        }

        return true;
    }

    bool CStereoObjectRecognizer::DestroyInternalImages()
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (m_ppInternalInputImages)
        {
            if (m_ppInternalInputImages[eLeftCamera])
            {
                delete m_ppInternalInputImages[eLeftCamera];

                m_ppInternalInputImages[eLeftCamera] = nullptr;
            }

            if (m_ppInternalInputImages[eRightCamera])
            {
                delete m_ppInternalInputImages[eRightCamera];

                m_ppInternalInputImages[eRightCamera] = nullptr;
            }

            delete[] m_ppInternalInputImages;

            m_ppInternalInputImages = nullptr;
        }

        if (m_ppInternalOutputImages)
        {
            if (m_ppInternalOutputImages[eLeftCamera])
            {
                delete m_ppInternalOutputImages[eLeftCamera];

                m_ppInternalOutputImages[eLeftCamera] = nullptr;
            }

            if (m_ppInternalOutputImages[eRightCamera])
            {
                delete m_ppInternalOutputImages[eRightCamera];

                m_ppInternalOutputImages[eRightCamera] = nullptr;
            }

            delete[] m_ppInternalOutputImages;

            m_ppInternalOutputImages = nullptr;
        }

        return true;
    }

    bool CStereoObjectRecognizer::LoadInputImages(const bool ImproveImageReconstruction)
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return false;
        }

        memcpy(m_ppInternalInputImages[eLeftCamera]->pixels, m_ppInputImages[eLeftCamera]->pixels, m_ppInputImages[eLeftCamera]->width * m_ppInputImages[eLeftCamera]->height * m_ppInputImages[eLeftCamera]->bytesPerPixel);

        memcpy(m_ppInternalInputImages[eRightCamera]->pixels, m_ppInputImages[eRightCamera]->pixels, m_ppInputImages[eRightCamera]->width * m_ppInputImages[eRightCamera]->height * m_ppInputImages[eRightCamera]->bytesPerPixel);

        if (ImproveImageReconstruction)
        {
            for (int i = 0; i < 2; ++i)
            {
                const MonocularSource Source = MonocularSource(i);

                CByteImage BayerPatternImage(m_ppInternalInputImages[Source]->width, m_ppInternalInputImages[Source]->height, CByteImage::eGrayScale);

                memset(BayerPatternImage.pixels, 0, m_ppInternalInputImages[Source]->width * m_ppInternalInputImages[Source]->height);

                BoundingBox2D InputActiveZone(0, 0, m_ppInternalInputImages[Source]->width - 1, m_ppInternalInputImages[Source]->height - 1);

                BoundingBox2D OutputActiveZone;

                if (!Mosaicing(m_ppInternalInputImages[Source], InputActiveZone, m_BayerPatterns[Source], &BayerPatternImage, OutputActiveZone))
                {
                    _GENERIC_ERROR_;

                    return false;
                }

                if (!Demosaicing(&BayerPatternImage, InputActiveZone, m_BayerPatterns[Source], m_ppInternalInputImages[Source], OutputActiveZone))
                {
                    _GENERIC_ERROR_;

                    return false;
                }

                BoundingBox2D* pInputActiveRegion = GetInputActiveRegion(Source);

                if (pInputActiveRegion->Containes(&OutputActiveZone))
                {
                    *pInputActiveRegion = OutputActiveZone;
                }
            }
        }

        return true;
    }

    bool CStereoObjectRecognizer::DropOutputImages()
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return false;
        }

        memcpy(m_ppOutputImages[eLeftCamera]->pixels, m_ppInternalOutputImages[eLeftCamera]->pixels, m_ppInternalOutputImages[eLeftCamera]->width * m_ppInternalOutputImages[eRightCamera]->height * m_ppInternalOutputImages[eLeftCamera]->bytesPerPixel);

        memcpy(m_ppOutputImages[eRightCamera]->pixels, m_ppInternalOutputImages[eRightCamera]->pixels, m_ppInternalOutputImages[eRightCamera]->width * m_ppInternalOutputImages[eRightCamera]->height * m_ppInternalOutputImages[eRightCamera]->bytesPerPixel);

        return true;
    }

    const CByteImage* CStereoObjectRecognizer::GetInternalInputImage(const MonocularSource Source) const
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return nullptr;
        }

        if (!IsValid(Source))
        {
            return nullptr;
        }

        return m_ppInternalInputImages[Source];
    }

    CByteImage* CStereoObjectRecognizer::GetInternalOutputImage(const MonocularSource Source)
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return nullptr;
        }

        if (!IsValid(Source))
        {
            return nullptr;
        }

        return m_ppInternalOutputImages[Source];
    }

    bool CStereoObjectRecognizer::GetInputImageDimensions(const MonocularSource Source, int& Width, int& Height)
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!IsValid(Source))
        {
            _GENERIC_ERROR_;

            return false;
        }

        Width = m_ppInputImages[Source]->width;

        Height = m_ppInputImages[Source]->height;

        return true;
    }

    bool CStereoObjectRecognizer::GetOutputImageDimensions(const MonocularSource Source, int& Width, int& Height)
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            Width = -1;

            Height = -1;

            return false;
        }

        if (!IsValid(Source))
        {
            Width = -1;

            Height = -1;

            return false;
        }

        Width = m_ppOutputImages[Source]->width;

        Height = m_ppOutputImages[Source]->height;

        return true;
    }

    const BoundingBox2D* CStereoObjectRecognizer::GetInputActiveRegion(const MonocularSource Source) const
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return nullptr;
        }

        if (!IsValid(Source))
        {
            _GENERIC_ERROR_;

            return nullptr;
        }

        return &m_InputActiveRegion[Source];
    }

    BoundingBox2D* CStereoObjectRecognizer::GetInputActiveRegion(const MonocularSource Source)
    {
        if (!m_IsInitialized)
        {
            _GENERIC_ERROR_;

            return nullptr;
        }

        if (!IsValid(Source))
        {
            _GENERIC_ERROR_;

            return nullptr;
        }

        return &m_InputActiveRegion[Source];
    }

    std::string CStereoObjectRecognizer::CreateFileName(const std::string& Directory, const MonocularSource Source, const std::string& Content, const std::string& Extension, const int Counter) const
    {
        std::ostringstream FileName;

        FileName << Directory << (Source == eLeftCamera ? "Left" : "Right") << "_[" << Content << "]_[" << Counter << "]." << Extension;

        return FileName.str();
    }

    void CStereoObjectRecognizer::StartTimeStamp(timeval& T)
    {
        gettimeofday(&T, nullptr);
    }

    const timeval& CStereoObjectRecognizer::StartTimeStamp()
    {
        gettimeofday(&m_T0, nullptr);

        return m_T0;
    }

    long int CStereoObjectRecognizer::StopTimeStamp()
    {
        gettimeofday(&m_T1, nullptr);

        const long int AT1 = m_T1.tv_sec * 1000000l + m_T1.tv_usec;

        const long int AT0 = m_T0.tv_sec * 1000000l + m_T0.tv_usec;

        return AT1 - AT0;
    }

    long int CStereoObjectRecognizer::StopTimeStamp(const timeval& T0)
    {
        gettimeofday(&m_T1, nullptr);

        const long int AT1 = m_T1.tv_sec * 1000000l + m_T1.tv_usec;

        const long int AT0 = T0.tv_sec * 1000000l + T0.tv_usec;

        return AT1 - AT0;
    }

    long int CStereoObjectRecognizer::StopTimeStamp(const timeval& T0, timeval& T1)
    {
        gettimeofday(&T1, nullptr);

        const long int AT1 = T1.tv_sec * 1000000l + T1.tv_usec;

        const long int AT0 = T0.tv_sec * 1000000l + T0.tv_usec;

        return AT1 - AT0;
    }

    bool CStereoObjectRecognizer::DetectBayerPatternType(const CByteImage* pRGBInputImage, BayerPatternType& BayerPattern, const int VerbosityLevel, const bool SaveImages, const std::string& Directory)
    {
        if (!pRGBInputImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBInputImage->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBInputImage->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBInputImage->type != CByteImage::eRGB24)
        {
            _GENERIC_ERROR_;

            return false;
        }

        CByteImage ReconstructedImage(pRGBInputImage->width, pRGBInputImage->height, CByteImage::eRGB24);

        CByteImage BayerPatternImage(pRGBInputImage->width, pRGBInputImage->height, CByteImage::eGrayScale);

        const BayerPatternType Types[4] =
        {   eGRBG, eRGGB, eBGGR, eGBRG};

        const std::string BayerPatternTypeStrings[4] =
        {   std::string("eGRBG"), std::string("eRGGB"), std::string("eBGGR"), std::string("eGBRG")};

        BoundingBox2D InputActiveZone(0, 0, BayerPatternImage.width - 1, BayerPatternImage.height - 1);

        BoundingBox2D OutputActiveZone;

        long int AbsolutDifferences[4] =
        {   0, 0, 0, 0};

        for (int i = 0; i < 4; ++i)
        {
            if (SaveImages)
            {
                const std::string SourceFileName = Directory + std::string("Source.bmp");

                if (!pRGBInputImage->SaveToFile(SourceFileName.c_str()))
                {
                    _GENERIC_ERROR_;

                    return false;
                }
            }

            if (!Mosaicing(pRGBInputImage, InputActiveZone, Types[i], &BayerPatternImage, OutputActiveZone))
            {
                _GENERIC_ERROR_;

                return false;
            }

            if (SaveImages)
            {
                const std::string MosaicingFileName = Directory + std::string("Mosaicing.bmp");

                if (!BayerPatternImage.SaveToFile(MosaicingFileName.c_str()))
                {
                    _GENERIC_ERROR_;

                    return false;
                }
            }

            timeval T0;

            StartTimeStamp(T0);

            if (!Demosaicing(&BayerPatternImage, InputActiveZone, Types[i], &ReconstructedImage, OutputActiveZone))
            {
                _GENERIC_ERROR_;

                return false;
            }

            if (VerbosityLevel)
            {
                timeval T1;

                std::cout << "Demosaicing = " << float(StopTimeStamp(T0, T1)) / 1000.0f << std::endl;
            }

            if (SaveImages)
            {
                const std::string DemosaicingFileName = Directory + std::string("Demosaicing_") + BayerPatternTypeStrings[i] + std::string("_.bmp");

                if (!ReconstructedImage.SaveToFile(DemosaicingFileName.c_str()))
                {
                    _GENERIC_ERROR_;

                    return false;
                }
            }

            if (!ComputeAbsolutDifference(pRGBInputImage, &ReconstructedImage, OutputActiveZone, AbsolutDifferences[i]))
            {
                _GENERIC_ERROR_;

                return false;
            }
        }

        long int MinimalAbsolutDifference = AbsolutDifferences[0];

        BayerPattern = Types[0];

        for (int i = 1; i < 4; ++i)
        {
            if (MinimalAbsolutDifference < AbsolutDifferences[i])
            {
                MinimalAbsolutDifference = AbsolutDifferences[i];

                BayerPattern = Types[1];
            }
        }

        return true;
    }

    bool CStereoObjectRecognizer::Mosaicing(const CByteImage* pRGBInputImage, const BoundingBox2D& InputActiveZone, const BayerPatternType BayerPattern, CByteImage* pBayerPatternOutputImage, BoundingBox2D& OutputActiveZone)
    {
        if (!pRGBInputImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBInputImage->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBInputImage->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBInputImage->type != CByteImage::eRGB24)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (InputActiveZone.IsEmpty())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!IsValid(BayerPattern))
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!pBayerPatternOutputImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pBayerPatternOutputImage->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pBayerPatternOutputImage->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pBayerPatternOutputImage->type != CByteImage::eGrayScale)
        {
            _GENERIC_ERROR_;

            return false;
        }

        int BayerPatternMap[2][2];

        if (!GetBayerPatternMap(BayerPattern, BayerPatternMap))
        {
            _GENERIC_ERROR_;

            return false;
        }

        const RGBPixel* pInputPixelBase = reinterpret_cast<const RGBPixel*>(pRGBInputImage->pixels);

        for (int Y = InputActiveZone.m_Y0; Y <= InputActiveZone.m_Y1; ++Y)
        {
            const RGBPixel* pInputPixel = pInputPixelBase + pRGBInputImage->width * Y + InputActiveZone.m_X0;

            unsigned char* pOutputPixel = pBayerPatternOutputImage->pixels + pBayerPatternOutputImage->width * Y + InputActiveZone.m_X0;

            for (int X = InputActiveZone.m_X0; X <= InputActiveZone.m_X1; ++X, ++pInputPixel)
            {
                *pOutputPixel++ = pInputPixel->m_Channels[BayerPatternMap[Y & 0x1][X & 0x1]];
            }
        }

        OutputActiveZone = InputActiveZone;

        return true;
    }

    void CStereoObjectRecognizer::LoadAvailableChannel(const CByteImage* pBayerPatternInputImage, const BoundingBox2D& OutZone, CByteImage* pRGBOutputImage, const int BayerPatternMap[2][2])
    {
        const unsigned char* pInPixelBase = pBayerPatternInputImage->pixels + pBayerPatternInputImage->width * OutZone.m_Y0 + OutZone.m_X0;

        RGBPixel* pOutPixelBase = reinterpret_cast<RGBPixel*>(pRGBOutputImage->pixels) + pRGBOutputImage->width * OutZone.m_Y0 + OutZone.m_X0;

        for (int Y = OutZone.m_Y0 ; Y <= OutZone.m_Y1 ; ++Y, pOutPixelBase += pRGBOutputImage->width, pInPixelBase += pBayerPatternInputImage->width)
        {
            const unsigned char* pInPixel = pInPixelBase;

            RGBPixel* pOutPixel = pOutPixelBase;

            for (int X = OutZone.m_X0 ; X <= OutZone.m_X1 ; ++X, ++pOutPixel)
            {
                pOutPixel->m_Channels[BayerPatternMap[Y & 0x1][X & 0x1]] = *pInPixel++;
            }
        }
    }

    void CStereoObjectRecognizer::EstimateGreenChannel(const CByteImage* pBayerPatternInputImage, CByteImage* pRGBOutputImage, const BoundingBox2D& OutZone, const int BayerPatternMap[2][2], const float LuminanceFactor, const float DW2_ChromaticFactor)
    {
        RGBPixel* const pOutPixelBase = reinterpret_cast<RGBPixel*>(pRGBOutputImage->pixels);

        const int N = -pBayerPatternInputImage->width, S = pBayerPatternInputImage->width, N2 = pBayerPatternInputImage->width * -2, S2 = pBayerPatternInputImage->width * 2;

        for (int Y = OutZone.m_Y0 ; Y <= OutZone.m_Y1 ; ++Y)
        {
            const int XB = (BayerPatternMap[Y & 0X1][OutZone.m_X0 & 0X1] == 1) ? OutZone.m_X0 + 1 : OutZone.m_X0;

            const unsigned char* pInPixel = pBayerPatternInputImage->pixels + pBayerPatternInputImage->width * Y + XB;

            RGBPixel* pOutPixel = pOutPixelBase + pRGBOutputImage->width * Y + XB;

            for (int X = XB ; X <= OutZone.m_X1 ; X += 2, pInPixel += 2, pOutPixel += 2)
            {
                float G = _WEIGHTED_DIFFERENCE_(pInPixel[N], pInPixel[S], LuminanceFactor);

                const float GN = GradientWeightingKernel(G + _WEIGHTED_DIFFERENCE_(*pInPixel, pInPixel[N2], DW2_ChromaticFactor));

                const float GS = GradientWeightingKernel(G + _WEIGHTED_DIFFERENCE_(*pInPixel, pInPixel[S2], DW2_ChromaticFactor));

                G = _WEIGHTED_DIFFERENCE_(pInPixel[_E_], pInPixel[_W_], LuminanceFactor);

                const float GE = GradientWeightingKernel(G + _WEIGHTED_DIFFERENCE_(*pInPixel, pInPixel[_E2_], DW2_ChromaticFactor));

                const float GW = GradientWeightingKernel(G + _WEIGHTED_DIFFERENCE_(*pInPixel, pInPixel[_W2_], DW2_ChromaticFactor));

                pOutPixel->m_Elements.m_G = _NORMALIZATION_(_WEIGHTED_VALUE_(pInPixel[N], GN) + _WEIGHTED_VALUE_(pInPixel[S], GS) + _WEIGHTED_VALUE_(pInPixel[_E_], GE) + _WEIGHTED_VALUE_(pInPixel[_W_], GW), GN, GS, GE, GW);
            }
        }
    }

    void CStereoObjectRecognizer::EstimateCrossDiagonalRedBlueChannels(const CByteImage* pBayerPatternInputImage, CByteImage* pRGBOutputImage, const BoundingBox2D& OutZone, const int BayerPatternMap[2][2], const float DW22_ChromaticFactor, const float DW11_LuminanceFactor)
    {
        RGBPixel* const pOutPixelBase = reinterpret_cast<RGBPixel*>(pRGBOutputImage->pixels);

        const int NE = 1 - pBayerPatternInputImage->width, NW = -pBayerPatternInputImage->width - 1, SE = pBayerPatternInputImage->width + 1, SW = pBayerPatternInputImage->width - 1;

        for (int Y = OutZone.m_Y0 ; Y <= OutZone.m_Y1 ; ++Y)
        {
            const int XB = (BayerPatternMap[Y & 0X1][OutZone.m_X0 & 0X1] == 1) ? OutZone.m_X0 + 1 : OutZone.m_X0;

            const unsigned char* pInPixel = pBayerPatternInputImage->pixels + pBayerPatternInputImage->width * Y + XB;

            RGBPixel* pOutPixel = pOutPixelBase + pRGBOutputImage->width * Y + XB;

            const int CurrentChannel = (BayerPatternMap[Y & 0x1][XB & 0x1] == 0) ? 2 : 0;

            for (int X = XB ; X <= OutZone.m_X1 ; X += 2, pInPixel += 2, pOutPixel += 2)
            {
                float G = _WEIGHTED_DIFFERENCE_(pInPixel[NE], pInPixel[SW], DW22_ChromaticFactor);

                const float GNE = GradientWeightingKernel(G + _WEIGHTED_DIFFERENCE_(pOutPixel->m_Elements.m_G, pOutPixel[NE].m_Elements.m_G, DW11_LuminanceFactor));

                const float GSW = GradientWeightingKernel(G + _WEIGHTED_DIFFERENCE_(pOutPixel->m_Elements.m_G, pOutPixel[SW].m_Elements.m_G, DW11_LuminanceFactor));

                G = _WEIGHTED_DIFFERENCE_(pInPixel[NW], pInPixel[SE], DW22_ChromaticFactor);

                const float GNW = GradientWeightingKernel(G + _WEIGHTED_DIFFERENCE_(pOutPixel->m_Elements.m_G, pOutPixel[NW].m_Elements.m_G, DW11_LuminanceFactor));

                const float GSE = GradientWeightingKernel(G + _WEIGHTED_DIFFERENCE_(pOutPixel->m_Elements.m_G, pOutPixel[SE].m_Elements.m_G, DW11_LuminanceFactor));

                pOutPixel->m_Channels[CurrentChannel] = _NORMALIZATION_(_WEIGHTED_VALUE_(pInPixel[NE], GNE) + _WEIGHTED_VALUE_(pInPixel[NW], GNW) + _WEIGHTED_VALUE_(pInPixel[SE], GSE) + _WEIGHTED_VALUE_(pInPixel[SW], GSW), GNE, GSE, GNW, GSW);
            }
        }
    }

    void CStereoObjectRecognizer::EstimateCrossAlignedRedBlueChannels(const CByteImage* pBayerPatternInputImage, CByteImage* pRGBOutputImage, const BoundingBox2D& OutZone, const int BayerPatternMap[2][2], const float DW2_LuminanceFactor, const float ChromaticFactor)
    {
        RGBPixel* const pOutPixelBase = reinterpret_cast<RGBPixel*>(pRGBOutputImage->pixels);

        const int N = -pBayerPatternInputImage->width, S = pBayerPatternInputImage->width, N2 = pBayerPatternInputImage->width * -2, S2 = pBayerPatternInputImage->width * 2;

        for (int Y = OutZone.m_Y0 ; Y <= OutZone.m_Y1 ; ++Y)
        {
            const int XB = (BayerPatternMap[Y & 0X1][OutZone.m_X0 & 0X1] == 1) ? OutZone.m_X0 : OutZone.m_X0 + 1;

            RGBPixel* pOutPixel = pOutPixelBase + pRGBOutputImage->width * Y + XB;

            for (int X = XB ; X <= OutZone.m_X1 ; X += 2, pOutPixel += 2)
            {
                const float WGN = _WEIGHTED_DIFFERENCE_(pOutPixel->m_Elements.m_G, pOutPixel[N2].m_Elements.m_G, DW2_LuminanceFactor);

                const float WGS = _WEIGHTED_DIFFERENCE_(pOutPixel->m_Elements.m_G, pOutPixel[S2].m_Elements.m_G, DW2_LuminanceFactor);

                const float WGE = _WEIGHTED_DIFFERENCE_(pOutPixel->m_Elements.m_G, pOutPixel[_E2_].m_Elements.m_G, DW2_LuminanceFactor);

                const float WGW = _WEIGHTED_DIFFERENCE_(pOutPixel->m_Elements.m_G, pOutPixel[_W2_].m_Elements.m_G, DW2_LuminanceFactor);

                float GV = _WEIGHTED_DIFFERENCE_(pOutPixel[N].m_Elements.m_R, pOutPixel[S].m_Elements.m_R, ChromaticFactor);

                float GN = GradientWeightingKernel(WGN + GV);

                float GS = GradientWeightingKernel(WGS + GV);

                float GH = _WEIGHTED_DIFFERENCE_(pOutPixel[_E_].m_Elements.m_R, pOutPixel[_W_].m_Elements.m_R, ChromaticFactor);

                float GE = GradientWeightingKernel(WGE + GH);

                float GW = GradientWeightingKernel(WGW + GH);

                pOutPixel->m_Elements.m_R = _NORMALIZATION_(_WEIGHTED_VALUE_(pOutPixel[N].m_Elements.m_R, GN) + _WEIGHTED_VALUE_(pOutPixel[S].m_Elements.m_R, GS) + _WEIGHTED_VALUE_(pOutPixel[_E_].m_Elements.m_R, GE) + _WEIGHTED_VALUE_(pOutPixel[_W_].m_Elements.m_R, GW), GN, GS, GE, GW);

                GV = _WEIGHTED_DIFFERENCE_(pOutPixel[N].m_Elements.m_B, pOutPixel[S].m_Elements.m_B, ChromaticFactor);

                GN = GradientWeightingKernel(WGN + GV);

                GS = GradientWeightingKernel(WGS + GV);

                GH = _WEIGHTED_DIFFERENCE_(pOutPixel[_E_].m_Elements.m_B, pOutPixel[_W_].m_Elements.m_B, ChromaticFactor);

                GE = GradientWeightingKernel(WGE + GH);

                GW = GradientWeightingKernel(WGW + GH);

                pOutPixel->m_Elements.m_B = _NORMALIZATION_(_WEIGHTED_VALUE_(pOutPixel[N].m_Elements.m_B, GN) + _WEIGHTED_VALUE_(pOutPixel[S].m_Elements.m_B, GS) + _WEIGHTED_VALUE_(pOutPixel[_E_].m_Elements.m_B, GE) + _WEIGHTED_VALUE_(pOutPixel[_W_].m_Elements.m_B, GW), GN, GS, GE, GW);
            }
        }
    }

    bool CStereoObjectRecognizer::DirectionalGradientDemosaicing(const CByteImage* pBayerPatternInputImage, const BoundingBox2D& InZone, const BayerPatternType BayerPattern, CByteImage* pRGBOutputImage, BoundingBox2D& OutZone, const float LuminanceFactor, const float ChromaticFactor)
    {
        int BayerPatternMap[2][2];

        if (!GetBayerPatternMap(BayerPattern, BayerPatternMap))
        {
            _GENERIC_ERROR_;

            return false;
        }

        OutZone = InZone;

        if (OutZone.IsEmpty())
        {
            _GENERIC_ERROR_;

            return false;
        }

        LoadAvailableChannel(pBayerPatternInputImage, OutZone, pRGBOutputImage, BayerPatternMap);

        OutZone.AddOffset(-2);

        if (OutZone.IsEmpty())
        {
            _GENERIC_ERROR_;

            return false;
        }

        EstimateGreenChannel(pBayerPatternInputImage, pRGBOutputImage, OutZone, BayerPatternMap, LuminanceFactor, 0.5f * ChromaticFactor);

        OutZone.AddOffset(-1);

        if (OutZone.IsEmpty())
        {
            _GENERIC_ERROR_;

            return false;
        }

        EstimateCrossDiagonalRedBlueChannels(pBayerPatternInputImage, pRGBOutputImage, OutZone, BayerPatternMap, ChromaticFactor / float(M_SQRT2), LuminanceFactor / float(M_SQRT2));

        OutZone.AddOffset(-1);

        if (OutZone.IsEmpty())
        {
            _GENERIC_ERROR_;

            return false;
        }

        EstimateCrossAlignedRedBlueChannels(pBayerPatternInputImage, pRGBOutputImage, OutZone, BayerPatternMap, 0.5f * LuminanceFactor, ChromaticFactor);

        return true;
    }

    bool CStereoObjectRecognizer::Demosaicing(const CByteImage* pBayerPatternInputImage, const BoundingBox2D& InputActiveZone, const BayerPatternType BayerPattern, CByteImage* pRGBOutputImage, BoundingBox2D& OutputActiveZone)
    {
        if (!pBayerPatternInputImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pBayerPatternInputImage->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pBayerPatternInputImage->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pBayerPatternInputImage->type != CByteImage::eGrayScale)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!IsValid(BayerPattern))
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!pRGBOutputImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBOutputImage->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBOutputImage->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBOutputImage->type != CByteImage::eRGB24)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pBayerPatternInputImage->width != pRGBOutputImage->width)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pBayerPatternInputImage->height != pRGBOutputImage->height)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!DirectionalGradientDemosaicing(pBayerPatternInputImage, InputActiveZone, BayerPattern, pRGBOutputImage, OutputActiveZone, _LUMINANCEFACTOR_, _CHROMATICFACTOR_))
        {
            _GENERIC_ERROR_;

            return false;
        }

        return true;
    }

    bool CStereoObjectRecognizer::ComputeAbsolutDifference(const CByteImage* pRGBInputImageA, const CByteImage* pRGBInputImageB, const BoundingBox2D& CommonActiveZone, long int& AbsolutDifference)
    {
        if (!pRGBInputImageA)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBInputImageA->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBInputImageA->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBInputImageA->type != CByteImage::eRGB24)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!pRGBInputImageB)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBInputImageB->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBInputImageB->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBInputImageB->type != CByteImage::eRGB24)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBInputImageA->width != pRGBInputImageB->width)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRGBInputImageA->height != pRGBInputImageB->height)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (CommonActiveZone.IsEmpty())
        {
            _GENERIC_ERROR_;

            return false;
        }

        AbsolutDifference = 0;

        for (int Y = CommonActiveZone.m_Y0; Y <= CommonActiveZone.m_Y1; ++Y)
        {
            const RGBPixel* pInputPixelA = reinterpret_cast<const RGBPixel*>(pRGBInputImageA->pixels) + pRGBInputImageA->width * Y + CommonActiveZone.m_X0;

            const RGBPixel* pInputPixelB = reinterpret_cast<const RGBPixel*>(pRGBInputImageB->pixels) + pRGBInputImageB->width * Y + CommonActiveZone.m_X0;

            for (int X = CommonActiveZone.m_X0; X <= CommonActiveZone.m_X1; ++X, ++pInputPixelA, ++pInputPixelB)
            {
                AbsolutDifference += abs(pInputPixelA->m_Elements.m_R - pInputPixelB->m_Elements.m_R);

                AbsolutDifference += abs(pInputPixelA->m_Elements.m_G - pInputPixelB->m_Elements.m_G);

                AbsolutDifference += abs(pInputPixelA->m_Elements.m_B - pInputPixelB->m_Elements.m_B);
            }
        }

        return true;
    }

    void CStereoObjectRecognizer::About()
    {
        std::string About;

        const unsigned char* pAbout = g_pAbout;

        while (*pAbout++ != 0xff)
        {
            About += *pAbout + *g_pAbout;
        }

        std::cout << About << std::endl;
    }
}
