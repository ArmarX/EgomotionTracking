/*
 * AttributeMap.cpp
 * ============================================================================
 *  Author:     Dr.-Ing. David I. Gonzalez Aguirre
 *  Mail:       david.gonzalez@kit.edu
 * ============================================================================
 */

#include "AttributeMap.h"

namespace Vision
{
    CAttributeMap::CAttributeMap() :
        m_AttributeMap()
    {
    }

    CAttributeMap::~CAttributeMap()
        = default;

    bool CAttributeMap::SetAttribute(const std::string& AttributeName, const int ScalarIntegerValue, const bool Overwrite)
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!Overwrite)
        {
            if (m_AttributeMap.find(AttributeName) != m_AttributeMap.end())
            {
                _GENERIC_ERROR_;

                return false;
            }
        }

        ValueContainer Container;

        Container.m_Type = ValueContainer::eScalarIntegerValue;

        Container.m_Value.m_ScalarIntegerValue = ScalarIntegerValue;

        if (!m_AttributeMap.insert(std::pair<std::string, ValueContainer>(AttributeName, Container)).second)
        {
            _GENERIC_ERROR_;

            return false;
        }

        return true;
    }

    bool CAttributeMap::SetAttribute(const std::string& AttributeName, const float ScalarRealValue, const bool Overwrite)
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!Overwrite)
        {
            if (m_AttributeMap.find(AttributeName) != m_AttributeMap.end())
            {
                _GENERIC_ERROR_;

                return false;
            }
        }

        ValueContainer Container;

        Container.m_Type = ValueContainer::eScalarRealValue;

        Container.m_Value.m_ScalarRealValue = ScalarRealValue;

        if (!m_AttributeMap.insert(std::pair<std::string, ValueContainer>(AttributeName, Container)).second)
        {
            _GENERIC_ERROR_;

            return false;
        }

        return true;
    }

    bool CAttributeMap::SetAttribute(const std::string& AttributeName, const Vec2d& Vector2D, const bool Overwrite)
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!Overwrite)
        {
            if (m_AttributeMap.find(AttributeName) != m_AttributeMap.end())
            {
                _GENERIC_ERROR_;

                return false;
            }
        }

        ValueContainer Container;

        Container.m_Type = ValueContainer::eVector2D;

        Container.m_Value.m_Vector2D = Vector2D;

        if (!m_AttributeMap.insert(std::pair<std::string, ValueContainer>(AttributeName, Container)).second)
        {
            _GENERIC_ERROR_;

            return false;
        }

        return true;
    }

    bool CAttributeMap::SetAttribute(const std::string& AttributeName, const PointPair2d& PointPair2D, const bool Overwrite)
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!Overwrite)
        {
            if (m_AttributeMap.find(AttributeName) != m_AttributeMap.end())
            {
                _GENERIC_ERROR_;

                return false;
            }
        }

        ValueContainer Container;

        Container.m_Type = ValueContainer::ePointPair2D;

        Container.m_Value.m_PointPair2D = PointPair2D;

        if (!m_AttributeMap.insert(std::pair<std::string, ValueContainer>(AttributeName, Container)).second)
        {
            _GENERIC_ERROR_;

            return false;
        }

        return true;
    }

    bool CAttributeMap::SetAttribute(const std::string& AttributeName, const Vec3d& Vector3D, const bool Overwrite)
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!Overwrite)
        {
            if (m_AttributeMap.find(AttributeName) != m_AttributeMap.end())
            {
                _GENERIC_ERROR_;

                return false;
            }
        }

        ValueContainer Container;

        Container.m_Type = ValueContainer::eVector3D;

        Container.m_Value.m_Vector3D = Vector3D;

        if (!m_AttributeMap.insert(std::pair<std::string, ValueContainer>(AttributeName, Container)).second)
        {
            _GENERIC_ERROR_;

            return false;
        }

        return true;
    }

    bool CAttributeMap::SetAttribute(const std::string& AttributeName, const PointPair3d& PointPair3D, const bool Overwrite)
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!Overwrite)
        {
            if (m_AttributeMap.find(AttributeName) != m_AttributeMap.end())
            {
                _GENERIC_ERROR_;

                return false;
            }
        }

        ValueContainer Container;

        Container.m_Type = ValueContainer::ePointPair3D;

        Container.m_Value.m_PointPair3D = PointPair3D;

        if (!m_AttributeMap.insert(std::pair<std::string, ValueContainer>(AttributeName, Container)).second)
        {
            _GENERIC_ERROR_;

            return false;
        }

        return true;
    }

    bool CAttributeMap::SetAttribute(const std::string& AttributeName, const Mat2d& Matrix2D, const bool Overwrite)
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!Overwrite)
        {
            if (m_AttributeMap.find(AttributeName) != m_AttributeMap.end())
            {
                _GENERIC_ERROR_;

                return false;
            }
        }

        ValueContainer Container;

        Container.m_Type = ValueContainer::eMatrix2D;

        Container.m_Value.m_Matrix2D = Matrix2D;

        if (!m_AttributeMap.insert(std::pair<std::string, ValueContainer>(AttributeName, Container)).second)
        {
            _GENERIC_ERROR_;

            return false;
        }

        return true;
    }

    bool CAttributeMap::SetAttribute(const std::string& AttributeName, const Mat3d& Matrix3D, const bool Overwrite)
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!Overwrite)
        {
            if (m_AttributeMap.find(AttributeName) != m_AttributeMap.end())
            {
                _GENERIC_ERROR_;

                return false;
            }
        }

        ValueContainer Container;

        Container.m_Type = ValueContainer::eMatrix3D;

        Container.m_Value.m_Matrix3D = Matrix3D;

        if (!m_AttributeMap.insert(std::pair<std::string, ValueContainer>(AttributeName, Container)).second)
        {
            _GENERIC_ERROR_;

            return false;
        }

        return true;
    }

    int CAttributeMap::GetTotalAttributes() const
    {
        return int(m_AttributeMap.size());
    }

    bool CAttributeMap::HasAttribute(const std::string& AttributeName) const
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_AttributeMap.size())
        {
            return false;
        }

        return (m_AttributeMap.find(AttributeName) != m_AttributeMap.end());
    }

    ValueContainer CAttributeMap::operator[](const std::string& AttributeName) const
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return ValueContainer();
        }

        if (!m_AttributeMap.size())
        {
            return ValueContainer();
        }

        std::map<std::string, ValueContainer>::const_iterator pAttribute = m_AttributeMap.find(AttributeName);

        if (pAttribute == m_AttributeMap.end())
        {
            return ValueContainer();
        }

        return pAttribute->second;
    }

    ValueContainer CAttributeMap::GetAttributeValue(const std::string& AttributeName) const
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return ValueContainer();
        }

        if (!m_AttributeMap.size())
        {
            return ValueContainer();
        }

        std::map<std::string, ValueContainer>::const_iterator pAttribute = m_AttributeMap.find(AttributeName);

        if (pAttribute == m_AttributeMap.end())
        {
            return ValueContainer();
        }

        return pAttribute->second;
    }

    bool CAttributeMap::GetAttributeValue(const std::string& AttributeName, int& ScalarIntegerValue) const
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_AttributeMap.size())
        {
            return false;
        }

        std::map<std::string, ValueContainer>::const_iterator pAttribute = m_AttributeMap.find(AttributeName);

        if (pAttribute == m_AttributeMap.end())
        {
            return false;
        }

        if (pAttribute->second.m_Type != ValueContainer::eScalarIntegerValue)
        {
            return false;
        }

        ScalarIntegerValue = pAttribute->second.m_Value.m_ScalarIntegerValue;

        return true;
    }

    bool CAttributeMap::GetAttributeValue(const std::string& AttributeName, float& ScalarRealValue) const
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_AttributeMap.size())
        {
            return false;
        }

        std::map<std::string, ValueContainer>::const_iterator pAttribute = m_AttributeMap.find(AttributeName);

        if (pAttribute == m_AttributeMap.end())
        {
            return false;
        }

        if (pAttribute->second.m_Type != ValueContainer::eScalarRealValue)
        {
            return false;
        }

        ScalarRealValue = pAttribute->second.m_Value.m_ScalarRealValue;

        return true;
    }

    bool CAttributeMap::GetAttributeValue(const std::string& AttributeName, Vec2d& Vector2D) const
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_AttributeMap.size())
        {
            return false;
        }

        std::map<std::string, ValueContainer>::const_iterator pAttribute = m_AttributeMap.find(AttributeName);

        if (pAttribute == m_AttributeMap.end())
        {
            return false;
        }

        if (pAttribute->second.m_Type != ValueContainer::eVector2D)
        {
            return false;
        }

        Vector2D = pAttribute->second.m_Value.m_Vector2D;

        return true;
    }

    bool CAttributeMap::GetAttributeValue(const std::string& AttributeName, PointPair2d& PointPair2D) const
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_AttributeMap.size())
        {
            return false;
        }

        std::map<std::string, ValueContainer>::const_iterator pAttribute = m_AttributeMap.find(AttributeName);

        if (pAttribute == m_AttributeMap.end())
        {
            return false;
        }

        if (pAttribute->second.m_Type != ValueContainer::ePointPair2D)
        {
            return false;
        }

        PointPair2D = pAttribute->second.m_Value.m_PointPair2D;

        return true;
    }

    bool CAttributeMap::GetAttributeValue(const std::string& AttributeName, Vec3d& Vector3D) const
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_AttributeMap.size())
        {
            return false;
        }

        std::map<std::string, ValueContainer>::const_iterator pAttribute = m_AttributeMap.find(AttributeName);

        if (pAttribute == m_AttributeMap.end())
        {
            return false;
        }

        if (pAttribute->second.m_Type != ValueContainer::eVector3D)
        {
            return false;
        }

        Vector3D = pAttribute->second.m_Value.m_Vector3D;

        return true;
    }

    bool CAttributeMap::GetAttributeValue(const std::string& AttributeName, PointPair3d& PointPair3D) const
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_AttributeMap.size())
        {
            return false;
        }

        std::map<std::string, ValueContainer>::const_iterator pAttribute = m_AttributeMap.find(AttributeName);

        if (pAttribute == m_AttributeMap.end())
        {
            return false;
        }

        if (pAttribute->second.m_Type != ValueContainer::ePointPair3D)
        {
            return false;
        }

        PointPair3D = pAttribute->second.m_Value.m_PointPair3D;

        return true;
    }

    bool CAttributeMap::GetAttributeValue(const std::string& AttributeName, Mat2d& Matrix2D) const
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_AttributeMap.size())
        {
            return false;
        }

        std::map<std::string, ValueContainer>::const_iterator pAttribute = m_AttributeMap.find(AttributeName);

        if (pAttribute == m_AttributeMap.end())
        {
            return false;
        }

        if (pAttribute->second.m_Type != ValueContainer::eMatrix2D)
        {
            return false;
        }

        Matrix2D = pAttribute->second.m_Value.m_Matrix2D;

        return true;
    }

    bool CAttributeMap::GetAttributeValue(const std::string& AttributeName, Mat3d& Matrix3D) const
    {
        if (!AttributeName.length())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!m_AttributeMap.size())
        {
            return false;
        }

        std::map<std::string, ValueContainer>::const_iterator pAttribute = m_AttributeMap.find(AttributeName);

        if (pAttribute == m_AttributeMap.end())
        {
            return false;
        }

        if (pAttribute->second.m_Type != ValueContainer::eMatrix3D)
        {
            return false;
        }

        Matrix3D = pAttribute->second.m_Value.m_Matrix3D;

        return true;
    }

    const std::map<std::string, ValueContainer>& CAttributeMap::GetAttributeMap() const
    {
        return m_AttributeMap;
    }

    std::map<std::string, ValueContainer>& CAttributeMap::GetAttributeMap()
    {
        return m_AttributeMap;
    }
}
