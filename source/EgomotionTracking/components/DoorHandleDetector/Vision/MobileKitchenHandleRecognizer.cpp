/*
 * MobileKitchenHandleRecognizer.cpp
 * ============================================================================
 *  Author:     Dr.-Ing. David I. Gonzalez Aguirre
 *  Mail:       david.gonzalez@kit.edu
 * ============================================================================
 */

#include "MobileKitchenHandleRecognizer.h"

#define _DOT_PRODUCT_2D_(A,B) (A.x * B.x + A.y * B.y)
#define _DOT_PRODUCT_3D_(A,B) (A.x * B.x + A.y * B.y + + A.z * B.z)

namespace Vision
{
    CMobileKitchenHandleRecognizer::CMobileKitchenHandleRecognizer(const CStereoCalibration* pStereoCalibration, const CByteImage** ppInputImages, CByteImage** ppOutputImages, const int VerbosityLevel) :
        CStereoObjectRecognizer(pStereoCalibration, ppInputImages, ppOutputImages),
        m_ppIntensityImages(nullptr),
        m_ppSmoothIntensityImages(nullptr),
        m_ppEdgeImages(nullptr),
        m_ppFilteredEdgeImages(nullptr),
        m_ppLowResolutionMaskImages(nullptr),
        m_ppHighResolutionMaskImages(nullptr),
        m_KernelRadius(0),
        m_KernelIntegral(0.0f),
        m_pKernel(nullptr),
        m_TotalAvailableHarrisPointsPerMonocularSource(0)
    {
        memset(m_pHarrisPoints, 0, sizeof(Vec2d*) * 2);

        memset(m_pDescriptors, 0, sizeof(float*) * 2);

        memset(&m_ParameterSet, 0, sizeof(ParameterSet));

        if (!IsInitialized())
        {
            _GENERIC_ERROR_;

            return;
        }

        if (!CreateIntermediateDataStructures(_MOBILE_KITCHEN_HANDLE_RECOGNIZER_MAXIMAL_AMOUNT_HARRIS_POINTS_, _MOBILE_KITCHEN_HANDLE_RECOGNIZER_DEFAULT_KERNEL_RADIUS_, VerbosityLevel))
        {
            _GENERIC_ERROR_;
        }
    }

    CMobileKitchenHandleRecognizer::~CMobileKitchenHandleRecognizer()
    {
        DestroyIntermediateDataStructures();
    }

    const std::vector<CMobileKitchenHandleRecognizer::MobileKitchenHandle>& CMobileKitchenHandleRecognizer::GetMobileKitchenHandleRecognizedInstances() const
    {
        return m_MobileKitchenHandle;
    }

    CMobileKitchenHandleRecognizer::ParameterSet CMobileKitchenHandleRecognizer::GetParameterSet(const CObjectDescription* pObjectDescription, const bool UsingImproveImageReconstruction, const int VerboseLevel) const
    {
        ParameterSet CurrentParameterSet;

        memset(&CurrentParameterSet, 0, sizeof(ParameterSet));

        if (!pObjectDescription)
        {
            _GENERIC_ERROR_;

            return CurrentParameterSet;
        }

        if (pObjectDescription->GetContextName() != _MOBILE_KITCHEN_HANDLE_RECOGNIZER_CONTEXT_)
        {
            _GENERIC_ERROR_;

            return CurrentParameterSet;
        }

        if (pObjectDescription->GetClassName() != _MOBILE_KITCHEN_HANDLE_RECOGNIZER_CLASS_)
        {
            _GENERIC_ERROR_;

            return CurrentParameterSet;
        }

        bool IsHorizontal = false;

        if (pObjectDescription->GetInstanceName() == _MOBILE_KITCHEN_HANDLE_RECOGNIZER_INTANCE_LEFT_DRAWER_HANDLER_)
        {
            IsHorizontal = true;
        }
        else if (pObjectDescription->GetInstanceName() == _MOBILE_KITCHEN_HANDLE_RECOGNIZER_INTANCE_RIGHT_DRAWER_HANDLER_)
        {
            IsHorizontal = true;
        }
        else if (pObjectDescription->GetInstanceName() == _MOBILE_KITCHEN_HANDLE_RECOGNIZER_INTANCE_SINK_DOOR_HANDLER_)
        {
            IsHorizontal = true;
        }
        else if (pObjectDescription->GetInstanceName() == _MOBILE_KITCHEN_HANDLE_RECOGNIZER_INTANCE_FRIDGE_DOOR_HANDLER_)
        {
            IsHorizontal = false;
        }
        else
        {
            _GENERIC_ERROR_;

            return CurrentParameterSet;
        }

        float MainEdgeLength = 0.0f;

        if (!pObjectDescription->GetAttributeValue(_MOBILE_KITCHEN_HANDLE_RECOGNIZER_CLASS_ATTRIBUTE_MAIN_EDGE_LENGTH_, MainEdgeLength))
        {
            _GENERIC_ERROR_;

            return CurrentParameterSet;
        }

        float SecondaryEdgeLength = 0.0f;

        if (pObjectDescription->HasAttribute(_MOBILE_KITCHEN_HANDLE_RECOGNIZER_CLASS_ATTRIBUTE_SECONDARY_EDGE_LENGTH_))
        {
            if (!pObjectDescription->GetAttributeValue(_MOBILE_KITCHEN_HANDLE_RECOGNIZER_CLASS_ATTRIBUTE_SECONDARY_EDGE_LENGTH_, SecondaryEdgeLength))
            {
                _GENERIC_ERROR_;

                return CurrentParameterSet;
            }
        }

        //========================================================================================================================================================

        CurrentParameterSet.m_ProcessImagesParameterSet.m_CannyLowThreshold = 32;

        CurrentParameterSet.m_ProcessImagesParameterSet.m_CannyHighThreshold = 32;

        CurrentParameterSet.m_ProcessImagesParameterSet.m_MinimalBlockSize = 64;

        CurrentParameterSet.m_ProcessImagesParameterSet.m_MinimalBlockDimension = 64;

        CurrentParameterSet.m_ProcessImagesParameterSet.m_IsHorizontal = IsHorizontal;

        CurrentParameterSet.m_ProcessImagesParameterSet.m_MaximalEigenRatio = 1.0f / 6.0f;

        CurrentParameterSet.m_ProcessImagesParameterSet.m_MaximalDeviation = 0.5f;

        CurrentParameterSet.m_ProcessImagesParameterSet.m_VerbosityLevel = VerboseLevel;

        CurrentParameterSet.m_ProcessImagesParameterSet.m_SavePartialResults = true;

        //========================================================================================================================================================

        CurrentParameterSet.m_ProcessPointsParameterSet.m_HarrisSaliency[eLeftCamera] = 0.001f;

        CurrentParameterSet.m_ProcessPointsParameterSet.m_HarrisSaliency[eRightCamera] = 0.001f;

        CurrentParameterSet.m_ProcessPointsParameterSet.m_HarrisDominanceDistance[eLeftCamera] = 0.0f;

        CurrentParameterSet.m_ProcessPointsParameterSet.m_HarrisDominanceDistance[eRightCamera] = 1.0f;

        CurrentParameterSet.m_ProcessPointsParameterSet.m_MaximalSearchDepth = 4000.0f;

        CurrentParameterSet.m_ProcessPointsParameterSet.m_MinimalSearchDepth = 300.0f;

        CurrentParameterSet.m_ProcessPointsParameterSet.m_MaximalEpipolarDeviation = 16.0f;

        CurrentParameterSet.m_ProcessPointsParameterSet.m_MaximalTriangulationDeviation = 50.0f;

        CurrentParameterSet.m_ProcessPointsParameterSet.m_MinimalPatchSimilarity = 0.1f;

        CurrentParameterSet.m_ProcessPointsParameterSet.m_VerbosityLevel = VerboseLevel;

        CurrentParameterSet.m_ProcessImagesParameterSet.m_SavePartialResults = true;

        //========================================================================================================================================================

        CurrentParameterSet.m_ExtractLinesParameterSet.m_IsHorizontal = IsHorizontal;

        CurrentParameterSet.m_ExtractLinesParameterSet.m_ImageMinimalLength = 32.0f;

        CurrentParameterSet.m_ExtractLinesParameterSet.m_ImageMaximalLength = 480.0f;

        CurrentParameterSet.m_ExtractLinesParameterSet.m_MinimalImageMaximalLengthRatio = 0.8f;

        CurrentParameterSet.m_ExtractLinesParameterSet.m_MinimalImageOrientationCoherence = float(cos(M_PI_4 * 0.1));

        CurrentParameterSet.m_ExtractLinesParameterSet.m_SpaceMinimalLength = MainEdgeLength * 0.75f;

        CurrentParameterSet.m_ExtractLinesParameterSet.m_SpaceMaximalLength = MainEdgeLength * 1.05f;

        CurrentParameterSet.m_ExtractLinesParameterSet.m_EdgeMargin = 1.4143f;

        CurrentParameterSet.m_ExtractLinesParameterSet.m_MaximalPointsAlongLineSegment = 1;

        CurrentParameterSet.m_ExtractLinesParameterSet.m_PointsAlongLineSegmentMargin = 3.0f;

        CurrentParameterSet.m_ExtractLinesParameterSet.m_VerbosityLevel = VerboseLevel;

        CurrentParameterSet.m_ExtractLinesParameterSet.m_SavePartialResults = true;

        //========================================================================================================================================================

        CurrentParameterSet.m_MatchingParameterSet.m_ClusteringImageBandwidth = CurrentParameterSet.m_ProcessPointsParameterSet.m_MaximalEpipolarDeviation * 0.5f;

        CurrentParameterSet.m_MatchingParameterSet.m_MinimalSpaceOrientationCoherence = float(cos(M_PI_4 * 0.25));

        CurrentParameterSet.m_MatchingParameterSet.m_ClusteringSpatialBandwidth = 10.0f;

        CurrentParameterSet.m_MatchingParameterSet.m_VerbosityLevel = VerboseLevel;

        CurrentParameterSet.m_MatchingParameterSet.m_SavePartialResults = true;

        //========================================================================================================================================================

        CurrentParameterSet.m_IsValid = true;

        return CurrentParameterSet;
    }

    RecognitionOutCome CMobileKitchenHandleRecognizer::Execute(const CObjectDescription* pObjectDescription, const bool UsingImproveImageReconstruction, const int VerboseLevel)
    {
        if (!IsInitialized())
        {
            _GENERIC_ERROR_;

            return eError;
        }

        if (!pObjectDescription)
        {
            _GENERIC_ERROR_;

            return eError;
        }

        //========================================================================================================================================================
        //      GET PARAMETER SET
        //========================================================================================================================================================

        m_ParameterSet = GetParameterSet(pObjectDescription, UsingImproveImageReconstruction, VerboseLevel);

        if (!m_ParameterSet.m_IsValid)
        {
            _GENERIC_ERROR_;

            return eError;
        }

        //========================================================================================================================================================
        //      PROCESS IMAGES @ Debug Mode ~ 47.149 ms @ Release Mode ~ 16.551 ms
        //========================================================================================================================================================

        timeval T0 = StartTimeStamp();

        if (!ProcessImages(m_ParameterSet.m_ProcessImagesParameterSet, UsingImproveImageReconstruction))
        {
            _GENERIC_ERROR_;

            return eError;
        }

        long int TotalElapsedMicroseconds = 0;

        long int ElapsedMicroseconds = StopTimeStamp(T0);

        TotalElapsedMicroseconds += ElapsedMicroseconds;

        if (VerboseLevel)
        {
            std::cout << "Process Images @ " << double(ElapsedMicroseconds) / 1000.0 << " ms" << std::endl;
        }

        //========================================================================================================================================================
        //      PROCESS POINTS @ Debug Mode ~ 23.374 ms @ Release Mode ~ 8.584 ms
        //========================================================================================================================================================

        T0 = StartTimeStamp();

        if (!ProcessPoints(m_ParameterSet.m_ProcessPointsParameterSet))
        {
            _GENERIC_ERROR_;

            return eError;
        }

        ElapsedMicroseconds = StopTimeStamp(T0);

        TotalElapsedMicroseconds += ElapsedMicroseconds;

        if (VerboseLevel)
        {
            std::cout << "Process Points @ " << double(ElapsedMicroseconds) / 1000.0 << " ms" << std::endl;
        }

        if (m_StereoTriangulationPoints.size() >= 2)
        {

            //========================================================================================================================================================
            //      PROCESS POINTS @ Debug Mode ~ 7.289 @ Release Mode ~ 0.474 ms
            //========================================================================================================================================================

            T0 = StartTimeStamp();

            if (!ProcessLineSegments(m_ParameterSet.m_ExtractLinesParameterSet))
            {
                _GENERIC_ERROR_;

                return eError;
            }

            ElapsedMicroseconds = StopTimeStamp(T0);

            TotalElapsedMicroseconds += ElapsedMicroseconds;

            if (VerboseLevel)
            {
                std::cout << "Process Line Segments @ " << double(ElapsedMicroseconds) / 1000.0 << " ms" << std::endl;
            }

            if (m_SpatialLineSegments.size())
            {

                //========================================================================================================================================================
                //      PROCESS MODEL DESCRIPTION MATCHING @ Debug Mode ~ 0.409 ms @ Release Mode ~ 0.156 ms
                //========================================================================================================================================================

                T0 = StartTimeStamp();

                if (!ProcessModelDescriptionMatching(m_ParameterSet.m_MatchingParameterSet))
                {
                    _GENERIC_ERROR_;

                    return eError;
                }

                ElapsedMicroseconds = StopTimeStamp(T0);

                TotalElapsedMicroseconds += ElapsedMicroseconds;

                if (VerboseLevel)
                {
                    std::cout << "Process Model Description Matching @ " << double(ElapsedMicroseconds) / 1000.0 << " ms" << std::endl;
                }
            }
        }

        //========================================================================================================================================================
        //      TOTAL ELAPSED TIME [NO DRAWING] @ Debug Mode ~ [MIN = 56.839, MAX = 69.701] ms @ Release Mode ~ [ MIN =  23.667, MAX = 28.363] ms
        //========================================================================================================================================================

        if (VerboseLevel)
        {
            std::cout << "Total Elapsed Time  =  " << double(TotalElapsedMicroseconds) / 1000.0 << " ms" << std::endl;
        }

        switch (m_MobileKitchenHandle.size())
        {
            case 0:

                return eNoRecognition;

            case 1:

                return eSingleRecognition;

            default:

                return eMultipleRecognition;
        }

        _GENERIC_ERROR_;

        return eError;
    }

    bool CMobileKitchenHandleRecognizer::Displaying(const RecognitionOutCome /*CurrentOutCome*/)
    {
        if (!IsInitialized())
        {
            _GENERIC_ERROR_;

            return false;
        }

        return true;
    }

    bool CMobileKitchenHandleRecognizer::CreateIntermediateDataStructures(const int TotalAvailableHarrisPointsPerMonocularSource, const int GaussianKernelRadius, const int VerboseLevel)
    {
        if (!IsInitialized())
        {
            _GENERIC_ERROR_;

            return false;
        }

        DestroyIntermediateDataStructures();

        if (TotalAvailableHarrisPointsPerMonocularSource < _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_AMOUNT_HARRIS_POINTS_)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (TotalAvailableHarrisPointsPerMonocularSource > _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MAXIMAL_AMOUNT_HARRIS_POINTS_)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (GaussianKernelRadius < _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_KERNEL_RADIUS_)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (GaussianKernelRadius > _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MAXIMAL_KERNEL_RADIUS_)
        {
            _GENERIC_ERROR_;

            return false;
        }

        int Width[2] =
        {   0, 0};

        int Height[2] =
        {   0, 0};

        if (!GetInputImageDimensions(eLeftCamera, Width[eLeftCamera], Height[eLeftCamera]))
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!GetInputImageDimensions(eRightCamera, Width[eRightCamera], Height[eRightCamera]))
        {
            _GENERIC_ERROR_;

            return false;
        }

        m_TotalAvailableHarrisPointsPerMonocularSource = TotalAvailableHarrisPointsPerMonocularSource;

        m_KernelRadius = GaussianKernelRadius;

        try
        {
            m_ppIntensityImages = new CByteImage*[2];

            m_ppIntensityImages[eLeftCamera] = new CByteImage(Width[eLeftCamera], Height[eLeftCamera], CByteImage::eGrayScale);

            m_ppIntensityImages[eRightCamera] = new CByteImage(Width[eRightCamera], Height[eRightCamera], CByteImage::eGrayScale);

            m_ppSmoothIntensityImages = new CByteImage*[2];

            m_ppSmoothIntensityImages[eLeftCamera] = new CByteImage(Width[eLeftCamera], Height[eLeftCamera], CByteImage::eGrayScale);

            m_ppSmoothIntensityImages[eRightCamera] = new CByteImage(Width[eRightCamera], Height[eRightCamera], CByteImage::eGrayScale);

            m_ppEdgeImages = new CByteImage*[2];

            m_ppEdgeImages[eLeftCamera] = new CByteImage(Width[eLeftCamera], Height[eLeftCamera], CByteImage::eGrayScale);

            m_ppEdgeImages[eRightCamera] = new CByteImage(Width[eRightCamera], Height[eRightCamera], CByteImage::eGrayScale);

            m_ppFilteredEdgeImages = new CByteImage*[2];

            m_ppFilteredEdgeImages[eLeftCamera] = new CByteImage(Width[eLeftCamera], Height[eLeftCamera], CByteImage::eGrayScale);

            m_ppFilteredEdgeImages[eRightCamera] = new CByteImage(Width[eRightCamera], Height[eRightCamera], CByteImage::eGrayScale);

            m_ppLowResolutionMaskImages = new CByteImage*[2];

            m_ppLowResolutionMaskImages[eLeftCamera] = new CByteImage(Width[eLeftCamera] >> _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_LOW_RESOLUTION_MASK_FACTOR_, Height[eLeftCamera] >> _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_LOW_RESOLUTION_MASK_FACTOR_, CByteImage::eGrayScale);

            m_ppLowResolutionMaskImages[eRightCamera] = new CByteImage(Width[eRightCamera] >> _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_LOW_RESOLUTION_MASK_FACTOR_, Height[eRightCamera] >> _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_LOW_RESOLUTION_MASK_FACTOR_, CByteImage::eGrayScale);

            m_ppHighResolutionMaskImages = new CByteImage*[2];

            m_ppHighResolutionMaskImages[eLeftCamera] = new CByteImage(Width[eLeftCamera], Height[eLeftCamera], CByteImage::eGrayScale);

            m_ppHighResolutionMaskImages[eRightCamera] = new CByteImage(Width[eRightCamera], Height[eRightCamera], CByteImage::eGrayScale);

            m_pHarrisPoints[eLeftCamera] = new Vec2d[m_TotalAvailableHarrisPointsPerMonocularSource];

            m_pHarrisPoints[eRightCamera] = new Vec2d[m_TotalAvailableHarrisPointsPerMonocularSource];

            m_ActiveHarrisPoints[eLeftCamera].reserve(m_TotalAvailableHarrisPointsPerMonocularSource);

            m_ActiveHarrisPoints[eRightCamera].reserve(m_TotalAvailableHarrisPointsPerMonocularSource);

            const int KernelDiameter = (m_KernelRadius << 1) + 1;

            const int KernelArea = KernelDiameter * KernelDiameter;

            const float ExternalSigma = sqrt((m_KernelRadius * m_KernelRadius) / (-log(_MOBILE_KITCHEN_HANDLE_RECOGNIZER_DEFAULT_KERNEL_MINIMAL_DENSITY_)));

            const float InternalSigma = ExternalSigma / _MOBILE_KITCHEN_HANDLE_RECOGNIZER_DEFAULT_KERNEL_INTERNAL_FACTOR_;

            const float ExternalExponentFactor = -1.0f / (2.0f * ExternalSigma * ExternalSigma);

            const float InternalExponentFactor = -1.0f / (2.0f * InternalSigma * InternalSigma);

            m_pKernel = new float[KernelArea];

            m_pDescriptors[eLeftCamera] = new float[KernelArea];

            m_pDescriptors[eRightCamera] = new float[KernelArea];

            m_KernelIntegral = 0.0f;

            if (VerboseLevel)
            {
                std::cout << "Cross correlation kernel:\n" << std::endl;
            }

            const float Scaling = exp(float(m_KernelRadius * m_KernelRadius * 2) * ExternalExponentFactor) - exp(float(m_KernelRadius * m_KernelRadius * 2) * InternalExponentFactor);

            for (int Dy = -m_KernelRadius, Index = 0; Dy <= m_KernelRadius; ++Dy)
            {
                const int Dy2 = Dy * Dy;

                if (VerboseLevel)
                {
                    std::cout << "|";
                }

                for (int Dx = -m_KernelRadius; Dx <= m_KernelRadius; ++Dx, ++Index)
                {
                    const float SquareRadius = Dy2 + Dx * Dx;

                    const float ExternalDensity = exp(SquareRadius * ExternalExponentFactor);

                    const float InternalDensity = exp(SquareRadius * InternalExponentFactor);

                    m_pKernel[Index] = (ExternalDensity - InternalDensity) / Scaling;

                    m_KernelIntegral += m_pKernel[Index];

                    if (VerboseLevel)
                    {
                        if (Dy || Dx)
                        {
                            std::cout << "\t" << m_pKernel[Index] << "\t";
                        }
                        else
                        {
                            std::cout << "\t[" << m_pKernel[Index] << "]\t";
                        }
                    }
                }

                if (VerboseLevel)
                {
                    std::cout << "\t|" << std::endl;
                }
            }

            BoundingBox2D* pLeftInputActiveRegion = GetInputActiveRegion(eLeftCamera);

            pLeftInputActiveRegion->Set(m_KernelRadius, m_KernelRadius, Width[eLeftCamera] - (m_KernelRadius + 1), Height[eLeftCamera] - (m_KernelRadius + 1));

            BoundingBox2D* pRightInputActiveRegion = GetInputActiveRegion(eRightCamera);

            pRightInputActiveRegion->Set(m_KernelRadius, m_KernelRadius, Width[eRightCamera] - (m_KernelRadius + 1), Height[eRightCamera] - (m_KernelRadius + 1));

        }
        catch (std::exception& /*Exception*/)
        {
            _GENERIC_ERROR_;

            return false;
        }

        return true;
    }

    void CMobileKitchenHandleRecognizer::DestroyIntermediateDataStructures()
    {
        if (m_ppIntensityImages)
        {
            delete m_ppIntensityImages[eLeftCamera];

            delete m_ppIntensityImages[eRightCamera];

            delete[] m_ppIntensityImages;

            m_ppIntensityImages = nullptr;
        }

        if (m_ppSmoothIntensityImages)
        {
            delete m_ppSmoothIntensityImages[eLeftCamera];

            delete m_ppSmoothIntensityImages[eRightCamera];

            delete[] m_ppSmoothIntensityImages;

            m_ppSmoothIntensityImages = nullptr;
        }

        if (m_ppEdgeImages)
        {
            delete m_ppEdgeImages[eLeftCamera];

            delete m_ppEdgeImages[eRightCamera];

            delete[] m_ppEdgeImages;

            m_ppEdgeImages = nullptr;
        }

        if (m_ppFilteredEdgeImages)
        {
            delete m_ppFilteredEdgeImages[eLeftCamera];

            delete m_ppFilteredEdgeImages[eRightCamera];

            delete[] m_ppFilteredEdgeImages;

            m_ppFilteredEdgeImages = nullptr;
        }

        if (m_ppLowResolutionMaskImages)
        {
            delete m_ppLowResolutionMaskImages[eLeftCamera];

            delete m_ppLowResolutionMaskImages[eRightCamera];

            delete[] m_ppLowResolutionMaskImages;

            m_ppLowResolutionMaskImages = nullptr;
        }

        if (m_ppHighResolutionMaskImages)
        {
            delete m_ppHighResolutionMaskImages[eLeftCamera];

            delete m_ppHighResolutionMaskImages[eRightCamera];

            delete[] m_ppHighResolutionMaskImages;

            m_ppHighResolutionMaskImages = nullptr;
        }

        if (m_pHarrisPoints[eLeftCamera])
        {
            delete[] m_pHarrisPoints[eLeftCamera];

            m_pHarrisPoints[eLeftCamera] = nullptr;
        }

        if (m_pHarrisPoints[eRightCamera])
        {
            delete[] m_pHarrisPoints[eRightCamera];

            m_pHarrisPoints[eRightCamera] = nullptr;
        }

        if (m_pKernel)
        {
            delete[] m_pKernel;

            m_pKernel = nullptr;
        }

        if (m_pDescriptors[eLeftCamera])
        {
            delete[] m_pDescriptors[eLeftCamera];

            m_pDescriptors[eLeftCamera] = nullptr;
        }

        if (m_pDescriptors[eRightCamera])
        {
            delete[] m_pDescriptors[eRightCamera];

            m_pDescriptors[eRightCamera] = nullptr;
        }
    }

    bool CMobileKitchenHandleRecognizer::ProcessImages(const ParameterSet::ProcessImagesParameterSet& Parameters, const bool UsingImproveImageReconstruction)
    {
        if (!IsInitialized())
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (Parameters.m_CannyLowThreshold < _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_LOWER_CANNY_THRESHOLD_)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (Parameters.m_CannyLowThreshold > _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MAXIMAL_LOWER_CANNY_THRESHOLD_)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (Parameters.m_CannyHighThreshold < _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_UPPER_CANNY_THRESHOLD_)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (Parameters.m_CannyHighThreshold > _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MAXIMAL_UPPER_CANNY_THRESHOLD_)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (Parameters.m_CannyLowThreshold > Parameters.m_CannyHighThreshold)
        {
            _GENERIC_ERROR_;

            return false;
        }

        for (int i = 0; i < 2; ++i)
        {
            const MonocularSource Source = MonocularSource(i);

            const CByteImage* pImage = GetInternalInputImage(Source);

            if (!pImage)
            {
                _GENERIC_ERROR_;

                return false;
            }

            if (Parameters.m_SavePartialResults && IsSavingPartialResults())
            {
                const std::string FileName = CreateFileName(GetPartialResultsDirectory(), Source, "Source", "bmp", 0);

                if (!pImage->SaveToFile(FileName.c_str()))
                {
                    _GENERIC_ERROR_;

                    return false;
                }
            }

            if (!ImageProcessor::ConvertImage(pImage, m_ppIntensityImages[Source]))
            {
                _GENERIC_ERROR_;

                return false;
            }

            if (Parameters.m_SavePartialResults && IsSavingPartialResults())
            {
                const std::string FileName = CreateFileName(GetPartialResultsDirectory(), Source, "Intensity", "bmp", 0);

                if (!m_ppIntensityImages[Source]->SaveToFile(FileName.c_str()))
                {
                    _GENERIC_ERROR_;

                    return false;
                }
            }

            if (!ImageProcessor::GaussianSmooth3x3(m_ppIntensityImages[Source], m_ppSmoothIntensityImages[Source]))
            {
                _GENERIC_ERROR_;

                return false;
            }

            if (Parameters.m_SavePartialResults && IsSavingPartialResults())
            {
                const std::string FileName = CreateFileName(GetPartialResultsDirectory(), Source, "SmoothIntensity", "bmp", 0);

                if (!m_ppSmoothIntensityImages[Source]->SaveToFile(FileName.c_str()))
                {
                    _GENERIC_ERROR_;

                    return false;
                }
            }

            if (!ImageProcessor::Canny(UsingImproveImageReconstruction ? m_ppIntensityImages[Source] : m_ppSmoothIntensityImages[Source], m_ppEdgeImages[Source], Parameters.m_CannyLowThreshold, Parameters.m_CannyHighThreshold))
            {
                _GENERIC_ERROR_;

                return false;
            }

            if (Parameters.m_SavePartialResults && IsSavingPartialResults())
            {
                const std::string FileName = CreateFileName(GetPartialResultsDirectory(), Source, "Edges", "bmp", 0);

                if (!m_ppEdgeImages[Source]->SaveToFile(FileName.c_str()))
                {
                    _GENERIC_ERROR_;

                    return false;
                }
            }

            if (!FilterEdges(m_ppEdgeImages[Source], m_ppFilteredEdgeImages[Source], m_ppLowResolutionMaskImages[Source], m_ppHighResolutionMaskImages[Source], _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_LOW_RESOLUTION_MASK_FACTOR_, _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_LOW_RESOLUTION_MASK_MARGIN_, Parameters.m_MinimalBlockSize, Parameters.m_MinimalBlockDimension, Parameters.m_MaximalEigenRatio, Parameters.m_MaximalDeviation, GetInputActiveRegion(Source), &m_HarrisActiveZone[Source], Parameters.m_IsHorizontal))
            {
                _GENERIC_ERROR_;

                return false;
            }

            if (Parameters.m_SavePartialResults && IsSavingPartialResults())
            {
                const std::string FilteredEdgesFileName = CreateFileName(GetPartialResultsDirectory(), Source, "FilteredEdges", "bmp", 0);

                if (!m_ppFilteredEdgeImages[Source]->SaveToFile(FilteredEdgesFileName.c_str()))
                {
                    _GENERIC_ERROR_;

                    return false;
                }

                const std::string LowResolutionMaskFileName = CreateFileName(GetPartialResultsDirectory(), Source, "LowResolutionMask", "bmp", 0);

                if (!m_ppLowResolutionMaskImages[Source]->SaveToFile(LowResolutionMaskFileName.c_str()))
                {
                    _GENERIC_ERROR_;

                    return false;
                }

                const std::string HighResolutionMaskFileName = CreateFileName(GetPartialResultsDirectory(), Source, "HighResolution", "bmp", 0);

                if (!m_ppHighResolutionMaskImages[Source]->SaveToFile(HighResolutionMaskFileName.c_str()))
                {
                    _GENERIC_ERROR_;

                    return false;
                }
            }
        }

        return true;
    }

    bool CMobileKitchenHandleRecognizer::ProcessPoints(const ParameterSet::ProcessPointsParameterSet& Parameters)
    {
        if (Parameters.m_HarrisSaliency[eLeftCamera] < FLT_EPSILON)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (Parameters.m_HarrisSaliency[eLeftCamera] > (1.0f - FLT_EPSILON))
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (Parameters.m_HarrisSaliency[eRightCamera] < FLT_EPSILON)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (Parameters.m_HarrisSaliency[eRightCamera] > (1.0f - FLT_EPSILON))
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (Parameters.m_HarrisDominanceDistance[eLeftCamera] < 0.0f)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (Parameters.m_HarrisDominanceDistance[eLeftCamera] > 20.0f)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (Parameters.m_HarrisDominanceDistance[eRightCamera] < 0.0f)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (Parameters.m_HarrisDominanceDistance[eRightCamera] > 20.0f)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!IsInitialized())
        {
            _GENERIC_ERROR_;

            return false;
        }

        const CCalibration* pLeftCalibration = GetMonocularCalibrations(eLeftCamera);

        if (!pLeftCalibration)
        {
            _GENERIC_ERROR_;

            return false;
        }

        const CCalibration* pRighCalibration = GetMonocularCalibrations(eRightCamera);

        if (!pRighCalibration)
        {
            _GENERIC_ERROR_;

            return false;
        }

        CStereoCalibration* pStereoCalibration = GetStereoCalibration();

        if (!pStereoCalibration)
        {
            _GENERIC_ERROR_;

            return false;
        }

        for (int i = 0; i < 2; ++i)
        {
            const MonocularSource Source = MonocularSource(i);

            const BoundingBox2D* pInputActiveRegion = GetInputActiveRegion(Source);

            if (!pInputActiveRegion)
            {
                _GENERIC_ERROR_;

                return false;
            }

            if (pInputActiveRegion->IsEmpty())
            {
                _GENERIC_ERROR_;

                return false;
            }

            if (m_HarrisActiveZone[Source].IsEmpty())
            {
                _GENERIC_ERROR_;

                return false;
            }

            const BoundingBox2D& CommonZone = pInputActiveRegion->Intersect(m_HarrisActiveZone[Source]);

            CByteImage ActiveZoneImage(CommonZone.GetWidth(), CommonZone.GetHeight(), CByteImage::eGrayScale);

            MyRegion CurrentRegion;

            CurrentRegion.min_x = CommonZone.m_X0;

            CurrentRegion.min_y = CommonZone.m_Y0;

            CurrentRegion.max_x = CommonZone.m_X1;

            CurrentRegion.max_y = CommonZone.m_Y1;

            if (!ImageProcessor::CopyImage(m_ppSmoothIntensityImages[Source], &ActiveZoneImage, &CurrentRegion, false))
            {
                _GENERIC_ERROR_;

                return false;
            }

            const int TotalExtractedHarrisPoints = ImageProcessor::CalculateHarrisInterestPoints(&ActiveZoneImage, m_pHarrisPoints[Source], m_TotalAvailableHarrisPointsPerMonocularSource, Parameters.m_HarrisSaliency[Source], Parameters.m_HarrisDominanceDistance[Source]);

            Vec2d* pInputPoint = m_pHarrisPoints[Source];

            const int Width = m_ppHighResolutionMaskImages[Source]->width;

            m_ActiveHarrisPoints[Source].clear();

            for (int i = 0; i < TotalExtractedHarrisPoints; ++i, ++pInputPoint)
            {
                if (m_ppHighResolutionMaskImages[Source]->pixels[(Width * (int(pInputPoint->y) + CurrentRegion.min_y)) + (int(pInputPoint->x) + CurrentRegion.min_x)])
                {
                    pInputPoint->x += CurrentRegion.min_x;

                    pInputPoint->y += CurrentRegion.min_y;

                    m_ActiveHarrisPoints[Source].push_back(*pInputPoint);
                }
            }

            stable_sort(m_ActiveHarrisPoints[Source].begin(), m_ActiveHarrisPoints[Source].end(), SortPredicateHarrisPoint);

            if (IsSavingPartialResults())
            {
                CByteImage* pInternalOutputImage = GetInternalOutputImage(Source);

                if (!pInternalOutputImage)
                {
                    _GENERIC_ERROR_;

                    return false;
                }

                if (!ImageProcessor::ConvertImage(m_ppHighResolutionMaskImages[Source], pInternalOutputImage))
                {
                    _GENERIC_ERROR_;

                    return false;
                }

                const int TotalActiveHarrisPoints = m_ActiveHarrisPoints[Source].size();

                for (int i = 0; i < TotalActiveHarrisPoints; ++i)
                {
                    PrimitivesDrawer::DrawPoint(pInternalOutputImage, m_ActiveHarrisPoints[Source][i], 255, 0, 0);
                }

                const std::string FileName = CreateFileName(GetPartialResultsDirectory(), Source, "HarrisPoints", "bmp", 0);

                if (!pInternalOutputImage->SaveToFile(FileName.c_str()))
                {
                    _GENERIC_ERROR_;

                    return false;
                }
            }
        }

#ifdef _VISUAL_DEBUG_
        //============================================================================================================================================================

        CByteImage* pLeftInternalOutputImage = GetInternalOutputImage(eLeftCamera);

        CByteImage* pRightInternalOutputImage = GetInternalOutputImage(eRightCamera);

        const std::string LeftFileName = CreateFileName(GetPartialResultsDirectory(), eLeftCamera, "Debug", "bmp", 0);

        const std::string RightFileName = CreateFileName(GetPartialResultsDirectory(), eRightCamera, "Debug", "bmp", 0);

        //============================================================================================================================================================
#endif

        m_StereoTriangulationPoints.clear();

        StereoTriangulationPoint CurrentPoint;

        PointPair2d EpipolarLineOnRightImage;

        Vec3d CloseIdealSpacePoint, FarIdealSpacePoint;

        Vec2d MaximalPseudoDisparityIdealImagePoint, MinimalPseudoDisparityIdealImagePoint, EpipolarDirection, EpipolarNormal, MinimalDelta, MaximalDelta, CurrentDelta, PerpendicularEpipolarOffset, EpipolarRestrictionPoints[4];

        const int TotalLeftActiveHarrisPoints = m_ActiveHarrisPoints[eLeftCamera].size();

        const int SubTotalRightActiveHarrisPoints = m_ActiveHarrisPoints[eRightCamera].size() - 1;

        const BoundingBox2D* pRightInputActiveRegion = GetInputActiveRegion(eRightCamera);

        for (int i = 0; i < TotalLeftActiveHarrisPoints; ++i)
        {
            CurrentPoint.m_LeftImagePoint = m_ActiveHarrisPoints[eLeftCamera][i];

            pLeftCalibration->ImageToWorldCoordinates(CurrentPoint.m_LeftImagePoint, CloseIdealSpacePoint, Parameters.m_MinimalSearchDepth, false);

            pLeftCalibration->ImageToWorldCoordinates(CurrentPoint.m_LeftImagePoint, FarIdealSpacePoint, Parameters.m_MaximalSearchDepth, false);

            pRighCalibration->WorldToImageCoordinates(CloseIdealSpacePoint, MaximalPseudoDisparityIdealImagePoint, false);

            pRighCalibration->WorldToImageCoordinates(FarIdealSpacePoint, MinimalPseudoDisparityIdealImagePoint, false);

            pStereoCalibration->CalculateEpipolarLineInRightImage(CurrentPoint.m_LeftImagePoint, EpipolarLineOnRightImage);

            Math2d::SubtractVecVec(EpipolarLineOnRightImage.p2, EpipolarLineOnRightImage.p1, EpipolarDirection);

            Math2d::NormalizeVec(EpipolarDirection);

            Math2d::RotateVec(EpipolarDirection, float(-M_PI_2), EpipolarNormal);

            Math2d::SubtractVecVec(MaximalPseudoDisparityIdealImagePoint, EpipolarLineOnRightImage.p1, MinimalDelta);

            Math2d::SubtractVecVec(MinimalPseudoDisparityIdealImagePoint, EpipolarLineOnRightImage.p1, MaximalDelta);

            const float MinimalPseudoDisparity = _DOT_PRODUCT_2D_(MinimalDelta, EpipolarDirection);

            const float MaximalPseudoDisparity = _DOT_PRODUCT_2D_(MaximalDelta, EpipolarDirection);

            Math2d::MulVecScalar(EpipolarNormal, Parameters.m_MaximalEpipolarDeviation, PerpendicularEpipolarOffset);

            Math2d::AddVecVec(MaximalPseudoDisparityIdealImagePoint, PerpendicularEpipolarOffset, EpipolarRestrictionPoints[0]);

            Math2d::AddVecVec(MinimalPseudoDisparityIdealImagePoint, PerpendicularEpipolarOffset, EpipolarRestrictionPoints[1]);

            Math2d::SubtractVecVec(MinimalPseudoDisparityIdealImagePoint, PerpendicularEpipolarOffset, EpipolarRestrictionPoints[2]);

            Math2d::SubtractVecVec(MaximalPseudoDisparityIdealImagePoint, PerpendicularEpipolarOffset, EpipolarRestrictionPoints[3]);

            const float X0 = std::max(float(pRightInputActiveRegion->m_X0), std::floor(std::min(_MIN_(EpipolarRestrictionPoints[0].x, EpipolarRestrictionPoints[1].x), _MIN_(EpipolarRestrictionPoints[2].x, EpipolarRestrictionPoints[3].x))));

            const float Y0 = std::max(float(pRightInputActiveRegion->m_Y0), std::floor(std::min(_MIN_(EpipolarRestrictionPoints[0].y, EpipolarRestrictionPoints[1].y), _MIN_(EpipolarRestrictionPoints[2].y, EpipolarRestrictionPoints[3].y))));

            const float X1 = std::min(float(pRightInputActiveRegion->m_X1), std::ceil(std::max(_MAX_(EpipolarRestrictionPoints[0].x, EpipolarRestrictionPoints[1].x), _MAX_(EpipolarRestrictionPoints[2].x, EpipolarRestrictionPoints[3].x))));

            const float Y1 = std::min(float(pRightInputActiveRegion->m_Y1), std::ceil(std::max(_MAX_(EpipolarRestrictionPoints[0].y, EpipolarRestrictionPoints[1].y), _MAX_(EpipolarRestrictionPoints[2].y, EpipolarRestrictionPoints[3].y))));

            StereoTriangulationPoint Match;

            memset(&Match, 0, sizeof(StereoTriangulationPoint));

#ifdef _VISUAL_DEBUG_
            //============================================================================================================================================================

            const bool Update = (CurrentPoint.m_LeftImagePoint.x > 275) && (CurrentPoint.m_LeftImagePoint.x < 325) && (CurrentPoint.m_LeftImagePoint.y > 270) && (CurrentPoint.m_LeftImagePoint.y < 320);

            if (Update)
            {
                ImageProcessor::ConvertImage(m_ppIntensityImages[eLeftCamera], pLeftInternalOutputImage);

                ImageProcessor::ConvertImage(m_ppIntensityImages[eRightCamera], pRightInternalOutputImage);

                PrimitivesDrawer::DrawCross(pLeftInternalOutputImage, CurrentPoint.m_LeftImagePoint, 2, 255, 0, 0);

                PrimitivesDrawer::DrawCircle(pLeftInternalOutputImage, CurrentPoint.m_LeftImagePoint, 4, 255, 0, 0, 1, true);

                PrimitivesDrawer::DrawCircle(pLeftInternalOutputImage, CurrentPoint.m_LeftImagePoint, 8, 255, 0, 0, 1, true);

                PrimitivesDrawer::DrawCircle(pLeftInternalOutputImage, CurrentPoint.m_LeftImagePoint, 16, 255, 0, 0, 1, true);

                PrimitivesDrawer::DrawCross(pRightInternalOutputImage, CurrentPoint.m_LeftImagePoint, 2, 255, 255, 0);

                PrimitivesDrawer::DrawCircle(pRightInternalOutputImage, CurrentPoint.m_LeftImagePoint, 4, 255, 255, 0, 1, true);

                PrimitivesDrawer::DrawCircle(pRightInternalOutputImage, CurrentPoint.m_LeftImagePoint, 8, 255, 255, 0, 1, true);

                PrimitivesDrawer::DrawCircle(pRightInternalOutputImage, CurrentPoint.m_LeftImagePoint, 16, 255, 255, 0, 1, true);

                PrimitivesDrawer::DrawCircle(pRightInternalOutputImage, MaximalPseudoDisparityIdealImagePoint, 2, 255, 0, 255, 1, true);

                PrimitivesDrawer::DrawCircle(pRightInternalOutputImage, MaximalPseudoDisparityIdealImagePoint, 4, 255, 0, 255, 1, true);

                PrimitivesDrawer::DrawCircle(pRightInternalOutputImage, MinimalPseudoDisparityIdealImagePoint, 2, 0, 255, 255, 1, true);

                PrimitivesDrawer::DrawCircle(pRightInternalOutputImage, MinimalPseudoDisparityIdealImagePoint, 4, 0, 255, 255, 1, true);

                for (int j = 0; j < 4; ++j)
                {
                    PrimitivesDrawer::DrawLine(pRightInternalOutputImage, EpipolarRestrictionPoints[j], EpipolarRestrictionPoints[(j + 1) % 4], 0, 0, 0, 1);
                }

                PointPair2d Line[4];

                Line[0].p1.x = X0 - 1, Line[0].p1.y = Y0 - 1, Line[0].p2.x = X1 + 1, Line[0].p2.y = Y0 - 1;

                Line[1].p1.x = X0 - 1, Line[1].p1.y = Y1 + 1, Line[1].p2.x = X1 + 1, Line[1].p2.y = Y1 + 1;

                Line[2].p1.x = X0 - 1, Line[2].p1.y = Y0 - 1, Line[2].p2.x = X0 - 1, Line[2].p2.y = Y1 + 1,

                       Line[3].p1.x = X1 + 1, Line[3].p1.y = Y0 - 1, Line[3].p2.x = X1 + 1, Line[3].p2.y = Y1 + 1;

                for (int j = 0; j < 4; ++j)
                {
                    PrimitivesDrawer::DrawLine(pRightInternalOutputImage, Line[j], 0, 255, 0, 1);
                }

                PrimitivesDrawer::DrawLine(pRightInternalOutputImage, EpipolarLineOnRightImage, 0, 0, 255, 1);

                pLeftInternalOutputImage->SaveToFile(LeftFileName.c_str());

                pRightInternalOutputImage->SaveToFile(RightFileName.c_str());

                ImageProcessor::CopyImage(pRightInternalOutputImage, pLeftInternalOutputImage);

            }

            //============================================================================================================================================================
#endif

            const int j0 = GetOrganizedIndex(0, SubTotalRightActiveHarrisPoints, X0, m_ActiveHarrisPoints[eRightCamera]);

            const int j1 = GetOrganizedIndex(j0, SubTotalRightActiveHarrisPoints, X1, m_ActiveHarrisPoints[eRightCamera]);

            for (int j = j0; j <= j1; ++j)
            {
                CurrentPoint.m_RightImagePoint = m_ActiveHarrisPoints[eRightCamera][j];

                if ((CurrentPoint.m_RightImagePoint.y >= Y0) && (CurrentPoint.m_RightImagePoint.y <= Y1))
                {
                    Math2d::SubtractVecVec(CurrentPoint.m_RightImagePoint, EpipolarLineOnRightImage.p1, CurrentDelta);

                    CurrentPoint.m_EpipolarDeviation = fabs(_DOT_PRODUCT_2D_(CurrentDelta, EpipolarNormal));

                    if (CurrentPoint.m_EpipolarDeviation <= Parameters.m_MaximalEpipolarDeviation)
                    {
                        const float PseudoDisparity = _DOT_PRODUCT_2D_(CurrentDelta, EpipolarDirection);

                        if ((PseudoDisparity >= MinimalPseudoDisparity) && (PseudoDisparity <= MaximalPseudoDisparity))
                        {
                            pStereoCalibration->Calculate3DPoint(CurrentPoint.m_LeftImagePoint, CurrentPoint.m_RightImagePoint, CurrentPoint.m_SpacePoint, false, false, &CurrentPoint.m_EstimatedPointsLine);

                            if ((CurrentPoint.m_SpacePoint.z >= Parameters.m_MinimalSearchDepth) && (CurrentPoint.m_SpacePoint.z <= Parameters.m_MaximalSearchDepth))
                            {
                                CurrentPoint.m_TriangulationAbsoluteDeviation = Math3d::Distance(CurrentPoint.m_EstimatedPointsLine.p1, CurrentPoint.m_EstimatedPointsLine.p2);

                                if (CurrentPoint.m_TriangulationAbsoluteDeviation <= Parameters.m_MaximalTriangulationDeviation)
                                {
                                    if (NormalizedCrossCorrelationIntensity(CurrentPoint.m_Similarity, m_ppSmoothIntensityImages[eLeftCamera], m_ppSmoothIntensityImages[eRightCamera], CurrentPoint.m_LeftImagePoint, CurrentPoint.m_RightImagePoint, m_KernelRadius, m_pKernel, m_pDescriptors[eLeftCamera], m_pDescriptors[eRightCamera], m_KernelIntegral))
                                    {
                                        if (CurrentPoint.m_Similarity >= Parameters.m_MinimalPatchSimilarity)
                                        {
                                            if (CurrentPoint.m_Similarity > Match.m_Similarity)
                                            {
                                                Match = CurrentPoint;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (Match.m_SpacePoint.z)
            {
                Match.m_Id = m_StereoTriangulationPoints.size();

                m_StereoTriangulationPoints.push_back(Match);

#ifdef _VISUAL_DEBUG_
                //============================================================================================================================================================

                if (Update)
                {

                    ImageProcessor::CopyImage(pLeftInternalOutputImage, pRightInternalOutputImage);

                    PrimitivesDrawer::DrawCross(pRightInternalOutputImage, Match.m_RightImagePoint, 2, 255, 0, 0);

                    PrimitivesDrawer::DrawCircle(pRightInternalOutputImage, Match.m_RightImagePoint, 4, 255, 0, 0, 1, true);

                    PrimitivesDrawer::DrawCircle(pRightInternalOutputImage, Match.m_RightImagePoint, 8, 255, 0, 0, 1, true);

                    PrimitivesDrawer::DrawCircle(pRightInternalOutputImage, Match.m_RightImagePoint, 16, 255, 0, 0, 1, true);

                    pRightInternalOutputImage->SaveToFile(RightFileName.c_str());

                }

                //============================================================================================================================================================
#endif
            }
        }

        if (m_StereoTriangulationPoints.size() < 2)
        {
            return true;
        }

        if (Parameters.m_SavePartialResults && IsSavingPartialResults())
        {
            CByteImage* pLeftInternalOutputImage = GetInternalOutputImage(eLeftCamera);

            if (!pLeftInternalOutputImage)
            {
                _GENERIC_ERROR_;

                return false;
            }

            if (!ImageProcessor::ConvertImage(m_ppIntensityImages[eLeftCamera], pLeftInternalOutputImage))
            {
                _GENERIC_ERROR_;

                return false;
            }

            CByteImage* pRightInternalOutputImage = GetInternalOutputImage(eRightCamera);

            if (!pRightInternalOutputImage)
            {
                _GENERIC_ERROR_;

                return false;
            }

            if (!ImageProcessor::ConvertImage(m_ppIntensityImages[eRightCamera], pRightInternalOutputImage))
            {
                _GENERIC_ERROR_;

                return false;
            }

            std::deque<StereoTriangulationPoint>::iterator TriangulationPointsEnd = m_StereoTriangulationPoints.end();

            for (std::deque<StereoTriangulationPoint>::iterator pTriangulationPoint = m_StereoTriangulationPoints.begin(); pTriangulationPoint != TriangulationPointsEnd; ++pTriangulationPoint)
            {
                PrimitivesDrawer::DrawPoint(pLeftInternalOutputImage, pTriangulationPoint->m_LeftImagePoint, 0, 0, 255);

                PrimitivesDrawer::DrawPoint(pRightInternalOutputImage, pTriangulationPoint->m_RightImagePoint, 0, 0, 255);
            }

            const std::string LeftFileName = CreateFileName(GetPartialResultsDirectory(), eLeftCamera, "MatchedPoints", "bmp", 0);

            if (!pLeftInternalOutputImage->SaveToFile(LeftFileName.c_str()))
            {
                _GENERIC_ERROR_;

                return false;
            }

            const std::string RightFileName = CreateFileName(GetPartialResultsDirectory(), eRightCamera, "MatchedPoints", "bmp", 0);

            if (!pRightInternalOutputImage->SaveToFile(RightFileName.c_str()))
            {
                _GENERIC_ERROR_;

                return false;
            }
        }

        return true;
    }

    bool CMobileKitchenHandleRecognizer::ProcessLineSegments(const ParameterSet::ExtractLinesParameterSet& Parameters)
    {
        m_SpatialLineSegments.clear();

        SpaceLineSegment CurrentSegment;

        std::deque<StereoTriangulationPoint>::iterator PointsEnd = m_StereoTriangulationPoints.end();

        for (std::deque<StereoTriangulationPoint>::iterator pPointA = m_StereoTriangulationPoints.begin() ; pPointA != PointsEnd ; ++pPointA)
        {
            CurrentSegment.m_PointA = *pPointA;

            std::deque<StereoTriangulationPoint>::iterator pPointB = pPointA;

            for (++pPointB; pPointB != PointsEnd ; ++pPointB)
            {
                CurrentSegment.m_PointB = *pPointB;

                Math2d::SubtractVecVec(CurrentSegment.m_PointB.m_LeftImagePoint, CurrentSegment.m_PointA.m_LeftImagePoint, CurrentSegment.m_ImageDirection[eLeftCamera]);

                if (Parameters.m_IsHorizontal ? fabs(CurrentSegment.m_ImageDirection[eLeftCamera].x) > fabs(CurrentSegment.m_ImageDirection[eLeftCamera].y) : fabs(CurrentSegment.m_ImageDirection[eLeftCamera].y) > fabs(CurrentSegment.m_ImageDirection[eLeftCamera].x))
                {
                    Math2d::SubtractVecVec(CurrentSegment.m_PointB.m_RightImagePoint, CurrentSegment.m_PointA.m_RightImagePoint, CurrentSegment.m_ImageDirection[eRightCamera]);

                    if (Parameters.m_IsHorizontal ? fabs(CurrentSegment.m_ImageDirection[eRightCamera].x) > fabs(CurrentSegment.m_ImageDirection[eRightCamera].y) : fabs(CurrentSegment.m_ImageDirection[eRightCamera].y) > fabs(CurrentSegment.m_ImageDirection[eRightCamera].x))
                    {
                        CurrentSegment.m_LeftImageLength = Math2d::Length(CurrentSegment.m_ImageDirection[eLeftCamera]);

                        if ((CurrentSegment.m_LeftImageLength >= Parameters.m_ImageMinimalLength) && (CurrentSegment.m_LeftImageLength <= Parameters.m_ImageMaximalLength))
                        {
                            CurrentSegment.m_RightImageLength = Math2d::Length(CurrentSegment.m_ImageDirection[eRightCamera]);

                            if ((CurrentSegment.m_RightImageLength >= Parameters.m_ImageMinimalLength) && (CurrentSegment.m_RightImageLength <= Parameters.m_ImageMaximalLength))
                            {
                                if ((_MIN_(CurrentSegment.m_LeftImageLength, CurrentSegment.m_RightImageLength) / _MAX_(CurrentSegment.m_LeftImageLength, CurrentSegment.m_RightImageLength)) >= Parameters.m_MinimalImageMaximalLengthRatio)
                                {
                                    CurrentSegment.m_ImageOrientationCoherence = fabs(_DOT_PRODUCT_2D_(CurrentSegment.m_ImageDirection[eLeftCamera], CurrentSegment.m_ImageDirection[eRightCamera]) / (CurrentSegment.m_LeftImageLength * CurrentSegment.m_RightImageLength));

                                    if (CurrentSegment.m_ImageOrientationCoherence >= Parameters.m_MinimalImageOrientationCoherence)
                                    {
                                        CurrentSegment.m_SpaceLength = Math3d::Distance(CurrentSegment.m_PointA.m_SpacePoint, CurrentSegment.m_PointB.m_SpacePoint);

                                        CurrentSegment.m_UncertaintyRadiusA = std::max(Math3d::Distance(CurrentSegment.m_PointA.m_EstimatedPointsLine.p1, CurrentSegment.m_PointA.m_SpacePoint), Math3d::Distance(CurrentSegment.m_PointA.m_EstimatedPointsLine.p2, CurrentSegment.m_PointA.m_SpacePoint));

                                        CurrentSegment.m_UncertaintyRadiusB = std::max(Math3d::Distance(CurrentSegment.m_PointB.m_EstimatedPointsLine.p1, CurrentSegment.m_PointB.m_SpacePoint), Math3d::Distance(CurrentSegment.m_PointB.m_EstimatedPointsLine.p2, CurrentSegment.m_PointB.m_SpacePoint));

                                        CurrentSegment.m_MaxmimalSpaceLength = CurrentSegment.m_SpaceLength + (CurrentSegment.m_UncertaintyRadiusA + CurrentSegment.m_UncertaintyRadiusB);

                                        if (CurrentSegment.m_MaxmimalSpaceLength >= Parameters.m_SpaceMinimalLength)
                                        {
                                            CurrentSegment.m_MinimalSpaceLength = CurrentSegment.m_SpaceLength - (CurrentSegment.m_UncertaintyRadiusA + CurrentSegment.m_UncertaintyRadiusB);

                                            if (CurrentSegment.m_MinimalSpaceLength <= Parameters.m_SpaceMaximalLength)
                                            {
                                                if (IsLineSegmentInMaskImage(m_ppHighResolutionMaskImages[eLeftCamera], CurrentSegment.m_PointA.m_LeftImagePoint, CurrentSegment.m_PointB.m_LeftImagePoint, Parameters.m_EdgeMargin))
                                                {
                                                    if (IsLineSegmentInMaskImage(m_ppHighResolutionMaskImages[eRightCamera], CurrentSegment.m_PointA.m_RightImagePoint, CurrentSegment.m_PointB.m_RightImagePoint, Parameters.m_EdgeMargin))
                                                    {
                                                        if (GetTotalPointsAlongLineSegment(CurrentSegment.m_PointA.m_LeftImagePoint, CurrentSegment.m_PointB.m_LeftImagePoint, Parameters.m_PointsAlongLineSegmentMargin, m_ActiveHarrisPoints[eLeftCamera]) <= Parameters.m_MaximalPointsAlongLineSegment)
                                                        {
                                                            if (GetTotalPointsAlongLineSegment(CurrentSegment.m_PointA.m_RightImagePoint, CurrentSegment.m_PointB.m_RightImagePoint, Parameters.m_PointsAlongLineSegmentMargin, m_ActiveHarrisPoints[eRightCamera]) <= Parameters.m_MaximalPointsAlongLineSegment)
                                                            {
                                                                Math2d::MulVecScalar(CurrentSegment.m_ImageDirection[eLeftCamera], 1.0f / CurrentSegment.m_LeftImageLength, CurrentSegment.m_ImageDirection[eLeftCamera]);

                                                                Math2d::MulVecScalar(CurrentSegment.m_ImageDirection[eRightCamera], 1.0f / CurrentSegment.m_RightImageLength, CurrentSegment.m_ImageDirection[eRightCamera]);

                                                                Math2d::Average(CurrentSegment.m_PointA.m_LeftImagePoint, CurrentSegment.m_PointB.m_LeftImagePoint, CurrentSegment.m_ImageCentralPoint[eLeftCamera]);

                                                                Math2d::Average(CurrentSegment.m_PointA.m_RightImagePoint, CurrentSegment.m_PointB.m_RightImagePoint, CurrentSegment.m_ImageCentralPoint[eRightCamera]);

                                                                Math3d::Average(CurrentSegment.m_PointA.m_SpacePoint, CurrentSegment.m_PointB.m_SpacePoint, CurrentSegment.m_SpaceCentralPoint);

                                                                Math3d::SubtractVecVec(CurrentSegment.m_PointB.m_SpacePoint, CurrentSegment.m_PointA.m_SpacePoint, CurrentSegment.m_SpaceDirection);

                                                                Math3d::NormalizeVec(CurrentSegment.m_SpaceDirection);

                                                                CurrentSegment.m_CentralUncertaintyRadius = _MAX_(CurrentSegment.m_UncertaintyRadiusA, CurrentSegment.m_UncertaintyRadiusB);

                                                                CurrentSegment.m_Id = m_SpatialLineSegments.size();

                                                                m_SpatialLineSegments.push_back(CurrentSegment);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!m_SpatialLineSegments.size())
        {
            return true;
        }

        if (Parameters.m_SavePartialResults && IsSavingPartialResults())
        {
            CByteImage* pLeftInternalOutputImage = GetInternalOutputImage(eLeftCamera);

            if (!pLeftInternalOutputImage)
            {
                _GENERIC_ERROR_;

                return false;
            }

            if (!ImageProcessor::ConvertImage(m_ppIntensityImages[eLeftCamera], pLeftInternalOutputImage))
            {
                _GENERIC_ERROR_;

                return false;
            }

            CByteImage* pRightInternalOutputImage = GetInternalOutputImage(eRightCamera);

            if (!pRightInternalOutputImage)
            {
                _GENERIC_ERROR_;

                return false;
            }

            if (!ImageProcessor::ConvertImage(m_ppIntensityImages[eRightCamera], pRightInternalOutputImage))
            {
                _GENERIC_ERROR_;

                return false;
            }

            std::vector<SpaceLineSegment>::iterator EndLineSegments = m_SpatialLineSegments.end();

            for (std::vector<SpaceLineSegment>::iterator pSpaceLineSegment = m_SpatialLineSegments.begin() ; pSpaceLineSegment != EndLineSegments ; ++pSpaceLineSegment)
            {
                PrimitivesDrawer::DrawLine(pLeftInternalOutputImage, pSpaceLineSegment->m_PointA.m_LeftImagePoint, pSpaceLineSegment->m_PointB.m_LeftImagePoint, 0, 255, 0, 1);

                PrimitivesDrawer::DrawLine(pRightInternalOutputImage, pSpaceLineSegment->m_PointA.m_RightImagePoint, pSpaceLineSegment->m_PointB.m_RightImagePoint, 0, 255, 0, 1);
            }

            const std::string LeftFileName = CreateFileName(GetPartialResultsDirectory(), eLeftCamera, "LineSegments", "bmp", 0);

            if (!pLeftInternalOutputImage->SaveToFile(LeftFileName.c_str()))
            {
                _GENERIC_ERROR_;

                return false;
            }

            const std::string RightFileName = CreateFileName(GetPartialResultsDirectory(), eRightCamera, "LineSegments", "bmp", 0);

            if (!pRightInternalOutputImage->SaveToFile(RightFileName.c_str()))
            {
                _GENERIC_ERROR_;

                return false;
            }
        }

        return true;
    }

    struct Linkage
    {
        struct Link
        {
            inline Link(const int Id, const float Distance) :
                m_Id(Id),
                m_Distance(Distance)
            {
            }

            int m_Id;

            float m_Distance;
        };

        static bool SortPredicateBestLinkage(const Linkage* lhs, const Linkage* rhs)
        {
            if (lhs->m_Links.size() == rhs->m_Links.size())
            {
                if (lhs->m_TotalDistance == rhs->m_TotalDistance)
                {
                    if (lhs->m_TotalOrientationDeviation == rhs->m_TotalOrientationDeviation)
                    {
                        return lhs->m_TotalSpatialUncertainty < rhs->m_TotalSpatialUncertainty;
                    }

                    return lhs->m_TotalOrientationDeviation < rhs->m_TotalOrientationDeviation;
                }

                return lhs->m_TotalDistance < rhs->m_TotalDistance;
            }

            return lhs->m_Links.size() > rhs->m_Links.size();
        }

        inline Linkage(const int Id) :
            m_Active(true),
            m_Id(Id),
            m_TotalDistance(0.0f),
            m_TotalOrientationDeviation(0.0f),
            m_TotalSpatialUncertainty(0.0f),
            m_Links()
        {
        }

        inline void AddLink(const int Id, const float Distance, const float OrientationDeviation, const float SpatialUncertainty)
        {
            m_Links.push_back(Link(Id, Distance));

            m_TotalDistance += Distance;

            m_TotalOrientationDeviation += OrientationDeviation;

            m_TotalSpatialUncertainty += SpatialUncertainty;
        }

        bool m_Active;

        int m_Id;

        float m_TotalDistance;

        float m_TotalOrientationDeviation;

        float m_TotalSpatialUncertainty;

        std::deque<Link> m_Links;
    };

    bool CMobileKitchenHandleRecognizer::ProcessModelDescriptionMatching(const ParameterSet::MatchingParameterSet& Parameters)
    {
        m_MobileKitchenHandle.clear();

        if (!m_SpatialLineSegments.size())
        {
            return true;
        }

        std::vector<Linkage*> SequentialLinking;

        std::vector<Linkage*> ConnectivitySortedLinking;

        std::vector<SpaceLineSegment>::iterator EndLineSegments = m_SpatialLineSegments.end();

        for (std::vector<SpaceLineSegment>::iterator pSpaceLineSegmentA = m_SpatialLineSegments.begin() ; pSpaceLineSegmentA != EndLineSegments ; ++pSpaceLineSegmentA)
        {
            try
            {
                Linkage* pLinkage = new Linkage(pSpaceLineSegmentA->m_Id);

                SequentialLinking.push_back(pLinkage);

                ConnectivitySortedLinking.push_back(pLinkage);
            }
            catch (std::exception& /*Exception*/)
            {
                _GENERIC_ERROR_;

                for (std::vector<Linkage*>::iterator pLinkage = ConnectivitySortedLinking.begin(); pLinkage != ConnectivitySortedLinking.end(); ++pLinkage)
                {
                    delete *pLinkage;
                }

                return false;
            }
        }

        for (std::vector<SpaceLineSegment>::iterator pSpaceLineSegmentA = m_SpatialLineSegments.begin() ; pSpaceLineSegmentA != EndLineSegments ; ++pSpaceLineSegmentA)
        {
            std::vector<SpaceLineSegment>::iterator pSpaceLineSegmentB = pSpaceLineSegmentA;

            for (++pSpaceLineSegmentB; pSpaceLineSegmentB != EndLineSegments ; ++pSpaceLineSegmentB)
            {
                const float AL = Math2d::Distance(pSpaceLineSegmentA->m_PointA.m_LeftImagePoint, pSpaceLineSegmentB->m_PointA.m_LeftImagePoint);

                if (AL <= Parameters.m_ClusteringImageBandwidth)
                {
                    const float BL = Math2d::Distance(pSpaceLineSegmentA->m_PointB.m_LeftImagePoint, pSpaceLineSegmentB->m_PointB.m_LeftImagePoint);

                    if (BL <= Parameters.m_ClusteringImageBandwidth)
                    {
                        const float AR = Math2d::Distance(pSpaceLineSegmentA->m_PointA.m_RightImagePoint, pSpaceLineSegmentB->m_PointA.m_RightImagePoint);

                        if (AR <= Parameters.m_ClusteringImageBandwidth)
                        {
                            const float BR = Math2d::Distance(pSpaceLineSegmentA->m_PointB.m_RightImagePoint, pSpaceLineSegmentB->m_PointB.m_RightImagePoint);

                            if (BR <= Parameters.m_ClusteringImageBandwidth)
                            {
                                const float LeftImageDistance = Math2d::Distance(pSpaceLineSegmentA->m_ImageCentralPoint[eLeftCamera], pSpaceLineSegmentB->m_ImageCentralPoint[eLeftCamera]);

                                if (LeftImageDistance <= Parameters.m_ClusteringImageBandwidth)
                                {
                                    const float RightImageDistance = Math2d::Distance(pSpaceLineSegmentA->m_ImageCentralPoint[eRightCamera], pSpaceLineSegmentB->m_ImageCentralPoint[eRightCamera]);

                                    if (RightImageDistance <= Parameters.m_ClusteringImageBandwidth)
                                    {
                                        const float SpaceOrientationCoherence = fabs(_DOT_PRODUCT_3D_(pSpaceLineSegmentA->m_SpaceDirection, pSpaceLineSegmentB->m_SpaceDirection));

                                        if (SpaceOrientationCoherence >= Parameters.m_MinimalSpaceOrientationCoherence)
                                        {
                                            const float SpaceDistance = Math3d::Distance(pSpaceLineSegmentA->m_SpaceCentralPoint, pSpaceLineSegmentB->m_SpaceCentralPoint);

                                            if (SpaceDistance <= std::max(Parameters.m_ClusteringSpatialBandwidth, pSpaceLineSegmentA->m_CentralUncertaintyRadius + pSpaceLineSegmentB->m_CentralUncertaintyRadius))
                                            {
                                                const float OrientationDeviation = 1.0f - SpaceOrientationCoherence;

                                                const float SpatialUncertainty = pSpaceLineSegmentA->m_CentralUncertaintyRadius + pSpaceLineSegmentB->m_CentralUncertaintyRadius;

                                                SequentialLinking[pSpaceLineSegmentA->m_Id]->AddLink(pSpaceLineSegmentB->m_Id, SpaceDistance, OrientationDeviation, SpatialUncertainty);

                                                SequentialLinking[pSpaceLineSegmentB->m_Id]->AddLink(pSpaceLineSegmentA->m_Id, SpaceDistance, OrientationDeviation, SpatialUncertainty);

#ifdef _VISUAL_DEBUG_
                                                //================================================================================================================================================================================================================================================================

                                                if (Parameters.m_VerbosityLevel)
                                                {
                                                    std::cout << "AId = " << pSpaceLineSegmentA->m_Id << " BId = " << pSpaceLineSegmentB->m_Id << " : " << SpaceDistance << " : " << SpaceOrientationCoherence << std::endl;
                                                }

                                                CByteImage* pLeftInternalOutputImage = GetInternalOutputImage(eLeftCamera);

                                                CByteImage* pRightInternalOutputImage = GetInternalOutputImage(eRightCamera);

                                                ImageProcessor::ConvertImage(m_ppIntensityImages[eLeftCamera], pLeftInternalOutputImage);

                                                ImageProcessor::ConvertImage(m_ppIntensityImages[eRightCamera], pRightInternalOutputImage);

                                                PrimitivesDrawer::DrawLine(pLeftInternalOutputImage, pSpaceLineSegmentA->m_PointA.m_LeftImagePoint, pSpaceLineSegmentA->m_PointB.m_LeftImagePoint, 255, 0, 0, 1);

                                                PrimitivesDrawer::DrawLine(pRightInternalOutputImage, pSpaceLineSegmentA->m_PointA.m_RightImagePoint, pSpaceLineSegmentA->m_PointB.m_RightImagePoint, 255, 0, 0, 1);

                                                PrimitivesDrawer::DrawLine(pLeftInternalOutputImage, pSpaceLineSegmentB->m_PointA.m_LeftImagePoint, pSpaceLineSegmentB->m_PointB.m_LeftImagePoint, 0, 0, 255, 1);

                                                PrimitivesDrawer::DrawLine(pRightInternalOutputImage, pSpaceLineSegmentB->m_PointA.m_RightImagePoint, pSpaceLineSegmentB->m_PointB.m_RightImagePoint, 0, 0, 255, 1);

                                                PrimitivesDrawer::DrawLine(pLeftInternalOutputImage, pSpaceLineSegmentA->m_ImageCentralPoint[eLeftCamera], pSpaceLineSegmentB->m_ImageCentralPoint[eLeftCamera], 0, 255, 0, 1);

                                                PrimitivesDrawer::DrawLine(pRightInternalOutputImage, pSpaceLineSegmentA->m_ImageCentralPoint[eRightCamera], pSpaceLineSegmentB->m_ImageCentralPoint[eRightCamera], 0, 255, 0, 1);

                                                const std::string LeftFileName = CreateFileName(GetPartialResultsDirectory(), eLeftCamera, "DebugMatching", "bmp", 0);

                                                pLeftInternalOutputImage->SaveToFile(LeftFileName.c_str());

                                                const std::string RightFileName = CreateFileName(GetPartialResultsDirectory(), eRightCamera, "DebugMatching", "bmp", 0);

                                                pRightInternalOutputImage->SaveToFile(RightFileName.c_str());
                                                //================================================================================================================================================================================================================================================================
#endif

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        stable_sort(ConnectivitySortedLinking.begin(), ConnectivitySortedLinking.end(), Linkage::SortPredicateBestLinkage);

        std::vector<Linkage*>::iterator EndLinkage = ConnectivitySortedLinking.end();

#ifdef _VISUAL_DEBUG_
        //================================================================================================================================================================================================================================================================

        if (Parameters.m_VerbosityLevel)
        {
            for (std::vector<Linkage*>::iterator pLinkage = ConnectivitySortedLinking.begin(); pLinkage != EndLinkage; ++pLinkage)
            {
                std::cout << "Id = " << (*pLinkage)->m_Id << " TotalLinks = " << (*pLinkage)->m_Links.size() << " TotalDistance = " << (*pLinkage)->m_TotalDistance << " TotalOrientationDeviation = " << (*pLinkage)->m_TotalOrientationDeviation << " TotalSpatialUncertainty = " << (*pLinkage)->m_TotalSpatialUncertainty << std::endl;

                std::cout << "Ids : " << std::endl;

                std::deque<Linkage::Link>::iterator EndLinks = (*pLinkage)->m_Links.end();

                for (std::deque<Linkage::Link>::iterator pLink = (*pLinkage)->m_Links.begin(); pLink != EndLinks; ++pLink)
                {
                    std::cout << "\t" << pLink->m_Id << "\t" << pLink->m_Distance << std::endl;
                }

                std::cout << std::endl;
            }
        }

        //================================================================================================================================================================================================================================================================
#endif

        for (std::vector<Linkage*>::iterator pLinkage = ConnectivitySortedLinking.begin() ; pLinkage != EndLinkage ; ++pLinkage)
        {
            if ((*pLinkage)->m_Active)
            {
                (*pLinkage)->m_Active = false;

                std::vector<int> ClusterIds;

                ClusterIds.push_back((*pLinkage)->m_Id);

                std::deque<Linkage::Link>::iterator EndLinks = (*pLinkage)->m_Links.end();

                for (std::deque<Linkage::Link>::iterator pLink = (*pLinkage)->m_Links.begin() ; pLink != EndLinks ; ++pLink)
                {
                    if (SequentialLinking[pLink->m_Id]->m_Active)
                    {
                        SequentialLinking[pLink->m_Id]->m_Active = false;

                        ClusterIds.push_back(pLink->m_Id);

#ifdef _VISUAL_DEBUG_
                        //================================================================================================================================================================================================================================================================

                        const SpaceLineSegment* pSpaceLineSegmentA = &m_SpatialLineSegments[(*pLinkage)->m_Id];

                        const SpaceLineSegment* pSpaceLineSegmentB = &m_SpatialLineSegments[pLink->m_Id];

                        CByteImage* pLeftInternalOutputImage = GetInternalOutputImage(eLeftCamera);

                        CByteImage* pRightInternalOutputImage = GetInternalOutputImage(eRightCamera);

                        ImageProcessor::ConvertImage(m_ppIntensityImages[eLeftCamera], pLeftInternalOutputImage);

                        ImageProcessor::ConvertImage(m_ppIntensityImages[eRightCamera], pRightInternalOutputImage);

                        PrimitivesDrawer::DrawLine(pLeftInternalOutputImage, pSpaceLineSegmentA->m_PointA.m_LeftImagePoint, pSpaceLineSegmentA->m_PointB.m_LeftImagePoint, 255, 0, 0, 1);

                        PrimitivesDrawer::DrawLine(pRightInternalOutputImage, pSpaceLineSegmentA->m_PointA.m_RightImagePoint, pSpaceLineSegmentA->m_PointB.m_RightImagePoint, 255, 0, 0, 1);

                        PrimitivesDrawer::DrawLine(pLeftInternalOutputImage, pSpaceLineSegmentB->m_PointA.m_LeftImagePoint, pSpaceLineSegmentB->m_PointB.m_LeftImagePoint, 0, 0, 255, 1);

                        PrimitivesDrawer::DrawLine(pRightInternalOutputImage, pSpaceLineSegmentB->m_PointA.m_RightImagePoint, pSpaceLineSegmentB->m_PointB.m_RightImagePoint, 0, 0, 255, 1);

                        PrimitivesDrawer::DrawLine(pLeftInternalOutputImage, pSpaceLineSegmentA->m_ImageCentralPoint[eRightCamera], pSpaceLineSegmentB->m_ImageCentralPoint[eRightCamera], 0, 255, 0, 1);

                        PrimitivesDrawer::DrawLine(pRightInternalOutputImage, pSpaceLineSegmentA->m_ImageCentralPoint[eRightCamera], pSpaceLineSegmentB->m_ImageCentralPoint[eRightCamera], 0, 255, 0, 1);

                        const std::string LeftFileName = CreateFileName(GetPartialResultsDirectory(), eLeftCamera, "DebugMatching", "bmp", 0);

                        pLeftInternalOutputImage->SaveToFile(LeftFileName.c_str());

                        const std::string RightFileName = CreateFileName(GetPartialResultsDirectory(), eRightCamera, "DebugMatching", "bmp", 0);

                        pRightInternalOutputImage->SaveToFile(RightFileName.c_str());

                        //================================================================================================================================================================================================================================================================
#endif

                    }
                }

                if (ClusterFusion(ClusterIds, m_SpatialLineSegments))
                {
                    if (Parameters.m_SavePartialResults && IsSavingPartialResults())
                    {
                        CByteImage* pLeftInternalOutputImage = GetInternalOutputImage(eLeftCamera);

                        if (!pLeftInternalOutputImage)
                        {
                            _GENERIC_ERROR_;

                            return false;
                        }

                        if (!ImageProcessor::ConvertImage(m_ppIntensityImages[eLeftCamera], pLeftInternalOutputImage))
                        {
                            _GENERIC_ERROR_;

                            return false;
                        }

                        CByteImage* pRightInternalOutputImage = GetInternalOutputImage(eRightCamera);

                        if (!pRightInternalOutputImage)
                        {
                            _GENERIC_ERROR_;

                            return false;
                        }

                        if (!ImageProcessor::ConvertImage(m_ppIntensityImages[eRightCamera], pRightInternalOutputImage))
                        {
                            _GENERIC_ERROR_;

                            return false;
                        }

                        const MobileKitchenHandle& CurrentHandle = m_MobileKitchenHandle.back();

                        CByteImage* pImages[2] = { pLeftInternalOutputImage, pRightInternalOutputImage };

                        Vec2d Points[4];

                        for (int i = 0 ; i < 2 ; ++i)
                        {
                            const int Radius = std::min(CurrentHandle.m_ImageBoundingBox[i].GetHeight(), CurrentHandle.m_ImageBoundingBox[i].GetWidth());

                            for (int j = 0 ; j < 2 ; ++j)
                            {
                                PrimitivesDrawer::DrawLine(pImages[i], CurrentHandle.m_ImageMeanPointA[i], CurrentHandle.m_ImageMeanPointB[i], 0, 0, 255, 1);

                                for (int k = 1 ; k <= 4 ; ++k)
                                {
                                    PrimitivesDrawer::DrawCircle(pImages[i], CurrentHandle.m_ImageMeanPointA[i], Radius + k * 4, 255, 0, 0, 1, true);

                                    PrimitivesDrawer::DrawCircle(pImages[i], CurrentHandle.m_ImageMeanPointB[i], Radius + k * 4, 0, 255, 0, 1, true);
                                }

                                PrimitivesDrawer::DrawCross(pImages[i], CurrentHandle.m_ImageMeanPointA[i], 3, 0, 0, 0);

                                PrimitivesDrawer::DrawCross(pImages[i], CurrentHandle.m_ImageMeanPointB[i], 3, 0, 0, 0);

                                CurrentHandle.m_ImageBoundingBox[i].GetBoxPoints(Points, 3.0f);

                                if (!j)
                                {
                                    ImageProcessor::GaussianSmooth3x3(pImages[i], pImages[i]);
                                }
                            }

                            for (int j = 0 ; j < 4 ; ++j)
                            {
                                PrimitivesDrawer::DrawLine(pImages[i], Points[j], Points[(j + 1) % 4], 255, 0, 255, 1);
                            }

                            CurrentHandle.m_ImageBoundingBoxPointA[i].GetBoxPoints(Points, 2.0f);

                            for (int j = 0 ; j < 4 ; ++j)
                            {
                                PrimitivesDrawer::DrawLine(pImages[i], Points[j], Points[(j + 1) % 4], 255, 255, 0, 1);
                            }

                            CurrentHandle.m_ImageBoundingBoxPointB[i].GetBoxPoints(Points, 2.0f);

                            for (int j = 0 ; j < 4 ; ++j)
                            {
                                PrimitivesDrawer::DrawLine(pImages[i], Points[j], Points[(j + 1) % 4], 0, 255, 255, 1);
                            }
                        }

                        const std::string LeftFileName = CreateFileName(GetPartialResultsDirectory(), eLeftCamera, "ProcessModelDescriptionMatching", "bmp", 0);

                        if (!pLeftInternalOutputImage->SaveToFile(LeftFileName.c_str()))
                        {
                            _GENERIC_ERROR_;

                            return false;
                        }

                        const std::string RightFileName = CreateFileName(GetPartialResultsDirectory(), eRightCamera, "ProcessModelDescriptionMatching", "bmp", 0);

                        if (!pRightInternalOutputImage->SaveToFile(RightFileName.c_str()))
                        {
                            _GENERIC_ERROR_;

                            return false;
                        }
                    }
                }
                else
                {
                    for (std::vector<Linkage*>::iterator pLinkage = ConnectivitySortedLinking.begin(); pLinkage != EndLinkage; ++pLinkage)
                    {
                        delete *pLinkage;
                    }

                    _GENERIC_ERROR_;

                    return false;
                }

            }
        }

        for (std::vector<Linkage*>::iterator pLinkage = ConnectivitySortedLinking.begin() ; pLinkage != EndLinkage ; ++pLinkage)
        {
            delete *pLinkage;
        }

        if (m_MobileKitchenHandle.size())
        {
            stable_sort(m_MobileKitchenHandle.begin(), m_MobileKitchenHandle.end(), SortPredicate);

            if (Parameters.m_SavePartialResults && IsSavingPartialResults())
            {
                CByteImage* pLeftInternalOutputImage = GetInternalOutputImage(eLeftCamera);

                if (!pLeftInternalOutputImage)
                {
                    _GENERIC_ERROR_;

                    return false;
                }

                if (!ImageProcessor::ConvertImage(m_ppIntensityImages[eLeftCamera], pLeftInternalOutputImage))
                {
                    _GENERIC_ERROR_;

                    return false;
                }

                CByteImage* pRightInternalOutputImage = GetInternalOutputImage(eRightCamera);

                if (!pRightInternalOutputImage)
                {
                    _GENERIC_ERROR_;

                    return false;
                }

                if (!ImageProcessor::ConvertImage(m_ppIntensityImages[eRightCamera], pRightInternalOutputImage))
                {
                    _GENERIC_ERROR_;

                    return false;
                }

                const MobileKitchenHandle& CurrentHandle = m_MobileKitchenHandle.front();

                CByteImage* pImages[2] = { pLeftInternalOutputImage, pRightInternalOutputImage };

                Vec2d Points[4];

                for (int i = 0 ; i < 2 ; ++i)
                {
                    const int Radius = std::min(CurrentHandle.m_ImageBoundingBox[i].GetHeight(), CurrentHandle.m_ImageBoundingBox[i].GetWidth());

                    for (int j = 0 ; j < 2 ; ++j)
                    {
                        PrimitivesDrawer::DrawLine(pImages[i], CurrentHandle.m_ImageMeanPointA[i], CurrentHandle.m_ImageMeanPointB[i], 0, 0, 255, 1);

                        for (int k = 1 ; k <= 4 ; ++k)
                        {
                            PrimitivesDrawer::DrawCircle(pImages[i], CurrentHandle.m_ImageMeanPointA[i], Radius + k * 4, 255, 0, 0, 1, true);

                            PrimitivesDrawer::DrawCircle(pImages[i], CurrentHandle.m_ImageMeanPointB[i], Radius + k * 4, 0, 255, 0, 1, true);
                        }

                        PrimitivesDrawer::DrawCross(pImages[i], CurrentHandle.m_ImageMeanPointA[i], 3, 0, 0, 0);

                        PrimitivesDrawer::DrawCross(pImages[i], CurrentHandle.m_ImageMeanPointB[i], 3, 0, 0, 0);

                        CurrentHandle.m_ImageBoundingBox[i].GetBoxPoints(Points, 3.0f);

                        if (!j)
                        {
                            ImageProcessor::GaussianSmooth3x3(pImages[i], pImages[i]);
                        }
                    }

                    for (int j = 0 ; j < 4 ; ++j)
                    {
                        PrimitivesDrawer::DrawLine(pImages[i], Points[j], Points[(j + 1) % 4], 255, 0, 255, 1);
                    }

                    CurrentHandle.m_ImageBoundingBoxPointA[i].GetBoxPoints(Points, 2.0f);

                    for (int j = 0 ; j < 4 ; ++j)
                    {
                        PrimitivesDrawer::DrawLine(pImages[i], Points[j], Points[(j + 1) % 4], 255, 255, 0, 1);
                    }

                    CurrentHandle.m_ImageBoundingBoxPointB[i].GetBoxPoints(Points, 2.0f);

                    for (int j = 0 ; j < 4 ; ++j)
                    {
                        PrimitivesDrawer::DrawLine(pImages[i], Points[j], Points[(j + 1) % 4], 0, 255, 255, 1);
                    }
                }

                const std::string LeftFileName = CreateFileName(GetPartialResultsDirectory(), eLeftCamera, "Recognition", "bmp", 0);

                if (!pLeftInternalOutputImage->SaveToFile(LeftFileName.c_str()))
                {
                    _GENERIC_ERROR_;

                    return false;
                }

                const std::string RightFileName = CreateFileName(GetPartialResultsDirectory(), eRightCamera, "Recognition", "bmp", 0);

                if (!pRightInternalOutputImage->SaveToFile(RightFileName.c_str()))
                {
                    _GENERIC_ERROR_;

                    return false;
                }
            }
        }

        return true;
    }

    bool CMobileKitchenHandleRecognizer::ClusterFusion(std::vector<int>& ClusterIds, std::vector<SpaceLineSegment>& SpatialLineSegments)
    {
        const int TotalClusterElements = ClusterIds.size();

        MobileKitchenHandle CurrentHandle;

#ifdef _VISUAL_DEBUG_
        //================================================================================================================================================================================================================================================================

        CByteImage* pLeftInternalOutputImage = GetInternalOutputImage(eLeftCamera);

        CByteImage* pRightInternalOutputImage = GetInternalOutputImage(eRightCamera);

        ImageProcessor::ConvertImage(m_ppIntensityImages[eLeftCamera], pLeftInternalOutputImage);

        ImageProcessor::ConvertImage(m_ppIntensityImages[eRightCamera], pRightInternalOutputImage);

        //================================================================================================================================================================================================================================================================
#endif

        for (int i = 0 ; i < TotalClusterElements ; ++i)
        {
            const SpaceLineSegment& LineSegment = SpatialLineSegments[ClusterIds[i]];

            CurrentHandle.m_ImageBoundingBoxPointA[eLeftCamera].Expand(LineSegment.m_PointA.m_LeftImagePoint.x, LineSegment.m_PointA.m_LeftImagePoint.y);

            CurrentHandle.m_ImageBoundingBoxPointB[eLeftCamera].Expand(LineSegment.m_PointB.m_LeftImagePoint.x, LineSegment.m_PointB.m_LeftImagePoint.y);

            CurrentHandle.m_ImageBoundingBoxPointA[eRightCamera].Expand(LineSegment.m_PointA.m_RightImagePoint.x, LineSegment.m_PointA.m_RightImagePoint.y);

            CurrentHandle.m_ImageBoundingBoxPointB[eRightCamera].Expand(LineSegment.m_PointB.m_RightImagePoint.x, LineSegment.m_PointB.m_RightImagePoint.y);

            Math2d::AddVecVec(CurrentHandle.m_ImageMeanPointA[eLeftCamera], LineSegment.m_PointA.m_LeftImagePoint, CurrentHandle.m_ImageMeanPointA[eLeftCamera]);

            Math2d::AddVecVec(CurrentHandle.m_ImageMeanPointB[eLeftCamera], LineSegment.m_PointB.m_LeftImagePoint, CurrentHandle.m_ImageMeanPointB[eLeftCamera]);

            Math2d::AddVecVec(CurrentHandle.m_ImageMeanPointA[eRightCamera], LineSegment.m_PointA.m_RightImagePoint, CurrentHandle.m_ImageMeanPointA[eRightCamera]);

            Math2d::AddVecVec(CurrentHandle.m_ImageMeanPointB[eRightCamera], LineSegment.m_PointA.m_RightImagePoint, CurrentHandle.m_ImageMeanPointB[eRightCamera]);

            Math3d::AddVecVec(CurrentHandle.m_SpaceMeanPointA, LineSegment.m_PointA.m_SpacePoint, CurrentHandle.m_SpaceMeanPointA);

            Math3d::AddVecVec(CurrentHandle.m_SpaceMeanPointB, LineSegment.m_PointB.m_SpacePoint, CurrentHandle.m_SpaceMeanPointB);

            Math3d::AddVecVec(CurrentHandle.m_SpaceMeanPointCentral, LineSegment.m_SpaceCentralPoint, CurrentHandle.m_SpaceMeanPointCentral);

#ifdef _VISUAL_DEBUG_
            //================================================================================================================================================================================================================================================================

            const SpaceLineSegment* pSpaceLineSegmentA = &LineSegment;

            PrimitivesDrawer::DrawLine(pLeftInternalOutputImage, pSpaceLineSegmentA->m_PointA.m_LeftImagePoint, pSpaceLineSegmentA->m_PointB.m_LeftImagePoint, 255, 0, 0, 1);

            PrimitivesDrawer::DrawLine(pRightInternalOutputImage, pSpaceLineSegmentA->m_PointA.m_RightImagePoint, pSpaceLineSegmentA->m_PointB.m_RightImagePoint, 255, 0, 0, 1);

            const std::string LeftFileName = CreateFileName(GetPartialResultsDirectory(), eLeftCamera, "DebugMatching", "bmp", 0);

            pLeftInternalOutputImage->SaveToFile(LeftFileName.c_str());

            const std::string RightFileName = CreateFileName(GetPartialResultsDirectory(), eRightCamera, "DebugMatching", "bmp", 0);

            pRightInternalOutputImage->SaveToFile(RightFileName.c_str());

            //================================================================================================================================================================================================================================================================
#endif

        }

        const float NormalizationFactor = 1.0f / TotalClusterElements;

        Math2d::MulVecScalar(CurrentHandle.m_ImageMeanPointA[eLeftCamera], NormalizationFactor, CurrentHandle.m_ImageMeanPointA[eLeftCamera]);

        Math2d::MulVecScalar(CurrentHandle.m_ImageMeanPointB[eLeftCamera], NormalizationFactor, CurrentHandle.m_ImageMeanPointB[eLeftCamera]);

        Math2d::MulVecScalar(CurrentHandle.m_ImageMeanPointA[eRightCamera], NormalizationFactor, CurrentHandle.m_ImageMeanPointA[eRightCamera]);

        Math2d::MulVecScalar(CurrentHandle.m_ImageMeanPointB[eRightCamera], NormalizationFactor, CurrentHandle.m_ImageMeanPointB[eRightCamera]);

        Math3d::MulVecScalar(CurrentHandle.m_SpaceMeanPointA, NormalizationFactor, CurrentHandle.m_SpaceMeanPointA);

        Math3d::MulVecScalar(CurrentHandle.m_SpaceMeanPointB, NormalizationFactor, CurrentHandle.m_SpaceMeanPointB);

        Math3d::MulVecScalar(CurrentHandle.m_SpaceMeanPointCentral, NormalizationFactor, CurrentHandle.m_SpaceMeanPointCentral);

        CurrentHandle.m_ImageBoundingBox[eLeftCamera].Expand(CurrentHandle.m_ImageBoundingBoxPointA[eLeftCamera]);

        CurrentHandle.m_ImageBoundingBox[eLeftCamera].Expand(CurrentHandle.m_ImageBoundingBoxPointB[eLeftCamera]);

        CurrentHandle.m_ImageBoundingBox[eRightCamera].Expand(CurrentHandle.m_ImageBoundingBoxPointA[eRightCamera]);

        CurrentHandle.m_ImageBoundingBox[eRightCamera].Expand(CurrentHandle.m_ImageBoundingBoxPointB[eRightCamera]);

        CurrentHandle.m_ClusteredElements = TotalClusterElements;

        CurrentHandle.m_Id = m_MobileKitchenHandle.size();

        m_MobileKitchenHandle.push_back(CurrentHandle);

        return true;
    }

    bool CMobileKitchenHandleRecognizer::FilterEdges(const CByteImage* pRawEdgesImage, CByteImage* pFilteredEdgesImage, CByteImage* pLowResolutionMaskEdgesImage, CByteImage* pHighResolutionMaskEdgesImage, const int BitShifts, const int ExpansionMargin, const int MinimalBlockSize, const int MinimalBlockDimension, const float MaximalEigenRatio, const float MaximalDeviation, const BoundingBox2D* pActiveBoundingBox, BoundingBox2D* pHarrisActiveZone, const bool IsHorizontal)
    {
        if (!pRawEdgesImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRawEdgesImage->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRawEdgesImage->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRawEdgesImage->bytesPerPixel != 1)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!pFilteredEdgesImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pFilteredEdgesImage->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pFilteredEdgesImage->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pFilteredEdgesImage->bytesPerPixel != 1)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRawEdgesImage == pFilteredEdgesImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRawEdgesImage->width != pFilteredEdgesImage->width)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pRawEdgesImage->height != pFilteredEdgesImage->height)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (MinimalBlockSize <= 1)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (MinimalBlockDimension <= 1)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!pActiveBoundingBox)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!pHarrisActiveZone)
        {
            _GENERIC_ERROR_;

            return false;
        }

        memcpy(pFilteredEdgesImage->pixels, pRawEdgesImage->pixels, pFilteredEdgesImage->width * pFilteredEdgesImage->height);

        const int SubWidth = pFilteredEdgesImage->width - 1;

        const int SubHeight = pFilteredEdgesImage->height - 1;

        unsigned char* pScanPixel = pFilteredEdgesImage->pixels;

        std::deque<CEdgeBlock*> EdgeBlocks;

        std::deque<PixelLocation> ExpandingLocations;

        PixelLocation ScanLocation;

        PixelLocation ExpandingLocation;

        for (ScanLocation.m_Y = 0; ScanLocation.m_Y <= SubHeight; ++ScanLocation.m_Y)
        {
            for (ScanLocation.m_X = 0; ScanLocation.m_X <= SubWidth; ++ScanLocation.m_X, ++pScanPixel)
            {
                if (*pScanPixel)
                {
                    try
                    {
                        CEdgeBlock* pEdgeBlock = new CEdgeBlock(ScanLocation);

                        *pScanPixel = 0;

                        ExpandingLocations.push_back(ScanLocation);

                        while (ExpandingLocations.size())
                        {
                            const PixelLocation CurrentLocation = ExpandingLocations.front();

                            ExpandingLocations.pop_front();

                            const int X0 = std::max(CurrentLocation.m_X - 1, 0);

                            const int Y0 = std::max(CurrentLocation.m_Y - 1, 0);

                            const int X1 = std::min(CurrentLocation.m_X + 1, SubWidth);

                            const int Y1 = std::min(CurrentLocation.m_Y + 1, SubHeight);

                            unsigned char* pSearchPixelBase = pFilteredEdgesImage->pixels + (pFilteredEdgesImage->width * Y0) + X0;

                            for (ExpandingLocation.m_Y = Y0; ExpandingLocation.m_Y <= Y1; ++ExpandingLocation.m_Y, pSearchPixelBase += pFilteredEdgesImage->width)
                            {
                                unsigned char* pSearchPixel = pSearchPixelBase;

                                for (ExpandingLocation.m_X = X0; ExpandingLocation.m_X <= X1; ++ExpandingLocation.m_X, ++pSearchPixel)
                                {
                                    if (*pSearchPixel)
                                    {
                                        *pSearchPixel = 0;

                                        ExpandingLocations.push_back(ExpandingLocation);

                                        pEdgeBlock->AddPixel(ExpandingLocation);
                                    }
                                }
                            }
                        }

                        if (pEdgeBlock->GetSize() >= MinimalBlockSize)
                        {
                            if (pActiveBoundingBox->Containes(&pEdgeBlock->GetBoundingBox()))
                            {
                                if (std::max(pEdgeBlock->GetWidth(), pEdgeBlock->GetHeight()) > MinimalBlockDimension)
                                {
                                    pEdgeBlock->SetId(EdgeBlocks.size());

                                    EdgeBlocks.push_back(pEdgeBlock);
                                }
                                else
                                {
                                    delete pEdgeBlock;
                                }

                            }
                            else
                            {
                                delete pEdgeBlock;
                            }
                        }
                        else
                        {
                            delete pEdgeBlock;
                        }
                    }
                    catch (std::exception& /*Exception*/)
                    {
                        _GENERIC_ERROR_;

                        return false;
                    }
                }
            }
        }

        stable_sort(EdgeBlocks.begin(), EdgeBlocks.end(), CEdgeBlock::SortPredicateByArea);

        std::deque<CEdgeBlock*>::const_iterator EndBlocks = EdgeBlocks.end();

        for (std::deque<CEdgeBlock*>::const_iterator ppBlockA = EdgeBlocks.begin(); ppBlockA != EndBlocks; ++ppBlockA)
        {
            std::deque<CEdgeBlock*>::const_iterator ppBlockB = ppBlockA;

            for (++ppBlockB; ppBlockB != EndBlocks; ++ppBlockB)
            {
                if ((*ppBlockA)->Containes(*ppBlockB))
                {
                    (*ppBlockA)->AddSubBlock(*ppBlockB);
                }
            }
        }

        memset(pLowResolutionMaskEdgesImage->pixels, 0, pLowResolutionMaskEdgesImage->width * pLowResolutionMaskEdgesImage->height);

        pHarrisActiveZone->SetEmpty();

        for (std::deque<CEdgeBlock*>::const_iterator ppBlock = EdgeBlocks.begin(); ppBlock != EndBlocks; ++ppBlock)
        {
            if ((*ppBlock)->GetTotalSubBlocks() <= 3)
            {
                (*ppBlock)->BasicCharacterize();

                if ((*ppBlock)->IsHorizontal() == IsHorizontal)
                {
                    if ((*ppBlock)->GetEigenRatio() <= MaximalEigenRatio)
                    {
                        (*ppBlock)->ExtendedCharacterize();

                        if (fabs((*ppBlock)->GetSecondaryAxisOccupancyBalance()) <= MaximalDeviation)
                        {
                            (*ppBlock)->SetActive(true);

                            (*ppBlock)->DropNoCheck(pFilteredEdgesImage, 255);

                            (*ppBlock)->ScaledDrop(pLowResolutionMaskEdgesImage, BitShifts, ExpansionMargin, 255);

                            pHarrisActiveZone->Expand((*ppBlock)->GetBoundingBox());

                        }
                    }
                }
            }

#ifdef _VISUAL_DEBUG_
            //================================================================================================================================================================================================================================================================

            CByteImage Display(pRawEdgesImage->width, pRawEdgesImage->height, CByteImage::eRGB24);

            (*ppBlock)->Draw(pRawEdgesImage, &Display);

            std::string FileName = std::string(GetPartialResultsDirectory()) + std::string("DebuggingBlock.bmp");

            Display.SaveToFile(FileName.c_str());

            //================================================================================================================================================================================================================================================================
#endif

            delete *ppBlock;
        }

        if (pHarrisActiveZone->IsEmpty())
        {
            ImageProcessor::Zero(pHighResolutionMaskEdgesImage);
        }
        else
        {
            pHarrisActiveZone->AddOffset((0x1 << BitShifts) + ExpansionMargin);

            if (!BinaryMapping(pLowResolutionMaskEdgesImage, pHighResolutionMaskEdgesImage))
            {
                _GENERIC_ERROR_;

                return false;
            }
        }

        return true;
    }

    int CMobileKitchenHandleRecognizer::GetTotalPointsAlongLineSegment(const Vec2d& A, const Vec2d& B, const float Margin, const std::vector<Vec2d>& ActiveHarrisPoints)
    {
        Vec2d UnitaryParallelAB = Math2d::zero_vec, UnitaryPerpendicularAB = Math2d::zero_vec, Delta = Math2d::zero_vec;

        const float Length = GetLineSegmentUnitaryVectors(A, B, UnitaryParallelAB, UnitaryPerpendicularAB);

        if (Length < 1.0f)
        {
            return 0;
        }

        const float X0 = int(_MIN_(A.x, B.x));

        const float Y0 = int(_MIN_(A.y, B.y));

        const float X1 = int(ceil(_MAX_(A.x, B.x)));

        const float Y1 = int(ceil(_MAX_(A.y, B.y)));

        int TotalPointsAlongLineSegment = 0;

        const int TotalPoints = ActiveHarrisPoints.size();

        for (int i = 0 ; i < TotalPoints ; ++i)
        {
            const Vec2d Point = ActiveHarrisPoints[i];

            if ((Point.x >= X0) && (Point.x <= X1) && (Point.y >= Y0) && (Point.y <= Y1))
            {
                Math2d::SubtractVecVec(Point, A, Delta);

                if (fabs(_DOT_PRODUCT_2D_(Delta, UnitaryPerpendicularAB)) <= Margin)
                {
                    const float ParrallelProjection = _DOT_PRODUCT_2D_(Delta, UnitaryParallelAB);

                    if ((ParrallelProjection > Margin) && (ParrallelProjection < (Length - Margin)))
                    {
                        ++TotalPointsAlongLineSegment;
                    }
                }
            }
        }

        return TotalPointsAlongLineSegment;
    }

    bool CMobileKitchenHandleRecognizer::NormalizedCrossCorrelationIntensity(float& NCCI, const CByteImage* pLeftImage, const CByteImage* pRightImage, const Vec2d& LeftPoint, const Vec2d& RightPoint, const int KernelRadius, const float* const pKernelBase, float* const pLeftDescriptor, float* const pRightDescriptor, const float KernelIntegral)
    {
        if (!pLeftImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!pRightImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        const int LX = int(LeftPoint.x + 0.5f);

        const int LY = int(LeftPoint.y + 0.5f);

        if ((LX - KernelRadius) < 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if ((LX + KernelRadius) >= pLeftImage->width)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if ((LY - KernelRadius) < 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if ((LY + KernelRadius) >= pLeftImage->height)
        {
            _GENERIC_ERROR_;

            return false;
        }

        const int RX = int(RightPoint.x + 0.5f);

        const int RY = int(RightPoint.y + 0.5f);

        if ((RX - KernelRadius) < 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if ((RX + KernelRadius) >= pRightImage->width)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if ((RY - KernelRadius) < 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if ((RY + KernelRadius) >= pRightImage->height)
        {
            _GENERIC_ERROR_;

            return false;
        }

        const int KernelDiameter = (KernelRadius << 1) + 1;

        float LeftWeightedAccumulator = 0.0f, RightWightedAccumulator = 0.0f;

        unsigned char* pLeftIntensityBase = pLeftImage->pixels + (pLeftImage->width * (LY - KernelRadius)) + (LX - KernelRadius);

        unsigned char* pRightIntensityBase = pRightImage->pixels + (pRightImage->width * (RY - KernelRadius)) + (RX - KernelRadius);

        const float* pKernel = pKernelBase;

        for (int i = 0, Index = 0; i < KernelDiameter; ++i, pLeftIntensityBase += pLeftImage->width, pRightIntensityBase += pRightImage->width)
        {
            unsigned char* pLeftIntensity = pLeftIntensityBase;

            unsigned char* pRightIntensity = pRightIntensityBase;

            for (int j = 0; j < KernelDiameter; ++j)
            {
                LeftWeightedAccumulator += float(*pLeftIntensity) * *pKernel;

                pLeftDescriptor[Index] = *pLeftIntensity++;

                RightWightedAccumulator += float(*pRightIntensity) * *pKernel++;

                pRightDescriptor[Index++] = *pRightIntensity++;
            }
        }

        const float LeftMean = LeftWeightedAccumulator / KernelIntegral;

        const float RightMean = RightWightedAccumulator / KernelIntegral;

        float LeftMax = (pLeftDescriptor[0] - LeftMean);

        float LeftMin = LeftMax;

        float RightMax = (pRightDescriptor[0] - RightMean);

        float RightMin = RightMax;

        const int KernelArea = KernelDiameter * KernelDiameter;

        for (int i = 0; i < KernelArea; ++i)
        {
            pLeftDescriptor[i] -= LeftMean;

            if (pLeftDescriptor[i] < LeftMin)
            {
                LeftMin = pLeftDescriptor[i];
            }
            else if (pLeftDescriptor[i] > LeftMax)
            {
                LeftMax = pLeftDescriptor[i];
            }

            pRightDescriptor[i] -= RightMean;

            if (pRightDescriptor[i] < RightMin)
            {
                RightMin = pRightDescriptor[i];
            }
            else if (pRightDescriptor[i] > RightMax)
            {
                RightMax = pRightDescriptor[i];
            }
        }

        const float LeftScale = 2.0f / (LeftMax - LeftMin);

        const float RightScale = 2.0f / (RightMax - RightMin);

        pKernel = pKernelBase;

        NCCI = 0.0f;

        for (int i = 0; i < KernelArea; ++i)
        {
            NCCI += *pKernel++ * ((LeftScale * (pLeftDescriptor[i] - LeftMin)) - 1.0f) * ((RightScale * (pRightDescriptor[i] - RightMin) - 1.0f));
        }

        NCCI /= KernelIntegral;

        return true;
    }

    bool CMobileKitchenHandleRecognizer::IsLineSegmentInMaskImage(const CByteImage* pMaskImage, const Vec2d& A, const Vec2d& B, const float Margin)
    {
        Vec2d UnitaryAlongAB = Math2d::zero_vec, UnitaryPerpendicularAB = Math2d::zero_vec;

        const float Length = GetLineSegmentUnitaryVectors(A, B, UnitaryAlongAB, UnitaryPerpendicularAB);

        if (Length < 1.0f)
        {
            const int XM = int((A.x + B.x) * 0.5f + 0.5f);

            const int YM = int((A.y + B.y) * 0.5f + 0.5f);

            return pMaskImage->pixels[pMaskImage->width * YM + XM];
        }

        const int X0 = int(_MIN_(A.x, B.x));

        if (X0 < 0)
        {
            return false;
        }

        const int X1 = int(ceil(_MAX_(A.x, B.x)));

        if (X1 >= pMaskImage->width)
        {
            return false;
        }

        const int Y0 = int(_MIN_(A.y, B.y));

        if (Y0 < 0)
        {
            return false;
        }

        const int Y1 = int(ceil(_MAX_(A.y, B.y)));

        if (Y1 >= pMaskImage->height)
        {
            return false;
        }

        Vec2d Delta;

        for (int Y = Y0 ; Y <= Y1 ; ++Y)
        {
            Delta.y = float(Y) - A.y;

            for (int X = X0 ; X <= X1 ; ++X)
            {
                Delta.x = float(X) - A.x;

                const float PerpendicularProjection = fabsf(_DOT_PRODUCT_2D_(Delta, UnitaryPerpendicularAB));

                if (PerpendicularProjection <= Margin)
                {
                    const float ParallelProjection = _DOT_PRODUCT_2D_(Delta, UnitaryAlongAB);

                    if ((ParallelProjection >= 0.0f) && (ParallelProjection <= Length))
                    {
                        if (!pMaskImage->pixels[pMaskImage->width * Y + X])
                        {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    float CMobileKitchenHandleRecognizer::GetLineSegmentUnitaryVectors(const Vec2d& A, const Vec2d& B, Vec2d& UnitaryParallelAB, Vec2d& UnitaryPerpendicularAB)
    {
        Vec2d Delta;

        Math2d::SubtractVecVec(B, A, Delta);

        const float Lenght = Math2d::Length(Delta);

        if (Lenght > FLT_EPSILON)
        {
            Math2d::NormalizeVec(Delta);

            UnitaryParallelAB = Delta;

            Math2d::RotateVec(Delta, float(-M_PI_2), UnitaryPerpendicularAB);
        }

        return Lenght;
    }

    bool CMobileKitchenHandleRecognizer::SortPredicateHarrisPoint(const Vec2d& lhs, const Vec2d& rhs)
    {
        if (lhs.x == rhs.x)
        {
            return lhs.y < rhs.y;
        }

        return lhs.x < rhs.x;
    }

    int CMobileKitchenHandleRecognizer::GetOrganizedIndex(const int L0, const int U1, const float X, std::vector<Vec2d>& SortedActiveHarrisPoints)
    {
        if (X < SortedActiveHarrisPoints[L0].x)
        {
            return U1;
        }

        if (X > SortedActiveHarrisPoints[U1].x)
        {
            return U1;
        }

        int L = L0;

        int U = U1;

        while ((U - L) > 1)
        {
            const int M = (U + L) >> 1;

            if (X <= SortedActiveHarrisPoints[M].x)
            {
                U = M;
            }
            else
            {
                L = M;
            }
        }

        return (X <= SortedActiveHarrisPoints[U].x) ? U : L;
    }

    bool CMobileKitchenHandleRecognizer::BinaryMapping(const CByteImage* pLowResolutionImage, CByteImage* pHighResolutionImage)
    {
        if (!pLowResolutionImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pLowResolutionImage->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pLowResolutionImage->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pLowResolutionImage->bytesPerPixel != 1)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!pHighResolutionImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pHighResolutionImage->width <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pHighResolutionImage->height <= 0)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pHighResolutionImage->bytesPerPixel != 1)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pLowResolutionImage->width >= pHighResolutionImage->width)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pLowResolutionImage->height >= pHighResolutionImage->height)
        {
            _GENERIC_ERROR_;

            return false;
        }

        const float WidthRatio = float(pHighResolutionImage->width) / float(pLowResolutionImage->width);

        const float HeightRatio = float(pHighResolutionImage->height) / float(pLowResolutionImage->height);

        if (WidthRatio != HeightRatio)
        {
            _GENERIC_ERROR_;

            return false;
        }

        const float ContinuousBitShifts = log(WidthRatio) / log(2.0f);

        const int BitShifts = ContinuousBitShifts;

        if ((ContinuousBitShifts - float(BitShifts)) > FLT_EPSILON)
        {
            _GENERIC_ERROR_;

            return false;
        }

        const int StaticBlockSize = 0x1 << BitShifts;

        memset(pHighResolutionImage->pixels, 0, pHighResolutionImage->width * pHighResolutionImage->height);

        const unsigned char* pLowResolutionPixel = pLowResolutionImage->pixels;

        const int LowResolutionResolutionSubWidth = pLowResolutionImage->width - 1;

        const int HighResolutionSubWidth = pHighResolutionImage->width - 1;

        const int HighResolutionSubHeight = pHighResolutionImage->height - 1;

        for (int YL = 0; YL < pLowResolutionImage->height; ++YL)
        {
            for (int XL = 0; XL < pLowResolutionImage->width; ++XL, ++pLowResolutionPixel)
            {
                if (*pLowResolutionPixel)
                {
                    const int XH0 = std::min(XL << BitShifts, HighResolutionSubWidth);

                    const int YH0 = std::min(YL << BitShifts, HighResolutionSubHeight);

                    const int YH1 = std::min(YH0 + StaticBlockSize, HighResolutionSubHeight);

                    int DynamicBlock = std::min(XH0 + StaticBlockSize, HighResolutionSubWidth) - XH0;

                    while ((XL < LowResolutionResolutionSubWidth) && pLowResolutionPixel[1])
                    {
                        ++XL;

                        ++pLowResolutionPixel;

                        DynamicBlock += StaticBlockSize;
                    }

                    unsigned char* pHighResolutionPixel = pHighResolutionImage->pixels + (pHighResolutionImage->width * YH0) + XH0;

                    for (int YH = YH0; YH < YH1; ++YH, pHighResolutionPixel += pHighResolutionImage->width)
                    {
                        memset(pHighResolutionPixel, 255, DynamicBlock);
                    }
                }
            }
        }

        return true;
    }

    bool CMobileKitchenHandleRecognizer::SortPredicate(const MobileKitchenHandle& lhs, const MobileKitchenHandle& rhs)
    {
        return lhs.m_SpaceMeanPointCentral.z < rhs.m_SpaceMeanPointCentral.z;
    }
}
