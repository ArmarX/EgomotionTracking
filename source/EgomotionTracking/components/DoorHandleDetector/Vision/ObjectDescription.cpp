/*
 * ObjectDescription.cpp
 * ============================================================================
 *  Author:     Dr.-Ing. David I. Gonzalez Aguirre
 *  Mail:       david.gonzalez@kit.edu
 * ============================================================================
 */

#include "ObjectDescription.h"

using namespace std;

namespace Vision
{
    CObjectDescription::CObjectDescription(const std::string& ContextName, const std::string& ClassName, const std::string& InstanceName, const int ContextId, const int ClassId, const int InstanceId) :
        CAttributeMap(),
        m_ContextName(ContextName),
        m_ClassName(ClassName),
        m_InstanceName(InstanceName),
        m_ContextId(ContextId),
        m_ClassId(ClassId),
        m_InstanceId(InstanceId)
    {
    }

    CObjectDescription::~CObjectDescription()
        = default;

    const std::string& CObjectDescription::GetContextName() const
    {
        return m_ContextName;
    }

    const std::string& CObjectDescription::GetClassName() const
    {
        return m_ClassName;
    }

    const std::string& CObjectDescription::GetInstanceName() const
    {
        return m_InstanceName;
    }

    int CObjectDescription::GetContextId() const
    {
        return m_ContextId;
    }

    int CObjectDescription::GetClassId() const
    {
        return m_ClassId;
    }

    int CObjectDescription::GetInstanceId() const
    {
        return m_InstanceId;
    }
}
