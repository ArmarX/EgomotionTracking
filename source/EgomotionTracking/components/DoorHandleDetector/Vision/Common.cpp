/*
 * Common.cpp
 * ============================================================================
 *  Author:     Dr.-Ing. David I. Gonzalez Aguirre
 *  Mail:       david.gonzalez@kit.edu
 * ============================================================================
 */

#include "Common.h"

namespace Vision
{
    bool IsValid(const MonocularSource Source)
    {
        switch (Source)
        {
            case eLeftCamera:

            case eRightCamera:

                return true;
        }

        _GENERIC_ERROR_;

        return false;
    }

    bool IsValid(const BayerPatternType BayerPattern)
    {
        switch (BayerPattern)
        {
            case eGRBG:

            case eRGGB:

            case eBGGR:

            case eGBRG:

                return true;
        }

        _GENERIC_ERROR_;

        return false;
    }

    bool GetBayerPatternMap(const BayerPatternType BayerPattern, int BayerPatternMap[2][2])
    {
        if (!IsValid(BayerPattern))
        {
            _GENERIC_ERROR_;

            return false;
        }
        //eGRBG = 0x2412, eRGGB = 0x4221, eBGGR = 0x1224, eGBRG = 0x2142
        for (int Y = 0, ShiftingBits = 0; Y < 2; ++Y)
        {
            for (int X = 0; X < 2; ++X, ShiftingBits += 4)
            {
                BayerPatternMap[Y][X] = ((BayerPattern & (0xF << ShiftingBits)) >> (ShiftingBits + 1));
            }
        }

        return true;
    }

    void OnError(const char* pFunctionCaller, const char* pFile, const int SourceCodeLine)
    {
        std::cerr << "ERROR @ [ " << pFunctionCaller << " | " << pFile << " : " << SourceCodeLine << " ]" << std::endl;
    }
}

