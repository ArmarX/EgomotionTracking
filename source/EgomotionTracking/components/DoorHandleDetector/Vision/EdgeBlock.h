/*
 * EdgeBlock.h
 * ============================================================================
 *  Author:     Dr.-Ing. David I. Gonzalez Aguirre
 *  Mail:       david.gonzalez@kit.edu
 * ============================================================================
 */

#pragma once

#include "Common.h"

namespace Vision
{
    class CEdgeBlock
    {
    public:

        CEdgeBlock(const PixelLocation& Location);

        ~CEdgeBlock()
        {
        }

        inline void SetId(const int Id)
        {
            m_Id = Id;
        }

        inline void SetActive(const bool Active)
        {
            m_Active = Active;
        }

        inline void AddPixel(const PixelLocation& Location)
        {
            m_BoundingBox.FastExpand(Location.m_X, Location.m_Y);

            m_EdgePixelLocations.push_back(Location);
        }

        inline int GetSize() const
        {
            return int(m_EdgePixelLocations.size());
        }

        inline int GetWidth() const
        {
            return m_BoundingBox.GetWidth();
        }

        inline int GetHeight() const
        {
            return m_BoundingBox.GetHeight();
        }

        inline int GetArea() const
        {
            return m_BoundingBox.GetArea();
        }

        inline float GetBoundingBoxCenterXNocheck() const
        {
            return m_BoundingBox.GetCenterX();
        }

        inline float GetBoundingBoxCenterYNocheck() const
        {
            return m_BoundingBox.GetCenterY();
        }

        inline bool GetBoundingBoxCenter(float& X, float& Y) const
        {
            if (m_BoundingBox.IsEmpty())
            {
                return false;
            }

            X = m_BoundingBox.GetCenterX();

            Y = m_BoundingBox.GetCenterY();

            return true;
        }

        inline bool Containes(const CEdgeBlock* pBlock) const
        {
            return m_BoundingBox.Containes(&pBlock->m_BoundingBox);
        }

        inline void AddSubBlock(CEdgeBlock* pBlock)
        {
            if (pBlock && (pBlock != this) && (pBlock->m_BoundingBox.GetArea() < m_BoundingBox.GetArea()))
            {
                m_ContainedComponents.push_back(pBlock);
            }
        }

        inline int GetTotalSubBlocks() const
        {
            return int(m_ContainedComponents.size());
        }

        static bool SortPredicateByArea(const CEdgeBlock* plhs, const CEdgeBlock* prhs)
        {
            return plhs->m_BoundingBox.GetArea() > prhs->m_BoundingBox.GetArea();
        }

        inline void DropNoCheck(CByteImage* pImage, unsigned char Value) const
        {
            std::deque<PixelLocation>::const_iterator EndPixelLocations = m_EdgePixelLocations.end();

            for (std::deque<PixelLocation>::const_iterator pPixelLocation = m_EdgePixelLocations.begin() ; pPixelLocation != EndPixelLocations ; ++pPixelLocation)
            {
                pImage->pixels[pImage->width * pPixelLocation->m_Y + pPixelLocation->m_X] = Value;
            }
        }

        bool ScaledDrop(CByteImage* pImage, const int BitShifts, const int Margin, unsigned char Value) const;

        bool Draw(const CByteImage* pSourceImage, CByteImage* pDestiantionImage) const;

        void BasicCharacterize();

        void ExtendedCharacterize();

        void GetFullZone(int& X0, int& Y0, int& X1, int& Y1) const;

        inline float GetEigenWidth() const
        {
            return (m_MaximalEigenPoint.x - m_MinimalEigenPoint.x);
        }

        inline float GetEigenHeight() const
        {
            return (m_MaximalEigenPoint.y - m_MinimalEigenPoint.y);
        }

        inline bool IsHorizontal() const
        {
            return fabs(m_MainAxis.x) > fabs(m_MainAxis.y);
        }

        inline float GetEigenRatio() const
        {
            return (m_MaximalEigenPoint.y - m_MinimalEigenPoint.y) / (m_MaximalEigenPoint.x - m_MinimalEigenPoint.x);
        }

        inline float GetEigenArea() const
        {
            return (m_MaximalEigenPoint.x - m_MinimalEigenPoint.x) * (m_MaximalEigenPoint.y - m_MinimalEigenPoint.y);
        }

        inline float GetMainAxisOccupancyBalance() const
        {
            return (m_QuadrantOccupancy[1] + m_QuadrantOccupancy[0]) - (m_QuadrantOccupancy[2] + m_QuadrantOccupancy[3]);
        }

        inline float GetMainAxisRelativeAreaBalance() const
        {
            return (m_QuadrantRelativeArea[1] + m_QuadrantRelativeArea[0]) - (m_QuadrantRelativeArea[2] + m_QuadrantRelativeArea[3]);
        }

        inline float GetMainAxisCoefficientBalance() const
        {
            return (m_QuadrantCoefficient[1] + m_QuadrantCoefficient[0]) - (m_QuadrantCoefficient[2] + m_QuadrantCoefficient[3]);
        }

        inline float GetSecondaryAxisOccupancyBalance() const
        {
            return (m_QuadrantOccupancy[3] + m_QuadrantOccupancy[0]) - (m_QuadrantOccupancy[2] + m_QuadrantOccupancy[1]);
        }

        inline float GetSecondaryAxisRelativeAreaBalance() const
        {
            return (m_QuadrantRelativeArea[3] + m_QuadrantRelativeArea[0]) - (m_QuadrantRelativeArea[2] + m_QuadrantRelativeArea[1]);
        }

        inline float GetSecondaryAxisCoefficientBalance() const
        {
            return (m_QuadrantCoefficient[3] + m_QuadrantCoefficient[0]) - (m_QuadrantCoefficient[2] + m_QuadrantCoefficient[1]);
        }

        inline const BoundingBox2D& GetBoundingBox() const
        {
            return m_BoundingBox;
        }

    protected:

        static void ComputeCentroid(Vec2d& Centroid, const std::deque<PixelLocation>& EdgePixelLocations);

        static void ComputeAxes(const Vec2d Centroid, Vec2d& MainAxis, Vec2d& SecondaryAxis, const std::deque<PixelLocation>& EdgePixelLocations);

        static void ComputeEigenBoundaries(const Vec2d Centroid, Vec2d& MainAxis, Vec2d& SecondaryAxis, Vec2d& MinimalEigenPoint, Vec2d& MaximalEigenPoint, const std::deque<PixelLocation>& EdgePixelLocations);

        static void ComputeQuadrants(float NormalizedQuadrantOccupancy[4], float QuadrantRelativeArea[4], float QuadrantCoefficient[4], const Vec2d Centroid, const Vec2d MainAxis, const Vec2d SecondaryAxis, const Vec2d MinimalEigenPoint, const Vec2d MaximalEigenPoint, const std::deque<PixelLocation>& EdgePixelLocations);

        static void ComputeEigenPoints(const Vec2d Centroid, const Vec2d MainAxis, const Vec2d SecondaryAxis, const Vec2d MinimalEigenPoint, const Vec2d MaximalEigenPoint, Vec2d EigenCrossPoints[4], Vec2d EigenBoxPoints[4]);

        int m_Id;

        bool m_Active;

        Vec2d m_Centroid;

        Vec2d m_MainAxis;

        Vec2d m_SecondaryAxis;

        Vec2d m_MinimalEigenPoint;

        Vec2d m_MaximalEigenPoint;

        Vec2d m_EigenBoxPoints[4];

        Vec2d m_EigenCrossPoints[4];

        float m_QuadrantOccupancy[4];

        float m_QuadrantRelativeArea[4];

        float m_QuadrantCoefficient[4];

        BoundingBox2D m_BoundingBox;

        std::deque<PixelLocation> m_EdgePixelLocations;

        std::deque<CEdgeBlock*> m_ContainedComponents;
    };
}

