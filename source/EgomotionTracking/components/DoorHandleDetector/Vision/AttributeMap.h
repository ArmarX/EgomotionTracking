/*
 * AttributeMap.h
 * ============================================================================
 *  Author:     Dr.-Ing. David I. Gonzalez Aguirre
 *  Mail:       david.gonzalez@kit.edu
 * ============================================================================
 */

#pragma once

#include "Common.h"

namespace Vision
{
    class CAttributeMap
    {
    public:

        CAttributeMap();

        virtual ~CAttributeMap();

        bool SetAttribute(const std::string& AttributeName, const int ScalarIntegerValue, const bool Overwrite = true);

        bool SetAttribute(const std::string& AttributeName, const float ScalarRealValue, const bool Overwrite = true);

        bool SetAttribute(const std::string& AttributeName, const Vec2d& Vector2D, const bool Overwrite = true);

        bool SetAttribute(const std::string& AttributeName, const PointPair2d& PointPair2D, const bool Overwrite = true);

        bool SetAttribute(const std::string& AttributeName, const Vec3d& Vector3D, const bool Overwrite = true);

        bool SetAttribute(const std::string& AttributeName, const PointPair3d& PointPair3D, const bool Overwrite = true);

        bool SetAttribute(const std::string& AttributeName, const Mat2d& Matrix2D, const bool Overwrite = true);

        bool SetAttribute(const std::string& AttributeName, const Mat3d& Matrix3D, const bool Overwrite = true);

        int GetTotalAttributes() const;

        bool HasAttribute(const std::string& AttributeName) const;

        ValueContainer operator[](const std::string& AttributeName) const;

        ValueContainer GetAttributeValue(const std::string& AttributeName) const;

        bool GetAttributeValue(const std::string& AttributeName, int& ScalarIntegerValue) const;

        bool GetAttributeValue(const std::string& AttributeName, float& ScalarRealValue) const;

        bool GetAttributeValue(const std::string& AttributeName, Vec2d& Vector2D) const;

        bool GetAttributeValue(const std::string& AttributeName, PointPair2d& PointPair2D) const;

        bool GetAttributeValue(const std::string& AttributeName, Vec3d& Vector3D) const;

        bool GetAttributeValue(const std::string& AttributeName, PointPair3d& PointPair3D) const;

        bool GetAttributeValue(const std::string& AttributeName, Mat2d& Matrix2D) const;

        bool GetAttributeValue(const std::string& AttributeName, Mat3d& Matrix3D) const;

        const std::map<std::string, ValueContainer>& GetAttributeMap() const;

        std::map<std::string, ValueContainer>& GetAttributeMap();

    protected:

        std::map<std::string, ValueContainer> m_AttributeMap;
    };
}

