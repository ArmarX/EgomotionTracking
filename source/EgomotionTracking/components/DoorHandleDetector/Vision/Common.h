/*
 * Common.h
 * ============================================================================
 *  Author:     Dr.-Ing. David I. Gonzalez Aguirre
 *  Mail:       david.gonzalez@kit.edu
 * ============================================================================
 */

#pragma once

//STD LIBS
#include <map>
#include <list>
#include <deque>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <limits>
#include <sys/time.h>
#include <unistd.h>
#include <sstream>
#include <fstream>

//IVT LIB
#include <Image/ByteImage.h>
#include <Image/IntImage.h>
#include <Image/ImageProcessor.h>
#include <Image/ImageProcessorCV.h>
#include <Image/PrimitivesDrawer.h>
#include <Image/PrimitivesDrawerCV.h>
#include <Math/Math2d.h>
#include <Math/Math3d.h>
#include <Structs/Structs.h>
#include <Math/FloatMatrix.h>
#include <Structs/Structs.h>
#include <Calibration/Calibration.h>
#include <Calibration/StereoCalibration.h>

#define _INT_32_INF_POSITIVE_ 2147483647

#define _INT_32_INF_NEGATIVE_ -2147483648

#define _MAX_(A,B) ((A>B)?A:B)

#define _MIN_(A,B) ((A<B)?A:B)

#define _GENERIC_ERROR_ Vision::OnError( __FUNCTION__,__FILE__,__LINE__)

namespace Vision
{
    void OnError(const char* pFunctionCaller, const char* pFile, const int SourceCodeLine);

    enum RecognitionOutCome
    {
        eError, eNoRecognition, eSingleRecognition, eMultipleRecognition
    };

    enum MonocularSource
    {
        eLeftCamera = 0, eRightCamera = 1
    };

    bool IsValid(const MonocularSource Source);

    enum BayerPatternType
    {
        eGRBG = 0x2412, eRGGB = 0x4221, eBGGR = 0x1224, eGBRG = 0x2142
    };

    bool IsValid(const BayerPatternType BayerPattern);

    bool GetBayerPatternMap(const BayerPatternType BayerPattern, int BayerPatternMap[2][2]);

    struct ValueContainer
    {
        union GenericValue
        {
            int m_ScalarIntegerValue;

            float m_ScalarRealValue;

            Vec2d m_Vector2D;

            PointPair2d m_PointPair2D;

            Vec3d m_Vector3D;

            PointPair3d m_PointPair3D;

            Mat2d m_Matrix2D;

            Mat3d m_Matrix3D;
        };

        enum ValueType
        {
            eNone = -1,

            eScalarIntegerValue,

            eScalarRealValue,

            eVector2D,

            ePointPair2D,

            eVector3D,

            ePointPair3D,

            eMatrix2D,

            eMatrix3D
        };

        GenericValue m_Value;

        ValueType m_Type;

        inline ValueContainer() :
            m_Type(eNone)
        {
            memset(&m_Value, 0, sizeof(GenericValue));
        }
    };

    union RGBPixel
    {
        struct Elements
        {
            unsigned char m_R;

            unsigned char m_G;

            unsigned char m_B;
        };

        Elements m_Elements;

        unsigned char m_Channels[3];
    };

    struct PixelLocation
    {
        int m_X;

        int m_Y;
    };

    struct BoundingBox2D
    {
        inline BoundingBox2D() :
            m_X0(_INT_32_INF_POSITIVE_),
            m_Y0(_INT_32_INF_POSITIVE_),
            m_X1(_INT_32_INF_NEGATIVE_),
            m_Y1(_INT_32_INF_NEGATIVE_)
        {
        }

        inline BoundingBox2D(const int X0, const int Y0, const int X1, const int Y1) :
            m_X0(X0),
            m_Y0(Y0),
            m_X1(X1),
            m_Y1(Y1)
        {
        }

        inline void Set(const int X0, const int Y0, const int X1, const int Y1)
        {
            m_X0 = X0;

            m_Y0 = Y0;

            m_X1 = X1;

            m_Y1 = Y1;
        }

        inline void SetEmpty()
        {
            m_Y0 = m_X0 = _INT_32_INF_POSITIVE_;

            m_Y1 = m_X1 = _INT_32_INF_NEGATIVE_;
        }

        inline void Expand(const BoundingBox2D& BoundingBox)
        {
            if ((BoundingBox.m_X0 <= BoundingBox.m_X1) && (BoundingBox.m_Y0 <= BoundingBox.m_Y1))
            {
                m_X0 = _MIN_(BoundingBox.m_X0, m_X0);

                m_Y0 = _MIN_(BoundingBox.m_Y0, m_Y0);

                m_X1 = _MAX_(BoundingBox.m_X1, m_X1);

                m_Y1 = _MAX_(BoundingBox.m_Y1, m_Y1);
            }
        }

        inline BoundingBox2D Intersect(const BoundingBox2D& BoundingBox) const
        {
            if ((m_X0 > m_X1) || (BoundingBox.m_X0 > BoundingBox.m_X1) || (m_Y0 > m_Y1) || (BoundingBox.m_Y0 > BoundingBox.m_Y1))
            {
                return BoundingBox2D();
            }

            return BoundingBox2D(_MAX_(m_X0, BoundingBox.m_X0), _MAX_(m_Y0, BoundingBox.m_Y0), _MIN_(m_X1, BoundingBox.m_X1), _MIN_(m_Y1, BoundingBox.m_Y1));
        }

        inline void Expand(const int X, const int Y)
        {
            if (X > m_X1)
            {
                m_X1 = X;
            }

            if (X < m_X0)
            {
                m_X0 = X;
            }

            if (Y > m_Y1)
            {
                m_Y1 = Y;
            }

            if (Y < m_Y0)
            {
                m_Y0 = Y;
            }
        }

        inline void FastExpand(const int X, const int Y)
        {
            if (X > m_X1)
            {
                m_X1 = X;
            }
            else if (X < m_X0)
            {
                m_X0 = X;
            }

            if (Y > m_Y1)
            {
                m_Y1 = Y;
            }
            else if (Y < m_Y0)
            {
                m_Y0 = Y;
            }
        }

        inline bool IsEmpty() const
        {
            return (m_X0 > m_X1) || (m_Y0 > m_Y1);
        }

        inline bool IsInside(const int X, const int Y) const
        {
            return ((m_X0 > m_X1) || (m_Y0 > m_Y1)) ? false : (X >= m_X0) && (X <= m_X1) && (Y >= m_Y0) && (Y <= m_Y1);
        }

        inline bool IsInsideNoCheck(const int X, const int Y) const
        {
            return (X >= m_X0) && (X <= m_X1) && (Y >= m_Y0) && (Y <= m_Y1);
        }

        inline bool Containes(const BoundingBox2D* pBoundingBox) const
        {
            return (pBoundingBox->m_X0 > m_X0) && (pBoundingBox->m_X1 < m_X1) && (pBoundingBox->m_Y0 > m_Y0) && (pBoundingBox->m_Y1 < m_Y1);
        }

        inline bool AddOffset(const int Offset)
        {
            if ((m_X0 > m_X1) || (m_Y0 > m_Y1))
            {
                return false;
            }

            m_X0 -= Offset;

            m_Y0 -= Offset;

            m_X1 += Offset;

            m_Y1 += Offset;

            return true;
        }

        inline int GetWidth() const
        {
            return m_X1 - m_X0 + 1;
        }

        inline int GetHeight() const
        {
            return m_Y1 - m_Y0 + 1;
        }

        inline int GetArea() const
        {
            return (m_X1 - m_X0 + 1) * (m_Y1 - m_Y0 + 1);
        }

        inline float GetCenterX() const
        {
            return float(m_X1 + m_X0) * 0.5f;
        }

        inline float GetCenterY() const
        {
            return float(m_Y1 + m_Y0) * 0.5f;
        }

        void GetBoxPoints(Vec2d Points[4], const float Offset = 0.0f) const
        {
            Points[0].x = float(m_X0) - Offset, Points[0].y = float(m_Y0) - Offset;

            Points[1].x = float(m_X1) + Offset, Points[1].y = float(m_Y0) - Offset;

            Points[2].x = float(m_X1) + Offset, Points[2].y = float(m_Y1) + Offset;

            Points[3].x = float(m_X0) - Offset, Points[3].y = float(m_Y1) + Offset;
        }

        int m_X0;

        int m_Y0;

        int m_X1;

        int m_Y1;
    };

    struct StereoTriangulationPoint
    {
        static bool SortSimilarityPredicate(const StereoTriangulationPoint& lhs, const StereoTriangulationPoint& rhs)
        {
            return lhs.m_Similarity > rhs.m_Similarity;
        }

        static bool SortTriangulationDeviationPredicate(const StereoTriangulationPoint& lhs, const StereoTriangulationPoint& rhs)
        {
            return lhs.m_TriangulationAbsoluteDeviation < rhs.m_TriangulationAbsoluteDeviation;
        }

        int m_Id;

        Vec2d m_LeftImagePoint;

        Vec2d m_RightImagePoint;

        Vec3d m_SpacePoint;

        PointPair3d m_EstimatedPointsLine;

        float m_EpipolarDeviation;

        float m_TriangulationAbsoluteDeviation;

        float m_Similarity;
    };

    struct SpaceLineSegment
    {
        int m_Id;

        StereoTriangulationPoint m_PointA;

        StereoTriangulationPoint m_PointB;

        Vec2d m_ImageDirection[2];

        Vec2d m_ImageCentralPoint[2];

        Vec3d m_SpaceCentralPoint;

        Vec3d m_SpaceDirection;

        float m_UncertaintyRadiusA;

        float m_UncertaintyRadiusB;

        float m_CentralUncertaintyRadius;

        float m_MaxmimalSpaceLength;

        float m_MinimalSpaceLength;

        float m_SpaceLength;

        float m_LeftImageLength;

        float m_RightImageLength;

        float m_ImageOrientationCoherence;
    };
}


