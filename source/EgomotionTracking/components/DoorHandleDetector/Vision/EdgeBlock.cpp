/*
 * EdgeBlock.cpp
 * ============================================================================
 *  Author:     Dr.-Ing. David I. Gonzalez Aguirre
 *  Mail:       david.gonzalez@kit.edu
 * ============================================================================
 */

#include "EdgeBlock.h"

#define _DOT_PRODUCT_2D_(A,B) (A.x * B.x + A.y * B.y)

namespace Vision
{
    CEdgeBlock::CEdgeBlock(const PixelLocation& Location) :
        m_Id(-1),
        m_Active(false),
        m_BoundingBox(Location.m_X, Location.m_Y, Location.m_X, Location.m_Y),
        m_EdgePixelLocations(),
        m_ContainedComponents()
    {
        m_EdgePixelLocations.push_back(Location);
    }

    bool CEdgeBlock::ScaledDrop(CByteImage* pImage, const int BitShifts, const int Margin, unsigned char Value) const
    {
        if (!pImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        const int SubWidth = pImage->width - 1;

        const int SubHeight = pImage->height - 1;

        std::deque<PixelLocation>::const_iterator EndPixelLocations = m_EdgePixelLocations.end();

        for (std::deque<PixelLocation>::const_iterator pPixelLocation = m_EdgePixelLocations.begin(); pPixelLocation != EndPixelLocations; ++pPixelLocation)
        {
            const int X0 = std::max((pPixelLocation->m_X >> BitShifts) - Margin, 0);

            const int MaskWidth = std::min((pPixelLocation->m_X >> BitShifts) + Margin, SubWidth) - X0 + 1;

            const int Y1 = std::min((pPixelLocation->m_Y >> BitShifts) + Margin, SubHeight);

            for (int YS = std::max((pPixelLocation->m_Y >> BitShifts) - Margin, 0); YS <= Y1; ++YS)
            {
                memset(pImage->pixels + (pImage->width * YS) + X0, Value, MaskWidth);
            }
        }

        return true;
    }

    bool CEdgeBlock::Draw(const CByteImage* pSourceImage, CByteImage* pDestiantionImage) const
    {
        if (!pSourceImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!pDestiantionImage)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pDestiantionImage->type != CByteImage::eRGB24)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pSourceImage->width != pDestiantionImage->width)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (pSourceImage->height != pDestiantionImage->height)
        {
            _GENERIC_ERROR_;

            return false;
        }

        if (!ImageProcessor::ConvertImage(pSourceImage, pDestiantionImage))
        {
            _GENERIC_ERROR_;

            return false;
        }

        int X0 = 0, Y0 = 0, X1 = 0, Y1 = 0;

        GetFullZone(X0, Y0, X1, Y1);

        Vec2d Center;

        Center.x = float(X1 + X0) * 0.5f;

        Center.y = float(Y1 + Y0) * 0.5f;

        const float Radius = ceil(hypotf(Y1 - Y0 + 1, X1 - X0 + 1) * 0.5f) + 5.0f;

        Vec2d BoundingPoints[4];

        m_BoundingBox.GetBoxPoints(BoundingPoints, 3.0f);

        for (int i = 0; i < 2; ++i)
        {
            if (m_Active)
            {
                PrimitivesDrawer::DrawCircle(pDestiantionImage, Center, Radius, 0, 255, 0, 2, true);
            }
            else
            {
                PrimitivesDrawer::DrawCircle(pDestiantionImage, Center, Radius, 255, 0, 0, 1, true);
            }

            PrimitivesDrawer::DrawCircle(pDestiantionImage, m_EigenCrossPoints[0], 2, 255, 0, 0, 2, false);

            PrimitivesDrawer::DrawCircle(pDestiantionImage, m_EigenCrossPoints[1], 2, 0, 64, 0, 2, false);

            PrimitivesDrawer::DrawCircle(pDestiantionImage, m_EigenCrossPoints[2], 2, 64, 0, 0, 2, false);

            PrimitivesDrawer::DrawCircle(pDestiantionImage, m_EigenCrossPoints[3], 2, 0, 255, 0, 2, false);

            PrimitivesDrawer::DrawLine(pDestiantionImage, m_EigenCrossPoints[0], m_EigenCrossPoints[2], 128, 0, 0, 1);

            PrimitivesDrawer::DrawLine(pDestiantionImage, m_EigenCrossPoints[3], m_EigenCrossPoints[1], 0, 128, 0, 1);

            for (int j = 0; j < 4; ++j)
            {
                PrimitivesDrawer::DrawLine(pDestiantionImage, BoundingPoints[j], BoundingPoints[(j + 1) % 4], 255, 0, 255, 1);

                PrimitivesDrawer::DrawLine(pDestiantionImage, m_EigenBoxPoints[j], m_EigenBoxPoints[(j + 1) % 4], 0, 255, 255, 1);
            }

            for (int j = 0; j < 3; ++j)
            {
                ImageProcessor::GaussianSmooth5x5(pDestiantionImage, pDestiantionImage);
            }
        }

        if (m_Active)
        {
            PrimitivesDrawer::DrawCircle(pDestiantionImage, Center, Radius, 0, 255, 0, 2, true);
        }
        else
        {
            PrimitivesDrawer::DrawCircle(pDestiantionImage, Center, Radius, 255, 0, 0, 1, true);
        }

        PrimitivesDrawer::DrawCircle(pDestiantionImage, m_EigenCrossPoints[0], 2, 255, 0, 0, 2, false);

        PrimitivesDrawer::DrawCircle(pDestiantionImage, m_EigenCrossPoints[2], 2, 64, 0, 0, 2, false);

        PrimitivesDrawer::DrawCircle(pDestiantionImage, m_EigenCrossPoints[3], 2, 0, 255, 0, 2, false);

        PrimitivesDrawer::DrawCircle(pDestiantionImage, m_EigenCrossPoints[1], 2, 0, 64, 0, 2, false);

        PrimitivesDrawer::DrawLine(pDestiantionImage, m_EigenCrossPoints[0], m_EigenCrossPoints[2], 128, 0, 0, 1);

        PrimitivesDrawer::DrawLine(pDestiantionImage, m_EigenCrossPoints[3], m_EigenCrossPoints[1], 0, 128, 0, 1);

        for (int i = 0; i < 4; ++i)
        {
            PrimitivesDrawer::DrawLine(pDestiantionImage, BoundingPoints[i], BoundingPoints[(i + 1) % 4], 255, 0, 255, 1);

            PrimitivesDrawer::DrawLine(pDestiantionImage, m_EigenBoxPoints[i], m_EigenBoxPoints[(i + 1) % 4], 0, 255, 255, 1);
        }

        std::deque<PixelLocation>::const_iterator EndPixelLocations = m_EdgePixelLocations.end();

        for (std::deque<PixelLocation>::const_iterator pPixelLocation = m_EdgePixelLocations.begin(); pPixelLocation != EndPixelLocations; ++pPixelLocation)
        {
            memset(pDestiantionImage->pixels + (((pDestiantionImage->width * pPixelLocation->m_Y) + pPixelLocation->m_X) * pDestiantionImage->bytesPerPixel), 255, pDestiantionImage->bytesPerPixel);
        }

        return true;
    }

    void CEdgeBlock::ComputeCentroid(Vec2d& Centroid, const std::deque<PixelLocation>& EdgePixelLocations)
    {
        long int AX = 0, AY = 0;

        std::deque<PixelLocation>::const_iterator EndPixelLocations = EdgePixelLocations.end();

        for (std::deque<PixelLocation>::const_iterator pPixelLocation = EdgePixelLocations.begin() ; pPixelLocation != EndPixelLocations ; ++pPixelLocation)
        {
            AX += pPixelLocation->m_X;

            AY += pPixelLocation->m_Y;
        }

        Centroid.x = float(AX) / float(EdgePixelLocations.size());

        Centroid.y = float(AY) / float(EdgePixelLocations.size());
    }

    void CEdgeBlock::ComputeAxes(const Vec2d Centroid, Vec2d& MainAxis, Vec2d& SecondaryAxis, const std::deque<PixelLocation>& EdgePixelLocations)
    {
        float AXX = 0.0f, AXY = 0.0f, AYY = 0.0f;

        std::deque<PixelLocation>::const_iterator EndPixelLocations = EdgePixelLocations.end();

        for (std::deque<PixelLocation>::const_iterator pPixelLocation = EdgePixelLocations.begin() ; pPixelLocation != EndPixelLocations ; ++pPixelLocation)
        {
            const float DX = float(pPixelLocation->m_X) - Centroid.x;

            const float DY = float(pPixelLocation->m_Y) - Centroid.y;

            AXX += DX * DX;

            AXY += DX * DY;

            AYY += DY * DY;
        }

        //See reference:
        //http://www.math.harvard.edu/archive/21b_fall_04/exhibits/2dmatrices/index.html
        //Faster than EIGEN

        if (fabs(AXY) > FLT_EPSILON)
        {
            const float T = (AXX + AYY) * 0.5f;

            const float A = (T * T) - (AXX * AYY - AXY * AXY);

            if (A < 0.0)
            {
                _GENERIC_ERROR_;

                return;
            }

            MainAxis.x = T + sqrt(A) - AYY;

            MainAxis.y = AXY;

            const float Magnitude = sqrt(MainAxis.x * MainAxis.x + MainAxis.y * MainAxis.y);

            MainAxis.x /= Magnitude;

            MainAxis.y /= Magnitude;

            SecondaryAxis.y = MainAxis.x;

            SecondaryAxis.x = -MainAxis.y;
        }
        else
        {
            SecondaryAxis.y = MainAxis.x = 1.0;

            SecondaryAxis.x = MainAxis.y = 0.0;
        }
    }

    void CEdgeBlock::ComputeEigenBoundaries(const Vec2d Centroid, Vec2d& MainAxis, Vec2d& SecondaryAxis, Vec2d& MinimalEigenPoint, Vec2d& MaximalEigenPoint, const std::deque<PixelLocation>& EdgePixelLocations)
    {
        Vec2d Point, MappedPoint;

        MinimalEigenPoint = Math2d::zero_vec;

        MaximalEigenPoint = Math2d::zero_vec;

        std::deque<PixelLocation>::const_iterator EndPixelLocations = EdgePixelLocations.end();

        for (std::deque<PixelLocation>::const_iterator pPixelLocation = EdgePixelLocations.begin() ; pPixelLocation != EndPixelLocations ; ++pPixelLocation)
        {
            Point.x = float(pPixelLocation->m_X) - Centroid.x;

            Point.y = float(pPixelLocation->m_Y) - Centroid.y;

            MappedPoint.x = _DOT_PRODUCT_2D_(Point, MainAxis);

            MappedPoint.y = _DOT_PRODUCT_2D_(Point, SecondaryAxis);

            if (MappedPoint.x < MinimalEigenPoint.x)
            {
                MinimalEigenPoint.x = MappedPoint.x;
            }
            else if (MappedPoint.x > MaximalEigenPoint.x)
            {
                MaximalEigenPoint.x = MappedPoint.x;
            }

            if (MappedPoint.y < MinimalEigenPoint.y)
            {
                MinimalEigenPoint.y = MappedPoint.y;
            }
            else if (MappedPoint.y > MaximalEigenPoint.y)
            {
                MaximalEigenPoint.y = MappedPoint.y;
            }
        }

        if ((MaximalEigenPoint.y - MinimalEigenPoint.y) > (MaximalEigenPoint.x - MinimalEigenPoint.x))
        {
            std::swap(MainAxis, SecondaryAxis);

            std::swap(MinimalEigenPoint.x, MinimalEigenPoint.y);

            std::swap(MaximalEigenPoint.x, MaximalEigenPoint.y);
        }
    }

    void CEdgeBlock::ComputeQuadrants(float NormalizedQuadrantOccupancy[4], float QuadrantRelativeArea[4], float QuadrantCoefficient[4], const Vec2d Centroid, const Vec2d MainAxis, const Vec2d SecondaryAxis, const Vec2d MinimalEigenPoint, const Vec2d MaximalEigenPoint, const std::deque<PixelLocation>& EdgePixelLocations)
    {
        Vec2d Point;

        int QuadrantOccupancy[4] = { 0, 0, 0, 0 };

        std::deque<PixelLocation>::const_iterator EndPixelLocations = EdgePixelLocations.end();

        for (std::deque<PixelLocation>::const_iterator pPixelLocation = EdgePixelLocations.begin() ; pPixelLocation != EndPixelLocations ; ++pPixelLocation)
        {
            Point.x = float(pPixelLocation->m_X) - Centroid.x;

            Point.y = float(pPixelLocation->m_Y) - Centroid.y;

            ++QuadrantOccupancy[(_DOT_PRODUCT_2D_(Point, MainAxis) >= 0.0f) ? ((_DOT_PRODUCT_2D_(Point, SecondaryAxis) >= 0.0f) ? 0 : 3) : ((_DOT_PRODUCT_2D_(Point, SecondaryAxis) >= 0.0f) ? 1 : 2)];
        }

        const float TotalPixels = EdgePixelLocations.size();

        NormalizedQuadrantOccupancy[0] = float(QuadrantOccupancy[0]) / TotalPixels;

        NormalizedQuadrantOccupancy[1] = float(QuadrantOccupancy[1]) / TotalPixels;

        NormalizedQuadrantOccupancy[2] = float(QuadrantOccupancy[2]) / TotalPixels;

        NormalizedQuadrantOccupancy[3] = float(QuadrantOccupancy[3]) / TotalPixels;

        const float Area = (MaximalEigenPoint.x - MinimalEigenPoint.x) * (MaximalEigenPoint.y - MinimalEigenPoint.y);

        QuadrantRelativeArea[0] = (MaximalEigenPoint.x * MaximalEigenPoint.y) / Area;

        QuadrantRelativeArea[1] = (-MinimalEigenPoint.x * MaximalEigenPoint.y) / Area;

        QuadrantRelativeArea[2] = (MinimalEigenPoint.x * MinimalEigenPoint.y) / Area;

        QuadrantRelativeArea[3] = (MaximalEigenPoint.x * -MinimalEigenPoint.y) / Area;

        QuadrantCoefficient[0] = QuadrantOccupancy[0] / QuadrantRelativeArea[0];

        QuadrantCoefficient[1] = QuadrantOccupancy[1] / QuadrantRelativeArea[1];

        QuadrantCoefficient[2] = QuadrantOccupancy[2] / QuadrantRelativeArea[2];

        QuadrantCoefficient[3] = QuadrantOccupancy[3] / QuadrantRelativeArea[3];

        const float MaximalCoefficient = _MAX_(_MAX_(QuadrantCoefficient[0], QuadrantCoefficient[1]), _MAX_(QuadrantCoefficient[2], QuadrantCoefficient[3]));

        const float MinimalCoefficient = _MIN_(_MIN_(QuadrantCoefficient[0], QuadrantCoefficient[1]), _MIN_(QuadrantCoefficient[2], QuadrantCoefficient[3]));

        const float CoefficientRange = MaximalCoefficient - MinimalCoefficient;

        QuadrantCoefficient[0] = (QuadrantCoefficient[0] - MinimalCoefficient) / CoefficientRange;

        QuadrantCoefficient[1] = (QuadrantCoefficient[1] - MinimalCoefficient) / CoefficientRange;

        QuadrantCoefficient[2] = (QuadrantCoefficient[2] - MinimalCoefficient) / CoefficientRange;

        QuadrantCoefficient[3] = (QuadrantCoefficient[3] - MinimalCoefficient) / CoefficientRange;
    }

    void CEdgeBlock::ComputeEigenPoints(const Vec2d Centroid, const Vec2d MainAxis, const Vec2d SecondaryAxis, const Vec2d MinimalEigenPoint, const Vec2d MaximalEigenPoint, Vec2d EigenCrossPoints[4], Vec2d EigenBoxPoints[4])
    {
        Vec2d MainAxisNegative, MainAxisPositive, SecondaryAxisNegative, SecondaryAxisPositive;

        Math2d::MulVecScalar(MainAxis, MinimalEigenPoint.x, MainAxisNegative);

        Math2d::MulVecScalar(MainAxis, MaximalEigenPoint.x, MainAxisPositive);

        Math2d::MulVecScalar(SecondaryAxis, MinimalEigenPoint.y, SecondaryAxisNegative);

        Math2d::MulVecScalar(SecondaryAxis, MaximalEigenPoint.y, SecondaryAxisPositive);

        Math2d::AddVecVec(Centroid, MainAxisPositive, EigenCrossPoints[0]);

        Math2d::AddVecVec(Centroid, SecondaryAxisNegative, EigenCrossPoints[1]);

        Math2d::AddVecVec(Centroid, MainAxisNegative, EigenCrossPoints[2]);

        Math2d::AddVecVec(Centroid, SecondaryAxisPositive, EigenCrossPoints[3]);

        Math2d::AddVecVec(EigenCrossPoints[0], SecondaryAxisNegative, EigenBoxPoints[0]);

        Math2d::AddVecVec(EigenCrossPoints[2], SecondaryAxisNegative, EigenBoxPoints[1]);

        Math2d::AddVecVec(EigenCrossPoints[2], SecondaryAxisPositive, EigenBoxPoints[2]);

        Math2d::AddVecVec(EigenCrossPoints[0], SecondaryAxisPositive, EigenBoxPoints[3]);
    }

    void CEdgeBlock::BasicCharacterize()
    {
        memset(&m_Centroid, 0, sizeof(Vec2d));

        memset(&m_MainAxis, 0, sizeof(Vec2d));

        memset(&m_SecondaryAxis, 0, sizeof(Vec2d));

        memset(&m_MinimalEigenPoint, 0, sizeof(Vec2d));

        memset(&m_MaximalEigenPoint, 0, sizeof(Vec2d));

        memset(m_EigenBoxPoints, 0, sizeof(Vec2d) * 4);

        memset(m_EigenCrossPoints, 0, sizeof(Vec2d) * 4);

        memset(m_QuadrantOccupancy, 0, sizeof(float) * 4);

        memset(m_QuadrantRelativeArea, 0, sizeof(float) * 4);

        memset(m_QuadrantCoefficient, 0, sizeof(float) * 4);

        ComputeCentroid(m_Centroid, m_EdgePixelLocations);

        ComputeAxes(m_Centroid, m_MainAxis, m_SecondaryAxis, m_EdgePixelLocations);

        ComputeEigenBoundaries(m_Centroid, m_MainAxis, m_SecondaryAxis, m_MinimalEigenPoint, m_MaximalEigenPoint, m_EdgePixelLocations);
    }

    void CEdgeBlock::ExtendedCharacterize()
    {
        ComputeQuadrants(m_QuadrantOccupancy, m_QuadrantRelativeArea, m_QuadrantCoefficient, m_Centroid, m_MainAxis, m_SecondaryAxis, m_MinimalEigenPoint, m_MaximalEigenPoint, m_EdgePixelLocations);

        ComputeEigenPoints(m_Centroid, m_MainAxis, m_SecondaryAxis, m_MinimalEigenPoint, m_MaximalEigenPoint, m_EigenCrossPoints, m_EigenBoxPoints);
    }

    void CEdgeBlock::GetFullZone(int& X0, int& Y0, int& X1, int& Y1) const
    {
        float FZX0 = FLT_MAX;

        float FZY0 = FLT_MAX;

        float FZX1 = -FLT_MAX;

        float FZY1 = -FLT_MAX;

        for (int i = 0 ; i < 4 ; ++i)
        {
            FZX0 = _MIN_(_MIN_(m_EigenBoxPoints[i].x, m_EigenCrossPoints[i].x), FZX0);

            FZY0 = _MIN_(_MIN_(m_EigenBoxPoints[i].y, m_EigenCrossPoints[i].y), FZY0);

            FZX1 = _MAX_(_MAX_(m_EigenBoxPoints[i].x, m_EigenCrossPoints[i].x), FZX1);

            FZY1 = _MAX_(_MAX_(m_EigenBoxPoints[i].y, m_EigenCrossPoints[i].y), FZY1);
        }

        X0 = int(FZX0);

        Y0 = int(FZY0);

        X1 = int(ceil(FZX1));

        Y1 = int(ceil(FZY1));
    }
}
