/*
 * StereoObjectRecognizer.h
 * ============================================================================
 *  Author:     Dr.-Ing. David I. Gonzalez Aguirre
 *  Mail:       david.gonzalez@kit.edu
 * ============================================================================
 */

#pragma once

#include "Common.h"
#include "About.h"
#include "ObjectDescription.h"

namespace Vision
{
    class CStereoObjectRecognizer
    {
    public:

        CStereoObjectRecognizer(const CStereoCalibration* pStereoCalibration, const CByteImage** ppInputImages, CByteImage** ppOutputImages);

        virtual ~CStereoObjectRecognizer();

        bool IsInitialized() const;

        bool IsRecognizing() const;

        const CStereoCalibration* GetStereoCalibration() const;

        const CCalibration* GetMonocularCalibrations(const MonocularSource Source) const;

        const CByteImage** GetInputImages() const;

        const CByteImage** GetOutputImages() const;

        CByteImage** GetOutputImages();

        virtual RecognitionOutCome Recognize(const CObjectDescription* pObjectDescription, const bool ImproveImageReconstruction, const bool DisplayOutput, const int VerboseLevel);

        bool SetPartialResultsDirectory(const std::string& PartialResultsDirectory);

        const std::string& GetPartialResultsDirectory() const;

        void SetSavePartialResults(const bool Enabled);

        bool IsSavingPartialResults() const;

        static void About();

    protected:

        CStereoCalibration* GetStereoCalibration();

        virtual RecognitionOutCome Execute(const CObjectDescription* pObjectDescription, const bool UsingImproveImageReconstruction, const int VerboseLevel) = 0;

        virtual bool Displaying(const RecognitionOutCome CurrentOutCome) = 0;

        bool Initialize();

        bool CreateInternalImages();

        bool DestroyInternalImages();

        bool LoadInputImages(const bool ImproveImageReconstruction = false);

        bool DropOutputImages();

        const CByteImage* GetInternalInputImage(const MonocularSource Source) const;

        CByteImage* GetInternalOutputImage(const MonocularSource Source);

        bool GetInputImageDimensions(const MonocularSource Source, int& Width, int& Height);

        bool GetOutputImageDimensions(const MonocularSource Source, int& Width, int& Height);

        const BoundingBox2D* GetInputActiveRegion(const MonocularSource Source) const;

        BoundingBox2D* GetInputActiveRegion(const MonocularSource Source);

        std::string CreateFileName(const std::string& Directory, const MonocularSource Source, const std::string& Content, const std::string& Extension, const int Counter) const;

        static void StartTimeStamp(timeval& T);

        const timeval& StartTimeStamp();

        long int StopTimeStamp();

        long int StopTimeStamp(const timeval& T0);

        static long int StopTimeStamp(const timeval& T0, timeval& T1);

        static bool DetectBayerPatternType(const CByteImage* pRGBInputImage, BayerPatternType& BayerPattern, const int VerbosityLevel, const bool SaveImages, const std::string& Directory);

        static bool Mosaicing(const CByteImage* pRGBInputImage, const BoundingBox2D& InputActiveZone, const BayerPatternType BayerPattern, CByteImage* pBayerPatternOutputImage, BoundingBox2D& OutputActiveZone);

        static inline float GradientWeightingKernel(const float Gradient)
        {
            return 1.0f / (1.0f + Gradient);
        }

        static void LoadAvailableChannel(const CByteImage* pBayerPatternInputImage, const BoundingBox2D& OutZone, CByteImage* pRGBOutputImage, const int BayerPatternMap[2][2]);

        static void EstimateGreenChannel(const CByteImage* pBayerPatternInputImage, CByteImage* pRGBOutputImage, const BoundingBox2D& OutZone, const int BayerPatternMap[2][2], const float LuminanceFactor, const float DW2_ChromaticFactor);

        static void EstimateCrossDiagonalRedBlueChannels(const CByteImage* pBayerPatternInputImage, CByteImage* pRGBOutputImage, const BoundingBox2D& OutZone, const int BayerPatternMap[2][2], const float DW22_ChromaticFactor, const float DW11_LuminanceFactor);

        static void EstimateCrossAlignedRedBlueChannels(const CByteImage* pBayerPatternInputImage, CByteImage* pRGBOutputImage, const BoundingBox2D& OutZone, const int BayerPatternMap[2][2], const float DW2_LuminanceFactor, const float ChromaticFactor);

        static bool DirectionalGradientDemosaicing(const CByteImage* pBayerPatternInputImage, const BoundingBox2D& InZone, const BayerPatternType BayerPattern, CByteImage* pRGBOutputImage, BoundingBox2D& OutZone, const float LuminanceFactor, const float ChromaticFactor);

        static bool Demosaicing(const CByteImage* pBayerPatternInputImage, const BoundingBox2D& InputActiveZone, const BayerPatternType BayerPattern, CByteImage* pRGBOutputImage, BoundingBox2D& OutputActiveZone);

        static bool ComputeAbsolutDifference(const CByteImage* pRGBInputImageA, const CByteImage* pRGBInputImageB, const BoundingBox2D& CommonActiveZone, long int& AbsolutDifference);

    private:

        bool m_IsInitialized;

        volatile bool m_IsRecognizing;

        const CStereoCalibration* m_pStereoCalibration;

        const CCalibration* m_pMonocularCalibrations[2];

        const CByteImage** m_ppInputImages;

        BayerPatternType m_BayerPatterns[2];

        CByteImage** m_ppOutputImages;

        CByteImage** m_ppInternalInputImages;

        CByteImage** m_ppInternalOutputImages;

        BoundingBox2D m_InputActiveRegion[2];

        bool m_IsSavingPartialResults;

        std::string m_PartialResultsDirectory;

        timeval m_T0;

        timeval m_T1;
    };
}

