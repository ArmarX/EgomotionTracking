/*
 * MobileKitchenHandleRecognizer.h
 * ============================================================================
 *  Author:     Dr.-Ing. David I. Gonzalez Aguirre
 *  Mail:       david.gonzalez@kit.edu
 * ============================================================================
 */

#pragma once

#include "Common.h"
#include "EdgeBlock.h"
#include "StereoObjectRecognizer.h"

/*
        ____________
       /\  ________ \
      /  \ \______/\ \
     / /\ \ \  / /\ \ \
    / / /\ \ \/ / /\ \ \
   / / /__\ \ \/_/__\_\ \__________
  / /_/____\ \__________  ________ \
  \ \ \____/ / ________/\ \______/\ \
   \ \ \  / / /\ \  / /\ \ \  / /\ \ \
    \ \ \/ / /\ \ \/ / /\ \ \/ / /\ \ \
     \ \/ / /__\_\/ / /__\ \ \/_/__\_\ \
      \  /_/______\/_/____\ \___________\
      /  \ \______/\ \____/ / ________  /
     / /\ \ \  / /\ \ \  / / /\ \  / / /
    / / /\ \ \/ / /\ \ \/ / /\ \ \/ / /
   / / /__\ \ \/_/__\_\/ / /__\_\/ / /
  / /_/____\ \_________\/ /______\/ /
  \ \ \____/ / ________  __________/
   \ \ \  / / /\ \  / / /
    \ \ \/ / /\ \ \/ / /
     \ \/ / /__\_\/ / /
      \  / /______\/ /
       \/___________/

*/

//All units in millimeters, radians and 8-bit intensity values and pixels.

#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_DEFAULT_AMOUNT_HARRIS_POINTS_             10000
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_AMOUNT_HARRIS_POINTS_             100
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MAXIMAL_AMOUNT_HARRIS_POINTS_             20000
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_DEFAULT_KERNEL_MINIMAL_DENSITY_           0.01f
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_DEFAULT_KERNEL_INTERNAL_FACTOR_           5.0f
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_DEFAULT_KERNEL_RADIUS_                    9
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_KERNEL_RADIUS_                    3
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MAXIMAL_KERNEL_RADIUS_                    16
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_LOWER_CANNY_THRESHOLD_            8
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MAXIMAL_LOWER_CANNY_THRESHOLD_            48
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_UPPER_CANNY_THRESHOLD_            16
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MAXIMAL_UPPER_CANNY_THRESHOLD_            64
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_LOW_RESOLUTION_MASK_FACTOR_       1
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_LOW_RESOLUTION_MASK_MARGIN_       2
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MINIMAL_DIFFUSION_VARIANCE_               1.0f
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_MAXIMAL_DIFFUSION_VARIANCE_               10.0f
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_CONTEXT_                                  "H2T_MOBILE_KITCHEN"
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_CLASS_                                    "DOOR_HANDLER"
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_CLASS_ATTRIBUTE_MAIN_EDGE_LENGTH_         "MAIN_EDGE_LENGTH"
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_CLASS_ATTRIBUTE_SECONDARY_EDGE_LENGTH_    "SECONDARY_EDGE_LENGTH"
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_INTANCE_LEFT_DRAWER_HANDLER_              "LEFT_DRAWER_HANDLER"
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_INTANCE_RIGHT_DRAWER_HANDLER_             "RIGHT_DRAWER_HANDLER"
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_INTANCE_SINK_DOOR_HANDLER_                "SINK_DOOR_HANDLER"
#define _MOBILE_KITCHEN_HANDLE_RECOGNIZER_INTANCE_FRIDGE_DOOR_HANDLER_              "FRIDGE_DOOR_HANDLER"

namespace Vision
{
    class CMobileKitchenHandleRecognizer : public CStereoObjectRecognizer
    {
    public:

        struct MobileKitchenHandle
        {
            inline MobileKitchenHandle() :
                m_Id(-1),
                m_ClusteredElements(0)
            {
                memset(m_ImageMeanPointA, 0, sizeof(Vec2d) * 2);

                memset(m_ImageMeanPointB, 0, sizeof(Vec2d) * 2);

                memset(&m_SpaceMeanPointA, 0, sizeof(Vec3d));

                memset(&m_SpaceMeanPointB, 0, sizeof(Vec3d));

                memset(&m_SpaceMeanPointCentral, 0, sizeof(Vec3d));
            }

            int m_Id;

            BoundingBox2D m_ImageBoundingBox[2];

            BoundingBox2D m_ImageBoundingBoxPointA[2];

            BoundingBox2D m_ImageBoundingBoxPointB[2];

            Vec2d m_ImageMeanPointA[2];

            Vec2d m_ImageMeanPointB[2];

            Vec3d m_SpaceMeanPointA;

            Vec3d m_SpaceMeanPointB;

            Vec3d m_SpaceMeanPointCentral;

            int m_ClusteredElements;
        };

        struct ParameterSet
        {
            struct ProcessImagesParameterSet
            {
                int m_CannyLowThreshold;

                int m_CannyHighThreshold;

                int m_MinimalBlockSize;

                int m_MinimalBlockDimension;

                bool m_IsHorizontal;

                float m_MaximalEigenRatio;

                float m_MaximalDeviation;

                int m_VerbosityLevel;

                bool m_SavePartialResults;
            };

            struct ProcessPointsParameterSet
            {
                float m_HarrisSaliency[2];

                float m_HarrisDominanceDistance[2];

                float m_MaximalSearchDepth;

                float m_MinimalSearchDepth;

                float m_MaximalEpipolarDeviation;

                float m_MaximalTriangulationDeviation;

                float m_MinimalPatchSimilarity;

                int m_VerbosityLevel;

                bool m_SavePartialResults;
            };

            struct ExtractLinesParameterSet
            {
                bool m_IsHorizontal;

                float m_ImageMinimalLength;

                float m_ImageMaximalLength;

                float m_MinimalImageMaximalLengthRatio;

                float m_MinimalImageOrientationCoherence;

                float m_SpaceMinimalLength;

                float m_SpaceMaximalLength;

                float m_EdgeMargin;

                int m_MaximalPointsAlongLineSegment;

                float m_PointsAlongLineSegmentMargin;

                int m_VerbosityLevel;

                bool m_SavePartialResults;
            };

            struct MatchingParameterSet
            {
                float m_ClusteringImageBandwidth;

                float m_MinimalSpaceOrientationCoherence;

                float m_ClusteringSpatialBandwidth;

                int m_VerbosityLevel;

                bool m_SavePartialResults;
            };

            bool m_IsValid;

            ProcessImagesParameterSet m_ProcessImagesParameterSet;

            ProcessPointsParameterSet m_ProcessPointsParameterSet;

            ExtractLinesParameterSet m_ExtractLinesParameterSet;

            MatchingParameterSet m_MatchingParameterSet;
        };

        CMobileKitchenHandleRecognizer(const CStereoCalibration* pStereoCalibration, const CByteImage** ppInputImages, CByteImage** ppOutputImages, const int VerbosityLevel = 0);

        ~CMobileKitchenHandleRecognizer() override;

        const std::vector<MobileKitchenHandle>& GetMobileKitchenHandleRecognizedInstances() const;

    protected:

        ParameterSet GetParameterSet(const CObjectDescription* pObjectDescription, const bool UsingImproveImageReconstruction, const int VerboseLevel) const;

        RecognitionOutCome Execute(const CObjectDescription* pObjectDescription, const bool UsingImproveImageReconstruction, const int VerboseLevel) override;

        bool Displaying(const RecognitionOutCome CurrentOutCome) override;

        bool CreateIntermediateDataStructures(const int TotalAvailableHarrisPointsPerMonocularSource, const int GaussianKernelRadius, const int VerboseLevel);

        void DestroyIntermediateDataStructures();

        bool ProcessImages(const ParameterSet::ProcessImagesParameterSet& Parameters, const bool UsingImproveImageReconstruction);

        bool ProcessPoints(const ParameterSet::ProcessPointsParameterSet& Parameters);

        bool ProcessLineSegments(const ParameterSet::ExtractLinesParameterSet& Parameters);

        bool ProcessModelDescriptionMatching(const ParameterSet::MatchingParameterSet& Parameters);

        bool FilterEdges(const CByteImage* pRawEdgesImage, CByteImage* pFilteredEdgesImage, CByteImage* pLowResolutionMaskEdgesImage, CByteImage* pHighResolutionMaskEdgesImage, const int BitShifts, const int ExpansionMargin, const int MinimalBlockSize, const int MinimalBlockDimension, const float MaximalEigenRatio, const float MaximalDeviation, const BoundingBox2D* pActiveBoundingBox, BoundingBox2D* pHarrisActiveZone, const bool IsHorizontal);

        static int GetTotalPointsAlongLineSegment(const Vec2d& A, const Vec2d& B, const float Margin, const std::vector<Vec2d>& ActiveHarrisPoints);

        static bool NormalizedCrossCorrelationIntensity(float& NCCI, const CByteImage* pLeftImage, const CByteImage* pRightImage, const Vec2d& LeftPoint, const Vec2d& RightPoint, const int KernelRadius, const float* const pKernelBase, float* const pLeftDescriptor, float* const pRightDescriptor, const float KernelIntegral);

        static bool IsLineSegmentInMaskImage(const CByteImage* pMaskImage, const Vec2d& A, const Vec2d& B, const float Margin);

        static float GetLineSegmentUnitaryVectors(const Vec2d& A, const Vec2d& B, Vec2d& UnitaryParallelAB, Vec2d& UnitaryPerpendicularAB);

        static bool SortPredicateHarrisPoint(const Vec2d& lhs, const Vec2d& rhs);

        static int GetOrganizedIndex(const int L0, const int U1, const float X, std::vector<Vec2d>& SortedActiveHarrisPoints);

        static bool BinaryMapping(const CByteImage* pLowResolutionImage, CByteImage* pHighResolutionImage);

        static bool SortPredicate(const MobileKitchenHandle& lhs, const MobileKitchenHandle& rhs);

        bool ClusterFusion(std::vector<int>& ClusterIds, std::vector<SpaceLineSegment>& SpatialLineSegments);

    private:

        CByteImage** m_ppIntensityImages;

        CByteImage** m_ppSmoothIntensityImages;

        CByteImage** m_ppEdgeImages;

        CByteImage** m_ppFilteredEdgeImages;

        CByteImage** m_ppLowResolutionMaskImages;

        CByteImage** m_ppHighResolutionMaskImages;

        int m_KernelRadius;

        float m_KernelIntegral;

        float* m_pKernel;

        float* m_pDescriptors[2];

        int m_TotalAvailableHarrisPointsPerMonocularSource;

        Vec2d* m_pHarrisPoints[2];

        BoundingBox2D m_HarrisActiveZone[2];

        std::vector<Vec2d> m_ActiveHarrisPoints[2];

        std::deque<StereoTriangulationPoint> m_StereoTriangulationPoints;

        std::vector<SpaceLineSegment> m_SpatialLineSegments;

        std::vector<MobileKitchenHandle> m_MobileKitchenHandle;

        ParameterSet m_ParameterSet;
    };
}

