//============================================================================
// Name			: main.cpp
// Author		: Dr.-Ing. David I. Gonzalez Aguirre
// Mail			: david.gonzalez@kit.edu
// Version		: 0.1
// Copyright	: Contact the author for copyright information
// Description	: Vision Module for ARMAR-IIIa/b Demonstrations
//============================================================================

#include "Vision/Common.h"
#include "Vision/ObjectDescription.h"
#include "Vision/MobileKitchenHandleRecognizer.h"

//========================================================================================================================================================
#define _FAR_TEST_SET_
//========================================================================================================================================================
#define _STEREO_CALIBRATION_FILE_ "/home/gonzalez/Projects/Software/Walkman/FastHandleRecognition/Files/camera_armar3b_640x480_wide.txt"
#define _PARTIAL_RESULTS_DIRECTORY_ "/home/gonzalez/Projects/Software/Walkman/FastHandleRecognition/PartialResults/"
//========================================================================================================================================================

//========================================================================================================================================================
#ifdef _FAR_TEST_SET_
//========================================================================================================================================================
#define _LEFT_INPUT_IMAGE_ "/home/gonzalez/Projects/Software/Walkman/FastHandleRecognition/Files/snapshot_left.bmp"
#define _RIGHT_INPUT_IMAGE_ "/home/gonzalez/Projects/Software/Walkman/FastHandleRecognition/Files/snapshot_right.bmp"
#define _CAMERA_DISTANCE_ 890.0f
//========================================================================================================================================================
#endif
//========================================================================================================================================================

//========================================================================================================================================================
#ifdef _NEAR_TEST_SET_
//========================================================================================================================================================
#define _LEFT_INPUT_IMAGE_ "/home/gonzalez/Projects/Software/Walkman/FastHandleRecognition/Files/snapshot_closer_left.bmp"
#define _RIGHT_INPUT_IMAGE_ "/home/gonzalez/Projects/Software/Walkman/FastHandleRecognition/Files/snapshot_closer_right.bmp"
#define _CAMERA_DISTANCE_ 630.0f
//========================================================================================================================================================
#endif
//========================================================================================================================================================

#ifdef _CHAIR_TEST_SET_
//========================================================================================================================================================
#define _LEFT_INPUT_IMAGE_ "/home/gonzalez/Projects/Software/Walkman/FastHandleRecognition/Files/DrawerOnChair/DrawerOnChairCloser/snapshot_0000_0.bmp"
#define _RIGHT_INPUT_IMAGE_ "/home/gonzalez/Projects/Software/Walkman/FastHandleRecognition/Files/DrawerOnChair/DrawerOnChairCloser/snapshot_0000_1.bmp"
#define _CAMERA_DISTANCE_ 630.0f
//========================================================================================================================================================
#endif
//========================================================================================================================================================

//========================================================================================================================================================
//		PROCESS IMAGES @ Debug Mode ~ 47.149 ms @ Release Mode ~ 16.551 ms
//========================================================================================================================================================
//		Execution Information:
//		Debug Mode: -O0 -g3 -Wall -c -fmessage-length=0
//		Release Mode: -O3 -Wall -c -fmessage-length=0
// 		Single Thread Execution:
//		Intel(R) Core(TM) i7 CPU       Q 720  @ 1.60GHz
//		Frequency 933.000 MHz
//		Cache L2: 6144 KB
//========================================================================================================================================================
//		PROCESS IMAGES @ Debug Mode ~ 47.149 ms @ Release Mode ~ 16.551 ms
//		PROCESS POINTS @ Debug Mode ~ 23.374 ms @ Release Mode ~ 8.584 ms
//		PROCESS POINTS @ Debug Mode ~ 7.289 @ Release Mode ~ 0.474 ms
//		PROCESS MODEL DESCRIPTION MATCHING @ Debug Mode ~ 0.409 ms @ Release Mode ~ 0.156 ms
//		TOTAL ELAPSED TIME [NO DRAWING] @ Debug Mode ~ [MIN = 56.839, MAX = 69.701] ms @ Release Mode ~ [ MIN =  23.667, MAX = 28.363] ms
//========================================================================================================================================================

using namespace std;

using namespace Vision;

CByteImage** ppInputImages = NULL;

CByteImage** ppOutputImages = NULL;

void ClearImages();

bool IsSameContent(const CByteImage* pLeftImage, const CByteImage* pRightImage);

int main()
{
	CStereoCalibration StereoCalibration;

	if (!StereoCalibration.LoadCameraParameters(_STEREO_CALIBRATION_FILE_))
	{
		_GENERIC_ERROR_;

		return -1;
	}

	try
	{
		ppInputImages = new CByteImage*[2];

		ppOutputImages = new CByteImage*[2];

		ppInputImages[eLeftCamera] = new CByteImage();

		ppInputImages[eRightCamera] = new CByteImage();

		ppOutputImages[eLeftCamera] = new CByteImage();

		ppOutputImages[eRightCamera] = new CByteImage();
	}
	catch(std::exception& /*Exception*/)
	{
		ClearImages();

		return -1;
	}

	if (!ppInputImages[eLeftCamera]->LoadFromFile(_LEFT_INPUT_IMAGE_))
	{
		_GENERIC_ERROR_;

		ClearImages();

		return -1;
	}

	if (!ppInputImages[eRightCamera]->LoadFromFile(_RIGHT_INPUT_IMAGE_))
	{
		_GENERIC_ERROR_;

		ClearImages();

		return -1;
	}

	if (IsSameContent(ppInputImages[eLeftCamera],ppInputImages[1]))
	{
		_GENERIC_ERROR_;

		ClearImages();

		return -1;
	}

	if (!ppOutputImages[eLeftCamera]->LoadFromFile(_LEFT_INPUT_IMAGE_))
	{
		_GENERIC_ERROR_;

		ClearImages();

		return -1;
	}

	if (!ppOutputImages[eRightCamera]->LoadFromFile(_RIGHT_INPUT_IMAGE_))
	{
		_GENERIC_ERROR_;

		ClearImages();

		return -1;
	}

	CObjectDescription Handler(_MOBILE_KITCHEN_HANDLE_RECOGNIZER_CONTEXT_,_MOBILE_KITCHEN_HANDLE_RECOGNIZER_CLASS_,_MOBILE_KITCHEN_HANDLE_RECOGNIZER_INTANCE_LEFT_DRAWER_HANDLER_,0,0,0);

	if (!Handler.SetAttribute(_MOBILE_KITCHEN_HANDLE_RECOGNIZER_CLASS_ATTRIBUTE_MAIN_EDGE_LENGTH_,191.0f))
	{
		_GENERIC_ERROR_;

		ClearImages();

		return -1;
	}

	if (!Handler.SetAttribute(_MOBILE_KITCHEN_HANDLE_RECOGNIZER_CLASS_ATTRIBUTE_SECONDARY_EDGE_LENGTH_,150.0f))
	{
		_GENERIC_ERROR_;

		ClearImages();

		return -1;
	}

	CMobileKitchenHandleRecognizer RecognizerModule(&StereoCalibration,const_cast<const CByteImage**>(ppInputImages),ppOutputImages);

	if (!RecognizerModule.SetPartialResultsDirectory(_PARTIAL_RESULTS_DIRECTORY_))
	{
		_GENERIC_ERROR_;

		ClearImages();

		return -1;
	}

	RecognizerModule.SetSavePartialResults(true);

	switch(RecognizerModule.Recognize(&Handler,true,true,1))
	{
		case eError:

		break;

		case eNoRecognition:

		break;

		case eSingleRecognition:

		break;

		case eMultipleRecognition:

		break;
	}

	ClearImages();

	return 0;
}

void ClearImages()
{
	if (ppInputImages)
	{
		delete ppInputImages[eLeftCamera];

		delete ppInputImages[eRightCamera];

		delete[] ppInputImages;

		ppInputImages = NULL;
	}

	if (ppOutputImages)
	{
		delete ppOutputImages[eLeftCamera];

		delete ppOutputImages[eRightCamera];

		delete[] ppOutputImages;

		ppOutputImages = NULL;
	}
}

bool IsSameContent(const CByteImage* pLeftImage, const CByteImage* pRightImage)
{
	if (!pLeftImage)
	{
		_GENERIC_ERROR_;

		return false;
	}

	if (!pRightImage)
	{
		_GENERIC_ERROR_;

		return false;
	}

	if ((pLeftImage->width != pRightImage->width) || (pLeftImage->height != pRightImage->height) || (pLeftImage->type != pRightImage->type))
	{
		_GENERIC_ERROR_;

		return false;
	}

	if (memcmp(pLeftImage->pixels,pRightImage->pixels,(pLeftImage->width * pLeftImage->height) * pLeftImage->bytesPerPixel) == 0)
	{
		return true;
	}

	return false;
}
