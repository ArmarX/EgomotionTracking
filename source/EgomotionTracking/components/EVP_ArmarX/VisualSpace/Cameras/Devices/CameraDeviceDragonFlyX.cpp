/*
 * CameraDeviceDragonFlyX.cpp
 *
 *  Created on: 14.04.2011
 *      Author: gonzalez
 */

#include "CameraDeviceDragonFlyX.h"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace Devices
            {
                namespace EVP_ArmarX
                {
                    CCameraDeviceDragonFlyX::CamerasInformation CCameraDeviceDragonFlyX::GetAvailableRegisteredCameras()
                    {
                        CamerasInformation Cameras;
                        memset(&Cameras, 0, sizeof(CamerasInformation));
                        CLinux1394Capture2* pLinux1394Capture2 = new CLinux1394Capture2(-1, CVideoCaptureInterface::e640x480, CVideoCaptureInterface::eBayerPatternToRGB24);
                        if (pLinux1394Capture2)
                        {
                            if (pLinux1394Capture2->OpenCamera())
                            {
                                uint TotalFoundCameras = pLinux1394Capture2->GetNumberOfCameras();
                                for (uint i = 0; i < TotalFoundCameras; ++i)
                                {
                                    const std::string GUID = DeviceIdentifierToString(pLinux1394Capture2->GetCameraHandle(i)->guid);
                                    if (GUID == ARMAR3A_LEFT_CAMERA_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X0001;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == ARMAR3A_RIGHT_CAMERA_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X0002;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == ARMAR3A_LEFT_CAMERA_FOVEAL_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X0004;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == ARMAR3A_RIGHT_CAMERA_FOVEAL_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X0008;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == ARMAR3B_LEFT_CAMERA_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X010;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == ARMAR3B_RIGHT_CAMERA_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X0020;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == ARMAR3B_LEFT_CAMERA_FOVEAL_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X0040;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == ARMAR3B_RIGHT_CAMERA_FOVEAL_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X0080;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == HEAD_LEFT_CAMERA_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X0100;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == HEAD_RIGHT_CAMERA_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X0200;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == HEAD_LEFT_CAMERA_FOVEAL_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X0400;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == HEAD_RIGHT_CAMERA_FOVEAL_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X0800;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == TRIPOD_LEFT_CAMERA_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X1000;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == TRIPOD_RIGHT_CAMERA_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X2000;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == TRIPOD_2_LEFT_CAMERA_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X4000;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                    if (GUID == TRIPOD_2_RIGHT_CAMERA_UID)
                                    {
                                        Cameras.m_BitFlags |= 0X8000;
                                        ++Cameras.m_TotalCameras;
                                        continue;
                                    }
                                }
                                pLinux1394Capture2->CloseCamera();
                            }
                            RELEASE_OBJECT(pLinux1394Capture2)
                        }
                        if (Cameras.m_TotalCameras)
                        {
                            Cameras.m_Armar3A.m_PeripherialRig = (Cameras.m_BitFlags & 0X0001) && (Cameras.m_BitFlags & 0X0002);
                            Cameras.m_Armar3A.m_FovealRig = (Cameras.m_BitFlags & 0X0004) && (Cameras.m_BitFlags & 0X0008);
                            Cameras.m_Armar3B.m_PeripherialRig = (Cameras.m_BitFlags & 0X0010) && (Cameras.m_BitFlags & 0X0020);
                            Cameras.m_Armar3B.m_FovealRig = (Cameras.m_BitFlags & 0X0040) && (Cameras.m_BitFlags & 0X0080);
                            Cameras.m_Head.m_PeripherialRig = (Cameras.m_BitFlags & 0X0100) && (Cameras.m_BitFlags & 0X0200);
                            Cameras.m_Head.m_FovealRig = (Cameras.m_BitFlags & 0X0400) && (Cameras.m_BitFlags & 0X0800);
                            Cameras.m_TripodRigV1 = (Cameras.m_BitFlags & 0X1000) && (Cameras.m_BitFlags & 0X2000);
                            Cameras.m_TripodRigV2 = (Cameras.m_BitFlags & 0X4000) && (Cameras.m_BitFlags & 0X8000);
                        }
                        usleep(500000);
                        return Cameras;
                    }

                    CCameraDeviceDragonFlyX::CCameraDeviceDragonFlyX(const_string pID0, const_string pID1, const_string pID2, const_string pID3) :
                        CCameraDevice(DeviceIdentifier(0)), m_HasDeviceOwnerShip(true), m_CameraIndex(-1u), m_TotalCameras(0), m_ppImagesBayerPattern(nullptr), m_ppByteImages(nullptr), m_pByteImage(nullptr), m_pImageBayerPattern(nullptr), m_pLinux1394Capture2(nullptr), m_pCameraHandle(nullptr), m_LastExposure(-1u)
                    {
                        memset(m_pCameraDevices, 0, sizeof(CCameraDeviceDragonFlyX*) * MAX_CAMERAS);
                        memset(&m_ExposureFeatureInformation, 0, sizeof(dc1394feature_info_t));
                        memset(&m_ShutterFeatureInformation, 0, sizeof(dc1394feature_info_t));
                        memset(&m_WhiteBalanceFeatureInformation, 0, sizeof(dc1394feature_info_t));
                        if (pID0)
                        {
                            m_Ids[0] = std::string(pID0);
                            ++m_TotalCameras;
                            if (pID1)
                            {
                                m_Ids[1] = pID1;
                                ++m_TotalCameras;
                                if (pID2)
                                {
                                    m_Ids[2] = pID2;
                                    ++m_TotalCameras;
                                    if (pID3)
                                    {
                                        m_Ids[3] = pID3;
                                        ++m_TotalCameras;
                                    }
                                }
                            }
                        }
                        m_IsEnabled = m_TotalCameras;
                    }

                    CCameraDeviceDragonFlyX::CCameraDeviceDragonFlyX(CLinux1394Capture2* pLinux1394Capture2, CByteImage** ppImagesBayerPattern, CByteImage** ppByteImages, const_string pID, uint CameraIndex) :
                        CCameraDevice(DeviceIdentifier(0)), m_HasDeviceOwnerShip(false), m_CameraIndex(CameraIndex), m_TotalCameras(0), m_ppImagesBayerPattern(ppImagesBayerPattern), m_ppByteImages(ppByteImages), m_pByteImage(nullptr), m_pImageBayerPattern(nullptr), m_pLinux1394Capture2(pLinux1394Capture2), m_pCameraHandle(nullptr), m_LastExposure(-1u)
                    {
                        memset(m_pCameraDevices, 0, sizeof(CCameraDeviceDragonFlyX*) * MAX_CAMERAS);
                        memset(&m_ExposureFeatureInformation, 0, sizeof(dc1394feature_info_t));
                        memset(&m_ShutterFeatureInformation, 0, sizeof(dc1394feature_info_t));
                        memset(&m_WhiteBalanceFeatureInformation, 0, sizeof(dc1394feature_info_t));
                        if (m_pLinux1394Capture2 && m_ppImagesBayerPattern && m_ppByteImages && pID)
                        {
                            const std::string IdText(pID);
                            m_pCameraHandle = GetCameraHandleById(pLinux1394Capture2, IdText);
                            if (m_pCameraHandle)
                            {
                                m_CameraIndex = CameraIndex;
                                m_TotalCameras = m_pLinux1394Capture2->GetNumberOfCameras();
                                if (m_CameraIndex < m_TotalCameras)
                                {
                                    m_CameraId = m_pCameraHandle->guid;
                                    m_pByteImage = m_ppByteImages[m_CameraIndex];
                                    m_pImageBayerPattern = m_ppImagesBayerPattern[m_CameraIndex];
                                    if (m_pByteImage && m_pImageBayerPattern)
                                    {
                                        m_ImageActiveZone.Set(m_pByteImage->width, m_pByteImage->height);
                                        m_OperationStatus = CCameraDevice::eOpen;
                                        m_Type = CCameraDevice::eDiscreteIntensity;
                                        m_ColorInterpolation = CCameraDevice::eNone;

                                        //XXX DAMIT ICHS MIT MEINER IVT KOMPILIEREN KANN... RÜCKGÄNGIG MACHEN!!!
                                        /*
                                        switch (m_pLinux1394Capture2->GetVideoMode())
                                        {
                                            case CVideoCaptureInterface::e320x240:
                                                m_Resolution = CCameraDevice::e320x240;
                                                break;
                                            case CVideoCaptureInterface::e640x480:
                                                m_Resolution = CCameraDevice::e640x480;
                                                break;
                                            case CVideoCaptureInterface::e800x600:
                                                m_Resolution = CCameraDevice::e800x600;
                                                break;
                                            case CVideoCaptureInterface::e768x576:
                                                m_Resolution = CCameraDevice::e768x576;
                                                break;
                                            case CVideoCaptureInterface::e1024x768:
                                                m_Resolution = CCameraDevice::e1024x768;
                                                break;
                                            case CVideoCaptureInterface::e1280x960:
                                                m_Resolution = CCameraDevice::e1280x960;
                                                break;
                                            case CVideoCaptureInterface::e1600x1200:
                                                m_Resolution = CCameraDevice::e1600x1200;
                                                break;
                                            default:
                                                m_Resolution = CCameraDevice::eUnknownImageResolution;
                                                break;
                                        }
                                        switch (m_pLinux1394Capture2->GetFrameRate())
                                        {
                                            case CVideoCaptureInterface::e1_875fps:
                                                m_FrameRate = CCameraDevice::e1_875;
                                                break;
                                            case CVideoCaptureInterface::e3_75fps:
                                                m_FrameRate = CCameraDevice::e3_75;
                                                break;
                                            case CVideoCaptureInterface::e7_5fps:
                                                m_FrameRate = CCameraDevice::e7_5;
                                                break;
                                            case CVideoCaptureInterface::e15fps:
                                                m_FrameRate = CCameraDevice::e15;
                                                break;
                                            case CVideoCaptureInterface::e30fps:
                                                m_FrameRate = CCameraDevice::e30;
                                                break;
                                            case CVideoCaptureInterface::e60fps:
                                                m_FrameRate = CCameraDevice::e60;
                                                break;
                                            default:
                                                m_FrameRate = CCameraDevice::eUnKnownCapturingFrameRate;
                                                break;
                                        }
                                        */
                                        m_IsEnabled = LoadFeatures();
                                    }
                                }
                            }
                        }
                    }

                    CCameraDeviceDragonFlyX::~CCameraDeviceDragonFlyX()
                    {
                        CloseCamera();
                    }

                    bool CCameraDeviceDragonFlyX::CaptureBayerPattern()
                    {
                        return m_IsEnabled && m_pLinux1394Capture2->CaptureBayerPatternImage(m_ppImagesBayerPattern);
                    }

                    BayerPatternType CCameraDeviceDragonFlyX::GetBayerPatternType()
                    {
                        BayerPatternType Type = eUnknownBayerPattern;
                        if (m_IsEnabled && m_pLinux1394Capture2 && (m_OperationStatus == CCameraDevice::eOpen) && (std::string(m_pCameraHandle->model).find("Dragonfly") == 0))
                            switch (m_pCameraHandle->model_id)
                            {
                                case 0X0: //Dragonfly 1
                                    Type = eRGGB;
                                    break;
                                case 0X1: //Dragonfly 2
                                    Type = eGRBG;
                                    break;
                            }
                        return Type;
                    }

                    const list<RadiometricCalibration::CExposure> CCameraDeviceDragonFlyX::GetExposures()
                    {
                        list<RadiometricCalibration::CExposure> Exposures;
                        CCameraDevice::OperationMode OriginalMode = GetExposureMode();
                        bool ModeHasChanged = false;
                        if (OriginalMode == CCameraDevice::eAutomatic)
                        {
                            SetExposureMode(CCameraDevice::eManual);
                            ModeHasChanged = true;
                        }
                        for (uint32_t E = m_ShutterFeatureInformation.min, ParametricIndex = 0; E <= m_ShutterFeatureInformation.max; ++E, ++ParametricIndex)
                        {
                            SetExposureValue(E);
                            Exposures.push_back(RadiometricCalibration::CExposure(GetExposureTime(), ParametricIndex));
                        }
                        if (ModeHasChanged)
                        {
                            SetExposureMode(OriginalMode);
                        }
                        return Exposures;
                    }

                    bool CCameraDeviceDragonFlyX::SaveExposures(const_string pPathFileName)
                    {
                        const list<RadiometricCalibration::CExposure> Exposures = GetExposures();
                        const uint TotalExposures = Exposures.size();
                        if (TotalExposures)
                        {
                            Files::COutFile OutputBinaryFile(pPathFileName, Files::CFile::eBinary);
                            if (OutputBinaryFile.IsReady())
                                if (OutputBinaryFile.WriteValue(TotalExposures))
                                {
                                    list<RadiometricCalibration::CExposure>::const_iterator ExposuresEnd = Exposures.end();
                                    for (list<RadiometricCalibration::CExposure>::const_iterator pExposure = Exposures.begin(); pExposure != ExposuresEnd; ++pExposure)
                                    {
                                        const real Time = pExposure->GetTime();
                                        if (!OutputBinaryFile.WriteValue(Time))
                                        {
                                            return false;
                                        }
                                        const uint ParametricIndex = pExposure->GetParametricIndex();
                                        if (!OutputBinaryFile.WriteValue(ParametricIndex))
                                        {
                                            return false;
                                        }
                                    }
                                }
                            return OutputBinaryFile.Close();
                        }
                        return false;
                    }

                    const list<RadiometricCalibration::CExposure> CCameraDeviceDragonFlyX::LoadExposures(const_string pPathFileName)
                    {
                        list<RadiometricCalibration::CExposure> Exposures;
                        if (Files::CFile::Exists(pPathFileName))
                        {
                            Files::CInFile InputBinaryFile(pPathFileName, Files::CFile::eBinary);
                            if (InputBinaryFile.IsReady())
                            {
                                uint TotalExposures = 0;
                                if (InputBinaryFile.ReadValue(TotalExposures))
                                {
                                    real Time = _REAL_ZERO_;
                                    uint ParametricIndex = 0;
                                    for (uint i = 0; i < TotalExposures; ++i)
                                        if (InputBinaryFile.ReadValue(Time) && InputBinaryFile.ReadValue(ParametricIndex))
                                        {
                                            Exposures.push_back(RadiometricCalibration::CExposure(Time, ParametricIndex));
                                        }
                                }
                            }
                        }
                        return Exposures;
                    }

                    bool CCameraDeviceDragonFlyX::SaveIndexedBayerPatternImageToFile(const_string pPathFileName) const
                    {
                        return m_pImageBayerPattern->SaveToFile(pPathFileName);
                    }

                    dc1394camera_t* CCameraDeviceDragonFlyX::GetCameraHandleById(CLinux1394Capture2* pLinux1394Capture2, const std::string& IdText)
                    {
                        if (pLinux1394Capture2)
                        {
                            const uint TotalCameras = pLinux1394Capture2->GetNumberOfCameras();
                            for (uint i = 0; i < TotalCameras; ++i)
                            {
                                dc1394camera_t* pCameraHandle = pLinux1394Capture2->GetCameraHandle(i);
                                if (IdText == DeviceIdentifierToString(pCameraHandle->guid))
                                {
                                    return pCameraHandle;
                                }
                            }
                        }
                        return nullptr;
                    }

                    bool CCameraDeviceDragonFlyX::LoadFeatures()
                    {
                        dc1394featureset_t FeatureSet;
                        if (dc1394_feature_get_all(m_pCameraHandle, &FeatureSet) == DC1394_SUCCESS)
                        {
                            bool ExposureLoaded = false;
                            bool ShutterLoaded = false;
                            bool WhiteBalanceLoaded = false;
                            for (uint i = 0; i < DC1394_FEATURE_NUM; ++i)
                            {
                                switch (FeatureSet.feature[i].id)
                                {
                                    case DC1394_FEATURE_EXPOSURE:
                                        m_ExposureFeatureInformation = FeatureSet.feature[i];
                                        ExposureLoaded = true;
                                        break;
                                    case DC1394_FEATURE_SHUTTER:
                                        m_ShutterFeatureInformation = FeatureSet.feature[i];
                                        ShutterLoaded = true;
                                        break;
                                    case DC1394_FEATURE_WHITE_BALANCE:
                                        m_WhiteBalanceFeatureInformation = FeatureSet.feature[i];
                                        WhiteBalanceLoaded = true;
                                        break;
                                    default:
                                        break;
                                }
                            }
                            return ExposureLoaded && ShutterLoaded && WhiteBalanceLoaded;
                        }
                        return false;
                    }

                    bool CCameraDeviceDragonFlyX::FlushImages(const uint TotalImages)
                    {
                        //XXX DAMIT MEIN IVT GEHT!!! RÜCKGÄNGIG MACHEN!!!
                        /*
                        if (m_pLinux1394Capture2 && TotalImages)
                        return m_pLinux1394Capture2->FlushImages(TotalImages);
                        */
                        return false;
                    }

                    real CCameraDeviceDragonFlyX::GetMinimalExposureValue()
                    {
                        if (m_IsEnabled && (dc1394_feature_get(m_pCameraHandle, &m_ShutterFeatureInformation) == DC1394_SUCCESS))
                        {
                            return real(m_ShutterFeatureInformation.min);
                        }
                        return _REAL_ZERO_;
                    }

                    real CCameraDeviceDragonFlyX::GetMaximalExposureValue()
                    {
                        if (m_IsEnabled && (dc1394_feature_get(m_pCameraHandle, &m_ShutterFeatureInformation) == DC1394_SUCCESS))
                        {
                            return real(m_ShutterFeatureInformation.max);
                        }
                        return _REAL_ZERO_;
                    }

                    real CCameraDeviceDragonFlyX::GetStepExposureValue()
                    {
                        return m_IsEnabled ? _REAL_ONE_ : _REAL_ZERO_;
                    }

                    real CCameraDeviceDragonFlyX::GetExposureValue()
                    {
                        if (m_IsEnabled && (dc1394_feature_get(m_pCameraHandle, &m_ShutterFeatureInformation) == DC1394_SUCCESS))
                        {
                            return real(m_ShutterFeatureInformation.value);
                        }
                        return _REAL_ZERO_;
                    }

                    real CCameraDeviceDragonFlyX::GetExposureTime()
                    {
                        /*
                         Source: http://www.ptgrey.com/support/kb/index.asp?a=4&q=202 [13.07.2011]
                         Article 202: Using absolute value registers to convert camera property values to real-world units.
                         SUMMARY:
                         Many PGR IEEE-1394 cameras implement "absolute value" modes for various camera settings. This article describes how to use the absolute value CSRs to determine real-world values, such as Shutter times in seconds (s) and Gain values in decibels (dB). APPLICABLE PRODUCTS :
                         FlyCapture 1.0 SDK •  All IEEE-1394 Camera Products •
                         ANSWER:
                         Using the absolute values contained in the ABS_VAL camera registers is easier and more efficient than applying complex conversion formulas to the information in the Value field of the associated Control and Status Register (CSR). In addition, conversion formulas can change between firmware versions. PGR therefore recommends using the absolute value registers to determine camera values. Absolute Value CSRs
                         Detailed information regarding the ABS_VAL register offsets and definitions can be located in the "Absolute Value CSR Registers" section of the PGR IEEE-1394 Digital Camera Register Reference, which can be found in our Downloads section.
                         */
                        float Value = _REAL_ZERO_;
                        if (m_IsEnabled && (dc1394_feature_get_absolute_value(m_pCameraHandle, DC1394_FEATURE_SHUTTER, &Value) == DC1394_SUCCESS))
                        {
                            return real(Value);
                        }
                        return _REAL_ZERO_;
                        /*
                         http://www.ptgrey.com/support/kb/index.asp?a=4&q=16&ST=
                         Article 16:    Calculating Dragonfly gain and shutter settings
                         Resolution 640x480: Time (ms) = (m_ShutterFeatureInformation.value * 30) / (16000 * m_FrameRate)
                         Resolution 1024x768: Time (ms) = (m_ShutterFeatureInformation.value * 15) / (12000 * m_FrameRate)
                         */
                    }

                    real CCameraDeviceDragonFlyX::GetMinimalNonBlankExposure(const byte NonBlankValue, const bool MultipleCameras, const real ImageFraction, const uint ExposureIntervalIncrement)
                    {
                        if (m_IsEnabled && (GetExposureMode() == CCameraDevice::eManual))
                        {
                            const byte SafeNonBlankValue = TMax(byte(1), NonBlankValue);
                            const uint SafeExposureIntervalIncrement = TMax(ExposureIntervalIncrement, 1u);
                            const real SafeImageFraction = TMin(TMax(ImageFraction, _REAL_ZERO_), _REAL_HALF_);
                            if (MultipleCameras && (m_TotalCameras > 1))
                            {
                                if (dc1394_feature_get(m_pCameraDevices[0]->m_pCameraHandle, &m_pCameraDevices[0]->m_ShutterFeatureInformation) != DC1394_SUCCESS)
                                {
                                    return _REAL_MAX_;
                                }
                                uint32_t E0 = m_pCameraDevices[0]->m_ShutterFeatureInformation.min, E1 = m_pCameraDevices[0]->m_ShutterFeatureInformation.max;
                                for (uint CameraIndex = 1; CameraIndex < m_TotalCameras; ++CameraIndex)
                                    if (dc1394_feature_get(m_pCameraDevices[CameraIndex]->m_pCameraHandle, &m_pCameraDevices[CameraIndex]->m_ShutterFeatureInformation) == DC1394_SUCCESS)
                                    {
                                        if (m_pCameraDevices[CameraIndex]->m_ShutterFeatureInformation.min < E0)
                                        {
                                            E0 = m_pCameraDevices[CameraIndex]->m_ShutterFeatureInformation.min;
                                        }
                                        if (m_pCameraDevices[CameraIndex]->m_ShutterFeatureInformation.max > E1)
                                        {
                                            E1 = m_pCameraDevices[CameraIndex]->m_ShutterFeatureInformation.max;
                                        }
                                    }
                                    else
                                    {
                                        return _REAL_MAX_;
                                    }
                                uint TotalSerchingCameras = m_TotalCameras;
                                bool* pSearchFlag = new bool[TotalSerchingCameras];
                                memset(pSearchFlag, true, m_TotalCameras * sizeof(bool));
                                for (uint32_t E = E0; E <= E1; E += SafeExposureIntervalIncrement)
                                {
                                    for (uint CameraIndex = 0; CameraIndex < m_TotalCameras; ++CameraIndex)
                                        if (dc1394_feature_set_value(m_pCameraDevices[CameraIndex]->m_pCameraHandle, m_pCameraDevices[CameraIndex]->m_ShutterFeatureInformation.id, E) != DC1394_SUCCESS)
                                        {
                                            return _REAL_MAX_;
                                        }
                                    FlushImages(8);
                                    if (m_pLinux1394Capture2->CaptureBayerPatternImage(m_ppImagesBayerPattern))
                                    {
                                        for (uint CameraIndex = 0; CameraIndex < m_TotalCameras; ++CameraIndex)
                                            if (pSearchFlag[CameraIndex])
                                            {
                                                const uint ImageArea = m_ppImagesBayerPattern[CameraIndex]->width * m_ppImagesBayerPattern[CameraIndex]->height;
                                                const byte* pBuffer = m_ppImagesBayerPattern[CameraIndex]->pixels;
                                                const byte* const pFinalBuffer = pBuffer + ImageArea;
                                                if (SafeImageFraction > _REAL_EPSILON_)
                                                {
                                                    const uint HitsThreshold = RoundPositiveRealToInteger(SafeImageFraction * real(ImageArea));
                                                    uint Hits = 0;
                                                    while (pBuffer < pFinalBuffer)
                                                        if (*pBuffer++ >= SafeNonBlankValue)
                                                        {
                                                            if (++Hits > HitsThreshold)
                                                            {
                                                                pSearchFlag[CameraIndex] = false;
                                                                --TotalSerchingCameras;
                                                                if (!TotalSerchingCameras)
                                                                {
                                                                    RELEASE_ARRAY(pSearchFlag)
                                                                    return real(E);
                                                                }
                                                                else
                                                                {
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                }
                                                else
                                                    while (pBuffer < pFinalBuffer)
                                                        if (*pBuffer++ >= SafeNonBlankValue)
                                                        {
                                                            pSearchFlag[CameraIndex] = false;
                                                            --TotalSerchingCameras;
                                                            if (!TotalSerchingCameras)
                                                            {
                                                                RELEASE_ARRAY(pSearchFlag)
                                                                return real(E);
                                                            }
                                                            else
                                                            {
                                                                break;
                                                            }
                                                        }
                                            }
                                    }
                                    else
                                    {
                                        RELEASE_ARRAY(pSearchFlag)
                                        return _REAL_MAX_;
                                    }
                                }
                                RELEASE_ARRAY(pSearchFlag)
                            }
                            else if (dc1394_feature_get(m_pCameraHandle, &m_ShutterFeatureInformation) == DC1394_SUCCESS)
                            {
                                for (uint32_t E = m_ShutterFeatureInformation.min; E < m_ShutterFeatureInformation.max; E += SafeExposureIntervalIncrement)
                                {
                                    if (dc1394_feature_set_value(m_pCameraHandle, m_ShutterFeatureInformation.id, E) == DC1394_SUCCESS)
                                    {
                                        FlushImages(8);
                                        if (m_pLinux1394Capture2->CaptureBayerPatternImage(m_ppImagesBayerPattern))
                                        {
                                            const uint ImageArea = m_ppImagesBayerPattern[m_CameraIndex]->width * m_ppImagesBayerPattern[m_CameraIndex]->height;
                                            const byte* pBuffer = m_ppImagesBayerPattern[m_CameraIndex]->pixels;
                                            const byte* const pFinalBuffer = pBuffer + ImageArea;
                                            if (SafeImageFraction > _REAL_EPSILON_)
                                            {
                                                const uint HitsThreshold = RoundPositiveRealToInteger(SafeImageFraction * real(ImageArea));
                                                uint Hits = 0;
                                                while (pBuffer < pFinalBuffer)
                                                    if (*pBuffer++ >= SafeNonBlankValue)
                                                    {
                                                        if (++Hits > HitsThreshold)
                                                        {
                                                            return real(E);
                                                        }
                                                    }
                                            }
                                            else
                                                while (pBuffer < pFinalBuffer)
                                                    if (*pBuffer++ >= SafeNonBlankValue)
                                                    {
                                                        return real(E);
                                                    }
                                        }
                                        else
                                        {
                                            return _REAL_MAX_;
                                        }
                                    }
                                    else
                                    {
                                        return _REAL_MAX_;
                                    }
                                }
                            }
                        }
                        return _REAL_MAX_;
                    }

                    real CCameraDeviceDragonFlyX::GetMaximalNonWhiteExposure(const byte NonWhiteValue, const bool MultipleCameras, const real ImageFraction, const uint ExposureIntervalIncrement)
                    {
                        if (m_IsEnabled && (GetExposureMode() == CCameraDevice::eManual))
                        {
                            const byte SafeNonWhiteValue = TMin(byte(255), NonWhiteValue);
                            const int SafeExposureIntervalIncrement = TMax(ExposureIntervalIncrement, 1u);
                            const real SafeImageFraction = TMin(TMax(ImageFraction, _REAL_ZERO_), _REAL_HALF_);
                            if (MultipleCameras && (m_TotalCameras > 1))
                            {
                                if (dc1394_feature_get(m_pCameraDevices[0]->m_pCameraHandle, &m_pCameraDevices[0]->m_ShutterFeatureInformation) != DC1394_SUCCESS)
                                {
                                    return _REAL_MAX_;
                                }
                                uint32_t E0 = m_pCameraDevices[0]->m_ShutterFeatureInformation.min, E1 = m_pCameraDevices[0]->m_ShutterFeatureInformation.max;
                                for (uint CameraIndex = 1; CameraIndex < m_TotalCameras; ++CameraIndex)
                                    if (dc1394_feature_get(m_pCameraDevices[CameraIndex]->m_pCameraHandle, &m_pCameraDevices[CameraIndex]->m_ShutterFeatureInformation) == DC1394_SUCCESS)
                                    {
                                        if (m_pCameraDevices[CameraIndex]->m_ShutterFeatureInformation.min < E0)
                                        {
                                            E0 = m_pCameraDevices[CameraIndex]->m_ShutterFeatureInformation.min;
                                        }
                                        if (m_pCameraDevices[CameraIndex]->m_ShutterFeatureInformation.max > E1)
                                        {
                                            E1 = m_pCameraDevices[CameraIndex]->m_ShutterFeatureInformation.max;
                                        }
                                    }
                                    else
                                    {
                                        return _REAL_MAX_;
                                    }
                                uint TotalSerchingCameras = m_TotalCameras;
                                bool* pSearchFlag = new bool[TotalSerchingCameras];
                                memset(pSearchFlag, true, m_TotalCameras * sizeof(bool));
                                for (int E = int(E1); E >= int(E0); E -= SafeExposureIntervalIncrement)
                                {
                                    for (uint CameraIndex = 0; CameraIndex < m_TotalCameras; ++CameraIndex)
                                        if (dc1394_feature_set_value(m_pCameraDevices[CameraIndex]->m_pCameraHandle, m_pCameraDevices[CameraIndex]->m_ShutterFeatureInformation.id, uint32_t(E)) != DC1394_SUCCESS)
                                        {
                                            return _REAL_MAX_;
                                        }
                                    FlushImages(8);
                                    if (m_pLinux1394Capture2->CaptureBayerPatternImage(m_ppImagesBayerPattern))
                                    {
                                        for (uint CameraIndex = 0; CameraIndex < m_TotalCameras; ++CameraIndex)
                                            if (pSearchFlag[CameraIndex])
                                            {
                                                const uint ImageArea = m_ppImagesBayerPattern[CameraIndex]->width * m_ppImagesBayerPattern[CameraIndex]->height;
                                                const byte* pBuffer = m_ppImagesBayerPattern[CameraIndex]->pixels;
                                                const byte* const pFinalBuffer = pBuffer + ImageArea;
                                                if (SafeImageFraction > _REAL_EPSILON_)
                                                {
                                                    const uint HitsThreshold = RoundPositiveRealToInteger(SafeImageFraction * real(ImageArea));
                                                    uint Hits = 0;
                                                    while (pBuffer < pFinalBuffer)
                                                        if (*pBuffer++ <= SafeNonWhiteValue)
                                                        {
                                                            if (++Hits > HitsThreshold)
                                                            {
                                                                pSearchFlag[CameraIndex] = false;
                                                                --TotalSerchingCameras;
                                                                if (!TotalSerchingCameras)
                                                                {
                                                                    RELEASE_ARRAY(pSearchFlag)
                                                                    return real(E);
                                                                }
                                                                else
                                                                {
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                }
                                                else
                                                    while (pBuffer < pFinalBuffer)
                                                        if (*pBuffer++ <= SafeNonWhiteValue)
                                                        {
                                                            pSearchFlag[CameraIndex] = false;
                                                            --TotalSerchingCameras;
                                                            if (!TotalSerchingCameras)
                                                            {
                                                                RELEASE_ARRAY(pSearchFlag)
                                                                return real(E);
                                                            }
                                                            else
                                                            {
                                                                break;
                                                            }
                                                        }
                                            }
                                    }
                                    else
                                    {
                                        RELEASE_ARRAY(pSearchFlag)
                                        return _REAL_MAX_;
                                    }
                                }
                                RELEASE_ARRAY(pSearchFlag)
                            }
                            else if (dc1394_feature_get(m_pCameraHandle, &m_ShutterFeatureInformation) == DC1394_SUCCESS)
                            {
                                int E0 = m_ShutterFeatureInformation.min, E1 = m_ShutterFeatureInformation.max;
                                for (int E = E1; E >= E0; E -= SafeExposureIntervalIncrement)
                                {
                                    if (dc1394_feature_set_value(m_pCameraHandle, m_ShutterFeatureInformation.id, E) == DC1394_SUCCESS)
                                    {
                                        FlushImages(8);
                                        if (m_pLinux1394Capture2->CaptureBayerPatternImage(m_ppImagesBayerPattern))
                                        {
                                            const uint ImageArea = m_ppImagesBayerPattern[m_CameraIndex]->width * m_ppImagesBayerPattern[m_CameraIndex]->height;
                                            const byte* pBuffer = m_ppImagesBayerPattern[m_CameraIndex]->pixels;
                                            const byte* const pFinalBuffer = pBuffer + ImageArea;
                                            if (SafeImageFraction > _REAL_EPSILON_)
                                            {
                                                const uint HitsThreshold = RoundPositiveRealToInteger(SafeImageFraction * real(ImageArea));
                                                uint Hits = 0;
                                                while (pBuffer < pFinalBuffer)
                                                    if (*pBuffer++ <= SafeNonWhiteValue)
                                                    {
                                                        if (++Hits > HitsThreshold)
                                                        {
                                                            return real(E);
                                                        }
                                                    }
                                            }
                                            else
                                                while (pBuffer < pFinalBuffer)
                                                    if (*pBuffer++ <= SafeNonWhiteValue)
                                                    {
                                                        return real(E);
                                                    }
                                        }
                                        else
                                        {
                                            return _REAL_MAX_;
                                        }
                                    }
                                    else
                                    {
                                        return _REAL_MAX_;
                                    }
                                }
                            }
                        }
                        return _REAL_MAX_;
                    }

                    CCameraDevice::OperationMode CCameraDeviceDragonFlyX::GetExposureMode()
                    {
                        if (m_IsEnabled)
                        {
                            const bool ShutterFeatureResult = (dc1394_feature_get(m_pCameraHandle, &m_ShutterFeatureInformation) == DC1394_SUCCESS);
                            const bool ExposureFeatureResult = (dc1394_feature_get(m_pCameraHandle, &m_ExposureFeatureInformation) == DC1394_SUCCESS);
                            if (ShutterFeatureResult && ExposureFeatureResult && (m_ShutterFeatureInformation.current_mode == m_ExposureFeatureInformation.current_mode))
                                switch (m_ShutterFeatureInformation.current_mode)
                                {
                                    case DC1394_FEATURE_MODE_MANUAL:
                                        return CCameraDevice::eManual;
                                        break;
                                    case DC1394_FEATURE_MODE_AUTO:
                                        return CCameraDevice::eAutomatic;
                                        break;
                                    default:
                                        break;
                                }
                        }
                        return CCameraDevice::eUknownOperationMode;
                    }

                    bool CCameraDeviceDragonFlyX::SetExposureMode(const CCameraDevice::OperationMode ExposureOperationMode)
                    {
                        if (m_IsEnabled)
                        {
                            dc1394feature_mode_t Mode;
                            switch (ExposureOperationMode)
                            {
                                case CCameraDevice::eAutomatic:
                                    Mode = DC1394_FEATURE_MODE_AUTO;
                                    break;
                                case CCameraDevice::eManual:
                                    Mode = DC1394_FEATURE_MODE_MANUAL;
                                    m_LastExposure = -1024;
                                    break;
                                default:
                                    return false;
                            }
                            const bool ExposureFeatureResult = (dc1394_feature_set_mode(m_pCameraHandle, m_ExposureFeatureInformation.id, Mode) == DC1394_SUCCESS);
                            const bool ShutterFeatureResult = (dc1394_feature_set_mode(m_pCameraHandle, m_ShutterFeatureInformation.id, Mode) == DC1394_SUCCESS);
                            if (ShutterFeatureResult && ExposureFeatureResult)
                            {
                                if (ExposureOperationMode == CCameraDevice::eManual)
                                {
                                    if (dc1394_feature_set_value(m_pCameraHandle, m_ExposureFeatureInformation.id, m_ExposureFeatureInformation.min) != DC1394_SUCCESS)
                                    {
                                        return false;
                                    }
                                }
                                //This delay is necessary for the camera to change the mode, if not used the first capture is corrupted.
                                usleep(50000);
                                if ((m_CameraIndex == 0) && m_pLinux1394Capture2 && m_ppImagesBayerPattern)
                                    for (uint i = 0; i < 4; ++i)
                                    {
                                        m_pLinux1394Capture2->CaptureBayerPatternImage(m_ppImagesBayerPattern);
                                    }
                                return true;
                            }
                        }
                        return false;
                    }

                    bool CCameraDeviceDragonFlyX::SetExposureValue(const real ExposureValue)
                    {
                        if (m_IsEnabled && (dc1394_feature_set_value(m_pCameraHandle, m_ShutterFeatureInformation.id, uint32_t(RealRound(ExposureValue))) == DC1394_SUCCESS))
                        {
                            if (/*(m_CameraIndex == 0) &&*/m_pLinux1394Capture2 && m_ppImagesBayerPattern)
                            {
                                const uint Exposure = RoundToInteger(ExposureValue);
                                FlushImages((Exposure > 16) ? 3 : 4);
                                //FlushImages(4);
                                m_LastExposure = Exposure;
                            }
                            return true;
                        }
                        return false;
                    }

                    bool CCameraDeviceDragonFlyX::SetExposureValueNoDelay(const real ExposureValue)
                    {
                        return m_IsEnabled && (dc1394_feature_set_value(m_pCameraHandle, m_ShutterFeatureInformation.id, uint32_t(RealRound(ExposureValue))) == DC1394_SUCCESS);
                    }

                    bool CCameraDeviceDragonFlyX::Flush(const uint Drops)
                    {
                        return m_IsEnabled && FlushImages(Drops);
                    }

                    real CCameraDeviceDragonFlyX::GetMinimalWhiteBalanceRedValue()
                    {
                        if (m_IsEnabled && (dc1394_feature_get(m_pCameraHandle, &m_WhiteBalanceFeatureInformation) == DC1394_SUCCESS))
                        {
                            return real(m_WhiteBalanceFeatureInformation.min);
                        }
                        return _REAL_ZERO_;
                    }

                    real CCameraDeviceDragonFlyX::GetMaximalWhiteBalanceRedValue()
                    {
                        if (m_IsEnabled && (dc1394_feature_get(m_pCameraHandle, &m_WhiteBalanceFeatureInformation) == DC1394_SUCCESS))
                        {
                            return real(m_WhiteBalanceFeatureInformation.max);
                        }
                        return _REAL_ZERO_;
                    }

                    real CCameraDeviceDragonFlyX::GetStepWhiteBalanceRedValue()
                    {
                        return m_IsEnabled ? _REAL_ONE_ : _REAL_ZERO_;
                    }

                    bool CCameraDeviceDragonFlyX::SetStepWhiteBalanceRedValue(const real RedValue)
                    {
                        return m_IsEnabled && (dc1394_feature_get(m_pCameraHandle, &m_WhiteBalanceFeatureInformation) == DC1394_SUCCESS) && (dc1394_feature_whitebalance_set_value(m_pCameraHandle, m_WhiteBalanceFeatureInformation.BU_value, uint32_t(RealRound(RedValue))) == DC1394_SUCCESS);
                    }

                    real CCameraDeviceDragonFlyX::GetMinimalWhiteBalanceBlueValue()
                    {
                        if (m_IsEnabled && (dc1394_feature_get(m_pCameraHandle, &m_WhiteBalanceFeatureInformation) == DC1394_SUCCESS))
                        {
                            return real(m_WhiteBalanceFeatureInformation.min);
                        }
                        return _REAL_ZERO_;
                    }

                    real CCameraDeviceDragonFlyX::GetMaximalWhiteBalanceBlueValue()
                    {
                        if (m_IsEnabled && (dc1394_feature_get(m_pCameraHandle, &m_WhiteBalanceFeatureInformation) == DC1394_SUCCESS))
                        {
                            return real(m_WhiteBalanceFeatureInformation.max);
                        }
                        return _REAL_ZERO_;
                    }

                    real CCameraDeviceDragonFlyX::GetStepWhiteBalanceBlueValue()
                    {
                        return m_IsEnabled ? _REAL_ONE_ : _REAL_ZERO_;
                    }

                    bool CCameraDeviceDragonFlyX::SetStepWhiteBalanceBlueValue(const real BlueValue)
                    {
                        return m_IsEnabled && (dc1394_feature_get(m_pCameraHandle, &m_WhiteBalanceFeatureInformation) == DC1394_SUCCESS) && (dc1394_feature_whitebalance_set_value(m_pCameraHandle, uint32_t(RealRound(BlueValue)), m_WhiteBalanceFeatureInformation.RV_value) == DC1394_SUCCESS);
                    }

                    CCameraDevice::OperationMode CCameraDeviceDragonFlyX::GetWhiteBalanceMode()
                    {
                        if (m_IsEnabled)
                        {
                            if (dc1394_feature_get(m_pCameraHandle, &m_WhiteBalanceFeatureInformation) == DC1394_SUCCESS)
                                switch (m_WhiteBalanceFeatureInformation.current_mode)
                                {
                                    case DC1394_FEATURE_MODE_MANUAL:
                                        return CCameraDevice::eManual;
                                        break;
                                    case DC1394_FEATURE_MODE_AUTO:
                                        return CCameraDevice::eAutomatic;
                                        break;
                                    default:
                                        break;
                                }
                        }
                        return CCameraDevice::eUknownOperationMode;
                    }

                    bool CCameraDeviceDragonFlyX::SetWhiteBalanceMode(const CCameraDevice::OperationMode WhiteBalanceOperationMode)
                    {
                        if (m_IsEnabled)
                        {
                            dc1394feature_mode_t Mode;
                            switch (WhiteBalanceOperationMode)
                            {
                                case CCameraDevice::eAutomatic:
                                    Mode = DC1394_FEATURE_MODE_MANUAL;
                                    break;
                                case CCameraDevice::eManual:
                                    Mode = DC1394_FEATURE_MODE_MANUAL;
                                    break;
                                default:
                                    return false;
                            }
                            bool Result = (dc1394_feature_set_mode(m_pCameraHandle, m_WhiteBalanceFeatureInformation.id, Mode) == DC1394_SUCCESS);
                            usleep(50000);
                            return Result;
                        }
                        return false;
                    }

                    vector<CCameraDevice::ImageType> CCameraDeviceDragonFlyX::GetSupportedImageTypes()
                    {
                        vector<CCameraDevice::ImageType> SupportedImageTypes(2);
                        SupportedImageTypes.push_back(CCameraDevice::eDiscreteIntensity);
                        SupportedImageTypes.push_back(CCameraDevice::eDiscreteRGB);
                        return SupportedImageTypes;
                    }

                    vector<CCameraDevice::ImageColorInterpolation> CCameraDeviceDragonFlyX::GetSupportedImageColorInterpolations()
                    {
                        vector<CCameraDevice::ImageColorInterpolation> SupportedImageColorInterpolations(1);
                        SupportedImageColorInterpolations.push_back(CCameraDevice::eBilineal);
                        return SupportedImageColorInterpolations;
                    }

                    vector<CCameraDevice::ImageResolution> CCameraDeviceDragonFlyX::GetSupportedImageResolutions()
                    {
                        vector<CCameraDevice::ImageResolution> SupportedImageResolutions;
                        SupportedImageResolutions.push_back(CCameraDevice::e320x240);
                        SupportedImageResolutions.push_back(CCameraDevice::e640x480);
                        return SupportedImageResolutions;
                    }

                    vector<CCameraDevice::CapturingFrameRate> CCameraDeviceDragonFlyX::GetSupportedCapturingFrameRates()
                    {
                        vector<CCameraDevice::CapturingFrameRate> SupportedCapturingFrameRates;
                        return SupportedCapturingFrameRates;
                    }

                    bool CCameraDeviceDragonFlyX::IsSupportedConfiguration(const CCameraDevice::ImageType Type, const CCameraDevice::ImageColorInterpolation ColorInterpolation, const CCameraDevice::ImageResolution Resolution, const CCameraDevice::CapturingFrameRate FrameRate)
                    {
                        if ((Type == CCameraDevice::eDiscreteIntensity) || (Type == CCameraDevice::eDiscreteRGB))
                        {
                            if (ColorInterpolation == CCameraDevice::eNone)
                            {
                                if ((Resolution == CCameraDevice::e640x480) || (Resolution == CCameraDevice::e320x240))
                                {
                                    if ((FrameRate == CCameraDevice::e30) || (FrameRate == CCameraDevice::e60) || (FrameRate == CCameraDevice::e15))
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                        return false;
                    }

                    bool CCameraDeviceDragonFlyX::OpenCamera(const CCameraDevice::ImageType Type, const CCameraDevice::ImageColorInterpolation ColorInterpolation, const CCameraDevice::ImageResolution Resolution, const CCameraDevice::CapturingFrameRate FrameRate)
                    {
                        if (m_IsEnabled && m_HasDeviceOwnerShip && (!m_pLinux1394Capture2) && m_TotalCameras)
                        {
                            if (IsSupportedConfiguration(Type, ColorInterpolation, Resolution, FrameRate))
                            {
                                m_Type = Type;
                                m_ColorInterpolation = ColorInterpolation;
                                m_Resolution = Resolution;
                                m_FrameRate = FrameRate;
                                CVideoCaptureInterface::VideoMode VideoResolution = CVideoCaptureInterface::e640x480;
                                switch (m_Resolution)
                                {
                                    case CCameraDevice::e320x240:
                                        VideoResolution = CVideoCaptureInterface::e320x240;
                                        break;
                                    case CCameraDevice::e640x480:
                                        VideoResolution = CVideoCaptureInterface::e640x480;
                                        break;
                                    default:
                                        return false;
                                        break;
                                }
                                CVideoCaptureInterface::ColorMode ImageType = CVideoCaptureInterface::eRGB24;
                                switch (m_Type)
                                {
                                    case CCameraDevice::eDiscreteIntensity:
                                    case CCameraDevice::eContinuousIntensity:
                                        ImageType = CVideoCaptureInterface::eGrayScale;
                                        break;
                                    case CCameraDevice::eContinuousRGB:
                                    case CCameraDevice::eDiscreteRGB:
                                        ImageType = CVideoCaptureInterface::eBayerPatternToRGB24;
                                        break;
                                    default:
                                        return false;
                                        break;
                                }
                                CVideoCaptureInterface::FrameRate FrameRate = CVideoCaptureInterface::e30fps;
                                switch (m_FrameRate)
                                {
                                    case CCameraDevice::e15:
                                        FrameRate = CVideoCaptureInterface::e15fps;
                                        break;
                                    case CCameraDevice::e30:
                                        FrameRate = CVideoCaptureInterface::e30fps;
                                        break;
                                    case CCameraDevice::e60:
                                        FrameRate = CVideoCaptureInterface::e60fps;
                                        break;
                                    default:
                                        return false;
                                        break;
                                }
                                switch (m_TotalCameras)
                                {
                                    case 1:
                                        m_pLinux1394Capture2 = new CLinux1394Capture2(VideoResolution, ImageType, ImageProcessor::eBayerRG, FrameRate, m_TotalCameras, m_Ids[0].c_str());
                                        break;
                                    case 2:
                                        switch (FrameRate)
                                        {
                                            case CVideoCaptureInterface::e15fps:
                                                m_pLinux1394Capture2 = new CLinux1394Capture2(VideoResolution, 15.0f, 0, 0, -1, -1, CVideoCaptureInterface::eBayerPatternToRGB24, ImageProcessor::eBayerGR, m_TotalCameras, m_Ids[0].c_str(), m_Ids[1].c_str());
                                                break;
                                            case CVideoCaptureInterface::e30fps:
                                                m_pLinux1394Capture2 = new CLinux1394Capture2(VideoResolution, 30.0f, 0, 0, -1, -1, CVideoCaptureInterface::eBayerPatternToRGB24, ImageProcessor::eBayerGR, m_TotalCameras, m_Ids[0].c_str(), m_Ids[1].c_str());
                                                break;
                                            case CVideoCaptureInterface::e60fps:
                                                m_pLinux1394Capture2 = new CLinux1394Capture2(VideoResolution, 52.5f, 0, 0, -1, -1, CVideoCaptureInterface::eBayerPatternToRGB24, ImageProcessor::eBayerGR, m_TotalCameras, m_Ids[0].c_str(), m_Ids[1].c_str());
                                                break;
                                            default:
                                                m_pLinux1394Capture2 = nullptr;
                                                break;
                                        }
                                        break;
                                    case 3:
                                        m_pLinux1394Capture2 = new CLinux1394Capture2(VideoResolution, ImageType, ImageProcessor::eBayerRG, FrameRate, m_TotalCameras, m_Ids[0].c_str(), m_Ids[1].c_str(), m_Ids[2].c_str());
                                        break;
                                    case 4:
                                        m_pLinux1394Capture2 = new CLinux1394Capture2(VideoResolution, ImageType, ImageProcessor::eBayerRG, FrameRate, m_TotalCameras, m_Ids[0].c_str(), m_Ids[1].c_str(), m_Ids[2].c_str(), m_Ids[3].c_str());
                                        break;
                                }
                                if (m_pLinux1394Capture2->OpenCamera())
                                {
                                    m_CameraIndex = 0;
                                    m_pCameraHandle = GetCameraHandleById(m_pLinux1394Capture2, m_Ids[m_CameraIndex]);
                                    m_OperationStatus = CCameraDevice::eOpen;
                                    m_CameraId = m_pCameraHandle->guid;
                                    const uint Width = m_pLinux1394Capture2->GetWidth();
                                    const uint Height = m_pLinux1394Capture2->GetHeight();
                                    m_ImageActiveZone.Set(Width, Height);
                                    CByteImage::ImageType ImageType = m_pLinux1394Capture2->GetType();
                                    m_ppByteImages = new CByteImage*[m_TotalCameras];
                                    m_ppImagesBayerPattern = new CByteImage*[m_TotalCameras];
                                    for (uint i = 0; i < m_TotalCameras; ++i)
                                    {
                                        m_ppByteImages[i] = new CByteImage(Width, Height, ImageType);
                                        m_ppImagesBayerPattern[i] = new CByteImage(Width, Height, CByteImage::eGrayScale);
                                    }
                                    m_pByteImage = m_ppByteImages[m_CameraIndex];
                                    m_pImageBayerPattern = m_ppImagesBayerPattern[m_CameraIndex];
                                    m_pCameraDevices[m_CameraIndex] = this;
                                    m_IsEnabled = LoadFeatures();
                                    for (uint i = 1; i < m_TotalCameras; ++i)
                                    {
                                        m_pCameraDevices[i] = new CCameraDeviceDragonFlyX(m_pLinux1394Capture2, m_ppImagesBayerPattern, m_ppByteImages, m_Ids[i].c_str(), i);
                                    }
                                    CCameraDeviceDragonFlyX* pCameraDeviceDragonFlyX = nullptr;
                                    for (uint i = 1; i < m_TotalCameras; ++i)
                                    {
                                        pCameraDeviceDragonFlyX = m_pCameraDevices[i];
                                        for (uint j = 0; j < m_TotalCameras; ++j)
                                        {
                                            pCameraDeviceDragonFlyX->m_pCameraDevices[j] = m_pCameraDevices[j];
                                            pCameraDeviceDragonFlyX->m_Ids[j] = m_Ids[j];
                                            pCameraDeviceDragonFlyX->m_OperationStatus = m_OperationStatus;
                                            pCameraDeviceDragonFlyX->m_Type = m_Type;
                                            pCameraDeviceDragonFlyX->m_ColorInterpolation = m_ColorInterpolation;
                                            pCameraDeviceDragonFlyX->m_Resolution = m_Resolution;
                                            pCameraDeviceDragonFlyX->m_FrameRate = m_FrameRate;
                                            pCameraDeviceDragonFlyX->m_ImageActiveZone = m_ImageActiveZone;
                                        }
                                    }
                                    return true;
                                }
                                else
                                {
                                    m_OperationStatus = CCameraDevice::eError;
                                    if (m_pLinux1394Capture2)
                                    {
                                        m_pLinux1394Capture2->CloseCamera();
                                        RELEASE_OBJECT(m_pLinux1394Capture2)
                                    }
                                }
                            }
                        }
                        return false;
                    }

                    bool CCameraDeviceDragonFlyX::CloseCamera()
                    {
                        if (m_IsEnabled)
                        {
                            if (m_HasDeviceOwnerShip && m_pLinux1394Capture2)
                            {
                                m_pLinux1394Capture2->CloseCamera();
                                RELEASE_OBJECT(m_pLinux1394Capture2)
                                for (uint i = 0; i < m_TotalCameras; ++i)
                                {
                                    RELEASE_OBJECT(m_ppImagesBayerPattern[i])
                                    RELEASE_OBJECT(m_ppByteImages[i])
                                    if (i)
                                        RELEASE_OBJECT(m_pCameraDevices[i])
                                    }
                                RELEASE_ARRAY(m_ppImagesBayerPattern)
                                RELEASE_ARRAY(m_ppByteImages)
                                m_pImageBayerPattern = nullptr;
                                m_pByteImage = nullptr;
                            }
                            return true;
                        }
                        return false;
                    }

                    bool CCameraDeviceDragonFlyX::CaptureImage()
                    {
                        if (m_IsEnabled && m_pLinux1394Capture2 && m_pLinux1394Capture2->CaptureImage(m_ppByteImages))
                        {
                            return true;
                        }
                        return false;
                    }

                    bool CCameraDeviceDragonFlyX::CaptureImage(TImage<byte>* pDiscreteIntensityImage)
                    {
                        if (m_IsEnabled && pDiscreteIntensityImage && (m_Type == CCameraDevice::eDiscreteIntensity) && (m_OperationStatus == CCameraDevice::eOpen) && m_pLinux1394Capture2->CaptureImage(m_ppByteImages))
                        {
                            return MapImage(pDiscreteIntensityImage);
                        }
                        return false;
                    }

                    bool CCameraDeviceDragonFlyX::CaptureImage(TImage<real>* pContinuousIntensityImage)
                    {
                        if (m_IsEnabled && pContinuousIntensityImage && (m_Type == CCameraDevice::eDiscreteIntensity) && (m_OperationStatus == CCameraDevice::eOpen) && m_pLinux1394Capture2->CaptureImage(m_ppByteImages))
                        {
                            return MapImage(pContinuousIntensityImage);
                        }
                        return false;
                    }

                    bool CCameraDeviceDragonFlyX::CaptureImage(TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage)
                    {
                        if (m_IsEnabled && pDiscreteRGBImage && (m_Type == CCameraDevice::eDiscreteRGB) && (m_OperationStatus == CCameraDevice::eOpen) && m_pLinux1394Capture2->CaptureImage(m_ppByteImages))
                        {
                            return MapImage(pDiscreteRGBImage);
                        }
                        return false;
                    }

                    bool CCameraDeviceDragonFlyX::CaptureImage(TImage<CContinuousTristimulusPixel>* pContinuousRGBImage)
                    {
                        if (m_IsEnabled && pContinuousRGBImage && (m_Type == CCameraDevice::eContinuousRGB) && (m_OperationStatus == CCameraDevice::eOpen) && m_pLinux1394Capture2->CaptureImage(m_ppByteImages))
                        {
                            return MapImage(pContinuousRGBImage);
                        }
                        return false;
                    }

                    bool CCameraDeviceDragonFlyX::MapImage(TImage<byte>* pDiscreteIntensityImage)
                    {
                        if (pDiscreteIntensityImage && (m_Type == CCameraDevice::eDiscreteIntensity) && pDiscreteIntensityImage->GetSize().Equals(m_pByteImage->width, m_pByteImage->height))
                        {
                            memcpy(pDiscreteIntensityImage->GetBeginWritableBuffer(), m_pByteImage->pixels, pDiscreteIntensityImage->GetBufferSize());
                            return true;
                        }
                        return false;
                    }

                    bool CCameraDeviceDragonFlyX::MapImage(TImage<real>* pContinuousIntensityImage)
                    {
                        if (pContinuousIntensityImage && (m_Type == CCameraDevice::eDiscreteIntensity) && pContinuousIntensityImage->GetSize().Equals(m_pByteImage->width, m_pByteImage->height))
                        {
                            const real* const pContinuousIntensityPixelEnd = pContinuousIntensityImage->GetEndReadOnlyBuffer();
                            const byte* pDiscreteIntensityPixel = m_pByteImage->pixels;
                            real* pContinuousIntensityPixel = pContinuousIntensityImage->GetBeginWritableBuffer();
                            while (pContinuousIntensityPixel < pContinuousIntensityPixelEnd)
                            {
                                *pContinuousIntensityPixel++ = *pDiscreteIntensityPixel++;
                            }
                            return true;
                        }
                        return false;
                    }

                    bool CCameraDeviceDragonFlyX::MapImage(TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage)
                    {
                        if (pDiscreteRGBImage && (m_Type == CCameraDevice::eDiscreteRGB) && pDiscreteRGBImage->GetSize().Equals(m_pByteImage->width, m_pByteImage->height))
                        {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
                            memcpy(pDiscreteRGBImage->GetBeginWritableBuffer(), m_pByteImage->pixels, pDiscreteRGBImage->GetBufferSize());
#pragma GCC diagnostic pop
                            return true;
                        }
                        return false;
                    }

                    bool CCameraDeviceDragonFlyX::MapImage(TImage<CContinuousTristimulusPixel>* pContinuousRGBImage)
                    {
                        if (pContinuousRGBImage && (m_Type == CCameraDevice::eDiscreteIntensity) && pContinuousRGBImage->GetSize().Equals(m_pByteImage->width, m_pByteImage->height))
                        {
                            const CContinuousTristimulusPixel* const pContinuousRGBPixelEnd = pContinuousRGBImage->GetEndReadOnlyBuffer();
                            const CDiscreteTristimulusPixel* pDiscreteRGBPixel = reinterpret_cast<CDiscreteTristimulusPixel*>(m_pByteImage->pixels);
                            CContinuousTristimulusPixel* pContinuousRGBPixel = pContinuousRGBImage->GetBeginWritableBuffer();
#ifdef _STRUCTURED_PIXELS_
                            while (pContinuousRGBPixel < pContinuousRGBPixelEnd)
                            {
                                for (int i = 0; i < 3; ++i)
                                {
                                    pContinuousRGBPixel->m_Element[i] = real(pDiscreteRGBPixel->m_Element[i]);
                                }
                                ++pContinuousRGBPixel;
                                ++pDiscreteRGBPixel;
                            }
#else
                            while (pContinuousRGBPixel < pContinuousRGBPixelEnd)
                            {
                                *pContinuousRGBPixel++ = *pDiscreteRGBPixel++;
                            }
#endif
                            return true;
                        }
                        return false;
                    }

                    std::string CCameraDeviceDragonFlyX::GetDeviceName()
                    {
                        if (m_IsEnabled && (m_OperationStatus == CCameraDevice::eOpen))
                        {
                            std::string DeviceName(m_pCameraHandle->model);
                            DeviceName += " [";
                            DeviceName += std::string(m_pCameraHandle->vendor);
                            DeviceName += "]";
                        }
                        return std::string("Unknown");
                    }

                    bool CCameraDeviceDragonFlyX::HasDeviceOwnerShip()
                    {
                        return m_HasDeviceOwnerShip;
                    }

                    const TImage<byte> CCameraDeviceDragonFlyX::GetIndexedBayerPatternImage()
                    {
                        return m_IsEnabled ? TImage<byte>(reinterpret_cast<const byte*>(m_pImageBayerPattern->pixels), m_ImageActiveZone.GetSize()) : TImage<byte>();
                    }

                    CCameraDeviceDragonFlyX* CCameraDeviceDragonFlyX::GetCameraDevicesByIndex(const uint Index)
                    {
                        return (m_IsEnabled && (m_OperationStatus == CCameraDevice::eOpen) && (Index < m_TotalCameras)) ? m_pCameraDevices[Index] : nullptr;
                    }

                }
            }
        }
    }
}
