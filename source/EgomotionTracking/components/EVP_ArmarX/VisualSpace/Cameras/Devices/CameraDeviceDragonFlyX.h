/*
 * CameraDeviceDragonFlyX.h
 *
 *  Created on: 14.04.2011
 *      Author: gonzalez
 */

#pragma once

//EVP
#include <EgomotionTracking/components/EVP/GlobalSettings.h>
#include <EgomotionTracking/components/EVP/Foundation/Files/File.h>
#include <EgomotionTracking/components/EVP/Foundation/Files/InFile.h>
#include <EgomotionTracking/components/EVP/Foundation/Files/OutFile.h>
#include <EgomotionTracking/components/EVP/VisualSpace/Cameras/Devices/CameraDevice.h>
#include <EgomotionTracking/components/EVP/VisualSpace/Cameras/RadiometricCalibration/Common/Exposure.h>

//IVT
#include <VideoCapture/Linux1394Capture2.h>
#include <Interfaces/VideoCaptureInterface.h>
#include <libraw1394/raw1394.h>
#include <dc1394/dc1394.h>

#define MAX_CAMERAS 4

//ARMAR3A_UID
#define ARMAR3A_LEFT_CAMERA_UID             "00B09D0100501BDE"
#define ARMAR3A_RIGHT_CAMERA_UID            "00B09D0100501BE6"
#define ARMAR3A_LEFT_CAMERA_FOVEAL_UID      "00B09D01006F3F69"
#define ARMAR3A_RIGHT_CAMERA_FOVEAL_UID     "00B09D0100501C05"

//ARMAR3B_UID

#define ARMAR3B_LEFT_CAMERA_UID             "00B09D01006BE716"
#define ARMAR3B_RIGHT_CAMERA_UID            "00B09D01006BE719"
#define ARMAR3B_LEFT_CAMERA_FOVEAL_UID      "00B09D01006BE71E"
#define ARMAR3B_RIGHT_CAMERA_FOVEAL_UID     "00B09D01006BE710"

//TRIPOD_UID
#define TRIPOD_LEFT_CAMERA_UID              "00B09D01006BE76D"
#define TRIPOD_RIGHT_CAMERA_UID             "00B09D01006BE76E"

//TRIPOD_UID 2
#define TRIPOD_2_LEFT_CAMERA_UID            "00B09D01009A72CF"
#define TRIPOD_2_RIGHT_CAMERA_UID           "00B09D01009A72CC"

//TRIPOD_UID 3
#define TRIPOD_3_LEFT_CAMERA_UID            "00B09D0100A98967"
#define TRIPOD_3_RIGHT_CAMERA_UID           "00B09D0100A98968"

//HEAD
#define HEAD_LEFT_CAMERA_UID                "00B09D01006BE715"
#define HEAD_RIGHT_CAMERA_UID               "00B09D01006BE751"
#define HEAD_LEFT_CAMERA_FOVEAL_UID         "00B09D01006BE717"
#define HEAD_RIGHT_CAMERA_FOVEAL_UID        "00B09D01006BE754"

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace Devices
            {
                namespace EVP_ArmarX
                {
                    class CCameraDeviceDragonFlyX: public CCameraDevice
                    {
                    public:

                        struct CamerasInformation
                        {
                            uint m_TotalCameras;
                            long unsigned int m_BitFlags;
                            struct ArmarXCameras
                            {
                                bool m_PeripherialRig;
                                bool m_FovealRig;
                            };
                            ArmarXCameras m_Armar3A;
                            ArmarXCameras m_Armar3B;
                            ArmarXCameras m_Head;
                            bool m_TripodRigV1;
                            bool m_TripodRigV2;
                        };

                        static CamerasInformation GetAvailableRegisteredCameras();
                        CCameraDeviceDragonFlyX(const_string pID0, const_string pID1 = NULL, const_string pID2 = NULL, const_string pID3 = NULL);
                        CCameraDeviceDragonFlyX(CLinux1394Capture2* pLinux1394Capture2, CByteImage** ppImagesBayerPattern, CByteImage** ppByteImages, const_string pID, uint CameraIndex);
                        ~CCameraDeviceDragonFlyX() override;

                        real GetMinimalExposureValue() override;
                        real GetMaximalExposureValue() override;
                        real GetStepExposureValue() override;
                        real GetExposureValue() override;
                        real GetExposureTime() override;

                        real GetMinimalNonBlankExposure(const byte NonBlankValue, const bool MultipleCameras, const real ImageFraction, const uint ExposureIntervalIncrement = 1u);
                        real GetMaximalNonWhiteExposure(const byte NonWhiteValue, const bool MultipleCameras, const real ImageFraction, const uint ExposureIntervalIncrement = 1u);

                        CCameraDevice::OperationMode GetExposureMode() override;
                        bool SetExposureMode(const CCameraDevice::OperationMode ExposureOperationMode) override;
                        bool SetExposureValue(const real ExposureValue) override;
                        bool SetExposureValueNoDelay(const real ExposureValue);

                        bool Flush(const uint Drops = 1);

                        real GetMinimalWhiteBalanceRedValue() override;
                        real GetMaximalWhiteBalanceRedValue() override;
                        real GetStepWhiteBalanceRedValue() override;
                        bool SetStepWhiteBalanceRedValue(const real RedValue) override;

                        real GetMinimalWhiteBalanceBlueValue() override;
                        real GetMaximalWhiteBalanceBlueValue() override;
                        real GetStepWhiteBalanceBlueValue() override;
                        bool SetStepWhiteBalanceBlueValue(const real BlueValue) override;

                        CCameraDevice::OperationMode GetWhiteBalanceMode() override;
                        bool SetWhiteBalanceMode(const CCameraDevice::OperationMode WhiteBalanceOperationMode) override;

                        vector<CCameraDevice::ImageType> GetSupportedImageTypes() override;
                        vector<CCameraDevice::ImageColorInterpolation> GetSupportedImageColorInterpolations() override;
                        vector<CCameraDevice::ImageResolution> GetSupportedImageResolutions() override;
                        vector<CCameraDevice::CapturingFrameRate> GetSupportedCapturingFrameRates() override;
                        bool IsSupportedConfiguration(const CCameraDevice::ImageType Type, const CCameraDevice::ImageColorInterpolation ColorInterpolation, const CCameraDevice::ImageResolution Resolution, const CCameraDevice::CapturingFrameRate FrameRate) override;

                        bool OpenCamera(const CCameraDevice::ImageType Type, const CCameraDevice::ImageColorInterpolation ColorInterpolation, const CCameraDevice::ImageResolution Resolution, const CCameraDevice::CapturingFrameRate FrameRate) override;
                        bool CloseCamera() override;

                        virtual  bool CaptureImage();
                        bool CaptureImage(TImage<byte>* pDiscreteIntensityImage) override;
                        bool CaptureImage(TImage<real>* pContinuousIntensityImage) override;
                        bool CaptureImage(TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage) override;
                        bool CaptureImage(TImage<CContinuousTristimulusPixel>* pContinuousRGBImage) override;

                        bool MapImage(TImage<byte>* pDiscreteIntensityImage);
                        bool MapImage(TImage<real>* pContinuousIntensityImage);
                        bool MapImage(TImage<CDiscreteTristimulusPixel>* pDiscreteRGBImage);
                        bool MapImage(TImage<CContinuousTristimulusPixel>* pContinuousRGBImage);

                        std::string GetDeviceName() override;
                        bool HasDeviceOwnerShip();

                        const TImage<byte> GetIndexedBayerPatternImage();

                        CCameraDeviceDragonFlyX* GetCameraDevicesByIndex(const uint Index);

                        bool CaptureBayerPattern();
                        BayerPatternType GetBayerPatternType() override;

                        //TODO ADD TO CCameraDevice
                        const list<RadiometricCalibration::CExposure> GetExposures();
                        bool SaveExposures(const_string pPathFileName);
                        static const list<RadiometricCalibration::CExposure> LoadExposures(const_string pPathFileName);

                        bool SaveIndexedBayerPatternImageToFile(const_string pPathFileName) const;

                    protected:

                        static dc1394camera_t* GetCameraHandleById(CLinux1394Capture2* pLinux1394Capture2, const string& IdText);

                        bool LoadFeatures();
                        bool FlushImages(const uint TotalImages);

                        bool m_HasDeviceOwnerShip;
                        std::string m_Ids[MAX_CAMERAS];
                        CCameraDeviceDragonFlyX* m_pCameraDevices[MAX_CAMERAS];
                        uint m_CameraIndex;
                        uint m_TotalCameras;
                        CByteImage** m_ppImagesBayerPattern;
                        CByteImage** m_ppByteImages;
                        CByteImage* m_pByteImage;
                        CByteImage* m_pImageBayerPattern;
                        CLinux1394Capture2* m_pLinux1394Capture2;
                        dc1394camera_t* m_pCameraHandle;
                        dc1394feature_info_t m_ExposureFeatureInformation;
                        dc1394feature_info_t m_ShutterFeatureInformation;
                        dc1394feature_info_t m_WhiteBalanceFeatureInformation;
                        uint m_LastExposure;
                    };
                }
            }
        }
    }
}

