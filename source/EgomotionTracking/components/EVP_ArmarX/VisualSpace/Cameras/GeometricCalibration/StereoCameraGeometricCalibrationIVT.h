/*
 * StereoCameraGeometricCalibrationIVT.h
 *
 *  Created on: 15.04.2011
 *      Author: gonzalez
 */

#pragma once

//EVP
#include <EgomotionTracking/components/EVP/VisualSpace/Cameras/GeometricCalibration/StereoCameraGeometricCalibration.h>

//IVT
#include <Structs/Structs.h>
#include <Math/Math2d.h>
#include <Calibration/Calibration.h>
#include <Calibration/StereoCalibration.h>

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace GeometricCalibration
            {
                namespace EVP_ArmarX
                {
                    class CStereoCameraGeometricCalibrationIVT: public CStereoCameraGeometricCalibration
                    {
                    public:

                        CStereoCameraGeometricCalibrationIVT();
                        CStereoCameraGeometricCalibrationIVT(CStereoCalibration* pStereoCalibration);
                        ~CStereoCameraGeometricCalibrationIVT() override;

                        bool LoadFromFile(const_string pPathFileName) override;
                        bool Calculate3DPoint(const EVP::Mathematics::_2D::CVector2D& LeftImagePoint, const EVP::Mathematics::_2D::CVector2D& RightImagePoint, Mathematics::_3D::CVector3D& SpacePoint, Mathematics::_3D::CVector3D& LeftOptimizationSpacePoint, Mathematics::_3D::CVector3D& RightOptimizationSpacePoint, CCameraGeometricCalibration::ImageMode ImageMode) override;
                        bool CalculateEpipolarLineInLeftImage(const EVP::Mathematics::_2D::CVector2D& RightImagePoint, EVP::Mathematics::_2D::CVector2D& EpipolarLinePointA, EVP::Mathematics::_2D::CVector2D& EpipolarLinePointB) override;
                        bool CalculateEpipolarLineInRightImage(const EVP::Mathematics::_2D::CVector2D& LeftImagePoint, EVP::Mathematics::_2D::CVector2D& EpipolarLinePointA, EVP::Mathematics::_2D::CVector2D& EpipolarLinePointB) override;
                        bool CalculateEpipolarLineSegmentInLeftImage(const EVP::Mathematics::_2D::CVector2D& RightImagePoint, const real MinimalDistance, const real MaximalDistance, EVP::Mathematics::_2D::CVector2D& MinimalDistanceEpipolarLinePoint, EVP::Mathematics::_2D::CVector2D& MaximalDistanceEpipolarLinePoint) override;
                        bool CalculateEpipolarLineSegmentInRightImage(const EVP::Mathematics::_2D::CVector2D& LeftImagePoint, const real MinimalDistance, const real MaximalDistance, EVP::Mathematics::_2D::CVector2D& MinimalDistanceEpipolarLinePoint, EVP::Mathematics::_2D::CVector2D& MaximalDistanceEpipolarLinePoint) override;

                        const CStereoCalibration* GetStereoCalibrationIVT() const ;

                    protected:

                        inline bool IntersectLineAndSpheres(const Mathematics::_3D::CVector3D& SphereCenter, real SphereRadiusA, real SphereRadiusB, const Mathematics::_3D::CVector3D& LinePointA, const Mathematics::_3D::CVector3D& LinePointB, Mathematics::_3D::CVector3D& IntersectionPointA, Mathematics::_3D::CVector3D& IntersectionPointB);

                        bool m_HasOwnerShip;
                        bool m_IsReady;
                        CStereoCalibration* m_pStereoCalibration;
                    };
                }
            }
        }
    }
}

