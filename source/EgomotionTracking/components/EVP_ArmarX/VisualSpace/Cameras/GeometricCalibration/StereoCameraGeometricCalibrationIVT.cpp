/*
 * StereoCameraGeometricCalibrationIVT.cpp
 *
 *  Created on: 15.04.2011
 *      Author: gonzalez
 */

#include "StereoCameraGeometricCalibrationIVT.h"
#include "CameraGeometricCalibrationIVT.h"

using namespace EVP::Mathematics::_2D;
using namespace EVP::Mathematics::_3D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace GeometricCalibration
            {
                namespace EVP_ArmarX
                {
                    CStereoCameraGeometricCalibrationIVT::CStereoCameraGeometricCalibrationIVT() :
                        CStereoCameraGeometricCalibration(), m_HasOwnerShip(true), m_IsReady(false), m_pStereoCalibration(nullptr)
                    {
                    }

                    CStereoCameraGeometricCalibrationIVT::CStereoCameraGeometricCalibrationIVT(CStereoCalibration* pStereoCalibration) :
                        CStereoCameraGeometricCalibration(), m_HasOwnerShip(false), m_IsReady(pStereoCalibration), m_pStereoCalibration(pStereoCalibration)
                    {
                        if (m_IsReady)
                        {
                            m_pLeftCameraGeometricCalibration = new CCameraGeometricCalibrationIVT(m_pStereoCalibration->GetLeftCalibration());
                            m_pRightCameraGeometricCalibration = new CCameraGeometricCalibrationIVT(m_pStereoCalibration->GetRightCalibration());
                        }
                    }

                    CStereoCameraGeometricCalibrationIVT::~CStereoCameraGeometricCalibrationIVT()
                    {
                        RELEASE_OBJECT_BY_OWNERSHIP(m_pStereoCalibration, m_HasOwnerShip)
                        RELEASE_OBJECT(m_pLeftCameraGeometricCalibration)
                        RELEASE_OBJECT(m_pRightCameraGeometricCalibration)
                    }

                    bool CStereoCameraGeometricCalibrationIVT::LoadFromFile(const_string pPathFileName)
                    {
                        if (m_pStereoCalibration)
                        {
                            if (m_pStereoCalibration->LoadCameraParameters(pPathFileName))
                            {
                                reinterpret_cast<CCameraGeometricCalibrationIVT*>(m_pLeftCameraGeometricCalibration)->LoadCalibration(m_pStereoCalibration->GetLeftCalibration());
                                reinterpret_cast<CCameraGeometricCalibrationIVT*>(m_pRightCameraGeometricCalibration)->LoadCalibration(m_pStereoCalibration->GetRightCalibration());
                                return true;
                            }
                        }
                        else if (m_HasOwnerShip)
                        {
                            if (!m_pStereoCalibration)
                            {
                                m_pStereoCalibration = new CStereoCalibration();
                            }
                            m_IsReady = m_pStereoCalibration->LoadCameraParameters(pPathFileName);
                            if (m_IsReady)
                            {
                                RELEASE_OBJECT(m_pLeftCameraGeometricCalibration)
                                RELEASE_OBJECT(m_pRightCameraGeometricCalibration)
                                m_pLeftCameraGeometricCalibration = new CCameraGeometricCalibrationIVT(m_pStereoCalibration->GetLeftCalibration());
                                m_pRightCameraGeometricCalibration = new CCameraGeometricCalibrationIVT(m_pStereoCalibration->GetRightCalibration());
                            }
                            return m_IsReady;
                        }
                        return false;
                    }

                    bool CStereoCameraGeometricCalibrationIVT::Calculate3DPoint(const CVector2D& LeftImagePoint, const CVector2D& RightImagePoint, CVector3D& SpacePoint, CVector3D& LeftOptimizationSpacePoint, CVector3D& RightOptimizationSpacePoint, CCameraGeometricCalibration::ImageMode ImageMode)
                    {
                        if (m_IsReady)
                        {
                            Vec2d LIP =
                            { float(LeftImagePoint.GetX()), float(LeftImagePoint.GetY()) };
                            Vec2d RIP =
                            { float(RightImagePoint.GetX()), float(RightImagePoint.GetY()) };
                            Vec3d SP;
                            PointPair3d OPP;
                            switch (ImageMode)
                            {
                                case CCameraGeometricCalibration::eDistorted:
                                    m_pStereoCalibration->Calculate3DPoint(LIP, RIP, SP, false, true, &OPP);
                                    break;
                                case CCameraGeometricCalibration::eUndistorted:
                                    m_pStereoCalibration->Calculate3DPoint(LIP, RIP, SP, false, false, &OPP);
                                    break;
                            }
                            SpacePoint.SetValue(SP.x, SP.y, SP.z);
                            LeftOptimizationSpacePoint.SetValue(OPP.p1.x, OPP.p1.y, OPP.p1.z);
                            RightOptimizationSpacePoint.SetValue(OPP.p2.x, OPP.p2.y, OPP.p2.z);
                            return true;
                        }
                        return false;
                    }

                    bool CStereoCameraGeometricCalibrationIVT::CalculateEpipolarLineInLeftImage(const CVector2D& RightImagePoint, CVector2D& EpipolarLinePointA, CVector2D& EpipolarLinePointB)
                    {
                        if (m_IsReady)
                        {
                            Vec2d RIP =
                            { float(RightImagePoint.GetX()), float(RightImagePoint.GetY()) };
                            PointPair2d PointPair;
                            m_pStereoCalibration->CalculateEpipolarLineInLeftImage(RIP, PointPair);
                            EpipolarLinePointA.SetValue(PointPair.p1.x, PointPair.p1.y);
                            EpipolarLinePointB.SetValue(PointPair.p2.x, PointPair.p2.y);
                            return true;
                        }
                        return false;
                    }

                    bool CStereoCameraGeometricCalibrationIVT::CalculateEpipolarLineInRightImage(const CVector2D& LeftImagePoint, CVector2D& EpipolarLinePointA, CVector2D& EpipolarLinePointB)
                    {
                        if (m_IsReady)
                        {
                            Vec2d LIP =
                            { float(LeftImagePoint.GetX()), float(LeftImagePoint.GetY()) };
                            PointPair2d PointPair;
                            m_pStereoCalibration->CalculateEpipolarLineInRightImage(LIP, PointPair);
                            EpipolarLinePointA.SetValue(PointPair.p1.x, PointPair.p1.y);
                            EpipolarLinePointB.SetValue(PointPair.p2.x, PointPair.p2.y);
                            return true;
                        }
                        return false;
                    }

                    bool CStereoCameraGeometricCalibrationIVT::CalculateEpipolarLineSegmentInLeftImage(const CVector2D& RightImagePoint, const real MinimalDistance, const real MaximalDistance, CVector2D& MinimalDistanceEpipolarLinePoint, CVector2D& MaximalDistanceEpipolarLinePoint)
                    {
                        if (m_IsReady)
                        {
                            CVector3D Direction;
                            if (!m_pRightCameraGeometricCalibration->MapPointFromImageToSpace(RightImagePoint, Direction, _REAL_ONE_, CCameraGeometricCalibration::eUndistorted))
                            {
                                return false;
                            }
                            const CVector3D& LeftCameraCenter = m_pLeftCameraGeometricCalibration->GetTranslationInverse();
                            const CVector3D& RightCameraCenter = m_pRightCameraGeometricCalibration->GetTranslationInverse();
                            CVector3D SpacePointA, SpacePointB;
                            if (!IntersectLineAndSpheres(LeftCameraCenter, MinimalDistance, MaximalDistance, RightCameraCenter, Direction, SpacePointA, SpacePointB))
                            {
                                return false;
                            }
                            if (!m_pLeftCameraGeometricCalibration->MapPointFromSpaceToImage(SpacePointA, MinimalDistanceEpipolarLinePoint, CCameraGeometricCalibration::eUndistorted))
                            {
                                return false;
                            }
                            if (!m_pLeftCameraGeometricCalibration->MapPointFromSpaceToImage(SpacePointB, MaximalDistanceEpipolarLinePoint, CCameraGeometricCalibration::eUndistorted))
                            {
                                return false;
                            }
                            return true;
                        }
                        return false;
                    }

                    bool CStereoCameraGeometricCalibrationIVT::CalculateEpipolarLineSegmentInRightImage(const CVector2D& LeftImagePoint, const real MinimalDistance, const real MaximalDistance, CVector2D& MinimalDistanceEpipolarLinePoint, CVector2D& MaximalDistanceEpipolarLinePoint)
                    {
                        if (m_IsReady)
                        {
                            CVector3D SpacePointA;
                            CVector3D SpacePointB;
                            if (!m_pLeftCameraGeometricCalibration->MapPointFromImageToSpace(LeftImagePoint, SpacePointA, MinimalDistance, CCameraGeometricCalibration::eUndistorted))
                            {
                                return false;
                            }
                            if (!m_pLeftCameraGeometricCalibration->MapPointFromImageToSpace(LeftImagePoint, SpacePointB, MaximalDistance, CCameraGeometricCalibration::eUndistorted))
                            {
                                return false;
                            }
                            if (!m_pRightCameraGeometricCalibration->MapPointFromSpaceToImage(SpacePointA, MinimalDistanceEpipolarLinePoint, CCameraGeometricCalibration::eUndistorted))
                            {
                                return false;
                            }
                            if (!m_pRightCameraGeometricCalibration->MapPointFromSpaceToImage(SpacePointB, MaximalDistanceEpipolarLinePoint, CCameraGeometricCalibration::eUndistorted))
                            {
                                return false;
                            }
                            return true;
                        }
                        return false;
                    }

                    const CStereoCalibration* CStereoCameraGeometricCalibrationIVT::GetStereoCalibrationIVT() const
                    {
                        return m_pStereoCalibration;
                    }

                    bool CStereoCameraGeometricCalibrationIVT::IntersectLineAndSpheres(const CVector3D& SphereCenter, real SphereRadiusA, real SphereRadiusB, const CVector3D& LinePointA, const CVector3D& LinePointB, CVector3D& IntersectionPointA, CVector3D& IntersectionPointB)
                    {
                        CVector3D LineDirection = LinePointB - LinePointA;
                        LineDirection.Normalize();
                        CVector3D DeltaSphereCenter = SphereCenter - LinePointA;
                        CVector3D SphereCenterProjectionOnLine = DeltaSphereCenter.ScalarProduct(LineDirection) * LineDirection + LinePointA;
                        real DisplacementLength = (SphereCenterProjectionOnLine - SphereCenter).GetLength();
                        if ((DisplacementLength > SphereRadiusA) || (DisplacementLength > SphereRadiusB))
                        {
                            return false;
                        }
                        real ComplementaryDisplacementLengthA = RealSqrt(SphereRadiusA * SphereRadiusA - DisplacementLength * DisplacementLength);
                        real ComplementaryDisplacementLengthB = RealSqrt(SphereRadiusB * SphereRadiusB - DisplacementLength * DisplacementLength);
                        IntersectionPointA = SphereCenterProjectionOnLine + LineDirection * ComplementaryDisplacementLengthA;
                        IntersectionPointB = SphereCenterProjectionOnLine + LineDirection * ComplementaryDisplacementLengthB;
                        return true;
                    }
                }
            }
        }
    }
}

