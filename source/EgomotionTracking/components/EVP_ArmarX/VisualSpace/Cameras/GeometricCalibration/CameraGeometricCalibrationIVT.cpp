/*
 * CameraGeometricCalibrationIVT.cpp
 *
 *  Created on: 15.04.2011
 *      Author: gonzalez
 */

#include "CameraGeometricCalibrationIVT.h"

using namespace EVP::Mathematics::_2D;
using namespace EVP::Mathematics::_3D;

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace GeometricCalibration
            {
                namespace EVP_ArmarX
                {
                    CCameraGeometricCalibrationIVT::CCameraGeometricCalibrationIVT() :
                        CCameraGeometricCalibration(), m_pCalibration(nullptr)
                    {
                    }

                    CCameraGeometricCalibrationIVT::CCameraGeometricCalibrationIVT(const CCalibration* pCalibrationIVT) :
                        CCameraGeometricCalibration(), m_pCalibration(nullptr)
                    {
                        LoadCalibration(pCalibrationIVT);
                    }

                    CCameraGeometricCalibrationIVT::~CCameraGeometricCalibrationIVT()
                        = default;

                    bool CCameraGeometricCalibrationIVT::LoadCalibration(const CCalibration* pCalibrationIVT)
                    {
                        if (pCalibrationIVT)
                        {
                            m_pCalibration = pCalibrationIVT;
                            const CCalibration::CCameraParameters& Parameters = m_pCalibration->GetCameraParameters();
                            m_ImageSize.Set(Parameters.width, Parameters.height);
                            m_FocalLengthX = Parameters.focalLength.x;
                            m_FocalLengthY = Parameters.focalLength.y;
                            for (uint i = 0; i < _CAMERA_GEOMETRIC_CALIBRATION_TOTAL_DISTORTION_COEFFICIENTS_; ++i)
                            {
                                m_DistortionCoefficients[i] = Parameters.distortion[i];
                            }
                            m_ImagePrincipalPoint.SetValue(Parameters.principalPoint.x, Parameters.principalPoint.y);
                            m_Translation.SetValue(Parameters.translation.x, Parameters.translation.y, Parameters.translation.z);
                            m_TranslationInverse.SetValue(m_pCalibration->m_translation_inverse.x, m_pCalibration->m_translation_inverse.y, m_pCalibration->m_translation_inverse.z);
                            const float* pRotationSource = (const float*) &Parameters.rotation;
                            const float* pInverseRotationSource = (const float*) &m_pCalibration->m_rotation_inverse;
                            for (uint r = 0; r < 3; ++r)
                                for (uint c = 0; c < 3; ++c)
                                {
                                    m_Rotation[r][c] = real(*pRotationSource++);
                                    m_RotationInverse[r][c] = real(*pInverseRotationSource++);
                                }
                            return true;
                        }
                        m_pCalibration = nullptr;
                        return false;
                    }

                    bool CCameraGeometricCalibrationIVT::MapPointFromSpaceToImage(const CVector3D& SpacePoint, CVector2D& ImagePoint, const CCameraGeometricCalibration::ImageMode ImageMode)
                    {
                        if (m_pCalibration)
                        {
                            Vec3d SP = { float(SpacePoint.GetX()), float(SpacePoint.GetY()), float(SpacePoint.GetZ()) };
                            Vec2d IP = { 0.0f, 0.0f };
                            switch (ImageMode)
                            {
                                case CCameraGeometricCalibration::eDistorted:
                                    m_pCalibration->WorldToImageCoordinates(SP, IP, true);
                                    break;
                                case CCameraGeometricCalibration::eUndistorted:
                                    m_pCalibration->WorldToImageCoordinates(SP, IP, false);
                                    break;
                            }
                            ImagePoint.SetValue(IP.x, IP.y);
                            return true;
                        }
                        return false;
                    }

                    bool CCameraGeometricCalibrationIVT::MapPointFromImageToSpace(const CVector2D& ImagePoint, CVector3D& SpacePoint, const real Depth, const CCameraGeometricCalibration::ImageMode ImageMode)
                    {
                        if (m_pCalibration)
                        {
                            Vec3d SP = { 0.0f, 0.0f, 0.0f };
                            Vec2d IP = { float(ImagePoint.GetX()), float(ImagePoint.GetY()) };
                            switch (ImageMode)
                            {
                                case CCameraGeometricCalibration::eDistorted:
                                    m_pCalibration->ImageToWorldCoordinates(IP, SP, Depth, true);
                                    break;
                                case CCameraGeometricCalibration::eUndistorted:
                                    m_pCalibration->ImageToWorldCoordinates(IP, SP, Depth, false);
                                    break;
                            }
                            SpacePoint.SetValue(SP.x, SP.y, SP.z);
                            return true;
                        }
                        return false;
                    }

                    real CCameraGeometricCalibrationIVT::GetDistanceToPrincipalPoint(const CVector2D& ImagePoint, const CCameraGeometricCalibration::ImageMode ImageMode) const
                    {
                        if (m_pCalibration)
                        {
                            Vec2d IP = { float(ImagePoint.GetX()), float(ImagePoint.GetY()) };
                            switch (ImageMode)
                            {
                                case CCameraGeometricCalibration::eDistorted:
                                    m_pCalibration->UndistortImageCoordinates(IP, IP);
                                    break;
                                case CCameraGeometricCalibration::eUndistorted:
                                    break;
                            }
                            return RealHypotenuse(IP.x - m_ImagePrincipalPoint.GetX(), IP.y - m_ImagePrincipalPoint.GetY());
                        }
                        return _REAL_ZERO_;
                    }
                }
            }
        }
    }
}
