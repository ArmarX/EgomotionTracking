/*
 * CameraGeometricCalibrationIVT.h
 *
 *  Created on: 15.04.2011
 *      Author: gonzalez
 */

#pragma once

//EVP
#include <EgomotionTracking/components/EVP/VisualSpace/Cameras/GeometricCalibration/CameraGeometricCalibration.h>

//IVT
#include <Calibration/Calibration.h>

namespace EVP
{
    namespace VisualSpace
    {
        namespace Cameras
        {
            namespace GeometricCalibration
            {
                namespace EVP_ArmarX
                {
                    class CCameraGeometricCalibrationIVT: public CCameraGeometricCalibration
                    {
                    public:

                        CCameraGeometricCalibrationIVT();
                        CCameraGeometricCalibrationIVT(const CCalibration* pCalibrationIVT);
                        ~CCameraGeometricCalibrationIVT() override;

                        bool LoadCalibration(const CCalibration* pCalibrationIVT);

                        bool MapPointFromSpaceToImage(const Mathematics::_3D::CVector3D& SpacePoint, Mathematics::_2D::CVector2D& ImagePoint, const CCameraGeometricCalibration::ImageMode ImageMode) override;
                        bool MapPointFromImageToSpace(const Mathematics::_2D::CVector2D& ImagePoint, Mathematics::_3D::CVector3D& SpacePoint, const real Depth, const CCameraGeometricCalibration::ImageMode ImageMode) override;
                        real GetDistanceToPrincipalPoint(const Mathematics::_2D::CVector2D& ImagePoint, const CCameraGeometricCalibration::ImageMode ImageMode) const override;

                    protected:

                        const CCalibration* m_pCalibration;
                    };
                }
            }
        }
    }
}

