/*
 * GeodesicSphere.cpp
 *
 *  Created on: Jul 10, 2014
 *      Author: gonzalez
 */

#include "GeodesicSphere.h"

int32_t CGeodesicSphere::Vertex::s_IdCounter = 0;

CGeodesicSphere::CGeodesicSphere()
{
    memset(m_pRoots, 0, sizeof(Face*) * 4);
}

CGeodesicSphere::~CGeodesicSphere()
{
    Clear();
}

void CGeodesicSphere::Generate(const int MaximalLevels)
{
    Clear();

    if (MaximalLevels >= 0)
    {
        Vertex* pVertices[4] = { new Vertex(1.0f, 0.0f, -M_SQRT1_2), new Vertex(-1.0f, 0.0f, -M_SQRT1_2), new Vertex(0.0f, 1.0f, M_SQRT1_2), new Vertex(0.0f, -1.0f, M_SQRT1_2) };

        for (int i = 0 ; i < 4 ; ++i)
        {
            pVertices[i]->Normalize();
            m_VerticesSet.AddVertex(pVertices[i]->GetSphericalCoordinates(), pVertices[i]);
        }

        for (int i = 0 ; i < 4 ; ++i)
        {
            m_pRoots[i] = new Face(pVertices, &m_VerticesSet, Face::Type(i), MaximalLevels);
        }
    }
}

int CGeodesicSphere::GetTotalTriangles() const
{
    if (m_pRoots[0])
    {
        int TotalTriangles = 4;

        for (int i = 0 ; i < 4 ; ++i)
            if (m_pRoots[i])
            {
                TotalTriangles += m_pRoots[i]->GetTotalSubFaces();
            }

        return TotalTriangles;
    }

    return 0;
}

void CGeodesicSphere::Clear()
{
    m_VerticesSet.Clear();

    if (m_pRoots[0])
        for (int i = 0 ; i < 4 ; ++i)
        {
            delete m_pRoots[i];
            m_pRoots[i] = NULL;
        }
}
