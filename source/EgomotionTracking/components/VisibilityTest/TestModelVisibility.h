#pragma once

#include "GeodesicSphere.h"

#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/RenderingModel.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/ParametricLineRenderer.h>

#include <Calibration/Calibration.h>
#include <Image/ByteImage.h>

#include <Inventor/SbVec3f.h>

#include <map>

class TestModelVisibility
{
public:

    TestModelVisibility();
    virtual ~TestModelVisibility();

    void setParametricLineRenderer(CParametricLineRenderer& parametricLineRenderer);
    void setRenderingModel(CRenderingModel* pRenderingModel);
    void setCameraCalibration(CCalibration const* pCameraCalibration);
    void setGeodesicSphereLevels(unsigned int levels);
    void setOutputImage(CByteImage* pOutputImage);
    void reset();
    bool testVisibilityNextCameraPosition();

private:

    void setAllStaticModelElementsToInvisible();
    void calculateModelOffsetAndCameraDistance();

    CParametricLineRenderer m_parametricLineRenderer;
    CRenderingModel* m_pRenderingModel;
    CGeodesicSphere m_geodesicSphere;
    CCalibration const* m_pCameraCalibration;

    CByteImage* m_pOutputImage;

    std::map<std::pair<float, float>, CGeodesicSphere::Vertex*> m_geodesicSphereVerticesSet;
    unsigned int m_geodesicSphereLevels;
    std::map<std::pair<float, float>, CGeodesicSphere::Vertex*>::iterator m_iteratorCurrentCameraPosition;
    SbVec3f m_modelOffset;
    float m_cameraDistance;
};

