/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    EgomotionTracker::components::VisibilityTest
 * @author     Thomas von der Heyde ( tvh242 at hotmail dot com )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VisibilityTest.h"

/* EgomotionTracker includes. */

#include <EgomotionTracking/components/EgomotionTracking/Helpers/XMLPrimitives.h>

/* Inventor includes. */

#include <Inventor/SbMatrix.h>

#include <Image/ImageProcessor.h>

using namespace armarx;
using namespace visionx;

VisibilityTest::VisibilityTest() : m_pOutputImage(NULL), m_imageWidth(640), m_imageHeight(480), m_minScreenLineLength(10), m_status(eVisibilityTestIdle), m_autoExecution(true),
    m_doStep(false), m_sphereLevels(2)
{

}

VisibilityTest::~VisibilityTest()
{

}

void VisibilityTest::onInitImageProcessor()
{
    loadConfiguration();
}

void VisibilityTest::onConnectImageProcessor()
{
    m_pOutputImage = new CByteImage(640, 480, CByteImage::eRGB24, false);
    ::ImageProcessor::Zero(m_pOutputImage);

    enableResultImages(1, ImageDimension(640, 480), visionx::eRgb);

    /* Load camera calibration file. */
    setlocale(LC_NUMERIC, "C");

    if (!m_stereoCalibrationGL.LoadFromFile("/home/SMBAD/heyde/home/TrackingFiles/EgomotionTrackingRessources/stereo_calibration.txt"))
    {
        std::cout << "ERROR! Could not load camera calibration file!" << std::endl;
        return;
    }

    /* Load IV model and global transformation files. */
    const std::string filename("/home/SMBAD/heyde/home/IV_Models/BigKitchen/LargeKitchen03.xml");

    if (!m_renderingModel.loadRVLModelFromXMLFile(filename))
    {
        std::cout << "ERROR! Could not load XML model file!" << std::endl;
        return;
    }

    QDomDocument document;

    if (!XMLPrimitives::ReadXMLDocumentFromFile(document, "/home/SMBAD/heyde/home/TrackingFiles/EgomotionTrackingRessources/bigKitchen/RootModelTransformation.xml"))
    {
        return;
    }

    QDomElement rootElement = document.documentElement();
    SbMatrix globalTransformation;

    if (XMLPrimitives::DeserializeQDomElementToSbMatrix(rootElement, globalTransformation))
    {
        m_renderingModel.SetGlobalModelElementsTransformation(globalTransformation);
    }

    /* Configure parametric line renderer. */
    m_parametricLineRenderer.SetVisibleScreenRegion(0, m_imageWidth, 0, m_imageHeight);
    m_parametricLineRenderer.SetRenderingModel(&m_renderingModel);
    m_parametricLineRenderer.SetImageSize(m_imageWidth, m_imageHeight);
    m_parametricLineRenderer.SetMinScreenLineLength(m_minScreenLineLength);
    m_parametricLineRenderer.SetProjectionMatrixGl(m_stereoCalibrationGL.GetLeftProjectionMatrixGL());

    /* Configure TestModelVisibility. */
    m_testModelVisibility.setParametricLineRenderer(m_parametricLineRenderer);
    m_testModelVisibility.setRenderingModel(&m_renderingModel);
    m_testModelVisibility.setCameraCalibration(m_stereoCalibrationGL.GetStereoCalibration().GetLeftCalibration());
    m_testModelVisibility.setOutputImage(m_pOutputImage);
    m_testModelVisibility.setGeodesicSphereLevels(m_sphereLevels);
}

void VisibilityTest::onDisconnectImageProcessor()
{

}

void VisibilityTest::onExitImageProcessor()
{
    delete m_pOutputImage;
}

void VisibilityTest::process()
{
    if (m_status == eVisibilityTestIdle)
    {

    }
    else
    {
        provideResultImages(&m_pOutputImage);

        if (m_autoExecution == true || (m_autoExecution == false && m_doStep == true))
        {
            ::ImageProcessor::Zero(m_pOutputImage);

            if (!m_testModelVisibility.testVisibilityNextCameraPosition())
            {
                m_status = eVisibilityTestIdle;
                std::cout << "Finished testing model visibility" << std::endl;
                const std::string filename("/home/SMBAD/heyde/home/IV_Models/BigKitchen/LargeKitchen04.xml");
                m_renderingModel.saveRVLModelToXMLFile(filename);
            }

            if (m_autoExecution == false)
            {
                m_doStep = false;
            }
        }
    }
}

void VisibilityTest::loadConfiguration()
{
    m_imageWidth = getProperty<float>("imageWidth").getValue();
    m_imageHeight = getProperty<float>("imageHeight").getValue();
    m_minScreenLineLength = getProperty<float>("minScreenLineLength").getValue();
}

void VisibilityTest::start(const Ice::Current& c)
{
    m_testModelVisibility.reset();
    m_status = eVisibilityTest;
}

void VisibilityTest::stop(const Ice::Current& c)
{
    m_status = eVisibilityTestIdle;
}

void VisibilityTest::setAutoExecution(bool autoExecution, const Ice::Current& c)
{
    m_autoExecution = autoExecution;
}

bool VisibilityTest::getAutoExecution(const Ice::Current& c)
{
    return m_autoExecution;
}

bool VisibilityTest::doNextStep(const Ice::Current& c)
{
    if (m_status == eVisibilityTestIdle || m_autoExecution == true)
    {
        return false;
    }

    m_doStep = true;
    return true;
}

void VisibilityTest::setImageWidth(float imageWidth, const Ice::Current& c)
{
    m_imageWidth = imageWidth;
    m_parametricLineRenderer.SetVisibleScreenRegion(0, m_imageWidth, 0, m_imageHeight);
    m_parametricLineRenderer.SetImageSize(m_imageWidth, m_imageHeight);
}

float VisibilityTest::getImageWidth(const Ice::Current& c)
{
    return m_imageWidth;
}

void VisibilityTest::setImageHeight(float imageHeight, const Ice::Current& c)
{
    m_imageHeight = imageHeight;
    m_parametricLineRenderer.SetVisibleScreenRegion(0, m_imageWidth, 0, m_imageHeight);
    m_parametricLineRenderer.SetImageSize(m_imageWidth, m_imageHeight);
}

float VisibilityTest::getImageHeight(const Ice::Current& c)
{
    return m_imageHeight;
}

void VisibilityTest::setMinScreenLineLength(float minScreenLineLength, const Ice::Current& c)
{
    m_minScreenLineLength = minScreenLineLength;
    m_parametricLineRenderer.SetMinScreenLineLength(m_minScreenLineLength);
}

float VisibilityTest::getMinScreenLineLength(const Ice::Current& c)
{
    return m_minScreenLineLength;
}

void VisibilityTest::setSphereLevels(int levels, const Ice::Current& c)
{
    m_sphereLevels = levels;
    m_testModelVisibility.setGeodesicSphereLevels(m_sphereLevels);
}

int VisibilityTest::getSphereLevels(const Ice::Current& c)
{
    return m_sphereLevels;
}

