/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    EgomotionTracker::components::VisibilityTest
 * @author     Thomas von der Heyde ( tvh242 at hotmail dot com )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "TestModelVisibility.h"

/* VisionX includes. */

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/interface/core/ImageProviderInterface.h>

/* STL includes. */

#include <string>
#include <vector>

/* EVP includes. */

#include <Calibration/StereoCalibration.h>
#include <Image/ByteImage.h>

/* EgomotionTracker includes. */

#include <EgomotionTracking/components/EgomotionTracking/Misc/CameraCalibration/StereoCalibrationGL.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/RenderingModel.h>
#include <EgomotionTracking/components/EgomotionTracking/Processing/ParametricLineRenderer/ParametricLineRenderer.h>

#include <VisionX/interface/components/VisibilityTestInterface.h>

namespace armarx
{
    class VisibilityTestProcessorPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        VisibilityTestProcessorPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<float>("imageWidth", 640, "Image width.");
            defineOptionalProperty<float>("imageHeight", 480, "Image height.");
            defineOptionalProperty<float>("minScreenLineLength", 10, "Minimal screen line length. Lines shorter than this limit are not drawn.");
        }
    };

    class VisibilityTest:
        virtual public visionx::ImageProcessor,
        virtual public VisibilityTestInterface
    {
    public:

        VisibilityTest();
        ~VisibilityTest() override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new VisibilityTestProcessorPropertyDefinitions(getConfigIdentifier()));
        }

        /* Inherited from ImageProcessor. */

        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onDisconnectImageProcessor() override;
        void onExitImageProcessor() override;
        void process() override;

        std::string getDefaultName() const override
        {
            return "VisibilityTest";
        }

        /* Interface implementations. */

        void start(const Ice::Current& c = Ice::emptyCurrent) override;
        void stop(const Ice::Current& c = Ice::emptyCurrent) override;
        void setAutoExecution(bool autoExecution, const Ice::Current& c = Ice::emptyCurrent) override;
        bool getAutoExecution(const Ice::Current& c = Ice::emptyCurrent) override;
        bool doNextStep(const Ice::Current& c = Ice::emptyCurrent) override;
        void setImageWidth(float imageWidth, const Ice::Current& c = Ice::emptyCurrent) override;
        float getImageWidth(const Ice::Current& c = Ice::emptyCurrent) override;
        void setImageHeight(float imageHeight, const Ice::Current& c = Ice::emptyCurrent) override;
        float getImageHeight(const Ice::Current& c = Ice::emptyCurrent) override;
        void setMinScreenLineLength(float minScreenLineLength, const Ice::Current& c = Ice::emptyCurrent) override;
        float getMinScreenLineLength(const Ice::Current& c = Ice::emptyCurrent) override;
        void setSphereLevels(int levels, const Ice::Current& c = Ice::emptyCurrent) override;
        int getSphereLevels(const Ice::Current& c = Ice::emptyCurrent) override;

    protected:

        CByteImage* m_pOutputImage;

        float m_imageWidth;
        float m_imageHeight;
        float m_minScreenLineLength;

        CStereoCalibrationGL m_stereoCalibrationGL;
        CRenderingModel m_renderingModel;
        CParametricLineRenderer m_parametricLineRenderer;
        TestModelVisibility m_testModelVisibility;

        VisibilityTestStatus m_status;
        bool m_autoExecution;
        bool m_doStep;
        int m_sphereLevels;

        void loadConfiguration();
    };
}

