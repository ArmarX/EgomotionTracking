#include "TestModelVisibility.h"

#include <EgomotionTracking/components/EgomotionTracking/Helpers/OpenGLCalculations.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/PrimitiveDrawer.h>

#include <EgomotionTracking/components/RVL/Representation/ModelBased/GeometricGraph/Base/ModelMesh.h>

#include <math.h>

#include <Inventor/SbVec3f.h>
#include <Inventor/SbMatrix.h>
#include <Inventor/SbRotation.h>

#include <Math/Math2d.h>

TestModelVisibility::TestModelVisibility() : m_geodesicSphereLevels(2)
{

}

TestModelVisibility::~TestModelVisibility()
{

}

void TestModelVisibility::setParametricLineRenderer(CParametricLineRenderer& parametricLineRenderer)
{
    m_parametricLineRenderer = parametricLineRenderer;
}

void TestModelVisibility::setRenderingModel(CRenderingModel* pRenderingModel)
{
    m_pRenderingModel = pRenderingModel;
}

void TestModelVisibility::setCameraCalibration(CCalibration const* pCameraCalibration)
{
    m_pCameraCalibration = pCameraCalibration;
}

void TestModelVisibility::setGeodesicSphereLevels(unsigned int levels)
{
    m_geodesicSphereLevels = levels;
}

void TestModelVisibility::setOutputImage(CByteImage* pOutputImage)
{
    m_pOutputImage = pOutputImage;
}

void TestModelVisibility::reset()
{
    setAllStaticModelElementsToInvisible();
    calculateModelOffsetAndCameraDistance();
    m_geodesicSphere.Generate(m_geodesicSphereLevels);
    m_geodesicSphereVerticesSet = m_geodesicSphere.GetVerticesSet().m_Set;
    m_iteratorCurrentCameraPosition = m_geodesicSphereVerticesSet.begin();
    std::cout << "CameraPositionCount: " << m_geodesicSphere.GetTotalVertices() << std::endl;
}

bool TestModelVisibility::testVisibilityNextCameraPosition()
{
    if (m_iteratorCurrentCameraPosition != m_geodesicSphereVerticesSet.end())
    {
        CGeodesicSphere::Vertex* pNormalisedCameraPosition = m_iteratorCurrentCameraPosition->second;

        SbVec3f cameraPosition(pNormalisedCameraPosition->m_X, pNormalisedCameraPosition->m_Y, pNormalisedCameraPosition->m_Z);
        cameraPosition *= m_cameraDistance;
        SbVec3f normalVector(pNormalisedCameraPosition->m_X, pNormalisedCameraPosition->m_Y, pNormalisedCameraPosition->m_Z);
        normalVector *= -1.0;

        SbVec3f upVector(0, 0, 1);
        SbVec3f zPrimeVector(normalVector[0], normalVector[1], normalVector[2]);
        SbVec3f xPrimeVector = zPrimeVector.cross(upVector);
        SbVec3f yPrimeVector = zPrimeVector.cross(xPrimeVector);

        SbVec3f offsetCameraPosition(cameraPosition[0] + m_modelOffset[0], cameraPosition[1] + m_modelOffset[1], cameraPosition[2] + m_modelOffset[2]);
        SbMatrix tempMatrix(xPrimeVector[0], xPrimeVector[1], xPrimeVector[2], 0,
                            yPrimeVector[0], yPrimeVector[1], yPrimeVector[2], 0,
                            zPrimeVector[0], zPrimeVector[1], zPrimeVector[2], 0,
                            0, 0, 0, 1);
        SbRotation rotationMatrix(tempMatrix);
        SbRotation convertedRotationMatrix = OpenGLCalculations::ConvertGLCameraOrientationCoordinateSystems(rotationMatrix);
        SbMatrix viewingMatrix = OpenGLCalculations::CalculateGLViewingMatrixFromGLCameraCoordinates(offsetCameraPosition, convertedRotationMatrix);

        //        std::cout << "Camera position is: " << offsetCameraPosition[0] << "/" << offsetCameraPosition[1] << "/" << offsetCameraPosition[2] << std::endl;
        //        std::cout << "Normal is: " << normalVector[0] << "/" << normalVector[1] << "/" << normalVector[2] << std::endl;
        //        std::cout << "Camera is looking at point: " << offsetCameraPosition[0] + normalVector[0] * m_cameraDistance << "/"
        //                                                    << offsetCameraPosition[1] + normalVector[1] * m_cameraDistance << "/"
        //                                                    << offsetCameraPosition[2] + normalVector[2] * m_cameraDistance << std::endl;
        //        std::cout << "XPrime is: " << xPrimeVector[0] << "/" << xPrimeVector[1] << "/" << xPrimeVector[2] << std::endl;
        //        std::cout << "YPrime is: " << yPrimeVector[0] << "/" << yPrimeVector[1] << "/" << yPrimeVector[2] << std::endl;
        //        std::cout << "ZPrime is: " << zPrimeVector[0] << "/" << zPrimeVector[1] << "/" << zPrimeVector[2] << std::endl;

        m_parametricLineRenderer.SetCameraPosition(viewingMatrix);

        m_parametricLineRenderer.RenderVisibleLines();

        const std::vector<CScreenLine>& visibleScreenLines =  m_parametricLineRenderer.GetVisibleScreenLineList();

        for (std::vector<CScreenLine>::const_iterator lineIterator = visibleScreenLines.begin(); lineIterator != visibleScreenLines.end(); ++lineIterator)
        {
            lineIterator->m_pWorldLineReference->m_pModelEdgeReference->SetVisibility(RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement::eStaticVisible);
            PrimitiveDrawer::DrawLine(lineIterator->m_p1, lineIterator->m_p2, 0, 0, 255, 1, m_pOutputImage);
        }

        const std::vector<CFaceTriangle>& projectedScreenTriangles =  m_parametricLineRenderer.GetProjectedScreenTriangleList();

        for (std::vector<CFaceTriangle>::const_iterator triangleIterator = projectedScreenTriangles.begin(); triangleIterator != projectedScreenTriangles.end(); ++triangleIterator)
        {
            if (!m_parametricLineRenderer.Check2dTriangleIsHidden(*triangleIterator, projectedScreenTriangles, 7))
            {
                triangleIterator->m_pWorldFaceTriangleReference->m_pModelFaceReference->SetVisibility(RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement::eStaticVisible);
            }
        }

        ++m_iteratorCurrentCameraPosition;
        return true;
    }

    return false;
}

void TestModelVisibility::setAllStaticModelElementsToInvisible()
{
    std::list<CLine>& modelLines = m_pRenderingModel->GetModelLineList();

    for (std::list<CLine>::iterator lineIterator = modelLines.begin(); lineIterator != modelLines.end(); ++lineIterator)
    {
        lineIterator->m_pModelEdgeReference->SetVisibility(RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement::eStaticOccluded);
    }

    std::list<CFaceTriangle>& modelFaceTriangles = m_pRenderingModel->GetModelFaceTriangleList();

    for (std::list<CFaceTriangle>::iterator faceTriangleIterator = modelFaceTriangles.begin(); faceTriangleIterator != modelFaceTriangles.end(); ++faceTriangleIterator)
    {
        faceTriangleIterator->m_pModelFaceReference->SetVisibility(RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement::eStaticOccluded);
    }
}

void TestModelVisibility::calculateModelOffsetAndCameraDistance()
{
    /* Get model bounding box. */
    SbVec3f boundingBoxMin, boundingBoxMax;
    m_pRenderingModel->CalculateAlignedModelBoundingBox(boundingBoxMin, boundingBoxMax);

    /* Bounding box center. Serves as center of the geodesic sphere. */
    m_modelOffset = SbVec3f((boundingBoxMin[0] + boundingBoxMax[0]) / 2, (boundingBoxMin[1] + boundingBoxMax[1]) / 2, (boundingBoxMin[2] + boundingBoxMax[2]) / 2);

    /* Bounding sphere radius. Used to calculate the distance at which the entire model is visible. */
    float boundingSphereRadius = sqrt((boundingBoxMax[0] - boundingBoxMin[0]) * (boundingBoxMax[0] - boundingBoxMin[0]) +
                                      (boundingBoxMax[1] - boundingBoxMin[1]) * (boundingBoxMax[1] - boundingBoxMin[1]) +
                                      (boundingBoxMax[2] - boundingBoxMin[2]) * (boundingBoxMax[2] - boundingBoxMin[2])) / 2;

    /* Get camera parameters. */
    CCalibration::CCameraParameters const& cameraParameters = m_pCameraCalibration->GetCameraParameters();
    float const width = static_cast<float>(cameraParameters.width);
    float const height = static_cast<float>(cameraParameters.height);
    Vec2d const focalLength = cameraParameters.focalLength;

    /* Calculate camera angles of view. */
    float alphaX = 2 * atan(width / (focalLength.x * 2));
    float alphaY = 2 * atan(height / (focalLength.y * 2));

    /* Calculate minimal distance at which the entire bounding sphere (model) is visible. */
    m_cameraDistance = boundingSphereRadius / sin(std::min(alphaX / 2, alphaY / 2));

    std::cout << "ModelOffset: " << m_modelOffset[0] << "/" << m_modelOffset[1] << "/" << m_modelOffset[2] << std::endl;
    std::cout << "CameraDistance: " << m_cameraDistance << std::endl;
}
