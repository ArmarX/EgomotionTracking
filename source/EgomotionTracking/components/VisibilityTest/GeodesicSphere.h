/*
 * GeodesicSphere.h
 *
 *  Created on: Jul 10, 2014
 *      Author: gonzalez
 */

#pragma once

#include <string.h>
#include <cmath>
#include <iostream>
#include <deque>
#include <map>
#include <math.h>
#include <utility>
#include <stdint.h>
#include <stdlib.h>

class CGeodesicSphere
{
public:

    static float Rand(const float A, const float B)
    {
        const float Upper = std::max(A, B);
        const float Lower = std::min(A, B);
        const float Range = Upper - Lower;
        return Range * float(rand()) / float(2147483647) + Lower;
    }

    struct Vertex
    {
        inline Vertex(const float X, const float Y, const float Z) :
            m_X(X),
            m_Y(Y),
            m_Z(Z),
            m_R0(0.85),
            m_R(1.0),
            m_R1(2.15),
            m_Id(++s_IdCounter)
        {

        }

        std::pair<float, float> GetSphericalCoordinates() const
        {
            return std::pair<float, float>(std::acos(m_Z), std::atan2(m_Y, m_X));
        }

        void Normalize()
        {
            const float Magnitud = std::sqrt(m_X * m_X + m_Y * m_Y + m_Z * m_Z);
            m_X /= Magnitud;
            m_Y /= Magnitud;
            m_Z /= Magnitud;
        }

        void Display() const
        {
            std::cout << "P[" << m_X << ",\t" << m_Y << ",\t" << m_Z << "]\tR(" << m_R0 << "," << m_R << "," << m_R1 << ")" << std::endl;
        }

        float m_X;
        float m_Y;
        float m_Z;
        float m_R0;
        float m_R;
        float m_R1;
        int32_t m_Id;
        static int32_t s_IdCounter;
    };


    struct VerticesSet
    {
        bool AddVertex(const std::pair<float, float>& SphericalCoordinates, Vertex* pVertex)
        {
            std::pair<std::map<std::pair<float, float>, Vertex*>::iterator, bool> Result = m_Set.insert(std::pair<std::pair<float, float>, Vertex*>(SphericalCoordinates, pVertex));
            return Result.second;
        }

        Vertex* GetVertex(const std::pair<float, float>& SphericalCoordinates)
        {
            std::map<std::pair<float, float>, Vertex*>::iterator pLocation = m_Set.find(SphericalCoordinates);

            if (pLocation == m_Set.end())
            {
                return NULL;
            }

            return pLocation->second;
        }

        Vertex* GetMidSphericalVertex(const Vertex* pVerterxA, const Vertex* pVerterxB)
        {
            float X = (pVerterxA->m_X + pVerterxB->m_X) * 0.5f;
            float Y = (pVerterxA->m_Y + pVerterxB->m_Y) * 0.5f;
            float Z = (pVerterxA->m_Z + pVerterxB->m_Z) * 0.5f;
            const float Displacement = std::sqrt(X * X + Y * Y + Z * Z);
            X /= Displacement;
            Y /= Displacement;
            Z /= Displacement;
            std::pair<float, float> SphericalCoordinates(std::acos(Z), std::atan2(Y, X));
            Vertex* pVertex = GetVertex(SphericalCoordinates);

            if (!pVertex)
            {
                pVertex = new Vertex(X, Y, Z);
                m_Set.insert(std::pair<std::pair<float, float>, Vertex*>(SphericalCoordinates, pVertex));
            }

            return pVertex;
        }

        void Clear()
        {
            for (std::map<std::pair<float, float>, Vertex*>::iterator pLocation = m_Set.begin() ; pLocation != m_Set.end() ; ++pLocation)
            {
                delete pLocation->second;
            }

            m_Set.clear();
        }

        std::map<std::pair<float, float>, Vertex*> m_Set;
    };

    struct Face
    {
        enum Type
        {
            eAlpha = 0, eBeta, eGamma, eDelta
        };

        Face(Vertex* pVertices[4], VerticesSet* pSet, const Type CurrentType, const int MaximalLevels) :
            m_Level(0),
            m_Type(CurrentType),
            m_pParent(NULL)
        {
            const int Map[4][3] = { { 0, 1, 2 }, { 0, 1, 3 }, { 0, 2, 3 }, { 1, 2, 3 } };
            m_pVertexA = pVertices[Map[m_Type][0]];
            m_pVertexB = pVertices[Map[m_Type][1]];
            m_pVertexC = pVertices[Map[m_Type][2]];

            if (m_Level < MaximalLevels)
                for (int i = 0 ; i < 4 ; ++i)
                {
                    m_pChildren[i] = new Face(pSet, this, Face::Type(i), MaximalLevels);
                }
        }

        Face(VerticesSet* pSet, const Face* pParent, const Type CurrentType, const int MaximalLevels) :
            m_Level(pParent->m_Level + 1),
            m_Type(CurrentType),
            m_pParent(pParent)
        {
            switch (m_Type)
            {
                case eAlpha:
                    m_pVertexA = m_pParent->m_pVertexA;
                    m_pVertexB = pSet->GetMidSphericalVertex(m_pParent->m_pVertexA, m_pParent->m_pVertexB);
                    m_pVertexC = pSet->GetMidSphericalVertex(m_pParent->m_pVertexA, m_pParent->m_pVertexC);
                    break;

                case eBeta:
                    m_pVertexA = pSet->GetMidSphericalVertex(m_pParent->m_pVertexA, m_pParent->m_pVertexB);
                    m_pVertexB = m_pParent->m_pVertexB;
                    m_pVertexC = pSet->GetMidSphericalVertex(m_pParent->m_pVertexB, m_pParent->m_pVertexC);
                    break;

                case eGamma:
                    m_pVertexA = pSet->GetMidSphericalVertex(m_pParent->m_pVertexA, m_pParent->m_pVertexC);
                    m_pVertexB = pSet->GetMidSphericalVertex(m_pParent->m_pVertexB, m_pParent->m_pVertexC);
                    m_pVertexC = m_pParent->m_pVertexC;
                    break;

                case eDelta:
                    m_pVertexA = pSet->GetMidSphericalVertex(m_pParent->m_pVertexA, m_pParent->m_pVertexB);
                    m_pVertexB = pSet->GetMidSphericalVertex(m_pParent->m_pVertexB, m_pParent->m_pVertexC);
                    m_pVertexC = pSet->GetMidSphericalVertex(m_pParent->m_pVertexC, m_pParent->m_pVertexA);
                    break;
            }

            if (m_Level < MaximalLevels)
                for (int i = 0 ; i < 4 ; ++i)
                {
                    m_pChildren[i] = new Face(pSet, this, Face::Type(i), MaximalLevels);
                }
            else
            {
                memset(m_pChildren, 0, sizeof(Face*) * 4);
            }
        }

        ~Face()
        {
            if (m_pChildren[0])
                for (int i = 0 ; i < 4 ; ++i)
                {
                    delete m_pChildren[i];
                }
        }

        int GetTotalSubFaces() const
        {
            if (m_pChildren[0])
            {
                int TotalTriangles = 4;

                for (int i = 0 ; i < 4 ; ++i)
                {
                    TotalTriangles += m_pChildren[i]->GetTotalSubFaces();
                }

                return TotalTriangles;
            }

            return 0;
        }

        void GetTerminalFaces(std::deque<const Face*>& TerminalNodes) const
        {
            if (m_pChildren[0])
                for (int i = 0 ; i < 4 ; ++i)
                {
                    m_pChildren[i]->GetTerminalFaces(TerminalNodes);
                }
            else
            {
                TerminalNodes.push_back(this);
            }
        }

        const int m_Level;
        const Type m_Type;
        const Vertex* m_pVertexA;
        const Vertex* m_pVertexB;
        const Vertex* m_pVertexC;
        const Face* m_pParent;
        Face* m_pChildren[4];
    };

    CGeodesicSphere();
    virtual ~CGeodesicSphere();

    void Generate(const int Levels);

    inline int GetTotalVertices() const
    {
        return m_VerticesSet.m_Set.size();
    }

    int GetTotalTriangles() const;

    inline const std::deque<const Face*> GetTerminalFaces() const
    {
        std::deque<const Face*> TerminalFaces;

        for (int i = 0 ; i < 4 ; ++i)
        {
            m_pRoots[i]->GetTerminalFaces(TerminalFaces);
        }

        return TerminalFaces;
    }

    inline const std::map<int64_t, std::pair<const Vertex*, const Vertex*> > GetTerminalEdges() const
    {
        std::map<int64_t, std::pair<const Vertex*, const Vertex*> > TerminalEdges;
        const std::deque<const Face*> TerminalFaces = GetTerminalFaces();

        for (std::deque<const Face*>::const_iterator ppFace = TerminalFaces.begin() ; ppFace != TerminalFaces.end() ; ++ppFace)
        {
            const int64_t AB = (int64_t(std::max((*ppFace)->m_pVertexA->m_Id, (*ppFace)->m_pVertexB->m_Id)) << 32) | int64_t(std::min((*ppFace)->m_pVertexA->m_Id, (*ppFace)->m_pVertexB->m_Id));

            if (TerminalEdges.find(AB) == TerminalEdges.end())
            {
                TerminalEdges[AB] = std::pair<const Vertex*, const Vertex*>((*ppFace)->m_pVertexA, (*ppFace)->m_pVertexB);
            }

            const int64_t BC = (int64_t(std::max((*ppFace)->m_pVertexB->m_Id, (*ppFace)->m_pVertexC->m_Id)) << 32) | int64_t(std::min((*ppFace)->m_pVertexB->m_Id, (*ppFace)->m_pVertexC->m_Id));

            if (TerminalEdges.find(BC) == TerminalEdges.end())
            {
                TerminalEdges[BC] = std::pair<const Vertex*, const Vertex*>((*ppFace)->m_pVertexB, (*ppFace)->m_pVertexC);
            }

            const int64_t CA = (int64_t(std::max((*ppFace)->m_pVertexC->m_Id, (*ppFace)->m_pVertexA->m_Id)) << 32) | int64_t(std::min((*ppFace)->m_pVertexC->m_Id, (*ppFace)->m_pVertexA->m_Id));

            if (TerminalEdges.find(CA) == TerminalEdges.end())
            {
                TerminalEdges[CA] = std::pair<const Vertex*, const Vertex*>((*ppFace)->m_pVertexC, (*ppFace)->m_pVertexA);
            }

        }

        return TerminalEdges;
    }

    inline const VerticesSet& GetVerticesSet() const
    {
        return m_VerticesSet;
    }

public:

    void Clear();

    VerticesSet m_VerticesSet;
    Face* m_pRoots[4];

};

