#include "RGBAColor.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Conversions.h>

CRGBAColor::CRGBAColor() :
    m_r(0.0), m_g(0.0), m_b(0.0), m_a(1.0)
{
}

CRGBAColor::CRGBAColor(float r, float g, float b) :
    m_a(1.0)
{
    Set(r, g, b);
}

CRGBAColor::CRGBAColor(float r, float g, float b, float a)
{
    Set(r, g, b, a);
}

void CRGBAColor::Set(float r, float g, float b)
{
    SetRedChannel(r);
    SetGreenChannel(g);
    SetBlueChannel(b);
}

void CRGBAColor::Set(float r, float g, float b, float a)
{
    Set(r, g, b);
    SetAlphaChannel(a);
}

void CRGBAColor::SetRedChannel(float r)
{
    m_r = Calculations::SaturizeToLimits(r, 0.0, 1.0);
}

void CRGBAColor::SetGreenChannel(float g)
{
    m_g = Calculations::SaturizeToLimits(g, 0.0, 1.0);
}

void CRGBAColor::SetBlueChannel(float b)
{
    m_b = Calculations::SaturizeToLimits(b, 0.0, 1.0);
}

void CRGBAColor::SetAlphaChannel(float a)
{
    m_a = Calculations::SaturizeToLimits(a, 0.0, 1.0);
}

float CRGBAColor::GetRedChannel() const
{
    return m_r;
}

float CRGBAColor::GetGreenChannel() const
{
    return m_g;
}

float CRGBAColor::GetBlueChannel() const
{
    return m_b;
}

float CRGBAColor::GetAlphaChannel() const
{
    return m_a;
}

unsigned char CRGBAColor::GetByteRedChannel() const
{
    return Conversions::FloatColorValueToByteColorValue(m_r);
}

unsigned char CRGBAColor::GetByteBlueChannel() const
{
    return Conversions::FloatColorValueToByteColorValue(m_b);
}

unsigned char CRGBAColor::GetByteGreenChannel() const
{
    return Conversions::FloatColorValueToByteColorValue(m_g);
}

unsigned char CRGBAColor::GetByteAlphaChannel() const
{
    return Conversions::FloatColorValueToByteColorValue(m_a);
}


void CRGBAColor::SetByteRedChannel(unsigned char r)
{
    m_r = Conversions::ByteColorValueToFloatColorValue(r);
}

void CRGBAColor::SetByteGreenChannel(unsigned char g)
{
    m_g = Conversions::ByteColorValueToFloatColorValue(g);
}

void CRGBAColor::SetByteBlueChannel(unsigned char b)
{
    m_b = Conversions::ByteColorValueToFloatColorValue(b);
}

void CRGBAColor::SetByteAlphaChannel(unsigned char a)
{
    m_a = Conversions::ByteColorValueToFloatColorValue(a);
}

void CRGBAColor::SetByteRGBA(unsigned char r, unsigned char g, unsigned char b)
{
    SetByteRedChannel(r);
    SetByteGreenChannel(g);
    SetByteBlueChannel(b);
}

void CRGBAColor::SetByteRGBA(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
    SetByteRGBA(r, g, b);
    SetByteAlphaChannel(a);
}
