#pragma once

class CRGBAColor
{
public:
    CRGBAColor();
    CRGBAColor(float r, float g, float b);
    CRGBAColor(float r, float g, float b, float a);

    void Set(float r, float g, float b);
    void Set(float r, float g, float b, float a);

    void SetByteRGBA(unsigned char r, unsigned char g, unsigned char b);
    void SetByteRGBA(unsigned char r, unsigned char g, unsigned char b, unsigned char a);

    void SetByteRedChannel(unsigned char r);
    void SetByteGreenChannel(unsigned char g);
    void SetByteBlueChannel(unsigned char b);
    void SetByteAlphaChannel(unsigned char a);

    void SetRedChannel(float r);
    void SetGreenChannel(float g);
    void SetBlueChannel(float b);
    void SetAlphaChannel(float a);

    float GetRedChannel() const;
    float GetGreenChannel() const;
    float GetBlueChannel() const;
    float GetAlphaChannel() const;

    unsigned char GetByteRedChannel() const;
    unsigned char GetByteBlueChannel() const;
    unsigned char GetByteGreenChannel() const;
    unsigned char GetByteAlphaChannel() const;

protected:
    float m_r;
    float m_g;
    float m_b;
    float m_a;
};

