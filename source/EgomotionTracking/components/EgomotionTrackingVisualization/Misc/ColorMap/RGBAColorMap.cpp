#include <cstdlib>
#include <float.h>

#include "RGBAColorMap.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>

CRGBAColorMap::CRGBAColorMap() : m_pLUT(NULL), m_lutQuantisationLevels(rgbaColorMapDefaultLUTQuantisationLevels), m_minAlpha(0.0)
{
}

const CRGBAColor CRGBAColorMap::GetColor(float value)
{
    if (m_pLUT != NULL && value >= 0.0 && value <= 1.0)
    {
        return m_pLUT[static_cast<int>(value * m_lutScaleFactor)];
    }
    return CRGBAColor();
}


bool CRGBAColorMap::AddColorKey(float position, const CRGBAColor& color)
{
    if (position < 0.0 || position > 1.0)
    {
        return false;
    }
    m_colorKeys.push_back(CColorKey(position, color));
    return true;
}

bool CRGBAColorMap::GenerateLUT()
{
    if (m_colorKeys.size() <= 0)
    {
        return false;
    }

    if (m_pLUT != NULL)
    {
        delete [] m_pLUT;
        m_pLUT = NULL;
    }

    m_colorKeys.sort(compareColorKey);

    if (m_colorKeys.front().m_position == 0.0 && m_colorKeys.back().m_position == 1.0)
    {
        const unsigned int LUTQuantizationLevels = m_lutQuantisationLevels;
        m_lutScaleFactor = static_cast<float>(LUTQuantizationLevels - 1);
        m_lutSize = LUTQuantizationLevels;
        m_pLUT = new CRGBAColor[m_lutSize];
        const float deltaPosition = 1.0 / static_cast<float>(LUTQuantizationLevels);

        std::list<CColorKey>::const_iterator colorKeyAIterator = m_colorKeys.begin();
        std::list<CColorKey>::const_iterator colorKeyBIterator = colorKeyAIterator;
        ++colorKeyBIterator;

        for (unsigned int currentTableIndex = 0; currentTableIndex < m_lutSize; ++currentTableIndex)
        {
            const float curPosition = deltaPosition * currentTableIndex;

            while (!(curPosition >= colorKeyAIterator->m_position - FLT_EPSILON && curPosition <= colorKeyBIterator->m_position + FLT_EPSILON))
            {
                ++colorKeyAIterator;
                ++colorKeyBIterator;

                if (colorKeyAIterator == m_colorKeys.end() || colorKeyBIterator == m_colorKeys.end())
                {
                    COUT_ERROR << "colorKeyAIterator == m_colorKeys.end() || colorKeyBIterator == m_colorKeys.end()" << std::endl;
                    std::cout << "current index:" << currentTableIndex  << "curPos:" << curPosition << std::endl;
                    exit(0);
                }
            }

            const float intervalLength = colorKeyBIterator->m_position - colorKeyAIterator->m_position;
            const float contributionA = (colorKeyBIterator->m_position - curPosition) / intervalLength;
            const float contributionB = (curPosition - colorKeyAIterator->m_position) / intervalLength;

            const float r = contributionA * colorKeyAIterator->m_color.GetRedChannel() + contributionB * colorKeyBIterator->m_color.GetRedChannel();
            const float g = contributionA * colorKeyAIterator->m_color.GetGreenChannel() + contributionB * colorKeyBIterator->m_color.GetGreenChannel();
            const float b = contributionA * colorKeyAIterator->m_color.GetBlueChannel() + contributionB * colorKeyBIterator->m_color.GetBlueChannel();
            const float a = contributionA * colorKeyAIterator->m_color.GetAlphaChannel() + contributionB * colorKeyBIterator->m_color.GetAlphaChannel();

            m_pLUT[currentTableIndex].Set(r, g, b, a);
        }

    }

    return false;
}

void CRGBAColorMap::SetLUTQuantisationLevels(unsigned int quantisationLevels)
{
    m_lutQuantisationLevels = quantisationLevels;
}

void CRGBAColorMap::SetPredefinedColorMap(CRGBAColorMap::PredefinedColorMap predefinedColorMap)
{
    ClearKeys();

    CRGBAColor definedColors[16];

    switch (predefinedColorMap)
    {
        case eRGB_RedYellowGreen:
            definedColors[0].Set(1.0, 0.0, 0.0);
            definedColors[1].Set(1.0, 1.0, 0.0);
            definedColors[2].Set(0.0, 1.0, 0.0);
            AddColorKey(0.0, definedColors[0]);
            AddColorKey(0.5, definedColors[1]);
            AddColorKey(1.0, definedColors[2]);
            GenerateLUT();
            break;
        case eRGBA_RedYellowGreen:
            definedColors[0].Set(1.0, 0.0, 0.0, 0.0);
            definedColors[1].Set(1.0, 1.0, 0.0, 0.5);
            definedColors[2].Set(0.0, 1.0, 0.0, 1.0);
            AddColorKey(0.0, definedColors[0]);
            AddColorKey(0.5, definedColors[1]);
            AddColorKey(1.0, definedColors[2]);
            GenerateLUT();
            break;

        case eRGB_Hot:
            definedColors[0].SetByteRGBA(0, 0, 189);
            definedColors[1].SetByteRGBA(0, 0, 255);
            definedColors[2].SetByteRGBA(0, 66, 255);
            definedColors[3].SetByteRGBA(0, 132, 255);
            definedColors[4].SetByteRGBA(0, 189, 255);
            definedColors[5].SetByteRGBA(0, 255, 255);
            definedColors[6].SetByteRGBA(66, 255, 189);
            definedColors[7].SetByteRGBA(132, 255, 132);
            definedColors[8].SetByteRGBA(189, 255, 66);
            definedColors[9].SetByteRGBA(255, 255, 0);
            definedColors[10].SetByteRGBA(255, 189, 0);
            definedColors[11].SetByteRGBA(255, 132, 0);
            definedColors[12].SetByteRGBA(255, 66, 0);
            definedColors[13].SetByteRGBA(189, 0, 0);
            definedColors[14].SetByteRGBA(132, 0, 0);
            definedColors[15].SetByteRGBA(128, 0, 0);

            for (int i = 0; i < 16; ++i)
            {
                AddColorKey(static_cast<float>(i) / 15.0, definedColors[i]);
            }
            GenerateLUT();
            break;

        case eRGBA_Hot:
            const float alphaInterval = 1.0 - m_minAlpha;
            definedColors[0].SetByteRGBA(0, 0, 189);
            definedColors[1].SetByteRGBA(0, 0, 255);
            definedColors[2].SetByteRGBA(0, 66, 255);
            definedColors[3].SetByteRGBA(0, 132, 255);
            definedColors[4].SetByteRGBA(0, 189, 255);
            definedColors[5].SetByteRGBA(0, 255, 255);
            definedColors[6].SetByteRGBA(66, 255, 189);
            definedColors[7].SetByteRGBA(132, 255, 132);
            definedColors[8].SetByteRGBA(189, 255, 66);
            definedColors[9].SetByteRGBA(255, 255, 0);
            definedColors[10].SetByteRGBA(255, 189, 0);
            definedColors[11].SetByteRGBA(255, 132, 0);
            definedColors[12].SetByteRGBA(255, 66, 0);
            definedColors[13].SetByteRGBA(189, 0, 0);
            definedColors[14].SetByteRGBA(132, 0, 0);
            definedColors[15].SetByteRGBA(128, 0, 0);
            for (int i = 0; i < 16; ++i)
            {
                const float curPos = static_cast<float>(i) / 15.0;
                definedColors[i].SetAlphaChannel(m_minAlpha + alphaInterval * curPos);
                AddColorKey(curPos, definedColors[i]);
            }
            GenerateLUT();
            break;
    }
}

void CRGBAColorMap::SetMinAlpha(float minAlpha)
{
    m_minAlpha = Calculations::SaturizeToLimits(minAlpha, 0.0, 1.0);
}


void CRGBAColorMap::ClearKeys()
{
    m_colorKeys.clear();
    if (m_pLUT != NULL)
    {
        delete [] m_pLUT;
        m_pLUT = NULL;
    }
}
