#pragma once

#include "RGBAColor.h"
#include <list>

const unsigned int rgbaColorMapDefaultLUTQuantisationLevels = 1000;

class CRGBAColorMap
{
public:
    CRGBAColorMap();

    const CRGBAColor GetColor(float value);

    void ClearKeys();
    bool AddColorKey(float position, const CRGBAColor& color);
    bool GenerateLUT();

    void SetLUTQuantisationLevels(unsigned int quantisationLevels);


    enum PredefinedColorMap
    {
        eRGB_Hot, eRGBA_Hot, eRGB_RedYellowGreen, eRGBA_RedYellowGreen
    };

    void SetPredefinedColorMap(PredefinedColorMap predefinedColorMap);
    void SetMinAlpha(float minAlpha);

protected:

    CRGBAColor* m_pLUT;
    unsigned int m_lutSize;

    float m_lutScaleFactor;
    unsigned int m_lutQuantisationLevels;

    float m_minAlpha;

    struct CColorKey
    {
        CColorKey(float position, const CRGBAColor& color) :
            m_position(position), m_color(color)
        {}
        float m_position;
        CRGBAColor m_color;
    };

    static bool compareColorKey(const CColorKey& lhs, const CColorKey& rhs)
    {
        return lhs.m_position < rhs.m_position;
    }

    std::list<CColorKey> m_colorKeys;

};

