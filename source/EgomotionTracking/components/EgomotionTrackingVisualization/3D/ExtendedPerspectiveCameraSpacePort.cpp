#include "ExtendedPerspectiveCameraSpacePort.h"

CExtendedPerspectiveCameraSpacePort::CExtendedPerspectiveCameraSpacePort(QWidget* pSoQtWidget, const bool Antialiasing, const uint TotalPasses)
{
    m_pWidget = pSoQtWidget;
    m_pExaminerViewer = new SoQtExaminerViewer(m_pWidget);
    /* for Opengl */


    m_pRoot = new SoExtSelection();
    m_pRoot->ref();
    m_pCamera = new EVP::Visualization::Geometry::_3D::OpenInventor::Extensions::CExtendedPerspectiveCamera();
    m_pRoot->addChild(m_pCamera);
    m_pStaticNodes = new SoSwitch;
    m_pRoot->addChild(m_pStaticNodes);
    m_pDynamicNodes = new SoSwitch;
    m_pRoot->addChild(m_pDynamicNodes);
    m_pStaticNodes->whichChild = SO_SWITCH_ALL;
    m_pDynamicNodes->whichChild = SO_SWITCH_ALL;
    m_pBoxHighlightRenderAction = new SoBoxHighlightRenderAction;
    //XXX sonst fehler
    //m_pBoxHighlightRenderAction->setLineWidth(0.5f);
    m_pBoxHighlightRenderAction->setLineWidth(1.0f);
    m_pBoxHighlightRenderAction->setLinePattern(0xF0F0);
    m_pBoxHighlightRenderAction->setSmoothing(true);
    m_pExaminerViewer->setGLRenderAction(m_pBoxHighlightRenderAction);
    m_pExaminerViewer->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_SORTED_TRIANGLE_BLEND);

    m_pExaminerViewer->setSceneGraph(m_pRoot);
    m_pExaminerViewer->setDecoration(false);
    m_pExaminerViewer->setBackgroundColor(SbColor(0.5f, 0.5f, 0.5f));
    m_ViewportRegion = m_pExaminerViewer->getViewportRegion();

    m_pExaminerViewer->setCamera(m_pCamera);
    m_pRoot->policy = SoSelection::SHIFT;
    //m_pRoot->addSelectionCallback(CExtendedPerspectiveCameraSpacePort::SelectionCallBack, this);
    //m_pRoot->addDeselectionCallback(CExtendedPerspectiveCameraSpacePort::DeselectionCallBack, this);


    SetAntialiasing(Antialiasing, TotalPasses);
}

CExtendedPerspectiveCameraSpacePort::~CExtendedPerspectiveCameraSpacePort()
{
    delete m_pExaminerViewer;
    m_pRoot->unref();
}

void CExtendedPerspectiveCameraSpacePort::SetAntialiasing(const bool Antialiasing, const uint TotalPasses)
{
    m_pExaminerViewer->setAccumulationBuffer(Antialiasing);
    if (!Antialiasing)
    {
        m_pExaminerViewer->setAntialiasing(Antialiasing, 0);
    }

    else
    {
        m_pExaminerViewer->setAntialiasing(Antialiasing, TotalPasses);
    }
}

void CExtendedPerspectiveCameraSpacePort::ViewAll(const bool SetAsHome)
{
    m_pCamera->viewAll(m_pRoot, m_ViewportRegion);
    if (SetAsHome)
    {
        m_pExaminerViewer->saveHomePosition();
    }
}

void CExtendedPerspectiveCameraSpacePort::SetHome()
{
    m_pExaminerViewer->saveHomePosition();
}

void CExtendedPerspectiveCameraSpacePort::ViewSelection(SoNode* pNode)
{
    if (pNode)
    {
        m_pCamera->viewAll(pNode, m_ViewportRegion);
    }
}

void CExtendedPerspectiveCameraSpacePort::DeselectionCallBack(void* pUserData, SoPath* pPath)
{
    SoNode* pNode = pPath->getTail();
    if (pNode->getUserData())
    {
        ((CExtendedPerspectiveCameraSpacePort*) pUserData)->RemoveFromSelection(pNode);
    }
}

const SoQtExaminerViewer* CExtendedPerspectiveCameraSpacePort::GetExaminerViewer()
{
    return m_pExaminerViewer;
}

void CExtendedPerspectiveCameraSpacePort::SelectionCallBack(void* pUserData, SoPath* pPath)
{
    SoNode* pNode = pPath->getTail();
    if (pNode->getUserData())
    {
        ((CExtendedPerspectiveCameraSpacePort*) pUserData)->AddToSelection(pNode);
    }
}

void CExtendedPerspectiveCameraSpacePort::ClearSelection()
{
    m_pRoot->deselectAll();
    BroadcastClearSelection();
}

void CExtendedPerspectiveCameraSpacePort::ExternRemoveFromSelection(SoNode* /*pNode*/)
{
    /*
     *
    if (RemoveFromSelection(pNode))
        m_pRoot->deselect(pNode);
        */
}

void CExtendedPerspectiveCameraSpacePort::ExternAddToSelection(SoNode* /*pNode*/)
{
    /* don` t work with qt4 */

    /*
    if (AddToSelection(pNode))
        m_pRoot->select(pNode);
        */

}

bool CExtendedPerspectiveCameraSpacePort::RemoveFromSelection(SoNode* pNode)
{
    if (pNode && m_SelectedNodes.size())
    {
        std::list<SoNode*>::iterator EndSelectedNodes = m_SelectedNodes.end();
        for (std::list<SoNode*>::iterator ppSoNode = m_SelectedNodes.begin(); ppSoNode != EndSelectedNodes; ++ppSoNode)
            if ((*ppSoNode) == pNode)
            {
                pNode->touch();
                m_SelectedNodes.erase(ppSoNode);
                BroadcastRemoveFromSelection(pNode);
                return true;
            }
    }
    return false;
}

bool CExtendedPerspectiveCameraSpacePort::AddToSelection(SoNode* pNode)
{
    if (pNode)
    {
        std::list<SoNode*>::iterator EndSelectedNodes = m_SelectedNodes.end();
        for (std::list<SoNode*>::iterator ppSoNode = m_SelectedNodes.begin(); ppSoNode != EndSelectedNodes; ++ppSoNode)
            if ((*ppSoNode) == pNode)
            {
                return false;
            }
        pNode->touch();
        m_SelectedNodes.push_back(pNode);
        BroadcastAddToSelection(pNode);
    }
    return true;
}

const std::list<SoNode*>& CExtendedPerspectiveCameraSpacePort::GetSelectedNodes() const
{
    return m_SelectedNodes;
}

void CExtendedPerspectiveCameraSpacePort::AddNode(SoNode* pNode, bool Focus)
{
    AddStaticNode(pNode, Focus);
}

void CExtendedPerspectiveCameraSpacePort::AddStaticNode(SoNode* pNode, bool Focus)
{
    if (pNode)
    {
        m_pStaticNodes->addChild(pNode);
        if (Focus)
        {
            m_pCamera->viewAll(pNode, m_ViewportRegion);
        }
    }
}

void CExtendedPerspectiveCameraSpacePort::AddDynamicNode(SoNode* pNode, bool Focus)
{
    if (pNode)
    {
        m_pDynamicNodes->addChild(pNode);
        if (Focus)
        {
            m_pCamera->viewAll(pNode, m_ViewportRegion);
        }
    }
}

void CExtendedPerspectiveCameraSpacePort::RemoveDynamicNode(SoNode* pNode)
{
    if (pNode)
    {
        m_pDynamicNodes->removeChild(pNode);
    }
}

void CExtendedPerspectiveCameraSpacePort::RemoveStaticNode(SoNode* pNode)
{
    if (pNode)
    {
        m_pStaticNodes->removeChild(pNode);
    }
}

void CExtendedPerspectiveCameraSpacePort::ClearStaticNodes()
{
    m_pStaticNodes->removeAllChildren();
}

void CExtendedPerspectiveCameraSpacePort::ClearDynmaicNodes()
{
    m_pDynamicNodes->removeAllChildren();
}

void CExtendedPerspectiveCameraSpacePort::AddSelectionListener(ISpacePortSelectionListener* pSelectionListener)
{
    if (pSelectionListener)
    {
        std::list<ISpacePortSelectionListener*>::iterator End = m_SelectionListeners.end();
        for (std::list<ISpacePortSelectionListener*>::iterator ppSelectionListener = m_SelectionListeners.begin(); ppSelectionListener != End; ++ppSelectionListener)
            if ((*ppSelectionListener) == pSelectionListener)
            {
                return;
            }
        m_SelectionListeners.push_back(pSelectionListener);
    }
}

void CExtendedPerspectiveCameraSpacePort::BroadcastRemoveFromSelection(SoNode* pNode)
{
    if (m_SelectionListeners.size())
    {
        std::list<ISpacePortSelectionListener*>::iterator End = m_SelectionListeners.end();
        for (std::list<ISpacePortSelectionListener*>::iterator ppSelectionListener = m_SelectionListeners.begin(); ppSelectionListener != End; ++ppSelectionListener)
        {
            (*ppSelectionListener)->RemoveFromSelection(this, pNode);
        }
    }
}

void CExtendedPerspectiveCameraSpacePort::BroadcastAddToSelection(SoNode* pNode)
{
    if (m_SelectionListeners.size())
    {
        std::list<ISpacePortSelectionListener*>::iterator End = m_SelectionListeners.end();
        for (std::list<ISpacePortSelectionListener*>::iterator ppSelectionListener = m_SelectionListeners.begin(); ppSelectionListener != End; ++ppSelectionListener)
        {
            (*ppSelectionListener)->AddToSelection(this, pNode);
        }
    }
}

void CExtendedPerspectiveCameraSpacePort::BroadcastClearSelection()
{
    if (m_SelectionListeners.size())
    {
        std::list<ISpacePortSelectionListener*>::iterator End = m_SelectionListeners.end();
        for (std::list<ISpacePortSelectionListener*>::iterator ppSelectionListener = m_SelectionListeners.begin(); ppSelectionListener != End; ++ppSelectionListener)
        {
            (*ppSelectionListener)->ClearSelection(this);
        }
    }
}

SoExtSelection* CExtendedPerspectiveCameraSpacePort::GetRoot()
{
    return m_pRoot;
}

void CExtendedPerspectiveCameraSpacePort::GetCameraConfiguration(SbVec3f& CameraPosition, SbRotation& CameraOrientation) const
{
    CameraPosition = m_pCamera->position.getValue();
    CameraOrientation = m_pCamera->orientation.getValue();
}

void CExtendedPerspectiveCameraSpacePort::SetCameraConfiguration(const SbVec3f& CameraPosition, const SbRotation& CameraOrientation)
{
    m_pCamera->position.setValue(CameraPosition);
    m_pCamera->orientation.setValue(CameraOrientation);
}

void CExtendedPerspectiveCameraSpacePort::Render()
{
    m_pExaminerViewer->render();
}

bool CExtendedPerspectiveCameraSpacePort::LoadCalibration(
    const EVP::VisualSpace::Cameras::GeometricCalibration::CCameraGeometricCalibration* pCameraGeometricCalibration,
    const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Near, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Far)
{
    return m_pCamera->LoadCalibration(pCameraGeometricCalibration, Near, Far);

}

bool CExtendedPerspectiveCameraSpacePort::SetRenderingMode(
    const EVP::Visualization::Geometry::_3D::OpenInventor::Extensions::CExtendedPerspectiveCamera::RenderingMode Mode)
{
    return m_pCamera->SetRenderingMode(Mode);
}

const SbMatrix& CExtendedPerspectiveCameraSpacePort::GetGLProjectionMatrix()
{
    return m_pCamera->GetGLProjectionMatrix();
}

void CExtendedPerspectiveCameraSpacePort::SetGLProjectionMatrix(const SbMatrix& projectionMatrix)
{
    m_pCamera->SetGLProjectionMatrix(projectionMatrix);
}
