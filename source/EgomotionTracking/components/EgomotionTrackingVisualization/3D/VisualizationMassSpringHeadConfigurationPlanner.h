#pragma once


#include "VisualizationPrimitive.h"

#include <EgomotionTracking/components/EgomotionTracking/HeadConfigurationPlanner/MassSpringHeadConfigurationPlanner.h>

#include <EgomotionTracking/components/EgomotionTrackingVisualization/Misc/ColorMap/RGBAColorMap.h>

#include "VisualizationHeadConfiguration.h"
#include "VisualizationHeadConfigurationSample.h"
#include "VisualizationGroundGrid.h"
#include "VisualizationBoundingBox.h"
#include "VisualizationCoordinateSystem.h"

#include <vector>

#include <Inventor/nodes/SoRotation.h>

class CVisualizationMassSpringHeadConfigurationPlanner : public CVisualizationPrimitive
{
public:
    CVisualizationMassSpringHeadConfigurationPlanner();
    ~CVisualizationMassSpringHeadConfigurationPlanner() override;

    void Update(const bool /*FullUpdate*/) override
    {
        UpdateVisualization();
    }

    virtual void UpdateVisualization();

    void SetMassSpringHeadConfigurationPlanner(const CMassSpringHeadConfigurationPlanner* pMassSpringHeadConfigurationPlanner);

    void ReinitializeSamplingVisualizationElements();

    void SetVisualizeLayers(bool visualizeLayers);
    void SetVisualizeBoundingBox(bool visualizeBoundingBox);

protected:
    void CreateVisualization();
    const CMassSpringHeadConfigurationPlanner* m_pMassSpringHeadConfigurationPlanner;

    CVisualizationcoordinateSystem* m_pVisualizationCoordinateSystem;
    CVisualizationHeadConfiguration* m_pVisualizationTargetHeadConfiguration;
    CVisualizationHeadConfiguration* m_pVisualizationSamplesMeanHeadConfiguration;
    CVisualizationHeadConfiguration* m_pVisualizationCurrentHeadConfiguration;
    CVisualizationHeadConfiguration* m_pVisualizationSpringTargetHeadConfiguration;

    CVisualizationBoundingBox* m_pVisualizationBoundingBox;

    CVisualizationGroundGrid* m_pVisualizationRollPitchLayer;
    CVisualizationGroundGrid* m_pVisualizationRollYawLayer;
    CVisualizationGroundGrid* m_pVisualizationPitchYawLayer;
    SoRotation* m_pRollYawLayerRotation;
    SoRotation* m_pPitchYawLayerRotation;

    SoSeparator* m_pVisualizationGroundsSeparator;

    SoSeparator* m_pVisualizationHeadConfigurationSamplesSeparator;
    std::vector<CVisualizationHeadConfigurationSample*> m_visualizationHeadConfigurationSamples;

    CRGBAColorMap m_colorMap;
};

