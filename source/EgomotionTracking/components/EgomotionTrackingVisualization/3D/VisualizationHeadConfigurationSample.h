#pragma once

#include "VisualizationPrimitive.h"

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/HeadConfigurationSample.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/Misc/ColorMap/RGBAColorMap.h>

#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoMaterial.h>

class CVisualizationHeadConfigurationSample : public CVisualizationPrimitive
{
public:
    CVisualizationHeadConfigurationSample();
    ~CVisualizationHeadConfigurationSample() override;

    void Update(const bool /*FullUpdate*/) override
    {
        UpdateVisualization();
    }

    void UpdateVisualization();

    void SetHeadConfigurationSampe(const CHeadConfigurationSample* pHeadConfigurationSample);
    void SetColorMap(CRGBAColorMap* pColorMap);
protected:
    void CreateVisualization();

    const CHeadConfigurationSample* m_pHeadConfigurationSample;

    SoTranslation* m_pVisualizationTranslation;
    SoMaterial* m_pVisualizationMaterial;
    SoSphere* m_pVisualizationSphere;

    CRGBAColorMap* m_pColorMap;
};

