/*
 * VisualizationSimpleLocationRegion.cpp
 *
 *  Created on: 13.03.2013
 *      Author: abyte
 */

#include "VisualizationSimpleLocationRegion.h"

CVisualizationSimpleLocationRegion::CVisualizationSimpleLocationRegion() :
    CVisualizationPrimitive(CVisualizationPrimitive::eGeometricPrimitive)
{
    CreateVisualization();
}

CVisualizationSimpleLocationRegion::~CVisualizationSimpleLocationRegion()
{
}

void CVisualizationSimpleLocationRegion::UpdateVisualization()
{

    const float zOffset = 10.0;

    SbVec3f corners[5];
    corners[0] = SbVec3f(m_pLocationRegion->GetMinX(), m_pLocationRegion->GetMinY(), zOffset);
    corners[1] = SbVec3f(m_pLocationRegion->GetMaxX(), m_pLocationRegion->GetMinY(), zOffset);
    corners[2] = SbVec3f(m_pLocationRegion->GetMaxX(), m_pLocationRegion->GetMaxY(), zOffset);
    corners[3] = SbVec3f(m_pLocationRegion->GetMinX(), m_pLocationRegion->GetMaxY(), zOffset);
    corners[4] = corners[0];

    m_pRegionCoordinate3->point.setValues(0, 5, corners);
}

void CVisualizationSimpleLocationRegion::CreateVisualization()
{
    m_pRegionMaterial = new SoMaterial;
    m_pBaseSeparator->addChild(m_pRegionMaterial);

    m_pRegionCoordinate3 = new SoCoordinate3;
    m_pBaseSeparator->addChild(m_pRegionCoordinate3);

    m_pRegionLineSet = new SoLineSet;
    m_pBaseSeparator->addChild(m_pRegionLineSet);

    m_pRegionMaterial->diffuseColor.setValue(SbColor(0.8, 0.0, 0.0));
    m_pRegionLineSet->numVertices.set1Value(0, 5);
}
