#pragma once

#include "VisualizationPrimitive.h"
#include "VisualizationCamera.h"

#include <EgomotionTracking/components/EgomotionTracking/HeadConfigurationPlanner/MassSpringHeadConfigurationPlanner.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/Misc/ColorMap/RGBAColorMap.h>

#include <vector>

class CVisualizationMassSpringHeadConfigurationPlannerHeadConfigurationSamples : public CVisualizationPrimitive
{
public:
    CVisualizationMassSpringHeadConfigurationPlannerHeadConfigurationSamples();
    ~CVisualizationMassSpringHeadConfigurationPlannerHeadConfigurationSamples() override;

    void Update(const bool /*FullUpdate*/) override
    {
        UpdateVisualization();
    }
    virtual void UpdateVisualization();

    void SetMassSpringHeadConfigurationPlanner(const CMassSpringHeadConfigurationPlanner* pMassSpringHeadConfigurationPlanner);
    void ReinitializeSamplingVisualizationElements();
protected:
    void CreateVisualization();
    const CMassSpringHeadConfigurationPlanner* m_pMassSpringHeadConfigurationPlanner;

    CRGBAColorMap m_colorMap;
    SoSeparator* m_pVisualizationSamplesCamerasSeparator;
    std::vector<CVisualizationCamera*> m_visualizationSamplesCameras;
};

