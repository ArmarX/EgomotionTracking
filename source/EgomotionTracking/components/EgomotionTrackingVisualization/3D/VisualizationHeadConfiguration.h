#pragma once


#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/HeadConfiguration.h>

#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/SbColor.h>

#include "VisualizationPrimitive.h"

class CVisualizationHeadConfiguration : public CVisualizationPrimitive
{
public:
    CVisualizationHeadConfiguration();
    ~CVisualizationHeadConfiguration() override;

    void Update(const bool /*FullUpdate*/) override
    {
        UpdateVisualization();
    }

    void UpdateVisualization();

    void SetHeadConfiguration(const CHeadConfiguration* pHeadConfiguration);
    void SetColor(const SbColor& color) override;
    void SetRadius(float radius);
protected:
    void CreateVisualization();

    const CHeadConfiguration* m_pHeadConfiguration;

    SoTranslation* m_pVisualizationTranslation;
    SoMaterial* m_pVisualizationMaterial;
    SoSphere* m_pVisualizationSphere;
};

