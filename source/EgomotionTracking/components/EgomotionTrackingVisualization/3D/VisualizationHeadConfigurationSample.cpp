#include "VisualizationHeadConfigurationSample.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Conversions.h>

CVisualizationHeadConfigurationSample::CVisualizationHeadConfigurationSample() :
    CVisualizationPrimitive(CVisualizationPrimitive::eGeometricPrimitive),
    m_pHeadConfigurationSample(NULL), m_pColorMap(NULL)
{
    CreateVisualization();
}

CVisualizationHeadConfigurationSample::~CVisualizationHeadConfigurationSample()
{
}


void CVisualizationHeadConfigurationSample::SetHeadConfigurationSampe(const CHeadConfigurationSample* pHeadConfigurationSample)
{
    m_pHeadConfigurationSample = pHeadConfigurationSample;
}

void CVisualizationHeadConfigurationSample::SetColorMap(CRGBAColorMap* pColorMap)
{
    m_pColorMap = pColorMap;
}

void CVisualizationHeadConfigurationSample::CreateVisualization()
{
    m_pVisualizationTranslation = new SoTranslation;
    m_pVisualizationMaterial = new SoMaterial;
    m_pVisualizationSphere = new SoSphere;
    m_pBaseSeparator->addChild(m_pVisualizationTranslation);
    m_pBaseSeparator->addChild(m_pVisualizationMaterial);
    m_pBaseSeparator->addChild(m_pVisualizationSphere);

    m_pVisualizationSphere->radius.setValue(1.0);
    m_pVisualizationMaterial->diffuseColor.setValue(1.0, 0.0, 0.0);
}


void CVisualizationHeadConfigurationSample::UpdateVisualization()
{
    if (m_pHeadConfigurationSample != NULL)
    {
        m_pVisualizationTranslation->translation.setValue(Conversions::RadiansToDegree(m_pHeadConfigurationSample->GetNeckRoll()), Conversions::RadiansToDegree(m_pHeadConfigurationSample->GetNeckPitch()), Conversions::RadiansToDegree(m_pHeadConfigurationSample->GetNeckYaw()));

        if (m_pColorMap != NULL)
        {
            CRGBAColor color = m_pColorMap->GetColor(m_pHeadConfigurationSample->GetRelevanceFactor());

            m_pVisualizationMaterial->diffuseColor.setValue(color.GetByteRedChannel(), color.GetGreenChannel(), color.GetBlueChannel());
            m_pVisualizationMaterial->transparency.setValue(1.0 - color.GetAlphaChannel());
        }
    }
}
