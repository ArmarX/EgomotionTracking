#include "VisualizationHeadConfiguration.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Conversions.h>

CVisualizationHeadConfiguration::CVisualizationHeadConfiguration() :
    CVisualizationPrimitive(CVisualizationPrimitive::eGeometricPrimitive),
    m_pHeadConfiguration(NULL)
{
    CreateVisualization();
}

CVisualizationHeadConfiguration::~CVisualizationHeadConfiguration()
{
}

void CVisualizationHeadConfiguration::UpdateVisualization()
{
    if (m_pHeadConfiguration != NULL)
    {
        m_pVisualizationTranslation->translation.setValue(Conversions::RadiansToDegree(m_pHeadConfiguration->GetNeckRoll()), Conversions::RadiansToDegree(m_pHeadConfiguration->GetNeckPitch()), Conversions::RadiansToDegree(m_pHeadConfiguration->GetNeckYaw()));
    }
}

void CVisualizationHeadConfiguration::SetHeadConfiguration(const CHeadConfiguration* pHeadConfiguration)
{
    m_pHeadConfiguration = pHeadConfiguration;
}

void CVisualizationHeadConfiguration::SetColor(const SbColor& color)
{
    m_pVisualizationMaterial->diffuseColor.setValue(color);
}

void CVisualizationHeadConfiguration::SetRadius(float radius)
{
    m_pVisualizationSphere->radius.setValue(radius);
}

void CVisualizationHeadConfiguration::CreateVisualization()
{
    m_pVisualizationTranslation = new SoTranslation;
    m_pVisualizationMaterial = new SoMaterial;
    m_pVisualizationSphere = new SoSphere;
    m_pBaseSeparator->addChild(m_pVisualizationTranslation);
    m_pBaseSeparator->addChild(m_pVisualizationMaterial);
    m_pBaseSeparator->addChild(m_pVisualizationSphere);

    m_pVisualizationSphere->radius.setValue(1.0);
    m_pVisualizationMaterial->diffuseColor.setValue(1.0, 1.0, 1.0);
}
