#include "VisualizationMassSpringHeadConfigurationPlanner.h"

CVisualizationMassSpringHeadConfigurationPlanner::CVisualizationMassSpringHeadConfigurationPlanner() :
    CVisualizationPrimitive(CVisualizationPrimitive::eGeometricPrimitive),
    m_pMassSpringHeadConfigurationPlanner(NULL)
{
    m_colorMap.SetPredefinedColorMap(CRGBAColorMap::eRGBA_Hot);
    CreateVisualization();
}

CVisualizationMassSpringHeadConfigurationPlanner::~CVisualizationMassSpringHeadConfigurationPlanner()
{
    delete m_pVisualizationCoordinateSystem;
    delete m_pVisualizationTargetHeadConfiguration;
    delete m_pVisualizationSamplesMeanHeadConfiguration;
    delete m_pVisualizationCurrentHeadConfiguration;
    delete m_pVisualizationSpringTargetHeadConfiguration;
}


void CVisualizationMassSpringHeadConfigurationPlanner::SetMassSpringHeadConfigurationPlanner(const CMassSpringHeadConfigurationPlanner* pMassSpringHeadConfigurationPlanner)
{
    m_pMassSpringHeadConfigurationPlanner = pMassSpringHeadConfigurationPlanner;
    m_pVisualizationTargetHeadConfiguration->SetHeadConfiguration(&m_pMassSpringHeadConfigurationPlanner->GetTargetHeadConfiguration());
    m_pVisualizationSamplesMeanHeadConfiguration->SetHeadConfiguration(&m_pMassSpringHeadConfigurationPlanner->GetHeadConfigurationSamplesMean());
    m_pVisualizationCurrentHeadConfiguration->SetHeadConfiguration(&m_pMassSpringHeadConfigurationPlanner->GetCurrentPlannedHeadConfiguration());
    m_pVisualizationSpringTargetHeadConfiguration->SetHeadConfiguration(&m_pMassSpringHeadConfigurationPlanner->GetHeadConfigurationSpringTarget());
    ReinitializeSamplingVisualizationElements();
}

void CVisualizationMassSpringHeadConfigurationPlanner::ReinitializeSamplingVisualizationElements()
{
    if (m_visualizationHeadConfigurationSamples.size() > 0)
    {
        for (std::vector<CVisualizationHeadConfigurationSample*>::iterator visualizationSampleIterator = m_visualizationHeadConfigurationSamples.begin(); visualizationSampleIterator != m_visualizationHeadConfigurationSamples.end(); ++visualizationSampleIterator)
        {
            CVisualizationHeadConfigurationSample* curVisSample = *visualizationSampleIterator;
            delete curVisSample;
        }
    }
    m_pVisualizationHeadConfigurationSamplesSeparator->removeAllChildren();
    m_visualizationHeadConfigurationSamples.clear();

    if (m_pMassSpringHeadConfigurationPlanner != NULL)
    {
        float minRoll = 0.0, minPitch = 0.0, minYaw = 0.0, maxRoll = 0.0, maxPitch = 0.0, maxYaw = 0.0;

        const std::vector<CHeadConfigurationSample>& headConfigurationSamples = m_pMassSpringHeadConfigurationPlanner->GetHeadConfigurationSamples();
        if (headConfigurationSamples.size() > 0)
        {
            for (std::vector<CHeadConfigurationSample>::const_iterator sampleIterator = headConfigurationSamples.begin(); sampleIterator != headConfigurationSamples.end(); ++sampleIterator)
            {
                CVisualizationHeadConfigurationSample* visualizationSample = new CVisualizationHeadConfigurationSample;
                visualizationSample->SetColorMap(&m_colorMap);
                visualizationSample->SetHeadConfigurationSampe(&(*sampleIterator));
                m_visualizationHeadConfigurationSamples.push_back(visualizationSample);
                m_pVisualizationHeadConfigurationSamplesSeparator->addChild(visualizationSample->GetBaseSwitch());

                if (minRoll > sampleIterator->GetNeckRoll())
                {
                    minRoll = sampleIterator->GetNeckRoll();
                }
                if (maxRoll < sampleIterator->GetNeckRoll())
                {
                    maxRoll = sampleIterator->GetNeckRoll();
                }

                if (minPitch > sampleIterator->GetNeckPitch())
                {
                    minPitch = sampleIterator->GetNeckPitch();
                }
                if (maxPitch < sampleIterator->GetNeckPitch())
                {
                    maxPitch = sampleIterator->GetNeckPitch();
                }

                if (minYaw > sampleIterator->GetNeckYaw())
                {
                    minYaw = sampleIterator->GetNeckYaw();
                }
                if (maxYaw < sampleIterator->GetNeckYaw())
                {
                    maxYaw = sampleIterator->GetNeckYaw();
                }


            }
        }

        minRoll -= Conversions::DegreeToRadians(2);
        minPitch -= Conversions::DegreeToRadians(2);
        minYaw -= Conversions::DegreeToRadians(2);
        maxRoll += Conversions::DegreeToRadians(2);
        maxPitch += Conversions::DegreeToRadians(2);
        maxYaw += Conversions::DegreeToRadians(2);


        const SbVec3f boundingsMin(Conversions::RadiansToDegree(minRoll), Conversions::RadiansToDegree(minPitch), Conversions::RadiansToDegree(minYaw));
        const SbVec3f boundingsMax(Conversions::RadiansToDegree(maxRoll), Conversions::RadiansToDegree(maxPitch), Conversions::RadiansToDegree(maxYaw));



        m_pVisualizationBoundingBox->SetBoundings(boundingsMin, boundingsMax);
        m_pVisualizationBoundingBox->SetColor(SbColor(1.0, 0.0, 0.0));
    }
}

void CVisualizationMassSpringHeadConfigurationPlanner::SetVisualizeLayers(bool visualizeLayers)
{
    if (visualizeLayers)
    {
        m_pVisualizationRollPitchLayer->GetBaseSwitch()->whichChild = SO_SWITCH_ALL;
        m_pVisualizationRollYawLayer->GetBaseSwitch()->whichChild = SO_SWITCH_ALL;
        m_pVisualizationPitchYawLayer->GetBaseSwitch()->whichChild = SO_SWITCH_ALL;
    }
    else
    {
        m_pVisualizationRollPitchLayer->GetBaseSwitch()->whichChild = SO_SWITCH_NONE;
        m_pVisualizationRollYawLayer->GetBaseSwitch()->whichChild = SO_SWITCH_NONE;
        m_pVisualizationPitchYawLayer->GetBaseSwitch()->whichChild = SO_SWITCH_NONE;
    }
}

void CVisualizationMassSpringHeadConfigurationPlanner::SetVisualizeBoundingBox(bool visualizeBoundingBox)
{
    if (visualizeBoundingBox)
    {
        m_pVisualizationBoundingBox->GetBaseSwitch()->whichChild = SO_SWITCH_ALL;
    }
    else
    {
        m_pVisualizationBoundingBox->GetBaseSwitch()->whichChild = SO_SWITCH_NONE;
    }
}

void CVisualizationMassSpringHeadConfigurationPlanner::CreateVisualization()
{
    m_pVisualizationCoordinateSystem = new CVisualizationcoordinateSystem;
    m_pBaseSeparator->addChild(m_pVisualizationCoordinateSystem->GetBaseSwitch());

    const float axesLength = 30.0;
    m_pVisualizationCoordinateSystem->SetAxesLength(axesLength);
    m_pVisualizationCoordinateSystem->SetConeLength(axesLength * 0.2);
    m_pVisualizationCoordinateSystem->SetConeWidth(axesLength / 15);
    m_pVisualizationCoordinateSystem->SetTransparency(0.0);
    m_pVisualizationCoordinateSystem->SetLabels("Roll", "Pitch", "Yaw");

    m_pVisualizationTargetHeadConfiguration = new CVisualizationHeadConfiguration;
    m_pVisualizationTargetHeadConfiguration->SetRadius(2.0);
    m_pVisualizationTargetHeadConfiguration->SetColor(SbColor(1.0, 0.0, 1.0));
    m_pBaseSeparator->addChild(m_pVisualizationTargetHeadConfiguration->GetBaseSwitch());

    m_pVisualizationSamplesMeanHeadConfiguration = new CVisualizationHeadConfiguration;
    m_pVisualizationSamplesMeanHeadConfiguration->SetRadius(2.0);
    m_pVisualizationSamplesMeanHeadConfiguration->SetColor(SbColor(0.0, 0.0, 1.0));
    m_pBaseSeparator->addChild(m_pVisualizationSamplesMeanHeadConfiguration->GetBaseSwitch());

    m_pVisualizationCurrentHeadConfiguration = new CVisualizationHeadConfiguration;
    m_pVisualizationCurrentHeadConfiguration->SetRadius(2.0);
    m_pVisualizationCurrentHeadConfiguration->SetColor(SbColor(1.0, 1.0, 1.0));
    m_pBaseSeparator->addChild(m_pVisualizationCurrentHeadConfiguration->GetBaseSwitch());

    m_pVisualizationSpringTargetHeadConfiguration = new CVisualizationHeadConfiguration;
    m_pVisualizationSpringTargetHeadConfiguration->SetRadius(2.0);
    m_pVisualizationSpringTargetHeadConfiguration->SetColor(SbColor(1.0, 1.0, 0.0));
    m_pBaseSeparator->addChild(m_pVisualizationSpringTargetHeadConfiguration->GetBaseSwitch());

    m_pVisualizationHeadConfigurationSamplesSeparator = new SoSeparator;
    m_pBaseSeparator->addChild(m_pVisualizationHeadConfigurationSamplesSeparator);

    m_pVisualizationBoundingBox = new CVisualizationBoundingBox();
    m_pBaseSeparator->addChild(m_pVisualizationBoundingBox->GetBaseSwitch());

    /* create ground layers */
    m_pVisualizationGroundsSeparator = new SoSeparator;
    m_pBaseSeparator->addChild(m_pVisualizationGroundsSeparator);
    m_pVisualizationRollPitchLayer = new CVisualizationGroundGrid(36, 180, 0.0f, EGOMOTIONTRACKING_RESSOURCE_DIRECTORY "/Floor.png", 0.65);
    m_pVisualizationGroundsSeparator->addChild(m_pVisualizationRollPitchLayer->GetBaseSwitch());

    m_pRollYawLayerRotation = new SoRotation;
    m_pRollYawLayerRotation->rotation.setValue(SbRotation(SbVec3f(1.0, 0.0, 0.0), -M_PI / 2));
    m_pVisualizationGroundsSeparator->addChild(m_pRollYawLayerRotation);
    m_pVisualizationRollYawLayer = new CVisualizationGroundGrid(36, 180, 0.0f, EGOMOTIONTRACKING_RESSOURCE_DIRECTORY "/Floor.png", 0.65);
    m_pVisualizationGroundsSeparator->addChild(m_pVisualizationRollYawLayer->GetBaseSwitch());
    m_pPitchYawLayerRotation = new SoRotation;
    m_pPitchYawLayerRotation->rotation.setValue(SbRotation(SbVec3f(0.0, 1.0, 0.0), M_PI / 2));
    m_pVisualizationGroundsSeparator->addChild(m_pPitchYawLayerRotation);
    m_pVisualizationPitchYawLayer = new CVisualizationGroundGrid(36, 180, 0.0f, EGOMOTIONTRACKING_RESSOURCE_DIRECTORY "/Floor.png", 0.65);
    m_pVisualizationGroundsSeparator->addChild(m_pVisualizationPitchYawLayer->GetBaseSwitch());

}

void CVisualizationMassSpringHeadConfigurationPlanner::UpdateVisualization()
{
    m_pVisualizationTargetHeadConfiguration->UpdateVisualization();
    m_pVisualizationSamplesMeanHeadConfiguration->UpdateVisualization();
    m_pVisualizationCurrentHeadConfiguration->UpdateVisualization();
    m_pVisualizationSpringTargetHeadConfiguration->UpdateVisualization();
    if (m_visualizationHeadConfigurationSamples.size() > 0)
    {
        for (std::vector<CVisualizationHeadConfigurationSample*>::iterator visualizationSampleIterator = m_visualizationHeadConfigurationSamples.begin(); visualizationSampleIterator != m_visualizationHeadConfigurationSamples.end(); ++visualizationSampleIterator)
        {
            (*visualizationSampleIterator)->UpdateVisualization();
        }
    }
}
