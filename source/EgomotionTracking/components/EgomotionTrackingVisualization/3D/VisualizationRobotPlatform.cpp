/*
 * VisualizationArmar.cpp
 */

#include "VisualizationRobotPlatform.h"

CVisualizationRobotPlatform::CVisualizationRobotPlatform() :
    CVisualizationPrimitive(CVisualizationPrimitive::eGeometricPrimitive)
{
    m_pSoTranslationCylinder = NULL;
    m_pSoTranslationText = NULL;
    m_pSoText = NULL;
    m_pSoCylinder = NULL;
    m_pSoRotationXYZ = NULL;
    m_pSoTranslationOffset = NULL;
    m_pSoMaterialCylinder = NULL;
    m_pSoMaterialText = NULL;
    m_pSoSeperatorCenter = NULL;
    m_pSoTranslationCone = NULL;
    m_pSoMaterialCone = NULL;
    m_pSoCone = NULL;
    m_pSoRotationXYZCone = NULL;
    m_pSoSphereCenter = NULL;
    m_pSoSeperatorSphere = NULL;
    m_pSoTranslationSphere = NULL;


    m_cylinderRadius = 100;
    m_cylinderHeight = 80;

    /* init colors */
    m_textColor = SbColor(1.0, 0, 0);
    m_cylinderColor = SbColor(0, 0, 1.0);
    m_coneColor = SbColor(0, 1.0, 0);
    m_cylinderTransparency = 0.7;
    m_coneTransparency = 0;

    /* distance offsets */
    m_offsetTextCylinder = 30; //3 cm3
    m_offsetCenterCylinder = 1000;
    m_offsetRadiusCylinder = 200;

    m_radiusCenterSphere = 50;

    CreateVisualization();
}

CVisualizationRobotPlatform::~CVisualizationRobotPlatform()
{

}

void CVisualizationRobotPlatform::CreateVisualization()
{

    SetColor(CVisualizationPrimitive::eRed);

    m_pSoTranslationCylinder = new SoTranslation;
    m_pBaseSeparator->addChild(m_pSoTranslationCylinder);

    m_pSoSeperatorSphere = new SoSeparator;
    m_pBaseSeparator->addChild(m_pSoSeperatorSphere);

    m_pSoTranslationOffset = new SoTranslation;
    m_pBaseSeparator->addChild(m_pSoTranslationOffset);
    m_pSoTranslationOffset->translation.setValue(0.0f, 0.0f, m_cylinderHeight / 2);

    m_pSoSeperatorCenter = new SoSeparator;
    m_pBaseSeparator->addChild(m_pSoSeperatorCenter);

    m_pSoRotationXYZ = new SoRotationXYZ;
    m_pBaseSeparator->addChild(m_pSoRotationXYZ);
    m_pSoRotationXYZ->axis = 0;
    m_pSoRotationXYZ->angle = M_PI / 2;

    m_pSoMaterialCylinder = new SoMaterial;
    m_pBaseSeparator->addChild(m_pSoMaterialCylinder);


    m_pSoCylinder = new SoCylinder;
    m_pBaseSeparator->addChild(m_pSoCylinder);

    m_pSoTranslationText = new SoTranslation;
    m_pBaseSeparator->addChild(m_pSoTranslationText);

    m_pSoMaterialText = new SoMaterial;
    m_pSoMaterialText->diffuseColor = m_textColor;
    m_pBaseSeparator->addChild(m_pSoMaterialText);

    m_pSoText = new SoText2;
    m_pBaseSeparator->addChild(m_pSoText);


    /* cone */
    m_pSoRotationXYZCone = new SoRotationXYZ;
    m_pSoSeperatorCenter->addChild(m_pSoRotationXYZCone);


    m_pVisualisationPlatformCoordinateSystem = new CVisualizationcoordinateSystem;
    m_pVisualisationPlatformCoordinateSystem->SetAxesLength(300.0);
    m_pVisualisationPlatformCoordinateSystem->SetConeLength(30.0);
    m_pVisualisationPlatformCoordinateSystem->SetConeWidth(20.0);
    m_pVisualisationPlatformCoordinateSystem->SetLabels("X_P", "Y_P", "Z_P");
    m_pVisualisationPlatformCoordinateSystem->SetTransparency(0.0);

    m_pSoSeperatorCenter->addChild(m_pVisualisationPlatformCoordinateSystem->GetBaseSwitch());

    m_pSoTranslationCone = new SoTranslation;
    m_pSoSeperatorCenter->addChild(m_pSoTranslationCone);

    m_pSoMaterialCone = new SoMaterial;
    m_pSoSeperatorCenter->addChild(m_pSoMaterialCone);


    m_pSoCone = new SoCone;
    m_pSoSeperatorCenter->addChild(m_pSoCone);

    /* center Sphere */
    m_pSoTranslationSphere = new SoTranslation;
    m_pSoSeperatorSphere->addChild(m_pSoTranslationSphere);
    m_pSoTranslationSphere->translation.setValue(0, 0, m_radiusCenterSphere / 2);

    m_pSoSphereCenter = new SoSphere;
    m_pSoSeperatorSphere->addChild(m_pSoSphereCenter);


    /* update cylinder size */
    m_pSoTranslationOffset->translation.setValue(0.0f, 0.0f, m_cylinderHeight / 2);
    m_pSoTranslationText->translation.setValue(0.0f, m_cylinderHeight / 2 + m_offsetTextCylinder, 0);

    m_pSoCylinder->radius = m_cylinderRadius;
    m_pSoCylinder->height = m_cylinderHeight;
    /* update Sphere size */
    m_pSoSphereCenter->radius = m_radiusCenterSphere;

    m_pSoCone->height = 3 * m_cylinderRadius / 2;
    m_pSoCone->bottomRadius = m_cylinderRadius / 4;
    m_pSoRotationXYZCone->axis = 2;
    m_pSoTranslationCone->translation.setValue(0, m_cylinderRadius, 0);

    UpdateVisualization();

    m_pSoCylinder->setUserData(this);
    m_pSoText->setUserData(this);
}

void CVisualizationRobotPlatform::UpdateVisualization()
{
    /* translate everything */
    m_pSoTranslationCylinder->translation.setValue(m_locationState.GetX(), m_locationState.GetY(), 0.0);


    /* update cone */
    m_pSoRotationXYZCone->angle = m_locationState.GetAlpha();

    m_pSoMaterialCone->diffuseColor = m_coneColor;
    m_pSoMaterialCone->transparency = m_coneTransparency;
    m_pSoMaterialCylinder->diffuseColor = m_cylinderColor;
    m_pSoMaterialCylinder->transparency = m_cylinderTransparency;

}

