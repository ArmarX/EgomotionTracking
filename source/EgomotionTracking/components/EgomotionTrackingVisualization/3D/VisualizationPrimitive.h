/*
 * VisualizationPrimitive.h
 *
 *  Created on: Feb 28, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once

#include <Inventor/SbColor.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoComplexity.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoSeparator.h>

class CVisualizationPrimitive
{
public:

    enum VisualizationTypeId
    {
        ecoordinateSystem, eVisualizationVolume, eGeometricPrimitive, eVolumeRendering, eIsoSurface
    };

    enum VisualizationPredefinedColor
    {
        eBlack, eGray1_4, eGray1_2, eGray3_4, eWhite, eRed, eRed3_4, eRed1_2, eRed1_4, eGreen, eGreen3_4, eGreen1_2, eGreen1_4, eBlue, eBlue3_4, eBlue1_2, eBlue1_4, eYellow, eYellow3_4, eYellow1_2, eYellow1_4, eCyan, eCyan3_4, eCyan1_2, eCyan1_4, eMagenta, eMagenta3_4, eMagenta1_2, eMagenta1_4
    };

    static SbColor GetPredefinedColor(CVisualizationPrimitive::VisualizationPredefinedColor Color);

    CVisualizationPrimitive(const VisualizationTypeId TypeId);
    virtual ~CVisualizationPrimitive();

    VisualizationTypeId GetVisualizationTypeId() const;

    SoSwitch* GetBaseSwitch() const;

    virtual void SetVisible(const bool Visible);
    virtual bool GetVisible() const;

    virtual void SetColor(const CVisualizationPrimitive::VisualizationPredefinedColor Color);
    virtual void SetColor(const float R, const float G, const float B);
    virtual void SetColor(const SbColor& Color);
    virtual const SbColor& GetColor() const;

    virtual void SetTransparency(const float Transparency);
    virtual float GetTransparency() const;

    virtual void SetDrawStyle(const SoDrawStyle::Style Style);
    virtual SoDrawStyle::Style GetDrawStyle() const;

    virtual void SetLineWidth(const float LineWidth);
    virtual float GetLineWidth() const;

    virtual void SetPointSize(const float PointSize);
    virtual float GetPointSize() const;

    virtual void SetLinePattern(const ushort Pattern);
    virtual ushort GetLinePattern() const;

    virtual void SetComplexityType(const SoComplexity::Type Type);
    virtual SoComplexity::Type GetComplexityType() const;

    virtual void SetComplexityValue(const float Complexity);
    virtual float GetComplexityValue() const;

    virtual void Update(const bool FullUpdate) = 0;

protected:

    void SwitchVisibility(const bool Visible);

    bool m_IsVisible;
    const VisualizationTypeId m_VisualizationTypeId;
    SoSwitch* m_pBaseSwitch;
    SoSeparator* m_pBaseSeparator;
    SoDrawStyle* m_pBaseDrawStyle;
    SoMaterial* m_pBaseMaterial;
    SoComplexity* m_pBaseComplexity;
};


