/*
 * VisualizationCamera.h
 *
 *  Created on: 12.03.2013
 *      Author: abyte
 */

#pragma once

#include "VisualizationPrimitive.h"
#include "VisualizationCoordinateSystem.h"

#include <Inventor/nodes/SoMatrixTransform.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/SbMatrix.h>
#include <Inventor/SbRotation.h>
#include <Inventor/SbVec3f.h>

class CVisualizationCamera : public CVisualizationPrimitive
{
public:
    CVisualizationCamera();
    ~CVisualizationCamera() override;

    void Update(bool FullUpdate = false) override;
    virtual void SetCameraPosition(const SbVec3f& position, const SbRotation& rotation);
    void SetTransparency(float Transparency) override;
    virtual void SetAxesLabels(const std::string& labelX, const std::string& labelY, const std::string& labelZ);

    void SetAxisLineColor(const SbColor& color);

protected:
    void CreateVisualization();

    SoMatrixTransform* m_pCameraViewingTransformation;
    CVisualizationcoordinateSystem* m_pVisualisationCameraCoordinateSystem;

    SbVec3f m_position;
    SbRotation m_rotation;
};

