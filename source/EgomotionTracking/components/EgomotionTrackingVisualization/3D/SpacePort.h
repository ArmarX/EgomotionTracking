#pragma once

//STD
#include <list>
#include <vector>
#include <iostream>

//QT
#include <qwidget.h>

//COIN
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/actions/SoBoxHighlightRenderAction.h>
#include <Inventor/nodes/SoExtSelection.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoCamera.h>
#include <Inventor/nodes/SoSwitch.h>

class CSpacePort
{
public:

    class ISpacePortSelectionListener
    {
    public:

        ISpacePortSelectionListener()
        {
        }
        ~ISpacePortSelectionListener()
        {
        }
        virtual void RemoveFromSelection(const CSpacePort* pSpacePort, SoNode* pNode) = 0;
        virtual void AddToSelection(const CSpacePort* pSpacePort, SoNode* pNode) = 0;
        virtual void ClearSelection(const CSpacePort* pSpacePort) = 0;
    };

    CSpacePort(QWidget* pSoQtWidget, const bool Antialiasing = true, const uint TotalPasses = 8);
    ~CSpacePort();

    void SetAntialiasing(const bool Antialiasing, const uint TotalPasses);
    void ViewAll(const bool SetAsHome = false);
    void SetHome();
    void ViewSelection(SoNode* pNode);

    void ClearSelection();
    void ExternRemoveFromSelection(SoNode* pNode);
    void ExternAddToSelection(SoNode* pNode);

    const std::list<SoNode*>& GetSelectedNodes() const;

    void AddStaticNode(SoNode* pNode, bool Focus = false);
    void AddDynamicNode(SoNode* pNode, bool Focus = false);
    void AddNode(SoNode* pNode, bool Focus = false);

    void RemoveDynamicNode(SoNode* pNode);
    void RemoveStaticNode(SoNode* pNode);

    void AddSelectionListener(ISpacePortSelectionListener* pSelectionListener);

    void ClearStaticNodes();
    void ClearDynmaicNodes();

    SoExtSelection* GetRoot();

    void GetCameraConfiguration(SbVec3f& CameraPosition, SbRotation& CameraOrientation) const;
    void SetCameraConfiguration(const SbVec3f& CameraPosition, const SbRotation& CameraOrientation);

    void Render();

    void SetBackgroundColor(const SbColor& backgroundColor);

    bool SaveImage(const std::string& filename);
    void SetResolution(int resolutionX, int resolutionY);
    void GetResolution(int& resolutionX, int& resolutionY) const;

protected:

    virtual bool RemoveFromSelection(SoNode* pNode);
    virtual bool AddToSelection(SoNode* pNode);

    void BroadcastRemoveFromSelection(SoNode* pNode);
    void BroadcastAddToSelection(SoNode* pNode);
    void BroadcastClearSelection();

    static void DeselectionCallBack(void* pUserData, SoPath* pPath);
    static void SelectionCallBack(void* pUserData, SoPath* pPath);

    QWidget* m_pWidget;
    SoQtExaminerViewer* m_pExaminerViewer;
    SoBoxHighlightRenderAction* m_pBoxHighlightRenderAction;
    SoExtSelection* m_pRoot;
    SoSwitch* m_pStaticNodes;
    SoSwitch* m_pDynamicNodes;
    SoCamera* m_pCamera;
    SbViewportRegion m_ViewportRegion;
    std::list<SoNode*> m_SelectedNodes;
    std::list<ISpacePortSelectionListener*> m_SelectionListeners;
    int m_resolutionX;
    int m_resolutionY;
};

