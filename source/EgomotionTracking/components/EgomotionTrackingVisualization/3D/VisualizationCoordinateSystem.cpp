/*
 * VisualizationCoordinateSystem.cpp
 *
 *  Created on: Mar 1, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "VisualizationCoordinateSystem.h"


CVisualizationcoordinateSystem::CVisualizationcoordinateSystem() :
    CVisualizationPrimitive(CVisualizationPrimitive::ecoordinateSystem)
{
    SoMaterialBinding* pAxesMaterialBinding = new SoMaterialBinding;
    m_pBaseSeparator->addChild(pAxesMaterialBinding);
    pAxesMaterialBinding->value = SoMaterialBinding::PER_PART;
    m_pAxesCoordinate3 = new SoCoordinate3;
    m_pBaseSeparator->addChild(m_pAxesCoordinate3);
    m_pAxesLineSet = new SoLineSet;
    m_pBaseSeparator->addChild(m_pAxesLineSet);
    SoMaterialBinding* pConesMaterialBinding = new SoMaterialBinding;
    m_pBaseSeparator->addChild(pConesMaterialBinding);
    for (int i = 0; i < 3; ++i)
    {
        SoSeparator* pAxisSeparator = new SoSeparator;
        m_pBaseSeparator->addChild(pAxisSeparator);
        m_Axes[i].m_pConeMaterial = new SoMaterial;
        pAxisSeparator->addChild(m_Axes[i].m_pConeMaterial);
        m_Axes[i].m_pConeTransform = new SoTransform;
        pAxisSeparator->addChild(m_Axes[i].m_pConeTransform);
        m_Axes[i].m_pCone = new SoCone;
        pAxisSeparator->addChild(m_Axes[i].m_pCone);
        m_Axes[i].m_pCone->parts = SoCone::SIDES;
        m_Axes[i].m_pLabelTranslation = new SoTranslation;
        pAxisSeparator->addChild(m_Axes[i].m_pLabelTranslation);
        m_Axes[i].m_pLabelMaterial = new SoMaterial;
        pAxisSeparator->addChild(m_Axes[i].m_pLabelMaterial);
        m_Axes[i].m_pLabelText = new SoText2;
        pAxisSeparator->addChild(m_Axes[i].m_pLabelText);
    }
    m_Axes[0].m_Label = "x";
    m_Axes[1].m_Label = "y";
    m_Axes[2].m_Label = "z";
    m_AxisLenght = 400;
    m_ConeLenght = 100;
    m_ConeWidth = 50;
    SetColors(CVisualizationPrimitive::eRed, CVisualizationPrimitive::eGreen, CVisualizationPrimitive::eBlue);
    SetTransparency(0.0);
    Update(true);
}

CVisualizationcoordinateSystem::~CVisualizationcoordinateSystem()
{
}

bool CVisualizationcoordinateSystem::SetColors(const CVisualizationPrimitive::VisualizationPredefinedColor AxisXColor, const CVisualizationPrimitive::VisualizationPredefinedColor AxisYColor, const CVisualizationPrimitive::VisualizationPredefinedColor AxisZColor)
{
    if ((AxisXColor == AxisYColor) || (AxisXColor == AxisZColor) || (AxisYColor == AxisZColor))
    {
        return false;
    }
    else
    {
        const SbColor ColorX = CVisualizationPrimitive::GetPredefinedColor(AxisXColor);
        const SbColor ColorY = CVisualizationPrimitive::GetPredefinedColor(AxisYColor);
        const SbColor ColorZ = CVisualizationPrimitive::GetPredefinedColor(AxisZColor);
        m_pBaseMaterial->diffuseColor.set1Value(0, ColorX);
        m_pBaseMaterial->diffuseColor.set1Value(1, ColorY);
        m_pBaseMaterial->diffuseColor.set1Value(2, ColorZ);
        m_Axes[0].m_pConeMaterial->diffuseColor = ColorX;
        m_Axes[1].m_pConeMaterial->diffuseColor = ColorY;
        m_Axes[2].m_pConeMaterial->diffuseColor = ColorZ;
        m_Axes[0].m_pLabelMaterial->diffuseColor = ColorX;
        m_Axes[1].m_pLabelMaterial->diffuseColor = ColorY;
        m_Axes[2].m_pLabelMaterial->diffuseColor = ColorZ;
        return true;
    }
}

void CVisualizationcoordinateSystem::GetColors(SbColor& AxisXColor, SbColor& AxisYColor, SbColor& AxisZColor)
{
    const SbColor* pColors = m_pBaseMaterial->diffuseColor.getValues(0);
    AxisXColor = pColors[0];
    AxisYColor = pColors[1];
    AxisZColor = pColors[2];
}

bool CVisualizationcoordinateSystem::SetGlobalLineColor(const SbColor& color)
{
    m_pBaseMaterial->diffuseColor.set1Value(0, color);
    m_pBaseMaterial->diffuseColor.set1Value(1, color);
    m_pBaseMaterial->diffuseColor.set1Value(2, color);
    return true;
}

void CVisualizationcoordinateSystem::SetTransparency(const float Transparency)
{
    m_Axes[0].m_pConeMaterial->transparency = Transparency;
    m_Axes[1].m_pConeMaterial->transparency = Transparency;
    m_Axes[2].m_pConeMaterial->transparency = Transparency;
}

void CVisualizationcoordinateSystem::SetAxesLength(const float Lenght)
{
    if (m_AxisLenght != Lenght)
    {
        m_AxisLenght = Lenght;
        Update(true);
    }
}

float CVisualizationcoordinateSystem::GetAxesLength()
{
    return m_AxisLenght;
}

void CVisualizationcoordinateSystem::SetConeLength(const float Lenght)
{
    if (m_ConeLenght != Lenght)
    {
        m_ConeLenght = Lenght;
        Update(true);
    }
}

float CVisualizationcoordinateSystem::GetConeLength()
{
    return m_ConeLenght;
}

void CVisualizationcoordinateSystem::SetConeWidth(const float Width)
{
    if (m_ConeWidth != Width)
    {
        m_ConeWidth = Width;
        Update(true);
    }
}

float CVisualizationcoordinateSystem::GetConeWidth()
{
    return m_ConeWidth;
}

void CVisualizationcoordinateSystem::Update(const bool FullUpdate)
{
    if (FullUpdate)
    {
        const float LabelOffset = m_ConeLenght * float(0.5);
        const float ConeTranslation = m_AxisLenght - LabelOffset;
        m_pAxesCoordinate3->point.set1Value(0, float(0.0), float(0.0), float(0.0));
        m_pAxesCoordinate3->point.set1Value(1, m_AxisLenght, float(0.0), float(0.0));
        m_pAxesCoordinate3->point.set1Value(2, float(0.0), float(0.0), float(0.0));
        m_pAxesCoordinate3->point.set1Value(3, float(0.0), m_AxisLenght, float(0.0));
        m_pAxesCoordinate3->point.set1Value(4, float(0.0), float(0.0), float(0.0));
        m_pAxesCoordinate3->point.set1Value(5, float(0.0), float(0.0), m_AxisLenght);
        m_pAxesLineSet->numVertices.set1Value(0, 2);
        m_pAxesLineSet->numVertices.set1Value(1, 2);
        m_pAxesLineSet->numVertices.set1Value(2, 2);
        const SbVec3f AxisY(float(0.0), float(1.0), float(0.0));
        const SbVec3f LabelTranslation(float(0.0), LabelOffset, float(0.0));
        for (int i = 0; i < 3; ++i)
        {
            SbVec3f Translation(float(0.0), float(0.0), float(0.0));
            SbVec3f Axis(float(0.0), float(0.0), float(0.0));
            Translation[i] = ConeTranslation;
            Axis[i] = float(1.0);
            m_Axes[i].m_pConeTransform->translation = Translation;
            m_Axes[i].m_pConeTransform->rotation = SbRotation(AxisY, Axis);
            m_Axes[i].m_pCone->height = m_ConeLenght;
            m_Axes[i].m_pCone->bottomRadius = m_ConeWidth;
            m_Axes[i].m_pLabelTranslation->translation = LabelTranslation;
            m_Axes[i].m_pLabelText->string = m_Axes[i].m_Label;
        }
    }
}

bool CVisualizationcoordinateSystem::SetLabels(const std::string& pLabelX, const std::string& pLabelY, const std::string& pLabelZ)
{
    m_Axes[0].m_pLabelText->string.setValue(pLabelX.c_str());
    m_Axes[1].m_pLabelText->string.setValue(pLabelY.c_str());
    m_Axes[2].m_pLabelText->string.setValue(pLabelZ.c_str());
    return true;
}
