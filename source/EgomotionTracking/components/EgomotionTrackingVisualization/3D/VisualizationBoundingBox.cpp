#include "VisualizationBoundingBox.h"

CVisualizationBoundingBox::CVisualizationBoundingBox() :
    CVisualizationPrimitive(CVisualizationPrimitive::eGeometricPrimitive)
{
    CreateVisualization();
}

CVisualizationBoundingBox::~CVisualizationBoundingBox()
{
}

void CVisualizationBoundingBox::UpdateVisualization()
{
}

void CVisualizationBoundingBox::SetBoundings(const SbVec3f boundingsMin, const SbVec3f boundingsMax)
{
    const SbVec3f p1(boundingsMin[0], boundingsMin[1], boundingsMin[2]);
    const SbVec3f p2(boundingsMax[0], boundingsMin[1], boundingsMin[2]);
    const SbVec3f p3(boundingsMax[0], boundingsMin[1], boundingsMax[2]);
    const SbVec3f p4(boundingsMin[0], boundingsMin[1], boundingsMax[2]);
    const SbVec3f p5(boundingsMin[0], boundingsMax[1], boundingsMin[2]);
    const SbVec3f p6(boundingsMax[0], boundingsMax[1], boundingsMin[2]);
    const SbVec3f p7(boundingsMax[0], boundingsMax[1], boundingsMax[2]);
    const SbVec3f p8(boundingsMin[0], boundingsMax[1], boundingsMax[2]);

    m_pVisualizationBoxCoordinate3->point.set1Value(0, p1);
    m_pVisualizationBoxCoordinate3->point.set1Value(1, p2);
    m_pVisualizationBoxCoordinate3->point.set1Value(2, p3);
    m_pVisualizationBoxCoordinate3->point.set1Value(3, p4);
    m_pVisualizationBoxCoordinate3->point.set1Value(4, p1);

    m_pVisualizationBoxCoordinate3->point.set1Value(5, p2);
    m_pVisualizationBoxCoordinate3->point.set1Value(6, p6);
    m_pVisualizationBoxCoordinate3->point.set1Value(7, p7);
    m_pVisualizationBoxCoordinate3->point.set1Value(8, p3);

    m_pVisualizationBoxCoordinate3->point.set1Value(9, p7);
    m_pVisualizationBoxCoordinate3->point.set1Value(10, p8);
    m_pVisualizationBoxCoordinate3->point.set1Value(11, p5);
    m_pVisualizationBoxCoordinate3->point.set1Value(12, p6);

    m_pVisualizationBoxCoordinate3->point.set1Value(13, p8);
    m_pVisualizationBoxCoordinate3->point.set1Value(14, p4);

    m_pVisualizationBoxCoordinate3->point.set1Value(15, p5);
    m_pVisualizationBoxCoordinate3->point.set1Value(16, p1);

    m_pVisualizationBoxLineSet->numVertices.set1Value(0, 5);
    m_pVisualizationBoxLineSet->numVertices.set1Value(1, 4);
    m_pVisualizationBoxLineSet->numVertices.set1Value(2, 4);
    m_pVisualizationBoxLineSet->numVertices.set1Value(3, 2);
    m_pVisualizationBoxLineSet->numVertices.set1Value(4, 2);
}

void CVisualizationBoundingBox::SetColor(const SbColor& color)
{
    m_pVisualizationBoxMaterial->diffuseColor.setValue(color);
}

void CVisualizationBoundingBox::CreateVisualization()
{
    m_pVisualizationBoxMaterial = new SoMaterial;
    m_pBaseSeparator->addChild(m_pVisualizationBoxMaterial);
    m_pVisualizationBoxCoordinate3 = new SoCoordinate3;
    m_pBaseSeparator->addChild(m_pVisualizationBoxCoordinate3);
    m_pVisualizationBoxLineSet = new SoLineSet;
    m_pBaseSeparator->addChild(m_pVisualizationBoxLineSet);
}
