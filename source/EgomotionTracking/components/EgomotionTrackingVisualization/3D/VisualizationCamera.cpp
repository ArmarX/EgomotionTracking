/*
 * VisualizationCamera.cpp
 *
 *  Created on: 12.03.2013
 *      Author: abyte
 */

#include "VisualizationCamera.h"

CVisualizationCamera::CVisualizationCamera() :
    CVisualizationPrimitive(CVisualizationPrimitive::eGeometricPrimitive)
{
    CreateVisualization();
}

CVisualizationCamera::~CVisualizationCamera()
{
}

void CVisualizationCamera::Update(bool /* constBool*/)
{
    SbMatrix transfMatrix;
    transfMatrix.setTransform(m_position, m_rotation, SbVec3f(1.0, 1.0, 1.0));
    m_pCameraViewingTransformation->matrix.setValue(transfMatrix);
}

void CVisualizationCamera::SetCameraPosition(const SbVec3f& position, const SbRotation& rotation)
{
    m_position = position;
    m_rotation = rotation;
}

void CVisualizationCamera::SetTransparency(float Transparency)
{
    m_pVisualisationCameraCoordinateSystem->CVisualizationPrimitive::SetTransparency(Transparency);
    m_pVisualisationCameraCoordinateSystem->SetTransparency(Transparency);
}

void CVisualizationCamera::SetAxesLabels(const std::string& labelX, const std::string& labelY, const std::string& labelZ)
{
    m_pVisualisationCameraCoordinateSystem->SetLabels(labelX, labelY, labelZ);
}

void CVisualizationCamera::SetAxisLineColor(const SbColor& color)
{
    m_pVisualisationCameraCoordinateSystem->SetGlobalLineColor(color);
}

void CVisualizationCamera::CreateVisualization()
{
    m_pCameraViewingTransformation = new SoMatrixTransform;
    m_pVisualisationCameraCoordinateSystem = new CVisualizationcoordinateSystem;
    m_pBaseSeparator->addChild(m_pCameraViewingTransformation);
    m_pBaseSeparator->addChild(m_pVisualisationCameraCoordinateSystem->GetBaseSwitch());

    m_pVisualisationCameraCoordinateSystem->SetAxesLength(150.0);
    m_pVisualisationCameraCoordinateSystem->SetConeLength(25.0);
    m_pVisualisationCameraCoordinateSystem->SetConeWidth(5.0);
    m_pVisualisationCameraCoordinateSystem->SetLabels("X_C", "Y_C", "Z_C");
    m_pVisualisationCameraCoordinateSystem->SetTransparency(0.0);
}
