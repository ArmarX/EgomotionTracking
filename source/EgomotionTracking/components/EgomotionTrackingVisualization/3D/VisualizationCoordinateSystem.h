/*
 * VisualizationcoordinateSystem.h
 *
 *  Created on: Mar 1, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#pragma once


//VISUALIZATION
#include "VisualizationPrimitive.h"
#include <string>

#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoText2.h>

class CVisualizationcoordinateSystem : public CVisualizationPrimitive
{
public:

    CVisualizationcoordinateSystem();
    ~CVisualizationcoordinateSystem() override;

    bool SetColors(const CVisualizationPrimitive::VisualizationPredefinedColor AxisXColor, const CVisualizationPrimitive::VisualizationPredefinedColor AxisYColor, const CVisualizationPrimitive::VisualizationPredefinedColor AxisZColor);
    void GetColors(SbColor& AxisXColor, SbColor& AxisYColor, SbColor& AxisZColor);

    bool SetGlobalLineColor(const SbColor& color);
    void SetTransparency(const float Transparency) override;

    void SetAxesLength(const float Lenght);
    float GetAxesLength();

    void SetConeLength(const float Lenght);
    float GetConeLength();

    void SetConeWidth(const float Width);
    float GetConeWidth();

    void Update(const bool FullUpdate) override;

    bool SetLabels(const std::string& pLabelX, const std::string& pLabelY, const std::string& pLabelZ);

protected:

    float m_AxisLenght;
    float m_ConeLenght;
    float m_ConeWidth;
    SoCoordinate3* m_pAxesCoordinate3;
    SoLineSet* m_pAxesLineSet;
    struct Axis
    {
        SbString m_Label;
        SoTransform* m_pConeTransform;
        SoMaterial* m_pConeMaterial;
        SoCone* m_pCone;
        SoTranslation* m_pLabelTranslation;
        SoMaterial* m_pLabelMaterial;
        SoText2* m_pLabelText;
    };
    Axis m_Axes[3];
};


