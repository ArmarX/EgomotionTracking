/*
 * VisualizationSimpleLocationRegion.h
 *
 *  Created on: 13.03.2013
 *      Author: abyte
 */

#pragma once

#include "VisualizationPrimitive.h"
#include <Inventor/nodes/SoNodes.h>

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/SimpleLocationRegion.h>

class CVisualizationSimpleLocationRegion : public CVisualizationPrimitive
{
public:
    CVisualizationSimpleLocationRegion();
    ~CVisualizationSimpleLocationRegion() override;

    void Update(const bool /*FullUpdate*/) override
    {
        UpdateVisualization();
    }

    void UpdateVisualization();

    void SetLocationRegion(const CSimpleLocationRegion* pLocationRegion)
    {
        m_pLocationRegion = pLocationRegion;
        UpdateVisualization();
    }

protected:
    void CreateVisualization();

protected:

    const CSimpleLocationRegion* m_pLocationRegion;

    SoMaterial* m_pRegionMaterial;
    SoCoordinate3* m_pRegionCoordinate3;
    SoLineSet* m_pRegionLineSet;

};

