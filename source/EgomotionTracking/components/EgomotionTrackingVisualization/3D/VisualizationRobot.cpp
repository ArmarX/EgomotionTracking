///*
// * VisualizationRobot.cpp
// *
// *  Created on: 16.03.2013
// *      Author: abyte
// */

//#include "VisualizationRobot.h"

//CVisualizationRobot::CVisualizationRobot(const CStereoCalibrationGL* pStereoCalibration) :
//    CVisualizationPrimitive(CVisualizationPrimitive::eGeometricPrimitive),
//  m_pStereoCalibration(pStereoCalibration)
//{
//  CreateVisualization();
//}

//CVisualizationRobot::~CVisualizationRobot() {
//}

//void CVisualizationRobot::SetRobotControl(const CRobotControlInterface* pRobotControl) {
//  m_pRobotControl = pRobotControl;
//}

//void CVisualizationRobot::SetLocationState(const CLocationState* locationState) {
//  m_pLocationState = locationState;
//}

//void CVisualizationRobot::UpdateVisualization() {
//  if(m_pRobotControl != NULL && m_pLocationState != NULL)
//  {
//      SbVec3f pos;
//      SbRotation rot;
//        m_pRobotControl->CalculateCameraPosition(*m_pLocationState, pos, rot);
//        m_pVisualizationCamera->SetCameraPosition(pos, rot);

//      m_pVisualizationPlatform->SetLocationState(*m_pLocationState);

//        m_pVirtualArmar3->SetPlatformConfiguration(m_pLocationState->GetX(), m_pLocationState->GetY(), m_pLocationState->GetAlpha() + M_PI/2);

//        CHeadConfiguration currentHeadConfiguration = m_pRobotControl->GetHeadConfiguration();

//        m_pVirtualArmar3->SetHeadConfiguration(currentHeadConfiguration);
//  }
//    m_pVisualizationCamera->Update(true);
//    m_pVisualizationPlatform->Update(true);
//}

//void CVisualizationRobot::CreateVisualization() {
//    m_pVisualizationCamera = new CVisualizationCamera;
//  m_pVisualizationPlatform = new CVisualizationRobotPlatform;
//    m_pBaseSeparator->addChild(m_pVisualizationCamera->GetBaseSwitch());
//  m_pBaseSeparator->addChild(m_pVisualizationPlatform->GetBaseSwitch());

//  m_pVisualizationPlatform->SetCylinderTransparency(0.4);
//  m_pVisualizationPlatform->SetCylinderRadius(100);

//  m_pVisualizationPlatform->SetRadiusCenterSphere(30);
//  m_pVisualizationPlatform->SetConeTransparency(0.0);
//    //XXX
//    m_pVisualizationPlatform->GetBaseSwitch()->whichChild = SO_SWITCH_NONE;

//    m_pVirtualArmar3SpaceFrame = new ExternOI::CSpaceFrame(0.0, 0.0, 0.0, 0.0, "", NULL);
//    m_pVirtualArmar3Cameras[0] = new ExternOI::CVirtualCamera(m_pStereoCalibration->GetStereoCalibration().GetLeftCalibration(), NULL);
//    m_pVirtualArmar3Cameras[1] = new ExternOI::CVirtualCamera(m_pStereoCalibration->GetStereoCalibration().GetRightCalibration(), NULL);
//    m_pVirtualArmar3 = new ExternOI::CVirtualArmarIIIa(m_pVirtualArmar3Cameras, m_pVirtualArmar3SpaceFrame);

//    m_pBaseSeparator->addChild(m_pVirtualArmar3->GetSwitch());
//}
