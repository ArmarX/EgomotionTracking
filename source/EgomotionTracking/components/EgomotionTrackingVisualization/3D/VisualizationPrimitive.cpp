/*
 * VisualizationPrimitive.cpp
 *
 *  Created on: Feb 28, 2011
 *      Author:  M.Sc. - Ing. David Israel Gonzalez-Aguirre
 */

#include "VisualizationPrimitive.h"



CVisualizationPrimitive::CVisualizationPrimitive(const VisualizationTypeId TypeId) :
    m_IsVisible(true), m_VisualizationTypeId(TypeId)
{
    m_pBaseSwitch = new SoSwitch;
    m_pBaseSwitch->ref();
    m_pBaseSeparator = new SoSeparator;
    m_pBaseSwitch->addChild(m_pBaseSeparator);
    m_pBaseDrawStyle = new SoDrawStyle;
    m_pBaseSeparator->addChild(m_pBaseDrawStyle);
    m_pBaseMaterial = new SoMaterial;
    m_pBaseSeparator->addChild(m_pBaseMaterial);
    m_pBaseComplexity = new SoComplexity;
    m_pBaseSeparator->addChild(m_pBaseComplexity);
    m_pBaseSwitch->whichChild = 0;
}

CVisualizationPrimitive::~CVisualizationPrimitive()
{
    m_pBaseSwitch->removeAllChildren();
    m_pBaseSwitch->unref();
}

CVisualizationPrimitive::VisualizationTypeId CVisualizationPrimitive::GetVisualizationTypeId() const
{
    return m_VisualizationTypeId;
}

SoSwitch* CVisualizationPrimitive::GetBaseSwitch() const
{
    return m_pBaseSwitch;
}

void CVisualizationPrimitive::SetVisible(const bool Visible)
{
    m_IsVisible = Visible;
    SwitchVisibility(Visible);
}

bool CVisualizationPrimitive::GetVisible() const
{
    return m_IsVisible;
}

void CVisualizationPrimitive::SetColor(const CVisualizationPrimitive::VisualizationPredefinedColor Color)
{
    m_pBaseMaterial->diffuseColor = CVisualizationPrimitive::GetPredefinedColor(Color);
}

void CVisualizationPrimitive::SetColor(const float R, const float G, const float B)
{
    m_pBaseMaterial->diffuseColor.setValue(R, G, B);
}

void CVisualizationPrimitive::SetColor(const SbColor& Color)
{
    m_pBaseMaterial->diffuseColor = Color;
}

const SbColor& CVisualizationPrimitive::GetColor() const
{
    return *m_pBaseMaterial->diffuseColor.getValues(0);
}

void CVisualizationPrimitive::SetTransparency(const float Transparency)
{
    m_pBaseMaterial->transparency = Transparency;
}

float CVisualizationPrimitive::GetTransparency() const
{
    return *m_pBaseMaterial->transparency.getValues(0);
}

void CVisualizationPrimitive::SetDrawStyle(const SoDrawStyle::Style Style)
{
    m_pBaseDrawStyle->style = Style;
}

SoDrawStyle::Style CVisualizationPrimitive::GetDrawStyle() const
{
    return (SoDrawStyle::Style) m_pBaseDrawStyle->style.getValue();
}

void CVisualizationPrimitive::SetLineWidth(const float LineWidth)
{
    m_pBaseDrawStyle->lineWidth = LineWidth;
}

float CVisualizationPrimitive::GetLineWidth() const
{
    return m_pBaseDrawStyle->lineWidth.getValue();
}

void CVisualizationPrimitive::SetPointSize(const float PointSize)
{
    m_pBaseDrawStyle->pointSize = PointSize;
}

float CVisualizationPrimitive::GetPointSize() const
{
    return m_pBaseDrawStyle->pointSize.getValue();
}

void CVisualizationPrimitive::SetLinePattern(const ushort Pattern)
{
    m_pBaseDrawStyle->linePattern = Pattern;
}

ushort CVisualizationPrimitive::GetLinePattern() const
{
    return m_pBaseDrawStyle->linePattern.getValue();
}

void CVisualizationPrimitive::SetComplexityType(const SoComplexity::Type Type)
{
    m_pBaseComplexity->type = Type;
}

SoComplexity::Type CVisualizationPrimitive::GetComplexityType() const
{
    return (SoComplexity::Type) m_pBaseComplexity->type.getValue();
}

void CVisualizationPrimitive::SetComplexityValue(const float Complexity)
{
    m_pBaseComplexity->value = Complexity;
}

float CVisualizationPrimitive::GetComplexityValue() const
{
    return m_pBaseComplexity->value.getValue();
}

SbColor CVisualizationPrimitive::GetPredefinedColor(CVisualizationPrimitive::VisualizationPredefinedColor Color)
{
    SbColor PredefinedColor;
    switch (Color)
    {
        case eBlack:
            return PredefinedColor.setValue(float(0.0), float(0.0), float(0.0));
            break;
        case eGray3_4:
            return PredefinedColor.setValue(float(0.75), float(0.75), float(0.75));
            break;
        case eGray1_2:
            return PredefinedColor.setValue(float(0.5), float(0.5), float(0.5));
            break;
        case eGray1_4:
            return PredefinedColor.setValue(float(0.25), float(0.25), float(0.25));
            break;
        case eWhite:
            return PredefinedColor.setValue(float(1.0), float(1.0), float(1.0));
            break;
        case eRed:
            return PredefinedColor.setValue(float(1.0), float(0.0), float(0.0));
            break;
        case eRed3_4:
            return PredefinedColor.setValue(float(0.75), float(0.0), float(0.0));
            break;
        case eRed1_2:
            return PredefinedColor.setValue(float(0.5), float(0.0), float(0.0));
            break;
        case eRed1_4:
            return PredefinedColor.setValue(float(0.25), float(0.0), float(0.0));
            break;
        case eGreen:
            return PredefinedColor.setValue(float(0.0), float(1.0), float(0.0));
            break;
        case eGreen3_4:
            return PredefinedColor.setValue(float(0.0), float(0.75), float(0.0));
            break;
        case eGreen1_2:
            return PredefinedColor.setValue(float(0.0), float(0.5), float(0.0));
            break;
        case eGreen1_4:
            return PredefinedColor.setValue(float(0.0), float(0.25), float(0.0));
            break;
        case eBlue:
            return PredefinedColor.setValue(float(0.0), float(0.0), float(1.0));
            break;
        case eBlue3_4:
            return PredefinedColor.setValue(float(0.0), float(0.0), float(0.75));
            break;
        case eBlue1_2:
            return PredefinedColor.setValue(float(0.0), float(0.0), float(0.5));
            break;
        case eBlue1_4:
            return PredefinedColor.setValue(float(0.0), float(0.0), float(0.25));
            break;
        case eYellow:
            return PredefinedColor.setValue(float(1.0), float(1.0), float(0.0));
            break;
        case eYellow3_4:
            return PredefinedColor.setValue(float(0.75), float(0.75), float(0.0));
            break;
        case eYellow1_2:
            return PredefinedColor.setValue(float(0.5), float(0.5), float(0.0));
            break;
        case eYellow1_4:
            return PredefinedColor.setValue(float(0.25), float(0.25), float(0.0));
            break;
        case eCyan:
            return PredefinedColor.setValue(float(0.0), float(1.0), float(1.0));
            break;
        case eCyan3_4:
            return PredefinedColor.setValue(float(0.0), float(0.75), float(0.75));
            break;
        case eCyan1_2:
            return PredefinedColor.setValue(float(0.0), float(0.5), float(0.5));
            break;
        case eCyan1_4:
            return PredefinedColor.setValue(float(0.0), float(0.25), float(0.25));
            break;
        case eMagenta:
            return PredefinedColor.setValue(float(1.0), float(0.0), float(1.0));
            break;
        case eMagenta3_4:
            return PredefinedColor.setValue(float(0.75), float(0.0), float(0.75));
            break;
        case eMagenta1_2:
            return PredefinedColor.setValue(float(0.5), float(0.0), float(0.5));
            break;
        case eMagenta1_4:
            return PredefinedColor.setValue(float(0.25), float(0.0), float(0.25));
            break;
    }
    return PredefinedColor;
}

void CVisualizationPrimitive::SwitchVisibility(const bool Visible)
{
    if (Visible)
    {
        if (m_pBaseSwitch->whichChild.getValue() == 0)
        {
            return;
        }
        m_pBaseSwitch->whichChild = 0;
    }
    else
    {
        if (m_pBaseSwitch->whichChild.getValue() == SO_SWITCH_NONE)
        {
            return;
        }
        m_pBaseSwitch->whichChild = SO_SWITCH_NONE;
    }
}

