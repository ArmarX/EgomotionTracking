#pragma once

#include "VisualizationPrimitive.h"

#include <Inventor/SbVec3f.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoMaterial.h>

class CVisualizationBoundingBox : public CVisualizationPrimitive
{
public:
    CVisualizationBoundingBox();
    ~CVisualizationBoundingBox() override;

    void Update(const bool /*FullUpdate*/) override
    {
        UpdateVisualization();
    }
    virtual void UpdateVisualization();

    void SetBoundings(const SbVec3f boundingsMin, const SbVec3f boundingsMax);

    void SetColor(const SbColor& color) override;
protected:
    void CreateVisualization();

    SoMaterial* m_pVisualizationBoxMaterial;
    SoCoordinate3* m_pVisualizationBoxCoordinate3;
    SoLineSet* m_pVisualizationBoxLineSet;
};

