#include "SpacePort.h"
#include <Inventor/SoOffscreenRenderer.h>
#include <Image/ByteImage.h>

CSpacePort::CSpacePort(QWidget* pSoQtWidget, const bool Antialiasing, const uint TotalPasses)
{
    m_pWidget = pSoQtWidget;
    m_pExaminerViewer = new SoQtExaminerViewer(m_pWidget);
    /* for Opengl */


    m_pRoot = new SoExtSelection();
    m_pRoot->ref();
    m_pStaticNodes = new SoSwitch;
    m_pRoot->addChild(m_pStaticNodes);
    m_pDynamicNodes = new SoSwitch;
    m_pRoot->addChild(m_pDynamicNodes);
    m_pStaticNodes->whichChild = SO_SWITCH_ALL;
    m_pDynamicNodes->whichChild = SO_SWITCH_ALL;
    m_pBoxHighlightRenderAction = new SoBoxHighlightRenderAction;
    //XXX sonst fehler
    //m_pBoxHighlightRenderAction->setLineWidth(0.5f);
    m_pBoxHighlightRenderAction->setLineWidth(1.0f);
    m_pBoxHighlightRenderAction->setLinePattern(0xF0F0);
    m_pBoxHighlightRenderAction->setSmoothing(true);
    m_pExaminerViewer->setGLRenderAction(m_pBoxHighlightRenderAction);
    m_pExaminerViewer->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_SORTED_TRIANGLE_BLEND);

    m_pExaminerViewer->setSceneGraph(m_pRoot);
    m_pExaminerViewer->setDecoration(false);
    m_pExaminerViewer->setBackgroundColor(SbColor(0.5f, 0.5f, 0.5f));
    m_ViewportRegion = m_pExaminerViewer->getViewportRegion();
    m_pCamera = m_pExaminerViewer->getCamera();
    m_pRoot->policy = SoSelection::SHIFT;

    m_resolutionX = 640;
    m_resolutionY = 480;
    //m_pRoot->addSelectionCallback(CSpacePort::SelectionCallBack, this);
    //m_pRoot->addDeselectionCallback(CSpacePort::DeselectionCallBack, this);

    SetAntialiasing(Antialiasing, TotalPasses);
}

CSpacePort::~CSpacePort()
{
    delete m_pExaminerViewer;
    m_pRoot->unref();
}

void CSpacePort::SetAntialiasing(const bool Antialiasing, const uint TotalPasses)
{
    m_pExaminerViewer->setAccumulationBuffer(Antialiasing);
    if (!Antialiasing)
    {
        m_pExaminerViewer->setAntialiasing(Antialiasing, 0);
    }

    else
    {
        m_pExaminerViewer->setAntialiasing(Antialiasing, TotalPasses);
    }
}

void CSpacePort::ViewAll(const bool SetAsHome)
{
    m_pCamera->viewAll(m_pRoot, m_ViewportRegion);
    if (SetAsHome)
    {
        m_pExaminerViewer->saveHomePosition();
    }
}

void CSpacePort::SetHome()
{
    m_pExaminerViewer->saveHomePosition();
}

void CSpacePort::ViewSelection(SoNode* pNode)
{
    if (pNode)
    {
        m_pCamera->viewAll(pNode, m_ViewportRegion);
    }
}

void CSpacePort::DeselectionCallBack(void* pUserData, SoPath* pPath)
{
    SoNode* pNode = pPath->getTail();
    if (pNode->getUserData())
    {
        ((CSpacePort*) pUserData)->RemoveFromSelection(pNode);
    }
}

void CSpacePort::SelectionCallBack(void* pUserData, SoPath* pPath)
{
    SoNode* pNode = pPath->getTail();
    if (pNode->getUserData())
    {
        ((CSpacePort*) pUserData)->AddToSelection(pNode);
    }
}

void CSpacePort::ClearSelection()
{
    m_pRoot->deselectAll();
    BroadcastClearSelection();
}

void CSpacePort::ExternRemoveFromSelection(SoNode* /*pNode*/)
{
    /*
     *
    if (RemoveFromSelection(pNode))
        m_pRoot->deselect(pNode);
        */
}

void CSpacePort::ExternAddToSelection(SoNode* /*pNode*/)
{
    /* don` t work with qt4 */

    /*
    if (AddToSelection(pNode))
        m_pRoot->select(pNode);
        */

}

bool CSpacePort::RemoveFromSelection(SoNode* pNode)
{
    if (pNode && m_SelectedNodes.size())
    {
        std::list<SoNode*>::iterator EndSelectedNodes = m_SelectedNodes.end();
        for (std::list<SoNode*>::iterator ppSoNode = m_SelectedNodes.begin(); ppSoNode != EndSelectedNodes; ++ppSoNode)
            if ((*ppSoNode) == pNode)
            {
                pNode->touch();
                m_SelectedNodes.erase(ppSoNode);
                BroadcastRemoveFromSelection(pNode);
                return true;
            }
    }
    return false;
}

bool CSpacePort::AddToSelection(SoNode* pNode)
{
    if (pNode)
    {
        std::list<SoNode*>::iterator EndSelectedNodes = m_SelectedNodes.end();
        for (std::list<SoNode*>::iterator ppSoNode = m_SelectedNodes.begin(); ppSoNode != EndSelectedNodes; ++ppSoNode)
            if ((*ppSoNode) == pNode)
            {
                return false;
            }
        pNode->touch();
        m_SelectedNodes.push_back(pNode);
        BroadcastAddToSelection(pNode);
    }
    return true;
}

const std::list<SoNode*>& CSpacePort::GetSelectedNodes() const
{
    return m_SelectedNodes;
}

void CSpacePort::AddNode(SoNode* pNode, bool Focus)
{
    AddStaticNode(pNode, Focus);
}

void CSpacePort::AddStaticNode(SoNode* pNode, bool Focus)
{
    if (pNode)
    {
        m_pStaticNodes->addChild(pNode);
        if (Focus)
        {
            m_pCamera->viewAll(pNode, m_ViewportRegion);
        }
    }
}

void CSpacePort::AddDynamicNode(SoNode* pNode, bool Focus)
{
    if (pNode)
    {
        m_pDynamicNodes->addChild(pNode);
        if (Focus)
        {
            m_pCamera->viewAll(pNode, m_ViewportRegion);
        }
    }
}

void CSpacePort::RemoveDynamicNode(SoNode* pNode)
{
    if (pNode)
    {
        m_pDynamicNodes->removeChild(pNode);
    }
}

void CSpacePort::RemoveStaticNode(SoNode* pNode)
{
    if (pNode)
    {
        m_pStaticNodes->removeChild(pNode);
    }
}

void CSpacePort::ClearStaticNodes()
{
    m_pStaticNodes->removeAllChildren();
}

void CSpacePort::ClearDynmaicNodes()
{
    m_pDynamicNodes->removeAllChildren();
}

void CSpacePort::AddSelectionListener(ISpacePortSelectionListener* pSelectionListener)
{
    if (pSelectionListener)
    {
        std::list<ISpacePortSelectionListener*>::iterator End = m_SelectionListeners.end();
        for (std::list<ISpacePortSelectionListener*>::iterator ppSelectionListener = m_SelectionListeners.begin(); ppSelectionListener != End; ++ppSelectionListener)
            if ((*ppSelectionListener) == pSelectionListener)
            {
                return;
            }
        m_SelectionListeners.push_back(pSelectionListener);
    }
}

void CSpacePort::BroadcastRemoveFromSelection(SoNode* pNode)
{
    if (m_SelectionListeners.size())
    {
        std::list<ISpacePortSelectionListener*>::iterator End = m_SelectionListeners.end();
        for (std::list<ISpacePortSelectionListener*>::iterator ppSelectionListener = m_SelectionListeners.begin(); ppSelectionListener != End; ++ppSelectionListener)
        {
            (*ppSelectionListener)->RemoveFromSelection(this, pNode);
        }
    }
}

void CSpacePort::BroadcastAddToSelection(SoNode* pNode)
{
    if (m_SelectionListeners.size())
    {
        std::list<ISpacePortSelectionListener*>::iterator End = m_SelectionListeners.end();
        for (std::list<ISpacePortSelectionListener*>::iterator ppSelectionListener = m_SelectionListeners.begin(); ppSelectionListener != End; ++ppSelectionListener)
        {
            (*ppSelectionListener)->AddToSelection(this, pNode);
        }
    }
}

void CSpacePort::BroadcastClearSelection()
{
    if (m_SelectionListeners.size())
    {
        std::list<ISpacePortSelectionListener*>::iterator End = m_SelectionListeners.end();
        for (std::list<ISpacePortSelectionListener*>::iterator ppSelectionListener = m_SelectionListeners.begin(); ppSelectionListener != End; ++ppSelectionListener)
        {
            (*ppSelectionListener)->ClearSelection(this);
        }
    }
}

SoExtSelection* CSpacePort::GetRoot()
{
    return m_pRoot;
}

void CSpacePort::GetCameraConfiguration(SbVec3f& CameraPosition, SbRotation& CameraOrientation) const
{
    CameraPosition = m_pCamera->position.getValue();
    CameraOrientation = m_pCamera->orientation.getValue();
}

void CSpacePort::SetCameraConfiguration(const SbVec3f& CameraPosition, const SbRotation& CameraOrientation)
{
    m_pCamera->position.setValue(CameraPosition);
    m_pCamera->orientation.setValue(CameraOrientation);
}

void CSpacePort::Render()
{
    m_pExaminerViewer->render();
}

void CSpacePort::SetBackgroundColor(const SbColor& backgroundColor)
{
    m_pExaminerViewer->setBackgroundColor(backgroundColor);
}

bool CSpacePort::SaveImage(const std::string& filename)
{
    m_ViewportRegion =  m_pExaminerViewer->getViewportRegion();
    SbViewportRegion ViewportRegion(m_resolutionX, m_resolutionY);

    SoOffscreenRenderer Renderer(ViewportRegion);

    Renderer.setBackgroundColor(m_pExaminerViewer->getBackgroundColor());

    Renderer.setGLRenderAction(m_pExaminerViewer->getGLRenderAction());
    if (Renderer.render(m_pExaminerViewer->getSceneManager()->getSceneGraph()))
    {
        short Width, Height, BytesPerline;
        ViewportRegion.getViewportSizePixels().getValue(Width, Height);
        BytesPerline = Width * 3;
        CByteImage Image(Width, Height, CByteImage::eRGB24);
        unsigned char* pSource = Renderer.getBuffer();
        unsigned char* pDestiny = Image.pixels + (Width * Height * 3) - BytesPerline;
        for (int y = 0; y < Height; ++y, pDestiny -= BytesPerline, pSource += BytesPerline)
        {
            memcpy(pDestiny, pSource, BytesPerline);
        }

        m_pExaminerViewer->setViewportRegion(m_ViewportRegion);
        return Image.SaveToFile(filename.c_str());
    }
    return false;
}

void CSpacePort::SetResolution(int resolutionX, int resolutionY)
{
    m_resolutionX = resolutionX;
    m_resolutionY = resolutionY;
}

void CSpacePort::GetResolution(int& resolutionX, int& resolutionY) const
{
    resolutionX = m_resolutionX;
    resolutionY = m_resolutionY;
}
