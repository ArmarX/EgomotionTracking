#pragma once

//STD
#include <list>
#include <vector>
#include <iostream>

//QT
#include <qwidget.h>

//COIN
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/actions/SoBoxHighlightRenderAction.h>
#include <Inventor/actions/SoLineHighlightRenderAction.h>

#include <Inventor/nodes/SoExtSelection.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoCamera.h>
#include <Inventor/nodes/SoSwitch.h>

#include <EgomotionTracking/components/EVP/Visualization/Geometry/3D/OpenInventor/Extensions/ExtendedPerspectiveCamera.h>
#include <Calibration/StereoCalibration.h>
#include <EgomotionTracking/components/EVP_ArmarX/VisualSpace/Cameras/GeometricCalibration/StereoCameraGeometricCalibrationIVT.h>


class CExtendedPerspectiveCameraSpacePort
{
public:

    class ISpacePortSelectionListener
    {
    public:

        ISpacePortSelectionListener()
        {
        }
        ~ISpacePortSelectionListener()
        {
        }
        virtual void RemoveFromSelection(const CExtendedPerspectiveCameraSpacePort* pSpacePort, SoNode* pNode) = 0;
        virtual void AddToSelection(const CExtendedPerspectiveCameraSpacePort* pSpacePort, SoNode* pNode) = 0;
        virtual void ClearSelection(const CExtendedPerspectiveCameraSpacePort* pSpacePort) = 0;
    };

    CExtendedPerspectiveCameraSpacePort(QWidget* pSoQtWidget, const bool Antialiasing = true, const uint TotalPasses = 8);
    virtual ~CExtendedPerspectiveCameraSpacePort();

    void SetAntialiasing(const bool Antialiasing, const uint TotalPasses);
    void ViewAll(const bool SetAsHome = false);
    void SetHome();
    void ViewSelection(SoNode* pNode);

    void ClearSelection();
    void ExternRemoveFromSelection(SoNode* pNode);
    void ExternAddToSelection(SoNode* pNode);

    const std::list<SoNode*>& GetSelectedNodes() const;

    void AddStaticNode(SoNode* pNode, bool Focus = false);
    void AddDynamicNode(SoNode* pNode, bool Focus = false);
    void AddNode(SoNode* pNode, bool Focus = false);

    void RemoveDynamicNode(SoNode* pNode);
    void RemoveStaticNode(SoNode* pNode);

    void AddSelectionListener(ISpacePortSelectionListener* pSelectionListener);

    void ClearStaticNodes();
    void ClearDynmaicNodes();

    SoExtSelection* GetRoot();

    void GetCameraConfiguration(SbVec3f& CameraPosition, SbRotation& CameraOrientation) const;
    void SetCameraConfiguration(const SbVec3f& CameraPosition, const SbRotation& CameraOrientation);

    void Render();

    bool LoadCalibration(const EVP::VisualSpace::Cameras::GeometricCalibration::CCameraGeometricCalibration* pCameraGeometricCalibration, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Near, const EVP::Visualization::Geometry::_3D::OpenInventor::vreal Far);
    bool SetRenderingMode(const EVP::Visualization::Geometry::_3D::OpenInventor::Extensions::CExtendedPerspectiveCamera::RenderingMode Mode);

    void SetViewing(bool enabled)
    {
        m_pExaminerViewer->setViewing(enabled);
    }

    void SetEventCallback(SoQtRenderAreaEventCB* func, void* user)
    {
        m_pExaminerViewer->setEventCallback(func, user);
    }

    const SoQtExaminerViewer* GetExaminerViewer();
    const SbMatrix& GetGLProjectionMatrix();
    void SetGLProjectionMatrix(const SbMatrix& projectionMatrix);

protected:

    virtual bool RemoveFromSelection(SoNode* pNode);
    virtual bool AddToSelection(SoNode* pNode);

    void BroadcastRemoveFromSelection(SoNode* pNode);
    void BroadcastAddToSelection(SoNode* pNode);
    void BroadcastClearSelection();

    static void DeselectionCallBack(void* pUserData, SoPath* pPath);
    static void SelectionCallBack(void* pUserData, SoPath* pPath);

    QWidget* m_pWidget;
    SoQtExaminerViewer* m_pExaminerViewer;
    SoBoxHighlightRenderAction* m_pBoxHighlightRenderAction;
    SoExtSelection* m_pRoot;
    SoSwitch* m_pStaticNodes;
    SoSwitch* m_pDynamicNodes;
    EVP::Visualization::Geometry::_3D::OpenInventor::Extensions::CExtendedPerspectiveCamera* m_pCamera;
    SbViewportRegion m_ViewportRegion;
    std::list<SoNode*> m_SelectedNodes;
    std::list<ISpacePortSelectionListener*> m_SelectionListeners;

};

