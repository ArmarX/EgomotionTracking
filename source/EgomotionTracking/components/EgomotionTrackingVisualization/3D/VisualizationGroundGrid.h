/*
 * VisualizationGroundGrid.h
 *
 *  Created on: 03.03.2012
 *      Author: vollert
 */

#pragma once

#include <Inventor/nodes/SoNormal.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoTexture2.h>
#include <Inventor/nodes/SoTextureCoordinate2.h>

#include "VisualizationPrimitive.h"
#include <string>

class CVisualizationGroundGrid : public CVisualizationPrimitive
{
public:
    CVisualizationGroundGrid(float Repetitions, float Length, float Offset, const std::string& filename, float Transparency);
    ~CVisualizationGroundGrid() override;
    void Update(const bool /*FullUpdate*/) override
    {
        UpdateVisualization();
    }
    void UpdateVisualization();

    void SetRepetitions(float Repetitions);
    void SetLength(float Length);
protected:
    virtual void CreateVisualization();

    float m_Repetitions;
    float m_Length;
    float m_Offset;
    std::string m_filename;
    float m_gridTransparency;

    SoMaterial* m_pGridMaterial;
    SoCoordinate3* m_pImagePlaneSoCoordinate3;
    SoNormal* m_pGridNormal;
    SoNormalBinding* m_pGridNormalBinding;
    SoFaceSet* m_pGridFaceSet;
    SoTextureCoordinate2* m_pGridTextureCoordinate2;
    SoTextureCoordinateBinding* m_pGridTextureCoordinateBinding;
    SoTexture2* m_pGridTexture2;
};

