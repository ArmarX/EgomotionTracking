
// * VisualizationRobot.h
// *
// *  Created on: 16.03.2013
// *      Author: abyte
// */

//#ifndef VISUALIZATIONROBOT_H_
//#define VISUALIZATIONROBOT_H_

//#include "VisualizationPrimitive.h"
//#include "VisualizationRobotPlatform.h"
//#include "VisualizationCamera.h"
//#include <EgomotionTracking/components/EgomotionTracking/RobotControl/RobotControlInterface.h>
//#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
//#include <EgomotionTracking/components/EgomotionTracking/Misc/CameraCalibration/StereoCalibrationGL.h>

//#include <EgomotionTracking/components/ExternLibs/Bk/VirtualArmarIIIa.h>


//class CVisualizationRobot : public CVisualizationPrimitive {
//    public:
//        CVisualizationRobot(const CStereoCalibrationGL* pStereoCalibration);
//        virtual ~CVisualizationRobot();

//        void SetRobotControl(const CRobotControlInterface* pRobotControl);
//        void SetLocationState(const CLocationState* locationState);

//        virtual void Update(const bool /*FullUpdate*/)
//        {
//            UpdateVisualization();
//        }

//        void UpdateVisualization();

//    protected:
//        void CreateVisualization();
//        const CStereoCalibrationGL* m_pStereoCalibration;

//        CVisualizationCamera* m_pVisualizationCamera;
//        CVisualizationRobotPlatform* m_pVisualizationPlatform;

//        ExternOI::CSpaceFrame* m_pVirtualArmar3SpaceFrame;
//        ExternOI::CVirtualCamera* m_pVirtualArmar3Cameras[2];
//        ExternOI::CVirtualArmarIIIa* m_pVirtualArmar3;


//        const CRobotControlInterface* m_pRobotControl;
//        const CLocationState* m_pLocationState;
//};

//#endif /* VISUALIZATIONROBOT_H_
