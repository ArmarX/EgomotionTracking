/*
 * VisualizationGroundGrid.cpp
 *
 *  Created on: 03.03.2012
 *      Author: vollert
 */

#include "VisualizationGroundGrid.h"

CVisualizationGroundGrid::CVisualizationGroundGrid(float Repetitions, float Length, float Offset, const std::string& filename, float Transparency) :
    CVisualizationPrimitive(CVisualizationPrimitive::eGeometricPrimitive),
    m_Repetitions(Repetitions), m_Length(Length), m_Offset(Offset), m_filename(filename), m_gridTransparency(Transparency)
{
    CreateVisualization();
    UpdateVisualization();
}

CVisualizationGroundGrid::~CVisualizationGroundGrid()
{
}

void CVisualizationGroundGrid::UpdateVisualization()
{
    const float X = m_Length / 2.0f;
    const float Y = m_Length / 2.0f;
    m_pImagePlaneSoCoordinate3->point.set1Value(0, SbVec3f(X, Y, m_Offset));
    m_pImagePlaneSoCoordinate3->point.set1Value(1, SbVec3f(-X, Y, m_Offset));
    m_pImagePlaneSoCoordinate3->point.set1Value(2, SbVec3f(-X, -Y, m_Offset));
    m_pImagePlaneSoCoordinate3->point.set1Value(3, SbVec3f(X, -Y, m_Offset));

    m_pGridTextureCoordinate2->point.set1Value(0, SbVec2f(m_Repetitions, 0));
    m_pGridTextureCoordinate2->point.set1Value(1, SbVec2f(0, 0));
    m_pGridTextureCoordinate2->point.set1Value(2, SbVec2f(0, m_Repetitions));
    m_pGridTextureCoordinate2->point.set1Value(3, SbVec2f(m_Repetitions, m_Repetitions));

}

void CVisualizationGroundGrid::SetRepetitions(float Repetitions)
{
    m_Repetitions = Repetitions;
}

void CVisualizationGroundGrid::SetLength(float Length)
{
    m_Length = Length;
}

void CVisualizationGroundGrid::CreateVisualization()
{
    SoSeparator* pGrid = m_pBaseSeparator;
    m_pGridMaterial = new SoMaterial;
    m_pGridMaterial->transparency = m_gridTransparency;
    m_pImagePlaneSoCoordinate3 = new SoCoordinate3;

    m_pGridNormal = new SoNormal;
    m_pGridNormal->vector.set1Value(0, SbVec3f(0.0f, 0.0f, -1.0f));
    m_pGridNormalBinding = new SoNormalBinding;
    m_pGridNormalBinding->value.setValue(SoNormalBinding::NONE);

    m_pGridFaceSet = new SoFaceSet;
    m_pGridFaceSet->numVertices.set1Value(0, 4);

    m_pGridTextureCoordinate2 = new SoTextureCoordinate2;

    m_pGridTextureCoordinateBinding = new SoTextureCoordinateBinding;
    m_pGridTextureCoordinateBinding->value.setValue(SoTextureCoordinateBinding::PER_VERTEX);
    m_pGridTexture2 = new SoTexture2;
    m_pGridTexture2->filename.setValue(m_filename.c_str());
    m_pGridTexture2->wrapS = m_pGridTexture2->wrapT = SoTexture2::REPEAT;

    pGrid->addChild(m_pGridMaterial);
    pGrid->addChild(m_pGridTextureCoordinate2);
    pGrid->addChild(m_pGridTextureCoordinateBinding);
    pGrid->addChild(m_pGridTexture2);
    pGrid->addChild(m_pImagePlaneSoCoordinate3);
    pGrid->addChild(m_pGridNormal);
    pGrid->addChild(m_pGridNormalBinding);
    pGrid->addChild(m_pGridFaceSet);
}
