#include "VisualizationMassSpringHeadConfigurationPlannerHeadConfigurationSamples.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>

CVisualizationMassSpringHeadConfigurationPlannerHeadConfigurationSamples::CVisualizationMassSpringHeadConfigurationPlannerHeadConfigurationSamples() :
    CVisualizationPrimitive(CVisualizationPrimitive::eGeometricPrimitive),
    m_pMassSpringHeadConfigurationPlanner(NULL)
{
    m_colorMap.SetPredefinedColorMap(CRGBAColorMap::eRGBA_Hot);
    CreateVisualization();
}

CVisualizationMassSpringHeadConfigurationPlannerHeadConfigurationSamples::~CVisualizationMassSpringHeadConfigurationPlannerHeadConfigurationSamples()
{
}

void CVisualizationMassSpringHeadConfigurationPlannerHeadConfigurationSamples::UpdateVisualization()
{
    if (m_pMassSpringHeadConfigurationPlanner != NULL && m_IsVisible)
    {
        const std::vector<CHeadConfigurationSample>& headConfigurationSamples = m_pMassSpringHeadConfigurationPlanner->GetHeadConfigurationSamples();

        if (headConfigurationSamples.size() != m_visualizationSamplesCameras.size())
        {
            COUT_WARNING << "headConfigurationSamples.size() != m_visualizationSamplesCameras.size()" << std::endl;
        }

        const CLocationEstimator* pLocationEstimator = m_pMassSpringHeadConfigurationPlanner->GetLocationEstimator();
        if (pLocationEstimator != NULL)
        {
            const CLocationState& currentLocation = pLocationEstimator->GetLocationEstimate();

            std::vector<CVisualizationCamera*>::iterator curVisSampleCameraIterator = m_visualizationSamplesCameras.begin();
            for (std::vector<CHeadConfigurationSample>::const_iterator curHeadConfigurationSampleIterator = headConfigurationSamples.begin(); curHeadConfigurationSampleIterator != headConfigurationSamples.end(); ++curHeadConfigurationSampleIterator, ++curVisSampleCameraIterator)
            {
                CVisualizationCamera* curVisualizationSampleCamera = *curVisSampleCameraIterator;
                const CHeadConfigurationSample& curHeadConfigurationSample = *curHeadConfigurationSampleIterator;

                SbRotation cameraOrientation;
                SbVec3f cameraPosition;

                if (m_pMassSpringHeadConfigurationPlanner->GetControllableRobot() != NULL)
                {
                    SbMatrix transformationPlatformToCamera;
                    m_pMassSpringHeadConfigurationPlanner->GetControllableRobot()->CalculateTransformationPlatformToCamera(curHeadConfigurationSample, m_pMassSpringHeadConfigurationPlanner->GetControllableRobot()->GetCameraId(), transformationPlatformToCamera);

                    Calculations::CalculateRobotCameraPosition(currentLocation, transformationPlatformToCamera, cameraPosition, cameraOrientation);
                }

                CRGBAColor color = m_colorMap.GetColor(curHeadConfigurationSample.GetRelevanceFactor());

                curVisualizationSampleCamera->SetTransparency(1.0 - color.GetAlphaChannel());
                curVisualizationSampleCamera->SetCameraPosition(cameraPosition, cameraOrientation);
                curVisualizationSampleCamera->SetAxisLineColor(SbColor(color.GetRedChannel(), color.GetGreenChannel(), color.GetBlueChannel()));
                curVisualizationSampleCamera->Update();
            }
        }
    }
}

void CVisualizationMassSpringHeadConfigurationPlannerHeadConfigurationSamples::SetMassSpringHeadConfigurationPlanner(const CMassSpringHeadConfigurationPlanner* pMassSpringHeadConfigurationPlanner)
{
    m_pMassSpringHeadConfigurationPlanner = pMassSpringHeadConfigurationPlanner;
    ReinitializeSamplingVisualizationElements();
}

void CVisualizationMassSpringHeadConfigurationPlannerHeadConfigurationSamples::ReinitializeSamplingVisualizationElements()
{
    if (m_visualizationSamplesCameras.size() > 0)
    {
        for (std::vector<CVisualizationCamera*>::iterator visualizationSampleCameraIterator = m_visualizationSamplesCameras.begin(); visualizationSampleCameraIterator != m_visualizationSamplesCameras.end(); ++visualizationSampleCameraIterator)
        {
            CVisualizationCamera* curVisSampleCamera = *visualizationSampleCameraIterator;
            delete curVisSampleCamera;
        }
    }
    m_pVisualizationSamplesCamerasSeparator->removeAllChildren();
    m_visualizationSamplesCameras.clear();

    if (m_pMassSpringHeadConfigurationPlanner != NULL)
    {
        const std::vector<CHeadConfigurationSample>& headConfigurationSamples = m_pMassSpringHeadConfigurationPlanner->GetHeadConfigurationSamples();
        for (unsigned int i = 0; i < headConfigurationSamples.size(); ++i)
        {
            CVisualizationCamera* pCurSampleVisualizationCamera = new CVisualizationCamera;
            pCurSampleVisualizationCamera->SetAxesLabels("", "", "");
            m_visualizationSamplesCameras.push_back(pCurSampleVisualizationCamera);
            m_pVisualizationSamplesCamerasSeparator->addChild(pCurSampleVisualizationCamera->GetBaseSwitch());
        }
    }
}

void CVisualizationMassSpringHeadConfigurationPlannerHeadConfigurationSamples::CreateVisualization()
{
    m_pVisualizationSamplesCamerasSeparator = new SoSeparator;
    m_pBaseSeparator->addChild(m_pVisualizationSamplesCamerasSeparator);
}
