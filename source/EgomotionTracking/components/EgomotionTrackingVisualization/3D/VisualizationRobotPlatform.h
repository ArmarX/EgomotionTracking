/*
 * VisualizationArmar.h
 */

#pragma once

#include "VisualizationPrimitive.h"
#include "VisualizationCoordinateSystem.h"

#include <string>

//COIN
#include <Inventor/SbLinear.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoRotationXYZ.h>
#include <Inventor/nodes/SoSphere.h>

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>

class CVisualizationRobotPlatform : public CVisualizationPrimitive
{
public:

    CVisualizationRobotPlatform();
    ~CVisualizationRobotPlatform() override;

    const CLocationState& GetLocationState() const
    {
        return m_locationState;
    }

    void SetLocationState(const CLocationState& locationState)
    {
        m_locationState = locationState;
        UpdateVisualization();
    }

    void Update(const bool /*FullUpdate*/) override
    {
        UpdateVisualization();
    }

    const SbColor& GetConeColor() const
    {
        return m_coneColor;
    }

    void SetConeColor(const SbColor& coneColor)
    {
        m_coneColor = coneColor;
    }

    float GetConeTransparency() const
    {
        return m_coneTransparency;
    }

    void SetConeTransparency(float coneTransparency)
    {
        m_coneTransparency = coneTransparency;
    }

    const SbColor& GetCylinderColor() const
    {
        return m_cylinderColor;
    }

    void SetCylinderColor(const SbColor& cylinderColor)
    {
        m_cylinderColor = cylinderColor;
    }

    float GetCylinderHeight() const
    {
        return m_cylinderHeight;
    }

    void SetCylinderHeight(float cylinderHeight)
    {
        m_cylinderHeight = cylinderHeight;
    }

    float GetCylinderRadius() const
    {
        return m_cylinderRadius;
    }

    void SetCylinderRadius(float cylinderRadius)
    {
        m_cylinderRadius = cylinderRadius;
    }

    float GetCylinderTransparency() const
    {
        return m_cylinderTransparency;
    }

    void SetCylinderTransparency(float cylinderTransparency)
    {
        m_cylinderTransparency = cylinderTransparency;
    }

    float GetRadiusCenterSphere() const
    {
        return m_radiusCenterSphere;
    }

    void SetRadiusCenterSphere(float radiusCenterSphere)
    {
        m_radiusCenterSphere = radiusCenterSphere;
    }

    const SbColor& GetTextColor() const
    {
        return m_textColor;
    }

    void SetTextColor(const SbColor& textColor)
    {
        m_textColor = textColor;
    }

    void SetLabel(const std::string& label)
    {
        m_pSoText->string.set(label.c_str());
    }

protected:

    virtual void CreateVisualization();
    void UpdateVisualization();

    CVisualizationcoordinateSystem* m_pVisualisationPlatformCoordinateSystem;

    /* Coin stuff */

    /* Cylinder */
    SoRotationXYZ* m_pSoRotationXYZ;
    SoTranslation* m_pSoTranslationOffset;
    SoTranslation* m_pSoTranslationCylinder;
    SoMaterial* m_pSoMaterialCylinder;
    SoCylinder* m_pSoCylinder;

    /* Cone */
    SoRotationXYZ* m_pSoRotationXYZCone;
    SoSeparator* m_pSoSeperatorCenter;
    SoTranslation* m_pSoTranslationCone;
    SoMaterial* m_pSoMaterialCone;
    SoCone* m_pSoCone;

    /* Center Sphere */
    SoSeparator* m_pSoSeperatorSphere;
    SoTranslation* m_pSoTranslationSphere;
    SoSphere* m_pSoSphereCenter;

    /* Text */
    SoTranslation* m_pSoTranslationText;
    SoMaterial* m_pSoMaterialText;
    SoText2* m_pSoText;

    /* Colors */
    SbColor m_cylinderColor;
    SbColor m_textColor;
    SbColor m_coneColor;

    float m_cylinderTransparency;
    float m_coneTransparency;

    /* Distance offsets */
    float m_offsetCenterCylinder;
    float m_offsetTextCylinder;
    float m_offsetRadiusCylinder;

    float m_radiusCenterSphere;

    float m_cylinderRadius;
    float m_cylinderHeight;

    CLocationState m_locationState;
};

