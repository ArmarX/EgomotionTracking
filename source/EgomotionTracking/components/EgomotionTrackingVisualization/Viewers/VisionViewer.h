#pragma once

#include <EgomotionTracking/components/EgomotionTrackingVisualization/2D/ImageVisualization/MultiViewPortInterface.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/2D/ImageVisualization/MdiAreaMultiViewPort.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/2D/ImageVisualization/TabbedMultiViewPort.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/2D/ImageVisualization/ViewPortImageInterface.h>

#include <QDialog>

class CVisionViewer : public QDialog
{
    Q_OBJECT
public:
    explicit CVisionViewer(QWidget* parent = 0);
    void AddViewToVisionMultiViewPort(const CViewPortImageInterface* pViewImage, const QString& viewName, bool minimized = false);
    void Update();

signals:

public slots:

protected:
    CMdiAreaMultiViewPort* m_pVisionMultiViewPortIVT;

};

