#pragma once

#include <EgomotionTracking/components/EgomotionTrackingVisualization/2D/TrackingVisualization/Tracking2DVisualizationWidget.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/2D/TrackingVisualization/Tracking2DVisualizationScene.h>

#include <QDialog>

class CParticlesViewer : public QDialog
{
public:
    Q_OBJECT
public:
    explicit CParticlesViewer(QWidget* parent = 0);

    CTracking2DVisualizationWidget* GetTracking2DVisualizationWidget();
    void SetTrackingVisualizationScene(CTracking2DVisualizationScene* pTrackingVisualizationScene);

    void Update();
signals:

public slots:

protected:
    CTracking2DVisualizationWidget* m_pTracking2DVisualizationWidget;

};

