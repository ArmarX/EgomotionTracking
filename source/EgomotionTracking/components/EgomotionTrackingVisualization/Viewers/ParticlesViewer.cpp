#include "ParticlesViewer.h"

CParticlesViewer::CParticlesViewer(QWidget* parent) :
    QDialog(parent)
{
    m_pTracking2DVisualizationWidget = new CTracking2DVisualizationWidget(this);
    layout()->addWidget(m_pTracking2DVisualizationWidget);
    layout()->setContentsMargins(0, 0, 0, 0);
}

CTracking2DVisualizationWidget* CParticlesViewer::GetTracking2DVisualizationWidget()
{
    return m_pTracking2DVisualizationWidget;
}

void CParticlesViewer::SetTrackingVisualizationScene(CTracking2DVisualizationScene* pTrackingVisualizationScene)
{
    m_pTracking2DVisualizationWidget->SetTrackingVisualizationScene(pTrackingVisualizationScene);
}

void CParticlesViewer::Update()
{
    m_pTracking2DVisualizationWidget->Update();
}
