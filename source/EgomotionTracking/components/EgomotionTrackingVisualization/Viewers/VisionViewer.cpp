//#include "VisionViewer.moc"
#include "VisionViewer.h"
#include <QLayout>
CVisionViewer::CVisionViewer(QWidget* parent) :
    QDialog(parent)
{
    m_pVisionMultiViewPortIVT = new CMdiAreaMultiViewPort(this);

    layout()->addWidget(m_pVisionMultiViewPortIVT);
    layout()->setContentsMargins(0, 0, 0, 0);
    //m_pVisionMultiViewPortIVTTop->SetAutoZoom(true);
    //m_pVisionMultiViewPortIVTBottom->SetAutoZoom(true);
}

void CVisionViewer::AddViewToVisionMultiViewPort(const CViewPortImageInterface* pViewImage, const QString& viewName, bool minimized)
{
    m_pVisionMultiViewPortIVT->AddView(pViewImage, viewName, minimized);
}


void CVisionViewer::Update()
{
    m_pVisionMultiViewPortIVT->Update();
}
