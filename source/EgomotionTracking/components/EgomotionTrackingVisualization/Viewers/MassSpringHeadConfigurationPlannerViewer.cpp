//#include "MassSpringHeadConfigurationPlannerViewer.moc"
#include "MassSpringHeadConfigurationPlannerViewer.h"

#include <iostream>

CMassSpringHeadConfigurationPlannerViewer::CMassSpringHeadConfigurationPlannerViewer(QWidget* pParent) :
    QDialog(pParent), m_pMassSpringHeadConfigurationPlanner(NULL)
{
    setLayout(new QGridLayout(this));
    m_pSpacePortFrame = new QFrame();
    m_pSpacePortFrame->setMinimumSize(640, 480);

    layout()->addWidget(m_pSpacePortFrame);
    layout()->setContentsMargins(0, 0, 0, 0);
    m_pSpacePort = new CSpacePort(m_pSpacePortFrame, false, 0);

    m_pSpacePort->SetBackgroundColor(SbColor(1.0, 1.0, 1.0));
    InitSpacePort();
}

CMassSpringHeadConfigurationPlannerViewer::~CMassSpringHeadConfigurationPlannerViewer()
{
    delete m_pVisualizationMassSpringHeadConfigurationPlanner;
}

void CMassSpringHeadConfigurationPlannerViewer::SetMassSpringHeadConfigurationPlanner(CMassSpringHeadConfigurationPlanner* pMassSpringHeadConfigurationPlanner)
{
    m_pMassSpringHeadConfigurationPlanner = pMassSpringHeadConfigurationPlanner;
    m_pVisualizationMassSpringHeadConfigurationPlanner->SetMassSpringHeadConfigurationPlanner(pMassSpringHeadConfigurationPlanner);
}

const CMassSpringHeadConfigurationPlanner* CMassSpringHeadConfigurationPlannerViewer::GetMassSpringHeadConfigurationPlanner() const
{
    return m_pMassSpringHeadConfigurationPlanner;
}

void CMassSpringHeadConfigurationPlannerViewer::Update()
{
    m_pVisualizationMassSpringHeadConfigurationPlanner->UpdateVisualization();
}

void CMassSpringHeadConfigurationPlannerViewer::ReinitializeSamplingVisualizationElements()
{
    m_pVisualizationMassSpringHeadConfigurationPlanner->ReinitializeSamplingVisualizationElements();
}

CVisualizationMassSpringHeadConfigurationPlanner& CMassSpringHeadConfigurationPlannerViewer::GetVisualizationMassSpringHeadConfigurationPlanner()
{
    return *m_pVisualizationMassSpringHeadConfigurationPlanner;
}

void CMassSpringHeadConfigurationPlannerViewer::SetRenderingMode(bool antialiasing, unsigned int totalPasses)
{
    m_pSpacePort->SetAntialiasing(antialiasing, totalPasses);
}

bool CMassSpringHeadConfigurationPlannerViewer::SaveToFile(const std::string& filename)
{
    return m_pSpacePort->SaveImage(filename);
}

void CMassSpringHeadConfigurationPlannerViewer::SetResolution(int resolutionX, int resolutionY)
{
    m_pSpacePort->SetResolution(resolutionX, resolutionY);
}

void CMassSpringHeadConfigurationPlannerViewer::InitSpacePort()
{
    //m_pSpacePort->SetBackgroundColor(SbColor(0.1, 0.1, 0.1));
    m_pSpacePort->SetBackgroundColor(SbColor(0.8, 0.8, 0.8));
    m_pVisualizationMassSpringHeadConfigurationPlanner = new CVisualizationMassSpringHeadConfigurationPlanner;

    m_pSpacePort->AddNode(m_pVisualizationMassSpringHeadConfigurationPlanner->GetBaseSwitch(), true);
}
