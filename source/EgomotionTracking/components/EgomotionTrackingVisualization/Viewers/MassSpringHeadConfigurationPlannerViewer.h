#pragma once

#include <EgomotionTracking/components/EgomotionTracking/HeadConfigurationPlanner/MassSpringHeadConfigurationPlanner.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/3D/SpacePort.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/3D/VisualizationMassSpringHeadConfigurationPlanner.h>
#include <QDialog>
#include <QFrame>
#include <QGridLayout>

class CMassSpringHeadConfigurationPlannerViewer : public QDialog
{
public:
    Q_OBJECT
public:
    CMassSpringHeadConfigurationPlannerViewer(QWidget* pParent = 0);
    ~CMassSpringHeadConfigurationPlannerViewer() override;
    void SetMassSpringHeadConfigurationPlanner(CMassSpringHeadConfigurationPlanner* pMassSpringHeadConfigurationPlanner);
    const CMassSpringHeadConfigurationPlanner* GetMassSpringHeadConfigurationPlanner() const;

    void Update();
    void ReinitializeSamplingVisualizationElements();

    CVisualizationMassSpringHeadConfigurationPlanner& GetVisualizationMassSpringHeadConfigurationPlanner();

    void SetRenderingMode(bool antialiasing, unsigned int totalPasses);
    bool SaveToFile(const std::string& filename);
    void SetResolution(int resolutionX, int resolutionY);
protected:
    void InitSpacePort();

    const CMassSpringHeadConfigurationPlanner* m_pMassSpringHeadConfigurationPlanner;
    QFrame* m_pSpacePortFrame;
    CSpacePort* m_pSpacePort;

    CVisualizationMassSpringHeadConfigurationPlanner* m_pVisualizationMassSpringHeadConfigurationPlanner;
};

