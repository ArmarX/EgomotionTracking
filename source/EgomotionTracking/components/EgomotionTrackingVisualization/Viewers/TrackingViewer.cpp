/*
 * TrackingViewer.cpp
 *
 *  Created on: 12.03.2013
 *      Author: abyte
 */

//#include "TrackingViewer.moc"
#include "TrackingViewer.h"
#include <iostream>

#include <EgomotionTracking/components/EgomotionTrackingConfiguration/TrackingConfiguration.h>

CTrackingViewer::CTrackingViewer(QDialog* /*parent */)
{
    setupUi(this);
    m_pTrackingSpacePort = new CSpacePort(m_p3DTrackingSpacePortFrame, configuration_spaceportAntialiasing, configuration_spaceportTotalPasses);

}

CTrackingViewer::~CTrackingViewer()
{
}

void CTrackingViewer::AddNodeToTrackingSpacePort(SoNode* pNode, bool focus)
{
    m_pTrackingSpacePort->AddDynamicNode(pNode, focus);
}

void CTrackingViewer::Update()
{
}

void CTrackingViewer::SetRenderingMode(bool antialiasing, unsigned int totalPasses)
{
    m_pTrackingSpacePort->SetAntialiasing(antialiasing, totalPasses);
}

bool CTrackingViewer::SaveToFile(const std::string& filename)
{
    return m_pTrackingSpacePort->SaveImage(filename);
}

void CTrackingViewer::SetResolution(int resolutionX, int resolutionY)
{
    m_pTrackingSpacePort->SetResolution(resolutionX, resolutionY);
}
