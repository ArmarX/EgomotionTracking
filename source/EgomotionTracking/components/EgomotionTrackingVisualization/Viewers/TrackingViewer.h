/*
 * TrackingViewer.h
 *
 *  Created on: 12.03.2013
 *      Author: abyte
 */

#pragma once
#include <QWidget>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/ui_TrackingViewerDLG.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/3D/SpacePort.h>

#include <Inventor/nodes/SoNodes.h>

#include <string>

class CTrackingViewer : public QDialog, private Ui::TrackingViewerDLG
{
public:
    Q_OBJECT
public:
    CTrackingViewer(QDialog* parent = 0);
    virtual ~CTrackingViewer();

    void AddNodeToTrackingSpacePort(SoNode* pNode, bool focus = false);

    void Update();

    void SetRenderingMode(bool antialiasing, unsigned int totalPasses);
    bool SaveToFile(const std::string& filename);
    void SetResolution(int resolutionX, int resolutionY);
protected:
    CSpacePort* m_pTrackingSpacePort;
};

