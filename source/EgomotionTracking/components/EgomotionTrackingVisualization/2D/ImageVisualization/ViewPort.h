/*
 * ViewPort.h
 *
 *  Created on: 14.07.2012
 *      Author: abyte
 */

#pragma once

#include <QWidget>
#include <QLabel>
#include <QDebug>
#include <QGridLayout>
#include <QResizeEvent>
#include <QScrollArea>
#include <QShowEvent>

#include <QCheckBox>

//#include <IVT/Image/ByteImage.h>
#include "ViewPortImageInterface.h"

class CViewPort: public QWidget
{
public:
    Q_OBJECT

public slots:
    void ShowContextMenu(const QPoint& pos);
    void SaveImageDialog();
    void ToggleAutoZoom();
    void Update();

public:
    CViewPort(QWidget* pParent, const CViewPortImageInterface* pViewImage);
    ~CViewPort() override;

    void SetAutoZoom(bool autoZoom);

    QSize sizeHint() const override;

    const CViewPortImageInterface* GetViewImage() const
    {
        return m_pViewImage;
    }

    void SetViewImage(const CViewPortImageInterface* pViewImage)
    {
        m_pViewImage = pViewImage;
    }

protected:
    void DisplayImage();
    void RenderImage();

    void resizeEvent(QResizeEvent* pResizeEvent) override;
    void showEvent(QShowEvent* pShowEvent) override;


    QLabel* m_pImageLabel;
    const CViewPortImageInterface* m_pViewImage;
    QImage m_currentImage;
    QGridLayout* m_pMainGridLayout;
    QGridLayout* m_pSecondaryGridLayout;
    QScrollArea* m_pScrollArea;

    bool m_isEnabledAutoZoom;
    bool m_updatedWithoutRendering;
};

