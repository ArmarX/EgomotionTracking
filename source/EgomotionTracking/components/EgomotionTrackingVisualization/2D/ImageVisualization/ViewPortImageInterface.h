/*
 * ViewPortImageInterface.h
 *
 *  Created on: 16.03.2013
 *      Author: abyte
 */

#pragma once

#include <QImage>

class CViewPortImageInterface
{
public:
    CViewPortImageInterface();
    virtual ~CViewPortImageInterface();

    virtual QImage GetViewportImage() const = 0;
    virtual bool SaveToFile(const QString& filename) const = 0;
    virtual QSize GetSize() const = 0;

    bool isVisible() const
    {
        return m_visible;
    }

    void SetVisible(bool visible)
    {
        m_visible = visible;
    }

protected:
    bool m_visible;

};

