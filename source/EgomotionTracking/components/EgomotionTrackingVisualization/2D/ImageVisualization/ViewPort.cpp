/*
 * ViewPortIVT.cpp
 *
 *  Created on: 14.07.2012
 *      Author: abyte
 */

#include <EgomotionTracking/components/EgomotionTracking/Helpers/ConsoleOutput.h>

//#include "ViewPort.moc"
#include "ViewPort.h"
#include <iostream>

#include <QMenu>
#include <QFileDialog>

CViewPort::CViewPort(QWidget* pParent, const CViewPortImageInterface* pViewImage)
    : QWidget(pParent), m_pViewImage(pViewImage), m_isEnabledAutoZoom(false), m_updatedWithoutRendering(false)
{
    m_pMainGridLayout = new QGridLayout(pParent);
    m_pMainGridLayout->addWidget(this, 0, 0);

    m_pSecondaryGridLayout = new QGridLayout(this);

    m_pImageLabel = new QLabel;


    m_pScrollArea = new QScrollArea;
    m_pScrollArea->setBackgroundRole(QPalette::Dark);
    m_pScrollArea->setWidget(m_pImageLabel);

    m_pScrollArea->setWidgetResizable(true);
    m_pSecondaryGridLayout->addWidget(m_pScrollArea, 0, 0);

    m_isEnabledAutoZoom = false;

    m_pSecondaryGridLayout->setContentsMargins(0, 0, 0, 0);
    m_pMainGridLayout->setContentsMargins(0, 0, 0, 0);

    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(ShowContextMenu(const QPoint&)));
}

CViewPort::~CViewPort()
{
    delete m_pImageLabel;
}


void CViewPort::Update()
{
    if (m_pViewImage->isVisible())
    {
        show();
    }
    else
    {
        hide();
    }

    if (isVisible())
    {
        RenderImage();
    }
    else
    {
        m_updatedWithoutRendering = true;
    }
}

void CViewPort::resizeEvent(QResizeEvent* /*pResizeEvent*/)
{
    if (m_isEnabledAutoZoom)
    {
        DisplayImage();
    }
}

void CViewPort::SetAutoZoom(bool autoZoom)
{
    m_isEnabledAutoZoom = autoZoom;
    DisplayImage();
}

void CViewPort::ShowContextMenu(const QPoint& pos)
{
    QPoint globalPos = this->mapToGlobal(pos);

    QMenu contextMenu;

    QAction UpdateImageAction("Update", this);
    connect(&UpdateImageAction, SIGNAL(triggered()), this, SLOT(Update()));
    contextMenu.addAction(&UpdateImageAction);

    QString toogleAutoZoomText("Enable Auto Zoom");
    if (m_isEnabledAutoZoom)
    {
        toogleAutoZoomText = QString("Disable Auto Zoom");
    }
    QAction toogleAutoZoomAction(toogleAutoZoomText, this);
    connect(&toogleAutoZoomAction, SIGNAL(triggered()), this, SLOT(ToggleAutoZoom()));
    contextMenu.addAction(&toogleAutoZoomAction);

    QAction saveImageAction("Save Image", this);
    connect(&saveImageAction, SIGNAL(triggered()), this, SLOT(SaveImageDialog()));
    contextMenu.addAction(&saveImageAction);

    contextMenu.exec(globalPos);
}

void CViewPort::SaveImageDialog()
{
    QString saveFilename = QFileDialog::getSaveFileName(this, "Save Image to File", NULL, "Image File (*.bmp)");

    if (!saveFilename.isNull())
    {
        if (m_pViewImage)
        {
            m_pViewImage->SaveToFile(saveFilename);
        }
    }
}

void CViewPort::ToggleAutoZoom()
{
    SetAutoZoom(!m_isEnabledAutoZoom);
}

void CViewPort::showEvent(QShowEvent* /*pShowEvent*/)
{
    if (m_updatedWithoutRendering)
    {
        RenderImage();
    }
}

QSize CViewPort::sizeHint() const
{
    return m_pViewImage->GetSize();
}

void CViewPort::RenderImage()
{
    m_updatedWithoutRendering = false;
    m_currentImage = m_pViewImage->GetViewportImage();
    DisplayImage();
}

void CViewPort::DisplayImage()
{
    if (!m_currentImage.isNull())
    {
        if (!m_isEnabledAutoZoom)
        {
            m_pImageLabel->setPixmap(QPixmap::fromImage(m_currentImage));
        }
        else
        {
            if (width() > 0 && height() > 0)
            {
                m_pImageLabel->setPixmap(QPixmap::fromImage(m_currentImage).scaled(width() - 10, height() - 10, Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }
        }
    }

}
