/*
 * MdiAreaMultiViewPort.h
 *
 *  Created on: 16.03.2013
 *      Author: abyte
 */

#pragma once

#include <QMdiArea>
#include <QGridLayout>
#include "MultiViewPortInterface.h"

class CMdiAreaMultiViewPort: public QMdiArea, public CMultiViewPortInteraface
{
public:
    Q_OBJECT
public slots:
    void ShowContextMenu(const QPoint& pos);
    void ToggleViewMode();
    void ToggleAutoZoom();


public:
    CMdiAreaMultiViewPort(QWidget* pParent);
    ~CMdiAreaMultiViewPort() override;

    void AddView(const CViewPortImageInterface* pViewImage, const QString& viewName) override
    {
        AddView(pViewImage, viewName, false);
    }
    void AddView(const CViewPortImageInterface* pViewImage, const QString& viewName, bool minimized);


protected:

    QGridLayout* m_pMainGridLayout;

};

