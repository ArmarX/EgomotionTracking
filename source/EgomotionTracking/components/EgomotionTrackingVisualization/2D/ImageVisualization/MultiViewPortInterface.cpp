/*
 * TripleViewPortIVT.cpp
 *
 *  Created on: 16.07.2012
 *      Author: abyte
 */

#include "MultiViewPortInterface.h"
#include <QLabel>

#include <iostream>

CMultiViewPortInteraface::CMultiViewPortInteraface()
    : m_viewPorts(), m_isEnabledAutoZoom(false)
{


}

CMultiViewPortInteraface::~CMultiViewPortInteraface()
{
    if (m_viewPorts.size() > 0)
    {
        for (std::list<CViewPort*>::iterator it = m_viewPorts.begin(); it != m_viewPorts.end(); ++it)
        {
            delete *it;
        }
    }
}

void CMultiViewPortInteraface::Update()
{
    if (m_viewPorts.size() > 0)
    {
        for (std::list<CViewPort*>::iterator it = m_viewPorts.begin(); it != m_viewPorts.end(); ++it)
        {
            (*it)->Update();
        }
    }
}

void CMultiViewPortInteraface::SetAutoZoom(bool autoZoom)
{
    m_isEnabledAutoZoom = autoZoom;

    if (m_viewPorts.size() > 0)
    {
        for (std::list<CViewPort*>::iterator it = m_viewPorts.begin(); it != m_viewPorts.end(); ++it)
        {
            (*it)->SetAutoZoom(autoZoom);
        }
    }
}

CViewPort* CMultiViewPortInteraface::createViewPort(const CViewPortImageInterface* pViewImage, const QString& viewName)
{
    CViewPort* newViewPort = new CViewPort(NULL, pViewImage);
    newViewPort->SetAutoZoom(m_isEnabledAutoZoom);
    newViewPort->setWindowTitle(viewName);
    m_viewPorts.push_back(newViewPort);
    return newViewPort;
}
