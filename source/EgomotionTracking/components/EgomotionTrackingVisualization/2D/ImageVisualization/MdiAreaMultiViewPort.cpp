/*
 * MdiAreaMultiViewPort.cpp
 *
 *  Created on: 16.03.2013
 *      Author: abyte
 */

//#include "MdiAreaMultiViewPort.moc"
#include "MdiAreaMultiViewPort.h"
#include <QMenu>
#include <QAction>
#include <iostream>
#include <QMdiSubWindow>
CMdiAreaMultiViewPort::CMdiAreaMultiViewPort(QWidget* pParent) :
    QMdiArea(pParent)
{
    m_pMainGridLayout = new QGridLayout(pParent);
    m_pMainGridLayout->addWidget(this, 0, 0);
    m_pMainGridLayout->setContentsMargins(0, 0, 0, 0);

    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(ShowContextMenu(const QPoint&)));

}

CMdiAreaMultiViewPort::~CMdiAreaMultiViewPort()
{
}

void CMdiAreaMultiViewPort::AddView(const CViewPortImageInterface* pViewImage, const QString& viewName, bool minimized)
{

    CViewPort* newViewPort = createViewPort(pViewImage, viewName);
    QMdiSubWindow* newSubWindow = addSubWindow(newViewPort, Qt::WindowTitleHint | Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint);

    newSubWindow->setContextMenuPolicy(Qt::NoContextMenu);
    if (minimized)
    {
        newSubWindow->setWindowState(Qt::WindowMinimized);
    }
    tileSubWindows();
}

void CMdiAreaMultiViewPort::ShowContextMenu(const QPoint& pos)
{

    QPoint globalPos = this->mapToGlobal(pos);

    QMenu contextMenu;
    /*
    QAction ToggleViewmodeAction("Toggle View Mode", this);
    connect(&ToggleViewmodeAction, SIGNAL(triggered()), this, SLOT(ToggleViewMode()));
    contextMenu.addAction(&ToggleViewmodeAction);
     */
    QAction cascadeSubWindowsAction("Cascade Sub Windows", this);
    connect(&cascadeSubWindowsAction, SIGNAL(triggered()), this, SLOT(cascadeSubWindows()));
    contextMenu.addAction(&cascadeSubWindowsAction);

    QAction tileWindowAction("Tile Sub Windows", this);
    connect(&tileWindowAction, SIGNAL(triggered()), this, SLOT(tileSubWindows()));
    contextMenu.addAction(&tileWindowAction);

    /*
    QAction minimizeAllAction("Minimize Sub Windows", this);
    connect(&minimizeAllAction, SIGNAL(triggered()), this, SLOT(MinimizeAllSubWindows()));
    contextMenu.addAction(&minimizeAllAction);

    QAction maximizeAllAction("Maximize Sub Windows", this);
    connect(&maximizeAllAction, SIGNAL(triggered()), this, SLOT(MaximizeAllSubWindows()));
    contextMenu.addAction(&maximizeAllAction);

    */

    QString toogleAutoZoomText("Enable Auto Zoom");
    if (m_isEnabledAutoZoom)
    {
        toogleAutoZoomText = QString("Disable Auto Zoom");
    }
    QAction toogleAutoZoomAction(toogleAutoZoomText, this);
    connect(&toogleAutoZoomAction, SIGNAL(triggered()), this, SLOT(ToggleAutoZoom()));
    contextMenu.addAction(&toogleAutoZoomAction);


    contextMenu.exec(globalPos);

}

void CMdiAreaMultiViewPort::ToggleAutoZoom()
{
    SetAutoZoom(!m_isEnabledAutoZoom);
}

void CMdiAreaMultiViewPort::ToggleViewMode()
{
    if (viewMode() == QMdiArea::SubWindowView)
    {
        setViewMode(QMdiArea::TabbedView);
    }
    else
    {
        setViewMode(QMdiArea::SubWindowView);
    }
}

