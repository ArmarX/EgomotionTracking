/*
 * MultiViewPortIVT.h
 *
 *  Created on: 16.07.2012
 *      Author: abyte
 */

#pragma once


#include <list>
#include "ViewPort.h"


class CMultiViewPortInteraface
{
public:
    CMultiViewPortInteraface();
    virtual ~CMultiViewPortInteraface();

    void Update();
    virtual void AddView(const CViewPortImageInterface* pViewImage, const QString& viewName) = 0;
    void SetAutoZoom(bool autoZoom);
protected:
    CViewPort* createViewPort(const CViewPortImageInterface* pViewImage, const QString& viewName);

    std::list<CViewPort*> m_viewPorts;
    bool m_isEnabledAutoZoom;
};

