/*
 * TabbedMultiViewPort.cpp
 *
 *  Created on: 16.03.2013
 *      Author: abyte
 */

#include "TabbedMultiViewPort.h"

CTabbedMultiViewPort::CTabbedMultiViewPort(QWidget* pParent)
    : QWidget(pParent)
{
    m_pMainGridLayout = new QGridLayout(pParent);
    m_pTabWidget = new QTabWidget;
    m_pMainGridLayout->addWidget(this, 0, 0);

    m_pSecondaryGridLayout = new QGridLayout(this);
    m_pSecondaryGridLayout->addWidget(m_pTabWidget, 0, 0);

    m_pSecondaryGridLayout->setContentsMargins(0, 0, 0, 0);
    m_pMainGridLayout->setContentsMargins(0, 0, 0, 0);
}

CTabbedMultiViewPort::~CTabbedMultiViewPort()
{
    // TODO Auto-generated destructor stub
}

void CTabbedMultiViewPort::AddView(const CViewPortImageInterface* pViewImage, const QString& viewName)
{
    CViewPort* newViewPort = createViewPort(pViewImage, viewName);
    m_pTabWidget->addTab(newViewPort, viewName);
}
