/*
 * TabbedMultiViewPort.h
 *
 *  Created on: 16.03.2013
 *      Author: abyte
 */

#pragma once

#include <QWidget>
#include <QGridLayout>
#include <QTabWidget>
#include "MultiViewPortInterface.h"

class CTabbedMultiViewPort : public QWidget, public CMultiViewPortInteraface
{
public:
    CTabbedMultiViewPort(QWidget* pParent);
    ~CTabbedMultiViewPort() override;
    void AddView(const CViewPortImageInterface* pViewImage, const QString& viewName) override;

protected:

    QGridLayout* m_pMainGridLayout;
    QGridLayout* m_pSecondaryGridLayout;
    QTabWidget* m_pTabWidget;
};


