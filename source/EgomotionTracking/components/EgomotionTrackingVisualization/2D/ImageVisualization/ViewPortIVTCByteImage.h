/*
 * ViewPortIVTCByteImage.h
 *
 *  Created on: 16.03.2013
 *      Author: abyte
 */

#pragma once

#include "ViewPortImageInterface.h"
#include <Image/ByteImage.h>

class CViewPortIVTCByteImage: public CViewPortImageInterface
{
public:
    CViewPortIVTCByteImage();
    CViewPortIVTCByteImage(unsigned int width, unsigned int height, CByteImage::ImageType type, bool bHeaderOnly);

    ~CViewPortIVTCByteImage() override;
    static QImage CByteImage2QImage(const CByteImage* pMatImage);

    QImage GetViewportImage() const override;
    bool SaveToFile(const QString& filename) const override;
    void SetCByteImageReference(const CByteImage& image);
    QSize GetSize() const override;

    CByteImage& GetCByteImage()
    {
        return m_byteImage;
    }

    operator CByteImage()
    {
        return m_byteImage;
    }
    operator CByteImage* ()
    {
        return &m_byteImage;
    }

protected:
    CByteImage m_byteImage;
};

