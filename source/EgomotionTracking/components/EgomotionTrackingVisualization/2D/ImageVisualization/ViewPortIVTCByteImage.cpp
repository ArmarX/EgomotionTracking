/*
 * ViewPortIVTCByteImage.cpp
 *
 *  Created on: 16.03.2013
 *      Author: abyte
 */

#include "ViewPortIVTCByteImage.h"
#include <QDebug>

CViewPortIVTCByteImage::CViewPortIVTCByteImage()
{

}
CViewPortIVTCByteImage::CViewPortIVTCByteImage(unsigned int width, unsigned int height, CByteImage::ImageType type, bool bHeaderOnly) :
    m_byteImage(width, height, type, bHeaderOnly)
{}

CViewPortIVTCByteImage::~CViewPortIVTCByteImage()
{
}


QImage CViewPortIVTCByteImage::CByteImage2QImage(const CByteImage* pMatImage)
{

    int height = pMatImage->height;
    int width = pMatImage->width;


    if (pMatImage->type == CByteImage::eRGB24)
    {
        QImage img(pMatImage->pixels, width, height, QImage::Format_RGB888);
        return img;
    }
    else if (pMatImage->type == CByteImage::eGrayScale)
    {
        QImage img(pMatImage->pixels, width, height, QImage::Format_Indexed8);

        QVector<QRgb> colorTable;
        for (int i = 0; i < 256; i++)
        {
            colorTable.push_back(qRgb(i, i, i));
        }
        img.setColorTable(colorTable);
        return img;
    }
    else
    {
        qWarning() << "Image cannot be converted.";
        return QImage();
    }
}

QImage CViewPortIVTCByteImage::GetViewportImage() const
{
    return CByteImage2QImage(&m_byteImage);
}

bool CViewPortIVTCByteImage::SaveToFile(const QString& filename) const
{
    return m_byteImage.SaveToFile(filename.toAscii().data());
}

void CViewPortIVTCByteImage::SetCByteImageReference(const CByteImage& image)
{
    if (m_byteImage.m_bOwnMemory)
    {
        delete[] m_byteImage.pixels;
    }

    m_byteImage.Set(image.width, image.height, image.type, true);
    m_byteImage.pixels = image.pixels;
}

QSize CViewPortIVTCByteImage::GetSize() const
{
    return QSize(m_byteImage.width, m_byteImage.height);
}
