#include "LocationControlScene.h"

CLocationControlScene::CLocationControlScene()
{
    m_pGraphicsScene = new QGraphicsScene;

    m_pRootCoordinateSystemGraphicsItem = new CCoordinateSystemGraphicsItem;
    m_pRootCoordinateSystemGraphicsItem->SetLabelX("X");
    m_pRootCoordinateSystemGraphicsItem->SetLabelY("Y");
    m_pRootCoordinateSystemGraphicsItem->SetAxesLineWidth(20);
    m_pRootCoordinateSystemGraphicsItem->SetLabelFont(QFont("Arial", 46));
    m_pRootCoordinateSystemGraphicsItem->setZValue(1);
    m_pGraphicsScene->addItem(m_pRootCoordinateSystemGraphicsItem);

    m_pScaleRulerGraphicsItem = new CScaleRulerGraphicsItem;
    m_pScaleRulerGraphicsItem->SetLabelFont(QFont("Arial", 66));
    m_pScaleRulerGraphicsItem->SetDrawGrid(true);
    m_pScaleRulerGraphicsItem->setZValue(-2);
    m_pGraphicsScene->addItem(m_pScaleRulerGraphicsItem);

    m_pEstimatedRobotLocationGraphicsItem = new CRobotLocationGraphicsItem;
    m_pEstimatedRobotLocationGraphicsItem->setZValue(4);
    m_pEstimatedRobotLocationGraphicsItem->SetTriangleBackgroundColor(Qt::darkGreen);
    m_pEstimatedRobotLocationGraphicsItem->SetCircleBackgroundColor(Qt::green);
    m_pGraphicsScene->addItem(m_pEstimatedRobotLocationGraphicsItem);

    m_pTargetRobotLocationGraphicsItem = new CRobotLocationGraphicsItem;
    m_pTargetRobotLocationGraphicsItem->setZValue(3);
    m_pTargetRobotLocationGraphicsItem->SetTriangleBackgroundColor(Qt::darkRed);
    m_pTargetRobotLocationGraphicsItem->SetCircleBackgroundColor(Qt::red);
    m_pGraphicsScene->addItem(m_pTargetRobotLocationGraphicsItem);
}

CLocationControlScene::~CLocationControlScene()
{
    CModelObjectGraphicsItem* currentModelObjectGI;
    foreach (currentModelObjectGI, m_modelObjectGraphicsItems)
    {
        delete currentModelObjectGI;
    }

    delete m_pRootCoordinateSystemGraphicsItem;
    delete m_pTargetRobotLocationGraphicsItem;
}

void CLocationControlScene::SetVisualizationArea(float minX, float minY, float maxX, float maxY)
{
    m_pScaleRulerGraphicsItem->SetScaleArea(QRectF(minX, minY, maxX - minX, maxY - minY));
    m_pGraphicsScene->setSceneRect(m_pScaleRulerGraphicsItem->boundingRect());
}

void CLocationControlScene::AddModelObject(float minX, float minY, float maxX, float maxY, const QString& objectName)
{
    QRectF objectRect(minX, minY, maxX - minX, maxY - minY);

    CModelObjectGraphicsItem* modelObjectGraphicsItem = new CModelObjectGraphicsItem;

    modelObjectGraphicsItem->SetLabel(objectName);
    modelObjectGraphicsItem->SetModelObjectBoundingRect(objectRect);
    modelObjectGraphicsItem->SetLabelFont(QFont("Arial", 82));
    modelObjectGraphicsItem->setZValue(-1);

    m_pGraphicsScene->addItem(modelObjectGraphicsItem);
    m_modelObjectGraphicsItems.push_back(modelObjectGraphicsItem);
}

void CLocationControlScene::SetEstimatedRobotLocation(const CLocationState* pRobotLocationState)
{
    m_pEstimatedRobotLocationGraphicsItem->SetLocationState(pRobotLocationState);
}

const CLocationState* CLocationControlScene::GetEstimatedRobotLocation()
{
    return m_pEstimatedRobotLocationGraphicsItem->GetLocationState();
}

void CLocationControlScene::SetTargetRobotLocation(const CLocationState* pRobotLocationState)
{
    m_pTargetRobotLocationGraphicsItem->SetLocationState(pRobotLocationState);
}

const CLocationState* CLocationControlScene::GetTargetRobotLocation()
{
    return m_pTargetRobotLocationGraphicsItem->GetLocationState();
}

void CLocationControlScene::SetTargetAngleIsValid(bool angleIsValid)
{
    m_pTargetRobotLocationGraphicsItem->SetAngleIsValid(angleIsValid);
}

void CLocationControlScene::Update()
{
    if (m_pEstimatedRobotLocationGraphicsItem != NULL)
    {
        m_pEstimatedRobotLocationGraphicsItem->Update();
    }
    if (m_pTargetRobotLocationGraphicsItem != NULL)
    {
        m_pTargetRobotLocationGraphicsItem->Update();
    }
}
