#pragma once

#include <qwidget.h>
#include <QGridLayout>

#include <QPainter>
#include <qpolygon.h>

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QWheelEvent>

#include <QList>

#include "ModelObjectGraphicsItem.h"
#include "LocationControlScene.h"

class CLocationControlWidget : public QWidget
{
public:
    Q_OBJECT
public:
    CLocationControlWidget(QWidget* pParent);
    void SetLocationControlScene(CLocationControlScene* pLocationControlScene);
    const CLocationState& GetCurrentTargetLocationState() const;
    void SetCurrentTargetLocationState(const CLocationState& currentTargetLocationState);
public slots:
    void Update();
    void FitInView();
    void mousePressEvent(QMouseEvent* e) override;

signals:
    void targetChanged();
protected:
    void wheelEvent(QWheelEvent* pEvent) override;
    bool eventFilter(QObject* pTarget, QEvent* pEvent) override;

    QGridLayout* m_pMainGridLayout;
    QGridLayout* m_pSecondaryGridLayout;
    QGraphicsView* m_pGraphicsView;
    CLocationControlScene* m_pLocationControlScene;

    CLocationState m_currentTargetLocationState;
};

