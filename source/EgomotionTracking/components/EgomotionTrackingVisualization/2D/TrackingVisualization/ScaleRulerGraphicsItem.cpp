/*
 * ScaleRulerGraphicsItem.cpp
 *
 *  Created on: 26.03.2013
 *      Author: abyte
 */

#include "ScaleRulerGraphicsItem.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>

CScaleRulerGraphicsItem::CScaleRulerGraphicsItem()
{
    m_scaleLineWidth = 10.0;

    m_scaleStepSmallLineLength = 30;
    m_scaleStepLargeLineLength = 60;

    m_scaleStepSmall = 100;
    m_scaleStepLarge = 500;

    m_outlineColor = Qt::black;
    m_labelColor = Qt::black;

    m_drawGrid = false;

    m_gridPenSmallStep = QPen(Qt::darkGray, 0, Qt::DotLine);
    m_gridPenLargeStep = QPen(Qt::darkGray, 0, Qt::DashLine);
}

CScaleRulerGraphicsItem::~CScaleRulerGraphicsItem()
{
}

const QColor& CScaleRulerGraphicsItem::GetLabelColor() const
{
    return m_labelColor;
}

void CScaleRulerGraphicsItem::SetLabelColor(const QColor& labelColor)
{
    m_labelColor = labelColor;
    update();
}

const QFont& CScaleRulerGraphicsItem::GetLabelFont() const
{
    return m_labelFont;
}

void CScaleRulerGraphicsItem::SetLabelFont(const QFont& labelFont)
{
    m_labelFont = labelFont;
    update();
}

const QColor& CScaleRulerGraphicsItem::GetOutlineColor() const
{
    return m_outlineColor;
}

void CScaleRulerGraphicsItem::SetOutlineColor(const QColor& outlineColor)
{
    m_outlineColor = outlineColor;
    update();
}

const QRectF& CScaleRulerGraphicsItem::GetScaleArea() const
{
    return m_scaleArea;
}

void CScaleRulerGraphicsItem::SetScaleArea(const QRectF& scaleArea)
{
    prepareGeometryChange();
    m_scaleArea = scaleArea;
    GenerateGridLines();
    update();
}

float CScaleRulerGraphicsItem::GetScaleLineWidth() const
{
    return m_scaleLineWidth;
}

void CScaleRulerGraphicsItem::SetScaleLineWidth(float scaleLineWidth)
{
    prepareGeometryChange();
    m_scaleLineWidth = scaleLineWidth;
    update();
}

float CScaleRulerGraphicsItem::GetScaleStepLarge() const
{
    return m_scaleStepLarge;
}

void CScaleRulerGraphicsItem::SetScaleStepLarge(float scaleStepLarge)
{
    prepareGeometryChange();
    m_scaleStepLarge = scaleStepLarge;
    GenerateGridLines();
    update();
}

float CScaleRulerGraphicsItem::GetScaleStepLargeLineLength() const
{
    return m_scaleStepLargeLineLength;
}

void CScaleRulerGraphicsItem::SetScaleStepLargeLineLength(float scaleStepLargeLineLength)
{
    prepareGeometryChange();
    m_scaleStepLargeLineLength = scaleStepLargeLineLength;
    GenerateGridLines();
    update();
}

float CScaleRulerGraphicsItem::GetScaleStepSmall() const
{
    return m_scaleStepSmall;
}

void CScaleRulerGraphicsItem::SetScaleStepSmall(float scaleStepSmall)
{
    prepareGeometryChange();
    m_scaleStepSmall = scaleStepSmall;
    GenerateGridLines();
    update();
}

float CScaleRulerGraphicsItem::GetScaleStepSmallLineLength() const
{
    return m_scaleStepSmallLineLength;
}

QRectF CScaleRulerGraphicsItem::boundingRect() const
{
    return GetFlippedYScaleAreaRect().adjusted(-m_scaleStepLarge, -m_scaleStepLarge, m_scaleStepLarge, m_scaleStepLarge);
}

void CScaleRulerGraphicsItem::paint(QPainter* pPainter, const QStyleOptionGraphicsItem* pOption, QWidget* /* pWidget */)
{
    QRectF scaleArea = GetFlippedYScaleAreaRect();

    const float xStart = scaleArea.x();
    const float xEnd = scaleArea.x() + scaleArea.width();

    const float yStart = scaleArea.y();
    const float yEnd = scaleArea.y() + scaleArea.height();

    const QPointF sharedStartPoint(xStart, yEnd);
    const QPointF endPointX(xEnd, yEnd);
    const QPointF endPointY(xStart, yStart);


    float levelOfDetail = pOption->levelOfDetailFromTransform(pPainter->transform());

    if (m_drawGrid)
    {
        QPen gridPen = m_gridPenSmallStep;

        if (levelOfDetail > 0.5)
        {
            pPainter->setPen(gridPen);
            pPainter->drawLines(m_gridLinesSmall);
        }

        gridPen = m_gridPenLargeStep;
        pPainter->setPen(gridPen);
        pPainter->drawLines(m_gridLinesLarge);
    }

    QPen pen(m_outlineColor, m_scaleLineWidth);
    pPainter->setPen(pen);

    pPainter->drawLine(sharedStartPoint, endPointX);
    pPainter->drawLine(sharedStartPoint, endPointY);

    for (float x = Calculations::RoundUpToNextMultiple(xStart, m_scaleStepSmall); x <= xEnd; x += m_scaleStepSmall)
    {
        pPainter->drawLine(x, yEnd + m_scaleStepSmallLineLength / 2, x, yEnd - m_scaleStepSmallLineLength / 2);
    }

    for (float y = Calculations::RoundUpToNextMultiple(yStart, m_scaleStepSmall); y <= yEnd; y += m_scaleStepSmall)
    {
        pPainter->drawLine(xStart + m_scaleStepSmallLineLength / 2, y, xStart - m_scaleStepSmallLineLength / 2, y);
    }


    for (float x = Calculations::RoundUpToNextMultiple(xStart, m_scaleStepLarge); x <= xEnd; x += m_scaleStepLarge)
    {
        pPainter->drawLine(x, yEnd + m_scaleStepLargeLineLength / 2, x, yEnd - m_scaleStepLargeLineLength / 2);
    }

    for (float y = Calculations::RoundUpToNextMultiple(yStart, m_scaleStepLarge); y <= yEnd; y += m_scaleStepLarge)
    {
        pPainter->drawLine(xStart + m_scaleStepLargeLineLength / 2, y, xStart - m_scaleStepLargeLineLength / 2, y);
    }


    pPainter->setFont(m_labelFont);
    pPainter->setPen(m_labelColor);
    for (float x = Calculations::RoundUpToNextMultiple(xStart, m_scaleStepLarge); x <= xEnd; x += m_scaleStepLarge)
    {
        const float textY = yEnd + m_scaleStepLargeLineLength;
        const QRectF textRect(x - m_scaleStepLarge, textY, 2 * m_scaleStepLarge, 1.5 * m_labelFont.pointSizeF());
        pPainter->drawText(textRect, Qt::AlignCenter | Qt::AlignTop, QString::number(x));
    }

    for (float y = Calculations::RoundUpToNextMultiple(yStart, m_scaleStepLarge); y <= yEnd; y += m_scaleStepLarge)
    {
        const float textX = xStart - m_scaleStepLargeLineLength;
        const float textBoxWidth = m_scaleStepLarge;
        const QRectF textRect(textX - textBoxWidth, y - m_scaleStepLarge, textBoxWidth, 2 * m_scaleStepLarge);
        pPainter->drawText(textRect, Qt::AlignCenter | Qt::AlignRight, QString::number(-y));
    }
}

void CScaleRulerGraphicsItem::SetScaleStepSmallLineLength(float scaleStepSmallLineLength)
{
    prepareGeometryChange();
    m_scaleStepSmallLineLength = scaleStepSmallLineLength;
    update();
}

QRectF CScaleRulerGraphicsItem::GetFlippedYScaleAreaRect() const
{
    return QRectF(m_scaleArea.x(), -m_scaleArea.y() - m_scaleArea.height(), m_scaleArea.width(), m_scaleArea.height());
}

bool CScaleRulerGraphicsItem::isDrawGrid() const
{
    return m_drawGrid;
}

void CScaleRulerGraphicsItem::SetDrawGrid(bool drawGrid)
{
    prepareGeometryChange();
    m_drawGrid = drawGrid;
    update();
}

const QPen& CScaleRulerGraphicsItem::GetGridPenLargeStep() const
{
    return m_gridPenLargeStep;
}

void CScaleRulerGraphicsItem::SetGridPenLargeStep(const QPen& gridPenLargeStep)
{
    prepareGeometryChange();
    m_gridPenLargeStep = gridPenLargeStep;
    update();
}

const QPen& CScaleRulerGraphicsItem::GetGridPenSmallStep() const
{
    return m_gridPenSmallStep;
}

void CScaleRulerGraphicsItem::SetGridPenSmallStep(const QPen& gridPenSmallStep)
{
    prepareGeometryChange();
    m_gridPenSmallStep = gridPenSmallStep;
    update();
}

void CScaleRulerGraphicsItem::GenerateGridLines()
{
    m_gridLinesSmall.clear();
    m_gridLinesLarge.clear();

    QRectF scaleArea = GetFlippedYScaleAreaRect();

    const float xStart = scaleArea.x();
    const float xEnd = scaleArea.x() + scaleArea.width();

    const float yStart = scaleArea.y();
    const float yEnd = scaleArea.y() + scaleArea.height();

    for (float x = Calculations::RoundUpToNextMultiple(xStart + 0.5 * m_scaleStepSmall, m_scaleStepSmall); x <= xEnd; x += m_scaleStepSmall)
    {
        m_gridLinesSmall.push_back(QLineF(x, yStart, x, yEnd));
    }

    for (float y = Calculations::RoundUpToNextMultiple(yStart + 0.5 * m_scaleStepSmall, m_scaleStepSmall); y <= yEnd; y += m_scaleStepSmall)
    {
        m_gridLinesSmall.push_back(QLineF(xStart, y, xEnd, y));
    }


    for (float x = Calculations::RoundUpToNextMultiple(xStart + 0.5 * m_scaleStepLarge, m_scaleStepLarge); x <= xEnd; x += m_scaleStepLarge)
    {
        m_gridLinesLarge.push_back(QLineF(x, yStart, x, yEnd));
    }

    for (float y = Calculations::RoundUpToNextMultiple(yStart, m_scaleStepLarge); y <= yEnd - 0.5 * m_scaleStepLarge; y += m_scaleStepLarge)
    {
        m_gridLinesLarge.push_back(QLineF(xStart, y, xEnd, y));
    }
}
