#pragma once

#include <EgomotionTracking/components/EgomotionTracking/LocationEstimator/TrackingFilter/EgomotionTrackingPF/PlaneScatteringDirections.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/Misc/ColorMap/RGBAColorMap.h>
#include "LocationParticleGraphicsItem.h"
#include "LocationParticleNeckErrorGraphicsItem.h"
#include <QGraphicsItemGroup>
#include <QGraphicsEllipseItem>

class CLocationParticleGraphicsGroup
{
public:
    CLocationParticleGraphicsGroup();
    virtual ~CLocationParticleGraphicsGroup();
    void AddLocationParticle(const CLocationParticle* pLocationParticle);
    void Update();
    QGraphicsItem* GetRootLocationGraphicsItem();
    QGraphicsItem* GetRootNeckRollErrorGraphicsItem();
    QGraphicsItem* GetRootNeckPitchErrorGraphicsItem();
    QGraphicsItem* GetRootNeckYawErrorGraphicsItem();

    void SetDrawParticles(bool drawParticles);
    void SetVisible(bool visible);
    bool GetVisible() const;
    void SetDrawPlaneScatteringEllipse(bool drawEllipse);
    void SetPlaneScatteringDirections(const CPlaneScatteringDirections* pPlaneScatteringDirections);

    void SetPredefinedColormap(CRGBAColorMap::PredefinedColorMap predefinedColormap);
    CRGBAColorMap::PredefinedColorMap GetPredefinedColormap() const;
protected:
    QGraphicsItemGroup* m_pLocationGraphicsItemGroup;
    QGraphicsItemGroup* m_pNeckRollErrorGraphicsItemGroup;
    QGraphicsItemGroup* m_pNeckPitchErrorGraphicsItemGroup;
    QGraphicsItemGroup* m_pNeckYawErrorGraphicsItemGroup;

    QList<CLocationParticleGraphicsItem*> m_locationParticleGraphicItems;
    QList<CLocationParticleNeckErrorGraphicsItem*> m_locationParticleNeckErrorGraphicItems;

    QGraphicsEllipseItem* m_pScatteringEllipse;
    QGraphicsLineItem* m_pMeanNeckRollErrorLine;
    QGraphicsLineItem* m_pMeanNeckPitchErrorLine;
    QGraphicsLineItem* m_pMeanNeckYawErrorLine;

    bool m_visible;

    const CPlaneScatteringDirections* m_pPlaneScatteringDirections;
    float m_scatteringEllipseFactor;
    CRGBAColorMap m_colorMap;

    CRGBAColorMap::PredefinedColorMap m_predefinedColormap;
};

