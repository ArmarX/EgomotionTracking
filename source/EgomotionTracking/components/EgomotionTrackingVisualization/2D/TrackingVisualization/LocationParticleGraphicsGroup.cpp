#include "LocationParticleGraphicsGroup.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Conversions.h>

CLocationParticleGraphicsGroup::CLocationParticleGraphicsGroup():
    m_visible(true), m_pPlaneScatteringDirections(NULL)
{
    m_pLocationGraphicsItemGroup = new QGraphicsItemGroup;
    m_pLocationGraphicsItemGroup->setZValue(10);

    m_pNeckRollErrorGraphicsItemGroup = new QGraphicsItemGroup;
    m_pNeckRollErrorGraphicsItemGroup->setZValue(10);
    m_pNeckPitchErrorGraphicsItemGroup = new QGraphicsItemGroup;
    m_pNeckPitchErrorGraphicsItemGroup->setZValue(10);
    m_pNeckYawErrorGraphicsItemGroup = new QGraphicsItemGroup;
    m_pNeckYawErrorGraphicsItemGroup->setZValue(10);

    m_pScatteringEllipse = new QGraphicsEllipseItem;
    m_pLocationGraphicsItemGroup->addToGroup(m_pScatteringEllipse);
    m_pScatteringEllipse->setVisible(true);
    QPen ellipsePen(Qt::red);
    ellipsePen.setWidth(2.0);
    m_pScatteringEllipse->setPen(ellipsePen);
    m_scatteringEllipseFactor = configuration_scatteringEllipseSigmaFactor;

    m_predefinedColormap = CRGBAColorMap::eRGB_Hot;
    m_colorMap.SetPredefinedColorMap(m_predefinedColormap);

    m_pMeanNeckRollErrorLine = new QGraphicsLineItem;
    m_pMeanNeckPitchErrorLine = new QGraphicsLineItem;
    m_pMeanNeckYawErrorLine = new QGraphicsLineItem;
    m_pNeckRollErrorGraphicsItemGroup->addToGroup(m_pMeanNeckRollErrorLine);
    m_pNeckPitchErrorGraphicsItemGroup->addToGroup(m_pMeanNeckPitchErrorLine);
    m_pNeckYawErrorGraphicsItemGroup->addToGroup(m_pMeanNeckYawErrorLine);

    m_pMeanNeckRollErrorLine->setZValue(50.0);
    m_pMeanNeckPitchErrorLine->setZValue(50.0);
    m_pMeanNeckYawErrorLine->setZValue(50.0);
}

CLocationParticleGraphicsGroup::~CLocationParticleGraphicsGroup()
{
    CLocationParticleGraphicsItem* currentLocationParticleGI;
    foreach (currentLocationParticleGI, m_locationParticleGraphicItems)
    {
        delete currentLocationParticleGI;
    }

    CLocationParticleNeckErrorGraphicsItem* currentLocationParticleNeckErrorGI;
    foreach (currentLocationParticleNeckErrorGI, m_locationParticleNeckErrorGraphicItems)
    {
        delete currentLocationParticleNeckErrorGI;
    }

    delete m_pMeanNeckRollErrorLine;
    delete m_pMeanNeckPitchErrorLine;
    delete m_pMeanNeckYawErrorLine;
    delete m_pScatteringEllipse;
    delete m_pLocationGraphicsItemGroup;
    delete m_pNeckRollErrorGraphicsItemGroup;
    delete m_pNeckPitchErrorGraphicsItemGroup;
    delete m_pNeckYawErrorGraphicsItemGroup;
}

void CLocationParticleGraphicsGroup::AddLocationParticle(const CLocationParticle* pLocationParticle)
{
    CLocationParticleGraphicsItem* pLocationParticleGraphicsItem = new CLocationParticleGraphicsItem(pLocationParticle);
    m_locationParticleGraphicItems.push_back(pLocationParticleGraphicsItem);
    pLocationParticleGraphicsItem->SetUseWeightDependedColor(true);
    pLocationParticleGraphicsItem->SetColorMap(&m_colorMap);
    m_pLocationGraphicsItemGroup->addToGroup(pLocationParticleGraphicsItem);

    CLocationParticleNeckErrorGraphicsItem* pNeckRollErrorGraphicsItem = new CLocationParticleNeckErrorGraphicsItem(pLocationParticle, CLocationParticleNeckErrorGraphicsItem::eNeckErrorRoll);
    CLocationParticleNeckErrorGraphicsItem* pNeckPitchErrorGraphicsItem = new CLocationParticleNeckErrorGraphicsItem(pLocationParticle, CLocationParticleNeckErrorGraphicsItem::eNeckErrorPitch);
    CLocationParticleNeckErrorGraphicsItem* pNeckYawErrorGraphicsItem = new CLocationParticleNeckErrorGraphicsItem(pLocationParticle, CLocationParticleNeckErrorGraphicsItem::eNeckErrorYaw);

    m_locationParticleNeckErrorGraphicItems.push_back(pNeckRollErrorGraphicsItem);
    m_locationParticleNeckErrorGraphicItems.push_back(pNeckPitchErrorGraphicsItem);
    m_locationParticleNeckErrorGraphicItems.push_back(pNeckYawErrorGraphicsItem);

    pNeckRollErrorGraphicsItem->SetColorMap(&m_colorMap);
    pNeckPitchErrorGraphicsItem->SetColorMap(&m_colorMap);
    pNeckYawErrorGraphicsItem->SetColorMap(&m_colorMap);

    m_pNeckRollErrorGraphicsItemGroup->addToGroup(pNeckRollErrorGraphicsItem);
    m_pNeckPitchErrorGraphicsItemGroup->addToGroup(pNeckPitchErrorGraphicsItem);
    m_pNeckYawErrorGraphicsItemGroup->addToGroup(pNeckYawErrorGraphicsItem);
}

void CLocationParticleGraphicsGroup::Update()
{
    if (m_visible)
    {
        CLocationParticleGraphicsItem* currentLocationParticleGI;
        foreach (currentLocationParticleGI, m_locationParticleGraphicItems)
        {
            currentLocationParticleGI->Update();
        }

        if (m_pPlaneScatteringDirections)
        {
            const float width = 2 * m_pPlaneScatteringDirections->GetSigma1() * m_scatteringEllipseFactor;
            const float height = 2 *  m_pPlaneScatteringDirections->GetSigma2() * m_scatteringEllipseFactor;
            const float topLeftX = -width / 2.0;
            const float topLeftY = -height / 2.0;
            m_pScatteringEllipse->setRect(topLeftX, topLeftY, width, height);
            m_pScatteringEllipse->setPos(m_pPlaneScatteringDirections->GetMean().x, -m_pPlaneScatteringDirections->GetMean().y);
            m_pScatteringEllipse->setRotation(-Conversions::RadiansToDegree(atan2(m_pPlaneScatteringDirections->GetDirection1().y, m_pPlaneScatteringDirections->GetDirection1().x)));
        }

        float meanRollError = 0.0, meanPitchError = 0.0, meanYawError = 0.0, sumOfWeights = 0.0, sumOfParticles = 0.0;
        CLocationParticleNeckErrorGraphicsItem* pCurrentNeckErrorGI;
        foreach (pCurrentNeckErrorGI, m_locationParticleNeckErrorGraphicItems)
        {
            pCurrentNeckErrorGI->Update();

            if (pCurrentNeckErrorGI->GetLocationParticle()->GetActive())
            {
                const CLocationParticle* currentLocationParticle = pCurrentNeckErrorGI->GetLocationParticle();
                const float curWeight = currentLocationParticle->GetImportanceWeight();
                meanRollError += curWeight * currentLocationParticle->GetNeckRollError();
                meanPitchError += curWeight * currentLocationParticle->GetNeckPitchError();
                meanYawError += curWeight * currentLocationParticle->GetNeckYawError();
                sumOfWeights += curWeight;
                sumOfParticles += 1.0;
            }
        }
        if (sumOfWeights > FLT_EPSILON)
        {
            meanRollError /= sumOfWeights;
            meanPitchError /= sumOfWeights;
            meanYawError /= sumOfWeights;
        }
        else
        {
            if (sumOfParticles > 0.0)
            {
                meanRollError /= sumOfParticles;
                meanPitchError /= sumOfParticles;
                meanYawError /= sumOfParticles;
            }
        }
        const float meanDrawPositionRoll = NECKERRORSCALEITEM_SCALEPERDEGREE * Conversions::RadiansToDegree(meanRollError);
        const float meanDrawPositionPitch = NECKERRORSCALEITEM_SCALEPERDEGREE * Conversions::RadiansToDegree(meanPitchError);
        const float meanDrawPositionYaw = NECKERRORSCALEITEM_SCALEPERDEGREE * Conversions::RadiansToDegree(meanYawError);

        const float meanLineLength = 200;
        m_pMeanNeckRollErrorLine->setLine(-meanLineLength / 2, meanDrawPositionRoll, meanLineLength / 2, meanDrawPositionRoll);
        m_pMeanNeckPitchErrorLine->setLine(-meanLineLength / 2, meanDrawPositionPitch, meanLineLength / 2, meanDrawPositionPitch);
        m_pMeanNeckYawErrorLine->setLine(-meanLineLength / 2, meanDrawPositionYaw, meanLineLength / 2, meanDrawPositionYaw);
    }
}

QGraphicsItem* CLocationParticleGraphicsGroup::GetRootLocationGraphicsItem()
{
    return m_pLocationGraphicsItemGroup;
}

void CLocationParticleGraphicsGroup::SetDrawParticles(bool drawParticles)
{
    CLocationParticleGraphicsItem* particleGI;
    foreach (particleGI, m_locationParticleGraphicItems)
    {
        particleGI->SetDrawParticles(drawParticles);
    }

    CLocationParticleNeckErrorGraphicsItem* pCurrentNeckErrorGI;
    foreach (pCurrentNeckErrorGI, m_locationParticleNeckErrorGraphicItems)
    {
        pCurrentNeckErrorGI->SetDrawParticles(drawParticles);
    }
}

void CLocationParticleGraphicsGroup::SetVisible(bool visible)
{
    m_visible = visible;
    m_pLocationGraphicsItemGroup->setVisible(visible);
    m_pNeckRollErrorGraphicsItemGroup->setVisible(visible);
    m_pNeckPitchErrorGraphicsItemGroup->setVisible(visible);
    m_pNeckYawErrorGraphicsItemGroup->setVisible(visible);
    if (visible)
    {
        Update();
    }
}

bool CLocationParticleGraphicsGroup::GetVisible() const
{
    return m_visible;
}

void CLocationParticleGraphicsGroup::SetDrawPlaneScatteringEllipse(bool drawEllipse)
{
    m_pScatteringEllipse->setVisible(drawEllipse);
}

void CLocationParticleGraphicsGroup::SetPlaneScatteringDirections(const CPlaneScatteringDirections* pPlaneScatteringDirections)
{
    m_pPlaneScatteringDirections = pPlaneScatteringDirections;
}

void CLocationParticleGraphicsGroup::SetPredefinedColormap(CRGBAColorMap::PredefinedColorMap predefinedColormap)
{
    m_colorMap.SetPredefinedColorMap(predefinedColormap);
}

CRGBAColorMap::PredefinedColorMap CLocationParticleGraphicsGroup::GetPredefinedColormap() const
{
    return m_predefinedColormap;
}

QGraphicsItem* CLocationParticleGraphicsGroup::GetRootNeckRollErrorGraphicsItem()
{
    return m_pNeckRollErrorGraphicsItemGroup;
}


QGraphicsItem* CLocationParticleGraphicsGroup::GetRootNeckPitchErrorGraphicsItem()
{
    return m_pNeckPitchErrorGraphicsItemGroup;
}

QGraphicsItem* CLocationParticleGraphicsGroup::GetRootNeckYawErrorGraphicsItem()
{
    return m_pNeckYawErrorGraphicsItemGroup;
}
