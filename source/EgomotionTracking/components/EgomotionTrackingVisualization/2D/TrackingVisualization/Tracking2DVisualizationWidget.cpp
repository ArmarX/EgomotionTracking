/*
 * Tracking2DVisualizationWidget.cpp
 *
 *  Created on: 25.03.2013
 *      Author: abyte
 */

//#include "Tracking2DVisualizationWidget.moc"
#include "Tracking2DVisualizationWidget.h"

#include <math.h>

#include <QMenu>
#include <QScrollBar>
#include <QFileDialog>

CTracking2DVisualizationWidget::CTracking2DVisualizationWidget(QWidget* pParent) :
    QWidget(pParent), m_followLocationEstimation(false), m_pTracking2DGraphicsScene(NULL)
{
    m_pMainGridLayout = new QGridLayout(pParent);
    m_pMainGridLayout->addWidget(this, 0, 0);
    m_pMainGridLayout->setContentsMargins(0, 0, 0, 0);


    m_pGraphicsView = new QGraphicsView(this);
    m_pGraphicsView->setBackgroundBrush(QColor(255, 255, 255));

    m_pSecondaryGridLayout = new QGridLayout(this);
    m_pSecondaryGridLayout->addWidget(m_pGraphicsView, 0, 0);
    m_pSecondaryGridLayout->setContentsMargins(0, 0, 0, 0);

    m_pGraphicsView->setDragMode(QGraphicsView::ScrollHandDrag);
    m_pGraphicsView->setRenderHint(QPainter::Antialiasing, true);
    m_pGraphicsView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    m_pGraphicsView->setResizeAnchor(QGraphicsView::AnchorUnderMouse);
    m_pGraphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_pGraphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_pGraphicsView->installEventFilter(this);
    m_pGraphicsView->horizontalScrollBar()->installEventFilter(this);
    m_pGraphicsView->verticalScrollBar()->installEventFilter(this);
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(ShowContextMenu(const QPoint&)));

    createMenu();
}

CTracking2DVisualizationWidget::~CTracking2DVisualizationWidget()
{
}


void CTracking2DVisualizationWidget::SetTrackingVisualizationScene(CTracking2DVisualizationScene* pTracking2DGraphicsScene)
{
    m_pTracking2DGraphicsScene = pTracking2DGraphicsScene;
    m_pGraphicsView->setScene(pTracking2DGraphicsScene->GetGraphicsScene());

    FitInView();
}

void CTracking2DVisualizationWidget::FitInView()
{
    if (m_pTracking2DGraphicsScene != NULL)
    {
        m_pGraphicsView->fitInView(m_pTracking2DGraphicsScene->GetGraphicsScene()->sceneRect(), Qt::KeepAspectRatio);
    }
}

void CTracking2DVisualizationWidget::FitInLocationEstimationView()
{
    if (m_pTracking2DGraphicsScene != NULL)
    {
        const CLocationState* pEstimatedLocation = m_pTracking2DGraphicsScene->GetEstimatedRobotLocation();
        if (pEstimatedLocation != NULL)
        {
            const float viewRectSize = 1000; //1.5m
            QPointF tmp = m_pTracking2DGraphicsScene->CalculateSceneCoordinates(*pEstimatedLocation);

            QRectF viewRect(tmp.x() - viewRectSize / 2, tmp.y() - viewRectSize / 2, viewRectSize, viewRectSize);
            m_pGraphicsView->fitInView(viewRect, Qt::KeepAspectRatio);
        }
    }
}

void CTracking2DVisualizationWidget::ToogleAutoFollowLocationEstimation()
{
    if (m_followLocationEstimation)
    {
        m_followLocationEstimation = false;
    }
    else
    {
        m_followLocationEstimation = true;
    }
    Update();
}

void CTracking2DVisualizationWidget::ShowContextMenu(const QPoint& pos)
{

    if (m_pTracking2DGraphicsScene == NULL)
    {
        return;
    }

    QPoint globalPos = this->mapToGlobal(pos);

    bool drawGrid = m_pTracking2DGraphicsScene->GetDrawGrid();
    m_pDrawGridCheckerAction->setChecked(drawGrid);

    m_pDrawParticlesCheckerAction->setChecked(m_pTracking2DGraphicsScene->GetDrawParticles());
    m_pDrawParticlesEllipseCheckerAction->setChecked(m_pTracking2DGraphicsScene->GetDrawParticlesEllipse());
    m_pDrawGroundTruthLocationCheckerAction->setChecked(m_pTracking2DGraphicsScene->GetDrawGroundTruthRobotLocation());
    m_pDrawEstimatedLocationCheckerAction->setChecked(m_pTracking2DGraphicsScene->GetDrawEstimatedRobotLocation());
    m_pFollowLocationEstimationAction->setChecked(m_followLocationEstimation);
    m_pDrawLocationTrackCheckerAction->setChecked(m_pTracking2DGraphicsScene->GetDrawTrack());
    m_pDrawLocationTrackParentLinksCheckerAction->setChecked(m_pTracking2DGraphicsScene->GetDrawTrackParentLinks());
    m_pDrawLocationTrackDisplacementLinksCheckerAction->setChecked(m_pTracking2DGraphicsScene->GetDrawTrackDisplacementLinks());

    m_pDrawNeckErrorVisualizationCheckerAction->setChecked(m_pTracking2DGraphicsScene->GetDrawNeckErrorVisualization());
    m_pDrawLocationVisualizationCheckerAction->setChecked(m_pTracking2DGraphicsScene->GetDrawLocationVisualization());

    m_pDrawLocationTrackParentLinksCheckerAction->setEnabled(m_pTracking2DGraphicsScene->GetDrawTrack());
    m_pDrawLocationTrackDisplacementLinksCheckerAction->setEnabled(m_pTracking2DGraphicsScene->GetDrawTrack());

    m_pContentMenu->exec(globalPos);
}

void CTracking2DVisualizationWidget::SaveSceneToSVGDialog()
{
    QString saveFilename = QFileDialog::getSaveFileName(this, "Save Scene to SVG File", NULL, "Image File (*.svg)");
    if (!saveFilename.isNull() && m_pTracking2DGraphicsScene != NULL)
    {
        m_pTracking2DGraphicsScene->SaveSceneToSVGFile(saveFilename, "Tracking Visualization Scene");
    }
}

void CTracking2DVisualizationWidget::SaveViewToImageDialog()
{
    QString saveFilename = QFileDialog::getSaveFileName(this, "Save Current View to Image File", NULL, "Image File (*.jpg, *.png)");

    if (!saveFilename.isNull())
    {
        SaveCurrentViewToImageFile(saveFilename);
    }
}

void CTracking2DVisualizationWidget::SaveSceneToImageDialog()
{
    QString saveFilename = QFileDialog::getSaveFileName(this, "Save Scene to Image File", NULL, "Image File (*.jpg, *.png)");

    if (!saveFilename.isNull() && m_pTracking2DGraphicsScene != NULL)
    {
        m_pTracking2DGraphicsScene->ExportSceneToImageFile(saveFilename, 4.0, true, false);
    }
}

void CTracking2DVisualizationWidget::SaveCurrentViewToImageFile(const QString& filename)
{
    QPixmap pixmap = QPixmap::grabWidget(m_pGraphicsView);
    pixmap.save(filename);
}

void CTracking2DVisualizationWidget::SetFollowLocationEstimation(bool follow)
{
    m_followLocationEstimation = follow;
}

void CTracking2DVisualizationWidget::ToogleDrawingGrid()
{
    if (m_pTracking2DGraphicsScene != NULL)
    {
        m_pTracking2DGraphicsScene->SetDrawGrid(!m_pTracking2DGraphicsScene->GetDrawGrid());
    }
}

void CTracking2DVisualizationWidget::ToogleDrawingParticles()
{
    if (m_pTracking2DGraphicsScene != NULL)
    {
        m_pTracking2DGraphicsScene->SetDrawParticles(!m_pTracking2DGraphicsScene->GetDrawParticles());
    }
}

void CTracking2DVisualizationWidget::ToogleDrawingParticlesScatteringEllipse()
{
    if (m_pTracking2DGraphicsScene != NULL)
    {
        m_pTracking2DGraphicsScene->SetDrawParticlesEllipse(!m_pTracking2DGraphicsScene->GetDrawParticlesEllipse());
    }
}

void CTracking2DVisualizationWidget::ToogleDrawingGroundTruthLocation()
{
    if (m_pTracking2DGraphicsScene != NULL)
    {
        m_pTracking2DGraphicsScene->SetDrawGroundTruthRobotLocation(!m_pTracking2DGraphicsScene->GetDrawGroundTruthRobotLocation());
    }
}

void CTracking2DVisualizationWidget::ToogleDrawingEstimatedLocation()
{
    if (m_pTracking2DGraphicsScene != NULL)
    {
        m_pTracking2DGraphicsScene->SetDrawEstimatedRobotLocation(!m_pTracking2DGraphicsScene->GetDrawEstimatedRobotLocation());
    }
}

void CTracking2DVisualizationWidget::ToogleDrawingNeckRPYError()
{
    if (m_pTracking2DGraphicsScene != NULL)
    {
        m_pTracking2DGraphicsScene->SetDrawNeckErrorVisualization(!m_pTracking2DGraphicsScene->GetDrawNeckErrorVisualization());
    }
}

void CTracking2DVisualizationWidget::ToogleDrawingLocationVisualization()
{
    if (m_pTracking2DGraphicsScene != NULL)
    {
        m_pTracking2DGraphicsScene->SetDrawLocationVisualization(!m_pTracking2DGraphicsScene->GetDrawLocationVisualization());
    }
}

void CTracking2DVisualizationWidget::ToogleDrawingLocationTrack()
{
    if (m_pTracking2DGraphicsScene != NULL)
    {
        m_pTracking2DGraphicsScene->SetDrawTrack(!m_pTracking2DGraphicsScene->GetDrawTrack());
    }
}

void CTracking2DVisualizationWidget::ToogleDrawingLocationTrackParentLinks()
{
    if (m_pTracking2DGraphicsScene != NULL)
    {
        m_pTracking2DGraphicsScene->SetDrawTrackParentLinks(!m_pTracking2DGraphicsScene->GetDrawTrackParentLinks());
    }
}

void CTracking2DVisualizationWidget::ToogleDrawingLocationTrackDisplacementLinks()
{
    if (m_pTracking2DGraphicsScene != NULL)
    {
        m_pTracking2DGraphicsScene->SetDrawTrackDisplacementLinks(!m_pTracking2DGraphicsScene->GetDrawTrackDisplacementLinks());
    }
}

void CTracking2DVisualizationWidget::Update()
{
    if (m_followLocationEstimation)
    {
        FitInLocationEstimationView();
    }
}

void CTracking2DVisualizationWidget::SetColorMapHot()
{
    m_pTracking2DGraphicsScene->SetPredefinedParticleWeightColormap(CRGBAColorMap::eRGB_Hot);
}

void CTracking2DVisualizationWidget::SetColorMapRedGreen()
{
    m_pTracking2DGraphicsScene->SetPredefinedParticleWeightColormap(CRGBAColorMap::eRGB_RedYellowGreen);
}

void CTracking2DVisualizationWidget::SetColorMapHotTransparency()
{
    m_pTracking2DGraphicsScene->SetPredefinedParticleWeightColormap(CRGBAColorMap::eRGBA_Hot);
}

void CTracking2DVisualizationWidget::SetColorMapRedGreenTransparency()
{
    m_pTracking2DGraphicsScene->SetPredefinedParticleWeightColormap(CRGBAColorMap::eRGBA_RedYellowGreen);
}

bool CTracking2DVisualizationWidget::eventFilter(QObject* pTarget, QEvent* pEvent)
{
    if (pTarget == m_pGraphicsView)
    {
        if (pEvent->type() == QEvent::Wheel)
        {
            QWheelEvent* pWheelEvent = dynamic_cast<QWheelEvent*>(pEvent);
            if (pWheelEvent != NULL)
            {
                wheelEvent(pWheelEvent);
            }
            return true;
        }
    }
    else if (pTarget == m_pGraphicsView->horizontalScrollBar() || pTarget == m_pGraphicsView->verticalScrollBar())
    {
        if (pEvent->type() == QEvent::Wheel)

        {
            return true;
        }
    }
    return QWidget::eventFilter(pTarget, pEvent);
}

void CTracking2DVisualizationWidget::createMenu()
{
    m_pContentMenu = new QMenu(this);
    m_pSaveSubmenu = m_pContentMenu->addMenu("Save");
    m_pDrawingOptionsSubmenu = m_pContentMenu->addMenu("Drawing Options");
    m_pViewOptionsSubmenu = m_pContentMenu->addMenu("View Options");
    m_pFitInViewAction = new QAction("Fit in View", this);
    m_pFitInLocationEstimationViewAction = new QAction("Fit in Location Estimation View", this);
    m_pFollowLocationEstimationAction = new QAction("Follow Location Estimation", this);
    m_pFollowLocationEstimationAction->setCheckable(true);

    m_pDrawGridCheckerAction = new QAction("Draw Grid", this);
    m_pDrawParticlesCheckerAction = new QAction("Draw Particles", this);
    m_pDrawParticlesEllipseCheckerAction = new QAction("Draw Scattering Ellipse", this);
    m_pDrawGroundTruthLocationCheckerAction = new QAction("Draw Ground-Truth Location", this);
    m_pDrawEstimatedLocationCheckerAction = new QAction("Draw Estimated Location", this);
    m_pDrawLocationTrackCheckerAction = new QAction("Draw Location Tracks", this);
    m_pDrawLocationTrackParentLinksCheckerAction = new QAction("Draw Parent Links", this);
    m_pDrawLocationTrackDisplacementLinksCheckerAction = new QAction("Draw Displacement Links", this);

    m_pDrawLocationVisualizationCheckerAction = new QAction("Draw Location Visualization", this);
    m_pDrawNeckErrorVisualizationCheckerAction = new QAction("Draw Neck Error Visualization", this);

    m_pDrawGridCheckerAction->setCheckable(true);
    m_pDrawParticlesCheckerAction->setCheckable(true);
    m_pDrawParticlesEllipseCheckerAction->setCheckable(true);
    m_pDrawGroundTruthLocationCheckerAction->setCheckable(true);
    m_pDrawEstimatedLocationCheckerAction->setCheckable(true);
    m_pDrawLocationTrackCheckerAction->setCheckable(true);
    m_pDrawLocationTrackParentLinksCheckerAction->setCheckable(true);
    m_pDrawLocationTrackDisplacementLinksCheckerAction->setCheckable(true);
    m_pDrawLocationVisualizationCheckerAction->setCheckable(true);
    m_pDrawNeckErrorVisualizationCheckerAction->setCheckable(true);

    m_pSaveViewToImageAction = new QAction("Save current View to Image", this);
    m_pSaveSceneToImageAction = new QAction("Save Scene to Image", this);
    m_pSaveSceneToSVGAction = new QAction("Save Scene to SVG File", this);

    connect(m_pFitInViewAction, SIGNAL(triggered()), this, SLOT(FitInView()));
    connect(m_pFitInLocationEstimationViewAction, SIGNAL(triggered()), this, SLOT(FitInLocationEstimationView()));
    connect(m_pFollowLocationEstimationAction, SIGNAL(triggered()), this, SLOT(ToogleAutoFollowLocationEstimation()));

    connect(m_pDrawGridCheckerAction, SIGNAL(triggered()), this, SLOT(ToogleDrawingGrid()));
    connect(m_pDrawParticlesCheckerAction, SIGNAL(triggered()), this, SLOT(ToogleDrawingParticles()));
    connect(m_pDrawParticlesEllipseCheckerAction, SIGNAL(triggered()), this, SLOT(ToogleDrawingParticlesScatteringEllipse()));
    connect(m_pDrawGroundTruthLocationCheckerAction, SIGNAL(triggered()), this, SLOT(ToogleDrawingGroundTruthLocation()));
    connect(m_pDrawEstimatedLocationCheckerAction, SIGNAL(triggered()), this, SLOT(ToogleDrawingEstimatedLocation()));
    connect(m_pDrawLocationTrackCheckerAction, SIGNAL(triggered()), this, SLOT(ToogleDrawingLocationTrack()));
    connect(m_pSaveSceneToSVGAction, SIGNAL(triggered()), this, SLOT(SaveSceneToSVGDialog()));
    connect(m_pSaveSceneToImageAction, SIGNAL(triggered()), this, SLOT(SaveSceneToImageDialog()));
    connect(m_pSaveViewToImageAction, SIGNAL(triggered()), this, SLOT(SaveViewToImageDialog()));
    connect(m_pDrawLocationTrackParentLinksCheckerAction, SIGNAL(triggered()), this, SLOT(ToogleDrawingLocationTrackParentLinks()));
    connect(m_pDrawLocationTrackDisplacementLinksCheckerAction, SIGNAL(triggered()), this, SLOT(ToogleDrawingLocationTrackDisplacementLinks()));
    connect(m_pDrawLocationVisualizationCheckerAction, SIGNAL(triggered()), this, SLOT(ToogleDrawingLocationVisualization()));
    connect(m_pDrawNeckErrorVisualizationCheckerAction, SIGNAL(triggered()), this, SLOT(ToogleDrawingNeckRPYError()));

    m_pDrawingOptionsSubmenu->addAction(m_pDrawLocationVisualizationCheckerAction);
    m_pDrawingOptionsSubmenu->addAction(m_pDrawNeckErrorVisualizationCheckerAction);
    m_pDrawingOptionsSubmenu->addSeparator();

    m_pParticleWeightsColorMapSubmenu = m_pDrawingOptionsSubmenu->addMenu("Set Weight Colormap");
    m_pDrawingTrackOptionsSubmenu = m_pDrawingOptionsSubmenu->addMenu("Drawing Track");

    m_pDrawingOptionsSubmenu->addSeparator();

    m_pDrawingOptionsSubmenu->addAction(m_pDrawGridCheckerAction);
    m_pDrawingOptionsSubmenu->addAction(m_pDrawParticlesCheckerAction);
    m_pDrawingOptionsSubmenu->addAction(m_pDrawParticlesEllipseCheckerAction);
    m_pDrawingOptionsSubmenu->addAction(m_pDrawGroundTruthLocationCheckerAction);
    m_pDrawingOptionsSubmenu->addAction(m_pDrawEstimatedLocationCheckerAction);

    m_pDrawingTrackOptionsSubmenu->addAction(m_pDrawLocationTrackCheckerAction);
    m_pDrawingTrackOptionsSubmenu->addAction(m_pDrawLocationTrackParentLinksCheckerAction);
    m_pDrawingTrackOptionsSubmenu->addAction(m_pDrawLocationTrackDisplacementLinksCheckerAction);

    m_pSaveSubmenu->addAction(m_pSaveSceneToSVGAction);
    m_pSaveSubmenu->addAction(m_pSaveSceneToImageAction);
    m_pSaveSubmenu->addAction(m_pSaveViewToImageAction);
    m_pViewOptionsSubmenu->addAction(m_pFitInViewAction);
    m_pViewOptionsSubmenu->addAction(m_pFitInLocationEstimationViewAction);
    m_pViewOptionsSubmenu->addAction(m_pFollowLocationEstimationAction);

    m_pColorMapHotAction = new QAction("Hot Colormap", this);
    m_pColorMapHotTransparencyAction = new QAction("Hot Colormap (with Transparency)", this);
    m_pColorMapRedGreenAction = new QAction("RedYellowGreen Colormap", this);
    m_pColorMapRedGreenTransparencyAction = new QAction("RedYellowGreen Colormap (with Transparency)", this);

    m_pParticleWeightsColorMapSubmenu->addAction(m_pColorMapHotAction);
    m_pParticleWeightsColorMapSubmenu->addAction(m_pColorMapHotTransparencyAction);
    m_pParticleWeightsColorMapSubmenu->addAction(m_pColorMapRedGreenAction);
    m_pParticleWeightsColorMapSubmenu->addAction(m_pColorMapRedGreenTransparencyAction);

    connect(m_pColorMapHotAction, SIGNAL(triggered()), this, SLOT(SetColorMapHot()));
    connect(m_pColorMapHotTransparencyAction, SIGNAL(triggered()), this, SLOT(SetColorMapHotTransparency()));
    connect(m_pColorMapRedGreenAction, SIGNAL(triggered()), this, SLOT(SetColorMapRedGreen()));
    connect(m_pColorMapRedGreenTransparencyAction, SIGNAL(triggered()), this, SLOT(SetColorMapRedGreenTransparency()));
}

void CTracking2DVisualizationWidget::wheelEvent(QWheelEvent* pEvent)
{
    double numDegrees = -pEvent->delta() / 8.0;
    double numSteps = numDegrees / 15.0;
    double factor = pow(1.125, numSteps);
    m_pGraphicsView->scale(factor, factor);
}

void CTracking2DVisualizationWidget::mousePressEvent(QMouseEvent* e)
{
    if (e->button() == Qt::MidButton)
    {
        FitInView();
    }
}
