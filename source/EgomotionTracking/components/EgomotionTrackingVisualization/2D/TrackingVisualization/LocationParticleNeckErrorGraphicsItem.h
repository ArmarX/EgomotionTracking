#pragma once

#include <QPainter>
#include <QGraphicsItem>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/Misc/ColorMap/RGBAColorMap.h>

#include "NeckErrorScaleItem.h"

class CLocationParticleNeckErrorGraphicsItem : public QGraphicsItem
{
public:
    enum NeckErrorType
    {
        eNeckErrorRoll,
        eNeckErrorPitch,
        eNeckErrorYaw
    };

    CLocationParticleNeckErrorGraphicsItem(const CLocationParticle* pLocationParticle, NeckErrorType neckErrorType);

    QRectF boundingRect() const override;
    void paint(QPainter* pPainter, const QStyleOptionGraphicsItem* pOption, QWidget* pWidget) override;

    void SetLocationParticle(CLocationParticle* pParticle);
    const CLocationParticle* GetLocationParticle() const;

    QColor GetBackgroundColor() const;
    void SetBackgroundColor(const QColor& backgroundColor);
    QColor GetOutlineColor() const;
    void SetOutlineColor(const QColor& outlineColor);
    float GetSize() const;
    void SetSize(float GetSize);

    void Update();


    inline QColor CalculateWeightColor(float importanceWeight)
    {
        if (m_pColorMap == NULL)
        {
            if (importanceWeight < 0.0 || importanceWeight > 1.0)
            {
                return Qt::blue;
            }

            if (importanceWeight < 0.5)
            {
                return QColor(255, importanceWeight * 2 * 255, 0);
            }
            return QColor(255 * 2 * (1.0 - importanceWeight), 255, 0);
        }
        else
        {
            const CRGBAColor color = m_pColorMap->GetColor(importanceWeight);
            setOpacity(color.GetAlphaChannel());
            return QColor(color.GetByteRedChannel(), color.GetByteGreenChannel(), color.GetByteBlueChannel());
        }
    }

    bool GetDrawParticles() const;
    void SetDrawParticles(bool value);

    void SetColorMap(CRGBAColorMap* pColorMap);

    void SetNeckErrorType(NeckErrorType neckErrorType);

    enum Orientation
    {
        eHorizontal,
        eVertical
    };
    void SetOrientation(Orientation orientation);

protected:
    bool m_drawParticles;
    const CLocationParticle* m_pLocationParticle;
    float m_size;
    float m_sizeFactor;
    CRGBAColorMap* m_pColorMap;
    QColor m_outlineColor;
    QColor m_backgroundColor;

    NeckErrorType m_neckErrorType;
    Orientation m_orientation;
};

