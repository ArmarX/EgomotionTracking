#include "RobotLocationGraphicsItem.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Conversions.h>

CRobotLocationGraphicsItem::CRobotLocationGraphicsItem() :
    m_pLocationState(NULL)
{
    m_outlineColor = Qt::black;
    m_triangleBackgroundColor = Qt::darkBlue;
    m_circleBackgroundColor = Qt::blue;
    m_size = 100;
    m_angleIsValid = true;
}

float CRobotLocationGraphicsItem::GetSize() const
{
    return m_size;
}

void CRobotLocationGraphicsItem::SetSize(float size)
{
    m_size = size;
}

QColor CRobotLocationGraphicsItem::GetTriangleBackgroundColor() const
{
    return m_triangleBackgroundColor;
}

void CRobotLocationGraphicsItem::SetTriangleBackgroundColor(const QColor& backgroundColor)
{
    m_triangleBackgroundColor = backgroundColor;
}

QColor CRobotLocationGraphicsItem::GetOutlineColor() const
{
    return m_outlineColor;
}

void CRobotLocationGraphicsItem::SetOutlineColor(const QColor& outlineColor)
{
    m_outlineColor = outlineColor;
}

QRectF CRobotLocationGraphicsItem::boundingRect() const
{
    return QRectF(-m_size / 2, -2 * m_size / 3, m_size, 7 * m_size / 6);
}

void CRobotLocationGraphicsItem::paint(QPainter* pPainter, const QStyleOptionGraphicsItem* /*pOption*/, QWidget* /*pWidget*/)
{
    if (m_pLocationState == NULL)
    {
        return;
    }

    QPen pen(m_outlineColor, m_size / 50);
    pPainter->setPen(pen);

    pPainter->setBrush(m_circleBackgroundColor);
    pPainter->drawEllipse(QPointF(0.0, 0.0), m_size / 2, m_size / 2);

    if (m_angleIsValid)
    {
        pPainter->setBrush(m_triangleBackgroundColor);
        QPointF triangle[3];
        triangle[0] = QPointF(-m_size / 3, m_size / 3);
        triangle[1] = QPointF(m_size / 3, m_size / 3);
        triangle[2] = QPointF(0, -2 * m_size / 3);

        pPainter->drawConvexPolygon(triangle, 3);
    }
}

void CRobotLocationGraphicsItem::Update()
{
    if (m_pLocationState != NULL)
    {
        setPos(m_pLocationState->GetX(), - m_pLocationState->GetY());
        setRotation(-(Conversions::RadiansToDegree(m_pLocationState->GetAlpha())));

    }
}

const CLocationState* CRobotLocationGraphicsItem::GetLocationState() const
{
    return m_pLocationState;
}

void CRobotLocationGraphicsItem::SetLocationState(const CLocationState* pLocationState)
{
    m_pLocationState = pLocationState;
}

QColor CRobotLocationGraphicsItem::GetCircleBackgroundColor() const
{
    return m_circleBackgroundColor;
}

void CRobotLocationGraphicsItem::SetCircleBackgroundColor(const QColor& circleBackgroundColor)
{
    m_circleBackgroundColor = circleBackgroundColor;
}

void CRobotLocationGraphicsItem::SetAngleIsValid(bool angleIsValid)
{
    m_angleIsValid = angleIsValid;
}
