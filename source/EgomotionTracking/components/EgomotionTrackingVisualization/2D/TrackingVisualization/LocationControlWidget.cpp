//#include "LocationControlWidget.moc"
#include "LocationControlWidget.h"

#include <math.h>

#include <QMenu>
#include <QScrollBar>
#include <QFileDialog>

CLocationControlWidget::CLocationControlWidget(QWidget* pParent)
{
    m_pMainGridLayout = new QGridLayout(pParent);
    m_pMainGridLayout->addWidget(this, 0, 0);
    m_pMainGridLayout->setContentsMargins(0, 0, 0, 0);

    m_pGraphicsView = new QGraphicsView(this);
    m_pGraphicsView->setBackgroundBrush(QColor(255, 255, 255));

    m_pSecondaryGridLayout = new QGridLayout(this);
    m_pSecondaryGridLayout->addWidget(m_pGraphicsView, 0, 0);
    m_pSecondaryGridLayout->setContentsMargins(0, 0, 0, 0);

    //m_pGraphicsView->setDragMode(QGraphicsView::ScrollHandDrag);
    m_pGraphicsView->setRenderHint(QPainter::Antialiasing, true);
    m_pGraphicsView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    m_pGraphicsView->setResizeAnchor(QGraphicsView::AnchorUnderMouse);
    m_pGraphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_pGraphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_pGraphicsView->installEventFilter(this);
    m_pGraphicsView->horizontalScrollBar()->installEventFilter(this);
    m_pGraphicsView->verticalScrollBar()->installEventFilter(this);
    setContextMenuPolicy(Qt::CustomContextMenu);
}

void CLocationControlWidget::SetLocationControlScene(CLocationControlScene* pLocationControlScene)
{
    m_pLocationControlScene = pLocationControlScene;
    m_pGraphicsView->setScene(m_pLocationControlScene->GetGraphicsScene());
    FitInView();
}

const CLocationState& CLocationControlWidget::GetCurrentTargetLocationState() const
{
    return m_currentTargetLocationState;
}

void CLocationControlWidget::SetCurrentTargetLocationState(const CLocationState& currentTargetLocationState)
{
    m_currentTargetLocationState = currentTargetLocationState;
    m_pLocationControlScene->SetTargetRobotLocation(&m_currentTargetLocationState);
    m_pLocationControlScene->Update();
}

void CLocationControlWidget::Update()
{
}

void CLocationControlWidget::FitInView()
{
    if (m_pLocationControlScene != NULL)
    {
        m_pGraphicsView->fitInView(m_pLocationControlScene->GetGraphicsScene()->sceneRect(), Qt::KeepAspectRatio);
    }
}

void CLocationControlWidget::mousePressEvent(QMouseEvent* e)
{
    if (e->button() == Qt::MidButton)
    {
        FitInView();
    }
    else if (e->button() == Qt::LeftButton)
    {
        QPointF scenePoint = m_pGraphicsView->mapToScene(e->pos());
        m_currentTargetLocationState.SetX(scenePoint.x());
        m_currentTargetLocationState.SetY(-scenePoint.y());
        m_pLocationControlScene->SetTargetRobotLocation(&m_currentTargetLocationState);
        m_pLocationControlScene->Update();
        emit targetChanged();
    }
    else if (e->button() == Qt::RightButton)
    {
        QPointF scenePoint = m_pGraphicsView->mapToScene(e->pos());
        const float dx = scenePoint.x() - m_currentTargetLocationState.GetX();
        const float dy = -scenePoint.y() - m_currentTargetLocationState.GetY();
        const float angle = Conversions::NormalizeRadiansTo2PiInterval(atan2(dy, dx) - M_PI / 2);
        m_currentTargetLocationState.SetAlpha(angle);
        m_pLocationControlScene->SetTargetRobotLocation(&m_currentTargetLocationState);
        m_pLocationControlScene->Update();
        emit targetChanged();
    }
    m_pLocationControlScene->Update();
    m_pGraphicsView->repaint();
}

void CLocationControlWidget::wheelEvent(QWheelEvent* pEvent)
{
    double numDegrees = -pEvent->delta() / 8.0;
    double numSteps = numDegrees / 15.0;
    double factor = pow(1.125, numSteps);
    m_pGraphicsView->scale(factor, factor);
}

bool CLocationControlWidget::eventFilter(QObject* pTarget, QEvent* pEvent)
{
    if (pTarget == m_pGraphicsView)
    {
        if (pEvent->type() == QEvent::Wheel)
        {
            QWheelEvent* pWheelEvent = dynamic_cast<QWheelEvent*>(pEvent);
            if (pWheelEvent != NULL)
            {
                wheelEvent(pWheelEvent);
            }
            return true;
        }
    }
    else if (pTarget == m_pGraphicsView->horizontalScrollBar() || pTarget == m_pGraphicsView->verticalScrollBar())
    {
        if (pEvent->type() == QEvent::Wheel)

        {
            return true;
        }
    }
    return QWidget::eventFilter(pTarget, pEvent);
}
