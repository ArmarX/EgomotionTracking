#pragma once

#include <QGraphicsItem>
#include <QPainter>
#include <QPen>
#include <QPoint>
#include <QStyleOptionGraphicsItem>

const float NECKERRORSCALEITEM_STARTX = 250;
const float NECKERRORSCALEITEM_ENDX = 2000;
const float NECKERRORSCALEITEM_SCALEPERDEGREE = 200;
class CNeckErrorScaleItem : public QGraphicsItem
{
public:
    CNeckErrorScaleItem();
    void SetDegreeRange(float degreeMin, float degreeMax);

    QRectF boundingRect() const override;
    void paint(QPainter* pPainter, const QStyleOptionGraphicsItem* pOption, QWidget* pWidget) override;

    const QColor& GetLabelColor() const;
    void SetLabelColor(const QColor& labelColor);
    const QFont& GetLabelFont() const;
    void SetLabelFont(const QFont& labelFont);

    QPointF GetOriginRoll() const;
    QPointF GetOriginPitch() const;
    QPointF GetOriginYaw() const;
protected:
    void GenerateGridLines();

    inline float calculateYPos(float degreeValue) const
    {
        return (m_degreeMax - degreeValue) * NECKERRORSCALEITEM_SCALEPERDEGREE;
    }

    float m_degreeMin;
    float m_degreeMax;
    float m_scaleLineWidth;

    QColor m_outlineColor;
    QColor m_labelColor;
    QFont m_labelFont;

    QPen m_gridPen;

    QVector<QLineF> m_gridLines;
};

