#pragma once

#include <QPainter>
#include <QGraphicsItem>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>
#include <iostream>

class CRobotLocationGraphicsItem : public QGraphicsItem
{
public:
    CRobotLocationGraphicsItem();

    QRectF boundingRect() const override;
    void paint(QPainter* pPainter, const QStyleOptionGraphicsItem* pOption, QWidget* pWidget) override;

    void Update();

    const CLocationState* GetLocationState() const;
    void SetLocationState(const CLocationState* locationState);


    QColor GetOutlineColor() const;
    void SetOutlineColor(const QColor& outlineColor);

    QColor GetTriangleBackgroundColor() const;
    void SetTriangleBackgroundColor(const QColor& backgroundColor);

    float GetSize() const;
    void SetSize(float size);

    QColor GetCircleBackgroundColor() const;
    void SetCircleBackgroundColor(const QColor& circleBackgroundColor);

    void SetAngleIsValid(bool angleIsValid);
protected:
    const CLocationState* m_pLocationState;
    QColor m_outlineColor;
    QColor m_triangleBackgroundColor;
    QColor m_circleBackgroundColor;
    float m_size;
    bool m_angleIsValid;
};

