#pragma once

#include <QPainter>
#include <QGraphicsItem>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/Misc/ColorMap/RGBAColorMap.h>
#include <iostream>

class CLocationParticleGraphicsItem : public QGraphicsItem
{
public:
    CLocationParticleGraphicsItem(const CLocationParticle* pLocationParticle);

    QRectF boundingRect() const override;
    void paint(QPainter* pPainter, const QStyleOptionGraphicsItem* pOption, QWidget* pWidget) override;

    void SetLocationParticle(CLocationParticle* pParticle);
    const CLocationParticle* GetLocationParticle() const;

    QColor GetBackgroundColor() const;
    void SetBackgroundColor(const QColor& GetBackgroundColor);
    QColor GetOutlineColor() const;
    void SetOutlineColor(const QColor& GetOutlineColor);
    float GetSize() const;
    void SetSize(float GetSize);

    void Update();
    bool useWeightDependedColor() const;
    void SetUseWeightDependedColor(bool useWeightDependedColor);

    inline QColor CalculateWeightColor(float importanceWeight)
    {
        if (m_pColorMap == NULL)
        {
            if (importanceWeight < 0.0 || importanceWeight > 1.0)
            {
                return Qt::blue;
            }

            if (importanceWeight < 0.5)
            {
                return QColor(255, importanceWeight * 2 * 255, 0);
            }
            return QColor(255 * 2 * (1.0 - importanceWeight), 255, 0);
        }
        else
        {
            const CRGBAColor color = m_pColorMap->GetColor(importanceWeight);
            setOpacity(color.GetAlphaChannel());
            return QColor(color.GetByteRedChannel(), color.GetByteGreenChannel(), color.GetByteBlueChannel());
        }
    }

    bool GetDrawParticles() const;
    void SetDrawParticles(bool value);

    void SetColorMap(CRGBAColorMap* pColorMap);

protected:
    bool m_drawParticles;
    const CLocationParticle* m_pLocationParticle;
    float m_size;
    QColor m_outlineColor;
    QColor m_backgroundColor;

    QColor m_drawOutlineColor;
    QColor m_drawBackgroundColor;

    bool m_useWeightDependedColor;

    CRGBAColorMap* m_pColorMap;
};

