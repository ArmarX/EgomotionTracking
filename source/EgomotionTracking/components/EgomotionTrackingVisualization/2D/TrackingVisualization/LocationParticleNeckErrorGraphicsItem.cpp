#include "LocationParticleNeckErrorGraphicsItem.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Conversions.h>

CLocationParticleNeckErrorGraphicsItem::CLocationParticleNeckErrorGraphicsItem(const CLocationParticle* pLocationParticle, NeckErrorType neckErrorType) :
    m_drawParticles(true), m_pLocationParticle(pLocationParticle),
    m_pColorMap(NULL), m_neckErrorType(neckErrorType), m_orientation(eVertical)
{
    m_outlineColor = Qt::black;
    m_backgroundColor = Qt::black;

    m_size = 60;
    setZValue(10);
}

QRectF CLocationParticleNeckErrorGraphicsItem::boundingRect() const
{
    return QRectF(-m_size / 2, -m_size / 2, m_size, m_size);
}

void CLocationParticleNeckErrorGraphicsItem::paint(QPainter* pPainter, const QStyleOptionGraphicsItem* pOption, QWidget* pWidget)
{
    QLine crossLines[2];
    const float size = m_size * m_sizeFactor;
    crossLines[0].setLine(-size / 2, -size / 2, size / 2, size / 2);
    crossLines[1].setLine(-size / 2, size / 2, size / 2, -size / 2);

    QPen pen(m_outlineColor, size / 20);
    pPainter->setPen(pen);
    pPainter->setBrush(m_backgroundColor);
    pPainter->drawLines(crossLines, 2);
}

void CLocationParticleNeckErrorGraphicsItem::SetLocationParticle(CLocationParticle* pParticle)
{
    m_pLocationParticle = pParticle;
}

const CLocationParticle* CLocationParticleNeckErrorGraphicsItem::GetLocationParticle() const
{
    return m_pLocationParticle;
}

QColor CLocationParticleNeckErrorGraphicsItem::GetBackgroundColor() const
{
    return m_backgroundColor;
}

void CLocationParticleNeckErrorGraphicsItem::SetBackgroundColor(const QColor& backgroundColor)
{
    m_backgroundColor = backgroundColor;
}

QColor CLocationParticleNeckErrorGraphicsItem::GetOutlineColor() const
{
    return m_outlineColor;
}

void CLocationParticleNeckErrorGraphicsItem::SetOutlineColor(const QColor& outlineColor)
{
    m_outlineColor = outlineColor;
}

float CLocationParticleNeckErrorGraphicsItem::GetSize() const
{
    return m_size;
}

void CLocationParticleNeckErrorGraphicsItem::SetSize(float GetSize)
{
    m_size = GetSize;
}

void CLocationParticleNeckErrorGraphicsItem::Update()
{
    if (m_pLocationParticle != NULL)
    {
        if (m_pLocationParticle->GetActive() && m_drawParticles)
        {
            setVisible(true);

            float errorValue = 0.0;
            if (m_neckErrorType == eNeckErrorRoll)
            {
                errorValue = m_pLocationParticle->GetNeckRollError();
            }
            else if (m_neckErrorType == eNeckErrorPitch)
            {
                errorValue = m_pLocationParticle->GetNeckPitchError();
            }
            else if (m_neckErrorType == eNeckErrorYaw)
            {
                errorValue = m_pLocationParticle->GetNeckYawError();
            }

            const float drawPosition = NECKERRORSCALEITEM_SCALEPERDEGREE * Conversions::RadiansToDegree(errorValue);
            if (m_orientation == eHorizontal)
            {
                setPos(drawPosition, 0.0);
            }
            else if (m_orientation == eVertical)
            {
                setPos(0.0, drawPosition);
            }

            setZValue(10 + m_pLocationParticle->GetImportanceWeight());

            m_outlineColor = CalculateWeightColor(m_pLocationParticle->GetImportanceWeight());

            m_sizeFactor = 0.7 * m_pLocationParticle->GetImportanceWeight() + 0.3;
        }
        else
        {
            setVisible(false);
        }
    }
}

bool CLocationParticleNeckErrorGraphicsItem::GetDrawParticles() const
{
    return m_drawParticles;
}

void CLocationParticleNeckErrorGraphicsItem::SetDrawParticles(bool value)
{
    m_drawParticles = value;
}

void CLocationParticleNeckErrorGraphicsItem::SetColorMap(CRGBAColorMap* pColorMap)
{
    m_pColorMap = pColorMap;
}

void CLocationParticleNeckErrorGraphicsItem::SetNeckErrorType(CLocationParticleNeckErrorGraphicsItem::NeckErrorType neckErrorType)
{
    m_neckErrorType = neckErrorType;
}

void CLocationParticleNeckErrorGraphicsItem::SetOrientation(CLocationParticleNeckErrorGraphicsItem::Orientation orientation)
{
    m_orientation = orientation;
}
