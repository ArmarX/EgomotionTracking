#pragma once

#include "LocationTrackGraphicsItem.h"

#include <QGraphicsItemGroup>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>

#include <list>

class CLocationTrackGraphicsGroup
{
public:
    CLocationTrackGraphicsGroup();
    ~CLocationTrackGraphicsGroup();

    void AddLocationStateToTrack(const CLocationState& locationState);
    void Clear();

    void SetVisible(bool visible);
    bool GetVisible() const;

    void SetSize(float size);
    float GetSize() const;
    QColor GetBackgroundColor() const;
    void SetBackgroundColor(const QColor& backgroundColor);
    QColor GetOutlineColor() const;
    void SetOutlineColor(const QColor& outlineColor);

    void SetDisplacementLinkColor(const QColor& linkColor);
    void SetParentLinkColor(const QColor& linkColor);
    void SetLinkSize(float linkSize);

    void SetDrawDisplacementLink(bool drawDisplacementLink);
    bool GetDrawDisplacementLink() const;
    void SetDrawParentLink(bool drawParentLink);
    bool GetDrawParentLink() const;

    const CLocationTrackGraphicsItem* GetLastLocationTrackGraphicsItem() const;
    bool SetDisplacementLinkToLastLocationTrackGraphicsItem(const CLocationTrackGraphicsItem* pDisplacementLink);

    QGraphicsItem* GetRootGraphicsItem();
protected:

    void updateParametersToGroup();
    QGraphicsItemGroup* m_pGraphicsItemGroup;

    QGraphicsItemGroup* m_pDisplacementLinksGraphicsItemGroup;
    QGraphicsItemGroup* m_pParentLinksGraphicsItemGroup;

    QList<CLocationTrackGraphicsItem*> m_locationTrackGraphicsItems;

    float m_size;
    QColor m_outlineColor;
    QColor m_backgroundColor;

    QColor m_displacementLinkColor;
    QColor m_parentLinkColor;
    float m_linkSize;
    bool m_drawDisplacementLink;
    bool m_drawParentLink;

};

