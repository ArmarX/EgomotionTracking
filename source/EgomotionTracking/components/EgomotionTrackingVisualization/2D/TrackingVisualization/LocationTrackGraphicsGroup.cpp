#include "LocationTrackGraphicsGroup.h"

CLocationTrackGraphicsGroup::CLocationTrackGraphicsGroup()
{
    m_pGraphicsItemGroup = new QGraphicsItemGroup;
    m_pGraphicsItemGroup->setZValue(-15);

    m_pDisplacementLinksGraphicsItemGroup = new QGraphicsItemGroup(m_pGraphicsItemGroup);
    m_pParentLinksGraphicsItemGroup = new QGraphicsItemGroup(m_pGraphicsItemGroup);
    m_pDisplacementLinksGraphicsItemGroup->setZValue(-20);
    m_pParentLinksGraphicsItemGroup->setZValue(-19);

    m_pGraphicsItemGroup->addToGroup(m_pDisplacementLinksGraphicsItemGroup);
    m_pGraphicsItemGroup->addToGroup(m_pParentLinksGraphicsItemGroup);
}

CLocationTrackGraphicsGroup::~CLocationTrackGraphicsGroup()
{
    Clear();
    delete m_pGraphicsItemGroup;
}

void CLocationTrackGraphicsGroup::SetVisible(bool visible)
{
    m_pGraphicsItemGroup->setVisible(visible);
}

bool CLocationTrackGraphicsGroup::GetVisible() const
{
    return m_pGraphicsItemGroup->isVisible();
}

void CLocationTrackGraphicsGroup::SetSize(float size)
{
    m_size = size;
    updateParametersToGroup();
}

void CLocationTrackGraphicsGroup::AddLocationStateToTrack(const CLocationState& locationState)
{
    CLocationTrackGraphicsItem* pParentLocationTrackGraphicsItem = NULL;
    if (m_locationTrackGraphicsItems.size() > 0)
    {
        pParentLocationTrackGraphicsItem = m_locationTrackGraphicsItems.back();
    }

    CLocationTrackGraphicsItem* pLocationTrackGraphicsItem = new CLocationTrackGraphicsItem(locationState);
    m_locationTrackGraphicsItems.push_back(pLocationTrackGraphicsItem);
    pLocationTrackGraphicsItem->setParentItem(m_pGraphicsItemGroup);

    pLocationTrackGraphicsItem->SetSize(m_size);
    pLocationTrackGraphicsItem->SetBackgroundColor(m_backgroundColor);
    pLocationTrackGraphicsItem->SetOutlineColor(m_outlineColor);
    m_pGraphicsItemGroup->addToGroup(pLocationTrackGraphicsItem);

    if (pParentLocationTrackGraphicsItem != NULL)
    {
        const CLocationState& parentLocation = pParentLocationTrackGraphicsItem->GetLocationState();
        QGraphicsLineItem* pLineItem = new QGraphicsLineItem(parentLocation.GetX(), -parentLocation.GetY(), locationState.GetX(), -locationState.GetY());

        pLineItem->setPen(QPen(m_parentLinkColor, m_linkSize));
        m_pParentLinksGraphicsItemGroup->addToGroup(pLineItem);
        pLineItem->setParentItem(m_pGraphicsItemGroup);
    }
}

void CLocationTrackGraphicsGroup::Clear()
{
    CLocationTrackGraphicsItem* pCurrentLocationTrackGraphicsItem;
    foreach (pCurrentLocationTrackGraphicsItem, m_locationTrackGraphicsItems)
    {
        m_pGraphicsItemGroup->removeFromGroup(pCurrentLocationTrackGraphicsItem);
        delete pCurrentLocationTrackGraphicsItem;
    }
    m_locationTrackGraphicsItems.clear();

    m_pGraphicsItemGroup->removeFromGroup(m_pDisplacementLinksGraphicsItemGroup);
    m_pGraphicsItemGroup->removeFromGroup(m_pParentLinksGraphicsItemGroup);
    delete m_pDisplacementLinksGraphicsItemGroup;
    delete m_pParentLinksGraphicsItemGroup;
    m_pDisplacementLinksGraphicsItemGroup = new QGraphicsItemGroup;
    m_pParentLinksGraphicsItemGroup = new QGraphicsItemGroup;
    m_pGraphicsItemGroup->addToGroup(m_pDisplacementLinksGraphicsItemGroup);
    m_pGraphicsItemGroup->addToGroup(m_pParentLinksGraphicsItemGroup);
    m_pDisplacementLinksGraphicsItemGroup->setVisible(m_drawDisplacementLink);
    m_pParentLinksGraphicsItemGroup->setVisible(m_drawParentLink);
}

void CLocationTrackGraphicsGroup::updateParametersToGroup()
{
    CLocationTrackGraphicsItem* pCurrentLocationTrackGraphicsItem;
    foreach (pCurrentLocationTrackGraphicsItem, m_locationTrackGraphicsItems)
    {
        pCurrentLocationTrackGraphicsItem->SetSize(m_size);
        pCurrentLocationTrackGraphicsItem->SetBackgroundColor(m_backgroundColor);
        pCurrentLocationTrackGraphicsItem->SetOutlineColor(m_outlineColor);
    }
}

void CLocationTrackGraphicsGroup::SetOutlineColor(const QColor& outlineColor)
{
    m_outlineColor = outlineColor;
    updateParametersToGroup();
}

void CLocationTrackGraphicsGroup::SetDisplacementLinkColor(const QColor& linkColor)
{
    m_displacementLinkColor = linkColor;
}

QGraphicsItem* CLocationTrackGraphicsGroup::GetRootGraphicsItem()
{
    return m_pGraphicsItemGroup;
}

QColor CLocationTrackGraphicsGroup::GetOutlineColor() const
{
    return m_outlineColor;
}

void CLocationTrackGraphicsGroup::SetBackgroundColor(const QColor& backgroundColor)
{
    m_backgroundColor = backgroundColor;
    updateParametersToGroup();
}

QColor CLocationTrackGraphicsGroup::GetBackgroundColor() const
{
    return m_backgroundColor;
}

float CLocationTrackGraphicsGroup::GetSize() const
{
    return m_size;
}

bool CLocationTrackGraphicsGroup::GetDrawParentLink() const
{
    return m_drawParentLink;
}

const CLocationTrackGraphicsItem* CLocationTrackGraphicsGroup::GetLastLocationTrackGraphicsItem() const
{
    if (m_locationTrackGraphicsItems.size() > 0)
    {
        return m_locationTrackGraphicsItems.last();
    }
    return NULL;
}

bool CLocationTrackGraphicsGroup::SetDisplacementLinkToLastLocationTrackGraphicsItem(const CLocationTrackGraphicsItem* pDisplacementLink)
{
    if (m_locationTrackGraphicsItems.size() > 0 && pDisplacementLink != NULL)
    {
        CLocationTrackGraphicsItem* lastLocationTrackGraphicsItem = m_locationTrackGraphicsItems.last();
        const CLocationState& lastLocation = lastLocationTrackGraphicsItem->GetLocationState();
        const CLocationState& displacementLocation = pDisplacementLink->GetLocationState();
        QGraphicsLineItem* pLineItem = new QGraphicsLineItem(lastLocation.GetX(), -lastLocation.GetY(), displacementLocation.GetX(), -displacementLocation.GetY());
        pLineItem->setPen(QPen(m_displacementLinkColor, m_linkSize));
        m_pDisplacementLinksGraphicsItemGroup->addToGroup(pLineItem);
        pLineItem->setParentItem(m_pGraphicsItemGroup);
        return true;
    }
    return false;
}

void CLocationTrackGraphicsGroup::SetDrawParentLink(bool drawParentLink)
{
    m_drawParentLink = drawParentLink;
    m_pParentLinksGraphicsItemGroup->setVisible(m_drawParentLink);
}

bool CLocationTrackGraphicsGroup::GetDrawDisplacementLink() const
{
    return m_drawDisplacementLink;
}

void CLocationTrackGraphicsGroup::SetDrawDisplacementLink(bool drawDisplacementLink)
{
    m_drawDisplacementLink = drawDisplacementLink;
    m_pDisplacementLinksGraphicsItemGroup->setVisible(m_drawDisplacementLink);
}

void CLocationTrackGraphicsGroup::SetLinkSize(float linkSize)
{
    m_linkSize = linkSize;
}

void CLocationTrackGraphicsGroup::SetParentLinkColor(const QColor& linkColor)
{
    m_parentLinkColor = linkColor;
}
