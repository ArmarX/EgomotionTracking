/*
 * ModelObjectGraphicsItem.h
 *
 *  Created on: 26.03.2013
 *      Author: abyte
 */

#pragma once

#include <QStyleOptionGraphicsItem>
#include <QGraphicsItem>
#include <QPainter>
#include <QPen>

class CModelObjectGraphicsItem : public QGraphicsItem
{
public:
    CModelObjectGraphicsItem();
    ~CModelObjectGraphicsItem() override;


    QRectF boundingRect() const override;
    void paint(QPainter* pPainter, const QStyleOptionGraphicsItem* pOption, QWidget* pWidget) override;

    const QColor& GetBackgroundColor() const;
    void SetBackgroundColor(const QColor& backgroundColor);
    const QString& GetLabel() const;
    void SetLabel(const QString& label);
    const QColor& GetOutlineColor() const;
    void SetOutlineColor(const QColor& outlineColor);

    const QRectF& GetModelObjectBoundingRect() const;
    void SetModelObjectBoundingRect(const QRectF& modelObjectBoundingRect);
    const QColor& GetLabelColor() const;
    void SetLabelColor(const QColor& labelColor);
    const QFont& GetLabelFont() const;
    void SetLabelFont(const QFont& labelFont);

protected:
    QRectF GetFlippedYRect() const;
    QString m_label;

    QFont m_labelFont;
    QColor m_labelColor;
    QColor m_backgroundColor;
    QColor m_outlineColor;
    QRectF m_modelObjectBoundingRect;
};

