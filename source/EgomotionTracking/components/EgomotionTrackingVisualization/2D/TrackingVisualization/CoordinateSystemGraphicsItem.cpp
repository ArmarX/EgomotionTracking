/*
 * CoordinateSystemGraphicsItem.cpp
 *
 *  Created on: 26.03.2013
 *      Author: abyte
 */

#include "CoordinateSystemGraphicsItem.h"

CCoordinateSystemGraphicsItem::CCoordinateSystemGraphicsItem()
{
    m_axesLength = 180;
    m_axesLineWidth = 2;
    m_axesTriangleLength = 70;
    m_axesTriangleWidth = 50;

    m_backgroundColor = Qt::black;
    m_outlineColor = Qt::black;
    m_labelColor = Qt::black;

}

CCoordinateSystemGraphicsItem::~CCoordinateSystemGraphicsItem()
{
}

float CCoordinateSystemGraphicsItem::GetAxesLength() const
{
    return m_axesLength;
}

void CCoordinateSystemGraphicsItem::SetAxesLength(float axesLength)
{
    m_axesLength = axesLength;
}

float CCoordinateSystemGraphicsItem::GetAxesLineWidth() const
{
    return m_axesLineWidth;
}

void CCoordinateSystemGraphicsItem::SetAxesLineWidth(float axesLineWidth)
{
    m_axesLineWidth = axesLineWidth;
}

float CCoordinateSystemGraphicsItem::GetAxesTriangleLength() const
{
    return m_axesTriangleLength;
}

void CCoordinateSystemGraphicsItem::SetAxesTriangleLength(float axesTriangleLength)
{
    m_axesTriangleLength = axesTriangleLength;
}

float CCoordinateSystemGraphicsItem::GetAxesTriangleWidth() const
{
    return m_axesTriangleWidth;
}

void CCoordinateSystemGraphicsItem::SetAxesTriangleWidth(float axesTriangleWidth)
{
    m_axesTriangleWidth = axesTriangleWidth;
}

const QColor& CCoordinateSystemGraphicsItem::GetBackgroundColor() const
{
    return m_backgroundColor;
}

void CCoordinateSystemGraphicsItem::SetBackgroundColor(const QColor& backgroundColor)
{
    m_backgroundColor = backgroundColor;
}

const QFont& CCoordinateSystemGraphicsItem::GetLabelFont() const
{
    return m_labelFont;
}

void CCoordinateSystemGraphicsItem::SetLabelFont(const QFont& labelFont)
{
    m_labelFont = labelFont;
}

const QString& CCoordinateSystemGraphicsItem::GetLabelX() const
{
    return m_labelX;
}

void CCoordinateSystemGraphicsItem::SetLabelX(const QString& labelX)
{
    m_labelX = labelX;
}

const QString& CCoordinateSystemGraphicsItem::GetLabelY() const
{
    return m_labelY;
}

void CCoordinateSystemGraphicsItem::SetLabelY(const QString& labelY)
{
    m_labelY = labelY;
}

const QColor& CCoordinateSystemGraphicsItem::GetOutlineColor() const
{
    return m_outlineColor;
}

QRectF CCoordinateSystemGraphicsItem::boundingRect() const
{
    return QRect(0, 0, m_axesLength + m_axesTriangleLength, m_axesLength + m_axesTriangleLength);
}

void CCoordinateSystemGraphicsItem::paint(QPainter* pPainter, const QStyleOptionGraphicsItem* pOption, QWidget* pWidget)
{
    QPen pen(m_outlineColor, m_axesLineWidth);

    pPainter->setPen(pen);
    pPainter->setBrush(m_backgroundColor);

    pPainter->drawLine(0.0, 0.0, m_axesLength, 0.0);
    pPainter->drawLine(0.0, 0.0, 0.0, -m_axesLength);


    QPointF triangleX[] =
    {
        QPointF(m_axesLength, +m_axesTriangleWidth / 2),
        QPointF(m_axesLength, -m_axesTriangleWidth / 2),
        QPointF(m_axesLength + m_axesTriangleLength, 0)
    };
    QPointF triangleY[] =
    {
        QPointF(-m_axesTriangleWidth / 2, -m_axesLength),
        QPointF(m_axesTriangleWidth / 2, -m_axesLength),
        QPointF(0, -m_axesLength - m_axesTriangleLength)
    };


    pPainter->setPen(QPen(m_outlineColor, 1));
    pPainter->drawConvexPolygon(triangleX, 3);
    pPainter->drawConvexPolygon(triangleY, 3);

    pPainter->setFont(m_labelFont);
    pPainter->setPen(m_labelColor);
    pPainter->drawText(QPoint(m_axesLength + 0.5 * m_axesTriangleLength, -(2 * m_axesTriangleWidth)), m_labelX);
    pPainter->drawText(QPoint(1.5 * m_axesTriangleWidth, -(m_axesLength + 0.5 * m_axesTriangleLength)), m_labelY);

}

void CCoordinateSystemGraphicsItem::SetOutlineColor(const QColor& outlineColor)
{
    m_outlineColor = outlineColor;
}

const QColor& CCoordinateSystemGraphicsItem::GetLabelColor() const
{
    return m_labelColor;
}

void CCoordinateSystemGraphicsItem::SetLabelColor(const QColor& labelColor)
{
    m_labelColor = labelColor;
}
