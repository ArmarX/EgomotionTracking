/*
 * Tracking2DVisualizationWidget.h
 *
 *  Created on: 25.03.2013
 *      Author: abyte
 */

#pragma once

#include <qwidget.h>
#include <QGridLayout>

#include <QPainter>
#include <qpolygon.h>

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QWheelEvent>

#include <QList>

#include <iostream>

#include "ModelObjectGraphicsItem.h"
#include "Tracking2DVisualizationScene.h"

class CTracking2DVisualizationWidget: public QWidget
{
public:
    Q_OBJECT
public:
    CTracking2DVisualizationWidget(QWidget* pParent);
    ~CTracking2DVisualizationWidget() override;

    void SetTrackingVisualizationScene(CTracking2DVisualizationScene* pTracking2DGraphicsScene);

    void SaveCurrentViewToImageFile(const QString& filename);
    void SetFollowLocationEstimation(bool follow);
public slots:
    void ShowContextMenu(const QPoint& pos);
    void FitInView();
    void FitInLocationEstimationView();
    void ToogleAutoFollowLocationEstimation();
    void SaveSceneToSVGDialog();
    void SaveViewToImageDialog();
    void SaveSceneToImageDialog();
    void ToogleDrawingGrid();
    void ToogleDrawingParticles();
    void ToogleDrawingParticlesScatteringEllipse();
    void ToogleDrawingGroundTruthLocation();
    void ToogleDrawingEstimatedLocation();
    void ToogleDrawingNeckRPYError();
    void ToogleDrawingLocationVisualization();

    void ToogleDrawingLocationTrack();
    void ToogleDrawingLocationTrackParentLinks();
    void ToogleDrawingLocationTrackDisplacementLinks();
    void Update();

    void SetColorMapHot();
    void SetColorMapRedGreen();
    void SetColorMapHotTransparency();
    void SetColorMapRedGreenTransparency();
protected:
    void wheelEvent(QWheelEvent* pEvent) override;
    void mousePressEvent(QMouseEvent* e) override;
    bool eventFilter(QObject* pTarget, QEvent* pEvent) override;
    void createMenu();

    bool m_followLocationEstimation;
    QGridLayout* m_pMainGridLayout;
    QGridLayout* m_pSecondaryGridLayout;
    QGraphicsView* m_pGraphicsView;
    CTracking2DVisualizationScene* m_pTracking2DGraphicsScene;



    QMenu* m_pContentMenu;
    QMenu* m_pSaveSubmenu;
    QMenu* m_pDrawingOptionsSubmenu;
    QMenu* m_pViewOptionsSubmenu;
    QMenu* m_pParticleWeightsColorMapSubmenu;
    QMenu* m_pDrawingTrackOptionsSubmenu;

    QAction* m_pFitInViewAction;
    QAction* m_pFitInLocationEstimationViewAction;
    QAction* m_pFollowLocationEstimationAction;
    QAction* m_pDrawGridCheckerAction;
    QAction* m_pDrawParticlesCheckerAction;
    QAction* m_pDrawParticlesEllipseCheckerAction;
    QAction* m_pDrawGroundTruthLocationCheckerAction;
    QAction* m_pDrawEstimatedLocationCheckerAction;
    QAction* m_pDrawLocationTrackCheckerAction;
    QAction* m_pDrawLocationTrackParentLinksCheckerAction;
    QAction* m_pDrawLocationTrackDisplacementLinksCheckerAction;
    QAction* m_pDrawLocationVisualizationCheckerAction;
    QAction* m_pDrawNeckErrorVisualizationCheckerAction;
    QAction* m_pSaveSceneToSVGAction;
    QAction* m_pSaveViewToImageAction;
    QAction* m_pSaveSceneToImageAction;

    QAction* m_pColorMapHotAction;
    QAction* m_pColorMapRedGreenAction;
    QAction* m_pColorMapHotTransparencyAction;
    QAction* m_pColorMapRedGreenTransparencyAction;

    QAction* m_pLocationTrackingEnabledCheckerAction;
};

