#include "LocationParticleGraphicsItem.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Conversions.h>

CLocationParticleGraphicsItem::CLocationParticleGraphicsItem(const CLocationParticle* pLocationParticle) :
    m_drawParticles(true), m_pLocationParticle(pLocationParticle), m_useWeightDependedColor(false),
    m_pColorMap(NULL)
{
    m_outlineColor = Qt::darkRed;
    m_backgroundColor = Qt::red;

    m_drawOutlineColor = m_outlineColor;
    m_drawBackgroundColor = m_backgroundColor;

    m_size = 10;
    setZValue(10);
    setOpacity(1.0);
}

QRectF CLocationParticleGraphicsItem::boundingRect() const
{
    return QRectF(-m_size / 3, -2 * m_size / 3, 2 * m_size / 3, m_size);
}

void CLocationParticleGraphicsItem::SetLocationParticle(CLocationParticle* pParticle)
{
    m_pLocationParticle = pParticle;
}


const CLocationParticle* CLocationParticleGraphicsItem::GetLocationParticle() const
{
    return m_pLocationParticle;
}

float CLocationParticleGraphicsItem::GetSize() const
{
    return m_size;
}

void CLocationParticleGraphicsItem::SetSize(float size)
{
}

void CLocationParticleGraphicsItem::Update()
{
    if (m_pLocationParticle != NULL)
    {
        if (m_pLocationParticle->GetActive() && m_drawParticles)
        {
            setVisible(true);
            setPos(m_pLocationParticle->GetX(), - m_pLocationParticle->GetY());
            setRotation(-(Conversions::RadiansToDegree(m_pLocationParticle->GetAlpha())));
            setZValue(10 + m_pLocationParticle->GetImportanceWeight());

            if (m_useWeightDependedColor)
            {
                m_drawBackgroundColor = CalculateWeightColor(m_pLocationParticle->GetImportanceWeight());
                m_drawOutlineColor = Qt::black;
            }
        }
        else
        {
            setVisible(false);
        }
    }
}

QColor CLocationParticleGraphicsItem::GetOutlineColor() const
{
    return m_outlineColor;
}

void CLocationParticleGraphicsItem::SetOutlineColor(const QColor& outlineColor)
{
    m_outlineColor = outlineColor;
}

QColor CLocationParticleGraphicsItem::GetBackgroundColor() const
{
    return m_backgroundColor;
}

void CLocationParticleGraphicsItem::SetBackgroundColor(const QColor& backgroundColor)
{
    m_backgroundColor = backgroundColor;
}

void CLocationParticleGraphicsItem::paint(QPainter* pPainter, const QStyleOptionGraphicsItem* /*pOption*/, QWidget* /*pWidget*/)
{
    if (m_pLocationParticle == NULL)
    {
        return;
    }

    QPen pen(m_drawOutlineColor, m_size / 20);
    pPainter->setPen(pen);
    pPainter->setBrush(m_drawBackgroundColor);

    QPointF triangle[3];
    triangle[0] = QPointF(-m_size / 3, m_size / 3);
    triangle[1] = QPointF(m_size / 3, m_size / 3);
    triangle[2] = QPointF(0, -2 * m_size / 3);

    pPainter->drawConvexPolygon(triangle, 3);
}

bool CLocationParticleGraphicsItem::useWeightDependedColor() const
{
    return m_useWeightDependedColor;
}

void CLocationParticleGraphicsItem::SetUseWeightDependedColor(bool useWeightDependedColor)
{
    m_useWeightDependedColor = useWeightDependedColor;
    if (!useWeightDependedColor)
    {
        m_drawBackgroundColor = m_backgroundColor;
        m_drawOutlineColor = m_outlineColor;
    }
}

bool CLocationParticleGraphicsItem::GetDrawParticles() const
{
    return m_drawParticles;
}

void CLocationParticleGraphicsItem::SetDrawParticles(bool value)
{
    m_drawParticles = value;
    Update();
}

void CLocationParticleGraphicsItem::SetColorMap(CRGBAColorMap* pColorMap)
{
    m_pColorMap = pColorMap;
}
