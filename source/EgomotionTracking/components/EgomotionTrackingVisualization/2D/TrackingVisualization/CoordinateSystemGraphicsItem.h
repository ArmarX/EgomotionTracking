/*
 * CoordinateSystemGraphicsItem.h
 *
 *  Created on: 26.03.2013
 *      Author: abyte
 */

#pragma once

#include <QGraphicsItem>
#include <QPainter>
#include <QPen>
#include <QPoint>

class CCoordinateSystemGraphicsItem : public QGraphicsItem
{
public:
    CCoordinateSystemGraphicsItem();
    ~CCoordinateSystemGraphicsItem() override;

    QRectF boundingRect() const override;
    void paint(QPainter* pPainter, const QStyleOptionGraphicsItem* pOption, QWidget* pWidget) override;

    float GetAxesLength() const;
    void SetAxesLength(float axesLength);
    float GetAxesLineWidth() const;
    void SetAxesLineWidth(float axesLineWidth);
    float GetAxesTriangleLength() const;
    void SetAxesTriangleLength(float axesTriangleLength);
    float GetAxesTriangleWidth() const;
    void SetAxesTriangleWidth(float axesTriangleWidth);
    const QColor& GetBackgroundColor() const;
    void SetBackgroundColor(const QColor& backgroundColor);
    const QFont& GetLabelFont() const;
    void SetLabelFont(const QFont& labelFont);
    const QString& GetLabelX() const;
    void SetLabelX(const QString& labelX);
    const QString& GetLabelY() const;
    void SetLabelY(const QString& labelY);
    const QColor& GetOutlineColor() const;
    void SetOutlineColor(const QColor& outlineColor);
    const QColor& GetLabelColor() const;
    void SetLabelColor(const QColor& labelColor);

protected:
    float m_axesLength;
    float m_axesLineWidth;
    float m_axesTriangleLength;
    float m_axesTriangleWidth;

    QColor m_backgroundColor;
    QColor m_outlineColor;
    QColor m_labelColor;
    QFont m_labelFont;

    QString m_labelX;
    QString m_labelY;

};

