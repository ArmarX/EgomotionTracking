#pragma once

#include <QPainter>
#include <QGraphicsItem>
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationState.h>

class CLocationTrackGraphicsItem : public QGraphicsItem
{
public:
    CLocationTrackGraphicsItem(const CLocationState& locationState);
    ~CLocationTrackGraphicsItem() override;

    QRectF boundingRect() const override;
    void paint(QPainter* pPainter, const QStyleOptionGraphicsItem* pOption, QWidget* pWidget) override;

    void SetSize(float size);
    float GetSize() const;
    QColor GetBackgroundColor() const;
    void SetBackgroundColor(const QColor& backgroundColor);
    QColor GetOutlineColor() const;
    void SetOutlineColor(const QColor& outlineColor);

    const CLocationState& GetLocationState() const;
protected:
    CLocationState m_locationState;

    const CLocationTrackGraphicsItem* m_pDisplacementLink;
    const CLocationTrackGraphicsItem* m_pParentLink;

    float m_size;
    QColor m_outlineColor;
    QColor m_backgroundColor;
};

