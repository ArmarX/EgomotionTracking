#pragma once

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QGraphicsLineItem>

#include "RobotLocationGraphicsItem.h"
#include "ModelObjectGraphicsItem.h"
#include "CoordinateSystemGraphicsItem.h"
#include "ScaleRulerGraphicsItem.h"
#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>

class CLocationControlScene
{
public:
    CLocationControlScene();
    ~CLocationControlScene();

    void SetVisualizationArea(float minX, float minY, float maxX, float maxY);
    void AddModelObject(float minX, float minY, float maxX, float maxY, const QString& objectName);

    void SetEstimatedRobotLocation(const CLocationState* pRobotLocationState);
    const CLocationState* GetEstimatedRobotLocation();

    void SetTargetRobotLocation(const CLocationState* pRobotLocationState);
    const CLocationState* GetTargetRobotLocation();
    void SetTargetAngleIsValid(bool angleIsValid);

    void Update();
    QGraphicsScene* GetGraphicsScene()
    {
        return m_pGraphicsScene;
    }
protected:
    QList<CModelObjectGraphicsItem*> m_modelObjectGraphicsItems;
    QGraphicsScene* m_pGraphicsScene;
    CCoordinateSystemGraphicsItem* m_pRootCoordinateSystemGraphicsItem;
    CScaleRulerGraphicsItem* m_pScaleRulerGraphicsItem;
    CRobotLocationGraphicsItem* m_pEstimatedRobotLocationGraphicsItem;
    CRobotLocationGraphicsItem* m_pTargetRobotLocationGraphicsItem;
};

