#include "NeckErrorScaleItem.h"
#include <EgomotionTracking/components/EgomotionTracking/Helpers/Calculations.h>

CNeckErrorScaleItem::CNeckErrorScaleItem()
{
    m_scaleLineWidth = 10.0;

    m_outlineColor = Qt::black;
    m_labelColor = Qt::black;

    m_gridPen = QPen(Qt::darkGray, 0, Qt::DotLine);
    SetDegreeRange(-10.0, 10.0);
}

void CNeckErrorScaleItem::SetDegreeRange(float degreeMin, float degreeMax)
{
    m_degreeMin = degreeMin;
    m_degreeMax = degreeMax;
    GenerateGridLines();
}


QRectF CNeckErrorScaleItem::boundingRect() const
{
    return QRectF(0.0, 0.0, NECKERRORSCALEITEM_ENDX, 1000 + calculateYPos(m_degreeMin));
}

void CNeckErrorScaleItem::paint(QPainter* pPainter, const QStyleOptionGraphicsItem* pOption, QWidget* pWidget)
{
    pPainter->setPen(m_gridPen);
    pPainter->drawLines(m_gridLines);

    const float xStart = NECKERRORSCALEITEM_STARTX;
    const float xEnd = NECKERRORSCALEITEM_ENDX;

    const float yStart = calculateYPos(m_degreeMin);
    const float yEnd = calculateYPos(m_degreeMax);

    pPainter->setPen(QPen(Qt::black, 0, Qt::SolidLine));
    pPainter->setFont(m_labelFont);

    const float stepX = 0.25 * (xEnd - xStart);
    float curX = xStart + stepX;

    for (int i = 0; i < 3; ++i)
    {
        pPainter->drawLine(curX, yStart + 100, curX, yEnd - 100);

        const float textY = yStart + 200;
        const QRectF textRect(curX - stepX, textY, 2 * stepX, 1.5 * m_labelFont.pointSizeF());

        QString text;
        if (i == 0)
        {
            text = QString("Roll");
        }
        else if (i == 1)
        {
            text = QString("Pitch");
        }
        else
        {
            text = QString("Yaw");
        }
        pPainter->drawText(textRect, Qt::AlignCenter | Qt::AlignTop, text);
        curX += stepX;
    }

    pPainter->setPen(m_labelColor);
    for (float curDegree = Calculations::RoundDownToNextMultiple(m_degreeMin, 1.0); curDegree <= Calculations::RoundUpToNextMultiple(m_degreeMax, 1.0); curDegree += 1.0)
    {
        const float textY = calculateYPos(curDegree);
        const QRectF textRect(0, textY - NECKERRORSCALEITEM_SCALEPERDEGREE, 0.9 * NECKERRORSCALEITEM_STARTX, 2 * NECKERRORSCALEITEM_SCALEPERDEGREE);
        pPainter->drawText(textRect, Qt::AlignCenter | Qt::AlignRight, QString("%1").arg(curDegree));
    }
}

const QColor& CNeckErrorScaleItem::GetLabelColor() const
{
    return m_labelColor;
}

void CNeckErrorScaleItem::SetLabelColor(const QColor& labelColor)
{
    m_labelColor = labelColor;
}

const QFont& CNeckErrorScaleItem::GetLabelFont() const
{
    return m_labelFont;
}

void CNeckErrorScaleItem::SetLabelFont(const QFont& labelFont)
{
    m_labelFont = labelFont;
}

QPointF CNeckErrorScaleItem::GetOriginRoll() const
{
    return QPointF(NECKERRORSCALEITEM_STARTX + 0.25 * (NECKERRORSCALEITEM_ENDX - NECKERRORSCALEITEM_STARTX), calculateYPos(0.0));
}

QPointF CNeckErrorScaleItem::GetOriginPitch() const
{
    return QPointF(NECKERRORSCALEITEM_STARTX + 0.5 * (NECKERRORSCALEITEM_ENDX - NECKERRORSCALEITEM_STARTX), calculateYPos(0.0));
}

QPointF CNeckErrorScaleItem::GetOriginYaw() const
{
    return QPointF(NECKERRORSCALEITEM_STARTX + 0.75 * (NECKERRORSCALEITEM_ENDX - NECKERRORSCALEITEM_STARTX), calculateYPos(0.0));
}

void CNeckErrorScaleItem::GenerateGridLines()
{
    const float xStart = NECKERRORSCALEITEM_STARTX;
    const float xEnd =  NECKERRORSCALEITEM_ENDX;
    for (float curDegree = Calculations::RoundDownToNextMultiple(m_degreeMin, 1.0); curDegree <= Calculations::RoundUpToNextMultiple(m_degreeMax, 1.0); curDegree += 1.0)
    {
        const float y = calculateYPos(curDegree);
        m_gridLines.push_back(QLineF(xStart, y, xEnd, y));
    }
}
