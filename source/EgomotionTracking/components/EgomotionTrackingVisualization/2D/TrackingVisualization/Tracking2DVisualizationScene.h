/*
 * Tracking2DVisualizationScene.h
 *
 *  Created on: 26.03.2013
 *      Author: abyte
 */

#pragma once

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QGraphicsLineItem>
#include <QGraphicsItemGroup>

#include "RobotLocationGraphicsItem.h"
#include "LocationParticleGraphicsItem.h"
#include "LocationParticleGraphicsGroup.h"
#include "ModelObjectGraphicsItem.h"
#include "CoordinateSystemGraphicsItem.h"
#include "ScaleRulerGraphicsItem.h"
#include "LocationTrackGraphicsGroup.h"
#include "NeckErrorScaleItem.h"

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationParticle.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/Misc/ColorMap/RGBAColorMap.h>

class CTracking2DVisualizationScene
{
public:
    CTracking2DVisualizationScene();
    virtual ~CTracking2DVisualizationScene();

    void SetVisualizationArea(float minX, float minY, float maxX, float maxY);
    void AddModelObject(float minX, float minY, float maxX, float maxY, const QString& objectName);
    void AddLocationParticleGroup(CLocationParticleGraphicsGroup* pLocationParticleGroup);
    void SetGroundTruthRobotLocation(const CLocationState* robotLocationState);
    const CLocationState* GetGroundTruthRobotLocation();
    void SetEstimatedRobotLocation(const CLocationState* robotLocationState);
    const CLocationState* GetEstimatedRobotLocation();

    void SetGroundTruthRobotVisibility(bool visible);
    void SetEstimatedRobotVisibility(bool visible);

    void Update();
    void ClearTrack();
    void AddLocationToGroundTruthLocationTrack(const CLocationState& locationState);
    void AddLocationToEstimatedLocationTrack(const CLocationState& locationState);
    void AddMeasurementPairToLocationTrack(const CLocationState& estimatedLocation, const CLocationState& groundTruthLocation);

    QGraphicsScene* GetGraphicsScene()
    {
        return m_pGraphicsScene;
    }

    bool SaveSceneToSVGFile(const QString& filename, const QString& title);
    bool ExportSceneToImageFile(const QString& filename, float downScaleFactor = 4.0, bool useAntialiasing = true, bool useTransparency = true);

    bool GetDrawGrid() const;
    void SetDrawGrid(bool drawGrid);
    bool GetDrawParticles() const;
    void SetDrawParticles(bool drawParticles);
    void SetDrawParticlesEllipse(bool drawEllipse);
    bool GetDrawParticlesEllipse() const;
    bool GetDrawGroundTruthRobotLocation() const;
    void SetDrawGroundTruthRobotLocation(bool drawLocation);
    bool GetDrawEstimatedRobotLocation() const;
    void SetDrawEstimatedRobotLocation(bool drawLocation);
    void SetDrawTrack(bool drawTracks);
    bool GetDrawTrack() const;
    void SetDrawLocationVisualization(bool drawLocationVisualization);
    bool GetDrawLocationVisualization() const;
    void SetDrawNeckErrorVisualization(bool drawNeckErrorVisualization);
    bool GetDrawNeckErrorVisualization() const;

    void SetDrawTrackDisplacementLinks(bool drawLinks);
    bool GetDrawTrackDisplacementLinks() const;
    void SetDrawTrackParentLinks(bool drawLinks);
    bool GetDrawTrackParentLinks() const;

    void SetPredefinedParticleWeightColormap(CRGBAColorMap::PredefinedColorMap predefinedColormap);

    QPointF CalculateSceneCoordinates(const CLocationState& location) const;
protected:
    bool m_drawParticles;
    bool m_drawEllipse;
    bool m_drawTracks;

    bool m_drawTrackDisplacementLinks;
    bool m_drawTrackParentLinks;
    QList<CModelObjectGraphicsItem*> m_modelObjectGraphicsItems;
    QList<CLocationParticleGraphicsGroup*> m_locationParticleGroups;
    QGraphicsScene* m_pGraphicsScene;

    QGraphicsItemGroup* m_pLocationVisualizationGraphicsItemGroup;
    QGraphicsItemGroup* m_pNeckErrorVisualizationGraphicsItemGroup;

    CCoordinateSystemGraphicsItem* m_pRootCoordinateSystemGraphicsItem;
    CScaleRulerGraphicsItem* m_pScaleRulerGraphicsItem;
    CRobotLocationGraphicsItem* m_pGroundTruthRobotLocationGraphicsItem;
    CRobotLocationGraphicsItem* m_pEstimatedRobotLocationGraphicsItem;

    CLocationTrackGraphicsGroup* m_pGroundTruthLocationTrackGraphicsGroup;
    CLocationTrackGraphicsGroup* m_pEstimatedLocationTrackGraphicsGroup;

    CNeckErrorScaleItem* m_pNeckErrorScaleItem;

    QPointF m_locationOrigin;
};

