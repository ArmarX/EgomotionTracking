/*
 * Tracking2DVisualizationScene.cpp
 *
 *  Created on: 26.03.2013
 *      Author: abyte
 */

#include "Tracking2DVisualizationScene.h"
#include <QSvgGenerator>
#include <QImage>

#include <iostream>

CTracking2DVisualizationScene::CTracking2DVisualizationScene()
{
    m_pGraphicsScene = new QGraphicsScene;

    m_pLocationVisualizationGraphicsItemGroup = new QGraphicsItemGroup;
    m_pNeckErrorVisualizationGraphicsItemGroup = new QGraphicsItemGroup;

    m_pGraphicsScene->addItem(m_pLocationVisualizationGraphicsItemGroup);
    m_pGraphicsScene->addItem(m_pNeckErrorVisualizationGraphicsItemGroup);

    m_pRootCoordinateSystemGraphicsItem = new CCoordinateSystemGraphicsItem;
    m_pRootCoordinateSystemGraphicsItem->SetLabelX("X");
    m_pRootCoordinateSystemGraphicsItem->SetLabelY("Y");
    m_pRootCoordinateSystemGraphicsItem->SetAxesLineWidth(20);
    m_pRootCoordinateSystemGraphicsItem->SetLabelFont(QFont("Arial", 46));
    m_pRootCoordinateSystemGraphicsItem->setZValue(1);
    m_pLocationVisualizationGraphicsItemGroup->addToGroup(m_pRootCoordinateSystemGraphicsItem);

    m_pScaleRulerGraphicsItem = new CScaleRulerGraphicsItem;
    m_pScaleRulerGraphicsItem->SetLabelFont(QFont("Arial", 66));
    m_pScaleRulerGraphicsItem->SetDrawGrid(true);
    m_pScaleRulerGraphicsItem->setZValue(-2);
    m_pLocationVisualizationGraphicsItemGroup->addToGroup(m_pScaleRulerGraphicsItem);

    m_pGroundTruthRobotLocationGraphicsItem = new CRobotLocationGraphicsItem;
    m_pGroundTruthRobotLocationGraphicsItem->setZValue(3);
    m_pLocationVisualizationGraphicsItemGroup->addToGroup(m_pGroundTruthRobotLocationGraphicsItem);

    m_pEstimatedRobotLocationGraphicsItem = new CRobotLocationGraphicsItem;
    m_pEstimatedRobotLocationGraphicsItem->setZValue(4);
    m_pEstimatedRobotLocationGraphicsItem->SetTriangleBackgroundColor(Qt::darkMagenta);
    m_pEstimatedRobotLocationGraphicsItem->SetCircleBackgroundColor(Qt::magenta);
    m_pLocationVisualizationGraphicsItemGroup->addToGroup(m_pEstimatedRobotLocationGraphicsItem);
    m_drawParticles = true;
    m_drawEllipse = true;

    m_pGroundTruthLocationTrackGraphicsGroup = new CLocationTrackGraphicsGroup;
    m_pGroundTruthLocationTrackGraphicsGroup->SetSize(5);
    m_pGroundTruthLocationTrackGraphicsGroup->SetBackgroundColor(Qt::blue);
    m_pGroundTruthLocationTrackGraphicsGroup->SetOutlineColor(Qt::darkBlue);
    m_pGroundTruthLocationTrackGraphicsGroup->SetDisplacementLinkColor(Qt::red);
    m_pGroundTruthLocationTrackGraphicsGroup->SetParentLinkColor(Qt::darkBlue);
    m_pGroundTruthLocationTrackGraphicsGroup->SetLinkSize(1.0);
    m_pLocationVisualizationGraphicsItemGroup->addToGroup(m_pGroundTruthLocationTrackGraphicsGroup->GetRootGraphicsItem());
    m_pGroundTruthLocationTrackGraphicsGroup->GetRootGraphicsItem()->setZValue(-20);

    m_pEstimatedLocationTrackGraphicsGroup = new CLocationTrackGraphicsGroup;
    m_pEstimatedLocationTrackGraphicsGroup->SetSize(5);
    m_pEstimatedLocationTrackGraphicsGroup->SetBackgroundColor(Qt::magenta);
    m_pEstimatedLocationTrackGraphicsGroup->SetOutlineColor(Qt::darkMagenta);
    m_pEstimatedLocationTrackGraphicsGroup->SetDisplacementLinkColor(Qt::red);
    m_pEstimatedLocationTrackGraphicsGroup->SetParentLinkColor(Qt::darkMagenta);
    m_pEstimatedLocationTrackGraphicsGroup->SetLinkSize(1.0);
    m_pLocationVisualizationGraphicsItemGroup->addToGroup(m_pEstimatedLocationTrackGraphicsGroup->GetRootGraphicsItem());
    m_pEstimatedLocationTrackGraphicsGroup->GetRootGraphicsItem()->setZValue(-19);


    m_pNeckErrorScaleItem = new CNeckErrorScaleItem;
    m_pNeckErrorScaleItem->SetLabelFont(QFont("Arial", 66));
    m_pNeckErrorVisualizationGraphicsItemGroup->addToGroup(m_pNeckErrorScaleItem);

    SetDrawTrack(true);
    SetDrawTrackDisplacementLinks(true);
    SetDrawTrackParentLinks(true);
}

CTracking2DVisualizationScene::~CTracking2DVisualizationScene()
{
    CModelObjectGraphicsItem* currentModelObjectGI;
    foreach (currentModelObjectGI, m_modelObjectGraphicsItems)
    {
        delete currentModelObjectGI;
    }

    delete m_pRootCoordinateSystemGraphicsItem;
    delete m_pGroundTruthLocationTrackGraphicsGroup;
    delete m_pNeckErrorVisualizationGraphicsItemGroup;
    delete m_pLocationVisualizationGraphicsItemGroup;
}

void CTracking2DVisualizationScene::SetVisualizationArea(float minX, float minY, float maxX, float maxY)
{
    m_pScaleRulerGraphicsItem->SetScaleArea(QRectF(minX, minY, maxX - minX, maxY - minY));
    //m_pGraphicsScene->setSceneRect(m_pScaleRulerGraphicsItem->boundingRect());

    const float borderSize = 500.0;

    const float width = 3 * borderSize + NECKERRORSCALEITEM_ENDX + maxX - minX;

    m_pGraphicsScene->setSceneRect(0, 0, width, maxY - minY + 2 * borderSize);
    m_locationOrigin.setX(NECKERRORSCALEITEM_ENDX + 2 * borderSize - minX);
    m_locationOrigin.setY(borderSize + maxY);
    m_pLocationVisualizationGraphicsItemGroup->setPos(m_locationOrigin);

    m_pNeckErrorVisualizationGraphicsItemGroup->setPos(borderSize, borderSize);
}

void CTracking2DVisualizationScene::AddModelObject(float minX, float minY, float maxX, float maxY, const QString& objectName)
{
    QRectF objectRect(minX, minY, maxX - minX, maxY - minY);

    CModelObjectGraphicsItem* modelObjectGraphicsItem = new CModelObjectGraphicsItem;

    modelObjectGraphicsItem->SetLabel(objectName);
    modelObjectGraphicsItem->SetModelObjectBoundingRect(objectRect);
    modelObjectGraphicsItem->SetLabelFont(QFont("Arial", 82));
    modelObjectGraphicsItem->setZValue(-1);

    m_pLocationVisualizationGraphicsItemGroup->addToGroup(modelObjectGraphicsItem);
    m_modelObjectGraphicsItems.push_back(modelObjectGraphicsItem);
}

void CTracking2DVisualizationScene::AddLocationParticleGroup(CLocationParticleGraphicsGroup* pLocationParticleGroup)
{
    m_locationParticleGroups.push_back(pLocationParticleGroup);
    m_pLocationVisualizationGraphicsItemGroup->addToGroup(pLocationParticleGroup->GetRootLocationGraphicsItem());
    pLocationParticleGroup->GetRootLocationGraphicsItem()->setPos(0, 0);

    m_pNeckErrorVisualizationGraphicsItemGroup->addToGroup(pLocationParticleGroup->GetRootNeckRollErrorGraphicsItem());
    pLocationParticleGroup->GetRootNeckRollErrorGraphicsItem()->setPos(m_pNeckErrorScaleItem->GetOriginRoll());
    m_pNeckErrorVisualizationGraphicsItemGroup->addToGroup(pLocationParticleGroup->GetRootNeckPitchErrorGraphicsItem());
    pLocationParticleGroup->GetRootNeckPitchErrorGraphicsItem()->setPos(m_pNeckErrorScaleItem->GetOriginPitch());
    m_pNeckErrorVisualizationGraphicsItemGroup->addToGroup(pLocationParticleGroup->GetRootNeckYawErrorGraphicsItem());
    pLocationParticleGroup->GetRootNeckYawErrorGraphicsItem()->setPos(m_pNeckErrorScaleItem->GetOriginYaw());
}

void CTracking2DVisualizationScene::SetGroundTruthRobotLocation(const CLocationState* pRobotLocationState)
{
    m_pGroundTruthRobotLocationGraphicsItem->SetLocationState(pRobotLocationState);
}

const CLocationState* CTracking2DVisualizationScene::GetGroundTruthRobotLocation()
{
    return m_pGroundTruthRobotLocationGraphicsItem->GetLocationState();
}

void CTracking2DVisualizationScene::SetEstimatedRobotLocation(const CLocationState* pRobotLocationState)
{
    m_pEstimatedRobotLocationGraphicsItem->SetLocationState(pRobotLocationState);
}

const CLocationState* CTracking2DVisualizationScene::GetEstimatedRobotLocation()
{
    return m_pEstimatedRobotLocationGraphicsItem->GetLocationState();
}


bool CTracking2DVisualizationScene::SaveSceneToSVGFile(const QString& filename, const QString& title)
{
    QSvgGenerator svgGenerator;

    QRectF sceneRect = m_pGraphicsScene->sceneRect();
    svgGenerator.setFileName(filename);
    svgGenerator.setSize(QSize(sceneRect.width(), sceneRect.height()));
    svgGenerator.setViewBox(sceneRect);
    svgGenerator.setTitle(title);

    QPainter painter(&svgGenerator);
    m_pGraphicsScene->render(&painter);

    return true;
}

bool CTracking2DVisualizationScene::ExportSceneToImageFile(const QString& filename, float downScaleFactor, bool useAntialiasing, bool useTransparency)
{
    QRectF sceneRect = m_pGraphicsScene->sceneRect();

    QImage::Format imageFormat = QImage::Format_RGB32;
    if (useTransparency)
    {
        imageFormat = QImage::Format_ARGB32;
    }
    QImage image(sceneRect.width() / downScaleFactor, sceneRect.height() / downScaleFactor, imageFormat);
    if (useTransparency)
    {
        image.fill(0);
    }
    else
    {
        image.fill(16777215);
    }

    QPainter painter(&image);
    if (useAntialiasing)
    {
        painter.setRenderHint(QPainter::Antialiasing);
    }
    m_pGraphicsScene->render(&painter);
    return image.save(filename);
}

void CTracking2DVisualizationScene::SetGroundTruthRobotVisibility(bool visible)
{
    m_pGroundTruthRobotLocationGraphicsItem->setVisible(visible);
}

void CTracking2DVisualizationScene::SetEstimatedRobotVisibility(bool visible)
{
    m_pEstimatedRobotLocationGraphicsItem->setVisible(visible);
}

void CTracking2DVisualizationScene::Update()
{
    if (m_pGroundTruthRobotLocationGraphicsItem != NULL)
    {
        m_pGroundTruthRobotLocationGraphicsItem->Update();
    }
    if (m_pEstimatedRobotLocationGraphicsItem != NULL)
    {
        m_pEstimatedRobotLocationGraphicsItem->Update();
    }
    CLocationParticleGraphicsGroup* groupIterator;
    foreach (groupIterator, m_locationParticleGroups)
    {
        groupIterator->Update();
    }
}

void CTracking2DVisualizationScene::ClearTrack()
{
    m_pGroundTruthLocationTrackGraphicsGroup->Clear();
    m_pEstimatedLocationTrackGraphicsGroup->Clear();
}

void CTracking2DVisualizationScene::AddLocationToGroundTruthLocationTrack(const CLocationState& locationState)
{
    m_pGroundTruthLocationTrackGraphicsGroup->AddLocationStateToTrack(locationState);
}

void CTracking2DVisualizationScene::AddLocationToEstimatedLocationTrack(const CLocationState& locationState)
{
    m_pEstimatedLocationTrackGraphicsGroup->AddLocationStateToTrack(locationState);
}

void CTracking2DVisualizationScene::AddMeasurementPairToLocationTrack(const CLocationState& estimatedLocation, const CLocationState& groundTruthLocation)
{
    AddLocationToGroundTruthLocationTrack(groundTruthLocation);
    AddLocationToEstimatedLocationTrack(estimatedLocation);

    m_pGroundTruthLocationTrackGraphicsGroup->SetDisplacementLinkToLastLocationTrackGraphicsItem(m_pEstimatedLocationTrackGraphicsGroup->GetLastLocationTrackGraphicsItem());
}

bool CTracking2DVisualizationScene::GetDrawGrid() const
{
    return m_pScaleRulerGraphicsItem->isDrawGrid();
}

void CTracking2DVisualizationScene::SetDrawGrid(bool drawGrid)
{
    m_pScaleRulerGraphicsItem->SetDrawGrid(drawGrid);
}

bool CTracking2DVisualizationScene::GetDrawParticles() const
{
    return m_drawParticles;
}

void CTracking2DVisualizationScene::SetDrawParticles(bool drawParticles)
{
    m_drawParticles = drawParticles;

    CLocationParticleGraphicsGroup* particleGroup;
    foreach (particleGroup, m_locationParticleGroups)
    {
        particleGroup->SetDrawParticles(drawParticles);
    }
}

void CTracking2DVisualizationScene::SetDrawParticlesEllipse(bool drawEllipse)
{
    m_drawEllipse = drawEllipse;
    CLocationParticleGraphicsGroup* particleGroup;
    foreach (particleGroup, m_locationParticleGroups)
    {
        particleGroup->SetDrawPlaneScatteringEllipse(drawEllipse);
    }
}

bool CTracking2DVisualizationScene::GetDrawParticlesEllipse() const
{
    return m_drawEllipse;
}

bool CTracking2DVisualizationScene::GetDrawGroundTruthRobotLocation() const
{
    return m_pGroundTruthRobotLocationGraphicsItem->isVisible();
}

void CTracking2DVisualizationScene::SetDrawGroundTruthRobotLocation(bool drawLocation)
{
    m_pGroundTruthRobotLocationGraphicsItem->setVisible(drawLocation);
}

bool CTracking2DVisualizationScene::GetDrawEstimatedRobotLocation() const
{
    return m_pEstimatedRobotLocationGraphicsItem->isVisible();
}

void CTracking2DVisualizationScene::SetDrawEstimatedRobotLocation(bool drawLocation)
{
    m_pEstimatedRobotLocationGraphicsItem->setVisible(drawLocation);
}

void CTracking2DVisualizationScene::SetDrawTrack(bool drawTracks)
{
    m_drawTracks = drawTracks;
    m_pEstimatedLocationTrackGraphicsGroup->SetVisible(drawTracks);
    m_pGroundTruthLocationTrackGraphicsGroup->SetVisible(drawTracks);
}

bool CTracking2DVisualizationScene::GetDrawTrack() const
{
    return m_drawTracks;
}

void CTracking2DVisualizationScene::SetDrawLocationVisualization(bool drawLocationVisualization)
{
    m_pLocationVisualizationGraphicsItemGroup->setVisible(drawLocationVisualization);
}

bool CTracking2DVisualizationScene::GetDrawLocationVisualization() const
{
    return m_pLocationVisualizationGraphicsItemGroup->isVisible();
}

void CTracking2DVisualizationScene::SetDrawNeckErrorVisualization(bool drawNeckErrorVisualization)
{
    m_pNeckErrorVisualizationGraphicsItemGroup->setVisible(drawNeckErrorVisualization);
}

bool CTracking2DVisualizationScene::GetDrawNeckErrorVisualization() const
{
    return m_pNeckErrorVisualizationGraphicsItemGroup->isVisible();
}

void CTracking2DVisualizationScene::SetDrawTrackDisplacementLinks(bool drawLinks)
{
    m_pGroundTruthLocationTrackGraphicsGroup->SetDrawDisplacementLink(drawLinks);
    m_pEstimatedLocationTrackGraphicsGroup->SetDrawDisplacementLink(drawLinks);
    m_drawTrackDisplacementLinks = drawLinks;
}

bool CTracking2DVisualizationScene::GetDrawTrackDisplacementLinks() const
{
    return m_drawTrackDisplacementLinks;
}

void CTracking2DVisualizationScene::SetDrawTrackParentLinks(bool drawLinks)
{
    m_pGroundTruthLocationTrackGraphicsGroup->SetDrawParentLink(drawLinks);
    m_pEstimatedLocationTrackGraphicsGroup->SetDrawParentLink(drawLinks);
    m_drawTrackParentLinks = drawLinks;
}

bool CTracking2DVisualizationScene::GetDrawTrackParentLinks() const
{
    return m_drawTrackParentLinks;
}

void CTracking2DVisualizationScene::SetPredefinedParticleWeightColormap(CRGBAColorMap::PredefinedColorMap predefinedColormap)
{
    CLocationParticleGraphicsGroup* pCurParticleGroup;
    foreach (pCurParticleGroup, m_locationParticleGroups)
    {
        pCurParticleGroup->SetPredefinedColormap(predefinedColormap);
    }
}

QPointF CTracking2DVisualizationScene::CalculateSceneCoordinates(const CLocationState& location) const
{
    QPointF result;
    result.setX(m_locationOrigin.x() + location.GetX());
    result.setY(m_locationOrigin.y() - location.GetY());
    return result;
}
