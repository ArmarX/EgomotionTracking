/*
 * ScaleRulerGraphicsItem.h
 *
 *  Created on: 26.03.2013
 *      Author: abyte
 */

#pragma once

#include <iostream>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsItem>
#include <QPainter>
#include <QPen>
#include <QPoint>

class CScaleRulerGraphicsItem : public QGraphicsItem
{
public:
    CScaleRulerGraphicsItem();
    ~CScaleRulerGraphicsItem() override;

    QRectF boundingRect() const override;
    void paint(QPainter* pPainter, const QStyleOptionGraphicsItem* pOption, QWidget* pWidget) override;

    const QColor& GetLabelColor() const;
    void SetLabelColor(const QColor& labelColor);
    const QFont& GetLabelFont() const;
    void SetLabelFont(const QFont& labelFont);
    const QColor& GetOutlineColor() const;
    void SetOutlineColor(const QColor& outlineColor);
    const QRectF& GetScaleArea() const;
    void SetScaleArea(const QRectF& scaleArea);
    float GetScaleLineWidth() const;
    void SetScaleLineWidth(float scaleLineWidth);
    float GetScaleStepLarge() const;
    void SetScaleStepLarge(float scaleStepLarge);
    float GetScaleStepLargeLineLength() const;
    void SetScaleStepLargeLineLength(float scaleStepLargeLineLength);
    float GetScaleStepSmall() const;
    void SetScaleStepSmall(float scaleStepSmall);
    float GetScaleStepSmallLineLength() const;
    void SetScaleStepSmallLineLength(float scaleStepSmallLineLength);
    bool isDrawGrid() const;
    void SetDrawGrid(bool drawGrid);
    const QPen& GetGridPenLargeStep() const;
    void SetGridPenLargeStep(const QPen& gridPenLargeStep);
    const QPen& GetGridPenSmallStep() const;
    void SetGridPenSmallStep(const QPen& gridPenSmallStep);

protected:

    void GenerateGridLines();

    QRectF GetFlippedYScaleAreaRect() const;

    bool m_drawGrid;

    float m_scaleLineWidth;

    float m_scaleStepSmallLineLength;
    float m_scaleStepLargeLineLength;

    float m_scaleStepSmall;
    float m_scaleStepLarge;

    QColor m_outlineColor;
    QColor m_labelColor;
    QFont m_labelFont;

    QPen m_gridPenSmallStep;
    QPen m_gridPenLargeStep;

    QVector<QLineF> m_gridLinesSmall;
    QVector<QLineF> m_gridLinesLarge;

    QRectF m_scaleArea;

};

