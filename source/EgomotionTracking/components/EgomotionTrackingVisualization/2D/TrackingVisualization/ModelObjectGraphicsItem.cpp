/*
 * ModelObjectGraphicsItem.cpp
 *
 *  Created on: 26.03.2013
 *      Author: abyte
 */

#include "ModelObjectGraphicsItem.h"

CModelObjectGraphicsItem::CModelObjectGraphicsItem()
{
    m_labelColor = Qt::black;
    m_outlineColor = Qt::gray;
    m_backgroundColor = Qt::darkYellow;
}

CModelObjectGraphicsItem::~CModelObjectGraphicsItem()
{
}

const QColor& CModelObjectGraphicsItem::GetBackgroundColor() const
{
    return m_backgroundColor;
}

void CModelObjectGraphicsItem::SetBackgroundColor(const QColor& backgroundColor)
{
    m_backgroundColor = backgroundColor;
    update();
}

const QString& CModelObjectGraphicsItem::GetLabel() const
{
    return m_label;
}

void CModelObjectGraphicsItem::SetLabel(const QString& label)
{
    prepareGeometryChange();
    m_label = label;
    update();
}

const QColor& CModelObjectGraphicsItem::GetOutlineColor() const
{
    return m_outlineColor;
}

void CModelObjectGraphicsItem::SetOutlineColor(const QColor& outlineColor)
{
    m_outlineColor = outlineColor;
    update();
}

const QRectF& CModelObjectGraphicsItem::GetModelObjectBoundingRect() const
{
    return m_modelObjectBoundingRect;
}

QRectF CModelObjectGraphicsItem::boundingRect() const
{
    const int Margin = 1;
    return GetFlippedYRect().adjusted(-Margin, -Margin, +Margin, +Margin);
}

void CModelObjectGraphicsItem::paint(QPainter* pPainter, const QStyleOptionGraphicsItem* /* pOption */, QWidget* /* pWidget */)
{

    QRectF drawRect = GetFlippedYRect();
    QPen pen(m_outlineColor);
    pPainter->setPen(pen);
    pPainter->setBrush(m_backgroundColor);

    pPainter->drawRect(drawRect);

    pPainter->setFont(m_labelFont);
    pPainter->setPen(m_labelColor);
    pPainter->drawText(drawRect, Qt::AlignCenter, m_label);
}

void CModelObjectGraphicsItem::SetModelObjectBoundingRect(const QRectF& modelObjectBoundingRect)
{
    prepareGeometryChange();
    m_modelObjectBoundingRect = modelObjectBoundingRect;
    update();
}

const QColor& CModelObjectGraphicsItem::GetLabelColor() const
{
    return m_labelColor;
}

void CModelObjectGraphicsItem::SetLabelColor(const QColor& labelColor)
{
    m_labelColor = labelColor;
}

const QFont& CModelObjectGraphicsItem::GetLabelFont() const
{
    return m_labelFont;
}

void CModelObjectGraphicsItem::SetLabelFont(const QFont& labelFont)
{
    m_labelFont = labelFont;
}

QRectF CModelObjectGraphicsItem::GetFlippedYRect() const
{
    return QRectF(m_modelObjectBoundingRect.x(), -m_modelObjectBoundingRect.y() - m_modelObjectBoundingRect.height(), m_modelObjectBoundingRect.width(), m_modelObjectBoundingRect.height());
}
