#include "LocationTrackGraphicsItem.h"

CLocationTrackGraphicsItem::CLocationTrackGraphicsItem(const CLocationState& locationState) :
    m_locationState(locationState)
{
}

CLocationTrackGraphicsItem::~CLocationTrackGraphicsItem()
{
}

QRectF CLocationTrackGraphicsItem::boundingRect() const
{
    return QRectF(m_locationState.GetX(), -m_locationState.GetY() - m_size / 2, m_size, m_size);
}

void CLocationTrackGraphicsItem::paint(QPainter* pPainter, const QStyleOptionGraphicsItem* pOption, QWidget* pWidget)
{
    QPen pen(m_outlineColor, m_size / 10);
    pPainter->setPen(pen);
    pPainter->setBrush(m_backgroundColor);
    pPainter->drawEllipse(m_locationState.GetX() - m_size / 2, -m_locationState.GetY() - m_size / 2, m_size, m_size);

}

void CLocationTrackGraphicsItem::SetSize(float size)
{
    m_size = size;
}

QColor CLocationTrackGraphicsItem::GetBackgroundColor() const
{
    return m_backgroundColor;
}

void CLocationTrackGraphicsItem::SetBackgroundColor(const QColor& backgroundColor)
{
    m_backgroundColor = backgroundColor;
}

QColor CLocationTrackGraphicsItem::GetOutlineColor() const
{
    return m_outlineColor;
}

void CLocationTrackGraphicsItem::SetOutlineColor(const QColor& outlineColor)
{
    m_outlineColor = outlineColor;
}

const CLocationState& CLocationTrackGraphicsItem::GetLocationState() const
{
    return m_locationState;
}

float CLocationTrackGraphicsItem::GetSize() const
{
    return m_size;
}
