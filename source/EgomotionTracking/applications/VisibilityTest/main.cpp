#include <EgomotionTracking/components/VisibilityTest/VisibilityTest.h>

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>

int main(int argc, char* argv[])
{
    return armarx::runSimpleComponentApp<armarx::VisibilityTest>(argc, argv, "VisibilityTest");
}

