#include <EgomotionTracking/components/SessionPlayer/EgomotionTrackerSessionPlayer.h>

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>

int main(int argc, char* argv[])
{
    return armarx::runSimpleComponentApp<armarx::EgomotionTrackerSessionPlayer>(argc, argv, "EgomotionTrackerSessionPlayer");
}
