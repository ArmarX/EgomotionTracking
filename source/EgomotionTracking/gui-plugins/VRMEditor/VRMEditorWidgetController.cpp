/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::VRMEditorWidgetController
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VRMEditorWidgetController.h"

#include <string>

using namespace armarx;

VRMEditorWidgetController::VRMEditorWidgetController()
{

    layout = new QGridLayout(getWidget());

    VRME* vrmeWidget = new VRME(getWidget());

    layout->addWidget(vrmeWidget, 0, 0);
}


VRMEditorWidgetController::~VRMEditorWidgetController()
    = default;


void VRMEditorWidgetController::loadSettings(QSettings* settings)
{

}

void VRMEditorWidgetController::saveSettings(QSettings* settings)
{

}


void VRMEditorWidgetController::onInitComponent()
{

}


void VRMEditorWidgetController::onConnectComponent()
{

}
