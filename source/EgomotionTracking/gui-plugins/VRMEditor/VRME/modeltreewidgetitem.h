#pragma once

//QT INCLUDE
#include <QTreeWidgetItem>

//RVL INCLUDE
#include "RVL.h"

//DISPLAY DEFINITIONS

#define _ICON_COLUMN_ 0
#define _INSTANCE_ID_COLUMN_ 1
#define _TYPE_COLUMN_ 2
#define _ENABLED_COLUMN_ 3
#define _INDEXABLE_COLUMN_ 4
#define _VISIBLITIY_COLUMN_ 5
#define _SALIENCY_WEIGHT_COLUMN_ 6
#define _LABEL_COLUMN_ 7
#define _MAX_COLUMN_ 7

class CModelTreeWidgetItem : public QTreeWidgetItem
{
public:

    enum CollectionType
    {
        eNoCollection = -1, eVertices = 0, eDynamicVertexConfigurations, eOrientedVertices, eDynamicOrientedVertexConfigurations, eEdges, eFaces, eComposedFaces, eMultipleMeshes
    };

    static std::string CollectionTypeToString(const CollectionType Type);

    CModelTreeWidgetItem(QTreeWidget* parent, RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement* pElement, const QIcon* pIcons);
    CModelTreeWidgetItem(QTreeWidgetItem* parent, RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement* pElement, const QIcon* pIcons);
    CModelTreeWidgetItem(QTreeWidgetItem* parent, const CollectionType Type, const QIcon* pIcons);

    /*
    RVL::Representation::ModelBased::GeometricGraph::Base::CModelMultipleMesh* m_pMultipleMesh;
    RVL::Representation::ModelBased::GeometricGraph::Base::CModelMesh* m_pMesh;
    RVL::Representation::ModelBased::GeometricGraph::Base::CModelComposedFace* m_pComposedFace;
    RVL::Representation::ModelBased::GeometricGraph::Base::CModelFace* m_pFace;
    RVL::Representation::ModelBased::GeometricGraph::Base::CModelEdge* m_pEdge;
    RVL::Representation::ModelBased::GeometricGraph::Base::CModelOrientedVertex* m_pOrientedVertex;
    RVL::Representation::ModelBased::GeometricGraph::Base::CModelVertex* m_pVertex;
    */
    template<typename ModelElement> inline ModelElement* GetModelElement()
    {
        return m_pElement ? dynamic_cast<ModelElement*>(m_pElement) : NULL;
    }

    inline RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement* GetGenericModelElement()
    {
        return m_pElement;
    }

    inline CollectionType GetCollectionType() const
    {
        return m_CollectionType;
    }

    void Update(const QIcon* pIcons);

protected:



    void Create(const QIcon* pIcons);

    RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement* m_pElement;
    CollectionType m_CollectionType;

};

