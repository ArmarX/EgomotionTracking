#include <QtGui/QApplication>
#include "vrme.h"

int main(int argc, char* argv[])
{
    QApplication Application(argc, argv);
    VRME MainWidget;
    MainWidget.showMaximized();
    return Application.exec();
}
