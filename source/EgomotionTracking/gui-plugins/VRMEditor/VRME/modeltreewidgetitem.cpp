#include "modeltreewidgetitem.h"

using namespace RVL::Representation::ModelBased::GeometricGraph::Base;

std::string CModelTreeWidgetItem::CollectionTypeToString(const CollectionType Type)
{
    switch (Type)
    {
        case eNoCollection:
            return std::string("No Set");

        case eVertices:
            return std::string("Vertex Set");

        case eDynamicVertexConfigurations:
            return std::string("Dynamic Vertex Configuration Set");

        case eOrientedVertices:
            return std::string("Oriented Vertex Set");

        case eDynamicOrientedVertexConfigurations:
            return std::string("Dynamic Oriented Vertex Configuration Set");

        case eEdges:
            return std::string("Edge Set");

        case eFaces:
            return std::string("Face Set");

        case eComposedFaces:
            return std::string("Composed Face Set");

        case eMultipleMeshes:
            return std::string("Multiple Meshe Set");
    }

    return std::string("ERROR!");
}

CModelTreeWidgetItem::CModelTreeWidgetItem(QTreeWidget* parent, CModelElement* pElement, const QIcon* pIcons):
    QTreeWidgetItem(parent), m_pElement(pElement), m_CollectionType(eNoCollection)
{
    setFlags(Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
    setCheckState(_ICON_COLUMN_, Qt::Checked);
    Create(pIcons);
}

CModelTreeWidgetItem::CModelTreeWidgetItem(QTreeWidgetItem* parent, CModelElement* pElement, const QIcon* pIcons):
    QTreeWidgetItem(parent), m_pElement(pElement), m_CollectionType(eNoCollection)
{
    setFlags(Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
    setCheckState(_ICON_COLUMN_, Qt::Checked);
    Create(pIcons);
}


CModelTreeWidgetItem::CModelTreeWidgetItem(QTreeWidgetItem* parent, const CollectionType Type, const QIcon* pIcons):
    QTreeWidgetItem(parent), m_pElement(nullptr), m_CollectionType(Type)
{
    setFlags(Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled);
    Create(pIcons);
}

void CModelTreeWidgetItem::Update(const QIcon* pIcons)
{
    if (m_pElement)
    {
        const CModelElement::TypeId CurrentType = m_pElement->GetTypeId();
        QTreeWidgetItem::setText(_ICON_COLUMN_, QString(""));
        QTreeWidgetItem::setText(_INSTANCE_ID_COLUMN_, QString("%1").arg(m_pElement->GetInstanceId()));

        if (m_pElement->GetTypeId() == CModelElement::eMesh)
        {
            CModelMesh* pMesh = dynamic_cast<CModelMesh*>(m_pElement);

            if (pMesh->IsDynamic())
            {
                QTreeWidgetItem::setText(_TYPE_COLUMN_, QString("Dynamic") + QString(CModelElement::TypeIdToString(CurrentType).c_str()));

                if (pIcons)
                {
                    QTreeWidgetItem::setIcon(_ICON_COLUMN_, pIcons[int(CModelElement::eMultipleMesh + 1)]);
                }
            }
            else
            {
                QTreeWidgetItem::setText(_TYPE_COLUMN_, QString(CModelElement::TypeIdToString(CurrentType).c_str()));

                if (pIcons)
                {
                    QTreeWidgetItem::setIcon(_ICON_COLUMN_, pIcons[int(CurrentType)]);
                }
            }
        }
        else
        {
            QTreeWidgetItem::setText(_TYPE_COLUMN_, QString(CModelElement::TypeIdToString(CurrentType).c_str()));

            if (pIcons)
            {
                QTreeWidgetItem::setIcon(_ICON_COLUMN_, pIcons[int(CurrentType)]);
            }
        }

        const std::string& Label = m_pElement->GetLabel();

        if (Label.size())
        {
            QTreeWidgetItem::setText(_LABEL_COLUMN_, QString(m_pElement->GetLabel().c_str()));
        }

        QTreeWidgetItem::setText(_ENABLED_COLUMN_, QString((m_pElement->IsEnabled(false) ? "true" : "false")));
        QTreeWidgetItem::setText(_INDEXABLE_COLUMN_, QString((m_pElement->IsIndexable(false) ? "true" : "false")));
        QTreeWidgetItem::setText(_VISIBLITIY_COLUMN_, QString(CModelElement::VisibilityToString(m_pElement->GetVisibility(false)).c_str()));
        QTreeWidgetItem::setText(_SALIENCY_WEIGHT_COLUMN_, QString("%1").arg(m_pElement->GetSaliencyWeight()));
        //QTreeWidgetItem::setText(_DATA_COLUMN_,QString((m_pElement->GetElementData()?"Data Available":"")));

    }
    else if (m_CollectionType != eNoCollection)
    {
        for (int i = 0; i <= _MAX_COLUMN_; ++i)
            if (i == _TYPE_COLUMN_)
            {
                QTreeWidgetItem::setText(i, QString(CollectionTypeToString(m_CollectionType).c_str()));
            }
            else
            {
                QTreeWidgetItem::setText(i, QString(""));
            }

        if (pIcons)
        {
            QTreeWidgetItem::setIcon(_ICON_COLUMN_, pIcons[int(m_CollectionType)]);
        }
    }

    setTextAlignment(_ICON_COLUMN_, Qt::AlignHCenter | Qt::AlignVCenter | Qt::AlignCenter);
    setTextAlignment(_INSTANCE_ID_COLUMN_, Qt::AlignHCenter | Qt::AlignVCenter | Qt::AlignCenter);
    setTextAlignment(_ENABLED_COLUMN_, Qt::AlignHCenter | Qt::AlignVCenter | Qt::AlignCenter);
    setTextAlignment(_INDEXABLE_COLUMN_, Qt::AlignHCenter | Qt::AlignVCenter | Qt::AlignCenter);
    setTextAlignment(_TYPE_COLUMN_, Qt::AlignLeft | Qt::AlignVCenter);
    setTextAlignment(_VISIBLITIY_COLUMN_, Qt::AlignLeft | Qt::AlignVCenter);
    setTextAlignment(_LABEL_COLUMN_, Qt::AlignLeft | Qt::AlignVCenter);
    setTextAlignment(_LABEL_COLUMN_, Qt::AlignLeft | Qt::AlignVCenter);
    setTextAlignment(_SALIENCY_WEIGHT_COLUMN_, Qt::AlignHCenter | Qt::AlignVCenter | Qt::AlignCenter);
}

void CModelTreeWidgetItem::Create(const QIcon* pIcons)
{
    Update(pIcons);
}
