#pragma once

//STD Includes
#include <map>

//QT Includes
#include <QWidget>
#include <QCheckBox>
#include <QFileDialog>
#include <QDebug>

//RVL Includes
#include "RVL.h"

//Project Includes
#include "modeltreewidgetitem.h"
#include "xmlhighlighter.h"

#define _TOTAL_ELEMENT_ICONS_ 7
#define _TOTAL_SETS_ICONS_ 8

//TODOS
/*

1) Floor visualization
2) UCS visualization
3) Global rigid transfromations
4) Rotation transformations
5) Multiple face visulization
DONE-> 6) Progress for:
DONE->     * Savig
DONE-> * Tree generation
DONE-> * Check with large model
7) Meshes remove
8) Load from Files
9) Export meshes per file


*/

//BUGS

/*

1) When a mesh is selected and the multimesh is switched to non-visible (check-box) the selected mesh is still visible in the secondary view port
2) When iv as been already saved as a xml the save button in edition is not enabled

*/

namespace Ui
{
    class VRME;
}

class VRME : public QWidget,
    protected RVL::Visualization::_3D::SceneGraph::OpenInventor::Extensions::SoQtExtendedExaminerViewer::ISelectionDispatcher,
    protected RVL::Processing::IOperationProgressInterface
{
    Q_OBJECT

public:

    explicit VRME(QWidget* parent = 0);
    ~VRME() override;

private slots:

    void on_loadProjectFilepushButton_clicked();
    void on_saveProjectFilepushButton_clicked();
    void on_treeRadioButton_toggled(bool checked);
    void on_spacePortRadioButton_toggled(bool checked);
    void on_FilterRadioButton_toggled(bool checked);
    void on_clearSelectionPushButton_clicked();
    void on_modelTreeWidget_itemChanged(QTreeWidgetItem* pItem, int column);
    void on_modelTreeWidget_itemDoubleClicked(QTreeWidgetItem* pItem, int column);

    void on_verticesVisibleDisplaySwitchCheckBox_toggled(bool checked);
    void on_verticesOccludedDisplaySwitchCheckBox_toggled(bool checked);
    void on_verticesDynamicDisplaySwitchCheckBox_toggled(bool checked);
    void on_edgesVisibleDisplaySwitchCheckBox_toggled(bool checked);
    void on_edgesOccludedDisplaySwitchCheckBox_toggled(bool checked);
    void on_edgesDynamicDisplaySwitchCheckBox_toggled(bool checked);
    void on_facesVisibleDisplaySwitchCheckBox_toggled(bool checked);
    void on_facesOccludedDisplaySwitchCheckBox_toggled(bool checked);
    void on_facesDynamicDisplaySwitchCheckBox_toggled(bool checked);
    void on_composedFacesVisibleDisplaySwitchCheckBox_toggled(bool checked);
    void on_composedFacesOccludedDisplaySwitchCheckBox_toggled(bool checked);
    void on_composedFacesDynamicDisplaySwitchCheckBox_toggled(bool checked);
    void on_modelTreeWidget_itemSelectionChanged();

    void on_enableOperationApplyPushButton_clicked();
    void on_IndexableOperationApplyPushButton_clicked();
    void on_visibilityOperationApplyPushButton_clicked();
    void on_enabledComboBox_currentIndexChanged(int index);
    void on_indexableComboBox_currentIndexChanged(int index);
    void on_visibilityComboBox_currentIndexChanged(int index);

    void on_enabledVerticeCountCheckBox_toggled(bool checked);
    void on_enabledOrientedVerticesCountCheckBox_toggled(bool checked);
    void on_enabledEdgesCountCheckBox_toggled(bool checked);
    void on_enabledFacesCountCheckBox_toggled(bool checked);
    void on_enabledComposedFacesCountCheckBox_toggled(bool checked);
    void on_enabledMeshesCountCheckBox_toggled(bool checked);
    void on_enabledMultipleMeshesCountCheckBox_toggled(bool checked);

    void on_indexableVerticeCountCheckBox_toggled(bool checked);
    void on_indexableOrientedVerticesCountCheckBox_toggled(bool checked);
    void on_indexableEdgesCountCheckBox_toggled(bool checked);
    void on_indexableFacesCountCheckBox_toggled(bool checked);
    void on_indexableComposedFacesCountCheckBox_toggled(bool checked);
    void on_indexableMeshesCountCheckBox_toggled(bool checked);
    void on_indexableMultipleMeshesCountCheckBox_toggled(bool checked);

    void on_visibilityVerticeCountCheckBox_toggled(bool checked);
    void on_visibilityOrientedVerticesCountCheckBox_toggled(bool checked);
    void on_visibilityEdgesCountCheckBox_toggled(bool checked);
    void on_visibilityFacesCountCheckBox_toggled(bool checked);
    void on_visibilityComposedFacesCountCheckBox_toggled(bool checked);
    void on_visibilityMeshesCountCheckBox_toggled(bool checked);
    void on_visibilityMultipleMeshesCountCheckBox_toggled(bool checked);

    void on_pushButtonCreateTranslationTransformation_clicked();
    void on_pushButtonModifyTranslationTransformation_clicked();
    void on_horizontalSliderTranslationDoF_sliderMoved(int Value);

    void on_vertexABCheckBox_clicked(bool checked);
    void on_vertexABCDCheckBox_clicked(bool checked);
    void on_edgeACheckBox_clicked(bool checked);
    void on_edgeABCheckBox_clicked(bool checked);

    void on_SourceComboBoxA_currentIndexChanged(const QString& Arg);
    void on_SourceComboBoxB_currentIndexChanged(const QString& Arg);
    void on_SourceComboBoxC_currentIndexChanged(const QString& Arg);
    void on_SourceComboBoxD_currentIndexChanged(const QString& Arg);
    void on_minimalDisplacementLineEdit_textChanged(const QString& Arg);
    void on_maximalDisplacementLineEdit_textChanged(const QString& Arg);
    void on_defaultDisplacementLineEdit_textChanged(const QString& Arg);
    void on_pushButtonDestroyTranslationTransformation_clicked();
    void on_assignTransformationPushButton_clicked();
    void on_removeTransformationPushButton_clicked();
    void on_availableTransformationsTableWidget_itemSelectionChanged();
    void on_transformationLabelLineEditTranslation_textChanged(const QString& Arg);
    void on_saveCurrentFilePushButton_clicked();
    void on_automaticComboBox_currentIndexChanged(int index);
    void on_automaticOperationApplyPushButton_clicked();
    void on_pushButton_clicked();

    void on_modelTreeWidget_customContextMenuRequested(const QPoint& PointerLocation);
    void on_transformationLabelLineEditRotation_textChanged(const QString& Arg);
    void on_minimalRotationLineEdit_textChanged(const QString& Arg);
    void on_maximalRotationLineEdit_textChanged(const QString& Arg);
    void on_defaultRotationLineEdit_textChanged(const QString& Arg);
    void on_pushButtonCreatRotationTransformation_clicked();
    void on_pushButtonModifyRotationTransformation_clicked();
    void on_pushButtonDestroyRotationTransformation_clicked();
    void on_vertexABCheckBoxRotation_clicked(bool checked);
    void on_vertexABCDCheckBoxRotation_clicked(bool checked);
    void on_edgeACheckBoxRotation_clicked(bool checked);
    void on_edgeABCheckBoxRotation_clicked(bool checked);
    void on_SourceComboBoxRotationA_currentIndexChanged(const QString& Arg);
    void on_SourceComboBoxRotationB_currentIndexChanged(const QString& Arg);
    void on_SourceComboBoxRotationC_currentIndexChanged(const QString& Arg);
    void on_SourceComboBoxRotationD_currentIndexChanged(const QString& Arg);
    void on_horizontalSliderRotationDoF_sliderMoved(int Value);
    void on_saliencyWeightingComboBox_currentIndexChanged(int index);
    void on_saliencyWeightingLineEdit_textChanged(const QString& Arg);
    void on_saliencyWeightingOperationApplyPushButton_clicked();

    void on_gridMainViewportDisplaySwitchCheckBox_clicked(bool checked);
    void on_gridSecondaryViewportDisplaySwitchCheckBox_clicked(bool checked);
    void on_UCSMainViewportDisplaySwitchCheckBox_clicked(bool checked);
    void on_UCSSecondaryViewportDisplaySwitchCheckBox_clicked(bool checked);
    void on_GloablAligmentGenerateFramepushButton_clicked();
    void on_DisplayFrameAligmentcheckBox_toggled(bool checked);

    void on_GloablAligmentApplyFramepushButton_clicked();

private:

    void OnAddToSelection(const RVL::Visualization::_3D::SceneGraph::OpenInventor::Extensions::SoQtExtendedExaminerViewer* pViewer, SoNode* pNode, void* pUserdata) override;
    void OnRemoveFromSelection(const RVL::Visualization::_3D::SceneGraph::OpenInventor::Extensions::SoQtExtendedExaminerViewer* pViewer, SoNode* pNode, void* pUserdata) override;

private:

    bool SetOperationSettings(const RVL::Processing::IOperationProgressInterface::ExectionMode Mode, const int OperationId, const int InitialStage, const int FinalStage) override;
    void OnProgress(const int OperationId, const int UnitaryProgress) override;

private:

    Ui::VRME* ui;

protected:

    //VARIOUS
    void ExtendedSetupUI();

    //HELPERS
    void updateVisibilityInViewerMain();
    void clearTransformationPanel();
    void updateTransformationPanel();
    void addInformationToLog(const QString& Information);
    void setSelectionInformation(RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement* pModelElement);
    CModelTreeWidgetItem* FindItem(RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement* pModelElement);
    void updateAvailableTransformations();
    void ActivateEnableOperationApplyPushButton();
    void ActivateIndexableOperationApplyPushButton();
    void ActivateVisibilityOperationApplyPushButton();
    void ActivateSaliencyOperationApplyPushButton();
    void SelectMesh(RVL::Representation::ModelBased::GeometricGraph::Visualization::OpenInventor::COpenInventorModelMesh* pOpenInventorModelMesh);
    void UnselectMesh(const bool UpdateTransformationPanel = true);
    bool IsPanelContentTranslationTransformationReady(const bool CheckOnlyValues);
    bool IsPanelContentRotationTransformationReady(const bool CheckOnlyValues);
    void DeleteMesh(RVL::Representation::ModelBased::GeometricGraph::Base::CModelMesh* pModelMesh);

    void DynamicVisibilityConsistencyCheck(RVL::Representation::ModelBased::GeometricGraph::Base::CModelMesh* pModelMesh);
    void IndexabilityConsistencyCheck(RVL::Representation::ModelBased::GeometricGraph::Base::CModelMesh* pModelMesh);
    void UpdateTreeItems();

    //eVertex = 0X1
    //eOrientedVertex = 0X2,
    //eEdge = 0X4
    //eFace = 0X8
    //eComposedFace = 0X10
    //eMesh = 0X20
    //eMultipleMesh = 0X40

    QList<CModelTreeWidgetItem*> FilterItems(QList<QTreeWidgetItem*> InList, const uint TypeFlags = 0X7F);

    int GetActiveSelectionEnabledStatus(int TotalElementsByTypeId[7], int& TotalElements, const uint TypeFlags);
    int GetActiveSelectionIndexableStatus(int TotalElementsByTypeId[7], int& TotalElements, const uint TypeFlags);
    int GetActiveSelectionVisibilityStatus(int TotalElementsByTypeId[7], int& TotalElements, const uint TypeFlags);
    int GetActiveSelectionSaliencyStatus(int TotalElementsByTypeId[7], int& TotalElements, const uint TypeFlags, RVL::Real& IsoSaliency);


    //MODEL MANAGMENT
    //bool loadModelToTree(const QString& FileName, const bool DoResetAll, const bool DoFullLoad);
    bool loadMultipleMeshToTree(RVL::Representation::ModelBased::GeometricGraph::Base::CModelMultipleMesh* pModelMultipleMesh, const bool DoFullLoad);
    bool loadMeshToTree(RVL::Representation::ModelBased::GeometricGraph::Base::CModelMesh* pModelMesh, CModelTreeWidgetItem* pModelMultipleMeshItem, const bool DoFullLoad);
    bool loadComposedFaces(RVL::Representation::ModelBased::GeometricGraph::Base::CModelMesh* pModelMesh, CModelTreeWidgetItem* pModelMeshItem);
    bool loadFaces(RVL::Representation::ModelBased::GeometricGraph::Base::CModelMesh* pModelMesh, CModelTreeWidgetItem* pModelMeshItem);
    bool loadEdges(RVL::Representation::ModelBased::GeometricGraph::Base::CModelMesh* pModelMesh, CModelTreeWidgetItem* pModelMeshItem);
    bool loadVertices(RVL::Representation::ModelBased::GeometricGraph::Base::CModelMesh* pModelMesh, CModelTreeWidgetItem* pModelMeshItem);

    //VISUALIZATION MANAGMENT
    bool loadVisualization(RVL::Representation::ModelBased::GeometricGraph::Base::CModelMultipleMesh* pModelMultipleMesh);

    void ResetAll();

    //DYNAMIC DATA
    //SelectionSource m_SelectionSource;
    RVL::Representation::ModelBased::GeometricGraph::Base::CModelMultipleMesh* m_pModelMultipleMeshes;
    RVL::Representation::ModelBased::GeometricGraph::Visualization::OpenInventor::COpenInventorModelMesh* m_pSelectedpOpenInventorModelMesh;

    //INTERNAL RESOURCES
    QIcon m_ElementIcons[_TOTAL_ELEMENT_ICONS_ * 2];
    QIcon m_SetsIcons[_TOTAL_SETS_ICONS_];

    //RVL OPEN INVENTOR COMPONENTS
    RVL::Visualization::_3D::SceneGraph::OpenInventor::Extensions::SoQtExtendedExaminerViewer* m_pViewerMain;
    RVL::Visualization::_3D::SceneGraph::OpenInventor::Extensions::SoQtExtendedExaminerViewer* m_pViewerSelection;

    SoSeparator* m_pMainGlobalProps;
    SoSeparator* m_pSecondaryGlobalProps;
    SoSeparator* m_pTLCSSeparator;
    SoTransform* m_pTransformation;
    SoSwitch* m_pMainGlobalPropsSwitch;
    SoSwitch* m_pSecondaryGlobalPropsSwitch;
    RVL::Visualization::_3D::SceneGraph::OpenInventor::Objects::COpenInventorTexturedRectangle* m_pFloor;
    RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame* m_pUCS;
    RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame* m_pTLCS;

    bool m_IgnoreEvent;
    std::vector< QString > m_Labels;
    std::vector< QCheckBox* > m_Enablers;
    std::vector< QCheckBox* > m_Indexers;
    std::vector< QCheckBox* > m_Visualizers;
    std::vector< QCheckBox* > m_SaliencyManagers;
    XmlHighlighter* m_pXmlHighlighters[2];
    QString m_CurrentFile;
};

