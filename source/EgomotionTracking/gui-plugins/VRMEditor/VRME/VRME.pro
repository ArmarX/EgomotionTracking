#-------------------------------------------------
#
# Project created by QtCreator 2014-08-13T09:35:49
#
#-------------------------------------------------

QT       += core gui

TARGET = VRME
TEMPLATE = app


SOURCES += main.cpp\
        vrme.cpp \
    modeltreewidgetitem.cpp \
    xmlhighlighter.cpp \
    Rendering/VisibleScreenRegion.cpp \
    Rendering/StereoCalibrationGL.cpp \
    Rendering/RenderingModel.cpp \
    Rendering/ParametricLineRenderer.cpp \
    Rendering/OpenGLCalculations.cpp \
    Rendering/FaceTriangle.cpp

HEADERS  += vrme.h \
    modeltreewidgetitem.h \
    RVL.h \
    xmlhighlighter.h \
    Rendering/VisibleScreenRegion.h \
    Rendering/StereoCalibrationGL.h \
    Rendering/RenderingModel.h \
    Rendering/ParametricLineRenderer.h \
    Rendering/OpenGLCalculations.h \
    Rendering/Line.h \
    Rendering/FaceTriangle.h

FORMS    += vrme.ui

LIBS += -L/home/gonzalez/IVT/build/lib
INCLUDEPATH += /home/gonzalez/IVT/src


unix:!macx:!symbian: LIBS += -L$$PWD/../../Phd/Debug/ -lRVL -lCoin -lSoQt -livt

INCLUDEPATH += $$PWD/../../Phd/src
DEPENDPATH += $$PWD/../../Phd/src

unix:!macx:!symbian: PRE_TARGETDEPS += $$PWD/../../Phd/Debug/libRVL.a

RESOURCES += \
    VRME_Resources.qrc

