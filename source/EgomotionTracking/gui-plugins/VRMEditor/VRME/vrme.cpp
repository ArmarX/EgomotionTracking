#include "vrme.h"
#include <EgomotionTracking/gui-plugins/VRMEditor/VRME/ui_vrme.h>

#include <QtGui>
#include <QMenu>

using namespace RVL::Representation::ModelBased::GeometricGraph::Loaders;
using namespace RVL::Representation::ModelBased::GeometricGraph::Loaders::OpenInventor;
using namespace RVL::Representation::ModelBased::GeometricGraph::Loaders::SimpleCollada;
using namespace RVL::Representation::ModelBased::GeometricGraph::Base;
using namespace RVL::Visualization::_3D::SceneGraph::OpenInventor;
using namespace RVL::Visualization::_3D::SceneGraph::OpenInventor::Extensions;
using namespace RVL::Representation::ModelBased::GeometricGraph::Visualization::OpenInventor;
using namespace RVL::Processing;
using namespace RVL::Xml;
using namespace RVL;

//#define AUTO_LOAD

VRME::VRME(QWidget* parent) :
    QWidget(parent),
    ISelectionDispatcher(),
    IOperationProgressInterface(),
    ui(new Ui::VRME),
    m_pModelMultipleMeshes(nullptr),
    m_pSelectedpOpenInventorModelMesh(nullptr),
    m_pViewerMain(nullptr),
    m_pViewerSelection(nullptr)
{
    ui->setupUi(this);

    ExtendedSetupUI();

#ifdef AUTO_LOAD
    QString fileName("/home/gonzalez/Desktop/TableModels/Tisch_David/square_table.iv");
    addInformationToLog(tr("Loading file: ") + fileName);
    COpenInventorModelLoader Loader;
    CModelMultipleMesh* pModelMultipleMesh = Loader.LoadFromFile(fileName.toStdString());

    if (pModelMultipleMesh)
    {
        m_pModelMultipleMeshes = pModelMultipleMesh;
        loadVisualization(m_pModelMultipleMeshes);
        ui->projectFilelineEdit->setText(fileName);
        ui->saveProjectFilepushButton->setEnabled(true);
        addInformationToLog(tr("Load Succesfull: ") + fileName);
        loadMultipleMeshToTree(m_pModelMultipleMeshes, true);
    }
    else
    {
        ui->projectFilelineEdit->setText("");
        ui->saveProjectFilepushButton->setEnabled(false);
        addInformationToLog(tr("Load Failure: ") + fileName);
    }

#endif

}

VRME::~VRME()
{
    delete ui;

    //
    //  Note:
    //  This are importan lines code. Destroy all instances of SoQtExtendedExaminerViewer
    //  This is particular importan in the context of pluging in ArmarX
    //

    m_pViewerMain->ClearNodes();
    m_pViewerSelection->ClearNodes();
    delete m_pFloor;
    delete m_pUCS;
    delete m_pTLCS;
    m_pMainGlobalProps->unref();
    m_pSecondaryGlobalProps->unref();

    delete m_pViewerMain;
    delete m_pViewerSelection;
}

void VRME::ExtendedSetupUI()
{
    //Load Icons
    QStringList InconFiles;
    InconFiles.append(QString(":/VertexIcon.png"));
    InconFiles.append(QString(":/OrientedVertexIcon.png"));
    InconFiles.append(QString(":/EdgeIcon.png"));
    InconFiles.append(QString(":/FaceIcon.png"));
    InconFiles.append(QString(":/ComposedFaceIcon.png"));
    InconFiles.append(QString(":/MeshIcon.png"));
    InconFiles.append(QString(":/MultipleMeshIcon.png"));
    InconFiles.append(QString(":/DynamicMeshIcon.png"));

    for (int i = 0; i < InconFiles.size(); ++i)
    {
        m_ElementIcons[i] = QIcon(InconFiles.at(i));
    }

    InconFiles.clear();
    InconFiles.append(QString(":/VertexSetIcon.png"));
    InconFiles.append(QString(":/DynamicVertexConfigurationsSetIcon.png"));
    InconFiles.append(QString(":/OrientedVertexSetIcon.png"));
    InconFiles.append(QString(":/DynamicOrientedVertexConfigurationsSetIcon.png"));
    InconFiles.append(QString(":/EdgeSetIcon.png"));
    InconFiles.append(QString(":/FaceSetIcon.png"));
    InconFiles.append(QString(":/ComposedFaceSetIcon.png"));
    InconFiles.append(QString(":/MultipleMeshSetIcon.png"));

    for (int i = 0; i < _TOTAL_SETS_ICONS_; ++i)
    {
        m_SetsIcons[i] = QIcon(InconFiles.at(i));
    }


    m_Labels.push_back(QString("%1 - Vertices"));
    m_Labels.push_back(QString("%1 - Oriented Vertices"));
    m_Labels.push_back(QString("%1 - Edges"));
    m_Labels.push_back(QString("%1 - Faces"));
    m_Labels.push_back(QString("%1 - Composed Faces"));
    m_Labels.push_back(QString("%1 - Meshes"));
    m_Labels.push_back(QString("%1 - Multi-Meshes"));

    m_Enablers.push_back(ui->enabledVerticeCountCheckBox);
    m_Enablers.push_back(ui->enabledOrientedVerticesCountCheckBox);
    m_Enablers.push_back(ui->enabledFacesCountCheckBox);
    m_Enablers.push_back(ui->enabledFacesCountCheckBox);
    m_Enablers.push_back(ui->enabledComposedFacesCountCheckBox);
    m_Enablers.push_back(ui->enabledMeshesCountCheckBox);
    m_Enablers.push_back(ui->enabledMultipleMeshesCountCheckBox);

    m_Indexers.push_back(ui->indexableVerticeCountCheckBox);
    m_Indexers.push_back(ui->indexableOrientedVerticesCountCheckBox);
    m_Indexers.push_back(ui->indexableEdgesCountCheckBox);
    m_Indexers.push_back(ui->indexableFacesCountCheckBox);
    m_Indexers.push_back(ui->indexableComposedFacesCountCheckBox);
    m_Indexers.push_back(ui->indexableMeshesCountCheckBox);
    m_Indexers.push_back(ui->indexableMultipleMeshesCountCheckBox);

    m_Visualizers.push_back(ui->visibilityVerticeCountCheckBox);
    m_Visualizers.push_back(ui->visibilityOrientedVerticesCountCheckBox);
    m_Visualizers.push_back(ui->visibilityEdgesCountCheckBox);
    m_Visualizers.push_back(ui->visibilityFacesCountCheckBox);
    m_Visualizers.push_back(ui->visibilityComposedFacesCountCheckBox);
    m_Visualizers.push_back(ui->visibilityMeshesCountCheckBox);
    m_Visualizers.push_back(ui->visibilityMultipleMeshesCountCheckBox);

    m_SaliencyManagers.push_back(ui->saliencyWeightingVerticeCountCheckBox);
    m_SaliencyManagers.push_back(ui->saliencyWeightingOrientedVerticesCountCheckBox);
    m_SaliencyManagers.push_back(ui->saliencyWeightingFacesCountCheckBox);
    m_SaliencyManagers.push_back(ui->saliencyWeightingFacesCountCheckBox);
    m_SaliencyManagers.push_back(ui->saliencyWeightingComposedFacesCountCheckBox);
    m_SaliencyManagers.push_back(ui->saliencyWeightingMeshesCountCheckBox);
    m_SaliencyManagers.push_back(ui->saliencyWeightingMultipleMeshesCountCheckBox);

    QWidget* pMainWidgetParent = SoQt::getTopLevelWidget();

    QWidget* pViewerMainSoQtWidget = nullptr;

    if (pMainWidgetParent)
    {
        pViewerMainSoQtWidget = new QWidget(pMainWidgetParent);
    }
    else
    {
        pViewerMainSoQtWidget = new QWidget();
        SoQt::init(pViewerMainSoQtWidget);
    }


    //QWidget* pViewerMainSoQtWidget = new QWidget();
    //SoQt::init(pViewerMainSoQtWidget);
    m_pViewerMain = new SoQtExtendedExaminerViewer(pViewerMainSoQtWidget, nullptr);
    QGridLayout* pViewerMainGridLayout = new QGridLayout(ui->mainSpacePortFrame);
    pViewerMainGridLayout->setSpacing(0);
    pViewerMainGridLayout->setContentsMargins(0, 0, 0, 0);
    pViewerMainGridLayout->setObjectName(QString::fromUtf8("ViewerMainGridLayout"));
    pViewerMainGridLayout->addWidget(pViewerMainSoQtWidget);
    m_pViewerMain->SetSelectionDispatcher(this);
    m_pViewerMain->SetTransparencyType(SoGLRenderAction::DELAYED_BLEND);
    m_pViewerMain->SetBackgroundColor(128, 128, 128);

    QWidget* pViewerSecondarySoQtWidget = nullptr;

    if (pMainWidgetParent)
    {
        pViewerSecondarySoQtWidget = new QWidget(pMainWidgetParent);
    }
    else
    {
        pViewerSecondarySoQtWidget = new QWidget();
    }

    m_pViewerSelection = new SoQtExtendedExaminerViewer(pViewerSecondarySoQtWidget, nullptr);
    QGridLayout* pViewerSecondaryGridLayout = new QGridLayout(ui->secondarySpacePortFrame);
    pViewerSecondaryGridLayout->setSpacing(0);
    pViewerSecondaryGridLayout->setContentsMargins(0, 0, 0, 0);
    pViewerSecondaryGridLayout->setObjectName(QString::fromUtf8("ViewerSeconaryGridLayout"));
    pViewerSecondaryGridLayout->addWidget(pViewerSecondarySoQtWidget);
    m_pViewerSelection->SetSelectionDispatcher(this);
    m_pViewerSelection->SetTransparencyType(SoGLRenderAction::DELAYED_BLEND);
    m_pViewerSelection->SetBackgroundColor(128, 128, 128);

    m_pViewerMain->DisplayingSelection(false);
    m_pViewerSelection->DisplayingSelection(false);
    m_IgnoreEvent = false;

    m_pXmlHighlighters[0] = new XmlHighlighter(ui->selectionInfoTextEdit);
    m_pXmlHighlighters[1] = new XmlHighlighter(ui->queryTextEdit);

    m_pMainGlobalProps = new SoSeparator;
    m_pMainGlobalProps->ref();

    m_pSecondaryGlobalProps = new SoSeparator;
    m_pSecondaryGlobalProps->ref();

    m_pMainGlobalPropsSwitch = new SoSwitch;
    m_pMainGlobalProps->addChild(m_pMainGlobalPropsSwitch);

    m_pSecondaryGlobalPropsSwitch = new SoSwitch;
    m_pSecondaryGlobalProps->addChild(m_pSecondaryGlobalPropsSwitch);



    //const std::string& FileName, const float LenghtX, const float LenghtY, const float OffsetZ, const float PatternRepetitionsX, const float PatternRepetitionsY
    const std::string FileName("/home/gonzalez/Projects/Software/VisualRecognitionModelEditor/VRME/Grid.png");
    m_pFloor = new RVL::Visualization::_3D::SceneGraph::OpenInventor::Objects::COpenInventorTexturedRectangle(FileName, 20000.0f, 20000.0f, 0.0f, 20.0f, 20.0f);

    std::map<RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame::SubElement, std::string> Labels;

    m_pUCS = new RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame();
    m_pUCS->SetAxesLength(1000.0f);
    m_pUCS->SetConeLength(50.0f);
    m_pUCS->SetConeWidth(25.0f);
    m_pUCS->SetLineWidth(2.0);

    Labels[RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame::eAxisXP] = std::string("X");
    Labels[RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame::eAxisXN] = std::string("-X");
    Labels[RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame::eAxisYP] = std::string("Y");
    Labels[RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame::eAxisYN] = std::string("-Y");
    Labels[RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame::eAxisZP] = std::string("Z");
    Labels[RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame::eAxisZN] = std::string("-Z");
    m_pUCS->SetLabels(Labels);
    Labels.clear();


    m_pTLCS = new RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame();
    m_pTLCS->SetAxesLength(500.0f);
    m_pTLCS->SetConeLength(25.0f);
    m_pTLCS->SetConeWidth(12.5f);
    m_pTLCS->SetLineWidth(1.0);

    Labels[RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame::eAxisXP] = std::string("x'");
    Labels[RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame::eAxisXN] = std::string("-x'");
    Labels[RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame::eAxisYP] = std::string("y'");
    Labels[RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame::eAxisYN] = std::string("-y'");
    Labels[RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame::eAxisZP] = std::string("z'");
    Labels[RVL::Visualization::_3D:: SceneGraph::OpenInventor::Frames::COpenInventorKinematicFrame::eAxisZN] = std::string("-z'");
    m_pTLCS->SetLabels(Labels);
    m_pTLCS->GetBaseSwitch()->whichChild.setValue(SO_SWITCH_NONE);

    m_pTLCSSeparator = new SoSeparator;
    m_pMainGlobalProps->addChild(m_pTLCSSeparator);
    m_pSecondaryGlobalProps->addChild(m_pTLCSSeparator);

    m_pTransformation = new SoTransform;
    m_pTLCSSeparator->addChild(m_pTransformation);
    m_pTLCSSeparator->addChild(m_pTLCS->GetBaseSwitch());


    m_pMainGlobalPropsSwitch->addChild(m_pFloor->GetBaseSwitch());
    m_pMainGlobalPropsSwitch->addChild(m_pUCS->GetBaseSwitch());
    m_pMainGlobalPropsSwitch->whichChild.setValue(SO_SWITCH_ALL);

    m_pSecondaryGlobalPropsSwitch->addChild(m_pFloor->GetBaseSwitch());
    m_pSecondaryGlobalPropsSwitch->addChild(m_pUCS->GetBaseSwitch());
    m_pSecondaryGlobalPropsSwitch->whichChild.setValue(SO_SWITCH_ALL);

    m_pViewerMain->AddNode(m_pMainGlobalProps);
    m_pViewerMain->ViewAll();

    m_pViewerSelection->AddNode(m_pSecondaryGlobalProps);
    m_pViewerSelection->ViewAll();
}

//EVENT HANDLERS

void VRME::on_loadProjectFilepushButton_clicked()
{
    QFileDialog openFileDialog(this, tr("Select Project File..."));
    openFileDialog.setAcceptMode(QFileDialog::AcceptOpen);
    openFileDialog.setFileMode(QFileDialog::ExistingFile);
    openFileDialog.setNameFilter(tr("Model File (*.iv *.xml *.dae)"));
    openFileDialog.setViewMode(QFileDialog::Detail);

    if (openFileDialog.exec())
    {
        QStringList fileNames = openFileDialog.selectedFiles();
        QString fileName = fileNames.at(0);

        addInformationToLog(tr("Loading file: ") + fileName);

        ResetAll();

        CModelMultipleMesh* pModelMultipleMesh = nullptr;

        if (fileName.endsWith(QString(".xml"), Qt::CaseInsensitive))
        {
            CXmlModelLoader XMLLoader;
            pModelMultipleMesh = XMLLoader.LoadFromFile(fileName.toStdString(), CModelLoader::eMillimeters, this);
        }
        else if (fileName.endsWith(QString(".dae"), Qt::CaseInsensitive))
        {
            SimpleColladaModelLoader colladaLoader;
            pModelMultipleMesh = colladaLoader.LoadFromFile(fileName.toStdString(), CModelLoader::eMillimeters, this);
        }
        else
        {
            const Real VertexPointDistanceTolerance = Real(0.001);
            const Real NormalOrientationTolerance = Real(0.999847695);
            const Real MinimalEdgeLength = VertexPointDistanceTolerance * Real(2);
            COpenInventorModelLoader OILoader(VertexPointDistanceTolerance, NormalOrientationTolerance, MinimalEdgeLength);
            pModelMultipleMesh = OILoader.LoadFromFile(fileName.toStdString(), CModelLoader::eMillimeters, this);
        }

        if (pModelMultipleMesh)
        {
            m_pModelMultipleMeshes = pModelMultipleMesh;
            loadVisualization(m_pModelMultipleMeshes);
            loadMultipleMeshToTree(m_pModelMultipleMeshes, true);
            ui->projectFilelineEdit->setText(fileName);
            ui->saveProjectFilepushButton->setEnabled(true);
            addInformationToLog(tr("Load Succesfull: ") + fileName);
            m_CurrentFile = fileName;

            if (fileName.endsWith(QString(".xml"), Qt::CaseInsensitive))
            {
                ui->saveCurrentFilePushButton->setEnabled(true);
            }
        }
        else
        {
            ui->projectFilelineEdit->setText("");
            ui->saveProjectFilepushButton->setEnabled(false);
            addInformationToLog(tr("Load Failure: ") + fileName);
            m_CurrentFile.clear();
            ui->saveCurrentFilePushButton->setEnabled(false);
        }
    }
}

void VRME::on_saveProjectFilepushButton_clicked()
{
    QFileDialog saveFileDialog(this, tr("Save Project File..."));
    saveFileDialog.setAcceptMode(QFileDialog::AcceptSave);
    saveFileDialog.setFileMode(QFileDialog::AnyFile);
    saveFileDialog.setNameFilter(tr("Model File (*.xml)"));
    saveFileDialog.setViewMode(QFileDialog::Detail);

    if (saveFileDialog.exec())
    {
        QStringList fileNames = saveFileDialog.selectedFiles();
        QString fileName = fileNames.at(0);
        addInformationToLog(tr("Saving file: ") + fileName);

        if (m_pModelMultipleMeshes)
        {
            if (m_pModelMultipleMeshes->SaveToFile(fileName.toLatin1().data(), true))
            {
                addInformationToLog(tr("Save Succesfull: ") + fileName);
                m_CurrentFile = fileName;
            }
            else
            {
                addInformationToLog(tr("Save Failure: ") + fileName);
            }
        }
    }
}

//HELPERS

void VRME::updateVisibilityInViewerMain()
{
    if (m_pModelMultipleMeshes)
    {
        bool Displaying[5][3];
        memset(Displaying, 0, sizeof(bool) * 15);
        Displaying[CModelElement::eVertex][CModelElement::eStaticOccluded] = ui->verticesOccludedDisplaySwitchCheckBox->isChecked();
        Displaying[CModelElement::eVertex][CModelElement::eStaticVisible] = ui->verticesVisibleDisplaySwitchCheckBox->isChecked();
        Displaying[CModelElement::eVertex][CModelElement::eDynamicVisible] = ui->verticesDynamicDisplaySwitchCheckBox->isChecked();
        Displaying[CModelElement::eEdge][CModelElement::eStaticOccluded] = ui->edgesOccludedDisplaySwitchCheckBox->isChecked();
        Displaying[CModelElement::eEdge][CModelElement::eStaticVisible] = ui->edgesVisibleDisplaySwitchCheckBox->isChecked();
        Displaying[CModelElement::eEdge][CModelElement::eDynamicVisible] = ui->edgesDynamicDisplaySwitchCheckBox->isChecked();
        Displaying[CModelElement::eFace][CModelElement::eStaticOccluded] = ui->facesOccludedDisplaySwitchCheckBox->isChecked();
        Displaying[CModelElement::eFace][CModelElement::eStaticVisible] = ui->facesVisibleDisplaySwitchCheckBox->isChecked();
        Displaying[CModelElement::eFace][CModelElement::eDynamicVisible] = ui->facesDynamicDisplaySwitchCheckBox->isChecked();
        Displaying[CModelElement::eComposedFace][CModelElement::eStaticOccluded] = ui->composedFacesOccludedDisplaySwitchCheckBox->isChecked();
        Displaying[CModelElement::eComposedFace][CModelElement::eStaticVisible] = ui->composedFacesVisibleDisplaySwitchCheckBox->isChecked();
        Displaying[CModelElement::eComposedFace][CModelElement::eDynamicVisible] = ui->composedFacesDynamicDisplaySwitchCheckBox->isChecked();
        const std::map<int, CModelMesh*>& Meshes = m_pModelMultipleMeshes->GetMeshes();
        std::map<int, CModelMesh*>::const_iterator EndMeshes = Meshes.end();

        for (std::map<int, CModelMesh*>::const_iterator pKeymesh = Meshes.begin(); pKeymesh != EndMeshes; ++pKeymesh)
        {
            COpenInventorModelMesh* pOpenInventorModelMesh = (COpenInventorModelMesh*)pKeymesh->second->GetElementData();

            for (int VisibilityType = CModelElement::eStaticOccluded; VisibilityType <= CModelElement::eDynamicVisible; ++VisibilityType)
            {
                pOpenInventorModelMesh->SetVerticesSetVisible(CModelElement::Visibility(VisibilityType), Displaying[CModelElement::eVertex][VisibilityType]);
                pOpenInventorModelMesh->SetEdgesSetVisible(CModelElement::Visibility(VisibilityType), Displaying[CModelElement::eEdge][VisibilityType]);
                pOpenInventorModelMesh->SetFaceSetVisible(CModelElement::Visibility(VisibilityType), Displaying[CModelElement::eFace][VisibilityType]);
                pOpenInventorModelMesh->SetMultipleFacesSetVisible(CModelElement::Visibility(VisibilityType), Displaying[CModelElement::eComposedFace][VisibilityType]);
            }
        }
    }
}

void VRME::clearTransformationPanel()
{
    //---------------------------------------------------------------
    //  Translation
    //---------------------------------------------------------------
    ui->pushButtonCreateTranslationTransformation->setEnabled(false);
    ui->pushButtonModifyTranslationTransformation->setEnabled(false);
    ui->pushButtonDestroyTranslationTransformation->setEnabled(false);

    ui->vertexABCheckBox->setChecked(false);
    ui->vertexABCDCheckBox->setChecked(false);
    ui->edgeACheckBox->setChecked(false);
    ui->edgeABCheckBox->setChecked(false);

    ui->vertexABCheckBox->setEnabled(false);
    ui->vertexABCDCheckBox->setEnabled(false);
    ui->edgeACheckBox->setEnabled(false);
    ui->edgeABCheckBox->setEnabled(false);

    ui->SourceComboBoxA->setEnabled(false);
    ui->SourceComboBoxB->setEnabled(false);
    ui->SourceComboBoxC->setEnabled(false);
    ui->SourceComboBoxD->setEnabled(false);
    ui->SourceComboBoxA->clear();
    ui->SourceComboBoxB->clear();
    ui->SourceComboBoxC->clear();
    ui->SourceComboBoxD->clear();

    ui->transformationLabelLineEditTranslation->setEnabled(false);
    ui->minimalDisplacementLineEdit->setEnabled(false);
    ui->maximalDisplacementLineEdit->setEnabled(false);
    ui->defaultDisplacementLineEdit->setEnabled(false);
    ui->transformationLabelLineEditTranslation->clear();
    ui->minimalDisplacementLineEdit->clear();
    ui->maximalDisplacementLineEdit->clear();
    ui->defaultDisplacementLineEdit->clear();

    ui->horizontalSliderTranslationDoF->setEnabled(false);
    ui->horizontalSliderTranslationDoF->setRange(0, 1);
    ui->horizontalSliderTranslationDoF->setValue(0);

    ui->meshTranslationTab->setEnabled(false);

    //---------------------------------------------------------------
    //  Rotation
    //---------------------------------------------------------------
    ui->pushButtonCreatRotationTransformation->setEnabled(false);
    ui->pushButtonModifyRotationTransformation->setEnabled(false);
    ui->pushButtonDestroyRotationTransformation->setEnabled(false);

    ui->vertexABCheckBoxRotation->setChecked(false);
    ui->vertexABCDCheckBoxRotation->setChecked(false);
    ui->edgeACheckBoxRotation->setChecked(false);
    ui->edgeABCheckBoxRotation->setChecked(false);

    ui->vertexABCheckBoxRotation->setEnabled(false);
    ui->vertexABCDCheckBoxRotation->setEnabled(false);
    ui->edgeACheckBoxRotation->setEnabled(false);
    ui->edgeABCheckBoxRotation ->setEnabled(false);

    ui->SourceComboBoxRotationA->setEnabled(false);
    ui->SourceComboBoxRotationB->setEnabled(false);
    ui->SourceComboBoxRotationC->setEnabled(false);
    ui->SourceComboBoxRotationD->setEnabled(false);

    ui->SourceComboBoxRotationA->clear();
    ui->SourceComboBoxRotationB->clear();
    ui->SourceComboBoxRotationC->clear();
    ui->SourceComboBoxRotationD->clear();

    ui->transformationLabelLineEditRotation->setEnabled(false);
    ui->minimalRotationLineEdit->setEnabled(false);
    ui->maximalRotationLineEdit->setEnabled(false);
    ui->defaultRotationLineEdit->setEnabled(false);

    ui->transformationLabelLineEditRotation->clear();
    ui->minimalRotationLineEdit->clear();
    ui->maximalRotationLineEdit->clear();
    ui->defaultRotationLineEdit->clear();

    ui->horizontalSliderRotationDoF->setEnabled(false);
    ui->horizontalSliderRotationDoF->setRange(0, 1);
    ui->horizontalSliderRotationDoF->setValue(0);

    ui->meshRotationTab->setEnabled(false);

    //---------------------------------------------------------------
    //  Global Aligment
    //---------------------------------------------------------------

    ui->GlobalAligmentVerticesSourcegroupBox->setEnabled(false);

    ui->GlobalAligmentSourceLabelTranslationBase->setEnabled(false);
    ui->GlobalAligmentSourceComboBoxBase->setEnabled(false);
    ui->GlobalAligmentSourceComboBoxBase->clear();

    ui->GlobalAligmentSourceLabelTranslationAxisX->setEnabled(false);
    ui->GlobalAligmentSourceComboBoxAxisX->setEnabled(false);
    ui->GlobalAligmentSourceComboBoxAxisX->clear();
    ui->GlobalAligmentSourceChechBoxBoxAxisX->setEnabled(false);
    ui->GlobalAligmentSourceChechBoxBoxAxisX->setChecked(false);

    ui->GlobalAligmentSourceLabelTranslationAxisY->setEnabled(false);
    ui->GlobalAligmentSourceComboBoxAxisY->setEnabled(false);
    ui->GlobalAligmentSourceComboBoxAxisY->clear();
    ui->GlobalAligmentSourceChechBoxBoxAxisY->setEnabled(false);
    ui->GlobalAligmentSourceChechBoxBoxAxisY->setChecked(false);

    ui->GlobalAligmentSourceLabelTranslationAxisZ->setEnabled(false);
    ui->GlobalAligmentSourceComboBoxAxisZ->setEnabled(false);
    ui->GlobalAligmentSourceComboBoxAxisZ->clear();
    ui->GlobalAligmentSourceChechBoxBoxAxisZ->setEnabled(false);
    ui->GlobalAligmentSourceChechBoxBoxAxisZ->setChecked(false);

    ui->GloablAligmentGenerateFramepushButton->setEnabled(false);
    ui->DisplayFrameAligmentcheckBox->setEnabled(false);
    ui->DisplayFrameAligmentcheckBox->setChecked(false);

    ui->GloablAligmentTransfromTextEdit->setEnabled(false);
    ui->GloablAligmentTransfromTextEdit->clear();
    ui->GloablAligmentApplyFramepushButton->setEnabled(false);

    //---------------------------------------------------------------
    //  Transformation
    //---------------------------------------------------------------
    ui->meshTransformationTabWidget->setEnabled(false);

    //---------------------------------------------------------------
    //  Assigment
    //---------------------------------------------------------------
    ui->assigmentTransformationMeshTab->setEnabled(false);
}

void VRME::updateTransformationPanel()
{
    clearTransformationPanel();

    QList<QTreeWidgetItem*> SelectedItems = ui->modelTreeWidget->selectedItems();
    QList<CModelTreeWidgetItem*> VertexItems = FilterItems(SelectedItems, 0X1); //eVertex = 0X1
    QList<CModelTreeWidgetItem*> EdgeItems = FilterItems(SelectedItems, 0X4); //eEdge = 0X4

    const int TotalVertices = VertexItems.size();
    const int TotalEdges = EdgeItems.size();
    const bool ActivateSelection = (TotalVertices >= 2) || TotalEdges;

    if (m_pSelectedpOpenInventorModelMesh)
    {
        ui->meshTransformationTabWidget->setEnabled(true);

        CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
        CModelTransformation* pGenericTransformation = pModelMesh->GetTransformation();

        if (pGenericTransformation)
        {
            ui->assigmentTransformationMeshTab->setEnabled(true);
            ui->removeTransformationPushButton->setEnabled(true);

            switch (pGenericTransformation->GetTransformationType())
            {
                case CModelTransformation::eTranslation:
                {
                    ui->meshTranslationTab->setEnabled(true);
                    ui->transformationLabelLineEditTranslation->setText(QString(pGenericTransformation->GetLabel().c_str()));
                    ui->transformationLabelLineEditTranslation->setEnabled(true);
                    ui->pushButtonCreateTranslationTransformation->setEnabled(false);
                    ui->pushButtonModifyTranslationTransformation->setEnabled(false);
                    ui->pushButtonDestroyTranslationTransformation->setEnabled(true);
                    ui->minimalDisplacementLineEdit->setEnabled(true);
                    ui->maximalDisplacementLineEdit->setEnabled(true);
                    ui->defaultDisplacementLineEdit->setEnabled(true);
                    ui->horizontalSliderTranslationDoF->setEnabled(true);
                    CModelTranslation* pModelTranslation = dynamic_cast<CModelTranslation*>(pGenericTransformation);
                    const Real MinimalDisplacement = pModelTranslation->GetMinimalDisplacement();
                    const Real MaximalDisplacement = pModelTranslation->GetMaximalDisplacement();
                    const Real DefaultDisplacement = pModelTranslation->GetDefaultDisplacement();
                    const Real CurrentDisplacement = pModelTranslation->GetCurrentDisplacement();
                    ui->minimalDisplacementLineEdit->setText(QString("%1").arg(MinimalDisplacement));
                    ui->maximalDisplacementLineEdit->setText(QString("%1").arg(MaximalDisplacement));
                    ui->defaultDisplacementLineEdit->setText(QString("%1").arg(DefaultDisplacement));
                    ui->horizontalSliderTranslationDoF->setRange(MinimalDisplacement, MaximalDisplacement);
                    ui->horizontalSliderTranslationDoF->setValue(CurrentDisplacement);
                    const CModelTransformation::GeometricCueSource& CueSource = pModelTranslation->GetGeometricCueSource();

                    switch (CueSource.m_CueType)
                    {
                        case CModelTransformation::GeometricCueSource::eVertexAB:
                            ui->SourceComboBoxA->addItem(QString("[Vertex:%1]").arg(CueSource.m_Ids[0]));
                            ui->SourceComboBoxB->addItem(QString("[Vertex:%1]").arg(CueSource.m_Ids[1]));
                            ui->SourceComboBoxA->setCurrentIndex(0);
                            ui->SourceComboBoxB->setCurrentIndex(0);
                            ui->vertexABCheckBox->setChecked(true);
                            break;

                        case CModelTransformation::GeometricCueSource::eVertexABC:
                            break;

                        case CModelTransformation::GeometricCueSource::eVertexABCD:
                            break;

                        case CModelTransformation::GeometricCueSource::eEdgeA:
                            ui->SourceComboBoxA->addItem(QString("[Edge:%1]").arg(CueSource.m_Ids[0]));
                            ui->SourceComboBoxA->setCurrentIndex(0);
                            ui->edgeACheckBox->setChecked(true);
                            break;

                        case CModelTransformation::GeometricCueSource::EdgeAB:
                            break;

                        default:
                            break;
                    }
                }
                break;

                case CModelTransformation::eRotation:
                {
                    ui->meshRotationTab->setEnabled(true);
                    ui->transformationLabelLineEditRotation->setText(QString(pGenericTransformation->GetLabel().c_str()));
                    ui->transformationLabelLineEditRotation->setEnabled(true);
                    ui->pushButtonCreatRotationTransformation->setEnabled(false);
                    ui->pushButtonModifyRotationTransformation->setEnabled(false);
                    ui->pushButtonDestroyRotationTransformation->setEnabled(true);
                    ui->minimalRotationLineEdit->setEnabled(true);
                    ui->maximalRotationLineEdit->setEnabled(true);
                    ui->defaultRotationLineEdit->setEnabled(true);
                    ui->horizontalSliderRotationDoF->setEnabled(true);
                    CModelRotation* pModelRotation = dynamic_cast<CModelRotation*>(pGenericTransformation);
                    const Real MinimalRotationInDegrees = pModelRotation->GetMinimalRotation() * (180.0 / M_PI);
                    const Real MaximalRotationInDegrees = pModelRotation->GetMaximalRotation() * (180.0 / M_PI);
                    const Real DefaultRotationInDegrees = pModelRotation->GetDefaultRotation() * (180.0 / M_PI);
                    const Real CurrentRotationInDegrees = pModelRotation->GetCurrentRotation() * (180.0 / M_PI);
                    ui->minimalRotationLineEdit->setText(QString("%1").arg(MinimalRotationInDegrees));
                    ui->maximalRotationLineEdit->setText(QString("%1").arg(MaximalRotationInDegrees));
                    ui->defaultRotationLineEdit->setText(QString("%1").arg(DefaultRotationInDegrees));
                    ui->horizontalSliderRotationDoF->setRange(MinimalRotationInDegrees, MaximalRotationInDegrees);
                    ui->horizontalSliderRotationDoF->setValue(CurrentRotationInDegrees);
                    const CModelTransformation::GeometricCueSource& CueSource = pModelRotation->GetGeometricCueSource();

                    switch (CueSource.m_CueType)
                    {
                        case CModelTransformation::GeometricCueSource::eVertexAB:
                            ui->SourceComboBoxRotationA->addItem(QString("[Vertex:%1]").arg(CueSource.m_Ids[0]));
                            ui->SourceComboBoxRotationB->addItem(QString("[Vertex:%1]").arg(CueSource.m_Ids[1]));
                            ui->SourceComboBoxRotationA->setCurrentIndex(0);
                            ui->SourceComboBoxRotationB->setCurrentIndex(0);
                            ui->vertexABCheckBoxRotation->setChecked(true);
                            break;

                        case CModelTransformation::GeometricCueSource::eVertexABC:
                            break;

                        case CModelTransformation::GeometricCueSource::eVertexABCD:
                            break;

                        case CModelTransformation::GeometricCueSource::eEdgeA:
                            ui->SourceComboBoxRotationA->addItem(QString("[Edge:%1]").arg(CueSource.m_Ids[0]));
                            ui->SourceComboBoxRotationA->setCurrentIndex(0);
                            ui->edgeACheckBoxRotation->setChecked(true);
                            break;

                        case CModelTransformation::GeometricCueSource::EdgeAB:
                            break;

                        default:
                            break;
                    }
                }
                break;

                case CModelTransformation::eFrameToFrameRigid:
                case CModelTransformation::eGeneralRigid:
                    break;
            }
        }
        else
        {
            if (ActivateSelection)
            {
                ui->meshTransformationTabWidget->setEnabled(true);

                //---------------------------------------------------------------
                //  Translation
                //---------------------------------------------------------------
                ui->meshTranslationTab->setEnabled(true);
                ui->groupGeometricDirectionSourceBoxTranslation->setEnabled(true);
                ui->vertexABCheckBox->setEnabled(TotalVertices >= 2);
                ui->vertexABCDCheckBox->setEnabled(TotalVertices >= 4);
                ui->edgeACheckBox->setEnabled(TotalEdges >= 1);
                ui->edgeABCheckBox->setEnabled(TotalEdges >= 2);
                ui->vertexABCheckBox->setChecked(false);
                ui->vertexABCDCheckBox->setChecked(false);
                ui->edgeACheckBox->setChecked(false);
                ui->edgeABCheckBox->setChecked(false);
                ui->transformationLabelLineEditTranslation->setEnabled(true);
                ui->minimalDisplacementLineEdit->setEnabled(true);
                ui->maximalDisplacementLineEdit->setEnabled(true);
                ui->defaultDisplacementLineEdit->setEnabled(true);

                //---------------------------------------------------------------
                //  Rotation
                //---------------------------------------------------------------
                ui->meshRotationTab->setEnabled(true);
                ui->RotationgroupBox->setEnabled(true);
                ui->vertexABCheckBoxRotation->setEnabled(TotalVertices >= 2);
                ui->vertexABCDCheckBoxRotation->setEnabled(TotalVertices >= 4);
                ui->edgeACheckBoxRotation->setEnabled(TotalEdges >= 1);
                ui->edgeABCheckBoxRotation->setEnabled(TotalEdges >= 2);
                ui->vertexABCheckBoxRotation->setChecked(false);
                ui->vertexABCDCheckBoxRotation->setChecked(false);
                ui->edgeACheckBoxRotation->setChecked(false);
                ui->edgeABCheckBoxRotation->setChecked(false);
                ui->transformationLabelLineEditRotation->setEnabled(true);
                ui->minimalRotationLineEdit->setEnabled(true);
                ui->maximalRotationLineEdit->setEnabled(true);
                ui->defaultRotationLineEdit->setEnabled(true);

                //---------------------------------------------------------------
                //  Global Aligment
                //---------------------------------------------------------------

                if (TotalVertices == 4)
                {
                    ui->GlobalAligmentVerticesSourcegroupBox->setEnabled(true);

                    ui->GlobalAligmentSourceLabelTranslationBase->setEnabled(true);
                    ui->GlobalAligmentSourceComboBoxBase->setEnabled(true);

                    ui->GlobalAligmentSourceLabelTranslationAxisX->setEnabled(true);
                    ui->GlobalAligmentSourceComboBoxAxisX->setEnabled(true);
                    ui->GlobalAligmentSourceChechBoxBoxAxisX->setEnabled(true);

                    ui->GlobalAligmentSourceLabelTranslationAxisY->setEnabled(true);
                    ui->GlobalAligmentSourceComboBoxAxisY->setEnabled(true);
                    ui->GlobalAligmentSourceChechBoxBoxAxisY->setEnabled(true);

                    ui->GlobalAligmentSourceLabelTranslationAxisZ->setEnabled(true);
                    ui->GlobalAligmentSourceComboBoxAxisZ->setEnabled(true);
                    ui->GlobalAligmentSourceChechBoxBoxAxisZ->setEnabled(true);

                    ui->GloablAligmentGenerateFramepushButton->setEnabled(true);
                    ui->DisplayFrameAligmentcheckBox->setEnabled(true);

                    ui->GloablAligmentTransfromTextEdit->setEnabled(true);

                    for (int i = 0; i < TotalVertices; ++i)
                    {
                        QString Item = QString("[Vertex:%1]").arg(VertexItems.at(i)->GetGenericModelElement()->GetInstanceId());
                        ui->GlobalAligmentSourceComboBoxBase->addItem(Item);
                        ui->GlobalAligmentSourceComboBoxAxisX->addItem(Item);
                        ui->GlobalAligmentSourceComboBoxAxisY->addItem(Item);
                        ui->GlobalAligmentSourceComboBoxAxisZ->addItem(Item);
                    }

                    ui->GlobalAligmentSourceComboBoxBase->setCurrentIndex(0);
                    ui->GlobalAligmentSourceComboBoxAxisX->setCurrentIndex(1);
                    ui->GlobalAligmentSourceComboBoxAxisY->setCurrentIndex(2);
                    ui->GlobalAligmentSourceComboBoxAxisZ->setCurrentIndex(3);
                }
            }

            if (m_pModelMultipleMeshes->HasTransformations())
            {
                ui->meshTransformationTabWidget->setEnabled(true);
                ui->assigmentTransformationMeshTab->setEnabled(true);
            }
        }
    }
}

void VRME::addInformationToLog(const QString& Information)
{
    ui->sourcetextEdit->append(Information);
}

void VRME::setSelectionInformation(RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement* pModelElement)
{
    if (pModelElement)
    {
        ui->selectionInfoTextEdit->setText(QString(CXmlNode::FormatAddTabs(pModelElement->ToString()).c_str()));
    }
    else
    {
        ui->selectionInfoTextEdit->setText("");
    }
}

CModelTreeWidgetItem* VRME::FindItem(RVL::Representation::ModelBased::GeometricGraph::Base::CModelElement* pModelElement)
{
    if (pModelElement)
    {
        QList<QTreeWidgetItem*> Items = ui->modelTreeWidget->findItems(QString("%1").arg(pModelElement->GetInstanceId()), Qt::MatchRecursive, _INSTANCE_ID_COLUMN_);

        if (Items.size() == 1)
        {
            return dynamic_cast<CModelTreeWidgetItem*>(Items.at(0));
        }
    }

    return nullptr;
}

void VRME::SelectMesh(COpenInventorModelMesh* pOpenInventorModelMesh)
{
    UnselectMesh(false);

    if (pOpenInventorModelMesh)
    {
        m_pSelectedpOpenInventorModelMesh = pOpenInventorModelMesh;
        m_pSelectedpOpenInventorModelMesh->CreateManipulableObjects();
        m_pSelectedpOpenInventorModelMesh->SetBoundingBoxVisible(true);
        m_pSelectedpOpenInventorModelMesh->SetVisualizationMode(COpenInventorModelPrimitive::eEdition);

        CModelTreeWidgetItem* pItem = FindItem(m_pSelectedpOpenInventorModelMesh->GetModelElement());

        if (pItem)
        {
            pItem->setSelected(true);
            pItem->setExpanded(true);
        }

        m_pViewerSelection->ClearNodes();

        m_pViewerSelection->AddNode(m_pSelectedpOpenInventorModelMesh->GetManipulableObjectsSwitch());
        m_pViewerSelection->ViewAll();

        m_pViewerSelection->AddNode(m_pSecondaryGlobalProps);

        setSelectionInformation(pItem->GetGenericModelElement());
    }

    updateTransformationPanel();
    updateAvailableTransformations();
    ui->automaticComboBox->setEnabled(m_pSelectedpOpenInventorModelMesh);
    ui->automaticComboBox->setCurrentIndex(0);
    ui->automaticOperationApplyPushButton->setEnabled(false);
    ui->automaticLabel->setEnabled(m_pSelectedpOpenInventorModelMesh);
}

void VRME::UnselectMesh(const bool UpdateTransformationPanel)
{
    if (m_pSelectedpOpenInventorModelMesh)
    {
        m_pSelectedpOpenInventorModelMesh->ClearManipulableObjects();
        m_pSelectedpOpenInventorModelMesh->SetBoundingBoxVisible(false);
        m_pSelectedpOpenInventorModelMesh->SetVisualizationMode(COpenInventorModelPrimitive::eDefault);

        CModelTreeWidgetItem* pItem = FindItem(m_pSelectedpOpenInventorModelMesh->GetModelElement());

        if (pItem)
        {
            pItem->setSelected(false);
            pItem->setExpanded(false);
        }

        m_pViewerSelection->ClearNodes();
        m_pViewerSelection->AddNode(m_pSecondaryGlobalProps);
        m_pViewerSelection->ViewAll();

        m_pSelectedpOpenInventorModelMesh = nullptr;
    }

    if (UpdateTransformationPanel)
    {
        updateTransformationPanel();
        updateAvailableTransformations();
        ui->automaticComboBox->setEnabled(m_pSelectedpOpenInventorModelMesh);
        ui->automaticComboBox->setCurrentIndex(0);
        ui->automaticOperationApplyPushButton->setEnabled(false);
        ui->automaticLabel->setEnabled(m_pSelectedpOpenInventorModelMesh);
    }
}

QList<CModelTreeWidgetItem*> VRME::FilterItems(QList<QTreeWidgetItem*> InList, const uint TypeFlags)
{
    QList<CModelTreeWidgetItem*> OutList;
    const int TotalInItems = InList.size();

    for (int i = 0; i < TotalInItems; ++i)
    {
        CModelTreeWidgetItem* pItem = (CModelTreeWidgetItem*)InList.at(i);
        CModelElement* pModelElement = pItem->GetGenericModelElement();

        if (pModelElement && (TypeFlags & (0x1 << int(pModelElement->GetTypeId()))))
        {
            OutList.push_back(pItem);
        }
    }

    return OutList;
}

int VRME::GetActiveSelectionEnabledStatus(int TotalElementsByTypeId[7], int& TotalElements, const uint TypeFlags)
{
    memset(TotalElementsByTypeId, 0, sizeof(int) * 7);
    QList<CModelTreeWidgetItem*> SelectedItems = FilterItems(ui->modelTreeWidget->selectedItems(), TypeFlags);
    TotalElements = SelectedItems.size();

    if (!TotalElements)
    {
        return 0;
    }

    int TotalEnabled = 0, TotalDisabled = 0;

    for (int i = 0; i < TotalElements; ++i)
    {
        CModelElement* pModelElement = SelectedItems.at(i)->GetGenericModelElement();
        ++TotalElementsByTypeId[int(pModelElement->GetTypeId())];

        if (pModelElement->IsEnabled(false))
        {
            ++TotalEnabled;
        }
        else
        {
            ++TotalDisabled;
        }
    }

    if (TotalEnabled == TotalElements)
    {
        return 2;
    }

    if (TotalDisabled == TotalElements)
    {
        return 1;
    }

    return 3;
}

int VRME::GetActiveSelectionIndexableStatus(int TotalElementsByTypeId[7], int& TotalElements, const uint TypeFlags)
{
    memset(TotalElementsByTypeId, 0, sizeof(int) * 7);
    QList<CModelTreeWidgetItem*> SelectedItems = FilterItems(ui->modelTreeWidget->selectedItems(), TypeFlags);
    TotalElements = SelectedItems.size();

    if (!TotalElements)
    {
        return 0;
    }

    int TotalIndexable = 0, TotalNonIndexable = 0;

    for (int i = 0; i < TotalElements; ++i)
    {
        CModelElement* pModelElement = SelectedItems.at(i)->GetGenericModelElement();
        ++TotalElementsByTypeId[int(pModelElement->GetTypeId())];

        if (pModelElement->IsIndexable(false))
        {
            ++TotalIndexable;
        }
        else
        {
            ++TotalNonIndexable;
        }
    }

    if (TotalIndexable == TotalElements)
    {
        return 2;
    }

    if (TotalNonIndexable == TotalElements)
    {
        return 1;
    }

    return 3;
}

int VRME::GetActiveSelectionVisibilityStatus(int TotalElementsByTypeId[7], int& TotalElements, const uint TypeFlags)
{
    memset(TotalElementsByTypeId, 0, sizeof(int) * 7);
    QList<CModelTreeWidgetItem*> SelectedItems = FilterItems(ui->modelTreeWidget->selectedItems(), TypeFlags);
    TotalElements = SelectedItems.size();

    if (!TotalElements)
    {
        return 0;
    }

    int TotalVisible = 0, TotalOccluded = 0, TotalDynamic = 0;

    for (int i = 0; i < TotalElements; ++i)
    {
        CModelElement* pModelElement = SelectedItems.at(i)->GetGenericModelElement();
        ++TotalElementsByTypeId[int(pModelElement->GetTypeId())];

        switch (pModelElement->GetVisibility(false))
        {
            case CModelElement::eStaticOccluded:
                ++TotalOccluded;
                break;

            case CModelElement::eStaticVisible:
                ++TotalVisible;
                break;

            case CModelElement::eDynamicVisible:
                ++TotalDynamic;
                break;
        }
    }

    if (TotalVisible == TotalElements)
    {
        return 2;
    }

    if (TotalOccluded == TotalElements)
    {
        return 1;
    }

    if (TotalDynamic == TotalElements)
    {
        return 3;
    }

    return 4;
}

int VRME::GetActiveSelectionSaliencyStatus(int TotalElementsByTypeId[7], int& TotalElements, const uint TypeFlags, RVL::Real& IsoSaliency)
{
    memset(TotalElementsByTypeId, 0, sizeof(int) * 7);
    QList<CModelTreeWidgetItem*> SelectedItems = FilterItems(ui->modelTreeWidget->selectedItems(), TypeFlags);
    TotalElements = SelectedItems.size();

    if (!TotalElements)
    {
        return 0;
    }

    std::list<Real> Saliencies;

    for (int i = 0; i < TotalElements; ++i)
    {
        CModelElement* pModelElement = SelectedItems.at(i)->GetGenericModelElement();
        ++TotalElementsByTypeId[int(pModelElement->GetTypeId())];
        Saliencies.push_back(pModelElement->GetSaliencyWeight());
    }

    Saliencies.sort();
    Saliencies.unique();

    if (Saliencies.size() == 0)
    {
        return 0;
    }

    if (Saliencies.size() == 1)
    {
        IsoSaliency = Saliencies.front();
        return 2;
    }

    return 1;
}
//MODEL MANAGMENT

bool VRME::loadMultipleMeshToTree(CModelMultipleMesh* pModelMultipleMesh, const bool DoFullLoad)
{
    if (pModelMultipleMesh)
    {
        CModelTreeWidgetItem* pModelMultipleMeshItem = new CModelTreeWidgetItem(ui->modelTreeWidget, pModelMultipleMesh, m_ElementIcons);
        pModelMultipleMeshItem->setExpanded(true);

        const std::map<int, CModelMesh*>& Meshes = pModelMultipleMesh->GetMeshes();
        const int TotalMeshes = Meshes.size();

        if (TotalMeshes)
        {
            SetOperationSettings(IOperationProgressInterface::eSynchronous, 0, 0, TotalMeshes - 1);

            StartOperation(0);

            int Index = 0;

            for (std::map<int, CModelMesh*>::const_iterator ppMesh = Meshes.begin(); ppMesh != Meshes.end(); ++ppMesh)
            {
                if (!loadMeshToTree(ppMesh->second, pModelMultipleMeshItem, DoFullLoad))
                {
                    return false;
                }

                SetOperationProgress(0, ++Index);
            }

            FinishOperation(0);

        }

        ui->modelTreeWidget->expandAll();
        const int TotalColumns = ui->modelTreeWidget->columnCount();

        for (int i = 0; i < TotalColumns; ++i)
        {
            ui->modelTreeWidget->resizeColumnToContents(i);
        }

        ui->modelTreeWidget->collapseAll();

        return true;
    }

    return false;
}

bool VRME::loadMeshToTree(CModelMesh* pModelMesh, CModelTreeWidgetItem* pModelMultipleMeshItem, const bool DoFullLoad)
{
    if (pModelMesh && pModelMultipleMeshItem) //TODO ADD PROGRESS
    {
        CModelTreeWidgetItem* pModelMeshItem = new CModelTreeWidgetItem(pModelMultipleMeshItem, pModelMesh, m_ElementIcons);
        pModelMeshItem->setExpanded(false);

        if (DoFullLoad)
        {
            CModelTreeWidgetItem* pComposedFacesCollectionItem = new CModelTreeWidgetItem(pModelMeshItem, CModelTreeWidgetItem::eComposedFaces, m_SetsIcons);
            pComposedFacesCollectionItem->setExpanded(false);
            loadComposedFaces(pModelMesh, pComposedFacesCollectionItem);

            CModelTreeWidgetItem* pFacesCollectionItem = new CModelTreeWidgetItem(pModelMeshItem, CModelTreeWidgetItem::eFaces, m_SetsIcons);
            pFacesCollectionItem->setExpanded(false);
            loadFaces(pModelMesh, pFacesCollectionItem);

            CModelTreeWidgetItem* pEdgesCollectionItem = new CModelTreeWidgetItem(pModelMeshItem, CModelTreeWidgetItem::eEdges, m_SetsIcons);
            pEdgesCollectionItem->setExpanded(false);
            loadEdges(pModelMesh, pEdgesCollectionItem);

            CModelTreeWidgetItem* pVerticesCollectionItem = new CModelTreeWidgetItem(pModelMeshItem, CModelTreeWidgetItem::eVertices, m_SetsIcons);
            pVerticesCollectionItem->setExpanded(false);
            loadVertices(pModelMesh, pVerticesCollectionItem);
        }

        return true;
    }

    return false;
}

bool VRME::loadComposedFaces(CModelMesh* pModelMesh, CModelTreeWidgetItem* pModelMeshItem)
{
    if (pModelMesh && pModelMeshItem)
    {
        const std::map<int, CModelComposedFace*>& ComposedFaces = pModelMesh->GetComposedFaces();

        if (ComposedFaces.size())
        {
            std::map<int, CModelComposedFace*>::const_iterator ComposedFacesEnd = ComposedFaces.end();

            for (std::map<int, CModelComposedFace*>::const_iterator ppComposedFace = ComposedFaces.begin(); ppComposedFace != ComposedFacesEnd; ++ppComposedFace)
            {
                new CModelTreeWidgetItem(pModelMeshItem, ppComposedFace->second, m_ElementIcons);
            }
        }

        return true;
    }

    return false;
}

bool VRME::loadFaces(CModelMesh* pModelMesh, CModelTreeWidgetItem* pModelMeshItem)
{
    if (pModelMesh && pModelMeshItem)
    {
        const std::map<int, CModelFace*>& Faces = pModelMesh->GetFaces();

        if (Faces.size())
        {
            std::map<int, CModelFace*>::const_iterator FacesEnd = Faces.end();

            for (std::map<int, CModelFace*>::const_iterator ppFace = Faces.begin(); ppFace != FacesEnd; ++ppFace)
            {
                CModelTreeWidgetItem* pModelFaceItem = new CModelTreeWidgetItem(pModelMeshItem, ppFace->second, m_ElementIcons);
                new CModelTreeWidgetItem(pModelFaceItem, ppFace->second->GetWritableOrientedVertexA(), m_ElementIcons);
                new CModelTreeWidgetItem(pModelFaceItem, ppFace->second->GetWritableOrientedVertexB(), m_ElementIcons);
                new CModelTreeWidgetItem(pModelFaceItem, ppFace->second->GetWritableOrientedVertexC(), m_ElementIcons);
            }
        }

        return true;
    }

    return false;
}

bool VRME::loadEdges(CModelMesh* pModelMesh, CModelTreeWidgetItem* pModelMeshItem)
{
    if (pModelMesh && pModelMeshItem)
    {
        const std::map<int, CModelEdge*>& Edges = pModelMesh->GetEdges();

        if (Edges.size())
        {
            std::map<int, CModelEdge*>::const_iterator EdgesEnd = Edges.end();

            for (std::map<int, CModelEdge*>::const_iterator ppEdges = Edges.begin(); ppEdges != EdgesEnd; ++ppEdges)
            {
                new CModelTreeWidgetItem(pModelMeshItem, ppEdges->second, m_ElementIcons);
            }
        }

        return true;
    }

    return false;
}

bool VRME::loadVertices(CModelMesh* pModelMesh, CModelTreeWidgetItem* pModelMeshItem)
{
    if (pModelMesh && pModelMeshItem)
    {
        const std::map<int, CModelVertex*>& Vertices = pModelMesh->GetVertices();

        if (Vertices.size())
        {
            std::map<int, CModelVertex*>::const_iterator VerticesEnd = Vertices.end();

            for (std::map<int, CModelVertex*>::const_iterator ppVertex = Vertices.begin(); ppVertex != VerticesEnd; ++ppVertex)
            {
                new CModelTreeWidgetItem(pModelMeshItem, ppVertex->second, m_ElementIcons);
            }
        }

        return true;
    }

    return false;
}

bool VRME::loadVisualization(CModelMultipleMesh* pModelMultipleMesh)
{
    if (pModelMultipleMesh)
    {
        COpenInventorModelMultipleMesh* pOpenInventorModelMultipleMesh = new COpenInventorModelMultipleMesh(pModelMultipleMesh, true);
        m_pViewerMain->AddNode(pOpenInventorModelMultipleMesh->GetBaseSwitch());
        m_pViewerMain->ViewAll();

        updateVisibilityInViewerMain();

        return true;
    }

    return false;
}

void VRME::ResetAll()
{
    ui->modelTreeWidget->clear();

    m_pViewerMain->ClearNodes();
    m_pViewerMain->AddNode(m_pMainGlobalProps);
    m_pViewerMain->ViewAll();

    m_pViewerSelection->ClearNodes();
    m_pViewerSelection->AddNode(m_pSecondaryGlobalProps);
    m_pViewerSelection->ViewAll();
}

void VRME::on_treeRadioButton_toggled(bool checked)
{
    ui->modelTreeWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
    m_pViewerMain->DisplayingSelection(!checked);
    m_pViewerSelection->DisplayingSelection(!checked);
}

void VRME::on_spacePortRadioButton_toggled(bool checked)
{
    ui->modelTreeWidget->setSelectionMode(QAbstractItemView::NoSelection);
    m_pViewerMain->DisplayingSelection(checked);
    m_pViewerSelection->DisplayingSelection(checked);
}

void VRME::on_FilterRadioButton_toggled(bool checked)
{
    ui->modelTreeWidget->setSelectionMode(QAbstractItemView::NoSelection);
    m_pViewerMain->DisplayingSelection(!checked);
    m_pViewerSelection->DisplayingSelection(!checked);
}

void VRME::on_clearSelectionPushButton_clicked()
{
    ui->modelTreeWidget->clearSelection();

    m_pViewerMain->ClearSelection();

    m_pViewerSelection->ClearNodes();
    m_pViewerSelection->AddNode(m_pSecondaryGlobalProps);
    m_pViewerSelection->ViewAll();

    UnselectMesh();
}


void VRME::on_modelTreeWidget_itemChanged(QTreeWidgetItem* pItem, int column)
{
    if (m_IgnoreEvent)
    {
        return;
    }

    m_IgnoreEvent = true;
    CModelTreeWidgetItem* pModelTreeWidgetItem = dynamic_cast<CModelTreeWidgetItem*>(pItem);

    if (pModelTreeWidgetItem)
    {
        CModelElement* pModelElement = pModelTreeWidgetItem->GetGenericModelElement();

        if (pModelElement)
            switch (column)
            {
                case _ICON_COLUMN_:
                    switch (pItem->checkState(_ICON_COLUMN_))
                    {
                        case Qt::Unchecked:
                        {
                            COpenInventorModelPrimitive* pOpenInventorModelPrimitive = (COpenInventorModelPrimitive*) pModelElement->GetElementData();

                            if (!pOpenInventorModelPrimitive)
                            {
                                break;
                            }

                            pOpenInventorModelPrimitive->SetVisible(false);
                            pOpenInventorModelPrimitive->Update(false);
                        }
                        break;

                        case Qt::PartiallyChecked:
                            break;

                        case Qt::Checked:
                        {
                            COpenInventorModelPrimitive* pOpenInventorModelPrimitive = (COpenInventorModelPrimitive*) pModelElement->GetElementData();

                            if (!pOpenInventorModelPrimitive)
                            {
                                break;
                            }

                            pOpenInventorModelPrimitive->SetVisible(true);
                            pOpenInventorModelPrimitive->Update(false);
                        }
                        break;
                    }

                    break;

                case _INSTANCE_ID_COLUMN_:
                    break;

                case _ENABLED_COLUMN_:
                {
                    QString TextKey = pItem->text(column).toUpper().trimmed();

                    if ((TextKey == "TRUE") || (TextKey == "1") || (TextKey == "ON") || (TextKey == "YES") || (TextKey == "Y"))
                    {
                        pModelElement->SetEnabled(true);
                    }
                    else if ((TextKey == "FALSE") || (TextKey == "0") || (TextKey == "OFF") || (TextKey == "NO") || (TextKey == "N"))
                    {
                        pModelElement->SetEnabled(false);
                    }
                }
                break;

                case _INDEXABLE_COLUMN_:
                {
                    QString TextKey = pItem->text(column).toUpper().trimmed();

                    if ((TextKey == "TRUE") || (TextKey == "1") || (TextKey == "ON") || (TextKey == "YES") || (TextKey == "Y"))
                    {
                        pModelElement->SetIndexable(true);
                    }
                    else if ((TextKey == "FALSE") || (TextKey == "0") || (TextKey == "OFF") || (TextKey == "NO") || (TextKey == "N"))
                    {
                        pModelElement->SetIndexable(false);
                    }
                }
                break;

                case _VISIBLITIY_COLUMN_:
                {
                    QString TextKey = pItem->text(column).toUpper().trimmed();

                    if ((TextKey == "VISIBLE") || (TextKey == "1") || (TextKey == "YES") || (TextKey == "Y") || (TextKey == "ALWAYS") || (TextKey == "V"))
                    {
                        pModelElement->SetVisibility(CModelElement::eStaticVisible);
                    }
                    else if ((TextKey == "OCCLUDED") || (TextKey == "0") || (TextKey == "NO") || (TextKey == "N") || (TextKey == "NEVER" || (TextKey == "O")))
                    {
                        pModelElement->SetVisibility(CModelElement::eStaticOccluded);
                    }
                    else if ((TextKey == "DYNAMIC") || (TextKey == "D") || (TextKey == "PARTIALLY"))
                    {
                        pModelElement->SetVisibility(CModelElement::eStaticOccluded);
                    }
                }
                break;

                case _LABEL_COLUMN_:
                {
                    QString TextKey = pItem->text(column).toUpper().trimmed();

                    if (TextKey.length())
                    {
                        pModelElement->SetLabel(std::string(pItem->text(column).toLocal8Bit()));
                    }
                    else
                    {
                        pModelElement->ClearLabel();
                    }
                }
                break;

                case _SALIENCY_WEIGHT_COLUMN_:
                {
                    QString TextKey = pItem->text(column).trimmed();
                    bool Ok;
                    double Value = TextKey.toDouble(&Ok);

                    if (Ok)
                    {
                        pModelElement->SetSaliencyWeight(Real(Value));
                    }
                }
                break;
            }

        pModelTreeWidgetItem->Update(pModelElement ? m_ElementIcons : m_SetsIcons);
    }

    m_IgnoreEvent = false;
}


void VRME::on_modelTreeWidget_itemDoubleClicked(QTreeWidgetItem* pItem, int column)
{
    if (!ui->treeRadioButton->isChecked())
    {
        return;
    }

    CModelTreeWidgetItem* pModelTreeWidgetItem = dynamic_cast<CModelTreeWidgetItem*>(pItem);

    if (pModelTreeWidgetItem)
    {
        CModelElement* pModelElement = pModelTreeWidgetItem->GetGenericModelElement();

        if (pModelElement)
        {
            switch (column)
            {
                case _ICON_COLUMN_:
                    switch (pModelElement->GetTypeId())
                    {
                        case CModelElement::eVertex:
                        case CModelElement::eOrientedVertex:
                        case CModelElement::eEdge:
                        case CModelElement::eFace:
                        case CModelElement::eComposedFace:
                        {
                            CModelElement* pModelMesh = pModelElement->GetSuperior();
                            SelectMesh((COpenInventorModelMesh*)pModelMesh->GetElementData());

                            COpenInventorModelMesh* pOpenInventorModelPrimitive = (COpenInventorModelMesh*)pModelMesh->GetElementData();

                            if (pOpenInventorModelPrimitive)
                            {
                                pOpenInventorModelPrimitive->Update(true);
                            }
                        }
                        break;

                        case CModelElement::eMesh:
                        {
                            SelectMesh((COpenInventorModelMesh*)pModelElement->GetElementData());
                        }
                        break;

                        case CModelElement::eMultipleMesh:
                            break;
                    }

                    break;

                case _ENABLED_COLUMN_:
                    pModelElement->SetEnabled(!pModelElement->IsEnabled(false));

                    switch (pModelElement->GetTypeId())
                    {
                        case CModelElement::eVertex:
                        case CModelElement::eOrientedVertex:
                        case CModelElement::eEdge:
                        case CModelElement::eFace:
                        case CModelElement::eComposedFace:
                        {
                            CModelElement* pModelMesh = pModelElement->GetSuperior();
                            COpenInventorModelMesh* pOpenInventorModelPrimitive = (COpenInventorModelMesh*)pModelMesh->GetElementData();

                            if (pOpenInventorModelPrimitive)
                            {
                                pOpenInventorModelPrimitive->Update(true);
                            }
                        }
                        break;

                        case CModelElement::eMesh:
                        case CModelElement::eMultipleMesh:
                            break;
                    }

                    break;

                case _INDEXABLE_COLUMN_:
                    pModelElement->SetIndexable(!pModelElement->IsIndexable(false));

                    switch (pModelElement->GetTypeId())
                    {
                        case CModelElement::eVertex:
                        case CModelElement::eOrientedVertex:
                        case CModelElement::eEdge:
                        case CModelElement::eFace:
                        case CModelElement::eComposedFace:
                        {
                            CModelElement* pModelMesh = pModelElement->GetSuperior();
                            COpenInventorModelMesh* pOpenInventorModelPrimitive = (COpenInventorModelMesh*)pModelMesh->GetElementData();

                            if (pOpenInventorModelPrimitive)
                            {
                                pOpenInventorModelPrimitive->Update(true);
                            }
                        }
                        break;

                        case CModelElement::eMesh:
                        case CModelElement::eMultipleMesh:
                            break;
                    }

                    break;

                case _VISIBLITIY_COLUMN_:
                    pModelElement->SetVisibility(CModelElement::Visibility((pModelElement->GetVisibility(false) + 1) % 3));

                    switch (pModelElement->GetTypeId())
                    {
                        case CModelElement::eVertex:
                        case CModelElement::eOrientedVertex:
                        case CModelElement::eEdge:
                        case CModelElement::eFace:
                        case CModelElement::eComposedFace:
                        {
                            CModelElement* pModelMesh = pModelElement->GetSuperior();
                            COpenInventorModelMesh* pOpenInventorModelPrimitive = (COpenInventorModelMesh*)pModelMesh->GetElementData();

                            if (pOpenInventorModelPrimitive)
                            {
                                pOpenInventorModelPrimitive->Update(true);
                            }
                        }
                        break;

                        case CModelElement::eMesh:
                        case CModelElement::eMultipleMesh:
                            break;
                    }

                    break;
            }

            pModelTreeWidgetItem->Update(m_ElementIcons);
        }
        else
            switch (pModelTreeWidgetItem->GetCollectionType())
            {
                case CModelTreeWidgetItem::eVertices:
                case CModelTreeWidgetItem::eDynamicVertexConfigurations:
                case CModelTreeWidgetItem::eOrientedVertices:
                case CModelTreeWidgetItem::eDynamicOrientedVertexConfigurations:
                case CModelTreeWidgetItem::eEdges:
                case CModelTreeWidgetItem::eFaces:
                case CModelTreeWidgetItem::eComposedFaces:

                    break;

                case CModelTreeWidgetItem::eMultipleMeshes:
                    break;

                case CModelTreeWidgetItem::eNoCollection:
                    break;
            }
    }
}

//Vertices
void VRME::on_verticesVisibleDisplaySwitchCheckBox_toggled(bool /*checked*/)
{
    updateVisibilityInViewerMain();
}
void VRME::on_verticesOccludedDisplaySwitchCheckBox_toggled(bool /*checked*/)
{
    updateVisibilityInViewerMain();
}
void VRME::on_verticesDynamicDisplaySwitchCheckBox_toggled(bool /*checked*/)
{
    updateVisibilityInViewerMain();
}

//Edges
void VRME::on_edgesVisibleDisplaySwitchCheckBox_toggled(bool /*checked*/)
{
    updateVisibilityInViewerMain();
}
void VRME::on_edgesOccludedDisplaySwitchCheckBox_toggled(bool /*checked*/)
{
    updateVisibilityInViewerMain();
}
void VRME::on_edgesDynamicDisplaySwitchCheckBox_toggled(bool /*checked*/)
{
    updateVisibilityInViewerMain();
}

//Faces
void VRME::on_facesVisibleDisplaySwitchCheckBox_toggled(bool /*checked*/)
{
    updateVisibilityInViewerMain();
}
void VRME::on_facesOccludedDisplaySwitchCheckBox_toggled(bool /*checked*/)
{
    updateVisibilityInViewerMain();
}
void VRME::on_facesDynamicDisplaySwitchCheckBox_toggled(bool /*checked*/)
{
    updateVisibilityInViewerMain();
}

//Composed Faces
void VRME::on_composedFacesVisibleDisplaySwitchCheckBox_toggled(bool /*checked*/)
{
    updateVisibilityInViewerMain();
}
void VRME::on_composedFacesOccludedDisplaySwitchCheckBox_toggled(bool /*checked*/)
{
    updateVisibilityInViewerMain();
}
void VRME::on_composedFacesDynamicDisplaySwitchCheckBox_toggled(bool /*checked*/)
{
    updateVisibilityInViewerMain();
}



void VRME::OnAddToSelection(const SoQtExtendedExaminerViewer* pViewer, SoNode* /*pNode*/, void* pUserdata)
{
    if (!ui->spacePortRadioButton->isChecked())
    {
        return;
    }

    if (!pUserdata)
    {
        return;
    }

    if ((pUserdata == m_pFloor) || (pUserdata == m_pUCS))
    {
        return;
    }

    if (pViewer == m_pViewerMain)
    {
        COpenInventorModelPrimitive* pOpenInventorModelPrimitive = reinterpret_cast<COpenInventorModelPrimitive*>(pUserdata);
        CModelElement* pModelElement = pOpenInventorModelPrimitive->GetModelElement();
        CModelTreeWidgetItem* pItem = FindItem(pModelElement);

        if (pItem)
        {
            pItem->setSelected(true);
            pItem->setExpanded(true);
            ui->modelTreeWidget->scrollToItem(pItem, QAbstractItemView::PositionAtCenter);

            switch (pModelElement->GetTypeId())
            {
                case CModelElement::eVertex:
                case CModelElement::eOrientedVertex:
                case CModelElement::eEdge:
                case CModelElement::eFace:
                case CModelElement::eComposedFace:

                    SelectMesh((COpenInventorModelMesh*)pModelElement->GetSuperior()->GetElementData());
                    break;

                case CModelElement::eMesh:
                    SelectMesh((COpenInventorModelMesh*)pModelElement->GetElementData());
                    break;

                case CModelElement::eMultipleMesh:
                    break;
            }
        }

        SbVec3f Position;
        SbRotation Orientation;
        m_pViewerMain->GetCameraPose(Position, Orientation);
        m_pViewerSelection->SetCameraPose(Position, Orientation);
        //m_pViewerSelection->ViewAll();
    }
    else if (pViewer == m_pViewerSelection)
    {
        COpenInventorModelPrimitive* pOpenInventorModelPrimitive = (COpenInventorModelPrimitive*)pUserdata;
        CModelElement* pModelElement = pOpenInventorModelPrimitive->GetModelElement();
        setSelectionInformation(pModelElement);
        CModelTreeWidgetItem* pItem = FindItem(pModelElement);

        if (pItem)
        {
            pItem->setSelected(true);
            pItem->setExpanded(true);
            ui->modelTreeWidget->scrollToItem(pItem, QAbstractItemView::PositionAtCenter);
        }

        SbVec3f Position;
        SbRotation Orientation;
        m_pViewerSelection->GetCameraPose(Position, Orientation);
        m_pViewerMain->SetCameraPose(Position, Orientation);
        //m_pViewerMain->ViewAll();
    }
}

void VRME::OnRemoveFromSelection(const SoQtExtendedExaminerViewer* pViewer, SoNode* /*pNode*/, void* pUserdata)
{
    if (!ui->spacePortRadioButton->isChecked())
    {
        return;
    }

    if (!pUserdata)
    {
        return;
    }

    if ((pUserdata == m_pFloor) || (pUserdata == m_pUCS))
    {
        return;
    }

    if (pViewer == m_pViewerMain)
    {
        COpenInventorModelPrimitive* pOpenInventorModelPrimitive = (COpenInventorModelPrimitive*)pUserdata;
        CModelElement* pModelElement = pOpenInventorModelPrimitive->GetModelElement();
        CModelTreeWidgetItem* pItem = FindItem(pModelElement);

        if (pItem)
        {
            pItem->setSelected(false);
        }
    }
    else if (pViewer == m_pViewerSelection)
    {
        COpenInventorModelPrimitive* pOpenInventorModelPrimitive = (COpenInventorModelPrimitive*)pUserdata;
        CModelElement* pModelElement = pOpenInventorModelPrimitive->GetModelElement();
        CModelTreeWidgetItem* pItem = FindItem(pModelElement);

        if (pItem)
        {
            pItem->setSelected(false);
        }
    }
}

bool VRME::SetOperationSettings(const IOperationProgressInterface::ExectionMode Mode, const int OperationId, const int InitialStage, const int FinalStage)
{
    if (IOperationProgressInterface::SetOperationSettings(Mode, OperationId, InitialStage, FinalStage))
    {
        ui->projectProgressBar->setMinimum(InitialStage);
        ui->projectProgressBar->setMaximum(FinalStage);
        return true;
    }

    return false;
}

void VRME::OnProgress(const int /*OperationId*/, const int Stage)
{
    ui->projectProgressBar->setValue(Stage);
}

void VRME::on_modelTreeWidget_itemSelectionChanged()
{
    ///ENABLING
    int EnabledStatusTotalPrimitives[7],
        IndexableStatusTotalPrimitives[7],
        VisibilityStatusTotalPrimitives[7],
        SaliencyStatusTotalPrimitives[7],
        TotalEnabledElements, TotalIndexableElements, TotalVisibleElements, TotalSaliencyElements;

    ui->enabledComboBox->setCurrentIndex(GetActiveSelectionEnabledStatus(EnabledStatusTotalPrimitives, TotalEnabledElements, 0X7F));
    ui->enabledComboBox->setEnabled(TotalEnabledElements);
    ui->enabledLabel->setEnabled(TotalEnabledElements);

    ui->indexableComboBox->setCurrentIndex(GetActiveSelectionIndexableStatus(IndexableStatusTotalPrimitives, TotalIndexableElements, 0X7F));
    ui->indexableComboBox->setEnabled(TotalIndexableElements);
    ui->IndexableLabel->setEnabled(TotalIndexableElements);

    ui->visibilityComboBox->setCurrentIndex(GetActiveSelectionVisibilityStatus(VisibilityStatusTotalPrimitives, TotalVisibleElements, 0X7F));
    ui->visibilityComboBox->setEnabled(TotalVisibleElements);
    ui->visibilitylabel->setEnabled(TotalVisibleElements);

    Real IsoSaliency = Real(0);
    const int SaliencyMode = GetActiveSelectionSaliencyStatus(SaliencyStatusTotalPrimitives, TotalSaliencyElements, 0X7F, IsoSaliency);

    if (SaliencyMode == 2)
    {
        ui->saliencyWeightingLineEdit->setEnabled(true);
        ui->saliencyWeightingLineEdit->setText(QString("%1").arg(IsoSaliency));
    }
    else
    {
        ui->saliencyWeightingLineEdit->setEnabled(false);
        ui->saliencyWeightingLineEdit->setText(QString(""));
    }

    ui->saliencyWeightingOperationApplyPushButton->setEnabled(false);
    ui->saliencyWeightingComboBox->setCurrentIndex(SaliencyMode);
    ui->saliencyWeightingComboBox->setEnabled(TotalSaliencyElements);
    ui->saliencyWeightingLabel->setEnabled(TotalSaliencyElements);


    for (int i = 0; i < 7; ++i)
    {
        m_Enablers[i]->setText(m_Labels[i].arg(EnabledStatusTotalPrimitives[i]));
        m_Enablers[i]->setCheckState(EnabledStatusTotalPrimitives[i] ? Qt::Checked : Qt::Unchecked);
        m_Enablers[i]->setEnabled(EnabledStatusTotalPrimitives[i]);
        m_Indexers[i]->setText(m_Labels[i].arg(IndexableStatusTotalPrimitives[i]));
        m_Indexers[i]->setCheckState(IndexableStatusTotalPrimitives[i] ? Qt::Checked : Qt::Unchecked);
        m_Indexers[i]->setEnabled(IndexableStatusTotalPrimitives[i]);
        m_Visualizers[i]->setText(m_Labels[i].arg(VisibilityStatusTotalPrimitives[i]));
        m_Visualizers[i]->setCheckState(VisibilityStatusTotalPrimitives[i] ? Qt::Checked : Qt::Unchecked);
        m_Visualizers[i]->setEnabled(VisibilityStatusTotalPrimitives[i]);
        m_SaliencyManagers[i]->setText(m_Labels[i].arg(SaliencyStatusTotalPrimitives[i]));
        m_SaliencyManagers[i]->setCheckState(SaliencyStatusTotalPrimitives[i] ? Qt::Checked : Qt::Unchecked);
        m_SaliencyManagers[i]->setEnabled(SaliencyStatusTotalPrimitives[i]);
    }

    if ((EnabledStatusTotalPrimitives[5] == 1) || (IndexableStatusTotalPrimitives[5] == 1) || (VisibilityStatusTotalPrimitives[5] == 1))
    {
        ui->automaticComboBox->setEnabled(true);
        ui->automaticComboBox->setCurrentIndex(0);
        ui->automaticOperationApplyPushButton->setEnabled(false);
        ui->automaticLabel->setEnabled(true);

        if (ui->treeRadioButton->isChecked())
        {
            QList<CModelTreeWidgetItem*> SelectedItems = FilterItems(ui->modelTreeWidget->selectedItems(), 0X20);

            if (SelectedItems.size() == 1)
            {
                SelectMesh((COpenInventorModelMesh*)SelectedItems.at(0)->GetGenericModelElement()->GetElementData());
            }
        }
    }
    else
    {
        ui->automaticComboBox->setEnabled(false);
        ui->automaticComboBox->setCurrentIndex(0);
        ui->automaticOperationApplyPushButton->setEnabled(false);
        ui->automaticLabel->setEnabled(false);
    }

    updateTransformationPanel();
    updateAvailableTransformations();
}

void VRME::on_enableOperationApplyPushButton_clicked()
{
    uint TypeFlags = 0;

    for (int i = 0; i < 7; ++i)
        if (m_Enablers[i]->isChecked())
        {
            TypeFlags |= (0x1 << i);
        }

    QList<CModelTreeWidgetItem*> SelectedItems = FilterItems(ui->modelTreeWidget->selectedItems(), TypeFlags);
    const int TotalSelecteditems = SelectedItems.size();

    if (TotalSelecteditems)
    {
        const bool Enabled = (ui->enabledComboBox->currentIndex() == 2);

        for (int i = 0; i < TotalSelecteditems; ++i)
        {
            CModelTreeWidgetItem* pItem = SelectedItems.at(i);
            pItem->GetGenericModelElement()->SetEnabled(Enabled);
            pItem->Update(m_ElementIcons);
        }
    }

    on_modelTreeWidget_itemSelectionChanged();
    ActivateEnableOperationApplyPushButton();
}

void VRME::on_IndexableOperationApplyPushButton_clicked()
{
    uint TypeFlags = 0;

    for (int i = 0; i < 7; ++i)
        if (m_Indexers[i]->isChecked())
        {
            TypeFlags |= (0x1 << i);
        }

    QList<CModelTreeWidgetItem*> SelectedItems = FilterItems(ui->modelTreeWidget->selectedItems(), TypeFlags);
    const int TotalSelecteditems = SelectedItems.size();

    if (TotalSelecteditems)
    {
        const bool Indexable = (ui->indexableComboBox->currentIndex() == 2);

        for (int i = 0; i < TotalSelecteditems; ++i)
        {
            CModelTreeWidgetItem* pItem = SelectedItems.at(i);
            pItem->GetGenericModelElement()->SetIndexable(Indexable);
            pItem->Update(m_ElementIcons);
        }
    }

    on_modelTreeWidget_itemSelectionChanged();
    ActivateIndexableOperationApplyPushButton();
}

void VRME::on_visibilityOperationApplyPushButton_clicked()
{
    uint TypeFlags = 0;

    for (int i = 0; i < 7; ++i)
        if (m_Visualizers[i]->isChecked())
        {
            TypeFlags |= (0x1 << i);
        }

    QList<CModelTreeWidgetItem*> SelectedItems = FilterItems(ui->modelTreeWidget->selectedItems(), TypeFlags);
    const int TotalSelecteditems = SelectedItems.size();

    if (TotalSelecteditems)
    {
        CModelElement::Visibility TargetVisibility = CModelElement::eStaticOccluded;

        switch (ui->visibilityComboBox->currentIndex())
        {
            case 1:
                TargetVisibility = CModelElement::eStaticOccluded;
                break;

            case 2:
                TargetVisibility = CModelElement::eStaticVisible;
                break;

            case 3:
                TargetVisibility = CModelElement::eDynamicVisible;
                break;
        }

        std::set<CModelElement*> Meshes;

        for (int i = 0; i < TotalSelecteditems; ++i)
        {
            CModelTreeWidgetItem* pItem = SelectedItems.at(i);
            CModelElement* pElement = pItem->GetGenericModelElement();
            pElement->SetVisibility(TargetVisibility);
            pItem->Update(m_ElementIcons);

            CModelElement* pSuperiorElement = pElement->GetSuperior();

            if (pSuperiorElement && pSuperiorElement->GetTypeId() == CModelElement::eMesh)
            {
                Meshes.insert(pSuperiorElement);
            }
        }

        if (Meshes.size())
            for (std::set<CModelElement*>::iterator ppModelElement = Meshes.begin(); ppModelElement != Meshes.end(); ++ppModelElement)
            {
                COpenInventorModelMesh* pOpenInventorModelMesh = (COpenInventorModelMesh*)(*ppModelElement)->GetElementData();
                pOpenInventorModelMesh->Update(true);
            }

    }

    on_modelTreeWidget_itemSelectionChanged();
    ActivateVisibilityOperationApplyPushButton();
}

void VRME::on_enabledComboBox_currentIndexChanged(int index)
{
    switch (index)
    {
        case 0:
        case 3:
            ui->enableOperationApplyPushButton->setEnabled(false);
            break;

        default:
            ActivateEnableOperationApplyPushButton();
            break;
    }
}

void VRME::on_indexableComboBox_currentIndexChanged(int index)
{
    switch (index)
    {
        case 0:
        case 3:
            ui->IndexableOperationApplyPushButton->setEnabled(false);
            break;

        default:
            ActivateIndexableOperationApplyPushButton();
            break;
    }
}

void VRME::on_visibilityComboBox_currentIndexChanged(int index)
{
    switch (index)
    {
        case 0:
        case 4:
            ui->visibilityOperationApplyPushButton->setEnabled(false);
            break;

        default:
            ActivateVisibilityOperationApplyPushButton();
            break;
    }
}

void VRME::updateAvailableTransformations()
{
    ui->availableTransformationsTableWidget->setRowCount(0);
    ui->availableTransformationsTableWidget->clearContents();
    ui->availableTransformationsTableWidget->setEnabled(false);
    ui->assignTransformationPushButton->setEnabled(false);
    ui->removeTransformationPushButton->setEnabled(false);

    if (m_pModelMultipleMeshes && m_pModelMultipleMeshes->HasTransformations() && m_pSelectedpOpenInventorModelMesh)
    {
        const std::map<int, CModelTransformation*>& Transformations = m_pModelMultipleMeshes->GetTransformations();

        ui->availableTransformationsTableWidget->setEnabled(true);
        ui->availableTransformationsTableWidget->setRowCount(Transformations.size());

        int Index = 0;
        std::map<int, CModelTransformation*>::const_iterator EndTransformations = Transformations.end();

        for (std::map<int, CModelTransformation*>::const_iterator pKeyTransformation = Transformations.begin(); pKeyTransformation != EndTransformations; ++pKeyTransformation, ++Index)
        {
            CModelTransformation* pModelTransformation = pKeyTransformation->second;

            QString Id = QString("%1").arg(pKeyTransformation->first);
            QString TransformationType = QString(CModelTransformation::TransformationTypeToString(pModelTransformation->GetTransformationType()).c_str());
            QString Label = QString(pModelTransformation->GetLabel().c_str());

            const std::map<int, CModelMesh*>& Meshes =  pModelTransformation->GetMeshes();
            const int TotalMeshes = Meshes.size();
            QString NumberOfDynamicMeshes;

            if (TotalMeshes)
            {
                std::map<int, CModelMesh*>::const_iterator pKeymesh = Meshes.begin();
                NumberOfDynamicMeshes  = QString("[%1] : { ").arg(TotalMeshes);

                for (int i = 0; i < TotalMeshes; ++i, ++pKeymesh)
                {
                    if (i + 1 < TotalMeshes)
                    {
                        NumberOfDynamicMeshes  += QString("%1 ,").arg(pKeymesh->first);
                    }
                    else
                    {
                        NumberOfDynamicMeshes  += QString("%1").arg(pKeymesh->first);
                    }
                }

                NumberOfDynamicMeshes  += QString(" }");
            }
            else
            {
                NumberOfDynamicMeshes  = QString("[0]");
            }

            QTableWidgetItem* pItem = new QTableWidgetItem(Id);
            pItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            ui->availableTransformationsTableWidget->setItem(Index, 0, pItem);
            pItem = new QTableWidgetItem(TransformationType);
            pItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            ui->availableTransformationsTableWidget->setItem(Index, 1, pItem);
            pItem = new QTableWidgetItem(Label);
            pItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            ui->availableTransformationsTableWidget->setItem(Index, 2, pItem);
            pItem = new QTableWidgetItem(NumberOfDynamicMeshes);
            pItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            ui->availableTransformationsTableWidget->setItem(Index, 3, pItem);
        }

        CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
        CModelTransformation* pModelTransformation = pModelMesh->GetTransformation();

        if (pModelTransformation)
        {
            const int Id = pModelTransformation->GetInstanceId();
            int Index = 0;

            for (std::map<int, CModelTransformation*>::const_iterator pKeyTransformation = Transformations.begin(); pKeyTransformation != EndTransformations; ++pKeyTransformation, ++Index)
                if (Id == pKeyTransformation->first)
                {
                    ui->availableTransformationsTableWidget->selectRow(Index);
                    break;
                }

            ui->removeTransformationPushButton->setEnabled(true);
        }

        //else
        //ui->assignTransformationPushButton->setEnabled(true);
    }
}

void VRME::ActivateEnableOperationApplyPushButton()
{
    uint TypeFlags = 0;

    for (uint i = 0; i < 7; ++i)
        if (m_Enablers[i]->isChecked())
        {
            TypeFlags |= (0X1 << i);
        }

    int EnabledStatusTotalPrimitives[7], TotalEnabledElements;
    ui->enableOperationApplyPushButton->setEnabled(TypeFlags && (ui->enabledComboBox->currentIndex() != GetActiveSelectionEnabledStatus(EnabledStatusTotalPrimitives, TotalEnabledElements, TypeFlags)));
}

void VRME::ActivateIndexableOperationApplyPushButton()
{
    uint TypeFlags = 0;

    for (uint i = 0; i < 7; ++i)
        if (m_Indexers[i]->isChecked())
        {
            TypeFlags |= (0X1 << i);
        }

    int IndexableStatusTotalPrimitives[7], TotalIndexableElements;
    ui->IndexableOperationApplyPushButton->setEnabled(TypeFlags && (ui->indexableComboBox->currentIndex() != GetActiveSelectionIndexableStatus(IndexableStatusTotalPrimitives, TotalIndexableElements, TypeFlags)));
}

void VRME::ActivateVisibilityOperationApplyPushButton()
{
    uint TypeFlags = 0;

    for (uint i = 0; i < 7; ++i)
        if (m_Visualizers[i]->isChecked())
        {
            TypeFlags |= (0X1 << i);
        }

    int VisibilityStatusTotalPrimitives[7], TotalVisibleElements;
    ui->visibilityOperationApplyPushButton->setEnabled(TypeFlags && (ui->visibilityComboBox->currentIndex() != GetActiveSelectionVisibilityStatus(VisibilityStatusTotalPrimitives, TotalVisibleElements, TypeFlags)));
}

void VRME::ActivateSaliencyOperationApplyPushButton()
{
    uint TypeFlags = 0;

    for (uint i = 0; i < 7; ++i)
        if (m_SaliencyManagers[i]->isChecked())
        {
            TypeFlags |= (0X1 << i);
        }

    int SaliencyStatusTotalPrimitives[7], TotalSaliencyElements;
    RVL::Real IsoSaliency = 0;
    ui->visibilityOperationApplyPushButton->setEnabled(TypeFlags && (2 == GetActiveSelectionSaliencyStatus(SaliencyStatusTotalPrimitives, TotalSaliencyElements, TypeFlags, IsoSaliency)));
}

void VRME::on_enabledVerticeCountCheckBox_toggled(bool /*checked*/)
{
    ActivateEnableOperationApplyPushButton();
}
void VRME::on_enabledOrientedVerticesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateEnableOperationApplyPushButton();
}
void VRME::on_enabledEdgesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateEnableOperationApplyPushButton();
}
void VRME::on_enabledFacesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateEnableOperationApplyPushButton();
}
void VRME::on_enabledComposedFacesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateEnableOperationApplyPushButton();
}
void VRME::on_enabledMeshesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateEnableOperationApplyPushButton();
}
void VRME::on_enabledMultipleMeshesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateEnableOperationApplyPushButton();
}

void VRME::on_indexableVerticeCountCheckBox_toggled(bool /*checked*/)
{
    ActivateIndexableOperationApplyPushButton();
}
void VRME::on_indexableOrientedVerticesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateIndexableOperationApplyPushButton();
}
void VRME::on_indexableEdgesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateIndexableOperationApplyPushButton();
}
void VRME::on_indexableFacesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateIndexableOperationApplyPushButton();
}
void VRME::on_indexableComposedFacesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateIndexableOperationApplyPushButton();
}
void VRME::on_indexableMeshesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateIndexableOperationApplyPushButton();
}
void VRME::on_indexableMultipleMeshesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateIndexableOperationApplyPushButton();
}

void VRME::on_visibilityVerticeCountCheckBox_toggled(bool /*checked*/)
{
    ActivateVisibilityOperationApplyPushButton();
}
void VRME::on_visibilityOrientedVerticesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateVisibilityOperationApplyPushButton();
}
void VRME::on_visibilityEdgesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateVisibilityOperationApplyPushButton();
}
void VRME::on_visibilityFacesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateVisibilityOperationApplyPushButton();
}
void VRME::on_visibilityComposedFacesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateVisibilityOperationApplyPushButton();
}
void VRME::on_visibilityMeshesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateVisibilityOperationApplyPushButton();
}
void VRME::on_visibilityMultipleMeshesCountCheckBox_toggled(bool /*checked*/)
{
    ActivateVisibilityOperationApplyPushButton();
}

int GetIdFromTag(const char* pText, const int Length)
{
    QString Filtered;

    for (int i = 0; i < Length; ++i)
        if ((pText[i] >= '0') && (pText[i] <= '9'))
        {
            Filtered += pText[i];
        }

    return Filtered.toInt();
}

void VRME::on_pushButtonCreateTranslationTransformation_clicked()
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
    CModelTranslation* pModelTranslation = nullptr;

    const Real MinimalDisplacement = Real(ui->minimalDisplacementLineEdit->text().toDouble());
    const Real MaximalDisplacement = Real(ui->maximalDisplacementLineEdit->text().toDouble());
    const Real DefaultDisplacement = Real(ui->defaultDisplacementLineEdit->text().toDouble());
    const Real CurrentNormalizedDisplacement = (Real(ui->horizontalSliderTranslationDoF->value()) - MinimalDisplacement) / (MaximalDisplacement - MinimalDisplacement);

    if (ui->vertexABCheckBox->isChecked())
    {
        QString TextIdA = ui->SourceComboBoxA->currentText();
        QString TextIdB = ui->SourceComboBoxB->currentText();
        const CModelVertex* pModelVertexA = pModelMesh->GetVertexById(GetIdFromTag(TextIdA.toAscii(), TextIdA.length()));
        const CModelVertex* pModelVertexB = pModelMesh->GetVertexById(GetIdFromTag(TextIdB.toAscii(), TextIdB.length()));
        pModelTranslation = new CModelTranslation(pModelMesh, pModelVertexA, pModelVertexB, MinimalDisplacement, MaximalDisplacement, DefaultDisplacement, CurrentNormalizedDisplacement);
    }
    else if (ui->edgeACheckBox->isChecked())
    {
        QString TextIdA = ui->SourceComboBoxA->currentText();
        const CModelEdge* pModelEdge = pModelMesh->GetEdgeById(GetIdFromTag(TextIdA.toAscii(), TextIdA.length()));
        pModelTranslation = new CModelTranslation(pModelMesh, pModelEdge, MinimalDisplacement, MaximalDisplacement, DefaultDisplacement, CurrentNormalizedDisplacement);
    }

    if (!pModelTranslation)
    {
        return;
    }

    QString Label = ui->transformationLabelLineEditTranslation->text();
    pModelTranslation->SetLabel(std::string(Label.toAscii().data()));



    CModelMultipleMesh* pModelMultipleMesh = dynamic_cast<CModelMultipleMesh*>(pModelMesh->GetSuperior());

    if (!pModelMultipleMesh)
    {
        return;
    }

    pModelMesh->SetTransformation(pModelTranslation);
    pModelMultipleMesh->AddTransformation(pModelTranslation);

    updateTransformationPanel();
    updateAvailableTransformations();
}

void VRME::on_pushButtonModifyTranslationTransformation_clicked()
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    const Real MinimalDisplacement = Real(ui->minimalDisplacementLineEdit->text().toDouble());
    const Real MaximalDisplacement = Real(ui->maximalDisplacementLineEdit->text().toDouble());
    const Real DefaultDisplacement = Real(ui->defaultDisplacementLineEdit->text().toDouble());
    const Real CurrentNormalizedDisplacement = (Real(ui->horizontalSliderTranslationDoF->value()) - MinimalDisplacement) / (MaximalDisplacement - MinimalDisplacement);

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
    CModelTranslation* pModelTranslation = dynamic_cast<CModelTranslation*>(pModelMesh->GetTransformation());

    pModelTranslation->Set(MinimalDisplacement, MaximalDisplacement, DefaultDisplacement, CurrentNormalizedDisplacement);

    QString Label = ui->transformationLabelLineEditTranslation->text();
    pModelTranslation->SetLabel(std::string(Label.toAscii().data()));

    updateTransformationPanel();
    updateAvailableTransformations();
}

void VRME::on_horizontalSliderTranslationDoF_sliderMoved(int Value)
{
    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
    CModelTranslation* pModelTranslation = dynamic_cast<CModelTranslation*>(pModelMesh->GetTransformation());

    if (pModelTranslation)
    {
        pModelTranslation->SetParametricTranslation(Value, -1);
        ui->pushButtonModifyTranslationTransformation->setEnabled(true);
    }
}

//Vertex [AB]
void VRME::on_vertexABCheckBox_clicked(bool checked)
{
    ui->SourceComboBoxA->setEnabled(checked);
    ui->SourceComboBoxB->setEnabled(checked);
    ui->SourceComboBoxC->setEnabled(!checked);
    ui->SourceComboBoxD->setEnabled(!checked);

    ui->SourceComboBoxA->clear();
    ui->SourceComboBoxB->clear();
    ui->SourceComboBoxC->clear();
    ui->SourceComboBoxD->clear();

    if (checked)
    {
        QList<CModelTreeWidgetItem*> VertexItems = FilterItems(ui->modelTreeWidget->selectedItems(), 0X1); //eVertex = 0X1

        for (int i = 0; i < VertexItems.size(); ++i)
        {
            QString Item = QString("[Vertex:%1]").arg(VertexItems.at(i)->GetGenericModelElement()->GetInstanceId());
            ui->SourceComboBoxA->addItem(Item);
            ui->SourceComboBoxB->addItem(Item);
        }
    }
}

//Transformation: Translation
//Vertex [AB]-[CD]
void VRME::on_vertexABCDCheckBox_clicked(bool checked)
{
    ui->SourceComboBoxA->setEnabled(checked);
    ui->SourceComboBoxB->setEnabled(checked);
    ui->SourceComboBoxC->setEnabled(checked);
    ui->SourceComboBoxD->setEnabled(checked);

    ui->SourceComboBoxA->clear();
    ui->SourceComboBoxB->clear();
    ui->SourceComboBoxC->clear();
    ui->SourceComboBoxD->clear();

    if (checked)
    {
        QList<CModelTreeWidgetItem*> VertexItems = FilterItems(ui->modelTreeWidget->selectedItems(), 0X1); //eVertex = 0X1

        for (int i = 0; i < VertexItems.size(); ++i)
        {
            QString Item = QString("[Vertex:%1]").arg(VertexItems.at(i)->GetGenericModelElement()->GetInstanceId());
            ui->SourceComboBoxA->addItem(Item);
            ui->SourceComboBoxB->addItem(Item);
            ui->SourceComboBoxC->addItem(Item);
            ui->SourceComboBoxD->addItem(Item);
        }
    }
}

//Transformation: Translation
//Edge [A]
void VRME::on_edgeACheckBox_clicked(bool checked)
{
    ui->SourceComboBoxA->setEnabled(checked);
    ui->SourceComboBoxB->setEnabled(!checked);
    ui->SourceComboBoxC->setEnabled(!checked);
    ui->SourceComboBoxD->setEnabled(!checked);

    ui->SourceComboBoxA->clear();
    ui->SourceComboBoxB->clear();
    ui->SourceComboBoxC->clear();
    ui->SourceComboBoxD->clear();

    if (checked)
    {
        QList<CModelTreeWidgetItem*> EdgeItems = FilterItems(ui->modelTreeWidget->selectedItems(), 0X4); //eEdge = 0X4

        for (int i = 0; i < EdgeItems.size(); ++i)
        {
            ui->SourceComboBoxA->addItem(QString("[Edge:%1]").arg(EdgeItems.at(i)->GetGenericModelElement()->GetInstanceId()));
        }
    }
}

//Transformation: Translation
//Edge [A]-[B]
void VRME::on_edgeABCheckBox_clicked(bool checked)
{
    ui->SourceComboBoxA->setEnabled(checked);
    ui->SourceComboBoxB->setEnabled(checked);
    ui->SourceComboBoxC->setEnabled(!checked);
    ui->SourceComboBoxD->setEnabled(!checked);

    ui->SourceComboBoxA->clear();
    ui->SourceComboBoxB->clear();
    ui->SourceComboBoxC->clear();
    ui->SourceComboBoxD->clear();

    if (checked)
    {
        QList<CModelTreeWidgetItem*> EdgeItems = FilterItems(ui->modelTreeWidget->selectedItems(), 0X4); //eEdge = 0X4

        for (int i = 0; i < EdgeItems.size(); ++i)
        {
            QString Item = QString("[Edge:%1]").arg(EdgeItems.at(i)->GetGenericModelElement()->GetInstanceId());
            ui->SourceComboBoxA->addItem(Item);
            ui->SourceComboBoxB->addItem(Item);
        }
    }
}

void VRME::on_SourceComboBoxA_currentIndexChanged(const QString& /*Arg*/)
{
    ui->pushButtonCreateTranslationTransformation->setEnabled(IsPanelContentTranslationTransformationReady(false));
}

void VRME::on_SourceComboBoxB_currentIndexChanged(const QString& /*Arg*/)
{
    ui->pushButtonCreateTranslationTransformation->setEnabled(IsPanelContentTranslationTransformationReady(false));
}

void VRME::on_SourceComboBoxC_currentIndexChanged(const QString& /*Arg*/)
{
    ui->pushButtonCreateTranslationTransformation->setEnabled(IsPanelContentTranslationTransformationReady(false));
}

void VRME::on_SourceComboBoxD_currentIndexChanged(const QString& /*Arg*/)
{
    ui->pushButtonCreateTranslationTransformation->setEnabled(IsPanelContentTranslationTransformationReady(false));
}

bool VRME::IsPanelContentTranslationTransformationReady(const bool CheckOnlyValues)
{
    bool LabelOk, MinOK, MaxOK, DefOK;
    QString LableUiContent = ui->transformationLabelLineEditTranslation->text();
    LabelOk = (LableUiContent.length() > 0) && (!(LableUiContent.contains(QChar('<')) || LableUiContent.contains(QChar('>')) || LableUiContent.contains(QChar('/'))));
    const double MinimalDisplacement = ui->minimalDisplacementLineEdit->text().toDouble(&MinOK);
    const double MaximalDisplacement = ui->maximalDisplacementLineEdit->text().toDouble(&MaxOK);
    const double DeafultDisplacement = ui->defaultDisplacementLineEdit->text().toDouble(&DefOK);

    if (LabelOk && MinOK && MaxOK && DefOK && (MinimalDisplacement < MaximalDisplacement) && (DeafultDisplacement >= MinimalDisplacement) && (DeafultDisplacement <= MaximalDisplacement))
    {
        if (CheckOnlyValues)
        {
            return true;
        }

        std::list<int> Indices;

        if (ui->SourceComboBoxA->count())
        {
            Indices.push_back(ui->SourceComboBoxA->currentIndex());
        }

        if (ui->SourceComboBoxB->count())
        {
            Indices.push_back(ui->SourceComboBoxB->currentIndex());
        }

        if (ui->SourceComboBoxC->count())
        {
            Indices.push_back(ui->SourceComboBoxC->currentIndex());
        }

        if (ui->SourceComboBoxD->count())
        {
            Indices.push_back(ui->SourceComboBoxD->currentIndex());
        }

        int TotalIndices = Indices.size();

        if (!TotalIndices)
        {
            return false;
        }

        if (TotalIndices > 1)
        {
            Indices.sort();
            int Prev = Indices.front();
            std::list<int>::iterator pIndex = Indices.begin();

            for (++pIndex ; pIndex != Indices.end(); ++pIndex)
                if (*pIndex == Prev)
                {
                    return false;
                }
                else
                {
                    Prev = *pIndex;
                }
        }

        return true;
    }

    return false;
}

bool VRME::IsPanelContentRotationTransformationReady(const bool CheckOnlyValues)
{
    bool LabelOk, MinOK, MaxOK, DefOK;
    QString LableUiContent = ui->transformationLabelLineEditRotation->text();
    LabelOk = (LableUiContent.length() > 0) && (!(LableUiContent.contains(QChar('<')) || LableUiContent.contains(QChar('>')) || LableUiContent.contains(QChar('/'))));
    const double MinimalRotation = ui->minimalRotationLineEdit->text().toDouble(&MinOK);
    const double MaximalRotation = ui->maximalRotationLineEdit->text().toDouble(&MaxOK);
    const double DeafultRotation = ui->defaultRotationLineEdit->text().toDouble(&DefOK);

    if (LabelOk && MinOK && MaxOK && DefOK && (MinimalRotation < MaximalRotation) && (DeafultRotation >= MinimalRotation) && (DeafultRotation <= MaximalRotation))
    {
        if (CheckOnlyValues)
        {
            return true;
        }

        std::list<int> Indices;

        if (ui->SourceComboBoxRotationA->count())
        {
            Indices.push_back(ui->SourceComboBoxRotationA->currentIndex());
        }

        if (ui->SourceComboBoxRotationB->count())
        {
            Indices.push_back(ui->SourceComboBoxRotationB->currentIndex());
        }

        if (ui->SourceComboBoxRotationC->count())
        {
            Indices.push_back(ui->SourceComboBoxRotationC->currentIndex());
        }

        if (ui->SourceComboBoxRotationD->count())
        {
            Indices.push_back(ui->SourceComboBoxRotationD->currentIndex());
        }

        int TotalIndices = Indices.size();

        if (!TotalIndices)
        {
            return false;
        }

        if (TotalIndices > 1)
        {
            Indices.sort();
            int Prev = Indices.front();
            std::list<int>::iterator pIndex = Indices.begin();

            for (++pIndex ; pIndex != Indices.end(); ++pIndex)
                if (*pIndex == Prev)
                {
                    return false;
                }
                else
                {
                    Prev = *pIndex;
                }
        }

        return true;
    }

    return false;
}

void VRME::DeleteMesh(RVL::Representation::ModelBased::GeometricGraph::Base::CModelMesh* pModelMesh)
{
    if (!(pModelMesh && m_pModelMultipleMeshes))
    {
        return;
    }

    CModelTreeWidgetItem* pItemMesh = FindItem(pModelMesh);

    CModelTreeWidgetItem* pItemMultipleMesh = FindItem(m_pModelMultipleMeshes);

    if (!(pItemMesh && pItemMultipleMesh))
    {
        return;
    }

    COpenInventorModelMesh* pOpenInventorModelMesh = (COpenInventorModelMesh*)(pModelMesh->GetElementData());

    if (!pOpenInventorModelMesh)
    {
        return;
    }

    COpenInventorModelMultipleMesh* pOpenInventorModelMultipleMesh = (COpenInventorModelMultipleMesh*)(m_pModelMultipleMeshes->GetElementData());

    if (!pOpenInventorModelMultipleMesh)
    {
        return;
    }

    if (m_pSelectedpOpenInventorModelMesh == pOpenInventorModelMesh)
    {
        UnselectMesh(true);
        m_pViewerMain->ClearSelection();
        m_pViewerSelection->ClearSelection();
    }

    pOpenInventorModelMultipleMesh->DeleteMeshVisualization(pOpenInventorModelMesh);
    m_pModelMultipleMeshes->DeleteMesh(pModelMesh);

    pItemMultipleMesh->removeChild(pItemMesh);



}

template <typename GenericModelElement> void SetConditionalVisibility(const std::map<int, GenericModelElement*>& Collection, const CModelElement::Visibility FromVisibility, const CModelElement::Visibility ToVisibility)
{
    typename std::map<int, GenericModelElement*>::const_iterator EndCollection = Collection.end();

    for (typename std::map<int, GenericModelElement*>::const_iterator ppModelElement = Collection.begin() ; ppModelElement != EndCollection ; ++ppModelElement)
        if (ppModelElement->second->GetVisibility(false) == FromVisibility)
        {
            ppModelElement->second->SetVisibility(ToVisibility);
        }
}

void VRME::DynamicVisibilityConsistencyCheck(CModelMesh* pModelMesh)
{
    if (!pModelMesh)
    {
        return;
    }

    CModelElement::Visibility FromVisibility = pModelMesh->IsDynamic() ? CModelElement::eStaticVisible : CModelElement::eDynamicVisible;
    CModelElement::Visibility ToVisibility = pModelMesh->IsDynamic() ? CModelElement::eDynamicVisible : CModelElement::eStaticVisible;

    if (pModelMesh->GetVisibility(false) == FromVisibility)
    {
        pModelMesh->SetVisibility(ToVisibility);
    }

    SetConditionalVisibility<CModelVertex>(pModelMesh->GetVertices(), FromVisibility, ToVisibility);
    SetConditionalVisibility<CModelEdge>(pModelMesh->GetEdges(), FromVisibility, ToVisibility);
    SetConditionalVisibility<CModelFace>(pModelMesh->GetFaces(), FromVisibility, ToVisibility);
    SetConditionalVisibility<CModelComposedFace>(pModelMesh->GetComposedFaces(), FromVisibility, ToVisibility);

    UpdateTreeItems();
}

template <typename GenericModelElement> void SetConditionalIndexability(const std::map<int, GenericModelElement*>& Collection)
{
    typename std::map<int, GenericModelElement*>::const_iterator EndCollection = Collection.end();

    for (typename std::map<int, GenericModelElement*>::const_iterator ppModelElement = Collection.begin() ; ppModelElement != EndCollection ; ++ppModelElement)
        if (ppModelElement->second->GetVisibility(false) == CModelElement::eStaticOccluded)
        {
            ppModelElement->second->SetIndexable(false);
        }
}

void VRME::IndexabilityConsistencyCheck(CModelMesh* pModelMesh)
{
    if (!pModelMesh)
    {
        return;
    }

    SetConditionalIndexability<CModelVertex>(pModelMesh->GetVertices());
    SetConditionalIndexability<CModelEdge>(pModelMesh->GetEdges());
    SetConditionalIndexability<CModelFace>(pModelMesh->GetFaces());
    SetConditionalIndexability<CModelComposedFace>(pModelMesh->GetComposedFaces());

    UpdateTreeItems();
}

void VRME::UpdateTreeItems()
{
    const int TotalTopLevels = ui->modelTreeWidget->topLevelItemCount();

    for (int i = 0; i < TotalTopLevels; ++i)
    {
        std::deque<CModelTreeWidgetItem*> Traversal;
        Traversal.push_back(dynamic_cast<CModelTreeWidgetItem*>(ui->modelTreeWidget->topLevelItem(i)));

        while (Traversal.size())
        {
            CModelTreeWidgetItem* pItem = Traversal.front();
            Traversal.pop_front();
            const int TotalChildren = pItem->childCount();

            for (int j = 0; j < TotalChildren; ++j)
            {
                CModelTreeWidgetItem* pSubItem = dynamic_cast<CModelTreeWidgetItem*>(pItem->child(j));
                pSubItem->Update(nullptr);
                Traversal.push_back(pSubItem);
            }
        }
    }
}

void VRME::on_minimalDisplacementLineEdit_textChanged(const QString& Arg)
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
    CModelTransformation* pGenericTransformation = pModelMesh->GetTransformation();
    bool OK = false;
    const double Value = Arg.toDouble(&OK);

    if (OK)
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eTranslation))
        {
            CModelTranslation* pModelTranslation = dynamic_cast<CModelTranslation*>(pGenericTransformation);
            const bool HasChanged = RVL::IsNonZero(Real(Value) - pModelTranslation->GetMinimalDisplacement());

            if (HasChanged)
            {
                if (IsPanelContentTranslationTransformationReady(true))
                {
                    ui->pushButtonModifyTranslationTransformation->setEnabled(HasChanged);
                }
                else
                {
                    ui->pushButtonModifyTranslationTransformation->setEnabled(false);
                    ui->minimalDisplacementLineEdit->setText(QString("%1").arg(pModelTranslation->GetMinimalDisplacement()));
                }
            }
            else
            {
                ui->pushButtonModifyTranslationTransformation->setEnabled(HasChanged);
            }
        }
        else
        {
            const bool TransformationReady = IsPanelContentTranslationTransformationReady(false);
            ui->pushButtonCreateTranslationTransformation->setEnabled(TransformationReady);

            if (TransformationReady)
            {
                const double MinimalDisplacement = ui->minimalDisplacementLineEdit->text().toDouble();
                const double MaximalDisplacement = ui->maximalDisplacementLineEdit->text().toDouble();
                const double DeafultDisplacement = ui->defaultDisplacementLineEdit->text().toDouble();
                ui->horizontalSliderTranslationDoF->setRange(MinimalDisplacement, MaximalDisplacement);
                ui->horizontalSliderTranslationDoF->setValue(DeafultDisplacement);
                ui->horizontalSliderTranslationDoF->setEnabled(TransformationReady && pGenericTransformation);
            }
        }
    }
    else
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eTranslation))
        {
            CModelTranslation* pModelTranslation = dynamic_cast<CModelTranslation*>(pGenericTransformation);
            ui->minimalDisplacementLineEdit->setText(QString("%1").arg(pModelTranslation->GetMinimalDisplacement()));
        }
        else
        {
            ui->minimalDisplacementLineEdit->clear();
        }
    }
}

void VRME::on_maximalDisplacementLineEdit_textChanged(const QString& Arg)
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
    CModelTransformation* pGenericTransformation = pModelMesh->GetTransformation();
    bool OK = false;
    const double Value = Arg.toDouble(&OK);

    if (OK)
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eTranslation))
        {
            CModelTranslation* pModelTranslation = dynamic_cast<CModelTranslation*>(pGenericTransformation);
            const bool HasChanged = RVL::IsNonZero(Real(Value) - pModelTranslation->GetMaximalDisplacement());

            if (HasChanged)
            {
                if (IsPanelContentTranslationTransformationReady(true))
                {
                    ui->pushButtonModifyTranslationTransformation->setEnabled(HasChanged);
                }
                else
                {
                    ui->pushButtonModifyTranslationTransformation->setEnabled(false);
                    ui->maximalDisplacementLineEdit->setText(QString("%1").arg(pModelTranslation->GetMaximalDisplacement()));
                }
            }
            else
            {
                ui->pushButtonModifyTranslationTransformation->setEnabled(HasChanged);
            }
        }
        else
        {
            const bool TransformationReady = IsPanelContentTranslationTransformationReady(false);
            ui->pushButtonCreateTranslationTransformation->setEnabled(TransformationReady);

            if (TransformationReady)
            {
                const double MinimalDisplacement = ui->minimalDisplacementLineEdit->text().toDouble();
                const double MaximalDisplacement = ui->maximalDisplacementLineEdit->text().toDouble();
                const double DeafultDisplacement = ui->defaultDisplacementLineEdit->text().toDouble();
                ui->horizontalSliderTranslationDoF->setRange(MinimalDisplacement, MaximalDisplacement);
                ui->horizontalSliderTranslationDoF->setValue(DeafultDisplacement);
                ui->horizontalSliderTranslationDoF->setEnabled(TransformationReady && pGenericTransformation);
            }
        }
    }
    else
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eTranslation))
        {
            CModelTranslation* pModelTranslation = dynamic_cast<CModelTranslation*>(pGenericTransformation);
            ui->maximalDisplacementLineEdit->setText(QString("%1").arg(pModelTranslation->GetMaximalDisplacement()));
        }
        else
        {
            ui->maximalDisplacementLineEdit->clear();
        }
    }
}

void VRME::on_defaultDisplacementLineEdit_textChanged(const QString& Arg)
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
    CModelTransformation* pGenericTransformation = pModelMesh->GetTransformation();
    bool OK = false;
    const double Value = Arg.toDouble(&OK);

    if (OK)
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eTranslation))
        {
            CModelTranslation* pModelTranslation = dynamic_cast<CModelTranslation*>(pGenericTransformation);
            const bool HasChanged = RVL::IsNonZero(Real(Value) - pModelTranslation->GetDefaultDisplacement());

            if (HasChanged)
            {
                if (IsPanelContentTranslationTransformationReady(true))
                {
                    ui->pushButtonModifyTranslationTransformation->setEnabled(HasChanged);
                }
                else
                {
                    ui->pushButtonModifyTranslationTransformation->setEnabled(false);
                    ui->defaultDisplacementLineEdit->setText(QString("%1").arg(pModelTranslation->GetDefaultDisplacement()));
                }
            }
            else
            {
                ui->pushButtonModifyTranslationTransformation->setEnabled(HasChanged);
            }
        }
        else
        {
            const bool TransformationReady = IsPanelContentTranslationTransformationReady(false);
            ui->pushButtonCreateTranslationTransformation->setEnabled(TransformationReady);

            if (TransformationReady)
            {
                const double MinimalDisplacement = ui->minimalDisplacementLineEdit->text().toDouble();
                const double MaximalDisplacement = ui->maximalDisplacementLineEdit->text().toDouble();
                const double DeafultDisplacement = ui->defaultDisplacementLineEdit->text().toDouble();
                ui->horizontalSliderTranslationDoF->setRange(MinimalDisplacement, MaximalDisplacement);
                ui->horizontalSliderTranslationDoF->setValue(DeafultDisplacement);
                ui->horizontalSliderTranslationDoF->setEnabled(TransformationReady && pGenericTransformation);
            }
        }
    }
    else
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eTranslation))
        {
            CModelTranslation* pModelTranslation = dynamic_cast<CModelTranslation*>(pGenericTransformation);
            ui->defaultDisplacementLineEdit->setText(QString("%1").arg(pModelTranslation->GetDefaultDisplacement()));
        }
        else
        {
            ui->defaultDisplacementLineEdit->clear();
        }
    }
}

void VRME::on_pushButtonDestroyTranslationTransformation_clicked()
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());

    if (!pModelMesh)
    {
        return;
    }

    CModelTransformation* pModelTransformation = pModelMesh->GetTransformation();

    if (!(pModelTransformation && (pModelTransformation->GetTransformationType() == CModelTransformation::eTranslation)))
    {
        return;
    }

    pModelMesh->RemoveTransformation();


    if (!pModelTransformation->GetTotalMeshes())
    {
        m_pModelMultipleMeshes->DeleteTransformation(pModelTransformation);
    }

    updateTransformationPanel();
    updateAvailableTransformations();
}

void VRME::on_assignTransformationPushButton_clicked()
{
    int Id = -1;
    QList<QTableWidgetItem*> Items = ui->availableTransformationsTableWidget->selectedItems();

    for (int i = 0; i < Items.size(); ++i)
        if (Items.at(i)->column() == 0)
        {
            Id = Items.at(i)->text().toInt();
            break;
        }

    if (Id < 1)
    {
        return;
    }

    const std::map<int, CModelTransformation*>& Transformations = m_pModelMultipleMeshes->GetTransformations();

    std::map<int, CModelTransformation*>::const_iterator pKeyTransformation = Transformations.find(Id);

    if (pKeyTransformation == Transformations.end())
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());

    pModelMesh->SetTransformation(pKeyTransformation->second);

    updateTransformationPanel();

    updateAvailableTransformations();
}

void VRME::on_removeTransformationPushButton_clicked()
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());

    if (!pModelMesh)
    {
        return;
    }

    CModelTransformation* pModelTransformation = pModelMesh->GetTransformation();

    if (!pModelTransformation)
    {
        return;
    }

    pModelMesh->RemoveTransformation();

    if (!pModelTransformation->GetTotalMeshes())
    {
        m_pModelMultipleMeshes->DeleteTransformation(pModelTransformation);
    }


    updateTransformationPanel();
    updateAvailableTransformations();
}

void VRME::on_availableTransformationsTableWidget_itemSelectionChanged()
{
    if (!m_pModelMultipleMeshes)
    {
        return;
    }

    if (!m_pModelMultipleMeshes->HasTransformations())
    {
        return;
    }

    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());

    ui->assignTransformationPushButton->setEnabled(!pModelMesh->GetTransformation());
}

void VRME::on_transformationLabelLineEditTranslation_textChanged(const QString& Arg)
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
    CModelTransformation* pGenericTransformation = pModelMesh->GetTransformation();
    bool OK = !(Arg.contains(QChar('<')) || Arg.contains(QChar('>')) || Arg.contains(QChar('/')));

    if (OK)
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eTranslation))
        {
            CModelTranslation* pModelTranslation = dynamic_cast<CModelTranslation*>(pGenericTransformation);
            const bool HasChanged = Arg != QString(pModelTranslation->GetLabel().c_str());

            if (HasChanged)
            {

            }
            else
            {
                ui->pushButtonModifyTranslationTransformation->setEnabled(HasChanged);
            }
        }
        else
        {
            const bool TransformationReady = IsPanelContentTranslationTransformationReady(false);
            ui->pushButtonCreateTranslationTransformation->setEnabled(TransformationReady);

            if (TransformationReady)
            {
                const double MinimalDisplacement = ui->minimalDisplacementLineEdit->text().toDouble();
                const double MaximalDisplacement = ui->maximalDisplacementLineEdit->text().toDouble();
                const double DeafultDisplacement = ui->defaultDisplacementLineEdit->text().toDouble();
                ui->horizontalSliderTranslationDoF->setRange(MinimalDisplacement, MaximalDisplacement);
                ui->horizontalSliderTranslationDoF->setValue(DeafultDisplacement);
                ui->horizontalSliderTranslationDoF->setEnabled(TransformationReady && pGenericTransformation);
            }
        }
    }
    else
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eTranslation))
        {
            CModelTranslation* pModelTranslation = dynamic_cast<CModelTranslation*>(pGenericTransformation);
            ui->minimalDisplacementLineEdit->setText(QString(pModelTranslation->GetLabel().c_str()));
        }
        else
        {
            ui->minimalDisplacementLineEdit->clear();
        }
    }
}

void VRME::on_saveCurrentFilePushButton_clicked()
{
    if (m_pModelMultipleMeshes && m_CurrentFile.length())
    {
        if (m_pModelMultipleMeshes->SaveToFile(m_CurrentFile.toLatin1().data(), true))
        {
            addInformationToLog(tr("Save Succesfull: ") + m_CurrentFile);
        }
        else
        {
            addInformationToLog(tr("Save Failure: ") + m_CurrentFile);
        }
    }
}

void VRME::on_automaticComboBox_currentIndexChanged(int index)
{
    ui->automaticOperationApplyPushButton->setEnabled(index);
}

void VRME::on_automaticOperationApplyPushButton_clicked()
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());

    if (!pModelMesh)
    {
        return;
    }

    switch (ui->automaticComboBox->currentIndex())
    {
        case 0://Nonde
            break;

        case 1://Mesh [Dynamic Visibility Consistency Check]
            DynamicVisibilityConsistencyCheck(pModelMesh);
            break;

        case 2://Mesh [Indexability Consistency Check]
            IndexabilityConsistencyCheck(pModelMesh);
            break;
    }
}

void VRME::on_pushButton_clicked()
{
    if (m_pModelMultipleMeshes)
    {

        ui->queryTextEdit->setText(QString("Run.."));

#ifdef SERILIZE_QUERY
        CXmlNode Information("Query");
#endif

        const std::map<int, CModelMesh*>& Meshes = m_pModelMultipleMeshes->GetMeshes();
        std::map<int, CModelMesh*> DynamicMeshes;

        for (std::map<int, CModelMesh*>::const_iterator pKeyMesh = Meshes.begin(); pKeyMesh != Meshes.end(); ++pKeyMesh)
            if (pKeyMesh->second->IsDynamic())
            {
                DynamicMeshes.insert(std::pair<int, CModelMesh*>(pKeyMesh->first, pKeyMesh->second));
            }


        const int64_t T0 = RVL::Time::CTimeEntry::Now();

        std::map<int, CModelMesh*>::const_iterator EndMeshes = DynamicMeshes.end();

        for (std::map<int, CModelMesh*>::const_iterator pKeyMesh = DynamicMeshes.begin(); pKeyMesh != EndMeshes; ++pKeyMesh)
        {

#ifdef SERILIZE_QUERY

            CModelMesh* pMesh = pKeyMesh->second;
            const std::map<int, CModelEdge*>& Edges = pMesh->GetEdges();
            const std::map<int, CModelFace*>& Faces = pMesh->GetFaces();

#else

            const std::map<int, CModelEdge*>& Edges = pKeyMesh->second->GetEdges();
            const std::map<int, CModelFace*>& Faces = pKeyMesh->second->GetFaces();
#endif

#ifdef SERILIZE_QUERY
            CXmlNode* pXmlDynamicMesh  = Information.CreateSubNode("DynamicMesh");
            CXmlNode* pXmlDynamicEdges  = pXmlDynamicMesh->CreateSubNode("Edges");
            CXmlNode* pXmlDynamicFaces  = pXmlDynamicMesh->CreateSubNode("Faces");
            pXmlDynamicMesh->SetAttribute("ID", pMesh->GetInstanceId());
            pXmlDynamicMesh->SetAttribute("TransformationId", pMesh->GetTransformation()->GetInstanceId());
            pXmlDynamicMesh->SetAttribute("TransformationType", CModelTransformation::TransformationTypeToString(pMesh->GetTransformation()->GetTransformationType()));
#endif

            Mathematics::_3D::CVector3D PointA, PointB, PointC;
            std::map<int, CModelEdge*>::const_iterator EndEdges = Edges.end();

            for (std::map<int, CModelEdge*>::const_iterator pKeyEdge = Edges.begin(); pKeyEdge != EndEdges; ++pKeyEdge)
                if (pKeyEdge->second->GetVisibility(false) == CModelElement::eDynamicVisible)
                {
#ifdef SERILIZE_QUERY

                    CModelEdge* pEdge = pKeyEdge->second;
                    CModelVertex* pVertexA = pEdge->GetWritableVertexA();
                    CModelVertex* pVertexB = pEdge->GetWritableVertexB();
                    Mathematics::_3D::CVector3D StaticA = pVertexA->GetPoint(false);
                    Mathematics::_3D::CVector3D StaticB = pVertexB->GetPoint(false);

#else

                    pKeyEdge->second->GetDynamicEndPoints(PointA, PointB);
                    //Mathematics::_3D::CVector3D DynamicA = pKeyEdge->second->GetWritableVertexA()->GetDynamicPoint();
                    //Mathematics::_3D::CVector3D DynamicB = pKeyEdge->second->GetWritableVertexB()->GetDynamicPoint();

#endif

#ifdef SERILIZE_QUERY
                    CXmlNode* pXmlDynamicEdge  = pXmlDynamicEdges->CreateSubNode("Edge");
                    CXmlNode* pXmlDynamicVertexA  = pXmlDynamicEdge->CreateSubNode("Vertex");
                    CXmlNode* pXmlDynamicVertexB  = pXmlDynamicEdge->CreateSubNode("Vertex");
                    pXmlDynamicEdge->SetAttribute("ID", pEdge->GetInstanceId());
                    pXmlDynamicVertexA->SetAttribute("ID", pVertexA->GetInstanceId());
                    pXmlDynamicVertexA->SetAttribute("Side", "A");
                    pXmlDynamicVertexA->SetAttribute("SX", StaticA.GetX());
                    pXmlDynamicVertexA->SetAttribute("SY", StaticA.GetY());
                    pXmlDynamicVertexA->SetAttribute("SZ", StaticA.GetZ());
                    pXmlDynamicVertexA->SetAttribute("DX", DynamicA.GetX());
                    pXmlDynamicVertexA->SetAttribute("DY", DynamicA.GetY());
                    pXmlDynamicVertexA->SetAttribute("DZ", DynamicA.GetZ());
                    pXmlDynamicVertexB->SetAttribute("ID", pVertexB->GetInstanceId());
                    pXmlDynamicVertexB->SetAttribute("Side", "B");
                    pXmlDynamicVertexB->SetAttribute("SX", StaticB.GetX());
                    pXmlDynamicVertexB->SetAttribute("SY", StaticB.GetY());
                    pXmlDynamicVertexB->SetAttribute("SZ", StaticB.GetZ());
                    pXmlDynamicVertexB->SetAttribute("DX", DynamicB.GetX());
                    pXmlDynamicVertexB->SetAttribute("DY", DynamicB.GetY());
                    pXmlDynamicVertexB->SetAttribute("DZ", DynamicB.GetZ());
#endif
                }

            std::map<int, CModelFace*>::const_iterator EndFace = Faces.end();

            for (std::map<int, CModelFace*>::const_iterator pKeyFace = Faces.begin(); pKeyFace != EndFace; ++pKeyFace)
                if (pKeyFace->second->GetVisibility(false) == CModelElement::eDynamicVisible)
                {
#ifdef SERILIZE_QUERY

                    CModelFace* pFace = pKeyFace->second;
                    CModelOrientedVertex* pOrientedVertexA = pFace->GetWritableOrientedVertexA();
                    CModelOrientedVertex* pOrientedVertexB = pFace->GetWritableOrientedVertexB();
                    CModelOrientedVertex* pOrientedVertexC = pFace->GetWritableOrientedVertexC();
                    CModelVertex* pVertexA = pOrientedVertexA->GetWritableModelVertex();
                    CModelVertex* pVertexB = pOrientedVertexB->GetWritableModelVertex();
                    CModelVertex* pVertexC = pOrientedVertexC->GetWritableModelVertex();
                    Mathematics::_3D::CVector3D StaticA = pVertexA->GetPoint(false);
                    Mathematics::_3D::CVector3D StaticB = pVertexB->GetPoint(false);
                    Mathematics::_3D::CVector3D StaticC = pVertexC->GetPoint(false);
                    Mathematics::_3D::CVector3D DynamicA = pVertexA->GetDynamicPoint(true);
                    Mathematics::_3D::CVector3D DynamicB = pVertexB->GetDynamicPoint(true);
                    Mathematics::_3D::CVector3D DynamicC = pVertexC->GetDynamicPoint(true);

#else

                    pKeyFace->second->GetDynamicEndPoints(PointA, PointB, PointC);

                    //Mathematics::_3D::CVector3D DynamicA = pKeyFace->second->GetWritableOrientedVertexA()->GetWritableModelVertex()->GetDynamicPoint();
                    //Mathematics::_3D::CVector3D DynamicB = pKeyFace->second->GetWritableOrientedVertexB()->GetWritableModelVertex()->GetDynamicPoint();
                    //Mathematics::_3D::CVector3D DynamicC = pKeyFace->second->GetWritableOrientedVertexC()->GetWritableModelVertex()->GetDynamicPoint();

#endif


#ifdef SERILIZE_QUERY
                    CXmlNode* pXmlDynamicFace  = pXmlDynamicFaces->CreateSubNode("Edge");
                    CXmlNode* pXmlDynamicVertexA  = pXmlDynamicFace->CreateSubNode("Vertex");
                    CXmlNode* pXmlDynamicVertexB  = pXmlDynamicFace->CreateSubNode("Vertex");
                    CXmlNode* pXmlDynamicVertexC  = pXmlDynamicFace->CreateSubNode("Vertex");
                    pXmlDynamicFace->SetAttribute("ID", pFace->GetInstanceId());
                    pXmlDynamicVertexA->SetAttribute("ID", pVertexA->GetInstanceId());
                    pXmlDynamicVertexA->SetAttribute("Side", "A");
                    pXmlDynamicVertexA->SetAttribute("SX", StaticA.GetX());
                    pXmlDynamicVertexA->SetAttribute("SY", StaticA.GetY());
                    pXmlDynamicVertexA->SetAttribute("SZ", StaticA.GetZ());
                    pXmlDynamicVertexA->SetAttribute("DX", DynamicA.GetX());
                    pXmlDynamicVertexA->SetAttribute("DY", DynamicA.GetY());
                    pXmlDynamicVertexA->SetAttribute("DZ", DynamicA.GetZ());
                    pXmlDynamicVertexA->SetAttribute("ID", pVertexB->GetInstanceId());
                    pXmlDynamicVertexB->SetAttribute("Side", "B");
                    pXmlDynamicVertexB->SetAttribute("SX", StaticB.GetX());
                    pXmlDynamicVertexB->SetAttribute("SY", StaticB.GetY());
                    pXmlDynamicVertexB->SetAttribute("SZ", StaticB.GetZ());
                    pXmlDynamicVertexB->SetAttribute("DX", DynamicB.GetX());
                    pXmlDynamicVertexB->SetAttribute("DY", DynamicB.GetY());
                    pXmlDynamicVertexB->SetAttribute("DZ", DynamicB.GetZ());
                    pXmlDynamicVertexC->SetAttribute("ID", pVertexC->GetInstanceId());
                    pXmlDynamicVertexC->SetAttribute("Side", "C");
                    pXmlDynamicVertexA->SetAttribute("SX", StaticC.GetX());
                    pXmlDynamicVertexC->SetAttribute("SY", StaticC.GetY());
                    pXmlDynamicVertexC->SetAttribute("SZ", StaticC.GetZ());
                    pXmlDynamicVertexC->SetAttribute("DX", DynamicC.GetX());
                    pXmlDynamicVertexC->SetAttribute("DY", DynamicC.GetY());
                    pXmlDynamicVertexC->SetAttribute("DZ", DynamicC.GetZ());
#endif
                }
        }

        const int64_t T1 = RVL::Time::CTimeEntry::Now();

#ifdef SERILIZE_QUERY

        Information.SetAttribute("ElapsedTime", Real(T1 - T0) / Real(1000.0));
        ui->queryTextEdit->setText(QString(CXmlNode::FormatAddTabs(Information.ToString()).c_str()));
#else
        ui->queryTextEdit->setText(QString("%1").arg(Real(T1 - T0) / Real(1000.0)));
#endif

    }
    else
    {
        ui->queryTextEdit->setText(QString("Loaded model First."));
    }
}

void VRME::on_modelTreeWidget_customContextMenuRequested(const QPoint& PointerLocation)
{
    CModelTreeWidgetItem* pItem = dynamic_cast<CModelTreeWidgetItem*>(ui->modelTreeWidget->itemAt(PointerLocation));

    if (!pItem)
    {
        return;
    }

    CModelElement* pModelElement = pItem->GetGenericModelElement();

    if (pModelElement) //Element
    {
        if (pModelElement->GetTypeId() == CModelElement::eMesh)
        {
            QMenu* pContextualMenu = new QMenu(this);
            QAction* pDeleteMeshAction = new QAction(QIcon(":/MeshIcon.png"), QString("Delete"), pContextualMenu);
            pContextualMenu->addAction(pDeleteMeshAction);
            pDeleteMeshAction->setShortcuts(QKeySequence::Delete);
            pDeleteMeshAction->setStatusTip(tr("Delete this element"));
            QAction* pSelectedAction = pContextualMenu->exec(ui->modelTreeWidget->mapToGlobal(PointerLocation));

            if (pSelectedAction == pDeleteMeshAction)
            {
                DeleteMesh((CModelMesh*)pModelElement);
            }
        }

    }
    else //Set
    {

    }
}


void VRME::on_transformationLabelLineEditRotation_textChanged(const QString& Arg)
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
    CModelTransformation* pGenericTransformation = pModelMesh->GetTransformation();
    bool OK = !(Arg.contains(QChar('<')) || Arg.contains(QChar('>')) || Arg.contains(QChar('/')));

    if (OK)
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eRotation))
        {
            CModelRotation* pModelRotation = dynamic_cast<CModelRotation*>(pGenericTransformation);
            const bool HasChanged = Arg != QString(pModelRotation->GetLabel().c_str());

            if (HasChanged)
            {

            }
            else
            {
                ui->pushButtonModifyRotationTransformation->setEnabled(HasChanged);
            }
        }
        else
        {
            const bool TransformationReady = IsPanelContentRotationTransformationReady(false);
            ui->pushButtonCreatRotationTransformation->setEnabled(TransformationReady);

            if (TransformationReady)
            {
                const double MinimalRotationInDegrees = ui->minimalRotationLineEdit->text().toDouble();
                const double MaximalRotationInDegrees = ui->maximalRotationLineEdit->text().toDouble();
                const double DeafultRotationInDegrees = ui->defaultRotationLineEdit->text().toDouble();
                ui->horizontalSliderRotationDoF->setRange(MinimalRotationInDegrees, MaximalRotationInDegrees);
                ui->horizontalSliderRotationDoF->setValue(DeafultRotationInDegrees);
                ui->horizontalSliderRotationDoF->setEnabled(TransformationReady && pGenericTransformation);
            }
        }
    }
    else
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eRotation))
        {
            CModelRotation* pModelRotation = dynamic_cast<CModelRotation*>(pGenericTransformation);
            ui->minimalRotationLineEdit->setText(QString(pModelRotation->GetLabel().c_str()));
        }
        else
        {
            ui->minimalRotationLineEdit->clear();
        }
    }
}

void VRME::on_minimalRotationLineEdit_textChanged(const QString& Arg)
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());

    CModelTransformation* pGenericTransformation = pModelMesh->GetTransformation();

    bool OK = false;
    const double Value = Arg.toDouble(&OK);

    if (OK)
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eRotation))
        {
            CModelRotation* pModelRotation = dynamic_cast<CModelRotation*>(pGenericTransformation);
            const bool HasChanged = RVL::IsNonZero(Real(Value) - (pModelRotation->GetMinimalRotation() * (180.0 / M_PI)));

            if (HasChanged)
            {
                if (IsPanelContentRotationTransformationReady(true))
                {
                    ui->pushButtonModifyRotationTransformation->setEnabled(HasChanged);
                }
                else
                {
                    ui->pushButtonModifyRotationTransformation->setEnabled(false);
                    ui->minimalRotationLineEdit->setText(QString("%1").arg(pModelRotation->GetMinimalRotation() * (180.0 / M_PI)));
                }
            }
            else
            {
                ui->pushButtonModifyRotationTransformation->setEnabled(HasChanged);
            }
        }
        else
        {
            const bool TransformationReady = IsPanelContentRotationTransformationReady(false);
            ui->pushButtonCreatRotationTransformation->setEnabled(TransformationReady);

            if (TransformationReady)
            {
                const double MinimalRotationInDegrees = ui->minimalRotationLineEdit->text().toDouble();
                const double MaximalRotationInDegrees = ui->maximalRotationLineEdit->text().toDouble();
                const double DeafultRotationInDegrees = ui->defaultRotationLineEdit->text().toDouble();
                ui->horizontalSliderRotationDoF->setRange(MinimalRotationInDegrees, MaximalRotationInDegrees);
                ui->horizontalSliderRotationDoF->setValue(DeafultRotationInDegrees);
                ui->horizontalSliderRotationDoF->setEnabled(TransformationReady && pGenericTransformation);
            }
        }
    }
    else
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eRotation))
        {
            CModelRotation* pModelRotation = dynamic_cast<CModelRotation*>(pGenericTransformation);
            ui->minimalRotationLineEdit->setText(QString("%1").arg(pModelRotation->GetMinimalRotation() * (180.0 / M_PI)));
        }
        else
        {
            ui->minimalRotationLineEdit->clear();
        }
    }
}

void VRME::on_maximalRotationLineEdit_textChanged(const QString& Arg)
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
    CModelTransformation* pGenericTransformation = pModelMesh->GetTransformation();
    bool OK = false;
    const double Value = Arg.toDouble(&OK);

    if (OK)
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eRotation))
        {
            CModelRotation* pModelRotation = dynamic_cast<CModelRotation*>(pGenericTransformation);
            const bool HasChanged = RVL::IsNonZero(Real(Value) - (pModelRotation->GetMaximalRotation() * (180.0 / M_PI)));

            if (HasChanged)
            {
                if (IsPanelContentRotationTransformationReady(true))
                {
                    ui->pushButtonModifyRotationTransformation->setEnabled(HasChanged);
                }
                else
                {
                    ui->pushButtonModifyRotationTransformation->setEnabled(false);
                    ui->maximalRotationLineEdit->setText(QString("%1").arg(pModelRotation->GetMaximalRotation() * (180.0 / M_PI)));
                }
            }
            else
            {
                ui->pushButtonModifyRotationTransformation->setEnabled(HasChanged);
            }


        }
        else
        {
            const bool TransformationReady = IsPanelContentRotationTransformationReady(false);
            ui->pushButtonCreatRotationTransformation->setEnabled(TransformationReady);

            if (TransformationReady)
            {
                const double MinimalRotationInDegrees = ui->minimalRotationLineEdit->text().toDouble();
                const double MaximalRotationInDegrees = ui->maximalRotationLineEdit->text().toDouble();
                const double DeafultRotationInDegrees = ui->defaultRotationLineEdit->text().toDouble();
                ui->horizontalSliderRotationDoF->setRange(MinimalRotationInDegrees, MaximalRotationInDegrees);
                ui->horizontalSliderRotationDoF->setValue(DeafultRotationInDegrees);
                ui->horizontalSliderRotationDoF->setEnabled(TransformationReady && pGenericTransformation);
            }
        }
    }
    else
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eRotation))
        {
            CModelRotation* pModelRotation = dynamic_cast<CModelRotation*>(pGenericTransformation);
            ui->maximalRotationLineEdit->setText(QString("%1").arg(pModelRotation->GetMaximalRotation() * (180.0 / M_PI)));
        }
        else
        {
            ui->maximalRotationLineEdit->clear();
        }
    }
}

void VRME::on_defaultRotationLineEdit_textChanged(const QString& Arg)
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
    CModelTransformation* pGenericTransformation = pModelMesh->GetTransformation();
    bool OK = false;
    const double Value = Arg.toDouble(&OK);

    if (OK)
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eRotation))
        {
            CModelRotation* pModelRotation = dynamic_cast<CModelRotation*>(pGenericTransformation);
            const bool HasChanged = RVL::IsNonZero(Real(Value) - (pModelRotation->GetDefaultRotation() * (180.0 / M_PI)));
            ui->pushButtonModifyRotationTransformation->setEnabled(HasChanged);

            if (HasChanged)
            {
                if (IsPanelContentRotationTransformationReady(true))
                {
                    ui->pushButtonModifyRotationTransformation->setEnabled(HasChanged);
                }
                else
                {
                    ui->pushButtonModifyRotationTransformation->setEnabled(false);
                    ui->defaultRotationLineEdit->setText(QString("%1").arg(pModelRotation->GetDefaultRotation() * (180.0 / M_PI)));
                }
            }
            else
            {
                ui->pushButtonModifyRotationTransformation->setEnabled(HasChanged);
            }
        }
        else
        {
            const bool TransformationReady = IsPanelContentRotationTransformationReady(false);
            ui->pushButtonCreatRotationTransformation->setEnabled(TransformationReady);

            if (TransformationReady)
            {
                const double MinimalRotation = ui->minimalRotationLineEdit->text().toDouble();
                const double MaximalRotation = ui->maximalRotationLineEdit->text().toDouble();
                const double DeafultRotation = ui->defaultRotationLineEdit->text().toDouble();
                ui->horizontalSliderRotationDoF->setRange(MinimalRotation, MaximalRotation);
                ui->horizontalSliderRotationDoF->setValue(DeafultRotation);
                ui->horizontalSliderRotationDoF->setEnabled(TransformationReady && pGenericTransformation);
            }
        }
    }
    else
    {
        if (pGenericTransformation && (pGenericTransformation->GetTransformationType() == CModelTransformation::eRotation))
        {
            CModelRotation* pModelRotation = dynamic_cast<CModelRotation*>(pGenericTransformation);
            ui->defaultRotationLineEdit->setText(QString("%1").arg(pModelRotation->GetDefaultRotation() * (180.0 / M_PI)));
        }
        else
        {
            ui->defaultRotationLineEdit->clear();
        }
    }
}

void VRME::on_pushButtonCreatRotationTransformation_clicked()
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());

    CModelRotation* pModelRotation = nullptr;

    const Real MinimalRotationInDegrees = Real(ui->minimalRotationLineEdit->text().toDouble());
    const Real MaximalRotationInDegrees = Real(ui->maximalRotationLineEdit->text().toDouble());
    const Real DefaultRotationInDegrees = Real(ui->defaultRotationLineEdit->text().toDouble());
    const Real CurrentNormalizedRotation = (Real(ui->horizontalSliderRotationDoF->value()) - MinimalRotationInDegrees) / (MaximalRotationInDegrees - MinimalRotationInDegrees);

    const Real MinimalRotation = MinimalRotationInDegrees * (M_PI / 180.0);
    const Real MaximalRotation = MaximalRotationInDegrees * (M_PI / 180.0);
    const Real DefaultRotation = DefaultRotationInDegrees * (M_PI / 180.0);

    if (ui->vertexABCheckBoxRotation->isChecked())
    {
        QString TextIdA = ui->SourceComboBoxRotationA->currentText();
        QString TextIdB = ui->SourceComboBoxRotationB->currentText();
        const CModelVertex* pModelVertexA = pModelMesh->GetVertexById(GetIdFromTag(TextIdA.toAscii(), TextIdA.length()));
        const CModelVertex* pModelVertexB = pModelMesh->GetVertexById(GetIdFromTag(TextIdB.toAscii(), TextIdB.length()));
        pModelRotation = new CModelRotation(pModelMesh, pModelVertexA, pModelVertexB, MinimalRotation, MaximalRotation, DefaultRotation, CurrentNormalizedRotation);
    }
    else if (ui->edgeACheckBoxRotation->isChecked())
    {
        QString TextIdA = ui->SourceComboBoxRotationA->currentText();
        const CModelEdge* pModelEdge = pModelMesh->GetEdgeById(GetIdFromTag(TextIdA.toAscii(), TextIdA.length()));
        pModelRotation = new CModelRotation(pModelMesh, pModelEdge, MinimalRotation, MaximalRotation, DefaultRotation, CurrentNormalizedRotation);
    }

    if (!pModelRotation)
    {
        return;
    }

    QString Label = ui->transformationLabelLineEditRotation->text();
    pModelRotation->SetLabel(std::string(Label.toAscii().data()));

    //if(!pModelRotation)
    //    return;

    CModelMultipleMesh* pModelMultipleMesh = dynamic_cast<CModelMultipleMesh*>(pModelMesh->GetSuperior());

    if (!pModelMultipleMesh)
    {
        return;
    }

    pModelMesh->SetTransformation(pModelRotation);
    pModelMultipleMesh->AddTransformation(pModelRotation);

    updateTransformationPanel();
    updateAvailableTransformations();
}

void VRME::on_pushButtonModifyRotationTransformation_clicked()
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    const Real MinimalRotation = Real(ui->minimalRotationLineEdit->text().toDouble());
    const Real MaximalRotation = Real(ui->maximalRotationLineEdit->text().toDouble());
    const Real DefaultRotation = Real(ui->defaultRotationLineEdit->text().toDouble());
    const Real CurrentNormalizedRotation = (Real(ui->horizontalSliderRotationDoF->value()) - MinimalRotation) / (MaximalRotation - MinimalRotation);

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());

    CModelRotation* pModelRotation = dynamic_cast<CModelRotation*>(pModelMesh->GetTransformation());

    pModelRotation->Set(MinimalRotation * (Real(M_PI) / Real(180.0)), MaximalRotation * (Real(M_PI) / Real(180.0)), DefaultRotation * (Real(M_PI) / Real(180.0)), CurrentNormalizedRotation);

    QString Label = ui->transformationLabelLineEditRotation->text();
    pModelRotation->SetLabel(std::string(Label.toAscii().data()));

    updateTransformationPanel();
    updateAvailableTransformations();
}

void VRME::on_pushButtonDestroyRotationTransformation_clicked()
{
    if (!m_pSelectedpOpenInventorModelMesh)
    {
        return;
    }

    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());

    if (!pModelMesh)
    {
        return;
    }

    CModelTransformation* pModelTransformation = pModelMesh->GetTransformation();

    if (!(pModelTransformation && (pModelTransformation->GetTransformationType() == CModelTransformation::eRotation)))
    {
        return;
    }

    pModelMesh->RemoveTransformation();


    if (!pModelTransformation->GetTotalMeshes())
    {

        m_pModelMultipleMeshes->DeleteTransformation(pModelTransformation);
    }

    updateTransformationPanel();
    updateAvailableTransformations();
}

//Transformation: Rotation
//Vertex [AB]
void VRME::on_vertexABCheckBoxRotation_clicked(bool checked)
{
    ui->SourceComboBoxRotationA->setEnabled(checked);
    ui->SourceComboBoxRotationB->setEnabled(checked);
    ui->SourceComboBoxRotationC->setEnabled(!checked);
    ui->SourceComboBoxRotationD->setEnabled(!checked);

    ui->SourceComboBoxRotationA->clear();
    ui->SourceComboBoxRotationB->clear();
    ui->SourceComboBoxRotationC->clear();
    ui->SourceComboBoxRotationD->clear();

    if (checked)
    {
        QList<CModelTreeWidgetItem*> VertexItems = FilterItems(ui->modelTreeWidget->selectedItems(), 0X1); //eVertex = 0X1

        for (int i = 0; i < VertexItems.size(); ++i)
        {
            QString Item = QString("[Vertex:%1]").arg(VertexItems.at(i)->GetGenericModelElement()->GetInstanceId());
            ui->SourceComboBoxRotationA->addItem(Item);
            ui->SourceComboBoxRotationB->addItem(Item);
        }
    }
}

//Transformation: Rotation
//Vertex [AB]-[CD]
void VRME::on_vertexABCDCheckBoxRotation_clicked(bool checked)
{
    ui->SourceComboBoxRotationA->setEnabled(checked);
    ui->SourceComboBoxRotationB->setEnabled(checked);
    ui->SourceComboBoxRotationC->setEnabled(checked);
    ui->SourceComboBoxRotationD->setEnabled(checked);

    ui->SourceComboBoxRotationA->clear();
    ui->SourceComboBoxRotationB->clear();
    ui->SourceComboBoxRotationC->clear();
    ui->SourceComboBoxRotationD->clear();

    if (checked)
    {
        QList<CModelTreeWidgetItem*> VertexItems = FilterItems(ui->modelTreeWidget->selectedItems(), 0X1); //eVertex = 0X1

        for (int i = 0; i < VertexItems.size(); ++i)
        {
            QString Item = QString("[Vertex:%1]").arg(VertexItems.at(i)->GetGenericModelElement()->GetInstanceId());
            ui->SourceComboBoxRotationA->addItem(Item);
            ui->SourceComboBoxRotationB->addItem(Item);
            ui->SourceComboBoxRotationC->addItem(Item);
            ui->SourceComboBoxRotationD->addItem(Item);
        }
    }
}

//Transformation: Rotation
//Edge [A]
void VRME::on_edgeACheckBoxRotation_clicked(bool checked)
{
    ui->SourceComboBoxRotationA->setEnabled(checked);
    ui->SourceComboBoxRotationB->setEnabled(!checked);
    ui->SourceComboBoxRotationC->setEnabled(!checked);
    ui->SourceComboBoxRotationD->setEnabled(!checked);

    ui->SourceComboBoxRotationA->clear();
    ui->SourceComboBoxRotationB->clear();
    ui->SourceComboBoxRotationC->clear();
    ui->SourceComboBoxRotationD->clear();

    if (checked)
    {
        QList<CModelTreeWidgetItem*> EdgeItems = FilterItems(ui->modelTreeWidget->selectedItems(), 0X4); //eEdge = 0X4

        for (int i = 0; i < EdgeItems.size(); ++i)
        {
            ui->SourceComboBoxRotationA->addItem(QString("[Edge:%1]").arg(EdgeItems.at(i)->GetGenericModelElement()->GetInstanceId()));
        }
    }
}

//Transformation: Rotation
//Edge [A]-[B]
void VRME::on_edgeABCheckBoxRotation_clicked(bool checked)
{
    ui->SourceComboBoxRotationA->setEnabled(checked);
    ui->SourceComboBoxRotationB->setEnabled(checked);
    ui->SourceComboBoxRotationC->setEnabled(!checked);
    ui->SourceComboBoxRotationD->setEnabled(!checked);

    ui->SourceComboBoxRotationA->clear();
    ui->SourceComboBoxRotationB->clear();
    ui->SourceComboBoxRotationC->clear();
    ui->SourceComboBoxRotationD->clear();

    if (checked)
    {
        QList<CModelTreeWidgetItem*> EdgeItems = FilterItems(ui->modelTreeWidget->selectedItems(), 0X4); //eEdge = 0X4

        for (int i = 0; i < EdgeItems.size(); ++i)
        {
            QString Item = QString("[Edge:%1]").arg(EdgeItems.at(i)->GetGenericModelElement()->GetInstanceId());
            ui->SourceComboBoxRotationA->addItem(Item);
            ui->SourceComboBoxRotationB->addItem(Item);
        }
    }
}

void VRME::on_SourceComboBoxRotationA_currentIndexChanged(const QString& /*Arg*/)
{
    ui->pushButtonCreatRotationTransformation->setEnabled(IsPanelContentRotationTransformationReady(false));
}

void VRME::on_SourceComboBoxRotationB_currentIndexChanged(const QString& /*Arg*/)
{
    ui->pushButtonCreatRotationTransformation->setEnabled(IsPanelContentRotationTransformationReady(false));
}

void VRME::on_SourceComboBoxRotationC_currentIndexChanged(const QString& /*Arg*/)
{
    ui->pushButtonCreatRotationTransformation->setEnabled(IsPanelContentRotationTransformationReady(false));
}

void VRME::on_SourceComboBoxRotationD_currentIndexChanged(const QString& /*Arg*/)
{
    ui->pushButtonCreatRotationTransformation->setEnabled(IsPanelContentRotationTransformationReady(false));
}

void VRME::on_horizontalSliderRotationDoF_sliderMoved(int Value)
{
    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
    CModelRotation* pModelRotation = dynamic_cast<CModelRotation*>(pModelMesh->GetTransformation());

    if (pModelRotation)
    {
        const Real MinimalRotation = Real(ui->horizontalSliderRotationDoF->minimum());
        const Real MaximalRotation = Real(ui->horizontalSliderRotationDoF->maximum());
        const Real RotationRange = MaximalRotation - MinimalRotation;
        const Real CurrentNormalizedRotation = (Real(Value) - MinimalRotation) / RotationRange;
        pModelRotation->SetParametricNormalizedRotation(CurrentNormalizedRotation, -1);
        ui->pushButtonModifyRotationTransformation->setEnabled(true);
    }
}

void VRME::on_saliencyWeightingComboBox_currentIndexChanged(int index)
{
    switch (index)
    {
        case 0:
        case 1:
            ui->saliencyWeightingLineEdit->setEnabled(false);
            break;

        default:
            ui->saliencyWeightingLineEdit->setEnabled(true);
            break;
    }
}

void VRME::on_saliencyWeightingLineEdit_textChanged(const QString& Arg)
{
    bool EnableButton = false;

    if (ui->saliencyWeightingComboBox->currentIndex() == 2)
    {
        if (Arg.length())
        {
            bool Ok;
            Arg.toDouble(&Ok);

            if (Ok)
            {
                EnableButton = true;
            }
        }
    }

    ui->saliencyWeightingOperationApplyPushButton->setEnabled(EnableButton);
}

void VRME::on_saliencyWeightingOperationApplyPushButton_clicked()
{
    uint TypeFlags = 0;

    for (int i = 0; i < 7; ++i)
        if (m_SaliencyManagers[i]->isChecked())
        {
            TypeFlags |= (0x1 << i);
        }

    QList<CModelTreeWidgetItem*> SelectedItems = FilterItems(ui->modelTreeWidget->selectedItems(), TypeFlags);
    const int TotalSelecteditems = SelectedItems.size();

    if (TotalSelecteditems)
    {
        bool Ok;
        const Real Saliency = ui->saliencyWeightingLineEdit->text().toDouble(&Ok);

        if (Ok)
            for (int i = 0; i < TotalSelecteditems; ++i)
            {
                CModelTreeWidgetItem* pItem = SelectedItems.at(i);
                pItem->GetGenericModelElement()->SetSaliencyWeight(Saliency);
                pItem->Update(m_ElementIcons);
            }
    }

    on_modelTreeWidget_itemSelectionChanged();
    //ActivateEnableOperationApplyPushButton();
}

void VRME::on_gridMainViewportDisplaySwitchCheckBox_clicked(bool /*checked*/)
{
    switch ((ui->gridMainViewportDisplaySwitchCheckBox->isChecked() ? 0x1 : 0x0) | (ui->UCSMainViewportDisplaySwitchCheckBox->isChecked() ? 0x2 : 0x0))
    {
        case 0:
            m_pMainGlobalPropsSwitch->whichChild.setValue(SO_SWITCH_NONE);
            break;

        case 1:
            m_pMainGlobalPropsSwitch->whichChild.setValue(0);
            break;

        case 2:
            m_pMainGlobalPropsSwitch->whichChild.setValue(1);
            break;

        case 3:
            m_pMainGlobalPropsSwitch->whichChild.setValue(SO_SWITCH_ALL);
            break;
    }
}

void VRME::on_gridSecondaryViewportDisplaySwitchCheckBox_clicked(bool /*checked*/)
{
    switch ((ui->gridSecondaryViewportDisplaySwitchCheckBox->isChecked() ? 0x1 : 0x0) | (ui->UCSSecondaryViewportDisplaySwitchCheckBox->isChecked() ? 0x2 : 0x0))
    {
        case 0:
            m_pSecondaryGlobalPropsSwitch->whichChild.setValue(SO_SWITCH_NONE);
            break;

        case 1:
            m_pSecondaryGlobalPropsSwitch->whichChild.setValue(0);
            break;

        case 2:
            m_pSecondaryGlobalPropsSwitch->whichChild.setValue(1);
            break;

        case 3:
            m_pSecondaryGlobalPropsSwitch->whichChild.setValue(SO_SWITCH_ALL);
            break;
    }
}

void VRME::on_UCSMainViewportDisplaySwitchCheckBox_clicked(bool /*checked*/)
{
    switch ((ui->gridMainViewportDisplaySwitchCheckBox->isChecked() ? 0x1 : 0x0) | (ui->UCSMainViewportDisplaySwitchCheckBox->isChecked() ? 0x2 : 0x0))
    {
        case 0:
            m_pMainGlobalPropsSwitch->whichChild.setValue(SO_SWITCH_NONE);
            break;

        case 1:
            m_pMainGlobalPropsSwitch->whichChild.setValue(0);
            break;

        case 2:
            m_pMainGlobalPropsSwitch->whichChild.setValue(1);
            break;

        case 3:
            m_pMainGlobalPropsSwitch->whichChild.setValue(SO_SWITCH_ALL);
            break;
    }
}

void VRME::on_UCSSecondaryViewportDisplaySwitchCheckBox_clicked(bool /*checked*/)
{
    switch ((ui->gridSecondaryViewportDisplaySwitchCheckBox->isChecked() ? 0x1 : 0x0) | (ui->UCSSecondaryViewportDisplaySwitchCheckBox->isChecked() ? 0x2 : 0x0))
    {
        case 0:
            m_pSecondaryGlobalPropsSwitch->whichChild.setValue(SO_SWITCH_NONE);
            break;

        case 1:
            m_pSecondaryGlobalPropsSwitch->whichChild.setValue(0);
            break;

        case 2:
            m_pSecondaryGlobalPropsSwitch->whichChild.setValue(1);
            break;

        case 3:
            m_pSecondaryGlobalPropsSwitch->whichChild.setValue(SO_SWITCH_ALL);
            break;
    }
}

const RVL::Mathematics::_3D::CVector3D InverReferencePoint(const bool Invert, const RVL::Mathematics::_3D::CVector3D& A, const RVL::Mathematics::_3D::CVector3D& Reference)
{
    if (Invert)
    {
        const RVL::Mathematics::_3D::CVector3D AD = (A - Reference) * -1.0;
        return Reference + AD;
    }
    else
    {
        return A;
    }
}

void VRME::on_GloablAligmentGenerateFramepushButton_clicked()
{
    ui->GloablAligmentApplyFramepushButton->setEnabled(false);

    QString TextIdBase = ui->GlobalAligmentSourceComboBoxBase->currentText();
    QString TextIdAxisX = ui->GlobalAligmentSourceComboBoxAxisX->currentText();
    QString TextIdAxisY = ui->GlobalAligmentSourceComboBoxAxisY->currentText();
    QString TextIdAxisZ = ui->GlobalAligmentSourceComboBoxAxisZ->currentText();
    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
    const CModelVertex* pModelVertexBase = pModelMesh->GetVertexById(GetIdFromTag(TextIdBase.toAscii(), TextIdBase.length()));
    const CModelVertex* pModelVertexAxisX = pModelMesh->GetVertexById(GetIdFromTag(TextIdAxisX.toAscii(), TextIdAxisX.length()));
    const CModelVertex* pModelVertexAxisY = pModelMesh->GetVertexById(GetIdFromTag(TextIdAxisY.toAscii(), TextIdAxisY.length()));
    const CModelVertex* pModelVertexAxisZ = pModelMesh->GetVertexById(GetIdFromTag(TextIdAxisZ.toAscii(), TextIdAxisZ.length()));
    const RVL::Mathematics::_3D::CVector3D& BasePoint = pModelVertexBase->GetPoint(true);
    const RVL::Mathematics::_3D::CVector3D& PointX = InverReferencePoint(ui->GlobalAligmentSourceChechBoxBoxAxisX->isChecked(), pModelVertexAxisX->GetPoint(true), BasePoint);
    const RVL::Mathematics::_3D::CVector3D& PointY = InverReferencePoint(ui->GlobalAligmentSourceChechBoxBoxAxisY->isChecked(), pModelVertexAxisY->GetPoint(true), BasePoint);
    const RVL::Mathematics::_3D::CVector3D& PointZ = InverReferencePoint(ui->GlobalAligmentSourceChechBoxBoxAxisZ->isChecked(), pModelVertexAxisZ->GetPoint(true), BasePoint);
    RVL::Mathematics::_4D::CMatrix4D Frame;

    ui->GloablAligmentTransfromTextEdit->clear();

    if (!RVL::Representation::ModelBased::GeometricGraph::Base::CModelMultipleMesh::CreateTransformation(BasePoint, PointX, PointY, PointZ, Frame))
    {
        ui->GloablAligmentTransfromTextEdit->append(QString("Transformation not possible using current set of points."));
        ui->GloablAligmentTransfromTextEdit->append(QString("Mesh Id: %1").arg(pModelMesh->GetInstanceId()));
        ui->GloablAligmentTransfromTextEdit->append(QString("\tBase vertex Id: [ %1 ] Point: [ %2, %3, %4 ]").arg(pModelVertexBase->GetInstanceId()).arg(BasePoint.GetX()).arg(BasePoint.GetY()).arg(BasePoint.GetZ()));
        ui->GloablAligmentTransfromTextEdit->append(QString("\tAxis X vertex Id: [ %1 ] Point: [ %2, %3, %4 ]").arg(pModelVertexAxisX->GetInstanceId()).arg(PointX.GetX()).arg(PointX.GetY()).arg(PointX.GetZ()));
        ui->GloablAligmentTransfromTextEdit->append(QString("\tAxis Y vertex Id: [ %1 ] Point: [ %2, %3, %4 ]").arg(pModelVertexAxisY->GetInstanceId()).arg(PointY.GetX()).arg(PointY.GetY()).arg(PointY.GetZ()));
        ui->GloablAligmentTransfromTextEdit->append(QString("\tAxis Z vertex Id: [ %1 ] Point: [ %2, %3, %4 ]").arg(pModelVertexAxisZ->GetInstanceId()).arg(PointZ.GetX()).arg(PointZ.GetY()).arg(PointZ.GetZ()));
    }

    bool IsSingular = false;
    const RVL::Mathematics::_4D::CMatrix4D InverseFrame = Frame.GetInverse(&IsSingular);
    //const RVL::Mathematics::_4D::CMatrix4D I = InverseFrame * Frame;
    SbMatrix T;

    for (int r = 0; r < 4; ++r)
        for (int c = 0; c < 4; ++c)
        {
            T[r][c] = Frame[c][r];
        }

    m_pTransformation->setMatrix(T);

    ui->GloablAligmentTransfromTextEdit->append(QString("Mesh Id: %1").arg(pModelMesh->GetInstanceId()));
    ui->GloablAligmentTransfromTextEdit->append(QString("\tBase vertex Id: [ %1 ] Point: [ %2, %3, %4 ]").arg(pModelVertexBase->GetInstanceId()).arg(BasePoint.GetX()).arg(BasePoint.GetY()).arg(BasePoint.GetZ()));
    ui->GloablAligmentTransfromTextEdit->append(QString("\tAxis X vertex Id: [ %1 ] Point: [ %2, %3, %4 ]").arg(pModelVertexAxisX->GetInstanceId()).arg(PointX.GetX()).arg(PointX.GetY()).arg(PointX.GetZ()));
    ui->GloablAligmentTransfromTextEdit->append(QString("\tAxis Y vertex Id: [ %1 ] Point: [ %2, %3, %4 ]").arg(pModelVertexAxisY->GetInstanceId()).arg(PointY.GetX()).arg(PointY.GetY()).arg(PointY.GetZ()));
    ui->GloablAligmentTransfromTextEdit->append(QString("\tAxis Z vertex Id: [ %1 ] Point: [ %2, %3, %4 ]").arg(pModelVertexAxisZ->GetInstanceId()).arg(PointZ.GetX()).arg(PointZ.GetY()).arg(PointZ.GetZ()));
    ui->GloablAligmentTransfromTextEdit->append(QString("\tForwards Tranformation Matrix:"));
    QString MatrixText;

    for (int r = 0; r < 4; ++r)
    {
        MatrixText += QString("\t|\t%1\t%2\t%3\t%4\t|").arg(Frame[r][0]).arg(Frame[r][1]).arg(Frame[r][2]).arg(Frame[r][3]);

        if (r < 3)
        {
            MatrixText += "\n";
        }
    }

    ui->GloablAligmentTransfromTextEdit->append(MatrixText);
    ui->GloablAligmentTransfromTextEdit->append(QString("\tBackward Tranformation Matrix:"));
    MatrixText = "";

    for (int r = 0; r < 4; ++r)
    {
        MatrixText += QString("\t|\t%1\t%2\t%3\t%4\t|").arg(InverseFrame[r][0]).arg(InverseFrame[r][1]).arg(InverseFrame[r][2]).arg(InverseFrame[r][3]);

        if (r < 3)
        {
            MatrixText += "\n";
        }
    }

    ui->GloablAligmentTransfromTextEdit->append(MatrixText);

    ui->GloablAligmentApplyFramepushButton->setEnabled(true);
}

void VRME::on_DisplayFrameAligmentcheckBox_toggled(bool checked)
{
    m_pTLCS->GetBaseSwitch()->whichChild.setValue(checked ? SO_SWITCH_ALL : SO_SWITCH_NONE);
}

void VRME::on_GloablAligmentApplyFramepushButton_clicked()
{
    QString TextIdBase = ui->GlobalAligmentSourceComboBoxBase->currentText();
    QString TextIdAxisX = ui->GlobalAligmentSourceComboBoxAxisX->currentText();
    QString TextIdAxisY = ui->GlobalAligmentSourceComboBoxAxisY->currentText();
    QString TextIdAxisZ = ui->GlobalAligmentSourceComboBoxAxisZ->currentText();
    CModelMesh* pModelMesh = dynamic_cast<CModelMesh*>(m_pSelectedpOpenInventorModelMesh->GetModelElement());
    const CModelVertex* pModelVertexBase = pModelMesh->GetVertexById(GetIdFromTag(TextIdBase.toAscii(), TextIdBase.length()));
    const CModelVertex* pModelVertexAxisX = pModelMesh->GetVertexById(GetIdFromTag(TextIdAxisX.toAscii(), TextIdAxisX.length()));
    const CModelVertex* pModelVertexAxisY = pModelMesh->GetVertexById(GetIdFromTag(TextIdAxisY.toAscii(), TextIdAxisY.length()));
    const CModelVertex* pModelVertexAxisZ = pModelMesh->GetVertexById(GetIdFromTag(TextIdAxisZ.toAscii(), TextIdAxisZ.length()));
    const RVL::Mathematics::_3D::CVector3D& BasePoint = pModelVertexBase->GetPoint(true);
    const RVL::Mathematics::_3D::CVector3D& PointX = InverReferencePoint(ui->GlobalAligmentSourceChechBoxBoxAxisX->isChecked(), pModelVertexAxisX->GetPoint(true), BasePoint);
    const RVL::Mathematics::_3D::CVector3D& PointY = InverReferencePoint(ui->GlobalAligmentSourceChechBoxBoxAxisY->isChecked(), pModelVertexAxisY->GetPoint(true), BasePoint);
    const RVL::Mathematics::_3D::CVector3D& PointZ = InverReferencePoint(ui->GlobalAligmentSourceChechBoxBoxAxisZ->isChecked(), pModelVertexAxisZ->GetPoint(true), BasePoint);
    RVL::Mathematics::_4D::CMatrix4D Frame;

    if (!RVL::Representation::ModelBased::GeometricGraph::Base::CModelMultipleMesh::CreateTransformation(BasePoint, PointX, PointY, PointZ, Frame))
    {
        return;
    }

    bool IsSingular = false;
    const RVL::Mathematics::_4D::CMatrix4D InverseFrame = Frame.GetInverse(&IsSingular);

    if (IsSingular)
    {
        return;
    }

    const std::map<int, CModelTransformation*>& Transformations = m_pModelMultipleMeshes->GetTransformations();

    for (std::map<int, CModelTransformation*>::const_iterator pIdTransformation = Transformations.begin(); pIdTransformation != Transformations.end(); ++pIdTransformation)
    {
        CModelTransformation* pModelTransformation = pIdTransformation->second;
        pModelTransformation->UpdateFromAligment(InverseFrame);
    }

    const std::map<int, CModelMesh*>& Meshes = m_pModelMultipleMeshes->GetMeshes();

    for (std::map<int, CModelMesh*>::const_iterator pIdMesh = Meshes.begin(); pIdMesh != Meshes.end(); ++pIdMesh)
    {
        CModelMesh* pMesh = pIdMesh->second;
        pMesh->Transform(InverseFrame);

    }

    SbMatrix T;
    T.makeIdentity();
    m_pTransformation->setMatrix(T);
    ui->GloablAligmentApplyFramepushButton->setEnabled(false);
}
