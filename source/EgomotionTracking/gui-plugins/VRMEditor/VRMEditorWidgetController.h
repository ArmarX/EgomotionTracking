/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::VRMEditorWidgetController
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <QLayout>
#include "VRME/vrme.h"

namespace armarx
{

    /*!
     * @class VRMEditorWidgetController
     * @brief VRMEditorWidgetController brief one line description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        VRMEditorWidgetController:
        public armarx::ArmarXComponentWidgetController
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit VRMEditorWidgetController();

        /**
         * Controller destructor
         */
        ~VRMEditorWidgetController() override;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        QString getWidgetName() const override
        {
            return "VisionX.VRMEditor";
        }

        /**
         * @see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

    public slots:
        /* QT slot declarations */

    signals:
        /* QT signal declarations */

    private:
        /**
         * Widget Form
         */
        QGridLayout* layout;
    };
}

