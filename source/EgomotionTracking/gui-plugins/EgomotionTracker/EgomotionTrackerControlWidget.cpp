#include "EgomotionTrackerControlWidget.h"
#include "EgomotionTrackerWidgetController.h"

#include <iostream>

using namespace visionx;
using namespace armarx;

EgomotionTrackerControlWidget::EgomotionTrackerControlWidget(EgomotionTrackerWidgetController* pEgomotionTrackerWidgetController)
{
    this->egomotionTrackerWidgetController = pEgomotionTrackerWidgetController;

    egomotionTrackerControlWidgetUi.setupUi(this);

    egomotionTrackerControlWidgetUi.buttonStartSelfLocalisationProcess->setEnabled(true);
    egomotionTrackerControlWidgetUi.buttonStartInitialisation->setEnabled(true);
    egomotionTrackerControlWidgetUi.buttonStartTracking->setEnabled(false);
    egomotionTrackerControlWidgetUi.buttonStartStatusEstimation->setEnabled(false);

    egomotionTrackerControlWidgetUi.buttonStop->setEnabled(true);
    egomotionTrackerControlWidgetUi.buttonStep->setEnabled(false);

    egomotionTrackerControlWidgetUi.checkBoxAutomaticExecution->setChecked(true);
    egomotionTrackerControlWidgetUi.checkBoxCycleTrackingStatusEstimation->setChecked(false);

    egomotionTrackerControlWidgetUi.checkBoxCycleTrackingStatusEstimation->setEnabled(true);
}

EgomotionTrackerControlWidget::~EgomotionTrackerControlWidget()
{

}

void EgomotionTrackerControlWidget::setEgomotionTrackerProxy(EgomotionTrackerInterfacePrx egomotionTrackerProxy)
{
    this->egomotionTrackerProxy = egomotionTrackerProxy;
    LoadCurrentEgomotionTrackerConfiguration();
    ConnectSignals();
}

void EgomotionTrackerControlWidget::onButtonStartSelfLocalisationProcessClicked()
{
    egomotionTrackerProxy->startSelfLocalisationProcess();

    egomotionTrackerControlWidgetUi.buttonStartSelfLocalisationProcess->setEnabled(true);
    egomotionTrackerControlWidgetUi.buttonStartInitialisation->setEnabled(false);
    egomotionTrackerControlWidgetUi.buttonStartTracking->setEnabled(false);
    egomotionTrackerControlWidgetUi.buttonStartStatusEstimation->setEnabled(false);

    egomotionTrackerControlWidgetUi.checkBoxCycleTrackingStatusEstimation->setChecked(true);
    egomotionTrackerControlWidgetUi.checkBoxCycleTrackingStatusEstimation->setEnabled(false);
}

void EgomotionTrackerControlWidget::onButtonStartInitialisationClicked()
{
    egomotionTrackerProxy->beginLocalisation();

    egomotionTrackerControlWidgetUi.buttonStartSelfLocalisationProcess->setEnabled(false);
    egomotionTrackerControlWidgetUi.buttonStartInitialisation->setEnabled(true);
    egomotionTrackerControlWidgetUi.buttonStartTracking->setEnabled(true);
    egomotionTrackerControlWidgetUi.buttonStartStatusEstimation->setEnabled(false);
}

void EgomotionTrackerControlWidget::onButtonStartTrackingClicked()
{
    egomotionTrackerProxy->beginTracking();

    egomotionTrackerControlWidgetUi.buttonStartSelfLocalisationProcess->setEnabled(false);
    egomotionTrackerControlWidgetUi.buttonStartInitialisation->setEnabled(true);
    egomotionTrackerControlWidgetUi.buttonStartTracking->setEnabled(true);
    egomotionTrackerControlWidgetUi.buttonStartStatusEstimation->setEnabled(true);
}

void EgomotionTrackerControlWidget::onButtonStartStatusEstimationClicked()
{
    egomotionTrackerProxy->beginStatusEstimation();

    egomotionTrackerControlWidgetUi.buttonStartSelfLocalisationProcess->setEnabled(false);
    egomotionTrackerControlWidgetUi.buttonStartInitialisation->setEnabled(true);
    egomotionTrackerControlWidgetUi.buttonStartTracking->setEnabled(true);
    egomotionTrackerControlWidgetUi.buttonStartStatusEstimation->setEnabled(true);
}

void EgomotionTrackerControlWidget::onButtonStopClicked()
{
    egomotionTrackerProxy->stopSelfLocalisationProcess();

    egomotionTrackerControlWidgetUi.buttonStartSelfLocalisationProcess->setEnabled(true);
    egomotionTrackerControlWidgetUi.buttonStartInitialisation->setEnabled(true);
    egomotionTrackerControlWidgetUi.buttonStartTracking->setEnabled(false);
    egomotionTrackerControlWidgetUi.buttonStartStatusEstimation->setEnabled(false);

    egomotionTrackerControlWidgetUi.buttonStop->setEnabled(true);
    egomotionTrackerControlWidgetUi.buttonStep->setEnabled(false);

    egomotionTrackerControlWidgetUi.checkBoxAutomaticExecution->setChecked(true);
    egomotionTrackerControlWidgetUi.checkBoxCycleTrackingStatusEstimation->setChecked(false);

    egomotionTrackerControlWidgetUi.checkBoxCycleTrackingStatusEstimation->setEnabled(true);
}

void EgomotionTrackerControlWidget::onButtonStepClicked()
{
    egomotionTrackerProxy->step();
}

void EgomotionTrackerControlWidget::onCheckBoxAutomaticExecutionClicked(bool toggled)
{
    egomotionTrackerProxy->setAutomaticExecution(toggled);
    egomotionTrackerControlWidgetUi.buttonStep->setDisabled(toggled);
}

void EgomotionTrackerControlWidget::onCheckBoxCycleTrackingStatusEstimationClicked(bool toggled)
{
    egomotionTrackerProxy->setCycleTrackingStatusEstimation(toggled);
}

void EgomotionTrackerControlWidget::onNeckErrorValueChanged(double value)
{
    egomotionTrackerProxy->setNeckErrorLimits(egomotionTrackerControlWidgetUi.spinBoxRollErrorMin->value(), egomotionTrackerControlWidgetUi.spinBoxRollErrorMax->value(),
            egomotionTrackerControlWidgetUi.spinBoxPitchErrorMin->value(), egomotionTrackerControlWidgetUi.spinBoxPitchErrorMax->value(),
            egomotionTrackerControlWidgetUi.spinBoxYawErrorMin->value(), egomotionTrackerControlWidgetUi.spinBoxYawErrorMax->value());
}

void EgomotionTrackerControlWidget::onCheckBoxUseOrientationDifferenceClicked(bool toggled)
{
    egomotionTrackerProxy->setUseOrientationDifferenceTracking(toggled);
    egomotionTrackerProxy->setUseOrientationDifferenceInitialisation(toggled);
}

void EgomotionTrackerControlWidget::onCheckBoxUseMahalanobisDistanceBasedScatteringReductionClicked(bool toggled)
{
    egomotionTrackerProxy->setUseMahalanobisDistanceBasedScatteringReduction(toggled);
}

void EgomotionTrackerControlWidget::LoadCurrentEgomotionTrackerConfiguration()
{
    egomotionTrackerControlWidgetUi.spinBoxRollErrorMin->setValue(egomotionTrackerProxy->getNeckErrorRollMin());
    egomotionTrackerControlWidgetUi.spinBoxRollErrorMax->setValue(egomotionTrackerProxy->getNeckErrorRollMax());
    egomotionTrackerControlWidgetUi.spinBoxPitchErrorMin->setValue(egomotionTrackerProxy->getNeckErrorPitchMin());
    egomotionTrackerControlWidgetUi.spinBoxPitchErrorMax->setValue(egomotionTrackerProxy->getNeckErrorPitchMax());
    egomotionTrackerControlWidgetUi.spinBoxYawErrorMin->setValue(egomotionTrackerProxy->getNeckErrorYawMin());
    egomotionTrackerControlWidgetUi.spinBoxYawErrorMax->setValue(egomotionTrackerProxy->getNeckErrorYawMax());
    egomotionTrackerControlWidgetUi.checkBoxUseOrientationDifference->setChecked(egomotionTrackerProxy->getUseOrientationDifferenceTracking());
    egomotionTrackerControlWidgetUi.checkBoxUseMahalanobisDistanceBasedScatteringReduction->setChecked(egomotionTrackerProxy->getUseMahalanobisDistanceBasedScatteringReduction());
}

void EgomotionTrackerControlWidget::ConnectSignals()
{
    connect(egomotionTrackerControlWidgetUi.buttonStartSelfLocalisationProcess, SIGNAL(clicked()), this, SLOT(onButtonStartSelfLocalisationProcessClicked()));
    connect(egomotionTrackerControlWidgetUi.buttonStartInitialisation, SIGNAL(clicked()), this, SLOT(onButtonStartInitialisationClicked()));
    connect(egomotionTrackerControlWidgetUi.buttonStartTracking, SIGNAL(clicked()), this, SLOT(onButtonStartTrackingClicked()));
    connect(egomotionTrackerControlWidgetUi.buttonStartStatusEstimation, SIGNAL(clicked()), this, SLOT(onButtonStartStatusEstimationClicked()));
    connect(egomotionTrackerControlWidgetUi.buttonStop, SIGNAL(clicked()), this, SLOT(onButtonStopClicked()));
    connect(egomotionTrackerControlWidgetUi.buttonStep, SIGNAL(clicked()), this, SLOT(onButtonStepClicked()));
    connect(egomotionTrackerControlWidgetUi.checkBoxAutomaticExecution, SIGNAL(clicked(bool)), this, SLOT(onCheckBoxAutomaticExecutionClicked(bool)));
    connect(egomotionTrackerControlWidgetUi.checkBoxCycleTrackingStatusEstimation, SIGNAL(clicked(bool)), this, SLOT(onCheckBoxCycleTrackingStatusEstimationClicked(bool)));
    connect(egomotionTrackerControlWidgetUi.spinBoxRollErrorMin, SIGNAL(valueChanged(double)), this, SLOT(onNeckErrorValueChanged(double)));
    connect(egomotionTrackerControlWidgetUi.spinBoxRollErrorMax, SIGNAL(valueChanged(double)), this, SLOT(onNeckErrorValueChanged(double)));
    connect(egomotionTrackerControlWidgetUi.spinBoxPitchErrorMin, SIGNAL(valueChanged(double)), this, SLOT(onNeckErrorValueChanged(double)));
    connect(egomotionTrackerControlWidgetUi.spinBoxPitchErrorMax, SIGNAL(valueChanged(double)), this, SLOT(onNeckErrorValueChanged(double)));
    connect(egomotionTrackerControlWidgetUi.spinBoxYawErrorMin, SIGNAL(valueChanged(double)), this, SLOT(onNeckErrorValueChanged(double)));
    connect(egomotionTrackerControlWidgetUi.spinBoxYawErrorMax, SIGNAL(valueChanged(double)), this, SLOT(onNeckErrorValueChanged(double)));
    connect(egomotionTrackerControlWidgetUi.checkBoxUseOrientationDifference, SIGNAL(clicked(bool)), this, SLOT(onCheckBoxUseOrientationDifferenceClicked(bool)));
    connect(egomotionTrackerControlWidgetUi.checkBoxUseMahalanobisDistanceBasedScatteringReduction, SIGNAL(clicked(bool)), this,
            SLOT(onCheckBoxUseMahalanobisDistanceBasedScatteringReductionClicked(bool)));
}


