#include "ParticleVisualizationWidgetController.h"

using namespace visionx;
using namespace armarx;

ParticleVisualizationWidgetController::ParticleVisualizationWidgetController()
{

}

ParticleVisualizationWidgetController::~ParticleVisualizationWidgetController()
{

}

QPointer<QWidget> ParticleVisualizationWidgetController::getWidget()
{
    if (!particleVisualizationWidget)
    {
        particleVisualizationWidget = new ParticleVisualizationWidget(this);
    }

    return qobject_cast<QWidget*>(particleVisualizationWidget);
}

void ParticleVisualizationWidgetController::onInitComponent()
{
    getWidget()->show();
    usingProxy("EgomotionTracker");
    connect(this, SIGNAL(proxyConnected()), particleVisualizationWidget, SLOT(initializeVisualisation()));
}

void ParticleVisualizationWidgetController::onConnectComponent()
{
    egomotionTrackerProxy = getProxy<EgomotionTrackerInterfacePrx>("EgomotionTracker");
    particleVisualizationWidget->setEgomotionTrackerProxy(egomotionTrackerProxy);
    emit proxyConnected();
}

void ParticleVisualizationWidgetController::onExitComponent()
{

}

void ParticleVisualizationWidgetController::loadSettings(QSettings* settings)
{

}

void ParticleVisualizationWidgetController::saveSettings(QSettings* settings)
{

}

