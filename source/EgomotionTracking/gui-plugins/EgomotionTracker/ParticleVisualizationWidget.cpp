#include "ParticleVisualizationWidget.h"

#include <EgomotionTracking/components/EgomotionTrackingConfiguration/TrackingConfiguration.h>

using namespace visionx;
using namespace armarx;

ParticleVisualizationWidget::ParticleVisualizationWidget(ParticleVisualizationWidgetController* particleVisualizationWidgetController)
{
    this->particleVisualizationWidgetController = particleVisualizationWidgetController;

    tracking2DVisualizationWidget = new CTracking2DVisualizationWidget(this);

    particleVisualizationWidgetUi.setupUi(this);
    particleVisualizationWidgetUi.layout->addWidget(tracking2DVisualizationWidget);
    particleVisualizationWidgetUi.layout->setContentsMargins(0, 0, 0, 0);

    updateTimer = new QTimer(this);

    connect(updateTimer, SIGNAL(timeout()), this, SLOT(updateVisualisation()));
}

ParticleVisualizationWidget::~ParticleVisualizationWidget()
{

}

void ParticleVisualizationWidget::setEgomotionTrackerProxy(EgomotionTrackerInterfacePrx egomotionTrackerProxy)
{
    this->egomotionTrackerProxy = egomotionTrackerProxy;
}

void ParticleVisualizationWidget::initializeVisualisation()
{
    trackingVisualizationScene = new CTracking2DVisualizationScene();
    tracking2DVisualizationWidget->SetTrackingVisualizationScene(trackingVisualizationScene);

    BoundingBox boundingBox = egomotionTrackerProxy->getBoundingBox();
    trackingVisualizationScene->AddModelObject(boundingBox.min[0], boundingBox.min[1], boundingBox.max[0], boundingBox.max[1], QString("Kitchen"));
    trackingVisualizationScene->SetVisualizationArea(configuration_trackingVisualizationAreaMinX, configuration_trackingVisualizationAreaMinY, configuration_trackingVisualizationAreaMaxX, configuration_trackingVisualizationAreaMaxY);
    visualizationParticles.resize(configuration_maxParticles, CLocationParticle(CLocationState(1000, 1000, 0)));

    trackingParticlesGraphicsGroup = new CLocationParticleGraphicsGroup();

    for (std::vector<CLocationParticle>::iterator particleIterator = visualizationParticles.begin(); particleIterator != visualizationParticles.end(); ++particleIterator)
    {
        particleIterator->SetActive(false);
        trackingParticlesGraphicsGroup->AddLocationParticle(&(*particleIterator));
    }

    trackingVisualizationScene->AddLocationParticleGroup(trackingParticlesGraphicsGroup);

    localisationEstimatorParticlesGraphicsGroup = new CLocationParticleGraphicsGroup();

    ParticleVector particlesPS = egomotionTrackerProxy->getParticlesPS();
    localisationParticles.resize(particlesPS.size());

    if (particlesPS.size() > 0)
    {
        int i = 0;

        for (std::vector<Particle>::const_iterator particleIterator = particlesPS.begin(); particleIterator != particlesPS.end(); ++particleIterator, ++i)
        {
            localisationParticles[i].SetX(particleIterator->locEst.x);
            localisationParticles[i].SetY(particleIterator->locEst.y);
            localisationParticles[i].SetAlpha(particleIterator->locEst.alpha);
            localisationParticles[i].SetNeckPitchError(particleIterator->neckPitchError);
            localisationParticles[i].SetNeckRollError(particleIterator->neckRollError);
            localisationParticles[i].SetNeckYawError(particleIterator->neckYawError);
            localisationParticles[i].SetImportanceWeight(particleIterator->importanceWeight);
            localisationParticles[i].SetActive(true);
            localisationEstimatorParticlesGraphicsGroup->AddLocationParticle(&(localisationParticles[i]));
        }

        PlaneScatteringDirections planeScatteringDirectionsPS = egomotionTrackerProxy->getPlaneScatteringDirectionsPS();
        CPlaneScatteringDirections* planeScatteringDirections = new CPlaneScatteringDirections();
        Vec2d temp1;
        temp1.x = planeScatteringDirectionsPS.meanScattDir.x;
        temp1.y = planeScatteringDirectionsPS.meanScattDir.y;
        planeScatteringDirections->SetMean(temp1);
        Vec2d temp2;
        temp2.x = planeScatteringDirectionsPS.direction1.x;
        temp2.y = planeScatteringDirectionsPS.direction1.y;
        planeScatteringDirections->setDirection1(temp2);
        planeScatteringDirections->setSigma1(planeScatteringDirectionsPS.sigma1);
        planeScatteringDirections->setSigma2(planeScatteringDirectionsPS.sigma2);
        localisationEstimatorParticlesGraphicsGroup->SetPlaneScatteringDirections(planeScatteringDirections);
        trackingVisualizationScene->AddLocationParticleGroup(localisationEstimatorParticlesGraphicsGroup);
    }

    trackingParticlesGraphicsGroup->SetVisible(false);
    localisationEstimatorParticlesGraphicsGroup->SetVisible(false);

    tracking2DVisualizationWidget->show();
    trackingVisualizationScene->Update();

    updateTimer->start(200);
}

void ParticleVisualizationWidget::updateVisualisation()
{
    trackingVisualizationScene->SetGroundTruthRobotLocation(NULL);

    LocationEstimate locEst = egomotionTrackerProxy->getLocationEstimate();
    CLocationState locationEstimate;
    locationEstimate.SetX(locEst.x);
    locationEstimate.SetY(locEst.y);
    locationEstimate.SetAlpha(locEst.alpha);
    trackingVisualizationScene->SetEstimatedRobotLocation(&locationEstimate);

    EgomotionTrackerStatus status = egomotionTrackerProxy->getStatus();

    switch (status)
    {
        case eStopped:
        {
            trackingParticlesGraphicsGroup->SetVisible(false);
            localisationEstimatorParticlesGraphicsGroup->SetVisible(false);

            break;
        }

        case ePositionInitialisation:
        {
            PlaneScatteringDirections planeScatteringDirectionsPS = egomotionTrackerProxy->getPlaneScatteringDirectionsPS();
            CPlaneScatteringDirections* planeScatteringDirectionsLocalisation = new CPlaneScatteringDirections();
            Vec2d temp11;
            temp11.x = planeScatteringDirectionsPS.meanScattDir.x;
            temp11.y = planeScatteringDirectionsPS.meanScattDir.y;
            planeScatteringDirectionsLocalisation->SetMean(temp11);
            Vec2d temp22;
            temp22.x = planeScatteringDirectionsPS.direction1.x;
            temp22.y = planeScatteringDirectionsPS.direction1.y;
            planeScatteringDirectionsLocalisation->setDirection1(temp22);
            planeScatteringDirectionsLocalisation->setSigma1(planeScatteringDirectionsPS.sigma1);
            planeScatteringDirectionsLocalisation->setSigma2(planeScatteringDirectionsPS.sigma2);
            localisationEstimatorParticlesGraphicsGroup->SetPlaneScatteringDirections(planeScatteringDirectionsLocalisation);

            ParticleVector particlesPS = egomotionTrackerProxy->getParticlesPS();
            int i = 0;

            for (std::vector<Particle>::const_iterator particleIterator = particlesPS.begin(); particleIterator != particlesPS.end(); ++particleIterator, ++i)
            {
                localisationParticles[i].SetX(particleIterator->locEst.x);
                localisationParticles[i].SetY(particleIterator->locEst.y);
                localisationParticles[i].SetAlpha(particleIterator->locEst.alpha);
                localisationParticles[i].SetNeckPitchError(particleIterator->neckPitchError);
                localisationParticles[i].SetNeckRollError(particleIterator->neckRollError);
                localisationParticles[i].SetNeckYawError(particleIterator->neckYawError);
                localisationParticles[i].SetImportanceWeight(particleIterator->importanceWeight);
                localisationParticles[i].SetActive(true);
            }

            trackingParticlesGraphicsGroup->SetVisible(false);
            localisationEstimatorParticlesGraphicsGroup->SetVisible(true);

            break;
        }

        case eTracking:
        {
            PlaneScatteringDirections planeScatteringDirectionsPF = egomotionTrackerProxy->getPlaneScatteringDirectionsPF();
            CPlaneScatteringDirections* planeScatteringDirectionsTracking = new CPlaneScatteringDirections();
            Vec2d temp1;
            temp1.x = planeScatteringDirectionsPF.meanScattDir.x;
            temp1.y = planeScatteringDirectionsPF.meanScattDir.y;
            planeScatteringDirectionsTracking->SetMean(temp1);
            Vec2d temp2;
            temp2.x = planeScatteringDirectionsPF.direction1.x;
            temp2.y = planeScatteringDirectionsPF.direction1.y;
            planeScatteringDirectionsTracking->setDirection1(temp2);
            planeScatteringDirectionsTracking->setSigma1(planeScatteringDirectionsPF.sigma1);
            planeScatteringDirectionsTracking->setSigma2(planeScatteringDirectionsPF.sigma2);
            trackingParticlesGraphicsGroup->SetPlaneScatteringDirections(planeScatteringDirectionsTracking);

            int numberOfActiveParticles = egomotionTrackerProxy->getNumberOfActiveParticles();

            ParticleVector particlesPF = egomotionTrackerProxy->getParticlesPF();
            int i = 0;

            for (std::vector<Particle>::const_iterator particleIterator = particlesPF.begin(); particleIterator != particlesPF.end(); ++particleIterator, ++i)
            {
                visualizationParticles[i].SetNeckRollError(particleIterator->neckRollError);
                visualizationParticles[i].SetNeckPitchError(particleIterator->neckPitchError);
                visualizationParticles[i].SetNeckYawError(particleIterator->neckYawError);
                visualizationParticles[i].SetX(particleIterator->locEst.x);
                visualizationParticles[i].SetY(particleIterator->locEst.y);
                visualizationParticles[i].SetAlpha(particleIterator->locEst.alpha);
                visualizationParticles[i].SetImportanceWeight(particleIterator->importanceWeight);
                visualizationParticles[i].SetActive(true);
            }

            for (uint j = numberOfActiveParticles; j < visualizationParticles.size(); j++)
            {
                visualizationParticles[j].SetActive(false);
            }

            trackingParticlesGraphicsGroup->SetVisible(true);
            localisationEstimatorParticlesGraphicsGroup->SetVisible(false);

            break;
        }

        case eStatusEstimate:
        {
            trackingParticlesGraphicsGroup->SetVisible(true);
            localisationEstimatorParticlesGraphicsGroup->SetVisible(false);

            break;
        }
    }

    trackingVisualizationScene->Update();
}
