#pragma once

#include <EgomotionTracking/gui-plugins/EgomotionTracker/ui_ParticleVisualizationWidget.h>

#include <VisionX/interface/components/EgomotionTrackerInterface.h>

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationSwarmParticle.h>

#include <EgomotionTracking/components/EgomotionTrackingVisualization/2D/TrackingVisualization/Tracking2DVisualizationScene.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/2D/TrackingVisualization/Tracking2DVisualizationWidget.h>

#include <QTimer>

namespace visionx
{

    class ParticleVisualizationWidgetController;

    class ParticleVisualizationWidget :
        public QWidget
    {
        Q_OBJECT
    public:
        ParticleVisualizationWidget(ParticleVisualizationWidgetController* controller);
        ~ParticleVisualizationWidget() override;
        void setEgomotionTrackerProxy(armarx::EgomotionTrackerInterfacePrx egomotionTrackerProxy);

    public slots:
        void initializeVisualisation();
        void updateVisualisation();

    signals:

    private:
        Ui::ParticleVisualizationWidget particleVisualizationWidgetUi;
        ParticleVisualizationWidgetController* particleVisualizationWidgetController;

        armarx::EgomotionTrackerInterfacePrx egomotionTrackerProxy;

        CTracking2DVisualizationScene* trackingVisualizationScene;
        CTracking2DVisualizationWidget* tracking2DVisualizationWidget;

        std::vector<CLocationParticle> visualizationParticles;
        std::vector<CLocationSwarmParticle> localisationParticles;
        CLocationParticleGraphicsGroup* trackingParticlesGraphicsGroup;
        CLocationParticleGraphicsGroup* localisationEstimatorParticlesGraphicsGroup;

        QTimer* updateTimer;
    };
}

