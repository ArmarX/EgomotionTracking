#include "EgomotionTrackerWidgetController.h"

using namespace visionx;
using namespace armarx;

EgomotionTrackerWidgetController::EgomotionTrackerWidgetController()
{

}

EgomotionTrackerWidgetController::~EgomotionTrackerWidgetController()
{

}

QPointer<QWidget> EgomotionTrackerWidgetController::getWidget()
{
    if (!egomotionTrackerControlWidget)
    {
        egomotionTrackerControlWidget = new EgomotionTrackerControlWidget(this);
    }

    return qobject_cast<QWidget*>(egomotionTrackerControlWidget);
}

void EgomotionTrackerWidgetController::onInitComponent()
{
    getWidget()->show();
    usingProxy("EgomotionTracker");
}

void EgomotionTrackerWidgetController::onConnectComponent()
{
    egomotionTrackerProxy = getProxy<EgomotionTrackerInterfacePrx>("EgomotionTracker");
    egomotionTrackerControlWidget->setEgomotionTrackerProxy(egomotionTrackerProxy);
}

void EgomotionTrackerWidgetController::onExitComponent()
{

}

void EgomotionTrackerWidgetController::loadSettings(QSettings* settings)
{

}

void EgomotionTrackerWidgetController::saveSettings(QSettings* settings)
{

}
