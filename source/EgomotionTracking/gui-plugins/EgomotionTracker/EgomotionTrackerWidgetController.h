#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <VisionX/interface/components/EgomotionTrackerInterface.h>

#include "EgomotionTrackerControlWidget.h"

namespace visionx
{
    /*!
    * \class EgomotionTrackerWidgetController
    * \brief TODO
    * \see EgomotionTrackerGuiPlugin
    */
    class EgomotionTrackerWidgetController :
        public armarx::ArmarXComponentWidgetController
    {
        Q_OBJECT
    public:
        EgomotionTrackerWidgetController();
        ~EgomotionTrackerWidgetController() override;

        QPointer<QWidget> getWidget() override;
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        QString getWidgetName() const override
        {
            return "EgomotionTracking.EgomotionTracker";
        }
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

    signals:

    public slots:

    private:
        QPointer<EgomotionTrackerControlWidget> egomotionTrackerControlWidget;
        armarx::EgomotionTrackerInterfacePrx egomotionTrackerProxy;
    };
}

