#pragma once

#include <EgomotionTracking/gui-plugins/EgomotionTracker/ui_EgomotionTrackerControlWidget.h>

#include <VisionX/interface/components/EgomotionTrackerInterface.h>

namespace visionx
{

    class EgomotionTrackerWidgetController;

    class EgomotionTrackerControlWidget :
        public QWidget
    {
        Q_OBJECT
    public:
        EgomotionTrackerControlWidget(EgomotionTrackerWidgetController* pEgomotionTrackerWidgetController);
        ~EgomotionTrackerControlWidget() override;
        void setEgomotionTrackerProxy(armarx::EgomotionTrackerInterfacePrx egomotionTrackerProxy);

    public slots:
        void onButtonStartSelfLocalisationProcessClicked();
        void onButtonStartInitialisationClicked();
        void onButtonStartTrackingClicked();
        void onButtonStartStatusEstimationClicked();
        void onButtonStopClicked();
        void onButtonStepClicked();
        void onCheckBoxAutomaticExecutionClicked(bool toggled);
        void onCheckBoxCycleTrackingStatusEstimationClicked(bool toggled);
        void onNeckErrorValueChanged(double value);
        void onCheckBoxUseOrientationDifferenceClicked(bool toggled);
        void onCheckBoxUseMahalanobisDistanceBasedScatteringReductionClicked(bool toggled);

    signals:

    private:
        Ui::EgomotionTrackerControlWidget egomotionTrackerControlWidgetUi;
        EgomotionTrackerWidgetController* egomotionTrackerWidgetController;

        armarx::EgomotionTrackerInterfacePrx egomotionTrackerProxy;

        void LoadCurrentEgomotionTrackerConfiguration();
        void ConnectSignals();
    };
}

