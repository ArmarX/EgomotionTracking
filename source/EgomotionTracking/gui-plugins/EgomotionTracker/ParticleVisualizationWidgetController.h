#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <VisionX/interface/components/EgomotionTrackerInterface.h>

#include "ParticleVisualizationWidget.h"

namespace visionx
{
    class ParticleVisualizationWidgetController :
        public armarx::ArmarXComponentWidgetController
    {
        Q_OBJECT
    public:
        ParticleVisualizationWidgetController();
        ~ParticleVisualizationWidgetController() override;

        QPointer<QWidget> getWidget() override;
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        QString getWidgetName() const override
        {
            return "EgomotionTracking.ParticleVisualization";
        }
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

    signals:
        void proxyConnected();

    public slots:

    private:
        QPointer<ParticleVisualizationWidget> particleVisualizationWidget;
        armarx::EgomotionTrackerInterfacePrx egomotionTrackerProxy;
    };
}

