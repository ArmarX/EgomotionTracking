#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <VisionX/interface/components/VisibilityTestInterface.h>

#include "VisibilityTestControlWidget.h"

namespace visionx
{
    class VisibilityTestWidgetController :
        public armarx::ArmarXComponentWidgetController
    {
        Q_OBJECT
    public:
        VisibilityTestWidgetController();
        ~VisibilityTestWidgetController() override;

        QPointer<QWidget> getWidget() override;
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        QString getWidgetName() const override
        {
            return "VisionX.VisibilityTestWidgetController";
        }
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

    signals:

    public slots:

    private:
        QPointer<VisibilityTestControlWidget> m_visibilityTestControlWidget;
        armarx::VisibilityTestInterfacePrx m_visibilityTestProxy;
    };
}

