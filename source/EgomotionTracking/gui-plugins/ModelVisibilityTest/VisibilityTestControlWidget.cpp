#include "VisibilityTestControlWidget.h"
#include "VisibilityTestWidgetController.h"

#include <iostream>

using namespace visionx;
using namespace armarx;

VisibilityTestControlWidget::VisibilityTestControlWidget(VisibilityTestWidgetController* pVisibilityTestWidgetController)
{
    this->m_pVisibilityTestWidgetController = pVisibilityTestWidgetController;

    m_visibilityTestControlWidgetUi.setupUi(this);
}

VisibilityTestControlWidget::~VisibilityTestControlWidget()
    = default;

void VisibilityTestControlWidget::setVisibilityTestProxy(VisibilityTestInterfacePrx visibilityTestProxy)
{
    this->m_visibilityTestProxy = visibilityTestProxy;
    loadCurrentConfiguration();
    connectSignals();
}

void VisibilityTestControlWidget::onButtonStartClicked(bool toggled)
{
    m_visibilityTestProxy->start();
}

void VisibilityTestControlWidget::onButtonStopClicked(bool toggled)
{
    m_visibilityTestProxy->stop();
}

void VisibilityTestControlWidget::onButtonStepClicked(bool toggled)
{
    m_visibilityTestProxy->doNextStep();
}

void VisibilityTestControlWidget::onAutoExecutionClicked(bool toggled)
{
    m_visibilityTestProxy->setAutoExecution(toggled);
}

void VisibilityTestControlWidget::onImageWidthValueChanged(double value)
{
    m_visibilityTestProxy->setImageWidth(value);
}

void VisibilityTestControlWidget::onImageHeightValueChanged(double value)
{
    m_visibilityTestProxy->setImageHeight(value);
}

void VisibilityTestControlWidget::onMinScreenLineLengthValueChanged(double value)
{
    m_visibilityTestProxy->setMinScreenLineLength(value);
}

void VisibilityTestControlWidget::onSphereLevelsValueChanged(int value)
{
    m_visibilityTestProxy->setSphereLevels(value);
}

void VisibilityTestControlWidget::loadCurrentConfiguration()
{
    m_visibilityTestControlWidgetUi.autoExecutionCheckBox->setChecked(m_visibilityTestProxy->getAutoExecution());
    m_visibilityTestControlWidgetUi.imageWidthSpinBox->setValue(m_visibilityTestProxy->getImageWidth());
    m_visibilityTestControlWidgetUi.imageHeightSpinBox->setValue(m_visibilityTestProxy->getImageHeight());
    m_visibilityTestControlWidgetUi.minScreenLineLengthSpinBox->setValue(m_visibilityTestProxy->getMinScreenLineLength());
    m_visibilityTestControlWidgetUi.sphereLevelsSpinBox->setValue(m_visibilityTestProxy->getSphereLevels());
}

void VisibilityTestControlWidget::connectSignals()
{
    connect(m_visibilityTestControlWidgetUi.buttonStart, SIGNAL(clicked(bool)), this, SLOT(onButtonStartClicked(bool)));
    connect(m_visibilityTestControlWidgetUi.buttonStop, SIGNAL(clicked(bool)), this, SLOT(onButtonStopClicked(bool)));
    connect(m_visibilityTestControlWidgetUi.buttonStep, SIGNAL(clicked(bool)), this, SLOT(onButtonStepClicked(bool)));
    connect(m_visibilityTestControlWidgetUi.autoExecutionCheckBox, SIGNAL(clicked(bool)), this, SLOT(onAutoExecutionClicked(bool)));
    connect(m_visibilityTestControlWidgetUi.imageWidthSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onImageWidthValueChanged(double)));
    connect(m_visibilityTestControlWidgetUi.imageHeightSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onImageHeightValueChanged(double)));
    connect(m_visibilityTestControlWidgetUi.minScreenLineLengthSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onMinScreenLineLengthValueChanged(double)));
    connect(m_visibilityTestControlWidgetUi.sphereLevelsSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onSphereLevelsValueChanged(int)));
}



