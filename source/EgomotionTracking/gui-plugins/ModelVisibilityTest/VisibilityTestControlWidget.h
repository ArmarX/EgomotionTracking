#pragma once

#include <EgomotionTracking/gui-plugins/ModelVisibilityTest/ui_VisibilityTestControlWidget.h>

#include <VisionX/interface/components/VisibilityTestInterface.h>

namespace visionx
{

    class VisibilityTestWidgetController;

    class VisibilityTestControlWidget :
        public QWidget
    {
        Q_OBJECT

    public:

        VisibilityTestControlWidget(VisibilityTestWidgetController* pVisibilityTestWidgetController);
        ~VisibilityTestControlWidget() override;
        void setVisibilityTestProxy(armarx::VisibilityTestInterfacePrx visibilityTestProxy);

    public slots:

        void onButtonStartClicked(bool toggled);
        void onButtonStopClicked(bool toggled);
        void onButtonStepClicked(bool toggled);
        void onAutoExecutionClicked(bool toggled);
        void onImageWidthValueChanged(double value);
        void onImageHeightValueChanged(double value);
        void onMinScreenLineLengthValueChanged(double value);
        void onSphereLevelsValueChanged(int value);

    signals:

    private:

        Ui::VisibilityTestControlWidget m_visibilityTestControlWidgetUi;
        VisibilityTestWidgetController* m_pVisibilityTestWidgetController;

        armarx::VisibilityTestInterfacePrx m_visibilityTestProxy;

        void loadCurrentConfiguration();
        void connectSignals();
    };
}

