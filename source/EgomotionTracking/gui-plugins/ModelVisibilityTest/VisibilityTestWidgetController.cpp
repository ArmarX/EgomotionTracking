#include "VisibilityTestWidgetController.h"

using namespace visionx;
using namespace armarx;

VisibilityTestWidgetController::VisibilityTestWidgetController()
    = default;

VisibilityTestWidgetController::~VisibilityTestWidgetController()
    = default;

QPointer<QWidget> VisibilityTestWidgetController::getWidget()
{
    if (!m_visibilityTestControlWidget)
    {
        m_visibilityTestControlWidget = new VisibilityTestControlWidget(this);
    }

    return qobject_cast<QWidget*>(m_visibilityTestControlWidget);
}

void VisibilityTestWidgetController::onInitComponent()
{
    getWidget()->show();
    usingProxy("VisibilityTest");
}

void VisibilityTestWidgetController::onConnectComponent()
{
    m_visibilityTestProxy = getProxy<VisibilityTestInterfacePrx>("VisibilityTest");
    m_visibilityTestControlWidget->setVisibilityTestProxy(m_visibilityTestProxy);
}

void VisibilityTestWidgetController::onExitComponent()
{

}

void VisibilityTestWidgetController::loadSettings(QSettings* settings)
{

}

void VisibilityTestWidgetController::saveSettings(QSettings* settings)
{

}
