#include "EgomotionTrackerSessionPlayerWidgetController.h"

using namespace visionx;
using namespace armarx;

EgomotionTrackerSessionPlayerWidgetController::EgomotionTrackerSessionPlayerWidgetController()
{

}

EgomotionTrackerSessionPlayerWidgetController::~EgomotionTrackerSessionPlayerWidgetController()
{

}

QPointer<QWidget> EgomotionTrackerSessionPlayerWidgetController::getWidget()
{
    if (!m_egomotionTrackerSessionPlayerControlWidget)
    {
        m_egomotionTrackerSessionPlayerControlWidget = new EgomotionTrackerSessionPlayerControlWidget(this);
    }

    return qobject_cast<QWidget*>(m_egomotionTrackerSessionPlayerControlWidget);
}

void EgomotionTrackerSessionPlayerWidgetController::onInitComponent()
{
    getWidget()->show();
    usingProxy("EgomotionTrackerSessionPlayer");
}

void EgomotionTrackerSessionPlayerWidgetController::onConnectComponent()
{
    m_egomotionTrackerSessionPlayerProxy = getProxy<EgomotionTrackerSessionPlayerInterfacePrx>("EgomotionTrackerSessionPlayer");
    m_egomotionTrackerSessionPlayerControlWidget->setEgomotionTrackerSessionPlayerProxy(m_egomotionTrackerSessionPlayerProxy);
}

void EgomotionTrackerSessionPlayerWidgetController::onExitComponent()
{

}

void EgomotionTrackerSessionPlayerWidgetController::loadSettings(QSettings* settings)
{

}

void EgomotionTrackerSessionPlayerWidgetController::saveSettings(QSettings* settings)
{

}
