#include "EgomotionTrackerSessionPlayerControlWidget.h"
#include "EgomotionTrackerSessionPlayerWidgetController.h"

#include <iostream>

using namespace visionx;
using namespace armarx;

EgomotionTrackerSessionPlayerControlWidget::EgomotionTrackerSessionPlayerControlWidget(EgomotionTrackerSessionPlayerWidgetController* egomotionTrackerSessionPlayerWidgetController)
{
    this->m_pEgomotionTrackerSessionPlayerWidgetController = egomotionTrackerSessionPlayerWidgetController;

    m_egomotionTrackerSessionPlayerControlWidgetUi.setupUi(this);
}

EgomotionTrackerSessionPlayerControlWidget::~EgomotionTrackerSessionPlayerControlWidget()
{

}

void EgomotionTrackerSessionPlayerControlWidget::setEgomotionTrackerSessionPlayerProxy(EgomotionTrackerSessionPlayerInterfacePrx egomotionTrackerSessionPlayerProxy)
{
    this->m_egomotionTrackerSessionPlayerProxy = egomotionTrackerSessionPlayerProxy;
    LoadCurrentEgomotionTrackerSessionPlayerConfiguration();
    ConnectSignals();
}

void EgomotionTrackerSessionPlayerControlWidget::buttonStartClicked(bool toggled)
{
    m_egomotionTrackerSessionPlayerProxy->start();
    m_egomotionTrackerSessionPlayerControlWidgetUi.m_spinBoxNumberOfRepetitions->setEnabled(false);
}

void EgomotionTrackerSessionPlayerControlWidget::buttonStopClicked(bool toggled)
{
    m_egomotionTrackerSessionPlayerProxy->stop();
    m_egomotionTrackerSessionPlayerControlWidgetUi.m_spinBoxNumberOfRepetitions->setEnabled(true);
}

void EgomotionTrackerSessionPlayerControlWidget::buttonStepClicked(bool toggled)
{
    m_egomotionTrackerSessionPlayerProxy->doNextStep();
}

void EgomotionTrackerSessionPlayerControlWidget::onAutoExecutionClicked(bool toggled)
{
    m_egomotionTrackerSessionPlayerProxy->setAutoExecution(toggled);
}

void EgomotionTrackerSessionPlayerControlWidget::onNumberOfRepetitionsValueChanged()
{
    m_egomotionTrackerSessionPlayerProxy->setNumberOfRepetitions(m_egomotionTrackerSessionPlayerControlWidgetUi.m_spinBoxNumberOfRepetitions->value());
}

void EgomotionTrackerSessionPlayerControlWidget::LoadCurrentEgomotionTrackerSessionPlayerConfiguration()
{
    m_egomotionTrackerSessionPlayerControlWidgetUi.m_spinBoxNumberOfRepetitions->setValue(m_egomotionTrackerSessionPlayerProxy->getNumberOfRepetitions());
    m_egomotionTrackerSessionPlayerControlWidgetUi.m_checkBoxAuto->setChecked(m_egomotionTrackerSessionPlayerProxy->getAutoExecution());
}

void EgomotionTrackerSessionPlayerControlWidget::ConnectSignals()
{
    connect(m_egomotionTrackerSessionPlayerControlWidgetUi.m_buttonStart, SIGNAL(clicked(bool)), this, SLOT(buttonStartClicked(bool)));
    connect(m_egomotionTrackerSessionPlayerControlWidgetUi.m_buttonStop, SIGNAL(clicked(bool)), this, SLOT(buttonStopClicked(bool)));
    connect(m_egomotionTrackerSessionPlayerControlWidgetUi.m_buttonStep, SIGNAL(clicked(bool)), this, SLOT(buttonStepClicked(bool)));
    connect(m_egomotionTrackerSessionPlayerControlWidgetUi.m_checkBoxAuto, SIGNAL(clicked(bool)), this, SLOT(onAutoExecutionClicked(bool)));
    connect(m_egomotionTrackerSessionPlayerControlWidgetUi.m_spinBoxNumberOfRepetitions, SIGNAL(valueChanged(int)), this, SLOT(onNumberOfRepetitionsValueChanged()));
}


