#include "SessionPlayerParticleVisualizationWidgetController.h"

using namespace visionx;
using namespace armarx;

SessionPlayerParticleVisualizationWidgetController::SessionPlayerParticleVisualizationWidgetController()
{

}

SessionPlayerParticleVisualizationWidgetController::~SessionPlayerParticleVisualizationWidgetController()
{

}

QPointer<QWidget> SessionPlayerParticleVisualizationWidgetController::getWidget()
{
    if (!m_sessionPlayerParticleVisualizationWidget)
    {
        m_sessionPlayerParticleVisualizationWidget = new SessionPlayerParticleVisualizationWidget(this);
    }

    return qobject_cast<QWidget*>(m_sessionPlayerParticleVisualizationWidget);
}

void SessionPlayerParticleVisualizationWidgetController::onInitComponent()
{
    getWidget()->show();
    usingProxy("EgomotionTrackerSessionPlayer");
    connect(this, SIGNAL(proxyConnected()), m_sessionPlayerParticleVisualizationWidget, SLOT(initializeVisualisation()));
}

void SessionPlayerParticleVisualizationWidgetController::onConnectComponent()
{
    m_egomotionTrackerSessionPlayerProxy = getProxy<EgomotionTrackerSessionPlayerInterfacePrx>("EgomotionTrackerSessionPlayer");
    m_sessionPlayerParticleVisualizationWidget->setEgomotionTrackerSessionPlayerProxy(m_egomotionTrackerSessionPlayerProxy);
    emit proxyConnected();
}

void SessionPlayerParticleVisualizationWidgetController::onExitComponent()
{

}

void SessionPlayerParticleVisualizationWidgetController::loadSettings(QSettings* settings)
{

}

void SessionPlayerParticleVisualizationWidgetController::saveSettings(QSettings* settings)
{

}

