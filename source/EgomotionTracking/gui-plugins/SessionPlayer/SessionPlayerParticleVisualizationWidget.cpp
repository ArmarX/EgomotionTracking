#include "SessionPlayerParticleVisualizationWidget.h"

#include <EgomotionTracking/components/EgomotionTrackingConfiguration/TrackingConfiguration.h>

using namespace visionx;
using namespace armarx;

SessionPlayerParticleVisualizationWidget::SessionPlayerParticleVisualizationWidget(SessionPlayerParticleVisualizationWidgetController* pSessionPlayerParticleVisualizationWidgetController)
{
    this->m_pSessionPlayerParticleVisualizationWidgetController = pSessionPlayerParticleVisualizationWidgetController;

    m_pTracking2DVisualizationWidget = new CTracking2DVisualizationWidget(this);

    m_sessionPlayerParticleVisualizationWidgetUi.setupUi(this);
    m_sessionPlayerParticleVisualizationWidgetUi.layout->addWidget(m_pTracking2DVisualizationWidget);
    m_sessionPlayerParticleVisualizationWidgetUi.layout->setContentsMargins(0, 0, 0, 0);

    m_pUpdateTimer = new QTimer(this);

    connect(m_pUpdateTimer, SIGNAL(timeout()), this, SLOT(updateVisualisation()));
}

SessionPlayerParticleVisualizationWidget::~SessionPlayerParticleVisualizationWidget()
{

}

void SessionPlayerParticleVisualizationWidget::setEgomotionTrackerSessionPlayerProxy(EgomotionTrackerSessionPlayerInterfacePrx egomotionTrackerSessionPlayerProxy)
{
    this->m_egomotionTrackerSessionPlayerProxy = egomotionTrackerSessionPlayerProxy;
}

void SessionPlayerParticleVisualizationWidget::initializeVisualisation()
{
    m_pTrackingVisualizationScene = new CTracking2DVisualizationScene();
    m_pTracking2DVisualizationWidget->SetTrackingVisualizationScene(m_pTrackingVisualizationScene);

    BoundingBox boundingBox = m_egomotionTrackerSessionPlayerProxy->getBoundingBox();
    m_pTrackingVisualizationScene->AddModelObject(boundingBox.min[0], boundingBox.min[1], boundingBox.max[0], boundingBox.max[1], QString("Kitchen"));
    m_pTrackingVisualizationScene->SetVisualizationArea(configuration_trackingVisualizationAreaMinX, configuration_trackingVisualizationAreaMinY, configuration_trackingVisualizationAreaMaxX, configuration_trackingVisualizationAreaMaxY);
    m_visualizationParticles.resize(configuration_maxParticles, CLocationParticle(CLocationState(1000, 1000, 0)));

    m_pTrackingParticlesGraphicsGroup = new CLocationParticleGraphicsGroup();

    for (std::vector<CLocationParticle>::iterator particleIterator = m_visualizationParticles.begin(); particleIterator != m_visualizationParticles.end(); ++particleIterator)
    {
        particleIterator->SetActive(false);
        m_pTrackingParticlesGraphicsGroup->AddLocationParticle(&(*particleIterator));
    }

    m_pTrackingVisualizationScene->AddLocationParticleGroup(m_pTrackingParticlesGraphicsGroup);

    m_pTrackingParticlesGraphicsGroup->SetVisible(false);

    m_pTracking2DVisualizationWidget->show();
    m_pTrackingVisualizationScene->Update();

    m_pUpdateTimer->start(50);
}

void SessionPlayerParticleVisualizationWidget::updateVisualisation()
{
    LocationEstimate grTruth = m_egomotionTrackerSessionPlayerProxy->getGroundTruthLocation();
    CLocationState groundTruthLocation;
    groundTruthLocation.SetX(grTruth.x);
    groundTruthLocation.SetY(grTruth.y);
    groundTruthLocation.SetAlpha(grTruth.alpha);
    m_pTrackingVisualizationScene->SetGroundTruthRobotLocation(&groundTruthLocation);

    LocationEstimate locEst = m_egomotionTrackerSessionPlayerProxy->getLocationEstimate();
    CLocationState locationEstimate;
    locationEstimate.SetX(locEst.x);
    locationEstimate.SetY(locEst.y);
    locationEstimate.SetAlpha(locEst.alpha);
    m_pTrackingVisualizationScene->SetEstimatedRobotLocation(&locationEstimate);

    PlaneScatteringDirections planeScatteringDirectionsPF = m_egomotionTrackerSessionPlayerProxy->getPlaneScatteringDirections();
    CPlaneScatteringDirections* planeScatteringDirectionsTracking = new CPlaneScatteringDirections();
    Vec2d temp1;
    temp1.x = planeScatteringDirectionsPF.meanScattDir.x;
    temp1.y = planeScatteringDirectionsPF.meanScattDir.y;
    planeScatteringDirectionsTracking->SetMean(temp1);
    Vec2d temp2;
    temp2.x = planeScatteringDirectionsPF.direction1.x;
    temp2.y = planeScatteringDirectionsPF.direction1.y;
    planeScatteringDirectionsTracking->setDirection1(temp2);
    planeScatteringDirectionsTracking->setSigma1(planeScatteringDirectionsPF.sigma1);
    planeScatteringDirectionsTracking->setSigma2(planeScatteringDirectionsPF.sigma2);
    m_pTrackingParticlesGraphicsGroup->SetPlaneScatteringDirections(planeScatteringDirectionsTracking);

    int numberOfActiveParticles = m_egomotionTrackerSessionPlayerProxy->getNumberOfActiveParticles();

    ParticleVector particlesPF = m_egomotionTrackerSessionPlayerProxy->getParticles();
    int i = 0;

    for (std::vector<Particle>::const_iterator particleIterator = particlesPF.begin(); particleIterator != particlesPF.end(); ++particleIterator, ++i)
    {
        m_visualizationParticles[i].SetNeckRollError(particleIterator->neckRollError);
        m_visualizationParticles[i].SetNeckPitchError(particleIterator->neckPitchError);
        m_visualizationParticles[i].SetNeckYawError(particleIterator->neckYawError);
        m_visualizationParticles[i].SetX(particleIterator->locEst.x);
        m_visualizationParticles[i].SetY(particleIterator->locEst.y);
        m_visualizationParticles[i].SetAlpha(particleIterator->locEst.alpha);
        m_visualizationParticles[i].SetImportanceWeight(particleIterator->importanceWeight);
        m_visualizationParticles[i].SetActive(true);
    }

    for (uint j = numberOfActiveParticles; j < m_visualizationParticles.size(); j++)
    {
        m_visualizationParticles[j].SetActive(false);
    }

    EgomotionTrackerSessionPlayerStatus status = m_egomotionTrackerSessionPlayerProxy->getStatus();

    switch (status)
    {
        case eSessionPlayerIdle:
            m_pTrackingParticlesGraphicsGroup->SetVisible(false);
            break;

        case ePlayingSession:
            m_pTrackingParticlesGraphicsGroup->SetVisible(true);
            break;
    }

    m_pTrackingVisualizationScene->Update();
}
