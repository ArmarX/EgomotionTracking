#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <VisionX/interface/components/EgomotionTrackerSessionPlayerInterface.h>

#include "SessionPlayerParticleVisualizationWidget.h"

namespace visionx
{
    class SessionPlayerParticleVisualizationWidgetController :
        public armarx::ArmarXComponentWidgetController
    {
        Q_OBJECT
    public:
        SessionPlayerParticleVisualizationWidgetController();
        ~SessionPlayerParticleVisualizationWidgetController() override;

        QPointer<QWidget> getWidget() override;
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        QString getWidgetName() const override
        {
            return "EgomotionTracking.SessionPlayerParticleVisualization";
        }
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

    signals:
        void proxyConnected();

    public slots:

    private:
        QPointer<SessionPlayerParticleVisualizationWidget> m_sessionPlayerParticleVisualizationWidget;
        armarx::EgomotionTrackerSessionPlayerInterfacePrx m_egomotionTrackerSessionPlayerProxy;
    };
}

