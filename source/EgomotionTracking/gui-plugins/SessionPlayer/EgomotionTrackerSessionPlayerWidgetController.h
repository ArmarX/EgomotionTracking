#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <VisionX/interface/components/EgomotionTrackerSessionPlayerInterface.h>

#include "EgomotionTrackerSessionPlayerControlWidget.h"

namespace visionx
{
    class EgomotionTrackerSessionPlayerWidgetController :
        public armarx::ArmarXComponentWidgetController
    {
        Q_OBJECT
    public:
        EgomotionTrackerSessionPlayerWidgetController();
        ~EgomotionTrackerSessionPlayerWidgetController() override;

        QPointer<QWidget> getWidget() override;
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        QString getWidgetName() const override
        {
            return "EgomotionTracking.SessionPlayer";
        }
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

    signals:

    public slots:

    private:
        QPointer<EgomotionTrackerSessionPlayerControlWidget> m_egomotionTrackerSessionPlayerControlWidget;
        armarx::EgomotionTrackerSessionPlayerInterfacePrx m_egomotionTrackerSessionPlayerProxy;
    };
}

