#pragma once

#include <EgomotionTracking/gui-plugins/SessionPlayer/ui_EgomotionTrackerSessionPlayerControlWidget.h>

#include <VisionX/interface/components/EgomotionTrackerSessionPlayerInterface.h>

namespace visionx
{

    class EgomotionTrackerSessionPlayerWidgetController;

    class EgomotionTrackerSessionPlayerControlWidget :
        public QWidget
    {
        Q_OBJECT
    public:
        EgomotionTrackerSessionPlayerControlWidget(EgomotionTrackerSessionPlayerWidgetController* controller);
        ~EgomotionTrackerSessionPlayerControlWidget() override;
        void setEgomotionTrackerSessionPlayerProxy(armarx::EgomotionTrackerSessionPlayerInterfacePrx egomotionTrackerSessionPlayerProxy);

    public slots:
        void buttonStartClicked(bool toggled);
        void buttonStopClicked(bool toggled);
        void buttonStepClicked(bool toggled);
        void onAutoExecutionClicked(bool toggled);
        void onNumberOfRepetitionsValueChanged();

    signals:

    private:
        Ui::EgomotionTrackerSessionPlayerControlWidget m_egomotionTrackerSessionPlayerControlWidgetUi;
        EgomotionTrackerSessionPlayerWidgetController* m_pEgomotionTrackerSessionPlayerWidgetController;

        armarx::EgomotionTrackerSessionPlayerInterfacePrx m_egomotionTrackerSessionPlayerProxy;

        void LoadCurrentEgomotionTrackerSessionPlayerConfiguration();
        void ConnectSignals();
    };
}

