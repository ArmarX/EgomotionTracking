#pragma once

#include <EgomotionTracking/gui-plugins/SessionPlayer/ui_SessionPlayerParticleVisualizationWidget.h>

#include <VisionX/interface/components/EgomotionTrackerSessionPlayerInterface.h>

#include <EgomotionTracking/components/EgomotionTracking/BasicTypes/LocationSwarmParticle.h>

#include <EgomotionTracking/components/EgomotionTrackingVisualization/2D/TrackingVisualization/Tracking2DVisualizationScene.h>
#include <EgomotionTracking/components/EgomotionTrackingVisualization/2D/TrackingVisualization/Tracking2DVisualizationWidget.h>

#include <QTimer>

namespace visionx
{

    class SessionPlayerParticleVisualizationWidgetController;

    class SessionPlayerParticleVisualizationWidget :
        public QWidget
    {
        Q_OBJECT
    public:
        SessionPlayerParticleVisualizationWidget(SessionPlayerParticleVisualizationWidgetController* pSessionPlayerParticleVisualizationWidgetController);
        ~SessionPlayerParticleVisualizationWidget() override;
        void setEgomotionTrackerSessionPlayerProxy(armarx::EgomotionTrackerSessionPlayerInterfacePrx egomotionTrackerSessionPlayerProxy);

    public slots:
        void initializeVisualisation();
        void updateVisualisation();

    signals:

    private:
        Ui::SessionPlayerParticleVisualizationWidget m_sessionPlayerParticleVisualizationWidgetUi;
        SessionPlayerParticleVisualizationWidgetController* m_pSessionPlayerParticleVisualizationWidgetController;

        armarx::EgomotionTrackerSessionPlayerInterfacePrx m_egomotionTrackerSessionPlayerProxy;

        CTracking2DVisualizationScene* m_pTrackingVisualizationScene;
        CTracking2DVisualizationWidget* m_pTracking2DVisualizationWidget;

        std::vector<CLocationParticle> m_visualizationParticles;
        CLocationParticleGraphicsGroup* m_pTrackingParticlesGraphicsGroup;

        QTimer* m_pUpdateTimer;
    };
}

